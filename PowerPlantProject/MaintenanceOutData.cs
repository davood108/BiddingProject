﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using ILOG.Concert;
using ILOG.CPLEX;
using NRI.SBS.Common;
namespace PowerPlantProject
{
    public class MaintenanceOutData
    {
        List<CMatrix> result;
        public List<CMatrix> Result_Plants_UnitsNum_Week
        {
            get { return result; }
            set { result = value; }
        }

        private string[] plantName;
        public string[] PlantsName
        {
            get { return plantName; }
            set { plantName = value; }
        }

        private string[] plant;
        public string[] Plants
        {
            get { return plant; }
            set { plant = value; }
        }

        int[] Plant_unitsNum;
        public int[] Plants_UnitsNum
        {
            get { return Plant_unitsNum; }
            set { Plant_unitsNum = value; }
        }

        string[,] Plant_UnitCode;
        public string[,] Plants_UnitCodes
        {
            get { return Plant_UnitCode; }
            set { Plant_UnitCode = value; }
        }

        private bool[] availableData;

        public bool[] AvailableData
        {
            get { return availableData; }
            set { availableData = value; }
        }
    }
}
