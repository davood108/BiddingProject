﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Windows.Forms;


namespace PowerPlantProject
{
    struct Computer
    {
        public string ComputerName,ServerName, DBName, UserName, Password;
        public int Timeout;
        
    }
    class Sync
    {
        public event EventHandler OnChangeProgress;
        int _progress;
        public int Progress { get { return _progress; }
            set
            {
                _progress = value;
                if (OnChangeProgress != null)
                    OnChangeProgress(this, new EventArgs());
            }
        }
        string _configsFileName, _tableFileName;
        string _sourceComputerName, _destComputerName;
        SqlConnection SourceConnection = new SqlConnection();
        SqlConnection DestConnection = new SqlConnection();
        SqlConnectionStringBuilder SourceBuilder = new SqlConnectionStringBuilder();
        SqlConnectionStringBuilder DestBuilder = new SqlConnectionStringBuilder();
        XmlDocument doc_Tables;
        XmlDocument doc_Configs;
        int currentIndex;
        int _tablesCount;

        string[] TableNames;
        string[] TableColumns;

        public int TablesCount { get { return _tablesCount; } }

        delegate void MyDelegate();
        MyDelegate del;

        public Sync( string ConfigsFileName, string TableFileName)
        {
            this.del += open;
            this.del = new MyDelegate(close);
            _configsFileName = ConfigsFileName;
            doc_Configs = new XmlDocument();
            doc_Configs.Load(_configsFileName);
            _tableFileName = TableFileName;
            currentIndex = 0;
            doc_Tables = new XmlDocument();
            doc_Tables.Load(_tableFileName);
            XmlNodeList tableNodes = doc_Tables.SelectNodes("Body/Table");
            _tablesCount = tableNodes.Count;
            TableNames = new string[_tablesCount];
            TableColumns = new string[_tablesCount];
            int t = 0;
            foreach (XmlNode tableNode in tableNodes)
            {
                TableNames[t] = tableNode.SelectSingleNode("Name").InnerText;
                TableColumns[t] = tableNode.SelectSingleNode("Column").InnerText;
                t++;
            }

        }

        /// <summary>
        /// Update Next Table 
        /// </summary>

        public void open()
        {
            currentIndex = 0;
            try
            {
                DestConnection.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show("Can not connect to " + _destComputerName + "\n Exception : " + e.Message);
            }
            try
            {
                SourceConnection.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show("Can not connect to "+_sourceComputerName +"\n Exception : "+e.Message);
            }
           
           
        }
        public void close()
        {
            currentIndex = 0;
            DestConnection.Close();
            SourceConnection.Close();
        }
        public bool UpdateNextTable(ref string TableName, ref string Status, ref int UpdatedRowsNO)
        {
           
            bool finishTables = false;
            if (currentIndex < _tablesCount)
            {
                TableName = TableNames[currentIndex];
               
                try
                {
                    string lastDate = GetLastDate(TableNames[currentIndex], TableColumns[currentIndex]);
                    SqlCommand dataCommand = new SqlCommand();
                    dataCommand.Connection = DestConnection;
                    dataCommand.CommandType = CommandType.Text;
                    dataCommand.CommandText = "SELECT * FROM " + TableNames[currentIndex] + " WHERE " + TableColumns[currentIndex] + ">'" + lastDate + "'";
                    if (TableName == "DetailFRM002") dataCommand.CommandText = "SELECT * FROM " + TableNames[currentIndex] + " WHERE " + TableColumns[currentIndex] + ">'" + lastDate + "' and  estimated=0";

                    DataTable dt = new System.Data.DataTable();
                    //DataReader:
                    SqlDataReader dr = dataCommand.ExecuteReader();
                    dt.Load(dr);
                    dr.Close();

                    SqlBulkCopy bulkCopy = new SqlBulkCopy(SourceConnection);
                    bulkCopy.DestinationTableName = TableNames[currentIndex];
                    bulkCopy.WriteToServer(dt);
                    UpdatedRowsNO = dt.Rows.Count;
                    if (UpdatedRowsNO > 0)
                        Status = "Updated";
                    else
                        Status = "Not Updated";


                }
                catch
                {
                    Status = "Error";
                }
                currentIndex++;
                Progress = currentIndex * 100 / TablesCount;
            }
            else
                finishTables = true;
            return !finishTables;
        }
        /// <summary>
        /// Update all tables and return a datatable showing statuses 
        /// </summary>
        public DataTable UpdateAll()
        {
            if (SourceConnection.State == ConnectionState.Closed || DestConnection.State == ConnectionState.Closed)
            {
                throw new Exception("Connections are closed , Call 'open' method");
            }
            else
            {
                DataTable statusDT = new DataTable();
                statusDT.Columns.Add("Table Name");
                statusDT.Columns.Add("Status");
                statusDT.Columns.Add("Updated Rows NO");
                XmlNodeList tableNodes = doc_Tables.SelectNodes("Body/Table");
                int TablesCount = tableNodes.Count;
                TableNames = new string[TablesCount];
                TableColumns = new string[TablesCount];
                int t = 0;
                foreach (XmlNode tableNode in tableNodes)
                {
                    TableNames[t] = tableNode.SelectSingleNode("Name").InnerText;
                    TableColumns[t] = tableNode.SelectSingleNode("Column").InnerText;
                    statusDT.Rows.Add();
                    statusDT.Rows[t]["Table Name"] = TableNames[t];
                    t++;
                }


                for (int i = 0; i < TablesCount; i++)
                {

                    try
                    {
                        string lastDate = GetLastDate(TableNames[i], TableColumns[i]);
                        SqlCommand dataCommand = new SqlCommand();
                        dataCommand.Connection = DestConnection;
                        dataCommand.CommandType = CommandType.Text;
                        dataCommand.CommandText = "SELECT * FROM " + TableNames[i] + " WHERE " + TableColumns[i] + ">'" + lastDate + "'";
                        DataTable dt = new System.Data.DataTable();
                        //DataReader:
                        SqlDataReader dr = dataCommand.ExecuteReader();
                        dt.Load(dr);
                        dr.Close();

                        SqlBulkCopy bulkCopy = new SqlBulkCopy(SourceConnection);
                        bulkCopy.DestinationTableName = TableNames[i];
                        bulkCopy.WriteToServer(dt);
                        statusDT.Rows[i]["Updated Rows NO"] = dt.Rows.Count;
                        if (dt.Rows.Count > 0)
                            statusDT.Rows[i]["Status"] = "Updated";
                        else
                            statusDT.Rows[i]["Status"] = "Not Updated";

                    }
                    catch
                    {
                        statusDT.Rows[i]["Status"] = "Error";
                    }
                }

                return statusDT;
            }

        }
        string GetLastDate(string TableName, string ColumnName)
        {
            //SourceConnection.Open();
            SqlCommand dataCommand = new SqlCommand();
            dataCommand.Connection = SourceConnection;
            dataCommand.CommandType = CommandType.Text;
            dataCommand.CommandText = "SELECT MAX(" + ColumnName + ") FROM " + TableName;
            if (TableName == "DetailFRM002") dataCommand.CommandText = "SELECT MAX(" + ColumnName + ") FROM " + TableName + " where estimated=0";

            SqlDataReader dr = dataCommand.ExecuteReader();
            string maxDate = "1388/01/01";
            try
            {
                if (dr.Read())
                {
                    maxDate = dr.GetString(0);
                }
            }
            catch { }
            finally
            {
                dr.Close();
            }
            //SourceConnection.Close();
            return maxDate;
        }

        public string[] GetComputers()
        {
            string[] computers;
            //XmlDocument doc = new XmlDocument();
            //doc.Load(_configsFileName);
            XmlNodeList computerNodes = doc_Configs.SelectNodes("Body/Computer");
            computers = new string[computerNodes.Count];
            int i = 0;
            foreach (XmlNode node in computerNodes)
            {
                var x = node.Attributes;
                computers[i] = x[0].Value;
                //computers[i]=node.SelectSingleNode("ComputerName").InnerText;
                i++;
            }
            return computers;
        }
        /// <summary>
        /// Initilize source and destination connections
        /// </summary>
       
        public void SetConnections(string SourceComputerName, string DestComputerName)
        {
            //XmlDocument doc = new XmlDocument();
            //doc.Load("Configs.xml");
            _sourceComputerName = SourceComputerName;
            _destComputerName = DestComputerName;
            XmlNode sourceNode = doc_Configs.SelectSingleNode("Body/Computer [@Name='" + SourceComputerName + "']");
            XmlNode destNode = doc_Configs.SelectSingleNode("Body/Computer [@Name='" + DestComputerName + "']");
            
           
            SourceBuilder.DataSource = sourceNode.SelectSingleNode("ServerName").InnerText;
            SourceBuilder.InitialCatalog = sourceNode.SelectSingleNode("DBName").InnerText;
            SourceBuilder.UserID = sourceNode.SelectSingleNode("UserName").InnerText;
            SourceBuilder.Password = sourceNode.SelectSingleNode("Password").InnerText;
            SourceBuilder.ConnectTimeout = Convert.ToInt32(sourceNode.SelectSingleNode("Timeout").InnerText);

            SourceConnection.ConnectionString = SourceBuilder.ConnectionString;

            DestBuilder.DataSource = destNode.SelectSingleNode("ServerName").InnerText;
            DestBuilder.InitialCatalog = destNode.SelectSingleNode("DBName").InnerText;
            DestBuilder.UserID = destNode.SelectSingleNode("UserName").InnerText;
            DestBuilder.Password = destNode.SelectSingleNode("Password").InnerText;
            DestBuilder.ConnectTimeout = Convert.ToInt32(destNode.SelectSingleNode("Timeout").InnerText);

            DestConnection.ConnectionString = DestBuilder.ConnectionString;

        }

        public Computer GetComputerInfo(string ComputerName) 
        {
            Computer temp = new Computer();
            XmlNode Node = doc_Configs.SelectSingleNode("Body/Computer [@Name='" + ComputerName + "']");
            temp.ComputerName = ComputerName;
            temp.ServerName= Node.SelectSingleNode("ServerName").InnerText;
            temp.DBName = Node.SelectSingleNode("DBName").InnerText;
            temp.UserName = Node.SelectSingleNode("UserName").InnerText;
            //temp.Password = Node.SelectSingleNode("Password").InnerText;
            temp.Password = "";//The user must not see the password 
            temp.Timeout = Convert.ToInt32(Node.SelectSingleNode("Timeout").InnerText);
            return temp;
        }

        public void SetComputerInfo(string OldComputerName,Computer com) 
        {
            XmlNode Node = doc_Configs.SelectSingleNode("Body/Computer [@Name='" + OldComputerName + "']");
            Node.Attributes[0].Value = com.ComputerName;
            Node.SelectSingleNode("ServerName").InnerText= com.ServerName ;

             Node.SelectSingleNode("DBName").InnerText=com.DBName ;
             Node.SelectSingleNode("UserName").InnerText = com.UserName;
            if(com.Password.Trim()!="")
                Node.SelectSingleNode("Password").InnerText = com.Password; 
             Node.SelectSingleNode("Timeout").InnerText = com.Timeout.ToString();
             doc_Configs.Save(_configsFileName);
        }

    }
}
