﻿namespace PowerPlantProject
{
    partial class NameFormat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NameFormat));
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboBox24 = new System.Windows.Forms.ComboBox();
            this.comboBox22 = new System.Windows.Forms.ComboBox();
            this.comboBox21 = new System.Windows.Forms.ComboBox();
            this.comboBox23 = new System.Windows.Forms.ComboBox();
            this.comboBox20 = new System.Windows.Forms.ComboBox();
            this.comboBox19 = new System.Windows.Forms.ComboBox();
            this.comboBox18 = new System.Windows.Forms.ComboBox();
            this.comboBox17 = new System.Windows.Forms.ComboBox();
            this.comboBox16 = new System.Windows.Forms.ComboBox();
            this.comboBox15 = new System.Windows.Forms.ComboBox();
            this.comboBox14 = new System.Windows.Forms.ComboBox();
            this.comboBox13 = new System.Windows.Forms.ComboBox();
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.comboBox12 = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cmbsheet = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.btnsave = new System.Windows.Forms.Button();
            this.rdformatmarket = new System.Windows.Forms.RadioButton();
            this.rdEms = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "day/1char",
            "Date Without Seperator/8char",
            "PlantName/English",
            "PlantName/Farsi",
            "MonthName/Finglish",
            "Year-6MaheAval",
            "Year-6MaheDovom"});
            this.comboBox1.Location = new System.Drawing.Point(5, 89);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(104, 21);
            this.comboBox1.TabIndex = 1;
            // 
            // comboBox2
            // 
            this.comboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "day/1char",
            "Date Without Seperator/8char",
            "PlantName/English",
            "PlantName/Farsi",
            "MonthName/Finglish",
            "Year-6MaheAval",
            "Year-6MaheDovom"});
            this.comboBox2.Location = new System.Drawing.Point(122, 89);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(104, 21);
            this.comboBox2.TabIndex = 1;
            // 
            // comboBox3
            // 
            this.comboBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "day/1char",
            "Date Without Seperator/8char",
            "PlantName/English",
            "PlantName/Farsi",
            "MonthName/Finglish",
            "Year-6MaheAval",
            "Year-6MaheDovom"});
            this.comboBox3.Location = new System.Drawing.Point(236, 89);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(104, 21);
            this.comboBox3.TabIndex = 1;
            // 
            // comboBox4
            // 
            this.comboBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "day/1char",
            "Date Without Seperator/8char",
            "PlantName/English",
            "PlantName/Farsi",
            "MonthName/Finglish",
            "Year-6MaheAval",
            "Year-6MaheDovom"});
            this.comboBox4.Location = new System.Drawing.Point(352, 89);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(104, 21);
            this.comboBox4.TabIndex = 1;
            // 
            // comboBox5
            // 
            this.comboBox5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "day/1char",
            "Date Without Seperator/8char",
            "PlantName/English",
            "PlantName/Farsi",
            "MonthName/Finglish",
            "Year-6MaheAval",
            "Year-6MaheDovom"});
            this.comboBox5.Location = new System.Drawing.Point(466, 89);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(104, 21);
            this.comboBox5.TabIndex = 1;
            // 
            // comboBox6
            // 
            this.comboBox6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Items.AddRange(new object[] {
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "day/1char",
            "Date Without Seperator/8char",
            "PlantName/English",
            "PlantName/Farsi",
            "MonthName/Finglish",
            "Year-6MaheAval",
            "Year-6MaheDovom"});
            this.comboBox6.Location = new System.Drawing.Point(579, 89);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(104, 21);
            this.comboBox6.TabIndex = 1;
            // 
            // comboBox7
            // 
            this.comboBox7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Items.AddRange(new object[] {
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "day/1char",
            "Date Without Seperator/8char",
            "PlantName/English",
            "PlantName/Farsi",
            "MonthName/Finglish",
            "Year-6MaheAval",
            "Year-6MaheDovom"});
            this.comboBox7.Location = new System.Drawing.Point(689, 89);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(104, 21);
            this.comboBox7.TabIndex = 1;
            // 
            // comboBox8
            // 
            this.comboBox8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Items.AddRange(new object[] {
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "day/1char",
            "Date Without Seperator/8char",
            "PlantName/English",
            "PlantName/Farsi",
            "MonthName/Finglish",
            "Year-6MaheAval",
            "Year-6MaheDovom"});
            this.comboBox8.Location = new System.Drawing.Point(799, 89);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(104, 21);
            this.comboBox8.TabIndex = 1;
            // 
            // comboBox9
            // 
            this.comboBox9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Items.AddRange(new object[] {
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "day/1char",
            "Date Without Seperator/8char",
            "PlantName/English",
            "PlantName/Farsi",
            "MonthName/Finglish",
            "Year-6MaheAval",
            "Year-6MaheDovom"});
            this.comboBox9.Location = new System.Drawing.Point(909, 89);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(104, 21);
            this.comboBox9.TabIndex = 1;
            // 
            // comboBox10
            // 
            this.comboBox10.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Items.AddRange(new object[] {
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "day/1char",
            "Date Without Seperator/8char",
            "PlantName/English",
            "PlantName/Farsi",
            "MonthName/Finglish",
            "Year-6MaheAval",
            "Year-6MaheDovom"});
            this.comboBox10.Location = new System.Drawing.Point(467, 134);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(104, 21);
            this.comboBox10.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 183);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Hard :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ftp :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label3.Location = new System.Drawing.Point(112, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "/";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label4.Location = new System.Drawing.Point(224, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "/";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label5.Location = new System.Drawing.Point(346, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "/";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label6.Location = new System.Drawing.Point(461, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "/";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label7.Location = new System.Drawing.Point(578, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "/";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label8.Location = new System.Drawing.Point(691, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "/";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label9.Location = new System.Drawing.Point(810, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "/";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label10.Location = new System.Drawing.Point(924, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "/";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label11.Location = new System.Drawing.Point(1039, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "/";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label12.Location = new System.Drawing.Point(112, 183);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "\\";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label13.Location = new System.Drawing.Point(224, 183);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "\\";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label14.Location = new System.Drawing.Point(346, 183);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(13, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "\\";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label15.Location = new System.Drawing.Point(461, 183);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 13);
            this.label15.TabIndex = 5;
            this.label15.Text = "\\";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label16.Location = new System.Drawing.Point(578, 183);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(13, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "\\";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label17.Location = new System.Drawing.Point(691, 183);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(13, 13);
            this.label17.TabIndex = 5;
            this.label17.Text = "\\";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label18.Location = new System.Drawing.Point(810, 183);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 13);
            this.label18.TabIndex = 5;
            this.label18.Text = "\\";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label19.Location = new System.Drawing.Point(924, 183);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 13);
            this.label19.TabIndex = 5;
            this.label19.Text = "\\";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label20.Location = new System.Drawing.Point(1039, 183);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(13, 13);
            this.label20.TabIndex = 5;
            this.label20.Text = "\\";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(1, 9);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(140, 13);
            this.label21.TabIndex = 6;
            this.label21.Text = "Please SelectFormat of Files";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.comboBox5);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.comboBox10);
            this.panel1.Controls.Add(this.comboBox2);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.comboBox3);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.comboBox4);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.comboBox6);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.comboBox7);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.comboBox8);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.comboBox9);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(1, 52);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1018, 207);
            this.panel1.TabIndex = 7;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.comboBox24);
            this.panel2.Controls.Add(this.comboBox22);
            this.panel2.Controls.Add(this.comboBox21);
            this.panel2.Controls.Add(this.comboBox23);
            this.panel2.Controls.Add(this.comboBox20);
            this.panel2.Controls.Add(this.comboBox19);
            this.panel2.Controls.Add(this.comboBox18);
            this.panel2.Controls.Add(this.comboBox17);
            this.panel2.Controls.Add(this.comboBox16);
            this.panel2.Controls.Add(this.comboBox15);
            this.panel2.Controls.Add(this.comboBox14);
            this.panel2.Controls.Add(this.comboBox13);
            this.panel2.Controls.Add(this.comboBox11);
            this.panel2.Controls.Add(this.comboBox12);
            this.panel2.Location = new System.Drawing.Point(4, 301);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1015, 160);
            this.panel2.TabIndex = 8;
            // 
            // comboBox24
            // 
            this.comboBox24.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox24.FormattingEnabled = true;
            this.comboBox24.Items.AddRange(new object[] {
            "FRM002",
            "FRM005",
            "FRM0022",
            "M002",
            "M005",
            "M009",
            "Plantname/english",
            "Plantname/Farsi",
            "PlantID",
            "Date Without Seperator/8char",
            "Date With Seperator/10char",
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "(",
            ")",
            "_",
            "-",
            "space",
            "VersionNumber/max10"});
            this.comboBox24.Location = new System.Drawing.Point(666, 97);
            this.comboBox24.Name = "comboBox24";
            this.comboBox24.Size = new System.Drawing.Size(104, 21);
            this.comboBox24.TabIndex = 1;
            // 
            // comboBox22
            // 
            this.comboBox22.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox22.FormattingEnabled = true;
            this.comboBox22.Items.AddRange(new object[] {
            "FRM002",
            "FRM005",
            "FRM0022",
            "M002",
            "M005",
            "M009",
            "Plantname/english",
            "Plantname/Farsi",
            "PlantID",
            "Date Without Seperator/8char",
            "Date With Seperator/10char",
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "(",
            ")",
            "_",
            "-",
            "space",
            "VersionNumber/max10"});
            this.comboBox22.Location = new System.Drawing.Point(446, 97);
            this.comboBox22.Name = "comboBox22";
            this.comboBox22.Size = new System.Drawing.Size(104, 21);
            this.comboBox22.TabIndex = 1;
            // 
            // comboBox21
            // 
            this.comboBox21.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox21.FormattingEnabled = true;
            this.comboBox21.Items.AddRange(new object[] {
            "FRM002",
            "FRM005",
            "FRM0022",
            "M002",
            "M005",
            "M009",
            "Plantname/english",
            "Plantname/Farsi",
            "PlantID",
            "Date Without Seperator/8char",
            "Date With Seperator/10char",
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "(",
            ")",
            "_",
            "-",
            "space",
            "VersionNumber/max10"});
            this.comboBox21.Location = new System.Drawing.Point(336, 97);
            this.comboBox21.Name = "comboBox21";
            this.comboBox21.Size = new System.Drawing.Size(104, 21);
            this.comboBox21.TabIndex = 1;
            // 
            // comboBox23
            // 
            this.comboBox23.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox23.FormattingEnabled = true;
            this.comboBox23.Items.AddRange(new object[] {
            "FRM002",
            "FRM005",
            "FRM0022",
            "M002",
            "M005",
            "M009",
            "Plantname/english",
            "Plantname/Farsi",
            "PlantID",
            "Date Without Seperator/8char",
            "Date With Seperator/10char",
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "(",
            ")",
            "_",
            "-",
            "space",
            "VersionNumber/max10"});
            this.comboBox23.Location = new System.Drawing.Point(556, 97);
            this.comboBox23.Name = "comboBox23";
            this.comboBox23.Size = new System.Drawing.Size(104, 21);
            this.comboBox23.TabIndex = 1;
            // 
            // comboBox20
            // 
            this.comboBox20.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox20.FormattingEnabled = true;
            this.comboBox20.Items.AddRange(new object[] {
            "FRM002",
            "FRM005",
            "FRM0022",
            "M002",
            "M005",
            "M009",
            "Plantname/english",
            "Plantname/Farsi",
            "PlantID",
            "Date Without Seperator/8char",
            "Date With Seperator/10char",
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "(",
            ")",
            "_",
            "-",
            "space",
            "VersionNumber/max10"});
            this.comboBox20.Location = new System.Drawing.Point(226, 97);
            this.comboBox20.Name = "comboBox20";
            this.comboBox20.Size = new System.Drawing.Size(104, 21);
            this.comboBox20.TabIndex = 1;
            // 
            // comboBox19
            // 
            this.comboBox19.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox19.FormattingEnabled = true;
            this.comboBox19.Items.AddRange(new object[] {
            "FRM002",
            "FRM005",
            "FRM0022",
            "M002",
            "M005",
            "M009",
            "Plantname/english",
            "Plantname/Farsi",
            "PlantID",
            "Date Without Seperator/8char",
            "Date With Seperator/10char",
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "(",
            ")",
            "_",
            "-",
            "space",
            "VersionNumber/max10"});
            this.comboBox19.Location = new System.Drawing.Point(906, 48);
            this.comboBox19.Name = "comboBox19";
            this.comboBox19.Size = new System.Drawing.Size(104, 21);
            this.comboBox19.TabIndex = 1;
            // 
            // comboBox18
            // 
            this.comboBox18.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox18.FormattingEnabled = true;
            this.comboBox18.Items.AddRange(new object[] {
            "FRM002",
            "FRM005",
            "FRM0022",
            "M002",
            "M005",
            "M009",
            "Plantname/english",
            "Plantname/Farsi",
            "PlantID",
            "Date Without Seperator/8char",
            "Date With Seperator/10char",
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "(",
            ")",
            "_",
            "-",
            "space",
            "VersionNumber/max10"});
            this.comboBox18.Location = new System.Drawing.Point(796, 48);
            this.comboBox18.Name = "comboBox18";
            this.comboBox18.Size = new System.Drawing.Size(104, 21);
            this.comboBox18.TabIndex = 1;
            // 
            // comboBox17
            // 
            this.comboBox17.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox17.FormattingEnabled = true;
            this.comboBox17.Items.AddRange(new object[] {
            "FRM002",
            "FRM005",
            "FRM0022",
            "M002",
            "M005",
            "M009",
            "Plantname/english",
            "Plantname/Farsi",
            "PlantID",
            "Date Without Seperator/8char",
            "Date With Seperator/10char",
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "(",
            ")",
            "_",
            "-",
            "space",
            "VersionNumber/max10"});
            this.comboBox17.Location = new System.Drawing.Point(687, 48);
            this.comboBox17.Name = "comboBox17";
            this.comboBox17.Size = new System.Drawing.Size(104, 21);
            this.comboBox17.TabIndex = 1;
            // 
            // comboBox16
            // 
            this.comboBox16.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox16.FormattingEnabled = true;
            this.comboBox16.Items.AddRange(new object[] {
            "FRM002",
            "FRM005",
            "FRM0022",
            "M002",
            "M005",
            "M009",
            "Plantname/english",
            "Plantname/Farsi",
            "PlantID",
            "Date Without Seperator/8char",
            "Date With Seperator/10char",
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "(",
            ")",
            "_",
            "-",
            "space",
            "VersionNumber/max10"});
            this.comboBox16.Location = new System.Drawing.Point(576, 48);
            this.comboBox16.Name = "comboBox16";
            this.comboBox16.Size = new System.Drawing.Size(104, 21);
            this.comboBox16.TabIndex = 1;
            // 
            // comboBox15
            // 
            this.comboBox15.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox15.FormattingEnabled = true;
            this.comboBox15.Items.AddRange(new object[] {
            "FRM002",
            "FRM005",
            "FRM0022",
            "M002",
            "M005",
            "M009",
            "Plantname/english",
            "Plantname/Farsi",
            "PlantID",
            "Date Without Seperator/8char",
            "Date With Seperator/10char",
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "(",
            ")",
            "_",
            "-",
            "space",
            "VersionNumber/max10"});
            this.comboBox15.Location = new System.Drawing.Point(463, 48);
            this.comboBox15.Name = "comboBox15";
            this.comboBox15.Size = new System.Drawing.Size(104, 21);
            this.comboBox15.TabIndex = 1;
            // 
            // comboBox14
            // 
            this.comboBox14.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox14.FormattingEnabled = true;
            this.comboBox14.Items.AddRange(new object[] {
            "FRM002",
            "FRM005",
            "FRM0022",
            "M002",
            "M005",
            "M009",
            "Plantname/english",
            "Plantname/Farsi",
            "PlantID",
            "Date Without Seperator/8char",
            "Date With Seperator/10char",
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "(",
            ")",
            "_",
            "-",
            "space",
            "VersionNumber/max10"});
            this.comboBox14.Location = new System.Drawing.Point(351, 48);
            this.comboBox14.Name = "comboBox14";
            this.comboBox14.Size = new System.Drawing.Size(104, 21);
            this.comboBox14.TabIndex = 1;
            // 
            // comboBox13
            // 
            this.comboBox13.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox13.FormattingEnabled = true;
            this.comboBox13.Items.AddRange(new object[] {
            "FRM002",
            "FRM005",
            "FRM0022",
            "M002",
            "M005",
            "M009",
            "Plantname/english",
            "Plantname/Farsi",
            "PlantID",
            "Date Without Seperator/8char",
            "Date With Seperator/10char",
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "(",
            ")",
            "_",
            "-",
            "space",
            "VersionNumber/max10"});
            this.comboBox13.Location = new System.Drawing.Point(242, 48);
            this.comboBox13.Name = "comboBox13";
            this.comboBox13.Size = new System.Drawing.Size(104, 21);
            this.comboBox13.TabIndex = 1;
            // 
            // comboBox11
            // 
            this.comboBox11.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Items.AddRange(new object[] {
            "FRM002",
            "FRM005",
            "FRM0022",
            "M002",
            "M005",
            "M009",
            "Plantname/english",
            "Plantname/Farsi",
            "PlantID",
            "Date Without Seperator/8char",
            "Date With Seperator/10char",
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "(",
            ")",
            "_",
            "-",
            "space",
            "VersionNumber/max10"});
            this.comboBox11.Location = new System.Drawing.Point(19, 48);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(104, 21);
            this.comboBox11.TabIndex = 1;
            // 
            // comboBox12
            // 
            this.comboBox12.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox12.FormattingEnabled = true;
            this.comboBox12.Items.AddRange(new object[] {
            "FRM002",
            "FRM005",
            "FRM0022",
            "M002",
            "M005",
            "M009",
            "Plantname/english",
            "Plantname/Farsi",
            "PlantID",
            "Date Without Seperator/8char",
            "Date With Seperator/10char",
            "Year/2char",
            "Year/4char",
            "month/2char",
            "month/1char",
            "day/2char",
            "(",
            ")",
            "_",
            "-",
            "space",
            "VersionNumber/max10"});
            this.comboBox12.Location = new System.Drawing.Point(132, 48);
            this.comboBox12.Name = "comboBox12";
            this.comboBox12.Size = new System.Drawing.Size(104, 21);
            this.comboBox12.TabIndex = 1;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.LemonChiffon;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label22.Location = new System.Drawing.Point(470, 43);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(80, 15);
            this.label22.TabIndex = 9;
            this.label22.Text = "Folders Path";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.LemonChiffon;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label23.Location = new System.Drawing.Point(472, 292);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(76, 15);
            this.label23.TabIndex = 9;
            this.label23.Text = "Excel Name";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.cmbsheet);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Location = new System.Drawing.Point(348, 485);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(324, 63);
            this.panel3.TabIndex = 10;
            // 
            // cmbsheet
            // 
            this.cmbsheet.FormattingEnabled = true;
            this.cmbsheet.Items.AddRange(new object[] {
            "Sheet1",
            "SHEET1",
            "sheet1",
            "PlantName",
            "PlantID",
            "اصلاحیه فردا",
            "FRM005",
            "قيمت ",
            "Lfoc",
            "تراز آمادگی",
            "DATA"});
            this.cmbsheet.Location = new System.Drawing.Point(115, 21);
            this.cmbsheet.Name = "cmbsheet";
            this.cmbsheet.Size = new System.Drawing.Size(121, 21);
            this.cmbsheet.TabIndex = 2;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(40, 24);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(69, 13);
            this.label24.TabIndex = 1;
            this.label24.Text = "SheetName :";
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.Color.CadetBlue;
            this.btnsave.BackgroundImage = global::PowerPlantProject.Properties.Resources.Savedata;
            this.btnsave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnsave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsave.Location = new System.Drawing.Point(464, 567);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(99, 28);
            this.btnsave.TabIndex = 0;
            this.btnsave.Text = "Save";
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // rdformatmarket
            // 
            this.rdformatmarket.AutoSize = true;
            this.rdformatmarket.Checked = true;
            this.rdformatmarket.Location = new System.Drawing.Point(33, 21);
            this.rdformatmarket.Name = "rdformatmarket";
            this.rdformatmarket.Size = new System.Drawing.Size(58, 17);
            this.rdformatmarket.TabIndex = 11;
            this.rdformatmarket.TabStop = true;
            this.rdformatmarket.Text = "Market";
            this.rdformatmarket.UseVisualStyleBackColor = true;
            // 
            // rdEms
            // 
            this.rdEms.AutoSize = true;
            this.rdEms.Location = new System.Drawing.Point(33, 44);
            this.rdEms.Name = "rdEms";
            this.rdEms.Size = new System.Drawing.Size(77, 17);
            this.rdEms.TabIndex = 11;
            this.rdEms.Text = "EmsFormat";
            this.rdEms.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdformatmarket);
            this.groupBox1.Controls.Add(this.rdEms);
            this.groupBox1.Location = new System.Drawing.Point(876, 530);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(143, 71);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Format";
            this.groupBox1.Visible = false;
            // 
            // NameFormat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.ClientSize = new System.Drawing.Size(1021, 605);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.btnsave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "NameFormat";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NameFormat";
            this.Load += new System.EventHandler(this.NameFormat_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox comboBox12;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox comboBox24;
        private System.Windows.Forms.ComboBox comboBox23;
        private System.Windows.Forms.ComboBox comboBox22;
        private System.Windows.Forms.ComboBox comboBox21;
        private System.Windows.Forms.ComboBox comboBox20;
        private System.Windows.Forms.ComboBox comboBox19;
        private System.Windows.Forms.ComboBox comboBox18;
        private System.Windows.Forms.ComboBox comboBox17;
        private System.Windows.Forms.ComboBox comboBox16;
        private System.Windows.Forms.ComboBox comboBox15;
        private System.Windows.Forms.ComboBox comboBox14;
        private System.Windows.Forms.ComboBox comboBox13;
        private System.Windows.Forms.ComboBox comboBox11;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cmbsheet;
        private System.Windows.Forms.RadioButton rdformatmarket;
        private System.Windows.Forms.RadioButton rdEms;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}