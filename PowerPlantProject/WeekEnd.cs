﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Common;
using ILOG.Concert;
using ILOG.CPLEX;
using PowerPlantProject;
using FarsiLibrary.Utils;
using NRI.SBS.Common;
using System.Collections;

namespace PowerPlantProject
{
   public class WeekEnd
    {
       public string date = "";
       public WeekEnd(string Date)
       {
           date = Date;

       }
      public bool weekDate()
       {
           
          ////////////////////////////////////////////////////////////////////
           bool Vacation = false;

           DateTime miladi = PersianDateConverter.ToGregorianDateTime(date);
           System.Globalization.HijriCalendar pr = new System.Globalization.HijriCalendar();

           string  imonth = pr.GetMonth(miladi).ToString();
           string  iyear = pr.GetYear(miladi).ToString();
           int dday = pr.GetDayOfMonth(miladi);
           string iday = dday.ToString();



           if (imonth.Length != 2)
               imonth = "0" + imonth;
           if (iday.Length != 2)
               iday = "0" + iday;

           string ghamari = imonth+ "/" + iday;


           if (date.Contains("/01/01") || date.Contains("/01/02") || date.Contains("/01/03") || date.Contains("/01/04") || date.Contains("/01/05") || date.Contains("/01/06") || date.Contains("/01/07") || date.Contains("/01/08") || date.Contains("/01/09") || date.Contains("/01/10") || date.Contains("/01/11") || date.Contains("/01/12") || date.Contains("/01/13"))
           {
               Vacation = true;
           }
           if (date.Contains("/03/14") || date.Contains("/03/15") || date.Contains("/12/29")) { Vacation = true; }
         
       
          if(ghamari.Contains("06/03") ||ghamari.Contains("07/13")||ghamari.Contains("07/27")||ghamari.Contains("08/15")||ghamari.Contains("09/16") ||ghamari.Contains("11/01")||ghamari.Contains("11/25")||ghamari.Contains("12/10")||ghamari.Contains("12/18") ||ghamari.Contains("01/09")||ghamari.Contains("01/10")||ghamari.Contains("02/20")||ghamari.Contains("02/28") ||ghamari.Contains("03/17")||ghamari.Contains("02/30"))
          {
               Vacation = true;
          }
          //////////////////////////////////////////////////////////////////////////////////

           return Vacation;
       }
    }
}
