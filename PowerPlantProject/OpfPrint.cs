﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PowerPlantProject
{
    public partial class OpfPrint : Form
    {
        DataTable Dg1 = null;
        string pname;
        string Date;

        public OpfPrint(DataTable dg1, string Pname,string DATE)
        {
            InitializeComponent();
            Dg1 = dg1;
            pname = Pname;
            Date = DATE;
        }

        private void OpfPrint_Load(object sender, EventArgs e)
        {
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgv.AllowUserToDeleteRows = false;
            label1.Text = "        Name :  " + pname+"   Date :  "+Date;        

            dgv.DataSource = Dg1;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            dgv.DataSource = Dg1;
            PrintDGV.info(label1.Text);
            PrintDGV.Print_DataGridView(dgv);
        }
    }
}
