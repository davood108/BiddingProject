﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using NRI.SBS.Common;
using System.Xml.Serialization;
using System.IO;

namespace PowerPlantProject
{
    public partial class PathForm : Form
    {
        string ConStr;
        public PathForm()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;
                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                foreach (Control c in grbftp.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }
                foreach (Control c in grbinternet.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }
                
                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            if ((M002Tb.Text != "") || (M005Tb.Text != "") || (M009Tb.Text != "") || (PriceTb.Text != "") || (LoadTb.Text != "") || (CapacityTb.Text != "") || (txtdispatchable.Text != "") || (M005Tba.Text != ""))
            {
                
                SettingParameters settingParam = new SettingParameters();
                settingParam.ServerProxy = txtProxy.Text.Trim();
                settingParam.Username =RijndaelEncoding.Encrypt(txtInternetUsername.Text.Trim());
                settingParam.Password = RijndaelEncoding.Encrypt(txtInternetPassword.Text.Trim());
                settingParam.Domain = txtDomain.Text.Trim();
                
                //////////////////////////////////////hard////////////////////////////////////////

                
                if (rbHardPath002.Checked)
                {
                    settingParam.IsFTP002 = false;
                    settingParam.M002Path = M002Tb.Text.Trim();
                      settingParam.FTPPassword = "";
                    settingParam.FTPServer = "";
                    settingParam.FTPUsername = "";
                }

                if(rbHardPath005.Checked)
                {
                   settingParam.IsFTP005=false;
                   settingParam.M005Path = M005Tb.Text.Trim();
                      settingParam.FTPPassword = "";
                    settingParam.FTPServer = "";
                    settingParam.FTPUsername = "";
                }

                if (rbHardPath005ba.Checked)
                {
                    settingParam.IsFTP005ba = false;
                    settingParam.M005Pathba = M005Tba.Text.Trim();
                    settingParam.FTPPassword = "";
                    settingParam.FTPServer = "";
                    settingParam.FTPUsername = "";
                }

                if(rbHardPathDispatch.Checked)
                {
                   settingParam.IsFTPDispatch=false;
                   settingParam.DispatchFilePath = txtdispatchable.Text.Trim();
                      settingParam.FTPPassword = "";
                    settingParam.FTPServer = "";
                    settingParam.FTPUsername = "";
                }

                if(rbHardPath0091.Checked)
                {
                   settingParam.IsFTP0091=false;
                   settingParam.M0091Path = M0091Tb.Text.Trim();
                      settingParam.FTPPassword = "";
                    settingParam.FTPServer = "";
                    settingParam.FTPUsername = "";

                }

                 if(rbHardPath009.Checked)
                {
                   settingParam.IsFTP009=false;
                   settingParam.M009Path = M009Tb.Text.Trim();
                   settingParam.FTPPassword = "";
                    settingParam.FTPServer = "";
                    settingParam.FTPUsername = "";

                }
                if(rbHardPathcounter.Checked)
                {
                    settingParam.IsFTPcounter=false;
                    settingParam.CounterPath = CounterTb.Text.Trim();
                      settingParam.FTPPassword = "";
                    settingParam.FTPServer = "";
                    settingParam.FTPUsername = "";
                }


                if(rbHardPathavg.Checked)
                {
                    settingParam.IsFTPavg=false;
                    settingParam.AvgPricePath = PriceTb.Text.Trim();
                      settingParam.FTPPassword = "";
                    settingParam.FTPServer = "";
                    settingParam.FTPUsername = "";
                }

                if(rbHardPathcapacity.Checked)
                {
                    settingParam.IsFTPcapacity=false;
                    settingParam.CapacityFactorsPath = CapacityTb.Text.Trim();
                      settingParam.FTPPassword = "";
                    settingParam.FTPServer = "";
                    settingParam.FTPUsername = "";
                }
                   
                if(rbHardPathload.Checked)
                {
                  settingParam.IsFTPload=false;
                  settingParam.LoadForcastingPath = LoadTb.Text.Trim();
                      settingParam.FTPPassword = "";
                    settingParam.FTPServer = "";
                    settingParam.FTPUsername = "";
                }
                  


                ///////////////////////////////////ftp//////////////////////////////////////////



                bool fillftp=false;  
                if ((txtftpip.Text != "") && (txtftppass.Text != "") && (txtftpuser.Text != ""))
                {
                    fillftp=true;

                }

                if(rbFTPPath002.Checked && fillftp)
                {
                  settingParam.IsFTP002 = true;
                  settingParam.M002Path = M002Tb.Text.Trim();
                  settingParam.FTPPassword = txtftppass.Text.Trim();
                  settingParam.FTPServer = txtftpip.Text.Trim();
                  settingParam.FTPUsername = txtftpuser.Text.Trim();
                  
                 }
                if(rbFTPPath005.Checked && fillftp)
                {
                   settingParam.IsFTP005=true;
                   settingParam.M005Path = M005Tb.Text.Trim();
                   settingParam.FTPPassword = txtftppass.Text.Trim();
                   settingParam.FTPServer = txtftpip.Text.Trim();
                   settingParam.FTPUsername = txtftpuser.Text.Trim();
                }
                if (rbFTPPath005ba.Checked && fillftp)
                {
                    settingParam.IsFTP005ba = true;
                    settingParam.M005Pathba = M005Tba.Text.Trim();
                    settingParam.FTPPassword = txtftppass.Text.Trim();
                    settingParam.FTPServer = txtftpip.Text.Trim();
                    settingParam.FTPUsername = txtftpuser.Text.Trim();
                }



                  if(rbFTPPathdispatch.Checked && fillftp)
                  {
                   settingParam.IsFTPDispatch=true;
                   settingParam.DispatchFilePath = txtdispatchable.Text.Trim();
                   settingParam.FTPPassword = txtftppass.Text.Trim();
                   settingParam.FTPServer = txtftpip.Text.Trim();
                   settingParam.FTPUsername = txtftpuser.Text.Trim();
                  }
                if(rbFTPPath009.Checked )
                {

                   settingParam.IsFTP009=true;
                   settingParam.M009Path = M009Tb.Text.Trim();
                   //settingParam.FTPPassword = txtftppass.Text.Trim();
                   //settingParam.FTPServer = txtftpip.Text.Trim();
                   //settingParam.FTPUsername = txtftpuser.Text.Trim();
                }

                 if(rbFTPPath0091.Checked && fillftp)
                {

                   settingParam.IsFTP0091=true;
                   settingParam.M0091Path = M0091Tb.Text.Trim();
                   settingParam.FTPPassword = txtftppass.Text.Trim();
                   settingParam.FTPServer = txtftpip.Text.Trim();
                   settingParam.FTPUsername = txtftpuser.Text.Trim();
                }
                if(rbFTPPathcounter.Checked && fillftp)
                {
                    settingParam.IsFTPcounter=true;
                    settingParam.CounterPath = CounterTb.Text.Trim();
                    settingParam.FTPPassword = txtftppass.Text.Trim();
                    settingParam.FTPServer = txtftpip.Text.Trim();
                    settingParam.FTPUsername = txtftpuser.Text.Trim(); 
                    
                }

                if(rbFTPPathavg.Checked && fillftp)
                {
                    settingParam.IsFTPavg=true;
                    settingParam.AvgPricePath = PriceTb.Text.Trim();
                    settingParam.FTPPassword = txtftppass.Text.Trim();
                    settingParam.FTPServer = txtftpip.Text.Trim();
                    settingParam.FTPUsername = txtftpuser.Text.Trim(); 
                }

                if(rbFTPPathload.Checked && fillftp)
                {
                    settingParam.IsFTPload=true;
                    settingParam.LoadForcastingPath = LoadTb.Text.Trim();
                    settingParam.FTPPassword = txtftppass.Text.Trim();
                    settingParam.FTPServer = txtftpip.Text.Trim();
                    settingParam.FTPUsername = txtftpuser.Text.Trim(); 

                }
                 if(rbFTPPathcapacity.Checked && fillftp)
                 {
                    settingParam.IsFTPcapacity=true;
                    settingParam.CapacityFactorsPath = CapacityTb.Text.Trim();
                    settingParam.FTPPassword = txtftppass.Text.Trim();
                    settingParam.FTPServer = txtftpip.Text.Trim();
                    settingParam.FTPUsername = txtftpuser.Text.Trim(); 
                 }
                  

                XmlSerializer mySerializer = new XmlSerializer(typeof(SettingParameters));
                try
                {
                    string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\SBS\\setting.xml";
                    FileInfo fInfo = new FileInfo(path);
                    if (!fInfo.Directory.Exists)
                        fInfo.Directory.Create();

                    FileStream myFileStream = new FileStream(path, FileMode.Create, FileAccess.Write);

                    mySerializer.Serialize(myFileStream, settingParam);
                    myFileStream.Close();
                    MessageBox.Show("Path Save Succesfully ");

                }
                catch (System.IO.FileNotFoundException fex)
                {
                    //throw fex;
                    MessageBox.Show("File Not Found");
                }
                catch (Exception ex)
                {
                    //throw ex;
                    MessageBox.Show(ex.Message);
                }

            }
        
            else 
                MessageBox.Show("Fill atLeast One Field!");
        }

        private void M002Tb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(M002Tb, @"Hard Example: c:\data\M002 <-----> FTP Example : M002");
        }

        private void M005Tb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(M005Tb, @"Hard Example: c:\data\M005 <-----> FTP Example : M005");
        }

        private void M009Tb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(M009Tb, @"Hard Example: c:\data\M009 <-----> FTP Example : M009 ");
        }

        private void PriceTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(PriceTb, @"Hard Example: c:\data\Averageprice <-----> FTP Example : Averageprice");
        }

        private void LoadTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(LoadTb, @"Hard Example: c:\data\loadForecast <-----> FTP Example : loadForecast");
        }

        private void CapacityTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(CapacityTb, @"Hard Example: c:\data\Capacity <-----> FTP Example : Capacity");
        }
        private void txtdispatchable_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(CapacityTb, @"Hard Example: c:\data\Dispatchable <-----> FTP Example : Dispatchable");
        }
        private void btnM002_Click(object sender, EventArgs e)
        {
            string path = getPath();
            M002Tb.Text = (path != "" ? path : M002Tb.Text);
        }
        private string getPath()
        {
            DialogResult result = this.folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                return folderBrowserDialog1.SelectedPath;
            }
            return "";
        }


        private void button2_Click(object sender, EventArgs e)
        {
            string path = getPath();
            M005Tb.Text = (path != "" ? path : M005Tb.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string path = getPath();
            PriceTb.Text = (path != "" ? path : PriceTb.Text);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string path = getPath();
            LoadTb.Text = (path != "" ? path : LoadTb.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string path = getPath();
            CapacityTb.Text = (path != "" ? path : CapacityTb.Text);
        }

        //private void Form7_Load(object sender, EventArgs e)
        //{
        // //   PopulateFields();
        //}

        private void PopulateFields()
        {



            SettingParameters settingParam = new SettingParameters();
            XmlSerializer mySerializer = new XmlSerializer(typeof(SettingParameters));
            try
            {
                string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\SBS\\setting.xml";
                
                    FileStream myFileStream = new FileStream(path, FileMode.Open);

                    settingParam = (SettingParameters)mySerializer.Deserialize(myFileStream);
                    myFileStream.Close();

                    txtProxy.Text = settingParam.ServerProxy;
                    txtInternetUsername.Text = RijndaelEncoding.Decrypt(settingParam.Username);
                    txtInternetPassword.Text = RijndaelEncoding.Decrypt(settingParam.Password);
                    txtDomain.Text = settingParam.Domain;

                    txtftpip.Text = settingParam.FTPServer;
                    txtftppass.Text = settingParam.FTPPassword;
                    txtftpuser.Text = settingParam.FTPUsername;




                    if (!settingParam.IsFTP002)
                    {
                        rbFTPPath002.Checked = false;
                        rbHardPath002.Checked = true;
                        M002Tb.Text = settingParam.M002Path;

                    }
                    else if (settingParam.IsFTP002)
                    {
                        rbFTPPath002.Checked = true;
                        rbHardPath002.Checked = false;
                        M002Tb.Text = settingParam.M002Path;


                    }


                    if (!settingParam.IsFTP005)
                    {
                        rbFTPPath005.Checked = false;
                        rbHardPath005.Checked = true;
                        M005Tb.Text = settingParam.M005Path;


                    }
                    else if (settingParam.IsFTP005)
                    {
                        rbFTPPath005.Checked = true;
                        rbHardPath005.Checked = false;
                        M005Tb.Text = settingParam.M005Path;

                    }

                ////ba mahdodiat////////////////////////////////

                    if (!settingParam.IsFTP005ba)
                    {
                        rbFTPPath005ba.Checked = false;
                        rbHardPath005ba.Checked = true;
                        M005Tba.Text = settingParam.M005Pathba;


                    }
                    else if (settingParam.IsFTP005ba)
                    {
                        rbFTPPath005ba.Checked = true;
                        rbHardPath005ba.Checked = false;
                        M005Tba.Text = settingParam.M005Pathba;

                    }




                ///////////////////////////////////////////////////////




                    if (!settingParam.IsFTP009)
                    {
                        rbFTPPath009.Checked = false;
                        rbHardPath009.Checked = true;
                        M009Tb.Text = settingParam.M009Path;


                    }
                    else if (settingParam.IsFTP009)
                    {
                        rbFTPPath009.Checked = true;
                        rbHardPath009.Checked = false;
                        M009Tb.Text = settingParam.M009Path;


                    }

                    if (!settingParam.IsFTP0091)
                    {
                        rbFTPPath0091.Checked = false;
                        rbHardPath0091.Checked = true;
                        M0091Tb.Text = settingParam.M0091Path;

                    }
                    else if (settingParam.IsFTP0091)
                    {
                        rbFTPPath0091.Checked = true;
                        rbHardPath0091.Checked = false;
                        M0091Tb.Text = settingParam.M0091Path;


                    }

                    if (!settingParam.IsFTPcounter)
                    {
                        rbFTPPathcounter.Checked = false;
                        rbHardPathcounter.Checked = true;
                        CounterTb.Text = settingParam.CounterPath;


                    }
                    else if (settingParam.IsFTPcounter)
                    {
                        rbFTPPathcounter.Checked = true;
                        rbHardPathcounter.Checked = false;
                        CounterTb.Text = settingParam.CounterPath;

                    }

                    if (!settingParam.IsFTPcapacity)
                    {
                        rbFTPPathcapacity.Checked = false;
                        rbHardPathcapacity.Checked = true;
                        CapacityTb.Text = settingParam.CapacityFactorsPath;


                    }
                    else if (settingParam.IsFTPcapacity)
                    {
                        rbFTPPathcapacity.Checked = true;
                        rbHardPathcapacity.Checked = false;
                        CapacityTb.Text = settingParam.CapacityFactorsPath;


                    }
                    if (!settingParam.IsFTPload)
                    {
                        rbFTPPathload.Checked = false;
                        rbHardPathload.Checked = true;
                        LoadTb.Text = settingParam.LoadForcastingPath;

                    }
                    else if (settingParam.IsFTPload)
                    {
                        rbFTPPathload.Checked = true;
                        rbHardPathload.Checked = false;
                        LoadTb.Text = settingParam.LoadForcastingPath;


                    }

                    if (!settingParam.IsFTPavg)
                    {
                        rbFTPPathavg.Checked = false;
                        rbHardPathavg.Checked = true;
                        PriceTb.Text = settingParam.AvgPricePath;

                    }
                    else if (settingParam.IsFTPavg)
                    {
                        rbFTPPathavg.Checked = true;
                        rbHardPathavg.Checked = false;
                        PriceTb.Text = settingParam.AvgPricePath;


                    }

                    if (!settingParam.IsFTPDispatch)
                    {
                        rbFTPPathdispatch.Checked = false;
                        rbHardPathDispatch.Checked = true;
                        txtdispatchable.Text = settingParam.DispatchFilePath;


                    }
                    else if (settingParam.IsFTPDispatch)
                    {
                        rbFTPPathdispatch.Checked = true;
                        rbHardPathDispatch.Checked = false;
                        txtdispatchable.Text = settingParam.DispatchFilePath;

                    }
               
               
            }
            catch (System.IO.FileNotFoundException fex)
            {
               // throw fex;
                //MessageBox.Show("File not found");
                rbHardPath002.Checked = true;
                        
                rbHardPath005.Checked = true;
                             
                rbHardPath009.Checked = true;

                rbHardPath0091.Checked = true;

                rbHardPathcounter.Checked = true;

                rbHardPathcapacity.Checked = true;

                rbHardPathload.Checked = true;

                rbHardPathavg.Checked = true;

                rbHardPathDispatch.Checked = true;
                                
                txtDomain.Clear();
                txtInternetPassword.Clear();
                txtInternetUsername.Clear();
                txtProxy.Clear();
                M002Tb.Clear();
                M005Tb.Clear();
                M005Tba.Clear();
                M009Tb.Clear();
                M0091Tb.Clear();
                CounterTb.Clear();
                CapacityTb.Clear();
                LoadTb.Clear();
                PriceTb.Clear();
                //...

            }
            catch (Exception ex)
            {
               // throw ex;
                MessageBox.Show(ex.Message);
            }




        }

        private void txtProxy_MouseClick_1(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(txtProxy, @"Example: http://172.16.1.100:8080");
        }
       
        private void button1_Click(object sender, EventArgs e)
        {
            string path = getPath();
            M009Tb.Text = (path != "" ? path : M009Tb.Text);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string path = getPath();
            M0091Tb.Text = (path != "" ? path : M0091Tb.Text);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string path = getPath();
            CounterTb.Text = (path != "" ? path : CounterTb.Text);
        }

        //private void rbFTPPath_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rbFTPPath002.Checked)
        //    {
        //        button1.Enabled = false;
        //        button3.Enabled = false;
        //        button4.Enabled = false;
        //        button5.Enabled = false;
        //        button6.Enabled = false;
        //        button7.Enabled = false;
        //        btnM002.Enabled = false;
        //        button2.Enabled = false;
        //        button8.Enabled = false;
        //        /////////////////////////////////////////////////
        //        M002Tb.ReadOnly = false;
        //        M005Tb.ReadOnly = false;
        //        M009Tb.ReadOnly = false;
        //        M0091Tb.ReadOnly = false;
        //        CounterTb.ReadOnly = false;
        //        CapacityTb.ReadOnly = false;
        //        LoadTb.ReadOnly = false;
        //        PriceTb.ReadOnly = false;
        //        txtdispatchable.ReadOnly = false;

        //        //////////////////////////////////////////////////
              
        //        M002Tb.Text = "";
        //        M005Tb.Text = "";
        //        M009Tb.Text = "";
        //        M0091Tb.Text = "";
        //        CounterTb.Text = "";
        //        CapacityTb.Text = "";
        //        LoadTb.Text = "";
        //        PriceTb.Text = "";
        //        txtdispatchable.Text = "";

        //    }
        //    else 
        //    {
        //        button1.Enabled = true;
        //        button3.Enabled = true;
        //        button4.Enabled = true;
        //        button5.Enabled = true;
        //        button6.Enabled = true;
        //        button7.Enabled = true;
        //        btnM002.Enabled = true;
        //        button2.Enabled = true;
        //        button8.Enabled = true;
        //       ////////////////////////////////
        //        txtftpuser.Text = "";
        //        txtftppass.Text = "";
        //        txtftpip.Text = "";
             
        //        M002Tb.Text = "";
        //        M005Tb.Text = "";
        //        M009Tb.Text = "";
        //        M0091Tb.Text = "";
        //        CounterTb.Text = "";
        //        CapacityTb.Text = "";
        //        LoadTb.Text = "";
        //        PriceTb.Text = "";
        //        txtdispatchable.Text = "";

        //        //////////////////////////////
        //        M002Tb.ReadOnly = true;
        //        M005Tb.ReadOnly = true;
        //        M009Tb.ReadOnly = true;
        //        M0091Tb.ReadOnly = true;
        //        CounterTb.ReadOnly = true;
        //        CapacityTb.ReadOnly = true;
        //        LoadTb.ReadOnly = true;
        //        PriceTb.ReadOnly = true;
        //        txtdispatchable.ReadOnly = true;
        //    }
        //}

        private void button8_Click(object sender, EventArgs e)
        {
            string path = getPath();
            txtdispatchable.Text = (path != "" ? path : txtdispatchable.Text);
        }

        private void PathForm_Load(object sender, EventArgs e)
        {
            PopulateFields();
        }

        private void rbHardPath002_CheckedChanged(object sender, EventArgs e)
        {
            if (rbHardPath002.Checked)
            {
                M002Tb.ReadOnly = true;
                btnM002.Enabled= true;
            }
           
        }

        private void rbFTPPath002_CheckedChanged(object sender, EventArgs e)
        {
            M002Tb.ReadOnly = false;
            btnM002.Enabled = false;
            M002Tb.Text = "";
        }

        private void rbHardPath005_CheckedChanged(object sender, EventArgs e)
        {
            M005Tb.ReadOnly = true;

            button2.Enabled = true;
        }

        private void rbFTPPath005_CheckedChanged(object sender, EventArgs e)
        {
            M005Tb.ReadOnly = false;
            button2.Enabled = false;
            M005Tb.Text = "";
        }

        private void rbHardPathDispatch_CheckedChanged(object sender, EventArgs e)
        {
            button8.Enabled = true;
            txtdispatchable.ReadOnly = true;
        }

        private void rbFTPPathdispatch_CheckedChanged(object sender, EventArgs e)
        {
            button8.Enabled = false;
            txtdispatchable.ReadOnly = false;
            txtdispatchable.Text = "";
        }

        private void rbHardPath009_CheckedChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
            M009Tb.ReadOnly = true;
        }

        private void rbFTPPath009_CheckedChanged(object sender, EventArgs e)
        {

            button1.Enabled = true;
            M009Tb.ReadOnly = true;
            //button1.Enabled = false;
            //M009Tb.ReadOnly = false;
            //M009Tb.Text = "";
        }

        private void rbHardPath0091_CheckedChanged(object sender, EventArgs e)
        {
            button6.Enabled = true;
            M0091Tb.ReadOnly = true;
        }

        private void rbFTPPath0091_CheckedChanged(object sender, EventArgs e)
        {
            button6.Enabled =false;
            M0091Tb.ReadOnly = false;
            M0091Tb.Text = "";
        }

        private void rbHardPathcounter_CheckedChanged(object sender, EventArgs e)
        {
            button7.Enabled = true;
            CounterTb.ReadOnly = true;
        }

        private void rbFTPPathcounter_CheckedChanged(object sender, EventArgs e)
        {
            button7.Enabled = false;
            CounterTb.ReadOnly =false;
            CounterTb.Text = "";
        }

        private void rbHardPathavg_CheckedChanged(object sender, EventArgs e)
        {
            button3.Enabled = true;
            PriceTb.ReadOnly = true;
        }

        private void rbFTPPathavg_CheckedChanged(object sender, EventArgs e)
        {
            button3.Enabled = false;
            PriceTb.ReadOnly = false;
            PriceTb.Text = "";
        }

        private void rbHardPathload_CheckedChanged(object sender, EventArgs e)
        {
            button4.Enabled = true;
            LoadTb.ReadOnly = true;

        }

        private void rbFTPPathload_CheckedChanged(object sender, EventArgs e)
        {
            button4.Enabled = false;
            LoadTb.ReadOnly = false;
            LoadTb.Text = "";
        }

        private void rbHardPathcapacity_CheckedChanged(object sender, EventArgs e)
        {
            button5.Enabled = true;
            CapacityTb.ReadOnly = true;
        }

        private void rbFTPPathcapacity_CheckedChanged(object sender, EventArgs e)
        {
            button5.Enabled = false;
            CapacityTb.ReadOnly = false;
            CapacityTb.Text = "";
        }

        private void btnformat002_Click(object sender, EventArgs e)
        {
            NameFormat f = new NameFormat("M002");
            f.Show();
        }

        private void btnformat005_Click(object sender, EventArgs e)
        {
            NameFormat f = new NameFormat("M005");
            f.Show();
        }

        private void btnformatdispatch_Click(object sender, EventArgs e)
        {
            NameFormat f = new NameFormat("Dispatch");
            f.Show();
        }

        private void btnformatsaled_Click(object sender, EventArgs e)
        {
            NameFormat f = new NameFormat("SaledEnergy");
            f.Show();
        }

        private void btnformatcounter_Click(object sender, EventArgs e)
        {
            NameFormat f = new NameFormat("Counters");
            f.Show();
        }

        private void btnformatavgprice_Click(object sender, EventArgs e)
        {
            NameFormat f = new NameFormat("AveragePrice");
            f.Show();
        }

        private void btnformatlfc_Click(object sender, EventArgs e)
        {
            NameFormat f = new NameFormat("LoadForecasting");
            f.Show();
        }

        private void btnformatcapacity_Click(object sender, EventArgs e)
        {
            NameFormat f = new NameFormat("CapacityFactor");
            f.Show();
        }

        private void btninternet_Click(object sender, EventArgs e)
        {
            InternetAddress d = new InternetAddress();
            d.Show();
        }

        private void M005Tba_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(M005Tba, @"Hard Example: c:\data\M005withlimmit <-----> FTP Example : M005withlimmit");
        }

        private void btnm005_Click(object sender, EventArgs e)
        {
            string path = getPath();
            M005Tba.Text = (path != "" ? path : M005Tba.Text);
        }

        private void rbHardPath005ba_CheckedChanged(object sender, EventArgs e)
        {
            M005Tba.ReadOnly = true;
            btnm005.Enabled = true;
        }

        private void rbFTPPath005ba_CheckedChanged(object sender, EventArgs e)
        {
            M005Tba.ReadOnly = false;
            btnm005.Enabled = false;
            M005Tba.Text = "";
        }

        private void btnformat005ba_Click(object sender, EventArgs e)
        {
            NameFormat f = new NameFormat("M005ba");
            f.Show();
        }

       

       

       
        
       
       

       

       
       

       

       

       

       
    }
}
