﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PowerPlantProject
{
    public partial class PrintPreviewForm : Form
    {
        public PrintPreviewForm(string pdfPathview)
        {
            InitializeComponent();
            pdfviewer.Navigate(pdfPathview);
        }
    }
}
