﻿namespace PowerPlantProject
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            this.linesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.powerPalntDBDataSet4BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.powerPalntDBDataSet4 = new PowerPlantProject.PowerPalntDBDataSet4();
            this.unitsDataMainBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.linesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.detailFRM002BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.treeView2 = new System.Windows.Forms.TreeView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.marketToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m002ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m005ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.averagePriceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadForecastingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.betToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.capacityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annualFactorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.weekFactorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.autoPathToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.linesTableAdapter = new PowerPlantProject.PowerPalntDBDataSet4TableAdapters.LinesTableAdapter();
            this.unitsDataMainTableAdapter = new PowerPlantProject.PowerPalntDBDataSet4TableAdapters.UnitsDataMainTableAdapter();
            this.detailFRM002TableAdapter = new PowerPlantProject.PowerPalntDBDataSet4TableAdapters.DetailFRM002TableAdapter();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.BiddingStrategy = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.rdStrategyBidding = new System.Windows.Forms.RadioButton();
            this.rdBidAllocation = new System.Windows.Forms.RadioButton();
            this.rdForecastingPrice = new System.Windows.Forms.RadioButton();
            this.btnRunBidding = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lstStatus = new System.Windows.Forms.TextBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.panel80 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.datePickerBidding = new FarsiLibrary.Win.Controls.FADatePicker();
            this.panel81 = new System.Windows.Forms.Panel();
            this.datePickerCurrent = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.panel24 = new System.Windows.Forms.Panel();
            this.cmbPeakBid = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel75 = new System.Windows.Forms.Panel();
            this.cmbCostLevel = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel77 = new System.Windows.Forms.Panel();
            this.cmbStrategyBidding = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel78 = new System.Windows.Forms.Panel();
            this.txtTraining = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel79 = new System.Windows.Forms.Panel();
            this.cmbRunSetting = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblPlantValue = new System.Windows.Forms.Label();
            this.lblGencoValue = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.grBoxCustomize = new System.Windows.Forms.GroupBox();
            this.menuStrip7 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy6 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak6 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy12 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak12 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy18 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak18 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy24 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak24 = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip8 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem23 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem24 = new System.Windows.Forms.ToolStripMenuItem();
            this.strategyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.peakToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.strategyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.strategyToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.peakToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.hourToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.strategyToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.peakToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip6 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy5 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak5 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy11 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak11 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy17 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak17 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy23 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak23 = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip5 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy4 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak4 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy10 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak10 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy16 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak16 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy22 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak22 = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip3 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy2 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak2 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy8 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak8 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy14 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak14 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy20 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak20 = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip4 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem25 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy3 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak3 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem26 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy9 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak9 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy15 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak15 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy21 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak21 = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.hour1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy1 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak1 = new System.Windows.Forms.ToolStripComboBox();
            this.hour2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy7 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak7 = new System.Windows.Forms.ToolStripComboBox();
            this.hour3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy13 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak13 = new System.Windows.Forms.ToolStripComboBox();
            this.hour4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy19 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak19 = new System.Windows.Forms.ToolStripComboBox();
            this.FinancialReport = new System.Windows.Forms.TabPage();
            this.FRMainPanel = new System.Windows.Forms.Panel();
            this.FRUnitPanel = new System.Windows.Forms.Panel();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.panel65 = new System.Windows.Forms.Panel();
            this.FRUnitHotTb = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.panel68 = new System.Windows.Forms.Panel();
            this.FRUnitColdTb = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.panel66 = new System.Windows.Forms.Panel();
            this.FRUnitBmainTb = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.panel67 = new System.Windows.Forms.Panel();
            this.FRUnitAmainTb = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.panel62 = new System.Windows.Forms.Panel();
            this.FRUnitCmargTb2 = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.panel63 = new System.Windows.Forms.Panel();
            this.FRUnitBmargTb2 = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.panel64 = new System.Windows.Forms.Panel();
            this.FRUnitAmargTb2 = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.panel59 = new System.Windows.Forms.Panel();
            this.FRUnitCmargTb1 = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.panel60 = new System.Windows.Forms.Panel();
            this.FRUnitBmargTb1 = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.panel61 = new System.Windows.Forms.Panel();
            this.FRUnitAmargTb1 = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.panel53 = new System.Windows.Forms.Panel();
            this.FRUnitVariableTb = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.panel57 = new System.Windows.Forms.Panel();
            this.FRUnitFixedTb = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.panel58 = new System.Windows.Forms.Panel();
            this.FRUnitCapitalTb = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.FRUnitCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.panel76 = new System.Windows.Forms.Panel();
            this.FRUnitIncomeTb = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.panel69 = new System.Windows.Forms.Panel();
            this.FRUnitEneryPayTb = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.panel70 = new System.Windows.Forms.Panel();
            this.FRUnitCapPayTb = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.panel71 = new System.Windows.Forms.Panel();
            this.FRUnitULPowerTb = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.panel73 = new System.Windows.Forms.Panel();
            this.FRUnitTotalPowerTb = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.panel74 = new System.Windows.Forms.Panel();
            this.FRUnitCapacityTb = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.FRUpdateBtn = new System.Windows.Forms.Button();
            this.FROKBtn = new System.Windows.Forms.Button();
            this.FRPlantPanel = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.FRPlantCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.panel54 = new System.Windows.Forms.Panel();
            this.FRPlantCostTb = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.panel55 = new System.Windows.Forms.Panel();
            this.FRPlantIncomeTb = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.panel56 = new System.Windows.Forms.Panel();
            this.FRPlantBenefitTB = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.panel47 = new System.Windows.Forms.Panel();
            this.FRPlantDecPayTb = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.panel48 = new System.Windows.Forms.Panel();
            this.FRPlantIncPayTb = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.panel49 = new System.Windows.Forms.Panel();
            this.FRPlantULPayTb = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.panel50 = new System.Windows.Forms.Panel();
            this.FRPlantBidPayTb = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.panel51 = new System.Windows.Forms.Panel();
            this.FRPlantEnergyPayTb = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.panel52 = new System.Windows.Forms.Panel();
            this.FRPlantCapPayTb = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.panel41 = new System.Windows.Forms.Panel();
            this.FRPlantDecPowerTb = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.panel42 = new System.Windows.Forms.Panel();
            this.FRPlantIncPowerTb = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.panel43 = new System.Windows.Forms.Panel();
            this.FRPlantULPowerTb = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.panel44 = new System.Windows.Forms.Panel();
            this.FRPlantBidPowerTb = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.panel45 = new System.Windows.Forms.Panel();
            this.FRPlantTotalPowerTb = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.panel46 = new System.Windows.Forms.Panel();
            this.FRPlantAvaCapTb = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.FRHeaderGb = new System.Windows.Forms.GroupBox();
            this.FRPlantLb = new System.Windows.Forms.Label();
            this.FRHeaderPanel = new System.Windows.Forms.Panel();
            this.FRTypeLb = new System.Windows.Forms.Label();
            this.FRUnitLb = new System.Windows.Forms.Label();
            this.FRPackLb = new System.Windows.Forms.Label();
            this.L20 = new System.Windows.Forms.Label();
            this.L19 = new System.Windows.Forms.Label();
            this.L18 = new System.Windows.Forms.Label();
            this.L17 = new System.Windows.Forms.Label();
            this.BidData = new System.Windows.Forms.TabPage();
            this.BDMainPanel = new System.Windows.Forms.Panel();
            this.BDPlotBtn = new System.Windows.Forms.Button();
            this.BDCur2 = new System.Windows.Forms.GroupBox();
            this.BDCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.BDCurGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power6DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price6DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power7DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price7DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power8DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price8DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power9DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price9DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power10DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price10DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label43 = new System.Windows.Forms.Label();
            this.BDCur1 = new System.Windows.Forms.GroupBox();
            this.panel38 = new System.Windows.Forms.Panel();
            this.BDMaxBidTb = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.panel39 = new System.Windows.Forms.Panel();
            this.BDPowerTb = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.panel40 = new System.Windows.Forms.Panel();
            this.BDStateTb = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.BDHeaderGb = new System.Windows.Forms.GroupBox();
            this.BDPlantLb = new System.Windows.Forms.Label();
            this.BDHeaderPanel = new System.Windows.Forms.Panel();
            this.BDTypeLb = new System.Windows.Forms.Label();
            this.BDUnitLb = new System.Windows.Forms.Label();
            this.BDPackLb = new System.Windows.Forms.Label();
            this.L16 = new System.Windows.Forms.Label();
            this.L15 = new System.Windows.Forms.Label();
            this.L14 = new System.Windows.Forms.Label();
            this.L13 = new System.Windows.Forms.Label();
            this.MarketResults = new System.Windows.Forms.TabPage();
            this.MRMainPanel = new System.Windows.Forms.Panel();
            this.MRPlotBtn = new System.Windows.Forms.Button();
            this.MRPlantCurGb = new System.Windows.Forms.GroupBox();
            this.panel34 = new System.Windows.Forms.Panel();
            this.MRPlantPowerTb = new System.Windows.Forms.TextBox();
            this.MRLabel2 = new System.Windows.Forms.Label();
            this.panel35 = new System.Windows.Forms.Panel();
            this.MRPlantOnUnitTb = new System.Windows.Forms.TextBox();
            this.MRLabel1 = new System.Windows.Forms.Label();
            this.MRGB = new System.Windows.Forms.GroupBox();
            this.MRCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.MRCurGrid2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MRCurGrid1 = new System.Windows.Forms.DataGridView();
            this.ITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label39 = new System.Windows.Forms.Label();
            this.MRUnitCurGb = new System.Windows.Forms.GroupBox();
            this.panel37 = new System.Windows.Forms.Panel();
            this.MRUnitMaxBidTb = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.panel36 = new System.Windows.Forms.Panel();
            this.MRUnitPowerTb = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.panel72 = new System.Windows.Forms.Panel();
            this.MRUnitStateTb = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.MRHeaderGB = new System.Windows.Forms.GroupBox();
            this.MRPlantLb = new System.Windows.Forms.Label();
            this.MRHeaderPanel = new System.Windows.Forms.Panel();
            this.MRTypeLb = new System.Windows.Forms.Label();
            this.MRUnitLb = new System.Windows.Forms.Label();
            this.MRPackLb = new System.Windows.Forms.Label();
            this.L12 = new System.Windows.Forms.Label();
            this.L11 = new System.Windows.Forms.Label();
            this.L10 = new System.Windows.Forms.Label();
            this.L9 = new System.Windows.Forms.Label();
            this.OperationalData = new System.Windows.Forms.TabPage();
            this.ODHeaderGB = new System.Windows.Forms.GroupBox();
            this.ODPlantLb = new System.Windows.Forms.Label();
            this.ODHeaderPanel = new System.Windows.Forms.Panel();
            this.ODTypeLb = new System.Windows.Forms.Label();
            this.ODUnitLb = new System.Windows.Forms.Label();
            this.ODPackLb = new System.Windows.Forms.Label();
            this.L8 = new System.Windows.Forms.Label();
            this.L7 = new System.Windows.Forms.Label();
            this.L6 = new System.Windows.Forms.Label();
            this.L5 = new System.Windows.Forms.Label();
            this.ODMainPanel = new System.Windows.Forms.Panel();
            this.ODUnitPanel = new System.Windows.Forms.Panel();
            this.ODUnitMainGB = new System.Windows.Forms.GroupBox();
            this.odunitmd2Valid = new System.Windows.Forms.Label();
            this.odunitmd1Valid = new System.Windows.Forms.Label();
            this.panel32 = new System.Windows.Forms.Panel();
            this.ODUnitMainEndDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label37 = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.ODUnitMainStartDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label38 = new System.Windows.Forms.Label();
            this.ODUnitMainCheck = new System.Windows.Forms.CheckBox();
            this.ODUnitPowerGB = new System.Windows.Forms.GroupBox();
            this.odunitpd2Valid = new System.Windows.Forms.Label();
            this.odunitpd1Valid = new System.Windows.Forms.Label();
            this.ODUnitPowerGrid2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn62 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn65 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn66 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ODUnitPowerGrid1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn67 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn68 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn69 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn70 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn71 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn72 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn73 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn74 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn75 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn76 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn77 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn78 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel25 = new System.Windows.Forms.Panel();
            this.ODUnitPowerEndDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label29 = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.ODUnitPowerStartDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label30 = new System.Windows.Forms.Label();
            this.ODUnitPowerCheck = new System.Windows.Forms.CheckBox();
            this.ODUnitServiceGB = new System.Windows.Forms.GroupBox();
            this.odunitosd2Valid = new System.Windows.Forms.Label();
            this.odunitosd1Valid = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.ODUnitServiceEndDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label34 = new System.Windows.Forms.Label();
            this.panel31 = new System.Windows.Forms.Panel();
            this.ODUnitServiceStartDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label35 = new System.Windows.Forms.Label();
            this.ODUnitNoOffCheck = new System.Windows.Forms.CheckBox();
            this.ODUnitNoOnCheck = new System.Windows.Forms.CheckBox();
            this.ODUnitOutCheck = new System.Windows.Forms.CheckBox();
            this.ODUnitFuelGB = new System.Windows.Forms.GroupBox();
            this.odunitsfd2Valid = new System.Windows.Forms.Label();
            this.odunitsfd1Valid = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.ODUnitFuelTB = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.ODUnitFuelEndDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label32 = new System.Windows.Forms.Label();
            this.panel29 = new System.Windows.Forms.Panel();
            this.ODUnitFuelStartDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label33 = new System.Windows.Forms.Label();
            this.ODUnitFuelCheck = new System.Windows.Forms.CheckBox();
            this.ODSaveBtn = new System.Windows.Forms.Button();
            this.ODPlantPanel = new System.Windows.Forms.Panel();
            this.ODPowerMinGB = new System.Windows.Forms.GroupBox();
            this.ODpd2Valid = new System.Windows.Forms.Label();
            this.ODpd1Valid = new System.Windows.Forms.Label();
            this.ODPowerGrid2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ODPowerGrid1 = new System.Windows.Forms.DataGridView();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel22 = new System.Windows.Forms.Panel();
            this.ODPowerEndDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label27 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.ODPowerStartDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label28 = new System.Windows.Forms.Label();
            this.ODPowerMinCheck = new System.Windows.Forms.CheckBox();
            this.ODFuelGB = new System.Windows.Forms.GroupBox();
            this.ODsfd2Valid = new System.Windows.Forms.Label();
            this.ODsfd1Valid = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.ODFuelQuantityTB = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.ODFuelEndDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.ODFuelStartDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.SecondFuelCheck = new System.Windows.Forms.CheckBox();
            this.ODServiceGB = new System.Windows.Forms.GroupBox();
            this.ODosd2Valid = new System.Windows.Forms.Label();
            this.ODosd1Valid = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.ODServiceEndDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.ODServiceStartDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.NoOffCheck = new System.Windows.Forms.CheckBox();
            this.NoOnCheck = new System.Windows.Forms.CheckBox();
            this.OutServiceCheck = new System.Windows.Forms.CheckBox();
            this.GeneralData = new System.Windows.Forms.TabPage();
            this.GDMainPanel = new System.Windows.Forms.Panel();
            this.GDUnitPanel = new System.Windows.Forms.Panel();
            this.Maintenancegb = new System.Windows.Forms.GroupBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.GDEndDateCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.GDStartDateCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.GDMainTypeTB = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.Currentgb = new System.Windows.Forms.GroupBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.GDFuelTB = new System.Windows.Forms.TextBox();
            this.GDFuelLb = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.GDStateTB = new System.Windows.Forms.TextBox();
            this.GDStateLb = new System.Windows.Forms.Label();
            this.Anonygb = new System.Windows.Forms.GroupBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.GDIntConsumeTB = new System.Windows.Forms.TextBox();
            this.GDConsLb = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.GDRampRateTB = new System.Windows.Forms.TextBox();
            this.GDRampLb = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.GDTimeHotStartTB = new System.Windows.Forms.TextBox();
            this.GDHotLb = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.GDTimeColdStartTB = new System.Windows.Forms.TextBox();
            this.GDColdLb = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.GDTimeDownTB = new System.Windows.Forms.TextBox();
            this.GDTimeDownLb = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.GDTimeUpTB = new System.Windows.Forms.TextBox();
            this.GDTUpLb = new System.Windows.Forms.Label();
            this.Powergb = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.GDPminTB = new System.Windows.Forms.TextBox();
            this.GDPminLb = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.GDPmaxTB = new System.Windows.Forms.TextBox();
            this.GDPmaxLb = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.GDcapacityTB = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TempGV = new System.Windows.Forms.DataGridView();
            this.GDNewBtn = new System.Windows.Forms.Button();
            this.GDDeleteBtn = new System.Windows.Forms.Button();
            this.GDSteamGroup = new System.Windows.Forms.GroupBox();
            this.Grid230 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CheckBox2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PlantGV2 = new System.Windows.Forms.DataGridView();
            this.unitCodeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packageCodeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unitTypeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.capacityDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pMinDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pMaxDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.GDGasGroup = new System.Windows.Forms.GroupBox();
            this.Grid400 = new System.Windows.Forms.DataGridView();
            this.lineCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fromBusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toBusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lineLengthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ownerGencoFromDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ownerGencoToDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CheckBox1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PlantGV1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PlantGV11 = new System.Windows.Forms.DataGridView();
            this.unitCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packageCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unitTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.capacityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pMaxDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pMinDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fuel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Maintenance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Outservice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GDHeaderGB = new System.Windows.Forms.GroupBox();
            this.GDPlantLb = new System.Windows.Forms.Label();
            this.GDHeaderPanel = new System.Windows.Forms.Panel();
            this.GDTypeLb = new System.Windows.Forms.Label();
            this.GDUnitLb = new System.Windows.Forms.Label();
            this.GDPackLb = new System.Windows.Forms.Label();
            this.L4 = new System.Windows.Forms.Label();
            this.L3 = new System.Windows.Forms.Label();
            this.L2 = new System.Windows.Forms.Label();
            this.L1 = new System.Windows.Forms.Label();
            this.MainTabs = new System.Windows.Forms.TabControl();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.linesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPalntDBDataSet4BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPalntDBDataSet4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unitsDataMainBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linesBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailFRM002BindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.BiddingStrategy.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.panel80.SuspendLayout();
            this.panel81.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel75.SuspendLayout();
            this.panel77.SuspendLayout();
            this.panel78.SuspendLayout();
            this.panel79.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grBoxCustomize.SuspendLayout();
            this.menuStrip7.SuspendLayout();
            this.menuStrip8.SuspendLayout();
            this.menuStrip6.SuspendLayout();
            this.menuStrip5.SuspendLayout();
            this.menuStrip3.SuspendLayout();
            this.menuStrip4.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.FinancialReport.SuspendLayout();
            this.FRMainPanel.SuspendLayout();
            this.FRUnitPanel.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.panel65.SuspendLayout();
            this.panel68.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.panel66.SuspendLayout();
            this.panel67.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.panel62.SuspendLayout();
            this.panel63.SuspendLayout();
            this.panel64.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.panel59.SuspendLayout();
            this.panel60.SuspendLayout();
            this.panel61.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.panel53.SuspendLayout();
            this.panel57.SuspendLayout();
            this.panel58.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.panel76.SuspendLayout();
            this.panel69.SuspendLayout();
            this.panel70.SuspendLayout();
            this.panel71.SuspendLayout();
            this.panel73.SuspendLayout();
            this.panel74.SuspendLayout();
            this.FRPlantPanel.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.panel54.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel56.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel48.SuspendLayout();
            this.panel49.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panel52.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel46.SuspendLayout();
            this.FRHeaderGb.SuspendLayout();
            this.FRHeaderPanel.SuspendLayout();
            this.BidData.SuspendLayout();
            this.BDMainPanel.SuspendLayout();
            this.BDCur2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BDCurGrid)).BeginInit();
            this.BDCur1.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel40.SuspendLayout();
            this.BDHeaderGb.SuspendLayout();
            this.BDHeaderPanel.SuspendLayout();
            this.MarketResults.SuspendLayout();
            this.MRMainPanel.SuspendLayout();
            this.MRPlantCurGb.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel35.SuspendLayout();
            this.MRGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MRCurGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MRCurGrid1)).BeginInit();
            this.MRUnitCurGb.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel72.SuspendLayout();
            this.MRHeaderGB.SuspendLayout();
            this.MRHeaderPanel.SuspendLayout();
            this.OperationalData.SuspendLayout();
            this.ODHeaderGB.SuspendLayout();
            this.ODHeaderPanel.SuspendLayout();
            this.ODMainPanel.SuspendLayout();
            this.ODUnitPanel.SuspendLayout();
            this.ODUnitMainGB.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel33.SuspendLayout();
            this.ODUnitPowerGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODUnitPowerGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ODUnitPowerGrid1)).BeginInit();
            this.panel25.SuspendLayout();
            this.panel26.SuspendLayout();
            this.ODUnitServiceGB.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel31.SuspendLayout();
            this.ODUnitFuelGB.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel29.SuspendLayout();
            this.ODPlantPanel.SuspendLayout();
            this.ODPowerMinGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODPowerGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ODPowerGrid1)).BeginInit();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            this.ODFuelGB.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel20.SuspendLayout();
            this.ODServiceGB.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel17.SuspendLayout();
            this.GeneralData.SuspendLayout();
            this.GDMainPanel.SuspendLayout();
            this.GDUnitPanel.SuspendLayout();
            this.Maintenancegb.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel14.SuspendLayout();
            this.Currentgb.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.Anonygb.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.Powergb.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TempGV)).BeginInit();
            this.GDSteamGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid230)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlantGV2)).BeginInit();
            this.GDGasGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid400)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlantGV1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlantGV11)).BeginInit();
            this.GDHeaderGB.SuspendLayout();
            this.GDHeaderPanel.SuspendLayout();
            this.MainTabs.SuspendLayout();
            this.SuspendLayout();
            // 
            // linesBindingSource
            // 
            this.linesBindingSource.DataMember = "Lines";
            this.linesBindingSource.DataSource = this.powerPalntDBDataSet4BindingSource;
            // 
            // powerPalntDBDataSet4BindingSource
            // 
            this.powerPalntDBDataSet4BindingSource.DataSource = this.powerPalntDBDataSet4;
            this.powerPalntDBDataSet4BindingSource.Position = 0;
            // 
            // powerPalntDBDataSet4
            // 
            this.powerPalntDBDataSet4.DataSetName = "PowerPalntDBDataSet4";
            this.powerPalntDBDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // unitsDataMainBindingSource
            // 
            this.unitsDataMainBindingSource.DataMember = "UnitsDataMain";
            this.unitsDataMainBindingSource.DataSource = this.powerPalntDBDataSet4BindingSource;
            // 
            // linesBindingSource1
            // 
            this.linesBindingSource1.DataMember = "Lines";
            this.linesBindingSource1.DataSource = this.powerPalntDBDataSet4BindingSource;
            // 
            // detailFRM002BindingSource
            // 
            this.detailFRM002BindingSource.DataMember = "DetailFRM002";
            this.detailFRM002BindingSource.DataSource = this.powerPalntDBDataSet4BindingSource;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::PowerPlantProject.Properties.Resources._10;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.treeView1);
            this.panel1.Location = new System.Drawing.Point(12, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(210, 259);
            this.panel1.TabIndex = 11;
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(17, 14);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(179, 227);
            this.treeView1.TabIndex = 0;
            this.treeView1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BackgroundImage = global::PowerPlantProject.Properties.Resources._10;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.treeView2);
            this.panel2.Location = new System.Drawing.Point(12, 319);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(210, 366);
            this.panel2.TabIndex = 12;
            // 
            // treeView2
            // 
            this.treeView2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.treeView2.Location = new System.Drawing.Point(17, 19);
            this.treeView2.Name = "treeView2";
            this.treeView2.Size = new System.Drawing.Size(179, 331);
            this.treeView2.TabIndex = 0;
            this.treeView2.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView2_NodeMouseClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackgroundImage = global::PowerPlantProject.Properties.Resources.untitled6;
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.importToolStripMenuItem,
            this.exportToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1028, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.menuToolStripMenuItem.Text = "&Menu";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.addToolStripMenuItem.Text = "&Add New Plant";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.closeToolStripMenuItem.Text = "&Exit";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.marketToolStripMenuItem,
            this.capacityToolStripMenuItem,
            this.toolStripMenuItem1,
            this.autoPathToolStripMenuItem});
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.importToolStripMenuItem.Text = "&Import";
            // 
            // marketToolStripMenuItem
            // 
            this.marketToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m002ToolStripMenuItem,
            this.m005ToolStripMenuItem,
            this.toolStripMenuItem3,
            this.averagePriceToolStripMenuItem,
            this.loadForecastingToolStripMenuItem,
            this.toolStripMenuItem4,
            this.betToolStripMenuItem,
            this.toolStripMenuItem2});
            this.marketToolStripMenuItem.Name = "marketToolStripMenuItem";
            this.marketToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.marketToolStripMenuItem.Text = "Market";
            // 
            // m002ToolStripMenuItem
            // 
            this.m002ToolStripMenuItem.Name = "m002ToolStripMenuItem";
            this.m002ToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.m002ToolStripMenuItem.Text = "M002";
            this.m002ToolStripMenuItem.Click += new System.EventHandler(this.m002ToolStripMenuItem_Click);
            // 
            // m005ToolStripMenuItem
            // 
            this.m005ToolStripMenuItem.Name = "m005ToolStripMenuItem";
            this.m005ToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.m005ToolStripMenuItem.Text = "M005";
            this.m005ToolStripMenuItem.Click += new System.EventHandler(this.m005ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem3.Text = "---------------------------";
            // 
            // averagePriceToolStripMenuItem
            // 
            this.averagePriceToolStripMenuItem.Name = "averagePriceToolStripMenuItem";
            this.averagePriceToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.averagePriceToolStripMenuItem.Text = "Average Price";
            this.averagePriceToolStripMenuItem.Click += new System.EventHandler(this.averagePriceToolStripMenuItem_Click);
            // 
            // loadForecastingToolStripMenuItem
            // 
            this.loadForecastingToolStripMenuItem.Name = "loadForecastingToolStripMenuItem";
            this.loadForecastingToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.loadForecastingToolStripMenuItem.Text = "Load Forecasting";
            this.loadForecastingToolStripMenuItem.Click += new System.EventHandler(this.loadForecastingToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem4.Text = "---------------------------";
            // 
            // betToolStripMenuItem
            // 
            this.betToolStripMenuItem.Name = "betToolStripMenuItem";
            this.betToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.betToolStripMenuItem.Text = "Interval";
            this.betToolStripMenuItem.Click += new System.EventHandler(this.betToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem2.Text = "Base Data";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // capacityToolStripMenuItem
            // 
            this.capacityToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.annualFactorToolStripMenuItem,
            this.weekFactorToolStripMenuItem});
            this.capacityToolStripMenuItem.Name = "capacityToolStripMenuItem";
            this.capacityToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.capacityToolStripMenuItem.Text = "Capacity";
            // 
            // annualFactorToolStripMenuItem
            // 
            this.annualFactorToolStripMenuItem.Name = "annualFactorToolStripMenuItem";
            this.annualFactorToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.annualFactorToolStripMenuItem.Text = "Annual Factor";
            this.annualFactorToolStripMenuItem.Click += new System.EventHandler(this.annualFactorToolStripMenuItem_Click);
            // 
            // weekFactorToolStripMenuItem
            // 
            this.weekFactorToolStripMenuItem.Name = "weekFactorToolStripMenuItem";
            this.weekFactorToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.weekFactorToolStripMenuItem.Text = "Week Factor";
            this.weekFactorToolStripMenuItem.Click += new System.EventHandler(this.weekFactorToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(182, 22);
            this.toolStripMenuItem1.Text = "---------------------------";
            // 
            // autoPathToolStripMenuItem
            // 
            this.autoPathToolStripMenuItem.Name = "autoPathToolStripMenuItem";
            this.autoPathToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.autoPathToolStripMenuItem.Text = "Auto Path";
            this.autoPathToolStripMenuItem.Click += new System.EventHandler(this.autoPathToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Enabled = false;
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.exportToolStripMenuItem.Text = "&Export";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Enabled = false;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // linesTableAdapter
            // 
            this.linesTableAdapter.ClearBeforeFill = true;
            // 
            // unitsDataMainTableAdapter
            // 
            this.unitsDataMainTableAdapter.ClearBeforeFill = true;
            // 
            // detailFRM002TableAdapter
            // 
            this.detailFRM002TableAdapter.ClearBeforeFill = true;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // BiddingStrategy
            // 
            this.BiddingStrategy.BackgroundImage = global::PowerPlantProject.Properties.Resources._10;
            this.BiddingStrategy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BiddingStrategy.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BiddingStrategy.Controls.Add(this.groupBox4);
            this.BiddingStrategy.Controls.Add(this.groupBox2);
            this.BiddingStrategy.Controls.Add(this.groupBox17);
            this.BiddingStrategy.Controls.Add(this.groupBox1);
            this.BiddingStrategy.Controls.Add(this.grBoxCustomize);
            this.BiddingStrategy.Location = new System.Drawing.Point(4, 29);
            this.BiddingStrategy.Name = "BiddingStrategy";
            this.BiddingStrategy.Size = new System.Drawing.Size(757, 625);
            this.BiddingStrategy.TabIndex = 4;
            this.BiddingStrategy.Text = "Bidding Strategy";
            this.BiddingStrategy.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.groupBox4.Controls.Add(this.btnExport);
            this.groupBox4.Controls.Add(this.rdStrategyBidding);
            this.groupBox4.Controls.Add(this.rdBidAllocation);
            this.groupBox4.Controls.Add(this.rdForecastingPrice);
            this.groupBox4.Controls.Add(this.btnRunBidding);
            this.groupBox4.Location = new System.Drawing.Point(8, 532);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox4.Size = new System.Drawing.Size(737, 73);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Run";
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.BackColor = System.Drawing.Color.Transparent;
            this.btnExport.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnExport.Location = new System.Drawing.Point(609, 34);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(80, 24);
            this.btnExport.TabIndex = 39;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // rdStrategyBidding
            // 
            this.rdStrategyBidding.AutoSize = true;
            this.rdStrategyBidding.Location = new System.Drawing.Point(196, 19);
            this.rdStrategyBidding.Name = "rdStrategyBidding";
            this.rdStrategyBidding.Size = new System.Drawing.Size(102, 17);
            this.rdStrategyBidding.TabIndex = 38;
            this.rdStrategyBidding.TabStop = true;
            this.rdStrategyBidding.Text = "Bidding Strategy";
            this.rdStrategyBidding.UseVisualStyleBackColor = true;
            // 
            // rdBidAllocation
            // 
            this.rdBidAllocation.AutoSize = true;
            this.rdBidAllocation.Location = new System.Drawing.Point(35, 42);
            this.rdBidAllocation.Name = "rdBidAllocation";
            this.rdBidAllocation.Size = new System.Drawing.Size(89, 17);
            this.rdBidAllocation.TabIndex = 37;
            this.rdBidAllocation.TabStop = true;
            this.rdBidAllocation.Text = "Bid Allocation";
            this.rdBidAllocation.UseVisualStyleBackColor = true;
            // 
            // rdForecastingPrice
            // 
            this.rdForecastingPrice.AutoSize = true;
            this.rdForecastingPrice.Location = new System.Drawing.Point(35, 22);
            this.rdForecastingPrice.Name = "rdForecastingPrice";
            this.rdForecastingPrice.Size = new System.Drawing.Size(107, 17);
            this.rdForecastingPrice.TabIndex = 36;
            this.rdForecastingPrice.TabStop = true;
            this.rdForecastingPrice.Text = "Price Forecasting";
            this.rdForecastingPrice.UseVisualStyleBackColor = true;
            // 
            // btnRunBidding
            // 
            this.btnRunBidding.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRunBidding.BackColor = System.Drawing.Color.Transparent;
            this.btnRunBidding.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnRunBidding.Location = new System.Drawing.Point(517, 34);
            this.btnRunBidding.Name = "btnRunBidding";
            this.btnRunBidding.Size = new System.Drawing.Size(80, 24);
            this.btnRunBidding.TabIndex = 32;
            this.btnRunBidding.Text = "Run";
            this.btnRunBidding.UseVisualStyleBackColor = false;
            this.btnRunBidding.Click += new System.EventHandler(this.btnRunBidding_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.lstStatus);
            this.groupBox2.Location = new System.Drawing.Point(8, 442);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox2.Size = new System.Drawing.Size(737, 83);
            this.groupBox2.TabIndex = 39;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Status";
            // 
            // lstStatus
            // 
            this.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstStatus.BackColor = System.Drawing.SystemColors.Window;
            this.lstStatus.Location = new System.Drawing.Point(14, 20);
            this.lstStatus.Multiline = true;
            this.lstStatus.Name = "lstStatus";
            this.lstStatus.ReadOnly = true;
            this.lstStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lstStatus.Size = new System.Drawing.Size(709, 54);
            this.lstStatus.TabIndex = 0;
            // 
            // groupBox17
            // 
            this.groupBox17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox17.Controls.Add(this.groupBox18);
            this.groupBox17.Controls.Add(this.groupBox19);
            this.groupBox17.Location = new System.Drawing.Point(7, 65);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox17.Size = new System.Drawing.Size(738, 194);
            this.groupBox17.TabIndex = 42;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Run";
            // 
            // groupBox18
            // 
            this.groupBox18.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox18.BackColor = System.Drawing.Color.Azure;
            this.groupBox18.Controls.Add(this.panel80);
            this.groupBox18.Controls.Add(this.panel81);
            this.groupBox18.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox18.Font = new System.Drawing.Font("Majalla Condensed", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox18.ForeColor = System.Drawing.Color.Black;
            this.groupBox18.Location = new System.Drawing.Point(179, 14);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox18.Size = new System.Drawing.Size(381, 75);
            this.groupBox18.TabIndex = 20;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = " DATE";
            // 
            // panel80
            // 
            this.panel80.BackColor = System.Drawing.Color.SkyBlue;
            this.panel80.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel80.Controls.Add(this.label15);
            this.panel80.Controls.Add(this.label16);
            this.panel80.Controls.Add(this.datePickerBidding);
            this.panel80.Location = new System.Drawing.Point(210, 17);
            this.panel80.Name = "panel80";
            this.panel80.Size = new System.Drawing.Size(140, 49);
            this.panel80.TabIndex = 6;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label15.Location = new System.Drawing.Point(33, 8);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 14);
            this.label15.TabIndex = 2;
            this.label15.Text = "Bidding Date";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label16.Location = new System.Drawing.Point(23, 8);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(0, 14);
            this.label16.TabIndex = 1;
            // 
            // datePickerBidding
            // 
            this.datePickerBidding.HasButtons = false;
            this.datePickerBidding.Location = new System.Drawing.Point(14, 24);
            this.datePickerBidding.Name = "datePickerBidding";
            this.datePickerBidding.Readonly = true;
            this.datePickerBidding.Size = new System.Drawing.Size(120, 20);
            this.datePickerBidding.TabIndex = 32;
            this.datePickerBidding.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.datePickerBidding.SelectedDateTimeChanged += new System.EventHandler(this.datePickerBidding_SelectedDateTimeChanged);
            // 
            // panel81
            // 
            this.panel81.BackColor = System.Drawing.Color.SkyBlue;
            this.panel81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel81.Controls.Add(this.datePickerCurrent);
            this.panel81.Controls.Add(this.label17);
            this.panel81.Location = new System.Drawing.Point(35, 17);
            this.panel81.Name = "panel81";
            this.panel81.Size = new System.Drawing.Size(140, 49);
            this.panel81.TabIndex = 6;
            // 
            // datePickerCurrent
            // 
            this.datePickerCurrent.HasButtons = false;
            this.datePickerCurrent.Location = new System.Drawing.Point(3, 24);
            this.datePickerCurrent.Name = "datePickerCurrent";
            this.datePickerCurrent.Readonly = true;
            this.datePickerCurrent.Size = new System.Drawing.Size(120, 20);
            this.datePickerCurrent.TabIndex = 33;
            this.datePickerCurrent.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label17.Location = new System.Drawing.Point(23, 7);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 14);
            this.label17.TabIndex = 0;
            this.label17.Text = "Current Date";
            // 
            // groupBox19
            // 
            this.groupBox19.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.groupBox19.BackColor = System.Drawing.Color.Azure;
            this.groupBox19.Controls.Add(this.panel24);
            this.groupBox19.Controls.Add(this.panel75);
            this.groupBox19.Controls.Add(this.panel77);
            this.groupBox19.Controls.Add(this.panel78);
            this.groupBox19.Controls.Add(this.panel79);
            this.groupBox19.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox19.Font = new System.Drawing.Font("Majalla Condensed", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox19.ForeColor = System.Drawing.Color.Black;
            this.groupBox19.Location = new System.Drawing.Point(15, 97);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox19.Size = new System.Drawing.Size(709, 88);
            this.groupBox19.TabIndex = 21;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "setting";
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.SkyBlue;
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel24.Controls.Add(this.cmbPeakBid);
            this.panel24.Controls.Add(this.label5);
            this.panel24.Controls.Add(this.label6);
            this.panel24.Controls.Add(this.label7);
            this.panel24.Location = new System.Drawing.Point(572, 21);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(120, 49);
            this.panel24.TabIndex = 9;
            // 
            // cmbPeakBid
            // 
            this.cmbPeakBid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cmbPeakBid.FormattingEnabled = true;
            this.cmbPeakBid.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeakBid.Location = new System.Drawing.Point(7, 21);
            this.cmbPeakBid.Name = "cmbPeakBid";
            this.cmbPeakBid.Size = new System.Drawing.Size(104, 21);
            this.cmbPeakBid.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(31, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 14);
            this.label5.TabIndex = 3;
            this.label5.Text = " Peak Bid";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label6.Location = new System.Drawing.Point(25, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 14);
            this.label6.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(23, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 14);
            this.label7.TabIndex = 1;
            // 
            // panel75
            // 
            this.panel75.BackColor = System.Drawing.Color.SkyBlue;
            this.panel75.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel75.Controls.Add(this.cmbCostLevel);
            this.panel75.Controls.Add(this.label9);
            this.panel75.Controls.Add(this.label10);
            this.panel75.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel75.Location = new System.Drawing.Point(433, 21);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(120, 49);
            this.panel75.TabIndex = 8;
            // 
            // cmbCostLevel
            // 
            this.cmbCostLevel.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbCostLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cmbCostLevel.Items.AddRange(new object[] {
            "Level 1",
            "Level 2",
            "Level 3",
            "Level 4",
            "Level 5",
            "Level 6",
            "Level 7"});
            this.cmbCostLevel.Location = new System.Drawing.Point(7, 22);
            this.cmbCostLevel.Name = "cmbCostLevel";
            this.cmbCostLevel.Size = new System.Drawing.Size(104, 21);
            this.cmbCostLevel.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label9.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label9.Location = new System.Drawing.Point(17, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 14);
            this.label9.TabIndex = 2;
            this.label9.Text = "Cost Level";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label10.Location = new System.Drawing.Point(23, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 14);
            this.label10.TabIndex = 1;
            // 
            // panel77
            // 
            this.panel77.BackColor = System.Drawing.Color.SkyBlue;
            this.panel77.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel77.Controls.Add(this.cmbStrategyBidding);
            this.panel77.Controls.Add(this.label11);
            this.panel77.Controls.Add(this.label12);
            this.panel77.Location = new System.Drawing.Point(155, 21);
            this.panel77.Name = "panel77";
            this.panel77.Size = new System.Drawing.Size(120, 49);
            this.panel77.TabIndex = 6;
            // 
            // cmbStrategyBidding
            // 
            this.cmbStrategyBidding.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbStrategyBidding.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cmbStrategyBidding.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategyBidding.Location = new System.Drawing.Point(9, 22);
            this.cmbStrategyBidding.Name = "cmbStrategyBidding";
            this.cmbStrategyBidding.Size = new System.Drawing.Size(104, 21);
            this.cmbStrategyBidding.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.label11.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label11.Location = new System.Drawing.Point(6, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(106, 14);
            this.label11.TabIndex = 2;
            this.label11.Text = " Strategy Bidding  ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label12.Location = new System.Drawing.Point(23, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 14);
            this.label12.TabIndex = 1;
            // 
            // panel78
            // 
            this.panel78.BackColor = System.Drawing.Color.SkyBlue;
            this.panel78.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel78.Controls.Add(this.txtTraining);
            this.panel78.Controls.Add(this.label13);
            this.panel78.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel78.Location = new System.Drawing.Point(294, 21);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(120, 49);
            this.panel78.TabIndex = 7;
            // 
            // txtTraining
            // 
            this.txtTraining.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTraining.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtTraining.Location = new System.Drawing.Point(7, 22);
            this.txtTraining.Name = "txtTraining";
            this.txtTraining.Size = new System.Drawing.Size(104, 20);
            this.txtTraining.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label13.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label13.Location = new System.Drawing.Point(12, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 14);
            this.label13.TabIndex = 0;
            this.label13.Text = "Training Days";
            // 
            // panel79
            // 
            this.panel79.BackColor = System.Drawing.Color.SkyBlue;
            this.panel79.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel79.Controls.Add(this.cmbRunSetting);
            this.panel79.Controls.Add(this.label14);
            this.panel79.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel79.Location = new System.Drawing.Point(16, 21);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(120, 49);
            this.panel79.TabIndex = 6;
            // 
            // cmbRunSetting
            // 
            this.cmbRunSetting.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbRunSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cmbRunSetting.Items.AddRange(new object[] {
            "Automatic",
            "Customize"});
            this.cmbRunSetting.Location = new System.Drawing.Point(7, 22);
            this.cmbRunSetting.Name = "cmbRunSetting";
            this.cmbRunSetting.Size = new System.Drawing.Size(104, 21);
            this.cmbRunSetting.TabIndex = 1;
            this.cmbRunSetting.SelectedIndexChanged += new System.EventHandler(this.cmbRunSetting_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label14.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label14.Location = new System.Drawing.Point(23, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 14);
            this.label14.TabIndex = 0;
            this.label14.Text = "Run Setting";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox1.BackColor = System.Drawing.Color.Azure;
            this.groupBox1.Controls.Add(this.lblPlantValue);
            this.groupBox1.Controls.Add(this.lblGencoValue);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Majalla Condensed", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(15, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox1.Size = new System.Drawing.Size(720, 53);
            this.groupBox1.TabIndex = 41;
            this.groupBox1.TabStop = false;
            // 
            // lblPlantValue
            // 
            this.lblPlantValue.AutoSize = true;
            this.lblPlantValue.BackColor = System.Drawing.Color.Transparent;
            this.lblPlantValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblPlantValue.Location = new System.Drawing.Point(493, 23);
            this.lblPlantValue.Name = "lblPlantValue";
            this.lblPlantValue.Size = new System.Drawing.Size(0, 14);
            this.lblPlantValue.TabIndex = 10;
            this.lblPlantValue.TextChanged += new System.EventHandler(this.lblPlantValue_TextChanged);
            // 
            // lblGencoValue
            // 
            this.lblGencoValue.AutoSize = true;
            this.lblGencoValue.BackColor = System.Drawing.Color.Transparent;
            this.lblGencoValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblGencoValue.Location = new System.Drawing.Point(89, 23);
            this.lblGencoValue.Name = "lblGencoValue";
            this.lblGencoValue.Size = new System.Drawing.Size(0, 14);
            this.lblGencoValue.TabIndex = 9;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label18.Location = new System.Drawing.Point(443, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(46, 14);
            this.label18.TabIndex = 8;
            this.label18.Text = "Plant  : ";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label23.Location = new System.Drawing.Point(32, 23);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 14);
            this.label23.TabIndex = 7;
            this.label23.Text = "Genco  : ";
            // 
            // grBoxCustomize
            // 
            this.grBoxCustomize.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.grBoxCustomize.Controls.Add(this.menuStrip7);
            this.grBoxCustomize.Controls.Add(this.menuStrip8);
            this.grBoxCustomize.Controls.Add(this.menuStrip6);
            this.grBoxCustomize.Controls.Add(this.menuStrip5);
            this.grBoxCustomize.Controls.Add(this.menuStrip3);
            this.grBoxCustomize.Controls.Add(this.menuStrip4);
            this.grBoxCustomize.Controls.Add(this.menuStrip2);
            this.grBoxCustomize.Location = new System.Drawing.Point(7, 261);
            this.grBoxCustomize.Name = "grBoxCustomize";
            this.grBoxCustomize.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grBoxCustomize.Size = new System.Drawing.Size(740, 176);
            this.grBoxCustomize.TabIndex = 38;
            this.grBoxCustomize.TabStop = false;
            this.grBoxCustomize.Text = "Customize";
            // 
            // menuStrip7
            // 
            this.menuStrip7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip7.AutoSize = false;
            this.menuStrip7.BackColor = System.Drawing.Color.SkyBlue;
            this.menuStrip7.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem17,
            this.cmbStrategy6,
            this.cmbPeak6,
            this.toolStripMenuItem18,
            this.cmbStrategy12,
            this.cmbPeak12,
            this.toolStripMenuItem19,
            this.cmbStrategy18,
            this.cmbPeak18,
            this.toolStripMenuItem20,
            this.cmbStrategy24,
            this.cmbPeak24});
            this.menuStrip7.Location = new System.Drawing.Point(6, 147);
            this.menuStrip7.Name = "menuStrip7";
            this.menuStrip7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip7.Size = new System.Drawing.Size(730, 24);
            this.menuStrip7.TabIndex = 5;
            this.menuStrip7.Text = "menuStrip7";
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(57, 20);
            this.toolStripMenuItem17.Text = "hour - 6";
            // 
            // cmbStrategy6
            // 
            this.cmbStrategy6.AutoSize = false;
            this.cmbStrategy6.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy6.Name = "cmbStrategy6";
            this.cmbStrategy6.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak6
            // 
            this.cmbPeak6.AutoSize = false;
            this.cmbPeak6.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak6.Name = "cmbPeak6";
            this.cmbPeak6.Size = new System.Drawing.Size(38, 21);
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(75, 20);
            this.toolStripMenuItem18.Text = "    hour - 12";
            // 
            // cmbStrategy12
            // 
            this.cmbStrategy12.AutoSize = false;
            this.cmbStrategy12.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy12.Name = "cmbStrategy12";
            this.cmbStrategy12.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak12
            // 
            this.cmbPeak12.AutoSize = false;
            this.cmbPeak12.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak12.Name = "cmbPeak12";
            this.cmbPeak12.Size = new System.Drawing.Size(38, 21);
            // 
            // toolStripMenuItem19
            // 
            this.toolStripMenuItem19.Name = "toolStripMenuItem19";
            this.toolStripMenuItem19.Size = new System.Drawing.Size(78, 20);
            this.toolStripMenuItem19.Text = "     hour - 18";
            // 
            // cmbStrategy18
            // 
            this.cmbStrategy18.AutoSize = false;
            this.cmbStrategy18.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy18.Name = "cmbStrategy18";
            this.cmbStrategy18.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak18
            // 
            this.cmbPeak18.AutoSize = false;
            this.cmbPeak18.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak18.Name = "cmbPeak18";
            this.cmbPeak18.Size = new System.Drawing.Size(38, 21);
            // 
            // toolStripMenuItem20
            // 
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            this.toolStripMenuItem20.Size = new System.Drawing.Size(78, 20);
            this.toolStripMenuItem20.Text = "     hour - 24";
            // 
            // cmbStrategy24
            // 
            this.cmbStrategy24.AutoSize = false;
            this.cmbStrategy24.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy24.Name = "cmbStrategy24";
            this.cmbStrategy24.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak24
            // 
            this.cmbPeak24.AutoSize = false;
            this.cmbPeak24.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak24.Name = "cmbPeak24";
            this.cmbPeak24.Size = new System.Drawing.Size(38, 21);
            // 
            // menuStrip8
            // 
            this.menuStrip8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip8.AutoSize = false;
            this.menuStrip8.BackColor = System.Drawing.Color.SkyBlue;
            this.menuStrip8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.menuStrip8.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip8.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.menuStrip8.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem21,
            this.toolStripMenuItem22,
            this.toolStripMenuItem23,
            this.toolStripMenuItem24,
            this.strategyToolStripMenuItem,
            this.peakToolStripMenuItem,
            this.strategyToolStripMenuItem1,
            this.strategyToolStripMenuItem2,
            this.peakToolStripMenuItem1,
            this.hourToolStripMenuItem,
            this.strategyToolStripMenuItem3,
            this.peakToolStripMenuItem2});
            this.menuStrip8.Location = new System.Drawing.Point(6, 16);
            this.menuStrip8.Margin = new System.Windows.Forms.Padding(1);
            this.menuStrip8.Name = "menuStrip8";
            this.menuStrip8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip8.Size = new System.Drawing.Size(730, 24);
            this.menuStrip8.TabIndex = 5;
            this.menuStrip8.Text = "menuStrip8";
            // 
            // toolStripMenuItem21
            // 
            this.toolStripMenuItem21.Name = "toolStripMenuItem21";
            this.toolStripMenuItem21.Size = new System.Drawing.Size(56, 20);
            this.toolStripMenuItem21.Text = "hour     ";
            // 
            // toolStripMenuItem22
            // 
            this.toolStripMenuItem22.Name = "toolStripMenuItem22";
            this.toolStripMenuItem22.Size = new System.Drawing.Size(70, 20);
            this.toolStripMenuItem22.Text = " Strategy  ";
            // 
            // toolStripMenuItem23
            // 
            this.toolStripMenuItem23.Name = "toolStripMenuItem23";
            this.toolStripMenuItem23.Size = new System.Drawing.Size(45, 20);
            this.toolStripMenuItem23.Text = "Peak ";
            // 
            // toolStripMenuItem24
            // 
            this.toolStripMenuItem24.Name = "toolStripMenuItem24";
            this.toolStripMenuItem24.Size = new System.Drawing.Size(56, 20);
            this.toolStripMenuItem24.Text = "  hour   ";
            // 
            // strategyToolStripMenuItem
            // 
            this.strategyToolStripMenuItem.Name = "strategyToolStripMenuItem";
            this.strategyToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.strategyToolStripMenuItem.Text = "     Strategy ";
            // 
            // peakToolStripMenuItem
            // 
            this.peakToolStripMenuItem.Name = "peakToolStripMenuItem";
            this.peakToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.peakToolStripMenuItem.Text = " Peak   ";
            // 
            // strategyToolStripMenuItem1
            // 
            this.strategyToolStripMenuItem1.Name = "strategyToolStripMenuItem1";
            this.strategyToolStripMenuItem1.Size = new System.Drawing.Size(62, 20);
            this.strategyToolStripMenuItem1.Text = " hour      ";
            // 
            // strategyToolStripMenuItem2
            // 
            this.strategyToolStripMenuItem2.Name = "strategyToolStripMenuItem2";
            this.strategyToolStripMenuItem2.Size = new System.Drawing.Size(67, 20);
            this.strategyToolStripMenuItem2.Text = "  Strategy";
            // 
            // peakToolStripMenuItem1
            // 
            this.peakToolStripMenuItem1.Name = "peakToolStripMenuItem1";
            this.peakToolStripMenuItem1.Size = new System.Drawing.Size(51, 20);
            this.peakToolStripMenuItem1.Text = " Peak  ";
            // 
            // hourToolStripMenuItem
            // 
            this.hourToolStripMenuItem.Name = "hourToolStripMenuItem";
            this.hourToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.hourToolStripMenuItem.Text = "  hour     ";
            // 
            // strategyToolStripMenuItem3
            // 
            this.strategyToolStripMenuItem3.Name = "strategyToolStripMenuItem3";
            this.strategyToolStripMenuItem3.Size = new System.Drawing.Size(70, 20);
            this.strategyToolStripMenuItem3.Text = "  Strategy ";
            // 
            // peakToolStripMenuItem2
            // 
            this.peakToolStripMenuItem2.Name = "peakToolStripMenuItem2";
            this.peakToolStripMenuItem2.Size = new System.Drawing.Size(48, 20);
            this.peakToolStripMenuItem2.Text = " Peak ";
            // 
            // menuStrip6
            // 
            this.menuStrip6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip6.AutoSize = false;
            this.menuStrip6.BackColor = System.Drawing.Color.SkyBlue;
            this.menuStrip6.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem13,
            this.cmbStrategy5,
            this.cmbPeak5,
            this.toolStripMenuItem14,
            this.cmbStrategy11,
            this.cmbPeak11,
            this.toolStripMenuItem15,
            this.cmbStrategy17,
            this.cmbPeak17,
            this.toolStripMenuItem16,
            this.cmbStrategy23,
            this.cmbPeak23});
            this.menuStrip6.Location = new System.Drawing.Point(6, 124);
            this.menuStrip6.Name = "menuStrip6";
            this.menuStrip6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip6.Size = new System.Drawing.Size(730, 24);
            this.menuStrip6.TabIndex = 4;
            this.menuStrip6.Text = "menuStrip6";
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(57, 20);
            this.toolStripMenuItem13.Text = "hour - 5";
            // 
            // cmbStrategy5
            // 
            this.cmbStrategy5.AutoSize = false;
            this.cmbStrategy5.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy5.Name = "cmbStrategy5";
            this.cmbStrategy5.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak5
            // 
            this.cmbPeak5.AutoSize = false;
            this.cmbPeak5.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak5.Name = "cmbPeak5";
            this.cmbPeak5.Size = new System.Drawing.Size(38, 21);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(75, 20);
            this.toolStripMenuItem14.Text = "    hour - 11";
            // 
            // cmbStrategy11
            // 
            this.cmbStrategy11.AutoSize = false;
            this.cmbStrategy11.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy11.Name = "cmbStrategy11";
            this.cmbStrategy11.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak11
            // 
            this.cmbPeak11.AutoSize = false;
            this.cmbPeak11.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak11.Name = "cmbPeak11";
            this.cmbPeak11.Size = new System.Drawing.Size(38, 21);
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(78, 20);
            this.toolStripMenuItem15.Text = "     hour - 17";
            // 
            // cmbStrategy17
            // 
            this.cmbStrategy17.AutoSize = false;
            this.cmbStrategy17.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy17.Name = "cmbStrategy17";
            this.cmbStrategy17.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak17
            // 
            this.cmbPeak17.AutoSize = false;
            this.cmbPeak17.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak17.Name = "cmbPeak17";
            this.cmbPeak17.Size = new System.Drawing.Size(38, 21);
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(78, 20);
            this.toolStripMenuItem16.Text = "     hour - 23";
            // 
            // cmbStrategy23
            // 
            this.cmbStrategy23.AutoSize = false;
            this.cmbStrategy23.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy23.Name = "cmbStrategy23";
            this.cmbStrategy23.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak23
            // 
            this.cmbPeak23.AutoSize = false;
            this.cmbPeak23.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak23.Name = "cmbPeak23";
            this.cmbPeak23.Size = new System.Drawing.Size(38, 21);
            // 
            // menuStrip5
            // 
            this.menuStrip5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip5.AutoSize = false;
            this.menuStrip5.BackColor = System.Drawing.Color.SkyBlue;
            this.menuStrip5.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem9,
            this.cmbStrategy4,
            this.cmbPeak4,
            this.toolStripMenuItem10,
            this.cmbStrategy10,
            this.cmbPeak10,
            this.toolStripMenuItem11,
            this.cmbStrategy16,
            this.cmbPeak16,
            this.toolStripMenuItem12,
            this.cmbStrategy22,
            this.cmbPeak22});
            this.menuStrip5.Location = new System.Drawing.Point(6, 104);
            this.menuStrip5.Name = "menuStrip5";
            this.menuStrip5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip5.Size = new System.Drawing.Size(730, 24);
            this.menuStrip5.TabIndex = 3;
            this.menuStrip5.Text = "menuStrip5";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(57, 20);
            this.toolStripMenuItem9.Text = "hour - 4";
            // 
            // cmbStrategy4
            // 
            this.cmbStrategy4.AutoSize = false;
            this.cmbStrategy4.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy4.Name = "cmbStrategy4";
            this.cmbStrategy4.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak4
            // 
            this.cmbPeak4.AutoSize = false;
            this.cmbPeak4.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak4.Name = "cmbPeak4";
            this.cmbPeak4.Size = new System.Drawing.Size(38, 21);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(75, 20);
            this.toolStripMenuItem10.Text = "    hour - 10";
            // 
            // cmbStrategy10
            // 
            this.cmbStrategy10.AutoSize = false;
            this.cmbStrategy10.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy10.Name = "cmbStrategy10";
            this.cmbStrategy10.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak10
            // 
            this.cmbPeak10.AutoSize = false;
            this.cmbPeak10.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak10.Name = "cmbPeak10";
            this.cmbPeak10.Size = new System.Drawing.Size(38, 21);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(78, 20);
            this.toolStripMenuItem11.Text = "     hour - 16";
            // 
            // cmbStrategy16
            // 
            this.cmbStrategy16.AutoSize = false;
            this.cmbStrategy16.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy16.Name = "cmbStrategy16";
            this.cmbStrategy16.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak16
            // 
            this.cmbPeak16.AutoSize = false;
            this.cmbPeak16.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak16.Name = "cmbPeak16";
            this.cmbPeak16.Size = new System.Drawing.Size(38, 21);
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(78, 20);
            this.toolStripMenuItem12.Text = "     hour - 22";
            // 
            // cmbStrategy22
            // 
            this.cmbStrategy22.AutoSize = false;
            this.cmbStrategy22.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy22.Name = "cmbStrategy22";
            this.cmbStrategy22.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak22
            // 
            this.cmbPeak22.AutoSize = false;
            this.cmbPeak22.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak22.Name = "cmbPeak22";
            this.cmbPeak22.Size = new System.Drawing.Size(38, 21);
            // 
            // menuStrip3
            // 
            this.menuStrip3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip3.AutoSize = false;
            this.menuStrip3.BackColor = System.Drawing.Color.SkyBlue;
            this.menuStrip3.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.cmbStrategy2,
            this.cmbPeak2,
            this.toolStripMenuItem6,
            this.cmbStrategy8,
            this.cmbPeak8,
            this.toolStripMenuItem7,
            this.cmbStrategy14,
            this.cmbPeak14,
            this.toolStripMenuItem8,
            this.cmbStrategy20,
            this.cmbPeak20});
            this.menuStrip3.Location = new System.Drawing.Point(6, 61);
            this.menuStrip3.Name = "menuStrip3";
            this.menuStrip3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip3.Size = new System.Drawing.Size(730, 24);
            this.menuStrip3.TabIndex = 1;
            this.menuStrip3.Text = "menuStrip3";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(57, 20);
            this.toolStripMenuItem5.Text = "hour - 2";
            // 
            // cmbStrategy2
            // 
            this.cmbStrategy2.AutoSize = false;
            this.cmbStrategy2.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy2.Name = "cmbStrategy2";
            this.cmbStrategy2.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak2
            // 
            this.cmbPeak2.AutoSize = false;
            this.cmbPeak2.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak2.Name = "cmbPeak2";
            this.cmbPeak2.Size = new System.Drawing.Size(38, 21);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(75, 20);
            this.toolStripMenuItem6.Text = "    hour - 8  ";
            // 
            // cmbStrategy8
            // 
            this.cmbStrategy8.AutoSize = false;
            this.cmbStrategy8.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy8.Name = "cmbStrategy8";
            this.cmbStrategy8.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak8
            // 
            this.cmbPeak8.AutoSize = false;
            this.cmbPeak8.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak8.Name = "cmbPeak8";
            this.cmbPeak8.Size = new System.Drawing.Size(38, 21);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(78, 20);
            this.toolStripMenuItem7.Text = "     hour - 14";
            // 
            // cmbStrategy14
            // 
            this.cmbStrategy14.AutoSize = false;
            this.cmbStrategy14.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy14.Name = "cmbStrategy14";
            this.cmbStrategy14.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak14
            // 
            this.cmbPeak14.AutoSize = false;
            this.cmbPeak14.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak14.Name = "cmbPeak14";
            this.cmbPeak14.Size = new System.Drawing.Size(38, 21);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(78, 20);
            this.toolStripMenuItem8.Text = "     hour - 20";
            // 
            // cmbStrategy20
            // 
            this.cmbStrategy20.AutoSize = false;
            this.cmbStrategy20.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy20.Name = "cmbStrategy20";
            this.cmbStrategy20.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak20
            // 
            this.cmbPeak20.AutoSize = false;
            this.cmbPeak20.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak20.Name = "cmbPeak20";
            this.cmbPeak20.Size = new System.Drawing.Size(38, 21);
            // 
            // menuStrip4
            // 
            this.menuStrip4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip4.AutoSize = false;
            this.menuStrip4.BackColor = System.Drawing.Color.SkyBlue;
            this.menuStrip4.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem25,
            this.cmbStrategy3,
            this.cmbPeak3,
            this.toolStripMenuItem26,
            this.cmbStrategy9,
            this.cmbPeak9,
            this.toolStripMenuItem27,
            this.cmbStrategy15,
            this.cmbPeak15,
            this.toolStripMenuItem28,
            this.cmbStrategy21,
            this.cmbPeak21});
            this.menuStrip4.Location = new System.Drawing.Point(6, 84);
            this.menuStrip4.Name = "menuStrip4";
            this.menuStrip4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip4.Size = new System.Drawing.Size(730, 24);
            this.menuStrip4.TabIndex = 2;
            this.menuStrip4.Text = "menuStrip4";
            // 
            // toolStripMenuItem25
            // 
            this.toolStripMenuItem25.Name = "toolStripMenuItem25";
            this.toolStripMenuItem25.Size = new System.Drawing.Size(57, 20);
            this.toolStripMenuItem25.Text = "hour - 3";
            // 
            // cmbStrategy3
            // 
            this.cmbStrategy3.AutoSize = false;
            this.cmbStrategy3.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy3.Name = "cmbStrategy3";
            this.cmbStrategy3.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak3
            // 
            this.cmbPeak3.AutoSize = false;
            this.cmbPeak3.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak3.Name = "cmbPeak3";
            this.cmbPeak3.Size = new System.Drawing.Size(38, 21);
            // 
            // toolStripMenuItem26
            // 
            this.toolStripMenuItem26.Name = "toolStripMenuItem26";
            this.toolStripMenuItem26.Size = new System.Drawing.Size(75, 20);
            this.toolStripMenuItem26.Text = "    hour - 9  ";
            // 
            // cmbStrategy9
            // 
            this.cmbStrategy9.AutoSize = false;
            this.cmbStrategy9.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy9.Name = "cmbStrategy9";
            this.cmbStrategy9.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak9
            // 
            this.cmbPeak9.AutoSize = false;
            this.cmbPeak9.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak9.Name = "cmbPeak9";
            this.cmbPeak9.Size = new System.Drawing.Size(38, 21);
            // 
            // toolStripMenuItem27
            // 
            this.toolStripMenuItem27.Name = "toolStripMenuItem27";
            this.toolStripMenuItem27.Size = new System.Drawing.Size(78, 20);
            this.toolStripMenuItem27.Text = "     hour - 15";
            // 
            // cmbStrategy15
            // 
            this.cmbStrategy15.AutoSize = false;
            this.cmbStrategy15.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy15.Name = "cmbStrategy15";
            this.cmbStrategy15.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak15
            // 
            this.cmbPeak15.AutoSize = false;
            this.cmbPeak15.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak15.Name = "cmbPeak15";
            this.cmbPeak15.Size = new System.Drawing.Size(38, 21);
            // 
            // toolStripMenuItem28
            // 
            this.toolStripMenuItem28.Name = "toolStripMenuItem28";
            this.toolStripMenuItem28.Size = new System.Drawing.Size(78, 20);
            this.toolStripMenuItem28.Text = "     hour - 21";
            // 
            // cmbStrategy21
            // 
            this.cmbStrategy21.AutoSize = false;
            this.cmbStrategy21.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy21.Name = "cmbStrategy21";
            this.cmbStrategy21.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak21
            // 
            this.cmbPeak21.AutoSize = false;
            this.cmbPeak21.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak21.Name = "cmbPeak21";
            this.cmbPeak21.Size = new System.Drawing.Size(38, 21);
            // 
            // menuStrip2
            // 
            this.menuStrip2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip2.AutoSize = false;
            this.menuStrip2.BackColor = System.Drawing.Color.SkyBlue;
            this.menuStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hour1ToolStripMenuItem,
            this.cmbStrategy1,
            this.cmbPeak1,
            this.hour2ToolStripMenuItem,
            this.cmbStrategy7,
            this.cmbPeak7,
            this.hour3ToolStripMenuItem,
            this.cmbStrategy13,
            this.cmbPeak13,
            this.hour4ToolStripMenuItem,
            this.cmbStrategy19,
            this.cmbPeak19});
            this.menuStrip2.Location = new System.Drawing.Point(6, 39);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip2.Size = new System.Drawing.Size(730, 24);
            this.menuStrip2.TabIndex = 0;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // hour1ToolStripMenuItem
            // 
            this.hour1ToolStripMenuItem.Name = "hour1ToolStripMenuItem";
            this.hour1ToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.hour1ToolStripMenuItem.Text = "hour - 1";
            // 
            // cmbStrategy1
            // 
            this.cmbStrategy1.AutoSize = false;
            this.cmbStrategy1.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy1.Name = "cmbStrategy1";
            this.cmbStrategy1.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak1
            // 
            this.cmbPeak1.AutoSize = false;
            this.cmbPeak1.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak1.Name = "cmbPeak1";
            this.cmbPeak1.Size = new System.Drawing.Size(38, 21);
            // 
            // hour2ToolStripMenuItem
            // 
            this.hour2ToolStripMenuItem.Name = "hour2ToolStripMenuItem";
            this.hour2ToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.hour2ToolStripMenuItem.Text = "    hour - 7  ";
            // 
            // cmbStrategy7
            // 
            this.cmbStrategy7.AutoSize = false;
            this.cmbStrategy7.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy7.Name = "cmbStrategy7";
            this.cmbStrategy7.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak7
            // 
            this.cmbPeak7.AutoSize = false;
            this.cmbPeak7.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak7.Name = "cmbPeak7";
            this.cmbPeak7.Size = new System.Drawing.Size(38, 21);
            // 
            // hour3ToolStripMenuItem
            // 
            this.hour3ToolStripMenuItem.Name = "hour3ToolStripMenuItem";
            this.hour3ToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.hour3ToolStripMenuItem.Text = "     hour - 13";
            // 
            // cmbStrategy13
            // 
            this.cmbStrategy13.AutoSize = false;
            this.cmbStrategy13.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy13.Name = "cmbStrategy13";
            this.cmbStrategy13.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak13
            // 
            this.cmbPeak13.AutoSize = false;
            this.cmbPeak13.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak13.Name = "cmbPeak13";
            this.cmbPeak13.Size = new System.Drawing.Size(38, 21);
            // 
            // hour4ToolStripMenuItem
            // 
            this.hour4ToolStripMenuItem.Name = "hour4ToolStripMenuItem";
            this.hour4ToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.hour4ToolStripMenuItem.Text = "     hour - 19";
            // 
            // cmbStrategy19
            // 
            this.cmbStrategy19.AutoSize = false;
            this.cmbStrategy19.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy19.Name = "cmbStrategy19";
            this.cmbStrategy19.Size = new System.Drawing.Size(65, 21);
            // 
            // cmbPeak19
            // 
            this.cmbPeak19.AutoSize = false;
            this.cmbPeak19.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak19.Name = "cmbPeak19";
            this.cmbPeak19.Size = new System.Drawing.Size(38, 21);
            // 
            // FinancialReport
            // 
            this.FinancialReport.BackgroundImage = global::PowerPlantProject.Properties.Resources._10;
            this.FinancialReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FinancialReport.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FinancialReport.Controls.Add(this.FRMainPanel);
            this.FinancialReport.Controls.Add(this.FRHeaderGb);
            this.FinancialReport.Location = new System.Drawing.Point(4, 29);
            this.FinancialReport.Name = "FinancialReport";
            this.FinancialReport.Size = new System.Drawing.Size(757, 625);
            this.FinancialReport.TabIndex = 3;
            this.FinancialReport.Text = "Financial Report";
            this.FinancialReport.UseVisualStyleBackColor = true;
            // 
            // FRMainPanel
            // 
            this.FRMainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.FRMainPanel.Controls.Add(this.FRUpdateBtn);
            this.FRMainPanel.Controls.Add(this.FROKBtn);
            this.FRMainPanel.Controls.Add(this.FRPlantPanel);
            this.FRMainPanel.Controls.Add(this.FRUnitPanel);
            this.FRMainPanel.Location = new System.Drawing.Point(3, 65);
            this.FRMainPanel.Name = "FRMainPanel";
            this.FRMainPanel.Size = new System.Drawing.Size(741, 555);
            this.FRMainPanel.TabIndex = 23;
            // 
            // FRUnitPanel
            // 
            this.FRUnitPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.FRUnitPanel.Controls.Add(this.groupBox9);
            this.FRUnitPanel.Controls.Add(this.groupBox10);
            this.FRUnitPanel.Location = new System.Drawing.Point(19, 2);
            this.FRUnitPanel.Name = "FRUnitPanel";
            this.FRUnitPanel.Size = new System.Drawing.Size(703, 522);
            this.FRUnitPanel.TabIndex = 37;
            this.FRUnitPanel.Visible = false;
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.groupBox15);
            this.groupBox9.Controls.Add(this.groupBox14);
            this.groupBox9.Controls.Add(this.groupBox13);
            this.groupBox9.Controls.Add(this.groupBox12);
            this.groupBox9.Controls.Add(this.groupBox11);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox9.Location = new System.Drawing.Point(3, 23);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(697, 302);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "COST FUNCTION";
            // 
            // groupBox15
            // 
            this.groupBox15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox15.BackColor = System.Drawing.Color.Azure;
            this.groupBox15.Controls.Add(this.panel65);
            this.groupBox15.Controls.Add(this.panel68);
            this.groupBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox15.Location = new System.Drawing.Point(401, 208);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(252, 86);
            this.groupBox15.TabIndex = 28;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "START-SHUTDOWN";
            // 
            // panel65
            // 
            this.panel65.BackColor = System.Drawing.Color.SkyBlue;
            this.panel65.Controls.Add(this.FRUnitHotTb);
            this.panel65.Controls.Add(this.label69);
            this.panel65.Location = new System.Drawing.Point(137, 19);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(95, 49);
            this.panel65.TabIndex = 2;
            // 
            // FRUnitHotTb
            // 
            this.FRUnitHotTb.BackColor = System.Drawing.Color.White;
            this.FRUnitHotTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitHotTb.Name = "FRUnitHotTb";
            this.FRUnitHotTb.ReadOnly = true;
            this.FRUnitHotTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitHotTb.TabIndex = 1;
            this.FRUnitHotTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitHotTb.Validated += new System.EventHandler(this.FRUnitHotTb_Validated);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.ForeColor = System.Drawing.SystemColors.Window;
            this.label69.Location = new System.Drawing.Point(26, 4);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(33, 13);
            this.label69.TabIndex = 0;
            this.label69.Text = "HOT";
            // 
            // panel68
            // 
            this.panel68.BackColor = System.Drawing.Color.SkyBlue;
            this.panel68.Controls.Add(this.FRUnitColdTb);
            this.panel68.Controls.Add(this.label72);
            this.panel68.Location = new System.Drawing.Point(17, 19);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(95, 49);
            this.panel68.TabIndex = 1;
            // 
            // FRUnitColdTb
            // 
            this.FRUnitColdTb.BackColor = System.Drawing.Color.White;
            this.FRUnitColdTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitColdTb.Name = "FRUnitColdTb";
            this.FRUnitColdTb.ReadOnly = true;
            this.FRUnitColdTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitColdTb.TabIndex = 1;
            this.FRUnitColdTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitColdTb.Validated += new System.EventHandler(this.FRUnitColdTb_Validated);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.ForeColor = System.Drawing.SystemColors.Window;
            this.label72.Location = new System.Drawing.Point(25, 6);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(40, 13);
            this.label72.TabIndex = 0;
            this.label72.Text = "COLD";
            // 
            // groupBox14
            // 
            this.groupBox14.BackColor = System.Drawing.Color.Azure;
            this.groupBox14.Controls.Add(this.panel66);
            this.groupBox14.Controls.Add(this.panel67);
            this.groupBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox14.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox14.Location = new System.Drawing.Point(32, 208);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(252, 86);
            this.groupBox14.TabIndex = 27;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "MAINTENANCE";
            // 
            // panel66
            // 
            this.panel66.BackColor = System.Drawing.Color.SkyBlue;
            this.panel66.Controls.Add(this.FRUnitBmainTb);
            this.panel66.Controls.Add(this.label70);
            this.panel66.Location = new System.Drawing.Point(137, 19);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(95, 49);
            this.panel66.TabIndex = 2;
            // 
            // FRUnitBmainTb
            // 
            this.FRUnitBmainTb.BackColor = System.Drawing.Color.White;
            this.FRUnitBmainTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitBmainTb.Name = "FRUnitBmainTb";
            this.FRUnitBmainTb.ReadOnly = true;
            this.FRUnitBmainTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitBmainTb.TabIndex = 1;
            this.FRUnitBmainTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitBmainTb.Validated += new System.EventHandler(this.FRUnitBmainTb_Validated);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.ForeColor = System.Drawing.SystemColors.Window;
            this.label70.Location = new System.Drawing.Point(3, 4);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(87, 13);
            this.label70.TabIndex = 0;
            this.label70.Text = "Bmaintenance";
            // 
            // panel67
            // 
            this.panel67.BackColor = System.Drawing.Color.SkyBlue;
            this.panel67.Controls.Add(this.FRUnitAmainTb);
            this.panel67.Controls.Add(this.label71);
            this.panel67.Location = new System.Drawing.Point(17, 19);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(95, 49);
            this.panel67.TabIndex = 1;
            // 
            // FRUnitAmainTb
            // 
            this.FRUnitAmainTb.BackColor = System.Drawing.Color.White;
            this.FRUnitAmainTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitAmainTb.Name = "FRUnitAmainTb";
            this.FRUnitAmainTb.ReadOnly = true;
            this.FRUnitAmainTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitAmainTb.TabIndex = 1;
            this.FRUnitAmainTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitAmainTb.Validated += new System.EventHandler(this.FRUnitAmainTb_Validated);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.ForeColor = System.Drawing.SystemColors.Window;
            this.label71.Location = new System.Drawing.Point(6, 4);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(87, 13);
            this.label71.TabIndex = 0;
            this.label71.Text = "Amaintenance";
            // 
            // groupBox13
            // 
            this.groupBox13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox13.BackColor = System.Drawing.Color.Azure;
            this.groupBox13.Controls.Add(this.panel62);
            this.groupBox13.Controls.Add(this.panel63);
            this.groupBox13.Controls.Add(this.panel64);
            this.groupBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox13.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox13.Location = new System.Drawing.Point(355, 109);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(335, 86);
            this.groupBox13.TabIndex = 26;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "SECONDRY FUEL";
            // 
            // panel62
            // 
            this.panel62.BackColor = System.Drawing.Color.SkyBlue;
            this.panel62.Controls.Add(this.FRUnitCmargTb2);
            this.panel62.Controls.Add(this.label66);
            this.panel62.Location = new System.Drawing.Point(232, 19);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(95, 49);
            this.panel62.TabIndex = 3;
            // 
            // FRUnitCmargTb2
            // 
            this.FRUnitCmargTb2.BackColor = System.Drawing.Color.White;
            this.FRUnitCmargTb2.Location = new System.Drawing.Point(9, 22);
            this.FRUnitCmargTb2.Name = "FRUnitCmargTb2";
            this.FRUnitCmargTb2.ReadOnly = true;
            this.FRUnitCmargTb2.Size = new System.Drawing.Size(75, 20);
            this.FRUnitCmargTb2.TabIndex = 1;
            this.FRUnitCmargTb2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitCmargTb2.Validated += new System.EventHandler(this.FRUnitCmargTb2_Validated);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.ForeColor = System.Drawing.SystemColors.Window;
            this.label66.Location = new System.Drawing.Point(16, 4);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(62, 13);
            this.label66.TabIndex = 0;
            this.label66.Text = "Cmarginal";
            // 
            // panel63
            // 
            this.panel63.BackColor = System.Drawing.Color.SkyBlue;
            this.panel63.Controls.Add(this.FRUnitBmargTb2);
            this.panel63.Controls.Add(this.label67);
            this.panel63.Location = new System.Drawing.Point(118, 19);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(95, 49);
            this.panel63.TabIndex = 2;
            // 
            // FRUnitBmargTb2
            // 
            this.FRUnitBmargTb2.BackColor = System.Drawing.Color.White;
            this.FRUnitBmargTb2.Location = new System.Drawing.Point(9, 22);
            this.FRUnitBmargTb2.Name = "FRUnitBmargTb2";
            this.FRUnitBmargTb2.ReadOnly = true;
            this.FRUnitBmargTb2.Size = new System.Drawing.Size(75, 20);
            this.FRUnitBmargTb2.TabIndex = 1;
            this.FRUnitBmargTb2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitBmargTb2.Validated += new System.EventHandler(this.FRUnitBmargTb2_Validated);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.ForeColor = System.Drawing.SystemColors.Window;
            this.label67.Location = new System.Drawing.Point(15, 4);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(62, 13);
            this.label67.TabIndex = 0;
            this.label67.Text = "Bmarginal";
            // 
            // panel64
            // 
            this.panel64.BackColor = System.Drawing.Color.SkyBlue;
            this.panel64.Controls.Add(this.FRUnitAmargTb2);
            this.panel64.Controls.Add(this.label68);
            this.panel64.Location = new System.Drawing.Point(6, 19);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(95, 49);
            this.panel64.TabIndex = 1;
            // 
            // FRUnitAmargTb2
            // 
            this.FRUnitAmargTb2.BackColor = System.Drawing.Color.White;
            this.FRUnitAmargTb2.Location = new System.Drawing.Point(9, 22);
            this.FRUnitAmargTb2.Name = "FRUnitAmargTb2";
            this.FRUnitAmargTb2.ReadOnly = true;
            this.FRUnitAmargTb2.Size = new System.Drawing.Size(75, 20);
            this.FRUnitAmargTb2.TabIndex = 1;
            this.FRUnitAmargTb2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitAmargTb2.Validated += new System.EventHandler(this.FRUnitAmargTb2_Validated);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.ForeColor = System.Drawing.SystemColors.Window;
            this.label68.Location = new System.Drawing.Point(14, 4);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(62, 13);
            this.label68.TabIndex = 0;
            this.label68.Text = "Amarginal";
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.Azure;
            this.groupBox12.Controls.Add(this.panel59);
            this.groupBox12.Controls.Add(this.panel60);
            this.groupBox12.Controls.Add(this.panel61);
            this.groupBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox12.Location = new System.Drawing.Point(8, 109);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(335, 86);
            this.groupBox12.TabIndex = 25;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "PRIMARY FUEL";
            // 
            // panel59
            // 
            this.panel59.BackColor = System.Drawing.Color.SkyBlue;
            this.panel59.Controls.Add(this.FRUnitCmargTb1);
            this.panel59.Controls.Add(this.label63);
            this.panel59.Location = new System.Drawing.Point(232, 19);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(95, 49);
            this.panel59.TabIndex = 3;
            // 
            // FRUnitCmargTb1
            // 
            this.FRUnitCmargTb1.BackColor = System.Drawing.Color.White;
            this.FRUnitCmargTb1.Location = new System.Drawing.Point(9, 22);
            this.FRUnitCmargTb1.Name = "FRUnitCmargTb1";
            this.FRUnitCmargTb1.ReadOnly = true;
            this.FRUnitCmargTb1.Size = new System.Drawing.Size(75, 20);
            this.FRUnitCmargTb1.TabIndex = 1;
            this.FRUnitCmargTb1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitCmargTb1.Validated += new System.EventHandler(this.FRUnitCmargTb1_Validated);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.ForeColor = System.Drawing.SystemColors.Window;
            this.label63.Location = new System.Drawing.Point(15, 4);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(62, 13);
            this.label63.TabIndex = 0;
            this.label63.Text = "Cmarginal";
            // 
            // panel60
            // 
            this.panel60.BackColor = System.Drawing.Color.SkyBlue;
            this.panel60.Controls.Add(this.FRUnitBmargTb1);
            this.panel60.Controls.Add(this.label64);
            this.panel60.Location = new System.Drawing.Point(118, 19);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(95, 49);
            this.panel60.TabIndex = 2;
            // 
            // FRUnitBmargTb1
            // 
            this.FRUnitBmargTb1.BackColor = System.Drawing.Color.White;
            this.FRUnitBmargTb1.Location = new System.Drawing.Point(9, 22);
            this.FRUnitBmargTb1.Name = "FRUnitBmargTb1";
            this.FRUnitBmargTb1.ReadOnly = true;
            this.FRUnitBmargTb1.Size = new System.Drawing.Size(75, 20);
            this.FRUnitBmargTb1.TabIndex = 1;
            this.FRUnitBmargTb1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitBmargTb1.Validated += new System.EventHandler(this.FRUnitBmargTb1_Validated);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.ForeColor = System.Drawing.SystemColors.Window;
            this.label64.Location = new System.Drawing.Point(15, 4);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(62, 13);
            this.label64.TabIndex = 0;
            this.label64.Text = "Bmarginal";
            // 
            // panel61
            // 
            this.panel61.BackColor = System.Drawing.Color.SkyBlue;
            this.panel61.Controls.Add(this.FRUnitAmargTb1);
            this.panel61.Controls.Add(this.label65);
            this.panel61.Location = new System.Drawing.Point(6, 19);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(95, 49);
            this.panel61.TabIndex = 1;
            // 
            // FRUnitAmargTb1
            // 
            this.FRUnitAmargTb1.BackColor = System.Drawing.Color.White;
            this.FRUnitAmargTb1.Location = new System.Drawing.Point(9, 22);
            this.FRUnitAmargTb1.Name = "FRUnitAmargTb1";
            this.FRUnitAmargTb1.ReadOnly = true;
            this.FRUnitAmargTb1.Size = new System.Drawing.Size(75, 20);
            this.FRUnitAmargTb1.TabIndex = 1;
            this.FRUnitAmargTb1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitAmargTb1.Validated += new System.EventHandler(this.FRUnitAmargTb1_Validated);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.ForeColor = System.Drawing.SystemColors.Window;
            this.label65.Location = new System.Drawing.Point(15, 4);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(62, 13);
            this.label65.TabIndex = 0;
            this.label65.Text = "Amarginal";
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.Color.Azure;
            this.groupBox11.Controls.Add(this.panel53);
            this.groupBox11.Controls.Add(this.panel57);
            this.groupBox11.Controls.Add(this.panel58);
            this.groupBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox11.Location = new System.Drawing.Point(183, 14);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(335, 86);
            this.groupBox11.TabIndex = 24;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "COST";
            // 
            // panel53
            // 
            this.panel53.BackColor = System.Drawing.Color.SkyBlue;
            this.panel53.Controls.Add(this.FRUnitVariableTb);
            this.panel53.Controls.Add(this.label57);
            this.panel53.Location = new System.Drawing.Point(232, 19);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(95, 49);
            this.panel53.TabIndex = 3;
            // 
            // FRUnitVariableTb
            // 
            this.FRUnitVariableTb.BackColor = System.Drawing.Color.White;
            this.FRUnitVariableTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitVariableTb.Name = "FRUnitVariableTb";
            this.FRUnitVariableTb.ReadOnly = true;
            this.FRUnitVariableTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitVariableTb.TabIndex = 1;
            this.FRUnitVariableTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitVariableTb.Validated += new System.EventHandler(this.FRUnitVariableTb_Validated);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.ForeColor = System.Drawing.SystemColors.Window;
            this.label57.Location = new System.Drawing.Point(14, 4);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(67, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "VARIABLE";
            // 
            // panel57
            // 
            this.panel57.BackColor = System.Drawing.Color.SkyBlue;
            this.panel57.Controls.Add(this.FRUnitFixedTb);
            this.panel57.Controls.Add(this.label61);
            this.panel57.Location = new System.Drawing.Point(118, 19);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(95, 49);
            this.panel57.TabIndex = 2;
            // 
            // FRUnitFixedTb
            // 
            this.FRUnitFixedTb.BackColor = System.Drawing.Color.White;
            this.FRUnitFixedTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitFixedTb.Name = "FRUnitFixedTb";
            this.FRUnitFixedTb.ReadOnly = true;
            this.FRUnitFixedTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitFixedTb.TabIndex = 1;
            this.FRUnitFixedTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitFixedTb.Validated += new System.EventHandler(this.FRUnitFixedTb_Validated);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.ForeColor = System.Drawing.SystemColors.Window;
            this.label61.Location = new System.Drawing.Point(21, 4);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(43, 13);
            this.label61.TabIndex = 0;
            this.label61.Text = "FIXED";
            // 
            // panel58
            // 
            this.panel58.BackColor = System.Drawing.Color.SkyBlue;
            this.panel58.Controls.Add(this.FRUnitCapitalTb);
            this.panel58.Controls.Add(this.label62);
            this.panel58.Location = new System.Drawing.Point(6, 19);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(95, 49);
            this.panel58.TabIndex = 1;
            // 
            // FRUnitCapitalTb
            // 
            this.FRUnitCapitalTb.BackColor = System.Drawing.Color.White;
            this.FRUnitCapitalTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitCapitalTb.Name = "FRUnitCapitalTb";
            this.FRUnitCapitalTb.ReadOnly = true;
            this.FRUnitCapitalTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitCapitalTb.TabIndex = 1;
            this.FRUnitCapitalTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.ForeColor = System.Drawing.SystemColors.Window;
            this.label62.Location = new System.Drawing.Point(21, 4);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(58, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "CAPITAL";
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox10.Controls.Add(this.FRUnitCal);
            this.groupBox10.Controls.Add(this.groupBox16);
            this.groupBox10.Controls.Add(this.label73);
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox10.Location = new System.Drawing.Point(6, 342);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(694, 160);
            this.groupBox10.TabIndex = 1;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "REVENUE";
            // 
            // FRUnitCal
            // 
            this.FRUnitCal.HasButtons = true;
            this.FRUnitCal.Location = new System.Drawing.Point(53, 15);
            this.FRUnitCal.Name = "FRUnitCal";
            this.FRUnitCal.Readonly = true;
            this.FRUnitCal.Size = new System.Drawing.Size(120, 20);
            this.FRUnitCal.TabIndex = 33;
            this.FRUnitCal.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.FRUnitCal.ValueChanged += new System.EventHandler(this.FRUnitCal_ValueChanged);
            // 
            // groupBox16
            // 
            this.groupBox16.BackColor = System.Drawing.Color.Azure;
            this.groupBox16.Controls.Add(this.panel76);
            this.groupBox16.Controls.Add(this.panel69);
            this.groupBox16.Controls.Add(this.panel70);
            this.groupBox16.Controls.Add(this.panel71);
            this.groupBox16.Controls.Add(this.panel73);
            this.groupBox16.Controls.Add(this.panel74);
            this.groupBox16.Location = new System.Drawing.Point(-10, 41);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(695, 79);
            this.groupBox16.TabIndex = 5;
            this.groupBox16.TabStop = false;
            // 
            // panel76
            // 
            this.panel76.BackColor = System.Drawing.Color.SkyBlue;
            this.panel76.Controls.Add(this.FRUnitIncomeTb);
            this.panel76.Controls.Add(this.label81);
            this.panel76.Location = new System.Drawing.Point(355, 19);
            this.panel76.Name = "panel76";
            this.panel76.Size = new System.Drawing.Size(95, 49);
            this.panel76.TabIndex = 1;
            // 
            // FRUnitIncomeTb
            // 
            this.FRUnitIncomeTb.BackColor = System.Drawing.Color.White;
            this.FRUnitIncomeTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitIncomeTb.Name = "FRUnitIncomeTb";
            this.FRUnitIncomeTb.ReadOnly = true;
            this.FRUnitIncomeTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitIncomeTb.TabIndex = 1;
            this.FRUnitIncomeTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.ForeColor = System.Drawing.SystemColors.Window;
            this.label81.Location = new System.Drawing.Point(19, 4);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(55, 13);
            this.label81.TabIndex = 0;
            this.label81.Text = "INCOME";
            // 
            // panel69
            // 
            this.panel69.BackColor = System.Drawing.Color.SkyBlue;
            this.panel69.Controls.Add(this.FRUnitEneryPayTb);
            this.panel69.Controls.Add(this.label74);
            this.panel69.Location = new System.Drawing.Point(580, 19);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(105, 49);
            this.panel69.TabIndex = 4;
            // 
            // FRUnitEneryPayTb
            // 
            this.FRUnitEneryPayTb.BackColor = System.Drawing.Color.White;
            this.FRUnitEneryPayTb.Location = new System.Drawing.Point(19, 21);
            this.FRUnitEneryPayTb.Name = "FRUnitEneryPayTb";
            this.FRUnitEneryPayTb.ReadOnly = true;
            this.FRUnitEneryPayTb.Size = new System.Drawing.Size(71, 20);
            this.FRUnitEneryPayTb.TabIndex = 1;
            this.FRUnitEneryPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.SystemColors.Window;
            this.label74.Location = new System.Drawing.Point(0, 4);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(98, 13);
            this.label74.TabIndex = 0;
            this.label74.Text = "Energy Payment";
            // 
            // panel70
            // 
            this.panel70.BackColor = System.Drawing.Color.SkyBlue;
            this.panel70.Controls.Add(this.FRUnitCapPayTb);
            this.panel70.Controls.Add(this.label75);
            this.panel70.Location = new System.Drawing.Point(460, 19);
            this.panel70.Name = "panel70";
            this.panel70.Size = new System.Drawing.Size(106, 49);
            this.panel70.TabIndex = 5;
            // 
            // FRUnitCapPayTb
            // 
            this.FRUnitCapPayTb.BackColor = System.Drawing.Color.White;
            this.FRUnitCapPayTb.Location = new System.Drawing.Point(13, 22);
            this.FRUnitCapPayTb.Name = "FRUnitCapPayTb";
            this.FRUnitCapPayTb.ReadOnly = true;
            this.FRUnitCapPayTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitCapPayTb.TabIndex = 1;
            this.FRUnitCapPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.SystemColors.Window;
            this.label75.Location = new System.Drawing.Point(0, 4);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(108, 13);
            this.label75.TabIndex = 0;
            this.label75.Text = "Capacity Payment";
            // 
            // panel71
            // 
            this.panel71.BackColor = System.Drawing.Color.SkyBlue;
            this.panel71.Controls.Add(this.FRUnitULPowerTb);
            this.panel71.Controls.Add(this.label76);
            this.panel71.Location = new System.Drawing.Point(253, 19);
            this.panel71.Name = "panel71";
            this.panel71.Size = new System.Drawing.Size(91, 49);
            this.panel71.TabIndex = 4;
            // 
            // FRUnitULPowerTb
            // 
            this.FRUnitULPowerTb.BackColor = System.Drawing.Color.White;
            this.FRUnitULPowerTb.Location = new System.Drawing.Point(6, 22);
            this.FRUnitULPowerTb.Name = "FRUnitULPowerTb";
            this.FRUnitULPowerTb.ReadOnly = true;
            this.FRUnitULPowerTb.Size = new System.Drawing.Size(79, 20);
            this.FRUnitULPowerTb.TabIndex = 1;
            this.FRUnitULPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.SystemColors.Window;
            this.label76.Location = new System.Drawing.Point(16, 4);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(62, 13);
            this.label76.TabIndex = 0;
            this.label76.Text = "UL Power";
            // 
            // panel73
            // 
            this.panel73.BackColor = System.Drawing.Color.SkyBlue;
            this.panel73.Controls.Add(this.FRUnitTotalPowerTb);
            this.panel73.Controls.Add(this.label78);
            this.panel73.Location = new System.Drawing.Point(142, 19);
            this.panel73.Name = "panel73";
            this.panel73.Size = new System.Drawing.Size(95, 49);
            this.panel73.TabIndex = 2;
            // 
            // FRUnitTotalPowerTb
            // 
            this.FRUnitTotalPowerTb.BackColor = System.Drawing.Color.White;
            this.FRUnitTotalPowerTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitTotalPowerTb.Name = "FRUnitTotalPowerTb";
            this.FRUnitTotalPowerTb.ReadOnly = true;
            this.FRUnitTotalPowerTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitTotalPowerTb.TabIndex = 1;
            this.FRUnitTotalPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.SystemColors.Window;
            this.label78.Location = new System.Drawing.Point(6, 4);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(75, 13);
            this.label78.TabIndex = 0;
            this.label78.Text = "Total Power";
            // 
            // panel74
            // 
            this.panel74.BackColor = System.Drawing.Color.SkyBlue;
            this.panel74.Controls.Add(this.FRUnitCapacityTb);
            this.panel74.Controls.Add(this.label79);
            this.panel74.Location = new System.Drawing.Point(16, 19);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(114, 49);
            this.panel74.TabIndex = 1;
            // 
            // FRUnitCapacityTb
            // 
            this.FRUnitCapacityTb.BackColor = System.Drawing.Color.White;
            this.FRUnitCapacityTb.Location = new System.Drawing.Point(20, 22);
            this.FRUnitCapacityTb.Name = "FRUnitCapacityTb";
            this.FRUnitCapacityTb.ReadOnly = true;
            this.FRUnitCapacityTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitCapacityTb.TabIndex = 1;
            this.FRUnitCapacityTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.SystemColors.Window;
            this.label79.Location = new System.Drawing.Point(-1, 4);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(112, 13);
            this.label79.TabIndex = 0;
            this.label79.Text = "Available Capacity";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(8, 18);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(42, 13);
            this.label73.TabIndex = 2;
            this.label73.Text = "Date :";
            // 
            // FRUpdateBtn
            // 
            this.FRUpdateBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRUpdateBtn.BackColor = System.Drawing.Color.SkyBlue;
            this.FRUpdateBtn.Enabled = false;
            this.FRUpdateBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.FRUpdateBtn.Location = new System.Drawing.Point(524, 530);
            this.FRUpdateBtn.Name = "FRUpdateBtn";
            this.FRUpdateBtn.Size = new System.Drawing.Size(75, 23);
            this.FRUpdateBtn.TabIndex = 36;
            this.FRUpdateBtn.Text = "Save";
            this.FRUpdateBtn.UseVisualStyleBackColor = false;
            this.FRUpdateBtn.Click += new System.EventHandler(this.FRUpdateBtn_Click);
            // 
            // FROKBtn
            // 
            this.FROKBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FROKBtn.BackColor = System.Drawing.Color.SkyBlue;
            this.FROKBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.FROKBtn.Location = new System.Drawing.Point(638, 530);
            this.FROKBtn.Name = "FROKBtn";
            this.FROKBtn.Size = new System.Drawing.Size(75, 23);
            this.FROKBtn.TabIndex = 35;
            this.FROKBtn.Text = "Edit Mode";
            this.FROKBtn.UseVisualStyleBackColor = false;
            this.FROKBtn.Click += new System.EventHandler(this.FROKBtn_Click);
            // 
            // FRPlantPanel
            // 
            this.FRPlantPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.FRPlantPanel.Controls.Add(this.groupBox5);
            this.FRPlantPanel.Location = new System.Drawing.Point(19, 48);
            this.FRPlantPanel.Name = "FRPlantPanel";
            this.FRPlantPanel.Size = new System.Drawing.Size(703, 424);
            this.FRPlantPanel.TabIndex = 34;
            this.FRPlantPanel.Visible = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox5.Controls.Add(this.FRPlantCal);
            this.groupBox5.Controls.Add(this.groupBox8);
            this.groupBox5.Controls.Add(this.groupBox7);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Controls.Add(this.label44);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox5.Location = new System.Drawing.Point(0, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(697, 402);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "FINANCIAL";
            // 
            // FRPlantCal
            // 
            this.FRPlantCal.HasButtons = true;
            this.FRPlantCal.Location = new System.Drawing.Point(64, 29);
            this.FRPlantCal.Name = "FRPlantCal";
            this.FRPlantCal.Readonly = true;
            this.FRPlantCal.Size = new System.Drawing.Size(120, 20);
            this.FRPlantCal.TabIndex = 33;
            this.FRPlantCal.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.FRPlantCal.ValueChanged += new System.EventHandler(this.FRPlantCal_ValueChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Azure;
            this.groupBox8.Controls.Add(this.panel54);
            this.groupBox8.Controls.Add(this.panel55);
            this.groupBox8.Controls.Add(this.panel56);
            this.groupBox8.Location = new System.Drawing.Point(232, 44);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(258, 139);
            this.groupBox8.TabIndex = 6;
            this.groupBox8.TabStop = false;
            // 
            // panel54
            // 
            this.panel54.BackColor = System.Drawing.Color.SkyBlue;
            this.panel54.Controls.Add(this.FRPlantCostTb);
            this.panel54.Controls.Add(this.label58);
            this.panel54.Location = new System.Drawing.Point(140, 74);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(95, 49);
            this.panel54.TabIndex = 5;
            // 
            // FRPlantCostTb
            // 
            this.FRPlantCostTb.Location = new System.Drawing.Point(9, 22);
            this.FRPlantCostTb.Name = "FRPlantCostTb";
            this.FRPlantCostTb.Size = new System.Drawing.Size(75, 20);
            this.FRPlantCostTb.TabIndex = 1;
            this.FRPlantCostTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.SystemColors.Window;
            this.label58.Location = new System.Drawing.Point(26, 6);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(40, 13);
            this.label58.TabIndex = 0;
            this.label58.Text = "COST";
            // 
            // panel55
            // 
            this.panel55.BackColor = System.Drawing.Color.SkyBlue;
            this.panel55.Controls.Add(this.FRPlantIncomeTb);
            this.panel55.Controls.Add(this.label59);
            this.panel55.Location = new System.Drawing.Point(17, 74);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(98, 49);
            this.panel55.TabIndex = 4;
            // 
            // FRPlantIncomeTb
            // 
            this.FRPlantIncomeTb.Location = new System.Drawing.Point(6, 22);
            this.FRPlantIncomeTb.Name = "FRPlantIncomeTb";
            this.FRPlantIncomeTb.Size = new System.Drawing.Size(79, 20);
            this.FRPlantIncomeTb.TabIndex = 1;
            this.FRPlantIncomeTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.SystemColors.Window;
            this.label59.Location = new System.Drawing.Point(18, 4);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(55, 13);
            this.label59.TabIndex = 0;
            this.label59.Text = "INCOME";
            // 
            // panel56
            // 
            this.panel56.BackColor = System.Drawing.Color.SkyBlue;
            this.panel56.Controls.Add(this.FRPlantBenefitTB);
            this.panel56.Controls.Add(this.label60);
            this.panel56.Location = new System.Drawing.Point(78, 19);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(101, 49);
            this.panel56.TabIndex = 3;
            // 
            // FRPlantBenefitTB
            // 
            this.FRPlantBenefitTB.Location = new System.Drawing.Point(8, 21);
            this.FRPlantBenefitTB.Name = "FRPlantBenefitTB";
            this.FRPlantBenefitTB.Size = new System.Drawing.Size(82, 20);
            this.FRPlantBenefitTB.TabIndex = 1;
            this.FRPlantBenefitTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.SystemColors.Window;
            this.label60.Location = new System.Drawing.Point(21, 5);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(59, 13);
            this.label60.TabIndex = 0;
            this.label60.Text = "BENEFIT";
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.BackColor = System.Drawing.Color.Azure;
            this.groupBox7.Controls.Add(this.panel47);
            this.groupBox7.Controls.Add(this.panel48);
            this.groupBox7.Controls.Add(this.panel49);
            this.groupBox7.Controls.Add(this.panel50);
            this.groupBox7.Controls.Add(this.panel51);
            this.groupBox7.Controls.Add(this.panel52);
            this.groupBox7.Location = new System.Drawing.Point(4, 309);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(689, 79);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            // 
            // panel47
            // 
            this.panel47.BackColor = System.Drawing.Color.SkyBlue;
            this.panel47.Controls.Add(this.FRPlantDecPayTb);
            this.panel47.Controls.Add(this.label51);
            this.panel47.Location = new System.Drawing.Point(575, 19);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(110, 49);
            this.panel47.TabIndex = 4;
            // 
            // FRPlantDecPayTb
            // 
            this.FRPlantDecPayTb.Location = new System.Drawing.Point(22, 22);
            this.FRPlantDecPayTb.Name = "FRPlantDecPayTb";
            this.FRPlantDecPayTb.Size = new System.Drawing.Size(73, 20);
            this.FRPlantDecPayTb.TabIndex = 1;
            this.FRPlantDecPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.SystemColors.Window;
            this.label51.Location = new System.Drawing.Point(-1, 4);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(113, 13);
            this.label51.TabIndex = 0;
            this.label51.Text = "Decrease Payment";
            // 
            // panel48
            // 
            this.panel48.BackColor = System.Drawing.Color.SkyBlue;
            this.panel48.Controls.Add(this.FRPlantIncPayTb);
            this.panel48.Controls.Add(this.label52);
            this.panel48.Location = new System.Drawing.Point(453, 19);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(111, 49);
            this.panel48.TabIndex = 5;
            // 
            // FRPlantIncPayTb
            // 
            this.FRPlantIncPayTb.Location = new System.Drawing.Point(14, 22);
            this.FRPlantIncPayTb.Name = "FRPlantIncPayTb";
            this.FRPlantIncPayTb.Size = new System.Drawing.Size(75, 20);
            this.FRPlantIncPayTb.TabIndex = 1;
            this.FRPlantIncPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.SystemColors.Window;
            this.label52.Location = new System.Drawing.Point(-3, 4);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(115, 13);
            this.label52.TabIndex = 0;
            this.label52.Text = "Increment Payment";
            // 
            // panel49
            // 
            this.panel49.BackColor = System.Drawing.Color.SkyBlue;
            this.panel49.Controls.Add(this.FRPlantULPayTb);
            this.panel49.Controls.Add(this.label53);
            this.panel49.Location = new System.Drawing.Point(347, 19);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(95, 49);
            this.panel49.TabIndex = 4;
            // 
            // FRPlantULPayTb
            // 
            this.FRPlantULPayTb.Location = new System.Drawing.Point(6, 22);
            this.FRPlantULPayTb.Name = "FRPlantULPayTb";
            this.FRPlantULPayTb.Size = new System.Drawing.Size(79, 20);
            this.FRPlantULPayTb.TabIndex = 1;
            this.FRPlantULPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.SystemColors.Window;
            this.label53.Location = new System.Drawing.Point(8, 4);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(75, 13);
            this.label53.TabIndex = 0;
            this.label53.Text = "UL Payment";
            // 
            // panel50
            // 
            this.panel50.BackColor = System.Drawing.Color.SkyBlue;
            this.panel50.Controls.Add(this.FRPlantBidPayTb);
            this.panel50.Controls.Add(this.label54);
            this.panel50.Location = new System.Drawing.Point(240, 19);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(101, 49);
            this.panel50.TabIndex = 3;
            // 
            // FRPlantBidPayTb
            // 
            this.FRPlantBidPayTb.Location = new System.Drawing.Point(8, 21);
            this.FRPlantBidPayTb.Name = "FRPlantBidPayTb";
            this.FRPlantBidPayTb.Size = new System.Drawing.Size(82, 20);
            this.FRPlantBidPayTb.TabIndex = 1;
            this.FRPlantBidPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.SystemColors.Window;
            this.label54.Location = new System.Drawing.Point(3, 3);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(77, 13);
            this.label54.TabIndex = 0;
            this.label54.Text = "Bid Payment";
            // 
            // panel51
            // 
            this.panel51.BackColor = System.Drawing.Color.SkyBlue;
            this.panel51.Controls.Add(this.FRPlantEnergyPayTb);
            this.panel51.Controls.Add(this.label55);
            this.panel51.Location = new System.Drawing.Point(126, 19);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(105, 49);
            this.panel51.TabIndex = 2;
            // 
            // FRPlantEnergyPayTb
            // 
            this.FRPlantEnergyPayTb.Location = new System.Drawing.Point(15, 22);
            this.FRPlantEnergyPayTb.Name = "FRPlantEnergyPayTb";
            this.FRPlantEnergyPayTb.Size = new System.Drawing.Size(75, 20);
            this.FRPlantEnergyPayTb.TabIndex = 1;
            this.FRPlantEnergyPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.SystemColors.Window;
            this.label55.Location = new System.Drawing.Point(3, 4);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(98, 13);
            this.label55.TabIndex = 0;
            this.label55.Text = "Energy Payment";
            // 
            // panel52
            // 
            this.panel52.BackColor = System.Drawing.Color.SkyBlue;
            this.panel52.Controls.Add(this.FRPlantCapPayTb);
            this.panel52.Controls.Add(this.label56);
            this.panel52.Location = new System.Drawing.Point(6, 19);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(111, 49);
            this.panel52.TabIndex = 1;
            // 
            // FRPlantCapPayTb
            // 
            this.FRPlantCapPayTb.Location = new System.Drawing.Point(18, 22);
            this.FRPlantCapPayTb.Name = "FRPlantCapPayTb";
            this.FRPlantCapPayTb.Size = new System.Drawing.Size(75, 20);
            this.FRPlantCapPayTb.TabIndex = 1;
            this.FRPlantCapPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.SystemColors.Window;
            this.label56.Location = new System.Drawing.Point(0, 4);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(108, 13);
            this.label56.TabIndex = 0;
            this.label56.Text = "Capacity Payment";
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.BackColor = System.Drawing.Color.Azure;
            this.groupBox6.Controls.Add(this.panel41);
            this.groupBox6.Controls.Add(this.panel42);
            this.groupBox6.Controls.Add(this.panel43);
            this.groupBox6.Controls.Add(this.panel44);
            this.groupBox6.Controls.Add(this.panel45);
            this.groupBox6.Controls.Add(this.panel46);
            this.groupBox6.Location = new System.Drawing.Point(4, 210);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(689, 79);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            // 
            // panel41
            // 
            this.panel41.BackColor = System.Drawing.Color.SkyBlue;
            this.panel41.Controls.Add(this.FRPlantDecPowerTb);
            this.panel41.Controls.Add(this.label45);
            this.panel41.Location = new System.Drawing.Point(578, 19);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(105, 49);
            this.panel41.TabIndex = 4;
            // 
            // FRPlantDecPowerTb
            // 
            this.FRPlantDecPowerTb.Location = new System.Drawing.Point(19, 21);
            this.FRPlantDecPowerTb.Name = "FRPlantDecPowerTb";
            this.FRPlantDecPowerTb.Size = new System.Drawing.Size(71, 20);
            this.FRPlantDecPowerTb.TabIndex = 1;
            this.FRPlantDecPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.SystemColors.Window;
            this.label45.Location = new System.Drawing.Point(0, 4);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(100, 13);
            this.label45.TabIndex = 0;
            this.label45.Text = "Decrease Power";
            // 
            // panel42
            // 
            this.panel42.BackColor = System.Drawing.Color.SkyBlue;
            this.panel42.Controls.Add(this.FRPlantIncPowerTb);
            this.panel42.Controls.Add(this.label46);
            this.panel42.Location = new System.Drawing.Point(458, 19);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(106, 49);
            this.panel42.TabIndex = 5;
            // 
            // FRPlantIncPowerTb
            // 
            this.FRPlantIncPowerTb.Location = new System.Drawing.Point(13, 22);
            this.FRPlantIncPowerTb.Name = "FRPlantIncPowerTb";
            this.FRPlantIncPowerTb.Size = new System.Drawing.Size(75, 20);
            this.FRPlantIncPowerTb.TabIndex = 1;
            this.FRPlantIncPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.SystemColors.Window;
            this.label46.Location = new System.Drawing.Point(3, 4);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(102, 13);
            this.label46.TabIndex = 0;
            this.label46.Text = "Increment Power";
            // 
            // panel43
            // 
            this.panel43.BackColor = System.Drawing.Color.SkyBlue;
            this.panel43.Controls.Add(this.FRPlantULPowerTb);
            this.panel43.Controls.Add(this.label47);
            this.panel43.Location = new System.Drawing.Point(356, 19);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(91, 49);
            this.panel43.TabIndex = 4;
            // 
            // FRPlantULPowerTb
            // 
            this.FRPlantULPowerTb.Location = new System.Drawing.Point(6, 22);
            this.FRPlantULPowerTb.Name = "FRPlantULPowerTb";
            this.FRPlantULPowerTb.Size = new System.Drawing.Size(79, 20);
            this.FRPlantULPowerTb.TabIndex = 1;
            this.FRPlantULPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.SystemColors.Window;
            this.label47.Location = new System.Drawing.Point(16, 4);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(62, 13);
            this.label47.TabIndex = 0;
            this.label47.Text = "UL Power";
            // 
            // panel44
            // 
            this.panel44.BackColor = System.Drawing.Color.SkyBlue;
            this.panel44.Controls.Add(this.FRPlantBidPowerTb);
            this.panel44.Controls.Add(this.label48);
            this.panel44.Location = new System.Drawing.Point(251, 19);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(90, 49);
            this.panel44.TabIndex = 3;
            // 
            // FRPlantBidPowerTb
            // 
            this.FRPlantBidPowerTb.Location = new System.Drawing.Point(3, 21);
            this.FRPlantBidPowerTb.Name = "FRPlantBidPowerTb";
            this.FRPlantBidPowerTb.Size = new System.Drawing.Size(82, 20);
            this.FRPlantBidPowerTb.TabIndex = 1;
            this.FRPlantBidPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.SystemColors.Window;
            this.label48.Location = new System.Drawing.Point(14, 4);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(64, 13);
            this.label48.TabIndex = 0;
            this.label48.Text = "Bid Power";
            // 
            // panel45
            // 
            this.panel45.BackColor = System.Drawing.Color.SkyBlue;
            this.panel45.Controls.Add(this.FRPlantTotalPowerTb);
            this.panel45.Controls.Add(this.label49);
            this.panel45.Location = new System.Drawing.Point(136, 19);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(95, 49);
            this.panel45.TabIndex = 2;
            // 
            // FRPlantTotalPowerTb
            // 
            this.FRPlantTotalPowerTb.Location = new System.Drawing.Point(9, 22);
            this.FRPlantTotalPowerTb.Name = "FRPlantTotalPowerTb";
            this.FRPlantTotalPowerTb.Size = new System.Drawing.Size(75, 20);
            this.FRPlantTotalPowerTb.TabIndex = 1;
            this.FRPlantTotalPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.SystemColors.Window;
            this.label49.Location = new System.Drawing.Point(6, 4);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(75, 13);
            this.label49.TabIndex = 0;
            this.label49.Text = "Total Power";
            // 
            // panel46
            // 
            this.panel46.BackColor = System.Drawing.Color.SkyBlue;
            this.panel46.Controls.Add(this.FRPlantAvaCapTb);
            this.panel46.Controls.Add(this.label50);
            this.panel46.Location = new System.Drawing.Point(6, 19);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(114, 49);
            this.panel46.TabIndex = 1;
            // 
            // FRPlantAvaCapTb
            // 
            this.FRPlantAvaCapTb.Location = new System.Drawing.Point(20, 22);
            this.FRPlantAvaCapTb.Name = "FRPlantAvaCapTb";
            this.FRPlantAvaCapTb.Size = new System.Drawing.Size(75, 20);
            this.FRPlantAvaCapTb.TabIndex = 1;
            this.FRPlantAvaCapTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.SystemColors.Window;
            this.label50.Location = new System.Drawing.Point(-1, 4);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(112, 13);
            this.label50.TabIndex = 0;
            this.label50.Text = "Available Capacity";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(19, 30);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(42, 13);
            this.label44.TabIndex = 2;
            this.label44.Text = "Date :";
            // 
            // FRHeaderGb
            // 
            this.FRHeaderGb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.FRHeaderGb.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.FRHeaderGb.BackColor = System.Drawing.Color.Azure;
            this.FRHeaderGb.Controls.Add(this.FRPlantLb);
            this.FRHeaderGb.Controls.Add(this.FRHeaderPanel);
            this.FRHeaderGb.Controls.Add(this.L17);
            this.FRHeaderGb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FRHeaderGb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FRHeaderGb.ForeColor = System.Drawing.Color.Black;
            this.FRHeaderGb.Location = new System.Drawing.Point(15, 11);
            this.FRHeaderGb.Name = "FRHeaderGb";
            this.FRHeaderGb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FRHeaderGb.Size = new System.Drawing.Size(720, 53);
            this.FRHeaderGb.TabIndex = 22;
            this.FRHeaderGb.TabStop = false;
            this.FRHeaderGb.Visible = false;
            // 
            // FRPlantLb
            // 
            this.FRPlantLb.AutoSize = true;
            this.FRPlantLb.Location = new System.Drawing.Point(62, 23);
            this.FRPlantLb.Name = "FRPlantLb";
            this.FRPlantLb.Size = new System.Drawing.Size(0, 13);
            this.FRPlantLb.TabIndex = 9;
            // 
            // FRHeaderPanel
            // 
            this.FRHeaderPanel.Controls.Add(this.FRTypeLb);
            this.FRHeaderPanel.Controls.Add(this.FRUnitLb);
            this.FRHeaderPanel.Controls.Add(this.FRPackLb);
            this.FRHeaderPanel.Controls.Add(this.L20);
            this.FRHeaderPanel.Controls.Add(this.L19);
            this.FRHeaderPanel.Controls.Add(this.L18);
            this.FRHeaderPanel.Location = new System.Drawing.Point(144, 12);
            this.FRHeaderPanel.Name = "FRHeaderPanel";
            this.FRHeaderPanel.Size = new System.Drawing.Size(536, 35);
            this.FRHeaderPanel.TabIndex = 8;
            this.FRHeaderPanel.Visible = false;
            // 
            // FRTypeLb
            // 
            this.FRTypeLb.AutoSize = true;
            this.FRTypeLb.Location = new System.Drawing.Point(441, 11);
            this.FRTypeLb.Name = "FRTypeLb";
            this.FRTypeLb.Size = new System.Drawing.Size(0, 13);
            this.FRTypeLb.TabIndex = 13;
            // 
            // FRUnitLb
            // 
            this.FRUnitLb.AutoSize = true;
            this.FRUnitLb.Location = new System.Drawing.Point(278, 11);
            this.FRUnitLb.Name = "FRUnitLb";
            this.FRUnitLb.Size = new System.Drawing.Size(0, 13);
            this.FRUnitLb.TabIndex = 12;
            // 
            // FRPackLb
            // 
            this.FRPackLb.AutoSize = true;
            this.FRPackLb.Location = new System.Drawing.Point(112, 11);
            this.FRPackLb.Name = "FRPackLb";
            this.FRPackLb.Size = new System.Drawing.Size(0, 13);
            this.FRPackLb.TabIndex = 11;
            // 
            // L20
            // 
            this.L20.AutoSize = true;
            this.L20.BackColor = System.Drawing.Color.Transparent;
            this.L20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L20.Location = new System.Drawing.Point(402, 11);
            this.L20.Name = "L20";
            this.L20.Size = new System.Drawing.Size(47, 13);
            this.L20.TabIndex = 10;
            this.L20.Text = "Type : ";
            // 
            // L19
            // 
            this.L19.AutoSize = true;
            this.L19.BackColor = System.Drawing.Color.Transparent;
            this.L19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L19.Location = new System.Drawing.Point(234, 11);
            this.L19.Name = "L19";
            this.L19.Size = new System.Drawing.Size(42, 13);
            this.L19.TabIndex = 9;
            this.L19.Text = "Unit : ";
            // 
            // L18
            // 
            this.L18.AutoSize = true;
            this.L18.BackColor = System.Drawing.Color.Transparent;
            this.L18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L18.Location = new System.Drawing.Point(45, 11);
            this.L18.Name = "L18";
            this.L18.Size = new System.Drawing.Size(69, 13);
            this.L18.TabIndex = 8;
            this.L18.Text = "Package : ";
            // 
            // L17
            // 
            this.L17.AutoSize = true;
            this.L17.BackColor = System.Drawing.Color.Transparent;
            this.L17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L17.Location = new System.Drawing.Point(10, 23);
            this.L17.Name = "L17";
            this.L17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.L17.Size = new System.Drawing.Size(48, 13);
            this.L17.TabIndex = 7;
            this.L17.Text = "Plant : ";
            this.L17.Visible = false;
            // 
            // BidData
            // 
            this.BidData.BackgroundImage = global::PowerPlantProject.Properties.Resources._10;
            this.BidData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BidData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BidData.Controls.Add(this.BDMainPanel);
            this.BidData.Controls.Add(this.BDHeaderGb);
            this.BidData.Location = new System.Drawing.Point(4, 29);
            this.BidData.Name = "BidData";
            this.BidData.Size = new System.Drawing.Size(757, 625);
            this.BidData.TabIndex = 2;
            this.BidData.Text = "Bid Data";
            this.BidData.UseVisualStyleBackColor = true;
            // 
            // BDMainPanel
            // 
            this.BDMainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.BDMainPanel.Controls.Add(this.BDPlotBtn);
            this.BDMainPanel.Controls.Add(this.BDCur2);
            this.BDMainPanel.Controls.Add(this.BDCur1);
            this.BDMainPanel.Location = new System.Drawing.Point(1, 66);
            this.BDMainPanel.Name = "BDMainPanel";
            this.BDMainPanel.Size = new System.Drawing.Size(743, 554);
            this.BDMainPanel.TabIndex = 22;
            // 
            // BDPlotBtn
            // 
            this.BDPlotBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BDPlotBtn.BackColor = System.Drawing.Color.SkyBlue;
            this.BDPlotBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BDPlotBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BDPlotBtn.Location = new System.Drawing.Point(638, 500);
            this.BDPlotBtn.Name = "BDPlotBtn";
            this.BDPlotBtn.Size = new System.Drawing.Size(75, 23);
            this.BDPlotBtn.TabIndex = 36;
            this.BDPlotBtn.Text = "Plot";
            this.BDPlotBtn.UseVisualStyleBackColor = false;
            this.BDPlotBtn.Visible = false;
            // 
            // BDCur2
            // 
            this.BDCur2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.BDCur2.BackColor = System.Drawing.Color.Azure;
            this.BDCur2.Controls.Add(this.BDCal);
            this.BDCur2.Controls.Add(this.BDCurGrid);
            this.BDCur2.Controls.Add(this.label43);
            this.BDCur2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BDCur2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.BDCur2.Location = new System.Drawing.Point(9, 144);
            this.BDCur2.Name = "BDCur2";
            this.BDCur2.Size = new System.Drawing.Size(724, 331);
            this.BDCur2.TabIndex = 35;
            this.BDCur2.TabStop = false;
            this.BDCur2.Text = "CURRENT STATE";
            this.BDCur2.Visible = false;
            // 
            // BDCal
            // 
            this.BDCal.HasButtons = true;
            this.BDCal.Location = new System.Drawing.Point(67, 23);
            this.BDCal.Name = "BDCal";
            this.BDCal.Readonly = true;
            this.BDCal.Size = new System.Drawing.Size(120, 20);
            this.BDCal.TabIndex = 32;
            this.BDCal.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.BDCal.ValueChanged += new System.EventHandler(this.BDCal_ValueChanged);
            // 
            // BDCurGrid
            // 
            this.BDCurGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.BDCurGrid.AutoGenerateColumns = false;
            this.BDCurGrid.BackgroundColor = System.Drawing.Color.White;
            this.BDCurGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BDCurGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.BDCurGrid.ColumnHeadersHeight = 22;
            this.BDCurGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40,
            this.dataGridViewTextBoxColumn41,
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewTextBoxColumn45,
            this.dataGridViewTextBoxColumn46,
            this.Column30,
            this.power6DataGridViewTextBoxColumn,
            this.price6DataGridViewTextBoxColumn,
            this.power7DataGridViewTextBoxColumn,
            this.price7DataGridViewTextBoxColumn,
            this.power8DataGridViewTextBoxColumn,
            this.price8DataGridViewTextBoxColumn,
            this.power9DataGridViewTextBoxColumn,
            this.price9DataGridViewTextBoxColumn,
            this.power10DataGridViewTextBoxColumn,
            this.price10DataGridViewTextBoxColumn});
            this.BDCurGrid.DataSource = this.detailFRM002BindingSource;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.BDCurGrid.DefaultCellStyle = dataGridViewCellStyle27;
            this.BDCurGrid.EnableHeadersVisualStyles = false;
            this.BDCurGrid.Location = new System.Drawing.Point(19, 58);
            this.BDCurGrid.Name = "BDCurGrid";
            this.BDCurGrid.ReadOnly = true;
            this.BDCurGrid.RowHeadersVisible = false;
            this.BDCurGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.BDCurGrid.Size = new System.Drawing.Size(685, 234);
            this.BDCurGrid.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.DataPropertyName = "Hour";
            this.dataGridViewTextBoxColumn37.HeaderText = "Hour";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.ReadOnly = true;
            this.dataGridViewTextBoxColumn37.Width = 62;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.DataPropertyName = "Power1";
            this.dataGridViewTextBoxColumn38.HeaderText = "Power1";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            this.dataGridViewTextBoxColumn38.Width = 62;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.DataPropertyName = "Price1";
            this.dataGridViewTextBoxColumn39.HeaderText = "Price1";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.ReadOnly = true;
            this.dataGridViewTextBoxColumn39.Width = 62;
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.DataPropertyName = "Power2";
            this.dataGridViewTextBoxColumn40.HeaderText = "Power2";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.ReadOnly = true;
            this.dataGridViewTextBoxColumn40.Width = 62;
            // 
            // dataGridViewTextBoxColumn41
            // 
            this.dataGridViewTextBoxColumn41.DataPropertyName = "Price2";
            this.dataGridViewTextBoxColumn41.HeaderText = "Price2";
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.ReadOnly = true;
            this.dataGridViewTextBoxColumn41.Width = 62;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.DataPropertyName = "Power3";
            this.dataGridViewTextBoxColumn42.HeaderText = "Power3";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.ReadOnly = true;
            this.dataGridViewTextBoxColumn42.Width = 62;
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.DataPropertyName = "Price3";
            this.dataGridViewTextBoxColumn43.HeaderText = "Price3";
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.ReadOnly = true;
            this.dataGridViewTextBoxColumn43.Width = 62;
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.DataPropertyName = "Power4";
            this.dataGridViewTextBoxColumn44.HeaderText = "Power4";
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.ReadOnly = true;
            this.dataGridViewTextBoxColumn44.Width = 62;
            // 
            // dataGridViewTextBoxColumn45
            // 
            this.dataGridViewTextBoxColumn45.DataPropertyName = "Price4";
            this.dataGridViewTextBoxColumn45.HeaderText = "Price4";
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            this.dataGridViewTextBoxColumn45.ReadOnly = true;
            this.dataGridViewTextBoxColumn45.Width = 62;
            // 
            // dataGridViewTextBoxColumn46
            // 
            this.dataGridViewTextBoxColumn46.DataPropertyName = "Power5";
            this.dataGridViewTextBoxColumn46.HeaderText = "Power5";
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.ReadOnly = true;
            this.dataGridViewTextBoxColumn46.Width = 62;
            // 
            // Column30
            // 
            this.Column30.DataPropertyName = "Price5";
            this.Column30.HeaderText = "Price5";
            this.Column30.Name = "Column30";
            this.Column30.ReadOnly = true;
            this.Column30.Width = 62;
            // 
            // power6DataGridViewTextBoxColumn
            // 
            this.power6DataGridViewTextBoxColumn.DataPropertyName = "Power6";
            this.power6DataGridViewTextBoxColumn.HeaderText = "Power6";
            this.power6DataGridViewTextBoxColumn.Name = "power6DataGridViewTextBoxColumn";
            this.power6DataGridViewTextBoxColumn.ReadOnly = true;
            this.power6DataGridViewTextBoxColumn.Width = 62;
            // 
            // price6DataGridViewTextBoxColumn
            // 
            this.price6DataGridViewTextBoxColumn.DataPropertyName = "Price6";
            this.price6DataGridViewTextBoxColumn.HeaderText = "Price6";
            this.price6DataGridViewTextBoxColumn.Name = "price6DataGridViewTextBoxColumn";
            this.price6DataGridViewTextBoxColumn.ReadOnly = true;
            this.price6DataGridViewTextBoxColumn.Width = 62;
            // 
            // power7DataGridViewTextBoxColumn
            // 
            this.power7DataGridViewTextBoxColumn.DataPropertyName = "Power7";
            this.power7DataGridViewTextBoxColumn.HeaderText = "Power7";
            this.power7DataGridViewTextBoxColumn.Name = "power7DataGridViewTextBoxColumn";
            this.power7DataGridViewTextBoxColumn.ReadOnly = true;
            this.power7DataGridViewTextBoxColumn.Width = 62;
            // 
            // price7DataGridViewTextBoxColumn
            // 
            this.price7DataGridViewTextBoxColumn.DataPropertyName = "Price7";
            this.price7DataGridViewTextBoxColumn.HeaderText = "Price7";
            this.price7DataGridViewTextBoxColumn.Name = "price7DataGridViewTextBoxColumn";
            this.price7DataGridViewTextBoxColumn.ReadOnly = true;
            this.price7DataGridViewTextBoxColumn.Width = 62;
            // 
            // power8DataGridViewTextBoxColumn
            // 
            this.power8DataGridViewTextBoxColumn.DataPropertyName = "Power8";
            this.power8DataGridViewTextBoxColumn.HeaderText = "Power8";
            this.power8DataGridViewTextBoxColumn.Name = "power8DataGridViewTextBoxColumn";
            this.power8DataGridViewTextBoxColumn.ReadOnly = true;
            this.power8DataGridViewTextBoxColumn.Width = 62;
            // 
            // price8DataGridViewTextBoxColumn
            // 
            this.price8DataGridViewTextBoxColumn.DataPropertyName = "Price8";
            this.price8DataGridViewTextBoxColumn.HeaderText = "Price8";
            this.price8DataGridViewTextBoxColumn.Name = "price8DataGridViewTextBoxColumn";
            this.price8DataGridViewTextBoxColumn.ReadOnly = true;
            this.price8DataGridViewTextBoxColumn.Width = 62;
            // 
            // power9DataGridViewTextBoxColumn
            // 
            this.power9DataGridViewTextBoxColumn.DataPropertyName = "Power9";
            this.power9DataGridViewTextBoxColumn.HeaderText = "Power9";
            this.power9DataGridViewTextBoxColumn.Name = "power9DataGridViewTextBoxColumn";
            this.power9DataGridViewTextBoxColumn.ReadOnly = true;
            this.power9DataGridViewTextBoxColumn.Width = 62;
            // 
            // price9DataGridViewTextBoxColumn
            // 
            this.price9DataGridViewTextBoxColumn.DataPropertyName = "Price9";
            this.price9DataGridViewTextBoxColumn.HeaderText = "Price9";
            this.price9DataGridViewTextBoxColumn.Name = "price9DataGridViewTextBoxColumn";
            this.price9DataGridViewTextBoxColumn.ReadOnly = true;
            this.price9DataGridViewTextBoxColumn.Width = 62;
            // 
            // power10DataGridViewTextBoxColumn
            // 
            this.power10DataGridViewTextBoxColumn.DataPropertyName = "Power10";
            this.power10DataGridViewTextBoxColumn.HeaderText = "Power10";
            this.power10DataGridViewTextBoxColumn.Name = "power10DataGridViewTextBoxColumn";
            this.power10DataGridViewTextBoxColumn.ReadOnly = true;
            this.power10DataGridViewTextBoxColumn.Width = 62;
            // 
            // price10DataGridViewTextBoxColumn
            // 
            this.price10DataGridViewTextBoxColumn.DataPropertyName = "Price10";
            this.price10DataGridViewTextBoxColumn.HeaderText = "Price10";
            this.price10DataGridViewTextBoxColumn.Name = "price10DataGridViewTextBoxColumn";
            this.price10DataGridViewTextBoxColumn.ReadOnly = true;
            this.price10DataGridViewTextBoxColumn.Width = 62;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(23, 25);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(42, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "Date :";
            // 
            // BDCur1
            // 
            this.BDCur1.BackColor = System.Drawing.Color.Azure;
            this.BDCur1.Controls.Add(this.panel38);
            this.BDCur1.Controls.Add(this.panel39);
            this.BDCur1.Controls.Add(this.panel40);
            this.BDCur1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BDCur1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.BDCur1.Location = new System.Drawing.Point(194, 31);
            this.BDCur1.Name = "BDCur1";
            this.BDCur1.Size = new System.Drawing.Size(335, 86);
            this.BDCur1.TabIndex = 34;
            this.BDCur1.TabStop = false;
            this.BDCur1.Text = "CURRENT STATE";
            this.BDCur1.Visible = false;
            // 
            // panel38
            // 
            this.panel38.BackColor = System.Drawing.Color.SkyBlue;
            this.panel38.Controls.Add(this.BDMaxBidTb);
            this.panel38.Controls.Add(this.label40);
            this.panel38.Location = new System.Drawing.Point(232, 19);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(95, 49);
            this.panel38.TabIndex = 3;
            // 
            // BDMaxBidTb
            // 
            this.BDMaxBidTb.BackColor = System.Drawing.Color.White;
            this.BDMaxBidTb.Location = new System.Drawing.Point(9, 22);
            this.BDMaxBidTb.Name = "BDMaxBidTb";
            this.BDMaxBidTb.ReadOnly = true;
            this.BDMaxBidTb.Size = new System.Drawing.Size(75, 20);
            this.BDMaxBidTb.TabIndex = 1;
            this.BDMaxBidTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.ForeColor = System.Drawing.SystemColors.Window;
            this.label40.Location = new System.Drawing.Point(21, 4);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(58, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "MAX BID";
            // 
            // panel39
            // 
            this.panel39.BackColor = System.Drawing.Color.SkyBlue;
            this.panel39.Controls.Add(this.BDPowerTb);
            this.panel39.Controls.Add(this.label41);
            this.panel39.Location = new System.Drawing.Point(118, 19);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(95, 49);
            this.panel39.TabIndex = 2;
            // 
            // BDPowerTb
            // 
            this.BDPowerTb.BackColor = System.Drawing.Color.White;
            this.BDPowerTb.Location = new System.Drawing.Point(9, 22);
            this.BDPowerTb.Name = "BDPowerTb";
            this.BDPowerTb.ReadOnly = true;
            this.BDPowerTb.Size = new System.Drawing.Size(75, 20);
            this.BDPowerTb.TabIndex = 1;
            this.BDPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.ForeColor = System.Drawing.SystemColors.Window;
            this.label41.Location = new System.Drawing.Point(21, 4);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(53, 13);
            this.label41.TabIndex = 0;
            this.label41.Text = "POWER";
            // 
            // panel40
            // 
            this.panel40.BackColor = System.Drawing.Color.SkyBlue;
            this.panel40.Controls.Add(this.BDStateTb);
            this.panel40.Controls.Add(this.label42);
            this.panel40.Location = new System.Drawing.Point(6, 19);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(95, 49);
            this.panel40.TabIndex = 1;
            // 
            // BDStateTb
            // 
            this.BDStateTb.BackColor = System.Drawing.Color.White;
            this.BDStateTb.Location = new System.Drawing.Point(9, 22);
            this.BDStateTb.Name = "BDStateTb";
            this.BDStateTb.ReadOnly = true;
            this.BDStateTb.Size = new System.Drawing.Size(75, 20);
            this.BDStateTb.TabIndex = 1;
            this.BDStateTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.ForeColor = System.Drawing.SystemColors.Window;
            this.label42.Location = new System.Drawing.Point(21, 4);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(47, 13);
            this.label42.TabIndex = 0;
            this.label42.Text = "STATE";
            // 
            // BDHeaderGb
            // 
            this.BDHeaderGb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.BDHeaderGb.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BDHeaderGb.BackColor = System.Drawing.Color.Azure;
            this.BDHeaderGb.Controls.Add(this.BDPlantLb);
            this.BDHeaderGb.Controls.Add(this.BDHeaderPanel);
            this.BDHeaderGb.Controls.Add(this.L13);
            this.BDHeaderGb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BDHeaderGb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.BDHeaderGb.ForeColor = System.Drawing.Color.Black;
            this.BDHeaderGb.Location = new System.Drawing.Point(15, 11);
            this.BDHeaderGb.Name = "BDHeaderGb";
            this.BDHeaderGb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.BDHeaderGb.Size = new System.Drawing.Size(720, 53);
            this.BDHeaderGb.TabIndex = 21;
            this.BDHeaderGb.TabStop = false;
            this.BDHeaderGb.Visible = false;
            // 
            // BDPlantLb
            // 
            this.BDPlantLb.AutoSize = true;
            this.BDPlantLb.Location = new System.Drawing.Point(62, 23);
            this.BDPlantLb.Name = "BDPlantLb";
            this.BDPlantLb.Size = new System.Drawing.Size(0, 13);
            this.BDPlantLb.TabIndex = 9;
            // 
            // BDHeaderPanel
            // 
            this.BDHeaderPanel.Controls.Add(this.BDTypeLb);
            this.BDHeaderPanel.Controls.Add(this.BDUnitLb);
            this.BDHeaderPanel.Controls.Add(this.BDPackLb);
            this.BDHeaderPanel.Controls.Add(this.L16);
            this.BDHeaderPanel.Controls.Add(this.L15);
            this.BDHeaderPanel.Controls.Add(this.L14);
            this.BDHeaderPanel.Location = new System.Drawing.Point(144, 12);
            this.BDHeaderPanel.Name = "BDHeaderPanel";
            this.BDHeaderPanel.Size = new System.Drawing.Size(537, 35);
            this.BDHeaderPanel.TabIndex = 8;
            this.BDHeaderPanel.Visible = false;
            // 
            // BDTypeLb
            // 
            this.BDTypeLb.AutoSize = true;
            this.BDTypeLb.Location = new System.Drawing.Point(445, 11);
            this.BDTypeLb.Name = "BDTypeLb";
            this.BDTypeLb.Size = new System.Drawing.Size(0, 13);
            this.BDTypeLb.TabIndex = 13;
            // 
            // BDUnitLb
            // 
            this.BDUnitLb.AutoSize = true;
            this.BDUnitLb.Location = new System.Drawing.Point(266, 11);
            this.BDUnitLb.Name = "BDUnitLb";
            this.BDUnitLb.Size = new System.Drawing.Size(0, 13);
            this.BDUnitLb.TabIndex = 12;
            // 
            // BDPackLb
            // 
            this.BDPackLb.AutoSize = true;
            this.BDPackLb.Location = new System.Drawing.Point(110, 11);
            this.BDPackLb.Name = "BDPackLb";
            this.BDPackLb.Size = new System.Drawing.Size(0, 13);
            this.BDPackLb.TabIndex = 11;
            // 
            // L16
            // 
            this.L16.AutoSize = true;
            this.L16.BackColor = System.Drawing.Color.Transparent;
            this.L16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L16.Location = new System.Drawing.Point(402, 11);
            this.L16.Name = "L16";
            this.L16.Size = new System.Drawing.Size(47, 13);
            this.L16.TabIndex = 10;
            this.L16.Text = "Type : ";
            // 
            // L15
            // 
            this.L15.AutoSize = true;
            this.L15.BackColor = System.Drawing.Color.Transparent;
            this.L15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L15.Location = new System.Drawing.Point(234, 11);
            this.L15.Name = "L15";
            this.L15.Size = new System.Drawing.Size(42, 13);
            this.L15.TabIndex = 9;
            this.L15.Text = "Unit : ";
            // 
            // L14
            // 
            this.L14.AutoSize = true;
            this.L14.BackColor = System.Drawing.Color.Transparent;
            this.L14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L14.Location = new System.Drawing.Point(45, 11);
            this.L14.Name = "L14";
            this.L14.Size = new System.Drawing.Size(69, 13);
            this.L14.TabIndex = 8;
            this.L14.Text = "Package : ";
            // 
            // L13
            // 
            this.L13.AutoSize = true;
            this.L13.BackColor = System.Drawing.Color.Transparent;
            this.L13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L13.Location = new System.Drawing.Point(10, 23);
            this.L13.Name = "L13";
            this.L13.Size = new System.Drawing.Size(48, 13);
            this.L13.TabIndex = 7;
            this.L13.Text = "Plant : ";
            this.L13.Visible = false;
            // 
            // MarketResults
            // 
            this.MarketResults.BackgroundImage = global::PowerPlantProject.Properties.Resources._10;
            this.MarketResults.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MarketResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.MarketResults.Controls.Add(this.MRMainPanel);
            this.MarketResults.Controls.Add(this.MRHeaderGB);
            this.MarketResults.Location = new System.Drawing.Point(4, 29);
            this.MarketResults.Name = "MarketResults";
            this.MarketResults.Padding = new System.Windows.Forms.Padding(3);
            this.MarketResults.Size = new System.Drawing.Size(757, 625);
            this.MarketResults.TabIndex = 1;
            this.MarketResults.Text = "Market Results";
            this.MarketResults.UseVisualStyleBackColor = true;
            // 
            // MRMainPanel
            // 
            this.MRMainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.MRMainPanel.Controls.Add(this.MRPlotBtn);
            this.MRMainPanel.Controls.Add(this.MRPlantCurGb);
            this.MRMainPanel.Controls.Add(this.MRGB);
            this.MRMainPanel.Controls.Add(this.MRUnitCurGb);
            this.MRMainPanel.Location = new System.Drawing.Point(6, 65);
            this.MRMainPanel.Name = "MRMainPanel";
            this.MRMainPanel.Size = new System.Drawing.Size(738, 550);
            this.MRMainPanel.TabIndex = 21;
            // 
            // MRPlotBtn
            // 
            this.MRPlotBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.MRPlotBtn.BackColor = System.Drawing.Color.SkyBlue;
            this.MRPlotBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.MRPlotBtn.Location = new System.Drawing.Point(636, 519);
            this.MRPlotBtn.Name = "MRPlotBtn";
            this.MRPlotBtn.Size = new System.Drawing.Size(75, 23);
            this.MRPlotBtn.TabIndex = 30;
            this.MRPlotBtn.Text = "Plot";
            this.MRPlotBtn.UseVisualStyleBackColor = false;
            this.MRPlotBtn.Visible = false;
            // 
            // MRPlantCurGb
            // 
            this.MRPlantCurGb.BackColor = System.Drawing.Color.Azure;
            this.MRPlantCurGb.Controls.Add(this.panel34);
            this.MRPlantCurGb.Controls.Add(this.panel35);
            this.MRPlantCurGb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MRPlantCurGb.ForeColor = System.Drawing.SystemColors.WindowText;
            this.MRPlantCurGb.Location = new System.Drawing.Point(252, 8);
            this.MRPlantCurGb.Name = "MRPlantCurGb";
            this.MRPlantCurGb.Size = new System.Drawing.Size(219, 86);
            this.MRPlantCurGb.TabIndex = 27;
            this.MRPlantCurGb.TabStop = false;
            this.MRPlantCurGb.Text = "CURRENT STATE";
            this.MRPlantCurGb.Visible = false;
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.Color.SkyBlue;
            this.panel34.Controls.Add(this.MRPlantPowerTb);
            this.panel34.Controls.Add(this.MRLabel2);
            this.panel34.Location = new System.Drawing.Point(118, 19);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(95, 49);
            this.panel34.TabIndex = 2;
            // 
            // MRPlantPowerTb
            // 
            this.MRPlantPowerTb.BackColor = System.Drawing.Color.White;
            this.MRPlantPowerTb.Location = new System.Drawing.Point(9, 22);
            this.MRPlantPowerTb.Name = "MRPlantPowerTb";
            this.MRPlantPowerTb.ReadOnly = true;
            this.MRPlantPowerTb.Size = new System.Drawing.Size(75, 20);
            this.MRPlantPowerTb.TabIndex = 1;
            this.MRPlantPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MRLabel2
            // 
            this.MRLabel2.AutoSize = true;
            this.MRLabel2.ForeColor = System.Drawing.SystemColors.Window;
            this.MRLabel2.Location = new System.Drawing.Point(12, 4);
            this.MRLabel2.Name = "MRLabel2";
            this.MRLabel2.Size = new System.Drawing.Size(53, 13);
            this.MRLabel2.TabIndex = 0;
            this.MRLabel2.Text = "POWER";
            // 
            // panel35
            // 
            this.panel35.BackColor = System.Drawing.Color.SkyBlue;
            this.panel35.Controls.Add(this.MRPlantOnUnitTb);
            this.panel35.Controls.Add(this.MRLabel1);
            this.panel35.Location = new System.Drawing.Point(6, 19);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(95, 49);
            this.panel35.TabIndex = 1;
            // 
            // MRPlantOnUnitTb
            // 
            this.MRPlantOnUnitTb.BackColor = System.Drawing.Color.White;
            this.MRPlantOnUnitTb.Location = new System.Drawing.Point(9, 22);
            this.MRPlantOnUnitTb.Name = "MRPlantOnUnitTb";
            this.MRPlantOnUnitTb.ReadOnly = true;
            this.MRPlantOnUnitTb.Size = new System.Drawing.Size(75, 20);
            this.MRPlantOnUnitTb.TabIndex = 1;
            this.MRPlantOnUnitTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MRLabel1
            // 
            this.MRLabel1.AutoSize = true;
            this.MRLabel1.ForeColor = System.Drawing.SystemColors.Window;
            this.MRLabel1.Location = new System.Drawing.Point(14, 4);
            this.MRLabel1.Name = "MRLabel1";
            this.MRLabel1.Size = new System.Drawing.Size(67, 13);
            this.MRLabel1.TabIndex = 0;
            this.MRLabel1.Text = "ON UNITS";
            // 
            // MRGB
            // 
            this.MRGB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.MRGB.BackColor = System.Drawing.Color.Azure;
            this.MRGB.Controls.Add(this.MRCal);
            this.MRGB.Controls.Add(this.MRCurGrid2);
            this.MRGB.Controls.Add(this.MRCurGrid1);
            this.MRGB.Controls.Add(this.label39);
            this.MRGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MRGB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.MRGB.Location = new System.Drawing.Point(4, 151);
            this.MRGB.Name = "MRGB";
            this.MRGB.Size = new System.Drawing.Size(724, 353);
            this.MRGB.TabIndex = 29;
            this.MRGB.TabStop = false;
            this.MRGB.Text = "CURRENT STATE";
            this.MRGB.Visible = false;
            // 
            // MRCal
            // 
            this.MRCal.HasButtons = true;
            this.MRCal.Location = new System.Drawing.Point(68, 21);
            this.MRCal.Name = "MRCal";
            this.MRCal.Readonly = true;
            this.MRCal.Size = new System.Drawing.Size(120, 20);
            this.MRCal.TabIndex = 33;
            this.MRCal.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.MRCal.ValueChanged += new System.EventHandler(this.MRCal_ValueChanged);
            // 
            // MRCurGrid2
            // 
            this.MRCurGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.MRCurGrid2.AutoGenerateColumns = false;
            this.MRCurGrid2.BackgroundColor = System.Drawing.Color.White;
            this.MRCurGrid2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MRCurGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle28;
            this.MRCurGrid2.ColumnHeadersHeight = 22;
            this.MRCurGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13});
            this.MRCurGrid2.DataSource = this.powerPalntDBDataSet4BindingSource;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.MRCurGrid2.DefaultCellStyle = dataGridViewCellStyle29;
            this.MRCurGrid2.EnableHeadersVisualStyles = false;
            this.MRCurGrid2.Location = new System.Drawing.Point(16, 205);
            this.MRCurGrid2.Name = "MRCurGrid2";
            this.MRCurGrid2.ReadOnly = true;
            this.MRCurGrid2.RowHeadersVisible = false;
            this.MRCurGrid2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MRCurGrid2.Size = new System.Drawing.Size(692, 117);
            this.MRCurGrid2.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ITEM";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 53;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "13";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 53;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "14";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 53;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "15";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 53;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "16";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 53;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "17";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 53;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "18";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 53;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "19";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 53;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "20";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 53;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "21";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 53;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "22";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 53;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "23";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 53;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "24";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 53;
            // 
            // MRCurGrid1
            // 
            this.MRCurGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.MRCurGrid1.AutoGenerateColumns = false;
            this.MRCurGrid1.BackgroundColor = System.Drawing.Color.White;
            this.MRCurGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MRCurGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.MRCurGrid1.ColumnHeadersHeight = 22;
            this.MRCurGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ITEM,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12});
            this.MRCurGrid1.DataSource = this.powerPalntDBDataSet4BindingSource;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.MRCurGrid1.DefaultCellStyle = dataGridViewCellStyle31;
            this.MRCurGrid1.EnableHeadersVisualStyles = false;
            this.MRCurGrid1.Location = new System.Drawing.Point(16, 58);
            this.MRCurGrid1.Name = "MRCurGrid1";
            this.MRCurGrid1.ReadOnly = true;
            this.MRCurGrid1.RowHeadersVisible = false;
            this.MRCurGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MRCurGrid1.Size = new System.Drawing.Size(692, 117);
            this.MRCurGrid1.TabIndex = 2;
            // 
            // ITEM
            // 
            this.ITEM.HeaderText = "ITEM";
            this.ITEM.Name = "ITEM";
            this.ITEM.ReadOnly = true;
            this.ITEM.Width = 53;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 53;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 53;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "3";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 53;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "4";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 53;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "5";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 53;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "6";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 53;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "7";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 53;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "8";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 53;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "9";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 53;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "10";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 53;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "11";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 53;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "12";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 53;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(23, 25);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(42, 13);
            this.label39.TabIndex = 0;
            this.label39.Text = "Date :";
            // 
            // MRUnitCurGb
            // 
            this.MRUnitCurGb.BackColor = System.Drawing.Color.Azure;
            this.MRUnitCurGb.Controls.Add(this.panel37);
            this.MRUnitCurGb.Controls.Add(this.panel36);
            this.MRUnitCurGb.Controls.Add(this.panel72);
            this.MRUnitCurGb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MRUnitCurGb.ForeColor = System.Drawing.SystemColors.WindowText;
            this.MRUnitCurGb.Location = new System.Drawing.Point(191, 31);
            this.MRUnitCurGb.Name = "MRUnitCurGb";
            this.MRUnitCurGb.Size = new System.Drawing.Size(335, 86);
            this.MRUnitCurGb.TabIndex = 28;
            this.MRUnitCurGb.TabStop = false;
            this.MRUnitCurGb.Text = "CURRENT STATE";
            this.MRUnitCurGb.Visible = false;
            // 
            // panel37
            // 
            this.panel37.BackColor = System.Drawing.Color.SkyBlue;
            this.panel37.Controls.Add(this.MRUnitMaxBidTb);
            this.panel37.Controls.Add(this.label36);
            this.panel37.Location = new System.Drawing.Point(232, 19);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(95, 49);
            this.panel37.TabIndex = 3;
            // 
            // MRUnitMaxBidTb
            // 
            this.MRUnitMaxBidTb.BackColor = System.Drawing.Color.White;
            this.MRUnitMaxBidTb.Location = new System.Drawing.Point(9, 22);
            this.MRUnitMaxBidTb.Name = "MRUnitMaxBidTb";
            this.MRUnitMaxBidTb.ReadOnly = true;
            this.MRUnitMaxBidTb.Size = new System.Drawing.Size(75, 20);
            this.MRUnitMaxBidTb.TabIndex = 1;
            this.MRUnitMaxBidTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.ForeColor = System.Drawing.SystemColors.Window;
            this.label36.Location = new System.Drawing.Point(21, 4);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(58, 13);
            this.label36.TabIndex = 0;
            this.label36.Text = "MAX BID";
            // 
            // panel36
            // 
            this.panel36.BackColor = System.Drawing.Color.SkyBlue;
            this.panel36.Controls.Add(this.MRUnitPowerTb);
            this.panel36.Controls.Add(this.label24);
            this.panel36.Location = new System.Drawing.Point(118, 19);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(95, 49);
            this.panel36.TabIndex = 2;
            // 
            // MRUnitPowerTb
            // 
            this.MRUnitPowerTb.BackColor = System.Drawing.Color.White;
            this.MRUnitPowerTb.Location = new System.Drawing.Point(9, 22);
            this.MRUnitPowerTb.Name = "MRUnitPowerTb";
            this.MRUnitPowerTb.ReadOnly = true;
            this.MRUnitPowerTb.Size = new System.Drawing.Size(75, 20);
            this.MRUnitPowerTb.TabIndex = 1;
            this.MRUnitPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.SystemColors.Window;
            this.label24.Location = new System.Drawing.Point(21, 4);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "POWER";
            // 
            // panel72
            // 
            this.panel72.BackColor = System.Drawing.Color.SkyBlue;
            this.panel72.Controls.Add(this.MRUnitStateTb);
            this.panel72.Controls.Add(this.label25);
            this.panel72.Location = new System.Drawing.Point(6, 19);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(95, 49);
            this.panel72.TabIndex = 1;
            // 
            // MRUnitStateTb
            // 
            this.MRUnitStateTb.BackColor = System.Drawing.Color.White;
            this.MRUnitStateTb.Location = new System.Drawing.Point(9, 22);
            this.MRUnitStateTb.Name = "MRUnitStateTb";
            this.MRUnitStateTb.ReadOnly = true;
            this.MRUnitStateTb.Size = new System.Drawing.Size(75, 20);
            this.MRUnitStateTb.TabIndex = 1;
            this.MRUnitStateTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.SystemColors.Window;
            this.label25.Location = new System.Drawing.Point(21, 4);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(47, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "STATE";
            // 
            // MRHeaderGB
            // 
            this.MRHeaderGB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.MRHeaderGB.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.MRHeaderGB.BackColor = System.Drawing.Color.Azure;
            this.MRHeaderGB.Controls.Add(this.MRPlantLb);
            this.MRHeaderGB.Controls.Add(this.MRHeaderPanel);
            this.MRHeaderGB.Controls.Add(this.L9);
            this.MRHeaderGB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MRHeaderGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MRHeaderGB.ForeColor = System.Drawing.Color.Black;
            this.MRHeaderGB.Location = new System.Drawing.Point(15, 11);
            this.MRHeaderGB.Name = "MRHeaderGB";
            this.MRHeaderGB.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.MRHeaderGB.Size = new System.Drawing.Size(720, 53);
            this.MRHeaderGB.TabIndex = 20;
            this.MRHeaderGB.TabStop = false;
            this.MRHeaderGB.Visible = false;
            // 
            // MRPlantLb
            // 
            this.MRPlantLb.AutoSize = true;
            this.MRPlantLb.Location = new System.Drawing.Point(62, 23);
            this.MRPlantLb.Name = "MRPlantLb";
            this.MRPlantLb.Size = new System.Drawing.Size(0, 13);
            this.MRPlantLb.TabIndex = 9;
            // 
            // MRHeaderPanel
            // 
            this.MRHeaderPanel.Controls.Add(this.MRTypeLb);
            this.MRHeaderPanel.Controls.Add(this.MRUnitLb);
            this.MRHeaderPanel.Controls.Add(this.MRPackLb);
            this.MRHeaderPanel.Controls.Add(this.L12);
            this.MRHeaderPanel.Controls.Add(this.L11);
            this.MRHeaderPanel.Controls.Add(this.L10);
            this.MRHeaderPanel.Location = new System.Drawing.Point(144, 12);
            this.MRHeaderPanel.Name = "MRHeaderPanel";
            this.MRHeaderPanel.Size = new System.Drawing.Size(537, 35);
            this.MRHeaderPanel.TabIndex = 8;
            this.MRHeaderPanel.Visible = false;
            // 
            // MRTypeLb
            // 
            this.MRTypeLb.AutoSize = true;
            this.MRTypeLb.Location = new System.Drawing.Point(445, 11);
            this.MRTypeLb.Name = "MRTypeLb";
            this.MRTypeLb.Size = new System.Drawing.Size(0, 13);
            this.MRTypeLb.TabIndex = 13;
            // 
            // MRUnitLb
            // 
            this.MRUnitLb.AutoSize = true;
            this.MRUnitLb.Location = new System.Drawing.Point(279, 11);
            this.MRUnitLb.Name = "MRUnitLb";
            this.MRUnitLb.Size = new System.Drawing.Size(0, 13);
            this.MRUnitLb.TabIndex = 12;
            // 
            // MRPackLb
            // 
            this.MRPackLb.AutoSize = true;
            this.MRPackLb.Location = new System.Drawing.Point(114, 11);
            this.MRPackLb.Name = "MRPackLb";
            this.MRPackLb.Size = new System.Drawing.Size(0, 13);
            this.MRPackLb.TabIndex = 11;
            // 
            // L12
            // 
            this.L12.AutoSize = true;
            this.L12.BackColor = System.Drawing.Color.Transparent;
            this.L12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L12.Location = new System.Drawing.Point(402, 11);
            this.L12.Name = "L12";
            this.L12.Size = new System.Drawing.Size(47, 13);
            this.L12.TabIndex = 10;
            this.L12.Text = "Type : ";
            // 
            // L11
            // 
            this.L11.AutoSize = true;
            this.L11.BackColor = System.Drawing.Color.Transparent;
            this.L11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L11.Location = new System.Drawing.Point(234, 11);
            this.L11.Name = "L11";
            this.L11.Size = new System.Drawing.Size(42, 13);
            this.L11.TabIndex = 9;
            this.L11.Text = "Unit : ";
            // 
            // L10
            // 
            this.L10.AutoSize = true;
            this.L10.BackColor = System.Drawing.Color.Transparent;
            this.L10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L10.Location = new System.Drawing.Point(45, 11);
            this.L10.Name = "L10";
            this.L10.Size = new System.Drawing.Size(69, 13);
            this.L10.TabIndex = 8;
            this.L10.Text = "Package : ";
            // 
            // L9
            // 
            this.L9.AutoSize = true;
            this.L9.BackColor = System.Drawing.Color.Transparent;
            this.L9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L9.Location = new System.Drawing.Point(10, 23);
            this.L9.Name = "L9";
            this.L9.Size = new System.Drawing.Size(48, 13);
            this.L9.TabIndex = 7;
            this.L9.Text = "Plant : ";
            this.L9.Visible = false;
            // 
            // OperationalData
            // 
            this.OperationalData.BackgroundImage = global::PowerPlantProject.Properties.Resources._10;
            this.OperationalData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.OperationalData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.OperationalData.Controls.Add(this.ODHeaderGB);
            this.OperationalData.Controls.Add(this.ODMainPanel);
            this.OperationalData.Location = new System.Drawing.Point(4, 29);
            this.OperationalData.Name = "OperationalData";
            this.OperationalData.Padding = new System.Windows.Forms.Padding(3);
            this.OperationalData.Size = new System.Drawing.Size(757, 625);
            this.OperationalData.TabIndex = 0;
            this.OperationalData.Text = "Operational Data";
            this.OperationalData.UseVisualStyleBackColor = true;
            // 
            // ODHeaderGB
            // 
            this.ODHeaderGB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ODHeaderGB.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ODHeaderGB.BackColor = System.Drawing.Color.Azure;
            this.ODHeaderGB.Controls.Add(this.ODPlantLb);
            this.ODHeaderGB.Controls.Add(this.ODHeaderPanel);
            this.ODHeaderGB.Controls.Add(this.L5);
            this.ODHeaderGB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ODHeaderGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.ODHeaderGB.ForeColor = System.Drawing.Color.Black;
            this.ODHeaderGB.Location = new System.Drawing.Point(15, 11);
            this.ODHeaderGB.Name = "ODHeaderGB";
            this.ODHeaderGB.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ODHeaderGB.Size = new System.Drawing.Size(720, 53);
            this.ODHeaderGB.TabIndex = 20;
            this.ODHeaderGB.TabStop = false;
            this.ODHeaderGB.Visible = false;
            // 
            // ODPlantLb
            // 
            this.ODPlantLb.AutoSize = true;
            this.ODPlantLb.Location = new System.Drawing.Point(62, 23);
            this.ODPlantLb.Name = "ODPlantLb";
            this.ODPlantLb.Size = new System.Drawing.Size(0, 13);
            this.ODPlantLb.TabIndex = 9;
            // 
            // ODHeaderPanel
            // 
            this.ODHeaderPanel.Controls.Add(this.ODTypeLb);
            this.ODHeaderPanel.Controls.Add(this.ODUnitLb);
            this.ODHeaderPanel.Controls.Add(this.ODPackLb);
            this.ODHeaderPanel.Controls.Add(this.L8);
            this.ODHeaderPanel.Controls.Add(this.L7);
            this.ODHeaderPanel.Controls.Add(this.L6);
            this.ODHeaderPanel.Location = new System.Drawing.Point(144, 12);
            this.ODHeaderPanel.Name = "ODHeaderPanel";
            this.ODHeaderPanel.Size = new System.Drawing.Size(550, 35);
            this.ODHeaderPanel.TabIndex = 8;
            this.ODHeaderPanel.Visible = false;
            // 
            // ODTypeLb
            // 
            this.ODTypeLb.AutoSize = true;
            this.ODTypeLb.Location = new System.Drawing.Point(445, 11);
            this.ODTypeLb.Name = "ODTypeLb";
            this.ODTypeLb.Size = new System.Drawing.Size(0, 13);
            this.ODTypeLb.TabIndex = 13;
            // 
            // ODUnitLb
            // 
            this.ODUnitLb.AutoSize = true;
            this.ODUnitLb.Location = new System.Drawing.Point(283, 11);
            this.ODUnitLb.Name = "ODUnitLb";
            this.ODUnitLb.Size = new System.Drawing.Size(0, 13);
            this.ODUnitLb.TabIndex = 12;
            // 
            // ODPackLb
            // 
            this.ODPackLb.AutoSize = true;
            this.ODPackLb.Location = new System.Drawing.Point(129, 11);
            this.ODPackLb.Name = "ODPackLb";
            this.ODPackLb.Size = new System.Drawing.Size(0, 13);
            this.ODPackLb.TabIndex = 11;
            // 
            // L8
            // 
            this.L8.AutoSize = true;
            this.L8.BackColor = System.Drawing.Color.Transparent;
            this.L8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L8.Location = new System.Drawing.Point(402, 11);
            this.L8.Name = "L8";
            this.L8.Size = new System.Drawing.Size(47, 13);
            this.L8.TabIndex = 10;
            this.L8.Text = "Type : ";
            // 
            // L7
            // 
            this.L7.AutoSize = true;
            this.L7.BackColor = System.Drawing.Color.Transparent;
            this.L7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L7.Location = new System.Drawing.Point(234, 11);
            this.L7.Name = "L7";
            this.L7.Size = new System.Drawing.Size(42, 13);
            this.L7.TabIndex = 9;
            this.L7.Text = "Unit : ";
            // 
            // L6
            // 
            this.L6.AutoSize = true;
            this.L6.BackColor = System.Drawing.Color.Transparent;
            this.L6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L6.Location = new System.Drawing.Point(45, 11);
            this.L6.Name = "L6";
            this.L6.Size = new System.Drawing.Size(69, 13);
            this.L6.TabIndex = 8;
            this.L6.Text = "Package : ";
            // 
            // L5
            // 
            this.L5.AutoSize = true;
            this.L5.BackColor = System.Drawing.Color.Transparent;
            this.L5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L5.Location = new System.Drawing.Point(10, 23);
            this.L5.Name = "L5";
            this.L5.Size = new System.Drawing.Size(48, 13);
            this.L5.TabIndex = 7;
            this.L5.Text = "Plant : ";
            this.L5.Visible = false;
            // 
            // ODMainPanel
            // 
            this.ODMainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ODMainPanel.Controls.Add(this.ODUnitPanel);
            this.ODMainPanel.Controls.Add(this.ODSaveBtn);
            this.ODMainPanel.Controls.Add(this.ODPlantPanel);
            this.ODMainPanel.Location = new System.Drawing.Point(-2, 66);
            this.ODMainPanel.Name = "ODMainPanel";
            this.ODMainPanel.Size = new System.Drawing.Size(746, 554);
            this.ODMainPanel.TabIndex = 21;
            // 
            // ODUnitPanel
            // 
            this.ODUnitPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ODUnitPanel.Controls.Add(this.ODUnitMainGB);
            this.ODUnitPanel.Controls.Add(this.ODUnitPowerGB);
            this.ODUnitPanel.Controls.Add(this.ODUnitServiceGB);
            this.ODUnitPanel.Controls.Add(this.ODUnitFuelGB);
            this.ODUnitPanel.Location = new System.Drawing.Point(4, 3);
            this.ODUnitPanel.Name = "ODUnitPanel";
            this.ODUnitPanel.Size = new System.Drawing.Size(738, 516);
            this.ODUnitPanel.TabIndex = 29;
            this.ODUnitPanel.Visible = false;
            // 
            // ODUnitMainGB
            // 
            this.ODUnitMainGB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ODUnitMainGB.BackColor = System.Drawing.Color.Azure;
            this.ODUnitMainGB.Controls.Add(this.odunitmd2Valid);
            this.ODUnitMainGB.Controls.Add(this.odunitmd1Valid);
            this.ODUnitMainGB.Controls.Add(this.panel32);
            this.ODUnitMainGB.Controls.Add(this.panel33);
            this.ODUnitMainGB.Controls.Add(this.ODUnitMainCheck);
            this.ODUnitMainGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ODUnitMainGB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ODUnitMainGB.Location = new System.Drawing.Point(11, 103);
            this.ODUnitMainGB.Name = "ODUnitMainGB";
            this.ODUnitMainGB.Size = new System.Drawing.Size(713, 81);
            this.ODUnitMainGB.TabIndex = 3;
            this.ODUnitMainGB.TabStop = false;
            this.ODUnitMainGB.Text = "MAINTENANCE";
            // 
            // odunitmd2Valid
            // 
            this.odunitmd2Valid.AutoSize = true;
            this.odunitmd2Valid.BackColor = System.Drawing.Color.Azure;
            this.odunitmd2Valid.ForeColor = System.Drawing.Color.Red;
            this.odunitmd2Valid.Location = new System.Drawing.Point(374, 48);
            this.odunitmd2Valid.Name = "odunitmd2Valid";
            this.odunitmd2Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.odunitmd2Valid.Size = new System.Drawing.Size(12, 13);
            this.odunitmd2Valid.TabIndex = 7;
            this.odunitmd2Valid.Text = "*";
            this.odunitmd2Valid.Visible = false;
            // 
            // odunitmd1Valid
            // 
            this.odunitmd1Valid.AutoSize = true;
            this.odunitmd1Valid.BackColor = System.Drawing.Color.Azure;
            this.odunitmd1Valid.ForeColor = System.Drawing.Color.Red;
            this.odunitmd1Valid.Location = new System.Drawing.Point(171, 48);
            this.odunitmd1Valid.Name = "odunitmd1Valid";
            this.odunitmd1Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.odunitmd1Valid.Size = new System.Drawing.Size(12, 13);
            this.odunitmd1Valid.TabIndex = 6;
            this.odunitmd1Valid.Text = "*";
            this.odunitmd1Valid.Visible = false;
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.SkyBlue;
            this.panel32.Controls.Add(this.ODUnitMainEndDate);
            this.panel32.Controls.Add(this.label37);
            this.panel32.Location = new System.Drawing.Point(388, 21);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(127, 49);
            this.panel32.TabIndex = 4;
            // 
            // ODUnitMainEndDate
            // 
            this.ODUnitMainEndDate.Enabled = false;
            this.ODUnitMainEndDate.HasButtons = true;
            this.ODUnitMainEndDate.Location = new System.Drawing.Point(3, 20);
            this.ODUnitMainEndDate.Name = "ODUnitMainEndDate";
            this.ODUnitMainEndDate.Readonly = true;
            this.ODUnitMainEndDate.Size = new System.Drawing.Size(120, 20);
            this.ODUnitMainEndDate.TabIndex = 8;
            this.ODUnitMainEndDate.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.ForeColor = System.Drawing.SystemColors.Window;
            this.label37.Location = new System.Drawing.Point(30, 4);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(70, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "END DATE";
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.SkyBlue;
            this.panel33.Controls.Add(this.ODUnitMainStartDate);
            this.panel33.Controls.Add(this.label38);
            this.panel33.Location = new System.Drawing.Point(185, 22);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(127, 49);
            this.panel33.TabIndex = 3;
            // 
            // ODUnitMainStartDate
            // 
            this.ODUnitMainStartDate.Enabled = false;
            this.ODUnitMainStartDate.HasButtons = true;
            this.ODUnitMainStartDate.Location = new System.Drawing.Point(3, 21);
            this.ODUnitMainStartDate.Name = "ODUnitMainStartDate";
            this.ODUnitMainStartDate.Readonly = true;
            this.ODUnitMainStartDate.Size = new System.Drawing.Size(120, 20);
            this.ODUnitMainStartDate.TabIndex = 7;
            this.ODUnitMainStartDate.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.ForeColor = System.Drawing.SystemColors.Window;
            this.label38.Location = new System.Drawing.Point(24, 4);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(85, 13);
            this.label38.TabIndex = 0;
            this.label38.Text = "START DATE";
            // 
            // ODUnitMainCheck
            // 
            this.ODUnitMainCheck.AutoSize = true;
            this.ODUnitMainCheck.Location = new System.Drawing.Point(17, 33);
            this.ODUnitMainCheck.Name = "ODUnitMainCheck";
            this.ODUnitMainCheck.Size = new System.Drawing.Size(115, 17);
            this.ODUnitMainCheck.TabIndex = 6;
            this.ODUnitMainCheck.Text = "MAINTENANCE";
            this.ODUnitMainCheck.UseVisualStyleBackColor = true;
            this.ODUnitMainCheck.CheckedChanged += new System.EventHandler(this.ODUnitMainCheck_CheckedChanged);
            // 
            // ODUnitPowerGB
            // 
            this.ODUnitPowerGB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ODUnitPowerGB.BackColor = System.Drawing.Color.Azure;
            this.ODUnitPowerGB.Controls.Add(this.odunitpd2Valid);
            this.ODUnitPowerGB.Controls.Add(this.odunitpd1Valid);
            this.ODUnitPowerGB.Controls.Add(this.ODUnitPowerGrid2);
            this.ODUnitPowerGB.Controls.Add(this.ODUnitPowerGrid1);
            this.ODUnitPowerGB.Controls.Add(this.panel25);
            this.ODUnitPowerGB.Controls.Add(this.panel26);
            this.ODUnitPowerGB.Controls.Add(this.ODUnitPowerCheck);
            this.ODUnitPowerGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ODUnitPowerGB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ODUnitPowerGB.Location = new System.Drawing.Point(14, 277);
            this.ODUnitPowerGB.Name = "ODUnitPowerGB";
            this.ODUnitPowerGB.Size = new System.Drawing.Size(713, 236);
            this.ODUnitPowerGB.TabIndex = 2;
            this.ODUnitPowerGB.TabStop = false;
            this.ODUnitPowerGB.Text = "POWER LIMITED";
            // 
            // odunitpd2Valid
            // 
            this.odunitpd2Valid.AutoSize = true;
            this.odunitpd2Valid.BackColor = System.Drawing.Color.Azure;
            this.odunitpd2Valid.ForeColor = System.Drawing.Color.Red;
            this.odunitpd2Valid.Location = new System.Drawing.Point(369, 208);
            this.odunitpd2Valid.Name = "odunitpd2Valid";
            this.odunitpd2Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.odunitpd2Valid.Size = new System.Drawing.Size(12, 13);
            this.odunitpd2Valid.TabIndex = 10;
            this.odunitpd2Valid.Text = "*";
            this.odunitpd2Valid.Visible = false;
            // 
            // odunitpd1Valid
            // 
            this.odunitpd1Valid.AutoSize = true;
            this.odunitpd1Valid.BackColor = System.Drawing.Color.Azure;
            this.odunitpd1Valid.ForeColor = System.Drawing.Color.Red;
            this.odunitpd1Valid.Location = new System.Drawing.Point(170, 206);
            this.odunitpd1Valid.Name = "odunitpd1Valid";
            this.odunitpd1Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.odunitpd1Valid.Size = new System.Drawing.Size(12, 13);
            this.odunitpd1Valid.TabIndex = 9;
            this.odunitpd1Valid.Text = "*";
            this.odunitpd1Valid.Visible = false;
            // 
            // ODUnitPowerGrid2
            // 
            this.ODUnitPowerGrid2.BackgroundColor = System.Drawing.Color.White;
            this.ODUnitPowerGrid2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ODUnitPowerGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle32;
            this.ODUnitPowerGrid2.ColumnHeadersHeight = 22;
            this.ODUnitPowerGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn55,
            this.dataGridViewTextBoxColumn56,
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn58,
            this.dataGridViewTextBoxColumn59,
            this.dataGridViewTextBoxColumn60,
            this.dataGridViewTextBoxColumn61,
            this.dataGridViewTextBoxColumn62,
            this.dataGridViewTextBoxColumn63,
            this.dataGridViewTextBoxColumn64,
            this.dataGridViewTextBoxColumn65,
            this.dataGridViewTextBoxColumn66});
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle33.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ODUnitPowerGrid2.DefaultCellStyle = dataGridViewCellStyle33;
            this.ODUnitPowerGrid2.Enabled = false;
            this.ODUnitPowerGrid2.EnableHeadersVisualStyles = false;
            this.ODUnitPowerGrid2.Location = new System.Drawing.Point(18, 113);
            this.ODUnitPowerGrid2.Name = "ODUnitPowerGrid2";
            this.ODUnitPowerGrid2.RowHeadersVisible = false;
            this.ODUnitPowerGrid2.RowTemplate.Height = 30;
            this.ODUnitPowerGrid2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.ODUnitPowerGrid2.Size = new System.Drawing.Size(663, 63);
            this.ODUnitPowerGrid2.TabIndex = 15;
            this.ODUnitPowerGrid2.Validated += new System.EventHandler(this.ODUnitPowerGrid2_Validated);
            // 
            // dataGridViewTextBoxColumn55
            // 
            this.dataGridViewTextBoxColumn55.HeaderText = "13";
            this.dataGridViewTextBoxColumn55.Name = "dataGridViewTextBoxColumn55";
            this.dataGridViewTextBoxColumn55.Width = 55;
            // 
            // dataGridViewTextBoxColumn56
            // 
            this.dataGridViewTextBoxColumn56.HeaderText = "14";
            this.dataGridViewTextBoxColumn56.Name = "dataGridViewTextBoxColumn56";
            this.dataGridViewTextBoxColumn56.Width = 55;
            // 
            // dataGridViewTextBoxColumn57
            // 
            this.dataGridViewTextBoxColumn57.HeaderText = "15";
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            this.dataGridViewTextBoxColumn57.Width = 55;
            // 
            // dataGridViewTextBoxColumn58
            // 
            this.dataGridViewTextBoxColumn58.HeaderText = "16";
            this.dataGridViewTextBoxColumn58.Name = "dataGridViewTextBoxColumn58";
            this.dataGridViewTextBoxColumn58.Width = 55;
            // 
            // dataGridViewTextBoxColumn59
            // 
            this.dataGridViewTextBoxColumn59.HeaderText = "17";
            this.dataGridViewTextBoxColumn59.Name = "dataGridViewTextBoxColumn59";
            this.dataGridViewTextBoxColumn59.Width = 55;
            // 
            // dataGridViewTextBoxColumn60
            // 
            this.dataGridViewTextBoxColumn60.HeaderText = "18";
            this.dataGridViewTextBoxColumn60.Name = "dataGridViewTextBoxColumn60";
            this.dataGridViewTextBoxColumn60.Width = 55;
            // 
            // dataGridViewTextBoxColumn61
            // 
            this.dataGridViewTextBoxColumn61.HeaderText = "19";
            this.dataGridViewTextBoxColumn61.Name = "dataGridViewTextBoxColumn61";
            this.dataGridViewTextBoxColumn61.Width = 55;
            // 
            // dataGridViewTextBoxColumn62
            // 
            this.dataGridViewTextBoxColumn62.HeaderText = "20";
            this.dataGridViewTextBoxColumn62.Name = "dataGridViewTextBoxColumn62";
            this.dataGridViewTextBoxColumn62.Width = 55;
            // 
            // dataGridViewTextBoxColumn63
            // 
            this.dataGridViewTextBoxColumn63.HeaderText = "21";
            this.dataGridViewTextBoxColumn63.Name = "dataGridViewTextBoxColumn63";
            this.dataGridViewTextBoxColumn63.Width = 55;
            // 
            // dataGridViewTextBoxColumn64
            // 
            this.dataGridViewTextBoxColumn64.HeaderText = "22";
            this.dataGridViewTextBoxColumn64.Name = "dataGridViewTextBoxColumn64";
            this.dataGridViewTextBoxColumn64.Width = 55;
            // 
            // dataGridViewTextBoxColumn65
            // 
            this.dataGridViewTextBoxColumn65.HeaderText = "23";
            this.dataGridViewTextBoxColumn65.Name = "dataGridViewTextBoxColumn65";
            this.dataGridViewTextBoxColumn65.Width = 55;
            // 
            // dataGridViewTextBoxColumn66
            // 
            this.dataGridViewTextBoxColumn66.HeaderText = "24";
            this.dataGridViewTextBoxColumn66.Name = "dataGridViewTextBoxColumn66";
            this.dataGridViewTextBoxColumn66.Width = 55;
            // 
            // ODUnitPowerGrid1
            // 
            this.ODUnitPowerGrid1.BackgroundColor = System.Drawing.Color.White;
            this.ODUnitPowerGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ODUnitPowerGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.ODUnitPowerGrid1.ColumnHeadersHeight = 22;
            this.ODUnitPowerGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn67,
            this.dataGridViewTextBoxColumn68,
            this.dataGridViewTextBoxColumn69,
            this.dataGridViewTextBoxColumn70,
            this.dataGridViewTextBoxColumn71,
            this.dataGridViewTextBoxColumn72,
            this.dataGridViewTextBoxColumn73,
            this.dataGridViewTextBoxColumn74,
            this.dataGridViewTextBoxColumn75,
            this.dataGridViewTextBoxColumn76,
            this.dataGridViewTextBoxColumn77,
            this.dataGridViewTextBoxColumn78});
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle35.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ODUnitPowerGrid1.DefaultCellStyle = dataGridViewCellStyle35;
            this.ODUnitPowerGrid1.Enabled = false;
            this.ODUnitPowerGrid1.EnableHeadersVisualStyles = false;
            this.ODUnitPowerGrid1.Location = new System.Drawing.Point(19, 46);
            this.ODUnitPowerGrid1.Name = "ODUnitPowerGrid1";
            this.ODUnitPowerGrid1.RowHeadersVisible = false;
            this.ODUnitPowerGrid1.RowTemplate.Height = 30;
            this.ODUnitPowerGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.ODUnitPowerGrid1.Size = new System.Drawing.Size(663, 63);
            this.ODUnitPowerGrid1.TabIndex = 14;
            this.ODUnitPowerGrid1.Validated += new System.EventHandler(this.ODUnitPowerGrid1_Validated);
            // 
            // dataGridViewTextBoxColumn67
            // 
            this.dataGridViewTextBoxColumn67.HeaderText = "1";
            this.dataGridViewTextBoxColumn67.Name = "dataGridViewTextBoxColumn67";
            this.dataGridViewTextBoxColumn67.Width = 55;
            // 
            // dataGridViewTextBoxColumn68
            // 
            this.dataGridViewTextBoxColumn68.HeaderText = "2";
            this.dataGridViewTextBoxColumn68.Name = "dataGridViewTextBoxColumn68";
            this.dataGridViewTextBoxColumn68.Width = 55;
            // 
            // dataGridViewTextBoxColumn69
            // 
            this.dataGridViewTextBoxColumn69.HeaderText = "3";
            this.dataGridViewTextBoxColumn69.Name = "dataGridViewTextBoxColumn69";
            this.dataGridViewTextBoxColumn69.Width = 55;
            // 
            // dataGridViewTextBoxColumn70
            // 
            this.dataGridViewTextBoxColumn70.HeaderText = "4";
            this.dataGridViewTextBoxColumn70.Name = "dataGridViewTextBoxColumn70";
            this.dataGridViewTextBoxColumn70.Width = 55;
            // 
            // dataGridViewTextBoxColumn71
            // 
            this.dataGridViewTextBoxColumn71.HeaderText = "5";
            this.dataGridViewTextBoxColumn71.Name = "dataGridViewTextBoxColumn71";
            this.dataGridViewTextBoxColumn71.Width = 55;
            // 
            // dataGridViewTextBoxColumn72
            // 
            this.dataGridViewTextBoxColumn72.HeaderText = "6";
            this.dataGridViewTextBoxColumn72.Name = "dataGridViewTextBoxColumn72";
            this.dataGridViewTextBoxColumn72.Width = 55;
            // 
            // dataGridViewTextBoxColumn73
            // 
            this.dataGridViewTextBoxColumn73.HeaderText = "7";
            this.dataGridViewTextBoxColumn73.Name = "dataGridViewTextBoxColumn73";
            this.dataGridViewTextBoxColumn73.Width = 55;
            // 
            // dataGridViewTextBoxColumn74
            // 
            this.dataGridViewTextBoxColumn74.HeaderText = "8";
            this.dataGridViewTextBoxColumn74.Name = "dataGridViewTextBoxColumn74";
            this.dataGridViewTextBoxColumn74.Width = 55;
            // 
            // dataGridViewTextBoxColumn75
            // 
            this.dataGridViewTextBoxColumn75.HeaderText = "9";
            this.dataGridViewTextBoxColumn75.Name = "dataGridViewTextBoxColumn75";
            this.dataGridViewTextBoxColumn75.Width = 55;
            // 
            // dataGridViewTextBoxColumn76
            // 
            this.dataGridViewTextBoxColumn76.HeaderText = "10";
            this.dataGridViewTextBoxColumn76.Name = "dataGridViewTextBoxColumn76";
            this.dataGridViewTextBoxColumn76.Width = 55;
            // 
            // dataGridViewTextBoxColumn77
            // 
            this.dataGridViewTextBoxColumn77.HeaderText = "11";
            this.dataGridViewTextBoxColumn77.Name = "dataGridViewTextBoxColumn77";
            this.dataGridViewTextBoxColumn77.Width = 55;
            // 
            // dataGridViewTextBoxColumn78
            // 
            this.dataGridViewTextBoxColumn78.HeaderText = "12";
            this.dataGridViewTextBoxColumn78.Name = "dataGridViewTextBoxColumn78";
            this.dataGridViewTextBoxColumn78.Width = 55;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.SkyBlue;
            this.panel25.Controls.Add(this.ODUnitPowerEndDate);
            this.panel25.Controls.Add(this.label29);
            this.panel25.Location = new System.Drawing.Point(387, 181);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(127, 49);
            this.panel25.TabIndex = 4;
            // 
            // ODUnitPowerEndDate
            // 
            this.ODUnitPowerEndDate.Enabled = false;
            this.ODUnitPowerEndDate.HasButtons = true;
            this.ODUnitPowerEndDate.Location = new System.Drawing.Point(3, 21);
            this.ODUnitPowerEndDate.Name = "ODUnitPowerEndDate";
            this.ODUnitPowerEndDate.Readonly = true;
            this.ODUnitPowerEndDate.Size = new System.Drawing.Size(120, 20);
            this.ODUnitPowerEndDate.TabIndex = 17;
            this.ODUnitPowerEndDate.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.SystemColors.Window;
            this.label29.Location = new System.Drawing.Point(29, 4);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(70, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "END DATE";
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.SkyBlue;
            this.panel26.Controls.Add(this.ODUnitPowerStartDate);
            this.panel26.Controls.Add(this.label30);
            this.panel26.Location = new System.Drawing.Point(185, 181);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(127, 49);
            this.panel26.TabIndex = 3;
            // 
            // ODUnitPowerStartDate
            // 
            this.ODUnitPowerStartDate.Enabled = false;
            this.ODUnitPowerStartDate.HasButtons = true;
            this.ODUnitPowerStartDate.Location = new System.Drawing.Point(3, 21);
            this.ODUnitPowerStartDate.Name = "ODUnitPowerStartDate";
            this.ODUnitPowerStartDate.Readonly = true;
            this.ODUnitPowerStartDate.Size = new System.Drawing.Size(120, 20);
            this.ODUnitPowerStartDate.TabIndex = 16;
            this.ODUnitPowerStartDate.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.SystemColors.Window;
            this.label30.Location = new System.Drawing.Point(24, 4);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(85, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "START DATE";
            // 
            // ODUnitPowerCheck
            // 
            this.ODUnitPowerCheck.AutoSize = true;
            this.ODUnitPowerCheck.Location = new System.Drawing.Point(17, 22);
            this.ODUnitPowerCheck.Name = "ODUnitPowerCheck";
            this.ODUnitPowerCheck.Size = new System.Drawing.Size(126, 17);
            this.ODUnitPowerCheck.TabIndex = 13;
            this.ODUnitPowerCheck.Text = "POWER LIMITED";
            this.ODUnitPowerCheck.UseVisualStyleBackColor = true;
            this.ODUnitPowerCheck.CheckedChanged += new System.EventHandler(this.ODUnitPowerCheck_CheckedChanged);
            // 
            // ODUnitServiceGB
            // 
            this.ODUnitServiceGB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ODUnitServiceGB.BackColor = System.Drawing.Color.Azure;
            this.ODUnitServiceGB.Controls.Add(this.odunitosd2Valid);
            this.ODUnitServiceGB.Controls.Add(this.odunitosd1Valid);
            this.ODUnitServiceGB.Controls.Add(this.panel30);
            this.ODUnitServiceGB.Controls.Add(this.panel31);
            this.ODUnitServiceGB.Controls.Add(this.ODUnitNoOffCheck);
            this.ODUnitServiceGB.Controls.Add(this.ODUnitNoOnCheck);
            this.ODUnitServiceGB.Controls.Add(this.ODUnitOutCheck);
            this.ODUnitServiceGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ODUnitServiceGB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ODUnitServiceGB.Location = new System.Drawing.Point(12, 6);
            this.ODUnitServiceGB.Name = "ODUnitServiceGB";
            this.ODUnitServiceGB.Size = new System.Drawing.Size(713, 92);
            this.ODUnitServiceGB.TabIndex = 0;
            this.ODUnitServiceGB.TabStop = false;
            this.ODUnitServiceGB.Text = "SERVICE";
            // 
            // odunitosd2Valid
            // 
            this.odunitosd2Valid.AutoSize = true;
            this.odunitosd2Valid.BackColor = System.Drawing.Color.Azure;
            this.odunitosd2Valid.ForeColor = System.Drawing.Color.Red;
            this.odunitosd2Valid.Location = new System.Drawing.Point(373, 61);
            this.odunitosd2Valid.Name = "odunitosd2Valid";
            this.odunitosd2Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.odunitosd2Valid.Size = new System.Drawing.Size(12, 13);
            this.odunitosd2Valid.TabIndex = 6;
            this.odunitosd2Valid.Text = "*";
            this.odunitosd2Valid.Visible = false;
            // 
            // odunitosd1Valid
            // 
            this.odunitosd1Valid.AutoSize = true;
            this.odunitosd1Valid.BackColor = System.Drawing.Color.Azure;
            this.odunitosd1Valid.ForeColor = System.Drawing.Color.Red;
            this.odunitosd1Valid.Location = new System.Drawing.Point(168, 61);
            this.odunitosd1Valid.Name = "odunitosd1Valid";
            this.odunitosd1Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.odunitosd1Valid.Size = new System.Drawing.Size(12, 13);
            this.odunitosd1Valid.TabIndex = 5;
            this.odunitosd1Valid.Text = "*";
            this.odunitosd1Valid.Visible = false;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.SkyBlue;
            this.panel30.Controls.Add(this.ODUnitServiceEndDate);
            this.panel30.Controls.Add(this.label34);
            this.panel30.Location = new System.Drawing.Point(388, 33);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(127, 49);
            this.panel30.TabIndex = 4;
            // 
            // ODUnitServiceEndDate
            // 
            this.ODUnitServiceEndDate.Enabled = false;
            this.ODUnitServiceEndDate.HasButtons = true;
            this.ODUnitServiceEndDate.Location = new System.Drawing.Point(3, 21);
            this.ODUnitServiceEndDate.Name = "ODUnitServiceEndDate";
            this.ODUnitServiceEndDate.Readonly = true;
            this.ODUnitServiceEndDate.Size = new System.Drawing.Size(120, 20);
            this.ODUnitServiceEndDate.TabIndex = 5;
            this.ODUnitServiceEndDate.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.ForeColor = System.Drawing.SystemColors.Window;
            this.label34.Location = new System.Drawing.Point(27, 4);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(70, 13);
            this.label34.TabIndex = 0;
            this.label34.Text = "END DATE";
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.SkyBlue;
            this.panel31.Controls.Add(this.ODUnitServiceStartDate);
            this.panel31.Controls.Add(this.label35);
            this.panel31.Location = new System.Drawing.Point(185, 33);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(127, 49);
            this.panel31.TabIndex = 3;
            // 
            // ODUnitServiceStartDate
            // 
            this.ODUnitServiceStartDate.Enabled = false;
            this.ODUnitServiceStartDate.HasButtons = true;
            this.ODUnitServiceStartDate.Location = new System.Drawing.Point(3, 21);
            this.ODUnitServiceStartDate.Name = "ODUnitServiceStartDate";
            this.ODUnitServiceStartDate.Readonly = true;
            this.ODUnitServiceStartDate.Size = new System.Drawing.Size(120, 20);
            this.ODUnitServiceStartDate.TabIndex = 4;
            this.ODUnitServiceStartDate.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.ForeColor = System.Drawing.SystemColors.Window;
            this.label35.Location = new System.Drawing.Point(24, 4);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(85, 13);
            this.label35.TabIndex = 0;
            this.label35.Text = "START DATE";
            // 
            // ODUnitNoOffCheck
            // 
            this.ODUnitNoOffCheck.AutoSize = true;
            this.ODUnitNoOffCheck.Location = new System.Drawing.Point(17, 65);
            this.ODUnitNoOffCheck.Name = "ODUnitNoOffCheck";
            this.ODUnitNoOffCheck.Size = new System.Drawing.Size(71, 17);
            this.ODUnitNoOffCheck.TabIndex = 3;
            this.ODUnitNoOffCheck.Text = "NO OFF";
            this.ODUnitNoOffCheck.UseVisualStyleBackColor = true;
            // 
            // ODUnitNoOnCheck
            // 
            this.ODUnitNoOnCheck.AutoSize = true;
            this.ODUnitNoOnCheck.Location = new System.Drawing.Point(17, 42);
            this.ODUnitNoOnCheck.Name = "ODUnitNoOnCheck";
            this.ODUnitNoOnCheck.Size = new System.Drawing.Size(66, 17);
            this.ODUnitNoOnCheck.TabIndex = 2;
            this.ODUnitNoOnCheck.Text = "NO ON";
            this.ODUnitNoOnCheck.UseVisualStyleBackColor = true;
            // 
            // ODUnitOutCheck
            // 
            this.ODUnitOutCheck.AutoSize = true;
            this.ODUnitOutCheck.Location = new System.Drawing.Point(17, 19);
            this.ODUnitOutCheck.Name = "ODUnitOutCheck";
            this.ODUnitOutCheck.Size = new System.Drawing.Size(109, 17);
            this.ODUnitOutCheck.TabIndex = 1;
            this.ODUnitOutCheck.Text = "OUT SERVICE";
            this.ODUnitOutCheck.UseVisualStyleBackColor = true;
            this.ODUnitOutCheck.CheckedChanged += new System.EventHandler(this.ODUnitOutCheck_CheckedChanged);
            // 
            // ODUnitFuelGB
            // 
            this.ODUnitFuelGB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ODUnitFuelGB.BackColor = System.Drawing.Color.Azure;
            this.ODUnitFuelGB.Controls.Add(this.odunitsfd2Valid);
            this.ODUnitFuelGB.Controls.Add(this.odunitsfd1Valid);
            this.ODUnitFuelGB.Controls.Add(this.panel27);
            this.ODUnitFuelGB.Controls.Add(this.panel28);
            this.ODUnitFuelGB.Controls.Add(this.panel29);
            this.ODUnitFuelGB.Controls.Add(this.ODUnitFuelCheck);
            this.ODUnitFuelGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ODUnitFuelGB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ODUnitFuelGB.Location = new System.Drawing.Point(12, 190);
            this.ODUnitFuelGB.Name = "ODUnitFuelGB";
            this.ODUnitFuelGB.Size = new System.Drawing.Size(713, 81);
            this.ODUnitFuelGB.TabIndex = 1;
            this.ODUnitFuelGB.TabStop = false;
            this.ODUnitFuelGB.Text = "FUEL";
            // 
            // odunitsfd2Valid
            // 
            this.odunitsfd2Valid.AutoSize = true;
            this.odunitsfd2Valid.BackColor = System.Drawing.Color.Azure;
            this.odunitsfd2Valid.ForeColor = System.Drawing.Color.Red;
            this.odunitsfd2Valid.Location = new System.Drawing.Point(373, 48);
            this.odunitsfd2Valid.Name = "odunitsfd2Valid";
            this.odunitsfd2Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.odunitsfd2Valid.Size = new System.Drawing.Size(12, 13);
            this.odunitsfd2Valid.TabIndex = 10;
            this.odunitsfd2Valid.Text = "*";
            this.odunitsfd2Valid.Visible = false;
            // 
            // odunitsfd1Valid
            // 
            this.odunitsfd1Valid.AutoSize = true;
            this.odunitsfd1Valid.BackColor = System.Drawing.Color.Azure;
            this.odunitsfd1Valid.ForeColor = System.Drawing.Color.Red;
            this.odunitsfd1Valid.Location = new System.Drawing.Point(170, 49);
            this.odunitsfd1Valid.Name = "odunitsfd1Valid";
            this.odunitsfd1Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.odunitsfd1Valid.Size = new System.Drawing.Size(12, 13);
            this.odunitsfd1Valid.TabIndex = 9;
            this.odunitsfd1Valid.Text = "*";
            this.odunitsfd1Valid.Visible = false;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.SkyBlue;
            this.panel27.Controls.Add(this.ODUnitFuelTB);
            this.panel27.Controls.Add(this.label31);
            this.panel27.Location = new System.Drawing.Point(568, 19);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(127, 49);
            this.panel27.TabIndex = 5;
            // 
            // ODUnitFuelTB
            // 
            this.ODUnitFuelTB.BackColor = System.Drawing.Color.White;
            this.ODUnitFuelTB.Enabled = false;
            this.ODUnitFuelTB.Location = new System.Drawing.Point(14, 22);
            this.ODUnitFuelTB.Name = "ODUnitFuelTB";
            this.ODUnitFuelTB.Size = new System.Drawing.Size(99, 20);
            this.ODUnitFuelTB.TabIndex = 12;
            this.ODUnitFuelTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.SystemColors.Window;
            this.label31.Location = new System.Drawing.Point(12, 4);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(105, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "FUEL QUANTITY";
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.SkyBlue;
            this.panel28.Controls.Add(this.ODUnitFuelEndDate);
            this.panel28.Controls.Add(this.label32);
            this.panel28.Location = new System.Drawing.Point(388, 21);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(127, 49);
            this.panel28.TabIndex = 4;
            // 
            // ODUnitFuelEndDate
            // 
            this.ODUnitFuelEndDate.Enabled = false;
            this.ODUnitFuelEndDate.HasButtons = true;
            this.ODUnitFuelEndDate.Location = new System.Drawing.Point(3, 20);
            this.ODUnitFuelEndDate.Name = "ODUnitFuelEndDate";
            this.ODUnitFuelEndDate.Readonly = true;
            this.ODUnitFuelEndDate.Size = new System.Drawing.Size(120, 20);
            this.ODUnitFuelEndDate.TabIndex = 11;
            this.ODUnitFuelEndDate.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.ForeColor = System.Drawing.SystemColors.Window;
            this.label32.Location = new System.Drawing.Point(30, 4);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(70, 13);
            this.label32.TabIndex = 0;
            this.label32.Text = "END DATE";
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.SkyBlue;
            this.panel29.Controls.Add(this.ODUnitFuelStartDate);
            this.panel29.Controls.Add(this.label33);
            this.panel29.Location = new System.Drawing.Point(185, 22);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(127, 49);
            this.panel29.TabIndex = 3;
            // 
            // ODUnitFuelStartDate
            // 
            this.ODUnitFuelStartDate.Enabled = false;
            this.ODUnitFuelStartDate.HasButtons = true;
            this.ODUnitFuelStartDate.Location = new System.Drawing.Point(3, 20);
            this.ODUnitFuelStartDate.Name = "ODUnitFuelStartDate";
            this.ODUnitFuelStartDate.Readonly = true;
            this.ODUnitFuelStartDate.Size = new System.Drawing.Size(120, 20);
            this.ODUnitFuelStartDate.TabIndex = 10;
            this.ODUnitFuelStartDate.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.ForeColor = System.Drawing.SystemColors.Window;
            this.label33.Location = new System.Drawing.Point(24, 4);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(85, 13);
            this.label33.TabIndex = 0;
            this.label33.Text = "START DATE";
            // 
            // ODUnitFuelCheck
            // 
            this.ODUnitFuelCheck.AutoSize = true;
            this.ODUnitFuelCheck.Location = new System.Drawing.Point(17, 33);
            this.ODUnitFuelCheck.Name = "ODUnitFuelCheck";
            this.ODUnitFuelCheck.Size = new System.Drawing.Size(112, 17);
            this.ODUnitFuelCheck.TabIndex = 9;
            this.ODUnitFuelCheck.Text = "SECOND FUEL";
            this.ODUnitFuelCheck.UseVisualStyleBackColor = true;
            this.ODUnitFuelCheck.CheckedChanged += new System.EventHandler(this.ODUnitFuelCheck_CheckedChanged);
            // 
            // ODSaveBtn
            // 
            this.ODSaveBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ODSaveBtn.BackColor = System.Drawing.Color.SkyBlue;
            this.ODSaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ODSaveBtn.Location = new System.Drawing.Point(644, 528);
            this.ODSaveBtn.Name = "ODSaveBtn";
            this.ODSaveBtn.Size = new System.Drawing.Size(75, 23);
            this.ODSaveBtn.TabIndex = 27;
            this.ODSaveBtn.Text = "Save";
            this.ODSaveBtn.UseVisualStyleBackColor = false;
            this.ODSaveBtn.Visible = false;
            this.ODSaveBtn.Click += new System.EventHandler(this.ODSaveBtn_Click);
            // 
            // ODPlantPanel
            // 
            this.ODPlantPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ODPlantPanel.Controls.Add(this.ODPowerMinGB);
            this.ODPlantPanel.Controls.Add(this.ODFuelGB);
            this.ODPlantPanel.Controls.Add(this.ODServiceGB);
            this.ODPlantPanel.Location = new System.Drawing.Point(4, 4);
            this.ODPlantPanel.Name = "ODPlantPanel";
            this.ODPlantPanel.Size = new System.Drawing.Size(738, 491);
            this.ODPlantPanel.TabIndex = 28;
            this.ODPlantPanel.Visible = false;
            // 
            // ODPowerMinGB
            // 
            this.ODPowerMinGB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ODPowerMinGB.BackColor = System.Drawing.Color.Azure;
            this.ODPowerMinGB.Controls.Add(this.ODpd2Valid);
            this.ODPowerMinGB.Controls.Add(this.ODpd1Valid);
            this.ODPowerMinGB.Controls.Add(this.ODPowerGrid2);
            this.ODPowerMinGB.Controls.Add(this.ODPowerGrid1);
            this.ODPowerMinGB.Controls.Add(this.panel22);
            this.ODPowerMinGB.Controls.Add(this.panel23);
            this.ODPowerMinGB.Controls.Add(this.ODPowerMinCheck);
            this.ODPowerMinGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ODPowerMinGB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ODPowerMinGB.Location = new System.Drawing.Point(13, 230);
            this.ODPowerMinGB.Name = "ODPowerMinGB";
            this.ODPowerMinGB.Size = new System.Drawing.Size(713, 246);
            this.ODPowerMinGB.TabIndex = 2;
            this.ODPowerMinGB.TabStop = false;
            this.ODPowerMinGB.Text = "POWER MIN";
            // 
            // ODpd2Valid
            // 
            this.ODpd2Valid.AutoSize = true;
            this.ODpd2Valid.BackColor = System.Drawing.Color.Azure;
            this.ODpd2Valid.ForeColor = System.Drawing.Color.Red;
            this.ODpd2Valid.Location = new System.Drawing.Point(373, 214);
            this.ODpd2Valid.Name = "ODpd2Valid";
            this.ODpd2Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ODpd2Valid.Size = new System.Drawing.Size(12, 13);
            this.ODpd2Valid.TabIndex = 8;
            this.ODpd2Valid.Text = "*";
            this.ODpd2Valid.Visible = false;
            // 
            // ODpd1Valid
            // 
            this.ODpd1Valid.AutoSize = true;
            this.ODpd1Valid.BackColor = System.Drawing.Color.Azure;
            this.ODpd1Valid.ForeColor = System.Drawing.Color.Red;
            this.ODpd1Valid.Location = new System.Drawing.Point(170, 212);
            this.ODpd1Valid.Name = "ODpd1Valid";
            this.ODpd1Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ODpd1Valid.Size = new System.Drawing.Size(12, 13);
            this.ODpd1Valid.TabIndex = 7;
            this.ODpd1Valid.Text = "*";
            this.ODpd1Valid.Visible = false;
            // 
            // ODPowerGrid2
            // 
            this.ODPowerGrid2.BackgroundColor = System.Drawing.Color.White;
            this.ODPowerGrid2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ODPowerGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle36;
            this.ODPowerGrid2.ColumnHeadersHeight = 22;
            this.ODPowerGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn47,
            this.dataGridViewTextBoxColumn48,
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewTextBoxColumn50,
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewTextBoxColumn53,
            this.dataGridViewTextBoxColumn54});
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ODPowerGrid2.DefaultCellStyle = dataGridViewCellStyle37;
            this.ODPowerGrid2.Enabled = false;
            this.ODPowerGrid2.EnableHeadersVisualStyles = false;
            this.ODPowerGrid2.Location = new System.Drawing.Point(26, 117);
            this.ODPowerGrid2.Name = "ODPowerGrid2";
            this.ODPowerGrid2.RowHeadersVisible = false;
            this.ODPowerGrid2.RowTemplate.Height = 30;
            this.ODPowerGrid2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.ODPowerGrid2.Size = new System.Drawing.Size(663, 63);
            this.ODPowerGrid2.TabIndex = 12;
            this.ODPowerGrid2.Validated += new System.EventHandler(this.ODPowerGrid2_Validated);
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.HeaderText = "13";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.Width = 55;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.HeaderText = "14";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.Width = 55;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.HeaderText = "15";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.Width = 55;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.HeaderText = "16";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.Width = 55;
            // 
            // dataGridViewTextBoxColumn47
            // 
            this.dataGridViewTextBoxColumn47.HeaderText = "17";
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            this.dataGridViewTextBoxColumn47.Width = 55;
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.HeaderText = "18";
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.Width = 55;
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.HeaderText = "19";
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.Width = 55;
            // 
            // dataGridViewTextBoxColumn50
            // 
            this.dataGridViewTextBoxColumn50.HeaderText = "20";
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.Width = 55;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.HeaderText = "21";
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.Width = 55;
            // 
            // dataGridViewTextBoxColumn52
            // 
            this.dataGridViewTextBoxColumn52.HeaderText = "22";
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.Width = 55;
            // 
            // dataGridViewTextBoxColumn53
            // 
            this.dataGridViewTextBoxColumn53.HeaderText = "23";
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.Width = 55;
            // 
            // dataGridViewTextBoxColumn54
            // 
            this.dataGridViewTextBoxColumn54.HeaderText = "24";
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            this.dataGridViewTextBoxColumn54.Width = 55;
            // 
            // ODPowerGrid1
            // 
            this.ODPowerGrid1.BackgroundColor = System.Drawing.Color.White;
            this.ODPowerGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ODPowerGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this.ODPowerGrid1.ColumnHeadersHeight = 22;
            this.ODPowerGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column18,
            this.Column19,
            this.Column20,
            this.Column21,
            this.Column22,
            this.Column23,
            this.Column24,
            this.Column25,
            this.Column26,
            this.Column27,
            this.Column28,
            this.Column29});
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle39.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ODPowerGrid1.DefaultCellStyle = dataGridViewCellStyle39;
            this.ODPowerGrid1.Enabled = false;
            this.ODPowerGrid1.EnableHeadersVisualStyles = false;
            this.ODPowerGrid1.Location = new System.Drawing.Point(26, 39);
            this.ODPowerGrid1.Name = "ODPowerGrid1";
            this.ODPowerGrid1.RowHeadersVisible = false;
            this.ODPowerGrid1.RowTemplate.Height = 30;
            this.ODPowerGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.ODPowerGrid1.Size = new System.Drawing.Size(663, 63);
            this.ODPowerGrid1.TabIndex = 11;
            this.ODPowerGrid1.Validated += new System.EventHandler(this.ODPowerGrid1_Validated);
            // 
            // Column18
            // 
            this.Column18.HeaderText = "1";
            this.Column18.Name = "Column18";
            this.Column18.Width = 55;
            // 
            // Column19
            // 
            this.Column19.HeaderText = "2";
            this.Column19.Name = "Column19";
            this.Column19.Width = 55;
            // 
            // Column20
            // 
            this.Column20.HeaderText = "3";
            this.Column20.Name = "Column20";
            this.Column20.Width = 55;
            // 
            // Column21
            // 
            this.Column21.HeaderText = "4";
            this.Column21.Name = "Column21";
            this.Column21.Width = 55;
            // 
            // Column22
            // 
            this.Column22.HeaderText = "5";
            this.Column22.Name = "Column22";
            this.Column22.Width = 55;
            // 
            // Column23
            // 
            this.Column23.HeaderText = "6";
            this.Column23.Name = "Column23";
            this.Column23.Width = 55;
            // 
            // Column24
            // 
            this.Column24.HeaderText = "7";
            this.Column24.Name = "Column24";
            this.Column24.Width = 55;
            // 
            // Column25
            // 
            this.Column25.HeaderText = "8";
            this.Column25.Name = "Column25";
            this.Column25.Width = 55;
            // 
            // Column26
            // 
            this.Column26.HeaderText = "9";
            this.Column26.Name = "Column26";
            this.Column26.Width = 55;
            // 
            // Column27
            // 
            this.Column27.HeaderText = "10";
            this.Column27.Name = "Column27";
            this.Column27.Width = 55;
            // 
            // Column28
            // 
            this.Column28.HeaderText = "11";
            this.Column28.Name = "Column28";
            this.Column28.Width = 55;
            // 
            // Column29
            // 
            this.Column29.HeaderText = "12";
            this.Column29.Name = "Column29";
            this.Column29.Width = 55;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.SkyBlue;
            this.panel22.Controls.Add(this.ODPowerEndDate);
            this.panel22.Controls.Add(this.label27);
            this.panel22.Location = new System.Drawing.Point(387, 186);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(127, 49);
            this.panel22.TabIndex = 4;
            // 
            // ODPowerEndDate
            // 
            this.ODPowerEndDate.Enabled = false;
            this.ODPowerEndDate.HasButtons = true;
            this.ODPowerEndDate.Location = new System.Drawing.Point(3, 21);
            this.ODPowerEndDate.Name = "ODPowerEndDate";
            this.ODPowerEndDate.Readonly = true;
            this.ODPowerEndDate.Size = new System.Drawing.Size(120, 20);
            this.ODPowerEndDate.TabIndex = 14;
            this.ODPowerEndDate.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.SystemColors.Window;
            this.label27.Location = new System.Drawing.Point(27, 4);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(70, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "END DATE";
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.SkyBlue;
            this.panel23.Controls.Add(this.ODPowerStartDate);
            this.panel23.Controls.Add(this.label28);
            this.panel23.Location = new System.Drawing.Point(185, 186);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(127, 49);
            this.panel23.TabIndex = 3;
            // 
            // ODPowerStartDate
            // 
            this.ODPowerStartDate.Enabled = false;
            this.ODPowerStartDate.HasButtons = true;
            this.ODPowerStartDate.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ODPowerStartDate.Location = new System.Drawing.Point(3, 22);
            this.ODPowerStartDate.Name = "ODPowerStartDate";
            this.ODPowerStartDate.Readonly = true;
            this.ODPowerStartDate.Size = new System.Drawing.Size(120, 20);
            this.ODPowerStartDate.TabIndex = 13;
            this.ODPowerStartDate.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.SystemColors.Window;
            this.label28.Location = new System.Drawing.Point(24, 4);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(85, 13);
            this.label28.TabIndex = 0;
            this.label28.Text = "START DATE";
            // 
            // ODPowerMinCheck
            // 
            this.ODPowerMinCheck.AutoSize = true;
            this.ODPowerMinCheck.Location = new System.Drawing.Point(17, 20);
            this.ODPowerMinCheck.Name = "ODPowerMinCheck";
            this.ODPowerMinCheck.Size = new System.Drawing.Size(99, 17);
            this.ODPowerMinCheck.TabIndex = 10;
            this.ODPowerMinCheck.Text = "POWER MIN";
            this.ODPowerMinCheck.UseVisualStyleBackColor = true;
            this.ODPowerMinCheck.CheckedChanged += new System.EventHandler(this.ODPowerMinCheck_CheckedChanged);
            // 
            // ODFuelGB
            // 
            this.ODFuelGB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ODFuelGB.BackColor = System.Drawing.Color.Azure;
            this.ODFuelGB.Controls.Add(this.ODsfd2Valid);
            this.ODFuelGB.Controls.Add(this.ODsfd1Valid);
            this.ODFuelGB.Controls.Add(this.panel21);
            this.ODFuelGB.Controls.Add(this.panel19);
            this.ODFuelGB.Controls.Add(this.panel20);
            this.ODFuelGB.Controls.Add(this.SecondFuelCheck);
            this.ODFuelGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ODFuelGB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ODFuelGB.Location = new System.Drawing.Point(13, 118);
            this.ODFuelGB.Name = "ODFuelGB";
            this.ODFuelGB.Size = new System.Drawing.Size(713, 100);
            this.ODFuelGB.TabIndex = 1;
            this.ODFuelGB.TabStop = false;
            this.ODFuelGB.Text = "FUEL";
            // 
            // ODsfd2Valid
            // 
            this.ODsfd2Valid.AutoSize = true;
            this.ODsfd2Valid.BackColor = System.Drawing.Color.Azure;
            this.ODsfd2Valid.ForeColor = System.Drawing.Color.Red;
            this.ODsfd2Valid.Location = new System.Drawing.Point(374, 60);
            this.ODsfd2Valid.Name = "ODsfd2Valid";
            this.ODsfd2Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ODsfd2Valid.Size = new System.Drawing.Size(12, 13);
            this.ODsfd2Valid.TabIndex = 7;
            this.ODsfd2Valid.Text = "*";
            this.ODsfd2Valid.Visible = false;
            // 
            // ODsfd1Valid
            // 
            this.ODsfd1Valid.AutoSize = true;
            this.ODsfd1Valid.BackColor = System.Drawing.Color.Azure;
            this.ODsfd1Valid.ForeColor = System.Drawing.Color.Red;
            this.ODsfd1Valid.Location = new System.Drawing.Point(170, 62);
            this.ODsfd1Valid.Name = "ODsfd1Valid";
            this.ODsfd1Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ODsfd1Valid.Size = new System.Drawing.Size(12, 13);
            this.ODsfd1Valid.TabIndex = 6;
            this.ODsfd1Valid.Text = "*";
            this.ODsfd1Valid.Visible = false;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.SkyBlue;
            this.panel21.Controls.Add(this.ODFuelQuantityTB);
            this.panel21.Controls.Add(this.label26);
            this.panel21.Location = new System.Drawing.Point(568, 33);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(127, 49);
            this.panel21.TabIndex = 5;
            // 
            // ODFuelQuantityTB
            // 
            this.ODFuelQuantityTB.BackColor = System.Drawing.Color.White;
            this.ODFuelQuantityTB.Enabled = false;
            this.ODFuelQuantityTB.Location = new System.Drawing.Point(14, 22);
            this.ODFuelQuantityTB.Name = "ODFuelQuantityTB";
            this.ODFuelQuantityTB.Size = new System.Drawing.Size(99, 20);
            this.ODFuelQuantityTB.TabIndex = 9;
            this.ODFuelQuantityTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.Window;
            this.label26.Location = new System.Drawing.Point(12, 4);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(105, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "FUEL QUANTITY";
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.SkyBlue;
            this.panel19.Controls.Add(this.ODFuelEndDate);
            this.panel19.Controls.Add(this.label3);
            this.panel19.Location = new System.Drawing.Point(388, 33);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(127, 49);
            this.panel19.TabIndex = 4;
            // 
            // ODFuelEndDate
            // 
            this.ODFuelEndDate.Enabled = false;
            this.ODFuelEndDate.HasButtons = true;
            this.ODFuelEndDate.Location = new System.Drawing.Point(3, 21);
            this.ODFuelEndDate.Name = "ODFuelEndDate";
            this.ODFuelEndDate.Readonly = true;
            this.ODFuelEndDate.Size = new System.Drawing.Size(120, 20);
            this.ODFuelEndDate.TabIndex = 8;
            this.ODFuelEndDate.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.Window;
            this.label3.Location = new System.Drawing.Point(28, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "END DATE";
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.SkyBlue;
            this.panel20.Controls.Add(this.ODFuelStartDate);
            this.panel20.Controls.Add(this.label4);
            this.panel20.Location = new System.Drawing.Point(185, 33);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(127, 49);
            this.panel20.TabIndex = 3;
            // 
            // ODFuelStartDate
            // 
            this.ODFuelStartDate.Enabled = false;
            this.ODFuelStartDate.HasButtons = true;
            this.ODFuelStartDate.Location = new System.Drawing.Point(3, 22);
            this.ODFuelStartDate.Name = "ODFuelStartDate";
            this.ODFuelStartDate.Readonly = true;
            this.ODFuelStartDate.Size = new System.Drawing.Size(120, 20);
            this.ODFuelStartDate.TabIndex = 7;
            this.ODFuelStartDate.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.Window;
            this.label4.Location = new System.Drawing.Point(24, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "START DATE";
            // 
            // SecondFuelCheck
            // 
            this.SecondFuelCheck.AutoSize = true;
            this.SecondFuelCheck.Location = new System.Drawing.Point(17, 42);
            this.SecondFuelCheck.Name = "SecondFuelCheck";
            this.SecondFuelCheck.Size = new System.Drawing.Size(112, 17);
            this.SecondFuelCheck.TabIndex = 6;
            this.SecondFuelCheck.Text = "SECOND FUEL";
            this.SecondFuelCheck.UseVisualStyleBackColor = true;
            this.SecondFuelCheck.CheckedChanged += new System.EventHandler(this.SecondFuelCheck_CheckedChanged);
            // 
            // ODServiceGB
            // 
            this.ODServiceGB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ODServiceGB.BackColor = System.Drawing.Color.Azure;
            this.ODServiceGB.Controls.Add(this.ODosd2Valid);
            this.ODServiceGB.Controls.Add(this.ODosd1Valid);
            this.ODServiceGB.Controls.Add(this.panel18);
            this.ODServiceGB.Controls.Add(this.panel17);
            this.ODServiceGB.Controls.Add(this.NoOffCheck);
            this.ODServiceGB.Controls.Add(this.NoOnCheck);
            this.ODServiceGB.Controls.Add(this.OutServiceCheck);
            this.ODServiceGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ODServiceGB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ODServiceGB.Location = new System.Drawing.Point(13, 7);
            this.ODServiceGB.Name = "ODServiceGB";
            this.ODServiceGB.Size = new System.Drawing.Size(713, 100);
            this.ODServiceGB.TabIndex = 0;
            this.ODServiceGB.TabStop = false;
            this.ODServiceGB.Text = "SERVICE";
            // 
            // ODosd2Valid
            // 
            this.ODosd2Valid.AutoSize = true;
            this.ODosd2Valid.BackColor = System.Drawing.Color.Azure;
            this.ODosd2Valid.ForeColor = System.Drawing.Color.Red;
            this.ODosd2Valid.Location = new System.Drawing.Point(367, 60);
            this.ODosd2Valid.Name = "ODosd2Valid";
            this.ODosd2Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ODosd2Valid.Size = new System.Drawing.Size(12, 13);
            this.ODosd2Valid.TabIndex = 6;
            this.ODosd2Valid.Text = "*";
            this.ODosd2Valid.Visible = false;
            // 
            // ODosd1Valid
            // 
            this.ODosd1Valid.AutoSize = true;
            this.ODosd1Valid.BackColor = System.Drawing.Color.Azure;
            this.ODosd1Valid.ForeColor = System.Drawing.Color.Red;
            this.ODosd1Valid.Location = new System.Drawing.Point(170, 63);
            this.ODosd1Valid.Name = "ODosd1Valid";
            this.ODosd1Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ODosd1Valid.Size = new System.Drawing.Size(12, 13);
            this.ODosd1Valid.TabIndex = 5;
            this.ODosd1Valid.Text = "*";
            this.ODosd1Valid.Visible = false;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.SkyBlue;
            this.panel18.Controls.Add(this.ODServiceEndDate);
            this.panel18.Controls.Add(this.label2);
            this.panel18.Location = new System.Drawing.Point(382, 33);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(133, 49);
            this.panel18.TabIndex = 4;
            // 
            // ODServiceEndDate
            // 
            this.ODServiceEndDate.Enabled = false;
            this.ODServiceEndDate.HasButtons = true;
            this.ODServiceEndDate.Location = new System.Drawing.Point(3, 22);
            this.ODServiceEndDate.Name = "ODServiceEndDate";
            this.ODServiceEndDate.Readonly = true;
            this.ODServiceEndDate.Size = new System.Drawing.Size(120, 20);
            this.ODServiceEndDate.TabIndex = 5;
            this.ODServiceEndDate.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.Window;
            this.label2.Location = new System.Drawing.Point(3, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "OutService End Date";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.SkyBlue;
            this.panel17.Controls.Add(this.ODServiceStartDate);
            this.panel17.Controls.Add(this.label1);
            this.panel17.Location = new System.Drawing.Point(185, 33);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(140, 49);
            this.panel17.TabIndex = 3;
            // 
            // ODServiceStartDate
            // 
            this.ODServiceStartDate.Enabled = false;
            this.ODServiceStartDate.HasButtons = true;
            this.ODServiceStartDate.Location = new System.Drawing.Point(3, 23);
            this.ODServiceStartDate.Name = "ODServiceStartDate";
            this.ODServiceStartDate.Readonly = true;
            this.ODServiceStartDate.Size = new System.Drawing.Size(120, 20);
            this.ODServiceStartDate.TabIndex = 4;
            this.ODServiceStartDate.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.Window;
            this.label1.Location = new System.Drawing.Point(2, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "OutService Start Date";
            // 
            // NoOffCheck
            // 
            this.NoOffCheck.AutoSize = true;
            this.NoOffCheck.Location = new System.Drawing.Point(17, 65);
            this.NoOffCheck.Name = "NoOffCheck";
            this.NoOffCheck.Size = new System.Drawing.Size(71, 17);
            this.NoOffCheck.TabIndex = 3;
            this.NoOffCheck.Text = "NO OFF";
            this.NoOffCheck.UseVisualStyleBackColor = true;
            // 
            // NoOnCheck
            // 
            this.NoOnCheck.AutoSize = true;
            this.NoOnCheck.Location = new System.Drawing.Point(17, 42);
            this.NoOnCheck.Name = "NoOnCheck";
            this.NoOnCheck.Size = new System.Drawing.Size(66, 17);
            this.NoOnCheck.TabIndex = 2;
            this.NoOnCheck.Text = "NO ON";
            this.NoOnCheck.UseVisualStyleBackColor = true;
            // 
            // OutServiceCheck
            // 
            this.OutServiceCheck.AutoSize = true;
            this.OutServiceCheck.Location = new System.Drawing.Point(17, 19);
            this.OutServiceCheck.Name = "OutServiceCheck";
            this.OutServiceCheck.Size = new System.Drawing.Size(109, 17);
            this.OutServiceCheck.TabIndex = 1;
            this.OutServiceCheck.Text = "OUT SERVICE";
            this.OutServiceCheck.UseVisualStyleBackColor = true;
            this.OutServiceCheck.CheckStateChanged += new System.EventHandler(this.OutServiceCheck_CheckStateChanged);
            // 
            // GeneralData
            // 
            this.GeneralData.BackgroundImage = global::PowerPlantProject.Properties.Resources._10;
            this.GeneralData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.GeneralData.Controls.Add(this.GDMainPanel);
            this.GeneralData.Controls.Add(this.GDHeaderGB);
            this.GeneralData.Location = new System.Drawing.Point(4, 29);
            this.GeneralData.Name = "GeneralData";
            this.GeneralData.Size = new System.Drawing.Size(757, 625);
            this.GeneralData.TabIndex = 6;
            this.GeneralData.Text = "General Data";
            this.GeneralData.UseVisualStyleBackColor = true;
            // 
            // GDMainPanel
            // 
            this.GDMainPanel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.GDMainPanel.Controls.Add(this.GDUnitPanel);
            this.GDMainPanel.Controls.Add(this.TempGV);
            this.GDMainPanel.Controls.Add(this.GDNewBtn);
            this.GDMainPanel.Controls.Add(this.GDDeleteBtn);
            this.GDMainPanel.Controls.Add(this.GDSteamGroup);
            this.GDMainPanel.Controls.Add(this.GDGasGroup);
            this.GDMainPanel.Location = new System.Drawing.Point(3, 71);
            this.GDMainPanel.Name = "GDMainPanel";
            this.GDMainPanel.Size = new System.Drawing.Size(743, 552);
            this.GDMainPanel.TabIndex = 20;
            // 
            // GDUnitPanel
            // 
            this.GDUnitPanel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.GDUnitPanel.Controls.Add(this.Maintenancegb);
            this.GDUnitPanel.Controls.Add(this.Currentgb);
            this.GDUnitPanel.Controls.Add(this.Anonygb);
            this.GDUnitPanel.Controls.Add(this.Powergb);
            this.GDUnitPanel.Location = new System.Drawing.Point(19, 7);
            this.GDUnitPanel.Name = "GDUnitPanel";
            this.GDUnitPanel.Size = new System.Drawing.Size(699, 493);
            this.GDUnitPanel.TabIndex = 32;
            this.GDUnitPanel.Visible = false;
            // 
            // Maintenancegb
            // 
            this.Maintenancegb.BackColor = System.Drawing.Color.Azure;
            this.Maintenancegb.Controls.Add(this.panel16);
            this.Maintenancegb.Controls.Add(this.panel15);
            this.Maintenancegb.Controls.Add(this.panel14);
            this.Maintenancegb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Maintenancegb.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Maintenancegb.Location = new System.Drawing.Point(126, 360);
            this.Maintenancegb.Name = "Maintenancegb";
            this.Maintenancegb.Size = new System.Drawing.Size(430, 80);
            this.Maintenancegb.TabIndex = 3;
            this.Maintenancegb.TabStop = false;
            this.Maintenancegb.Text = "MAINTENANCE";
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.SkyBlue;
            this.panel16.Controls.Add(this.GDEndDateCal);
            this.panel16.Controls.Add(this.label21);
            this.panel16.Location = new System.Drawing.Point(292, 25);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(123, 49);
            this.panel16.TabIndex = 3;
            // 
            // GDEndDateCal
            // 
            this.GDEndDateCal.Enabled = false;
            this.GDEndDateCal.HasButtons = true;
            this.GDEndDateCal.Location = new System.Drawing.Point(0, 25);
            this.GDEndDateCal.Name = "GDEndDateCal";
            this.GDEndDateCal.Readonly = true;
            this.GDEndDateCal.Size = new System.Drawing.Size(120, 20);
            this.GDEndDateCal.TabIndex = 14;
            this.GDEndDateCal.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.SystemColors.Window;
            this.label21.Location = new System.Drawing.Point(26, 4);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "End Date";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.SkyBlue;
            this.panel15.Controls.Add(this.GDStartDateCal);
            this.panel15.Controls.Add(this.label20);
            this.panel15.Location = new System.Drawing.Point(147, 25);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(122, 49);
            this.panel15.TabIndex = 2;
            // 
            // GDStartDateCal
            // 
            this.GDStartDateCal.Enabled = false;
            this.GDStartDateCal.HasButtons = true;
            this.GDStartDateCal.Location = new System.Drawing.Point(0, 25);
            this.GDStartDateCal.Name = "GDStartDateCal";
            this.GDStartDateCal.Readonly = true;
            this.GDStartDateCal.Size = new System.Drawing.Size(120, 20);
            this.GDStartDateCal.TabIndex = 13;
            this.GDStartDateCal.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.SystemColors.Window;
            this.label20.Location = new System.Drawing.Point(26, 4);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Start Date";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.SkyBlue;
            this.panel14.Controls.Add(this.GDMainTypeTB);
            this.panel14.Controls.Add(this.label19);
            this.panel14.Location = new System.Drawing.Point(6, 25);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(115, 49);
            this.panel14.TabIndex = 1;
            // 
            // GDMainTypeTB
            // 
            this.GDMainTypeTB.BackColor = System.Drawing.Color.White;
            this.GDMainTypeTB.Location = new System.Drawing.Point(6, 22);
            this.GDMainTypeTB.Name = "GDMainTypeTB";
            this.GDMainTypeTB.ReadOnly = true;
            this.GDMainTypeTB.Size = new System.Drawing.Size(103, 20);
            this.GDMainTypeTB.TabIndex = 12;
            this.GDMainTypeTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.SystemColors.Window;
            this.label19.Location = new System.Drawing.Point(42, 6);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(39, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "TYPE";
            // 
            // Currentgb
            // 
            this.Currentgb.BackColor = System.Drawing.Color.Azure;
            this.Currentgb.Controls.Add(this.panel13);
            this.Currentgb.Controls.Add(this.panel12);
            this.Currentgb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Currentgb.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Currentgb.Location = new System.Drawing.Point(234, 256);
            this.Currentgb.Name = "Currentgb";
            this.Currentgb.Size = new System.Drawing.Size(219, 86);
            this.Currentgb.TabIndex = 2;
            this.Currentgb.TabStop = false;
            this.Currentgb.Text = "CURRENT STATE";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.SkyBlue;
            this.panel13.Controls.Add(this.GDFuelTB);
            this.panel13.Controls.Add(this.GDFuelLb);
            this.panel13.Location = new System.Drawing.Point(118, 19);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(95, 49);
            this.panel13.TabIndex = 2;
            // 
            // GDFuelTB
            // 
            this.GDFuelTB.BackColor = System.Drawing.Color.White;
            this.GDFuelTB.Location = new System.Drawing.Point(9, 22);
            this.GDFuelTB.Name = "GDFuelTB";
            this.GDFuelTB.ReadOnly = true;
            this.GDFuelTB.Size = new System.Drawing.Size(75, 20);
            this.GDFuelTB.TabIndex = 11;
            this.GDFuelTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // GDFuelLb
            // 
            this.GDFuelLb.AutoSize = true;
            this.GDFuelLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDFuelLb.Location = new System.Drawing.Point(26, 4);
            this.GDFuelLb.Name = "GDFuelLb";
            this.GDFuelLb.Size = new System.Drawing.Size(38, 13);
            this.GDFuelLb.TabIndex = 0;
            this.GDFuelLb.Text = "FUEL";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.SkyBlue;
            this.panel12.Controls.Add(this.GDStateTB);
            this.panel12.Controls.Add(this.GDStateLb);
            this.panel12.Location = new System.Drawing.Point(6, 19);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(95, 49);
            this.panel12.TabIndex = 1;
            // 
            // GDStateTB
            // 
            this.GDStateTB.BackColor = System.Drawing.Color.White;
            this.GDStateTB.Location = new System.Drawing.Point(9, 22);
            this.GDStateTB.Name = "GDStateTB";
            this.GDStateTB.ReadOnly = true;
            this.GDStateTB.Size = new System.Drawing.Size(75, 20);
            this.GDStateTB.TabIndex = 10;
            this.GDStateTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // GDStateLb
            // 
            this.GDStateLb.AutoSize = true;
            this.GDStateLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDStateLb.Location = new System.Drawing.Point(26, 4);
            this.GDStateLb.Name = "GDStateLb";
            this.GDStateLb.Size = new System.Drawing.Size(47, 13);
            this.GDStateLb.TabIndex = 0;
            this.GDStateLb.Text = "STATE";
            // 
            // Anonygb
            // 
            this.Anonygb.BackColor = System.Drawing.Color.Azure;
            this.Anonygb.Controls.Add(this.panel11);
            this.Anonygb.Controls.Add(this.panel10);
            this.Anonygb.Controls.Add(this.panel9);
            this.Anonygb.Controls.Add(this.panel8);
            this.Anonygb.Controls.Add(this.panel7);
            this.Anonygb.Controls.Add(this.panel6);
            this.Anonygb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Anonygb.Location = new System.Drawing.Point(6, 152);
            this.Anonygb.Name = "Anonygb";
            this.Anonygb.Size = new System.Drawing.Size(689, 79);
            this.Anonygb.TabIndex = 1;
            this.Anonygb.TabStop = false;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.SkyBlue;
            this.panel11.Controls.Add(this.GDIntConsumeTB);
            this.panel11.Controls.Add(this.GDConsLb);
            this.panel11.Location = new System.Drawing.Point(573, 19);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(110, 49);
            this.panel11.TabIndex = 4;
            // 
            // GDIntConsumeTB
            // 
            this.GDIntConsumeTB.BackColor = System.Drawing.Color.White;
            this.GDIntConsumeTB.Location = new System.Drawing.Point(9, 22);
            this.GDIntConsumeTB.Name = "GDIntConsumeTB";
            this.GDIntConsumeTB.ReadOnly = true;
            this.GDIntConsumeTB.Size = new System.Drawing.Size(86, 20);
            this.GDIntConsumeTB.TabIndex = 9;
            this.GDIntConsumeTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDIntConsumeTB.Validated += new System.EventHandler(this.GDIntConsumeTB_Validated);
            // 
            // GDConsLb
            // 
            this.GDConsLb.AutoSize = true;
            this.GDConsLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GDConsLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDConsLb.Location = new System.Drawing.Point(3, 4);
            this.GDConsLb.Name = "GDConsLb";
            this.GDConsLb.Size = new System.Drawing.Size(105, 13);
            this.GDConsLb.TabIndex = 0;
            this.GDConsLb.Text = "Internal Consume";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.SkyBlue;
            this.panel10.Controls.Add(this.GDRampRateTB);
            this.panel10.Controls.Add(this.GDRampLb);
            this.panel10.Location = new System.Drawing.Point(466, 19);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(95, 49);
            this.panel10.TabIndex = 5;
            // 
            // GDRampRateTB
            // 
            this.GDRampRateTB.BackColor = System.Drawing.Color.White;
            this.GDRampRateTB.Location = new System.Drawing.Point(9, 22);
            this.GDRampRateTB.Name = "GDRampRateTB";
            this.GDRampRateTB.ReadOnly = true;
            this.GDRampRateTB.Size = new System.Drawing.Size(75, 20);
            this.GDRampRateTB.TabIndex = 8;
            this.GDRampRateTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDRampRateTB.Validated += new System.EventHandler(this.GDRampRateTB_Validated);
            // 
            // GDRampLb
            // 
            this.GDRampLb.AutoSize = true;
            this.GDRampLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GDRampLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDRampLb.Location = new System.Drawing.Point(10, 4);
            this.GDRampLb.Name = "GDRampLb";
            this.GDRampLb.Size = new System.Drawing.Size(66, 13);
            this.GDRampLb.TabIndex = 0;
            this.GDRampLb.Text = "RampRate";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.SkyBlue;
            this.panel9.Controls.Add(this.GDTimeHotStartTB);
            this.panel9.Controls.Add(this.GDHotLb);
            this.panel9.Location = new System.Drawing.Point(349, 19);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(98, 49);
            this.panel9.TabIndex = 4;
            // 
            // GDTimeHotStartTB
            // 
            this.GDTimeHotStartTB.BackColor = System.Drawing.Color.White;
            this.GDTimeHotStartTB.Location = new System.Drawing.Point(6, 22);
            this.GDTimeHotStartTB.Name = "GDTimeHotStartTB";
            this.GDTimeHotStartTB.ReadOnly = true;
            this.GDTimeHotStartTB.Size = new System.Drawing.Size(79, 20);
            this.GDTimeHotStartTB.TabIndex = 7;
            this.GDTimeHotStartTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDTimeHotStartTB.Validated += new System.EventHandler(this.GDTimeHotStartTB_Validated);
            // 
            // GDHotLb
            // 
            this.GDHotLb.AutoSize = true;
            this.GDHotLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GDHotLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDHotLb.Location = new System.Drawing.Point(2, 4);
            this.GDHotLb.Name = "GDHotLb";
            this.GDHotLb.Size = new System.Drawing.Size(89, 13);
            this.GDHotLb.TabIndex = 0;
            this.GDHotLb.Text = "Time Hot Start";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.SkyBlue;
            this.panel8.Controls.Add(this.GDTimeColdStartTB);
            this.panel8.Controls.Add(this.GDColdLb);
            this.panel8.Location = new System.Drawing.Point(228, 19);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(101, 49);
            this.panel8.TabIndex = 3;
            // 
            // GDTimeColdStartTB
            // 
            this.GDTimeColdStartTB.BackColor = System.Drawing.Color.White;
            this.GDTimeColdStartTB.Location = new System.Drawing.Point(8, 21);
            this.GDTimeColdStartTB.Name = "GDTimeColdStartTB";
            this.GDTimeColdStartTB.ReadOnly = true;
            this.GDTimeColdStartTB.Size = new System.Drawing.Size(82, 20);
            this.GDTimeColdStartTB.TabIndex = 6;
            this.GDTimeColdStartTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDTimeColdStartTB.Validated += new System.EventHandler(this.GDTimeColdStartTB_Validated);
            // 
            // GDColdLb
            // 
            this.GDColdLb.AutoSize = true;
            this.GDColdLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GDColdLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDColdLb.Location = new System.Drawing.Point(3, 3);
            this.GDColdLb.Name = "GDColdLb";
            this.GDColdLb.Size = new System.Drawing.Size(94, 13);
            this.GDColdLb.TabIndex = 0;
            this.GDColdLb.Text = "Time Cold Start";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.SkyBlue;
            this.panel7.Controls.Add(this.GDTimeDownTB);
            this.panel7.Controls.Add(this.GDTimeDownLb);
            this.panel7.Location = new System.Drawing.Point(112, 19);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(95, 49);
            this.panel7.TabIndex = 2;
            // 
            // GDTimeDownTB
            // 
            this.GDTimeDownTB.BackColor = System.Drawing.Color.White;
            this.GDTimeDownTB.Location = new System.Drawing.Point(9, 22);
            this.GDTimeDownTB.Name = "GDTimeDownTB";
            this.GDTimeDownTB.ReadOnly = true;
            this.GDTimeDownTB.Size = new System.Drawing.Size(75, 20);
            this.GDTimeDownTB.TabIndex = 5;
            this.GDTimeDownTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDTimeDownTB.Validated += new System.EventHandler(this.GDTimeDownTB_Validated);
            // 
            // GDTimeDownLb
            // 
            this.GDTimeDownLb.AutoSize = true;
            this.GDTimeDownLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GDTimeDownLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDTimeDownLb.Location = new System.Drawing.Point(6, 4);
            this.GDTimeDownLb.Name = "GDTimeDownLb";
            this.GDTimeDownLb.Size = new System.Drawing.Size(70, 13);
            this.GDTimeDownLb.TabIndex = 0;
            this.GDTimeDownLb.Text = "Time Down";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.SkyBlue;
            this.panel6.Controls.Add(this.GDTimeUpTB);
            this.panel6.Controls.Add(this.GDTUpLb);
            this.panel6.Location = new System.Drawing.Point(6, 19);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(95, 49);
            this.panel6.TabIndex = 1;
            // 
            // GDTimeUpTB
            // 
            this.GDTimeUpTB.BackColor = System.Drawing.Color.White;
            this.GDTimeUpTB.Location = new System.Drawing.Point(9, 22);
            this.GDTimeUpTB.Name = "GDTimeUpTB";
            this.GDTimeUpTB.ReadOnly = true;
            this.GDTimeUpTB.Size = new System.Drawing.Size(75, 20);
            this.GDTimeUpTB.TabIndex = 4;
            this.GDTimeUpTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDTimeUpTB.Validated += new System.EventHandler(this.GDTimeUpTB_Validated);
            // 
            // GDTUpLb
            // 
            this.GDTUpLb.AutoSize = true;
            this.GDTUpLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GDTUpLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDTUpLb.Location = new System.Drawing.Point(17, 4);
            this.GDTUpLb.Name = "GDTUpLb";
            this.GDTUpLb.Size = new System.Drawing.Size(54, 13);
            this.GDTUpLb.TabIndex = 0;
            this.GDTUpLb.Text = "Time Up";
            // 
            // Powergb
            // 
            this.Powergb.BackColor = System.Drawing.Color.Azure;
            this.Powergb.Controls.Add(this.panel5);
            this.Powergb.Controls.Add(this.panel4);
            this.Powergb.Controls.Add(this.panel3);
            this.Powergb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Powergb.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Powergb.Location = new System.Drawing.Point(234, 13);
            this.Powergb.Name = "Powergb";
            this.Powergb.Size = new System.Drawing.Size(219, 122);
            this.Powergb.TabIndex = 0;
            this.Powergb.TabStop = false;
            this.Powergb.Text = "POWER";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.SkyBlue;
            this.panel5.Controls.Add(this.GDPminTB);
            this.panel5.Controls.Add(this.GDPminLb);
            this.panel5.Location = new System.Drawing.Point(118, 65);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(95, 49);
            this.panel5.TabIndex = 2;
            // 
            // GDPminTB
            // 
            this.GDPminTB.BackColor = System.Drawing.Color.White;
            this.GDPminTB.Location = new System.Drawing.Point(9, 22);
            this.GDPminTB.Name = "GDPminTB";
            this.GDPminTB.ReadOnly = true;
            this.GDPminTB.Size = new System.Drawing.Size(75, 20);
            this.GDPminTB.TabIndex = 3;
            this.GDPminTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDPminTB.Validated += new System.EventHandler(this.GDPminTB_Validated);
            // 
            // GDPminLb
            // 
            this.GDPminLb.AutoSize = true;
            this.GDPminLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDPminLb.Location = new System.Drawing.Point(26, 4);
            this.GDPminLb.Name = "GDPminLb";
            this.GDPminLb.Size = new System.Drawing.Size(34, 13);
            this.GDPminLb.TabIndex = 0;
            this.GDPminLb.Text = "Pmin";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.SkyBlue;
            this.panel4.Controls.Add(this.GDPmaxTB);
            this.panel4.Controls.Add(this.GDPmaxLb);
            this.panel4.Location = new System.Drawing.Point(6, 65);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(95, 49);
            this.panel4.TabIndex = 1;
            // 
            // GDPmaxTB
            // 
            this.GDPmaxTB.BackColor = System.Drawing.Color.White;
            this.GDPmaxTB.Location = new System.Drawing.Point(9, 22);
            this.GDPmaxTB.Name = "GDPmaxTB";
            this.GDPmaxTB.ReadOnly = true;
            this.GDPmaxTB.Size = new System.Drawing.Size(75, 20);
            this.GDPmaxTB.TabIndex = 2;
            this.GDPmaxTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDPmaxTB.Validated += new System.EventHandler(this.GDPmaxTB_Validated);
            // 
            // GDPmaxLb
            // 
            this.GDPmaxLb.AutoSize = true;
            this.GDPmaxLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDPmaxLb.Location = new System.Drawing.Point(26, 4);
            this.GDPmaxLb.Name = "GDPmaxLb";
            this.GDPmaxLb.Size = new System.Drawing.Size(37, 13);
            this.GDPmaxLb.TabIndex = 0;
            this.GDPmaxLb.Text = "Pmax";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SkyBlue;
            this.panel3.Controls.Add(this.GDcapacityTB);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Location = new System.Drawing.Point(63, 10);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(95, 49);
            this.panel3.TabIndex = 0;
            // 
            // GDcapacityTB
            // 
            this.GDcapacityTB.BackColor = System.Drawing.Color.White;
            this.GDcapacityTB.Location = new System.Drawing.Point(9, 22);
            this.GDcapacityTB.Name = "GDcapacityTB";
            this.GDcapacityTB.ReadOnly = true;
            this.GDcapacityTB.Size = new System.Drawing.Size(75, 20);
            this.GDcapacityTB.TabIndex = 1;
            this.GDcapacityTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDcapacityTB.Validated += new System.EventHandler(this.GDcapacityTB_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.Window;
            this.label8.Location = new System.Drawing.Point(11, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "CAPACITY";
            // 
            // TempGV
            // 
            this.TempGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TempGV.Location = new System.Drawing.Point(10, 492);
            this.TempGV.Name = "TempGV";
            this.TempGV.Size = new System.Drawing.Size(92, 46);
            this.TempGV.TabIndex = 33;
            this.TempGV.Visible = false;
            // 
            // GDNewBtn
            // 
            this.GDNewBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.GDNewBtn.BackColor = System.Drawing.Color.SkyBlue;
            this.GDNewBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.GDNewBtn.Location = new System.Drawing.Point(528, 522);
            this.GDNewBtn.Name = "GDNewBtn";
            this.GDNewBtn.Size = new System.Drawing.Size(75, 23);
            this.GDNewBtn.TabIndex = 28;
            this.GDNewBtn.Text = "Add Unit";
            this.GDNewBtn.UseVisualStyleBackColor = false;
            this.GDNewBtn.Visible = false;
            this.GDNewBtn.Click += new System.EventHandler(this.GDNewBtn_Click);
            // 
            // GDDeleteBtn
            // 
            this.GDDeleteBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GDDeleteBtn.BackColor = System.Drawing.Color.SkyBlue;
            this.GDDeleteBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.GDDeleteBtn.Location = new System.Drawing.Point(643, 520);
            this.GDDeleteBtn.Name = "GDDeleteBtn";
            this.GDDeleteBtn.Size = new System.Drawing.Size(75, 23);
            this.GDDeleteBtn.TabIndex = 31;
            this.GDDeleteBtn.Text = "Delete Unit";
            this.GDDeleteBtn.UseVisualStyleBackColor = false;
            this.GDDeleteBtn.Visible = false;
            this.GDDeleteBtn.Click += new System.EventHandler(this.GDDeleteBtn_Click);
            // 
            // GDSteamGroup
            // 
            this.GDSteamGroup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.GDSteamGroup.BackColor = System.Drawing.Color.Azure;
            this.GDSteamGroup.Controls.Add(this.Grid230);
            this.GDSteamGroup.Controls.Add(this.PlantGV2);
            this.GDSteamGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GDSteamGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.GDSteamGroup.ForeColor = System.Drawing.Color.Black;
            this.GDSteamGroup.Location = new System.Drawing.Point(19, 263);
            this.GDSteamGroup.Name = "GDSteamGroup";
            this.GDSteamGroup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.GDSteamGroup.Size = new System.Drawing.Size(713, 237);
            this.GDSteamGroup.TabIndex = 30;
            this.GDSteamGroup.TabStop = false;
            this.GDSteamGroup.Text = "STEAM";
            this.GDSteamGroup.Visible = false;
            // 
            // Grid230
            // 
            this.Grid230.AutoGenerateColumns = false;
            this.Grid230.BackgroundColor = System.Drawing.Color.White;
            this.Grid230.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle40.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grid230.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle40;
            this.Grid230.ColumnHeadersHeight = 22;
            this.Grid230.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn32,
            this.CheckBox2});
            this.Grid230.DataSource = this.linesBindingSource;
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle41.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle41.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grid230.DefaultCellStyle = dataGridViewCellStyle41;
            this.Grid230.EnableHeadersVisualStyles = false;
            this.Grid230.Location = new System.Drawing.Point(17, 55);
            this.Grid230.Name = "Grid230";
            this.Grid230.RowHeadersVisible = false;
            this.Grid230.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grid230.Size = new System.Drawing.Size(678, 150);
            this.Grid230.TabIndex = 2;
            this.Grid230.Visible = false;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "LineCode";
            this.dataGridViewTextBoxColumn14.HeaderText = "Line";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Width = 60;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "FromBus";
            this.dataGridViewTextBoxColumn15.HeaderText = "From Bus";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Width = 75;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "ToBus";
            this.dataGridViewTextBoxColumn16.HeaderText = "To Bus";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Width = 75;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "LineLength";
            this.dataGridViewTextBoxColumn27.HeaderText = "Line Length";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.Width = 80;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "Owner_GencoFrom";
            this.dataGridViewTextBoxColumn28.HeaderText = "Genco Start Owner";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.Width = 125;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "Owner_GencoTo";
            this.dataGridViewTextBoxColumn29.HeaderText = "Genco End Owner";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.Width = 125;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "RateA";
            this.dataGridViewTextBoxColumn30.HeaderText = "Capacity";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.Width = 60;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.HeaderText = "Status";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            this.dataGridViewTextBoxColumn32.Width = 50;
            // 
            // CheckBox2
            // 
            this.CheckBox2.FalseValue = "0";
            this.CheckBox2.HeaderText = "";
            this.CheckBox2.Name = "CheckBox2";
            this.CheckBox2.ReadOnly = true;
            this.CheckBox2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CheckBox2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.CheckBox2.TrueValue = "1";
            this.CheckBox2.Width = 25;
            // 
            // PlantGV2
            // 
            this.PlantGV2.AutoGenerateColumns = false;
            this.PlantGV2.BackgroundColor = System.Drawing.Color.White;
            this.PlantGV2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle42.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle42.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PlantGV2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle42;
            this.PlantGV2.ColumnHeadersHeight = 22;
            this.PlantGV2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.unitCodeDataGridViewTextBoxColumn1,
            this.packageCodeDataGridViewTextBoxColumn1,
            this.unitTypeDataGridViewTextBoxColumn1,
            this.capacityDataGridViewTextBoxColumn1,
            this.pMinDataGridViewTextBoxColumn1,
            this.pMaxDataGridViewTextBoxColumn1,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column31});
            this.PlantGV2.DataSource = this.unitsDataMainBindingSource;
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle43.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle43.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PlantGV2.DefaultCellStyle = dataGridViewCellStyle43;
            this.PlantGV2.EnableHeadersVisualStyles = false;
            this.PlantGV2.Location = new System.Drawing.Point(17, 38);
            this.PlantGV2.Name = "PlantGV2";
            this.PlantGV2.RowHeadersVisible = false;
            this.PlantGV2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PlantGV2.Size = new System.Drawing.Size(678, 150);
            this.PlantGV2.TabIndex = 3;
            this.PlantGV2.Visible = false;
            // 
            // unitCodeDataGridViewTextBoxColumn1
            // 
            this.unitCodeDataGridViewTextBoxColumn1.DataPropertyName = "UnitCode";
            this.unitCodeDataGridViewTextBoxColumn1.HeaderText = "Unit";
            this.unitCodeDataGridViewTextBoxColumn1.Name = "unitCodeDataGridViewTextBoxColumn1";
            this.unitCodeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.unitCodeDataGridViewTextBoxColumn1.Width = 70;
            // 
            // packageCodeDataGridViewTextBoxColumn1
            // 
            this.packageCodeDataGridViewTextBoxColumn1.DataPropertyName = "PackageCode";
            this.packageCodeDataGridViewTextBoxColumn1.HeaderText = "Package";
            this.packageCodeDataGridViewTextBoxColumn1.Name = "packageCodeDataGridViewTextBoxColumn1";
            this.packageCodeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.packageCodeDataGridViewTextBoxColumn1.Width = 65;
            // 
            // unitTypeDataGridViewTextBoxColumn1
            // 
            this.unitTypeDataGridViewTextBoxColumn1.DataPropertyName = "UnitType";
            this.unitTypeDataGridViewTextBoxColumn1.HeaderText = "Type";
            this.unitTypeDataGridViewTextBoxColumn1.Name = "unitTypeDataGridViewTextBoxColumn1";
            this.unitTypeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.unitTypeDataGridViewTextBoxColumn1.Width = 60;
            // 
            // capacityDataGridViewTextBoxColumn1
            // 
            this.capacityDataGridViewTextBoxColumn1.DataPropertyName = "Capacity";
            this.capacityDataGridViewTextBoxColumn1.HeaderText = "Capacity";
            this.capacityDataGridViewTextBoxColumn1.Name = "capacityDataGridViewTextBoxColumn1";
            this.capacityDataGridViewTextBoxColumn1.ReadOnly = true;
            this.capacityDataGridViewTextBoxColumn1.Width = 75;
            // 
            // pMinDataGridViewTextBoxColumn1
            // 
            this.pMinDataGridViewTextBoxColumn1.DataPropertyName = "PMin";
            this.pMinDataGridViewTextBoxColumn1.HeaderText = "PMin";
            this.pMinDataGridViewTextBoxColumn1.Name = "pMinDataGridViewTextBoxColumn1";
            this.pMinDataGridViewTextBoxColumn1.ReadOnly = true;
            this.pMinDataGridViewTextBoxColumn1.Width = 55;
            // 
            // pMaxDataGridViewTextBoxColumn1
            // 
            this.pMaxDataGridViewTextBoxColumn1.DataPropertyName = "PMax";
            this.pMaxDataGridViewTextBoxColumn1.HeaderText = "PMax";
            this.pMaxDataGridViewTextBoxColumn1.Name = "pMaxDataGridViewTextBoxColumn1";
            this.pMaxDataGridViewTextBoxColumn1.ReadOnly = true;
            this.pMaxDataGridViewTextBoxColumn1.Width = 55;
            // 
            // Column14
            // 
            this.Column14.HeaderText = "Fuel";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 55;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "Status";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 65;
            // 
            // Column16
            // 
            this.Column16.HeaderText = "Maintenance";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Width = 75;
            // 
            // Column17
            // 
            this.Column17.HeaderText = "Outservice";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 75;
            // 
            // Column31
            // 
            this.Column31.FalseValue = "0";
            this.Column31.HeaderText = "";
            this.Column31.Name = "Column31";
            this.Column31.ReadOnly = true;
            this.Column31.TrueValue = "1";
            this.Column31.Width = 25;
            // 
            // GDGasGroup
            // 
            this.GDGasGroup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.GDGasGroup.BackColor = System.Drawing.Color.Azure;
            this.GDGasGroup.Controls.Add(this.Grid400);
            this.GDGasGroup.Controls.Add(this.PlantGV1);
            this.GDGasGroup.Controls.Add(this.PlantGV11);
            this.GDGasGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GDGasGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.GDGasGroup.ForeColor = System.Drawing.Color.Black;
            this.GDGasGroup.Location = new System.Drawing.Point(19, 30);
            this.GDGasGroup.Name = "GDGasGroup";
            this.GDGasGroup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.GDGasGroup.Size = new System.Drawing.Size(713, 204);
            this.GDGasGroup.TabIndex = 29;
            this.GDGasGroup.TabStop = false;
            this.GDGasGroup.Text = "GAS";
            this.GDGasGroup.Visible = false;
            // 
            // Grid400
            // 
            this.Grid400.AutoGenerateColumns = false;
            this.Grid400.BackgroundColor = System.Drawing.Color.White;
            this.Grid400.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle44.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle44.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle44.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle44.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle44.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grid400.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle44;
            this.Grid400.ColumnHeadersHeight = 22;
            this.Grid400.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.lineCodeDataGridViewTextBoxColumn,
            this.fromBusDataGridViewTextBoxColumn,
            this.toBusDataGridViewTextBoxColumn,
            this.lineLengthDataGridViewTextBoxColumn,
            this.ownerGencoFromDataGridViewTextBoxColumn,
            this.ownerGencoToDataGridViewTextBoxColumn,
            this.Column13,
            this.dataGridViewTextBoxColumn31,
            this.CheckBox1});
            this.Grid400.DataSource = this.linesBindingSource1;
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle45.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle45.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle45.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle45.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle45.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grid400.DefaultCellStyle = dataGridViewCellStyle45;
            this.Grid400.EnableHeadersVisualStyles = false;
            this.Grid400.Location = new System.Drawing.Point(17, 47);
            this.Grid400.Name = "Grid400";
            this.Grid400.RowHeadersVisible = false;
            this.Grid400.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grid400.Size = new System.Drawing.Size(678, 150);
            this.Grid400.TabIndex = 1;
            this.Grid400.Visible = false;
            // 
            // lineCodeDataGridViewTextBoxColumn
            // 
            this.lineCodeDataGridViewTextBoxColumn.DataPropertyName = "LineCode";
            this.lineCodeDataGridViewTextBoxColumn.HeaderText = "Line";
            this.lineCodeDataGridViewTextBoxColumn.Name = "lineCodeDataGridViewTextBoxColumn";
            this.lineCodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.lineCodeDataGridViewTextBoxColumn.Width = 60;
            // 
            // fromBusDataGridViewTextBoxColumn
            // 
            this.fromBusDataGridViewTextBoxColumn.DataPropertyName = "FromBus";
            this.fromBusDataGridViewTextBoxColumn.HeaderText = "From Bus";
            this.fromBusDataGridViewTextBoxColumn.Name = "fromBusDataGridViewTextBoxColumn";
            this.fromBusDataGridViewTextBoxColumn.ReadOnly = true;
            this.fromBusDataGridViewTextBoxColumn.Width = 75;
            // 
            // toBusDataGridViewTextBoxColumn
            // 
            this.toBusDataGridViewTextBoxColumn.DataPropertyName = "ToBus";
            this.toBusDataGridViewTextBoxColumn.HeaderText = "To Bus";
            this.toBusDataGridViewTextBoxColumn.Name = "toBusDataGridViewTextBoxColumn";
            this.toBusDataGridViewTextBoxColumn.ReadOnly = true;
            this.toBusDataGridViewTextBoxColumn.Width = 75;
            // 
            // lineLengthDataGridViewTextBoxColumn
            // 
            this.lineLengthDataGridViewTextBoxColumn.DataPropertyName = "LineLength";
            this.lineLengthDataGridViewTextBoxColumn.HeaderText = "Line Length";
            this.lineLengthDataGridViewTextBoxColumn.Name = "lineLengthDataGridViewTextBoxColumn";
            this.lineLengthDataGridViewTextBoxColumn.ReadOnly = true;
            this.lineLengthDataGridViewTextBoxColumn.Width = 80;
            // 
            // ownerGencoFromDataGridViewTextBoxColumn
            // 
            this.ownerGencoFromDataGridViewTextBoxColumn.DataPropertyName = "Owner_GencoFrom";
            this.ownerGencoFromDataGridViewTextBoxColumn.HeaderText = "Genco Start Owner";
            this.ownerGencoFromDataGridViewTextBoxColumn.Name = "ownerGencoFromDataGridViewTextBoxColumn";
            this.ownerGencoFromDataGridViewTextBoxColumn.ReadOnly = true;
            this.ownerGencoFromDataGridViewTextBoxColumn.Width = 125;
            // 
            // ownerGencoToDataGridViewTextBoxColumn
            // 
            this.ownerGencoToDataGridViewTextBoxColumn.DataPropertyName = "Owner_GencoTo";
            this.ownerGencoToDataGridViewTextBoxColumn.HeaderText = "Genco End Owner";
            this.ownerGencoToDataGridViewTextBoxColumn.Name = "ownerGencoToDataGridViewTextBoxColumn";
            this.ownerGencoToDataGridViewTextBoxColumn.ReadOnly = true;
            this.ownerGencoToDataGridViewTextBoxColumn.Width = 125;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "RateA";
            this.Column13.HeaderText = "Capacity";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 65;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.HeaderText = "Status";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.Width = 50;
            // 
            // CheckBox1
            // 
            this.CheckBox1.DataPropertyName = "LineNumber";
            this.CheckBox1.FalseValue = "0";
            this.CheckBox1.HeaderText = "";
            this.CheckBox1.Name = "CheckBox1";
            this.CheckBox1.ReadOnly = true;
            this.CheckBox1.TrueValue = "1";
            this.CheckBox1.Width = 20;
            // 
            // PlantGV1
            // 
            this.PlantGV1.AllowUserToOrderColumns = true;
            this.PlantGV1.AutoGenerateColumns = false;
            this.PlantGV1.BackgroundColor = System.Drawing.Color.White;
            this.PlantGV1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle46.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle46.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle46.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle46.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle46.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PlantGV1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle46;
            this.PlantGV1.ColumnHeadersHeight = 22;
            this.PlantGV1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.CheckBox});
            this.PlantGV1.DataSource = this.unitsDataMainBindingSource;
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle48.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle48.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle48.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle48.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle48.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PlantGV1.DefaultCellStyle = dataGridViewCellStyle48;
            this.PlantGV1.EnableHeadersVisualStyles = false;
            this.PlantGV1.Location = new System.Drawing.Point(17, 27);
            this.PlantGV1.Name = "PlantGV1";
            this.PlantGV1.RowHeadersVisible = false;
            this.PlantGV1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PlantGV1.Size = new System.Drawing.Size(678, 150);
            this.PlantGV1.TabIndex = 4;
            this.PlantGV1.Visible = false;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "UnitCode";
            this.dataGridViewTextBoxColumn17.HeaderText = "Unit";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Width = 70;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "PackageCode";
            this.dataGridViewTextBoxColumn18.HeaderText = "Package";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Width = 65;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "UnitType";
            this.dataGridViewTextBoxColumn19.HeaderText = "Type";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Width = 60;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "Capacity";
            this.dataGridViewTextBoxColumn20.HeaderText = "Capacity";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Width = 75;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "PMin";
            this.dataGridViewTextBoxColumn22.HeaderText = "PMin";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.Width = 55;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "PMax";
            this.dataGridViewTextBoxColumn21.HeaderText = "PMax";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Width = 55;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.HeaderText = "Fuel";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Width = 55;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.HeaderText = "Status";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Width = 65;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.HeaderText = "Maintenance";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.Width = 75;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.HeaderText = "Outservice";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Width = 75;
            // 
            // CheckBox
            // 
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle47.Font = new System.Drawing.Font("Miriam Fixed", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle47.NullValue = false;
            dataGridViewCellStyle47.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle47.SelectionForeColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle47.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.CheckBox.DefaultCellStyle = dataGridViewCellStyle47;
            this.CheckBox.FalseValue = "0";
            this.CheckBox.HeaderText = "";
            this.CheckBox.Name = "CheckBox";
            this.CheckBox.ReadOnly = true;
            this.CheckBox.TrueValue = "1";
            this.CheckBox.Width = 25;
            // 
            // PlantGV11
            // 
            this.PlantGV11.AutoGenerateColumns = false;
            this.PlantGV11.BackgroundColor = System.Drawing.Color.White;
            this.PlantGV11.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle49.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle49.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle49.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle49.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle49.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PlantGV11.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle49;
            this.PlantGV11.ColumnHeadersHeight = 22;
            this.PlantGV11.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.unitCodeDataGridViewTextBoxColumn,
            this.packageCodeDataGridViewTextBoxColumn,
            this.unitTypeDataGridViewTextBoxColumn,
            this.capacityDataGridViewTextBoxColumn,
            this.pMaxDataGridViewTextBoxColumn,
            this.pMinDataGridViewTextBoxColumn,
            this.Fuel,
            this.Status,
            this.Maintenance,
            this.Outservice});
            this.PlantGV11.DataMember = "UnitsDataMain";
            this.PlantGV11.DataSource = this.powerPalntDBDataSet4BindingSource;
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle50.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle50.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle50.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle50.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle50.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PlantGV11.DefaultCellStyle = dataGridViewCellStyle50;
            this.PlantGV11.EnableHeadersVisualStyles = false;
            this.PlantGV11.Location = new System.Drawing.Point(15, 152);
            this.PlantGV11.Name = "PlantGV11";
            this.PlantGV11.RowHeadersVisible = false;
            this.PlantGV11.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PlantGV11.Size = new System.Drawing.Size(678, 150);
            this.PlantGV11.TabIndex = 0;
            this.PlantGV11.Visible = false;
            // 
            // unitCodeDataGridViewTextBoxColumn
            // 
            this.unitCodeDataGridViewTextBoxColumn.DataPropertyName = "UnitCode";
            this.unitCodeDataGridViewTextBoxColumn.HeaderText = "Unit";
            this.unitCodeDataGridViewTextBoxColumn.Name = "unitCodeDataGridViewTextBoxColumn";
            this.unitCodeDataGridViewTextBoxColumn.Width = 60;
            // 
            // packageCodeDataGridViewTextBoxColumn
            // 
            this.packageCodeDataGridViewTextBoxColumn.DataPropertyName = "PackageCode";
            this.packageCodeDataGridViewTextBoxColumn.HeaderText = "Package";
            this.packageCodeDataGridViewTextBoxColumn.Name = "packageCodeDataGridViewTextBoxColumn";
            this.packageCodeDataGridViewTextBoxColumn.Width = 75;
            // 
            // unitTypeDataGridViewTextBoxColumn
            // 
            this.unitTypeDataGridViewTextBoxColumn.DataPropertyName = "UnitType";
            this.unitTypeDataGridViewTextBoxColumn.HeaderText = "Type";
            this.unitTypeDataGridViewTextBoxColumn.Name = "unitTypeDataGridViewTextBoxColumn";
            this.unitTypeDataGridViewTextBoxColumn.Width = 60;
            // 
            // capacityDataGridViewTextBoxColumn
            // 
            this.capacityDataGridViewTextBoxColumn.DataPropertyName = "Capacity";
            this.capacityDataGridViewTextBoxColumn.HeaderText = "Capacity";
            this.capacityDataGridViewTextBoxColumn.Name = "capacityDataGridViewTextBoxColumn";
            this.capacityDataGridViewTextBoxColumn.Width = 75;
            // 
            // pMaxDataGridViewTextBoxColumn
            // 
            this.pMaxDataGridViewTextBoxColumn.DataPropertyName = "PMax";
            this.pMaxDataGridViewTextBoxColumn.HeaderText = "PMax";
            this.pMaxDataGridViewTextBoxColumn.Name = "pMaxDataGridViewTextBoxColumn";
            this.pMaxDataGridViewTextBoxColumn.Width = 60;
            // 
            // pMinDataGridViewTextBoxColumn
            // 
            this.pMinDataGridViewTextBoxColumn.DataPropertyName = "PMin";
            this.pMinDataGridViewTextBoxColumn.HeaderText = "PMin";
            this.pMinDataGridViewTextBoxColumn.Name = "pMinDataGridViewTextBoxColumn";
            this.pMinDataGridViewTextBoxColumn.Width = 60;
            // 
            // Fuel
            // 
            this.Fuel.DataPropertyName = "GencoCode";
            this.Fuel.HeaderText = "Fuel";
            this.Fuel.Name = "Fuel";
            this.Fuel.Width = 60;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "GencoCode";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.Width = 75;
            // 
            // Maintenance
            // 
            this.Maintenance.DataPropertyName = "GencoCode";
            this.Maintenance.HeaderText = "Maintenance";
            this.Maintenance.Name = "Maintenance";
            this.Maintenance.Width = 75;
            // 
            // Outservice
            // 
            this.Outservice.DataPropertyName = "GencoCode";
            this.Outservice.HeaderText = "Outservice";
            this.Outservice.Name = "Outservice";
            this.Outservice.Width = 75;
            // 
            // GDHeaderGB
            // 
            this.GDHeaderGB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.GDHeaderGB.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.GDHeaderGB.BackColor = System.Drawing.Color.Azure;
            this.GDHeaderGB.Controls.Add(this.GDPlantLb);
            this.GDHeaderGB.Controls.Add(this.GDHeaderPanel);
            this.GDHeaderGB.Controls.Add(this.L1);
            this.GDHeaderGB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GDHeaderGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.GDHeaderGB.ForeColor = System.Drawing.Color.Black;
            this.GDHeaderGB.Location = new System.Drawing.Point(15, 11);
            this.GDHeaderGB.Name = "GDHeaderGB";
            this.GDHeaderGB.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.GDHeaderGB.Size = new System.Drawing.Size(720, 53);
            this.GDHeaderGB.TabIndex = 19;
            this.GDHeaderGB.TabStop = false;
            this.GDHeaderGB.Visible = false;
            // 
            // GDPlantLb
            // 
            this.GDPlantLb.AutoSize = true;
            this.GDPlantLb.Location = new System.Drawing.Point(62, 23);
            this.GDPlantLb.Name = "GDPlantLb";
            this.GDPlantLb.Size = new System.Drawing.Size(0, 13);
            this.GDPlantLb.TabIndex = 9;
            // 
            // GDHeaderPanel
            // 
            this.GDHeaderPanel.Controls.Add(this.GDTypeLb);
            this.GDHeaderPanel.Controls.Add(this.GDUnitLb);
            this.GDHeaderPanel.Controls.Add(this.GDPackLb);
            this.GDHeaderPanel.Controls.Add(this.L4);
            this.GDHeaderPanel.Controls.Add(this.L3);
            this.GDHeaderPanel.Controls.Add(this.L2);
            this.GDHeaderPanel.Location = new System.Drawing.Point(144, 12);
            this.GDHeaderPanel.Name = "GDHeaderPanel";
            this.GDHeaderPanel.Size = new System.Drawing.Size(551, 35);
            this.GDHeaderPanel.TabIndex = 8;
            this.GDHeaderPanel.Visible = false;
            // 
            // GDTypeLb
            // 
            this.GDTypeLb.AutoSize = true;
            this.GDTypeLb.Location = new System.Drawing.Point(445, 11);
            this.GDTypeLb.Name = "GDTypeLb";
            this.GDTypeLb.Size = new System.Drawing.Size(0, 13);
            this.GDTypeLb.TabIndex = 13;
            // 
            // GDUnitLb
            // 
            this.GDUnitLb.AutoSize = true;
            this.GDUnitLb.Location = new System.Drawing.Point(281, 11);
            this.GDUnitLb.Name = "GDUnitLb";
            this.GDUnitLb.Size = new System.Drawing.Size(0, 13);
            this.GDUnitLb.TabIndex = 12;
            // 
            // GDPackLb
            // 
            this.GDPackLb.AutoSize = true;
            this.GDPackLb.Location = new System.Drawing.Point(118, 11);
            this.GDPackLb.Name = "GDPackLb";
            this.GDPackLb.Size = new System.Drawing.Size(0, 13);
            this.GDPackLb.TabIndex = 11;
            // 
            // L4
            // 
            this.L4.AutoSize = true;
            this.L4.BackColor = System.Drawing.Color.Transparent;
            this.L4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L4.Location = new System.Drawing.Point(402, 11);
            this.L4.Name = "L4";
            this.L4.Size = new System.Drawing.Size(47, 13);
            this.L4.TabIndex = 10;
            this.L4.Text = "Type : ";
            // 
            // L3
            // 
            this.L3.AutoSize = true;
            this.L3.BackColor = System.Drawing.Color.Transparent;
            this.L3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L3.Location = new System.Drawing.Point(234, 11);
            this.L3.Name = "L3";
            this.L3.Size = new System.Drawing.Size(42, 13);
            this.L3.TabIndex = 9;
            this.L3.Text = "Unit : ";
            // 
            // L2
            // 
            this.L2.AutoSize = true;
            this.L2.BackColor = System.Drawing.Color.Transparent;
            this.L2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L2.Location = new System.Drawing.Point(45, 11);
            this.L2.Name = "L2";
            this.L2.Size = new System.Drawing.Size(69, 13);
            this.L2.TabIndex = 8;
            this.L2.Text = "Package : ";
            // 
            // L1
            // 
            this.L1.AutoSize = true;
            this.L1.BackColor = System.Drawing.Color.Transparent;
            this.L1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L1.Location = new System.Drawing.Point(10, 23);
            this.L1.Name = "L1";
            this.L1.Size = new System.Drawing.Size(48, 13);
            this.L1.TabIndex = 7;
            this.L1.Text = "Plant : ";
            this.L1.Visible = false;
            // 
            // MainTabs
            // 
            this.MainTabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.MainTabs.Controls.Add(this.GeneralData);
            this.MainTabs.Controls.Add(this.OperationalData);
            this.MainTabs.Controls.Add(this.MarketResults);
            this.MainTabs.Controls.Add(this.BidData);
            this.MainTabs.Controls.Add(this.FinancialReport);
            this.MainTabs.Controls.Add(this.BiddingStrategy);
            this.MainTabs.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.MainTabs.ItemSize = new System.Drawing.Size(125, 25);
            this.MainTabs.Location = new System.Drawing.Point(251, 27);
            this.MainTabs.Name = "MainTabs";
            this.MainTabs.Padding = new System.Drawing.Point(6, 6);
            this.MainTabs.SelectedIndex = 0;
            this.MainTabs.Size = new System.Drawing.Size(765, 658);
            this.MainTabs.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.MainTabs.TabIndex = 10;
            this.MainTabs.SelectedIndexChanged += new System.EventHandler(this.MainTabs_SelectedIndexChanged);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.BackgroundImage = global::PowerPlantProject.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1028, 704);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.MainTabs);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimumSize = new System.Drawing.Size(1022, 736);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form2_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form2_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.linesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPalntDBDataSet4BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPalntDBDataSet4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unitsDataMainBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linesBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailFRM002BindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.BiddingStrategy.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.panel80.ResumeLayout(false);
            this.panel80.PerformLayout();
            this.panel81.ResumeLayout(false);
            this.panel81.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel75.ResumeLayout(false);
            this.panel75.PerformLayout();
            this.panel77.ResumeLayout(false);
            this.panel77.PerformLayout();
            this.panel78.ResumeLayout(false);
            this.panel78.PerformLayout();
            this.panel79.ResumeLayout(false);
            this.panel79.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grBoxCustomize.ResumeLayout(false);
            this.menuStrip7.ResumeLayout(false);
            this.menuStrip7.PerformLayout();
            this.menuStrip8.ResumeLayout(false);
            this.menuStrip8.PerformLayout();
            this.menuStrip6.ResumeLayout(false);
            this.menuStrip6.PerformLayout();
            this.menuStrip5.ResumeLayout(false);
            this.menuStrip5.PerformLayout();
            this.menuStrip3.ResumeLayout(false);
            this.menuStrip3.PerformLayout();
            this.menuStrip4.ResumeLayout(false);
            this.menuStrip4.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.FinancialReport.ResumeLayout(false);
            this.FRMainPanel.ResumeLayout(false);
            this.FRUnitPanel.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.panel65.ResumeLayout(false);
            this.panel65.PerformLayout();
            this.panel68.ResumeLayout(false);
            this.panel68.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.panel66.ResumeLayout(false);
            this.panel66.PerformLayout();
            this.panel67.ResumeLayout(false);
            this.panel67.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.panel62.ResumeLayout(false);
            this.panel62.PerformLayout();
            this.panel63.ResumeLayout(false);
            this.panel63.PerformLayout();
            this.panel64.ResumeLayout(false);
            this.panel64.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.panel59.ResumeLayout(false);
            this.panel59.PerformLayout();
            this.panel60.ResumeLayout(false);
            this.panel60.PerformLayout();
            this.panel61.ResumeLayout(false);
            this.panel61.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.panel53.ResumeLayout(false);
            this.panel53.PerformLayout();
            this.panel57.ResumeLayout(false);
            this.panel57.PerformLayout();
            this.panel58.ResumeLayout(false);
            this.panel58.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.panel76.ResumeLayout(false);
            this.panel76.PerformLayout();
            this.panel69.ResumeLayout(false);
            this.panel69.PerformLayout();
            this.panel70.ResumeLayout(false);
            this.panel70.PerformLayout();
            this.panel71.ResumeLayout(false);
            this.panel71.PerformLayout();
            this.panel73.ResumeLayout(false);
            this.panel73.PerformLayout();
            this.panel74.ResumeLayout(false);
            this.panel74.PerformLayout();
            this.FRPlantPanel.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.panel54.ResumeLayout(false);
            this.panel54.PerformLayout();
            this.panel55.ResumeLayout(false);
            this.panel55.PerformLayout();
            this.panel56.ResumeLayout(false);
            this.panel56.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.panel47.ResumeLayout(false);
            this.panel47.PerformLayout();
            this.panel48.ResumeLayout(false);
            this.panel48.PerformLayout();
            this.panel49.ResumeLayout(false);
            this.panel49.PerformLayout();
            this.panel50.ResumeLayout(false);
            this.panel50.PerformLayout();
            this.panel51.ResumeLayout(false);
            this.panel51.PerformLayout();
            this.panel52.ResumeLayout(false);
            this.panel52.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.panel41.ResumeLayout(false);
            this.panel41.PerformLayout();
            this.panel42.ResumeLayout(false);
            this.panel42.PerformLayout();
            this.panel43.ResumeLayout(false);
            this.panel43.PerformLayout();
            this.panel44.ResumeLayout(false);
            this.panel44.PerformLayout();
            this.panel45.ResumeLayout(false);
            this.panel45.PerformLayout();
            this.panel46.ResumeLayout(false);
            this.panel46.PerformLayout();
            this.FRHeaderGb.ResumeLayout(false);
            this.FRHeaderGb.PerformLayout();
            this.FRHeaderPanel.ResumeLayout(false);
            this.FRHeaderPanel.PerformLayout();
            this.BidData.ResumeLayout(false);
            this.BDMainPanel.ResumeLayout(false);
            this.BDCur2.ResumeLayout(false);
            this.BDCur2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BDCurGrid)).EndInit();
            this.BDCur1.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panel38.PerformLayout();
            this.panel39.ResumeLayout(false);
            this.panel39.PerformLayout();
            this.panel40.ResumeLayout(false);
            this.panel40.PerformLayout();
            this.BDHeaderGb.ResumeLayout(false);
            this.BDHeaderGb.PerformLayout();
            this.BDHeaderPanel.ResumeLayout(false);
            this.BDHeaderPanel.PerformLayout();
            this.MarketResults.ResumeLayout(false);
            this.MRMainPanel.ResumeLayout(false);
            this.MRPlantCurGb.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            this.MRGB.ResumeLayout(false);
            this.MRGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MRCurGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MRCurGrid1)).EndInit();
            this.MRUnitCurGb.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            this.panel37.PerformLayout();
            this.panel36.ResumeLayout(false);
            this.panel36.PerformLayout();
            this.panel72.ResumeLayout(false);
            this.panel72.PerformLayout();
            this.MRHeaderGB.ResumeLayout(false);
            this.MRHeaderGB.PerformLayout();
            this.MRHeaderPanel.ResumeLayout(false);
            this.MRHeaderPanel.PerformLayout();
            this.OperationalData.ResumeLayout(false);
            this.ODHeaderGB.ResumeLayout(false);
            this.ODHeaderGB.PerformLayout();
            this.ODHeaderPanel.ResumeLayout(false);
            this.ODHeaderPanel.PerformLayout();
            this.ODMainPanel.ResumeLayout(false);
            this.ODUnitPanel.ResumeLayout(false);
            this.ODUnitMainGB.ResumeLayout(false);
            this.ODUnitMainGB.PerformLayout();
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            this.ODUnitPowerGB.ResumeLayout(false);
            this.ODUnitPowerGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODUnitPowerGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ODUnitPowerGrid1)).EndInit();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.ODUnitServiceGB.ResumeLayout(false);
            this.ODUnitServiceGB.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.ODUnitFuelGB.ResumeLayout(false);
            this.ODUnitFuelGB.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.ODPlantPanel.ResumeLayout(false);
            this.ODPowerMinGB.ResumeLayout(false);
            this.ODPowerMinGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODPowerGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ODPowerGrid1)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.ODFuelGB.ResumeLayout(false);
            this.ODFuelGB.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.ODServiceGB.ResumeLayout(false);
            this.ODServiceGB.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.GeneralData.ResumeLayout(false);
            this.GDMainPanel.ResumeLayout(false);
            this.GDUnitPanel.ResumeLayout(false);
            this.Maintenancegb.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.Currentgb.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.Anonygb.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.Powergb.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TempGV)).EndInit();
            this.GDSteamGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grid230)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlantGV2)).EndInit();
            this.GDGasGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grid400)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlantGV1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlantGV11)).EndInit();
            this.GDHeaderGB.ResumeLayout(false);
            this.GDHeaderGB.PerformLayout();
            this.GDHeaderPanel.ResumeLayout(false);
            this.GDHeaderPanel.PerformLayout();
            this.MainTabs.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TreeView treeView2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private PowerPalntDBDataSet4 powerPalntDBDataSet4;
        private System.Windows.Forms.BindingSource powerPalntDBDataSet4BindingSource;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.BindingSource linesBindingSource;
        private PowerPlantProject.PowerPalntDBDataSet4TableAdapters.LinesTableAdapter linesTableAdapter;
        private System.Windows.Forms.BindingSource linesBindingSource1;
        private System.Windows.Forms.BindingSource unitsDataMainBindingSource;
        private PowerPlantProject.PowerPalntDBDataSet4TableAdapters.UnitsDataMainTableAdapter unitsDataMainTableAdapter;
        private System.Windows.Forms.BindingSource detailFRM002BindingSource;
        private PowerPlantProject.PowerPalntDBDataSet4TableAdapters.DetailFRM002TableAdapter detailFRM002TableAdapter;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.TabControl MainTabs;
        private System.Windows.Forms.TabPage GeneralData;
        private System.Windows.Forms.GroupBox GDHeaderGB;
        private System.Windows.Forms.Label GDPlantLb;
        private System.Windows.Forms.Panel GDHeaderPanel;
        private System.Windows.Forms.Label GDTypeLb;
        private System.Windows.Forms.Label GDUnitLb;
        private System.Windows.Forms.Label GDPackLb;
        private System.Windows.Forms.Label L4;
        private System.Windows.Forms.Label L3;
        private System.Windows.Forms.Label L2;
        private System.Windows.Forms.Label L1;
        private System.Windows.Forms.TabPage OperationalData;
        private System.Windows.Forms.GroupBox ODHeaderGB;
        private System.Windows.Forms.Label ODPlantLb;
        private System.Windows.Forms.Panel ODHeaderPanel;
        private System.Windows.Forms.Label ODTypeLb;
        private System.Windows.Forms.Label ODUnitLb;
        private System.Windows.Forms.Label ODPackLb;
        private System.Windows.Forms.Label L8;
        private System.Windows.Forms.Label L7;
        private System.Windows.Forms.Label L6;
        private System.Windows.Forms.Label L5;
        private System.Windows.Forms.TabPage MarketResults;
        private System.Windows.Forms.GroupBox MRHeaderGB;
        private System.Windows.Forms.Label MRPlantLb;
        private System.Windows.Forms.Panel MRHeaderPanel;
        private System.Windows.Forms.Label MRTypeLb;
        private System.Windows.Forms.Label MRUnitLb;
        private System.Windows.Forms.Label MRPackLb;
        private System.Windows.Forms.Label L12;
        private System.Windows.Forms.Label L11;
        private System.Windows.Forms.Label L10;
        private System.Windows.Forms.Label L9;
        private System.Windows.Forms.TabPage BidData;
        private System.Windows.Forms.GroupBox BDHeaderGb;
        private System.Windows.Forms.Label BDPlantLb;
        private System.Windows.Forms.Panel BDHeaderPanel;
        private System.Windows.Forms.Label BDTypeLb;
        private System.Windows.Forms.Label BDUnitLb;
        private System.Windows.Forms.Label BDPackLb;
        private System.Windows.Forms.Label L16;
        private System.Windows.Forms.Label L15;
        private System.Windows.Forms.Label L14;
        private System.Windows.Forms.Label L13;
        private System.Windows.Forms.TabPage FinancialReport;
        private System.Windows.Forms.GroupBox FRHeaderGb;
        private System.Windows.Forms.Label FRPlantLb;
        private System.Windows.Forms.Panel FRHeaderPanel;
        private System.Windows.Forms.Label FRTypeLb;
        private System.Windows.Forms.Label FRUnitLb;
        private System.Windows.Forms.Label FRPackLb;
        private System.Windows.Forms.Label L20;
        private System.Windows.Forms.Label L19;
        private System.Windows.Forms.Label L18;
        private System.Windows.Forms.Label L17;
        private System.Windows.Forms.TabPage BiddingStrategy;
        private System.Windows.Forms.ToolStripMenuItem marketToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m002ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m005ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem averagePriceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadForecastingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem capacityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annualFactorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem weekFactorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoPathToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.Panel MRMainPanel;
        private System.Windows.Forms.Button MRPlotBtn;
        private System.Windows.Forms.GroupBox MRPlantCurGb;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.TextBox MRPlantPowerTb;
        private System.Windows.Forms.Label MRLabel2;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.TextBox MRPlantOnUnitTb;
        private System.Windows.Forms.Label MRLabel1;
        private System.Windows.Forms.GroupBox MRGB;
        private FarsiLibrary.Win.Controls.FADatePicker MRCal;
        private System.Windows.Forms.DataGridView MRCurGrid2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridView MRCurGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox MRUnitCurGb;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.TextBox MRUnitMaxBidTb;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.TextBox MRUnitPowerTb;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.TextBox MRUnitStateTb;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel GDMainPanel;
        private System.Windows.Forms.DataGridView TempGV;
        private System.Windows.Forms.Button GDNewBtn;
        private System.Windows.Forms.Button GDDeleteBtn;
        private System.Windows.Forms.GroupBox GDSteamGroup;
        private System.Windows.Forms.DataGridView Grid230;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckBox2;
        private System.Windows.Forms.DataGridView PlantGV2;
        private System.Windows.Forms.DataGridViewTextBoxColumn unitCodeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn packageCodeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn unitTypeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn capacityDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pMinDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pMaxDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column31;
        private System.Windows.Forms.GroupBox GDGasGroup;
        private System.Windows.Forms.DataGridView Grid400;
        private System.Windows.Forms.DataGridViewTextBoxColumn lineCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fromBusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn toBusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lineLengthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ownerGencoFromDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ownerGencoToDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckBox1;
        private System.Windows.Forms.DataGridView PlantGV1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckBox;
        private System.Windows.Forms.DataGridView PlantGV11;
        private System.Windows.Forms.DataGridViewTextBoxColumn unitCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn packageCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn unitTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn capacityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pMaxDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pMinDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fuel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn Maintenance;
        private System.Windows.Forms.DataGridViewTextBoxColumn Outservice;
        private System.Windows.Forms.Panel ODMainPanel;
        private System.Windows.Forms.Button ODSaveBtn;
        private System.Windows.Forms.Panel ODPlantPanel;
        private System.Windows.Forms.GroupBox ODPowerMinGB;
        private System.Windows.Forms.Label ODpd2Valid;
        private System.Windows.Forms.Label ODpd1Valid;
        private System.Windows.Forms.DataGridView ODPowerGrid2;
        private System.Windows.Forms.DataGridView ODPowerGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.Panel panel22;
        private FarsiLibrary.Win.Controls.FADatePicker ODPowerEndDate;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel panel23;
        private FarsiLibrary.Win.Controls.FADatePicker ODPowerStartDate;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox ODPowerMinCheck;
        private System.Windows.Forms.GroupBox ODFuelGB;
        private System.Windows.Forms.Label ODsfd2Valid;
        private System.Windows.Forms.Label ODsfd1Valid;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.TextBox ODFuelQuantityTB;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel19;
        private FarsiLibrary.Win.Controls.FADatePicker ODFuelEndDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel20;
        private FarsiLibrary.Win.Controls.FADatePicker ODFuelStartDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox SecondFuelCheck;
        private System.Windows.Forms.GroupBox ODServiceGB;
        private System.Windows.Forms.Label ODosd2Valid;
        private System.Windows.Forms.Label ODosd1Valid;
        private System.Windows.Forms.Panel panel18;
        private FarsiLibrary.Win.Controls.FADatePicker ODServiceEndDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel17;
        private FarsiLibrary.Win.Controls.FADatePicker ODServiceStartDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox NoOffCheck;
        private System.Windows.Forms.CheckBox NoOnCheck;
        private System.Windows.Forms.CheckBox OutServiceCheck;
        private System.Windows.Forms.Panel ODUnitPanel;
        private System.Windows.Forms.GroupBox ODUnitMainGB;
        private System.Windows.Forms.Label odunitmd2Valid;
        private System.Windows.Forms.Label odunitmd1Valid;
        private System.Windows.Forms.Panel panel32;
        private FarsiLibrary.Win.Controls.FADatePicker ODUnitMainEndDate;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel panel33;
        private FarsiLibrary.Win.Controls.FADatePicker ODUnitMainStartDate;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.CheckBox ODUnitMainCheck;
        private System.Windows.Forms.GroupBox ODUnitPowerGB;
        private System.Windows.Forms.Label odunitpd2Valid;
        private System.Windows.Forms.Label odunitpd1Valid;
        private System.Windows.Forms.DataGridView ODUnitPowerGrid2;
        private System.Windows.Forms.DataGridView ODUnitPowerGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn67;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn68;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn69;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn70;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn71;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn72;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn73;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn74;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn75;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn76;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn77;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn78;
        private System.Windows.Forms.Panel panel25;
        private FarsiLibrary.Win.Controls.FADatePicker ODUnitPowerEndDate;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panel26;
        private FarsiLibrary.Win.Controls.FADatePicker ODUnitPowerStartDate;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.CheckBox ODUnitPowerCheck;
        private System.Windows.Forms.GroupBox ODUnitServiceGB;
        private System.Windows.Forms.Label odunitosd2Valid;
        private System.Windows.Forms.Label odunitosd1Valid;
        private System.Windows.Forms.Panel panel30;
        private FarsiLibrary.Win.Controls.FADatePicker ODUnitServiceEndDate;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panel31;
        private FarsiLibrary.Win.Controls.FADatePicker ODUnitServiceStartDate;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.CheckBox ODUnitNoOffCheck;
        private System.Windows.Forms.CheckBox ODUnitNoOnCheck;
        private System.Windows.Forms.CheckBox ODUnitOutCheck;
        private System.Windows.Forms.GroupBox ODUnitFuelGB;
        private System.Windows.Forms.Label odunitsfd2Valid;
        private System.Windows.Forms.Label odunitsfd1Valid;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.TextBox ODUnitFuelTB;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panel28;
        private FarsiLibrary.Win.Controls.FADatePicker ODUnitFuelEndDate;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel panel29;
        private FarsiLibrary.Win.Controls.FADatePicker ODUnitFuelStartDate;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.CheckBox ODUnitFuelCheck;
        private System.Windows.Forms.Panel BDMainPanel;
        private System.Windows.Forms.Button BDPlotBtn;
        private System.Windows.Forms.GroupBox BDCur2;
        private FarsiLibrary.Win.Controls.FADatePicker BDCal;
        private System.Windows.Forms.DataGridView BDCurGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn power6DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn price6DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn power7DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn price7DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn power8DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn price8DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn power9DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn price9DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn power10DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn price10DataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.GroupBox BDCur1;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.TextBox BDMaxBidTb;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.TextBox BDPowerTb;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.TextBox BDStateTb;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Panel FRMainPanel;
        private System.Windows.Forms.Button FRUpdateBtn;
        private System.Windows.Forms.Button FROKBtn;
        private System.Windows.Forms.Panel FRUnitPanel;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Panel panel65;
        private System.Windows.Forms.TextBox FRUnitHotTb;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Panel panel68;
        private System.Windows.Forms.TextBox FRUnitColdTb;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Panel panel66;
        private System.Windows.Forms.TextBox FRUnitBmainTb;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.TextBox FRUnitAmainTb;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.TextBox FRUnitCmargTb2;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.TextBox FRUnitBmargTb2;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.TextBox FRUnitAmargTb2;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.TextBox FRUnitCmargTb1;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.TextBox FRUnitBmargTb1;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.TextBox FRUnitAmargTb1;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.TextBox FRUnitVariableTb;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.TextBox FRUnitFixedTb;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.TextBox FRUnitCapitalTb;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.GroupBox groupBox10;
        private FarsiLibrary.Win.Controls.FADatePicker FRUnitCal;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Panel panel76;
        private System.Windows.Forms.TextBox FRUnitIncomeTb;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Panel panel69;
        private System.Windows.Forms.TextBox FRUnitEneryPayTb;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Panel panel70;
        private System.Windows.Forms.TextBox FRUnitCapPayTb;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Panel panel71;
        private System.Windows.Forms.TextBox FRUnitULPowerTb;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Panel panel73;
        private System.Windows.Forms.TextBox FRUnitTotalPowerTb;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Panel panel74;
        private System.Windows.Forms.TextBox FRUnitCapacityTb;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Panel FRPlantPanel;
        private System.Windows.Forms.GroupBox groupBox5;
        private FarsiLibrary.Win.Controls.FADatePicker FRPlantCal;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.TextBox FRPlantCostTb;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.TextBox FRPlantIncomeTb;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.TextBox FRPlantBenefitTB;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.TextBox FRPlantDecPayTb;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.TextBox FRPlantIncPayTb;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.TextBox FRPlantULPayTb;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.TextBox FRPlantBidPayTb;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.TextBox FRPlantEnergyPayTb;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.TextBox FRPlantCapPayTb;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.TextBox FRPlantDecPowerTb;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.TextBox FRPlantIncPowerTb;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.TextBox FRPlantULPowerTb;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.TextBox FRPlantBidPowerTb;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.TextBox FRPlantTotalPowerTb;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.TextBox FRPlantAvaCapTb;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn55;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn56;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn58;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn59;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn61;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn62;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn63;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn64;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn65;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn66;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerBidding;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Panel panel80;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel81;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox grBoxCustomize;
        private System.Windows.Forms.MenuStrip menuStrip7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy6;
        private System.Windows.Forms.ToolStripComboBox cmbPeak6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy12;
        private System.Windows.Forms.ToolStripComboBox cmbPeak12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem19;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy18;
        private System.Windows.Forms.ToolStripComboBox cmbPeak18;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem20;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy24;
        private System.Windows.Forms.ToolStripComboBox cmbPeak24;
        private System.Windows.Forms.MenuStrip menuStrip6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy5;
        private System.Windows.Forms.ToolStripComboBox cmbPeak5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy11;
        private System.Windows.Forms.ToolStripComboBox cmbPeak11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy17;
        private System.Windows.Forms.ToolStripComboBox cmbPeak17;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy23;
        private System.Windows.Forms.ToolStripComboBox cmbPeak23;
        private System.Windows.Forms.MenuStrip menuStrip5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy4;
        private System.Windows.Forms.ToolStripComboBox cmbPeak4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy10;
        private System.Windows.Forms.ToolStripComboBox cmbPeak10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy16;
        private System.Windows.Forms.ToolStripComboBox cmbPeak16;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy22;
        private System.Windows.Forms.ToolStripComboBox cmbPeak22;
        private System.Windows.Forms.MenuStrip menuStrip3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy2;
        private System.Windows.Forms.ToolStripComboBox cmbPeak2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy8;
        private System.Windows.Forms.ToolStripComboBox cmbPeak8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy14;
        private System.Windows.Forms.ToolStripComboBox cmbPeak14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy20;
        private System.Windows.Forms.ToolStripComboBox cmbPeak20;
        private System.Windows.Forms.MenuStrip menuStrip4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem25;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy3;
        private System.Windows.Forms.ToolStripComboBox cmbPeak3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem26;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy9;
        private System.Windows.Forms.ToolStripComboBox cmbPeak9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem27;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy15;
        private System.Windows.Forms.ToolStripComboBox cmbPeak15;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem28;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy21;
        private System.Windows.Forms.ToolStripComboBox cmbPeak21;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem hour1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy1;
        private System.Windows.Forms.ToolStripComboBox cmbPeak1;
        private System.Windows.Forms.ToolStripMenuItem hour2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy7;
        private System.Windows.Forms.ToolStripComboBox cmbPeak7;
        private System.Windows.Forms.ToolStripMenuItem hour3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy13;
        private System.Windows.Forms.ToolStripComboBox cmbPeak13;
        private System.Windows.Forms.ToolStripMenuItem hour4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy19;
        private System.Windows.Forms.ToolStripComboBox cmbPeak19;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerCurrent;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnRunBidding;
        private System.Windows.Forms.Label lblPlantValue;
        private System.Windows.Forms.Label lblGencoValue;
        private System.Windows.Forms.MenuStrip menuStrip8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem24;
        private System.Windows.Forms.ToolStripMenuItem strategyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem peakToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem strategyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem strategyToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem peakToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem hourToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem strategyToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem peakToolStripMenuItem2;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.ComboBox cmbPeakBid;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel75;
        private System.Windows.Forms.ComboBox cmbCostLevel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel77;
        private System.Windows.Forms.ComboBox cmbStrategyBidding;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel78;
        private System.Windows.Forms.TextBox txtTraining;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel79;
        private System.Windows.Forms.ComboBox cmbRunSetting;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ToolStripMenuItem betToolStripMenuItem;
        private System.Windows.Forms.RadioButton rdStrategyBidding;
        private System.Windows.Forms.RadioButton rdBidAllocation;
        private System.Windows.Forms.RadioButton rdForecastingPrice;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.TextBox lstStatus;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Panel GDUnitPanel;
        private System.Windows.Forms.GroupBox Maintenancegb;
        private System.Windows.Forms.Panel panel16;
        private FarsiLibrary.Win.Controls.FADatePicker GDEndDateCal;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel15;
        private FarsiLibrary.Win.Controls.FADatePicker GDStartDateCal;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.TextBox GDMainTypeTB;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox Currentgb;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.TextBox GDFuelTB;
        private System.Windows.Forms.Label GDFuelLb;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox GDStateTB;
        private System.Windows.Forms.Label GDStateLb;
        private System.Windows.Forms.GroupBox Anonygb;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.TextBox GDIntConsumeTB;
        private System.Windows.Forms.Label GDConsLb;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.TextBox GDRampRateTB;
        private System.Windows.Forms.Label GDRampLb;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox GDTimeHotStartTB;
        private System.Windows.Forms.Label GDHotLb;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox GDTimeColdStartTB;
        private System.Windows.Forms.Label GDColdLb;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox GDTimeDownTB;
        private System.Windows.Forms.Label GDTimeDownLb;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox GDTimeUpTB;
        private System.Windows.Forms.Label GDTUpLb;
        private System.Windows.Forms.GroupBox Powergb;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox GDPminTB;
        private System.Windows.Forms.Label GDPminLb;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox GDPmaxTB;
        private System.Windows.Forms.Label GDPmaxLb;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox GDcapacityTB;
        private System.Windows.Forms.Label label8;
    }
}