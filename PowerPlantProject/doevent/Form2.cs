﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using NRI.Common;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;

namespace PowerPlantProject
{
    public partial class Form2 : Form
    {

        ProgressForm progressFrm;

        //Form3 NewPlant;
        public int PPID;
        public int line;
        public int TextboxTab = 0;
        //string ConStr = "";
        const int NumPPID = 20;
        string[] PPIDArray = new string[NumPPID];
        double PAvailableCapacity = 0, PTotalPower = 0, PULPower = 0, PBidPower = 0, PIncrementPower = 0,
        PDecreasePower = 0, PCapacityPayment = 0, PBidPayment = 0, PULPayment = 0, PIncrementPayment = 0,
        PDecreasePayment = 0, PEnergyPayment = 0, PIncome = 0, PCost = 0, PBenefit = 0, CCTotalPower = 0,
        NoCCTotalPower = 0, CCIncPower = 0, NoCCIncPower = 0, CCDecPower = 0, NoCCDecPower = 0,
        CCUnitPower = 0, NoCCUnitPower = 0;
        double[] CCMaxBid = new double[24];
        double[] NoCCMaxBid = new double[24];
        bool m002Generated = false;
        SqlConnection MyConnection = ConnectionManager.GetInstance().Connection;

        public Form2()
        {
            InitializeComponent();
            //NewPlant = new Form3(this);
            //ConStr = System.Configuration.ConfigurationManager.ConnectionStrings["PowerPlantProject"].ConnectionString;
            //ConStr = "Data Source=.;Initial Catalog=PowerPalntDB;Integrated Security=True";

            for (int j = 0; j < 20; j++) PPIDArray[j] = "0";

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT PPID FROM PowerPlant", MyConnection);
            Myda.Fill(MyDS, "ppid");
            int i = 0;
            foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
            {
                PPIDArray[i] = MyRow["PPID"].ToString().Trim();
                i++;
            }
            PPIDArray[i] = "132";
            i++;
            PPIDArray[i] = "145";

            BDCurGrid.DataSource = null;

            //SET Calendars
            FALocalizeManager.CustomCulture = FALocalizeManager.FarsiCulture;
            GDStartDateCal.SelectedDateTime = System.DateTime.Now;
            GDStartDateCal.IsNull = true;
            GDEndDateCal.SelectedDateTime = System.DateTime.Now;
            GDEndDateCal.IsNull = true;
            BDCal.SelectedDateTime = System.DateTime.Now;
            BDCal.IsNull = true;
            FRUnitCal.SelectedDateTime = System.DateTime.Now;
            FRUnitCal.IsNull = true;
            FRPlantCal.SelectedDateTime = System.DateTime.Now;
            FRPlantCal.IsNull = true;
            MRCal.SelectedDateTime = System.DateTime.Now;
            MRCal.IsNull = true;
            ODServiceStartDate.SelectedDateTime = System.DateTime.Now;
            ODServiceStartDate.IsNull = true;
            ODServiceEndDate.SelectedDateTime = System.DateTime.Now;
            ODServiceEndDate.IsNull = true;
            ODFuelStartDate.SelectedDateTime = System.DateTime.Now;
            ODFuelStartDate.IsNull = true;
            ODFuelEndDate.SelectedDateTime = System.DateTime.Now;
            ODFuelEndDate.IsNull = true;
            ODPowerStartDate.SelectedDateTime = System.DateTime.Now;
            ODPowerStartDate.IsNull = true;
            ODPowerEndDate.SelectedDateTime = System.DateTime.Now;
            ODPowerEndDate.IsNull = true;
            ODUnitServiceStartDate.SelectedDateTime = System.DateTime.Now;
            ODUnitServiceStartDate.IsNull = true;
            ODUnitServiceEndDate.SelectedDateTime = System.DateTime.Now;
            ODUnitServiceEndDate.IsNull = true;
            ODUnitMainStartDate.SelectedDateTime = System.DateTime.Now;
            ODUnitMainStartDate.IsNull = true;
            ODUnitMainEndDate.SelectedDateTime = System.DateTime.Now;
            ODUnitMainEndDate.IsNull = true;
            ODUnitFuelStartDate.SelectedDateTime = System.DateTime.Now;
            ODUnitFuelStartDate.IsNull = true;
            ODUnitFuelEndDate.SelectedDateTime = System.DateTime.Now;
            ODUnitFuelEndDate.IsNull = true;
            ODUnitPowerStartDate.SelectedDateTime = System.DateTime.Now;
            ODUnitPowerStartDate.IsNull = true;
            ODUnitPowerEndDate.SelectedDateTime = System.DateTime.Now;
            ODUnitPowerEndDate.IsNull = true;
        }
        //---------------------Form2_Load-----------------------------------
        private void Form2_Load(object sender, EventArgs e)
        {
            buildTreeView1();
            LoadBiddingStrategy();

            //AutomaticFillEconomics();
            //Initialze StartDate for Calculate
            //string StartDate="";
            //JustOneTime(StartDate);

            //General Data
            //int x1 = GeneralData.Size.Width / 2;
            //int y1 = GeneralData.Size.Height / 2;
            //int x2 = GDMainPanel.Size.Width / 2;
            //int y2 = GDMainPanel.Size.Height / 2;
            //Point pnt = new Point(x1 - x2, y1 - y2);
            //GDMainPanel.Location = pnt;


            ////Operational Data
            //x1 = OperationalData.Size.Width / 2;
            //y1 = OperationalData.Size.Height / 2;
            //x2 = ODMainPanel.Size.Width / 2;
            //y2 = ODMainPanel.Size.Height / 2;
            //pnt.X = x1 - x2;
            //pnt.Y = y1 - y2;
            //ODMainPanel.Location = pnt;


            ////Market Result
            //x1 = MarketResults.Size.Width / 2;
            //y1 = MarketResults.Size.Height / 2;
            //x2 = MRMainPanel.Size.Width / 2;
            //y2 = MRMainPanel.Size.Height / 2;
            //pnt.X = x1 - x2;
            //pnt.Y = y1 - y2;
            //MRMainPanel.Location = pnt;


            ////Bid Data
            //x1 = BidData.Size.Width / 2;
            //y1 = BidData.Size.Height / 2;
            //x2 = BDMainPanel.Size.Width / 2;
            //y2 = BDMainPanel.Size.Height / 2;
            //pnt.X = x1 - x2;
            //pnt.Y = y1 - y2;
            //BDMainPanel.Location = pnt;


            ////Financial Report
            //x1 = FinancialReport.Size.Width / 2;
            //y1 = FinancialReport.Size.Height / 2;
            //x2 = FRMainPanel.Size.Width / 2;
            //y2 = FRMainPanel.Size.Height / 2;
            //pnt.X = x1 - x2;
            //pnt.Y = y1 - y2;
            //FRMainPanel.Location = pnt;



        }
        //-------------------------------bulidTreeView1---------------------------------
        private void buildTreeView1()
        {
            treeView1.Nodes.Clear();
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();

            //Insert Trec
            TreeNode MyNode = new TreeNode();
            MyNode.Text = "Trec";
            treeView1.Nodes.Add(MyNode);

            //Insert Plants
            Myda.SelectCommand = new SqlCommand("SELECT PPName FROM PowerPlant order by ppid", MyConnection);
            Myda.Fill(MyDS, "plantname");
            TreeNode PlantNode = new TreeNode();
            PlantNode.Text = "Plant";
            MyNode.Nodes.Add(PlantNode);
            foreach (DataRow MyRow in MyDS.Tables["plantname"].Rows)
            {
                TreeNode ChildNode = new TreeNode();
                ChildNode.Text = MyRow["PPName"].ToString();
                PlantNode.Nodes.Add(ChildNode);
            }

            //Insert Transmission
            Myda.SelectCommand = new SqlCommand("SELECT DISTINCT LineNumber FROM TransLine", MyConnection);
            Myda.Fill(MyDS, "transtype");
            TreeNode TransNode = new TreeNode();
            TransNode.Text = "Transmission";
            MyNode.Nodes.Add(TransNode);
            foreach (DataRow MyRow in MyDS.Tables["transtype"].Rows)
            {
                TreeNode ChildNode = new TreeNode();
                ChildNode.Text = MyRow["LineNumber"].ToString();
                TransNode.Nodes.Add(ChildNode);
            }
            treeView1.ExpandAll();
        }
        //--------------------buildPPtree----------------------------------------
        private void buildPPtree(string Num)
        {
            treeView2.Nodes.Clear();
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT DISTINCT PackageType FROM UnitsDataMain WHERE PPID=@Num", MyConnection);
            Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
            Myda.SelectCommand.Parameters["@Num"].Value = Num;
            Myda.Fill(MyDS, "UnitType");

            foreach (DataRow MyRow in MyDS.Tables["UnitType"].Rows)
            {
                string Utype = MyRow["PackageType"].ToString();
                Utype = Utype.Trim();
                switch (Utype)
                {
                    case "Steam":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Steam%' order by PPID,UnitCode", MyConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = Num;
                        Myda.Fill(MyDS, "Steam");
                        TreeNode MyNode = new TreeNode();
                        MyNode.Text = "Steam";
                        treeView2.Nodes.Add(MyNode);
                        string temp = "";
                        DataView sdv = new DataView(MyDS.Tables["Steam"]);
                        sdv.Sort = "UnitCode ASC";
                        foreach (DataRow ChildRow in sdv.Table.Rows)
                        //foreach (DataRow ChildRow in MyDS.Tables["Steam"].Rows)
                        {
                            temp = ChildRow["unitCode"].ToString();
                            temp = temp.Trim();
                            if (temp.Length == 6)
                            {
                                TreeNode ChildNode = new TreeNode();
                                ChildNode.Text = ChildRow["UnitCode"].ToString();
                                MyNode.Nodes.Add(ChildNode);
                            }
                        }
                        foreach (DataRow ChildRow in sdv.Table.Rows)
                        //foreach (DataRow ChildRow in MyDS.Tables["Steam"].Rows)
                        {
                            temp = ChildRow["UnitCode"].ToString();
                            temp = temp.Trim();
                            if (temp.Length == 7)
                            {
                                TreeNode ChildNode = new TreeNode();
                                ChildNode.Text = ChildRow["UnitCode"].ToString();
                                MyNode.Nodes.Add(ChildNode);
                            }
                        }
                        break;
                    case "Gas":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Gas%' order by UnitCode", MyConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = Num;
                        Myda.Fill(MyDS, "Gas");
                        TreeNode Node1 = new TreeNode();
                        Node1.Text = "Gas";
                        treeView2.Nodes.Add(Node1);
                        DataView gdv = new DataView(MyDS.Tables["Gas"]);
                        gdv.Sort = "UnitCode";
                        foreach (DataRow ChildRow in gdv.Table.Rows)
                        //foreach (DataRow ChildRow in MyDS.Tables["Gas"].Rows)
                        {
                            temp = ChildRow["UnitCode"].ToString();
                            temp = temp.Trim();
                            if (temp.Length == 4)
                            {
                                TreeNode ChildNode = new TreeNode();
                                ChildNode.Text = ChildRow["UnitCode"].ToString();
                                Node1.Nodes.Add(ChildNode);
                            }
                        }
                        foreach (DataRow ChildRow in gdv.Table.Rows)
                        //foreach (DataRow ChildRow in MyDS.Tables["Gas"].Rows)
                        {
                            temp = ChildRow["UnitCode"].ToString();
                            temp = temp.Trim();
                            if (temp.Length == 5)
                            {
                                TreeNode ChildNode = new TreeNode();
                                ChildNode.Text = ChildRow["UnitCode"].ToString();
                                Node1.Nodes.Add(ChildNode);
                            }
                        }
                        break;
                    default:
                        Myda.SelectCommand = new SqlCommand("SELECT DISTINCT PackageCode FROM [UnitsDataMain] WHERE PPID=@Num AND PackageType LIKE 'CC%'", MyConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = Num;
                        Myda.Fill(MyDS, "CCPackage");
                        foreach (DataRow ChildRow in MyDS.Tables["CCPackage"].Rows)
                        {
                            TreeNode PNode = new TreeNode();
                            PNode.Text = "Combined Cycle" + ChildRow["PackageCode"].ToString();
                            treeView2.Nodes.Add(PNode);
                        }

                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageCode FROM [UnitsDataMain] WHERE PPID=@Num AND PackageType LIKE 'CC%' order by UnitCode", MyConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = Num;
                        Myda.Fill(MyDS, "Combined");
                        DataView cdv = new DataView(MyDS.Tables["Combined"]);
                        cdv.Sort = "UnitCode ASC";
                        foreach (DataRow ChildRow in cdv.Table.Rows)
                            //foreach (DataRow ChildRow in MyDS.Tables["Combined"].Rows)
                            foreach (TreeNode mynode in treeView2.Nodes)
                                if (mynode.Text.Contains(ChildRow["PackageCode"].ToString()))
                                {
                                    TreeNode GNode = new TreeNode();
                                    GNode.Text = ChildRow["UnitCode"].ToString();
                                    mynode.Nodes.Add(GNode);
                                }
                        break;
                }
            }

            MyDS.Dispose();
            Myda.Dispose();
            treeView2.ExpandAll();
        }

        //---------------------Form2_FormClosed-----------------------------------------
        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        //------------------------buildTRANStree----------------------------------
        private void buildTRANStree(string Num)
        {
            TreeNode MyNode, ChildNode, CNode;
            treeView2.Nodes.Clear();
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT LineNumber,Name,LineCode FROM TransLine WHERE LineNumber=@Num ORDER BY TransLine.Name", MyConnection);
            Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.Int);
            Myda.SelectCommand.Parameters["@Num"].Value = Num;
            Myda.Fill(MyDS, "Trans");

            foreach (DataRow MyRow in MyDS.Tables["Trans"].Rows)
            {
                bool Rthereis = false;
                int type = int.Parse(MyRow["LineNumber"].ToString());
                foreach (TreeNode node in treeView2.Nodes)
                {
                    if (node.Text == MyRow["LineNumber"].ToString())
                    {
                        Rthereis = true;
                        bool Cthereis = false;
                        foreach (TreeNode node1 in node.Nodes)
                            if (node1.Text == MyRow["Name"].ToString())
                            {
                                Cthereis = true;
                                CNode = new TreeNode();
                                CNode.Text = MyRow["LineCode"].ToString();
                                node1.Nodes.Add(CNode);
                            }
                        if (!Cthereis)
                        {
                            ChildNode = new TreeNode();
                            ChildNode.Text = MyRow["Name"].ToString();
                            node.Nodes.Add(ChildNode);
                            CNode = new TreeNode();
                            CNode.Text = MyRow["LineCode"].ToString();
                            ChildNode.Nodes.Add(CNode);
                        }
                    }
                }
                if (!Rthereis)
                {
                    MyNode = new TreeNode();
                    MyNode.Text = MyRow["LineNumber"].ToString();
                    treeView2.Nodes.Add(MyNode);
                    bool Cthereis = false;
                    foreach (TreeNode node1 in MyNode.Nodes)
                        if (node1.Text == MyRow["Name"].ToString())
                        {
                            Cthereis = true;
                            CNode = new TreeNode();
                            CNode.Text = MyRow["LineCode"].ToString();
                            node1.Nodes.Add(CNode);
                        }
                    if (!Cthereis)
                    {
                        ChildNode = new TreeNode();
                        ChildNode.Text = MyRow["Name"].ToString();
                        MyNode.Nodes.Add(ChildNode);
                        CNode = new TreeNode();
                        CNode.Text = MyRow["LineCode"].ToString();
                        ChildNode.Nodes.Add(CNode);
                    }
                }
            }

            MyDS.Dispose();
            Myda.Dispose();

            treeView2.ExpandAll();
        }
        //----------------------------FillMRVlues------------------------------------
        private void FillMRVlues(string Num)
        {
            //Clear Market Result Tab
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            MRPlantOnUnitTb.Text = "";
            MRPlantPowerTb.Text = "";

            //detect current date and hour
            System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
            DateTime CurDate = DateTime.Now;
            string day = pr.GetDayOfMonth(CurDate).ToString();
            if (int.Parse(day) < 10) day = "0" + day;
            string mydate = pr.GetYear(CurDate) + "/" + pr.GetMonth(CurDate) + "/" + day;
            int myhour = CurDate.Hour;

            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;


            //set OnUnits & Power TextBox
            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            MyCom.CommandText = "SELECT @re1=COUNT(DISTINCT Block), @re2=SUM (Required) FROM [DetailFRM005] WHERE" +
            "  TargetMarketDate=@date AND PPID=@pid AND Hour=@hour AND Required <> 0";
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = mydate;
            MyCom.Parameters.Add("@pid", SqlDbType.NChar, 10);
            MyCom.Parameters["@pid"].Value = Num;
            MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
            MyCom.Parameters["@hour"].Value = myhour;
            MyCom.Parameters.Add("@re1", SqlDbType.Int);
            MyCom.Parameters["@re1"].Direction = ParameterDirection.Output;
            MyCom.Parameters.Add("@re2", SqlDbType.Real);
            MyCom.Parameters["@re2"].Direction = ParameterDirection.Output;
            MyCom.ExecuteNonQuery();
            MRPlantOnUnitTb.Text = MyCom.Parameters["@re1"].Value.ToString();
            if (MyCom.Parameters["@re2"].Value.ToString() != "")
                MRPlantPowerTb.Text = MyCom.Parameters["@re2"].Value.ToString();
            else MRPlantPowerTb.Text = "0";

            //Set GridViews
            if (!MRCal.IsNull)
                FillMRPlantGrid();
            //{
            //    DataSet MyDS = new DataSet();
            //    SqlDataAdapter Myda = new SqlDataAdapter();

            //    Myda.SelectCommand = new SqlCommand("SELECT Hour,SUM(Required) AS SR FROM [DetailFRM005] WHERE PPID= "+Num+" AND TargetMarketDate=@date GROUP BY Hour", MyConnection);
            //    Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
            //    Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
            //    Myda.Fill(MyDS);
            //    MRCurGrid1.Rows[0].Cells[0].Value = "Power";
            //    MRCurGrid2.Rows[0].Cells[0].Value = "Power";
            //    foreach (DataRow MyRow in MyDS.Tables[0].Rows)
            //    {
            //        int index=int.Parse(MyRow["Hour"].ToString());
            //        if (index <= 12)
            //            MRCurGrid1.Rows[0].Cells[index].Value = MyRow["SR"];
            //        else
            //            MRCurGrid2.Rows[0].Cells[index - 12].Value = MyRow["SR"];
            //    }
            // }

        }
        //---------------------------------------ClearODPlantTab()---------------------------------
        private void ClearODPlantTab()
        {
            OutServiceCheck.Checked = false;
            NoOnCheck.Checked = false;
            NoOffCheck.Checked = false;
            ODServiceStartDate.IsNull = true;
            ODServiceEndDate.IsNull = true;
            SecondFuelCheck.Checked = false;
            ODFuelStartDate.IsNull = true;
            ODFuelEndDate.IsNull = true;
            ODFuelQuantityTB.Text = "";
            ODPowerMinCheck.Checked = false;
            ODPowerStartDate.IsNull = true;
            ODPowerEndDate.IsNull = true;
            ODPowerGrid1.DataSource = null;
            if (ODPowerGrid1.Rows != null) ODPowerGrid1.Rows.Clear();
            ODPowerGrid2.DataSource = null;
            if (ODPowerGrid2.Rows != null) ODPowerGrid2.Rows.Clear();
        }
        //-----------------------------------FillODValues------------------------------------------
        private void FillODValues(string Num)
        {
            ClearODPlantTab();
            //detect current date and hour
            System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
            DateTime CurDate = DateTime.Now;
            string day = pr.GetDayOfMonth(CurDate).ToString();
            if (int.Parse(day) < 10) day = "0" + day;
            string mydate = pr.GetYear(CurDate) + "/" + pr.GetMonth(CurDate) + "/" + day;
            int myhour = CurDate.Hour;

            //Is There a Row in DB for this Plant?
            SqlCommand MyCom = new SqlCommand();
            MyCom.CommandText = "SELECT  @result1 =count(PPID) FROM [ConditionPlant] WHERE PPID=@num";
            MyCom.Connection = MyConnection;
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPID;
            MyCom.Parameters.Add("@result1", SqlDbType.Int);
            MyCom.Parameters["@result1"].Direction = ParameterDirection.Output;
            //try
            // {
            MyCom.ExecuteNonQuery();
            // }
            //catch()
            //{
            //}
            int result1;
            result1 = (int)MyCom.Parameters["@result1"].Value;
            if (result1 != 0)
            {
                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT OutService,OutServiceStartDate,OutServiceEndDate,NoOn,NoOff," +
                "SecondFuel,SecondFuelStartDate,SecondFuelEndDate,FuelQuantity,PowerMin,PowerMinStartDate,PowerMinEndDate," +
                "PowerMinHour1,PowerMinHour2,PowerMinHour3,PowerMinHour4,PowerMinHour5,PowerMinHour6,PowerMinHour7,PowerMinHour8," +
                "PowerMinHour9,PowerMinHour10,PowerMinHour11,PowerMinHour12,PowerMinHour13,PowerMinHour14,PowerMinHour15," +
                "PowerMinHour16,PowerMinHour17,PowerMinHour18,PowerMinHour19,PowerMinHour20,PowerMinHour21,PowerMinHour22," +
                "PowerMinHour23,PowerMinHour24 FROM ConditionPlant WHERE PPID=" + PPID, MyConnection);
                Myda.Fill(MyDS);
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    if (bool.Parse(MyRow[0].ToString()))
                        if (CheckDate(mydate, MyRow[1].ToString(), MyRow[2].ToString()))
                        {
                            OutServiceCheck.Checked = true;
                            ODServiceStartDate.IsNull = false;
                            ODServiceEndDate.IsNull = false;
                            ODServiceStartDate.Text = MyRow[1].ToString();
                            ODServiceEndDate.Text = MyRow[2].ToString();
                        }
                    if (bool.Parse(MyRow[3].ToString()))
                        NoOnCheck.Checked = true;
                    if (bool.Parse(MyRow[4].ToString()))
                        NoOffCheck.Checked = true;
                    if (bool.Parse(MyRow[5].ToString()))
                        if (CheckDate(mydate, MyRow[6].ToString(), MyRow[7].ToString()))
                        {
                            SecondFuelCheck.Checked = true;
                            ODFuelStartDate.IsNull = false;
                            ODFuelEndDate.IsNull = false;
                            ODFuelStartDate.Text = MyRow[6].ToString();
                            ODFuelEndDate.Text = MyRow[7].ToString();
                            ODFuelQuantityTB.Text = MyRow[8].ToString();
                        }
                    if (bool.Parse(MyRow[9].ToString()))
                        if (CheckDate(mydate, MyRow[10].ToString(), MyRow[11].ToString()))
                        {
                            ODPowerMinCheck.Checked = true;
                            ODPowerStartDate.IsNull = false;
                            ODPowerEndDate.IsNull = false;
                            ODPowerStartDate.Text = MyRow[10].ToString();
                            ODPowerEndDate.Text = MyRow[11].ToString();
                            ODPowerGrid1.Rows[0].Cells[0].Value = MyRow[12].ToString();
                            ODPowerGrid1.Rows[0].Cells[1].Value = MyRow[13].ToString();
                            ODPowerGrid1.Rows[0].Cells[2].Value = MyRow[14].ToString();
                            ODPowerGrid1.Rows[0].Cells[3].Value = MyRow[15].ToString();
                            ODPowerGrid1.Rows[0].Cells[4].Value = MyRow[16].ToString();
                            ODPowerGrid1.Rows[0].Cells[5].Value = MyRow[17].ToString();
                            ODPowerGrid1.Rows[0].Cells[6].Value = MyRow[18].ToString();
                            ODPowerGrid1.Rows[0].Cells[7].Value = MyRow[19].ToString();
                            ODPowerGrid1.Rows[0].Cells[8].Value = MyRow[20].ToString();
                            ODPowerGrid1.Rows[0].Cells[9].Value = MyRow[21].ToString();
                            ODPowerGrid1.Rows[0].Cells[10].Value = MyRow[22].ToString();
                            ODPowerGrid1.Rows[0].Cells[11].Value = MyRow[23].ToString();
                            ODPowerGrid2.Rows[0].Cells[0].Value = MyRow[24].ToString();
                            ODPowerGrid2.Rows[0].Cells[1].Value = MyRow[25].ToString();
                            ODPowerGrid2.Rows[0].Cells[2].Value = MyRow[26].ToString();
                            ODPowerGrid2.Rows[0].Cells[3].Value = MyRow[27].ToString();
                            ODPowerGrid2.Rows[0].Cells[4].Value = MyRow[28].ToString();
                            ODPowerGrid2.Rows[0].Cells[5].Value = MyRow[29].ToString();
                            ODPowerGrid2.Rows[0].Cells[6].Value = MyRow[30].ToString();
                            ODPowerGrid2.Rows[0].Cells[7].Value = MyRow[31].ToString();
                            ODPowerGrid2.Rows[0].Cells[8].Value = MyRow[32].ToString();
                            ODPowerGrid2.Rows[0].Cells[9].Value = MyRow[33].ToString();
                            ODPowerGrid2.Rows[0].Cells[10].Value = MyRow[34].ToString();
                            ODPowerGrid2.Rows[0].Cells[11].Value = MyRow[35].ToString();
                        }
                }
            }
        }
        //------------------------------------ClearODUnitTab()---------------------------------
        private void ClearODUnitTab()
        {
            ODUnitOutCheck.Checked = false;
            ODUnitNoOnCheck.Checked = false;
            ODUnitNoOffCheck.Checked = false;
            ODUnitServiceStartDate.IsNull = true;
            ODUnitServiceEndDate.IsNull = true;
            ODUnitMainCheck.Checked = false;
            ODUnitMainStartDate.IsNull = true;
            ODUnitMainEndDate.IsNull = true;
            ODUnitFuelCheck.Checked = false;
            ODUnitFuelStartDate.IsNull = true;
            ODUnitFuelEndDate.IsNull = true;
            ODUnitFuelTB.Text = "";
            ODUnitPowerCheck.Checked = false;
            ODUnitPowerStartDate.IsNull = true;
            ODUnitPowerEndDate.IsNull = true;
            ODUnitPowerGrid1.DataSource = null;
            if (ODUnitPowerGrid1.Rows != null) ODUnitPowerGrid1.Rows.Clear();
            ODUnitPowerGrid2.DataSource = null;
            if (ODUnitPowerGrid2.Rows != null) ODUnitPowerGrid2.Rows.Clear();
        }
        //----------------------------------FillODUnit--------------------------------------------
        private void FillODUnit(string unit, string package, int index)
        {
            ClearODUnitTab();
            //detect current date and hour
            System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
            DateTime CurDate = DateTime.Now;
            string day = pr.GetDayOfMonth(CurDate).ToString();
            if (int.Parse(day) < 10) day = "0" + day;
            string mydate = pr.GetYear(CurDate) + "/" + pr.GetMonth(CurDate) + "/" + day;
            int myhour = CurDate.Hour;


            //Is There a Row in DB for this Unit?
            SqlCommand MyCom = new SqlCommand();
            MyCom.CommandText = "SELECT  @result1 =count(PPID) FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@unit " +
            "AND PackageType=@type SELECT @result2=COUNT(PPID) FROM PowerLimitedUnit WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type";
            MyCom.Connection = MyConnection;
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPID;
            MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
            MyCom.Parameters["@unit"].Value = unit;
            string type = package;
            if (type.Contains("Combined")) type = "CC";
            MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
            MyCom.Parameters["@type"].Value = type;
            MyCom.Parameters.Add("@result1", SqlDbType.Int);
            MyCom.Parameters["@result1"].Direction = ParameterDirection.Output;
            MyCom.Parameters.Add("@result2", SqlDbType.Int);
            MyCom.Parameters["@result2"].Direction = ParameterDirection.Output;
            //try
            // {
            MyCom.ExecuteNonQuery();
            // }
            //catch()
            //{
            //}
            int result1;
            result1 = (int)MyCom.Parameters["@result1"].Value;
            int result2;
            result2 = (int)MyCom.Parameters["@result2"].Value;
            if (result1 != 0)
            {
                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT OutService,OutServiceStartDate,OutServiceEndDate,NoOn,NoOff," +
                "SecondFuel,SecondFuelStartDate,SecondFuelEndDate,FuelForOneDay,Maintenance,MaintenanceStartDate," +
                "MaintenanceEndDate FROM ConditionUnit WHERE UnitCode=@unit AND PackageType=@type AND PPID=" + PPID, MyConnection);
                Myda.SelectCommand.Parameters.Add("@unit", SqlDbType.NChar, 20);
                Myda.SelectCommand.Parameters["@unit"].Value = unit;
                Myda.SelectCommand.Parameters.Add("@type", SqlDbType.NChar, 10);
                Myda.SelectCommand.Parameters["@type"].Value = type;
                Myda.Fill(MyDS);
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    if (bool.Parse(MyRow[0].ToString()))
                        if (CheckDate(mydate, MyRow[1].ToString(), MyRow[2].ToString()))
                        {
                            ODUnitOutCheck.Checked = true;
                            ODUnitServiceStartDate.IsNull = false;
                            ODUnitServiceEndDate.IsNull = false;
                            ODUnitServiceStartDate.Text = MyRow[1].ToString();
                            ODUnitServiceEndDate.Text = MyRow[2].ToString();
                        }
                    if (bool.Parse(MyRow[3].ToString()))
                        ODUnitNoOnCheck.Checked = true;
                    if (bool.Parse(MyRow[4].ToString()))
                        ODUnitNoOffCheck.Checked = true;
                    if (bool.Parse(MyRow[9].ToString()))
                        if (CheckDate(mydate, MyRow[10].ToString(), MyRow[11].ToString()))
                        {
                            ODUnitMainCheck.Checked = true;
                            ODUnitMainStartDate.IsNull = false;
                            ODUnitMainEndDate.IsNull = false;
                            ODUnitMainStartDate.Text = MyRow[10].ToString();
                            ODUnitMainEndDate.Text = MyRow[11].ToString();
                        }
                    if (bool.Parse(MyRow[5].ToString()))
                        if (CheckDate(mydate, MyRow[6].ToString(), MyRow[7].ToString()))
                        {
                            ODUnitFuelCheck.Checked = true;
                            ODUnitFuelStartDate.IsNull = false;
                            ODUnitFuelEndDate.IsNull = false;
                            ODUnitFuelStartDate.Text = MyRow[6].ToString();
                            ODUnitFuelEndDate.Text = MyRow[7].ToString();
                            ODUnitFuelTB.Text = MyRow[8].ToString();
                        }
                }
            }
            if (result2 != 0)
            {
                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT StartDate,EndDate,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6," +
                "Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21," +
                "Hour22,Hour23,Hour24 FROM PowerLimitedUnit WHERE UnitCode=@unit AND PackageType=@type AND PPID=" + PPID, MyConnection);
                Myda.SelectCommand.Parameters.Add("@unit", SqlDbType.NChar, 20);
                Myda.SelectCommand.Parameters["@unit"].Value = unit;
                Myda.SelectCommand.Parameters.Add("@type", SqlDbType.NChar, 10);
                Myda.SelectCommand.Parameters["@type"].Value = type;
                Myda.Fill(MyDS);
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    if (CheckDate(mydate, MyRow[0].ToString(), MyRow[1].ToString()))
                    {
                        ODUnitPowerCheck.Checked = true;
                        ODUnitPowerStartDate.IsNull = false;
                        ODUnitPowerEndDate.IsNull = false;
                        ODUnitPowerStartDate.Text = MyRow[0].ToString();
                        ODUnitPowerEndDate.Text = MyRow[1].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[0].Value = MyRow[2].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[1].Value = MyRow[3].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[2].Value = MyRow[4].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[3].Value = MyRow[5].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[4].Value = MyRow[6].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[5].Value = MyRow[7].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[6].Value = MyRow[8].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[7].Value = MyRow[9].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[8].Value = MyRow[10].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[9].Value = MyRow[11].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[10].Value = MyRow[12].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[11].Value = MyRow[13].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[0].Value = MyRow[14].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[1].Value = MyRow[15].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[2].Value = MyRow[16].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[3].Value = MyRow[17].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[4].Value = MyRow[18].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[5].Value = MyRow[19].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[6].Value = MyRow[20].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[7].Value = MyRow[21].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[8].Value = MyRow[22].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[9].Value = MyRow[23].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[10].Value = MyRow[24].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[11].Value = MyRow[25].ToString();
                    }
                }
            }
        }
        //------------------------------FillTransmissionGrid--------------------------------------
        private void FillTransmissionGrid(string Num)
        {
            //Clear Grids
            //PlantGV1.DataSource = null;
            //if (PlantGV1.Rows != null) PlantGV1.Rows.Clear();
            //PlantGV2.DataSource = null;
            //if (PlantGV2.Rows != null) PlantGV2.Rows.Clear();

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();

            //detect current date and hour
            System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
            DateTime CurDate = DateTime.Now;
            string day = pr.GetDayOfMonth(CurDate).ToString();
            if (int.Parse(day) < 10) day = "0" + day;
            string mydate = pr.GetYear(CurDate) + "/" + pr.GetMonth(CurDate) + "/" + day;

            Myda.SelectCommand = new SqlCommand("SELECT LineCode,FromBus,ToBus,LineLength,Owner_GencoFrom,Owner_GencoTo" +
            ",RateA,OutService,OutServiceStartDate,OutServiceEndDate FROM [Lines] WHERE LineNumber=" + Num, MyConnection);
            Myda.Fill(MyDS);
            if (Num.Contains("400"))
            {
                //for (int i = 0; i < 7; i++)
                //Grid400.Rows[Grid400.RowCount - 1].Cells[i].ReadOnly = false;
                Grid400.DataSource = MyDS.Tables[0].DefaultView;
                //Grid400.ReadOnly = true;
                int index = 0;
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    Grid400.Rows[index].Cells[7].Value = "Work";
                    Grid400.Rows[index].Cells[Grid400.ColumnCount - 1].ReadOnly = false;
                    if (MyRow[7].ToString() == "True")
                        if (CheckDate(mydate, MyRow[8].ToString(), MyRow[9].ToString()))
                            Grid400.Rows[index].Cells[7].Value = "OutService";

                    index++;
                }
                Grid400.Rows[Grid400.RowCount - 1].Cells[Grid400.ColumnCount - 1].ReadOnly = true;
            }
            else
            {
                Grid230.DataSource = MyDS.Tables[0].DefaultView;
                //for (int i = 0; i < 7; i++)
                //Grid230.Rows[Grid230.RowCount - 1].Cells[i].ReadOnly = false;
                //Grid230.ReadOnly = true;
                int index = 0;
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    Grid230.Rows[index].Cells[7].Value = "Work";
                    Grid230.Rows[index].Cells[Grid230.ColumnCount - 1].ReadOnly = false;
                    if (MyRow[7].ToString() == "True")
                        if (CheckDate(mydate, MyRow[8].ToString(), MyRow[9].ToString()))
                            Grid230.Rows[index].Cells[7].Value = "OutService";

                    index++;
                }
                Grid230.Rows[Grid230.RowCount - 1].Cells[Grid230.ColumnCount - 1].ReadOnly = true;
            }

        }
        //-------------------------FillPlantGrid---------------------------------------
        private void FillPlantGrid(string Num)
        {
            //Clear Grids
            //PlantGV1.DataSource = null;
            //if (PlantGV1.Rows != null) PlantGV1.Rows.Clear();
            //PlantGV2.DataSource = null;
            //if (PlantGV2.Rows != null) PlantGV2.Rows.Clear();

            Currentgb.Text = "CURRENT STATE";
            //Detect Farsi Date
            System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
            DateTime CurDate = DateTime.Now;
            string day = pr.GetDayOfMonth(CurDate).ToString();
            if (int.Parse(day) < 10) day = "0" + day;
            string mydate = pr.GetYear(CurDate) + "/" + pr.GetMonth(CurDate) + "/" + day;
            int myhour = CurDate.Hour;

            //detect the type of packages(Data GridView Labels)

            DataSet dataDS = new DataSet();
            SqlDataAdapter dataDA = new SqlDataAdapter();
            dataDA.SelectCommand = new SqlCommand("SELECT PackageType FROM PPUnit WHERE PPID=" + Num, MyConnection);
            dataDA.Fill(dataDS);
            int count = 0;
            GDGasGroup.Visible = false;
            GDSteamGroup.Visible = false;
            foreach (DataRow MyRow in dataDS.Tables[0].Rows)
            {
                if (count == 0)
                {
                    GDGasGroup.Visible = true;
                    GDGasGroup.Text = MyRow["PackageType"].ToString();
                    string type = MyRow["PackageType"].ToString();
                    type = type.Trim();
                    if (type == "Combined Cycle") type = "CC";
                    PlantGV1.Visible = true;
                    DataSet MyDS = new DataSet();
                    SqlDataAdapter Myda = new SqlDataAdapter();
                    Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageCode," +
                    "UnitType,Capacity,PMin,PMax FROM [UnitsDataMain] WHERE PPID=" + Num + "AND (PackageType LIKE '" + type + "%')", MyConnection);
                    Myda.Fill(MyDS);
                    PlantGV1.DataSource = MyDS.Tables[0].DefaultView;
                    PlantGV1.Sort(PlantGV1.Columns[1], ListSortDirection.Ascending);
                    FillPlantGV1Remained(mydate, Num, type, myhour);
                    GDSteamGroup.Text = "";
                }
                else if (count == 1)
                {
                    GDSteamGroup.Visible = true;
                    GDSteamGroup.Text = MyRow["PackageType"].ToString();
                    string type = MyRow["PackageType"].ToString();
                    type = type.Trim();
                    if (type == "Combined Cycle") type = "CC";
                    PlantGV2.Visible = true;
                    DataSet SteamDS = new DataSet();
                    SqlDataAdapter Steamda = new SqlDataAdapter();
                    Steamda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageCode," +
                    "UnitType,Capacity,PMin,PMax FROM [UnitsDataMain] WHERE PPID=" + Num + "AND (PackageType LIKE '" + type + "%')", MyConnection);
                    Steamda.Fill(SteamDS);
                    PlantGV2.DataSource = SteamDS.Tables[0].DefaultView;
                    PlantGV2.Sort(PlantGV2.Columns[1], ListSortDirection.Ascending);
                    FillPlantGV2Remained(mydate, Num, type, myhour);
                }
                count++;
            }
            if (count == 1) GDSteamGroup.Visible = false;

            MRLabel1.Text = "On Units";
            MRLabel2.Text = "Power";
            Grid230.Visible = false;
            Grid400.Visible = false;
        }
        //----------------------------FillPlantGV1Remained---------------------------
        private void FillPlantGV1Remained(string mydate, string Num, string type, int myhour)
        {
            PlantGV1.Rows[PlantGV1.RowCount - 1].Cells[PlantGV1.ColumnCount - 1].ReadOnly = true;

            //for (int i = 0; i < 3; i++)
            //PlantGV1.Rows[PlantGV1.RowCount - 1].Cells[i].ReadOnly = false;
            for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
            {
                PlantGV1.Rows[i].Cells[PlantGV1.ColumnCount - 1].ReadOnly = false;
                SqlCommand MyCom = new SqlCommand();

                MyCom.CommandText = "SELECT  @md1 =MaintenanceStartDate,@md2=MaintenanceEndDate,@mt=MaintenanceType " +
                "FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type SELECT  " +
                "@fd1 =SecondFuelStartDate,@fd2=SecondFuelEndDate FROM [ConditionUnit] WHERE PPID=@num AND " +
                "UnitCode=@uc AND PackageType=@type SELECT  @osd1 =OutServiceStartDate,@osd2=OutServiceEndDate " +
                "FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type SELECT @re=COUNT(PPID) " +
                "FROM [DetailFRM005] WHERE TargetMarketDate=@date AND PPID=@num AND Block=@block AND Hour=@hour AND Required<>0" +
                "SELECT @sf=SecondFuel FROM [UnitsDataMain] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type";
                MyCom.Connection = MyConnection;

                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = mydate;
                MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                MyCom.Parameters["@num"].Value = Num;
                MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                //detect unit name
                string temp = PlantGV1.Rows[i].Cells[0].Value.ToString();
                temp = temp.ToLower();
                if (type == "CC")
                {
                    int x = int.Parse(Num);
                    if ((x == 131) || (x == 144)) x++;
                    temp = x + "-" + "C" + PlantGV1.Rows[i].Cells[1].Value.ToString();
                }
                else if (temp.Contains("gas"))
                {
                    temp = temp.Replace("gas", "G");
                    temp = Num + "-" + temp;
                }
                else
                {
                    temp = temp.Replace("steam", "S");
                    temp = Num + "-" + temp;
                }
                MyCom.Parameters["@block"].Value = temp;
                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                MyCom.Parameters["@hour"].Value = myhour;
                MyCom.Parameters.Add("@uc", SqlDbType.NChar, 20);
                MyCom.Parameters["@uc"].Value = PlantGV1.Rows[i].Cells[0].Value.ToString();
                MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
                MyCom.Parameters["@type"].Value = type;

                MyCom.Parameters.Add("@md1", SqlDbType.Char, 10);
                MyCom.Parameters["@md1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@md2", SqlDbType.Char, 10);
                MyCom.Parameters["@md2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@mt", SqlDbType.NChar, 12);
                MyCom.Parameters["@mt"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@fd1", SqlDbType.Char, 10);
                MyCom.Parameters["@fd1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@fd2", SqlDbType.Char, 10);
                MyCom.Parameters["@fd2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@osd1", SqlDbType.Char, 10);
                MyCom.Parameters["@osd1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@osd2", SqlDbType.Char, 10);
                MyCom.Parameters["@osd2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@re", SqlDbType.Int);
                MyCom.Parameters["@re"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@sf", SqlDbType.NChar, 10);
                MyCom.Parameters["@sf"].Direction = ParameterDirection.Output;

                MyCom.ExecuteNonQuery();

                string md1, md2, fd1, fd2, osd1, osd2, sf, mt;
                md1 = MyCom.Parameters["@md1"].Value.ToString();
                md2 = MyCom.Parameters["@md2"].Value.ToString();
                fd1 = MyCom.Parameters["@fd1"].Value.ToString();
                fd2 = MyCom.Parameters["@fd2"].Value.ToString();
                osd1 = MyCom.Parameters["@osd1"].Value.ToString();
                osd2 = MyCom.Parameters["@osd2"].Value.ToString();
                sf = MyCom.Parameters["@sf"].Value.ToString();
                mt = MyCom.Parameters["@mt"].Value.ToString();
                int result = int.Parse(MyCom.Parameters["@re"].Value.ToString());

                if (result != 0) PlantGV1.Rows[i].Cells[7].Value = "ON";
                else PlantGV1.Rows[i].Cells[7].Value = "OFF";
                if (CheckDate(mydate, fd1, fd2)) PlantGV1.Rows[i].Cells[6].Value = sf;
                else PlantGV1.Rows[i].Cells[6].Value = "Gas";
                if (CheckDate(mydate, md1, md2)) PlantGV1.Rows[i].Cells[8].Value = mt;
                else PlantGV1.Rows[i].Cells[8].Value = "No";
                if (CheckDate(mydate, osd1, osd2)) PlantGV1.Rows[i].Cells[9].Value = "Yes";
                else PlantGV1.Rows[i].Cells[9].Value = "No";
            }

        }
        //----------------------------FillPlantGV2Remained---------------------------
        private void FillPlantGV2Remained(string mydate, string Num, string type, int myhour)
        {
            PlantGV2.Rows[PlantGV2.RowCount - 1].Cells[PlantGV2.ColumnCount - 1].ReadOnly = true;

            //for (int i = 0; i < 3; i++)
            //PlantGV2.Rows[PlantGV2.RowCount - 1].Cells[i].ReadOnly = false;
            for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
            {
                PlantGV2.Rows[i].Cells[PlantGV2.ColumnCount - 1].ReadOnly = false;
                SqlCommand MyCom = new SqlCommand();

                MyCom.CommandText = "SELECT  @md1 =MaintenanceStartDate,@md2=MaintenanceEndDate,@mt=MaintenanceType " +
                "FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type SELECT  " +
                "@fd1 =SecondFuelStartDate,@fd2=SecondFuelEndDate FROM [ConditionUnit] WHERE PPID=@num AND " +
                "UnitCode=@uc AND PackageType=@type SELECT  @osd1 =OutServiceStartDate,@osd2=OutServiceEndDate " +
                "FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type SELECT @re=COUNT(PPID) " +
                "FROM [DetailFRM005] WHERE TargetMarketDate=@date AND PPID=@num AND Block=@block AND Hour=@hour AND Required<>0" +
                "SELECT @sf=SecondFuel FROM [UnitsDataMain] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type";
                MyCom.Connection = MyConnection;

                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = mydate;
                MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                MyCom.Parameters["@num"].Value = Num;
                MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                //detect unit name
                string temp = PlantGV2.Rows[i].Cells[0].Value.ToString();
                temp = temp.ToLower();
                if (type == "CC")
                {
                    int x = int.Parse(Num);
                    if ((x == 131) || (x == 144)) x++;
                    temp = x + "-" + "C" + PlantGV2.Rows[i].Cells[1].Value.ToString();
                }
                else if (temp.Contains("gas"))
                {
                    temp = temp.Replace("gas", "G");
                    temp = Num + "-" + temp;
                }
                else
                {
                    temp = temp.Replace("steam", "S");
                    temp = Num + "-" + temp;
                }

                MyCom.Parameters["@block"].Value = temp;
                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                MyCom.Parameters["@hour"].Value = myhour;
                MyCom.Parameters.Add("@uc", SqlDbType.NChar, 20);
                MyCom.Parameters["@uc"].Value = PlantGV2.Rows[i].Cells[0].Value.ToString();
                MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
                MyCom.Parameters["@type"].Value = type;

                MyCom.Parameters.Add("@md1", SqlDbType.Char, 10);
                MyCom.Parameters["@md1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@md2", SqlDbType.Char, 10);
                MyCom.Parameters["@md2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@mt", SqlDbType.NChar, 12);
                MyCom.Parameters["@mt"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@fd1", SqlDbType.Char, 10);
                MyCom.Parameters["@fd1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@fd2", SqlDbType.Char, 10);
                MyCom.Parameters["@fd2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@osd1", SqlDbType.Char, 10);
                MyCom.Parameters["@osd1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@osd2", SqlDbType.Char, 10);
                MyCom.Parameters["@osd2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@re", SqlDbType.Int);
                MyCom.Parameters["@re"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@sf", SqlDbType.NChar, 10);
                MyCom.Parameters["@sf"].Direction = ParameterDirection.Output;

                MyCom.ExecuteNonQuery();

                string md1, md2, fd1, fd2, osd1, osd2, sf, mt;
                md1 = MyCom.Parameters["@md1"].Value.ToString();
                md2 = MyCom.Parameters["@md2"].Value.ToString();
                fd1 = MyCom.Parameters["@fd1"].Value.ToString();
                fd2 = MyCom.Parameters["@fd2"].Value.ToString();
                osd1 = MyCom.Parameters["@osd1"].Value.ToString();
                osd2 = MyCom.Parameters["@osd2"].Value.ToString();
                sf = MyCom.Parameters["@sf"].Value.ToString();
                mt = MyCom.Parameters["@mt"].Value.ToString();
                int result = int.Parse(MyCom.Parameters["@re"].Value.ToString());

                if (result != 0) PlantGV2.Rows[i].Cells[7].Value = "ON";
                else PlantGV2.Rows[i].Cells[7].Value = "OFF";
                if (CheckDate(mydate, fd1, fd2)) PlantGV2.Rows[i].Cells[6].Value = sf;
                else PlantGV2.Rows[i].Cells[6].Value = "Gas";
                if (CheckDate(mydate, md1, md2)) PlantGV2.Rows[i].Cells[8].Value = mt;
                else PlantGV2.Rows[i].Cells[8].Value = "No";
                if (CheckDate(mydate, osd1, osd2)) PlantGV2.Rows[i].Cells[9].Value = "Yes";
                else PlantGV2.Rows[i].Cells[9].Value = "No";
            }
        }
        //----------------------------CheckDate--------------------------------
        private bool CheckDate(string date, string start, string end)
        {
            bool result = false;
            string temp1 = date.Remove(4);
            start = start.Trim();
            end = end.Trim();
            if ((start != "") && (end != ""))
            {
                string temp2 = start.Remove(4);
                if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
                else
                {
                    temp1 = date.Remove(7);
                    temp1 = temp1.Remove(0, temp1.Length - 2);
                    temp2 = start.Remove(7);
                    temp2 = temp2.Remove(0, temp2.Length - 2);
                    if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                    else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
                    else
                    {
                        temp1 = date.Remove(0, date.Length - 2);
                        temp2 = start.Remove(0, start.Length - 2);
                        if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                        else result = false;
                    }
                }
                if (!result) return (result);
                else
                {
                    temp1 = date.Remove(4);
                    temp2 = end.Remove(4);
                    if (int.Parse(temp1) < int.Parse(temp2)) return (true);
                    else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
                    else
                    {
                        temp1 = date.Remove(7);
                        temp1 = temp1.Remove(0, temp1.Length - 2);
                        temp2 = end.Remove(7);
                        temp2 = temp2.Remove(0, temp2.Length - 2);
                        if (int.Parse(temp1) < int.Parse(temp2)) return (true);
                        else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
                        else
                        {
                            temp1 = date.Remove(0, date.Length - 2);
                            temp2 = end.Remove(0, end.Length - 2);
                            if ((int.Parse(temp1) < int.Parse(temp2)) || (temp1 == temp2)) return (true);
                            else return (false);
                        }
                    }
                }
            }
            return false;
        }
        //------------------------------button1_Click--------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            //FRM002
            /*for (int num002 = 0; num002 < NumPPID; num002++)
            {
                //build path for FRM002 files
                string Doc002 = "";
                Doc002 += PPIDArray[num002];
                //add date to path name
                Doc002 += "-13880830";
                //read from FRM002.xls into datagridview
                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\\data\\" + Doc002 + ".xls;Extended Properties=Excel 8.0";
                OleDbConnection objConn = new OleDbConnection(sConnectionString);
                objConn.Open();
                string price = "Sheet1";
                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                objAdapter1.SelectCommand = objCmdSelect;
                DataSet objDataset1 = new DataSet();
                objAdapter1.Fill(objDataset1);
                TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                objConn.Close();

                //Insert into DB (MainFRM002)
                string path = @"c:\data\" + Doc002 + ".xls";
                Excel.Application exobj = new Excel.Application();
                Excel.Workbook book = null;
                book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);


                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                int type = 0;
                int PID = 0;
                string date2 = "";
                foreach (Excel.Worksheet workSheet in book.Worksheets)
                    if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                    {
                        if ((((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString().Contains("سيكل")) || (((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString().Contains("ccp")))
                            type = 1;
                        string date1 = ((Excel.Range)workSheet.Cells[4, 2]).Value2.ToString();
                        date2 = date1.Remove(4);
                        date2 += "/";
                        date2 += date1[4];
                        date2 += date1[5];
                        date2 += "/";
                        date2 += date1[6];
                        date2 += date1[7];
                    }

                MyCom.Parameters.Add("@id", SqlDbType.NChar,10);
                MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
                MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
                MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
                MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
                MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

                MyCom.CommandText = "INSERT INTO [MainFRM002] (PPID,TargetMarketDate,PPName,PPType,"
                + "DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
                foreach (Excel.Worksheet workSheet in book.Worksheets)
                    if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                    {
                        PID = findPPID(((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString());
                        //if ((PID==131)&&(type==1)) PID=132;
                        //if ((PID==144)&&(type==1)) PID=145;
                        MyCom.Parameters["@id"].Value = PID;
                        MyCom.Parameters["@tdate"].Value = date2;
                        MyCom.Parameters["@name"].Value = ((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString();
                        MyCom.Parameters["@type"].Value = type;
                        if (((Excel.Range)workSheet.Cells[2, 2]).Value2 != null)
                            MyCom.Parameters["@idate"].Value = ((Excel.Range)workSheet.Cells[2, 2]).Value2.ToString();
                        else MyCom.Parameters["@idate"].Value = null;
                        if (((Excel.Range)workSheet.Cells[3, 2]).Value2 != null)
                            MyCom.Parameters["@time"].Value = ((Excel.Range)workSheet.Cells[3, 2]).Value2.ToString();
                        else MyCom.Parameters["@time"].Value = null;
                        if (((Excel.Range)workSheet.Cells[7, 2]).Value2 != null)
                            MyCom.Parameters["@revision"].Value = ((Excel.Range)workSheet.Cells[7, 2]).Value2.ToString();
                        else MyCom.Parameters["@revision"].Value = 0;
                        if (((Excel.Range)workSheet.Cells[8, 2]).Value2 != null)
                            MyCom.Parameters["@filled"].Value = ((Excel.Range)workSheet.Cells[8, 2]).Value2.ToString();
                        else MyCom.Parameters["@filled"].Value = null;
                        if (((Excel.Range)workSheet.Cells[9, 2]).Value2 != null)
                            MyCom.Parameters["@approved"].Value = ((Excel.Range)workSheet.Cells[9, 2]).Value2.ToString();
                        else MyCom.Parameters["@approved"].Value = null;
                    }
                try
                {
                    MyCom.ExecuteNonQuery();
                }
                catch (Exception exp)
                {
                    string str = exp.Message;
                }

                //Insert into DB (BlockFRM002)
                int x = 10;
                MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                MyCom.Parameters.Add("@peak", SqlDbType.Real);
                MyCom.Parameters.Add("@max", SqlDbType.Real);
                while (x < (TempGV.Rows.Count - 1))
                {
                    if (TempGV.Rows[x].Cells[0].Value.ToString() != "")
                    {
                        MyCom.CommandText = "INSERT INTO [BlockFRM002] (PPID,TargetMarketDate,PPType,Block,"
                        + "Peak,MaxDailyGeneration) VALUES (@id,@tdate,@type,@block,@peak,@max)";

                        //read directly and cell by cell
                        foreach (Excel.Worksheet workSheet in book.Worksheets)
                            if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                            {
                                MyCom.Parameters["@id"].Value = PID;
                                MyCom.Parameters["@tdate"].Value = date2;
                                MyCom.Parameters["@type"].Value = type;
                                MyCom.Parameters["@block"].Value = ((Excel.Range)workSheet.Cells[x+2, 1]).Value2.ToString();
                                if (((Excel.Range)workSheet.Cells[x + 2, 2]).Value2 != null)
                                    MyCom.Parameters["@peak"].Value = ((Excel.Range)workSheet.Cells[x + 2, 2]).Value2.ToString();
                                else MyCom.Parameters["@peak"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[x + 2, 3]).Value2 != null)
                                    MyCom.Parameters["@max"].Value = ((Excel.Range)workSheet.Cells[x + 2, 3]).Value2.ToString();
                                else MyCom.Parameters["@max"].Value = 0;
                            }
                        try
                        {
                            MyCom.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                        }
                    }
                    x++;
                }
                //Insert into DB (DetailFRM002)
                x = 10;
                MyCom.Parameters.Add("@deccap", SqlDbType.Real);
                MyCom.Parameters.Add("@dispachcap", SqlDbType.Real);
                MyCom.Parameters.Add("@price1", SqlDbType.Real);
                MyCom.Parameters.Add("@power1", SqlDbType.Real);
                MyCom.Parameters.Add("@price2", SqlDbType.Real);
                MyCom.Parameters.Add("@power2", SqlDbType.Real);
                MyCom.Parameters.Add("@price3", SqlDbType.Real);
                MyCom.Parameters.Add("@power3", SqlDbType.Real);
                MyCom.Parameters.Add("@price4", SqlDbType.Real);
                MyCom.Parameters.Add("@power4", SqlDbType.Real);
                MyCom.Parameters.Add("@price5", SqlDbType.Real);
                MyCom.Parameters.Add("@power5", SqlDbType.Real);
                MyCom.Parameters.Add("@price6", SqlDbType.Real);
                MyCom.Parameters.Add("@power6", SqlDbType.Real);
                MyCom.Parameters.Add("@price7", SqlDbType.Real);
                MyCom.Parameters.Add("@power7", SqlDbType.Real);
                MyCom.Parameters.Add("@price8", SqlDbType.Real);
                MyCom.Parameters.Add("@power8", SqlDbType.Real);
                MyCom.Parameters.Add("@price9", SqlDbType.Real);
                MyCom.Parameters.Add("@power9", SqlDbType.Real);
                MyCom.Parameters.Add("@price10", SqlDbType.Real);
                MyCom.Parameters.Add("@power10", SqlDbType.Real);
                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                while (x < (TempGV.Rows.Count - 2))
                {
                    if (TempGV.Rows[x].Cells[0].Value.ToString() != "")
                    {
                        for (int j = 0; j < 24; j++)
                        {
                            MyCom.CommandText = "INSERT INTO [DetailFRM002] (TargetMarketDate,PPID,Block,PPType,Hour"+
                            ",DeclaredCapacity,DispachableCapacity,Power1,Price1,Power2,Price2,Power3,Price3,Power4,"+
                            "Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,"+
                            "Price10) VALUES (@tdate,@id,@block,@type,@hour,@deccap,@dispachcap,@power1,"+
                            "@price1,@power2,@price2,@power3,@price3,@power4,@price4,@power5,@price5,@power6,@price6,"+
                            "@power7,@price7,@power8,@price8,@power9,@price9,@power10,@price10)";

                            //read directly and cell by cell
                            foreach (Excel.Worksheet workSheet in book.Worksheets)
                                if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                                {

                                    MyCom.Parameters["@id"].Value = PID;
                                    MyCom.Parameters["@tdate"].Value = date2;
                                    MyCom.Parameters["@block"].Value = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString();
                                    MyCom.Parameters["@type"].Value = type;
                                    MyCom.Parameters["@hour"].Value = j + 1;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 5]).Value2 != null)
                                        MyCom.Parameters["@deccap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 5]).Value2.ToString();
                                    else MyCom.Parameters["@deccap"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2 != null)
                                        MyCom.Parameters["@dispachcap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2.ToString();
                                    else MyCom.Parameters["@dispachcap"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2 != null)
                                        MyCom.Parameters["@power1"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2.ToString();
                                    else MyCom.Parameters["@power1"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 8]).Value2 != null)
                                        MyCom.Parameters["@price1"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 8]).Value2.ToString();
                                    else MyCom.Parameters["@price1"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 9]).Value2 != null)
                                        MyCom.Parameters["@power2"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 9]).Value2.ToString();
                                    else MyCom.Parameters["@power2"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 10]).Value2 != null)
                                        MyCom.Parameters["@price2"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 10]).Value2.ToString();
                                    else MyCom.Parameters["@price2"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 11]).Value2 != null)
                                        MyCom.Parameters["@power3"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 11]).Value2.ToString();
                                    else MyCom.Parameters["@power3"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 12]).Value2 != null)
                                        MyCom.Parameters["@price3"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 12]).Value2.ToString();
                                    else MyCom.Parameters["@price3"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 13]).Value2 != null)
                                        MyCom.Parameters["@power4"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 13]).Value2.ToString();
                                    else MyCom.Parameters["@power4"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 14]).Value2 != null)
                                        MyCom.Parameters["@price4"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 14]).Value2.ToString();
                                    else MyCom.Parameters["@price4"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 15]).Value2 != null)
                                        MyCom.Parameters["@power5"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 15]).Value2.ToString();
                                    else MyCom.Parameters["@power5"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 16]).Value2 != null)
                                        MyCom.Parameters["@price5"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 16]).Value2.ToString();
                                    else MyCom.Parameters["@price5"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 17]).Value2 != null)
                                        MyCom.Parameters["@power6"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 17]).Value2.ToString();
                                    else MyCom.Parameters["@power6"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 18]).Value2 != null)
                                        MyCom.Parameters["@price6"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 18]).Value2.ToString();
                                    else MyCom.Parameters["@price6"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 19]).Value2 != null)
                                        MyCom.Parameters["@power7"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 19]).Value2.ToString();
                                    else MyCom.Parameters["@power7"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 20]).Value2 != null)
                                        MyCom.Parameters["@price7"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 20]).Value2.ToString();
                                    else MyCom.Parameters["@price7"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 21]).Value2 != null)
                                        MyCom.Parameters["@power8"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 21]).Value2.ToString();
                                    else MyCom.Parameters["@power8"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 22]).Value2 != null)
                                        MyCom.Parameters["@price8"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 22]).Value2.ToString();
                                    else MyCom.Parameters["@price8"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 23]).Value2 != null)
                                        MyCom.Parameters["@power9"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 23]).Value2.ToString();
                                    else MyCom.Parameters["@power9"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 24]).Value2 != null)
                                        MyCom.Parameters["@price9"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 24]).Value2.ToString();
                                    else MyCom.Parameters["@price9"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 25]).Value2 != null)
                                        MyCom.Parameters["@power10"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 25]).Value2.ToString();
                                    else MyCom.Parameters["@power10"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 26]).Value2 != null)
                                        MyCom.Parameters["@price10"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 26]).Value2.ToString();
                                    else MyCom.Parameters["@price10"].Value = 0;
                                }
                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                            }
                        }
                    }
                    x++;
                }

             
                book.Close(false, book, Type.Missing);
            }*/

            //FRM005
            /*for (int num = 0; num < NumPPID; num++)
            {

                //build path for FRM005 files
                string DocName = "005-";
                DocName += PPIDArray[num];
                //add date to path name
                DocName += "-13880830";
                //read from FRM005.xls into datagridview
                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\\data\\"+DocName+".xls;Extended Properties=Excel 8.0";
                OleDbConnection objConn = new OleDbConnection(sConnectionString);
                objConn.Open();
                string price = "FRM005";
                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                objAdapter1.SelectCommand = objCmdSelect;
                DataSet objDataset1 = new DataSet();
                objAdapter1.Fill(objDataset1);
                TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                objConn.Close();

                //Insert into DB (MainFRM005)
                string path = @"c:\data\"+DocName+ ".xls";
                Excel.Application exobj = new Excel.Application();
                Excel.Workbook book = null;
                book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                int type = 0;
                int PID=0;
                if ((TempGV.Rows[3].Cells[1].Value.ToString().Contains("سيكل")) || (TempGV.Rows[3].Cells[1].Value.ToString().Contains("ccp")))
                    type = 1;
                MyCom.Parameters.Add("@id", SqlDbType.NChar,10);
                MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
                MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
                MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
                MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
                MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

                MyCom.CommandText = "INSERT INTO [MainFRM005] (PPID,TargetMarketDate,PPName,PPType,"
                +"DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
                PID=findPPID(TempGV.Rows[3].Cells[1].Value.ToString());
                //if ((PID==131)&&(type==1)) PID=132;
                //if ((PID==144)&&(type==1)) PID=145;
                MyCom.Parameters["@id"].Value = PID;
                MyCom.Parameters["@tdate"].Value = TempGV.Rows[2].Cells[1].Value.ToString();
                MyCom.Parameters["@name"].Value = TempGV.Rows[3].Cells[1].Value.ToString();
                MyCom.Parameters["@type"].Value = type;
                MyCom.Parameters["@idate"].Value = TempGV.Rows[0].Cells[1].Value.ToString();
                MyCom.Parameters["@time"].Value = TempGV.Rows[1].Cells[1].Value.ToString();
                MyCom.Parameters["@revision"].Value = TempGV.Rows[4].Cells[1].Value.ToString();
                MyCom.Parameters["@filled"].Value = TempGV.Rows[5].Cells[1].Value.ToString();
                MyCom.Parameters["@approved"].Value = TempGV.Rows[6].Cells[1].Value.ToString();

                try
                {
                    MyCom.ExecuteNonQuery();
                }
                catch (Exception exp)
                {
                    string str = exp.Message;
                }

                //Insert into DB (BlockFRM005)
                int x = 10;
                MyCom.Parameters.Add("@num", SqlDbType.NChar, 20);
                MyCom.Parameters.Add("@prequired", SqlDbType.Real);
                MyCom.Parameters.Add("@pdispach", SqlDbType.Real);
                MyCom.Parameters.Add("@drequierd", SqlDbType.Real);
                MyCom.Parameters.Add("@ddispach", SqlDbType.Real);
                while (x < (TempGV.Rows.Count - 1))
                {
                    if (TempGV.Rows[x].Cells[0].Value.ToString() != "")
                    {
                        MyCom.CommandText = "INSERT INTO [BlockFRM005] (PPID,TargetMarketDate,PPType,Block,"
                        +"PeakRequired,PeakDispatchable,DailyTotalRequired,DailyTotalDispachable)"
                        + "VALUES (@id,@tdate,@type,@num,@prequired,@pdispach,@drequierd,@ddispach)";

                        MyCom.Parameters["@id"].Value = PID;
                        MyCom.Parameters["@tdate"].Value = TempGV.Rows[2].Cells[1].Value.ToString();
                        MyCom.Parameters["@num"].Value = TempGV.Rows[x].Cells[0].Value.ToString();
                        MyCom.Parameters["@type"].Value = type;
                        //read directly and cell by cell
                        foreach (Excel.Worksheet workSheet in book.Worksheets)
                            if (workSheet.Name == "FRM005")
                            {
                                if (((Excel.Range)workSheet.Cells[x + 2, 27]).Value2 != null)
                                    MyCom.Parameters["@prequired"].Value = ((Excel.Range)workSheet.Cells[x + 2, 27]).Value2.ToString();
                                else MyCom.Parameters["@prequired"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[x + 3, 27]).Value2 != null)
                                    MyCom.Parameters["@pdispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, 27]).Value2.ToString();
                                else MyCom.Parameters["@pdispach"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[x + 2, 28]).Value2 != null)
                                    MyCom.Parameters["@drequierd"].Value = ((Excel.Range)workSheet.Cells[x + 2, 28]).Value2.ToString();
                                else MyCom.Parameters["@drequierd"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[x + 3, 28]).Value2 != null)
                                    MyCom.Parameters["@ddispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, 28]).Value2.ToString();
                                else MyCom.Parameters["@ddispach"].Value = 0;
                            }
                        try
                        {
                            MyCom.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                        }
                    }
                    x++;
                }
                //Insert into DB (DetailFRM005)
                x = 10;
                MyCom.Parameters.Add("@required", SqlDbType.Real);
                MyCom.Parameters.Add("@dispach", SqlDbType.Real);
                MyCom.Parameters.Add("@contribution", SqlDbType.Char, 2);
                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                while (x < (TempGV.Rows.Count - 2))
                {
                    if (TempGV.Rows[x].Cells[0].Value.ToString() != "")
                    {
                        for (int j = 0; j < 24; j++)
                        {
                            MyCom.CommandText = "INSERT INTO [DetailFRM005] (TargetMarketDate,PPID,Block,PPType,Hour,Required,Dispatchable,Contribution) VALUES (@tdate,@id,@num,@type,@hour,@required,@dispach,@contribution)";

                            MyCom.Parameters["@id"].Value = PID;
                            MyCom.Parameters["@tdate"].Value = TempGV.Rows[2].Cells[1].Value.ToString();
                            MyCom.Parameters["@num"].Value = TempGV.Rows[x].Cells[0].Value.ToString();
                            MyCom.Parameters["@type"].Value = type;

                            //read directly and cell by cell
                            foreach (Excel.Worksheet workSheet in book.Worksheets)
                                if (workSheet.Name == "FRM005")
                                {
                                    MyCom.Parameters["@hour"].Value = j + 1;
                                    if (((Excel.Range)workSheet.Cells[x + 2, j + 3]).Value2 != null)
                                        MyCom.Parameters["@required"].Value = ((Excel.Range)workSheet.Cells[x + 2, j + 3]).Value2.ToString();
                                    else MyCom.Parameters["@required"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 3, j + 3]).Value2 != null)
                                        MyCom.Parameters["@dispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, j + 3]).Value2.ToString();
                                    else MyCom.Parameters["@dispach"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2 != null)
                                        MyCom.Parameters["@contribution"].Value = ((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2.ToString();
                                    else MyCom.Parameters["@contribution"].Value = null;
                                }
                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                            }
                        }
                    }
                    x++;
                }
               
                book.Close(false, book, Type.Missing);
            }*/
        }
        //------------------------------findPPID-----------------------------
        private int findPPID(string name)
        {
            int ID = 0;
            if (name.Contains("بعثت")) ID = 101;
            else if (name.Contains("پرند")) ID = 104;
            else if (name.Contains("منتظر")) ID = 131;
            else if ((name.Contains("فيروزي")) || (name.Contains("طرشت"))) ID = 133;
            else if ((!name.Contains("بخاري")) && (name.Contains("ري"))) ID = 138;
            else if ((name.Contains("رجائي")) || (name.Contains("رجايي"))) ID = 144;
            else if ((name.Contains("قم")) || (name.Contains("Qom"))) ID = 149;
            return ID;
        }
        //------------------------SetHeader2Plant----------------------------
        private void SetHeader2Plant(string package, string unit, string type)
        {
            GDHeaderGB.Visible = true;
            ODHeaderGB.Visible = true;
            MRHeaderGB.Visible = true;
            BDHeaderGb.Visible = true;
            FRHeaderGb.Visible = true;
            GDNewBtn.Visible = true;
            GDDeleteBtn.Visible = true;
            ODSaveBtn.Visible = true;
            FROKBtn.Visible = true;
            FRUpdateBtn.Visible = true;
            BDPlotBtn.Visible = true;
            MRPlotBtn.Visible = true;
            GDPlantLb.Visible = true;
            ODPlantLb.Visible = true;
            FRPlantLb.Visible = true;
            BDPlantLb.Visible = true;
            MRPlantLb.Visible = true;
            Point pnt = new Point(62, 23);
            GDPlantLb.Location = pnt;
            ODPlantLb.Location = pnt;
            MRPlantLb.Location = pnt;
            BDPlantLb.Location = pnt;
            FRPlantLb.Location = pnt;
            FRPlantPanel.Visible = false;
            FRUnitPanel.Visible = true;
            MRUnitCurGb.Visible = true;
            MRPlantCurGb.Visible = false;
            ODUnitPanel.Visible = true;
            ODPlantPanel.Visible = false;
            GDUnitPanel.Visible = true;
            GDGasGroup.Visible = false;
            GDSteamGroup.Visible = false;
            GDHeaderPanel.Visible = true;
            ODHeaderPanel.Visible = true;
            MRHeaderPanel.Visible = true;
            FRHeaderPanel.Visible = true;
            BDHeaderPanel.Visible = true;
            BDCur1.Visible = true;
            BDCur2.Visible = true;
            L1.Visible = true;
            L1.Text = "Plant: ";
            L5.Visible = true;
            L5.Text = "Plant: ";
            L9.Visible = true;
            L9.Text = "Plant: ";
            L13.Visible = true;
            L13.Text = "Plant: ";
            L17.Visible = true;
            L17.Text = "Plant: ";
            Currentgb.Text = "CURRENT STATE";
            Maintenancegb.Text = "MAINTENANCE";
            GDPmaxLb.Text = "Pmax";
            GDPminLb.Text = "Pmin";
            GDTUpLb.Text = "Time Up";
            GDTimeDownLb.Text = "Time Down";
            GDColdLb.Text = "Time Cold Start";
            GDHotLb.Text = "Time Hot Start";
            GDRampLb.Text = "Ramp Rate";
            GDConsLb.Text = "Internal Consume";
            GDStateLb.Text = "STATE";
            GDFuelLb.Text = "FUEL";
            GDPackLb.Text = package;
            ODPackLb.Text = package;
            MRPackLb.Text = package;
            BDPackLb.Text = package;
            FRPackLb.Text = package;
            GDUnitLb.Text = unit;
            ODUnitLb.Text = unit;
            MRUnitLb.Text = unit;
            BDUnitLb.Text = unit;
            FRUnitLb.Text = unit;
            GDTypeLb.Text = type;
            ODTypeLb.Text = type;
            MRTypeLb.Text = type;
            BDTypeLb.Text = type;
            FRTypeLb.Text = type;
            BDCur1.Visible = true;
            BDCur2.Visible = true;
            MRGB.Visible = true;
            GDDeleteBtn.Text = "Edit Mode";
            GDNewBtn.Enabled = false;
            GDNewBtn.Text = "Save";
            FROKBtn.Visible = true;
            FRUpdateBtn.Enabled = false;
            //??????????????????
            FRUpdateBtn.Enabled = true;
            FRUnitCapitalTb.ReadOnly = true;
            FRUnitFixedTb.ReadOnly = true;
            FRUnitVariableTb.ReadOnly = true;
            FRUnitAmargTb1.ReadOnly = true;
            FRUnitBmargTb1.ReadOnly = true;
            FRUnitCmargTb1.ReadOnly = true;
            FRUnitAmargTb2.ReadOnly = true;
            FRUnitBmargTb2.ReadOnly = true;
            FRUnitCmargTb2.ReadOnly = true;
            FRUnitAmainTb.ReadOnly = true;
            FRUnitBmainTb.ReadOnly = true;
            FRUnitColdTb.ReadOnly = true;
            FRUnitHotTb.ReadOnly = true;
        }
        //------------------------SetHeader1Plant---------------------------
        private void SetHeader1Plant()
        {
            GDHeaderGB.Visible = true;
            ODHeaderGB.Visible = true;
            MRHeaderGB.Visible = true;
            BDHeaderGb.Visible = true;
            FRHeaderGb.Visible = true;
            GDNewBtn.Visible = true;
            GDDeleteBtn.Visible = true;
            ODSaveBtn.Visible = true;
            BDPlotBtn.Visible = false;
            MRPlotBtn.Visible = true;
            FROKBtn.Visible = false;
            FRUpdateBtn.Visible = false;
            GDPlantLb.Visible = true;
            ODPlantLb.Visible = true;
            FRPlantLb.Visible = true;
            BDPlantLb.Visible = true;
            MRPlantLb.Visible = true;
            Point pnt = new Point(62, 23);
            GDPlantLb.Location = pnt;
            ODPlantLb.Location = pnt;
            MRPlantLb.Location = pnt;
            BDPlantLb.Location = pnt;
            FRPlantLb.Location = pnt;
            FRPlantPanel.Visible = true;
            FRUnitPanel.Visible = false;
            MRUnitCurGb.Visible = false;
            MRPlantCurGb.Visible = true;
            ODUnitPanel.Visible = false;
            ODPlantPanel.Visible = true;
            GDUnitPanel.Visible = false;
            GDGasGroup.Visible = true;
            GDSteamGroup.Visible = true;
            GDHeaderPanel.Visible = false;
            ODHeaderPanel.Visible = false;
            MRHeaderPanel.Visible = false;
            FRHeaderPanel.Visible = false;
            BDHeaderPanel.Visible = false;
            Grid230.Visible = false;
            Grid400.Visible = false;
            PlantGV1.Visible = true;
            PlantGV2.Visible = true;
            BDCur1.Visible = false;
            BDCur2.Visible = false;
            L1.Visible = true;
            L1.Text = "Plant: ";
            L5.Visible = true;
            L5.Text = "Plant: ";
            L9.Visible = true;
            L9.Text = "Plant: ";
            L13.Visible = true;
            L13.Text = "Plant: ";
            L17.Visible = true;
            L17.Text = "Plant: ";
            BDCur1.Visible = false;
            BDCur2.Visible = false;
            MRGB.Visible = true;
            MRLabel1.Text = "ON UNITS";
            MRLabel2.Text = "POWER";
            GDDeleteBtn.Text = "Delete Unit";
            GDNewBtn.Enabled = true;
            GDNewBtn.Text = "Add Unit";
            //??????????????????age faghat neveshtan dar DB dashte bashe doroste???????????
            FRUpdateBtn.Enabled = true;
            FROKBtn.Visible = false;
        }
        //-----------------------------SetHeader1Transmission------------------------
        private void SetHeader1Transmission()
        {
            GDHeaderGB.Visible = true;
            ODHeaderGB.Visible = true;
            MRHeaderGB.Visible = true;
            BDHeaderGb.Visible = true;
            FRHeaderGb.Visible = true;
            GDNewBtn.Visible = true;
            GDDeleteBtn.Visible = true;
            ODSaveBtn.Visible = false;
            BDPlotBtn.Visible = false;
            MRPlotBtn.Visible = true;
            FROKBtn.Visible = false;
            FRUpdateBtn.Visible = false;
            GDHeaderPanel.Visible = false;
            ODHeaderPanel.Visible = false;
            MRHeaderPanel.Visible = false;
            FRHeaderPanel.Visible = false;
            BDHeaderPanel.Visible = false;
            FRPlantPanel.Visible = false;
            FRUnitPanel.Visible = false;
            ODUnitPanel.Visible = false;
            ODPlantPanel.Visible = false;
            BDCur1.Visible = false;
            BDCur2.Visible = false;
            GDPlantLb.Text = "Tehran";
            GDPlantLb.Visible = true;
            MRPlantLb.Visible = true;
            ODPlantLb.Visible = true;
            FRPlantLb.Visible = true;
            BDPlantLb.Visible = true;
            Point pnt = new Point(100, 23);
            GDPlantLb.Location = pnt;
            ODPlantLb.Location = pnt;
            MRPlantLb.Location = pnt;
            BDPlantLb.Location = pnt;
            FRPlantLb.Location = pnt;
            L1.Visible = true;
            L1.Text = "Transmission: ";
            //L5.Visible = false;
            L9.Visible = true;
            L9.Text = "Transmission: ";
            //L13.Visible = false;
            //L17.Visible = false;
            L5.Text = "Transmission: ";
            L13.Text = "Transmission: ";
            L17.Text = "Transmission: ";
            Grid230.Visible = true;
            Grid400.Visible = true;
            PlantGV1.Visible = false;
            PlantGV2.Visible = false;
            Currentgb.Text = "OTHER OWNER";
            MRLabel1.Text = "Input Power";
            MRLabel2.Text = "Output Power";
            GDGasGroup.Text = "400";
            GDSteamGroup.Text = "230";
            GDGasGroup.Visible = true;
            GDSteamGroup.Visible = true;
            GDUnitPanel.Visible = false;
            MRGB.Visible = true;
            MRUnitCurGb.Visible = false;
            MRPlantCurGb.Visible = true;
            MRLabel1.Text = "Input Power";
            MRLabel2.Text = "Output Power";
            GDDeleteBtn.Text = "Delete Line";
            GDNewBtn.Enabled = true;
            GDNewBtn.Text = "Add Line";
        }
        //------------------------------SetHeader2Transmission----------------------
        private void SetHeader2Transmission()
        {
            GDHeaderGB.Visible = true;
            ODHeaderGB.Visible = true;
            MRHeaderGB.Visible = true;
            BDHeaderGb.Visible = true;
            FRHeaderGb.Visible = true;
            GDNewBtn.Visible = true;
            GDDeleteBtn.Visible = true;
            ODSaveBtn.Visible = false;
            BDPlotBtn.Visible = false;
            MRPlotBtn.Visible = true;
            FROKBtn.Visible = false;
            FRUpdateBtn.Visible = false;
            GDHeaderPanel.Visible = false;
            ODHeaderPanel.Visible = false;
            MRHeaderPanel.Visible = false;
            FRHeaderPanel.Visible = false;
            BDHeaderPanel.Visible = false;
            GDPlantLb.Visible = true;
            MRPlantLb.Visible = true;
            ODPlantLb.Visible = true;
            FRPlantLb.Visible = true;
            BDPlantLb.Visible = true;
            Point pnt = new Point(62, 23);
            GDPlantLb.Location = pnt;
            ODPlantLb.Location = pnt;
            MRPlantLb.Location = pnt;
            BDPlantLb.Location = pnt;
            FRPlantLb.Location = pnt;
            L1.Visible = true;
            L1.Text = "Line: ";
            //L5.Visible = false;
            L9.Visible = true;
            L9.Text = "Line: ";
            //L13.Visible = false;
            //L17.Visible = false;
            L5.Text = "Line: ";
            L13.Text = "Line: ";
            L17.Text = "Line: ";
            Currentgb.Text = "OTHER OWNER";
            Maintenancegb.Text = "OUT SERVICE";
            GDPmaxLb.Text = "From Bus";
            GDPminLb.Text = "To Bus";
            GDTUpLb.Text = "Circuit ID";
            GDTimeDownLb.Text = "Line-R";
            GDColdLb.Text = "Line-X";
            GDHotLb.Text = "Suseptance";
            GDRampLb.Text = "Line Length";
            GDConsLb.Text = "Status";
            GDStateLb.Text = "Genco";
            GDFuelLb.Text = "Percent";
            GDGasGroup.Visible = false;
            GDSteamGroup.Visible = false;
            GDUnitPanel.Visible = true;
            MRGB.Visible = true;
            MRUnitCurGb.Visible = false;
            MRPlantCurGb.Visible = true;
            GDDeleteBtn.Text = "Edit Mode";
            GDNewBtn.Enabled = false;
            GDNewBtn.Text = "Save";
        }
        //--------------------------ODSaveBtn_Click--------------------------
        private void ODSaveBtn_Click(object sender, EventArgs e)
        {
            //A Unit is Selected
            if (ODUnitPanel.Visible)
            {
                bool check = true;
                if (ODUnitOutCheck.Checked)
                {
                    if (ODUnitServiceStartDate.IsNull)
                    {
                        odunitosd1Valid.Visible = true;
                        check = false;
                    }
                    if (ODUnitServiceEndDate.IsNull)
                    {
                        odunitosd2Valid.Visible = true;
                        check = false;
                    }
                }
                if (ODUnitMainCheck.Checked)
                {
                    if (ODUnitMainStartDate.IsNull)
                    {
                        odunitmd1Valid.Visible = true;
                        check = false;
                    }
                    if (ODUnitMainEndDate.IsNull)
                    {
                        odunitmd2Valid.Visible = true;
                        check = false;
                    }
                }
                if (ODUnitFuelCheck.Checked)
                {
                    if (ODUnitFuelStartDate.IsNull)
                    {
                        odunitsfd1Valid.Visible = true;
                        check = false;
                    }
                    if (ODUnitFuelEndDate.IsNull)
                    {
                        odunitsfd2Valid.Visible = true;
                        check = false;
                    }
                    if (errorProvider1.GetError(ODUnitFuelTB) != "")
                        check = false;
                }
                if (ODUnitPowerCheck.Checked)
                {
                    if (ODUnitPowerStartDate.IsNull)
                    {
                        odunitpd1Valid.Visible = true;
                        check = false;
                    }
                    if (ODUnitPowerEndDate.IsNull)
                    {
                        odunitpd2Valid.Visible = true;
                        check = false;
                    }
                    if (errorProvider1.GetError(ODUnitPowerGrid1) != "") check = false;
                    if (errorProvider1.GetError(ODUnitPowerGrid2) != "") check = false;
                }

                if (!check) MessageBox.Show("Please Fill Marked Fields With Valid Values");
                else
                {
 
                    //Is There a Row in DB for this Unit?
                    SqlCommand MyCom = new SqlCommand();
                    MyCom.CommandText = "SELECT  @result1 =count(PPID) FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@unit " +
                    "AND PackageType=@type SELECT @result2=COUNT(PPID) FROM PowerLimitedUnit WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type";
                    MyCom.Connection = MyConnection;
                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                    MyCom.Parameters["@num"].Value = PPID;
                    MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
                    MyCom.Parameters["@unit"].Value = ODUnitLb.Text;
                    string type = ODPackLb.Text;
                    if (type.Contains("Combined")) type = "CC";
                    MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
                    MyCom.Parameters["@type"].Value = type;
                    MyCom.Parameters.Add("@result1", SqlDbType.Int);
                    MyCom.Parameters["@result1"].Direction = ParameterDirection.Output;
                    MyCom.Parameters.Add("@result2", SqlDbType.Int);
                    MyCom.Parameters["@result2"].Direction = ParameterDirection.Output;
                    //try
                    // {
                    MyCom.ExecuteNonQuery();
                    // }
                    //catch()
                    //{
                    //}
                    int result1;
                    result1 = (int)MyCom.Parameters["@result1"].Value;
                    int result2;
                    result2 = (int)MyCom.Parameters["@result2"].Value;
                    if (result1 == 0)
                    {
                        MyCom.CommandText = "INSERT INTO [ConditionUnit] (PPID,UnitCode,PackageType,OutService,OutServiceStartDate," +
                        "OutServiceEndDate,NoOn,NoOff,Maintenance,MaintenanceStartDate,MaintenanceEndDate,SecondFuel," +
                        "SecondFuelStartDate,SecondFuelEndDate,FuelForOneDay) VALUES (@num,@unit,@type,@os,@osd1,@osd2," +
                        "@on,@off,@m,@md1,@md2,@sf,@sfd1,@sfd2,@sfq) ";
                    }
                    else
                    {
                        MyCom.CommandText = "UPDATE [ConditionUnit] SET OutService=@os,OutServiceStartDate=@osd1," +
                       "OutServiceEndDate=@osd2,NoOn=@on,NoOff=@off,Maintenance=@m,MaintenanceStartDate=@md1," +
                       "MaintenanceEndDate=@md2,SecondFuel=@sf,SecondFuelStartDate=@sfd1,SecondFuelEndDate=@sfd2," +
                       "FuelForOneDay=@sfq WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type";

                    }
                    MyCom.Parameters.Add("@os", SqlDbType.Bit);
                    MyCom.Parameters["@os"].Value = ODUnitOutCheck.Checked;
                    MyCom.Parameters.Add("@osd1", SqlDbType.Char, 10);
                    if (ODUnitOutCheck.Checked)
                        MyCom.Parameters["@osd1"].Value = ODUnitServiceStartDate.Text;
                    else MyCom.Parameters["@osd1"].Value = "";
                    MyCom.Parameters.Add("@osd2", SqlDbType.Char, 10);
                    if (ODUnitOutCheck.Checked)
                        MyCom.Parameters["@osd2"].Value = ODUnitServiceEndDate.Text;
                    else MyCom.Parameters["@osd2"].Value = "";
                    MyCom.Parameters.Add("@on", SqlDbType.Bit);
                    MyCom.Parameters["@on"].Value = ODUnitNoOnCheck.Checked;
                    MyCom.Parameters.Add("@off", SqlDbType.Bit);
                    MyCom.Parameters["@off"].Value = ODUnitNoOffCheck.Checked;
                    MyCom.Parameters.Add("@m", SqlDbType.Bit);
                    MyCom.Parameters["@m"].Value = ODUnitMainCheck.Checked;
                    MyCom.Parameters.Add("@md1", SqlDbType.Char, 10);
                    if (ODUnitMainCheck.Checked)
                        MyCom.Parameters["@md1"].Value = ODUnitMainStartDate.Text;
                    else MyCom.Parameters["@md1"].Value = "";
                    MyCom.Parameters.Add("@md2", SqlDbType.Char, 10);
                    if (ODUnitMainCheck.Checked)
                        MyCom.Parameters["@md2"].Value = ODUnitMainEndDate.Text;
                    else MyCom.Parameters["@md2"].Value = "";
                    MyCom.Parameters.Add("@sf", SqlDbType.Bit);
                    MyCom.Parameters["@sf"].Value = ODUnitFuelCheck.Checked;
                    MyCom.Parameters.Add("@sfd1", SqlDbType.Char, 10);
                    if (ODUnitFuelCheck.Checked)
                        MyCom.Parameters["@sfd1"].Value = ODUnitFuelStartDate.Text;
                    else MyCom.Parameters["@sfd1"].Value = "";
                    MyCom.Parameters.Add("@sfd2", SqlDbType.Char, 10);
                    if (ODUnitFuelCheck.Checked)
                        MyCom.Parameters["@sfd2"].Value = ODUnitFuelEndDate.Text;
                    else MyCom.Parameters["@sfd2"].Value = "";
                    MyCom.Parameters.Add("@sfq", SqlDbType.Real);
                    if ((ODUnitFuelCheck.Checked) && (ODUnitFuelTB.Text != ""))
                        MyCom.Parameters["@sfq"].Value = ODUnitFuelTB.Text;
                    else MyCom.Parameters["@sfq"].Value = "0";

                    //try
                    //{
                    MyCom.ExecuteNonQuery();
                    //}
                    //catch (Exception exp)
                    //{
                    //  string str = exp.Message;
                    //}

                    if (ODUnitPowerCheck.Checked)
                    {
                        MyCom.CommandText = "INSERT INTO [PowerLimitedUnit] (PPID,UnitCode," +
                        "PackageType,StartDate,EndDate,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10," +
                        "Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24) " +
                        "VALUES (@num,@unit,@type,@date1,@date2,@h1,@h2,@h3,@h4,@h5,@h6,@h7,@h8,@h9,@h10,@h11,@h12,@h13," +
                        "@h14,@h15,@h16,@h17,@h18,@h19,@h20,@h21,@h22,@h23,@h24)";
                        if (result2 == 1)
                        {
                            //MyCom.CommandText = "SELECT @result=COUNT(PPID) FROM PowerLimitedUnit WHERE PPID=@num AND "+
                            //"UnitCode=@unit AND PackageType=@type AND StartDate=@Sdate AND EndDate=@Edate";
                            //MyCom.Parameters.Add("@Sdate", SqlDbType.Char, 10);
                            //MyCom.Parameters["@Sdate"].Value = ODUnitPowerStartDate.Text;
                            //MyCom.Parameters.Add("@Edate", SqlDbType.Char, 10);
                            //MyCom.Parameters["@Edate"].Value = ODUnitPowerEndDate.Text;
                            //MyCom.Parameters.Add("@result", SqlDbType.Int);
                            //MyCom.Parameters["@result"].Direction = ParameterDirection.Output;
                            ////try
                            //// {
                            //MyCom.ExecuteNonQuery();
                            //// }
                            ////catch()
                            ////{
                            ////}
                            //int result;
                            //result = (int)MyCom.Parameters["@result"].Value;
                            //if (result == 1)
                            //{
                            MyCom.CommandText = "UPDATE [PowerLimitedUnit] SET Hour1=@h1,Hour2=@h2,Hour3=@h3," +
                            "Hour4=@h4,Hour5=@h5,Hour6=@h6,Hour7=@h7,Hour8=@h8,Hour9=@h9,Hour10=@h10,Hour11=@h11," +
                            "Hour12=@h12,Hour13=@h13,Hour14=@h14,Hour15=@h15,Hour16=@h16,Hour17=@h17,Hour18=@h18," +
                            "Hour19=@h19,Hour20=@h20,Hour21=@h21,Hour22=@h22,Hour23=@h23,Hour24=@h24,StartDate=@date1," +
                            "EndDate=@date2 WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type";
                            //}
                            //else
                            //{
                            //    MyCom.CommandText = "INSERT INTO [PowerLimitedUnit] (PPID,UnitCode," +
                            //    "PackageType,StartDate,EndDate,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10," +
                            //    "Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24) " +
                            //    "VALUES (@num,@unit,@type,@date1,@date2,@h1,@h2,@h3,@h4,@h5,@h6,@h7,@h8,@h9,@h10,@h11,@h12,@h13," +
                            //    "@h14,@h15,@h16,@h17,@h18,@h19,@h20,@h21,@h22,@h23,@h24)";
                            //}
                        }
                        MyCom.Parameters.Add("@date1", SqlDbType.Char, 10);
                        MyCom.Parameters["@date1"].Value = ODUnitPowerStartDate.Text;
                        MyCom.Parameters.Add("@date2", SqlDbType.Char, 10);
                        MyCom.Parameters["@date2"].Value = ODUnitPowerEndDate.Text;
                        MyCom.Parameters.Add("@h1", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[0].Value != null))
                            MyCom.Parameters["@h1"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[0].Value.ToString());
                        else MyCom.Parameters["@h1"].Value = 0;
                        MyCom.Parameters.Add("@h2", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[1].Value != null))
                            MyCom.Parameters["@h2"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[1].Value.ToString());
                        else MyCom.Parameters["@h2"].Value = 0;
                        MyCom.Parameters.Add("@h3", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[2].Value != null))
                            MyCom.Parameters["@h3"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[2].Value.ToString());
                        else MyCom.Parameters["@h3"].Value = 0;
                        MyCom.Parameters.Add("@h4", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[3].Value != null))
                            MyCom.Parameters["@h4"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[3].Value.ToString());
                        else MyCom.Parameters["@h4"].Value = 0;
                        MyCom.Parameters.Add("@h5", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[4].Value != null))
                            MyCom.Parameters["@h5"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[4].Value.ToString());
                        else MyCom.Parameters["@h5"].Value = 0;
                        MyCom.Parameters.Add("@h6", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[5].Value != null))
                            MyCom.Parameters["@h6"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[5].Value.ToString());
                        else MyCom.Parameters["@h6"].Value = 0;
                        MyCom.Parameters.Add("@h7", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[6].Value != null))
                            MyCom.Parameters["@h7"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[6].Value.ToString());
                        else MyCom.Parameters["@h7"].Value = 0;
                        MyCom.Parameters.Add("@h8", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[7].Value != null))
                            MyCom.Parameters["@h8"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[7].Value.ToString());
                        else MyCom.Parameters["@h8"].Value = 0;
                        MyCom.Parameters.Add("@h9", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[8].Value != null))
                            MyCom.Parameters["@h9"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[8].Value.ToString());
                        else MyCom.Parameters["@h9"].Value = 0;
                        MyCom.Parameters.Add("@h10", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[9].Value != null))
                            MyCom.Parameters["@h10"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[9].Value.ToString());
                        else MyCom.Parameters["@h10"].Value = 0;
                        MyCom.Parameters.Add("@h11", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[10].Value != null))
                            MyCom.Parameters["@h11"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[10].Value.ToString());
                        else MyCom.Parameters["@h11"].Value = 0;
                        MyCom.Parameters.Add("@h12", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[11].Value != null))
                            MyCom.Parameters["@h12"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[11].Value.ToString());
                        else MyCom.Parameters["@h12"].Value = 0;
                        MyCom.Parameters.Add("@h13", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[0].Value != null))
                            MyCom.Parameters["@h13"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[0].Value.ToString());
                        else MyCom.Parameters["@h13"].Value = 0;
                        MyCom.Parameters.Add("@h14", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[1].Value != null))
                            MyCom.Parameters["@h14"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[1].Value.ToString());
                        else MyCom.Parameters["@h14"].Value = 0;
                        MyCom.Parameters.Add("@h15", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[2].Value != null))
                            MyCom.Parameters["@h15"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[2].Value.ToString());
                        else MyCom.Parameters["@h15"].Value = 0;
                        MyCom.Parameters.Add("@h16", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[3].Value != null))
                            MyCom.Parameters["@h16"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[3].Value.ToString());
                        else MyCom.Parameters["@h16"].Value = 0;
                        MyCom.Parameters.Add("@h17", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[4].Value != null))
                            MyCom.Parameters["@h17"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[4].Value.ToString());
                        else MyCom.Parameters["@h17"].Value = 0;
                        MyCom.Parameters.Add("@h18", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[5].Value != null))
                            MyCom.Parameters["@h18"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[5].Value.ToString());
                        else MyCom.Parameters["@h18"].Value = 0;
                        MyCom.Parameters.Add("@h19", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[6].Value != null))
                            MyCom.Parameters["@h19"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[6].Value.ToString());
                        else MyCom.Parameters["@h19"].Value = 0;
                        MyCom.Parameters.Add("@h20", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[7].Value != null))
                            MyCom.Parameters["@h20"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[7].Value.ToString());
                        else MyCom.Parameters["@h20"].Value = 0;
                        MyCom.Parameters.Add("@h21", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[8].Value != null))
                            MyCom.Parameters["@h21"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[8].Value.ToString());
                        else MyCom.Parameters["@h21"].Value = 0;
                        MyCom.Parameters.Add("@h22", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[9].Value != null))
                            MyCom.Parameters["@h22"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[9].Value.ToString());
                        else MyCom.Parameters["@h22"].Value = 0;
                        MyCom.Parameters.Add("@h23", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[10].Value != null))
                            MyCom.Parameters["@h23"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[10].Value.ToString());
                        else MyCom.Parameters["@h23"].Value = 0;
                        MyCom.Parameters.Add("@h24", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[11].Value != null))
                            MyCom.Parameters["@h24"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[11].Value.ToString());
                        else MyCom.Parameters["@h24"].Value = 0;
                        //try
                        //{
                        MyCom.ExecuteNonQuery();
                        //}
                        //catch (Exception exp)
                        //{
                        //  string str = exp.Message;
                        //}
                    }
                }
            }
            //A Plant Is Selected
            else if (ODPlantPanel.Visible)
            {
                //Check Filling Neccessary Fields
                bool check = true;
                if (OutServiceCheck.Checked)
                {
                    if (ODServiceStartDate.IsNull)
                    {
                        ODosd1Valid.Visible = true;
                        check = false;
                    }
                    if (ODServiceEndDate.IsNull)
                    {
                        ODosd2Valid.Visible = true;
                        check = false;
                    }
                }
                if (SecondFuelCheck.Checked)
                {
                    if (ODFuelStartDate.IsNull)
                    {
                        ODsfd1Valid.Visible = true;
                        check = false;
                    }
                    if (ODFuelEndDate.IsNull)
                    {
                        ODsfd2Valid.Visible = true;
                        check = false;
                    }
                    if (errorProvider1.GetError(ODFuelQuantityTB) != "")
                        check = false;
                }
                if (ODPowerMinCheck.Checked)
                {
                    if (ODPowerStartDate.IsNull)
                    {
                        ODpd1Valid.Visible = true;
                        check = false;
                    }
                    if (ODPowerEndDate.IsNull)
                    {
                        ODpd2Valid.Visible = true;
                        check = false;
                    }
                    if (errorProvider1.GetError(ODPowerGrid1) != "") check = false;
                    if (errorProvider1.GetError(ODPowerGrid2) != "") check = false;
                }

                if (!check) MessageBox.Show("Please Fill Marked Fields With Valid Values");
                else
                {
                    ODosd1Valid.Visible = false;
                    ODosd2Valid.Visible = false;
                    ODsfd1Valid.Visible = false;
                    ODsfd2Valid.Visible = false;
                    ODpd1Valid.Visible = false;
                    ODpd2Valid.Visible = false;

        
                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Connection = MyConnection;
                    //Is There a Row in DB for this Plant?
                    MyCom.CommandText = "SELECT  @result1 =count(PPID) FROM [ConditionPlant] WHERE PPID=@num";
                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                    MyCom.Parameters["@num"].Value = PPID;
                    MyCom.Parameters.Add("@result1", SqlDbType.Int);
                    MyCom.Parameters["@result1"].Direction = ParameterDirection.Output;
                    //try
                    // {
                    MyCom.ExecuteNonQuery();
                    // }
                    //catch()
                    //{
                    //}
                    int result1;
                    result1 = (int)MyCom.Parameters["@result1"].Value;
                    if (result1 == 0)
                    {
                        MyCom.CommandText = "INSERT INTO ConditionPlant (PPID,OutService,OutServiceStartDate,OutServiceEndDate," +
                        "NoOff,NoOn,SecondFuel,SecondFuelStartDate,SecondFuelEndDate,FuelQuantity,PowerMin,PowerMinStartDate," +
                        "PowerMinEndDate,PowerMinHour1,PowerMinHour2,PowerMinHour3,PowerMinHour4,PowerMinHour5,PowerMinHour6," +
                        "PowerMinHour7,PowerMinHour8,PowerMinHour9,PowerMinHour10,PowerMinHour11,PowerMinHour12,PowerMinHour13," +
                        "PowerMinHour14,PowerMinHour15,PowerMinHour16,PowerMinHour17,PowerMinHour18,PowerMinHour19,PowerMinHour20," +
                        "PowerMinHour21,PowerMinHour22,PowerMinHour23,PowerMinHour24) VALUES (@num,@os,@osd1,@osd2,@off,@on," +
                        "@sf,@sfd1,@sfd2,@fq,@pm,@pmd1,@pmd2,@h1,@h2,@h3,@h4,@h5,@h6,@h7,@h8,@h9,@h10,@h11,@h12,@h13,@h14," +
                        "@h15,@h16,@h17,@h18,@h19,@h20,@h21,@h22,@h23,@h24)";
                    }
                    else
                    {
                        MyCom.CommandText = "UPDATE ConditionPlant SET OutService=@os,OutServiceStartDate=@osd1," +
                        "OutServiceEndDate=@osd2,NoOff=@off,NoOn=@on,SecondFuel=@sf,SecondFuelStartDate=@sfd1," +
                        "SecondFuelEndDate=@sfd2,FuelQuantity=@fq,PowerMin=@pm,PowerMinStartDate=@pmd1,PowerMinEndDate=@pmd2," +
                        "PowerMinHour1=@h1,PowerMinHour2=@h2,PowerMinHour3=@h3,PowerMinHour4=@h4,PowerMinHour5=@h5," +
                        "PowerMinHour6=@h6,PowerMinHour7=@h7,PowerMinHour8=@h8,PowerMinHour9=@h9,PowerMinHour10=@h10," +
                        "PowerMinHour11=@h11,PowerMinHour12=@h12,PowerMinHour13=@h13,PowerMinHour14=@h14,PowerMinHour15=@h15," +
                        "PowerMinHour16=@h16,PowerMinHour17=@h17,PowerMinHour18=@h18,PowerMinHour19=@h19,PowerMinHour20=@h20," +
                        "PowerMinHour21=@h21,PowerMinHour22=@h22,PowerMinHour23=@h23,PowerMinHour24=@h24 WHERE PPID=@num";
                    }
                    MyCom.Parameters.Add("@os", SqlDbType.Bit);
                    MyCom.Parameters["@os"].Value = OutServiceCheck.Checked;
                    MyCom.Parameters.Add("@osd1", SqlDbType.Char, 10);
                    if (OutServiceCheck.Checked)
                        MyCom.Parameters["@osd1"].Value = ODServiceStartDate.Text;
                    else MyCom.Parameters["@osd1"].Value = "";
                    MyCom.Parameters.Add("@osd2", SqlDbType.Char, 10);
                    if (OutServiceCheck.Checked)
                        MyCom.Parameters["@osd2"].Value = ODServiceEndDate.Text;
                    else MyCom.Parameters["@osd2"].Value = "";
                    MyCom.Parameters.Add("@on", SqlDbType.Bit);
                    MyCom.Parameters["@on"].Value = NoOnCheck.Checked;
                    MyCom.Parameters.Add("@off", SqlDbType.Bit);
                    MyCom.Parameters["@off"].Value = NoOffCheck.Checked;
                    MyCom.Parameters.Add("@sf", SqlDbType.Bit);
                    MyCom.Parameters["@sf"].Value = SecondFuelCheck.Checked;
                    MyCom.Parameters.Add("@sfd1", SqlDbType.Char, 10);
                    if (SecondFuelCheck.Checked)
                        MyCom.Parameters["@sfd1"].Value = ODFuelStartDate.Text;
                    else MyCom.Parameters["@sfd1"].Value = "";
                    MyCom.Parameters.Add("@sfd2", SqlDbType.Char, 10);
                    if (SecondFuelCheck.Checked)
                        MyCom.Parameters["@sfd2"].Value = ODFuelEndDate.Text;
                    else MyCom.Parameters["@sfd2"].Value = "";
                    MyCom.Parameters.Add("@fq", SqlDbType.Real);
                    if ((SecondFuelCheck.Checked) && (ODFuelQuantityTB.Text != ""))
                        MyCom.Parameters["@fq"].Value = ODFuelQuantityTB.Text;
                    else MyCom.Parameters["@fq"].Value = "0";
                    MyCom.Parameters.Add("@pm", SqlDbType.Bit);
                    MyCom.Parameters["@pm"].Value = ODPowerMinCheck.Checked;
                    MyCom.Parameters.Add("@pmd1", SqlDbType.Char, 10);
                    if (ODPowerMinCheck.Checked)
                        MyCom.Parameters["@pmd1"].Value = ODPowerStartDate.Text;
                    else MyCom.Parameters["@pmd1"].Value = "";
                    MyCom.Parameters.Add("@pmd2", SqlDbType.Char, 10);
                    if (ODPowerMinCheck.Checked)
                        MyCom.Parameters["@pmd2"].Value = ODPowerEndDate.Text;
                    else MyCom.Parameters["@pmd2"].Value = "";
                    MyCom.Parameters.Add("@h1", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid1.Rows[0].Cells[0].Value != null))
                        MyCom.Parameters["@h1"].Value = double.Parse(ODPowerGrid1.Rows[0].Cells[0].Value.ToString());
                    else MyCom.Parameters["@h1"].Value = 0;
                    MyCom.Parameters.Add("@h2", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid1.Rows[0].Cells[1].Value != null))
                        MyCom.Parameters["@h2"].Value = double.Parse(ODPowerGrid1.Rows[0].Cells[1].Value.ToString());
                    else MyCom.Parameters["@h2"].Value = 0;
                    MyCom.Parameters.Add("@h3", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid1.Rows[0].Cells[2].Value != null))
                        MyCom.Parameters["@h3"].Value = double.Parse(ODPowerGrid1.Rows[0].Cells[2].Value.ToString());
                    else MyCom.Parameters["@h3"].Value = 0;
                    MyCom.Parameters.Add("@h4", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid1.Rows[0].Cells[3].Value != null))
                        MyCom.Parameters["@h4"].Value = double.Parse(ODPowerGrid1.Rows[0].Cells[3].Value.ToString());
                    else MyCom.Parameters["@h4"].Value = 0;
                    MyCom.Parameters.Add("@h5", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid1.Rows[0].Cells[4].Value != null))
                        MyCom.Parameters["@h5"].Value = double.Parse(ODPowerGrid1.Rows[0].Cells[4].Value.ToString());
                    else MyCom.Parameters["@h5"].Value = 0;
                    MyCom.Parameters.Add("@h6", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid1.Rows[0].Cells[5].Value != null))
                        MyCom.Parameters["@h6"].Value = double.Parse(ODPowerGrid1.Rows[0].Cells[5].Value.ToString());
                    else MyCom.Parameters["@h6"].Value = 0;
                    MyCom.Parameters.Add("@h7", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid1.Rows[0].Cells[6].Value != null))
                        MyCom.Parameters["@h7"].Value = double.Parse(ODPowerGrid1.Rows[0].Cells[6].Value.ToString());
                    else MyCom.Parameters["@h7"].Value = 0;
                    MyCom.Parameters.Add("@h8", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid1.Rows[0].Cells[7].Value != null))
                        MyCom.Parameters["@h8"].Value = double.Parse(ODPowerGrid1.Rows[0].Cells[7].Value.ToString());
                    else MyCom.Parameters["@h8"].Value = 0;
                    MyCom.Parameters.Add("@h9", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid1.Rows[0].Cells[8].Value != null))
                        MyCom.Parameters["@h9"].Value = double.Parse(ODPowerGrid1.Rows[0].Cells[8].Value.ToString());
                    else MyCom.Parameters["@h9"].Value = 0;
                    MyCom.Parameters.Add("@h10", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid1.Rows[0].Cells[9].Value != null))
                        MyCom.Parameters["@h10"].Value = double.Parse(ODPowerGrid1.Rows[0].Cells[9].Value.ToString());
                    else MyCom.Parameters["@h10"].Value = 0;
                    MyCom.Parameters.Add("@h11", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid1.Rows[0].Cells[10].Value != null))
                        MyCom.Parameters["@h11"].Value = double.Parse(ODPowerGrid1.Rows[0].Cells[10].Value.ToString());
                    else MyCom.Parameters["@h11"].Value = 0;
                    MyCom.Parameters.Add("@h12", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid1.Rows[0].Cells[11].Value != null))
                        MyCom.Parameters["@h12"].Value = double.Parse(ODPowerGrid1.Rows[0].Cells[11].Value.ToString());
                    else MyCom.Parameters["@h12"].Value = 0;
                    MyCom.Parameters.Add("@h13", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid2.Rows[0].Cells[0].Value != null))
                        MyCom.Parameters["@h13"].Value = double.Parse(ODPowerGrid2.Rows[0].Cells[0].Value.ToString());
                    else MyCom.Parameters["@h13"].Value = 0;
                    MyCom.Parameters.Add("@h14", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid2.Rows[0].Cells[1].Value != null))
                        MyCom.Parameters["@h14"].Value = double.Parse(ODPowerGrid2.Rows[0].Cells[1].Value.ToString());
                    else MyCom.Parameters["@h14"].Value = 0;
                    MyCom.Parameters.Add("@h15", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid2.Rows[0].Cells[2].Value != null))
                        MyCom.Parameters["@h15"].Value = double.Parse(ODPowerGrid2.Rows[0].Cells[2].Value.ToString());
                    else MyCom.Parameters["@h15"].Value = 0;
                    MyCom.Parameters.Add("@h16", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid2.Rows[0].Cells[3].Value != null))
                        MyCom.Parameters["@h16"].Value = double.Parse(ODPowerGrid2.Rows[0].Cells[3].Value.ToString());
                    else MyCom.Parameters["@h16"].Value = 0;
                    MyCom.Parameters.Add("@h17", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid2.Rows[0].Cells[4].Value != null))
                        MyCom.Parameters["@h17"].Value = double.Parse(ODPowerGrid2.Rows[0].Cells[4].Value.ToString());
                    else MyCom.Parameters["@h17"].Value = 0;
                    MyCom.Parameters.Add("@h18", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid2.Rows[0].Cells[5].Value != null))
                        MyCom.Parameters["@h18"].Value = double.Parse(ODPowerGrid2.Rows[0].Cells[5].Value.ToString());
                    else MyCom.Parameters["@h18"].Value = 0;
                    MyCom.Parameters.Add("@h19", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid2.Rows[0].Cells[6].Value != null))
                        MyCom.Parameters["@h19"].Value = double.Parse(ODPowerGrid2.Rows[0].Cells[6].Value.ToString());
                    else MyCom.Parameters["@h19"].Value = 0;
                    MyCom.Parameters.Add("@h20", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid2.Rows[0].Cells[7].Value != null))
                        MyCom.Parameters["@h20"].Value = double.Parse(ODPowerGrid2.Rows[0].Cells[7].Value.ToString());
                    else MyCom.Parameters["@h20"].Value = 0;
                    MyCom.Parameters.Add("@h21", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid2.Rows[0].Cells[8].Value != null))
                        MyCom.Parameters["@h21"].Value = double.Parse(ODPowerGrid2.Rows[0].Cells[8].Value.ToString());
                    else MyCom.Parameters["@h21"].Value = 0;
                    MyCom.Parameters.Add("@h22", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid2.Rows[0].Cells[9].Value != null))
                        MyCom.Parameters["@h22"].Value = double.Parse(ODPowerGrid2.Rows[0].Cells[9].Value.ToString());
                    else MyCom.Parameters["@h22"].Value = 0;
                    MyCom.Parameters.Add("@h23", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid2.Rows[0].Cells[10].Value != null))
                        MyCom.Parameters["@h23"].Value = double.Parse(ODPowerGrid2.Rows[0].Cells[10].Value.ToString());
                    else MyCom.Parameters["@h23"].Value = 0;
                    MyCom.Parameters.Add("@h24", SqlDbType.Real);
                    if ((ODPowerMinCheck.Checked) && (ODPowerGrid2.Rows[0].Cells[11].Value != null))
                        MyCom.Parameters["@h24"].Value = double.Parse(ODPowerGrid2.Rows[0].Cells[11].Value.ToString());
                    else MyCom.Parameters["@h24"].Value = 0;

                    //try
                    //{
                    MyCom.ExecuteNonQuery();
                    //}
                    //catch (Exception exp)
                    //{
                    //  string str = exp.Message;
                    //}
                }
            }
        }
        //----------------------------------------treeView1_NodeMouseClick-------------------------------
        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {

            if (MainTabs.SelectedTab.Name == "BiddingStrategy")
                MainTabs_SelectedIndexChanged(sender, e);


            //string[] PPNameArray = new string[20];
            //for (int x = 0; x < 20; x++)
            //    PPNameArray = "";

            //Clear Grids
            PlantGV1.DataSource = null;
            if (PlantGV1.Rows != null) PlantGV1.Rows.Clear();
            PlantGV2.DataSource = null;
            if (PlantGV2.Rows != null) PlantGV2.Rows.Clear();

            //GeneralDataHeaderPanel.Visible = false;
            string PPName = "";
            if (e.Node.Parent != null) //be sure the root isnot selected!
            {
                if (e.Node.Parent.Text == "Transmission")
                {
                    SetHeader1Transmission();
                    string TransType = e.Node.Text;
                    TransType = TransType.Trim();
                    MRPlantLb.Text = TransType + " (KV)";
                    BDPlantLb.Text = TransType + " (KV)";
                    FRPlantLb.Text = TransType + " (KV)";
                    ODPlantLb.Text = TransType + " (KV)";
                    buildTRANStree(TransType);
                    FillTransmissionGrid("230");
                    FillTransmissionGrid("400");
                    line = int.Parse(TransType);
                    //TAB : Market Result
                    FillMRTransmission(TransType);
                }
                else if (e.Node.Parent.Text == "Plant")
                {
                    SetHeader1Plant();
                    PPName = e.Node.Text;
                    PPName = PPName.Trim();

                    //Detect PPID

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Connection = MyConnection;
                    MyCom.CommandText = "SELECT @num=PPID FROM PowerPlant WHERE PPName=@name";
                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                    MyCom.Parameters.Add("@name", SqlDbType.NChar, 20);
                    MyCom.Parameters["@num"].Direction = ParameterDirection.Output;
                    MyCom.Parameters["@name"].Value = PPName;
                    MyCom.ExecuteNonQuery();
                    string id = MyCom.Parameters["@num"].Value.ToString();
                    buildPPtree(id);
                    PPID = int.Parse(id);
                    GDPlantLb.Text = PPName;
                    ODPlantLb.Text = PPName;
                    MRPlantLb.Text = PPName;
                    BDPlantLb.Text = PPName;
                    FRPlantLb.Text = PPName;
                    //TAB : GENERAL DATA
                    FillPlantGrid(id);
                    //TAB: OPERATIONAL DATA
                    FillODValues(id);
                    //TAB :MARKETRESULTS
                    FillMRVlues(id);
                    //TAB : FINANCIAL REPORT
                    FillFRValues(id);
                    //Read Exsiting Plants From PowerPlant
                    //DataSet MyDS = new DataSet();
                    //SqlDataAdapter Myda = new SqlDataAdapter();
                    //Myda.SelectCommand = new SqlCommand("SELECT PPName FROM PowerPlant", MyConnection);
                    //Myda.Fill(MyDS, "ppname");
                    //int ind = 0;
                    //foreach (DataRow MyRow in MyDS.Tables["ppname"].Rows)
                    //{
                    //    PPNameArray[ind] = MyRow["PPName"].ToString().Trim();
                    //    ind++;
                    //}
                }
                else treeView2.Nodes.Clear();
            }
            else treeView2.Nodes.Clear();

            //switch (PPName)
            //{
            //    case "Besat":
            //        buildPPtree("101");
            //        PPID = 101;
            //        GDPlantLb.Text = "Besat";
            //        ODPlantLb.Text = "Besat";
            //        MRPlantLb.Text = "Besat";
            //        BDPlantLb.Text = "Besat";
            //        FRPlantLb.Text = "Besat";
            //        //TAB : GENERAL DATA
            //        FillPlantGrid("101");
            //        //TAB: OPERATIONAL DATA
            //        FillODValues("101");
            //        //TAB :MARKETRESULTS
            //        FillMRVlues("101");
            //        //TAB : FINANCIAL REPORT
            //        FillFRValues("101");
            //        break;
            //    case "Montazer Qaem":
            //        buildPPtree("131");
            //        PPID = 131;
            //        GDPlantLb.Text = "Montazer Qaem";
            //        ODPlantLb.Text = "Montazer Qaem";
            //        MRPlantLb.Text = "Montazer Qaem";
            //        BDPlantLb.Text = "Montazer Qaem";
            //        FRPlantLb.Text = "Montazer Qaem";
            //        //TAB : GENERAL DATA
            //        FillPlantGrid("131");
            //        //TAB: OPERATIONAL DATA
            //        FillODValues("131");
            //        //TAB :MARKETRESULTS
            //        FillMRVlues("131");
            //        //TAB : FINANCIAL REPORT
            //        FillFRValues("131");
            //        break;
            //    case "Parand":
            //        buildPPtree("104");
            //        PPID = 104;
            //        GDPlantLb.Text = "Parand";
            //        ODPlantLb.Text = "Parand";
            //        MRPlantLb.Text = "Parand";
            //        BDPlantLb.Text = "Parand";
            //        FRPlantLb.Text = "Parand";
            //        //TAB : GENERAL DATA
            //        FillPlantGrid("104");
            //        //TAB: OPERATIONAL DATA
            //        FillODValues("104");
            //        //TAB :MARKETRESULTS
            //        FillMRVlues("104");
            //        //TAB : FINANCIAL REPORT
            //        FillFRValues("104");
            //        break;
            //    case "Tarasht":
            //        buildPPtree("133");
            //        PPID = 133;
            //        GDPlantLb.Text = "Tarasht";
            //        ODPlantLb.Text = "Tarasht";
            //        MRPlantLb.Text = "Tarasht";
            //        BDPlantLb.Text = "Tarasht";
            //        FRPlantLb.Text = "Tarasht";
            //        //TAB : GENERAL DATA
            //        FillPlantGrid("133");
            //        //TAB: OPERATIONAL DATA
            //        FillODValues("133");
            //        //TAB :MARKETRESULTS
            //        FillMRVlues("133");
            //        //TAB : FINANCIAL REPORT
            //        FillFRValues("133");
            //        break;
            //    case "Rey":
            //        buildPPtree("138");
            //        PPID = 138;
            //        GDPlantLb.Text = "Rey";
            //        ODPlantLb.Text = "Rey";
            //        MRPlantLb.Text = "Rey";
            //        BDPlantLb.Text = "Rey";
            //        FRPlantLb.Text = "Rey";
            //        //TAB : GENERAL DATA
            //        FillPlantGrid("138");
            //        //TAB: OPERATIONAL DATA
            //        FillODValues("138");
            //        //TAB :MARKETRESULTS
            //        FillMRVlues("138");
            //        //TAB : FINANCIAL REPORT
            //        FillFRValues("138");
            //        break;
            //    case "Shahid Rajayi":
            //        buildPPtree("144");
            //        PPID = 144;
            //        GDPlantLb.Text = "Shahid Rajayi";
            //        ODPlantLb.Text = "Shahid Rajayi";
            //        MRPlantLb.Text = "Shahid Rajayi";
            //        BDPlantLb.Text = "Shahid Rajayi";
            //        FRPlantLb.Text = "Shahid Rajayi";
            //        //TAB : GENERAL DATA
            //        FillPlantGrid("144");
            //        //TAB: OPERATIONAL DATA
            //        FillODValues("144");
            //        //TAB :MARKETRESULTS
            //        FillMRVlues("144");
            //        //TAB : FINANCIAL REPORT
            //        FillFRValues("144");
            //        break;
            //    case "Qom":
            //        buildPPtree("149");
            //        PPID = 149;
            //        GDPlantLb.Text = "Qom";
            //        ODPlantLb.Text = "Qom";
            //        MRPlantLb.Text = "Qom";
            //        BDPlantLb.Text = "Qom";
            //        FRPlantLb.Text = "Qom";
            //        //TAB : GENERAL DATA
            //        FillPlantGrid("149");
            //        //TAB: OPERATIONAL DATA
            //        FillODValues("149");
            //        //TAB :MARKETRESULTS
            //        FillMRVlues("149");
            //        //TAB : FINANCIAL REPORT
            //        FillFRValues("149");
            //        break;
            //}
        }
        //---------------------------------------ClearFRPlantTab---------------------------------------------
        private void ClearFRPlantTab()
        {
            FRPlantBenefitTB.Text = "";
            FRPlantIncomeTb.Text = "";
            FRPlantCostTb.Text = "";
            FRPlantAvaCapTb.Text = "";
            FRPlantTotalPowerTb.Text = "";
            FRPlantBidPowerTb.Text = "";
            FRPlantULPowerTb.Text = "";
            FRPlantIncPowerTb.Text = "";
            FRPlantDecPowerTb.Text = "";
            FRPlantCapPayTb.Text = "";
            FRPlantEnergyPayTb.Text = "";
            FRPlantBidPayTb.Text = "";
            FRPlantULPayTb.Text = "";
            FRPlantIncPayTb.Text = "";
            FRPlantDecPayTb.Text = "";
        }
        //----------------------------------------FillFRValues()--------------------------------------------------
        private void FillFRValues(string Num)
        {
            ClearFRPlantTab();
            if (!FRPlantCal.IsNull)
            {
                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT Benefit,Income,Cost,AvailableCapacity,TotalPower,BidPower," +
                "ULPower,IncrementPower,DecreasePower,CapacityPayment,EnergyPayment,BidPayment,ULPayment,IncrementPayment," +
                "DecreasePayment FROM [EconomicPlant] WHERE Date=@date AND PPID=" + PPID, MyConnection);
                Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Myda.SelectCommand.Parameters["@date"].Value = FRPlantCal.Text;
                Myda.Fill(MyDS);
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    FRPlantBenefitTB.Text = MyRow[0].ToString();
                    FRPlantIncomeTb.Text = MyRow[1].ToString();
                    FRPlantCostTb.Text = MyRow[2].ToString();
                    FRPlantAvaCapTb.Text = MyRow[3].ToString();
                    FRPlantTotalPowerTb.Text = MyRow[4].ToString();
                    FRPlantBidPowerTb.Text = MyRow[5].ToString();
                    FRPlantULPowerTb.Text = MyRow[6].ToString();
                    FRPlantIncPowerTb.Text = MyRow[7].ToString();
                    FRPlantDecPowerTb.Text = MyRow[8].ToString();
                    FRPlantCapPayTb.Text = MyRow[9].ToString();
                    FRPlantEnergyPayTb.Text = MyRow[10].ToString();
                    FRPlantBidPayTb.Text = MyRow[11].ToString();
                    FRPlantULPayTb.Text = MyRow[12].ToString();
                    FRPlantIncPayTb.Text = MyRow[13].ToString();
                    FRPlantDecPayTb.Text = MyRow[14].ToString();
                }
               
            }
        }
        //-----------------------------------------treeView2_NodeMouseClick-----------------------------------
        private void treeView2_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Parent != null) //be sure the root isnot selected!
            {
                string parent = e.Node.Parent.Text;
                //if a unit of plants is selected
                if ((parent.Contains("Steam")) || (parent.Contains("Gas")) || (parent.Contains("Combined")))
                {
                    string unit = e.Node.Text;
                    unit = unit.Trim();
                    string package = e.Node.Parent.Text;
                    package = package.Trim();
                    string type = unit.ToLower();
                    if (type.Contains("steam")) type = "Steam";
                    else type = "Gas";

                    //Get Index Of Selected Unit(Row) in GridView (In Generad data Tab)
                    int index = 0;
                    if (GDSteamGroup.Text.Contains(package))
                        for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                        {
                            if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                                index = i;
                        }
                    else
                        for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                        {
                            if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                                index = i;
                        }

                    //set HeaderPanel of Tabs
                    SetHeader2Plant(package, unit, type);

                    //TAB:GENERAL DATA
                    // set values of TextBoxes
                    FillGDUnit(unit, package, index);

                    //TAB:GENERAL DATA
                    // set values of TextBoxes
                    FillODUnit(unit, package, index);

                    //TAB : MARKET RESULTS
                    //Set TextBoxes And Grids
                    FillMRUnit(unit, package, index);

                    //TAB : BID DATA
                    //SET TextBoxes AND DataGrid
                    FillBDUnit(unit, package, index);

                    //TAB : FINANCIAL REPORT
                    //SET TextBoxes
                    FillFRUnit(unit, package, index);

                }

                //if a line of Transmission is selected
                else if (e.Node.Parent.Parent != null)
                {
                    SetHeader2Transmission();
                    string TransLine = e.Node.Text.Trim();
                    string TransType = e.Node.Parent.Parent.Text.Trim();
                    GDPlantLb.Text = TransLine;
                    ODPlantLb.Text = TransLine;
                    MRPlantLb.Text = TransLine;
                    BDPlantLb.Text = TransLine;
                    FRPlantLb.Text = TransLine;
                    FillGDLine(TransType, TransLine);
                    FillMRLine(TransType, TransLine);
                }
            }
        }
        //------------------------------FillMRTransmission-------------------------
        private void FillMRTransmission(string TransType)
        {
            //Clear Market Result Tab
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            MRPlantOnUnitTb.Text = "";
            MRPlantPowerTb.Text = "";
            if (!MRCal.IsNull)
            {
                SqlCommand MyCom = new SqlCommand();
                MyCom.CommandText = "SELECT  @result1=SUM(Hour1+Hour2+Hour3+Hour4+Hour5+Hour6+Hour7+Hour8+Hour9+Hour10+" +
                "Hour11+Hour12+Hour13+Hour14+Hour15+Hour16+Hour17+Hour18+Hour19+Hour20+Hour21+Hour22+Hour23+Hour24) " +
                "FROM InterchangedEnergy INNER JOIN TransLine ON InterchangedEnergy.Code = TransLine.LineCode WHERE " +
                "InterchangedEnergy.Date=@date AND TransLine.LineNumber=@trans AND TransLine.Type=-1" +
                "SELECT  @result2=SUM(Hour1+Hour2+Hour3+Hour4+Hour5+Hour6+Hour7+Hour8+Hour9+Hour10+" +
                "Hour11+Hour12+Hour13+Hour14+Hour15+Hour16+Hour17+Hour18+Hour19+Hour20+Hour21+Hour22+Hour23+Hour24) " +
                "FROM InterchangedEnergy INNER JOIN TransLine ON InterchangedEnergy.Code = TransLine.LineCode WHERE " +
                "InterchangedEnergy.Date=@date AND TransLine.LineNumber=@trans AND TransLine.Type=1";
                MyCom.Connection = MyConnection;

                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = MRCal.Text;
                MyCom.Parameters.Add("@trans", SqlDbType.Int);
                MyCom.Parameters["@trans"].Value = int.Parse(TransType);

                MyCom.Parameters.Add("@result1", SqlDbType.Real);
                MyCom.Parameters["@result1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@result2", SqlDbType.Real);
                MyCom.Parameters["@result2"].Direction = ParameterDirection.Output;
                //try
                //{
                MyCom.ExecuteNonQuery();
                //}
                //catch(Exception e)
                //{
                //}
                int thereis = 0;
                try
                {
                    double re1 = double.Parse(MyCom.Parameters["@result1"].Value.ToString());
                    if (re1 < 0) re1 = re1 * (-1);
                    MRPlantOnUnitTb.Text = re1.ToString();
                }
                catch
                {
                    thereis++;
                    MRPlantOnUnitTb.Text = "0";
                }
                try
                {
                    double re2 = double.Parse(MyCom.Parameters["@result2"].Value.ToString());
                    if (re2 < 0) re2 = re2 * (-1);
                    MRPlantPowerTb.Text = re2.ToString();
                }
                catch
                {
                    MRPlantPowerTb.Text = "0";
                    thereis++;
                }
                //if (thereis == 2)
                ////    if MRCal.s
                // MessageBox.Show("There is no Data for selected Date!");
                //else
                if (thereis != 2)
                {
                    DataSet MyDS = new DataSet();
                    SqlDataAdapter Myda = new SqlDataAdapter();
                    Myda.SelectCommand = new SqlCommand("SELECT  SUM((TransLine.Type)*Hour1),SUM((TransLine.Type)*Hour2)," +
                    "SUM((TransLine.Type)*Hour3),SUM((TransLine.Type)*Hour4),SUM((TransLine.Type)*Hour5),SUM((TransLine.Type)*Hour6)," +
                    "SUM((TransLine.Type)*Hour7),SUM((TransLine.Type)*Hour8),SUM((TransLine.Type)*Hour9),SUM((TransLine.Type)*Hour10)," +
                    "SUM((TransLine.Type)*Hour11),SUM((TransLine.Type)*Hour12),SUM((TransLine.Type)*Hour13),SUM((TransLine.Type)*Hour14)," +
                    "SUM((TransLine.Type)*Hour15),SUM((TransLine.Type)*Hour16),SUM((TransLine.Type)*Hour17),SUM((TransLine.Type)*Hour18)," +
                    "SUM((TransLine.Type)*Hour19),SUM((TransLine.Type)*Hour20),SUM((TransLine.Type)*Hour21),SUM((TransLine.Type)*Hour22)," +
                    "SUM((TransLine.Type)*Hour23),SUM((TransLine.Type)*Hour24) FROM InterchangedEnergy INNER JOIN TransLine " +
                    "ON InterchangedEnergy.Code = TransLine.LineCode WHERE InterchangedEnergy.Date=@date AND TransLine.LineNumber=@trans", MyConnection);
                    Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                    Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                    Myda.SelectCommand.Parameters.Add("@trans", SqlDbType.Int);
                    Myda.SelectCommand.Parameters["@trans"].Value = int.Parse(TransType);
                    Myda.Fill(MyDS);

                    MRCurGrid1.Rows[0].Cells[0].Value = "Power";
                    MRCurGrid2.Rows[0].Cells[0].Value = "Power";
                    foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                    {
                        for (int i = 0; i < 24; i++)
                            if (i < 12)
                                MRCurGrid1.Rows[0].Cells[i + 1].Value = MyRow[i].ToString();
                            else
                                MRCurGrid2.Rows[0].Cells[i - 11].Value = MyRow[i].ToString();
                    }

                }
            }
        }
        //----------------------------FillMRLine------------------------------------
        private void FillMRLine(string TransType, string TransLine)
        {
            //Clear Market Result Tab
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            MRPlantOnUnitTb.Text = "";
            MRPlantPowerTb.Text = "";
            if (!MRCal.IsNull)
            {
                SqlCommand MyCom = new SqlCommand();
                MyCom.CommandText = "SELECT  @result=(Hour1+Hour2+Hour3+Hour4+Hour5+Hour6+Hour7+Hour8+Hour9+Hour10+" +
                "Hour11+Hour12+Hour13+Hour14+Hour15+Hour16+Hour17+Hour18+Hour19+Hour20+Hour21+Hour22+Hour23+Hour24), " +
                "@type=TransLine.Type FROM InterchangedEnergy INNER JOIN TransLine ON InterchangedEnergy.Code = " +
                "TransLine.LineCode WHERE InterchangedEnergy.Date=@date AND TransLine.LineNumber=@trans AND " +
                "InterchangedEnergy.Code =@code";
                MyCom.Connection = MyConnection;

                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = MRCal.Text;
                MyCom.Parameters.Add("@trans", SqlDbType.Int);
                MyCom.Parameters["@trans"].Value = int.Parse(TransType);
                MyCom.Parameters.Add("@code", SqlDbType.NChar, 20);
                MyCom.Parameters["@code"].Value = TransLine;

                MyCom.Parameters.Add("@result", SqlDbType.Real);
                MyCom.Parameters["@result"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@type", SqlDbType.Int);
                MyCom.Parameters["@type"].Direction = ParameterDirection.Output;
                //try
                //{
                MyCom.ExecuteNonQuery();
                //}
                //catch(Exception e)
                //{
                //}
                int thereis = 0, type = 0;
                try
                {
                    type = int.Parse(MyCom.Parameters["@type"].Value.ToString());
                }
                catch
                {
                    thereis++;
                    //MessageBox.Show("There is no Data for selected Date!");
                }
                if (thereis == 0)
                {
                    try
                    {
                        double re = double.Parse(MyCom.Parameters["@result"].Value.ToString());
                        if (re < 0) re = re * (-1);
                        if (type == -1)
                            MRPlantOnUnitTb.Text = re.ToString();
                        else
                            MRPlantPowerTb.Text = re.ToString();
                    }
                    catch
                    {
                        MRPlantPowerTb.Text = "0";
                        MRPlantOnUnitTb.Text = "0";
                    }

                    DataSet MyDS = new DataSet();
                    SqlDataAdapter Myda = new SqlDataAdapter();
                    Myda.SelectCommand = new SqlCommand("SELECT  (TransLine.Type)*Hour1,(TransLine.Type)*Hour2," +
                    "(TransLine.Type)*Hour3,(TransLine.Type)*Hour4,(TransLine.Type)*Hour5,(TransLine.Type)*Hour6," +
                    "(TransLine.Type)*Hour7,(TransLine.Type)*Hour8,(TransLine.Type)*Hour9,(TransLine.Type)*Hour10," +
                    "(TransLine.Type)*Hour11,(TransLine.Type)*Hour12,(TransLine.Type)*Hour13,(TransLine.Type)*Hour14," +
                    "(TransLine.Type)*Hour15,(TransLine.Type)*Hour16,(TransLine.Type)*Hour17,(TransLine.Type)*Hour18," +
                    "(TransLine.Type)*Hour19,(TransLine.Type)*Hour20,(TransLine.Type)*Hour21,(TransLine.Type)*Hour22," +
                    "(TransLine.Type)*Hour23,(TransLine.Type)*Hour24 FROM InterchangedEnergy INNER JOIN TransLine " +
                    "ON InterchangedEnergy.Code = TransLine.LineCode WHERE InterchangedEnergy.Date=@date AND " +
                    "TransLine.LineNumber=@trans AND InterchangedEnergy.Code =@code", MyConnection);
                    Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                    Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                    Myda.SelectCommand.Parameters.Add("@trans", SqlDbType.Int);
                    Myda.SelectCommand.Parameters["@trans"].Value = int.Parse(TransType);
                    Myda.SelectCommand.Parameters.Add("@code", SqlDbType.NChar, 20);
                    Myda.SelectCommand.Parameters["@code"].Value = TransLine;
                    Myda.Fill(MyDS);

                    MRCurGrid1.Rows[0].Cells[0].Value = "Power";
                    MRCurGrid2.Rows[0].Cells[0].Value = "Power";
                    foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                    {
                        for (int i = 0; i < 24; i++)
                            if (i < 12)
                                MRCurGrid1.Rows[0].Cells[i + 1].Value = MyRow[i].ToString();
                            else
                                MRCurGrid2.Rows[0].Cells[i - 11].Value = MyRow[i].ToString();
                    }
                }
            }
        }
        //-----------------------------ClearGDUnitTab()------------------------------
        private void ClearGDUnitTab()
        {
            GDcapacityTB.Text = "";
            GDPmaxTB.Text = "";
            GDPminTB.Text = "";
            GDTimeUpTB.Text = "";
            GDTimeDownTB.Text = "";
            GDTimeColdStartTB.Text = "";
            GDTimeHotStartTB.Text = "";
            GDRampRateTB.Text = "";
            GDIntConsumeTB.Text = "";
            GDStateTB.Text = "";
            GDFuelTB.Text = "";
            GDMainTypeTB.Text = "";
            GDStartDateCal.IsNull = true;
            GDEndDateCal.IsNull = true;
        }
        //-------------------------FillGDUnit----------------------------------------
        private void FillGDUnit(string unit, string package, int index)
        {
            ClearGDUnitTab();
            string ABpackage = package;
            if (ABpackage.Contains("Combined")) 
                ABpackage = "CC";

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT TUp,TDown,TStartCold,TStartHot," +
            "RampUpRate,InternalConsume FROM [UnitsDataMain] WHERE PPID=" + PPID + " AND UnitCode LIKE '" + unit + "%'", MyConnection);
            Myda.Fill(MyDS);
            foreach (DataRow MyRow in MyDS.Tables[0].Rows)
            {
                GDTimeUpTB.Text = MyRow[0].ToString();
                GDTimeDownTB.Text = MyRow[1].ToString();
                GDTimeColdStartTB.Text = MyRow[2].ToString();
                GDTimeHotStartTB.Text = MyRow[3].ToString();
                GDRampRateTB.Text = MyRow[4].ToString();
                GDIntConsumeTB.Text = MyRow[5].ToString();
            }

            //Detect Farsi Date
            System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
            DateTime CurDate = DateTime.Now;
            string day = pr.GetDayOfMonth(CurDate).ToString();
            if (int.Parse(day) < 10) day = "0" + day;
            string mydate = pr.GetYear(CurDate) + "/" + pr.GetMonth(CurDate) + "/" + day;

            //SqlCommand MyCom = new SqlCommand();
            //MyCom.Connection = MyConnection;
            //MyCom.CommandText = "SELECT @IntCon=InternalConsume FROM EconomicUnit WHERE PPID=@num AND " +
            //"UnitCode=@unit AND Date=@date";
            //MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            //MyCom.Parameters["@num"].Value = PPID;
            //MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
            //string myunit = unit;
            //if (package.Contains("Combined"))
            //{
            //    string packagecode = "";
            //    if (GDSteamGroup.Text.Contains(package))
            //        packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
            //    else packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
            //    myunit = "C" + packagecode;
            //}
            //MyCom.Parameters["@unit"].Value = myunit;
            //MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            //MyCom.Parameters["@date"].Value = mydate;
            //MyCom.Parameters.Add("@IntCon", SqlDbType.Real);
            //MyCom.Parameters["@IntCon"].Direction = ParameterDirection.Output;
            ////try
            ////{
            //MyCom.ExecuteNonQuery();
            ////}
            ////catch (Exception exp)
            ////{
            ////  string str = exp.Message;
            ////}
            if (package.Contains("Combined")) package = "Combined Cycle";
            if (GDGasGroup.Text.Contains(package))
            {
                GDcapacityTB.Text = PlantGV1.Rows[index].Cells[3].Value.ToString();
                GDPminTB.Text = PlantGV1.Rows[index].Cells[4].Value.ToString();
                GDPmaxTB.Text = PlantGV1.Rows[index].Cells[5].Value.ToString();
                GDStateTB.Text = PlantGV1.Rows[index].Cells[7].Value.ToString();
                GDFuelTB.Text = PlantGV1.Rows[index].Cells[6].Value.ToString();
                GDMainTypeTB.Text = PlantGV1.Rows[index].Cells[8].Value.ToString();
            }
            else if (GDSteamGroup.Text.Contains(package))
            {
                GDcapacityTB.Text = PlantGV2.Rows[index].Cells[3].Value.ToString();
                GDPminTB.Text = PlantGV2.Rows[index].Cells[4].Value.ToString();
                GDPmaxTB.Text = PlantGV2.Rows[index].Cells[5].Value.ToString();
                GDStateTB.Text = PlantGV2.Rows[index].Cells[7].Value.ToString();
                GDFuelTB.Text = PlantGV2.Rows[index].Cells[6].Value.ToString();
                GDMainTypeTB.Text = PlantGV2.Rows[index].Cells[8].Value.ToString();
            }

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            MyCom.CommandText = "SELECT  @m=Maintenance, @md1 =MaintenanceStartDate,@md2=MaintenanceEndDate " +
                "FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@unit AND PackageType=@package";
            MyCom.Parameters.Add("@package", SqlDbType.NChar, 10);
            string type = package;
            if (package.Contains("Combined")) type = "CC";
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPID;
            MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
            string myunit = unit;
            if (package.Contains("Combined"))
            {
                string packagecode = "";
                if (GDSteamGroup.Text.Contains(package))
                    packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                else packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                myunit = "C" + packagecode;
            }
            MyCom.Parameters["@unit"].Value = myunit;
            MyCom.Parameters["@package"].Value = type;
            MyCom.Parameters.Add("@m", SqlDbType.Bit);
            MyCom.Parameters["@m"].Direction = ParameterDirection.Output;
            MyCom.Parameters.Add("@md1", SqlDbType.Char, 10);
            MyCom.Parameters["@md1"].Direction = ParameterDirection.Output;
            MyCom.Parameters.Add("@md2", SqlDbType.Char, 10);
            MyCom.Parameters["@md2"].Direction = ParameterDirection.Output;
            //try
            //{
            MyCom.ExecuteNonQuery();
            //}
            //catch (Exception exp)
            //{
            //  string str = exp.Message;
            //}
            if (MyCom.Parameters["@m"].Value.ToString() != "")
                if (bool.Parse(MyCom.Parameters["@m"].Value.ToString()))
                {
                    GDStartDateCal.Text = MyCom.Parameters["@md1"].Value.ToString();
                    GDEndDateCal.Text = MyCom.Parameters["@md2"].Value.ToString();
                }
        }
        //----------------------------FillMRUnit-------------------------------------
        private void FillMRUnit(string unit, string package, int index)
        {
            //Clear Market Result Tab
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            MRUnitStateTb.Text = "";
            MRUnitPowerTb.Text = "";
            MRUnitMaxBidTb.Text = "";

            //Detect Farsi Date
            System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
            DateTime CurDate = DateTime.Now;
            string day = pr.GetDayOfMonth(CurDate).ToString();
            if (int.Parse(day) < 10) day = "0" + day;
            string mydate = pr.GetYear(CurDate) + "/" + pr.GetMonth(CurDate) + "/" + day;
            int myhour = CurDate.Hour;
            //Detect Block For FRM005
            string temp = unit;
            temp = temp.ToLower();
            if (package.Contains("Combined"))
            {
                int x = PPID;
                if ((x == 131) || (x == 144)) x++;
                string packagecode = "";
                if (GDSteamGroup.Text.Contains(package))
                    packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                else packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                temp = x + "-" + "C" + packagecode;
            }
            else if (temp.Contains("gas"))
            {
                temp = temp.Replace("gas", "G");
                temp = PPID + "-" + temp;
            }
            else
            {
                temp = temp.Replace("steam", "S");
                temp = PPID + "-" + temp;
            }

            MRUnitStateTb.Text = GDStateTB.Text;
            if (MRUnitStateTb.Text == "ON")
            {
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                MyCom.CommandText = "SELECT @re=Required FROM [DetailFRM005] WHERE TargetMarketDate=@date " +
                "AND PPID=@num AND Block=@block AND Hour=@hour ";
                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = mydate;
                MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                MyCom.Parameters["@num"].Value = PPID;
                MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                MyCom.Parameters["@block"].Value = temp;
                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                MyCom.Parameters["@hour"].Value = myhour;
                MyCom.Parameters.Add("@re", SqlDbType.Real);
                MyCom.Parameters["@re"].Direction = ParameterDirection.Output;
                //try
                //{
                MyCom.ExecuteNonQuery();
                //}
                //catch (Exception exp)
                //{
                //  string str = exp.Message;
                //}
                MRUnitPowerTb.Text = MyCom.Parameters["@re"].Value.ToString();

                //Detect MaxBid field
                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10 FROM [DetailFRM002] " +
                "WHERE TargetMarketDate=@date AND Block=@ptype AND Hour=@hour AND PPID=" + PPID, MyConnection);
                Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Myda.SelectCommand.Parameters["@date"].Value = mydate;
                Myda.SelectCommand.Parameters.Add("@hour", SqlDbType.SmallInt);
                Myda.SelectCommand.Parameters["@hour"].Value = myhour;
                //Detect Block For FRM002
                string ptype = unit;
                ptype = ptype.ToLower();
                if (package.Contains("Combined"))
                {
                    string packagecode = "";
                    if (GDGasGroup.Text.Contains("Combined"))
                        packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                    else packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                    ptype = "C" + packagecode;
                }
                else
                {
                    if (ptype.Contains("gas"))
                        ptype = ptype.Replace("gas", "G");
                    else if (ptype.Contains("steam"))
                        ptype = ptype.Replace("steam", "S");
                }
                ptype = ptype.Trim();
                Myda.SelectCommand.Parameters.Add("@ptype", SqlDbType.NChar, 20);
                Myda.SelectCommand.Parameters["@ptype"].Value = ptype;
                Myda.Fill(MyDS);
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    double required = double.Parse(MRUnitPowerTb.Text);
                    int Dsindex = 0;
                    while ((Dsindex < 20) && (double.Parse(MyRow[Dsindex].ToString()) < required)) 
                        Dsindex = Dsindex + 2;
                    if (Dsindex < 20)
                        MRUnitMaxBidTb.Text = MyRow[Dsindex + 1].ToString();
                    else MRUnitMaxBidTb.Text = "0";
                }
                MyCom.CommandText = "UPDATE DetailFRM005 SET MaxBid=@maxbid WHERE TargetMarketDate=@date AND " +
                "PPID=@num AND Block=@block AND Hour=@hour";
                MyCom.Parameters.Add("@maxbid", SqlDbType.Real);
                MyCom.Parameters["@maxbid"].Value = double.Parse(MRUnitMaxBidTb.Text);
                //try
                //{
                MyCom.ExecuteNonQuery();
                //}
                //catch (Exception exp)
                //{
                //  string str = exp.Message;
                //}
            }


            if (!MRCal.IsNull)
                FillMRUnitGrid();
        }
        //--------------------------------------FillMRUnitGrid()------------------------------------------
        private void FillMRUnitGrid()
        {
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            if (!MRCal.IsNull)
            {
                string package = MRPackLb.Text.Trim();
                string unit = MRUnitLb.Text.Trim();
                //Get Index Of Selected Unit(Row) in GridView (In Generad data Tab)
                int index = 0;
                if (GDSteamGroup.Text.Contains(package))
                    for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                    {
                        if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                            index = i;
                    }
                else
                    for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                    {
                        if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                            index = i;
                    }

                //Detect Block For FRM005
                string temp = unit;
                temp = temp.ToLower();
                if (package.Contains("Combined"))
                {
                    int x = PPID;
                    if ((x == 131) || (x == 144)) x++;
                    string packagecode = "";
                    if (GDSteamGroup.Text.Contains(package))
                        packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                    else packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                    temp = x + "-" + "C" + packagecode;
                }
                else if (temp.Contains("gas"))
                {
                    temp = temp.Replace("gas", "G");
                    temp = PPID + "-" + temp;
                }
                else
                {
                    temp = temp.Replace("steam", "S");
                    temp = PPID + "-" + temp;
                }

                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();

                Myda.SelectCommand = new SqlCommand("SELECT Hour,Required,Dispatchable,Contribution FROM [DetailFRM005] WHERE PPID= " + PPID + " AND TargetMarketDate=@date AND Block=@block", MyConnection);
                Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                Myda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
                Myda.SelectCommand.Parameters["@block"].Value = temp;
                Myda.Fill(MyDS);
                MRCurGrid1.DataSource = MyDS.Tables[0].DefaultView;
                MRCurGrid2.DataSource = MyDS.Tables[0].DefaultView;
                MRCurGrid1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                MRCurGrid2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                if (MRCurGrid1.RowCount > 1)
                {
                    MRCurGrid1.Rows[0].Cells[0].Value = "Required";
                    MRCurGrid2.Rows[0].Cells[0].Value = "Required";
                    MRCurGrid1.Rows[1].Cells[0].Value = "Dispatchable";
                    MRCurGrid2.Rows[1].Cells[0].Value = "Dispatchable";
                    MRCurGrid1.Rows[2].Cells[0].Value = "Contribution";
                    MRCurGrid2.Rows[2].Cells[0].Value = "Conrtibution";
                    MRCurGrid1.Rows[3].Cells[0].Value = "Price";
                    MRCurGrid2.Rows[3].Cells[0].Value = "Price";
                    //???????????????Price????????????????????
                    foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                    {
                        int x = int.Parse(MyRow["Hour"].ToString());
                        if (x <= 12)
                        {
                            MRCurGrid1.Rows[0].Cells[x].Value = MyRow["Required"];
                            MRCurGrid1.Rows[1].Cells[x].Value = MyRow["Dispatchable"];
                            MRCurGrid1.Rows[2].Cells[x].Value = MyRow["Contribution"];
                        }
                        else
                        {
                            MRCurGrid2.Rows[0].Cells[x - 12].Value = MyRow["Required"];
                            MRCurGrid2.Rows[1].Cells[x - 12].Value = MyRow["Dispatchable"];
                            MRCurGrid2.Rows[2].Cells[x - 12].Value = MyRow["Contribution"];
                        }
                    }
                    for (int i = (MRCurGrid1.RowCount - 2); i > 3; i--)
                        MRCurGrid1.Rows.Remove(MRCurGrid1.Rows[i]);
                    for (int i = (MRCurGrid2.RowCount - 2); i > 3; i--)
                        MRCurGrid2.Rows.Remove(MRCurGrid2.Rows[i]);

                    //Detect MaxBid field in MRCurGrid1
                    foreach (DataGridViewColumn DC in MRCurGrid1.Columns)
                    {
                        if (DC.Index > 0)
                        {
                            SqlConnection MyCon = ConnectionManager.GetInstance().Connection;

                            DataSet MaxDS = new DataSet();
                            SqlDataAdapter Maxda = new SqlDataAdapter();
                            Maxda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                            "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10 FROM [DetailFRM002] " +
                            "WHERE TargetMarketDate=@date AND Block=@ptype AND Hour=@hour AND PPID=" + PPID, MyConnection);
                            Maxda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                            Maxda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                            Maxda.SelectCommand.Parameters.Add("@hour", SqlDbType.SmallInt);
                            Maxda.SelectCommand.Parameters["@hour"].Value = DC.Index;
                            //Detect Block For FRM002
                            string ptype = unit;
                            ptype = ptype.ToLower();
                            if (package.Contains("Combined"))
                            {
                                string packagecode = "";
                                if (GDGasGroup.Text.Contains("Combined"))
                                    packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                                else packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                                ptype = "C" + packagecode;
                            }
                            else
                            {
                                if (ptype.Contains("gas"))
                                    ptype = ptype.Replace("gas", "G");
                                else if (ptype.Contains("steam"))
                                    ptype = ptype.Replace("steam", "S");
                            }
                            ptype = ptype.Trim();
                            Maxda.SelectCommand.Parameters.Add("@ptype", SqlDbType.NChar, 20);
                            Maxda.SelectCommand.Parameters["@ptype"].Value = ptype;
                            Maxda.Fill(MaxDS);

                            SqlCommand MyCom = new SqlCommand();
                            MyCom.Connection = MyConnection;
                            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                            MyCom.Parameters["@date"].Value = MRCal.Text;
                            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                            MyCom.Parameters["@num"].Value = PPID.ToString();
                            MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                            MyCom.Parameters["@block"].Value = temp;
                            MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                            MyCom.Parameters.Add("@mb", SqlDbType.Real);

                            foreach (DataRow MaxRow in MaxDS.Tables[0].Rows)
                            {
                                double required = double.Parse(MRCurGrid1.Rows[0].Cells[DC.Index].Value.ToString());
                                int Dsindex = 0;
                                while ((Dsindex < 20) && (double.Parse(MaxRow[Dsindex].ToString()) < required)) Dsindex = Dsindex + 2;
                                if (Dsindex < 20)
                                    MRCurGrid1.Rows[3].Cells[DC.Index].Value = MaxRow[Dsindex + 1].ToString();
                                else MRCurGrid1.Rows[3].Cells[DC.Index].Value = "";

                                //Insert MaxBid Into DetailFRM005
                                MyCom.CommandText = "UPDATE DetailFRM005 SET MaxBid=@mb WHERE TargetMarketDate=@date AND PPID=@num " +
                                "AND Block=@block AND Hour=@hour";
                                MyCom.Parameters["@hour"].Value = DC.Index;
                                try
                                {
                                    MyCom.Parameters["@mb"].Value = double.Parse(MRCurGrid1.Rows[3].Cells[DC.Index].Value.ToString());
                                }
                                catch (Exception e)
                                {
                                    MyCom.Parameters["@mb"].Value = "0";
                                }
                                try
                                {
                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception e)
                                {
                                    string a = e.Message;
                                }
                            }
                        }
                    }

                    //Detect MaxBid field in MRCurGrid2
                    foreach (DataGridViewColumn DC in MRCurGrid2.Columns)
                    {
                        if (DC.Index > 0)
                        {
                            SqlConnection MyCon = ConnectionManager.GetInstance().Connection;

                            DataSet MaxDS = new DataSet();
                            SqlDataAdapter Maxda = new SqlDataAdapter();
                            Maxda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                            "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10 FROM [DetailFRM002] " +
                            "WHERE TargetMarketDate=@date AND Block=@ptype AND Hour=@hour AND PPID=" + PPID, MyConnection);
                            Maxda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                            Maxda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                            Maxda.SelectCommand.Parameters.Add("@hour", SqlDbType.SmallInt);
                            Maxda.SelectCommand.Parameters["@hour"].Value = DC.Index + 12;
                            //Detect Block For FRM002
                            string ptype = unit;
                            ptype = ptype.ToLower();
                            if (package.Contains("Combined"))
                            {
                                string packagecode = "";
                                if (GDGasGroup.Text.Contains("Combined"))
                                    packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                                else packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                                ptype = "C" + packagecode;
                            }
                            else
                            {
                                if (ptype.Contains("gas"))
                                    ptype = ptype.Replace("gas", "G");
                                else if (ptype.Contains("steam"))
                                    ptype = ptype.Replace("steam", "S");
                            }
                            ptype = ptype.Trim();
                            Maxda.SelectCommand.Parameters.Add("@ptype", SqlDbType.NChar, 20);
                            Maxda.SelectCommand.Parameters["@ptype"].Value = ptype;
                            Maxda.Fill(MaxDS);

                            SqlCommand MyCom = new SqlCommand();
                            MyCom.Connection = MyConnection;
                            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                            MyCom.Parameters["@date"].Value = MRCal.Text;
                            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                            MyCom.Parameters["@num"].Value = PPID.ToString();
                            MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                            MyCom.Parameters["@block"].Value = temp;
                            MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                            MyCom.Parameters.Add("@mb", SqlDbType.Real);

                            foreach (DataRow MaxRow in MaxDS.Tables[0].Rows)
                            {
                                double required = double.Parse(MRCurGrid2.Rows[0].Cells[DC.Index].Value.ToString());
                                int Dsindex = 0;
                                while ((Dsindex < 20) && (double.Parse(MaxRow[Dsindex].ToString()) < required)) Dsindex = Dsindex + 2;
                                if (Dsindex < 20)
                                    MRCurGrid2.Rows[3].Cells[DC.Index].Value = MaxRow[Dsindex + 1].ToString();
                                else MRCurGrid2.Rows[3].Cells[DC.Index].Value = "";

                                //Insert MaxBid Into DetailFRM005
                                MyCom.CommandText = "UPDATE DetailFRM005 SET MaxBid=@mb WHERE TargetMarketDate=@date AND PPID=@num " +
                                "AND Block=@block AND Hour=@hour";
                                MyCom.Parameters["@hour"].Value = DC.Index + 12;
                                try
                                {
                                    MyCom.Parameters["@mb"].Value = double.Parse(MRCurGrid2.Rows[3].Cells[DC.Index].Value.ToString());
                                }
                                catch (Exception e)
                                {
                                    MyCom.Parameters["@mb"].Value = "0";
                                }
                                try
                                {
                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception e)
                                {
                                    string a = e.Message;
                                }
                            }
                        }
                    }
                }
                //else MessageBox.Show("No Data for selected Date!");
            }

        }
        //-------------------------------FillBDUnitGrid-----------------------------------
        private void FillBDUnitGrid()
        {
            BDCurGrid.DataSource = null;
            if (BDCurGrid.Rows != null) BDCurGrid.Rows.Clear();

            if (!BDCal.IsNull)
            {
                string unit = BDUnitLb.Text.Trim();
                string temp = unit;
                string package = BDPackLb.Text.Trim();
                //Get Index Of Selected Unit(Row) in GridView (In Generad data Tab)
                int index = 0;
                if (GDSteamGroup.Text.Contains(package))
                    for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                    {
                        if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                            index = i;
                    }
                else
                    for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                    {
                        if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                            index = i;
                    }

                //Build the Bolck for FRM002
                temp = temp.ToLower();
                if (package.Contains("Combined"))
                {
                    string packagecode = "";
                    if (GDGasGroup.Text.Contains("Combined"))
                        packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                    else packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                    temp = "C" + packagecode;
                }
                else
                {
                    if (temp.Contains("gas"))
                        temp = temp.Replace("gas", "G");
                    else if (temp.Contains("steam"))
                        temp = temp.Replace("steam", "S");
                }
                temp = temp.Trim();

                SqlConnection MyCon = ConnectionManager.GetInstance().Connection;

                DataSet PowerDS = new DataSet();
                SqlDataAdapter Powerda = new SqlDataAdapter();

                Powerda.SelectCommand = new SqlCommand("SELECT Hour,Power1,Price1,Power2,Price2,Power3,Price3,Power4," +
                "Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10 " +
                "FROM [DetailFRM002] WHERE TargetMarketDate=@date AND Block=@temp AND PPID=" + PPID, MyCon);
                Powerda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Powerda.SelectCommand.Parameters["@date"].Value = BDCal.Text;
                Powerda.SelectCommand.Parameters.Add("@temp", SqlDbType.NChar, 20);
                Powerda.SelectCommand.Parameters["@temp"].Value = temp;
                Powerda.Fill(PowerDS);
                BDCurGrid.DataSource = PowerDS.Tables[0].DefaultView;
            }
        }
        //---------------------------------FillMRPlantGrid--------------------------------
        private void FillMRPlantGrid()
        {
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            if (!MRCal.IsNull)
            {

                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();

                Myda.SelectCommand = new SqlCommand("SELECT Hour,SUM(Required) AS SR FROM [DetailFRM005] WHERE PPID= " + PPID + " AND TargetMarketDate=@date GROUP BY Hour", MyConnection);
                Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                Myda.Fill(MyDS);
                MRCurGrid1.Rows[0].Cells[0].Value = "Power";
                MRCurGrid2.Rows[0].Cells[0].Value = "Power";
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    int index = int.Parse(MyRow["Hour"].ToString());
                    if (index <= 12)
                        MRCurGrid1.Rows[0].Cells[index].Value = MyRow["SR"];
                    else
                        MRCurGrid2.Rows[0].Cells[index - 12].Value = MyRow["SR"];
                }
            }
        }
        //---------------------------FillBDUnit---------------------------------------
        private void FillBDUnit(string unit, string package, int index)
        {
            BDStateTb.Text = MRUnitStateTb.Text;
            BDPowerTb.Text = MRUnitPowerTb.Text;
            BDMaxBidTb.Text = MRUnitMaxBidTb.Text;

            if (!BDCal.IsNull) FillBDUnitGrid();
        }
        //-------------------------------------ClearFRUnitTab()--------------------------------
        private void ClearFRUnitTab()
        {
            FRUnitCapitalTb.Text = "";
            FRUnitFixedTb.Text = "";
            FRUnitVariableTb.Text = "";
            FRUnitAmargTb1.Text = "";
            FRUnitAmargTb2.Text = "";
            FRUnitBmargTb1.Text = "";
            FRUnitBmargTb2.Text = "";
            FRUnitCmargTb1.Text = "";
            FRUnitCmargTb2.Text = "";
            FRUnitAmainTb.Text = "";
            FRUnitBmainTb.Text = "";
            FRUnitColdTb.Text = "";
            FRUnitHotTb.Text = "";
            FRUnitCapacityTb.Text = "";
            FRUnitTotalPowerTb.Text = "";
            FRUnitULPowerTb.Text = "";
            FRUnitCapPayTb.Text = "";
            FRUnitEneryPayTb.Text = "";
            FRUnitIncomeTb.Text = "";
        }
        //-----------------------------------FillFRUnit---------------------------------------
        private void FillFRUnit(string unit, string package, int index)
        {
            ClearFRUnitTab();
            SqlConnection FRCon = ConnectionManager.GetInstance().Connection;
            //Fill Cost Function GroupBox
            DataSet FRDS = new DataSet();
            SqlDataAdapter FRda = new SqlDataAdapter();
            FRda.SelectCommand = new SqlCommand("SELECT CapitalCost,FixedCost,VariableCost,PrimaryFuelAmargin," +
            "PrimaryFuelBmargin,PrimaryFuelCmargin,SecondFuelAmargin,SecondFuelBmargin,SecondFuelCmargin," +
            "BMaintenance,CMaintenance,CostStartCold,CostStartHot FROM [UnitsDataMain] WHERE PPID=" + PPID +
            " AND PackageType=@type AND UnitCode LIKE '" + unit + "%'", FRCon);
            FRda.SelectCommand.Parameters.Add("@type", SqlDbType.NChar, 10);
            string type = package;
            if (type.Contains("Combined")) type = "CC";
            FRda.SelectCommand.Parameters["@type"].Value = type;
            FRda.Fill(FRDS);
            foreach (DataRow MyRow in FRDS.Tables[0].Rows)
            {
                FRUnitCapitalTb.Text = MyRow[0].ToString().Trim();
                FRUnitFixedTb.Text = MyRow[1].ToString();
                FRUnitVariableTb.Text = MyRow[2].ToString();
                FRUnitAmargTb1.Text = MyRow[3].ToString();
                FRUnitBmargTb1.Text = MyRow[4].ToString();
                FRUnitCmargTb1.Text = MyRow[5].ToString();
                FRUnitAmargTb2.Text = MyRow[6].ToString();
                FRUnitBmargTb2.Text = MyRow[7].ToString();
                FRUnitCmargTb2.Text = MyRow[8].ToString();
                FRUnitAmainTb.Text = MyRow[9].ToString();
                FRUnitBmainTb.Text = MyRow[10].ToString();
                FRUnitColdTb.Text = MyRow[11].ToString();
                FRUnitHotTb.Text = MyRow[12].ToString();
            }

            //Fill Revenue GroupBox
            FillFRUnitRevenue(unit, package, index);

        }
        //-------------------------------------FillFRUnitRevenue()--------------------------------------
        private void FillFRUnitRevenue(string unit, string package, int index)
        {
            if ((!FRUnitCal.IsNull) && (FRUnitLb.Text != ""))
            {
                FRUnitCapacityTb.Text = "";
                FRUnitTotalPowerTb.Text = "";
                FRUnitULPowerTb.Text = "";
                FRUnitCapPayTb.Text = "";
                FRUnitEneryPayTb.Text = "";
                FRUnitIncomeTb.Text = "";

                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT AvailableCapacity,TotalPower,ULPower," +
                "CapacityPayment,EnergyPayment,Income FROM [EconomicUnit] WHERE UnitCode=@Punit AND Date=@date AND PPID=" + PPID, MyConnection);
                Myda.SelectCommand.Parameters.Add("@Punit", SqlDbType.NChar, 20);
                string Pcode = "";
                if (GDSteamGroup.Text.Contains(package))
                    Pcode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                else Pcode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                string Punit = unit;
                if (package.Contains("Combined"))
                    Punit = "C" + Pcode;
                Punit = Punit.Trim();
                Myda.SelectCommand.Parameters["@Punit"].Value = Punit;
                Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Myda.SelectCommand.Parameters["@date"].Value = FRUnitCal.Text;
                Myda.Fill(MyDS);
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    FRUnitCapacityTb.Text = MyRow[0].ToString();
                    FRUnitTotalPowerTb.Text = MyRow[1].ToString();
                    FRUnitULPowerTb.Text = MyRow[2].ToString();
                    FRUnitCapPayTb.Text = MyRow[3].ToString();
                    FRUnitEneryPayTb.Text = MyRow[4].ToString();
                    FRUnitIncomeTb.Text = MyRow[5].ToString();
                }
            
            }
        }
        //----------------------------------FillGDLine---------------------------------------
        private void FillGDLine(string TransType, string TransLine)
        {
            ClearGDUnitTab();


            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT FromBus,ToBus,CircuitID,LineR,LineX,LineSuseptance," +
            "LineLength,OutService,OutServiceStartDate,OutServiceEndDate,Owner_GencoFrom,Owner_GencoTo,PercentOwnerGencoFrom," +
            "PercentOwnerGencoTo,RateA,OutServiceType FROM [Lines] WHERE LineNumber=@num AND LineCode=@code", MyConnection);
            Myda.SelectCommand.Parameters.Add("@num", SqlDbType.Int);
            Myda.SelectCommand.Parameters["@num"].Value = TransType;
            Myda.SelectCommand.Parameters.Add("@code", SqlDbType.NChar, 20);
            Myda.SelectCommand.Parameters["@code"].Value = TransLine;
            Myda.Fill(MyDS);
            foreach (DataRow MyRow in MyDS.Tables[0].Rows)
            {
                GDcapacityTB.Text = MyRow[14].ToString();
                GDPmaxTB.Text = MyRow[0].ToString();
                GDPminTB.Text = MyRow[1].ToString();
                GDTimeUpTB.Text = MyRow[2].ToString();
                GDTimeDownTB.Text = MyRow[3].ToString();
                GDTimeColdStartTB.Text = MyRow[4].ToString();
                GDTimeHotStartTB.Text = MyRow[5].ToString();
                GDRampRateTB.Text = MyRow[6].ToString();
                GDIntConsumeTB.Text = "Work";
                string temp = MyRow[7].ToString();
                if (MyRow[7].ToString() == "True")
                {
                    //Detect Farsi Date
                    System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
                    DateTime CurDate = DateTime.Now;
                    string day = pr.GetDayOfMonth(CurDate).ToString();
                    if (int.Parse(day) < 10) day = "0" + day;
                    string mydate = pr.GetYear(CurDate) + "/" + pr.GetMonth(CurDate) + "/" + day;
                    int myhour = CurDate.Hour;

                    GDStartDateCal.Text = MyRow[8].ToString();
                    GDEndDateCal.Text = MyRow[9].ToString();
                    if (CheckDate(mydate, GDStartDateCal.Text, GDEndDateCal.Text))
                        GDIntConsumeTB.Text = "OutService";
                    GDMainTypeTB.Text = MyRow[15].ToString();
                }
                string from = MyRow[10].ToString().ToLower();
                string to = MyRow[11].ToString().ToLower();
                if (!from.Contains("tehran"))
                {
                    GDStateTB.Text = MyRow[10].ToString();
                    GDFuelTB.Text = MyRow[12].ToString();
                }
                else if (!to.Contains("tehran"))
                {
                    GDStateTB.Text = MyRow[11].ToString();
                    GDFuelTB.Text = MyRow[13].ToString();
                }
                else
                {
                    GDStateTB.Text = "Tehran";
                    GDFuelTB.Text = "100";
                }

            }
        }
        //------------------------OutServiceCheck_CheckStateChanged-------------------------
        private void OutServiceCheck_CheckStateChanged(object sender, EventArgs e)
        {
            if (OutServiceCheck.Checked)
            {
                ODServiceStartDate.Enabled = true;
                ODServiceEndDate.Enabled = true;
            }
            else
            {
                ODServiceStartDate.Enabled = false;
                ODServiceEndDate.Enabled = false;
            }
        }
        //---------------------------------SecondFuelCheck_CheckedChanged----------------------------
        private void SecondFuelCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (SecondFuelCheck.Checked)
            {
                ODFuelStartDate.Enabled = true;
                ODFuelEndDate.Enabled = true;
                ODFuelQuantityTB.Enabled = true;
            }
            else
            {
                ODFuelStartDate.Enabled = false;
                ODFuelEndDate.Enabled = false;
                ODFuelQuantityTB.Enabled = false;
            }
        }
        //------------------------------ODPowerMinCheck_CheckedChanged-----------------------------
        private void ODPowerMinCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (ODPowerMinCheck.Checked)
            {
                ODPowerStartDate.Enabled = true;
                ODPowerEndDate.Enabled = true;
                ODPowerGrid1.Enabled = true;
                ODPowerGrid2.Enabled = true;
            }
            else
            {
                ODPowerStartDate.Enabled = false;
                ODPowerEndDate.Enabled = false;
                ODPowerGrid1.Enabled = false;
                ODPowerGrid2.Enabled = false;
            }
        }
        //-------------------------------FRUpdateBtn_Click-------------------------------
        private void FRUpdateBtn_Click(object sender, EventArgs e)
        {
            //We Are In Plant Panel
            //if ((FRPlantPanel.Visible) && (!FRPlantCal.IsNull))
            //{
            //    bool check = false;
            //    if ((errorProvider1.GetError(FRPlantBenefitTB) == "") && (errorProvider1.GetError(FRPlantDecPayTb) == "")
            //     && (errorProvider1.GetError(FRPlantIncomeTb) == "") && (errorProvider1.GetError(FRPlantCostTb) == "")
            //     && (errorProvider1.GetError(FRPlantAvaCapTb) == "") && (errorProvider1.GetError(FRPlantTotalPowerTb) == "")
            //     && (errorProvider1.GetError(FRPlantBidPowerTb) == "") && (errorProvider1.GetError(FRPlantULPowerTb) == "")
            //     && (errorProvider1.GetError(FRPlantIncPowerTb) == "") && (errorProvider1.GetError(FRPlantDecPowerTb) == "")
            //     && (errorProvider1.GetError(FRPlantCapPayTb) == "") && (errorProvider1.GetError(FRPlantEnergyPayTb) == "")
            //     && (errorProvider1.GetError(FRPlantBidPayTb) == "") && (errorProvider1.GetError(FRPlantULPayTb) == "")
            //     && (errorProvider1.GetError(FRPlantIncPayTb) == ""))
            //        check = true;
            //    if (check)
            //    {
            //        SqlCommand MyCom = new SqlCommand();
            //        MyCom.CommandText = "INSERT INTO EconomicPlant (PPID,Date,Benefit,Income,Cost,AvailableCapacity,TotalPower," +
            //        "BidPower,ULPower,IncrementPower,DecreasePower,CapacityPayment,EnergyPayment,BidPayment,ULpayment," +
            //        "IncrementPayment,DecreasePayment) VALUES (@num,@date,@benefit,@income,@cost,@AvaCap,@totalpo,@bidpo,@ulpo," +
            //        "@incpo,@decpo,@cappa,@energypa,@bidpa,@ulpa,@incpa,@decpa)";
            //        MyCom.Connection = MyConnection;
            //        MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            //        MyCom.Parameters["@num"].Value = PPID;
            //        MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            //        MyCom.Parameters["@date"].Value = FRPlantCal.Text;
            //        MyCom.Parameters.Add("@benefit", SqlDbType.Real);
            //        if (FRPlantBenefitTB.Text != "")
            //            MyCom.Parameters["@benefit"].Value = double.Parse(FRPlantBenefitTB.Text);
            //        else MyCom.Parameters["@benefit"].Value = 0;
            //        MyCom.Parameters.Add("@income", SqlDbType.Real);
            //        if (FRPlantIncomeTb.Text != "")
            //            MyCom.Parameters["@income"].Value = double.Parse(FRPlantIncomeTb.Text);
            //        else MyCom.Parameters["@income"].Value = 0;
            //        MyCom.Parameters.Add("@cost", SqlDbType.Real);
            //        if (FRPlantCostTb.Text != "")
            //            MyCom.Parameters["@cost"].Value = double.Parse(FRPlantCostTb.Text);
            //        else MyCom.Parameters["@cost"].Value = 0;
            //        MyCom.Parameters.Add("@AvaCap", SqlDbType.Real);
            //        if (FRPlantAvaCapTb.Text != "")
            //            MyCom.Parameters["@AvaCap"].Value = double.Parse(FRPlantAvaCapTb.Text);
            //        else MyCom.Parameters["@AvaCap"].Value = 0;
            //        MyCom.Parameters.Add("@totalpo", SqlDbType.Real);
            //        if (FRPlantTotalPowerTb.Text != "")
            //            MyCom.Parameters["@totalpo"].Value = double.Parse(FRPlantTotalPowerTb.Text);
            //        else MyCom.Parameters["@totalpo"].Value = 0;
            //        MyCom.Parameters.Add("@bidpo", SqlDbType.Real);
            //        if (FRPlantBidPowerTb.Text != "")
            //            MyCom.Parameters["@bidpo"].Value = double.Parse(FRPlantBidPowerTb.Text);
            //        else MyCom.Parameters["@bidpo"].Value = 0;
            //        MyCom.Parameters.Add("@ulpo", SqlDbType.Real);
            //        if (FRPlantULPowerTb.Text != "")
            //            MyCom.Parameters["@ulpo"].Value = double.Parse(FRPlantULPowerTb.Text);
            //        else MyCom.Parameters["@ulpo"].Value = 0;
            //        MyCom.Parameters.Add("@incpo", SqlDbType.Real);
            //        if (FRPlantIncPowerTb.Text != "")
            //            MyCom.Parameters["@incpo"].Value = double.Parse(FRPlantIncPowerTb.Text);
            //        else MyCom.Parameters["@incpo"].Value = 0;
            //        MyCom.Parameters.Add("@decpo", SqlDbType.Real);
            //        if (FRPlantDecPowerTb.Text != "")
            //            MyCom.Parameters["@decpo"].Value = double.Parse(FRPlantDecPowerTb.Text);
            //        else MyCom.Parameters["@decpo"].Value = 0;
            //        MyCom.Parameters.Add("@cappa", SqlDbType.Real);
            //        if (FRPlantCapPayTb.Text != "")
            //            MyCom.Parameters["@cappa"].Value = double.Parse(FRPlantCapPayTb.Text);
            //        else MyCom.Parameters["@cappa"].Value = 0;
            //        MyCom.Parameters.Add("@energypa", SqlDbType.Real);
            //        if (FRPlantEnergyPayTb.Text != "")
            //            MyCom.Parameters["@energypa"].Value = double.Parse(FRPlantEnergyPayTb.Text);
            //        else MyCom.Parameters["@energypa"].Value = 0;
            //        MyCom.Parameters.Add("@bidpa", SqlDbType.Real);
            //        if (FRPlantBidPayTb.Text != "")
            //            MyCom.Parameters["@bidpa"].Value = double.Parse(FRPlantBidPayTb.Text);
            //        else MyCom.Parameters["@bidpa"].Value = 0;
            //        MyCom.Parameters.Add("@ulpa", SqlDbType.Real);
            //        if (FRPlantULPayTb.Text != "")
            //            MyCom.Parameters["@ulpa"].Value = double.Parse(FRPlantULPayTb.Text);
            //        else MyCom.Parameters["@ulpa"].Value = 0;
            //        MyCom.Parameters.Add("@incpa", SqlDbType.Real);
            //        if (FRPlantIncPayTb.Text != "")
            //            MyCom.Parameters["@incpa"].Value = double.Parse(FRPlantIncPayTb.Text);
            //        else MyCom.Parameters["@incpa"].Value = 0;
            //        MyCom.Parameters.Add("@decpa", SqlDbType.Real);
            //        if (FRPlantDecPayTb.Text != "")
            //            MyCom.Parameters["@decpa"].Value = double.Parse(FRPlantDecPayTb.Text);
            //        else MyCom.Parameters["@decpa"].Value = 0;
            //        //try
            //        //{
            //        MyCom.ExecuteNonQuery();
            //        //}
            //        //catch (Exception exp)
            //        //{
            //        //  string str = exp.Message;
            //        //}
            //    }
            //    else MessageBox.Show("Please Fill Fields With Correct Values!");
            //}
            //else if ((FRPlantPanel.Visible) && (FRPlantCal.IsNull))
            //    MessageBox.Show("Please Fill Date Field!");

            //We Are In UnitPanel
            if (FRUnitPanel.Visible)
            {
                if ((errorProvider1.GetError(FRUnitHotTb) == "") && (errorProvider1.GetError(FRUnitFixedTb) == "")
                && (errorProvider1.GetError(FRUnitVariableTb) == "") && (errorProvider1.GetError(FRUnitAmargTb1) == "")
                && (errorProvider1.GetError(FRUnitBmargTb1) == "") && (errorProvider1.GetError(FRUnitCmargTb1) == "")
                && (errorProvider1.GetError(FRUnitAmargTb2) == "") && (errorProvider1.GetError(FRUnitBmargTb2) == "")
                && (errorProvider1.GetError(FRUnitCmargTb2) == "") && (errorProvider1.GetError(FRUnitColdTb) == "")
                && (errorProvider1.GetError(FRUnitAmainTb) == "") && (errorProvider1.GetError(FRUnitBmainTb) == ""))
                {
                    SqlCommand MyCom = new SqlCommand();
                    MyCom.CommandText = "UPDATE UnitsDataMain SET CapitalCost=@Ccost,FixedCost=@Fcost,VariableCost=@Vcost," +
                    "PrimaryFuelAmargin=@Amargin1,PrimaryFuelBmargin=@Bmargin1,PrimaryFuelCmargin=@Cmargin1,SecondFuelAmargin=@Amargin2," +
                    "SecondFuelBmargin=@Bmargin2,SecondFuelCmargin=@Cmargin2,CostStartCold=@cold,CostStartHot=@hot," +
                    "BMaintenance=@am,CMaintenance=@bm WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type";
                    MyCom.Connection = MyConnection;
                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                    MyCom.Parameters["@num"].Value = PPID;
                    MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
                    MyCom.Parameters["@unit"].Value = FRUnitLb.Text;
                    MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
                    string type = FRPackLb.Text;
                    if (type.Contains("Combined")) type = "CC";
                    MyCom.Parameters["@type"].Value = type;
                    MyCom.Parameters.Add("@Ccost", SqlDbType.NChar, 10);
                    MyCom.Parameters["@Ccost"].Value = FRUnitCapitalTb.Text;
                    MyCom.Parameters.Add("@Fcost", SqlDbType.Real);
                    if (FRUnitFixedTb.Text != "")
                        MyCom.Parameters["@Fcost"].Value = FRUnitFixedTb.Text;
                    else MyCom.Parameters["@Fcost"].Value = 0;
                    MyCom.Parameters.Add("@Vcost", SqlDbType.Real);
                    if (FRUnitVariableTb.Text != "")
                        MyCom.Parameters["@Vcost"].Value = FRUnitVariableTb.Text;
                    else MyCom.Parameters["@Vcost"].Value = 0;
                    MyCom.Parameters.Add("@Amargin1", SqlDbType.Real);
                    if (FRUnitAmargTb1.Text != "")
                        MyCom.Parameters["@Amargin1"].Value = FRUnitAmargTb1.Text;
                    else MyCom.Parameters["@Amargin1"].Value = 0;
                    MyCom.Parameters.Add("@Bmargin1", SqlDbType.Real);
                    if (FRUnitBmargTb1.Text != "")
                        MyCom.Parameters["@Bmargin1"].Value = FRUnitBmargTb1.Text;
                    else MyCom.Parameters["@Bmargin1"].Value = 0;
                    MyCom.Parameters.Add("@Cmargin1", SqlDbType.Real);
                    if (FRUnitCmargTb1.Text != "")
                        MyCom.Parameters["@Cmargin1"].Value = FRUnitCmargTb1.Text;
                    else MyCom.Parameters["@Cmargin1"].Value = 0;
                    MyCom.Parameters.Add("@Amargin2", SqlDbType.Real);
                    if (FRUnitAmargTb2.Text != "")
                        MyCom.Parameters["@Amargin2"].Value = FRUnitAmargTb2.Text;
                    else MyCom.Parameters["@Amargin2"].Value = 0;
                    MyCom.Parameters.Add("@Bmargin2", SqlDbType.Real);
                    if (FRUnitBmargTb2.Text != "")
                        MyCom.Parameters["@Bmargin2"].Value = FRUnitBmargTb2.Text;
                    else MyCom.Parameters["@Bmargin2"].Value = 0;
                    MyCom.Parameters.Add("@Cmargin2", SqlDbType.Real);
                    if (FRUnitCmargTb2.Text != "")
                        MyCom.Parameters["@Cmargin2"].Value = FRUnitCmargTb2.Text;
                    else MyCom.Parameters["@Cmargin2"].Value = 0;
                    MyCom.Parameters.Add("@cold", SqlDbType.Real);
                    if (FRUnitColdTb.Text != "")
                        MyCom.Parameters["@cold"].Value = FRUnitColdTb.Text;
                    else MyCom.Parameters["@cold"].Value = 0;
                    MyCom.Parameters.Add("@hot", SqlDbType.Real);
                    if (FRUnitHotTb.Text != "")
                        MyCom.Parameters["@hot"].Value = FRUnitHotTb.Text;
                    else MyCom.Parameters["@hot"].Value = 0;
                    MyCom.Parameters.Add("@am", SqlDbType.NChar, 10);
                    MyCom.Parameters["@am"].Value = FRUnitAmainTb.Text;
                    MyCom.Parameters.Add("@bm", SqlDbType.NChar, 10);
                    MyCom.Parameters["@bm"].Value = FRUnitBmainTb.Text;
                    //try
                    //{
                    MyCom.ExecuteNonQuery();
                    //}
                    //catch (Exception exp)
                    //{
                    //  string str = exp.Message;
                    //}
                }
                FRUnitCapitalTb.ReadOnly = true;
                FRUnitFixedTb.ReadOnly = true;
                FRUnitVariableTb.ReadOnly = true;
                FRUnitBmainTb.ReadOnly = true;
                FRUnitAmainTb.ReadOnly = true;
                FRUnitHotTb.ReadOnly = true;
                FRUnitColdTb.ReadOnly = true;
                FRUnitCmargTb2.ReadOnly = true;
                FRUnitCmargTb1.ReadOnly = true;
                FRUnitBmargTb2.ReadOnly = true;
                FRUnitBmargTb1.ReadOnly = true;
                FRUnitAmargTb2.ReadOnly = true;
                FRUnitAmargTb1.ReadOnly = true;
            }

        }
        //----------------------------------PlantGV1_ColumnHeaderMouseClick-------------------------------
        private void PlantGV1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Detect Farsi Date
            System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
            DateTime CurDate = DateTime.Now;
            string day = pr.GetDayOfMonth(CurDate).ToString();
            if (int.Parse(day) < 10) day = "0" + day;
            string mydate = pr.GetYear(CurDate) + "/" + pr.GetMonth(CurDate) + "/" + day;
            int myhour = CurDate.Hour;
            string type = GDGasGroup.Text;
            type = type.Trim();
            if (type == "Combined Cycle") type = "CC";
            FillPlantGV1Remained(mydate, PPID.ToString(), type, myhour);

        }
        //----------------------------PlantGV2_ColumnHeaderMouseClick-----------------------------------------
        private void PlantGV2_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Detect Farsi Date
            System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
            DateTime CurDate = DateTime.Now;
            string day = pr.GetDayOfMonth(CurDate).ToString();
            if (int.Parse(day) < 10) day = "0" + day;
            string mydate = pr.GetYear(CurDate) + "/" + pr.GetMonth(CurDate) + "/" + day;
            int myhour = CurDate.Hour;
            string type = GDSteamGroup.Text;
            type = type.Trim();
            if (type == "Combined Cycle") type = "CC";
            FillPlantGV2Remained(mydate, PPID.ToString(), type, myhour);
        }
        //------------------------------------ODFuelQuantityTB_Validated--------------------------------
        private void ODFuelQuantityTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(ODFuelQuantityTB.Text, 1))
                errorProvider1.SetError(ODFuelQuantityTB, "");
            else if (SecondFuelCheck.Checked)
                errorProvider1.SetError(ODFuelQuantityTB, "just real number!");
        }
        //---------------------------------CheckValidated----------------------------------------
        private bool CheckValidated(string text, int type)
        {
            if (text != "")
            {
                if (type == 1)
                {
                    try
                    {
                        double k = double.Parse(text);
                        return true;
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
                else
                {
                    try
                    {
                        int k = int.Parse(text);
                        return true;
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
            }
            else return true;
        }
        //---------------------------ODPowerGrid1_Validated-----------------------------
        private void ODPowerGrid1_Validated(object sender, EventArgs e)
        {
            for (int i = 0; i < (ODPowerGrid1.ColumnCount - 1); i++)
            {
                if (ODPowerGrid1.Rows[0].Cells[i].Value != null)
                {
                    if (CheckValidated(ODPowerGrid1.Rows[0].Cells[i].Value.ToString(), 1))
                        errorProvider1.SetError(ODPowerGrid1, "");
                    else if (ODPowerMinCheck.Checked)
                        errorProvider1.SetError(ODPowerGrid1, "Just real number!");
                }
            }
        }
        //----------------------------ODPowerGrid2_Validated-------------------------
        private void ODPowerGrid2_Validated(object sender, EventArgs e)
        {
            for (int i = 0; i < (ODPowerGrid2.ColumnCount - 1); i++)
            {
                if (ODPowerGrid2.Rows[0].Cells[i].Value != null)
                {
                    if (CheckValidated(ODPowerGrid2.Rows[0].Cells[i].Value.ToString(), 1))
                        errorProvider1.SetError(ODPowerGrid2, "");
                    else if (ODPowerMinCheck.Checked)
                        errorProvider1.SetError(ODPowerGrid2, "Just real number!");
                }
            }
        }
        //-----------------------------ODUnitFuelTB_Validated----------------------------------
        private void ODUnitFuelTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(ODUnitFuelTB.Text, 1))
                errorProvider1.SetError(ODUnitFuelTB, "");
            else if (ODUnitFuelCheck.Checked)
                errorProvider1.SetError(ODUnitFuelTB, "just real number!");
        }
        //-------------------------------ODUnitPowerGrid1_Validated-------------------------------------
        private void ODUnitPowerGrid1_Validated(object sender, EventArgs e)
        {
            for (int i = 0; i < (ODUnitPowerGrid1.ColumnCount - 1); i++)
            {
                if (ODUnitPowerGrid1.Rows[0].Cells[i].Value != null)
                {
                    if (CheckValidated(ODUnitPowerGrid1.Rows[0].Cells[i].Value.ToString(), 1))
                        errorProvider1.SetError(ODUnitPowerGrid1, "");
                    else if (ODUnitPowerCheck.Checked)
                        errorProvider1.SetError(ODUnitPowerGrid1, "Just real number!");
                }
            }
        }
        //-------------------------------ODUnitPowerGrid2_Validated-------------------------------
        private void ODUnitPowerGrid2_Validated(object sender, EventArgs e)
        {
            for (int i = 0; i < (ODUnitPowerGrid2.ColumnCount - 1); i++)
            {
                if (ODUnitPowerGrid2.Rows[0].Cells[i].Value != null)
                {
                    if (CheckValidated(ODUnitPowerGrid2.Rows[0].Cells[i].Value.ToString(), 1))
                        errorProvider1.SetError(ODUnitPowerGrid2, "");
                    else if (ODUnitPowerCheck.Checked)
                        errorProvider1.SetError(ODUnitPowerGrid2, "Just Number!");
                }
            }
        }
        //---------------------------ODUnitPowerCheck_CheckedChanged--------------------------------------
        private void ODUnitPowerCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (ODUnitPowerCheck.Checked)
            {
                ODUnitPowerStartDate.Enabled = true;
                ODUnitPowerEndDate.Enabled = true;
                ODUnitPowerGrid1.Enabled = true;
                ODUnitPowerGrid2.Enabled = true;
            }
            else
            {
                ODUnitPowerStartDate.Enabled = false;
                ODUnitPowerEndDate.Enabled = false;
                ODUnitPowerGrid1.Enabled = false;
                ODUnitPowerGrid2.Enabled = false;
            }
        }
        //----------------------------------ODUnitOutCheck_CheckedChanged------------------------------
        private void ODUnitOutCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (ODUnitOutCheck.Checked)
            {
                ODUnitServiceStartDate.Enabled = true;
                ODUnitServiceEndDate.Enabled = true;
            }
            else
            {
                ODUnitServiceStartDate.Enabled = false;
                ODUnitServiceEndDate.Enabled = false;
            }
        }
        //-----------------------------ODUnitMainCheck_CheckedChanged-------------------------------
        private void ODUnitMainCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (ODUnitMainCheck.Checked)
            {
                ODUnitMainStartDate.Enabled = true;
                ODUnitMainEndDate.Enabled = true;
            }
            else
            {
                ODUnitMainStartDate.Enabled = false;
                ODUnitMainEndDate.Enabled = false;
            }
        }
        //--------------------------ODUnitFuelCheck_CheckedChanged--------------------------------
        private void ODUnitFuelCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (ODUnitFuelCheck.Checked)
            {
                ODUnitFuelStartDate.Enabled = true;
                ODUnitFuelEndDate.Enabled = true;
                ODUnitFuelTB.Enabled = true;
            }
            else
            {
                ODUnitFuelStartDate.Enabled = false;
                ODUnitFuelEndDate.Enabled = false;
                ODUnitFuelTB.Enabled = false;
            }
        }
        //--------------------------FRPlantBenefitTB_Validated--------------------------------
        private void FRPlantBenefitTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantBenefitTB.Text, 1))
                errorProvider1.SetError(FRPlantBenefitTB, "");
            else errorProvider1.SetError(FRPlantBenefitTB, "just real number!");
        }
        //------------------------------FRPlantIncomeTb_Validated------------------------------
        private void FRPlantIncomeTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantIncomeTb.Text, 1))
                errorProvider1.SetError(FRPlantIncomeTb, "");
            else errorProvider1.SetError(FRPlantIncomeTb, "just real number!");
        }
        //-------------------------------FRPlantCostTb_Validated---------------------------------
        private void FRPlantCostTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantCostTb.Text, 1))
                errorProvider1.SetError(FRPlantCostTb, "");
            else errorProvider1.SetError(FRPlantCostTb, "just real number!");
        }
        //---------------------------------FRPlantAvaCapTb_Validated----------------------------------
        private void FRPlantAvaCapTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantAvaCapTb.Text, 1))
                errorProvider1.SetError(FRPlantAvaCapTb, "");
            else errorProvider1.SetError(FRPlantAvaCapTb, "just real number!");
        }
        //---------------------------------FRPlantTotalPowerTb_Validated---------------------------------
        private void FRPlantTotalPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantTotalPowerTb.Text, 1))
                errorProvider1.SetError(FRPlantTotalPowerTb, "");
            else errorProvider1.SetError(FRPlantTotalPowerTb, "just real number!");
        }
        //------------------------------------FRPlantBidPowerTb_Validated-------------------------------------
        private void FRPlantBidPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantBidPowerTb.Text, 1))
                errorProvider1.SetError(FRPlantBidPowerTb, "");
            else errorProvider1.SetError(FRPlantBidPowerTb, "just real number!");
        }
        //--------------------------------------FRPlantULPowerTb_Validated--------------------------------------
        private void FRPlantULPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantULPowerTb.Text, 1))
                errorProvider1.SetError(FRPlantULPowerTb, "");
            else errorProvider1.SetError(FRPlantULPowerTb, "just real number!");
        }
        //---------------------------------------FRPlantIncPowerTb_Validated---------------------------------
        private void FRPlantIncPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantIncPowerTb.Text, 1))
                errorProvider1.SetError(FRPlantIncPowerTb, "");
            else errorProvider1.SetError(FRPlantIncPowerTb, "just real number!");
        }
        //---------------------------------------FRPlantDecPowerTb_Validated------------------------------------
        private void FRPlantDecPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantDecPowerTb.Text, 1))
                errorProvider1.SetError(FRPlantDecPowerTb, "");
            else errorProvider1.SetError(FRPlantDecPowerTb, "just real number!");
        }
        //----------------------------------------FRPlantCapPayTb_Validated-----------------------------------
        private void FRPlantCapPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantCapPayTb.Text, 1))
                errorProvider1.SetError(FRPlantCapPayTb, "");
            else errorProvider1.SetError(FRPlantCapPayTb, "just real number!");
        }
        //---------------------------------------FRPlantEnergyPayTb_Validated-------------------------------------
        private void FRPlantEnergyPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantEnergyPayTb.Text, 1))
                errorProvider1.SetError(FRPlantEnergyPayTb, "");
            else errorProvider1.SetError(FRPlantEnergyPayTb, "just real number!");
        }
        //------------------------------------FRPlantBidPayTb_Validated--------------------------------------------
        private void FRPlantBidPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantBidPayTb.Text, 1))
                errorProvider1.SetError(FRPlantBidPayTb, "");
            else errorProvider1.SetError(FRPlantBidPayTb, "just real number!");
        }
        //------------------------------------FRPlantULPayTb_Validated----------------------------------------
        private void FRPlantULPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantULPayTb.Text, 1))
                errorProvider1.SetError(FRPlantULPayTb, "");
            else errorProvider1.SetError(FRPlantULPayTb, "just real number!");
        }
        //------------------------------------FRPlantIncPayTb_Validated-----------------------------------
        private void FRPlantIncPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantIncPayTb.Text, 1))
                errorProvider1.SetError(FRPlantIncPayTb, "");
            else errorProvider1.SetError(FRPlantIncPayTb, "just real number!");
        }
        //-----------------------------------FRPlantDecPayTb_Validated----------------------------------------
        private void FRPlantDecPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantDecPayTb.Text, 1))
                errorProvider1.SetError(FRPlantDecPayTb, "");
            else errorProvider1.SetError(FRPlantDecPayTb, "just real number!");
        }
        //-----------------------------------RUnitCapacityTb_Validated----------------------------------------
        private void FRUnitCapacityTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitCapacityTb.Text, 1))
                errorProvider1.SetError(FRUnitCapacityTb, "");
            else errorProvider1.SetError(FRUnitCapacityTb, "just real number!");
        }
        //------------------------------------FRUnitTotalPowerTb_Validated-------------------------------------
        private void FRUnitTotalPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitTotalPowerTb.Text, 1))
                errorProvider1.SetError(FRUnitTotalPowerTb, "");
            else errorProvider1.SetError(FRUnitTotalPowerTb, "just real number!");
        }
        //-----------------------------------FRUnitULPowerTb_Validated-------------------------------------------
        private void FRUnitULPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitULPowerTb.Text, 1))
                errorProvider1.SetError(FRUnitULPowerTb, "");
            else errorProvider1.SetError(FRUnitULPowerTb, "just real number!");
        }
        //-------------------------------------FRUnitCapPayTb_Validated--------------------------------------------
        private void FRUnitCapPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitCapPayTb.Text, 1))
                errorProvider1.SetError(FRUnitCapPayTb, "");
            else errorProvider1.SetError(FRUnitCapPayTb, "just real number!");
        }
        //--------------------------------------FRUnitEneryPayTb_Validated-----------------------------------------
        private void FRUnitEneryPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitEneryPayTb.Text, 1))
                errorProvider1.SetError(FRUnitEneryPayTb, "");
            else errorProvider1.SetError(FRUnitEneryPayTb, "just real number!");
        }
        //---------------------------------------FRUnitIncomeTb_Validated------------------------------------------
        private void FRUnitIncomeTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitIncomeTb.Text, 1))
                errorProvider1.SetError(FRUnitIncomeTb, "");
            else errorProvider1.SetError(FRUnitIncomeTb, "just real number!");
        }
        //-------------------------------------------GDDeleteBtn_Click----------------------------------------------
        private void GDDeleteBtn_Click(object sender, EventArgs e)
        {
            //We Are in GDPlantPanel
            if (GDDeleteBtn.Text.Contains("Delete"))
            {
                // A Plant is selected
                if ((PlantGV1.Visible) || (PlantGV2.Visible))
                {
                    bool delete = true;
                    int first = 0;
                    if (PlantGV1.Visible)
                        foreach (DataGridViewRow dr in PlantGV1.Rows)
                        {
                            if ((dr.Cells[10].Value != null) && (dr.Cells[10].Value.ToString() == "1"))
                            //Cells[10] Because in cell 10th cell we have added checkbox          
                            {
                                if (first == 0)
                                {
                                    DialogResult result = MessageBox.Show("Are You Sure to delete Selected Row(s)?", "Warning", MessageBoxButtons.YesNo);
                                    if (result == DialogResult.No) delete = false;
                                }
                                if (delete)
                                {
                                    //delete this row
                                    SqlCommand MyCom = new SqlCommand();
                                    MyCom.CommandText = "DELETE FROM [UnitsDataMain] WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type";
                                    MyCom.Connection = MyConnection;
                                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                                    MyCom.Parameters["@num"].Value = PPID;
                                    MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
                                    MyCom.Parameters["@unit"].Value = dr.Cells[0].Value.ToString();
                                    MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
                                    string type = GDGasGroup.Text;
                                    if (type.Contains("Combined")) type = "CC";
                                    MyCom.Parameters["@type"].Value = type;
                                    //try
                                    //{
                                    MyCom.ExecuteNonQuery();
                                    //}
                                    //catch (Exception exp)
                                    //{
                                    //  string str = exp.Message;
                                    //}
                                    if (PlantGV1.RowCount <= 2)
                                    {
                                        MyCom.CommandText = "DELETE FROM [PPUnit] WHERE PPID=@num AND PackageType=@packagetype";
                                        MyCom.Parameters["@num"].Value = PPID;
                                        MyCom.Parameters.Add("@packagetype", SqlDbType.NChar, 20);
                                        MyCom.Parameters["@packagetype"].Value = GDGasGroup.Text;
                                        //try
                                        //{
                                        MyCom.ExecuteNonQuery();
                                        //}
                                        //catch (Exception exp)
                                        //{
                                        //  string str = exp.Message;
                                        //}

                                    }
                                }
                                first++;
                            }
                        }
                    if (PlantGV2.Visible)
                        foreach (DataGridViewRow dr in PlantGV2.Rows)
                        {
                            if ((dr.Cells[10].Value != null) && (dr.Cells[10].Value.ToString() == "1"))
                            //Cells[10] Because in cell 10th cell we have added checkbox          
                            {
                                if (first == 0)
                                {
                                    DialogResult result = MessageBox.Show("Are You Sure to delete Selected Row(s)?", "Warning", MessageBoxButtons.YesNo);
                                    if (result == DialogResult.No) delete = false;
                                }
                                if (delete)
                                {
                                    //delete this row
  
                                    SqlCommand MyCom = new SqlCommand();
                                    MyCom.CommandText = "DELETE FROM [UnitsDataMain] WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type";
                                    MyCom.Connection = MyConnection;
                                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                                    MyCom.Parameters["@num"].Value = PPID;
                                    MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
                                    MyCom.Parameters["@unit"].Value = dr.Cells[0].Value.ToString();
                                    MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
                                    string type = GDSteamGroup.Text;
                                    if (type.Contains("Combined")) type = "CC";
                                    MyCom.Parameters["@type"].Value = type;
                                    //try
                                    //{
                                    MyCom.ExecuteNonQuery();
                                    //}
                                    //catch (Exception exp)
                                    //{
                                    //  string str = exp.Message;
                                    //}
                                    if (PlantGV2.RowCount <= 2)
                                    {
                                        MyCom.CommandText = "DELETE FROM [PPUnit] WHERE PPID=@num AND PackageType=@packagetype2";
                                        MyCom.Parameters["@num"].Value = PPID;
                                        MyCom.Parameters.Add("@packagetype2", SqlDbType.NChar, 20);
                                        MyCom.Parameters["@packagetype2"].Value = GDSteamGroup.Text;
                                        //try
                                        //{
                                        MyCom.ExecuteNonQuery();
                                        //}
                                        //catch (Exception exp)
                                        //{
                                        //  string str = exp.Message;
                                        //}

                                    }

                                }
                                first++;
                            }
                        }
                    if (delete)
                    {
                        //PlantGV1.Rows[PlantGV1.RowCount - 1].Cells[PlantGV1.ColumnCount - 1].ReadOnly = true;
                        //PlantGV2.Rows[PlantGV2.RowCount - 1].Cells[PlantGV2.ColumnCount - 1].ReadOnly = true;
                        buildPPtree(PPID.ToString());
                        FillPlantGrid(PPID.ToString());
                    }
                }
                // A Transmission Line is selected
                if ((Grid230.Visible) || (Grid400.Visible))
                {
                    bool delete = true;
                    int first = 0;
                    if (Grid400.Visible)
                    {
                        foreach (DataGridViewRow dr in Grid400.Rows)
                        {
                            if ((dr.Cells[8].Value != null) && (dr.Cells[8].Value.ToString() == "1"))
                            //Cells[8] Because in cell 8th cell we have added checkbox          
                            {
                                if (first == 0)
                                {
                                    DialogResult result = MessageBox.Show("Are You Sure to delete Selected Row(s)?", "Warning", MessageBoxButtons.YesNo);
                                    if (result == DialogResult.No) delete = false;
                                }
                                if (delete)
                                {
                                    //delete this row
                                    SqlCommand MyCom = new SqlCommand();
                                    MyCom.CommandText = "DELETE FROM [Lines] WHERE LineNumber=@num AND LineCode=@code DELETE FROM [TransLine] WHERE LineNumber=@num AND LineCode=@code";
                                    MyCom.Connection = MyConnection;
                                    MyCom.Parameters.Add("@num", SqlDbType.Int);
                                    MyCom.Parameters["@num"].Value = int.Parse(GDGasGroup.Text);
                                    MyCom.Parameters.Add("@code", SqlDbType.NChar, 20);
                                    MyCom.Parameters["@code"].Value = dr.Cells[0].Value.ToString();
                                    //try
                                    //{
                                    MyCom.ExecuteNonQuery();
                                    //}
                                    //catch (Exception exp)
                                    //{
                                    //  string str = exp.Message;
                                    //}
                                }
                                first++;
                            }
                        }
                        buildTRANStree(GDGasGroup.Text);
                        FillTransmissionGrid(GDGasGroup.Text);
                    }
                    if (Grid230.Visible)
                    {
                        foreach (DataGridViewRow dr in Grid230.Rows)
                        {
                            if ((dr.Cells[8].Value != null) && (dr.Cells[8].Value.ToString() == "1"))
                            //Cells[8] Because in cell 8th cell we have added checkbox          
                            {
                                if (first == 0)
                                {
                                    DialogResult result = MessageBox.Show("Are You Sure to delete Selected Row(s)?", "Warning", MessageBoxButtons.YesNo);
                                    if (result == DialogResult.No) delete = false;
                                }
                                if (delete)
                                {
                                    //delete this row
                                    SqlCommand MyCom = new SqlCommand();
                                    MyCom.CommandText = "DELETE FROM [Lines] WHERE LineNumber=@num AND LineCode=@code DELETE FROM [TransLine] WHERE LineNumber=@num AND LineCode=@code";
                                    MyCom.Connection = MyConnection;
                                    MyCom.Parameters.Add("@num", SqlDbType.Int);
                                    MyCom.Parameters["@num"].Value = int.Parse(GDSteamGroup.Text);
                                    MyCom.Parameters.Add("@code", SqlDbType.NChar, 20);
                                    MyCom.Parameters["@code"].Value = dr.Cells[0].Value.ToString();
                                    //try
                                    //{
                                    MyCom.ExecuteNonQuery();
                                    //}
                                    //catch (Exception exp)
                                    //{
                                    //  string str = exp.Message;
                                    //}
                                }
                                first++;
                            }
                        }
                        buildTRANStree(GDSteamGroup.Text);
                        FillTransmissionGrid(GDSteamGroup.Text);
                    }
                }
            }
            //We Are in GDUnitPanel
            else if (GDDeleteBtn.Text.Contains("Edit"))
            {
                GDNewBtn.Enabled = true;
                GDcapacityTB.ReadOnly = false;
                GDPmaxTB.ReadOnly = false;
                GDPminTB.ReadOnly = false;
                GDTimeUpTB.ReadOnly = false;
                GDTimeDownTB.ReadOnly = false;
                GDTimeColdStartTB.ReadOnly = false;
                GDTimeHotStartTB.ReadOnly = false;
                GDRampRateTB.ReadOnly = false;
                if (Currentgb.Text.Contains("STATE")) GDIntConsumeTB.ReadOnly = false;
            }
        }
        //-----------------------------------addToolStripMenuItem_Click--------------------------------------------------
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //NewPlant.Show();
            //this.Hide();
            Form3 newPlant = new Form3();
            DialogResult result = newPlant.ShowDialog();
            if (result == DialogResult.OK) buildTreeView1();
        }
        //-------------------------------------PlantGV1_DataError-------------------------------------------------------
        private void PlantGV1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            if ((!PlantGV1.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 1))
            {
                try
                {
                    int i = int.Parse(PlantGV1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
        }
        //------------------------------------------PlantGV2_DataError----------------------------------------
        private void PlantGV2_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            if ((!PlantGV2.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 1))
            {
                try
                {
                    int i = int.Parse(PlantGV2.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
        }
        //----------------------------------------------GDNewBtn_Click-----------------------------------------------
        private void GDNewBtn_Click(object sender, EventArgs e)
        {
            //We Are in GDPlanttPanel
            if (GDNewBtn.Text.Contains("Add"))
            {
                //IF a Plant is selected
                if ((PlantGV1.Visible) || (PlantGV2.Visible))
                {
                    Form4 newUnit = new Form4();
                    DialogResult re = newUnit.ShowDialog();
                    if (re == DialogResult.OK)
                    {
                        string[] UnitResult = newUnit.result.Split(',');

                        SqlCommand MyCom = new SqlCommand();
                        MyCom.CommandText = "INSERT INTO UnitsDataMain (PPID,UnitCode,UnitType,PackageCode,PackageType) VALUES (@num,@ucode,@utype,@pcode,@ptype)";
                        MyCom.Connection = MyConnection;
                        MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                        MyCom.Parameters["@num"].Value = PPID;
                        MyCom.Parameters.Add("@ucode", SqlDbType.NChar, 20);
                        MyCom.Parameters["@ucode"].Value = UnitResult[0];
                        MyCom.Parameters.Add("@utype", SqlDbType.NChar, 8);
                        MyCom.Parameters["@utype"].Value = UnitResult[1];
                        MyCom.Parameters.Add("@pcode", SqlDbType.Int);
                        MyCom.Parameters["@pcode"].Value = int.Parse(UnitResult[2]);
                        MyCom.Parameters.Add("@ptype", SqlDbType.NChar, 10);
                        string type = UnitResult[3];
                        if (type.Contains("Combined")) type = "CC";
                        MyCom.Parameters["@ptype"].Value = type;
                        try
                        {
                            MyCom.ExecuteNonQuery();
                            string Mypackage = UnitResult[3];
                            Mypackage = Mypackage.Trim();
                            MyCom.CommandText = "SELECT @re=COUNT(PPID) FROM PPUnit WHERE PPID=@num AND PackageType=@packagetype";
                            MyCom.Parameters["@num"].Value = PPID;
                            MyCom.Parameters.Add("@packagetype", SqlDbType.NChar, 20);
                            MyCom.Parameters["@packagetype"].Value = Mypackage;
                            MyCom.Parameters.Add("@re", SqlDbType.Int);
                            MyCom.Parameters["@re"].Direction = ParameterDirection.Output;

                            MyCom.ExecuteNonQuery();

                            int result1;
                            result1 = (int)MyCom.Parameters["@re"].Value;
                            if (result1 == 0)
                            {
                                MyCom.CommandText = "INSERT INTO PPUnit (PPID,PackageType) VALUES (@num,@packagetype)";
                                MyCom.Parameters["@num"].Value = PPID;
                                MyCom.Parameters["@packagetype"].Value = Mypackage;

                                MyCom.ExecuteNonQuery();
                            }
                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                            if (str.Contains("PRIMARY KEY"))
                                MessageBox.Show("This Unit Exists Now!");
                        }
                        SetHeader1Plant();
                        buildPPtree(PPID.ToString());
                        //TAB : GENERAL DATA
                        FillPlantGrid(PPID.ToString());
                        //TAB :MARKETRESULTS
                        FillMRVlues(PPID.ToString());
                    }

                    //    if (PlantGV1.Visible)
                    //    {
                    //        foreach (DataGridViewRow dr in PlantGV1.Rows)
                    //        {
                    //            if ((dr.Cells[3].Value == null) || (dr.Cells[3].Value.ToString() == ""))
                    //                if ((dr.Cells[0].Value != null) && (dr.Cells[1].Value != null) && (dr.Cells[2].Value != null))
                    //                {
                    //                    SqlCommand MyCom = new SqlCommand();
                    //                    MyCom.CommandText = "INSERT INTO UnitsDataMain (PPID,UnitCode,UnitType,PackageCode,PackageType) VALUES (@num,@ucode,@utype,@pcode,@ptype)";
                    //                    MyCom.Connection = MyConnection;
                    //                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                    //                    MyCom.Parameters["@num"].Value = PPID;
                    //                    MyCom.Parameters.Add("@ucode", SqlDbType.NChar, 20);
                    //                    string ucode = dr.Cells[0].Value.ToString();
                    //                    MyCom.Parameters["@ucode"].Value = ucode;
                    //                    MyCom.Parameters.Add("@utype", SqlDbType.NChar, 8);
                    //                    ucode = dr.Cells[2].Value.ToString();
                    //                    ucode = ucode.ToLower();
                    //                    if (ucode.Contains("steam")) ucode = "Steam";
                    //                    else ucode = "Gas";
                    //                    MyCom.Parameters["@utype"].Value = ucode;
                    //                    MyCom.Parameters.Add("@pcode", SqlDbType.Int);
                    //                    MyCom.Parameters["@pcode"].Value = int.Parse(dr.Cells[1].Value.ToString());
                    //                    MyCom.Parameters.Add("@ptype", SqlDbType.NChar, 10);
                    //                    string type = GDGasGroup.Text;
                    //                    if (type.Contains("Combined")) type = "CC";
                    //                    MyCom.Parameters["@ptype"].Value = type;
                    //                    //try
                    //                    //{
                    //                    MyCom.ExecuteNonQuery();
                    //                    //}
                    //                    //catch (Exception exp)
                    //                    //{
                    //                    //MessageBox.Show("This Unit Exists Now!");
                    //                    //  string str = exp.Message;
                    //                    //}
                    //                }
                    //        }
                    //    }
                    //    if (PlantGV2.Visible)
                    //    {
                    //        foreach (DataGridViewRow dr in PlantGV2.Rows)
                    //        {
                    //            if ((dr.Cells[3].Value == null) || (dr.Cells[3].Value.ToString() == ""))
                    //                if ((dr.Cells[0].Value != null) && (dr.Cells[1].Value != null) && (dr.Cells[2].Value != null))
                    //                {
                    //                    SqlCommand MyCom = new SqlCommand();
                    //                    MyCom.CommandText = "INSERT INTO UnitsDataMain (PPID,UnitCode,UnitType,PackageCode,PackageType) VALUES (@num,@ucode,@utype,@pcode,@ptype)";
                    //                    MyCom.Connection = MyConnection;
                    //                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                    //                    MyCom.Parameters["@num"].Value = PPID;
                    //                    MyCom.Parameters.Add("@ucode", SqlDbType.NChar, 20);
                    //                    string ucode = dr.Cells[0].Value.ToString();
                    //                    MyCom.Parameters["@ucode"].Value = ucode;
                    //                    MyCom.Parameters.Add("@utype", SqlDbType.NChar, 8);
                    //                    ucode = dr.Cells[2].Value.ToString();
                    //                    ucode = ucode.ToLower();
                    //                    if (ucode.Contains("steam")) ucode = "Steam";
                    //                    else ucode = "Gas";
                    //                    MyCom.Parameters["@utype"].Value = ucode;
                    //                    MyCom.Parameters.Add("@pcode", SqlDbType.Int);
                    //                    MyCom.Parameters["@pcode"].Value = int.Parse(dr.Cells[1].Value.ToString());
                    //                    MyCom.Parameters.Add("@ptype", SqlDbType.NChar, 10);
                    //                    string type = GDSteamGroup.Text;
                    //                    if (type.Contains("Combined")) type = "CC";
                    //                    MyCom.Parameters["@ptype"].Value = type;
                    //                    //try
                    //                    //{
                    //                    MyCom.ExecuteNonQuery();
                    //                    //}
                    //                    //catch (Exception exp)
                    //                    //{
                    //                    //MessageBox.Show("This Unit Exists Now!");
                    //                    //  string str = exp.Message;
                    //                    //}
                    //                }
                    //        }
                    //    }
                    //    buildPPtree(PPID.ToString());
                    //    //TAB : GENERAL DATA
                    //    FillPlantGrid(PPID.ToString());
                    //    //TAB :MARKETRESULTS
                    //    FillMRVlues(PPID.ToString());
                }

                //IF A Transmission Line is selected
                if ((Grid230.Visible) || (Grid400.Visible))
                {
                    Form5 newLine = new Form5();
                    DialogResult re = newLine.ShowDialog();
                    if (re == DialogResult.OK)
                    {
                        string[] UnitResult = newLine.result.Split(',');

                        SqlCommand MyCom = new SqlCommand();
                        MyCom.CommandText = "INSERT INTO Lines (LineNumber,LineCode,FromBus,ToBus,LineLength," +
                        "Owner_GencoFrom,Owner_GencoTo,RateA) VALUES (@num,@code,@from,@to,@lentgh,@gencofrom," +
                        "@gencoto,@capacity) INSERT INTO TransLine (LineNumber,LineCode,Name) VALUES (@num,@code,@name) ";
                        MyCom.Connection = MyConnection;
                        MyCom.Parameters.Add("@num", SqlDbType.Int);
                        MyCom.Parameters["@num"].Value = line;
                        MyCom.Parameters.Add("@code", SqlDbType.NChar, 20);
                        MyCom.Parameters["@code"].Value = UnitResult[0];
                        MyCom.Parameters.Add("@from", SqlDbType.SmallInt);
                        if (UnitResult[1] != "")
                            MyCom.Parameters["@from"].Value = int.Parse(UnitResult[1]);
                        else MyCom.Parameters["@from"].Value = 0;
                        MyCom.Parameters.Add("@to", SqlDbType.SmallInt);
                        if (UnitResult[2] != "")
                            MyCom.Parameters["@to"].Value = int.Parse(UnitResult[2]);
                        else MyCom.Parameters["@to"].Value = 0;
                        MyCom.Parameters.Add("@lentgh", SqlDbType.Real);
                        if (UnitResult[3] != "")
                            MyCom.Parameters["@lentgh"].Value = double.Parse(UnitResult[3]);
                        else MyCom.Parameters["@lentgh"].Value = 0;
                        MyCom.Parameters.Add("@gencofrom", SqlDbType.NChar, 10);
                        MyCom.Parameters["@gencofrom"].Value = UnitResult[5];
                        MyCom.Parameters.Add("@gencoto", SqlDbType.NChar, 10);
                        MyCom.Parameters["@gencoto"].Value = UnitResult[6];
                        MyCom.Parameters.Add("@capacity", SqlDbType.Real);
                        if (UnitResult[4] != "")
                            MyCom.Parameters["@capacity"].Value = double.Parse(UnitResult[4]);
                        else MyCom.Parameters["@capacity"].Value = 0;
                        MyCom.Parameters.Add("@name", SqlDbType.NChar, 20);
                        string name = "";
                        if ((UnitResult[5] != "") && (UnitResult[6] != ""))
                            name = UnitResult[5] + "-" + UnitResult[6];
                        MyCom.Parameters["@name"].Value = name;
                        try
                        {
                            MyCom.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                            if (str.Contains("PRIMARY KEY"))
                                MessageBox.Show("This Line Exists Now!");
                        }
                        buildTRANStree(line.ToString());
                        FillTransmissionGrid(line.ToString());
                    }
                    //foreach (DataGridViewRow dr in Grid400.Rows)
                    //{
                    //    if ((dr.Cells[7].Value == null) || (dr.Cells[7].Value.ToString() == ""))
                    //        if (dr.Cells[0].Value != null)
                    //        {
                    //            SqlCommand MyCom = new SqlCommand();
                    //            MyCom.CommandText = "INSERT INTO Lines (LineNumber,LineCode,FromBus,ToBus,LineLength,"+
                    //            "Owner_GencoFrom,Owner_GencoTo,RateA) VALUES (@num,@code,@from,@to,@lentgh,@gencofrom,"+
                    //            "@gencoto,@capacity) INSERT INTO TransLine (LineNumber,LineCode,Name) VALUES (@num,@code,@name) ";
                    //            MyCom.Connection = MyConnection;
                    //            MyCom.Parameters.Add("@num", SqlDbType.Int);
                    //            MyCom.Parameters["@num"].Value = int.Parse(GDGasGroup.Text);
                    //            MyCom.Parameters.Add("@code", SqlDbType.NChar, 20);
                    //            MyCom.Parameters["@code"].Value = dr.Cells[0].Value.ToString();
                    //            MyCom.Parameters.Add("@from", SqlDbType.SmallInt);
                    //            if ((dr.Cells[1].Value!=null)&&(dr.Cells[1].Value.ToString()!=""))
                    //                MyCom.Parameters["@from"].Value = int.Parse(dr.Cells[1].Value.ToString());
                    //            else MyCom.Parameters["@from"].Value =0;
                    //            MyCom.Parameters.Add("@to", SqlDbType.SmallInt);
                    //            if ((dr.Cells[2].Value!=null)&&(dr.Cells[2].Value.ToString()!=""))
                    //                MyCom.Parameters["@to"].Value = int.Parse(dr.Cells[2].Value.ToString());
                    //            else MyCom.Parameters["@to"].Value =0;
                    //            MyCom.Parameters.Add("@lentgh", SqlDbType.Real);
                    //            if ((dr.Cells[3].Value!=null)&&(dr.Cells[3].Value.ToString()!=""))
                    //                MyCom.Parameters["@lentgh"].Value = double.Parse(dr.Cells[3].Value.ToString());
                    //            else MyCom.Parameters["@lentgh"].Value =0;
                    //            MyCom.Parameters.Add("@gencofrom", SqlDbType.NChar,10);
                    //            if (dr.Cells[4].Value!=null)
                    //                MyCom.Parameters["@gencofrom"].Value = dr.Cells[4].Value.ToString();
                    //            else MyCom.Parameters["@gencofrom"].Value ="";
                    //            MyCom.Parameters.Add("@gencoto", SqlDbType.NChar,10);
                    //            if (dr.Cells[5].Value!=null)
                    //                MyCom.Parameters["@gencoto"].Value = dr.Cells[5].Value.ToString();
                    //            else MyCom.Parameters["@gencoto"].Value ="";
                    //            MyCom.Parameters.Add("@capacity", SqlDbType.Real);
                    //            if ((dr.Cells[6].Value!=null)&&(dr.Cells[6].Value.ToString()!=""))
                    //                MyCom.Parameters["@capacity"].Value = double.Parse(dr.Cells[6].Value.ToString());
                    //            else MyCom.Parameters["@capacity"].Value =0;
                    //            MyCom.Parameters.Add("@name", SqlDbType.NChar,20);
                    //            string name="";
                    //            if ((dr.Cells[4].Value!=null)&&(dr.Cells[5].Value!=null))
                    //                name=dr.Cells[4].Value.ToString()+"-"+dr.Cells[5].Value.ToString();
                    //            MyCom.Parameters["@name"].Value=name;
                    //            //try
                    //            //{
                    //            MyCom.ExecuteNonQuery();
                    //            //}
                    //            //catch (Exception exp)
                    //            //{
                    //            //MessageBox.Show("This Unit Exists Now!");
                    //            //  string str = exp.Message;
                    //            //}
                    //        }
                    //    }
                    //    foreach (DataGridViewRow dr in Grid230.Rows)
                    //    {
                    //    if ((dr.Cells[7].Value == null) || (dr.Cells[7].Value.ToString() == ""))
                    //        if (dr.Cells[0].Value != null)
                    //        {
                    //            SqlCommand MyCom = new SqlCommand();
                    //            MyCom.CommandText = "INSERT INTO Lines (LineNumber,LineCode,FromBus,ToBus,LineLength,"+
                    //            "Owner_GencoFrom,Owner_GencoTo,RateA) VALUES (@num,@code,@from,@to,@lentgh,@gencofrom,"+
                    //            "@gencoto,@capacity) INSERT INTO TransLine (LineNumber,LineCode,Name) VALUES (@num,@code,@name)";
                    //            MyCom.Connection = MyConnection;
                    //            MyCom.Parameters.Add("@num", SqlDbType.Int);
                    //            MyCom.Parameters["@num"].Value = int.Parse(GDSteamGroup.Text);
                    //            MyCom.Parameters.Add("@code", SqlDbType.NChar, 20);
                    //            MyCom.Parameters["@code"].Value = dr.Cells[0].Value.ToString();
                    //            MyCom.Parameters.Add("@from", SqlDbType.SmallInt);
                    //            if ((dr.Cells[1].Value!=null)&&(dr.Cells[1].Value.ToString()!=""))
                    //                MyCom.Parameters["@from"].Value = int.Parse(dr.Cells[1].Value.ToString());
                    //            else MyCom.Parameters["@from"].Value =0;
                    //            MyCom.Parameters.Add("@to", SqlDbType.SmallInt);
                    //            if ((dr.Cells[2].Value!=null)&&(dr.Cells[2].Value.ToString()!=""))
                    //                MyCom.Parameters["@to"].Value = int.Parse(dr.Cells[2].Value.ToString());
                    //            else MyCom.Parameters["@to"].Value =0;
                    //            MyCom.Parameters.Add("@lentgh", SqlDbType.Real);
                    //            if ((dr.Cells[3].Value!=null)&&(dr.Cells[3].Value.ToString()!=""))
                    //                MyCom.Parameters["@lentgh"].Value = double.Parse(dr.Cells[3].Value.ToString());
                    //            else MyCom.Parameters["@lentgh"].Value =0;
                    //            MyCom.Parameters.Add("@gencofrom", SqlDbType.NChar,10);
                    //            if (dr.Cells[4].Value!=null)
                    //                MyCom.Parameters["@gencofrom"].Value = dr.Cells[4].Value.ToString();
                    //            else MyCom.Parameters["@gencofrom"].Value ="";
                    //            MyCom.Parameters.Add("@gencoto", SqlDbType.NChar,10);
                    //            if (dr.Cells[5].Value!=null)
                    //                MyCom.Parameters["@gencoto"].Value = dr.Cells[5].Value.ToString();
                    //            else MyCom.Parameters["@gencoto"].Value ="";
                    //            MyCom.Parameters.Add("@capacity", SqlDbType.Real);
                    //            if ((dr.Cells[6].Value!=null)&&(dr.Cells[6].Value.ToString()!=""))
                    //                MyCom.Parameters["@capacity"].Value = double.Parse(dr.Cells[6].Value.ToString());
                    //            else MyCom.Parameters["@capacity"].Value =0;
                    //            MyCom.Parameters.Add("@name", SqlDbType.NChar, 20);
                    //            string name = "";
                    //            if ((dr.Cells[4].Value != null) && (dr.Cells[5].Value != null))
                    //                name = dr.Cells[4].Value.ToString() + "-" + dr.Cells[5].Value.ToString();
                    //            MyCom.Parameters["@name"].Value = name;
                    //            //try
                    //            //{
                    //            MyCom.ExecuteNonQuery();
                    //            //}
                    //            //catch (Exception exp)
                    //            //{
                    //            //MessageBox.Show("This Unit Exists Now!");
                    //            //  string str = exp.Message;
                    //            //}
                    //        }
                    //}
                }
            }
            //We Are in GDUnitPanel
            else if (GDNewBtn.Text.Contains("Save"))
            {
                //A Unit is selected
                if (Currentgb.Text.Contains("STATE"))
                {
                    if ((errorProvider1.GetError(GDcapacityTB) == "") && (errorProvider1.GetError(GDPmaxTB) == "") &&
                        (errorProvider1.GetError(GDPminTB) == "") && (errorProvider1.GetError(GDTimeUpTB) == "") &&
                        (errorProvider1.GetError(GDTimeDownTB) == "") && (errorProvider1.GetError(GDTimeColdStartTB) == "") &&
                        (errorProvider1.GetError(GDTimeHotStartTB) == "") && (errorProvider1.GetError(GDRampRateTB) == "") &&
                        (errorProvider1.GetError(GDIntConsumeTB) == ""))
                    {
                        SqlCommand MyCom = new SqlCommand();
                        MyCom.CommandText = "UPDATE UnitsDataMain SET Capacity=@cap,PMax=@pmax,PMin=@pmin,TUp=@tup,TDown=@tdown," +
                        "TStartCold=@tcold,TStartHot=@thot,RampUpRate=@ramp,InternalConsume=@cons WHERE PPID=@num AND UnitCode=@unit AND " +
                        "PackageType=@package ";
                        MyCom.Connection = MyConnection;
                        MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                        MyCom.Parameters["@num"].Value = PPID;
                        MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
                        string unit = GDUnitLb.Text;
                        string package = GDPackLb.Text;
                        MyCom.Parameters["@unit"].Value = unit;
                        MyCom.Parameters.Add("@package", SqlDbType.NChar, 10);
                        if (package.Contains("Combined")) package = "CC";
                        MyCom.Parameters["@package"].Value = package;

                        //DETECT unit (for combined Cycles)
                        MyCom.Parameters.Add("@myunit", SqlDbType.NChar, 20);
                        package = GDPackLb.Text;
                        if (package.Contains("Combined"))
                        {
                            string packagecode = "";
                            if (GDSteamGroup.Text.Contains(package))
                                for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                                {
                                    if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                                        packagecode = PlantGV2.Rows[i].Cells[1].Value.ToString();
                                }
                            else
                                for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                                {
                                    if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                                        packagecode = PlantGV1.Rows[i].Cells[1].Value.ToString();
                                }
                            unit = "C" + packagecode;
                        }

                        MyCom.Parameters["@myunit"].Value = unit;
                        MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                        //Detect Farsi Date
                        System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
                        DateTime CurDate = DateTime.Now;
                        string day = pr.GetDayOfMonth(CurDate).ToString();
                        if (int.Parse(day) < 10) day = "0" + day;
                        string mydate = pr.GetYear(CurDate) + "/" + pr.GetMonth(CurDate) + "/" + day;
                        MyCom.Parameters["@date"].Value = mydate;
                        MyCom.Parameters.Add("@cap", SqlDbType.Real);
                        if (GDcapacityTB.Text != "")
                            MyCom.Parameters["@cap"].Value = double.Parse(GDcapacityTB.Text);
                        else MyCom.Parameters["@cap"].Value = 0;
                        MyCom.Parameters.Add("@pmax", SqlDbType.Real);
                        if (GDPmaxTB.Text != "")
                            MyCom.Parameters["@pmax"].Value = double.Parse(GDPmaxTB.Text);
                        else MyCom.Parameters["@pmax"].Value = 0;
                        MyCom.Parameters.Add("@pmin", SqlDbType.SmallInt);
                        if (GDPminTB.Text != "")
                            MyCom.Parameters["@pmin"].Value = int.Parse(GDPminTB.Text);
                        else MyCom.Parameters["@pmin"].Value = 0;
                        MyCom.Parameters.Add("@tup", SqlDbType.SmallInt);
                        if (GDTimeUpTB.Text != "")
                            MyCom.Parameters["@tup"].Value = int.Parse(GDTimeUpTB.Text);
                        else MyCom.Parameters["@tup"].Value = 0;
                        MyCom.Parameters.Add("@tdown", SqlDbType.SmallInt);
                        if (GDTimeDownTB.Text != "")
                            MyCom.Parameters["@tdown"].Value = int.Parse(GDTimeDownTB.Text);
                        else MyCom.Parameters["@tdown"].Value = 0;
                        MyCom.Parameters.Add("@tcold", SqlDbType.Real);
                        if (GDTimeColdStartTB.Text != "")
                            MyCom.Parameters["@tcold"].Value = double.Parse(GDTimeColdStartTB.Text);
                        else MyCom.Parameters["@tcold"].Value = 0;
                        MyCom.Parameters.Add("@thot", SqlDbType.Real);
                        if (GDTimeHotStartTB.Text != "")
                            MyCom.Parameters["@thot"].Value = double.Parse(GDTimeHotStartTB.Text);
                        else MyCom.Parameters["@thot"].Value = 0;
                        MyCom.Parameters.Add("@ramp", SqlDbType.SmallInt);
                        if (GDRampRateTB.Text != "")
                            MyCom.Parameters["@ramp"].Value = int.Parse(GDRampRateTB.Text);
                        else MyCom.Parameters["@ramp"].Value = 0;
                        MyCom.Parameters.Add("@cons", SqlDbType.Real);
                        if (GDIntConsumeTB.Text != "")
                            MyCom.Parameters["@cons"].Value = double.Parse(GDIntConsumeTB.Text);
                        else MyCom.Parameters["@cons"].Value = 0;

                        //try
                        //{
                        MyCom.ExecuteNonQuery();
                        //}
                        //catch (Exception exp)
                        //{
                        //  string str = exp.Message;
                        //}
                        FillPlantGrid(PPID.ToString());
                    }
                }

                //A Transmission Line is selected
                else if (Currentgb.Text.Contains("OWNER"))
                {
                    if ((errorProvider1.GetError(GDcapacityTB) == "") && (errorProvider1.GetError(GDPmaxTB) == "") &&
                    (errorProvider1.GetError(GDPminTB) == "") && (errorProvider1.GetError(GDRampRateTB) == "") &&
                    (errorProvider1.GetError(GDTimeDownTB) == "") && (errorProvider1.GetError(GDTimeColdStartTB) == "") &&
                    (errorProvider1.GetError(GDTimeHotStartTB) == ""))
                    {
                        SqlCommand MyCom = new SqlCommand();
                        MyCom.CommandText = "UPDATE Lines SET RateA=@cap,FromBus=@from,ToBus=@to,CircuitID=@cid,LineR=@lr," +
                        "LineX=@lx,LineSuseptance=@sus,LineLength=@length WHERE LineNumber=@num AND LineCode=@code";
                        MyCom.Connection = MyConnection;
                        MyCom.Parameters.Add("@num", SqlDbType.Int);
                        MyCom.Parameters["@num"].Value = line;
                        MyCom.Parameters.Add("@code", SqlDbType.NChar, 20);
                        MyCom.Parameters["@code"].Value = GDPlantLb.Text;
                        MyCom.Parameters.Add("@cap", SqlDbType.Real);
                        if (GDcapacityTB.Text != "")
                            MyCom.Parameters["@cap"].Value = double.Parse(GDcapacityTB.Text);
                        else MyCom.Parameters["@cap"].Value = 0;
                        MyCom.Parameters.Add("@from", SqlDbType.SmallInt);
                        if (GDPmaxTB.Text != "")
                            MyCom.Parameters["@from"].Value = int.Parse(GDPmaxTB.Text);
                        else MyCom.Parameters["@from"].Value = 0;
                        MyCom.Parameters.Add("@to", SqlDbType.SmallInt);
                        if (GDPminTB.Text != "")
                            MyCom.Parameters["@to"].Value = int.Parse(GDPminTB.Text);
                        else MyCom.Parameters["@to"].Value = 0;
                        MyCom.Parameters.Add("@cid", SqlDbType.NChar, 5);
                        MyCom.Parameters["@cid"].Value = GDTimeUpTB.Text;
                        MyCom.Parameters.Add("@lr", SqlDbType.Real);
                        if (GDTimeDownTB.Text != "")
                            MyCom.Parameters["@lr"].Value = double.Parse(GDTimeDownTB.Text);
                        else MyCom.Parameters["@lr"].Value = 0;
                        MyCom.Parameters.Add("@lx", SqlDbType.Real);
                        if (GDTimeColdStartTB.Text != "")
                            MyCom.Parameters["@lx"].Value = double.Parse(GDTimeColdStartTB.Text);
                        else MyCom.Parameters["@lx"].Value = 0;
                        MyCom.Parameters.Add("@sus", SqlDbType.Real);
                        if (GDTimeHotStartTB.Text != "")
                            MyCom.Parameters["@sus"].Value = double.Parse(GDTimeHotStartTB.Text);
                        else MyCom.Parameters["@sus"].Value = 0;
                        MyCom.Parameters.Add("@length", SqlDbType.Real);
                        if (GDRampRateTB.Text != "")
                            MyCom.Parameters["@length"].Value = double.Parse(GDRampRateTB.Text);
                        else MyCom.Parameters["@length"].Value = 0;
                        //try
                        //{
                        MyCom.ExecuteNonQuery();
                        //}
                        //catch (Exception exp)
                        //{
                        //  string str = exp.Message;
                        //}
                    }
                }
                GDcapacityTB.ReadOnly = true;
                GDPmaxTB.ReadOnly = true;
                GDPminTB.ReadOnly = true;
                GDTimeUpTB.ReadOnly = true;
                GDTimeDownTB.ReadOnly = true;
                GDTimeColdStartTB.ReadOnly = true;
                GDTimeHotStartTB.ReadOnly = true;
                GDRampRateTB.ReadOnly = true;
                GDIntConsumeTB.ReadOnly = true;
            }
        }
        //-----------------------------------GDcapacityTB_Validated------------------------------------
        private void GDcapacityTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(GDcapacityTB.Text, 1))
                errorProvider1.SetError(GDcapacityTB, "");
            else errorProvider1.SetError(GDcapacityTB, "just real number!");

        }
        //------------------------------------GDPmaxTB_Validated------------------------------------
        private void GDPmaxTB_Validated(object sender, EventArgs e)
        {
            if (Currentgb.Text.Contains("STATE"))
            {
                if (CheckValidated(GDPmaxTB.Text, 1))
                    errorProvider1.SetError(GDPmaxTB, "");
                else errorProvider1.SetError(GDPmaxTB, "just real number!");
            }
            else
            {
                if (CheckValidated(GDPmaxTB.Text, 0))
                    errorProvider1.SetError(GDPmaxTB, "");
                else errorProvider1.SetError(GDPmaxTB, "just Integer!");
            }
        }
        //-----------------------------------GDPminTB_Validated--------------------------------------
        private void GDPminTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(GDPminTB.Text, 0))
                errorProvider1.SetError(GDPminTB, "");
            else errorProvider1.SetError(GDPminTB, "just integer!");
        }
        //-----------------------------------GDTimeUpTB_Validated--------------------------------
        private void GDTimeUpTB_Validated(object sender, EventArgs e)
        {
            if (Currentgb.Text.Contains("STATE"))
            {
                if (CheckValidated(GDTimeUpTB.Text, 0))
                    errorProvider1.SetError(GDTimeUpTB, "");
                else errorProvider1.SetError(GDTimeUpTB, "just integer!");
            }
        }
        //--------------------------------GDTimeDownTB_Validated---------------------------------------
        private void GDTimeDownTB_Validated(object sender, EventArgs e)
        {
            if (Currentgb.Text.Contains("STATE"))
            {
                if (CheckValidated(GDTimeDownTB.Text, 0))
                    errorProvider1.SetError(GDTimeDownTB, "");
                else errorProvider1.SetError(GDTimeDownTB, "just integer!");
            }
            else
            {
                if (CheckValidated(GDTimeDownTB.Text, 1))
                    errorProvider1.SetError(GDTimeDownTB, "");
                else errorProvider1.SetError(GDTimeDownTB, "just real number!");
            }
        }
        //----------------------------------GDTimeColdStartTB_Validated-------------------------------------
        private void GDTimeColdStartTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(GDTimeColdStartTB.Text, 1))
                errorProvider1.SetError(GDTimeColdStartTB, "");
            else errorProvider1.SetError(GDTimeColdStartTB, "just real number!");
        }
        //--------------------------------GDTimeHotStartTB_Validated---------------------------------------
        private void GDTimeHotStartTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(GDTimeHotStartTB.Text, 1))
                errorProvider1.SetError(GDTimeHotStartTB, "");
            else errorProvider1.SetError(GDTimeHotStartTB, "just real number!");
        }
        //-------------------------------GDRampRateTB_Validated-------------------------------------
        private void GDRampRateTB_Validated(object sender, EventArgs e)
        {
            if (Currentgb.Text.Contains("STATE"))
            {
                if (CheckValidated(GDRampRateTB.Text, 0))
                    errorProvider1.SetError(GDRampRateTB, "");
                else errorProvider1.SetError(GDRampRateTB, "just integer!");
            }
            else
            {
                if (CheckValidated(GDRampRateTB.Text, 1))
                    errorProvider1.SetError(GDRampRateTB, "");
                else errorProvider1.SetError(GDRampRateTB, "just real number!");
            }
        }
        //------------------------------GDIntConsumeTB_Validated-----------------------------------
        private void GDIntConsumeTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(GDIntConsumeTB.Text, 1))
                errorProvider1.SetError(GDIntConsumeTB, "");
            else errorProvider1.SetError(GDIntConsumeTB, "just real number!");
        }
        //----------------------------------FRUnitFixedTb_Validated-------------------------------------
        private void FRUnitFixedTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitFixedTb.Text, 1))
                errorProvider1.SetError(FRUnitFixedTb, "");
            else errorProvider1.SetError(FRUnitFixedTb, "just real number!");
        }
        //-----------------------------------FRUnitVariableTb_Validated----------------------------------
        private void FRUnitVariableTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitVariableTb.Text, 1))
                errorProvider1.SetError(FRUnitVariableTb, "");
            else errorProvider1.SetError(FRUnitVariableTb, "just real number!");
        }
        //----------------------------------FRUnitAmargTb1_Validated------------------------------------
        private void FRUnitAmargTb1_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitAmargTb1.Text, 1))
                errorProvider1.SetError(FRUnitAmargTb1, "");
            else errorProvider1.SetError(FRUnitAmargTb1, "just real number!");
        }
        //----------------------------------FRUnitBmargTb1_Validated--------------------------------------
        private void FRUnitBmargTb1_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitBmargTb1.Text, 1))
                errorProvider1.SetError(FRUnitBmargTb1, "");
            else errorProvider1.SetError(FRUnitBmargTb1, "just real number!");
        }
        //-------------------------------------FRUnitCmargTb1_Validated-------------------------------------
        private void FRUnitCmargTb1_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitCmargTb1.Text, 1))
                errorProvider1.SetError(FRUnitCmargTb1, "");
            else errorProvider1.SetError(FRUnitCmargTb1, "just real number!");
        }
        //---------------------------------------FRUnitAmargTb2_Validated------------------------------------
        private void FRUnitAmargTb2_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitAmargTb2.Text, 1))
                errorProvider1.SetError(FRUnitAmargTb2, "");
            else errorProvider1.SetError(FRUnitAmargTb2, "just real number!");
        }
        //--------------------------------------FRUnitBmargTb2_Validated--------------------------------------
        private void FRUnitBmargTb2_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitBmargTb2.Text, 1))
                errorProvider1.SetError(FRUnitBmargTb2, "");
            else errorProvider1.SetError(FRUnitBmargTb2, "just real number!");
        }
        //--------------------------------------FRUnitCmargTb2_Validated-----------------------------------------
        private void FRUnitCmargTb2_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitCmargTb2.Text, 1))
                errorProvider1.SetError(FRUnitCmargTb2, "");
            else errorProvider1.SetError(FRUnitCmargTb2, "just real number!");
        }
        //--------------------------------------FRUnitColdTb_Validated----------------------------------------------
        private void FRUnitColdTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitColdTb.Text, 1))
                errorProvider1.SetError(FRUnitColdTb, "");
            else errorProvider1.SetError(FRUnitColdTb, "just real number!");
        }
        //-------------------------------------FRUnitHotTb_Validated--------------------------------------------
        private void FRUnitHotTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitHotTb.Text, 1))
                errorProvider1.SetError(FRUnitHotTb, "");
            else errorProvider1.SetError(FRUnitHotTb, "just real number!");
        }
        //---------------------------------------FROKBtn_Click------------------------------------------
        private void FROKBtn_Click(object sender, EventArgs e)
        {
            if (FRUnitPanel.Visible)
            {
                FRUpdateBtn.Enabled = true;
                FRUnitCapitalTb.ReadOnly = false;
                FRUnitFixedTb.ReadOnly = false;
                FRUnitVariableTb.ReadOnly = false;
                FRUnitAmargTb1.ReadOnly = false;
                FRUnitBmargTb1.ReadOnly = false;
                FRUnitCmargTb1.ReadOnly = false;
                FRUnitAmargTb2.ReadOnly = false;
                FRUnitBmargTb2.ReadOnly = false;
                FRUnitCmargTb2.ReadOnly = false;
                FRUnitAmainTb.ReadOnly = false;
                FRUnitBmainTb.ReadOnly = false;
                FRUnitColdTb.ReadOnly = false;
                FRUnitHotTb.ReadOnly = false;
            }
        }
        //-----------------------------------------Grid400_DataError---------------------------------------
        private void Grid400_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            if ((!Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 1))
            {
                try
                {
                    int i = int.Parse(Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
            if ((!Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 2))
            {
                try
                {
                    int i = int.Parse(Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
            if ((!Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 3))
            {
                try
                {
                    double i = double.Parse(Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
            if ((!Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 6))
            {
                try
                {
                    double i = double.Parse(Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
        }
        //-------------------------------------------Grid230_DataError-----------------------------------------
        private void Grid230_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            if ((!Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 1))
            {
                try
                {
                    int i = int.Parse(Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
            if ((!Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 2))
            {
                try
                {
                    int i = int.Parse(Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
            if ((!Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 3))
            {
                try
                {
                    double i = double.Parse(Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Real Number!");
                }
            }
            if ((!Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 6))
            {
                try
                {
                    double i = double.Parse(Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Real Number!");
                }
            }
        }
        //-------------------------------------MRPlotBtn_Click------------------------------
        private void MRPlotBtn_Click(object sender, EventArgs e)
        {
            //if (MRHeaderPanel.Visible)
            //    DrawUnitnemoodar
            //else DrawPlanNemoodar
        }
        //----------------------------------BDCal_ValueChanged--------------------------------------------
        private void BDCal_ValueChanged(object sender, EventArgs e)
        {
            FillBDUnitGrid();

        }
        //------------------------------MRCal_ValueChanged-------------------------------------
        private void MRCal_ValueChanged(object sender, EventArgs e)
        {
            if (MRHeaderPanel.Visible)
                FillMRUnitGrid();
            else if (L9.Text.Contains("Transmission"))
                FillMRTransmission(line.ToString());
            else if (L9.Text.Contains("Line"))
                FillMRLine(line.ToString(), MRPlantLb.Text.Trim());
            else FillMRPlantGrid();
        }
        //---------------------------------FRPlantCal_ValueChanged--------------------------------------------
        private void FRPlantCal_ValueChanged(object sender, EventArgs e)
        {
            FillFRValues(PPID.ToString());
        }
        //-----------------------------------m002ToolStripMenuItem_Click-------------------------------------
        private void m002ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool IsValid = true;
            DialogResult re = openFileDialog1.ShowDialog();
            if (re != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName;
                //read from FRM002.xls into datagridview
                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                OleDbConnection objConn = new OleDbConnection(sConnectionString);
                objConn.Open();
                string price = "Sheet1";
                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                objAdapter1.SelectCommand = objCmdSelect;
                DataSet objDataset1 = new DataSet();
                try
                {
                    objAdapter1.Fill(objDataset1);
                    TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                }
                catch (Exception ex)
                {
                    IsValid = false;
                    throw ex;
                }
                objConn.Close();
                //IS IT A Valid File?
                if ((IsValid) && (TempGV.Columns[1].HeaderText.Contains("M002")))
                {
                    //Insert into DB (MainFRM002)
                    //string path = @"c:\data\" + Doc002 + ".xls";
                    Excel.Application exobj = new Excel.Application();
                    exobj.Visible = true;
                    exobj.UserControl = true;
                    Excel.Workbook book = null;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Connection = MyConnection;
                    int type = 0;
                    int PID = 0;
                    string date2 = "";
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                        {
                            if ((((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString().Contains("سيكل")) || (((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString().Contains("ccp")))
                                type = 1;
                            string date1 = ((Excel.Range)workSheet.Cells[4, 2]).Value2.ToString();
                            date2 = date1.Remove(4);
                            date2 += "/";
                            date2 += date1[4];
                            date2 += date1[5];
                            date2 += "/";
                            date2 += date1[6];
                            date2 += date1[7];
                        }

                    MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                    MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
                    MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
                    MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

                    MyCom.CommandText = "INSERT INTO [MainFRM002] (PPID,TargetMarketDate,PPName,PPType,"
                    + "DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                        {
                            PID = findPPID(((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString());
                            //if ((PID==131)&&(type==1)) PID=132;
                            //if ((PID==144)&&(type==1)) PID=145;
                            MyCom.Parameters["@id"].Value = PID;
                            MyCom.Parameters["@tdate"].Value = date2;
                            MyCom.Parameters["@name"].Value = ((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString();
                            MyCom.Parameters["@type"].Value = type;
                            if (((Excel.Range)workSheet.Cells[2, 2]).Value2 != null)
                                MyCom.Parameters["@idate"].Value = ((Excel.Range)workSheet.Cells[2, 2]).Value2.ToString();
                            else MyCom.Parameters["@idate"].Value = null;
                            if (((Excel.Range)workSheet.Cells[3, 2]).Value2 != null)
                                MyCom.Parameters["@time"].Value = ((Excel.Range)workSheet.Cells[3, 2]).Value2.ToString();
                            else MyCom.Parameters["@time"].Value = null;
                            if (((Excel.Range)workSheet.Cells[7, 2]).Value2 != null)
                                MyCom.Parameters["@revision"].Value = ((Excel.Range)workSheet.Cells[7, 2]).Value2.ToString();
                            else MyCom.Parameters["@revision"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[8, 2]).Value2 != null)
                                MyCom.Parameters["@filled"].Value = ((Excel.Range)workSheet.Cells[8, 2]).Value2.ToString();
                            else MyCom.Parameters["@filled"].Value = null;
                            if (((Excel.Range)workSheet.Cells[9, 2]).Value2 != null)
                                MyCom.Parameters["@approved"].Value = ((Excel.Range)workSheet.Cells[9, 2]).Value2.ToString();
                            else MyCom.Parameters["@approved"].Value = null;
                        }
                    try
                    {
                        MyCom.ExecuteNonQuery();
                    }
                    catch (Exception exp)
                    {
                        string str = exp.Message;
                    }

                    //Insert into DB (BlockFRM002)
                    int x = 10;
                    MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@peak", SqlDbType.Real);
                    MyCom.Parameters.Add("@max", SqlDbType.Real);
                    while (x < (TempGV.Rows.Count - 1))
                    {
                        if (TempGV.Rows[x].Cells[0].Value.ToString() != "")
                        {
                            MyCom.CommandText = "INSERT INTO [BlockFRM002] (PPID,TargetMarketDate,PPType,Block,"
                            + "Peak,MaxDailyGeneration) VALUES (@id,@tdate,@type,@block,@peak,@max)";

                            //read directly and cell by cell
                            foreach (Excel.Worksheet workSheet in book.Worksheets)
                                if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                                {
                                    MyCom.Parameters["@id"].Value = PID;
                                    MyCom.Parameters["@tdate"].Value = date2;
                                    MyCom.Parameters["@type"].Value = type;
                                    MyCom.Parameters["@block"].Value = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString();
                                    if (((Excel.Range)workSheet.Cells[x + 2, 2]).Value2 != null)
                                        MyCom.Parameters["@peak"].Value = ((Excel.Range)workSheet.Cells[x + 2, 2]).Value2.ToString();
                                    else MyCom.Parameters["@peak"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2, 3]).Value2 != null)
                                        MyCom.Parameters["@max"].Value = ((Excel.Range)workSheet.Cells[x + 2, 3]).Value2.ToString();
                                    else MyCom.Parameters["@max"].Value = 0;
                                }
                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                            }
                        }
                        x++;
                    }
                    //Insert into DB (DetailFRM002)
                    x = 10;
                    MyCom.Parameters.Add("@deccap", SqlDbType.Real);
                    MyCom.Parameters.Add("@dispachcap", SqlDbType.Real);
                    MyCom.Parameters.Add("@price1", SqlDbType.Real);
                    MyCom.Parameters.Add("@power1", SqlDbType.Real);
                    MyCom.Parameters.Add("@price2", SqlDbType.Real);
                    MyCom.Parameters.Add("@power2", SqlDbType.Real);
                    MyCom.Parameters.Add("@price3", SqlDbType.Real);
                    MyCom.Parameters.Add("@power3", SqlDbType.Real);
                    MyCom.Parameters.Add("@price4", SqlDbType.Real);
                    MyCom.Parameters.Add("@power4", SqlDbType.Real);
                    MyCom.Parameters.Add("@price5", SqlDbType.Real);
                    MyCom.Parameters.Add("@power5", SqlDbType.Real);
                    MyCom.Parameters.Add("@price6", SqlDbType.Real);
                    MyCom.Parameters.Add("@power6", SqlDbType.Real);
                    MyCom.Parameters.Add("@price7", SqlDbType.Real);
                    MyCom.Parameters.Add("@power7", SqlDbType.Real);
                    MyCom.Parameters.Add("@price8", SqlDbType.Real);
                    MyCom.Parameters.Add("@power8", SqlDbType.Real);
                    MyCom.Parameters.Add("@price9", SqlDbType.Real);
                    MyCom.Parameters.Add("@power9", SqlDbType.Real);
                    MyCom.Parameters.Add("@price10", SqlDbType.Real);
                    MyCom.Parameters.Add("@power10", SqlDbType.Real);
                    MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                    while (x < (TempGV.Rows.Count - 2))
                    {
                        if (TempGV.Rows[x].Cells[0].Value.ToString() != "")
                        {
                            for (int j = 0; j < 24; j++)
                            {
                                MyCom.CommandText = "INSERT INTO [DetailFRM002] (TargetMarketDate,PPID,Block,PPType,Hour" +
                                ",DeclaredCapacity,DispachableCapacity,Power1,Price1,Power2,Price2,Power3,Price3,Power4," +
                                "Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10," +
                                "Price10) VALUES (@tdate,@id,@block,@type,@hour,@deccap,@dispachcap,@power1," +
                                "@price1,@power2,@price2,@power3,@price3,@power4,@price4,@power5,@price5,@power6,@price6," +
                                "@power7,@price7,@power8,@price8,@power9,@price9,@power10,@price10)";

                                //read directly and cell by cell
                                foreach (Excel.Worksheet workSheet in book.Worksheets)
                                    if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                                    {

                                        MyCom.Parameters["@id"].Value = PID;
                                        MyCom.Parameters["@tdate"].Value = date2;
                                        MyCom.Parameters["@block"].Value = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString();
                                        MyCom.Parameters["@type"].Value = type;
                                        MyCom.Parameters["@hour"].Value = j + 1;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 5]).Value2 != null)
                                            MyCom.Parameters["@deccap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 5]).Value2.ToString();
                                        else MyCom.Parameters["@deccap"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2 != null)
                                            MyCom.Parameters["@dispachcap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2.ToString();
                                        else MyCom.Parameters["@dispachcap"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2 != null)
                                            MyCom.Parameters["@power1"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2.ToString();
                                        else MyCom.Parameters["@power1"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 8]).Value2 != null)
                                            MyCom.Parameters["@price1"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 8]).Value2.ToString();
                                        else MyCom.Parameters["@price1"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 9]).Value2 != null)
                                            MyCom.Parameters["@power2"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 9]).Value2.ToString();
                                        else MyCom.Parameters["@power2"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 10]).Value2 != null)
                                            MyCom.Parameters["@price2"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 10]).Value2.ToString();
                                        else MyCom.Parameters["@price2"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 11]).Value2 != null)
                                            MyCom.Parameters["@power3"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 11]).Value2.ToString();
                                        else MyCom.Parameters["@power3"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 12]).Value2 != null)
                                            MyCom.Parameters["@price3"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 12]).Value2.ToString();
                                        else MyCom.Parameters["@price3"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 13]).Value2 != null)
                                            MyCom.Parameters["@power4"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 13]).Value2.ToString();
                                        else MyCom.Parameters["@power4"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 14]).Value2 != null)
                                            MyCom.Parameters["@price4"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 14]).Value2.ToString();
                                        else MyCom.Parameters["@price4"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 15]).Value2 != null)
                                            MyCom.Parameters["@power5"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 15]).Value2.ToString();
                                        else MyCom.Parameters["@power5"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 16]).Value2 != null)
                                            MyCom.Parameters["@price5"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 16]).Value2.ToString();
                                        else MyCom.Parameters["@price5"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 17]).Value2 != null)
                                            MyCom.Parameters["@power6"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 17]).Value2.ToString();
                                        else MyCom.Parameters["@power6"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 18]).Value2 != null)
                                            MyCom.Parameters["@price6"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 18]).Value2.ToString();
                                        else MyCom.Parameters["@price6"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 19]).Value2 != null)
                                            MyCom.Parameters["@power7"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 19]).Value2.ToString();
                                        else MyCom.Parameters["@power7"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 20]).Value2 != null)
                                            MyCom.Parameters["@price7"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 20]).Value2.ToString();
                                        else MyCom.Parameters["@price7"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 21]).Value2 != null)
                                            MyCom.Parameters["@power8"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 21]).Value2.ToString();
                                        else MyCom.Parameters["@power8"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 22]).Value2 != null)
                                            MyCom.Parameters["@price8"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 22]).Value2.ToString();
                                        else MyCom.Parameters["@price8"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 23]).Value2 != null)
                                            MyCom.Parameters["@power9"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 23]).Value2.ToString();
                                        else MyCom.Parameters["@power9"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 24]).Value2 != null)
                                            MyCom.Parameters["@price9"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 24]).Value2.ToString();
                                        else MyCom.Parameters["@price9"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 25]).Value2 != null)
                                            MyCom.Parameters["@power10"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 25]).Value2.ToString();
                                        else MyCom.Parameters["@power10"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 26]).Value2 != null)
                                            MyCom.Parameters["@price10"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 26]).Value2.ToString();
                                        else MyCom.Parameters["@price10"].Value = 0;
                                    }
                                try
                                {
                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception exp)
                                {
                                    string str = exp.Message;
                                }
                            }
                        }
                        x++;
                    }

  
                    book.Close(false, book, Type.Missing);
                    exobj.Workbooks.Close();
                    exobj.Quit();
                    System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                }
                else MessageBox.Show("Selected File Is not Valid!");
            }
        }
        //----------------------------------m005ToolStripMenuItem_Click---------------------------------------
        private void m005ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult re = openFileDialog1.ShowDialog();
            if (re != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName;
                //Is It a Valid File?
                if (path.Contains("005"))
                {
                    //read from FRM005.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "FRM005";
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                    objConn.Close();

                    //Insert into DB (MainFRM005)
                    Excel.Application exobj = new Excel.Application();
                    exobj.Visible = true;
                    exobj.UserControl = true;
                    Excel.Workbook book = null;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Connection = MyConnection;
                    int type = 0;
                    int PID = 0;
                    if ((TempGV.Rows[3].Cells[1].Value.ToString().Contains("سيكل")) || (TempGV.Rows[3].Cells[1].Value.ToString().Contains("ccp")))
                        type = 1;
                    MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                    MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
                    MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
                    MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

                    MyCom.CommandText = "INSERT INTO [MainFRM005] (PPID,TargetMarketDate,PPName,PPType,"
                    + "DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
                    PID = findPPID(TempGV.Rows[3].Cells[1].Value.ToString());
                    //if ((PID==131)&&(type==1)) PID=132;
                    //if ((PID==144)&&(type==1)) PID=145;
                    MyCom.Parameters["@id"].Value = PID;
                    MyCom.Parameters["@tdate"].Value = TempGV.Rows[2].Cells[1].Value.ToString();
                    MyCom.Parameters["@name"].Value = TempGV.Rows[3].Cells[1].Value.ToString();
                    MyCom.Parameters["@type"].Value = type;
                    MyCom.Parameters["@idate"].Value = TempGV.Rows[0].Cells[1].Value.ToString();
                    MyCom.Parameters["@time"].Value = TempGV.Rows[1].Cells[1].Value.ToString();
                    MyCom.Parameters["@revision"].Value = TempGV.Rows[4].Cells[1].Value.ToString();
                    MyCom.Parameters["@filled"].Value = TempGV.Rows[5].Cells[1].Value.ToString();
                    MyCom.Parameters["@approved"].Value = TempGV.Rows[6].Cells[1].Value.ToString();

                    try
                    {
                        MyCom.ExecuteNonQuery();
                    }
                    catch (Exception exp)
                    {
                        string str = exp.Message;
                    }

                    //Insert into DB (BlockFRM005)
                    int x = 10;
                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@prequired", SqlDbType.Real);
                    MyCom.Parameters.Add("@pdispach", SqlDbType.Real);
                    MyCom.Parameters.Add("@drequierd", SqlDbType.Real);
                    MyCom.Parameters.Add("@ddispach", SqlDbType.Real);
                    while (x < (TempGV.Rows.Count - 1))
                    {
                        if (TempGV.Rows[x].Cells[0].Value.ToString() != "")
                        {
                            MyCom.CommandText = "INSERT INTO [BlockFRM005] (PPID,TargetMarketDate,PPType,Block,"
                            + "PeakRequired,PeakDispatchable,DailyTotalRequired,DailyTotalDispachable)"
                            + "VALUES (@id,@tdate,@type,@num,@prequired,@pdispach,@drequierd,@ddispach)";

                            MyCom.Parameters["@id"].Value = PID;
                            MyCom.Parameters["@tdate"].Value = TempGV.Rows[2].Cells[1].Value.ToString();
                            MyCom.Parameters["@num"].Value = TempGV.Rows[x].Cells[0].Value.ToString();
                            MyCom.Parameters["@type"].Value = type;
                            //read directly and cell by cell
                            foreach (Excel.Worksheet workSheet in book.Worksheets)
                                if (workSheet.Name == "FRM005")
                                {
                                    if (((Excel.Range)workSheet.Cells[x + 2, 27]).Value2 != null)
                                        MyCom.Parameters["@prequired"].Value = ((Excel.Range)workSheet.Cells[x + 2, 27]).Value2.ToString();
                                    else MyCom.Parameters["@prequired"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 3, 27]).Value2 != null)
                                        MyCom.Parameters["@pdispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, 27]).Value2.ToString();
                                    else MyCom.Parameters["@pdispach"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2, 28]).Value2 != null)
                                        MyCom.Parameters["@drequierd"].Value = ((Excel.Range)workSheet.Cells[x + 2, 28]).Value2.ToString();
                                    else MyCom.Parameters["@drequierd"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 3, 28]).Value2 != null)
                                        MyCom.Parameters["@ddispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, 28]).Value2.ToString();
                                    else MyCom.Parameters["@ddispach"].Value = 0;
                                }
                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                            }
                        }
                        x++;
                    }
                    //Insert into DB (DetailFRM005)
                    x = 10;
                    MyCom.Parameters.Add("@required", SqlDbType.Real);
                    MyCom.Parameters.Add("@dispach", SqlDbType.Real);
                    MyCom.Parameters.Add("@contribution", SqlDbType.Char, 2);
                    MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                    while (x < (TempGV.Rows.Count - 2))
                    {
                        if (TempGV.Rows[x].Cells[0].Value.ToString() != "")
                        {
                            for (int j = 0; j < 24; j++)
                            {
                                MyCom.CommandText = "INSERT INTO [DetailFRM005] (TargetMarketDate,PPID,Block,PPType,Hour,Required,Dispatchable,Contribution) VALUES (@tdate,@id,@num,@type,@hour,@required,@dispach,@contribution)";

                                MyCom.Parameters["@id"].Value = PID;
                                MyCom.Parameters["@tdate"].Value = TempGV.Rows[2].Cells[1].Value.ToString();
                                MyCom.Parameters["@num"].Value = TempGV.Rows[x].Cells[0].Value.ToString();
                                MyCom.Parameters["@type"].Value = type;

                                //read directly and cell by cell
                                foreach (Excel.Worksheet workSheet in book.Worksheets)
                                    if (workSheet.Name == "FRM005")
                                    {
                                        MyCom.Parameters["@hour"].Value = j + 1;
                                        if (((Excel.Range)workSheet.Cells[x + 2, j + 3]).Value2 != null)
                                            MyCom.Parameters["@required"].Value = ((Excel.Range)workSheet.Cells[x + 2, j + 3]).Value2.ToString();
                                        else MyCom.Parameters["@required"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 3, j + 3]).Value2 != null)
                                            MyCom.Parameters["@dispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, j + 3]).Value2.ToString();
                                        else MyCom.Parameters["@dispach"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2 != null)
                                            MyCom.Parameters["@contribution"].Value = ((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2.ToString();
                                        else MyCom.Parameters["@contribution"].Value = null;
                                    }
                                try
                                {
                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception exp)
                                {
                                    string str = exp.Message;
                                }
                            }
                        }
                        x++;
                    }

                    book.Close(false, book, Type.Missing);
                    exobj.Workbooks.Close();
                    exobj.Quit();
                    System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                }
                else MessageBox.Show("Selected File Isnot Valid!");
            }
        }
        //---------------------------------------averagePriceToolStripMenuItem_Click-------------------------------------
        private void averagePriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool check = true;
            DialogResult re = openFileDialog1.ShowDialog();
            if (re != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName;
                //Is It a Valid File?
                if (path.Contains("nprice"))
                {
                    //Save AS AveragePrice.xls file
                    //string path = @"c:\data\AveragePrice.xls";
                    Excel.Application exobj1 = new Excel.Application();
                    exobj1.Visible = true;
                    exobj1.UserControl = true;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    Excel.Workbook book1 = null;
                    book1 = exobj1.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book1.Save();
                    book1.Close(true, book1, Type.Missing);
                    exobj1.Workbooks.Close();
                    exobj1.Quit();

                    //read from AveragePrice.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "قيمت ";
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                    objConn.Close();

                    //Insert into DB (AveragePrice)
 
                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@date1", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                    MyCom.Parameters.Add("@Pmin", SqlDbType.Int);
                    MyCom.Parameters.Add("@Pmax", SqlDbType.Int);
                    MyCom.Parameters.Add("@Amin", SqlDbType.Int);
                    MyCom.Parameters.Add("@Amax", SqlDbType.Int);
                    MyCom.Parameters.Add("@Aav", SqlDbType.Int);
                    for (int i = 0; i < 24; i++)
                    {
                        //has Selected file saved before?
                        if (check)
                        {
                            MyCom.CommandText = "INSERT INTO [AveragePrice] (Date,Hour,ProposedMin,ProposedMax,AcceptedMin,AcceptedMax,AcceptedAverage) VALUES (@date1,@hour,@Pmin,@Pmax,@Amin,@Amax,@Aav)";
                            MyCom.Connection = MyConnection;
                            MyCom.Parameters["@date1"].Value = TempGV.Columns[0].HeaderText.ToString();
                            MyCom.Parameters["@hour"].Value = TempGV.Rows[i + 3].Cells[0].Value.ToString();
                            MyCom.Parameters["@Pmin"].Value = TempGV.Rows[i + 3].Cells[1].Value.ToString();
                            MyCom.Parameters["@Pmax"].Value = TempGV.Rows[i + 3].Cells[2].Value.ToString();
                            MyCom.Parameters["@Amin"].Value = TempGV.Rows[i + 3].Cells[3].Value.ToString();
                            MyCom.Parameters["@Amax"].Value = TempGV.Rows[i + 3].Cells[4].Value.ToString();
                            MyCom.Parameters["@Aav"].Value = TempGV.Rows[i + 3].Cells[5].Value.ToString();
                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                                if (str.Contains("PRIMARY KEY"))
                                {
                                    MessageBox.Show("Selected File Had been saved Before!");
                                    check = false;
                                }
                            }
                        }
                    }
                    if (check) MessageBox.Show("Selected File Has been saved!");
                    System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                }
                else MessageBox.Show("Selected File Isnot Valid!");
            }
        }
        //-------------------------------loadForecastingToolStripMenuItem_Click------------------------------------
        private void loadForecastingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult re = openFileDialog1.ShowDialog();
            if (re != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName;
                //Is It a Valid File?
                if (path.Contains("load"))
                {
                    //Save AS LoadForecasting.xls file
                    //string path1 = @"c:\data\LoadForecasting.xls";
                    Excel.Application exobj = new Excel.Application();
                    exobj.Visible = true;
                    exobj.UserControl = true;
                    Excel.Workbook book = null;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book.Save();
                    book.Close(true, book, Type.Missing);

                    //read from LoadForecasting.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "Lfoc";
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                    objConn.Close();

                    //read from LoadForecasting.xls into strings
                    string edate = "";
                    string[] date = new string[4];
                    book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if (workSheet.Name == "Lfoc")
                        {
                            edate = ((Excel.Range)workSheet.Cells[3, 4]).Value2.ToString();
                            date[0] = ((Excel.Range)workSheet.Cells[4, 3]).Value2.ToString();
                            date[1] = ((Excel.Range)workSheet.Cells[4, 4]).Value2.ToString();
                            date[2] = ((Excel.Range)workSheet.Cells[4, 5]).Value2.ToString();
                            date[3] = ((Excel.Range)workSheet.Cells[4, 6]).Value2.ToString();
                        }
                    book.Close(false, book, Type.Missing);
                    exobj.Workbooks.Close();
                    exobj.Quit();

                    //Insert into DB (LoadForecasting)

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@date11", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@edate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@peak", SqlDbType.Real);
                    MyCom.Parameters.Add("@h1", SqlDbType.Real);
                    MyCom.Parameters.Add("@h2", SqlDbType.Real);
                    MyCom.Parameters.Add("@h3", SqlDbType.Real);
                    MyCom.Parameters.Add("@h4", SqlDbType.Real);
                    MyCom.Parameters.Add("@h5", SqlDbType.Real);
                    MyCom.Parameters.Add("@h6", SqlDbType.Real);
                    MyCom.Parameters.Add("@h7", SqlDbType.Real);
                    MyCom.Parameters.Add("@h8", SqlDbType.Real);
                    MyCom.Parameters.Add("@h9", SqlDbType.Real);
                    MyCom.Parameters.Add("@h10", SqlDbType.Real);
                    MyCom.Parameters.Add("@h11", SqlDbType.Real);
                    MyCom.Parameters.Add("@h12", SqlDbType.Real);
                    MyCom.Parameters.Add("@h13", SqlDbType.Real);
                    MyCom.Parameters.Add("@h14", SqlDbType.Real);
                    MyCom.Parameters.Add("@h15", SqlDbType.Real);
                    MyCom.Parameters.Add("@h16", SqlDbType.Real);
                    MyCom.Parameters.Add("@h17", SqlDbType.Real);
                    MyCom.Parameters.Add("@h18", SqlDbType.Real);
                    MyCom.Parameters.Add("@h19", SqlDbType.Real);
                    MyCom.Parameters.Add("@h20", SqlDbType.Real);
                    MyCom.Parameters.Add("@h21", SqlDbType.Real);
                    MyCom.Parameters.Add("@h22", SqlDbType.Real);
                    MyCom.Parameters.Add("@h23", SqlDbType.Real);
                    MyCom.Parameters.Add("@h24", SqlDbType.Real);

                    for (int z = 0; z < 4; z++)
                    {
                        MyCom.CommandText = "INSERT INTO [LoadForecasting] (Date,DateEstimate,Peak,Hour1,Hour2,Hour3,Hour4," +
                        "Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19" +
                        ",Hour20,Hour21,Hour22,Hour23,Hour24) VALUES (@date11,@edate,@peak,@h1,@h2,@h3,@h4,@h5,@h6,@h7,@h8" +
                        ",@h9,@h10,@h11,@h12,@h13,@h14,@h15,@h16,@h17,@h18,@h19,@h20,@h21,@h22,@h23,@h24)";
                        MyCom.Connection = MyConnection;
                        MyCom.Parameters["@date11"].Value = date[z];
                        MyCom.Parameters["@edate"].Value = edate;
                        MyCom.Parameters["@peak"].Value = TempGV.Rows[28].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h1"].Value = TempGV.Rows[4].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h2"].Value = TempGV.Rows[5].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h3"].Value = TempGV.Rows[6].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h4"].Value = TempGV.Rows[7].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h5"].Value = TempGV.Rows[8].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h6"].Value = TempGV.Rows[9].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h7"].Value = TempGV.Rows[10].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h8"].Value = TempGV.Rows[11].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h9"].Value = TempGV.Rows[12].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h10"].Value = TempGV.Rows[13].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h11"].Value = TempGV.Rows[14].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h12"].Value = TempGV.Rows[15].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h13"].Value = TempGV.Rows[16].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h14"].Value = TempGV.Rows[17].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h15"].Value = TempGV.Rows[18].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h16"].Value = TempGV.Rows[19].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h17"].Value = TempGV.Rows[20].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h18"].Value = TempGV.Rows[21].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h19"].Value = TempGV.Rows[22].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h20"].Value = TempGV.Rows[23].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h21"].Value = TempGV.Rows[24].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h22"].Value = TempGV.Rows[25].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h23"].Value = TempGV.Rows[26].Cells[2 + z].Value.ToString();
                        MyCom.Parameters["@h24"].Value = TempGV.Rows[27].Cells[2 + z].Value.ToString();

                        try
                        {
                            MyCom.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                        }
                    }
                    MessageBox.Show("Selected File Has been Saved!");
                    System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                }
                else MessageBox.Show("Selected File Isnot Valid!");
            }
        }
        //---------------------------toolStripMenuItem2_Click------------------------------------------------
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Form6 baseData = new Form6();
            baseData.ShowDialog();
        }
        //-----------------------------------annualFactorToolStripMenuItem_Click---------------------------------------
        private void annualFactorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool check = true;
            DialogResult re = openFileDialog1.ShowDialog();
            if (re != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName;
                //Is It a Valid File?
                if (path.Contains("HCPF"))
                {
                    //read from HCPF.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "Data";
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                    objConn.Close();

                    //Insert into DB (HCPF)

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@h1", SqlDbType.Real);
                    MyCom.Parameters.Add("@h2", SqlDbType.Real);
                    MyCom.Parameters.Add("@h3", SqlDbType.Real);
                    MyCom.Parameters.Add("@h4", SqlDbType.Real);
                    MyCom.Parameters.Add("@h5", SqlDbType.Real);
                    MyCom.Parameters.Add("@h6", SqlDbType.Real);
                    MyCom.Parameters.Add("@h7", SqlDbType.Real);
                    MyCom.Parameters.Add("@h8", SqlDbType.Real);
                    MyCom.Parameters.Add("@h9", SqlDbType.Real);
                    MyCom.Parameters.Add("@h10", SqlDbType.Real);
                    MyCom.Parameters.Add("@h11", SqlDbType.Real);
                    MyCom.Parameters.Add("@h12", SqlDbType.Real);
                    MyCom.Parameters.Add("@h13", SqlDbType.Real);
                    MyCom.Parameters.Add("@h14", SqlDbType.Real);
                    MyCom.Parameters.Add("@h15", SqlDbType.Real);
                    MyCom.Parameters.Add("@h16", SqlDbType.Real);
                    MyCom.Parameters.Add("@h17", SqlDbType.Real);
                    MyCom.Parameters.Add("@h18", SqlDbType.Real);
                    MyCom.Parameters.Add("@h19", SqlDbType.Real);
                    MyCom.Parameters.Add("@h20", SqlDbType.Real);
                    MyCom.Parameters.Add("@h21", SqlDbType.Real);
                    MyCom.Parameters.Add("@h22", SqlDbType.Real);
                    MyCom.Parameters.Add("@h23", SqlDbType.Real);
                    MyCom.Parameters.Add("@h24", SqlDbType.Real);
                    MyCom.Connection = MyConnection;
                    int i = 0;
                    while (i < TempGV.RowCount)
                    {
                        //has Selected file saved before?
                        if ((check) && (TempGV.Rows[i].Cells[0].Value != null) && (TempGV.Rows[i].Cells[0].Value.ToString() != ""))
                        {
                            MyCom.CommandText = "INSERT INTO [HCPF] (Date,H1,H2,H3,H4,H5,H6,H7,H8,H9,H10,H11,H12," +
                            "H13,H14,H15,H16,H17,H18,H19,H20,H21,H22,H23,H24) VALUES (@date,@h1,@h2,@h3,@h4,@h5,@h6" +
                            ",@h7,@h8,@h9,@h10,@h11,@h12,@h13,@h14,@h15,@h16,@h17,@h18,@h19,@h20,@h21,@h22,@h23,@h24)";

                            //Translate Date in the Right Formet 1111/11/11
                            string date1 = TempGV.Rows[i].Cells[0].Value.ToString();
                            string date2 = date1.Remove(4);
                            date2 += "/";
                            date2 += date1[4];
                            date2 += date1[5];
                            date2 += "/";
                            date2 += date1[6];
                            date2 += date1[7];
                            MyCom.Parameters["@date"].Value = date2;
                            MyCom.Parameters["@h1"].Value = TempGV.Rows[i].Cells[1].Value.ToString();
                            MyCom.Parameters["@h2"].Value = TempGV.Rows[i].Cells[2].Value.ToString();
                            MyCom.Parameters["@h3"].Value = TempGV.Rows[i].Cells[3].Value.ToString();
                            MyCom.Parameters["@h4"].Value = TempGV.Rows[i].Cells[4].Value.ToString();
                            MyCom.Parameters["@h5"].Value = TempGV.Rows[i].Cells[5].Value.ToString();
                            MyCom.Parameters["@h6"].Value = TempGV.Rows[i].Cells[6].Value.ToString();
                            MyCom.Parameters["@h7"].Value = TempGV.Rows[i].Cells[7].Value.ToString();
                            MyCom.Parameters["@h8"].Value = TempGV.Rows[i].Cells[8].Value.ToString();
                            MyCom.Parameters["@h9"].Value = TempGV.Rows[i].Cells[9].Value.ToString();
                            MyCom.Parameters["@h10"].Value = TempGV.Rows[i].Cells[10].Value.ToString();
                            MyCom.Parameters["@h11"].Value = TempGV.Rows[i].Cells[11].Value.ToString();
                            MyCom.Parameters["@h12"].Value = TempGV.Rows[i].Cells[12].Value.ToString();
                            MyCom.Parameters["@h13"].Value = TempGV.Rows[i].Cells[13].Value.ToString();
                            MyCom.Parameters["@h14"].Value = TempGV.Rows[i].Cells[14].Value.ToString();
                            MyCom.Parameters["@h15"].Value = TempGV.Rows[i].Cells[15].Value.ToString();
                            MyCom.Parameters["@h16"].Value = TempGV.Rows[i].Cells[16].Value.ToString();
                            MyCom.Parameters["@h17"].Value = TempGV.Rows[i].Cells[17].Value.ToString();
                            MyCom.Parameters["@h18"].Value = TempGV.Rows[i].Cells[18].Value.ToString();
                            MyCom.Parameters["@h19"].Value = TempGV.Rows[i].Cells[19].Value.ToString();
                            MyCom.Parameters["@h20"].Value = TempGV.Rows[i].Cells[20].Value.ToString();
                            MyCom.Parameters["@h21"].Value = TempGV.Rows[i].Cells[21].Value.ToString();
                            MyCom.Parameters["@h22"].Value = TempGV.Rows[i].Cells[22].Value.ToString();
                            MyCom.Parameters["@h23"].Value = TempGV.Rows[i].Cells[23].Value.ToString();
                            MyCom.Parameters["@h24"].Value = TempGV.Rows[i].Cells[24].Value.ToString();

                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                                if (str.Contains("PRIMARY KEY"))
                                {
                                    //MessageBox.Show("Selected File Had been saved Before!");
                                    check = false;
                                }
                            }
                        }
                        //Selected file has been saved before?
                        if ((!check) && (TempGV.Rows[i].Cells[0].Value != null) && (TempGV.Rows[i].Cells[0].Value.ToString() != ""))
                        {
                            MyCom.CommandText = "UPDATE [HCPF] SET H1=@h1,H2=@h2,H3=@h3,H4=@h4,H5=@h5,H6=@h6," +
                            "H7=@h7,H8=@h8,H9=@h9,H10=@h10,H11=@h11,H12=@h12,H13=@h13,H14=@h14,H15=@h15,H16=@h16," +
                            "H17=@h17,H18=@h18,H19=@h19,H20=@h20,H21=@h21,H22=@h22,H23=@h23,H24=@h24 WHERE Date=@date";

                            //Translate Date in the Right Formet 1111/11/11
                            string date1 = TempGV.Rows[i].Cells[0].Value.ToString();
                            string date2 = date1.Remove(4);
                            date2 += "/";
                            date2 += date1[4];
                            date2 += date1[5];
                            date2 += "/";
                            date2 += date1[6];
                            date2 += date1[7];
                            MyCom.Parameters["@date"].Value = date2;
                            MyCom.Parameters["@h1"].Value = TempGV.Rows[i].Cells[1].Value.ToString();
                            MyCom.Parameters["@h2"].Value = TempGV.Rows[i].Cells[2].Value.ToString();
                            MyCom.Parameters["@h3"].Value = TempGV.Rows[i].Cells[3].Value.ToString();
                            MyCom.Parameters["@h4"].Value = TempGV.Rows[i].Cells[4].Value.ToString();
                            MyCom.Parameters["@h5"].Value = TempGV.Rows[i].Cells[5].Value.ToString();
                            MyCom.Parameters["@h6"].Value = TempGV.Rows[i].Cells[6].Value.ToString();
                            MyCom.Parameters["@h7"].Value = TempGV.Rows[i].Cells[7].Value.ToString();
                            MyCom.Parameters["@h8"].Value = TempGV.Rows[i].Cells[8].Value.ToString();
                            MyCom.Parameters["@h9"].Value = TempGV.Rows[i].Cells[9].Value.ToString();
                            MyCom.Parameters["@h10"].Value = TempGV.Rows[i].Cells[10].Value.ToString();
                            MyCom.Parameters["@h11"].Value = TempGV.Rows[i].Cells[11].Value.ToString();
                            MyCom.Parameters["@h12"].Value = TempGV.Rows[i].Cells[12].Value.ToString();
                            MyCom.Parameters["@h13"].Value = TempGV.Rows[i].Cells[13].Value.ToString();
                            MyCom.Parameters["@h14"].Value = TempGV.Rows[i].Cells[14].Value.ToString();
                            MyCom.Parameters["@h15"].Value = TempGV.Rows[i].Cells[15].Value.ToString();
                            MyCom.Parameters["@h16"].Value = TempGV.Rows[i].Cells[16].Value.ToString();
                            MyCom.Parameters["@h17"].Value = TempGV.Rows[i].Cells[17].Value.ToString();
                            MyCom.Parameters["@h18"].Value = TempGV.Rows[i].Cells[18].Value.ToString();
                            MyCom.Parameters["@h19"].Value = TempGV.Rows[i].Cells[19].Value.ToString();
                            MyCom.Parameters["@h20"].Value = TempGV.Rows[i].Cells[20].Value.ToString();
                            MyCom.Parameters["@h21"].Value = TempGV.Rows[i].Cells[21].Value.ToString();
                            MyCom.Parameters["@h22"].Value = TempGV.Rows[i].Cells[22].Value.ToString();
                            MyCom.Parameters["@h23"].Value = TempGV.Rows[i].Cells[23].Value.ToString();
                            MyCom.Parameters["@h24"].Value = TempGV.Rows[i].Cells[24].Value.ToString();

                            //try
                            //{
                            MyCom.ExecuteNonQuery();
                            //}
                            //catch (Exception exp)
                            //{
                            //    string str = exp.Message;
                            //}
                        }
                        i++;
                    }
                    MessageBox.Show("Selected File Has been saved!");
                }
                else MessageBox.Show("Selected File Isnot Valid!");
            }
        }
        //----------------------------------weekFactorToolStripMenuItem-------------------------------------------
        private void weekFactorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool check = true;
            DialogResult re = openFileDialog1.ShowDialog();
            if (re != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName;
                //Is It a Valid File?
                if (path.Contains("WCPF"))
                {
                    //read from WCPF.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "Data";
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                    objConn.Close();

                    //Insert into DB (WCPF)

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@year", SqlDbType.Int);
                    MyCom.Parameters.Add("@week", SqlDbType.Int);
                    MyCom.Parameters.Add("@value", SqlDbType.Real);
                    MyCom.Connection = MyConnection;
                    int i = 0;
                    while (i < TempGV.RowCount)
                    {
                        //has Selected file saved before?
                        if ((check) && (TempGV.Rows[i].Cells[0].Value != null) && (TempGV.Rows[i].Cells[0].Value.ToString() != ""))
                        {
                            MyCom.CommandText = "INSERT INTO [WCPF] (Year,Week,Value) VALUES (@year,@week,@value)";
                            MyCom.Parameters["@year"].Value = TempGV.Rows[i].Cells[0].Value.ToString();
                            MyCom.Parameters["@week"].Value = TempGV.Rows[i].Cells[1].Value.ToString();
                            MyCom.Parameters["@value"].Value = TempGV.Rows[i].Cells[2].Value.ToString();
                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                                if (str.Contains("PRIMARY KEY"))
                                {
                                    //MessageBox.Show("Selected File Had been saved Before!");
                                    check = false;
                                }
                            }
                        }
                        if ((!check) && (TempGV.Rows[i].Cells[0].Value != null) && (TempGV.Rows[i].Cells[0].Value.ToString() != ""))
                        {
                            MyCom.CommandText = "UPDATE [WCPF] SET Value=@value WHERE Year=@year AND Week=@week";
                            MyCom.Parameters["@year"].Value = TempGV.Rows[i].Cells[0].Value.ToString();
                            MyCom.Parameters["@week"].Value = TempGV.Rows[i].Cells[1].Value.ToString();
                            MyCom.Parameters["@value"].Value = TempGV.Rows[i].Cells[2].Value.ToString();
                            //try
                            //{
                            MyCom.ExecuteNonQuery();
                            //}
                            //catch (Exception exp)
                            //{
                            //    string str = exp.Message;
                            //}
                        }
                        i++;
                    }
                    MessageBox.Show("Selected File Has been saved!");
                }
                else MessageBox.Show("Selected File Isnot Valid!");
            }
        }

        //private void JustOneTime(string StartDate)
        //{
        //    //detect Yesterday
        //    System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
        //    DateTime CurDate = DateTime.Now;
        //    int imonth = pr.GetMonth(CurDate);
        //    int iyear = pr.GetYear(CurDate);
        //    int iday = pr.GetDayOfMonth(CurDate) - 1;
        //    if (iday < 1)
        //    {
        //        imonth = imonth - 1;
        //        if (imonth < 1)
        //        {
        //            iyear = iyear - 1;
        //            imonth = 12;
        //        }
        //        if (imonth < 7) iday = 31;
        //        else if (imonth < 12) iday = 30;
        //        else iday = 29;
        //    }
        //    string day = iday.ToString();
        //    if (int.Parse(day) < 10) day = "0" + day;
        //    string mydate = iyear.ToString() + "/" + imonth.ToString() + "/" + day;

        //    while (StartDate != mydate)
        //    {
        //        SqlConnection MyCon = new SqlConnection();
        //        MyCon.ConnectionString = ConStr;
        //        MyCon.Open();
        //        SqlCommand MyCom1 = new SqlCommand();
        //        MyCom1.Connection = MyCon;

        //        //is there any data for this date on Database?
        //        MyCom1.CommandText = "SELECT @result=COUNT(Date) FROM EconomicPlant WHERE Date=@date";
        //        MyCom1.Parameters.Add("@date", SqlDbType.Char, 10);
        //        MyCom1.Parameters["@date"].Value = StartDate;
        //        MyCom1.Parameters.Add("@result", SqlDbType.Int);
        //        MyCom1.Parameters["@result"].Direction = ParameterDirection.Output;
        //        //try
        //        //{
        //        MyCom1.ExecuteNonQuery();
        //        //}
        //        //catch (Exception e)
        //        //{
        //        //string message = e.Message;
        //        //}
        //        int result = int.Parse(MyCom1.Parameters["@result"].Value.ToString());
        //        if (result == 0)
        //        {
        //            //???????????????SET DATE??????????????
        //            //string mydate = FRUnitCal.Text;
        //            //mydate = "1388/08/01";
        //            //for all palnts
        //            MyCom1.CommandText = "SELECT @updatedate=Date FROM BaseData WHERE BaseID=IDENT_CURRENT('BaseData')";
        //            MyCom1.Parameters.Add("@updatedate", SqlDbType.Char, 10);
        //            MyCom1.Parameters["@updatedate"].Direction = ParameterDirection.Output;
        //            //try
        //            //{
        //            MyCom1.ExecuteNonQuery();
        //            //}
        //            //catch (Exception e)
        //            //{
        //            //string message = e.Message;
        //            //}
        //            //detect date of last inserted row in BaseData 
        //            string UpdateDate = MyCom1.Parameters["@updatedate"].Value.ToString();
        //            for (int k = 0; k < NumPPID; k++)
        //                if ((PPIDArray[k] != "0") && (PPIDArray[k] != "132") && (PPIDArray[k] != "145")) FillEconomicPlant(k, UpdateDate, StartDate);
        //        }
        //        MyCon.Close();

        //        //Build Next day
        //        string TempDate = StartDate;
        //        int Tempday = int.Parse(TempDate.Substring(8));
        //        int TempMonth = int.Parse(TempDate.Substring(5, 2));
        //        int Tempyear = int.Parse(TempDate.Substring(0, 4));
        //        Tempday = Tempday + 1;
        //        if (TempMonth < 7)
        //        {
        //            if (Tempday > 31)
        //            {
        //                Tempday = 1;
        //                TempMonth++;
        //            }
        //        }
        //        else if (TempMonth < 12)
        //        {
        //            if (Tempday > 30)
        //            {
        //                Tempday = 1;
        //                TempMonth++;
        //            }
        //        }
        //        else if (TempMonth == 12)
        //            if (Tempday > 29)
        //            {
        //                Tempday = 1;
        //                TempMonth = 1;
        //                Tempyear++;
        //            }
        //        string tempday = "";
        //        string tempmonth = "";
        //        if (Tempday < 10) tempday = "0" + Tempday.ToString();
        //        if (TempMonth < 10) tempmonth = "0" + TempMonth.ToString();
        //        StartDate = Tempyear + "/" + tempmonth + "/" + tempday;
        //    }

        //}
        //------------------------------------------AutomaticFillEconomics---------------------------------
        private void AutomaticFillEconomics()
        {
            //detect Yesterday
            System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
            DateTime CurDate = DateTime.Now;
            int imonth = pr.GetMonth(CurDate);
            int iyear = pr.GetYear(CurDate);
            int iday = pr.GetDayOfMonth(CurDate) - 1;
            if (iday < 1)
            {
                imonth = imonth - 1;
                if (imonth < 1)
                {
                    iyear = iyear - 1;
                    imonth = 12;
                }
                if (imonth < 7) iday = 31;
                else if (imonth < 12) iday = 30;
                else iday = 29;
            }
            string day = iday.ToString();
            if (int.Parse(day) < 10) day = "0" + day;
            string mydate = iyear.ToString() + "/" + imonth.ToString() + "/" + day;

            SqlConnection MyCon = ConnectionManager.GetInstance().Connection;
            SqlCommand MyCom1 = new SqlCommand();
            MyCom1.Connection = MyCon;

            //is there any data for this date on Database?
            MyCom1.CommandText = "SELECT @result=COUNT(Date) FROM EconomicPlant WHERE Date=@date";
            MyCom1.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom1.Parameters["@date"].Value = mydate;
            MyCom1.Parameters.Add("@result", SqlDbType.Int);
            MyCom1.Parameters["@result"].Direction = ParameterDirection.Output;
            //try
            //{
            MyCom1.ExecuteNonQuery();
            //}
            //catch (Exception e)
            //{
            //string message = e.Message;
            //}
            int result = int.Parse(MyCom1.Parameters["@result"].Value.ToString());
            if (result == 0)
            {
                //???????????????SET DATE??????????????
                //string mydate = FRUnitCal.Text;
                //mydate = "1388/08/01";
                //for all palnts
                MyCom1.CommandText = "SELECT @updatedate=Date FROM BaseData WHERE BaseID=IDENT_CURRENT('BaseData')";
                MyCom1.Parameters.Add("@updatedate", SqlDbType.Char, 10);
                MyCom1.Parameters["@updatedate"].Direction = ParameterDirection.Output;
                //try
                //{
                MyCom1.ExecuteNonQuery();
                //}
                //catch (Exception e)
                //{
                //string message = e.Message;
                //}
                //detect date of last inserted row in BaseData 
                string UpdateDate = MyCom1.Parameters["@updatedate"].Value.ToString();
                for (int k = 0; k < NumPPID; k++)
                    if ((PPIDArray[k] != "0") && (PPIDArray[k] != "132") && (PPIDArray[k] != "145")) FillEconomicPlant(k, UpdateDate, mydate);
            }
        }
        //----------------------------------------FillEconomicPlant----------------------------------------
        private void FillEconomicPlant(int k, string UpdateDate, string mydate)
        {
            ResetVariables();

            for (int z = 0; z < 24; z++)
            {
                NoCCMaxBid[z] = 0;
                CCMaxBid[z] = 0;
            }

            SqlConnection PlantCon = ConnectionManager.GetInstance().Connection;
            DataSet PlantDS = new DataSet();
            SqlDataAdapter Plantda = new SqlDataAdapter();
            Plantda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode,Capacity,FixedCost," +
            "VariableCost,Bmaintenance,CMaintenance,PrimaryFuelAmargin,PrimaryFuelBmargin,PrimaryFuelCmargin," +
            "SecondFuelAmargin,SecondFuelBmargin,SecondFuelCmargin,CostStartCold,CostStartHot FROM [UnitsDataMain]" +
            "WHERE PPID=@num", PlantCon);
            Plantda.SelectCommand.Parameters.Add("@num", SqlDbType.NChar, 10);
            Plantda.SelectCommand.Parameters["@num"].Value = PPIDArray[k].ToString();
            Plantda.Fill(PlantDS);
            TempGV.DataSource = PlantDS.Tables[0].DefaultView;


            //for all Units of selected plant
            bool[] check = new bool[TempGV.RowCount - 1];
            for (int x = 0; x < TempGV.RowCount - 1; x++) check[x] = false;
            for (int index = 0; index < (TempGV.RowCount - 1); index++)
                if (!check[index])
                {
                    int Pcode = int.Parse(TempGV.Rows[index].Cells[2].Value.ToString());
                    //for  CombinedCycle 's Units one time run this loop
                    check[index] = true;
                    foreach (DataGridViewRow DR in TempGV.Rows)
                        if (DR.Index < TempGV.RowCount - 1)
                            if ((int.Parse(DR.Cells[2].Value.ToString()) == Pcode) && (DR.Cells[1].Value.ToString().Contains("CC")))
                                check[DR.Index] = true;

                    FillEconomicUnit(k, UpdateDate, mydate, index);
                }

            //FILL EconomicPlant For EVERY Plant!

            //****************PTotalPower*****************
            SqlConnection MyCon = ConnectionManager.GetInstance().Connection;
            SqlCommand MyCom1 = new SqlCommand();
            MyCom1.Connection = MyCon;
            MyCom1.CommandText = "SELECT @re=SUM(Hour1+Hour2+Hour3+Hour4+Hour5+Hour6+Hour7+Hour8+Hour9+Hour10+" +
            "Hour11+Hour12+Hour13+Hour14+Hour15+Hour16+Hour17+Hour18+Hour19+Hour20+Hour21+Hour22+Hour23+Hour24)" +
            " FROM ProducedEnergy WHERE PPCode=@num AND Date=@mdate";
            MyCom1.Parameters.Add("@mdate", SqlDbType.Char, 10);
            MyCom1.Parameters["@mdate"].Value = mydate;
            MyCom1.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom1.Parameters["@num"].Value = PPIDArray[k];
            MyCom1.Parameters.Add("@re", SqlDbType.Real);
            MyCom1.Parameters["@re"].Direction = ParameterDirection.Output;
            //try
            //{
            MyCom1.ExecuteNonQuery();
            //}
            //catch (Exception e)
            //{
            //string message = e.Message;
            //}
            try { NoCCTotalPower = double.Parse(MyCom1.Parameters["@re"].Value.ToString()); }
            catch { NoCCTotalPower = 0; }
            int CCID = int.Parse(PPIDArray[k]);
            if ((CCID == 131) || (CCID == 144))
            {
                CCID++;
                MyCom1.CommandText = "SELECT @CCre=SUM(Hour1+Hour2+Hour3+Hour4+Hour5+Hour6+Hour7+Hour8+Hour9+Hour10+" +
                "Hour11+Hour12+Hour13+Hour14+Hour15+Hour16+Hour17+Hour18+Hour19+Hour20+Hour21+Hour22+Hour23+Hour24)" +
                " FROM ProducedEnergy WHERE PPCode=@CCnum AND Date=@mdate";
                MyCom1.Parameters.Add("@CCnum", SqlDbType.NChar, 10);
                MyCom1.Parameters["@CCnum"].Value = CCID;
                MyCom1.Parameters.Add("@CCre", SqlDbType.Real);
                MyCom1.Parameters["@CCre"].Direction = ParameterDirection.Output;
                //try
                //{
                MyCom1.ExecuteNonQuery();
                //}
                //catch (Exception e)
                //{
                //string message = e.Message;
                //}
                try { CCTotalPower = double.Parse(MyCom1.Parameters["@CCre"].Value.ToString()); }
                catch { CCTotalPower = 0; }
            }
            else CCTotalPower = 0;
            PTotalPower = NoCCTotalPower + CCTotalPower;

            //****************IncrementPower & DecreasePower
            NoCCIncPower = 0;
            NoCCDecPower = 0;
            if ((NoCCTotalPower - NoCCUnitPower) > 0)
            {
                NoCCIncPower = NoCCTotalPower - NoCCUnitPower;
                NoCCDecPower = 0;
            }
            else if ((NoCCTotalPower - NoCCUnitPower) < 0)
            {
                NoCCIncPower = 0;
                NoCCDecPower = NoCCUnitPower - NoCCTotalPower;
            }
            if ((int.Parse(PPIDArray[k]) == 131) || (int.Parse(PPIDArray[k]) == 144))
            {
                if ((CCTotalPower - CCUnitPower) > 0)
                {
                    CCIncPower = CCTotalPower - CCUnitPower;
                    CCDecPower = 0;
                }
                else if ((CCTotalPower - CCUnitPower) < 0)
                {
                    CCIncPower = 0;
                    CCDecPower = CCUnitPower - CCTotalPower;
                }

            }
            else
            {
                CCIncPower = 0;
                CCDecPower = 0;
            }

            PIncrementPower = NoCCIncPower + CCIncPower;
            PDecreasePower = NoCCDecPower + CCDecPower;
            //*************************BidPower
            PBidPower = PBidPower - PULPower;

            //*************************IncrementPayment/DecreasePayment
            PIncrementPayment = 0;
            PDecreasePayment = 0;
            double NoCCDecPayment = 0;
            // Detect Hazineye tolid
            switch (PPIDArray[k])
            {
                case "101":
                    MyCom1.CommandText = "SELECT @NoCC=SteamProduction FROM BaseData WHERE Date=@update";
                    break;
                case "131":
                    MyCom1.CommandText = "SELECT @NoCC=SteamProduction,@CC=CCProduction FROM BaseData WHERE Date=@update";
                    break;
                case "104":
                    MyCom1.CommandText = "SELECT @NoCC=GasProduction FROM BaseData WHERE Date=@update";
                    break;
                case "133":
                    MyCom1.CommandText = "SELECT @NoCC=SteamProduction FROM BaseData WHERE Date=@update";
                    break;
                case "138":
                    MyCom1.CommandText = "SELECT @NoCC=GasProduction FROM BaseData WHERE Date=@update";
                    break;
                case "144":
                    MyCom1.CommandText = "SELECT @NoCC=SteamProduction,@CC=CCProduction FROM BaseData WHERE Date=@update";
                    break;
                case "149":
                    MyCom1.CommandText = "SELECT @NoCC=CCProduction FROM BaseData WHERE Date=@update";
                    break;
            }
            MyCom1.Parameters.Add("@update", SqlDbType.Char, 10);
            MyCom1.Parameters["@update"].Value = UpdateDate;
            MyCom1.Parameters.Add("@CC", SqlDbType.Real);
            MyCom1.Parameters["@CC"].Direction = ParameterDirection.Output;
            MyCom1.Parameters.Add("@NoCC", SqlDbType.Real);
            MyCom1.Parameters["@NoCC"].Direction = ParameterDirection.Output;
            //try
            //{
            MyCom1.ExecuteNonQuery();
            //}
            //catch (Exception e)
            //{
            //string message = e.Message;
            //}
            double NoCC, CC = 0;
            NoCC = double.Parse(MyCom1.Parameters["@NoCC"].Value.ToString());
            if ((int.Parse(PPIDArray[k]) == 131) || (int.Parse(PPIDArray[k]) == 144))
                CC = double.Parse(MyCom1.Parameters["@CC"].Value.ToString());

            double mycost = NoCC;
            double[] myMaxbid = new double[24];
            for (int w = 0; w < 24; w++) myMaxbid[w] = NoCCMaxBid[w];
            int repeatIndex = 0;
            bool repeat = true;
            while ((repeat) && (repeatIndex < 2))
            {
                double[] re = new double[24];
                double[] produce = new double[24];
                int type = 0;
                string num = PPIDArray[k];

                SqlConnection FRCon = ConnectionManager.GetInstance().Connection;
                DataSet FRDS = new DataSet();
                SqlDataAdapter FRda = new SqlDataAdapter();
                FRda.SelectCommand = new SqlCommand("SELECT SUM(Required),Hour FROM " +
                "[DetailFRM005] WHERE TargetMarketDate=@date AND PPID=@num AND PPType=@type GROUP BY Hour ", FRCon);
                FRda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                FRda.SelectCommand.Parameters["@date"].Value = mydate;
                FRda.SelectCommand.Parameters.Add("@num", SqlDbType.NChar, 10);
                FRda.SelectCommand.Parameters["@num"].Value = num;
                FRda.SelectCommand.Parameters.Add("@type", SqlDbType.SmallInt);
                FRda.SelectCommand.Parameters["@type"].Value = type;
                FRda.Fill(FRDS);
                foreach (DataRow MyRow in FRDS.Tables[0].Rows)
                    re[int.Parse(MyRow[1].ToString()) - 1] = double.Parse(MyRow[0].ToString());

                SqlConnection PCon = ConnectionManager.GetInstance().Connection;
                DataSet PDS = new DataSet();
                SqlDataAdapter Pda = new SqlDataAdapter();
                Pda.SelectCommand = new SqlCommand("SELECT SUM(Hour1),SUM(Hour2),SUM(Hour3),SUM(Hour4),SUM(Hour5),SUM(Hour6)" +
                ",SUM(Hour7),SUM(Hour8),SUM(Hour9),SUM(Hour10),SUM(Hour11),SUM(Hour12),SUM(Hour13),SUM(Hour14),SUM(Hour15)," +
                "SUM(Hour16),SUM(Hour17),SUM(Hour18),SUM(Hour19),SUM(Hour20),SUM(Hour21),SUM(Hour22),SUM(Hour23),SUM(Hour24) " +
                "FROM [ProducedEnergy] WHERE Date=@date AND PPCode=@num", PCon);
                Pda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Pda.SelectCommand.Parameters["@date"].Value = mydate;
                Pda.SelectCommand.Parameters.Add("@num", SqlDbType.NChar, 10);
                Pda.SelectCommand.Parameters["@num"].Value = num;
                Pda.Fill(PDS);
                foreach (DataRow MyRow in PDS.Tables[0].Rows)
                    for (int i = 0; i < 24; i++)
                    {
                        //produce[int.Parse(MyRow[1].ToString())] = double.Parse(MyRow[0].ToString());
                        try { produce[i] = double.Parse(MyRow[i].ToString()); }
                        catch { produce[i] = 0; }
                    }

                double[] tempIncPower = new double[24];
                double[] tempDecPower = new double[24];
                double[] tempIncPay = new double[24];
                double[] tempDecPay = new double[24];

                for (int q = 0; q < 24; q++)
                {

                    if (produce[q] > re[q])
                    {
                        tempIncPower[q] = produce[q] - re[q];
                        tempDecPower[q] = 0;
                    }
                    else
                    {
                        tempIncPower[q] = 0;
                        tempDecPower[q] = re[q] - produce[q];
                    }
                    tempIncPay[q] = tempIncPower[q] * myMaxbid[q];
                    tempDecPay[q] = tempDecPower[q] * (myMaxbid[q] - mycost);

                    PIncrementPayment += tempIncPay[q];
                    PDecreasePayment += tempDecPay[q];
                }
                if (repeatIndex == 0)
                    NoCCDecPayment = PDecreasePayment;
                if ((int.Parse(PPIDArray[k]) == 131) || (int.Parse(PPIDArray[k]) == 144))
                {
                    repeat = true;
                    num = (int.Parse(PPIDArray[k]) + 1).ToString(); ;
                    type = 1;
                    for (int w = 0; w < 24; w++) myMaxbid[w] = CCMaxBid[w];
                    mycost = CC;
                }
                else repeat = false;
                repeatIndex++;
            }

            //*************************EnergyPayment
            PEnergyPayment = PBidPayment + PULPayment + PIncrementPayment - (NoCCDecPayment * NoCC) - ((PDecreasePayment - NoCCDecPayment) * CC);

            //*************************Income
            PIncome = PEnergyPayment + PCapacityPayment;

            //*************************Benefit
            PBenefit = PIncome - PCost;

            //INSERT INTO EconomicPlant Table
            MyCom1.CommandText = "INSERT INTO [EconomicPlant] (PPID,Date,AvailableCapacity,TotalPower,BidPower," +
            "ULPower,Cost,CapacityPayment,EnergyPayment,Income,Benefit,IncrementPower,DecreasePower,BidPayment," +
            "ULPayment,IncrementPayment,DecreasePayment) VALUES (@num,@date,@avacap,@tpower,@bpower,@upower," +
            "@cost,@cap,@energy,@income,@benefit,@incpo,@decpo,@bidpay,@upay,@incpay,@decpay)";
            MyCom1.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom1.Parameters["@date"].Value = mydate;
            MyCom1.Parameters.Add("@avacap", SqlDbType.Real);
            MyCom1.Parameters["@avacap"].Value = PAvailableCapacity;
            MyCom1.Parameters.Add("@tpower", SqlDbType.Real);
            MyCom1.Parameters["@tpower"].Value = PTotalPower;
            MyCom1.Parameters.Add("@bpower", SqlDbType.Real);
            MyCom1.Parameters["@bpower"].Value = PBidPower;
            MyCom1.Parameters.Add("@upower", SqlDbType.Real);
            MyCom1.Parameters["@upower"].Value = PULPower;
            MyCom1.Parameters.Add("@cost", SqlDbType.Real);
            MyCom1.Parameters["@cost"].Value = PCost;
            MyCom1.Parameters.Add("@cap", SqlDbType.Real);
            MyCom1.Parameters["@cap"].Value = PCapacityPayment;
            MyCom1.Parameters.Add("@energy", SqlDbType.Real);
            MyCom1.Parameters["@energy"].Value = PEnergyPayment;
            MyCom1.Parameters.Add("@income", SqlDbType.Real);
            MyCom1.Parameters["@income"].Value = PIncome;
            MyCom1.Parameters.Add("@benefit", SqlDbType.Real);
            MyCom1.Parameters["@benefit"].Value = PBenefit;
            MyCom1.Parameters.Add("@incpo", SqlDbType.Real);
            MyCom1.Parameters["@incpo"].Value = PIncrementPower;
            MyCom1.Parameters.Add("@decpo", SqlDbType.Real);
            MyCom1.Parameters["@decpo"].Value = PDecreasePower;
            MyCom1.Parameters.Add("@bidpay", SqlDbType.Real);
            MyCom1.Parameters["@bidpay"].Value = PBidPayment;
            MyCom1.Parameters.Add("@upay", SqlDbType.Real);
            MyCom1.Parameters["@upay"].Value = PULPayment;
            MyCom1.Parameters.Add("@incpay", SqlDbType.Real);
            MyCom1.Parameters["@incpay"].Value = PIncrementPayment;
            MyCom1.Parameters.Add("@decpay", SqlDbType.Real);
            MyCom1.Parameters["@decpay"].Value = PDecreasePayment;

            //try
            //{
            MyCom1.ExecuteNonQuery();
            //}
            //catch (Exception e)
            //{
            //string message = e.Message;
            //}
        }
        //-----------------------------------------------------------FillEconomicUnit()-----------------------------------
        private void FillEconomicUnit(int k, string UpdateDate, string mydate, int index)
        {
            double AvailableCapacity = 0, TotalPower = 0, ULPower = 0, CostPayment = 0, CapacityPayment = 0,
            EnergyPayment = 0, Income = 0, Benefit = 0;

            string unit = TempGV.Rows[index].Cells[0].Value.ToString().Trim();
            string package = TempGV.Rows[index].Cells[1].Value.ToString().Trim();
            int Pcode = int.Parse(TempGV.Rows[index].Cells[2].Value.ToString());

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;

            //Detect Block For FRM005
            string temp = unit;
            temp = temp.ToLower();
            if (package.Contains("CC"))
            {
                int x = int.Parse(PPIDArray[k]);
                if ((x == 131) || (x == 144)) x++;
                temp = x + "-" + "C" + Pcode;
            }
            else if (temp.Contains("gas"))
            {
                temp = temp.Replace("gas", "G");
                temp = PPIDArray[k] + "-" + temp;
            }
            else
            {
                temp = temp.Replace("steam", "S");
                temp = PPIDArray[k] + "-" + temp;
            }
            //*************************SET AvailableCapacity/ TotalPower/ ULPower
            AvailableCapacity = SetAvailableCap(k, mydate, index, temp);
            TotalPower = SetTotalPower(k, mydate, index, temp);
            ULPower = SetULPower(k, mydate, index, temp);

            //*************************************SET CostPayment
            double[] P = new double[24];
            double[] Dis = new double[24];
            string[] Contribution = new string[24];
            double[] W = new double[24];
            double[] U = new double[24];
            double F4 = 0;
            SqlConnection FRCon = ConnectionManager.GetInstance().Connection;
            DataSet FRDS = new DataSet();
            SqlDataAdapter FRda = new SqlDataAdapter();
            FRda.SelectCommand = new SqlCommand("SELECT Required,Hour,Dispatchable,Contribution FROM " +
            "[DetailFRM005] WHERE TargetMarketDate=@date AND PPID=@num AND Block=@block", FRCon);
            FRda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
            FRda.SelectCommand.Parameters["@date"].Value = mydate;
            FRda.SelectCommand.Parameters.Add("@num", SqlDbType.NChar, 10);
            FRda.SelectCommand.Parameters["@num"].Value = PPIDArray[k];
            FRda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
            FRda.SelectCommand.Parameters["@block"].Value = temp;
            FRda.Fill(FRDS);
            for (int i = 0; i < 24; i++)
            {
                P[i] = 0;
                Dis[i] = 0;
                Contribution[i] = "N";
            }
            foreach (DataRow MyRow in FRDS.Tables[0].Rows)
            {
                P[int.Parse(MyRow[1].ToString()) - 1] = double.Parse(MyRow[0].ToString());
                Dis[int.Parse(MyRow[1].ToString()) - 1] = double.Parse(MyRow[2].ToString());
                Contribution[int.Parse(MyRow[1].ToString()) - 1] = MyRow[3].ToString();
            }

            CostPayment = SetCostPayment(k, index, P, Dis);
            //*************************SET CapacityPayment
            CapacityPayment = SetCapacityPayment(k, UpdateDate, mydate, index, Dis);

            //*************************SET EnergyPayment
            SqlConnection PowerCon = ConnectionManager.GetInstance().Connection;
            DataSet PowerDS = new DataSet();
            SqlDataAdapter Powerda = new SqlDataAdapter();
            Powerda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3," +
            "Power4,Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10," +
            "Price10,Hour FROM [DetailFRM002] WHERE TargetMarketDate=@date AND PPID=@num AND Block=@block", FRCon);
            Powerda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
            Powerda.SelectCommand.Parameters["@date"].Value = mydate;
            Powerda.SelectCommand.Parameters.Add("@num", SqlDbType.NChar, 10);
            Powerda.SelectCommand.Parameters["@num"].Value = PPIDArray[k];
            Powerda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
            //DETECT Block For FRM002
            string block = unit;
            block = block.ToLower();
            if (package.Contains("CC"))
                block = "C" + Pcode;
            else
            {
                if (block.Contains("gas"))
                    block = block.Replace("gas", "G");
                else if (block.Contains("steam"))
                    block = block.Replace("steam", "S");
            }
            block = block.Trim();
            Powerda.SelectCommand.Parameters["@block"].Value = block;
            Powerda.Fill(PowerDS);

            //SET Total Of EnergyPayment for all of those Contribution=="UL" or Contribution=="N" 
            double N_EPUnit = 0, UL_EPUnit = 0;

            double[] EP = new double[24];
            foreach (DataRow PRow in PowerDS.Tables[0].Rows)
            {
                int hour = int.Parse(PRow[20].ToString()) - 1;
                EP[hour] = 0;
                double[] Power = new double[10];
                double[] price = new double[10];
                for (int z = 0; z < 10; z++) Power[z] = double.Parse(PRow[2 * z].ToString());
                for (int z = 0; z < 10; z++) price[z] = double.Parse(PRow[(2 * z) + 1].ToString());
                if (Contribution[hour].Contains("N"))
                {
                    int p1 = 0;
                    while (p1 < 10)
                    {
                        if (Power[p1] <= P[hour]) EP[hour] += Power[p1] * price[p1];
                        else if ((Power[p1] > P[hour]))
                        {
                            if (p1 > 0) EP[hour] += (P[hour] - Power[p1 - 1]) * price[p1];
                            else EP[hour] += P[hour] * price[p1];
                            p1 = 10;
                        }
                        p1++;
                    }
                    N_EPUnit += EP[hour];
                }
                //if (Contribution[hour].Contains("UL"))
                else
                {
                    //DETECT Min Price Form AveragePrice Table
                    SqlConnection MyCon = ConnectionManager.GetInstance().Connection;
                    SqlCommand MyCom1 = new SqlCommand();
                    MyCom1.Connection = MyCon;

                    MyCom1.CommandText = "SELECT @min=AcceptedMin FROM [AveragePrice] WHERE Date=@date " +
                    "AND Hour=@hour";
                    MyCom1.Parameters.Add("@date", SqlDbType.Char, 10);
                    MyCom1.Parameters["@date"].Value = mydate;
                    MyCom1.Parameters.Add("@hour", SqlDbType.SmallInt);
                    MyCom1.Parameters["@hour"].Value = hour + 1;
                    MyCom1.Parameters.Add("@min", SqlDbType.Real);
                    MyCom1.Parameters["@min"].Direction = ParameterDirection.Output;
                    //try
                    //{
                    MyCom1.ExecuteNonQuery();
                    //}
                    //catch (Exception e)
                    //{
                    //string message = e.Message;
                    //}
                    double minPrice;
                    try { minPrice = double.Parse(MyCom1.Parameters["@min"].Value.ToString()); }
                    catch { minPrice = 0; }
                    EP[hour] = Power[0] * minPrice * 0.9;
                    UL_EPUnit += EP[hour];
                }

                //*************************SET MaxBid
                double TempBid = 0;
                int x = 0;
                while ((x < 10) && (Power[x] < P[hour])) x++;
                if ((x < 10) && (Power[x] >= P[hour])) TempBid = price[x];
                if ((int.Parse(PPIDArray[k]) == 131) || (int.Parse(PPIDArray[k]) == 144))
                {
                    if ((package.Contains("CC")) && (CCMaxBid[hour] < TempBid)) CCMaxBid[hour] = TempBid;
                    else if ((!(package.Contains("CC"))) && (NoCCMaxBid[hour] < TempBid)) NoCCMaxBid[hour] = TempBid;
                }
                //if ((!(package.Contains("Combined"))) && (NoCCMaxBid < TempBid)) 
                else if (NoCCMaxBid[hour] < TempBid)
                    NoCCMaxBid[hour] = TempBid;
            }

            EnergyPayment = 0;
            for (int x = 0; x < 24; x++)
                EnergyPayment += EP[x];

            //*************************SET Income
            Income = EnergyPayment + CapacityPayment;

            //*************************SET Benefit
            Benefit = Income - CostPayment;


            //INSERT INTO EconomicUnit Table
            MyCom.CommandText = "INSERT INTO [EconomicUnit] (PPID,UnitCode,Date,AvailableCapacity,TotalPower," +
            "ULPower,CostPayment,CapacityPayment,EnergyPayment,Income,Benefit) VALUES (@num,@Punit,@date," +
            "@avacap,@tpower,@upower,@cost,@cap,@energy,@income,@benefit)";
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPIDArray[k];
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = mydate;
            MyCom.Parameters.Add("@Punit", SqlDbType.NChar, 20);
            string Punit = unit;
            if (package.Contains("CC"))
                Punit = "C" + Pcode;
            Punit = Punit.Trim();
            MyCom.Parameters["@Punit"].Value = Punit;
            MyCom.Parameters.Add("@avacap", SqlDbType.Real);
            MyCom.Parameters["@avacap"].Value = AvailableCapacity;
            MyCom.Parameters.Add("@tpower", SqlDbType.Real);
            MyCom.Parameters["@tpower"].Value = TotalPower;
            MyCom.Parameters.Add("@upower", SqlDbType.Real);
            MyCom.Parameters["@upower"].Value = ULPower;
            MyCom.Parameters.Add("@cost", SqlDbType.Real);
            MyCom.Parameters["@cost"].Value = CostPayment;
            MyCom.Parameters.Add("@cap", SqlDbType.Real);
            MyCom.Parameters["@cap"].Value = CapacityPayment;
            MyCom.Parameters.Add("@energy", SqlDbType.Real);
            MyCom.Parameters["@energy"].Value = EnergyPayment;
            MyCom.Parameters.Add("@income", SqlDbType.Real);
            MyCom.Parameters["@income"].Value = Income;
            MyCom.Parameters.Add("@benefit", SqlDbType.Real);
            MyCom.Parameters["@benefit"].Value = Benefit;
            //try
            //{
            MyCom.ExecuteNonQuery();
            //}
            //catch (Exception e)
            //{
            //string message = e.Message;
            //}

            //SET Some Items Of selected Plant, Incrementally!
            PAvailableCapacity += AvailableCapacity;
            PULPower += ULPower;
            PBidPower += TotalPower;
            PCapacityPayment += CapacityPayment;
            PBidPayment += N_EPUnit;
            PULPayment += UL_EPUnit;
            PCost += CostPayment;
            
        }
        //--------------------------------------SetCapacityPayment---------------------------------
        private double SetCapacityPayment(int k, string UpdateDate, string mydate, int index, double[] Dis)
        {
            double CapacityPayment = 0;

            SqlConnection HourCon = ConnectionManager.GetInstance().Connection;
            DataSet HourDS = new DataSet();
            SqlDataAdapter Hourda = new SqlDataAdapter();
            Hourda.SelectCommand = new SqlCommand("SELECT H1,H2,H3,H4,H5,H6,H7,H8,H9,H10,H11,H12,H13," +
            "H14,H15,H16,H17,H18,H19,H20,H21,H22,H23,H24 FROM [HCPF] WHERE Date=@date", HourCon);
            Hourda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
            Hourda.SelectCommand.Parameters["@date"].Value = mydate;
            Hourda.Fill(HourDS);
            double TempCap = 0;
            foreach (DataRow HRow in HourDS.Tables[0].Rows)
            {
                for (int c = 0; c < 24; c++)
                    TempCap += double.Parse(HRow[c].ToString()) * Dis[c];
            }

            // DETECT The Week Of the Selected day!
            int year = int.Parse(mydate.Remove(4));
            int day = int.Parse(mydate.Substring(8, 2));
            int month = int.Parse(mydate.Substring(5, 2));
            if (month < 7) day = day + ((month - 1) * 31);
            else day = 186 + day + ((month - 7) * 30);
            int week = day / 7;
            if (((day % 7) != 0) && (week < 52)) week++;

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            MyCom.CommandText = "SELECT @value=Value FROM [WCPF] WHERE Week=@week AND Year=@year " +
            "SELECT @mycap=CapacityPayment FROM [BaseData] WHERE Date=@udate";
            MyCom.Parameters.Add("@year", SqlDbType.Int);
            MyCom.Parameters["@year"].Value = year;
            MyCom.Parameters.Add("@week", SqlDbType.Int);
            MyCom.Parameters["@week"].Value = week;
            MyCom.Parameters.Add("@udate", SqlDbType.Char, 10);
            MyCom.Parameters["@udate"].Value = UpdateDate;
            MyCom.Parameters.Add("@value", SqlDbType.Real);
            MyCom.Parameters["@value"].Direction = ParameterDirection.Output;
            MyCom.Parameters.Add("@mycap", SqlDbType.Real);
            MyCom.Parameters["@mycap"].Direction = ParameterDirection.Output;
            //try
            //{
            MyCom.ExecuteNonQuery();
            //}
            //catch (Exception e)
            //{
            //string message = e.Message;
            //}
            double myweek;
            try { myweek = double.Parse(MyCom.Parameters["@value"].Value.ToString()); }
            catch { myweek = 0; }
            double BaseCap;
            try { BaseCap = double.Parse(MyCom.Parameters["@mycap"].Value.ToString()); }
            catch { BaseCap = 0; }
            CapacityPayment = myweek * BaseCap * TempCap;
            return (CapacityPayment);
            
        }
        //----------------------------------------SetCostPayment-----------------------------
        private double SetCostPayment(int k, int index, double[] P, double[] Dis)
        {
            double CostPayment = 0;
            int[] V = new int[24];
            int[] S = new int[24];
            double[] F1 = new double[24];
            double[] F2 = new double[24];
            double[] F3 = new double[24];
            double[] F5 = new double[24];
            double[] W = new double[24];
            double[] U = new double[24];
            double F4 = 0;

            string unit = TempGV.Rows[index].Cells[0].Value.ToString().Trim();
            string package = TempGV.Rows[index].Cells[1].Value.ToString().Trim();
            int Pcode = int.Parse(TempGV.Rows[index].Cells[2].Value.ToString());

            bool SF = isSecondFuel(k, index);

            for (int i = 0; i < 24; i++) if (P[i] > 0) V[i] = 1; else V[i] = 0;
            for (int i = 0; i < 23; i++) S[i] = V[i] - V[i + 1];

            //# of Required are less than zero
            int Minus = 0;
            for (int i = 0; i < 23; i++) if (S[i] < 0) Minus++;

            //IF selected unit is CombinedCycle
            if (package.Contains("CC"))
            {
                int GasIndex = index;
                int SteamIndex = index;
                foreach (DataGridViewRow DR in TempGV.Rows)
                {
                    if (DR.Index < (TempGV.RowCount - 1))
                        if ((int.Parse(DR.Cells[2].Value.ToString()) == Pcode) && (DR.Cells[1].Value.ToString().Contains("CC")))
                        {
                            if ((!unit.Contains("Gas")) && (DR.Cells[0].Value.ToString().Contains("Gas"))) GasIndex = DR.Index;
                            if (DR.Cells[0].Value.ToString().Contains("Steam")) SteamIndex = DR.Index;
                        }
                }
                //SET U & W
                double capacity = double.Parse(TempGV.Rows[GasIndex].Cells[3].Value.ToString());
                for (int z = 0; z < 24; z++)
                {
                    if (Dis[z] == 0) { W[z] = 0; U[z] = 0; }
                    else if (Dis[z] <= ((1.5) * capacity)) { W[z] = 0; U[z] = 1; }
                    else if (Dis[z] <= (3 * capacity)) { W[z] = 1; U[z] = 1; }
                }
                //SET F1 ,F2,F3,F4,F5
                for (int j = 0; j < 24; j++)
                {
                    double x19 = (double)1 / (double)9;
                    double x13 = (double)1 / (double)3;
                    if (SF)
                        F1[j] = (double.Parse(TempGV.Rows[GasIndex].Cells[11].Value.ToString()) * x19 * P[j] * P[j] + x13 * double.Parse(TempGV.Rows[GasIndex].Cells[12].Value.ToString()) * P[j] + double.Parse(TempGV.Rows[GasIndex].Cells[13].Value.ToString())) * (U[j] + W[j]);
                    else
                        F1[j] = (double.Parse(TempGV.Rows[GasIndex].Cells[8].Value.ToString()) * x19 * P[j] * P[j] + x13 * double.Parse(TempGV.Rows[GasIndex].Cells[9].Value.ToString()) * P[j] + double.Parse(TempGV.Rows[GasIndex].Cells[10].Value.ToString())) * (U[j] + W[j]);
                    F2[j] = (double.Parse(TempGV.Rows[SteamIndex].Cells[5].Value.ToString()) * ((U[j] + W[j]) / (double)2)) + (double.Parse(TempGV.Rows[GasIndex].Cells[5].Value.ToString()) * (U[j] + W[j]));
                    F3[j] = double.Parse(TempGV.Rows[SteamIndex].Cells[4].Value.ToString()) + 2 * double.Parse(TempGV.Rows[GasIndex].Cells[4].Value.ToString());
                    if (V[j] == 0) F5[j] = 0;
                    else
                        F5[j] = (double.Parse(TempGV.Rows[SteamIndex].Cells[6].Value.ToString()) * P[j]) + (double.Parse(TempGV.Rows[GasIndex].Cells[7].Value.ToString()) * (U[j] + W[j]));
                }
                if (Minus > 1)
                {
                    try { F4 = Minus * double.Parse(TempGV.Rows[GasIndex].Cells[15].Value.ToString()); }
                    catch { F4 = 0; }
                }
                else if (Minus == 1)
                {
                    try { F4 = double.Parse(TempGV.Rows[GasIndex].Cells[14].Value.ToString()) + (double.Parse(TempGV.Rows[SteamIndex].Cells[15].Value.ToString()) / (double)2); }
                    catch { F4 = 0; }
                }
                else F4 = 0;

            }
            //IF selected unit IsNOT CombinedCycle
            else
            {
                for (int j = 0; j < 24; j++)
                {
                    if (SF)
                        F1[j] = double.Parse(TempGV.Rows[index].Cells[11].Value.ToString()) * P[j] * P[j] + double.Parse(TempGV.Rows[index].Cells[12].Value.ToString()) * P[j] + double.Parse(TempGV.Rows[index].Cells[13].Value.ToString()) * V[j];
                    else
                        F1[j] = double.Parse(TempGV.Rows[index].Cells[8].Value.ToString()) * P[j] * P[j] + double.Parse(TempGV.Rows[index].Cells[9].Value.ToString()) * P[j] + double.Parse(TempGV.Rows[index].Cells[10].Value.ToString()) * V[j];
                    F2[j] = double.Parse(TempGV.Rows[index].Cells[5].Value.ToString()) * V[j];
                    F3[j] = double.Parse(TempGV.Rows[index].Cells[4].Value.ToString());
                    if (V[j] == 0) F5[j] = 0;
                    else
                    {
                        if (package.Contains("Steam")) F5[j] = double.Parse(TempGV.Rows[index].Cells[6].Value.ToString()) * P[j];
                        else F5[j] = double.Parse(TempGV.Rows[index].Cells[7].Value.ToString()) * V[j];
                    }
                }
                if (Minus > 1)
                {
                    try { F4 = Minus * double.Parse(TempGV.Rows[index].Cells[15].Value.ToString()); }
                    catch { F4 = 0; }
                }
                else if (Minus == 1)
                {
                    try { F4 = double.Parse(TempGV.Rows[index].Cells[14].Value.ToString()); }
                    catch { F4 = 0; }
                }
                else F4 = 0;
            }
            CostPayment = 0;
            for (int z = 0; z < 24; z++)
                CostPayment = CostPayment + F1[z] + F2[z] + F3[z] + F5[z];
            CostPayment = CostPayment + F4;
            return (CostPayment);
        }
        //---------------------------------isSecondFuel-----------------------
        private bool isSecondFuel(int k, int index)
        {
            string unit = TempGV.Rows[index].Cells[0].Value.ToString().Trim();
            string package = TempGV.Rows[index].Cells[1].Value.ToString().Trim();
            int Pcode = int.Parse(TempGV.Rows[index].Cells[2].Value.ToString());

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            MyCom.CommandText = "SELECT @SF=SecondFuel FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@unit " +
            "AND PackageType=@type ";
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPIDArray[k];
            MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
            MyCom.Parameters["@unit"].Value = unit;
            MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
            string type = package;
            if (type.Contains("CC")) type = "CC";
            MyCom.Parameters["@type"].Value = type;
            MyCom.Parameters.Add("@SF", SqlDbType.Bit);
            MyCom.Parameters["@SF"].Direction = ParameterDirection.Output;
            //try
            //{
            MyCom.ExecuteNonQuery();
            //}
            //catch (Exception e)
            //{
            //string message = e.Message;
            //}
            bool SF;
            try { SF = bool.Parse(MyCom.Parameters["@SF"].Value.ToString()); }
            catch { SF = false; }
            return (SF);

        }
        //---------------------------------------SetAvailableCap----------------------------
        private double SetAvailableCap(int k, string mydate, int index, string temp)
        {
            double AvailableCapacity = 0;
            string unit = TempGV.Rows[index].Cells[0].Value.ToString().Trim();
            string package = TempGV.Rows[index].Cells[1].Value.ToString().Trim();
            int Pcode = int.Parse(TempGV.Rows[index].Cells[2].Value.ToString());

            SqlCommand MyCom = new SqlCommand();
            MyCom.CommandText = "SELECT  @Dispatch =SUM(Dispatchable) FROM [DetailFRM005] WHERE TargetMarketDate=@date " +
            "AND PPID=@num AND Block=@block";
            MyCom.Connection = MyConnection;

            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = mydate;
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPIDArray[k];
            MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
            MyCom.Parameters["@block"].Value = temp;
            MyCom.Parameters.Add("@Dispatch", SqlDbType.Real);
            MyCom.Parameters["@Dispatch"].Direction = ParameterDirection.Output;
            //try
            //{
            MyCom.ExecuteNonQuery();
            //}
            //catch (Exception e)
            //{
            //string message = e.Message;
            //}
            try { AvailableCapacity = double.Parse(MyCom.Parameters["@Dispatch"].Value.ToString()); }
            catch { AvailableCapacity = 0; }
            return (AvailableCapacity);
        }
        //------------------------------------SetTotalPower--------------------------------
        private double SetTotalPower(int k, string mydate, int index, string temp)
        {
            double TotalPower = 0;
            string unit = TempGV.Rows[index].Cells[0].Value.ToString().Trim();
            string package = TempGV.Rows[index].Cells[1].Value.ToString().Trim();
            int Pcode = int.Parse(TempGV.Rows[index].Cells[2].Value.ToString());

            SqlCommand MyCom = new SqlCommand();
            MyCom.CommandText = "SELECT @Required=SUM(Required) FROM [DetailFRM005] WHERE TargetMarketDate=@date " +
            "AND PPID=@num AND Block=@block";
            MyCom.Connection = MyConnection;

            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = mydate;
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPIDArray[k];
            MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
            MyCom.Parameters["@block"].Value = temp;
            MyCom.Parameters.Add("@Required", SqlDbType.Real);
            MyCom.Parameters["@Required"].Direction = ParameterDirection.Output;
            //try
            //{
            MyCom.ExecuteNonQuery();
            //}
            //catch (Exception e)
            //{
            //string message = e.Message;
            //}
            try { TotalPower = double.Parse(MyCom.Parameters["@Required"].Value.ToString()); }
            catch { TotalPower = 0; }
            if ((int.Parse(PPIDArray[k]) == 131) || (int.Parse(PPIDArray[k]) == 144))
            {
                if (package.Contains("CC")) CCUnitPower += TotalPower;
                else NoCCUnitPower += TotalPower;
            }
            else 
                NoCCUnitPower += TotalPower;
            return (TotalPower);
        }
        //--------------------------------------SetULPower------------------------------------
        private double SetULPower(int k, string mydate, int index, string temp)
        {
            double ULPower = 0;
            string unit = TempGV.Rows[index].Cells[0].Value.ToString().Trim();
            string package = TempGV.Rows[index].Cells[1].Value.ToString().Trim();
            int Pcode = int.Parse(TempGV.Rows[index].Cells[2].Value.ToString());

            SqlCommand MyCom = new SqlCommand();
            MyCom.CommandText = "SELECT  @UL =SUM(Required) FROM [DetailFRM005] WHERE TargetMarketDate=@date " +
            "AND PPID=@num AND Block=@block AND Contribution=@cont";
            MyCom.Connection = MyConnection;

            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = mydate;
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPIDArray[k];
            MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
            MyCom.Parameters["@block"].Value = temp;
            MyCom.Parameters.Add("@cont", SqlDbType.Char, 2);
            MyCom.Parameters["@cont"].Value = "UL";
            MyCom.Parameters.Add("@UL", SqlDbType.Real);
            MyCom.Parameters["@UL"].Direction = ParameterDirection.Output;
            //try
            //{
            MyCom.ExecuteNonQuery();
            //}
            //catch (Exception e)
            //{
            //string message = e.Message;
            //}
            try { ULPower = double.Parse(MyCom.Parameters["@UL"].Value.ToString()); }
            catch { ULPower = 0; }

            return (ULPower);
        }
        //---------------------------------------------autoPathToolStripMenuItem_Click---------------------------------------
        private void autoPathToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form7 Path = new Form7();
            Path.ShowDialog();

        }
        //-------------------------------ReseTVariables------------------------------------
        private void ResetVariables()
        {
            PAvailableCapacity = 0;
            PTotalPower = 0;
            PULPower = 0;
            PBidPower = 0;
            PIncrementPower = 0;
            PDecreasePower = 0;
            PCapacityPayment = 0;
            PBidPayment = 0;
            PULPayment = 0;
            PIncrementPayment = 0;
            PDecreasePayment = 0;
            PEnergyPayment = 0;
            PIncome = 0;
            PCost = 0;
            PBenefit = 0;
            CCTotalPower = 0;
            NoCCTotalPower = 0;
            CCIncPower = 0;
            NoCCIncPower = 0;
            CCDecPower = 0;
            NoCCDecPower = 0;
            CCUnitPower = 0;
            NoCCUnitPower = 0;
        }
        //-------------------------------FRUnitCal_ValueChanged--------------------------------
        private void FRUnitCal_ValueChanged(object sender, EventArgs e)
        {
            string unit = FRUnitLb.Text.Trim();
            string package = FRPackLb.Text.Trim();
            int index = 0;
            if (GDSteamGroup.Text.Contains(package))
                for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                {
                    if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                        index = i;
                }
            else
                for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                {
                    if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                        index = i;
                }
            FillFRUnitRevenue(unit, package, index);
        }


        private void FRUnitAmainTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitAmainTb.Text, 1))
                errorProvider1.SetError(FRUnitAmainTb, "");
            else errorProvider1.SetError(FRUnitAmainTb, "just real number!");
        }

        private void FRUnitBmainTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitBmainTb.Text, 1))
                errorProvider1.SetError(FRUnitBmainTb, "");
            else errorProvider1.SetError(FRUnitBmainTb, "just real number!");

        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //private double[] Get40Day_Prices_old(DateTime baseDate, DateTime dateTime)
        //{
        //    int maxAttemp = 3;
        //    string package = MRPackLb.Text.Trim();
        //    string unit = MRUnitLb.Text.Trim();


        //    int maxCount = 40 * 24;
        //    double[] prices = new double[maxCount];

        //    if (dateTime != null)
        //    {

        //        for (int daysbefore = 0; daysbefore < 40; daysbefore++)
        //        {
        //            DateTime selectedDate = PersianDateConverter.ToGregorianDateTime(dateTime).Subtract(new TimeSpan(daysbefore, 0, 0, 0));
        //            string date = new PersianDate(selectedDate).ToString("d");

        //            //CMatrix oneDayPrice = GetOneDayPrices(date);
        //            CMatrix oneDayPrice = CalculateMaxBid(selectedDate, PPID);

        //            // in case price is null we iterate three times;
        //            int attemp = 0;
        //            while (oneDayPrice == null && selectedDate <= baseDate && attemp < maxAttemp)
        //            {
        //                attemp++;
        //                selectedDate = PersianDateConverter.ToGregorianDateTime(dateTime).Subtract(new TimeSpan(daysbefore + attemp, 0, 0, 0));
        //                date = new PersianDate(selectedDate).ToString("d");
        //                oneDayPrice = CalculateMaxBid(selectedDate, PPID);
        //            }

        //            if (oneDayPrice != null)
        //            {
        //                for (int hour = 0; hour < 24; hour++)
        //                    prices[maxCount - (24 * daysbefore) - hour - 1] = oneDayPrice[0, hour];
        //            }
        //            else if (selectedDate > baseDate)
        //            // if price doesn't exist
        //            {
        //                // can read from forcast

        //                DataTable forcast = GetForcast(PPID.ToString(), unit, date);
        //                if (forcast.Rows.Count > 0)
        //                {
        //                    foreach (DataRow row in forcast.Rows)
        //                    {
        //                        double hour = double.Parse(row["hour"].ToString());
        //                        double value = double.Parse(row["forecast"].ToString());
        //                        int i = (int)(maxCount - (24 * daysbefore) - hour - 1);
        //                        prices[i] = value;
        //                    }
        //                }
        //                else
        //                {
        //                    //   throw Exception;
        //                }

        //            }
        //            else
        //            {
        //                //   throw Exception;
        //            }

        //            //else MessageBox.Show("No Data for selected Date!");
        //        }// end loop for days
        //    }
        //    //for (int l=0; l<maxCount;l++)
        //    //    prices[l]=456;
        //    return prices;

        //}

        private double[] Get40Day_PowerPlantPrices(int ppId, DataSet dsUnits, DateTime baseDate, DateTime dateTime, MixUnits mixUnitStatus)
        {
            int maxAttemp = 3;
            int nday = 40;
            if (txtTraining.Text != "")
            {
                nday = int.Parse(txtTraining.Text.Trim());
            }
            int maxCount = nday * 24;
            double[] prices = new double[maxCount];

            for (int daysbefore = 0; daysbefore < nday; daysbefore++)
            {
                DateTime selectedDate = PersianDateConverter.ToGregorianDateTime(dateTime).Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                string date = new PersianDate(selectedDate).ToString("d");

                //CMatrix oneDayPrice = GetOneDayPrices(date);
                CMatrix oneDayPrice = CalculatePowerPlantMaxBid(ppId, dsUnits, selectedDate, mixUnitStatus);

                // in case price is null we iterate three times;
                int attemp = 0;
                while ((oneDayPrice == null || oneDayPrice.Zero)
                        && selectedDate <= baseDate && attemp < maxAttemp)
                {
                    attemp++;
                    selectedDate = PersianDateConverter.ToGregorianDateTime(dateTime).Subtract(new TimeSpan(daysbefore + attemp, 0, 0, 0));
                    date = new PersianDate(selectedDate).ToString("d");
                    oneDayPrice = CalculatePowerPlantMaxBid(ppId, dsUnits, selectedDate, mixUnitStatus);
                }
                if ((oneDayPrice == null || oneDayPrice.Zero) && selectedDate > baseDate)
                {
                    // can read from forcast
                    DataTable forcast = GetForcast(ppId, mixUnitStatus, date);
                    if (forcast.Rows.Count > 0)
                    {
                        oneDayPrice = new CMatrix(1, 24);
                        foreach (DataRow row in forcast.Rows)
                        {
                            int hour = Int32.Parse(row["hour"].ToString());
                            double value = double.Parse(row["forecast"].ToString());
                            oneDayPrice[0, hour - 1] = value;
                        }
                    }
                }
                if (oneDayPrice != null)
                {
                    for (int hour = 0; hour < 24; hour++)
                        prices[maxCount - (24 * daysbefore) - 24 + hour] = oneDayPrice[0, hour];
                }
                else
                {
                    MessageBox.Show("No Data for selected Date!");

                }

            }// end loop for days

            //for (int l=0; l<maxCount;l++)
            //    prices[l]=456;
            return prices;

        }

        private CMatrix GetOneDayPricesForEachUnit(int ppid, string date, string blockM005, string blockM002)
        {
            blockM005 = blockM005.Trim();
            blockM002 = blockM002.Trim();

            //DataTable oDataTable = utilities.returntbl("select max(PMax) from UnitsDataMain where ppid='" + ppid.ToString() + "'");
            //double pMaxUnitDataMain = 0;
            //try
            //{
            //    pMaxUnitDataMain = double.Parse(oDataTable.Rows[0][0].ToString());
            //}
            //catch
            //{
            //}

            CMatrix price = new CMatrix(1, 24);

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();

            Myda.SelectCommand = new SqlCommand("SELECT Hour,Required FROM [DetailFRM005] WHERE PPID= '" + ppid.ToString() + "' AND TargetMarketDate=@date AND Block=@block", MyConnection);
            Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
            Myda.SelectCommand.Parameters["@date"].Value = date;
            Myda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
            Myda.SelectCommand.Parameters["@block"].Value = blockM005;
            Myda.Fill(MyDS);
            DataTable dt = MyDS.Tables[0];

            double[] requiredTable = new double[24];

            // if price exist
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow MyRow in dt.Rows)
                {
                    int x = int.Parse(MyRow["Hour"].ToString());
                    requiredTable[x - 1] = double.Parse(MyRow["Required"].ToString());
                }

                /////////// calculate price
                double[] maxBid = new double[24];
                //Detect MaxBid field in MRCurGrid1



                for (int index = 0; index < 24; index++)
                {

                    DataSet MaxDS = new DataSet();
                    SqlDataAdapter Maxda = new SqlDataAdapter();
                    string strCmd = "SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                        "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10,DispachableCapacity FROM [DetailFRM002] " +
                        "WHERE TargetMarketDate=@date AND Block=@block AND Hour=@hour AND PPID='" + ppid.ToString() + "'" +
                        " and ( Estimated is null or Estimated = 0)"; 
                    Maxda.SelectCommand = new SqlCommand(strCmd, MyConnection);
                    Maxda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                    Maxda.SelectCommand.Parameters["@date"].Value = date;
                    Maxda.SelectCommand.Parameters.Add("@hour", SqlDbType.SmallInt);
                    Maxda.SelectCommand.Parameters["@hour"].Value = index + 1;
                    Maxda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
                    Maxda.SelectCommand.Parameters["@block"].Value = blockM002;

                    Maxda.Fill(MaxDS);

                    double required = requiredTable[index];
                    double DispachableCapacity = 0;
                    try
                    {
                        DispachableCapacity = double.Parse(MaxDS.Tables[0].Rows[0]["DispachableCapacity"].ToString());
                    }
                    catch { }
                    /////~~~~~~~~~~~~~~~~ BEGIN  FOR PARAND!!!!!!!!!!!!!    ~~~~~~~~~~~~~~~~~~~~~~~~~~
                    if (DispachableCapacity < required && DispachableCapacity > 0)
                    {
                        maxBid[index] = 0;
                        string str = "select price1,price2,price3,price4,price5,price6,price7,price8,price9,price10" +
                            " from dbo.DetailFRM002 where ppid='" + ppid.ToString() + "'" +
                            " and TargetMarketDate='" + date + "'" +
                            " AND Hour='" + (index+1).ToString() + "'" +
                            " AND block='" + blockM002 + "'";

                        DataTable oDataTable = utilities.returntbl(str);
                        double max = 0;
                        if (oDataTable.Rows.Count > 0)
                            for (int i = 0; i < 10; i++)
                            {
                                if (double.Parse(oDataTable.Rows[0][i].ToString()) > max)
                                    max = double.Parse(oDataTable.Rows[0][i].ToString());
                            }

                            maxBid[index] = max;
                    }
                    /////~~~~~~~~~~~~~~~~ END  FOR PARAND!!!!!!!!!!!!!    ~~~~~~~~~~~~~~~~~~~~~~~~~~

                    else
                    {
                        foreach (DataRow MaxRow in MaxDS.Tables[0].Rows)
                        {
                            int Dsindex = 0;
                            while ((Dsindex < 20) && (Math.Round( double.Parse(MaxRow[Dsindex].ToString()),1) < required))
                                Dsindex = Dsindex + 2;
                            if (Dsindex < 20)
                                maxBid[index] = double.Parse(MaxRow[Dsindex + 1].ToString());
                            else
                                maxBid[index] = 0;

                        }
                    }

                    price[0, index] = maxBid[index];

                }

            }
            else
                price = null;

            return price;

        }

        private DataTable GetForcast(int ppId, MixUnits mixUnitStatus, string date)
        {

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText = "select FinalForecastHourly.hour, FinalForecastHourly.forecast" +
                    " from FinalForecastHourly inner join FinalForecast" +
                    " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id" +
                    " where FinalForecast.PPId=@ppID AND FinalForecast.MixUnitStatus=@mixUnitStatus AND FinalForecast.date=@date" +
                    " order by FinalForecastHourly.hour";

            MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
            MyCom.Parameters["@ppID"].Value = ppId.ToString();
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = date;
            MyCom.Parameters.Add("@mixUnitStatus", SqlDbType.Char, 20);
            MyCom.Parameters["@mixUnitStatus"].Value = mixUnitStatus.ToString();
            Maxda.Fill(MaxDS);

            return MaxDS.Tables[0];
        }

        public bool CalculateBidding(DateTime baseDate, DateTime fututeDate, int ppId, string ppname)
        {
            DataSet myDs = GetUnits(ppId);
            MixUnits mixUnitsStatus = (ppId == 131 || ppId == 144 ? MixUnits.JustCombined : MixUnits.All);

            int forcastDaysAhead = (new DateTime(fututeDate.Year, fututeDate.Month, fututeDate.Day, 0, 0, 0) - new DateTime(baseDate.Year, baseDate.Month, baseDate.Day, 0, 0, 0)).Days;
            progressFrm.progressBar1.Minimum = 0;
            progressFrm.progressBar1.Maximum = 24 + 2;
            //progressFrm.Description = ppname + " Price Forecasting is in Progress...";

            /** !!!!!!!!!!! log */
            DateTime logTimebase = DateTime.Now;
            Core_pf_Dll.Core_pf_Class oCore_pf = new Core_pf_Class();
            DateTime logTimeCur = DateTime.Now;
            Log("Core_pf_Dll instance", logTimeCur - logTimebase);
            /** !!!!!!!!!!!!!!! */

            do
            {
                int nday = 40;
                if (txtTraining.Text != "")
                {
                    nday = int.Parse(txtTraining.Text.Trim());
                }
                int loc_n = 24 * nday + 1 /*   */;

                for (int k = 1; k <= forcastDaysAhead; k++)
                {

                    Log("POWER PLANT " + PPID.ToString() + ", DAY " + k.ToString());
                    DateTime curDate = baseDate.AddDays(k);

                    progressFrm.progressBar1.Value = 1;

                    /** !!!!!!!!!!! log */
                    logTimebase = DateTime.Now;
                    double[] price = Get40Day_PowerPlantPrices(ppId, myDs, baseDate, curDate, mixUnitsStatus);
                    logTimeCur = DateTime.Now;
                    Log("Get40Day PowerPlantPrices", logTimeCur - logTimebase);
                    /** !!!!!!!!!!!!!!! */
                    bool noData = true;

                    for (int i = 0; i < 24 * nday && noData; i++)
                        if (price[i] != 0)
                            noData = false;

                    if (noData)
                    {
                        string str = "Due to lack of Data, Price Forecasting is not possible for " + ppname + " righ now." +
                            "\r\nYou need to load M002 Data for this plant...";

                        MessageBox.Show(str);
                        return false;
                    }

                    int Test = 0;
                    int[] Error_price = new int[24 * nday];
                    for (int x = 72; x < 24 * nday; x++)
                    {
                        Test = 0;
                        if ((price[x] == 0))
                        {
                            if (price[x - 24] != 0)
                            {
                                price[x] = price[x - 24];
                                Test = 1;
                            }
                            else if (x - 48 >= 0 && (price[x - 48] != 0) && (Test == 0))
                            {
                                price[x] = price[x - 48];
                                Test = 1;
                            }
                            else if (x - 72 >= 0 && (price[x - 72] != 0) && (Test == 0))
                            {
                                price[x] = price[x - 72];
                                Test = 1;
                            }
                            if ((price[x] == 0))
                            {
                                Error_price[x] = 1;
                            }
                        }
                    }

                    /** !!!!!!!!!!! log */
                    logTimebase = DateTime.Now;
                    Application.DoEvents();
                    CMatrix LC_H_N = Get40Day_LC_HValue(curDate);
                    logTimeCur = DateTime.Now;
                    Log("Get40Day Line Interchange Value", logTimeCur - logTimebase);
                    /** !!!!!!!!!!!!!!! */
                    progressFrm.progressBar1.Value = 2;
                    ///**************************************************/


                    double[] final_Forecast = new double[24];
                    double[] vr = new double[24];

                    CMatrix ff = new CMatrix(0, 201); // a new row gets added in each j loop
                    CMatrix pdist = new CMatrix(0, 201); // a new row gets added in each j loop

                    int[,] Co = new int[2, nday - 7];
                    for (int i = 0; i <= nday-8; i++)
                    {
                        Co[0, i] = 0;
                        Co[1, i] = 1;
                    }
                    Co[0, 1] = 2;
                    Co[1, 1] = 2;

                    double[,] Ee = new double[1, nday - 7];
                    for (int i = 0; i <= nday-8; i++)
                    {
                        Ee[0, i] = 0;
                    }
                    Ee[0, 2] = 0.00000001;

                    Application.DoEvents();

                    for (int j = 1; j <= 24; j++)
                    {
                        CMatrix P = new CMatrix(43, nday - 4);
                        CMatrix T = new CMatrix(1, nday - 5);

                        /** !!!!!!!!!!! log */
                        logTimebase = DateTime.Now;
                        /** !!!!!!!!!!!!!!! */

                        for (int i = 1; i <= nday - 8; i++)
                        {
                            int e = 24;

                            int temp1 = loc_n - (24 * i) + j;
                            int temp2 = loc_n - (24 * i) - e + j;

                            P[0, i - 1] = (price[temp1 - 2] - price[temp2 - 2] + Ee[0, i]) / price[temp2 - 2];
                            P[1, i - 1] = (price[temp1 - 2] - price[temp2 - 3] + Ee[0, i]) / price[temp2 - 3];
                            P[2, i - 1] = (price[temp1 - 2] - price[temp2 - 1] + Ee[0, i]) / price[temp2 - 1];
                            P[3, i - 1] = (price[temp1 - 2] - price[temp2 - 4] + Ee[0, i]) / price[temp2 - 4];
                            P[4, i - 1] = (price[temp1 - 2] - price[temp2] + Ee[0, i]) / price[temp2];

                            P[5, i - 1] = (price[temp1 - 3] - price[temp2 - 3] + Ee[0, i]) / price[temp2 - 3];
                            P[6, i - 1] = (price[temp1 - Co[1, i]] - price[temp2 - 1] + Ee[0, i]) / price[temp2 - 1];
                            P[7, i - 1] = (price[temp1 - 4] - price[temp2 - 4] + Ee[0, i]) / price[temp2 - 4];
                            P[8, i - 1] = (price[temp1 - Co[0, i]] - price[temp2] + Ee[0, i]) / price[temp2];

                            e = 48;
                            temp2 = loc_n - 24 * i - e + j;
                            P[9, i - 1] = (price[temp1 - 2] - price[temp2 - 2] + Ee[0, i]) / price[temp2 - 2];
                            P[10, i - 1] = (price[temp1 - 2] - price[temp2 - 3] + Ee[0, i]) / price[temp2 - 3];
                            P[11, i - 1] = (price[temp1 - 2] - price[temp2 - 1] + Ee[0, i]) / price[temp2 - 1];
                            P[12, i - 1] = (price[temp1 - 2] - price[temp2 - 4] + Ee[0, i]) / price[temp2 - 4];
                            P[13, i - 1] = (price[temp1 - 2] - price[temp2] + Ee[0, i]) / price[temp2];

                            P[14, i - 1] = (price[temp1 - 3] - price[temp2 - 3] + Ee[0, i]) / price[temp2 - 3];
                            P[15, i - 1] = (price[temp1 - Co[1, i]] - price[temp2 - 1] + Ee[0, i]) / price[temp2 - 1];
                            P[16, i - 1] = (price[temp1 - 4] - price[temp2 - 4] + Ee[0, i]) / price[temp2 - 4];
                            P[17, i - 1] = (price[temp1 - Co[0, i]] - price[temp2] + Ee[0, i]) / price[temp2];

                            e = 168;
                            temp2 = loc_n - 24 * i - e + j;
                            P[18, i - 1] = (price[temp1 - 2] - price[temp2 - 2] + Ee[0, i]) / price[temp2 - 2];
                            P[19, i - 1] = (price[temp1 - 2] - price[temp2 - 3] + Ee[0, i]) / price[temp2 - 3];
                            P[20, i - 1] = (price[temp1 - 2] - price[temp2 - 1] + Ee[0, i]) / price[temp2 - 1];
                            P[21, i - 1] = (price[temp1 - 2] - price[temp2 - 4] + Ee[0, i]) / price[temp2 - 4];
                            P[22, i - 1] = (price[temp1 - 2] - price[temp2] + Ee[0, i]) / price[temp2];

                            P[23, i - 1] = (price[temp1 - 3] - price[temp2 - 3] + Ee[0, i]) / price[temp2 - 3];
                            P[24, i - 1] = (price[temp1 - Co[1, i]] - price[temp2 - 1] + Ee[0, i]) / price[temp2 - 1];
                            P[25, i - 1] = (price[temp1 - 4] - price[temp2 - 4] + Ee[0, i]) / price[temp2 - 4];
                            P[26, i - 1] = (price[temp1 - Co[0, i]] - price[temp2] + Ee[0, i]) / price[temp2];

                            //P[27, i-1] = A(temp1 - 1, 4);
                            string strDayOfWeek =
                                curDate.Subtract(new TimeSpan(temp1 - 1, 0, 0)).
                                    DayOfWeek.ToString();

                            int dayIndex;
                            for (dayIndex = 1; dayIndex <= 7; dayIndex++)
                            {
                                Days curDay = (Days)Enum.Parse(typeof(Days), dayIndex.ToString());
                                if (curDay.ToString() == strDayOfWeek)
                                    break;
                            }
                            P[27, i - 1] = dayIndex;

                            for (int b = 0; b < 15; b++)
                            {
                                try
                                {
                                    P[b + 28, i - 1] = LC_H_N[b, temp1 - 2] + Ee[0, i];
                                }
                                catch (Exception)
                                {
                                    throw;
                                }
                            }

                            if (i - 2 >= 0)
                                T[0, i - 2] = ((price[temp1 + 24 - 2] - price[temp1 - 2]) + Ee[0, i])
                                                / price[temp1 - 2];
                        }

                        /** !!!!!!!!!!! log */
                        logTimeCur = DateTime.Now;
                        Log("40 loop for each hour", logTimeCur - logTimebase);
                        /** !!!!!!!!!!!!!!! */

                        CMatrix XInput = P.GetColumn(0);
                        P = P.RemoveAtColumn(0);

                        ///////////
                        progressFrm.progressBar1.Value = j + 2;
                        System.Threading.Thread.CurrentThread.Join(0);
                        Thread.Sleep(1);

                        //if (!P.Zero && !XInput.Zero && !T.Zero)
                        //{
                        /** !!!!!!!!!!! log */
                        logTimebase = DateTime.Now;
                        /** !!!!!!!!!!!!!!! */
                        Application.DoEvents();

                        MWArray[] argsOut = oCore_pf.core_pf(4,
                            (MWArray)DoubleArrayToMWNumericArray(P.GetDoubleMatrix()),
                            (MWArray)DoubleArrayToMWNumericArray(XInput.GetDoubleMatrix()),
                            (MWArray)DoubleArrayToMWNumericArray(T.GetDoubleMatrix()));

                        Application.DoEvents();

                        MWNumericArray temp = new MWNumericArray();
                        temp = (MWNumericArray)argsOut[0];
                        CMatrix q = new CMatrix(MWNumericArrayToDoubleArray(temp, 1, 1));

                        temp = (MWNumericArray)argsOut[1];
                        CMatrix xOut = new CMatrix(MWNumericArrayToDoubleArray(temp, 1, 201));

                        temp = (MWNumericArray)argsOut[2];
                        CMatrix pdist_n = new CMatrix(MWNumericArrayToDoubleArray(temp, 400, 201));

                        temp = (MWNumericArray)argsOut[3];
                        CMatrix s = new CMatrix(MWNumericArrayToDoubleArray(temp, 1, 1));

                        /** !!!!!!!!!!! log */
                        logTimeCur = DateTime.Now;
                        Log("Call DLL", logTimeCur - logTimebase);
                        /** !!!!!!!!!!!!!!! */

                        Application.DoEvents();
                        double gainNum = price[loc_n - 24 + j - 2];

                        final_Forecast[j - 1] = gainNum * (1 + q[0, 0]); ///  ????????? 

                        CMatrix gainMatrix = new CMatrix(1, 201);
                        for (int colj = 0; colj < 201; colj++)
                            gainMatrix[0, colj] = gainNum;

                        ff = ff.AddRow((gainNum * xOut) + gainMatrix);
                        CMatrix PdistLarge = pdist_n / gainNum;
                        vr[j - 1] = gainNum * s[0, 0] / 20;
                        pdist = pdist.AddRow(PdistLarge.GetColumnMean());
                        //}
                        //else
                        //{

                        //}

                    }
                    /** !!!!!!!!!!! log */
                    logTimebase = DateTime.Now;
                    SaveForcastResults(ppId, new PersianDate(curDate), final_Forecast, vr, ff, pdist, mixUnitsStatus);

                    logTimeCur = DateTime.Now;
                    Log("SaveForcastResults", logTimeCur - logTimebase);
                    /** !!!!!!!!!!!!!!! */

                    //loc_n += 24;

                    progressFrm.progressBar1.Value = progressFrm.progressBar1.Maximum;
                    Thread.Sleep(1);

                }
                if (mixUnitsStatus == MixUnits.JustCombined)
                    mixUnitsStatus = MixUnits.ExeptCombined;
                else
                    // just to break the loop
                    mixUnitsStatus = MixUnits.All;


            } while (mixUnitsStatus != MixUnits.All);
            return true;
        }

        //public static void Log(string txt, DateTime time)
        //{
        //    System.IO.StreamWriter sw = new System.IO.StreamWriter("Log.txt", true);
        //    sw.WriteLine("'" +time.ToString()  +"':  " + txt);
        //    sw.Close();
        //}

        public static void Log(string txt, TimeSpan time)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter("Log.txt", true);
            sw.WriteLine(time.ToString() + ":    " + txt);
            sw.Close();
        }

        public static void Log(string txt)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter("Log.txt", true);
            //sw.WriteLine("");
            //sw.WriteLine("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            sw.WriteLine(txt);
            sw.Close();
        }

        public void SaveForcastResults(int ppID, PersianDate date, double[] final_Forecast, double[] vr, CMatrix ff, CMatrix pdist, MixUnits mixunitStatus)
        {

            //////// DELETE

            string whereCmd = " where FinalForecast.PPId='" + ppID.ToString() +
                "' AND FinalForecast.date = '" + date.ToString("d") +
                "' and FinalForecast.MixUnitStatus='" + mixunitStatus.ToString() + "'";


            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;

            MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            MyCom.CommandText = "delete from dbo.FinalForecast201Item where fk_ffHourly in(" +
                        " select FinalForecastHourly.id from dbo.FinalForecastHourly" +
                        " inner join dbo.FinalForecast on FinalForecast.id = FinalForecastHourly.fk_FinalForecastId" +
                        whereCmd + ")";
            MyCom.ExecuteNonQuery();

            MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            MyCom.CommandText = "delete from FinalForecastHourly where fk_FinalForecastId in(" +
                " select id from dbo.FinalForecast" +
                    whereCmd + ")";
            MyCom.ExecuteNonQuery();

            MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            MyCom.CommandText = "delete from FinalForecast" +
                    whereCmd;
            MyCom.ExecuteNonQuery();

            /////////////   INSERT
            MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;

            // Insert into FinalForcast table
            MyCom.CommandText = "insert INTO [FinalForecast] (PPId,date,MixUnitStatus)"
                        + " VALUES (@ppid,@date,@mixUnitStatus)";
            MyCom.Parameters.Add("@ppid", SqlDbType.NChar, 10);
            MyCom.Parameters["@ppid"].Value = ppID.ToString();
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = date.ToString("d");
            MyCom.Parameters.Add("@mixUnitStatus", SqlDbType.Char, 20);
            MyCom.Parameters["@mixUnitStatus"].Value = mixunitStatus.ToString();
            MyCom.ExecuteNonQuery();

            // retrieve the newly inserted id in FinalForcast
            MyCom.CommandText = "select max(id) from [FinalForecast]";
            int FinalForcastId = (int)MyCom.ExecuteScalar();

            for (int j = 0; j < 24; j++)
            {
                // Insert into FinalForcastHourly table
                MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                MyCom.CommandText = "insert INTO [FinalForecastHourly] (fk_FinalForecastId,hour,forecast, vr)"
                            + " VALUES (@finalForcastId,@hour,@forecast, @vr)";

                MyCom.Parameters.Add("@finalForcastId", SqlDbType.Int);
                MyCom.Parameters["@finalForcastId"].Value = FinalForcastId;
                MyCom.Parameters.Add("@hour", SqlDbType.Int);
                MyCom.Parameters["@hour"].Value = j + 1;
                MyCom.Parameters.Add("@forecast", SqlDbType.Float);
                MyCom.Parameters["@forecast"].Value = final_Forecast[j];
                MyCom.Parameters.Add("@vr", SqlDbType.Float);
                MyCom.Parameters["@vr"].Value = vr[j];
                MyCom.ExecuteNonQuery();

                // retrieve the newly inserted id in FinalForcastHourly
                MyCom.CommandText = "select max(id) from [FinalForecastHourly]";
                int FinalForcastHourlyId = (int)MyCom.ExecuteScalar();


                for (int k = 0; k < 201; k++)
                {
                    // Insert into FinalForecast201Item table
                    MyCom = new SqlCommand();
                    MyCom.CommandText = "insert INTO [FinalForecast201Item] (fk_ffHourly,[index],pdist, ff)"
                                + " VALUES (@fk_ffHourly,@index,@pdist, @ff)";
                    MyCom.Connection = MyConnection;
                    MyCom.Parameters.Add("@fk_ffHourly", SqlDbType.Int);
                    MyCom.Parameters["@fk_ffHourly"].Value = FinalForcastHourlyId;
                    MyCom.Parameters.Add("@index", SqlDbType.Int);
                    MyCom.Parameters["@index"].Value = k + 1;
                    MyCom.Parameters.Add("@pdist", SqlDbType.Float);
                    MyCom.Parameters["@pdist"].Value = pdist[j, k];
                    MyCom.Parameters.Add("@ff", SqlDbType.Float);
                    MyCom.Parameters["@ff"].Value = ff[j, k];
                    MyCom.ExecuteNonQuery();

                }
            }
            //MyConnection.Close();
        }
        private CMatrix Get40Day_LC_HValue(DateTime date)
        {
            int nday = 40;
            if (txtTraining.Text != "")
            {
                nday = int.Parse(txtTraining.Text.Trim());
            }
            int maxCount = nday * 24;
            int lineTypesNo = Enum.GetNames(typeof(Line_codes)).Length;
            CMatrix LC_H = new CMatrix(lineTypesNo, maxCount);

            string endDate = new PersianDate(date).ToString("d");
            string startDate = new PersianDate(date.Subtract(new TimeSpan(nday, 0, 0, 0))).ToString("d");

            for (int index = 1; index <= lineTypesNo; index++)
            {
                Line_codes line = (Line_codes)Enum.Parse(typeof(Line_codes), index.ToString());

                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = new SqlCommand("SELECT * FROM [InterchangedEnergy] " +
                "WHERE Date>=@startdate AND Date<=@enddate AND Code=@code ORDER BY Date", MyConnection);
                Maxda.SelectCommand.Parameters.Add("@startdate", SqlDbType.Char, 10);
                Maxda.SelectCommand.Parameters["@startdate"].Value = startDate;
                Maxda.SelectCommand.Parameters.Add("@enddate", SqlDbType.Char, 10);
                Maxda.SelectCommand.Parameters["@enddate"].Value = endDate;
                Maxda.SelectCommand.Parameters.Add("@code", SqlDbType.NChar, 20);
                Maxda.SelectCommand.Parameters["@code"].Value = line.ToString();
                Maxda.Fill(MaxDS);

                DataTable dt = MaxDS.Tables[0];

                for (int daysbefore = nday-1; daysbefore >= 0; daysbefore--)
                {
                    DateTime selectedDate = date.Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                    string strDate = new PersianDate(selectedDate).ToString("d");

                    DataRow selectedRow = null;
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["Date"].ToString().Trim() == strDate)
                            selectedRow = row;
                    }
                    if (selectedRow != null)
                    {
                        for (int j = 0; j < 24; j++)
                        {
                            string colName = "Hour" + (j + 1).ToString();
                            int col = maxCount - (24 * daysbefore) - 24 + j;
                            LC_H[index - 1, col] = double.Parse(selectedRow[colName].ToString());
                        }
                    }
                    else
                    {
                        // it is by default equal to zero
                    }
                }

            }

            return LC_H;

        }

        private double[,] MWNumericArrayToDoubleArray(MWNumericArray mwNumericArray, int row, int col)
        {
            double[,] doubleArray = new double[row, col];

            for (int i = 0; i < row; i++)
                for (int j = 0; j < col; j++)
                    doubleArray[i, j] = mwNumericArray[i + 1, j + 1].ToScalarDouble();

            return doubleArray;
        }

        private MWNumericArray DoubleArrayToMWNumericArray(double[,] doubleArray)
        {
            int noX = doubleArray.GetLength(0);
            int noY = doubleArray.GetLength(1);

            //MWNumericArray mwNumericArray = MWNumericArray.MakeSparse(noX, noY, MWArrayComplexity.Real, 1);
            MWNumericArray mwNumericArray = new MWNumericArray(MWArrayComplexity.Real, MWNumericType.Double, noX, noY);
            //mwNumericArray = (MWNumericArray)doubleArray;

            for (int i = 0; i < noX; i++)
                for (int j = 0; j < noY; j++)
                    mwNumericArray[i + 1, j + 1] = doubleArray[i, j];

            return mwNumericArray;
        }

        private bool ValidateBiddingDates()
        {
            string msg = "";
            if (lblPlantValue.Text.Trim() == "")
            {
                msg = "Please select the plant(s)";
            }
            else if (datePickerCurrent.IsNull || datePickerBidding.IsNull)
            {
                msg = "Please select current and bidding dates";
            }
            else if (datePickerCurrent.SelectedDateTime > datePickerBidding.SelectedDateTime)
            {
                msg = "Bidding date should be ahead of current date";
            }
            if (msg == "")
                return true;
            else
            {
                MessageBox.Show(msg);
                return false;
            }
        }

        private void btnRunBidding_Click(object sender, EventArgs e)
        {
            
            if (!ValidateBiddingDates())
                return;
            //Classfinal classfinal = new Classfinal();
            //classfinal.value();

            // Create a background thread

            if (rdBidAllocation.Checked)
                RunBidding();
            else
            {
                this.Hide();
                BackgroundWorker bw = new BackgroundWorker();

                bw.DoWork += new DoWorkEventHandler(bw_DoWork);

                bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler
                                (bw_RunWorkerCompleted);

                // Create a progress form on the UI thread
                progressFrm = new ProgressForm();
                progressFrm.FormTitle = " Price Forecasting";
                //progressFrm.Description = " Test";
                // Kick off the Async thread
                bw.RunWorkerAsync();

                // Lock up the UI with this modal progress form.
                progressFrm.ShowDialog(this);
                progressFrm = null;
                this.Show();
            }
            ///////////////////////////////////
            //RunBidding();
        }


        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(100);
            progressFrm.Invoke((MethodInvoker)delegate()
            {
                RunBidding();
            });
        }

        private void bw_RunWorkerCompleted
                (object sender, RunWorkerCompletedEventArgs e)
        {
            // The background process is complete. First we should hide the
            // modal Progress Form to unlock the UI. Then we need to inspect our
            // response to see if an error occurred, a cancel was requested or
            // if we completed successfully.
            // Hide the Progress Form
            if (progressFrm != null)
            {
                Thread.Sleep(300);
                progressFrm.Hide();
                progressFrm = null;
            }
            // Check to see if an error occurred in the
            // background process.
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
                return;
            }
            // Check to see if the background process was cancelled.
            if (e.Cancelled)
            {
                MessageBox.Show("Processing cancelled.");
                return;
            }
            // Everything completed normally.
            // process the response using e.Result
            //MessageBox.Show("Processing is complete.");
        }

        private void RunBidding()
        {
            SaveBiddingStrategy();

            string strCmd = "select * from powerplant";

            if (lblPlantValue.Text.Trim().ToLower() != "all")
            {
                strCmd += " WHERE PPName= '" + lblPlantValue.Text.Trim() + "'";
            }
            DataTable dtPlants = utilities.returntbl(strCmd);
            int plants_Num = dtPlants.Rows.Count;
            bool[] firstDLLResults = new bool[dtPlants.Rows.Count];

            bool firstDll = rdForecastingPrice.Checked;
            bool secondDll = rdBidAllocation.Checked;

            if (rdStrategyBidding.Checked)
            {
                firstDll = true;
                secondDll = true;
            }


            if (firstDll)
            {
                //try
                //{
                    int i = 0;
                    foreach (DataRow row in dtPlants.Rows)
                    {
                        firstDLLResults[i++] = CalculateBidding(datePickerCurrent.SelectedDateTime, datePickerBidding.SelectedDateTime,
                            int.Parse(row["PPID"].ToString().Trim()),
                            row["PPName"].ToString().Trim());
                    }
                //}
                //catch (DllNotFoundException ex1)
                //{
                //    MessageBox.Show("A problem occured in Matlab RunTime Component!\r\nPlease inform the administrator...");
                //}
                //catch (TypeInitializationException ex2)
                //{
                //    MessageBox.Show("A problem occured in Matlab RunTime Component!\r\nPlease inform the administrator...");
                //}
                //catch (Exception exgeneral)
                //{
                //    MessageBox.Show("A problem has occured!\r\nPlease Try Later...");
                //    Log(exgeneral.Message);
                //}
            }
            
            if (secondDll)
            {
                bool canRunSecondDll = true;

                if (firstDll == true)
                {
                    for (int i = 0; i < plants_Num; i++)
                        if (firstDLLResults[i] == null || firstDLLResults[i] == false)
                            canRunSecondDll = false;
                }
                if (canRunSecondDll)
                {
                    //try
                    //{
                        Classfinal classFinal = new Classfinal(lblPlantValue.Text,
                            ((PersianDate)PersianDateConverter.ToPersianDate(datePickerCurrent.SelectedDateTime)).ToString("d"),
                        ((PersianDate)PersianDateConverter.ToPersianDate(datePickerBidding.SelectedDateTime)).ToString("d")
                        );
                        classFinal.value();
                        m002Generated = true;
                        btnExport.Enabled = m002Generated;
                    //}
                    //catch (DllNotFoundException ex)
                    //{
                    //    MessageBox.Show("A problem occured in CPlex Registration!\r\nPlease inform the administrator...");
                    //}
                    //catch(Exception exgeneral)
                    //{
                    //    MessageBox.Show("A problem has occured!\r\nPlease Try Later...");
                    //    Log(exgeneral.Message);
                    //}
                }
                else
                {
                    string str = "Due to unsuccessfull Price Forcasting, bid allocation is not possible...";
                    MessageBox.Show(str);
                }
            }
        }
        private void MainTabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            TreeNode selectedNode = null;
            if (e.GetType() == typeof(TreeNodeMouseClickEventArgs))
            {
                TreeNodeMouseClickEventArgs e2 = (TreeNodeMouseClickEventArgs)e;
                selectedNode = e2.Node;
            }
            else
                selectedNode = treeView1.SelectedNode;

            if (MainTabs.SelectedTab.Name == "BiddingStrategy")
            {
                if (selectedNode != null)
                {
                    lblPlantValue.Text = selectedNode.Text.Trim();
                    if (selectedNode.Text.Trim() == "Plant" || selectedNode.Text.Trim() == "Trec")
                        lblPlantValue.Text = "All";
                }

                //LoadBiddingStrategy();
            }
        }

        private void cmbRunSetting_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbRunSetting.SelectedItem.ToString() == "Customize")
                grBoxCustomize.Enabled = true;
            else
                grBoxCustomize.Enabled = false;

        }

        //private void InitializeCustomizeGroupBox(MenuStrip menuStrip)
        //{
        //        foreach (ToolStripItem item in menuStrip.Items)
        //        {
        //            if (item.GetType() == typeof(ToolStripComboBox))
        //            {
        //                ToolStripComboBox cmb = (ToolStripComboBox)item;
        //                if (item.Name.Contains("cmbPeak"))
        //                {

        //                    cmb.Items.Add("Yes");
        //                    cmb.Items.Add("No");
        //                }
        //                else if (cmb.Name.Contains("cmbStrategy"))
        //                {

        //                    cmb.Items.Add("Low");
        //                    cmb.Items.Add("Medium");
        //                    cmb.Items.Add("High");

        //                }
        //            }
        //        }


        //}

        private void btnSaveBidding_Click(object sender, EventArgs e)
        {
            SaveBiddingStrategy();
        }

        private void SaveBiddingStrategy()
        {
            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;

            //////// Delete previous results
            int BiddingStrategySettingId = 0;

            MyCom.CommandText = "select @id=max(id) from [BiddingStrategySetting] " +
            " where Plant=@Plant AND CurrentDate=@CurrentDate AND BiddingDate=@BiddingDate";

            MyCom.Parameters.Add("@Plant", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@CurrentDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@BiddingDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@id", SqlDbType.Int);
            MyCom.Parameters["@Plant"].Value = lblPlantValue.Text.Trim();
            MyCom.Parameters["@CurrentDate"].Value = new PersianDate(datePickerCurrent.SelectedDateTime).ToString("d"); ;
            MyCom.Parameters["@BiddingDate"].Value = new PersianDate(datePickerBidding.SelectedDateTime).ToString("d"); ; ;
            MyCom.Parameters["@id"].Direction = ParameterDirection.Output;

            MyCom.ExecuteNonQuery();

            try
            {
                BiddingStrategySettingId = int.Parse(MyCom.Parameters["@id"].Value.ToString());
            }
            catch { }

            if (BiddingStrategySettingId != 0)
            {
                MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                MyCom.CommandText = "Delete from BiddingStrategySetting where id=" + BiddingStrategySettingId.ToString();
                MyCom.ExecuteNonQuery();

                MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                MyCom.CommandText = "Delete from BiddingStrategyCustomized where FkBiddingStrategySettingId=" + BiddingStrategySettingId.ToString();
                MyCom.ExecuteNonQuery();
            }

            // Insert into FinalForcast table
            MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            MyCom.CommandText = "insert INTO [BiddingStrategySetting] " +
                "(Plant, CurrentDate, BiddingDate, RunSetting, StrategyBidding, TrainingDays, CostLevel, PeakBid, Status, FlagForecastingPrice, FlagBidAllocation, FlagStrategyBidding)"
                        + " VALUES (@Plant, @CurrentDate, @BiddingDate, @RunSetting, @StrategyBidding, @TrainingDays, @CostLevel, @PeakBid, @Status, @FlagForecastingPrice, @FlagBidAllocation, @FlagStrategyBidding)";

            MyCom.Parameters.Add("@Plant", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@CurrentDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@BiddingDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@RunSetting", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@StrategyBidding", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@PeakBid", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@TrainingDays", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@CostLevel", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@Status", SqlDbType.VarChar);
            MyCom.Parameters.Add("@FlagForecastingPrice", SqlDbType.Bit);
            MyCom.Parameters.Add("@FlagBidAllocation", SqlDbType.Bit);
            MyCom.Parameters.Add("@FlagStrategyBidding", SqlDbType.Bit);

            MyCom.Parameters["@Plant"].Value = lblPlantValue.Text;
            MyCom.Parameters["@CurrentDate"].Value = new PersianDate(datePickerCurrent.SelectedDateTime).ToString("d"); ;
            MyCom.Parameters["@BiddingDate"].Value = new PersianDate(datePickerBidding.SelectedDateTime).ToString("d"); ; ;
            MyCom.Parameters["@RunSetting"].Value = cmbRunSetting.Text;
            MyCom.Parameters["@StrategyBidding"].Value = cmbStrategyBidding.Text;
            MyCom.Parameters["@TrainingDays"].Value = txtTraining.Text;
            MyCom.Parameters["@CostLevel"].Value = cmbCostLevel.Text;
            MyCom.Parameters["@Status"].Value = lstStatus.Text;
            MyCom.Parameters["@PeakBid"].Value = cmbPeakBid.Text;
            MyCom.Parameters["@FlagForecastingPrice"].Value = rdForecastingPrice.Checked;
            MyCom.Parameters["@FlagBidAllocation"].Value = rdBidAllocation.Checked;
            MyCom.Parameters["@FlagStrategyBidding"].Value = rdStrategyBidding.Checked;
            MyCom.ExecuteNonQuery();

            // retrieve the newly inserted id in FinalForcast

            if (cmbRunSetting.Text == "Customize")
            {
                MyCom.CommandText = "select max(id) from [BiddingStrategySetting]";
                int maxId = (int)MyCom.ExecuteScalar();

                for (int j = 1; j <= 24; j++)
                {
                    // Insert into FinalForcastHourly table
                    MyCom = new SqlCommand();
                    MyCom.Connection = MyConnection;
                    MyCom.CommandText = "insert INTO [BiddingStrategyCustomized] (FkBiddingStrategySettingId,hour,Strategy, Peak)"
                                + " VALUES (@FkBiddingStrategySettingId,@hour,@Strategy, @Peak)";

                    MyCom.Parameters.Add("@FkBiddingStrategySettingId", SqlDbType.Int);
                    MyCom.Parameters.Add("@hour", SqlDbType.Int);
                    MyCom.Parameters.Add("@Strategy", SqlDbType.NChar, 10);
                    MyCom.Parameters.Add("@Peak", SqlDbType.NChar, 10);

                    ToolStripComboBox cmbStrategy = FindBiddingComboBox("cmbStrategy", j);
                    ToolStripComboBox cmbPeak = FindBiddingComboBox("cmbPeak", j);

                    MyCom.Parameters["@FkBiddingStrategySettingId"].Value = maxId;
                    MyCom.Parameters["@hour"].Value = j;
                    MyCom.Parameters["@Strategy"].Value = cmbStrategy.Text;
                    MyCom.Parameters["@Peak"].Value = cmbPeak.Text;
                    MyCom.ExecuteNonQuery();

                }
            }

        }

        private void LoadBiddingStrategy()
        {
            datePickerCurrent.SelectedDateTime = DateTime.Now;
            datePickerBidding.SelectedDateTime = DateTime.Now.AddDays(1);
            lblGencoValue.Text = "Tehran";
            lblPlantValue.Text = "All";

 
            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            // Insert into FinalForcast table
            MyCom.CommandText = "select max(id) from [BiddingStrategySetting]";
            try
            {
                int maxId = (int)MyCom.ExecuteScalar();


                MyCom.CommandText = "select * from [BiddingStrategySetting] " +
                    "where id=" + maxId.ToString();

                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;
                Maxda.Fill(MaxDS);

                DataTable dt = MaxDS.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    lblPlantValue.Text = row["Plant"].ToString();
                    datePickerCurrent.SelectedDateTime = PersianDateConverter.ToGregorianDateTime(new PersianDate(row["CurrentDate"].ToString()));
                    datePickerBidding.SelectedDateTime = PersianDateConverter.ToGregorianDateTime(new PersianDate(row["BiddingDate"].ToString()));
                    cmbRunSetting.Text = row["RunSetting"].ToString().Trim();
                    cmbStrategyBidding.Text = row["StrategyBidding"].ToString().Trim();
                    txtTraining.Text = row["TrainingDays"].ToString().Trim();
                    cmbCostLevel.Text = row["CostLevel"].ToString().Trim();
                    cmbPeakBid.Text = row["PeakBid"].ToString().Trim();
                    //lstStatus.Text = row["Status"].ToString().Trim();
                    string strBool = row["FlagForecastingPrice"].ToString().Trim().ToLower();
                    rdForecastingPrice.Checked = (strBool == "true" ? true : false);

                    strBool = row["FlagBidAllocation"].ToString().Trim().ToLower();
                    rdBidAllocation.Checked = (strBool == "true" ? true : false);

                    strBool = row["FlagStrategyBidding"].ToString().Trim().ToLower();
                    rdStrategyBidding.Checked = (strBool == "true" ? true : false);


                    MyCom.CommandText = "select * from [BiddingStrategyCustomized] " +
                        "where FkBiddingStrategySettingId = " + maxId.ToString();
                    MaxDS = new DataSet();
                    Maxda = new SqlDataAdapter();
                    Maxda.SelectCommand = MyCom;
                    Maxda.Fill(MaxDS);

                    dt = MaxDS.Tables[0];
                    foreach (DataRow row2 in dt.Rows)
                    {
                        int hour = Int16.Parse(row2["hour"].ToString().Trim());

                        ToolStripComboBox cmbStrategy = FindBiddingComboBox("cmbStrategy", hour);
                        ToolStripComboBox cmbPeak = FindBiddingComboBox("cmbPeak", hour);

                        cmbStrategy.Text = row2["Strategy"].ToString().Trim();
                        cmbPeak.Text = row2["Peak"].ToString().Trim();
                    }
                }

            }
            catch
            {
            }

        }

        private ToolStripComboBox FindBiddingComboBox(string type, int index)
        {
            ToolStripComboBox cmb = null;
            cmb = FindBiddingComboBox_2(this.menuStrip1, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip2, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip3, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip4, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip5, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip6, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip7, type, index);
            if (cmb != null)
                return cmb;
            return FindBiddingComboBox_2(this.menuStrip8, type, index);
        }

        private ToolStripComboBox FindBiddingComboBox_2(MenuStrip menuStrip, string type, int index)
        {
            foreach (ToolStripItem item in menuStrip.Items)
            {
                if (item.GetType() == typeof(ToolStripComboBox))
                {
                    ToolStripComboBox cmb = (ToolStripComboBox)item;
                    if (item.Name.ToLower() == type.ToLower() + index.ToString())
                    {
                        return cmb;
                    }

                }
            }
            return null;
        }

        private CMatrix CalculatePowerPlantMaxBid(int ppid, DataSet dsUnits, DateTime date, MixUnits mixUnitStatus)
        {
            CMatrix maxBids = new CMatrix(1, 24);
            for (int unitType = 1; unitType < dsUnits.Tables.Count; unitType++)
            {
                DataTable myDt = dsUnits.Tables[unitType];
                if (mixUnitStatus == MixUnits.All ||
                    (mixUnitStatus == MixUnits.JustCombined && myDt.TableName == "Combined") ||
                    (mixUnitStatus == MixUnits.ExeptCombined && (myDt.TableName == "Steam" || myDt.TableName == "Gas"))
                    )
                {
                    foreach (DataRow unitRow in myDt.Rows)
                    {
                        string package = unitRow["PackageType"].ToString().Trim();
                        string unit = unitRow["UnitCode"].ToString().Trim();
                        string packagecode = unitRow["PackageCode"].ToString().Trim();

                        string temp = unit;
                        temp = temp.ToLower();
                        if (package.Contains("CC"))
                        {
                            int x = ppid;
                            if ((x == 131) || (x == 144)) x++;
                            temp = x + "-" + "C" + packagecode;
                        }
                        else if (temp.Contains("gas"))
                        {
                            temp = temp.Replace("gas", "G");
                            temp = ppid + "-" + temp;
                        }
                        else
                        {
                            temp = temp.Replace("steam", "S");
                            temp = ppid + "-" + temp;
                        }
                        string blockM005 = temp.Trim();

                        /////////////////////////////////////

                        string blockM002 = unit.ToLower();
                        if (package.Contains("CC"))
                        {

                            blockM002 = "C" + packagecode;
                        }
                        else
                        {
                            if (blockM002.Contains("gas"))
                                blockM002 = blockM002.Replace("gas", "G");
                            else if (blockM002.Contains("steam"))
                                blockM002 = blockM002.Replace("steam", "S");
                        }
                        blockM002 = blockM002.Trim();

                        string strDate = new PersianDate(date).ToString("d");

                        CMatrix currentMaxBid = GetOneDayPricesForEachUnit(ppid, strDate, blockM005, blockM002);
                        //SaveMaxBid(currentMaxBid, ppid, blockM005, strDate);

                        if (currentMaxBid != null)
                            for (int p = 0; p < 24; p++)
                                if (maxBids[0, p] < currentMaxBid[0, p])
                                    maxBids[0, p] = currentMaxBid[0, p];
                    }
                }
            }

            return maxBids;

        }



        private DataSet GetUnits(int ppId)
        {
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT DISTINCT PackageType FROM UnitsDataMain WHERE PPID=@Num", MyConnection);
            Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
            Myda.SelectCommand.Parameters["@Num"].Value = ppId;
            Myda.Fill(MyDS, "UnitType");

            foreach (DataRow MyRow in MyDS.Tables["UnitType"].Rows)
            {
                string Utype = MyRow["PackageType"].ToString();
                Utype = Utype.Trim();
                switch (Utype)
                {
                    case "Steam":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Steam%'", MyConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, "Steam");

                        DataView sdv = new DataView(MyDS.Tables["Steam"]);
                        sdv.Sort = "UnitCode ASC";
                        break;
                    case "Gas":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Gas%'", MyConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, "Gas");
                        DataView gdv = new DataView(MyDS.Tables["Gas"]);
                        gdv.Sort = "UnitCode";

                        break;
                    default:

                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM [UnitsDataMain] WHERE PPID=@Num AND PackageType LIKE 'CC%'", MyConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, "Combined");
                        DataView cdv = new DataView(MyDS.Tables["Combined"]);
                        cdv.Sort = "UnitCode ASC";
                        break;
                }
            }

            MyDS.Dispose();
            Myda.Dispose();

            return MyDS;

        }

        private void betToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormDownloadInterval frm = new FormDownloadInterval();
            frm.Show();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowNewFolderButton = true;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                string path = folderBrowserDialog1.SelectedPath;

                if (lblPlantValue.Text.ToLower() != "all")
                {
                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Connection = ConnectionManager.GetInstance().Connection;
                    MyCom.CommandText = "SELECT @num=PPID, @nameFarsi=PNameFarsi FROM PowerPlant WHERE PPName=@name";
                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                    MyCom.Parameters.Add("@name", SqlDbType.NChar, 20);
                    MyCom.Parameters["@num"].Direction = ParameterDirection.Output;
                    MyCom.Parameters["@name"].Value = lblPlantValue.Text.Trim();
                    MyCom.Parameters.Add("@nameFarsi", SqlDbType.NVarChar, 50);
                    MyCom.Parameters["@nameFarsi"].Direction = ParameterDirection.Output;
                    MyCom.ExecuteNonQuery();

                    ExportM002Excel(path,
                        int.Parse(MyCom.Parameters["@num"].Value.ToString().Trim()),
                        MyCom.Parameters["@nameFarsi"].Value.ToString().Trim());
                }
                else
                {
                    DataTable dt = utilities.returntbl("select * from powerplant");
                    foreach (DataRow row in dt.Rows)
                    {
                        ExportM002Excel(path,
                            int.Parse(row["PPID"].ToString().Trim()),
                            row["PNameFarsi"].ToString().Trim());
                    }
                }
            }

            

            /////////////////
            //Excel.Application exobj1 = new Excel.Application();
            //exobj1.Visible = true;
            //exobj1.UserControl = true;
            //System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            //Excel.Workbook book1 = null;
            //book1 = exobj1.Workbooks.Open("C://test.xls", 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            //book1.Save();
            //book1.Close(true, book1, Type.Missing);
            //exobj1.Workbooks.Close();
            //exobj1.Quit();


            //////////////////


        }

        private void ExportToExcel(string filename,  System.Data.DataTable DT, string[] colNames)
        {
            int MaxRowsPerSheet = 1000;
            int rowIndex = 1000;
            int colIndex = 50;
            int sheetIndex = 0;
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            Excel.Application oExcel = new Excel.Application();

            oExcel.SheetsInNewWorkbook = 1;
            Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
            Excel.Worksheet sheet = null;
            Excel.Range range = null;

            foreach (DataRow row in DT.Rows)
            {
                if (rowIndex == MaxRowsPerSheet)
                {
                    rowIndex = 1;
                    colIndex = 1;
                    sheetIndex++;
                    if (sheet != null)
                        sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
                    else
                        sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

                    if (colNames == null)
                    {
                        foreach (DataColumn col in DT.Columns)
                        {
                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 = col.ColumnName.ToString();
                            range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                        }
                    }
                    else
                    {
                        foreach (string str in colNames)
                        {
                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 = str.ToString();
                            range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                        }
                    }
                    rowIndex++;
                    sheet.Name = "Page " + (sheetIndex);
                }
                colIndex = 1;
                foreach (object item in row.ItemArray)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, colIndex];
                    range.Value2 = item.ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    if (item.ToString().Length > 11 && range.NumberFormat != "#")
                    {
                        bool IsPhoneNumber = true;
                        string strItem = item.ToString();
                        for (int i = 0; i < 100000000000; i++)
                        {
                            if (!Char.IsDigit(strItem, i))
                            {
                                IsPhoneNumber = false;
                                break;
                            }
                        }
                        if (IsPhoneNumber)
                            range.EntireColumn.NumberFormat = "#";
                    }
                    colIndex++;
                }
                rowIndex++;
            }
            for (int i = 1; i <= WB.Worksheets.Count; i++)
            {
                sheet = (Excel.Worksheet)WB.Worksheets["Page " + i];
                int num_cols = DT.Columns.Count;
                for (int j = 1; j <= num_cols; j++)
                {
                    range = (Excel.Range)sheet.Cells[1, j];
                    range = range.EntireColumn;
                    range.AutoFit();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
            }
            sheet = ((Excel.Worksheet)WB.Worksheets["Page 1"]);
            sheet.Activate();
            ////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //string filename = page.Server.MapPath("/APP_TPH_local/temp") + "\\" + page.Session.SessionID + ".tmp";
            oExcel.Visible = true;
            //WB.SaveAs(filename, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            //WB.Close(Missing.Value, Missing.Value, Missing.Value);
            //oExcel.Workbooks.Close();
            //oExcel.Quit();

            /////////////////////

            //System.IO.FileStream input = new System.IO.FileStream(filename, System.IO.FileMode.Open);
            //System.IO.Stream output = res.OutputStream;
            //int len = (int)input.Length;
            //byte[] buffer = new byte[len];
            //input.Read(buffer, 0, len);
            //output.Write(buffer, 0, len);
            //input.Close();
            //output.Close();
            //System.IO.File.Delete(filename);
            //DT.Dispose();
            //DT = null;
            //System.Runtime.InteropServices.Marshal.ReleaseComObject(range);
            //System.Runtime.InteropServices.Marshal.ReleaseComObject(sheet);
            //System.Runtime.InteropServices.Marshal.ReleaseComObject(WB);
            //while (System.Runtime.InteropServices.Marshal.ReleaseComObject(oExcel) > 0) ;

            ////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //sheet = null;
            //WB = null;
            //oExcel = null;
            //range = null;
            //GC.Collect();

        }

        private void ExportM002Excel(string path, int ppid, string ppnameFarsi)
        {
            MixUnits mixUnitStatus = MixUnits.All;
            if (ppid == 131 || ppid == 144)
                mixUnitStatus = MixUnits.ExeptCombined;

            string[] headers = { "Block/Unit No", "Peak", "Max. Daily Generation", "Hour", 
                                   "Declared Capacity", "Dispatchable Capacity"};

            string[] firstColValues = { "Form Name :", "Date Of Issue :", "Time Of Issue :", "Target Market Date :",
                                    "Power Plant Name :", "Code :", "Revision :", 
                                    "Filled By :", "Approved By :" };

            do
            {
                int rowIndex = 0;
                int colIndex = 50;

                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                Excel.Application oExcel = new Excel.Application();

                oExcel.SheetsInNewWorkbook = 1;
                Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
                Excel.Worksheet sheet = null;
                Excel.Range range = null;

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers
                rowIndex = 0;
                colIndex = 0;
                int sheetIndex = 1;
                if (sheet != null)
                    sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
                else
                    sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

                sheet.Name = "Page " + (sheetIndex);

                int ppidToPrint = ppid;
                if (mixUnitStatus == MixUnits.JustCombined)// the second one
                    ppidToPrint++;
                string[] secondColValues = { "M0022", 
                                          DateTime.Now.ToString("d"), DateTime.Now.ToString("T"),
                                          datePickerBidding.Text,
                                          ppnameFarsi, ppidToPrint.ToString(), 
                                          "0", "", "" };


                for (int i = 0; i < firstColValues.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[i + 1, 1];
                    range.Value2 = firstColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    range = (Excel.Range)sheet.Cells[i + 1, 2];
                    range.Value2 = secondColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


                rowIndex = firstColValues.Length + 2;
                colIndex = 0;
                for (int i = 0; i < headers.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = headers[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
                for (int i = 1; i <= 10; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = "Power_" + i.ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = "Price_" + i.ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                string strCmd = "select distinct block from DetailFRM002 where ppid='" + ppid.ToString() +
                "' AND TargetMarketDate='" + datePickerBidding.Text + "'";
                if (mixUnitStatus == MixUnits.ExeptCombined)
                    strCmd += " AND block like 'S%'";
                else if (mixUnitStatus == MixUnits.JustCombined)
                    strCmd += " AND block like 'C%'";

                DataTable oDataTable = utilities.returntbl(strCmd);

                string[] blocks = new string[oDataTable.Rows.Count];
                for (int i = 0; i < oDataTable.Rows.Count; i++)
                    blocks[i] = oDataTable.Rows[i][0].ToString().Trim();

                //strCmd = "select * from DetailFRM002 where ppid='" + ppid.ToString() +
                //    "' AND TargetMarketDate='" + datePickerBidding.Text + "' order by block, hour";
                //oDataTable = utilities.returntbl(strCmd);

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                rowIndex = firstColValues.Length + 3;
                foreach (string blockName in blocks)
                {
                    int firstLine = rowIndex;
                    range = (Excel.Range)sheet.Cells[rowIndex, 1];
                    range.Value2 = blockName;
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    ////////!!!!!!! Peak!!!>???????????????

                    strCmd = "select * from DetailFRM002 where ppid='" + ppid.ToString() +
                        "' AND Block='" + blockName +
                        "' AND TargetMarketDate='" + datePickerBidding.Text + "'" +
                        " order by block, hour";
                    oDataTable = utilities.returntbl(strCmd);

                    colIndex = 4;

                    double max = 0;
                    foreach (DataRow row in oDataTable.Rows)
                    {
                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = (int.Parse(row["Hour"].ToString().Trim())).ToString();
                        //range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["DeclaredCapacity"].ToString().Trim();


                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["DispachableCapacity"].ToString().Trim();
                        double dC = double.Parse(row["DispachableCapacity"].ToString().Trim());

                        if (dC > max)
                            max = dC;

                        for (int i = 1; i <= 10; i++)
                        {
                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 = row["Power" + i.ToString()].ToString().Trim();

                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 = row["Price" + i.ToString()].ToString().Trim();
                        }

                        rowIndex++;
                        colIndex = 4;
                    }

                    range = (Excel.Range)sheet.Cells[firstLine, 2];
                    range.Value2 = max.ToString();
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                sheet = ((Excel.Worksheet)WB.Worksheets["Page 1"]);

                range = (Excel.Range)sheet.Cells[1, 1];
                range = range.EntireColumn;
                range.AutoFit();

                range = (Excel.Range)sheet.Cells[1, 2];
                range = range.EntireColumn;
                range.AutoFit();

                sheet.Activate();
                //oExcel.Visible = true;

                string strDate = datePickerBidding.Text;
                strDate = strDate.Remove(7, 1);
                strDate = strDate.Remove(4, 1);
                string filePath = path + "\\" + ppidToPrint.ToString() + "-" + strDate + ".xls";
                //oExcel.Visible = true;
                WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                WB.Close(Missing.Value, Missing.Value, Missing.Value);
                oExcel.Workbooks.Close();
                oExcel.Quit();

                if (mixUnitStatus == MixUnits.ExeptCombined)
                    mixUnitStatus = MixUnits.JustCombined;
                else
                    mixUnitStatus = MixUnits.All;

            } while (mixUnitStatus != MixUnits.All);
            
        }

        private void lblPlantValue_TextChanged(object sender, EventArgs e)
        {
            UpdateBiddingTab();
        }

        private void UpdateBiddingTab()
        {
            m002Generated = false;
            btnExport.Enabled = m002Generated;

            string strDate = PersianDateConverter.ToPersianDate(datePickerBidding.SelectedDateTime).ToString("d");

            lstStatus.Text = "";
            if (lblPlantValue.Text.Trim() != "All" && strDate != "")
            {
                CheckInfoAvailability();
                CheckFuelAvailabiliyty();

            }
        }

        private void CheckInfoAvailability()
        {
            string strDate = PersianDateConverter.ToPersianDate(datePickerCurrent.SelectedDateTime).ToString("d");

            string[] M002Tables = { "MainFRM002", "DetailFRM002", "BlockFRM002" };
            string[] M005Tables = { "MainFRM005", "DetailFRM005", "BlockFRM005" };

            string strCmdFirst = "SELECT @count=count(*) from ";
            string strCmdEnd = " as table1 inner join dbo.PowerPlant on PowerPlant.PPID=table1.PPID" +
                " where table1.TargetMarketDate ='" + strDate + "' AND PowerPlant.PPName='" + lblPlantValue.Text.Trim() + "'";

            bool M002Available = true, M005Available = true;
            for (int i = 0; i < 3 && (M002Available || M005Available); i++)
            {
                SqlCommand command = new SqlCommand(strCmdFirst + M002Tables[i] + strCmdEnd, ConnectionManager.GetInstance().Connection);
                command.Parameters.Add("@count", SqlDbType.Int);
                command.Parameters["@count"].Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                int count = int.Parse(command.Parameters["@count"].Value.ToString());
                if (count == 0)
                    M002Available = false;

                command = new SqlCommand(strCmdFirst + M005Tables[i] + strCmdEnd, ConnectionManager.GetInstance().Connection);
                command.Parameters.Add("@count", SqlDbType.Int);
                command.Parameters["@count"].Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                count = int.Parse(command.Parameters["@count"].Value.ToString());
                if (count == 0)
                    M005Available = false;

            }

            if (M002Available)
                lstStatus.Text += "M002 Data Available...\r\n";
            else
                lstStatus.Text += "M002 Data Not Available...\r\n";

            if (M005Available)
                lstStatus.Text += "M005 Data Available...\r\n";
            else
                lstStatus.Text += "M005 Data Not Available...\r\n";

        }

        private void CheckFuelAvailabiliyty()
        {
            string strDate = PersianDateConverter.ToPersianDate(datePickerBidding.SelectedDateTime).ToString("d");

            string strCmd ="select UnitCode,SecondFuel,SecondFuelStartDate,SecondFuelEndDate " +
                " from ConditionUnit inner join PowerPlant on PowerPlant.PPID=ConditionUnit.PPID" +
                " where PowerPlant.ppname='" + lblPlantValue.Text.Trim()+ "'" +
                " and SecondFuelStartDate<'" + strDate+ "' and '" + strDate + "'<SecondFuelEndDate";

            DataTable oDataTable = utilities.returntbl(strCmd);
            foreach (DataRow myrow in oDataTable.Rows)
            {
                if (myrow["SecondFuel"].ToString()!="" && bool.Parse(myrow["SecondFuel"].ToString()) == true)
                {
                    if (oDataTable.Rows.Count != 0)
                    {
                        //   MessageBox.Show("Test");
                        lstStatus.Text += "*********************units of plant that have second fuel*****************\r\n";
                        lstStatus.Text +=myrow["UnitCode"].ToString() + "\r\n";

                        lstStatus.Text += "***************dates of second fuel <<< " + myrow["UnitCode"].ToString().Trim() + " >>> ********************\r\n";
                        lstStatus.Text += myrow["SecondFuelStartDate"].ToString() + "\r\n";

                        lstStatus.Text += myrow["SecondFuelEndDate"].ToString() + "\r\n";
                    }
                }
            }
            ////////////////////
            //lstStatus.Text += "\r\n";
            //lstStatus.Text += "\r\n";

            strCmd = "select UnitCode,Maintenance,MaintenanceStartDate,MaintenanceEndDate " +
                " from ConditionUnit inner join PowerPlant on PowerPlant.PPID=ConditionUnit.PPID" +
                " where PowerPlant.ppname='" + lblPlantValue.Text.Trim() + "'" +
                " and MaintenanceStartDate<'" + strDate + "' and '" + strDate + "'<MaintenanceEndDate";

            oDataTable = utilities.returntbl(strCmd);

            string[] mm = new string[oDataTable.Rows.Count];
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {
                mm[i] = oDataTable.Rows[i][0].ToString();
            }

            foreach (DataRow myrow1 in oDataTable.Rows)
            {

                if (myrow1["Maintenance"].ToString()!="" && bool.Parse(myrow1["Maintenance"].ToString()) == true)
                {
                    lstStatus.Text += "*********************units of plant that have maintenance***************\r\n";
                    lstStatus.Text += myrow1["UnitCode"].ToString() + "\r\n"; ;

                    lstStatus.Text += "***************dates of maintenance <<<" + myrow1["UnitCode"].ToString().Trim() + " >>>*****************\r\n";
                    lstStatus.Text += myrow1["MaintenanceStartDate"].ToString() + "\r\n"; ;

                    lstStatus.Text += myrow1["MaintenanceEndDate"].ToString() + "\r\n"; ;


                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            
            lstStatus.Text += "\r\n";
            lstStatus.Text += "\r\n";

            strCmd = "select UnitCode,OutService,OutServiceStartDate,OutServiceEndDate" +
                " from ConditionUnit inner join PowerPlant on PowerPlant.PPID=ConditionUnit.PPID" +
                " where PowerPlant.ppname='" + lblPlantValue.Text.Trim() + "'" +
                " and OutServiceStartDate<'" + strDate + "' and '" + strDate + "'<OutServiceEndDate";

            oDataTable = utilities.returntbl(strCmd);

            foreach (DataRow myrow1 in oDataTable.Rows)
            {

                if (bool.Parse(myrow1["OutService"].ToString()) == true)
                {
                    lstStatus.Text += "*********************units of plant that have outservice***************\r\n";
                    lstStatus.Text += myrow1["UnitCode"].ToString() + "\r\n";

                    lstStatus.Text += "***************dates of outservice <<<" + myrow1["UnitCode"].ToString().Trim() + " >>>*****************\r\n";
                    lstStatus.Text += myrow1["OutServiceStartDate"].ToString() + "\r\n";

                    lstStatus.Text += myrow1["OutServiceEndDate"].ToString() + "\r\n";


                }
            }
        }

        private void datePickerBidding_SelectedDateTimeChanged(object sender, EventArgs e)
        {
            UpdateBiddingTab();
        }

    }
}