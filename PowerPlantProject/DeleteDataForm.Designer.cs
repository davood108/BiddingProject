﻿namespace PowerPlantProject
{
    partial class DeleteDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeleteDataForm));
            this.datePickerTo = new FarsiLibrary.Win.Controls.FADatePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.datePickerFrom = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label44 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rdOptional = new System.Windows.Forms.RadioButton();
            this.rdAll = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rbmarketbill = new System.Windows.Forms.RadioButton();
            this.rbfinance = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbForeCasting = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbrep12 = new System.Windows.Forms.RadioButton();
            this.rbInterchangedEnergy = new System.Windows.Forms.RadioButton();
            this.rbSaledEnergy = new System.Windows.Forms.RadioButton();
            this.rbAveragePrice = new System.Windows.Forms.RadioButton();
            this.rbUnitNetComp = new System.Windows.Forms.RadioButton();
            this.rbManategh = new System.Windows.Forms.RadioButton();
            this.rbRegionNetComp = new System.Windows.Forms.RadioButton();
            this.rbLineNetComp = new System.Windows.Forms.RadioButton();
            this.rbProducedEnergy = new System.Windows.Forms.RadioButton();
            this.rbOutageUnits = new System.Windows.Forms.RadioButton();
            this.rbLoadForecasting = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdm005ba = new System.Windows.Forms.RadioButton();
            this.rb005 = new System.Windows.Forms.RadioButton();
            this.rb002 = new System.Windows.Forms.RadioButton();
            this.rbdispatch = new System.Windows.Forms.RadioButton();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // datePickerTo
            // 
            this.datePickerTo.HasButtons = true;
            this.datePickerTo.Location = new System.Drawing.Point(373, 30);
            this.datePickerTo.Name = "datePickerTo";
            this.datePickerTo.Readonly = true;
            this.datePickerTo.Size = new System.Drawing.Size(120, 20);
            this.datePickerTo.TabIndex = 41;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.CadetBlue;
            this.button1.BackgroundImage = global::PowerPlantProject.Properties.Resources.deletered8;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button1.Location = new System.Drawing.Point(263, 504);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 34);
            this.button1.TabIndex = 39;
            this.button1.Text = "Delete";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // datePickerFrom
            // 
            this.datePickerFrom.HasButtons = true;
            this.datePickerFrom.Location = new System.Drawing.Point(188, 30);
            this.datePickerFrom.Name = "datePickerFrom";
            this.datePickerFrom.Readonly = true;
            this.datePickerFrom.Size = new System.Drawing.Size(120, 20);
            this.datePickerFrom.TabIndex = 42;
            this.datePickerFrom.ValueChanged += new System.EventHandler(this.datePickerFrom_ValueChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Location = new System.Drawing.Point(146, 33);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(36, 13);
            this.label44.TabIndex = 40;
            this.label44.Text = "From :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(339, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 40;
            this.label1.Text = "To :";
            // 
            // rdOptional
            // 
            this.rdOptional.AutoSize = true;
            this.rdOptional.Checked = true;
            this.rdOptional.Location = new System.Drawing.Point(188, 85);
            this.rdOptional.Name = "rdOptional";
            this.rdOptional.Size = new System.Drawing.Size(97, 17);
            this.rdOptional.TabIndex = 43;
            this.rdOptional.TabStop = true;
            this.rdOptional.Text = "SelectManually";
            this.rdOptional.UseVisualStyleBackColor = true;
            this.rdOptional.CheckedChanged += new System.EventHandler(this.rdOptional_CheckedChanged);
            // 
            // rdAll
            // 
            this.rdAll.AutoSize = true;
            this.rdAll.Location = new System.Drawing.Point(373, 85);
            this.rdAll.Name = "rdAll";
            this.rdAll.Size = new System.Drawing.Size(66, 17);
            this.rdAll.TabIndex = 44;
            this.rdAll.Text = "SelectAll";
            this.rdAll.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(12, 123);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(618, 361);
            this.panel1.TabIndex = 45;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.groupBox4.Controls.Add(this.rbmarketbill);
            this.groupBox4.Controls.Add(this.rbfinance);
            this.groupBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox4.Location = new System.Drawing.Point(15, 284);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(592, 48);
            this.groupBox4.TabIndex = 46;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Financial";
            // 
            // rbmarketbill
            // 
            this.rbmarketbill.AutoSize = true;
            this.rbmarketbill.Location = new System.Drawing.Point(351, 19);
            this.rbmarketbill.Name = "rbmarketbill";
            this.rbmarketbill.Size = new System.Drawing.Size(61, 17);
            this.rbmarketbill.TabIndex = 0;
            this.rbmarketbill.Text = "IgmcBill";
            this.rbmarketbill.UseVisualStyleBackColor = true;
            // 
            // rbfinance
            // 
            this.rbfinance.AutoSize = true;
            this.rbfinance.Location = new System.Drawing.Point(161, 19);
            this.rbfinance.Name = "rbfinance";
            this.rbfinance.Size = new System.Drawing.Size(51, 17);
            this.rbfinance.TabIndex = 0;
            this.rbfinance.Text = "NriBill";
            this.rbfinance.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.groupBox3.Controls.Add(this.rbForeCasting);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox3.Location = new System.Drawing.Point(15, 213);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(592, 51);
            this.groupBox3.TabIndex = 41;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "ForeCasting";
            // 
            // rbForeCasting
            // 
            this.rbForeCasting.AutoSize = true;
            this.rbForeCasting.Location = new System.Drawing.Point(227, 19);
            this.rbForeCasting.Name = "rbForeCasting";
            this.rbForeCasting.Size = new System.Drawing.Size(111, 17);
            this.rbForeCasting.TabIndex = 0;
            this.rbForeCasting.Text = "ForeCastingResult";
            this.rbForeCasting.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.groupBox1.Controls.Add(this.rbrep12);
            this.groupBox1.Controls.Add(this.rbInterchangedEnergy);
            this.groupBox1.Controls.Add(this.rbSaledEnergy);
            this.groupBox1.Controls.Add(this.rbAveragePrice);
            this.groupBox1.Controls.Add(this.rbUnitNetComp);
            this.groupBox1.Controls.Add(this.rbManategh);
            this.groupBox1.Controls.Add(this.rbRegionNetComp);
            this.groupBox1.Controls.Add(this.rbLineNetComp);
            this.groupBox1.Controls.Add(this.rbProducedEnergy);
            this.groupBox1.Controls.Add(this.rbOutageUnits);
            this.groupBox1.Controls.Add(this.rbLoadForecasting);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox1.Location = new System.Drawing.Point(15, 96);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(592, 100);
            this.groupBox1.TabIndex = 39;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "internet";
            // 
            // rbrep12
            // 
            this.rbrep12.AutoSize = true;
            this.rbrep12.Location = new System.Drawing.Point(465, 77);
            this.rbrep12.Name = "rbrep12";
            this.rbrep12.Size = new System.Drawing.Size(87, 17);
            this.rbrep12.TabIndex = 1;
            this.rbrep12.TabStop = true;
            this.rbrep12.Text = "Rep12Pages";
            this.rbrep12.UseVisualStyleBackColor = true;
            // 
            // rbInterchangedEnergy
            // 
            this.rbInterchangedEnergy.AutoSize = true;
            this.rbInterchangedEnergy.Location = new System.Drawing.Point(465, 30);
            this.rbInterchangedEnergy.Name = "rbInterchangedEnergy";
            this.rbInterchangedEnergy.Size = new System.Drawing.Size(121, 17);
            this.rbInterchangedEnergy.TabIndex = 0;
            this.rbInterchangedEnergy.Text = "InterchangedEnergy";
            this.rbInterchangedEnergy.UseVisualStyleBackColor = true;
            // 
            // rbSaledEnergy
            // 
            this.rbSaledEnergy.AutoSize = true;
            this.rbSaledEnergy.Location = new System.Drawing.Point(12, 77);
            this.rbSaledEnergy.Name = "rbSaledEnergy";
            this.rbSaledEnergy.Size = new System.Drawing.Size(85, 17);
            this.rbSaledEnergy.TabIndex = 0;
            this.rbSaledEnergy.Text = "SaledEnergy";
            this.rbSaledEnergy.UseVisualStyleBackColor = true;
            // 
            // rbAveragePrice
            // 
            this.rbAveragePrice.AutoSize = true;
            this.rbAveragePrice.Location = new System.Drawing.Point(12, 31);
            this.rbAveragePrice.Name = "rbAveragePrice";
            this.rbAveragePrice.Size = new System.Drawing.Size(89, 17);
            this.rbAveragePrice.TabIndex = 0;
            this.rbAveragePrice.Text = "AveragePrice";
            this.rbAveragePrice.UseVisualStyleBackColor = true;
            // 
            // rbUnitNetComp
            // 
            this.rbUnitNetComp.AutoSize = true;
            this.rbUnitNetComp.Location = new System.Drawing.Point(355, 77);
            this.rbUnitNetComp.Name = "rbUnitNetComp";
            this.rbUnitNetComp.Size = new System.Drawing.Size(88, 17);
            this.rbUnitNetComp.TabIndex = 0;
            this.rbUnitNetComp.Text = "UnitNetComp";
            this.rbUnitNetComp.UseVisualStyleBackColor = true;
            // 
            // rbManategh
            // 
            this.rbManategh.AutoSize = true;
            this.rbManategh.Location = new System.Drawing.Point(133, 30);
            this.rbManategh.Name = "rbManategh";
            this.rbManategh.Size = new System.Drawing.Size(73, 17);
            this.rbManategh.TabIndex = 0;
            this.rbManategh.Text = "Manategh";
            this.rbManategh.UseVisualStyleBackColor = true;
            // 
            // rbRegionNetComp
            // 
            this.rbRegionNetComp.AutoSize = true;
            this.rbRegionNetComp.Location = new System.Drawing.Point(243, 77);
            this.rbRegionNetComp.Name = "rbRegionNetComp";
            this.rbRegionNetComp.Size = new System.Drawing.Size(103, 17);
            this.rbRegionNetComp.TabIndex = 0;
            this.rbRegionNetComp.Text = "RegionNetComp";
            this.rbRegionNetComp.UseVisualStyleBackColor = true;
            // 
            // rbLineNetComp
            // 
            this.rbLineNetComp.AutoSize = true;
            this.rbLineNetComp.Location = new System.Drawing.Point(244, 30);
            this.rbLineNetComp.Name = "rbLineNetComp";
            this.rbLineNetComp.Size = new System.Drawing.Size(89, 17);
            this.rbLineNetComp.TabIndex = 0;
            this.rbLineNetComp.Text = "LineNetComp";
            this.rbLineNetComp.UseVisualStyleBackColor = true;
            // 
            // rbProducedEnergy
            // 
            this.rbProducedEnergy.AutoSize = true;
            this.rbProducedEnergy.Location = new System.Drawing.Point(133, 77);
            this.rbProducedEnergy.Name = "rbProducedEnergy";
            this.rbProducedEnergy.Size = new System.Drawing.Size(104, 17);
            this.rbProducedEnergy.TabIndex = 0;
            this.rbProducedEnergy.Text = "ProducedEnergy";
            this.rbProducedEnergy.UseVisualStyleBackColor = true;
            // 
            // rbOutageUnits
            // 
            this.rbOutageUnits.AutoSize = true;
            this.rbOutageUnits.Location = new System.Drawing.Point(355, 30);
            this.rbOutageUnits.Name = "rbOutageUnits";
            this.rbOutageUnits.Size = new System.Drawing.Size(84, 17);
            this.rbOutageUnits.TabIndex = 0;
            this.rbOutageUnits.Text = "OutageUnits";
            this.rbOutageUnits.UseVisualStyleBackColor = true;
            // 
            // rbLoadForecasting
            // 
            this.rbLoadForecasting.AutoSize = true;
            this.rbLoadForecasting.Location = new System.Drawing.Point(12, 54);
            this.rbLoadForecasting.Name = "rbLoadForecasting";
            this.rbLoadForecasting.Size = new System.Drawing.Size(104, 17);
            this.rbLoadForecasting.TabIndex = 0;
            this.rbLoadForecasting.Text = "LoadForecasting";
            this.rbLoadForecasting.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.groupBox2.Controls.Add(this.rdm005ba);
            this.groupBox2.Controls.Add(this.rb005);
            this.groupBox2.Controls.Add(this.rb002);
            this.groupBox2.Controls.Add(this.rbdispatch);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox2.Location = new System.Drawing.Point(15, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(592, 68);
            this.groupBox2.TabIndex = 40;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "m002-m005";
            // 
            // rdm005ba
            // 
            this.rdm005ba.AutoSize = true;
            this.rdm005ba.Location = new System.Drawing.Point(294, 26);
            this.rdm005ba.Name = "rdm005ba";
            this.rdm005ba.Size = new System.Drawing.Size(109, 17);
            this.rdm005ba.TabIndex = 1;
            this.rdm005ba.Text = "M005 With Limmit";
            this.rdm005ba.UseVisualStyleBackColor = true;
            // 
            // rb005
            // 
            this.rb005.AutoSize = true;
            this.rb005.Location = new System.Drawing.Point(206, 26);
            this.rb005.Name = "rb005";
            this.rb005.Size = new System.Drawing.Size(52, 17);
            this.rb005.TabIndex = 0;
            this.rb005.Text = "M005";
            this.rb005.UseVisualStyleBackColor = true;
            // 
            // rb002
            // 
            this.rb002.AutoSize = true;
            this.rb002.Location = new System.Drawing.Point(103, 26);
            this.rb002.Name = "rb002";
            this.rb002.Size = new System.Drawing.Size(52, 17);
            this.rb002.TabIndex = 0;
            this.rb002.Text = "M002";
            this.rb002.UseVisualStyleBackColor = true;
            // 
            // rbdispatch
            // 
            this.rbdispatch.AutoSize = true;
            this.rbdispatch.Location = new System.Drawing.Point(438, 26);
            this.rbdispatch.Name = "rbdispatch";
            this.rbdispatch.Size = new System.Drawing.Size(67, 17);
            this.rbdispatch.TabIndex = 0;
            this.rbdispatch.Text = "Dispatch";
            this.rbdispatch.UseVisualStyleBackColor = true;
            // 
            // DeleteDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(638, 557);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.rdAll);
            this.Controls.Add(this.rdOptional);
            this.Controls.Add(this.datePickerTo);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.datePickerFrom);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label44);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "DeleteDataForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DeleteDataForm";
            this.Load += new System.EventHandler(this.DeleteDataForm_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FarsiLibrary.Win.Controls.FADatePicker datePickerTo;
        private System.Windows.Forms.Button button1;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerFrom;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rdOptional;
        private System.Windows.Forms.RadioButton rdAll;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbForeCasting;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbInterchangedEnergy;
        private System.Windows.Forms.RadioButton rbSaledEnergy;
        private System.Windows.Forms.RadioButton rbAveragePrice;
        private System.Windows.Forms.RadioButton rbUnitNetComp;
        private System.Windows.Forms.RadioButton rbManategh;
        private System.Windows.Forms.RadioButton rbRegionNetComp;
        private System.Windows.Forms.RadioButton rbLineNetComp;
        private System.Windows.Forms.RadioButton rbProducedEnergy;
        private System.Windows.Forms.RadioButton rbOutageUnits;
        private System.Windows.Forms.RadioButton rbLoadForecasting;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rb005;
        private System.Windows.Forms.RadioButton rb002;
        private System.Windows.Forms.RadioButton rbdispatch;
        private System.Windows.Forms.RadioButton rbrep12;
        private System.Windows.Forms.RadioButton rdm005ba;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rbfinance;
        private System.Windows.Forms.RadioButton rbmarketbill;
    }
}