﻿namespace PowerPlantProject
{
    partial class MaintenancePlantReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          //  this.MaintenancecrystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
           // this.MaintenanceCrystalReportDoc1 = new PowerPlantProject.MaintenanceCrystalReport();
            this.SuspendLayout();
            // 
            // MaintenancecrystalReportViewer1
            // 
         //   this.MaintenancecrystalReportViewer1.ActiveViewIndex = -1;
           // this.MaintenancecrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          //  this.MaintenancecrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
          //  this.MaintenancecrystalReportViewer1.Location = new System.Drawing.Point(0, 0);
          //  this.MaintenancecrystalReportViewer1.Name = "MaintenancecrystalReportViewer1";
          //  this.MaintenancecrystalReportViewer1.SelectionFormula = "";
          //  this.MaintenancecrystalReportViewer1.Size = new System.Drawing.Size(292, 266);
          //  this.MaintenancecrystalReportViewer1.TabIndex = 0;
          //  this.MaintenancecrystalReportViewer1.ViewTimeSelectionFormula = "";
            // 
            // MaintenancePlantReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
           // this.Controls.Add(this.MaintenancecrystalReportViewer1);
            this.Name = "MaintenancePlantReportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MaintenancePlantReportForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MaintenancePlantReportForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

       // private CrystalDecisions.Windows.Forms.CrystalReportViewer MaintenancecrystalReportViewer1;
       // private MaintenanceCrystalReport MaintenanceCrystalReportDoc1;
    }
}