﻿namespace PowerPlantProject
{
    partial class Hub_Contract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Hub_Contract));
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.delete = new System.Windows.Forms.DataGridViewImageColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.datePickerFrom = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.datePickerto = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmbplant = new System.Windows.Forms.ComboBox();
            this.cmbunit = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbthour = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbfhour = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdmustrun = new System.Windows.Forms.RadioButton();
            this.rdMustNet = new System.Windows.Forms.RadioButton();
            this.rdMustOff = new System.Windows.Forms.RadioButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.H1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "Delete";
            this.dataGridViewImageColumn1.Image = global::PowerPlantProject.Properties.Resources.deletered81;
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.delete});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.Location = new System.Drawing.Point(25, 349);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.Size = new System.Drawing.Size(936, 258);
            this.dataGridView2.TabIndex = 67;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // delete
            // 
            this.delete.HeaderText = "Delete";
            this.delete.Image = global::PowerPlantProject.Properties.Resources.deletered81;
            this.delete.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.delete.Name = "delete";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.datePickerFrom);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.datePickerto);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Location = new System.Drawing.Point(95, 31);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(368, 68);
            this.groupBox4.TabIndex = 66;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Date";
            // 
            // datePickerFrom
            // 
            this.datePickerFrom.HasButtons = true;
            this.datePickerFrom.Location = new System.Drawing.Point(57, 31);
            this.datePickerFrom.Name = "datePickerFrom";
            this.datePickerFrom.Readonly = true;
            this.datePickerFrom.Size = new System.Drawing.Size(120, 20);
            this.datePickerFrom.TabIndex = 44;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 48;
            this.label1.Text = "From :";
            // 
            // datePickerto
            // 
            this.datePickerto.HasButtons = true;
            this.datePickerto.Location = new System.Drawing.Point(228, 31);
            this.datePickerto.Name = "datePickerto";
            this.datePickerto.Readonly = true;
            this.datePickerto.Size = new System.Drawing.Size(120, 20);
            this.datePickerto.TabIndex = 44;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(199, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 48;
            this.label7.Text = "To:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmbplant);
            this.groupBox3.Controls.Add(this.cmbunit);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(502, 31);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(404, 68);
            this.groupBox3.TabIndex = 65;
            this.groupBox3.TabStop = false;
            // 
            // cmbplant
            // 
            this.cmbplant.FormattingEnabled = true;
            this.cmbplant.Location = new System.Drawing.Point(59, 26);
            this.cmbplant.Name = "cmbplant";
            this.cmbplant.Size = new System.Drawing.Size(121, 21);
            this.cmbplant.TabIndex = 0;
            this.cmbplant.SelectedValueChanged += new System.EventHandler(this.cmbplant_SelectedValueChanged);
            // 
            // cmbunit
            // 
            this.cmbunit.FormattingEnabled = true;
            this.cmbunit.Location = new System.Drawing.Point(261, 27);
            this.cmbunit.Name = "cmbunit";
            this.cmbunit.Size = new System.Drawing.Size(121, 21);
            this.cmbunit.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 48;
            this.label2.Text = "Plant";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(229, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 48;
            this.label3.Text = "Unit";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbthour);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cmbfhour);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(95, 107);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(368, 71);
            this.groupBox2.TabIndex = 64;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Hour";
            // 
            // cmbthour
            // 
            this.cmbthour.FormattingEnabled = true;
            this.cmbthour.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"});
            this.cmbthour.Location = new System.Drawing.Point(228, 23);
            this.cmbthour.Name = "cmbthour";
            this.cmbthour.Size = new System.Drawing.Size(63, 21);
            this.cmbthour.TabIndex = 49;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(202, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 48;
            this.label6.Text = "To";
            // 
            // cmbfhour
            // 
            this.cmbfhour.FormattingEnabled = true;
            this.cmbfhour.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"});
            this.cmbfhour.Location = new System.Drawing.Point(70, 23);
            this.cmbfhour.Name = "cmbfhour";
            this.cmbfhour.Size = new System.Drawing.Size(62, 21);
            this.cmbfhour.TabIndex = 49;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 48;
            this.label5.Text = "From";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdmustrun);
            this.groupBox1.Controls.Add(this.rdMustNet);
            this.groupBox1.Controls.Add(this.rdMustOff);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(502, 107);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(404, 71);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            // 
            // rdmustrun
            // 
            this.rdmustrun.AutoSize = true;
            this.rdmustrun.Location = new System.Drawing.Point(16, 19);
            this.rdmustrun.Name = "rdmustrun";
            this.rdmustrun.Size = new System.Drawing.Size(73, 17);
            this.rdmustrun.TabIndex = 45;
            this.rdmustrun.Text = "Exchange";
            this.rdmustrun.UseVisualStyleBackColor = true;
            this.rdmustrun.CheckedChanged += new System.EventHandler(this.rdmustrun_CheckedChanged);
            // 
            // rdMustNet
            // 
            this.rdMustNet.AutoSize = true;
            this.rdMustNet.Location = new System.Drawing.Point(75, 42);
            this.rdMustNet.Name = "rdMustNet";
            this.rdMustNet.Size = new System.Drawing.Size(65, 17);
            this.rdMustNet.TabIndex = 45;
            this.rdMustNet.Text = "Contract";
            this.rdMustNet.UseVisualStyleBackColor = true;
            this.rdMustNet.CheckedChanged += new System.EventHandler(this.rdMustNet_CheckedChanged);
            // 
            // rdMustOff
            // 
            this.rdMustOff.AutoSize = true;
            this.rdMustOff.Location = new System.Drawing.Point(135, 19);
            this.rdMustOff.Name = "rdMustOff";
            this.rdMustOff.Size = new System.Drawing.Size(45, 17);
            this.rdMustOff.TabIndex = 45;
            this.rdMustOff.Text = "Hub";
            this.rdMustOff.UseVisualStyleBackColor = true;
            this.rdMustOff.CheckedChanged += new System.EventHandler(this.rdMustOff_CheckedChanged);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(262, 28);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(120, 20);
            this.textBox1.TabIndex = 46;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(218, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 48;
            this.label4.Text = "Power";
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::PowerPlantProject.Properties.Resources.deletered81;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Location = new System.Drawing.Point(490, 201);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(33, 23);
            this.button2.TabIndex = 71;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = global::PowerPlantProject.Properties.Resources.add89;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(437, 201);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 23);
            this.button1.TabIndex = 70;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.H1,
            this.H2,
            this.H3,
            this.H4,
            this.H5,
            this.H6,
            this.H7,
            this.H8,
            this.H9,
            this.H10,
            this.H11,
            this.H12,
            this.H13,
            this.H14,
            this.H15,
            this.H16,
            this.H17,
            this.H18,
            this.H19,
            this.H20,
            this.H21,
            this.H22,
            this.H23,
            this.H24});
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(25, 240);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(936, 60);
            this.dataGridView1.TabIndex = 69;
            // 
            // H1
            // 
            this.H1.HeaderText = "H1";
            this.H1.Name = "H1";
            // 
            // H2
            // 
            this.H2.HeaderText = "H2";
            this.H2.Name = "H2";
            // 
            // H3
            // 
            this.H3.HeaderText = "H3";
            this.H3.Name = "H3";
            // 
            // H4
            // 
            this.H4.HeaderText = "H4";
            this.H4.Name = "H4";
            // 
            // H5
            // 
            this.H5.HeaderText = "H5";
            this.H5.Name = "H5";
            // 
            // H6
            // 
            this.H6.HeaderText = "H6";
            this.H6.Name = "H6";
            // 
            // H7
            // 
            this.H7.HeaderText = "H7";
            this.H7.Name = "H7";
            // 
            // H8
            // 
            this.H8.HeaderText = "H8";
            this.H8.Name = "H8";
            // 
            // H9
            // 
            this.H9.HeaderText = "H9";
            this.H9.Name = "H9";
            // 
            // H10
            // 
            this.H10.HeaderText = "H10";
            this.H10.Name = "H10";
            // 
            // H11
            // 
            this.H11.HeaderText = "H11";
            this.H11.Name = "H11";
            // 
            // H12
            // 
            this.H12.HeaderText = "H12";
            this.H12.Name = "H12";
            // 
            // H13
            // 
            this.H13.HeaderText = "H13";
            this.H13.Name = "H13";
            // 
            // H14
            // 
            this.H14.HeaderText = "H14";
            this.H14.Name = "H14";
            // 
            // H15
            // 
            this.H15.HeaderText = "H15";
            this.H15.Name = "H15";
            // 
            // H16
            // 
            this.H16.HeaderText = "H16";
            this.H16.Name = "H16";
            // 
            // H17
            // 
            this.H17.HeaderText = "H17";
            this.H17.Name = "H17";
            // 
            // H18
            // 
            this.H18.HeaderText = "H18";
            this.H18.Name = "H18";
            // 
            // H19
            // 
            this.H19.HeaderText = "H19";
            this.H19.Name = "H19";
            // 
            // H20
            // 
            this.H20.HeaderText = "H20";
            this.H20.Name = "H20";
            // 
            // H21
            // 
            this.H21.HeaderText = "H21";
            this.H21.Name = "H21";
            // 
            // H22
            // 
            this.H22.HeaderText = "H22";
            this.H22.Name = "H22";
            // 
            // H23
            // 
            this.H23.HeaderText = "H23";
            this.H23.Name = "H23";
            // 
            // H24
            // 
            this.H24.HeaderText = "H24";
            this.H24.Name = "H24";
            // 
            // button3
            // 
            this.button3.BackgroundImage = global::PowerPlantProject.Properties.Resources.savemaeium;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Location = new System.Drawing.Point(463, 310);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(33, 28);
            this.button3.TabIndex = 72;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Hub_Contract
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(980, 619);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Hub_Contract";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hub_Contract";
            this.Load += new System.EventHandler(this.Hub_Contract_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewImageColumn delete;
        private System.Windows.Forms.GroupBox groupBox4;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerFrom;
        private System.Windows.Forms.Label label1;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cmbplant;
        private System.Windows.Forms.ComboBox cmbunit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbthour;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbfhour;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdmustrun;
        private System.Windows.Forms.RadioButton rdMustNet;
        private System.Windows.Forms.RadioButton rdMustOff;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn H1;
        private System.Windows.Forms.DataGridViewTextBoxColumn H2;
        private System.Windows.Forms.DataGridViewTextBoxColumn H3;
        private System.Windows.Forms.DataGridViewTextBoxColumn H4;
        private System.Windows.Forms.DataGridViewTextBoxColumn H5;
        private System.Windows.Forms.DataGridViewTextBoxColumn H6;
        private System.Windows.Forms.DataGridViewTextBoxColumn H7;
        private System.Windows.Forms.DataGridViewTextBoxColumn H8;
        private System.Windows.Forms.DataGridViewTextBoxColumn H9;
        private System.Windows.Forms.DataGridViewTextBoxColumn H10;
        private System.Windows.Forms.DataGridViewTextBoxColumn H11;
        private System.Windows.Forms.DataGridViewTextBoxColumn H12;
        private System.Windows.Forms.DataGridViewTextBoxColumn H13;
        private System.Windows.Forms.DataGridViewTextBoxColumn H14;
        private System.Windows.Forms.DataGridViewTextBoxColumn H15;
        private System.Windows.Forms.DataGridViewTextBoxColumn H16;
        private System.Windows.Forms.DataGridViewTextBoxColumn H17;
        private System.Windows.Forms.DataGridViewTextBoxColumn H18;
        private System.Windows.Forms.DataGridViewTextBoxColumn H19;
        private System.Windows.Forms.DataGridViewTextBoxColumn H20;
        private System.Windows.Forms.DataGridViewTextBoxColumn H21;
        private System.Windows.Forms.DataGridViewTextBoxColumn H22;
        private System.Windows.Forms.DataGridViewTextBoxColumn H23;
        private System.Windows.Forms.DataGridViewTextBoxColumn H24;
        private System.Windows.Forms.Button button3;

    }
}