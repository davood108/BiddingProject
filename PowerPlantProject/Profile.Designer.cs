﻿namespace PowerPlantProject
{
    partial class Profile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Profile));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txtg11status = new System.Windows.Forms.TextBox();
            this.txtg11out = new System.Windows.Forms.TextBox();
            this.txtg11fuel = new System.Windows.Forms.TextBox();
            this.txtg11avc = new System.Windows.Forms.TextBox();
            this.txtg11pmax = new System.Windows.Forms.TextBox();
            this.txtg11pmin = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txtg13status = new System.Windows.Forms.TextBox();
            this.txtg13out = new System.Windows.Forms.TextBox();
            this.txtg13fuel = new System.Windows.Forms.TextBox();
            this.txtg13avc = new System.Windows.Forms.TextBox();
            this.txtg13pmax = new System.Windows.Forms.TextBox();
            this.txtg13pmin = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.txtG14status = new System.Windows.Forms.TextBox();
            this.txtG14out = new System.Windows.Forms.TextBox();
            this.txtG14fuel = new System.Windows.Forms.TextBox();
            this.txG14avc = new System.Windows.Forms.TextBox();
            this.txtG14pmax = new System.Windows.Forms.TextBox();
            this.txtG14pmin = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label41 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.lblmarketpower = new System.Windows.Forms.Label();
            this.lbldate = new System.Windows.Forms.Label();
            this.lbllosspayment = new System.Windows.Forms.Label();
            this.lblcostpayment = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.lblminpower = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.lblcapbid = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.lblminprice = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.LBLMaxprice = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label42 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.ls2ok = new System.Windows.Forms.Panel();
            this.ls2cancel = new System.Windows.Forms.Panel();
            this.financok = new System.Windows.Forms.Panel();
            this.financecancel = new System.Windows.Forms.Panel();
            this.m002ok = new System.Windows.Forms.Panel();
            this.m005ok = new System.Windows.Forms.Panel();
            this.m002cancel = new System.Windows.Forms.Panel();
            this.m005cancel = new System.Windows.Forms.Panel();
            this.dispatchok = new System.Windows.Forms.Panel();
            this.dispatchcancel = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txt15sta = new System.Windows.Forms.TextBox();
            this.txt15out = new System.Windows.Forms.TextBox();
            this.txt15fuel = new System.Windows.Forms.TextBox();
            this.txt15avc = new System.Windows.Forms.TextBox();
            this.txt15pmax = new System.Windows.Forms.TextBox();
            this.TXT15PMIN = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tXT12STATUS = new System.Windows.Forms.TextBox();
            this.tXT12OUT = new System.Windows.Forms.TextBox();
            this.tXT12FUEL = new System.Windows.Forms.TextBox();
            this.txt12AVC = new System.Windows.Forms.TextBox();
            this.tXT12PMAX = new System.Windows.Forms.TextBox();
            this.txt12pmin = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txt16status = new System.Windows.Forms.TextBox();
            this.txt16out = new System.Windows.Forms.TextBox();
            this.txt16fuel = new System.Windows.Forms.TextBox();
            this.txt16avc = new System.Windows.Forms.TextBox();
            this.txt16pmax = new System.Windows.Forms.TextBox();
            this.txt16pmin = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel11.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.Location = new System.Drawing.Point(466, 262);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "CCORUMIEH POWER PLANT";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(396, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "GAS11";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(259, 272);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "GAS13";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Transparent;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.txtg11status);
            this.panel7.Controls.Add(this.txtg11out);
            this.panel7.Controls.Add(this.txtg11fuel);
            this.panel7.Controls.Add(this.txtg11avc);
            this.panel7.Controls.Add(this.txtg11pmax);
            this.panel7.Controls.Add(this.txtg11pmin);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Controls.Add(this.label8);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.label5);
            this.panel7.Location = new System.Drawing.Point(298, 99);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(247, 105);
            this.panel7.TabIndex = 6;
            // 
            // txtg11status
            // 
            this.txtg11status.Location = new System.Drawing.Point(167, 74);
            this.txtg11status.Name = "txtg11status";
            this.txtg11status.Size = new System.Drawing.Size(75, 20);
            this.txtg11status.TabIndex = 5;
            this.txtg11status.Text = "OFF";
            this.txtg11status.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtg11out
            // 
            this.txtg11out.Location = new System.Drawing.Point(86, 74);
            this.txtg11out.Name = "txtg11out";
            this.txtg11out.Size = new System.Drawing.Size(75, 20);
            this.txtg11out.TabIndex = 4;
            this.txtg11out.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtg11fuel
            // 
            this.txtg11fuel.Location = new System.Drawing.Point(166, 22);
            this.txtg11fuel.Name = "txtg11fuel";
            this.txtg11fuel.Size = new System.Drawing.Size(75, 20);
            this.txtg11fuel.TabIndex = 2;
            this.txtg11fuel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtg11avc
            // 
            this.txtg11avc.Location = new System.Drawing.Point(5, 74);
            this.txtg11avc.Name = "txtg11avc";
            this.txtg11avc.Size = new System.Drawing.Size(75, 20);
            this.txtg11avc.TabIndex = 3;
            this.txtg11avc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtg11pmax
            // 
            this.txtg11pmax.Location = new System.Drawing.Point(85, 22);
            this.txtg11pmax.Name = "txtg11pmax";
            this.txtg11pmax.Size = new System.Drawing.Size(75, 20);
            this.txtg11pmax.TabIndex = 1;
            this.txtg11pmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtg11pmin
            // 
            this.txtg11pmin.Location = new System.Drawing.Point(4, 22);
            this.txtg11pmin.Name = "txtg11pmin";
            this.txtg11pmin.Size = new System.Drawing.Size(75, 20);
            this.txtg11pmin.TabIndex = 0;
            this.txtg11pmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(179, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "STATUS";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(107, 58);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "OUT";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(25, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "AVC";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(185, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "FUEL";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(101, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "PMAX";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(22, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "PMIN";
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Location = new System.Drawing.Point(445, 429);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(76, 27);
            this.button4.TabIndex = 9;
            this.button4.Text = "BIDDING";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Transparent;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.txtg13status);
            this.panel8.Controls.Add(this.txtg13out);
            this.panel8.Controls.Add(this.txtg13fuel);
            this.panel8.Controls.Add(this.txtg13avc);
            this.panel8.Controls.Add(this.txtg13pmax);
            this.panel8.Controls.Add(this.txtg13pmin);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Controls.Add(this.label12);
            this.panel8.Controls.Add(this.label13);
            this.panel8.Controls.Add(this.label14);
            this.panel8.Controls.Add(this.label15);
            this.panel8.Controls.Add(this.label16);
            this.panel8.Location = new System.Drawing.Point(170, 306);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(245, 105);
            this.panel8.TabIndex = 12;
            // 
            // txtg13status
            // 
            this.txtg13status.Location = new System.Drawing.Point(167, 74);
            this.txtg13status.Name = "txtg13status";
            this.txtg13status.Size = new System.Drawing.Size(75, 20);
            this.txtg13status.TabIndex = 5;
            this.txtg13status.Text = "OFF";
            this.txtg13status.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtg13out
            // 
            this.txtg13out.Location = new System.Drawing.Point(86, 74);
            this.txtg13out.Name = "txtg13out";
            this.txtg13out.Size = new System.Drawing.Size(75, 20);
            this.txtg13out.TabIndex = 4;
            this.txtg13out.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtg13fuel
            // 
            this.txtg13fuel.Location = new System.Drawing.Point(166, 22);
            this.txtg13fuel.Name = "txtg13fuel";
            this.txtg13fuel.Size = new System.Drawing.Size(75, 20);
            this.txtg13fuel.TabIndex = 2;
            this.txtg13fuel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtg13avc
            // 
            this.txtg13avc.Location = new System.Drawing.Point(5, 74);
            this.txtg13avc.Name = "txtg13avc";
            this.txtg13avc.Size = new System.Drawing.Size(75, 20);
            this.txtg13avc.TabIndex = 3;
            this.txtg13avc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtg13pmax
            // 
            this.txtg13pmax.Location = new System.Drawing.Point(85, 22);
            this.txtg13pmax.Name = "txtg13pmax";
            this.txtg13pmax.Size = new System.Drawing.Size(75, 20);
            this.txtg13pmax.TabIndex = 1;
            this.txtg13pmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtg13pmin
            // 
            this.txtg13pmin.Location = new System.Drawing.Point(4, 22);
            this.txtg13pmin.Name = "txtg13pmin";
            this.txtg13pmin.Size = new System.Drawing.Size(75, 20);
            this.txtg13pmin.TabIndex = 0;
            this.txtg13pmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(179, 58);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "STATUS";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(107, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "OUT";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(25, 58);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "AVC";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(185, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "FUEL";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(101, 6);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "PMAX";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(22, 6);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(38, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "PMIN";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Transparent;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.txtG14status);
            this.panel9.Controls.Add(this.txtG14out);
            this.panel9.Controls.Add(this.txtG14fuel);
            this.panel9.Controls.Add(this.txG14avc);
            this.panel9.Controls.Add(this.txtG14pmax);
            this.panel9.Controls.Add(this.txtG14pmin);
            this.panel9.Controls.Add(this.label17);
            this.panel9.Controls.Add(this.label18);
            this.panel9.Controls.Add(this.label19);
            this.panel9.Controls.Add(this.label20);
            this.panel9.Controls.Add(this.label21);
            this.panel9.Controls.Add(this.label22);
            this.panel9.ForeColor = System.Drawing.Color.White;
            this.panel9.Location = new System.Drawing.Point(707, 317);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(247, 107);
            this.panel9.TabIndex = 13;
            // 
            // txtG14status
            // 
            this.txtG14status.Location = new System.Drawing.Point(166, 74);
            this.txtG14status.Name = "txtG14status";
            this.txtG14status.Size = new System.Drawing.Size(75, 20);
            this.txtG14status.TabIndex = 5;
            this.txtG14status.Text = "OFF";
            this.txtG14status.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtG14out
            // 
            this.txtG14out.Location = new System.Drawing.Point(85, 74);
            this.txtG14out.Name = "txtG14out";
            this.txtG14out.Size = new System.Drawing.Size(75, 20);
            this.txtG14out.TabIndex = 4;
            this.txtG14out.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtG14fuel
            // 
            this.txtG14fuel.Location = new System.Drawing.Point(165, 22);
            this.txtG14fuel.Name = "txtG14fuel";
            this.txtG14fuel.Size = new System.Drawing.Size(75, 20);
            this.txtG14fuel.TabIndex = 2;
            this.txtG14fuel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txG14avc
            // 
            this.txG14avc.Location = new System.Drawing.Point(4, 74);
            this.txG14avc.Name = "txG14avc";
            this.txG14avc.Size = new System.Drawing.Size(75, 20);
            this.txG14avc.TabIndex = 3;
            this.txG14avc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtG14pmax
            // 
            this.txtG14pmax.Location = new System.Drawing.Point(84, 22);
            this.txtG14pmax.Name = "txtG14pmax";
            this.txtG14pmax.Size = new System.Drawing.Size(75, 20);
            this.txtG14pmax.TabIndex = 1;
            this.txtG14pmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtG14pmin
            // 
            this.txtG14pmin.Location = new System.Drawing.Point(3, 22);
            this.txtG14pmin.Name = "txtG14pmin";
            this.txtG14pmin.Size = new System.Drawing.Size(75, 20);
            this.txtG14pmin.TabIndex = 0;
            this.txtG14pmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(178, 58);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "STATUS";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(106, 58);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(33, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "OUT";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(24, 58);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(31, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "AVC";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(184, 7);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(38, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "FUEL";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(100, 6);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "PMAX";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(21, 6);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(38, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "PMIN";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::PowerPlantProject.Properties.Resources._100848480727;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(445, 260);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(237, 196);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label41.ForeColor = System.Drawing.Color.White;
            this.label41.Location = new System.Drawing.Point(807, 288);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(46, 13);
            this.label41.TabIndex = 5;
            this.label41.Text = "GAS14";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Transparent;
            this.panel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.lblmarketpower);
            this.panel11.Controls.Add(this.lbldate);
            this.panel11.Controls.Add(this.lbllosspayment);
            this.panel11.Controls.Add(this.lblcostpayment);
            this.panel11.Controls.Add(this.label39);
            this.panel11.Controls.Add(this.lblminpower);
            this.panel11.Controls.Add(this.label37);
            this.panel11.Controls.Add(this.lblcapbid);
            this.panel11.Controls.Add(this.label35);
            this.panel11.Controls.Add(this.label34);
            this.panel11.Controls.Add(this.label32);
            this.panel11.Controls.Add(this.lblminprice);
            this.panel11.Controls.Add(this.label30);
            this.panel11.Controls.Add(this.label28);
            this.panel11.Controls.Add(this.label26);
            this.panel11.Controls.Add(this.LBLMaxprice);
            this.panel11.Controls.Add(this.label24);
            this.panel11.Location = new System.Drawing.Point(9, 482);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(269, 222);
            this.panel11.TabIndex = 15;
            this.panel11.Visible = false;
            // 
            // lblmarketpower
            // 
            this.lblmarketpower.AutoSize = true;
            this.lblmarketpower.ForeColor = System.Drawing.SystemColors.Info;
            this.lblmarketpower.Location = new System.Drawing.Point(176, 105);
            this.lblmarketpower.Name = "lblmarketpower";
            this.lblmarketpower.Size = new System.Drawing.Size(23, 13);
            this.lblmarketpower.TabIndex = 0;
            this.lblmarketpower.Text = "No";
            // 
            // lbldate
            // 
            this.lbldate.AutoSize = true;
            this.lbldate.ForeColor = System.Drawing.SystemColors.Info;
            this.lbldate.Location = new System.Drawing.Point(176, 85);
            this.lbldate.Name = "lbldate";
            this.lbldate.Size = new System.Drawing.Size(48, 13);
            this.lbldate.TabIndex = 0;
            this.lbldate.Text = "label24";
            // 
            // lbllosspayment
            // 
            this.lbllosspayment.AutoSize = true;
            this.lbllosspayment.ForeColor = System.Drawing.SystemColors.Info;
            this.lbllosspayment.Location = new System.Drawing.Point(177, 193);
            this.lbllosspayment.Name = "lbllosspayment";
            this.lbllosspayment.Size = new System.Drawing.Size(23, 13);
            this.lbllosspayment.TabIndex = 0;
            this.lbllosspayment.Text = "No";
            // 
            // lblcostpayment
            // 
            this.lblcostpayment.AutoSize = true;
            this.lblcostpayment.ForeColor = System.Drawing.SystemColors.Info;
            this.lblcostpayment.Location = new System.Drawing.Point(176, 171);
            this.lblcostpayment.Name = "lblcostpayment";
            this.lblcostpayment.Size = new System.Drawing.Size(23, 13);
            this.lblcostpayment.TabIndex = 0;
            this.lblcostpayment.Text = "No";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.ForeColor = System.Drawing.SystemColors.Info;
            this.label39.Location = new System.Drawing.Point(56, 193);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(93, 13);
            this.label39.TabIndex = 0;
            this.label39.Text = "Loss Payment :";
            // 
            // lblminpower
            // 
            this.lblminpower.AutoSize = true;
            this.lblminpower.ForeColor = System.Drawing.SystemColors.Info;
            this.lblminpower.Location = new System.Drawing.Point(176, 149);
            this.lblminpower.Name = "lblminpower";
            this.lblminpower.Size = new System.Drawing.Size(23, 13);
            this.lblminpower.TabIndex = 0;
            this.lblminpower.Text = "No";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.ForeColor = System.Drawing.SystemColors.Info;
            this.label37.Location = new System.Drawing.Point(55, 171);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(92, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "Cost Payment :";
            // 
            // lblcapbid
            // 
            this.lblcapbid.AutoSize = true;
            this.lblcapbid.ForeColor = System.Drawing.SystemColors.Info;
            this.lblcapbid.Location = new System.Drawing.Point(177, 127);
            this.lblcapbid.Name = "lblcapbid";
            this.lblcapbid.Size = new System.Drawing.Size(23, 13);
            this.lblcapbid.TabIndex = 0;
            this.lblcapbid.Text = "No";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.ForeColor = System.Drawing.SystemColors.Info;
            this.label35.Location = new System.Drawing.Point(55, 149);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(74, 13);
            this.label35.TabIndex = 0;
            this.label35.Text = "Min Power :";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.ForeColor = System.Drawing.SystemColors.Info;
            this.label34.Location = new System.Drawing.Point(55, 127);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(59, 13);
            this.label34.TabIndex = 0;
            this.label34.Text = "Cap Bid :";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.ForeColor = System.Drawing.SystemColors.Info;
            this.label32.Location = new System.Drawing.Point(55, 105);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(93, 13);
            this.label32.TabIndex = 0;
            this.label32.Text = "Market Power :";
            // 
            // lblminprice
            // 
            this.lblminprice.AutoSize = true;
            this.lblminprice.ForeColor = System.Drawing.SystemColors.Info;
            this.lblminprice.Location = new System.Drawing.Point(177, 35);
            this.lblminprice.Name = "lblminprice";
            this.lblminprice.Size = new System.Drawing.Size(48, 13);
            this.lblminprice.TabIndex = 0;
            this.lblminprice.Text = "label24";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.SystemColors.Info;
            this.label30.Location = new System.Drawing.Point(54, 85);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(115, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "Max Bidding Date :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.SystemColors.Info;
            this.label28.Location = new System.Drawing.Point(6, 62);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(57, 13);
            this.label28.TabIndex = 0;
            this.label28.Text = "Bidding :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.Info;
            this.label26.Location = new System.Drawing.Point(6, 35);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(111, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Market Price Min :";
            // 
            // LBLMaxprice
            // 
            this.LBLMaxprice.AutoSize = true;
            this.LBLMaxprice.ForeColor = System.Drawing.SystemColors.Info;
            this.LBLMaxprice.Location = new System.Drawing.Point(177, 9);
            this.LBLMaxprice.Name = "LBLMaxprice";
            this.LBLMaxprice.Size = new System.Drawing.Size(48, 13);
            this.LBLMaxprice.TabIndex = 0;
            this.LBLMaxprice.Text = "label24";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.SystemColors.Info;
            this.label24.Location = new System.Drawing.Point(6, 9);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(106, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Market Price Max";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label42
            // 
            this.label42.BackColor = System.Drawing.Color.White;
            this.label42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label42.Location = new System.Drawing.Point(849, 20);
            this.label42.Name = "label42";
            this.label42.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label42.Size = new System.Drawing.Size(150, 21);
            this.label42.TabIndex = 16;
            this.label42.Tag = "";
            this.label42.Text = "label42";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(28, 20);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(56, 13);
            this.label25.TabIndex = 17;
            this.label25.Text = "Ls2Plant";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(28, 62);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(58, 13);
            this.label27.TabIndex = 17;
            this.label27.Text = "Financial";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(39, 94);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(38, 13);
            this.label29.TabIndex = 17;
            this.label29.Text = "M002";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.ForeColor = System.Drawing.Color.White;
            this.label31.Location = new System.Drawing.Point(39, 134);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(38, 13);
            this.label31.TabIndex = 17;
            this.label31.Text = "M005";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(39, 173);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(57, 13);
            this.label33.TabIndex = 17;
            this.label33.Text = "Dispatch";
            // 
            // ls2ok
            // 
            this.ls2ok.BackColor = System.Drawing.Color.Transparent;
            this.ls2ok.BackgroundImage = global::PowerPlantProject.Properties.Resources.yes;
            this.ls2ok.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ls2ok.Location = new System.Drawing.Point(113, 13);
            this.ls2ok.Name = "ls2ok";
            this.ls2ok.Size = new System.Drawing.Size(34, 28);
            this.ls2ok.TabIndex = 18;
            // 
            // ls2cancel
            // 
            this.ls2cancel.BackColor = System.Drawing.Color.Transparent;
            this.ls2cancel.BackgroundImage = global::PowerPlantProject.Properties.Resources.dialog_cancel;
            this.ls2cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ls2cancel.Location = new System.Drawing.Point(151, 13);
            this.ls2cancel.Name = "ls2cancel";
            this.ls2cancel.Size = new System.Drawing.Size(34, 28);
            this.ls2cancel.TabIndex = 18;
            // 
            // financok
            // 
            this.financok.BackColor = System.Drawing.Color.Transparent;
            this.financok.BackgroundImage = global::PowerPlantProject.Properties.Resources.yes;
            this.financok.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.financok.Location = new System.Drawing.Point(113, 51);
            this.financok.Name = "financok";
            this.financok.Size = new System.Drawing.Size(34, 28);
            this.financok.TabIndex = 18;
            // 
            // financecancel
            // 
            this.financecancel.BackColor = System.Drawing.Color.Transparent;
            this.financecancel.BackgroundImage = global::PowerPlantProject.Properties.Resources.dialog_cancel;
            this.financecancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.financecancel.Location = new System.Drawing.Point(151, 51);
            this.financecancel.Name = "financecancel";
            this.financecancel.Size = new System.Drawing.Size(34, 28);
            this.financecancel.TabIndex = 18;
            // 
            // m002ok
            // 
            this.m002ok.BackColor = System.Drawing.Color.Transparent;
            this.m002ok.BackgroundImage = global::PowerPlantProject.Properties.Resources.yes;
            this.m002ok.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.m002ok.Location = new System.Drawing.Point(113, 88);
            this.m002ok.Name = "m002ok";
            this.m002ok.Size = new System.Drawing.Size(34, 28);
            this.m002ok.TabIndex = 18;
            // 
            // m005ok
            // 
            this.m005ok.BackColor = System.Drawing.Color.Transparent;
            this.m005ok.BackgroundImage = global::PowerPlantProject.Properties.Resources.yes;
            this.m005ok.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.m005ok.Location = new System.Drawing.Point(113, 125);
            this.m005ok.Name = "m005ok";
            this.m005ok.Size = new System.Drawing.Size(34, 28);
            this.m005ok.TabIndex = 18;
            // 
            // m002cancel
            // 
            this.m002cancel.BackColor = System.Drawing.Color.Transparent;
            this.m002cancel.BackgroundImage = global::PowerPlantProject.Properties.Resources.dialog_cancel;
            this.m002cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.m002cancel.Location = new System.Drawing.Point(151, 88);
            this.m002cancel.Name = "m002cancel";
            this.m002cancel.Size = new System.Drawing.Size(34, 28);
            this.m002cancel.TabIndex = 18;
            // 
            // m005cancel
            // 
            this.m005cancel.BackColor = System.Drawing.Color.Transparent;
            this.m005cancel.BackgroundImage = global::PowerPlantProject.Properties.Resources.dialog_cancel;
            this.m005cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.m005cancel.Location = new System.Drawing.Point(151, 125);
            this.m005cancel.Name = "m005cancel";
            this.m005cancel.Size = new System.Drawing.Size(34, 28);
            this.m005cancel.TabIndex = 18;
            // 
            // dispatchok
            // 
            this.dispatchok.BackColor = System.Drawing.Color.Transparent;
            this.dispatchok.BackgroundImage = global::PowerPlantProject.Properties.Resources.yes;
            this.dispatchok.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.dispatchok.Location = new System.Drawing.Point(113, 166);
            this.dispatchok.Name = "dispatchok";
            this.dispatchok.Size = new System.Drawing.Size(34, 28);
            this.dispatchok.TabIndex = 18;
            // 
            // dispatchcancel
            // 
            this.dispatchcancel.BackColor = System.Drawing.Color.Transparent;
            this.dispatchcancel.BackgroundImage = global::PowerPlantProject.Properties.Resources.dialog_cancel;
            this.dispatchcancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.dispatchcancel.Location = new System.Drawing.Point(151, 166);
            this.dispatchcancel.Name = "dispatchcancel";
            this.dispatchcancel.Size = new System.Drawing.Size(34, 28);
            this.dispatchcancel.TabIndex = 18;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.Transparent;
            this.panel20.Controls.Add(this.ls2ok);
            this.panel20.Controls.Add(this.ls2cancel);
            this.panel20.Controls.Add(this.dispatchcancel);
            this.panel20.Controls.Add(this.m005cancel);
            this.panel20.Controls.Add(this.m002cancel);
            this.panel20.Controls.Add(this.financecancel);
            this.panel20.Controls.Add(this.label25);
            this.panel20.Controls.Add(this.dispatchok);
            this.panel20.Controls.Add(this.label27);
            this.panel20.Controls.Add(this.m005ok);
            this.panel20.Controls.Add(this.label29);
            this.panel20.Controls.Add(this.financok);
            this.panel20.Controls.Add(this.label31);
            this.panel20.Controls.Add(this.m002ok);
            this.panel20.Controls.Add(this.label33);
            this.panel20.Location = new System.Drawing.Point(12, 20);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(200, 210);
            this.panel20.TabIndex = 19;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.ForeColor = System.Drawing.Color.White;
            this.linkLabel1.LinkColor = System.Drawing.Color.White;
            this.linkLabel1.Location = new System.Drawing.Point(901, 53);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(45, 13);
            this.linkLabel1.TabIndex = 20;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Report";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(414, 492);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "GAS15";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txt15sta);
            this.panel1.Controls.Add(this.txt15out);
            this.panel1.Controls.Add(this.txt15fuel);
            this.panel1.Controls.Add(this.txt15avc);
            this.panel1.Controls.Add(this.txt15pmax);
            this.panel1.Controls.Add(this.TXT15PMIN);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label36);
            this.panel1.Controls.Add(this.label38);
            this.panel1.Controls.Add(this.label40);
            this.panel1.Controls.Add(this.label43);
            this.panel1.Controls.Add(this.label44);
            this.panel1.Location = new System.Drawing.Point(314, 526);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(245, 105);
            this.panel1.TabIndex = 12;
            // 
            // txt15sta
            // 
            this.txt15sta.Location = new System.Drawing.Point(167, 74);
            this.txt15sta.Name = "txt15sta";
            this.txt15sta.Size = new System.Drawing.Size(75, 20);
            this.txt15sta.TabIndex = 5;
            this.txt15sta.Text = "OFF";
            this.txt15sta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt15out
            // 
            this.txt15out.Location = new System.Drawing.Point(86, 74);
            this.txt15out.Name = "txt15out";
            this.txt15out.Size = new System.Drawing.Size(75, 20);
            this.txt15out.TabIndex = 4;
            this.txt15out.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt15fuel
            // 
            this.txt15fuel.Location = new System.Drawing.Point(166, 22);
            this.txt15fuel.Name = "txt15fuel";
            this.txt15fuel.Size = new System.Drawing.Size(75, 20);
            this.txt15fuel.TabIndex = 2;
            this.txt15fuel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt15avc
            // 
            this.txt15avc.Location = new System.Drawing.Point(5, 74);
            this.txt15avc.Name = "txt15avc";
            this.txt15avc.Size = new System.Drawing.Size(75, 20);
            this.txt15avc.TabIndex = 3;
            this.txt15avc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt15pmax
            // 
            this.txt15pmax.Location = new System.Drawing.Point(85, 22);
            this.txt15pmax.Name = "txt15pmax";
            this.txt15pmax.Size = new System.Drawing.Size(75, 20);
            this.txt15pmax.TabIndex = 1;
            this.txt15pmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TXT15PMIN
            // 
            this.TXT15PMIN.Location = new System.Drawing.Point(4, 22);
            this.TXT15PMIN.Name = "TXT15PMIN";
            this.TXT15PMIN.Size = new System.Drawing.Size(75, 20);
            this.TXT15PMIN.TabIndex = 0;
            this.TXT15PMIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(179, 58);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "STATUS";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.ForeColor = System.Drawing.Color.White;
            this.label36.Location = new System.Drawing.Point(107, 58);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(33, 13);
            this.label36.TabIndex = 0;
            this.label36.Text = "OUT";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Location = new System.Drawing.Point(25, 58);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(31, 13);
            this.label38.TabIndex = 0;
            this.label38.Text = "AVC";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(185, 7);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(38, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "FUEL";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(101, 6);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(41, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "PMAX";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(22, 6);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(38, 13);
            this.label44.TabIndex = 0;
            this.label44.Text = "PMIN";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(648, 71);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(46, 13);
            this.label45.TabIndex = 5;
            this.label45.Text = "GAS12";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.tXT12STATUS);
            this.panel2.Controls.Add(this.tXT12OUT);
            this.panel2.Controls.Add(this.tXT12FUEL);
            this.panel2.Controls.Add(this.txt12AVC);
            this.panel2.Controls.Add(this.tXT12PMAX);
            this.panel2.Controls.Add(this.txt12pmin);
            this.panel2.Controls.Add(this.label46);
            this.panel2.Controls.Add(this.label47);
            this.panel2.Controls.Add(this.label48);
            this.panel2.Controls.Add(this.label49);
            this.panel2.Controls.Add(this.label50);
            this.panel2.Controls.Add(this.label51);
            this.panel2.Location = new System.Drawing.Point(564, 101);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(245, 105);
            this.panel2.TabIndex = 12;
            // 
            // tXT12STATUS
            // 
            this.tXT12STATUS.Location = new System.Drawing.Point(167, 74);
            this.tXT12STATUS.Name = "tXT12STATUS";
            this.tXT12STATUS.Size = new System.Drawing.Size(75, 20);
            this.tXT12STATUS.TabIndex = 5;
            this.tXT12STATUS.Text = "OFF";
            this.tXT12STATUS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tXT12OUT
            // 
            this.tXT12OUT.Location = new System.Drawing.Point(86, 74);
            this.tXT12OUT.Name = "tXT12OUT";
            this.tXT12OUT.Size = new System.Drawing.Size(75, 20);
            this.tXT12OUT.TabIndex = 4;
            this.tXT12OUT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tXT12FUEL
            // 
            this.tXT12FUEL.Location = new System.Drawing.Point(166, 22);
            this.tXT12FUEL.Name = "tXT12FUEL";
            this.tXT12FUEL.Size = new System.Drawing.Size(75, 20);
            this.tXT12FUEL.TabIndex = 2;
            this.tXT12FUEL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt12AVC
            // 
            this.txt12AVC.Location = new System.Drawing.Point(5, 74);
            this.txt12AVC.Name = "txt12AVC";
            this.txt12AVC.Size = new System.Drawing.Size(75, 20);
            this.txt12AVC.TabIndex = 3;
            this.txt12AVC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tXT12PMAX
            // 
            this.tXT12PMAX.Location = new System.Drawing.Point(85, 22);
            this.tXT12PMAX.Name = "tXT12PMAX";
            this.tXT12PMAX.Size = new System.Drawing.Size(75, 20);
            this.tXT12PMAX.TabIndex = 1;
            this.tXT12PMAX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt12pmin
            // 
            this.txt12pmin.Location = new System.Drawing.Point(4, 22);
            this.txt12pmin.Name = "txt12pmin";
            this.txt12pmin.Size = new System.Drawing.Size(75, 20);
            this.txt12pmin.TabIndex = 0;
            this.txt12pmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.ForeColor = System.Drawing.Color.White;
            this.label46.Location = new System.Drawing.Point(179, 58);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(56, 13);
            this.label46.TabIndex = 0;
            this.label46.Text = "STATUS";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.ForeColor = System.Drawing.Color.White;
            this.label47.Location = new System.Drawing.Point(107, 58);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(33, 13);
            this.label47.TabIndex = 0;
            this.label47.Text = "OUT";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.ForeColor = System.Drawing.Color.White;
            this.label48.Location = new System.Drawing.Point(25, 58);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(31, 13);
            this.label48.TabIndex = 0;
            this.label48.Text = "AVC";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(185, 7);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(38, 13);
            this.label49.TabIndex = 0;
            this.label49.Text = "FUEL";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.ForeColor = System.Drawing.Color.White;
            this.label50.Location = new System.Drawing.Point(101, 6);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(41, 13);
            this.label50.TabIndex = 0;
            this.label50.Text = "PMAX";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.ForeColor = System.Drawing.Color.White;
            this.label51.Location = new System.Drawing.Point(22, 6);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(38, 13);
            this.label51.TabIndex = 0;
            this.label51.Text = "PMIN";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label52.ForeColor = System.Drawing.Color.White;
            this.label52.Location = new System.Drawing.Point(654, 492);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(46, 13);
            this.label52.TabIndex = 5;
            this.label52.Text = "GAS16";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.txt16status);
            this.panel3.Controls.Add(this.txt16out);
            this.panel3.Controls.Add(this.txt16fuel);
            this.panel3.Controls.Add(this.txt16avc);
            this.panel3.Controls.Add(this.txt16pmax);
            this.panel3.Controls.Add(this.txt16pmin);
            this.panel3.Controls.Add(this.label53);
            this.panel3.Controls.Add(this.label54);
            this.panel3.Controls.Add(this.label55);
            this.panel3.Controls.Add(this.label56);
            this.panel3.Controls.Add(this.label57);
            this.panel3.Controls.Add(this.label58);
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(575, 526);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(245, 105);
            this.panel3.TabIndex = 12;
            // 
            // txt16status
            // 
            this.txt16status.Location = new System.Drawing.Point(167, 74);
            this.txt16status.Name = "txt16status";
            this.txt16status.Size = new System.Drawing.Size(75, 20);
            this.txt16status.TabIndex = 5;
            this.txt16status.Text = "OFF";
            this.txt16status.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt16out
            // 
            this.txt16out.Location = new System.Drawing.Point(86, 74);
            this.txt16out.Name = "txt16out";
            this.txt16out.Size = new System.Drawing.Size(75, 20);
            this.txt16out.TabIndex = 4;
            this.txt16out.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt16fuel
            // 
            this.txt16fuel.Location = new System.Drawing.Point(166, 22);
            this.txt16fuel.Name = "txt16fuel";
            this.txt16fuel.Size = new System.Drawing.Size(75, 20);
            this.txt16fuel.TabIndex = 2;
            this.txt16fuel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt16avc
            // 
            this.txt16avc.Location = new System.Drawing.Point(5, 74);
            this.txt16avc.Name = "txt16avc";
            this.txt16avc.Size = new System.Drawing.Size(75, 20);
            this.txt16avc.TabIndex = 3;
            this.txt16avc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt16pmax
            // 
            this.txt16pmax.Location = new System.Drawing.Point(85, 22);
            this.txt16pmax.Name = "txt16pmax";
            this.txt16pmax.Size = new System.Drawing.Size(75, 20);
            this.txt16pmax.TabIndex = 1;
            this.txt16pmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt16pmin
            // 
            this.txt16pmin.Location = new System.Drawing.Point(4, 22);
            this.txt16pmin.Name = "txt16pmin";
            this.txt16pmin.Size = new System.Drawing.Size(75, 20);
            this.txt16pmin.TabIndex = 0;
            this.txt16pmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.ForeColor = System.Drawing.Color.White;
            this.label53.Location = new System.Drawing.Point(179, 58);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(56, 13);
            this.label53.TabIndex = 0;
            this.label53.Text = "STATUS";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.ForeColor = System.Drawing.Color.White;
            this.label54.Location = new System.Drawing.Point(107, 58);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(33, 13);
            this.label54.TabIndex = 0;
            this.label54.Text = "OUT";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.ForeColor = System.Drawing.Color.White;
            this.label55.Location = new System.Drawing.Point(25, 58);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(31, 13);
            this.label55.TabIndex = 0;
            this.label55.Text = "AVC";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.ForeColor = System.Drawing.Color.White;
            this.label56.Location = new System.Drawing.Point(185, 7);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(38, 13);
            this.label56.TabIndex = 0;
            this.label56.Text = "FUEL";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.ForeColor = System.Drawing.Color.White;
            this.label57.Location = new System.Drawing.Point(101, 6);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(41, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "PMAX";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.ForeColor = System.Drawing.Color.White;
            this.label58.Location = new System.Drawing.Point(22, 6);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(38, 13);
            this.label58.TabIndex = 0;
            this.label58.Text = "PMIN";
            // 
            // Profile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.BackgroundImage = global::PowerPlantProject.Properties.Resources._100848480725;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1028, 711);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.panel20);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Profile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Profile";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Profile_Load);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox txtg11pmax;
        private System.Windows.Forms.TextBox txtg11pmin;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtg11status;
        private System.Windows.Forms.TextBox txtg11out;
        private System.Windows.Forms.TextBox txtg11fuel;
        private System.Windows.Forms.TextBox txtg11avc;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox txtg13status;
        private System.Windows.Forms.TextBox txtg13out;
        private System.Windows.Forms.TextBox txtg13fuel;
        private System.Windows.Forms.TextBox txtg13avc;
        private System.Windows.Forms.TextBox txtg13pmax;
        private System.Windows.Forms.TextBox txtg13pmin;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox txtG14status;
        private System.Windows.Forms.TextBox txtG14out;
        private System.Windows.Forms.TextBox txtG14fuel;
        private System.Windows.Forms.TextBox txG14avc;
        private System.Windows.Forms.TextBox txtG14pmax;
        private System.Windows.Forms.TextBox txtG14pmin;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label lblmarketpower;
        private System.Windows.Forms.Label lbldate;
        private System.Windows.Forms.Label lbllosspayment;
        private System.Windows.Forms.Label lblcostpayment;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label lblminpower;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label lblcapbid;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label lblminprice;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label LBLMaxprice;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel ls2ok;
        private System.Windows.Forms.Panel ls2cancel;
        private System.Windows.Forms.Panel financok;
        private System.Windows.Forms.Panel financecancel;
        private System.Windows.Forms.Panel m002ok;
        private System.Windows.Forms.Panel m005ok;
        private System.Windows.Forms.Panel m002cancel;
        private System.Windows.Forms.Panel m005cancel;
        private System.Windows.Forms.Panel dispatchok;
        private System.Windows.Forms.Panel dispatchcancel;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txt15sta;
        private System.Windows.Forms.TextBox txt15out;
        private System.Windows.Forms.TextBox txt15fuel;
        private System.Windows.Forms.TextBox txt15avc;
        private System.Windows.Forms.TextBox txt15pmax;
        private System.Windows.Forms.TextBox TXT15PMIN;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tXT12STATUS;
        private System.Windows.Forms.TextBox tXT12OUT;
        private System.Windows.Forms.TextBox tXT12FUEL;
        private System.Windows.Forms.TextBox txt12AVC;
        private System.Windows.Forms.TextBox tXT12PMAX;
        private System.Windows.Forms.TextBox txt12pmin;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txt16status;
        private System.Windows.Forms.TextBox txt16out;
        private System.Windows.Forms.TextBox txt16fuel;
        private System.Windows.Forms.TextBox txt16avc;
        private System.Windows.Forms.TextBox txt16pmax;
        private System.Windows.Forms.TextBox txt16pmin;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
    }
}