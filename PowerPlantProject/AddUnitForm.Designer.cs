﻿namespace PowerPlantProject
{
    partial class AddUnitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddUnitForm));
            this.PackageTypeValid = new System.Windows.Forms.Label();
            this.ODSaveBtn = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.PackageCodeTb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.PackageType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.UnitType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.unitNumTb = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel4 = new System.Windows.Forms.Panel();
            this.UnitNameTb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtStatusexist = new System.Windows.Forms.TextBox();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // PackageTypeValid
            // 
            this.PackageTypeValid.AutoSize = true;
            this.PackageTypeValid.BackColor = System.Drawing.Color.Transparent;
            this.PackageTypeValid.ForeColor = System.Drawing.Color.Red;
            this.PackageTypeValid.Location = new System.Drawing.Point(0, 202);
            this.PackageTypeValid.Name = "PackageTypeValid";
            this.PackageTypeValid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PackageTypeValid.Size = new System.Drawing.Size(11, 13);
            this.PackageTypeValid.TabIndex = 1;
            this.PackageTypeValid.Text = "*";
            this.PackageTypeValid.Visible = false;
            // 
            // ODSaveBtn
            // 
            this.ODSaveBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.ODSaveBtn.BackgroundImage = global::PowerPlantProject.Properties.Resources.Savedata;
            this.ODSaveBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ODSaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ODSaveBtn.Location = new System.Drawing.Point(94, 252);
            this.ODSaveBtn.Name = "ODSaveBtn";
            this.ODSaveBtn.Size = new System.Drawing.Size(86, 29);
            this.ODSaveBtn.TabIndex = 0;
            this.ODSaveBtn.Text = "Save";
            this.ODSaveBtn.UseVisualStyleBackColor = false;
            this.ODSaveBtn.Click += new System.EventHandler(this.ODSaveBtn_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.CadetBlue;
            this.panel3.Controls.Add(this.PackageCodeTb);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(150, 177);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(118, 49);
            this.panel3.TabIndex = 3;
            // 
            // PackageCodeTb
            // 
            this.PackageCodeTb.BackColor = System.Drawing.Color.White;
            this.PackageCodeTb.Location = new System.Drawing.Point(9, 24);
            this.PackageCodeTb.Name = "PackageCodeTb";
            this.PackageCodeTb.Size = new System.Drawing.Size(99, 20);
            this.PackageCodeTb.TabIndex = 0;
            this.PackageCodeTb.Validated += new System.EventHandler(this.PackageCodeTb_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Window;
            this.label3.Location = new System.Drawing.Point(8, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Package Code";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.CadetBlue;
            this.panel2.Controls.Add(this.PackageType);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(12, 177);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(122, 49);
            this.panel2.TabIndex = 2;
            // 
            // PackageType
            // 
            this.PackageType.FormattingEnabled = true;
            this.PackageType.Items.AddRange(new object[] {
            "Gas",
            "Steam",
            "Combined Cycle"});
            this.PackageType.Location = new System.Drawing.Point(9, 23);
            this.PackageType.Name = "PackageType";
            this.PackageType.Size = new System.Drawing.Size(102, 21);
            this.PackageType.TabIndex = 0;
            this.PackageType.TextChanged += new System.EventHandler(this.PackageType_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Window;
            this.label2.Location = new System.Drawing.Point(14, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Package Type";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.CadetBlue;
            this.panel1.Controls.Add(this.UnitType);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(150, 108);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(118, 49);
            this.panel1.TabIndex = 1;
            // 
            // UnitType
            // 
            this.UnitType.FormattingEnabled = true;
            this.UnitType.Items.AddRange(new object[] {
            "Gas",
            "Steam"});
            this.UnitType.Location = new System.Drawing.Point(6, 21);
            this.UnitType.Name = "UnitType";
            this.UnitType.Size = new System.Drawing.Size(102, 21);
            this.UnitType.TabIndex = 0;
            this.UnitType.TextChanged += new System.EventHandler(this.UnitType_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Window;
            this.label1.Location = new System.Drawing.Point(22, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Unit Type";
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.CadetBlue;
            this.panel27.Controls.Add(this.unitNumTb);
            this.panel27.Controls.Add(this.label31);
            this.panel27.Location = new System.Drawing.Point(12, 107);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(122, 49);
            this.panel27.TabIndex = 0;
            // 
            // unitNumTb
            // 
            this.unitNumTb.BackColor = System.Drawing.Color.White;
            this.unitNumTb.Location = new System.Drawing.Point(10, 22);
            this.unitNumTb.Name = "unitNumTb";
            this.unitNumTb.Size = new System.Drawing.Size(99, 20);
            this.unitNumTb.TabIndex = 0;
            this.unitNumTb.TextChanged += new System.EventHandler(this.unitNumTb_TextChanged);
            this.unitNumTb.Validated += new System.EventHandler(this.unitNameTb_Validated);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.SystemColors.Window;
            this.label31.Location = new System.Drawing.Point(19, 6);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(77, 13);
            this.label31.TabIndex = 9;
            this.label31.Text = "Unit Number";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.CadetBlue;
            this.panel4.Controls.Add(this.UnitNameTb);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(78, 34);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(122, 49);
            this.panel4.TabIndex = 34;
            // 
            // UnitNameTb
            // 
            this.UnitNameTb.BackColor = System.Drawing.Color.White;
            this.UnitNameTb.Location = new System.Drawing.Point(9, 22);
            this.UnitNameTb.Name = "UnitNameTb";
            this.UnitNameTb.ReadOnly = true;
            this.UnitNameTb.Size = new System.Drawing.Size(99, 20);
            this.UnitNameTb.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Window;
            this.label4.Location = new System.Drawing.Point(19, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Unit Name";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(288, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(256, 13);
            this.label6.TabIndex = 39;
            this.label6.Text = "Please Add New Unit Based on Exist Units. ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(294, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 38;
            // 
            // txtStatusexist
            // 
            this.txtStatusexist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStatusexist.BackColor = System.Drawing.Color.LemonChiffon;
            this.txtStatusexist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStatusexist.Location = new System.Drawing.Point(278, 55);
            this.txtStatusexist.Multiline = true;
            this.txtStatusexist.Name = "txtStatusexist";
            this.txtStatusexist.ReadOnly = true;
            this.txtStatusexist.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtStatusexist.Size = new System.Drawing.Size(525, 226);
            this.txtStatusexist.TabIndex = 40;
            // 
            // AddUnitForm
            // 
            this.AcceptButton = this.ODSaveBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(809, 293);
            this.Controls.Add(this.txtStatusexist);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.PackageTypeValid);
            this.Controls.Add(this.ODSaveBtn);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel27);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AddUnitForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add New Unit";
            this.Load += new System.EventHandler(this.AddUnitForm_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label PackageTypeValid;
        private System.Windows.Forms.Button ODSaveBtn;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox PackageType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.TextBox unitNumTb;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox PackageCodeTb;
        private System.Windows.Forms.ComboBox UnitType;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox UnitNameTb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtStatusexist;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
    }
}