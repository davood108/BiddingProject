﻿namespace PowerPlantProject
{
    partial class StatusForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatusForm));
            this.dataGridStatus = new System.Windows.Forms.DataGridView();
            this.statusdatePicker = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridinternet = new System.Windows.Forms.DataGridView();
            this.Unitnetcomp = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Linenetcomp = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Reginnetcomp = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.InterchangedEnergy = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Manategh = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ProduceEnergy = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Rep12Page = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PostFiles = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridothers = new System.Windows.Forms.DataGridView();
            this.ChartPrice = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Lfc = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SaledEnergy = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dgbill = new System.Windows.Forms.DataGridView();
            this.Monthly = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Hub = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Transmission = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PlantName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Diapatcable = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.RealPower = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CostFunction = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.GeneralData = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.M002 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.M005 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.M005WithFuelLimmit = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridinternet)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridothers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgbill)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridStatus
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dataGridStatus.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridStatus.BackgroundColor = System.Drawing.Color.White;
            this.dataGridStatus.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dataGridStatus.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridStatus.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridStatus.ColumnHeadersHeight = 22;
            this.dataGridStatus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PlantName,
            this.Diapatcable,
            this.RealPower,
            this.CostFunction,
            this.GeneralData,
            this.M002,
            this.M005,
            this.M005WithFuelLimmit});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridStatus.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridStatus.EnableHeadersVisualStyles = false;
            this.dataGridStatus.GridColor = System.Drawing.Color.CadetBlue;
            this.dataGridStatus.Location = new System.Drawing.Point(165, 110);
            this.dataGridStatus.Name = "dataGridStatus";
            this.dataGridStatus.RowHeadersVisible = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LemonChiffon;
            this.dataGridStatus.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridStatus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridStatus.Size = new System.Drawing.Size(815, 141);
            this.dataGridStatus.TabIndex = 0;
            // 
            // statusdatePicker
            // 
            this.statusdatePicker.HasButtons = true;
            this.statusdatePicker.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.statusdatePicker.Location = new System.Drawing.Point(69, 15);
            this.statusdatePicker.Name = "statusdatePicker";
            this.statusdatePicker.Size = new System.Drawing.Size(116, 20);
            this.statusdatePicker.TabIndex = 15;
            this.statusdatePicker.ValueChanged += new System.EventHandler(this.statusdatePicker_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(15, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Date :";
            // 
            // dataGridinternet
            // 
            this.dataGridinternet.BackgroundColor = System.Drawing.Color.White;
            this.dataGridinternet.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dataGridinternet.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridinternet.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridinternet.ColumnHeadersHeight = 22;
            this.dataGridinternet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Unitnetcomp,
            this.Linenetcomp,
            this.Reginnetcomp,
            this.InterchangedEnergy,
            this.Manategh,
            this.ProduceEnergy,
            this.Rep12Page,
            this.PostFiles});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridinternet.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridinternet.EnableHeadersVisualStyles = false;
            this.dataGridinternet.GridColor = System.Drawing.Color.CadetBlue;
            this.dataGridinternet.Location = new System.Drawing.Point(165, 305);
            this.dataGridinternet.Name = "dataGridinternet";
            this.dataGridinternet.RowHeadersVisible = false;
            this.dataGridinternet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridinternet.Size = new System.Drawing.Size(815, 75);
            this.dataGridinternet.TabIndex = 17;
            // 
            // Unitnetcomp
            // 
            this.Unitnetcomp.HeaderText = "Unitnetcomp";
            this.Unitnetcomp.Name = "Unitnetcomp";
            // 
            // Linenetcomp
            // 
            this.Linenetcomp.HeaderText = "Linenetcomp";
            this.Linenetcomp.Name = "Linenetcomp";
            // 
            // Reginnetcomp
            // 
            this.Reginnetcomp.HeaderText = "Reginnetcomp";
            this.Reginnetcomp.Name = "Reginnetcomp";
            // 
            // InterchangedEnergy
            // 
            this.InterchangedEnergy.HeaderText = "InterchangeEnergy";
            this.InterchangedEnergy.Name = "InterchangedEnergy";
            // 
            // Manategh
            // 
            this.Manategh.HeaderText = "Manategh";
            this.Manategh.Name = "Manategh";
            // 
            // ProduceEnergy
            // 
            this.ProduceEnergy.HeaderText = "ProduceEnergy";
            this.ProduceEnergy.Name = "ProduceEnergy";
            // 
            // Rep12Page
            // 
            this.Rep12Page.HeaderText = "Rep12Page";
            this.Rep12Page.Name = "Rep12Page";
            // 
            // PostFiles
            // 
            this.PostFiles.HeaderText = "PostFiles";
            this.PostFiles.Name = "PostFiles";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(166, 271);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Internet Files -  Posts File :";
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(8, 8);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(139, 619);
            this.treeView1.TabIndex = 19;
            this.treeView1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.CadetBlue;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.statusdatePicker);
            this.panel1.Location = new System.Drawing.Point(416, 15);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 50);
            this.panel1.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label3.Location = new System.Drawing.Point(170, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Bidding Data :";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panel2.Controls.Add(this.treeView1);
            this.panel2.Location = new System.Drawing.Point(4, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(156, 630);
            this.panel2.TabIndex = 22;
            // 
            // dataGridothers
            // 
            this.dataGridothers.BackgroundColor = System.Drawing.Color.White;
            this.dataGridothers.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dataGridothers.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridothers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridothers.ColumnHeadersHeight = 22;
            this.dataGridothers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ChartPrice,
            this.Lfc,
            this.SaledEnergy});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridothers.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridothers.EnableHeadersVisualStyles = false;
            this.dataGridothers.GridColor = System.Drawing.Color.CadetBlue;
            this.dataGridothers.Location = new System.Drawing.Point(165, 429);
            this.dataGridothers.Name = "dataGridothers";
            this.dataGridothers.RowHeadersVisible = false;
            this.dataGridothers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridothers.Size = new System.Drawing.Size(815, 75);
            this.dataGridothers.TabIndex = 23;
            // 
            // ChartPrice
            // 
            this.ChartPrice.HeaderText = "ChartPrice";
            this.ChartPrice.Name = "ChartPrice";
            // 
            // Lfc
            // 
            this.Lfc.HeaderText = "Lfc";
            this.Lfc.Name = "Lfc";
            // 
            // SaledEnergy
            // 
            this.SaledEnergy.HeaderText = "SaledEnergy";
            this.SaledEnergy.Name = "SaledEnergy";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label4.Location = new System.Drawing.Point(166, 397);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Market Data :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label5.Location = new System.Drawing.Point(166, 520);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Bill Data :";
            // 
            // dgbill
            // 
            this.dgbill.BackgroundColor = System.Drawing.Color.White;
            this.dgbill.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgbill.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgbill.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgbill.ColumnHeadersHeight = 22;
            this.dgbill.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Monthly,
            this.Hub,
            this.Transmission});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgbill.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgbill.EnableHeadersVisualStyles = false;
            this.dgbill.GridColor = System.Drawing.Color.CadetBlue;
            this.dgbill.Location = new System.Drawing.Point(165, 554);
            this.dgbill.Name = "dgbill";
            this.dgbill.RowHeadersVisible = false;
            this.dgbill.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgbill.Size = new System.Drawing.Size(815, 75);
            this.dgbill.TabIndex = 25;
            // 
            // Monthly
            // 
            this.Monthly.HeaderText = "Monthly";
            this.Monthly.Name = "Monthly";
            // 
            // Hub
            // 
            this.Hub.HeaderText = "Hub";
            this.Hub.Name = "Hub";
            // 
            // Transmission
            // 
            this.Transmission.HeaderText = "Transmission";
            this.Transmission.Name = "Transmission";
            // 
            // PlantName
            // 
            this.PlantName.HeaderText = "PlantName";
            this.PlantName.Name = "PlantName";
            this.PlantName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Diapatcable
            // 
            this.Diapatcable.HeaderText = "Diapatcable";
            this.Diapatcable.Name = "Diapatcable";
            this.Diapatcable.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Diapatcable.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // RealPower
            // 
            this.RealPower.HeaderText = "RealPower";
            this.RealPower.Name = "RealPower";
            this.RealPower.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.RealPower.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // CostFunction
            // 
            this.CostFunction.HeaderText = "CostFunction";
            this.CostFunction.Name = "CostFunction";
            this.CostFunction.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CostFunction.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // GeneralData
            // 
            this.GeneralData.HeaderText = "GeneralData";
            this.GeneralData.Name = "GeneralData";
            this.GeneralData.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.GeneralData.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // M002
            // 
            this.M002.HeaderText = "M002";
            this.M002.Name = "M002";
            this.M002.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.M002.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // M005
            // 
            this.M005.HeaderText = "M005";
            this.M005.Name = "M005";
            this.M005.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.M005.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // M005WithFuelLimmit
            // 
            this.M005WithFuelLimmit.HeaderText = "M005WithFuelLimmit";
            this.M005WithFuelLimmit.Name = "M005WithFuelLimmit";
            // 
            // StatusForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(984, 639);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dgbill);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dataGridothers);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridinternet);
            this.Controls.Add(this.dataGridStatus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "StatusForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StatusForm";
            this.Load += new System.EventHandler(this.StatusForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridinternet)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridothers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgbill)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridStatus;
        private FarsiLibrary.Win.Controls.FADatePicker statusdatePicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridinternet;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridothers;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ChartPrice;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Lfc;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SaledEnergy;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Unitnetcomp;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Linenetcomp;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Reginnetcomp;
        private System.Windows.Forms.DataGridViewCheckBoxColumn InterchangedEnergy;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Manategh;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ProduceEnergy;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Rep12Page;
        private System.Windows.Forms.DataGridViewCheckBoxColumn PostFiles;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgbill;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Monthly;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Hub;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Transmission;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlantName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Diapatcable;
        private System.Windows.Forms.DataGridViewCheckBoxColumn RealPower;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CostFunction;
        private System.Windows.Forms.DataGridViewCheckBoxColumn GeneralData;
        private System.Windows.Forms.DataGridViewCheckBoxColumn M002;
        private System.Windows.Forms.DataGridViewCheckBoxColumn M005;
        private System.Windows.Forms.DataGridViewCheckBoxColumn M005WithFuelLimmit;
    }
}