﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
namespace PowerPlantProject
{
    public class FormColors
    {
        public Color Formbackcolor { get; set; }


        public Color Buttonbackcolor { get; set; }


        public Color Panelbackcolor { get; set; }


        public Color Textbackcolor { get; set; }


        public Color Gridbackcolor { get; set; }

        ///////////////////////////////////////

        static FormColors _self = null;

        private FormColors()
        {

        }

        public static FormColors GetColor()
        {
            if (_self == null)
                _self = new FormColors();
            return _self;
        }



    }
}
