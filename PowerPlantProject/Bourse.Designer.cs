﻿namespace PowerPlantProject
{
    partial class Bourse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Bourse));
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.delete = new System.Windows.Forms.DataGridViewImageColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.datePickerFrom = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.datePickerto = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbthour = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbfhour = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdpeak = new System.Windows.Forms.RadioButton();
            this.rdMedium = new System.Windows.Forms.RadioButton();
            this.rdBase = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.H1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.delete});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.Location = new System.Drawing.Point(20, 365);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.Size = new System.Drawing.Size(936, 151);
            this.dataGridView2.TabIndex = 67;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // delete
            // 
            this.delete.HeaderText = "Delete";
            this.delete.Image = global::PowerPlantProject.Properties.Resources.deletered81;
            this.delete.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.delete.Name = "delete";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.datePickerFrom);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.datePickerto);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Location = new System.Drawing.Point(288, 34);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(368, 68);
            this.groupBox4.TabIndex = 66;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Date";
            // 
            // datePickerFrom
            // 
            this.datePickerFrom.HasButtons = true;
            this.datePickerFrom.Location = new System.Drawing.Point(57, 31);
            this.datePickerFrom.Name = "datePickerFrom";
            this.datePickerFrom.Readonly = true;
            this.datePickerFrom.Size = new System.Drawing.Size(120, 20);
            this.datePickerFrom.TabIndex = 44;
            this.datePickerFrom.ValueChanged += new System.EventHandler(this.datePickerFrom_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 48;
            this.label1.Text = "From :";
            // 
            // datePickerto
            // 
            this.datePickerto.HasButtons = true;
            this.datePickerto.Location = new System.Drawing.Point(228, 31);
            this.datePickerto.Name = "datePickerto";
            this.datePickerto.Readonly = true;
            this.datePickerto.Size = new System.Drawing.Size(120, 20);
            this.datePickerto.TabIndex = 44;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(199, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 48;
            this.label7.Text = "To:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbthour);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cmbfhour);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(83, 108);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(368, 71);
            this.groupBox2.TabIndex = 64;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Hour";
            // 
            // cmbthour
            // 
            this.cmbthour.FormattingEnabled = true;
            this.cmbthour.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"});
            this.cmbthour.Location = new System.Drawing.Point(228, 23);
            this.cmbthour.Name = "cmbthour";
            this.cmbthour.Size = new System.Drawing.Size(63, 21);
            this.cmbthour.TabIndex = 49;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(202, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 48;
            this.label6.Text = "To";
            // 
            // cmbfhour
            // 
            this.cmbfhour.FormattingEnabled = true;
            this.cmbfhour.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"});
            this.cmbfhour.Location = new System.Drawing.Point(70, 23);
            this.cmbfhour.Name = "cmbfhour";
            this.cmbfhour.Size = new System.Drawing.Size(62, 21);
            this.cmbfhour.TabIndex = 49;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 48;
            this.label5.Text = "From";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdpeak);
            this.groupBox1.Controls.Add(this.rdMedium);
            this.groupBox1.Controls.Add(this.rdBase);
            this.groupBox1.Location = new System.Drawing.Point(490, 108);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(404, 71);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            // 
            // rdpeak
            // 
            this.rdpeak.AutoSize = true;
            this.rdpeak.Location = new System.Drawing.Point(59, 27);
            this.rdpeak.Name = "rdpeak";
            this.rdpeak.Size = new System.Drawing.Size(50, 17);
            this.rdpeak.TabIndex = 45;
            this.rdpeak.Text = "Peak";
            this.rdpeak.UseVisualStyleBackColor = true;
            // 
            // rdMedium
            // 
            this.rdMedium.AutoSize = true;
            this.rdMedium.Location = new System.Drawing.Point(284, 27);
            this.rdMedium.Name = "rdMedium";
            this.rdMedium.Size = new System.Drawing.Size(62, 17);
            this.rdMedium.TabIndex = 45;
            this.rdMedium.Text = "Medium";
            this.rdMedium.UseVisualStyleBackColor = true;
            // 
            // rdBase
            // 
            this.rdBase.AutoSize = true;
            this.rdBase.Location = new System.Drawing.Point(170, 27);
            this.rdBase.Name = "rdBase";
            this.rdBase.Size = new System.Drawing.Size(49, 17);
            this.rdBase.TabIndex = 45;
            this.rdBase.Text = "Base";
            this.rdBase.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::PowerPlantProject.Properties.Resources.deletered81;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Location = new System.Drawing.Point(477, 207);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(33, 23);
            this.button2.TabIndex = 62;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.Color.Transparent;
            this.btnadd.BackgroundImage = global::PowerPlantProject.Properties.Resources.add89;
            this.btnadd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnadd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnadd.Location = new System.Drawing.Point(437, 207);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(33, 23);
            this.btnadd.TabIndex = 61;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::PowerPlantProject.Properties.Resources.savemaeium;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(452, 325);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(35, 24);
            this.button1.TabIndex = 60;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.H1,
            this.H2,
            this.H3,
            this.H4,
            this.H5,
            this.H6,
            this.H7,
            this.H8,
            this.H9,
            this.H10,
            this.H11,
            this.H12,
            this.H13,
            this.H14,
            this.H15,
            this.H16,
            this.H17,
            this.H18,
            this.H19,
            this.H20,
            this.H21,
            this.H22,
            this.H23,
            this.H24});
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(20, 245);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(936, 60);
            this.dataGridView1.TabIndex = 59;
            // 
            // H1
            // 
            this.H1.HeaderText = "H1";
            this.H1.Name = "H1";
            // 
            // H2
            // 
            this.H2.HeaderText = "H2";
            this.H2.Name = "H2";
            // 
            // H3
            // 
            this.H3.HeaderText = "H3";
            this.H3.Name = "H3";
            // 
            // H4
            // 
            this.H4.HeaderText = "H4";
            this.H4.Name = "H4";
            // 
            // H5
            // 
            this.H5.HeaderText = "H5";
            this.H5.Name = "H5";
            // 
            // H6
            // 
            this.H6.HeaderText = "H6";
            this.H6.Name = "H6";
            // 
            // H7
            // 
            this.H7.HeaderText = "H7";
            this.H7.Name = "H7";
            // 
            // H8
            // 
            this.H8.HeaderText = "H8";
            this.H8.Name = "H8";
            // 
            // H9
            // 
            this.H9.HeaderText = "H9";
            this.H9.Name = "H9";
            // 
            // H10
            // 
            this.H10.HeaderText = "H10";
            this.H10.Name = "H10";
            // 
            // H11
            // 
            this.H11.HeaderText = "H11";
            this.H11.Name = "H11";
            // 
            // H12
            // 
            this.H12.HeaderText = "H12";
            this.H12.Name = "H12";
            // 
            // H13
            // 
            this.H13.HeaderText = "H13";
            this.H13.Name = "H13";
            // 
            // H14
            // 
            this.H14.HeaderText = "H14";
            this.H14.Name = "H14";
            // 
            // H15
            // 
            this.H15.HeaderText = "H15";
            this.H15.Name = "H15";
            // 
            // H16
            // 
            this.H16.HeaderText = "H16";
            this.H16.Name = "H16";
            // 
            // H17
            // 
            this.H17.HeaderText = "H17";
            this.H17.Name = "H17";
            // 
            // H18
            // 
            this.H18.HeaderText = "H18";
            this.H18.Name = "H18";
            // 
            // H19
            // 
            this.H19.HeaderText = "H19";
            this.H19.Name = "H19";
            // 
            // H20
            // 
            this.H20.HeaderText = "H20";
            this.H20.Name = "H20";
            // 
            // H21
            // 
            this.H21.HeaderText = "H21";
            this.H21.Name = "H21";
            // 
            // H22
            // 
            this.H22.HeaderText = "H22";
            this.H22.Name = "H22";
            // 
            // H23
            // 
            this.H23.HeaderText = "H23";
            this.H23.Name = "H23";
            // 
            // H24
            // 
            this.H24.HeaderText = "H24";
            this.H24.Name = "H24";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::PowerPlantProject.Properties.Resources.yes;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Location = new System.Drawing.Point(899, 522);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(57, 30);
            this.panel1.TabIndex = 71;
            this.panel1.Visible = false;
            this.panel1.Click += new System.EventHandler(this.panel1_Click);
            // 
            // Bourse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 561);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnadd);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Bourse";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bourse";
            this.Load += new System.EventHandler(this.Bourse_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewImageColumn delete;
        private System.Windows.Forms.GroupBox groupBox4;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerFrom;
        private System.Windows.Forms.Label label1;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbthour;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbfhour;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdpeak;
        private System.Windows.Forms.RadioButton rdMedium;
        private System.Windows.Forms.RadioButton rdBase;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn H1;
        private System.Windows.Forms.DataGridViewTextBoxColumn H2;
        private System.Windows.Forms.DataGridViewTextBoxColumn H3;
        private System.Windows.Forms.DataGridViewTextBoxColumn H4;
        private System.Windows.Forms.DataGridViewTextBoxColumn H5;
        private System.Windows.Forms.DataGridViewTextBoxColumn H6;
        private System.Windows.Forms.DataGridViewTextBoxColumn H7;
        private System.Windows.Forms.DataGridViewTextBoxColumn H8;
        private System.Windows.Forms.DataGridViewTextBoxColumn H9;
        private System.Windows.Forms.DataGridViewTextBoxColumn H10;
        private System.Windows.Forms.DataGridViewTextBoxColumn H11;
        private System.Windows.Forms.DataGridViewTextBoxColumn H12;
        private System.Windows.Forms.DataGridViewTextBoxColumn H13;
        private System.Windows.Forms.DataGridViewTextBoxColumn H14;
        private System.Windows.Forms.DataGridViewTextBoxColumn H15;
        private System.Windows.Forms.DataGridViewTextBoxColumn H16;
        private System.Windows.Forms.DataGridViewTextBoxColumn H17;
        private System.Windows.Forms.DataGridViewTextBoxColumn H18;
        private System.Windows.Forms.DataGridViewTextBoxColumn H19;
        private System.Windows.Forms.DataGridViewTextBoxColumn H20;
        private System.Windows.Forms.DataGridViewTextBoxColumn H21;
        private System.Windows.Forms.DataGridViewTextBoxColumn H22;
        private System.Windows.Forms.DataGridViewTextBoxColumn H23;
        private System.Windows.Forms.DataGridViewTextBoxColumn H24;
        public System.Windows.Forms.Panel panel1;
    }
}