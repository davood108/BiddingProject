﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using NRI.SBS.Common;
using System.Security.Cryptography;
using System.Collections;
using System.Management;
using FarsiLibrary.Utils;
using System.Xml.Serialization;
using PowerPlantProject.New;

namespace PowerPlantProject
{
    public partial class FormLogin : Form
    {
        MainForm secondpage;
        DialogResult j = DialogResult.None;

        string ExpireDate = "";
        bool NoEX = false;
        public FormLogin()
        {                    
            InitializeComponent();                  
            CenterForm();
        }
        private void CenterForm()
        {
            Point center = new Point();
            center.X = (Screen.PrimaryScreen.WorkingArea.Width - Width) / 2;
            center.Y = (Screen.PrimaryScreen.WorkingArea.Height - Height) / 2;

            Location = center;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            //remove
            //var license = new LicenseHelper();
            //license.Required();
            //  string s = NRI.SBS.Common.RijndaelEncoding.Decrypt("e/KKtgJiAQ5aQ3cWjwQ6dZFyYfRBWyFI/WKoWomiusg=");
            //MessageBox.Show(System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));

        }



        private void LoginBtn_Click(object sender, EventArgs e)
        {

            secondpage = new MainForm();

            ///////////get inital colors/////////////////////////

            FormColors f = FormColors.GetColor();
            DataTable dt1 = Utilities.GetTable("select * from colors");
            int forma, formr, formg, formb = 255;
            int buttona, buttonr, buttong, buttonb = 255;
            int texta, textr, textg, textb = 255;
            int panela, panelr, panelg, panelb = 255;
            int grida = 255, gridr = 255, gridg = 255, gridb = 255;
            if (dt1.Rows.Count > 0)
            {
                forma = int.Parse(dt1.Rows[0]["formA"].ToString());
                formr = int.Parse(dt1.Rows[0]["formR"].ToString());
                formg = int.Parse(dt1.Rows[0]["formG"].ToString());
                formb = int.Parse(dt1.Rows[0]["formB"].ToString());

                buttona = int.Parse(dt1.Rows[0]["buttonA"].ToString());
                buttonr = int.Parse(dt1.Rows[0]["buttonR"].ToString());
                buttong = int.Parse(dt1.Rows[0]["buttonG"].ToString());
                buttonb = int.Parse(dt1.Rows[0]["buttonB"].ToString());

                texta = int.Parse(dt1.Rows[0]["textA"].ToString());
                textr = int.Parse(dt1.Rows[0]["textR"].ToString());
                textg = int.Parse(dt1.Rows[0]["textG"].ToString());
                textb = int.Parse(dt1.Rows[0]["textB"].ToString());


                panela = int.Parse(dt1.Rows[0]["panelA"].ToString());
                panelr = int.Parse(dt1.Rows[0]["panelR"].ToString());
                panelg = int.Parse(dt1.Rows[0]["panelG"].ToString());
                panelb = int.Parse(dt1.Rows[0]["panelB"].ToString());

                grida = int.Parse(dt1.Rows[0]["gridA"].ToString());
                gridr = int.Parse(dt1.Rows[0]["gridR"].ToString());
                gridg = int.Parse(dt1.Rows[0]["gridG"].ToString());
                gridb = int.Parse(dt1.Rows[0]["gridB"].ToString());


                try
                {
                    //////////////////////////////////string name to color////////////////////////////////
                    //ColorConverter ccon = new ColorConverter();
                    /////////////////////////////button/////////////////////////////////////////////////
                    //Color fc1 = (Color)ccon.ConvertFromString(dt.Rows[0]["button"].ToString());
                    //f.Buttonbackcolor = fc1;
                    ///////////////////////////////form////////////////////////////////////////////////
                    //Color fc2 = (Color)ccon.ConvertFromString(dt.Rows[0]["form"].ToString());
                    //f.Formbackcolor = fc2;
                    ///////////////////////////////text///////////////////////////////////////////////////
                    //Color fc3 = (Color)ccon.ConvertFromString(dt.Rows[0]["text"].ToString());
                    //f.Textbackcolor = fc3;
                    /////////////////////////////////panel//////////////////////////////////////////////////
                    //Color fc4 = (Color)ccon.ConvertFromString(dt.Rows[0]["panel"].ToString());
                    //f.Panelbackcolor = fc4;
                    /////////////////////////////////////grid////////////////////////////////////////////
                    //Color fc5 = (Color)ccon.ConvertFromString(dt.Rows[0]["grid"].ToString());
                    //f.Gridbackcolor = fc5;
                    /////////////////////////////////////////////////////////////////////////////////////////

                    if (dt1.Rows.Count > 0)
                    {

                        f.Formbackcolor = Color.FromArgb(forma, formr, formg, formb);
                        f.Gridbackcolor = Color.FromArgb(grida, gridr, gridg, gridb);
                        f.Panelbackcolor = Color.FromArgb(panela, panelr, panelg, panelb);
                        f.Textbackcolor = Color.FromArgb(texta, textr, textg, textb);
                        f.Buttonbackcolor = Color.FromArgb(buttona, buttonr, buttong, buttonb);
                    }
                }
                catch
                {

                    f.Formbackcolor = Color.Beige;
                    f.Gridbackcolor = Color.White;
                    f.Panelbackcolor = Color.Beige;
                    f.Textbackcolor = Color.White;
                    f.Buttonbackcolor = Color.CadetBlue;
                }

            }
            else
            {
                f.Formbackcolor = Color.Beige;
                f.Gridbackcolor = Color.White;
                f.Panelbackcolor = Color.Beige;
                f.Textbackcolor = Color.White;
                f.Buttonbackcolor = Color.CadetBlue;
            }


            ////////////////////////////////////////////////////////








            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                     "\\SBS\\SystemInfo.txt";
            string caption = "ورود به سيستم";
            string message = "";
            if ((UserTxt.Text == "") || (PassTxt.Text == ""))
            {
                errormessage.Text = "Please Fill the Blank!";
                if (UserTxt.Text == "") ValidUser.Visible = true; else ValidUser.Visible = false;
                if (PassTxt.Text == "") ValidPass.Visible = true; else ValidPass.Visible = false;

            }
            else if (VerifyUser() && File.Exists(path))
            {

                SetUserIdentity();
                secondpage.Show();
                this.Hide();


            }
            else
            {
                bool success = VerifyUser();
                if (success)
                {
                    SetUserIdentity();


                    ///////////////////////////////////if remove//////////////////////////////////////////
                    try
                    {
                        string hardtest = combineinfohard().Trim();
                    }
                    catch
                    {
                        MessageBox.Show("Problem in Detect HardWare!");
                        secondpage.Show();
                        this.Hide();
                    }

                    string hard = combineinfohard().Trim();
                    DataTable dyy = Utilities.GetTable("select * from dbo.SystemAccount");
                    bool samesys = false;
                    string chy = combineinfohard().Trim();
                    if (dyy != null)
                    {
                        if (dyy.Rows.Count > 0)
                        {
                            int index = 0;
                            foreach (DataRow b in dyy.Rows)
                            {
                                if (chy == b["HardInfo"].ToString().Trim())
                                {
                                    samesys = true;
                                    break;
                                }
                                index++;
                            }
                            if (samesys)
                            {
                                TimeSpan max = new TimeSpan();
                                TimeSpan span = new TimeSpan();
                                TimeSpan spanstart = new TimeSpan();
                                string notxpire = "";
                                string st = "";
                                string en = "";
                                string xspan = "";
                                string id = "";
                                string username = "";
                                string pass = "";
                                int c = 1;
                                int maxc = 0;
                                bool validserial = false;
                                string txtserial = txt1.Text.Trim() + txt2.Text.Trim() + txt3.Text.Trim() + txt4.Text.Trim();

                                notxpire = mainhashed(dyy.Rows[index]["NoExpire"].ToString().Trim());
                                st = dyy.Rows[index]["StartDate"].ToString().Trim();
                                en = dyy.Rows[index]["EndDate"].ToString().Trim();
                                xspan = mainhashed(dyy.Rows[index]["CountDay"].ToString().Trim());
                                max = PersianDateConverter.ToGregorianDateTime(en).Date - PersianDateConverter.ToGregorianDateTime(st).Date;
                                if (max.Days <= 0)
                                    maxc = 0;
                                else
                                    maxc = max.Days;

                                span = PersianDateConverter.ToGregorianDateTime(en).Date - DateTime.Now.Date;

                                spanstart = DateTime.Now.Date - PersianDateConverter.ToGregorianDateTime(st).Date;
                                if (spanstart.Days > 0 && span.Days > 0 && span.Days.ToString() != xspan)
                                {
                                    if (int.Parse(xspan) < spanstart.Days)
                                    {
                                        DataTable dt = Utilities.GetTable("update SystemAccount set CountDay='" + Encrypt(spanstart.Days.ToString().Trim()) + "'where HardInfo='" + hard + "'");
                                    }
                                }

                                if (notxpire == "True")
                                {
                                    secondpage.Show();
                                    this.Hide();

                                }
                                else if (MyintParse(xspan) > maxc && txtserial == "")
                                {
                                    // this.Close();
                                    linkLabelhard.Visible = true;
                                    MessageBox.Show("Referesh Account");
                                }



                                else if (maxc == int.Parse(xspan))
                                {

                                    panel2.Visible = true;
                                    linkLabelhard.Visible = true;

                                    if (txtserial == "")
                                    {
                                        j = MessageBox.Show("Serial Number Expire! \r\n\r\n please insert new serial number", "", MessageBoxButtons.OK);

                                    }
                                    else if (txtserial != "")
                                    {

                                        if (j == DialogResult.OK && serialvalid())
                                        {
                                            string start = new PersianDate(DateTime.Now).ToString("d");
                                            //  string end = new PersianDate(DateTime.Now.AddDays(30)).ToString("d"); 
                                            c = 1;
                                            string hashNoEX = Encrypt(NoEX.ToString());
                                            string hashc = Encrypt(c.ToString());
                                            DataTable dt = Utilities.GetTable("update SystemAccount set CountDay='" + hashc + "',NoExpire='" + hashNoEX + "',StartDate='" + start + "',EndDate='" + ExpireDate + "'where HardInfo='" + hard + "'");
                                            secondpage.Show();
                                            this.Hide();
                                        }
                                        else
                                        {
                                            MessageBox.Show("Invalid Serial");
                                        }


                                    }

                                }
                                else if ((new PersianDate(DateTime.Now).ToString("d") != en) && (span.Days >= 0) && (maxc != 0) && (notxpire == "False"))
                                {
                                    secondpage.Show();
                                    this.Hide();
                                }
                                else
                                {
                                    // this.Close(); ;                                    
                                    linkLabelhard.Visible = true;
                                    MessageBox.Show("Referesh Account");
                                }
                            }
                            else
                            {
                                //serial number////////////////////////////////////
                                TimeSpan max = new TimeSpan();
                                TimeSpan span = new TimeSpan();
                                TimeSpan spanstart = new TimeSpan();
                                string notxpire = "";
                                string st = "";
                                string en = "";
                                string xspan = "";
                                string id = "";
                                string username = "";
                                string pass = "";
                                int c = 1;
                                int maxc = 0;
                                bool validserial = false;
                                string txtserial = txt1.Text.Trim() + txt2.Text.Trim() + txt3.Text.Trim() + txt4.Text.Trim();


                                string start = new PersianDate(DateTime.Now).ToString("d");

                                c = 1;
                                panel2.Visible = true;
                                linkLabelhard.Visible = true;
                                validserial = serialvalid();

                                DialogResult d = new DialogResult();
                                if (txtserial == "")
                                    d = MessageBox.Show("please insert serial number", "Serial Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                else d = DialogResult.OK;
                                if (d == DialogResult.OK)
                                {
                                    if (validserial)
                                    {
                                        string hashNoEX = Encrypt(NoEX.ToString());
                                        string hashc = Encrypt(c.ToString());
                                        // DataTable dyy1 = utilities.GetTable("insert into SystemAccount(,UserName,Pass,StartDate,EndDate,NoExpire,CountDay) values('" + id + "','" + username + "','" + pass + "','" + start + "','" + ExpireDate + "','" + hashNoEX + "','" + hashc + "')");
                                        DataTable dyy1 = Utilities.GetTable("insert into dbo.SystemAccount (HardInfo,StartDate,EndDate,NoExpire,CountDay)values('" + hard + "','" + start + "','" + ExpireDate + "','" + hashNoEX + "','" + hashc + "')");

                                        secondpage.Show();
                                        this.Hide();
                                    }
                                    else
                                    {
                                        //  MessageBox.Show("Invalid Serial");
                                    }
                                }

                            }

                        }
                        else if (dyy.Rows.Count == 0)
                        {

                            TimeSpan max = new TimeSpan();
                            TimeSpan span = new TimeSpan();
                            TimeSpan spanstart = new TimeSpan();
                            string notxpire = "";
                            string st = "";
                            string en = "";
                            string xspan = "";
                            string id = "";
                            string username = "";
                            string pass = "";
                            int c = 1;
                            int maxc = 0;
                            bool validserial = false;
                            string txtserial = txt1.Text.Trim() + txt2.Text.Trim() + txt3.Text.Trim() + txt4.Text.Trim();


                            string start = new PersianDate(DateTime.Now).ToString("d");

                            c = 1;
                            panel2.Visible = true;
                            linkLabelhard.Visible = true;
                            validserial = serialvalid();

                            DialogResult d = new DialogResult();
                            if (txtserial == "")
                                d = MessageBox.Show("please insert serial number", "Serial Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            else d = DialogResult.OK;
                            if (d == DialogResult.OK)
                            {
                                if (validserial)
                                {
                                    string hashNoEX = Encrypt(NoEX.ToString());
                                    string hashc = Encrypt(c.ToString());
                                    // DataTable dyy1 = utilities.GetTable("insert into SystemAccount(,UserName,Pass,StartDate,EndDate,NoExpire,CountDay) values('" + id + "','" + username + "','" + pass + "','" + start + "','" + ExpireDate + "','" + hashNoEX + "','" + hashc + "')");
                                    DataTable dyy1 = Utilities.GetTable("insert into dbo.SystemAccount (HardInfo,StartDate,EndDate,NoExpire,CountDay)values('" + hard + "','" + start + "','" + ExpireDate + "','" + hashNoEX + "','" + hashc + "')");

                                    secondpage.Show();
                                    this.Hide();
                                }
                                else
                                {
                                    if (txtserial != "")
                                        MessageBox.Show("Invalid Serial");
                                }
                            }
                        }
                    }

                    //////////////////////////////////if remove////////////////////////////////////////
                    //secondpage.Show();
                    //this.Hide();

                    //////////////////////////////////////////////////////////////////////////////////





                }
            }
            return;

        }
        private string Encrypt(string name)
        {
            SHA1Managed sh = new SHA1Managed();
            byte[] hash = sh.ComputeHash(System.Text.UnicodeEncoding.Unicode.GetBytes(name));

            string hashedPassword = "";
            foreach (byte b in hash)
                hashedPassword += b.ToString();
            return hashedPassword;
        }
        private static double MyintParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private bool serialvalid()
        {
            if (txt1.Text.Trim() == "" || txt2.Text.Trim() == "" || txt3.Text.Trim() == "" || txt4.Text.Trim() == "") return false;
            ///////////////////////////////////////////detect date /////////////////////////////////////////////////////
            string xx = txt1.Text.Trim() + txt2.Text.Trim() + txt3.Text.Trim() + txt4.Text.Trim();
            string y1 = txt1.Text.Substring(1, 1).Trim();
            string y2 = txt2.Text.Substring(1, 1).Trim();
            string y3 = txt3.Text.Substring(1, 1).Trim();
            string y4 = txt4.Text.Substring(1, 1).Trim();

            string m1 = txt4.Text.Substring(3, 1).Trim();
            string m2 = txt1.Text.Substring(3, 1).Trim();

            string d1 = txt2.Text.Substring(2, 1).Trim();
            string d2 = txt3.Text.Substring(2, 1).Trim();

            ExpireDate = y1 + y2 + y3 + y4 + "/" + m1 + m2 + "/" + d1 + d2;

            if (txt2.Text.Substring(1, 1).Trim() == "i")
            {
                ExpireDate = new PersianDate(DateTime.Now).ToString("d");
                NoEX = true;
            }
            /////////////////////////////////////////end of detect date//////////////////////////////////////////////////////
            /////////////////////////////////////////detect hard//////////////////////////////////////////////
            string name = combinehardinfopass().Trim();
            string nd1 = name.Substring(0, 1).Trim();
            string nd2 = name.Substring(4, 1).Trim();
            string nd3 = name.Substring(3, 1).Trim();
            string nd4 = name.Substring(2, 1).Trim();
            //////////////////////////////////////////////////////////////////////////////////////////////
            if (txt1.Text.Substring(0, 1) != nd1) return false;
            if (txt1.Text.Substring(2, 1) != nd2) return false;
            if (txt2.Text.Substring(3, 1) != nd3) return false;
            if (txt4.Text.Substring(2, 1) != nd4) return false;
            ///////////////////////////////////////////////////////////////////////////////////////////
            if (txt3.Text.Substring(3, 1) != "7") return false;

            ///////////////////////////////////////////////////////////////////////////////////////
            return true;

        }
        private string mainhashed(string x)
        {
            int lenght = x.Length;

            string y = "";

            if (x == "219182205164213164125168153211181192192771761491409231") y = "False";
            else if (x == "233188132182471034323719994178311601306423944232113") y = "True";

            else
            {
                for (int i = 0; i <= 100; i++)
                {
                    if (Encrypt(i.ToString().Trim()) == x)
                    {
                        y = i.ToString();
                        break;
                    }


                }
            }

            return y;

        }
        private string combinehardinfopass()
        {
            string[] x1 = InsertInfo1("Win32_BaseBoard");
            string[] x2 = InsertInfo1("Win32_Processor");
            string xx = "";
            SHA1Managed sh = new SHA1Managed();


            if (x1[0] == " ") x1[0] = "1";
            if (x2[0] == " ") x2[0] = "1";

            xx = x1[0] + x2[0];


            byte[] hash = sh.ComputeHash(System.Text.UnicodeEncoding.Unicode.GetBytes(xx));
            string hashedPassword = "";
            foreach (byte b in hash)
                hashedPassword += b.ToString();
            string final = hashedPassword;
            return final;

        }
        private string combineinfohard()
        {
            string[] x1 = InsertInfo1("Win32_BaseBoard");
            string[] x2 = InsertInfo1("Win32_Processor");
            string xx = "";
            SHA1Managed sh = new SHA1Managed();

            if (x1[0] == " ") x1[0] = "1";
            if (x1[1] == " ") x1[1] = "1";
            if (x2[0] == " ") x2[0] = "1";
            if (x2[1] == " ") x2[1] = "1";


            xx = x1[0] + x2[0] + x1[1] + x2[1];


            byte[] hash = sh.ComputeHash(System.Text.UnicodeEncoding.Unicode.GetBytes(xx));
            string hashedPassword = "";
            foreach (byte b in hash)
                hashedPassword += b.ToString();
            string final = hashedPassword;
            return final;
        }
        private string[] InsertInfo1(string Key)
        {
            string[] y = new string[5];
            string[] x = new string[5];
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from " + Key);

            try
            {
                foreach (ManagementObject share in searcher.Get())
                {

                    string grp = "";
                    try
                    {
                        grp = share["Name"].ToString();
                    }
                    catch
                    {
                        grp = share.ToString();
                    }

                    if (share.Properties.Count <= 0)
                    {
                        //MessageBox.Show("No Information Available", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //return;
                    }

                    foreach (PropertyData PC in share.Properties)
                    {

                        ListViewItem item = new ListViewItem(grp);

                        item.Text = PC.Name;

                        if (PC.Value != null && PC.Value.ToString() != "")
                        {
                            switch (PC.Value.GetType().ToString())
                            {
                                case "System.String[]":
                                    string[] str = (string[])PC.Value;

                                    string str2 = "";
                                    foreach (string st in str)
                                        str2 += st + " ";
                                    if (Key == "Win32_BaseBoard")
                                    {
                                        if (item.Text == "SerialNumber") x[0] = str2;
                                        if (item.Text == "Product") x[1] = str2;
                                        if (item.Text == "Manufacturer") x[2] = str2;
                                        if (item.Text == "Version") x[3] = str2;
                                        if (item.Text == "Name") x[4] = str2;

                                    }
                                    if (Key == "Win32_Processor")
                                    {
                                        if (item.Text == "Processorid") y[0] = str2;
                                        if (item.Text == "SystemName") y[1] = str2;
                                        if (item.Text == "Version") y[2] = str2;
                                        if (item.Text == "Revision") y[3] = str2;
                                        if (item.Text == "Name") y[4] = str2;

                                    }
                                    item.SubItems.Add(str2);

                                    break;
                                case "System.UInt16[]":
                                    ushort[] shortData = (ushort[])PC.Value;


                                    string tstr2 = "";
                                    foreach (ushort st in shortData)
                                        tstr2 += st.ToString() + " ";


                                    if (Key == "Win32_BaseBoard")
                                    {
                                        if (item.Text == "SerialNumber") x[0] = tstr2;
                                        if (item.Text == "Product") x[1] = tstr2;
                                        if (item.Text == "Manufacturer") x[2] = tstr2;
                                        if (item.Text == "Version") x[3] = tstr2;
                                        if (item.Text == "Name") x[4] = tstr2;

                                    }
                                    if (Key == "Win32_Processor")
                                    {
                                        if (item.Text == "Processorid") y[0] = tstr2;
                                        if (item.Text == "SystemName") y[1] = tstr2;
                                        if (item.Text == "Version") y[2] = tstr2;
                                        if (item.Text == "Revision") y[3] = tstr2;
                                        if (item.Text == "Name") y[4] = tstr2;
                                    }

                                    item.SubItems.Add(tstr2);

                                    break;

                                default:
                                    item.SubItems.Add(PC.Value.ToString());
                                    if (Key == "Win32_BaseBoard")
                                    {
                                        if (item.Text == "SerialNumber") x[0] = PC.Value.ToString();
                                        if (item.Text == "Product") x[1] = PC.Value.ToString();
                                        if (item.Text == "Manufacturer") x[2] = PC.Value.ToString();
                                        if (item.Text == "Version") x[3] = PC.Value.ToString();
                                        if (item.Text == "Name") x[4] = PC.Value.ToString();

                                    }
                                    if (Key == "Win32_Processor")
                                    {
                                        if (item.Text == "ProcessorId") y[0] = PC.Value.ToString();
                                        if (item.Text == "SystemName") y[1] = PC.Value.ToString();
                                        if (item.Text == "Version") y[2] = PC.Value.ToString();
                                        if (item.Text == "Revision") y[3] = PC.Value.ToString();
                                        if (item.Text == "Name") y[4] = PC.Value.ToString();
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            //if (!DontInsertNull)
                            //    item.SubItems.Add("No Information available");
                            //else
                            //    continue;
                        }


                    }
                }
            }


            catch (Exception exp)
            {
                // MessageBox.Show("can't get data because of the followeing error \n" + exp.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            if (Key == "Win32_BaseBoard") return x;
            else if (Key == "Win32_Processor") return y;
            else return null;
        }
        private void SetUserIdentity()
        {
            string strCmd = "select * from login where" +
            " Username= '" + UserTxt.Text + "'" +
            " AND Password='" + EncryptParrword() + "'";
            DataRow row = Utilities.GetTable(strCmd).Rows[0];
            User user = User.getUser();
            user.Username = row["username"].ToString().Trim();
            user.FirstName = row["FirstName"].ToString().Trim();
            user.LastName = row["LastName"].ToString().Trim();
            user.HashedPassword = EncryptParrword();
            DefinedRoles role = DefinedRoles.Unassigned;
            try
            {
                role = (DefinedRoles)Enum.Parse(typeof(DefinedRoles), row["Role"].ToString().Trim());
            }
            catch
            {
            }
            user.Role = role;
            //user.Deleted
        }
        private void txt1_Leave(object sender, EventArgs e)
        {
            if (txt1.Text.Length != 4) txt1.Text = "";
        }

        private void txt2_Leave(object sender, EventArgs e)
        {
            if (txt2.Text.Length != 4) txt2.Text = "";
        }

        private void txt3_Leave(object sender, EventArgs e)
        {
            if (txt3.Text.Length != 4) txt3.Text = "";
        }

        private void txt4_Leave(object sender, EventArgs e)
        {
            if (txt4.Text.Length != 4) txt4.Text = "";
        }

        private bool VerifyUser()
        {

            ValidUser.Visible = false;
            ValidPass.Visible = false;

            string pass = EncryptParrword();
            string str1 = "SELECT  count(Username) FROM [login] WHERE Username='" + UserTxt.Text.Trim() + "'" +
                " AND Password='" + pass + "'";
            string str2 = "SELECT  count(Username) FROM [login] WHERE Username='" + UserTxt.Text.Trim() + "'" +
                " AND Password!='" + pass + "'";


            int result1 = int.Parse(Utilities.GetTable(str1).Rows[0][0].ToString());
            int result2 = int.Parse(Utilities.GetTable(str2).Rows[0][0].ToString());

            if (result1 == 1)
            {
                return true;

            }
            else
            {
                if (result2 == 1)
                {
                    errormessage.Text = "Invalid Password!";
                }
                else
                    errormessage.Text = "Invalid Username!";
                return false;
            }

        }

        private string EncryptParrword()
        {
            SHA1Managed sh = new SHA1Managed();
            byte[] hash = sh.ComputeHash(System.Text.UnicodeEncoding.Unicode.GetBytes(PassTxt.Text));

            string hashedPassword = "";
            foreach (byte b in hash)
                hashedPassword += b.ToString();
            return hashedPassword;
        }

        private void Exit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Application.Exit();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }





        private void linkLabelhard_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            HardInfo b = new HardInfo();
            b.Show();
        }



        private void linfresh_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabelhard.Visible = true;
            string hard = combineinfohard().Trim();
            bool enter = false;
            DataTable dyy = Utilities.GetTable("select * from dbo.SystemAccount");
            if (dyy != null)
            {
                if (dyy.Rows.Count > 0)
                {

                    foreach (DataRow b in dyy.Rows)
                    {
                        if (hard == b["HardInfo"].ToString().Trim())
                            enter = true;

                    }
                }
            }

            if (UserTxt.Text != "" && PassTxt.Text != "")
            {
                if (enter)
                {
                    errormessage.Text = "";
                    string txtserial = txt1.Text.Trim() + txt2.Text.Trim() + txt3.Text.Trim() + txt4.Text.Trim();
                    panel2.Visible = true;

                    if (txtserial == "")
                    {
                        j = MessageBox.Show("please insert new serial number", "", MessageBoxButtons.OK);

                    }
                    else if (txtserial != "" && enter)
                    {

                        if (j == DialogResult.OK && serialvalid())
                        {
                            string start = new PersianDate(DateTime.Now).ToString("d");
                            //  string end = new PersianDate(DateTime.Now.AddDays(30)).ToString("d"); 
                            int c = 1;
                            string hashNoEX = Encrypt(NoEX.ToString());
                            string hashc = Encrypt(c.ToString());
                            //DataTable dd = utilities.GetTable("delete from  dbo.AccountUser where UserName='" + UserTxt.Text.Trim() + "' and Pass='" + EncryptParrword() + "'");
                            DataTable dt = Utilities.GetTable("update SystemAccount set CountDay='" + hashc + "',NoExpire='" + hashNoEX + "',StartDate='" + start + "',EndDate='" + ExpireDate + "'where HardInfo='" + hard + "'");
                            secondpage.Show();
                            this.Hide();
                        }
                        else
                        {
                            if (txtserial != "")
                                MessageBox.Show("Invalid Serial");
                        }

                    }
                }
                else
                {
                    MessageBox.Show("press login.");

                }
            }
            else errormessage.Text = "Please fill user and pass.";
        }


        public CDBConfigurationParams internetReadDBConfig()
        {
            // ConfigInfoObj = new CDBConfigurationParams();//2
            CDBConfigurationParams ConfigInfoObj = new CDBConfigurationParams();//1
            XmlSerializer mySerializer = new XmlSerializer(typeof(CDBConfigurationParams));
            try
            {
                //FileStream myFileStream = new FileStream("ConfigDB.xml", FileMode.Open);
                string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                "\\SBS\\internetConfigDB.xml";
                FileStream myFileStream = new FileStream(path, FileMode.Open);

                ConfigInfoObj = (CDBConfigurationParams)mySerializer.Deserialize(myFileStream);
                myFileStream.Close();
            }
            catch (System.IO.FileNotFoundException fex)
            {
                throw fex;
            }
            catch (Exception ex)
            {
                throw ex;
                //ConfigInfoObj = null;
                //throw new DMSGeneralException //impossible because can not call NRI.SBS.Common from CIM_DA
                //and if transfer this file to NRI.SBS.Common NHibernateHelper_CIM could not use this
            }
            return ConfigInfoObj;
        }



        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            groupBox1.Visible = true;
            button1.Visible = true;

            try
            {

                CConfigDBXMLController xmlController = new CConfigDBXMLController();

                CDBConfigurationParams ConfigDBObj = xmlController.internetReadDBConfig();
                string[] a = ConfigDBObj.ServerName.Split(',');

                textBox1.Text = a[0].Trim();
                if (a.Length == 2)
                {
                    textBox2.Text = a[1].Trim();
                }
                textBox3.Text = ConfigDBObj.ServerDBName;
                textBox4.Text = NRI.SBS.Common.RijndaelEncoding.Decrypt(ConfigDBObj.ServerUserPassword);
                textBox5.Text = NRI.SBS.Common.RijndaelEncoding.Decrypt(ConfigDBObj.ServerUserName);

            }
            catch (System.IO.FileNotFoundException fex)
            {
                MessageBox.Show("XML Error!!");
                //throw fex;
            }

            catch (Exception ex)
            {
                MessageBox.Show("XML Error!!");
                //throw ex;
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            string servername = textBox1.Text.Trim() + "," + textBox2.Text.Trim();
            servername = servername.TrimEnd(',');
            string serverdbname = textBox3.Text.Trim();
            string serverusername = textBox5.Text.Trim();
            string serverpassword = textBox4.Text.Trim();
            //configdb
            string history = NRI.SBS.Common.ConnectionManager.ConnectionString;
            /////

            ///internetconfigdb///////////////////////////////////////////////////
            try
            {
                if (serverdbname != "" && serverdbname != "" && serverusername != "" && serverpassword != "")
                {

                    NRI.SBS.Common.ConnectionManager.ConnectionString = "Server=" + servername + "; initial catalog=" + serverdbname + "; timeout=30; User ID=" + serverusername + "; Password=" + serverpassword;
                    //xml update///////////////////////////////////////////


                    CDBConfigurationParams c = new CDBConfigurationParams();
                    c.ServerName = servername;
                    c.ServerDBName = serverdbname;
                    c.ServerDBTimeout = "30";
                    c.ServerUserName = NRI.SBS.Common.RijndaelEncoding.Encrypt(serverusername);
                    c.ServerUserPassword = NRI.SBS.Common.RijndaelEncoding.Encrypt(serverpassword);

                    XmlSerializer mySerializer = new XmlSerializer(typeof(CDBConfigurationParams));

                    string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\SBS\\internetConfigDB.xml";
                    FileInfo fInfo = new FileInfo(path);
                    if (!fInfo.Directory.Exists)
                        fInfo.Directory.Create();

                    FileStream myFileStream = new FileStream(path, FileMode.Create, FileAccess.Write);

                    mySerializer.Serialize(myFileStream, c);
                    myFileStream.Close();


                    //////////////////////////////////////////////////////
                    DataTable d = Utilities.GetTable("select * from login");
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("Connect Successfully");

                }
                else
                {

                    MessageBox.Show("Fill All Correctly");
                }
            }
            catch
            {
                NRI.SBS.Common.ConnectionManager.ConnectionString = history;
                MessageBox.Show("Connect UnSuccessfully");
            }
        }







    }
}
