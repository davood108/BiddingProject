﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NRI.SBS.Common;
namespace PowerPlantProject
{
    public partial class ExcelFormat : Form
    {
        public string result;
        public string date;

        public ExcelFormat(string DATE)
        {
            date = DATE;
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }
        private void ExcelFormat_Load(object sender, EventArgs e)
        {
            if (date != null)
            {
                datePicker.Text = date;

            }
            else
            {
                datePicker.Text = "";
                datePicker.IsNull = true;
            }
            DataTable dt = Utilities.GetTable("select distinct ppid from powerplant");
            foreach (DataRow m in dt.Rows)
            {
                cmbppid.Items.Add(m["ppid"].ToString().Trim());
            }
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (datePicker.Text == "" && datePicker.Enabled)
            {
                MessageBox.Show(" Please Fill Blank !");

            }
            else if (datePicker.Text == "[Empty Value]" && datePicker.Enabled)
            {
                MessageBox.Show(" Please Fill Blank !");

            }

            else if(cmbppid.Text== "" && cmbppid.Enabled)
            {
                MessageBox.Show(" Please Fill Blank !");
            }
            else if (cmbsheet.Text == "" && cmbsheet.Enabled)
            {
                MessageBox.Show(" Please Fill Blank !");
            }
            else
            {

                result = cmbsheet.Text + "," + datePicker.Text + "," + cmbppid.Text.Trim();
                this.DialogResult = DialogResult.OK;
                this.Close();

            }
        }

        private void datePicker_ValueChanged(object sender, EventArgs e)
        {
            if (!datePicker.IsNull)
            {
                label1.Text = "You Must Choose Excel File In Date :" + datePicker.Text;
            }
        }

     

        
    }
}
