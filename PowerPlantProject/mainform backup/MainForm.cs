﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.DMS.Common;
using System.Collections;

namespace PowerPlantProject
{
    public partial class MainForm : Form
    {
        //Is There this PlantID in the PowerPlant?
        // dar FindPPID meghdar dehi mishavad
        bool CheckPPID;

        public string PPID="0";
        public int line;
        public int TextboxTab = 0;
        ArrayList PPIDArray = new ArrayList();
        ArrayList PPIDType = new ArrayList();
        double PAvailableCapacity = 0, PTotalPower = 0, PULPower = 0, PBidPower = 0, PIncrementPower = 0,
        PDecreasePower = 0, PCapacityPayment = 0, PBidPayment = 0, PULPayment = 0, PIncrementPayment = 0,
        PDecreasePayment = 0, PEnergyPayment = 0, PIncome = 0, PCost = 0, PBenefit = 0;
        bool m002Generated = false;
        
        public MainForm()
        {
            
            InitializeComponent();

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            Myda.SelectCommand = new SqlCommand("SELECT PPID FROM PowerPlant", myConnection);
            Myda.Fill(MyDS, "ppid");
            foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
            {
                PPIDArray.Add(MyRow["PPID"].ToString().Trim());
                PPIDType.Add("real");
            }

            for (int i = 0; i < PPIDArray.Count; i++)
            { 
                SqlCommand mycom=new SqlCommand();
                mycom.Connection = myConnection;
                mycom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id SELECT @result2 =count(PPID) FROM [PPUnit] WHERE PPID=@id AND PackageType LIKE 'Combined Cycle%'";
                mycom.Parameters.Add("@id", SqlDbType.NChar, 10);
                mycom.Parameters["@id"].Value = PPIDArray[i].ToString();
                mycom.Parameters.Add("@result1", SqlDbType.Int);
                mycom.Parameters["@result1"].Direction = ParameterDirection.Output;
                mycom.Parameters.Add("@result2", SqlDbType.Int);
                mycom.Parameters["@result2"].Direction = ParameterDirection.Output;
                mycom.ExecuteNonQuery();
                int result1 = (int)mycom.Parameters["@result1"].Value;
                int result2 = (int)mycom.Parameters["@result2"].Value;
                if ((result1 > 1) && (result2 > 0))
                {
                    PPIDArray.Add((int.Parse(PPIDArray[i].ToString())) + 1);
                    PPIDType.Add("virtual");
                }
            }

            BDCurGrid.DataSource = null;

            //SET Calendars
            FALocalizeManager.CustomCulture = FALocalizeManager.FarsiCulture;
            GDStartDateCal.SelectedDateTime = System.DateTime.Now;
            GDStartDateCal.IsNull = true;
            GDEndDateCal.SelectedDateTime = System.DateTime.Now;
            GDEndDateCal.IsNull = true;
            BDCal.SelectedDateTime = System.DateTime.Now;
            BDCal.IsNull = true;
            FRUnitCal.SelectedDateTime = System.DateTime.Now;
            FRUnitCal.IsNull = true;
            FRPlantCal.SelectedDateTime = System.DateTime.Now;
            FRPlantCal.IsNull = true;
            MRCal.SelectedDateTime = System.DateTime.Now;
            MRCal.IsNull = true;
            ODUnitServiceStartDate.SelectedDateTime = System.DateTime.Now;
            ODUnitServiceStartDate.IsNull = true;
            ODUnitServiceEndDate.SelectedDateTime = System.DateTime.Now;
            ODUnitServiceEndDate.IsNull = true;
            ODUnitMainStartDate.SelectedDateTime = System.DateTime.Now;
            ODUnitMainStartDate.IsNull = true;
            ODUnitMainEndDate.SelectedDateTime = System.DateTime.Now;
            ODUnitMainEndDate.IsNull = true;
            ODUnitFuelStartDate.SelectedDateTime = System.DateTime.Now;
            ODUnitFuelStartDate.IsNull = true;
            ODUnitFuelEndDate.SelectedDateTime = System.DateTime.Now;
            ODUnitFuelEndDate.IsNull = true;
            ODUnitPowerStartDate.SelectedDateTime = System.DateTime.Now;
            ODUnitPowerStartDate.IsNull = true;
            ODUnitPowerEndDate.SelectedDateTime = System.DateTime.Now;
            ODUnitPowerEndDate.IsNull = true;
            myConnection.Close();

        }
        //---------------------Form2_Load-----------------------------------
        private void Form2_Load(object sender, EventArgs e)
        {
            buildTreeView1();
            LoadBiddingStrategy();
            LoadMaintenance();
            //AutomaticFillEconomics();
            //Initialze StartDate for Calculate
            //string StartDate="";
            //JustOneTime(StartDate);

        }
        //-------------------------------bulidTreeView1---------------------------------
        private void buildTreeView1()
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            treeView1.Nodes.Clear();
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();

            //Insert Trec
            TreeNode MyNode = new TreeNode();
            MyNode.Text = "Trec";
            treeView1.Nodes.Add(MyNode);

            //Insert Plants
            Myda.SelectCommand = new SqlCommand("SELECT PPName FROM PowerPlant order by ppid", myConnection);
            Myda.Fill(MyDS, "plantname");
            TreeNode PlantNode = new TreeNode();
            PlantNode.Text = "Plant";
            MyNode.Nodes.Add(PlantNode);
            foreach (DataRow MyRow in MyDS.Tables["plantname"].Rows)
            {
                TreeNode ChildNode = new TreeNode();
                ChildNode.Text = MyRow["PPName"].ToString().Trim();
                PlantNode.Nodes.Add(ChildNode);
            }

            //Insert Transmission
            Myda.SelectCommand = new SqlCommand("SELECT DISTINCT LineNumber FROM TransLine", myConnection);
            Myda.Fill(MyDS, "transtype");
            myConnection.Close();

            TreeNode TransNode = new TreeNode();
            TransNode.Text = "Transmission";
            MyNode.Nodes.Add(TransNode);
            foreach (DataRow MyRow in MyDS.Tables["transtype"].Rows)
            {
                TreeNode ChildNode = new TreeNode();
                ChildNode.Text = MyRow["LineNumber"].ToString().Trim();
                TransNode.Nodes.Add(ChildNode);
            }
            treeView1.ExpandAll();
        }
        //--------------------buildPPtree----------------------------------------
        private void buildPPtree(string Num)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            treeView2.Nodes.Clear();
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT DISTINCT PackageType FROM UnitsDataMain WHERE PPID=@Num", myConnection);
            Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
            Myda.SelectCommand.Parameters["@Num"].Value = Num;
            Myda.Fill(MyDS, "UnitType");

            foreach (DataRow MyRow in MyDS.Tables["UnitType"].Rows)
            {
                string Utype = MyRow["PackageType"].ToString().Trim();
                Utype = Utype.Trim();
                switch (Utype)
                {
                    case "Steam":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Steam%' order by PPID,UnitCode", myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = Num;
                        Myda.Fill(MyDS, "Steam");
                        TreeNode MyNode = new TreeNode();
                        MyNode.Text = "Steam";
                        treeView2.Nodes.Add(MyNode);
                        string temp = "";
                        DataView sdv = new DataView(MyDS.Tables["Steam"]);
                        sdv.Sort = "UnitCode ASC";
                        foreach (DataRow ChildRow in sdv.Table.Rows)
                        //foreach (DataRow ChildRow in MyDS.Tables["Steam"].Rows)
                        {
                            temp = ChildRow["unitCode"].ToString().Trim();
                            if (temp.Length == 6)
                            {
                                TreeNode ChildNode = new TreeNode();
                                ChildNode.Text = ChildRow["UnitCode"].ToString().Trim();
                                MyNode.Nodes.Add(ChildNode);
                            }
                        }
                        foreach (DataRow ChildRow in sdv.Table.Rows)
                        //foreach (DataRow ChildRow in MyDS.Tables["Steam"].Rows)
                        {
                            temp = ChildRow["UnitCode"].ToString().Trim();
                            if (temp.Length == 7)
                            {
                                TreeNode ChildNode = new TreeNode();
                                ChildNode.Text = ChildRow["UnitCode"].ToString().Trim();
                                MyNode.Nodes.Add(ChildNode);
                            }
                        }
                        break;
                    case "Gas":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Gas%' order by UnitCode", myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = Num;
                        Myda.Fill(MyDS, "Gas");
                        TreeNode Node1 = new TreeNode();
                        Node1.Text = "Gas";
                        treeView2.Nodes.Add(Node1);
                        DataView gdv = new DataView(MyDS.Tables["Gas"]);
                        gdv.Sort = "UnitCode";
                        foreach (DataRow ChildRow in gdv.Table.Rows)
                        //foreach (DataRow ChildRow in MyDS.Tables["Gas"].Rows)
                        {
                            temp = ChildRow["UnitCode"].ToString().Trim();
                            if (temp.Length == 4)
                            {
                                TreeNode ChildNode = new TreeNode();
                                ChildNode.Text = ChildRow["UnitCode"].ToString().Trim();
                                Node1.Nodes.Add(ChildNode);
                            }
                        }
                        foreach (DataRow ChildRow in gdv.Table.Rows)
                        //foreach (DataRow ChildRow in MyDS.Tables["Gas"].Rows)
                        {
                            temp = ChildRow["UnitCode"].ToString().Trim();
                            if (temp.Length == 5)
                            {
                                TreeNode ChildNode = new TreeNode();
                                ChildNode.Text = ChildRow["UnitCode"].ToString().Trim();
                                Node1.Nodes.Add(ChildNode);
                            }
                        }
                        break;
                    default:
                        Myda.SelectCommand = new SqlCommand("SELECT DISTINCT PackageCode FROM [UnitsDataMain] WHERE PPID=@Num AND PackageType LIKE 'CC%'", myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = Num;
                        Myda.Fill(MyDS, "CCPackage");
                        foreach (DataRow ChildRow in MyDS.Tables["CCPackage"].Rows)
                        {
                            TreeNode PNode = new TreeNode();
                            PNode.Text = "Combined Cycle" + ChildRow["PackageCode"].ToString().Trim();
                            treeView2.Nodes.Add(PNode);
                        }

                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageCode FROM [UnitsDataMain] WHERE PPID=@Num AND PackageType LIKE 'CC%' order by UnitCode", myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = Num;
                        Myda.Fill(MyDS, "Combined");
                        DataView cdv = new DataView(MyDS.Tables["Combined"]);
                        cdv.Sort = "UnitCode ASC";
                        foreach (DataRow ChildRow in cdv.Table.Rows)
                            //foreach (DataRow ChildRow in MyDS.Tables["Combined"].Rows)
                            foreach (TreeNode mynode in treeView2.Nodes)
                                if (mynode.Text.Contains(ChildRow["PackageCode"].ToString().Trim()))
                                {
                                    TreeNode GNode = new TreeNode();
                                    GNode.Text = ChildRow["UnitCode"].ToString().Trim();
                                    mynode.Nodes.Add(GNode);
                                }
                        break;
                }
            }

            MyDS.Dispose();
            Myda.Dispose();
            myConnection.Close();
            treeView2.ExpandAll();
        }

        //---------------------Form2_FormClosed-----------------------------------------
        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        //------------------------buildTRANStree----------------------------------
        private void buildTRANStree(string Num)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            TreeNode MyNode, ChildNode, CNode;
            treeView2.Nodes.Clear();
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT LineNumber,Name,LineCode FROM TransLine WHERE LineNumber=@Num ORDER BY TransLine.Name", myConnection);
            Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.Int);
            Myda.SelectCommand.Parameters["@Num"].Value = Num;
            Myda.Fill(MyDS, "Trans");

            foreach (DataRow MyRow in MyDS.Tables["Trans"].Rows)
            {
                bool Rthereis = false;
                int type = int.Parse(MyRow["LineNumber"].ToString().Trim());
                foreach (TreeNode node in treeView2.Nodes)
                {
                    if (node.Text == MyRow["LineNumber"].ToString().Trim())
                    {
                        Rthereis = true;
                        bool Cthereis = false;
                        foreach (TreeNode node1 in node.Nodes)
                            if (node1.Text == MyRow["Name"].ToString().Trim())
                            {
                                Cthereis = true;
                                CNode = new TreeNode();
                                CNode.Text = MyRow["LineCode"].ToString().Trim();
                                node1.Nodes.Add(CNode);
                            }
                        if (!Cthereis)
                        {
                            ChildNode = new TreeNode();
                            ChildNode.Text = MyRow["Name"].ToString().Trim();
                            node.Nodes.Add(ChildNode);
                            CNode = new TreeNode();
                            CNode.Text = MyRow["LineCode"].ToString().Trim();
                            ChildNode.Nodes.Add(CNode);
                        }
                    }
                }
                if (!Rthereis)
                {
                    MyNode = new TreeNode();
                    MyNode.Text = MyRow["LineNumber"].ToString().Trim();
                    treeView2.Nodes.Add(MyNode);
                    bool Cthereis = false;
                    foreach (TreeNode node1 in MyNode.Nodes)
                        if (node1.Text == MyRow["Name"].ToString().Trim())
                        {
                            Cthereis = true;
                            CNode = new TreeNode();
                            CNode.Text = MyRow["LineCode"].ToString().Trim();
                            node1.Nodes.Add(CNode);
                        }
                    if (!Cthereis)
                    {
                        ChildNode = new TreeNode();
                        ChildNode.Text = MyRow["Name"].ToString().Trim();
                        MyNode.Nodes.Add(ChildNode);
                        CNode = new TreeNode();
                        CNode.Text = MyRow["LineCode"].ToString().Trim();
                        ChildNode.Nodes.Add(CNode);
                    }
                }
            }

            MyDS.Dispose();
            Myda.Dispose();
            myConnection.Close();
            treeView2.ExpandAll();
        }
        //----------------------------CheckDate--------------------------------
        private bool CheckDate(string date, string start, string end)
        {
            bool result = false;
            string temp1 = date.Remove(4);
            start = start.Trim();
            end = end.Trim();
            if ((start != "") && (end != ""))
            {
                string temp2 = start.Remove(4);
                if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
                else
                {
                    temp1 = date.Remove(7);
                    temp1 = temp1.Remove(0, temp1.Length - 2);
                    temp2 = start.Remove(7);
                    temp2 = temp2.Remove(0, temp2.Length - 2);
                    if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                    else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
                    else
                    {
                        temp1 = date.Remove(0, date.Length - 2);
                        temp2 = start.Remove(0, start.Length - 2);
                        if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                        else result = false;
                    }
                }
                if (!result) return (result);
                else
                {
                    temp1 = date.Remove(4);
                    temp2 = end.Remove(4);
                    if (int.Parse(temp1) < int.Parse(temp2)) return (true);
                    else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
                    else
                    {
                        temp1 = date.Remove(7);
                        temp1 = temp1.Remove(0, temp1.Length - 2);
                        temp2 = end.Remove(7);
                        temp2 = temp2.Remove(0, temp2.Length - 2);
                        if (int.Parse(temp1) < int.Parse(temp2)) return (true);
                        else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
                        else
                        {
                            temp1 = date.Remove(0, date.Length - 2);
                            temp2 = end.Remove(0, end.Length - 2);
                            if ((int.Parse(temp1) < int.Parse(temp2)) || (temp1 == temp2)) return (true);
                            else return (false);
                        }
                    }
                }
            }
            return false;
        }

        //------------------------------findPPID-----------------------------
        private string findPPID(string name,string type)
        {
            if (type == "m002")
            {
                int dashPos = 0, starPos = 0;
                name = name.Replace("\\", "*");
                for (int i = name.Length - 1; i > 0; i--)
                {
                    if (name[i] == '-') dashPos = i;
                    if (name[i] == '*') { starPos = i; break; }
                }
                name = name.Remove(0, starPos + 1);
                dashPos = dashPos - (starPos + 1);
                name = name.Remove(dashPos, name.Length - dashPos);
            }
            else
            {
                int firstPos = 0, secondPos = 0;
                int count=0;
                for (int i = name.Length - 1; i > 0; i--)
                {
                    if (count < 2)
                    {
                        if (name[i] == '-')
                            if (count == 0) { secondPos = i; count++; }
                            else { firstPos = i; count++; break; }
                    }
                }
                name = name.Remove(0, firstPos + 1);
                secondPos = secondPos - (firstPos + 1);
                name = name.Remove(secondPos, name.Length - secondPos);
            }
            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = new SqlConnection(ConnectionManager.ConnectionString);
            MyCom.Connection.Open();

            MyCom.CommandText = "SELECT @result=COUNT(PPID) FROM PowerPlant WHERE PPID=@num";
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = name;
            MyCom.Parameters.Add("@result", SqlDbType.Int);
            MyCom.Parameters["@result"].Direction = ParameterDirection.Output;
            MyCom.ExecuteNonQuery();
            int result = int.Parse(MyCom.Parameters["@result"].Value.ToString().Trim());
            MyCom.Connection.Close();

            CheckPPID = true;
            if (result == 0)
            {
                int newname = int.Parse(name) - 1;
                name = newname.ToString();
                CheckPPID = false;
            }
            return name;
        }
        //--------------------------FindDate---------------------------------
        private string findDate(string name)
        {
            if (name.Contains(".xls")) name = name.Replace(".xls", "");
            int Pos = 0;
            for (int i = name.Length - 1; i > 0; i--)
                if (name[i] == '-') { Pos = i; break; }
            name = name.Remove(0, Pos + 1);
            return name;
        }
        //------------------------SetHeader2Plant----------------------------
        private void SetHeader2Plant(string package, string unit, string type)
        {
            GDHeaderGB.Visible = true;
            ODHeaderGB.Visible = true;
            MRHeaderGB.Visible = true;
            BDHeaderGb.Visible = true;
            FRHeaderGb.Visible = true;
            GDNewBtn.Visible = true;
            GDDeleteBtn.Visible = true;
            ODSaveBtn.Visible = true;
            FROKBtn.Visible = true;
            FRUpdateBtn.Visible = true;
            BDPlotBtn.Visible = true;
            MRPlotBtn.Visible = true;
            GDPlantLb.Visible = true;
            ODPlantLb.Visible = true;
            FRPlantLb.Visible = true;
            BDPlantLb.Visible = true;
            MRPlantLb.Visible = true;
            Point pnt = new Point(62, 23);
            GDPlantLb.Location = pnt;
            ODPlantLb.Location = pnt;
            MRPlantLb.Location = pnt;
            BDPlantLb.Location = pnt;
            FRPlantLb.Location = pnt;
            FRPlantPanel.Visible = false;
            FRUnitPanel.Visible = true;
            MRUnitCurGb.Visible = true;
            MRPlantCurGb.Visible = false;
            ODUnitPanel.Visible = true;
            //ODPlantPanel.Visible = false;
            GDUnitPanel.Visible = true;
            GDGasGroup.Visible = false;
            GDSteamGroup.Visible = false;
            GDHeaderPanel.Visible = true;
            ODHeaderPanel.Visible = true;
            MRHeaderPanel.Visible = true;
            FRHeaderPanel.Visible = true;
            BDHeaderPanel.Visible = true;
            BDCur1.Visible = true;
            BDCur2.Visible = true;
            L1.Visible = true;
            L1.Text = "Plant: ";
            L5.Visible = true;
            L5.Text = "Plant: ";
            L9.Visible = true;
            L9.Text = "Plant: ";
            L13.Visible = true;
            L13.Text = "Plant: ";
            L17.Visible = true;
            L17.Text = "Plant: ";
            Currentgb.Text = "CURRENT STATE";
            Maintenancegb.Text = "MAINTENANCE";
            Currentgb.Visible = false;
            Maintenancegb.Visible = false;
            GDUnitMaintenanceGB.Visible = true;
            GDPmaxLb.Text = "Pmax";
            GDPminLb.Text = "Pmin";
            GDTUpLb.Text = "Time Up";
            GDTimeDownLb.Text = "Time Down";
            GDColdLb.Text = "Time Cold Start";
            GDHotLb.Text = "Time Hot Start";
            GDRampLb.Text = "Ramp Rate";
            GDConsLb.Text = "Internal Consume";
            GDStateLb.Text = "STATE";
            GDFuelLb.Text = "FUEL";
            GDPackLb.Text = package;
            ODPackLb.Text = package;
            MRPackLb.Text = package;
            BDPackLb.Text = package;
            FRPackLb.Text = package;
            GDUnitLb.Text = unit;
            ODUnitLb.Text = unit;
            MRUnitLb.Text = unit;
            BDUnitLb.Text = unit;
            FRUnitLb.Text = unit;
            GDTypeLb.Text = type;
            ODTypeLb.Text = type;
            MRTypeLb.Text = type;
            BDTypeLb.Text = type;
            FRTypeLb.Text = type;
            BDCur1.Visible = true;
            BDCur2.Visible = true;
            MRGB.Visible = true;
            GDDeleteBtn.Text = "Edit Mode";
            GDNewBtn.Enabled = false;
            GDNewBtn.Text = "Save";
            FROKBtn.Visible = true;
            FRUpdateBtn.Enabled = false;
            //??????????????????
            FRUpdateBtn.Enabled = true;
            FRUnitCapitalTb.ReadOnly = true;
            FRUnitFixedTb.ReadOnly = true;
            FRUnitVariableTb.ReadOnly = true;
            FRUnitAmargTb1.ReadOnly = true;
            FRUnitBmargTb1.ReadOnly = true;
            FRUnitCmargTb1.ReadOnly = true;
            FRUnitAmargTb2.ReadOnly = true;
            FRUnitBmargTb2.ReadOnly = true;
            FRUnitCmargTb2.ReadOnly = true;
            FRUnitAmainTb.ReadOnly = true;
            FRUnitBmainTb.ReadOnly = true;
            FRUnitColdTb.ReadOnly = true;
            FRUnitHotTb.ReadOnly = true;
        }
        //------------------------SetHeader1Plant---------------------------
        private void SetHeader1Plant()
        {
            GDHeaderGB.Visible = true;
            ODHeaderGB.Visible = true;
            MRHeaderGB.Visible = true;
            BDHeaderGb.Visible = true;
            FRHeaderGb.Visible = true;
            GDNewBtn.Visible = true;
            GDDeleteBtn.Visible = true;
            ODSaveBtn.Visible = true;
            BDPlotBtn.Visible = false;
            MRPlotBtn.Visible = true;
            FROKBtn.Visible = false;
            FRUpdateBtn.Visible = false;
            GDPlantLb.Visible = true;
            ODPlantLb.Visible = true;
            FRPlantLb.Visible = true;
            BDPlantLb.Visible = true;
            MRPlantLb.Visible = true;
            Point pnt = new Point(62, 23);
            GDPlantLb.Location = pnt;
            ODPlantLb.Location = pnt;
            MRPlantLb.Location = pnt;
            BDPlantLb.Location = pnt;
            FRPlantLb.Location = pnt;
            FRPlantPanel.Visible = true;
            FRUnitPanel.Visible = false;
            MRUnitCurGb.Visible = false;
            MRPlantCurGb.Visible = true;
            ODUnitPanel.Visible = false;
            //ODPlantPanel.Visible = true;
            GDUnitPanel.Visible = false;
            GDGasGroup.Visible = true;
            GDSteamGroup.Visible = true;
            GDHeaderPanel.Visible = false;
            ODHeaderPanel.Visible = false;
            MRHeaderPanel.Visible = false;
            FRHeaderPanel.Visible = false;
            BDHeaderPanel.Visible = false;
            Grid230.Visible = false;
            Grid400.Visible = false;
            PlantGV1.Visible = true;
            PlantGV2.Visible = true;
            BDCur1.Visible = false;
            BDCur2.Visible = false;
            L1.Visible = true;
            L1.Text = "Plant: ";
            L5.Visible = true;
            L5.Text = "Plant: ";
            L9.Visible = true;
            L9.Text = "Plant: ";
            L13.Visible = true;
            L13.Text = "Plant: ";
            L17.Visible = true;
            L17.Text = "Plant: ";
            BDCur1.Visible = false;
            BDCur2.Visible = false;
            MRGB.Visible = true;
            Point pnt1 = new Point(30, 1);
            MRLabel1.Location = pnt1;
            MRLabel2.Location = pnt1;
            MRLabel1.Text = "ON UNITS";
            MRLabel2.Text = "POWER";
            GDDeleteBtn.Text = "Delete Unit";
            GDNewBtn.Enabled = true;
            GDNewBtn.Text = "Add Unit";
            //??????????????????age faghat neveshtan dar DB dashte bashe doroste???????????
            FRUpdateBtn.Enabled = true;
            FROKBtn.Visible = false;
        }
        //-----------------------------SetHeader1Transmission------------------------
        private void SetHeader1Transmission()
        {
            GDHeaderGB.Visible = true;
            ODHeaderGB.Visible = true;
            MRHeaderGB.Visible = true;
            BDHeaderGb.Visible = true;
            FRHeaderGb.Visible = true;
            GDNewBtn.Visible = true;
            GDDeleteBtn.Visible = true;
            ODSaveBtn.Visible = false;
            BDPlotBtn.Visible = false;
            MRPlotBtn.Visible = true;
            FROKBtn.Visible = false;
            FRUpdateBtn.Visible = false;
            GDHeaderPanel.Visible = false;
            ODHeaderPanel.Visible = false;
            MRHeaderPanel.Visible = false;
            FRHeaderPanel.Visible = false;
            BDHeaderPanel.Visible = false;
            FRPlantPanel.Visible = false;
            FRUnitPanel.Visible = false;
            ODUnitPanel.Visible = false;
            //ODPlantPanel.Visible = false;
            BDCur1.Visible = false;
            BDCur2.Visible = false;
            GDPlantLb.Text = "Tehran";
            GDPlantLb.Visible = true;
            MRPlantLb.Visible = true;
            ODPlantLb.Visible = true;
            FRPlantLb.Visible = true;
            BDPlantLb.Visible = true;
            Point pnt = new Point(100, 23);
            GDPlantLb.Location = pnt;
            ODPlantLb.Location = pnt;
            MRPlantLb.Location = pnt;
            BDPlantLb.Location = pnt;
            FRPlantLb.Location = pnt;
            L1.Visible = true;
            L1.Text = "Transmission: ";
            //L5.Visible = false;
            L9.Visible = true;
            L9.Text = "Transmission: ";
            //L13.Visible = false;
            //L17.Visible = false;
            L5.Text = "Transmission: ";
            L13.Text = "Transmission: ";
            L17.Text = "Transmission: ";
            Grid230.Visible = true;
            Grid400.Visible = true;
            PlantGV1.Visible = false;
            PlantGV2.Visible = false;
            Currentgb.Text = "OTHER OWNER";
            Point pnt1 = new Point(1, 1);
            MRLabel1.Location = pnt1;
            MRLabel2.Location=pnt1;
            MRLabel1.Text = "Capacity Payment";
            MRLabel2.Text = "Energy Payment";
            GDGasGroup.Text = "400";
            GDSteamGroup.Text = "230";
            GDGasGroup.Visible = true;
            GDSteamGroup.Visible = true;
            GDUnitPanel.Visible = false;
            MRGB.Visible = true;
            MRUnitCurGb.Visible = false;
            MRPlantCurGb.Visible = true;
            GDDeleteBtn.Text = "Delete Line";
            GDNewBtn.Enabled = true;
            GDNewBtn.Text = "Add Line";
        }
        //------------------------------SetHeader2Transmission----------------------
        private void SetHeader2Transmission()
        {
            GDHeaderGB.Visible = true;
            ODHeaderGB.Visible = true;
            MRHeaderGB.Visible = true;
            BDHeaderGb.Visible = true;
            FRHeaderGb.Visible = true;
            GDNewBtn.Visible = true;
            GDDeleteBtn.Visible = true;
            ODSaveBtn.Visible = false;
            BDPlotBtn.Visible = false;
            MRPlotBtn.Visible = true;
            FROKBtn.Visible = false;
            FRUpdateBtn.Visible = false;
            GDHeaderPanel.Visible = false;
            ODHeaderPanel.Visible = false;
            MRHeaderPanel.Visible = false;
            FRHeaderPanel.Visible = false;
            BDHeaderPanel.Visible = false;
            GDPlantLb.Visible = true;
            MRPlantLb.Visible = true;
            ODPlantLb.Visible = true;
            FRPlantLb.Visible = true;
            BDPlantLb.Visible = true;
            Point pnt = new Point(62, 23);
            GDPlantLb.Location = pnt;
            ODPlantLb.Location = pnt;
            MRPlantLb.Location = pnt;
            BDPlantLb.Location = pnt;
            FRPlantLb.Location = pnt;
            L1.Visible = true;
            L1.Text = "Line: ";
            //L5.Visible = false;
            L9.Visible = true;
            L9.Text = "Line: ";
            //L13.Visible = false;
            //L17.Visible = false;
            L5.Text = "Line: ";
            L13.Text = "Line: ";
            L17.Text = "Line: ";
            Currentgb.Text = "OTHER OWNER";
            Maintenancegb.Text = "OUT SERVICE";
            Currentgb.Visible = true;
            Maintenancegb.Visible = true;
            GDUnitMaintenanceGB.Visible = false;
            GDPmaxLb.Text = "From Bus";
            GDPminLb.Text = "To Bus";
            GDTUpLb.Text = "Circuit ID";
            GDTimeDownLb.Text = "Line-R";
            GDColdLb.Text = "Line-X";
            GDHotLb.Text = "Suseptance";
            GDRampLb.Text = "Line Length";
            GDConsLb.Text = "Status";
            GDStateLb.Text = "Genco";
            GDFuelLb.Text = "Percent";
            GDGasGroup.Visible = false;
            GDSteamGroup.Visible = false;
            GDUnitPanel.Visible = true;
            MRGB.Visible = true;
            MRUnitCurGb.Visible = false;
            MRPlantCurGb.Visible = true;
            GDDeleteBtn.Text = "Edit Mode";
            GDNewBtn.Enabled = false;
            GDNewBtn.Text = "Save";
        }

        //----------------------------------------treeView1_NodeMouseClick-------------------------------

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {

            if (MainTabs.SelectedTab.Name == "BiddingStrategy")
            {
                MainTabs_SelectedIndexChanged(sender, e);
            }


            //string[] PPNameArray = new string[20];
            //for (int x = 0; x < 20; x++)
            //    PPNameArray = "";

            //Clear Grids
            PlantGV1.DataSource = null;
            if (PlantGV1.Rows != null) PlantGV1.Rows.Clear();
            PlantGV2.DataSource = null;
            if (PlantGV2.Rows != null) PlantGV2.Rows.Clear();

            //GeneralDataHeaderPanel.Visible = false;
            string PPName = "";
            if (e.Node.Parent != null) //be sure the root isnot selected!
            {
                if (e.Node.Parent.Text == "Transmission")
                {
                    SetHeader1Transmission();
                    string TransType = e.Node.Text;
                    TransType = TransType.Trim();
                    MRPlantLb.Text = TransType + " (KV)";
                    BDPlantLb.Text = TransType + " (KV)";
                    FRPlantLb.Text = TransType + " (KV)";
                    ODPlantLb.Text = TransType + " (KV)";
                    buildTRANStree(TransType);
                    FillTransmissionGrid("230");
                    FillTransmissionGrid("400");
                    line = int.Parse(TransType);
                    //TAB : Market Result
                    FillMRTransmission(TransType);
                }
                else if (e.Node.Parent.Text == "Plant")
                {
                    SetHeader1Plant();
                    PPName = e.Node.Text;
                    PPName = PPName.Trim();

                    //Detect PPID

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Connection = new SqlConnection(ConnectionManager.ConnectionString);
                    MyCom.Connection.Open();

                    MyCom.CommandText = "SELECT @num=PPID FROM PowerPlant WHERE PPName=@name";
                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                    MyCom.Parameters.Add("@name", SqlDbType.NChar, 20);
                    MyCom.Parameters["@num"].Direction = ParameterDirection.Output;
                    MyCom.Parameters["@name"].Value = PPName;
                    MyCom.ExecuteNonQuery();
                    string id = MyCom.Parameters["@num"].Value.ToString().Trim();
                    MyCom.Connection.Close();

                    buildPPtree(id);
                    PPID = id;
                    GDPlantLb.Text = PPName;
                    ODPlantLb.Text = PPName;
                    MRPlantLb.Text = PPName;
                    BDPlantLb.Text = PPName;
                    FRPlantLb.Text = PPName;
                    //TAB : GENERAL DATA
                    FillPlantGrid(id);
                    //TAB: OPERATIONAL DATA
                    //FillODValues(id);
                    //TAB :MARKETRESULTS
                    FillMRVlues(id);
                    //TAB : FINANCIAL REPORT
                    FillFRValues(id);
                    //Read Exsiting Plants From PowerPlant
                    //DataSet MyDS = new DataSet();
                    //SqlDataAdapter Myda = new SqlDataAdapter();
                    //Myda.SelectCommand = new SqlCommand("SELECT PPName FROM PowerPlant", MyConnection);
                    //Myda.Fill(MyDS, "ppname");
                    //int ind = 0;
                    //foreach (DataRow MyRow in MyDS.Tables["ppname"].Rows)
                    //{
                    //    PPNameArray[ind] = MyRow["PPName"].ToString().Trim();
                    //    ind++;
                    //}
                }
                else treeView2.Nodes.Clear();
            }
            else treeView2.Nodes.Clear();

            //switch (PPName)
            //{
            //    case "Besat":
            //        buildPPtree("101");
            //        PPID = 101;
            //        GDPlantLb.Text = "Besat";
            //        ODPlantLb.Text = "Besat";
            //        MRPlantLb.Text = "Besat";
            //        BDPlantLb.Text = "Besat";
            //        FRPlantLb.Text = "Besat";
            //        //TAB : GENERAL DATA
            //        FillPlantGrid("101");
            //        //TAB: OPERATIONAL DATA
            //        FillODValues("101");
            //        //TAB :MARKETRESULTS
            //        FillMRVlues("101");
            //        //TAB : FINANCIAL REPORT
            //        FillFRValues("101");
            //        break;
            //    case "Montazer Qaem":
            //        buildPPtree("131");
            //        PPID = 131;
            //        GDPlantLb.Text = "Montazer Qaem";
            //        ODPlantLb.Text = "Montazer Qaem";
            //        MRPlantLb.Text = "Montazer Qaem";
            //        BDPlantLb.Text = "Montazer Qaem";
            //        FRPlantLb.Text = "Montazer Qaem";
            //        //TAB : GENERAL DATA
            //        FillPlantGrid("131");
            //        //TAB: OPERATIONAL DATA
            //        FillODValues("131");
            //        //TAB :MARKETRESULTS
            //        FillMRVlues("131");
            //        //TAB : FINANCIAL REPORT
            //        FillFRValues("131");
            //        break;
            //    case "Parand":
            //        buildPPtree("104");
            //        PPID = 104;
            //        GDPlantLb.Text = "Parand";
            //        ODPlantLb.Text = "Parand";
            //        MRPlantLb.Text = "Parand";
            //        BDPlantLb.Text = "Parand";
            //        FRPlantLb.Text = "Parand";
            //        //TAB : GENERAL DATA
            //        FillPlantGrid("104");
            //        //TAB: OPERATIONAL DATA
            //        FillODValues("104");
            //        //TAB :MARKETRESULTS
            //        FillMRVlues("104");
            //        //TAB : FINANCIAL REPORT
            //        FillFRValues("104");
            //        break;
            //    case "Tarasht":
            //        buildPPtree("133");
            //        PPID = 133;
            //        GDPlantLb.Text = "Tarasht";
            //        ODPlantLb.Text = "Tarasht";
            //        MRPlantLb.Text = "Tarasht";
            //        BDPlantLb.Text = "Tarasht";
            //        FRPlantLb.Text = "Tarasht";
            //        //TAB : GENERAL DATA
            //        FillPlantGrid("133");
            //        //TAB: OPERATIONAL DATA
            //        FillODValues("133");
            //        //TAB :MARKETRESULTS
            //        FillMRVlues("133");
            //        //TAB : FINANCIAL REPORT
            //        FillFRValues("133");
            //        break;
            //    case "Rey":
            //        buildPPtree("138");
            //        PPID = 138;
            //        GDPlantLb.Text = "Rey";
            //        ODPlantLb.Text = "Rey";
            //        MRPlantLb.Text = "Rey";
            //        BDPlantLb.Text = "Rey";
            //        FRPlantLb.Text = "Rey";
            //        //TAB : GENERAL DATA
            //        FillPlantGrid("138");
            //        //TAB: OPERATIONAL DATA
            //        FillODValues("138");
            //        //TAB :MARKETRESULTS
            //        FillMRVlues("138");
            //        //TAB : FINANCIAL REPORT
            //        FillFRValues("138");
            //        break;
            //    case "Shahid Rajayi":
            //        buildPPtree("144");
            //        PPID = 144;
            //        GDPlantLb.Text = "Shahid Rajayi";
            //        ODPlantLb.Text = "Shahid Rajayi";
            //        MRPlantLb.Text = "Shahid Rajayi";
            //        BDPlantLb.Text = "Shahid Rajayi";
            //        FRPlantLb.Text = "Shahid Rajayi";
            //        //TAB : GENERAL DATA
            //        FillPlantGrid("144");
            //        //TAB: OPERATIONAL DATA
            //        FillODValues("144");
            //        //TAB :MARKETRESULTS
            //        FillMRVlues("144");
            //        //TAB : FINANCIAL REPORT
            //        FillFRValues("144");
            //        break;
            //    case "Qom":
            //        buildPPtree("149");
            //        PPID = 149;
            //        GDPlantLb.Text = "Qom";
            //        ODPlantLb.Text = "Qom";
            //        MRPlantLb.Text = "Qom";
            //        BDPlantLb.Text = "Qom";
            //        FRPlantLb.Text = "Qom";
            //        //TAB : GENERAL DATA
            //        FillPlantGrid("149");
            //        //TAB: OPERATIONAL DATA
            //        FillODValues("149");
            //        //TAB :MARKETRESULTS
            //        FillMRVlues("149");
            //        //TAB : FINANCIAL REPORT
            //        FillFRValues("149");
            //        break;
            //}
        }
        //-----------------------------------------treeView2_NodeMouseClick-----------------------------------
        private void treeView2_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Parent != null) //be sure the root isnot selected!
            {
                string parent = e.Node.Parent.Text;
                //if a unit of plants is selected
                if ((parent.Contains("Steam")) || (parent.Contains("Gas")) || (parent.Contains("Combined")))
                {
                    string unit = e.Node.Text;
                    unit = unit.Trim();
                    string package = e.Node.Parent.Text;
                    package = package.Trim();
                    string type = unit.ToLower();
                    if (type.Contains("steam")) type = "Steam";
                    else type = "Gas";

                    //Get Index Of Selected Unit(Row) in GridView (In Generad data Tab)
                    int index = 0;
                    if (GDSteamGroup.Text.Contains(package))
                        for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                        {
                            if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                                index = i;
                        }
                    else
                        for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                        {
                            if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                                index = i;
                        }

                    //set HeaderPanel of Tabs
                    SetHeader2Plant(package, unit, type);

                    //TAB:GENERAL DATA
                    // set values of TextBoxes
                    FillGDUnit(unit, package, index);

                    //TAB:Operational DATA
                    // set values of TextBoxes
                    FillODUnit(unit, package, index);

                    //TAB : MARKET RESULTS
                    //Set TextBoxes And Grids
                    FillMRUnit(unit, package, index);

                    //TAB : BID DATA
                    //SET TextBoxes AND DataGrid
                    FillBDUnit(unit, package, index);

                    //TAB : FINANCIAL REPORT
                    //SET TextBoxes
                    FillFRUnit(unit, package, index);

                }

                //if a line of Transmission is selected
                else if (e.Node.Parent.Parent != null)
                {
                    SetHeader2Transmission();
                    string TransLine = e.Node.Text.Trim();
                    string TransType = e.Node.Parent.Parent.Text.Trim();
                    GDPlantLb.Text = TransLine;
                    ODPlantLb.Text = TransLine;
                    MRPlantLb.Text = TransLine;
                    BDPlantLb.Text = TransLine;
                    FRPlantLb.Text = TransLine;
                    FillGDLine(TransType, TransLine);
                    FillMRLine(TransType, TransLine);
                }
            }
        }
        //------------------------OutServiceCheck_CheckStateChanged-------------------------
        //private void OutServiceCheck_CheckStateChanged(object sender, EventArgs e)
        //{
        //    if (OutServiceCheck.Checked)
        //    {
        //        ODServiceStartDate.Enabled = true;
        //        ODServiceEndDate.Enabled = true;
        //    }
        //    else
        //    {
        //        ODServiceStartDate.Enabled = false;
        //        ODServiceEndDate.Enabled = false;
        //    }
        //}
        //---------------------------------SecondFuelCheck_CheckedChanged----------------------------
        //private void SecondFuelCheck_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (SecondFuelCheck.Checked)
        //    {
        //        ODFuelStartDate.Enabled = true;
        //        ODFuelEndDate.Enabled = true;
        //        ODFuelQuantityTB.Enabled = true;
        //    }
        //    else
        //    {
        //        ODFuelStartDate.Enabled = false;
        //        ODFuelEndDate.Enabled = false;
        //        ODFuelQuantityTB.Enabled = false;
        //    }
        //}
        //------------------------------ODPowerMinCheck_CheckedChanged-----------------------------
        //private void ODPowerMinCheck_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (ODPowerMinCheck.Checked)
        //    {
        //        ODPowerStartDate.Enabled = true;
        //        ODPowerEndDate.Enabled = true;
        //        ODPowerGrid1.Enabled = true;
        //        ODPowerGrid2.Enabled = true;
        //    }
        //    else
        //    {
        //        ODPowerStartDate.Enabled = false;
        //        ODPowerEndDate.Enabled = false;
        //        ODPowerGrid1.Enabled = false;
        //        ODPowerGrid2.Enabled = false;
        //    }
        //}

        //----------------------------------PlantGV1_ColumnHeaderMouseClick-------------------------------
        private void PlantGV1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Detect Farsi Date
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");
            int myhour = DateTime.Now.Hour;
            string type = GDGasGroup.Text;
            type = type.Trim();
            if (type == "Combined Cycle") type = "CC";
            FillPlantGV1Remained(mydate, PPID, type, myhour);

        }
        //----------------------------PlantGV2_ColumnHeaderMouseClick-----------------------------------------
        private void PlantGV2_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Detect Farsi Date
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");
            int myhour = DateTime.Now.Hour;
            string type = GDSteamGroup.Text;
            type = type.Trim();
            if (type == "Combined Cycle") type = "CC";
            FillPlantGV2Remained(mydate, PPID, type, myhour);
        }
        //------------------------------------ODFuelQuantityTB_Validated--------------------------------
        //private void ODFuelQuantityTB_Validated(object sender, EventArgs e)
        //{
        //    if (CheckValidated(ODFuelQuantityTB.Text, 1))
        //        errorProvider1.SetError(ODFuelQuantityTB, "");
        //    else if (SecondFuelCheck.Checked)
        //        errorProvider1.SetError(ODFuelQuantityTB, "just real number!");
        //}
        //---------------------------------CheckValidated----------------------------------------
        private bool CheckValidated(string text, int type)
        {
            if (text != "")
            {
                if (type == 1)
                {
                    try
                    {
                        double k = double.Parse(text);
                        return true;
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
                else
                {
                    try
                    {
                        int k = int.Parse(text);
                        return true;
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
            }
            else return true;
        }
        //---------------------------ODPowerGrid1_Validated-----------------------------
        //private void ODPowerGrid1_Validated(object sender, EventArgs e)
        //{
        //    for (int i = 0; i < (ODPowerGrid1.ColumnCount - 1); i++)
        //    {
        //        if (ODPowerGrid1.Rows[0].Cells[i].Value != null)
        //        {
        //            if (CheckValidated(ODPowerGrid1.Rows[0].Cells[i].Value.ToString(), 1))
        //                errorProvider1.SetError(ODPowerGrid1, "");
        //            else if (ODPowerMinCheck.Checked)
        //                errorProvider1.SetError(ODPowerGrid1, "Just real number!");
        //        }
        //    }
        //}
        //----------------------------ODPowerGrid2_Validated-------------------------
        //private void ODPowerGrid2_Validated(object sender, EventArgs e)
        //{
        //    for (int i = 0; i < (ODPowerGrid2.ColumnCount - 1); i++)
        //    {
        //        if (ODPowerGrid2.Rows[0].Cells[i].Value != null)
        //        {
        //            if (CheckValidated(ODPowerGrid2.Rows[0].Cells[i].Value.ToString(), 1))
        //                errorProvider1.SetError(ODPowerGrid2, "");
        //            else if (ODPowerMinCheck.Checked)
        //                errorProvider1.SetError(ODPowerGrid2, "Just real number!");
        //        }
        //    }
        //}
        //-----------------------------ODUnitFuelTB_Validated----------------------------------
        private void ODUnitFuelTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(ODUnitFuelTB.Text, 1))
                errorProvider1.SetError(ODUnitFuelTB, "");
            else if (ODUnitFuelCheck.Checked)
                errorProvider1.SetError(ODUnitFuelTB, "just real number!");
        }
        //-------------------------------ODUnitPowerGrid1_Validated-------------------------------------
        private void ODUnitPowerGrid1_Validated(object sender, EventArgs e)
        {
            for (int i = 0; i < (ODUnitPowerGrid1.ColumnCount - 1); i++)
            {
                if (ODUnitPowerGrid1.Rows[0].Cells[i].Value != null)
                {
                    if (CheckValidated(ODUnitPowerGrid1.Rows[0].Cells[i].Value.ToString(), 1))
                        errorProvider1.SetError(ODUnitPowerGrid1, "");
                    else if (ODUnitPowerCheck.Checked)
                        errorProvider1.SetError(ODUnitPowerGrid1, "Just real number!");
                }
            }
        }
        //-------------------------------ODUnitPowerGrid2_Validated-------------------------------
        private void ODUnitPowerGrid2_Validated(object sender, EventArgs e)
        {
            for (int i = 0; i < (ODUnitPowerGrid2.ColumnCount - 1); i++)
            {
                if (ODUnitPowerGrid2.Rows[0].Cells[i].Value != null)
                {
                    if (CheckValidated(ODUnitPowerGrid2.Rows[0].Cells[i].Value.ToString(), 1))
                        errorProvider1.SetError(ODUnitPowerGrid2, "");
                    else if (ODUnitPowerCheck.Checked)
                        errorProvider1.SetError(ODUnitPowerGrid2, "Just Number!");
                }
            }
        }
        //---------------------------ODUnitPowerCheck_CheckedChanged--------------------------------------
        private void ODUnitPowerCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (ODUnitPowerCheck.Checked)
            {
                ODUnitPowerStartDate.Enabled = true;
                ODUnitPowerEndDate.Enabled = true;
                ODUnitPowerGrid1.Enabled = true;
                ODUnitPowerGrid2.Enabled = true;
            }
            else
            {
                ODUnitPowerStartDate.Enabled = false;
                ODUnitPowerEndDate.Enabled = false;
                ODUnitPowerGrid1.Enabled = false;
                ODUnitPowerGrid2.Enabled = false;
            }
        }
        //----------------------------------ODUnitOutCheck_CheckedChanged------------------------------
        private void ODUnitOutCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (ODUnitOutCheck.Checked)
            {
                ODUnitServiceStartDate.Enabled = true;
                ODUnitServiceEndDate.Enabled = true;
                ODOutServiceStartHour.Enabled = true;
                ODOutServiceEndHour.Enabled = true;
            }
            else
            {
                ODUnitServiceStartDate.Enabled = false;
                ODUnitServiceEndDate.Enabled = false;
                ODOutServiceStartHour.Enabled = false;
                ODOutServiceEndHour.Enabled = false;
            }
        }
        //-----------------------------ODUnitMainCheck_CheckedChanged-------------------------------
        private void ODUnitMainCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (ODUnitMainCheck.Checked)
            {
                ODUnitMainStartDate.Enabled = true;
                ODUnitMainEndDate.Enabled = true;
                ODMaintenanceStartHour.Enabled = true;
                ODMaintenanceEndHour.Enabled = true;
                ODMaintenanceType.Enabled = true;
            }
            else
            {
                ODUnitMainStartDate.Enabled = false;
                ODUnitMainEndDate.Enabled = false;
                ODMaintenanceEndHour.Enabled = false;
                ODMaintenanceStartHour.Enabled = false;
                ODMaintenanceType.Enabled = false;
            }
        }
        //--------------------------ODUnitFuelCheck_CheckedChanged--------------------------------
        private void ODUnitFuelCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (ODUnitFuelCheck.Checked)
            {
                ODUnitFuelStartDate.Enabled = true;
                ODUnitFuelEndDate.Enabled = true;
                ODSecondFuelEndHour.Enabled = true;
                ODSecondFuelStartHour.Enabled = true;
                ODUnitFuelTB.Enabled = true;
            }
            else
            {
                ODUnitFuelStartDate.Enabled = false;
                ODUnitFuelEndDate.Enabled = false;
                ODSecondFuelStartHour.Enabled = false;
                ODSecondFuelEndHour.Enabled = false;
                ODUnitFuelTB.Enabled = false;
            }
        }
        //--------------------------FRPlantBenefitTB_Validated--------------------------------
        private void FRPlantBenefitTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantBenefitTB.Text, 1))
                errorProvider1.SetError(FRPlantBenefitTB, "");
            else errorProvider1.SetError(FRPlantBenefitTB, "just real number!");
        }
        //------------------------------FRPlantIncomeTb_Validated------------------------------
        private void FRPlantIncomeTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantIncomeTb.Text, 1))
                errorProvider1.SetError(FRPlantIncomeTb, "");
            else errorProvider1.SetError(FRPlantIncomeTb, "just real number!");
        }
        //-------------------------------FRPlantCostTb_Validated---------------------------------
        private void FRPlantCostTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantCostTb.Text, 1))
                errorProvider1.SetError(FRPlantCostTb, "");
            else errorProvider1.SetError(FRPlantCostTb, "just real number!");
        }
        //---------------------------------FRPlantAvaCapTb_Validated----------------------------------
        private void FRPlantAvaCapTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantAvaCapTb.Text, 1))
                errorProvider1.SetError(FRPlantAvaCapTb, "");
            else errorProvider1.SetError(FRPlantAvaCapTb, "just real number!");
        }
        //---------------------------------FRPlantTotalPowerTb_Validated---------------------------------
        private void FRPlantTotalPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantTotalPowerTb.Text, 1))
                errorProvider1.SetError(FRPlantTotalPowerTb, "");
            else errorProvider1.SetError(FRPlantTotalPowerTb, "just real number!");
        }
        //------------------------------------FRPlantBidPowerTb_Validated-------------------------------------
        private void FRPlantBidPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantBidPowerTb.Text, 1))
                errorProvider1.SetError(FRPlantBidPowerTb, "");
            else errorProvider1.SetError(FRPlantBidPowerTb, "just real number!");
        }
        //--------------------------------------FRPlantULPowerTb_Validated--------------------------------------
        private void FRPlantULPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantULPowerTb.Text, 1))
                errorProvider1.SetError(FRPlantULPowerTb, "");
            else errorProvider1.SetError(FRPlantULPowerTb, "just real number!");
        }
        //---------------------------------------FRPlantIncPowerTb_Validated---------------------------------
        private void FRPlantIncPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantIncPowerTb.Text, 1))
                errorProvider1.SetError(FRPlantIncPowerTb, "");
            else errorProvider1.SetError(FRPlantIncPowerTb, "just real number!");
        }
        //---------------------------------------FRPlantDecPowerTb_Validated------------------------------------
        private void FRPlantDecPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantDecPowerTb.Text, 1))
                errorProvider1.SetError(FRPlantDecPowerTb, "");
            else errorProvider1.SetError(FRPlantDecPowerTb, "just real number!");
        }
        //----------------------------------------FRPlantCapPayTb_Validated-----------------------------------
        private void FRPlantCapPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantCapPayTb.Text, 1))
                errorProvider1.SetError(FRPlantCapPayTb, "");
            else errorProvider1.SetError(FRPlantCapPayTb, "just real number!");
        }
        //---------------------------------------FRPlantEnergyPayTb_Validated-------------------------------------
        private void FRPlantEnergyPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantEnergyPayTb.Text, 1))
                errorProvider1.SetError(FRPlantEnergyPayTb, "");
            else errorProvider1.SetError(FRPlantEnergyPayTb, "just real number!");
        }
        //------------------------------------FRPlantBidPayTb_Validated--------------------------------------------
        private void FRPlantBidPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantBidPayTb.Text, 1))
                errorProvider1.SetError(FRPlantBidPayTb, "");
            else errorProvider1.SetError(FRPlantBidPayTb, "just real number!");
        }
        //------------------------------------FRPlantULPayTb_Validated----------------------------------------
        private void FRPlantULPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantULPayTb.Text, 1))
                errorProvider1.SetError(FRPlantULPayTb, "");
            else errorProvider1.SetError(FRPlantULPayTb, "just real number!");
        }
        //------------------------------------FRPlantIncPayTb_Validated-----------------------------------
        private void FRPlantIncPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantIncPayTb.Text, 1))
                errorProvider1.SetError(FRPlantIncPayTb, "");
            else errorProvider1.SetError(FRPlantIncPayTb, "just real number!");
        }
        //-----------------------------------FRPlantDecPayTb_Validated----------------------------------------
        private void FRPlantDecPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantDecPayTb.Text, 1))
                errorProvider1.SetError(FRPlantDecPayTb, "");
            else errorProvider1.SetError(FRPlantDecPayTb, "just real number!");
        }
        //-----------------------------------RUnitCapacityTb_Validated----------------------------------------
        private void FRUnitCapacityTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitCapacityTb.Text, 1))
                errorProvider1.SetError(FRUnitCapacityTb, "");
            else errorProvider1.SetError(FRUnitCapacityTb, "just real number!");
        }
        //------------------------------------FRUnitTotalPowerTb_Validated-------------------------------------
        private void FRUnitTotalPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitTotalPowerTb.Text, 1))
                errorProvider1.SetError(FRUnitTotalPowerTb, "");
            else errorProvider1.SetError(FRUnitTotalPowerTb, "just real number!");
        }
        //-----------------------------------FRUnitULPowerTb_Validated-------------------------------------------
        private void FRUnitULPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitULPowerTb.Text, 1))
                errorProvider1.SetError(FRUnitULPowerTb, "");
            else errorProvider1.SetError(FRUnitULPowerTb, "just real number!");
        }
        //-------------------------------------FRUnitCapPayTb_Validated--------------------------------------------
        private void FRUnitCapPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitCapPayTb.Text, 1))
                errorProvider1.SetError(FRUnitCapPayTb, "");
            else errorProvider1.SetError(FRUnitCapPayTb, "just real number!");
        }
        //--------------------------------------FRUnitEneryPayTb_Validated-----------------------------------------
        private void FRUnitEneryPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitEneryPayTb.Text, 1))
                errorProvider1.SetError(FRUnitEneryPayTb, "");
            else errorProvider1.SetError(FRUnitEneryPayTb, "just real number!");
        }
        //---------------------------------------FRUnitIncomeTb_Validated------------------------------------------
        private void FRUnitIncomeTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitIncomeTb.Text, 1))
                errorProvider1.SetError(FRUnitIncomeTb, "");
            else errorProvider1.SetError(FRUnitIncomeTb, "just real number!");
        }


        private void DisableUnitTab(bool enableStatues, string unitType)
        {
            
            GDNewBtn.Enabled = !enableStatues;
            GDcapacityTB.ReadOnly = enableStatues;
            GDPmaxTB.ReadOnly = enableStatues;
            GDPminTB.ReadOnly = enableStatues;
            GDTimeUpTB.ReadOnly = enableStatues;
            GDTimeDownTB.ReadOnly = enableStatues;
            GDTimeColdStartTB.ReadOnly = enableStatues;
            GDTimeHotStartTB.ReadOnly = enableStatues;
            GDRampRateTB.ReadOnly = enableStatues;

            txtMaintHGPDur.ReadOnly = enableStatues;
            txtMaintHGPHours.ReadOnly = enableStatues;
            if (unitType == "Steam")
            {
                txtMaintHGPDur.ReadOnly = true;
                txtMaintHGPHours.ReadOnly = true;
            }

            txtDurabilityHours.ReadOnly = enableStatues;
            txtMaintCIDur.ReadOnly = enableStatues;
            txtMaintCIHours.ReadOnly = enableStatues;
            txtMaintMODur.ReadOnly = enableStatues;
            txtMaintMOHours.ReadOnly = enableStatues;
            datePickerDurability.Readonly = enableStatues;
            datePickerDurability.Enabled = !enableStatues;
            
        }
        //-----------------------------------addToolStripMenuItem_Click--------------------------------------------------
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //NewPlant.Show();
            //this.Hide();
            AddPlantForm newPlant = new AddPlantForm();
            DialogResult result = newPlant.ShowDialog();
            if (result == DialogResult.OK) buildTreeView1();
        }
        //-------------------------------------PlantGV1_DataError-------------------------------------------------------
        private void PlantGV1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            if ((!PlantGV1.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 1))
            {
                try
                {
                    int i = int.Parse(PlantGV1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
        }
        //------------------------------------------PlantGV2_DataError----------------------------------------
        private void PlantGV2_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            if ((!PlantGV2.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 1))
            {
                try
                {
                    int i = int.Parse(PlantGV2.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
        }

        //-----------------------------------GDcapacityTB_Validated------------------------------------
        private void GDcapacityTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(GDcapacityTB.Text, 1))
                errorProvider1.SetError(GDcapacityTB, "");
            else errorProvider1.SetError(GDcapacityTB, "just real number!");

        }
        //------------------------------------GDPmaxTB_Validated------------------------------------
        private void GDPmaxTB_Validated(object sender, EventArgs e)
        {
            if (Currentgb.Text.Contains("STATE"))
            {
                if (CheckValidated(GDPmaxTB.Text, 1))
                    errorProvider1.SetError(GDPmaxTB, "");
                else errorProvider1.SetError(GDPmaxTB, "just real number!");
            }
            else
            {
                if (CheckValidated(GDPmaxTB.Text, 0))
                    errorProvider1.SetError(GDPmaxTB, "");
                else errorProvider1.SetError(GDPmaxTB, "just Integer!");
            }
        }
        //-----------------------------------GDPminTB_Validated--------------------------------------
        private void GDPminTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(GDPminTB.Text, 0))
                errorProvider1.SetError(GDPminTB, "");
            else errorProvider1.SetError(GDPminTB, "just integer!");
        }
        //-----------------------------------GDTimeUpTB_Validated--------------------------------
        private void GDTimeUpTB_Validated(object sender, EventArgs e)
        {
            if (Currentgb.Text.Contains("STATE"))
            {
                if (CheckValidated(GDTimeUpTB.Text, 0))
                    errorProvider1.SetError(GDTimeUpTB, "");
                else errorProvider1.SetError(GDTimeUpTB, "just integer!");
            }
        }
        //--------------------------------GDTimeDownTB_Validated---------------------------------------
        private void GDTimeDownTB_Validated(object sender, EventArgs e)
        {
            if (Currentgb.Text.Contains("STATE"))
            {
                if (CheckValidated(GDTimeDownTB.Text, 0))
                    errorProvider1.SetError(GDTimeDownTB, "");
                else errorProvider1.SetError(GDTimeDownTB, "just integer!");
            }
            else
            {
                if (CheckValidated(GDTimeDownTB.Text, 1))
                    errorProvider1.SetError(GDTimeDownTB, "");
                else errorProvider1.SetError(GDTimeDownTB, "just real number!");
            }
        }
        //----------------------------------GDTimeColdStartTB_Validated-------------------------------------
        private void GDTimeColdStartTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(GDTimeColdStartTB.Text, 1))
                errorProvider1.SetError(GDTimeColdStartTB, "");
            else errorProvider1.SetError(GDTimeColdStartTB, "just real number!");
        }
        //--------------------------------GDTimeHotStartTB_Validated---------------------------------------
        private void GDTimeHotStartTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(GDTimeHotStartTB.Text, 1))
                errorProvider1.SetError(GDTimeHotStartTB, "");
            else errorProvider1.SetError(GDTimeHotStartTB, "just real number!");
        }
        //-------------------------------GDRampRateTB_Validated-------------------------------------
        private void GDRampRateTB_Validated(object sender, EventArgs e)
        {
            if (Currentgb.Text.Contains("STATE"))
            {
                if (CheckValidated(GDRampRateTB.Text, 0))
                    errorProvider1.SetError(GDRampRateTB, "");
                else errorProvider1.SetError(GDRampRateTB, "just integer!");
            }
            else
            {
                if (CheckValidated(GDRampRateTB.Text, 1))
                    errorProvider1.SetError(GDRampRateTB, "");
                else errorProvider1.SetError(GDRampRateTB, "just real number!");
            }
        }
        //------------------------------GDIntConsumeTB_Validated-----------------------------------
        private void GDIntConsumeTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(GDIntConsumeTB.Text, 1))
                errorProvider1.SetError(GDIntConsumeTB, "");
            else errorProvider1.SetError(GDIntConsumeTB, "just real number!");
        }
        //----------------------------------FRUnitFixedTb_Validated-------------------------------------
        private void FRUnitFixedTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitFixedTb.Text, 1))
                errorProvider1.SetError(FRUnitFixedTb, "");
            else errorProvider1.SetError(FRUnitFixedTb, "just real number!");
        }
        //-----------------------------------FRUnitVariableTb_Validated----------------------------------
        private void FRUnitVariableTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitVariableTb.Text, 1))
                errorProvider1.SetError(FRUnitVariableTb, "");
            else errorProvider1.SetError(FRUnitVariableTb, "just real number!");
        }
        //----------------------------------FRUnitAmargTb1_Validated------------------------------------
        private void FRUnitAmargTb1_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitAmargTb1.Text, 1))
                errorProvider1.SetError(FRUnitAmargTb1, "");
            else errorProvider1.SetError(FRUnitAmargTb1, "just real number!");
        }
        //----------------------------------FRUnitBmargTb1_Validated--------------------------------------
        private void FRUnitBmargTb1_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitBmargTb1.Text, 1))
                errorProvider1.SetError(FRUnitBmargTb1, "");
            else errorProvider1.SetError(FRUnitBmargTb1, "just real number!");
        }
        //-------------------------------------FRUnitCmargTb1_Validated-------------------------------------
        private void FRUnitCmargTb1_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitCmargTb1.Text, 1))
                errorProvider1.SetError(FRUnitCmargTb1, "");
            else errorProvider1.SetError(FRUnitCmargTb1, "just real number!");
        }
        //---------------------------------------FRUnitAmargTb2_Validated------------------------------------
        private void FRUnitAmargTb2_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitAmargTb2.Text, 1))
                errorProvider1.SetError(FRUnitAmargTb2, "");
            else errorProvider1.SetError(FRUnitAmargTb2, "just real number!");
        }
        //--------------------------------------FRUnitBmargTb2_Validated--------------------------------------
        private void FRUnitBmargTb2_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitBmargTb2.Text, 1))
                errorProvider1.SetError(FRUnitBmargTb2, "");
            else errorProvider1.SetError(FRUnitBmargTb2, "just real number!");
        }
        //--------------------------------------FRUnitCmargTb2_Validated-----------------------------------------
        private void FRUnitCmargTb2_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitCmargTb2.Text, 1))
                errorProvider1.SetError(FRUnitCmargTb2, "");
            else errorProvider1.SetError(FRUnitCmargTb2, "just real number!");
        }
        //--------------------------------------FRUnitColdTb_Validated----------------------------------------------
        private void FRUnitColdTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitColdTb.Text, 1))
                errorProvider1.SetError(FRUnitColdTb, "");
            else errorProvider1.SetError(FRUnitColdTb, "just real number!");
        }
        //-------------------------------------FRUnitHotTb_Validated--------------------------------------------
        private void FRUnitHotTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitHotTb.Text, 1))
                errorProvider1.SetError(FRUnitHotTb, "");
            else errorProvider1.SetError(FRUnitHotTb, "just real number!");
        }

        //-----------------------------------------Grid400_DataError---------------------------------------
        private void Grid400_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            if ((!Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 1))
            {
                try
                {
                    int i = int.Parse(Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
            if ((!Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 2))
            {
                try
                {
                    int i = int.Parse(Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
            if ((!Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 3))
            {
                try
                {
                    double i = double.Parse(Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
            if ((!Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 6))
            {
                try
                {
                    double i = double.Parse(Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
        }
        //-------------------------------------------Grid230_DataError-----------------------------------------
        private void Grid230_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            if ((!Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 1))
            {
                try
                {
                    int i = int.Parse(Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
            if ((!Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 2))
            {
                try
                {
                    int i = int.Parse(Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
            if ((!Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 3))
            {
                try
                {
                    double i = double.Parse(Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Real Number!");
                }
            }
            if ((!Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 6))
            {
                try
                {
                    double i = double.Parse(Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Real Number!");
                }
            }
        }
        //-------------------------------------MRPlotBtn_Click------------------------------
        private void MRPlotBtn_Click(object sender, EventArgs e)
        {
            //if (MRHeaderPanel.Visible)
            //    DrawUnitnemoodar
            //else DrawPlanNemoodar
        }

        //-----------------------------------m002ToolStripMenuItem_Click-------------------------------------
        private void m002ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool IsValid = true;
            DialogResult re = openFileDialog1.ShowDialog();
            if (re != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName;
                //read from FRM002.xls into datagridview
                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                OleDbConnection objConn = new OleDbConnection(sConnectionString);
                objConn.Open();
                string price = "Sheet1";
                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                objAdapter1.SelectCommand = objCmdSelect;
                DataSet objDataset1 = new DataSet();
                try
                {
                    objAdapter1.Fill(objDataset1);
                    TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                }
                catch (Exception ex)
                {
                    IsValid = false;
                    throw ex;
                }
                objConn.Close();
                //IS IT A Valid File?
                if ((IsValid) && (TempGV.Columns[1].HeaderText.Contains("M002")))
                {
                    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                    myConnection.Open();

                    //Insert into DB (MainFRM002)
                    //string path =@"c:\data\" + Doc002 + ".xls";
                    Excel.Application exobj = new Excel.Application();
                    exobj.Visible = true;
                    exobj.UserControl = true;
                    Excel.Workbook book = null;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Connection = myConnection;
                    int type = 0;
                    string PID = "0";
                    string date1 = findDate(path);
                    string date2 = date1.Remove(4);
                    date2 += "/";
                    date2 += date1[4];
                    date2 += date1[5];
                    date2 += "/";
                    date2 += date1[6];
                    date2 += date1[7];


                    //foreach (Excel.Worksheet workSheet in book.Worksheets)
                    //    if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                    //    {
                    //        if ((((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString().Contains("سيكل")) || (((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString().Contains("ccp")))
                    //            type = 1;
                    //        string date1 = ((Excel.Range)workSheet.Cells[4, 2]).Value2.ToString();
                    //        date2 = date1.Remove(4);
                    //        date2 += "/";
                    //        date2 += date1[4];
                    //        date2 += date1[5];
                    //        date2 += "/";
                    //        date2 += date1[6];
                    //        date2 += date1[7];
                    //    }

                    MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                    MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
                    MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
                    MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

                    MyCom.CommandText = "INSERT INTO [MainFRM002] (PPID,TargetMarketDate,PPName,PPType,"
                    + "DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                        {
                            PID = findPPID(path, "m002");
                            if (!CheckPPID) type = 1; else type = 0;

                            MyCom.Parameters["@id"].Value = PID;
                            MyCom.Parameters["@tdate"].Value = date2;
                            MyCom.Parameters["@name"].Value = ((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString().Trim();
                            MyCom.Parameters["@type"].Value = type;
                            if (((Excel.Range)workSheet.Cells[2, 2]).Value2 != null)
                                MyCom.Parameters["@idate"].Value = ((Excel.Range)workSheet.Cells[2, 2]).Value2.ToString().Trim();
                            else MyCom.Parameters["@idate"].Value = null;
                            if (((Excel.Range)workSheet.Cells[3, 2]).Value2 != null)
                                MyCom.Parameters["@time"].Value = ((Excel.Range)workSheet.Cells[3, 2]).Value2.ToString().Trim();
                            else MyCom.Parameters["@time"].Value = null;
                            if (((Excel.Range)workSheet.Cells[7, 2]).Value2 != null)
                                MyCom.Parameters["@revision"].Value = ((Excel.Range)workSheet.Cells[7, 2]).Value2.ToString().Trim();
                            else MyCom.Parameters["@revision"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[8, 2]).Value2 != null)
                                MyCom.Parameters["@filled"].Value = ((Excel.Range)workSheet.Cells[8, 2]).Value2.ToString().Trim();
                            else MyCom.Parameters["@filled"].Value = null;
                            if (((Excel.Range)workSheet.Cells[9, 2]).Value2 != null)
                                MyCom.Parameters["@approved"].Value = ((Excel.Range)workSheet.Cells[9, 2]).Value2.ToString().Trim();
                            else MyCom.Parameters["@approved"].Value = null;
                        }
                    try
                    {
                        /////// delete previous records, if any:

                        SqlCommand MyComDeleteMain = new SqlCommand();
                        MyComDeleteMain.Connection = myConnection;
                        MyComDeleteMain.CommandText = "Delete From [MainFRM002] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                        MyComDeleteMain.Parameters.Add("@id", SqlDbType.NChar, 10);
                        MyComDeleteMain.Parameters.Add("@tdate", SqlDbType.Char, 10);
                        MyComDeleteMain.Parameters.Add("@type", SqlDbType.SmallInt);
                        MyComDeleteMain.Parameters["@id"].Value = PID;
                        MyComDeleteMain.Parameters["@tdate"].Value = date2;
                        MyComDeleteMain.Parameters["@type"].Value = type;
                        MyComDeleteMain.ExecuteNonQuery();
                        MyComDeleteMain.Dispose();
                        ///////////////////////////

                        /////// delete previous records, if any:

                        SqlCommand MyComDeleteBlock = new SqlCommand();
                        MyComDeleteBlock.Connection = myConnection;
                        MyComDeleteBlock.CommandText = "Delete From [BlockFRM002] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                        MyComDeleteBlock.Parameters.Add("@id", SqlDbType.NChar, 10);
                        MyComDeleteBlock.Parameters.Add("@tdate", SqlDbType.Char, 10);
                        MyComDeleteBlock.Parameters.Add("@type", SqlDbType.SmallInt);
                        MyComDeleteBlock.Parameters["@id"].Value = PID;
                        MyComDeleteBlock.Parameters["@tdate"].Value = date2;
                        MyComDeleteBlock.Parameters["@type"].Value = type;
                        MyComDeleteBlock.ExecuteNonQuery();
                        MyComDeleteBlock.Dispose();
                        ///////////////////////////

                        SqlCommand MyComDelete = new SqlCommand();
                        MyComDelete.Connection = myConnection;

                        MyComDelete.CommandText = "delete from DetailFRM002 where TargetMarketDate=@deleteDate" +
                                " AND PPID=@ppID AND PPType=@ppType AND Estimated<>1";
                        MyComDelete.Parameters.Add("@deleteDate", SqlDbType.Char, 10);
                        MyComDelete.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                        MyComDelete.Parameters.Add("@ppType", SqlDbType.SmallInt);

                        MyComDelete.Parameters["@PPID"].Value = PID;
                        MyComDelete.Parameters["@deleteDate"].Value = date2;
                        MyComDelete.Parameters["@ppType"].Value = type;

                        MyComDelete.ExecuteNonQuery();
                        MyComDelete.Dispose();

                        /////// END delete

                        MyCom.ExecuteNonQuery();
                    }
                    catch (Exception exp)
                    {
                        string str = exp.Message;
                    }

                    //Insert into DB (BlockFRM002)
                    int x = 10;
                    MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@peak", SqlDbType.Real);
                    MyCom.Parameters.Add("@max", SqlDbType.Real);

                    //read directly and cell by cell
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                        {
                            while (x < (TempGV.Rows.Count - 1))
                            {
                                if (TempGV.Rows[x].Cells[0].Value.ToString() != "")
                                {
                                    MyCom.CommandText = "INSERT INTO [BlockFRM002] (PPID,TargetMarketDate,PPType,Block,"
                                    + "Peak,MaxDailyGeneration) VALUES (@id,@tdate,@type,@block,@peak,@max)";

                                    MyCom.Parameters["@id"].Value = PID;
                                    MyCom.Parameters["@tdate"].Value = date2;
                                    MyCom.Parameters["@type"].Value = type;
                                    MyCom.Parameters["@block"].Value = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString().Trim();
                                    if (((Excel.Range)workSheet.Cells[x + 2, 2]).Value2 != null)
                                        MyCom.Parameters["@peak"].Value = ((Excel.Range)workSheet.Cells[x + 2, 2]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@peak"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2, 3]).Value2 != null)
                                        MyCom.Parameters["@max"].Value = ((Excel.Range)workSheet.Cells[x + 2, 3]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@max"].Value = 0;
                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        string str = exp.Message;
                                    }
                                }
                                x++;
                            }
                        }
                    //Insert into DB (DetailFRM002)
                    x = 10;
                    MyCom.Parameters.Add("@deccap", SqlDbType.Real);
                    MyCom.Parameters.Add("@dispachcap", SqlDbType.Real);
                    MyCom.Parameters.Add("@price1", SqlDbType.Real);
                    MyCom.Parameters.Add("@power1", SqlDbType.Real);
                    MyCom.Parameters.Add("@price2", SqlDbType.Real);
                    MyCom.Parameters.Add("@power2", SqlDbType.Real);
                    MyCom.Parameters.Add("@price3", SqlDbType.Real);
                    MyCom.Parameters.Add("@power3", SqlDbType.Real);
                    MyCom.Parameters.Add("@price4", SqlDbType.Real);
                    MyCom.Parameters.Add("@power4", SqlDbType.Real);
                    MyCom.Parameters.Add("@price5", SqlDbType.Real);
                    MyCom.Parameters.Add("@power5", SqlDbType.Real);
                    MyCom.Parameters.Add("@price6", SqlDbType.Real);
                    MyCom.Parameters.Add("@power6", SqlDbType.Real);
                    MyCom.Parameters.Add("@price7", SqlDbType.Real);
                    MyCom.Parameters.Add("@power7", SqlDbType.Real);
                    MyCom.Parameters.Add("@price8", SqlDbType.Real);
                    MyCom.Parameters.Add("@power8", SqlDbType.Real);
                    MyCom.Parameters.Add("@price9", SqlDbType.Real);
                    MyCom.Parameters.Add("@power9", SqlDbType.Real);
                    MyCom.Parameters.Add("@price10", SqlDbType.Real);
                    MyCom.Parameters.Add("@power10", SqlDbType.Real);
                    MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);

                    //read directly and cell by cell
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                        {
                            while (x < (TempGV.Rows.Count - 2))
                            {
                                if (TempGV.Rows[x].Cells[0].Value.ToString() != "")
                                {
                                    for (int j = 0; j < 24; j++)
                                    {
                                        MyCom.CommandText = "INSERT INTO [DetailFRM002] (TargetMarketDate,PPID,Block,PPType,Hour" +
                                        ",DeclaredCapacity,DispachableCapacity,Power1,Price1,Power2,Price2,Power3,Price3,Power4," +
                                        "Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10," +
                                        "Price10) VALUES (@tdate,@id,@block,@type,@hour,@deccap,@dispachcap,@power1," +
                                        "@price1,@power2,@price2,@power3,@price3,@power4,@price4,@power5,@price5,@power6,@price6," +
                                        "@power7,@price7,@power8,@price8,@power9,@price9,@power10,@price10)";


                                        MyCom.Parameters["@id"].Value = PID;
                                        MyCom.Parameters["@tdate"].Value = date2;
                                        MyCom.Parameters["@block"].Value = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString().Trim();
                                        MyCom.Parameters["@type"].Value = type;
                                        MyCom.Parameters["@hour"].Value = j + 1;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 5]).Value2 != null)
                                            MyCom.Parameters["@deccap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 5]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@deccap"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2 != null)
                                            MyCom.Parameters["@dispachcap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@dispachcap"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2 != null)
                                            MyCom.Parameters["@power1"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@power1"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 8]).Value2 != null)
                                            MyCom.Parameters["@price1"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 8]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@price1"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 9]).Value2 != null)
                                            MyCom.Parameters["@power2"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 9]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@power2"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 10]).Value2 != null)
                                            MyCom.Parameters["@price2"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 10]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@price2"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 11]).Value2 != null)
                                            MyCom.Parameters["@power3"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 11]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@power3"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 12]).Value2 != null)
                                            MyCom.Parameters["@price3"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 12]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@price3"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 13]).Value2 != null)
                                            MyCom.Parameters["@power4"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 13]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@power4"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 14]).Value2 != null)
                                            MyCom.Parameters["@price4"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 14]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@price4"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 15]).Value2 != null)
                                            MyCom.Parameters["@power5"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 15]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@power5"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 16]).Value2 != null)
                                            MyCom.Parameters["@price5"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 16]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@price5"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 17]).Value2 != null)
                                            MyCom.Parameters["@power6"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 17]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@power6"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 18]).Value2 != null)
                                            MyCom.Parameters["@price6"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 18]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@price6"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 19]).Value2 != null)
                                            MyCom.Parameters["@power7"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 19]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@power7"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 20]).Value2 != null)
                                            MyCom.Parameters["@price7"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 20]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@price7"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 21]).Value2 != null)
                                            MyCom.Parameters["@power8"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 21]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@power8"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 22]).Value2 != null)
                                            MyCom.Parameters["@price8"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 22]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@price8"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 23]).Value2 != null)
                                            MyCom.Parameters["@power9"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 23]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@power9"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 24]).Value2 != null)
                                            MyCom.Parameters["@price9"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 24]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@price9"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 25]).Value2 != null)
                                            MyCom.Parameters["@power10"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 25]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@power10"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 26]).Value2 != null)
                                            MyCom.Parameters["@price10"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 26]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@price10"].Value = 0;
                                        try
                                        {
                                            MyCom.ExecuteNonQuery();
                                        }
                                        catch (Exception exp)
                                        {
                                            string str = exp.Message;
                                        }
                                    }
                                }
                                x++;
                            }
                        }
                    myConnection.Close();

                    book.Close(false, book, Type.Missing);
                    exobj.Workbooks.Close();
                    exobj.Quit();
                    System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                }
                else MessageBox.Show("Selected File Is not Valid!");
            }
        }
        //----------------------------------m005ToolStripMenuItem_Click---------------------------------------
        private void m005ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult re = openFileDialog1.ShowDialog();
            if (re != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName;
                //Is It a Valid File?
                if (path.Contains("005"))
                {
                    //read from FRM005.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "FRM005";
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                    objConn.Close();

                    //Insert into DB (MainFRM005)
                    Excel.Application exobj = new Excel.Application();
                    exobj.Visible = true;
                    exobj.UserControl = true;
                    Excel.Workbook book = null;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                    SqlCommand MyCom = new SqlCommand();
                    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                    myConnection.Open();

                    MyCom.Connection = myConnection;
                    int type = 0;
                    string PID = "0";
                    string date1 = findDate(path);
                    string date = date1.Remove(4);
                    date += "/";
                    date += date1[4];
                    date += date1[5];
                    date += "/";
                    date += date1[6];
                    date += date1[7];

                    //if ((TempGV.Rows[3].Cells[1].Value.ToString().Contains("سيكل")) || (TempGV.Rows[3].Cells[1].Value.ToString().Contains("ccp")))
                    //    type = 1;
                    MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                    MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
                    MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
                    MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

                    MyCom.CommandText = "INSERT INTO [MainFRM005] (PPID,TargetMarketDate,PPName,PPType,"
                    + "DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
                    PID = findPPID(path,"m005");
                    if (!CheckPPID) type = 1; else type = 0;

                    MyCom.Parameters["@id"].Value = PID;
                    MyCom.Parameters["@tdate"].Value = date;
                    MyCom.Parameters["@name"].Value = TempGV.Rows[3].Cells[1].Value.ToString().Trim();
                    MyCom.Parameters["@type"].Value = type;
                    MyCom.Parameters["@idate"].Value = TempGV.Rows[0].Cells[1].Value.ToString().Trim();
                    MyCom.Parameters["@time"].Value = TempGV.Rows[1].Cells[1].Value.ToString().Trim();
                    MyCom.Parameters["@revision"].Value = TempGV.Rows[4].Cells[1].Value.ToString().Trim();
                    MyCom.Parameters["@filled"].Value = TempGV.Rows[5].Cells[1].Value.ToString().Trim();
                    MyCom.Parameters["@approved"].Value = TempGV.Rows[6].Cells[1].Value.ToString().Trim();

                    try
                    {
                        /////// delete previous records, if any:

                        SqlCommand MyComDeleteMain = new SqlCommand();
                        MyComDeleteMain.Connection = myConnection;
                        MyComDeleteMain.CommandText = "Delete From [MainFRM005] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                        MyComDeleteMain.Parameters.Add("@id", SqlDbType.NChar, 10);
                        MyComDeleteMain.Parameters.Add("@tdate", SqlDbType.Char, 10);
                        MyComDeleteMain.Parameters.Add("@type", SqlDbType.SmallInt);
                        MyComDeleteMain.Parameters["@id"].Value = PID;
                        MyComDeleteMain.Parameters["@tdate"].Value = date;
                        MyComDeleteMain.Parameters["@type"].Value = type;
                        MyComDeleteMain.ExecuteNonQuery();
                        MyComDeleteMain.Dispose();
                        ///////////////////////////

                        /////// delete previous records, if any:

                        SqlCommand MyComDeleteBlock = new SqlCommand();
                        MyComDeleteBlock.Connection = myConnection;
                        MyComDeleteBlock.CommandText = "Delete From [BlockFRM005] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                        MyComDeleteBlock.Parameters.Add("@id", SqlDbType.NChar, 10);
                        MyComDeleteBlock.Parameters.Add("@tdate", SqlDbType.Char, 10);
                        MyComDeleteBlock.Parameters.Add("@type", SqlDbType.SmallInt);
                        MyComDeleteBlock.Parameters["@id"].Value = PID;
                        MyComDeleteBlock.Parameters["@tdate"].Value = date;
                        MyComDeleteBlock.Parameters["@type"].Value = type;
                        MyComDeleteBlock.ExecuteNonQuery();
                        MyComDeleteBlock.Dispose();
                        ///////////////////////////

                        SqlCommand MyComDelete = new SqlCommand();
                        MyComDelete.Connection = myConnection;

                        MyComDelete.CommandText = "delete from DetailFRM005 where TargetMarketDate=@deleteDate" +
                                " AND PPID=@ppID AND PPType=@ppType";
                        MyComDelete.Parameters.Add("@deleteDate", SqlDbType.Char, 10);
                        MyComDelete.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                        MyComDelete.Parameters.Add("@ppType", SqlDbType.SmallInt);

                        MyComDelete.Parameters["@ppID"].Value = PID;
                        MyComDelete.Parameters["@deleteDate"].Value = date;
                        MyComDelete.Parameters["@ppType"].Value = type;

                        MyComDelete.ExecuteNonQuery();
                        MyComDelete.Dispose();

                        /////// END delete

                        MyCom.ExecuteNonQuery();
                    }
                    catch (Exception exp)
                    {
                        string str = exp.Message;
                    }

                    //Insert into DB (BlockFRM005)
                    int x = 10;
                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@prequired", SqlDbType.Real);
                    MyCom.Parameters.Add("@pdispach", SqlDbType.Real);
                    MyCom.Parameters.Add("@drequierd", SqlDbType.Real);
                    MyCom.Parameters.Add("@ddispach", SqlDbType.Real);

                    //read directly and cell by cell
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if (workSheet.Name == "FRM005")
                        {
                            while (x < (TempGV.Rows.Count - 1))
                            {
                                if (TempGV.Rows[x].Cells[0].Value.ToString() != "")
                                {
                                    MyCom.CommandText = "INSERT INTO [BlockFRM005] (PPID,TargetMarketDate,PPType,Block,"
                                    + "PeakRequired,PeakDispatchable,DailyTotalRequired,DailyTotalDispachable)"
                                    + "VALUES (@id,@tdate,@type,@num,@prequired,@pdispach,@drequierd,@ddispach)";

                                    MyCom.Parameters["@id"].Value = PID;
                                    MyCom.Parameters["@tdate"].Value = date;
                                    MyCom.Parameters["@num"].Value = TempGV.Rows[x].Cells[0].Value.ToString().Trim();
                                    MyCom.Parameters["@type"].Value = type;
                                    if (((Excel.Range)workSheet.Cells[x + 2, 27]).Value2 != null)
                                        MyCom.Parameters["@prequired"].Value = ((Excel.Range)workSheet.Cells[x + 2, 27]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@prequired"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 3, 27]).Value2 != null)
                                        MyCom.Parameters["@pdispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, 27]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@pdispach"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2, 28]).Value2 != null)
                                        MyCom.Parameters["@drequierd"].Value = ((Excel.Range)workSheet.Cells[x + 2, 28]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@drequierd"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 3, 28]).Value2 != null)
                                        MyCom.Parameters["@ddispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, 28]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@ddispach"].Value = 0;
                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        string str = exp.Message;
                                    }
                                }
                                x++;
                            }
                        }
                    //Insert into DB (DetailFRM005)
                    x = 10;
                    MyCom.Parameters.Add("@required", SqlDbType.Real);
                    MyCom.Parameters.Add("@dispach", SqlDbType.Real);
                    MyCom.Parameters.Add("@contribution", SqlDbType.Char, 2);
                    MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);

                    //read directly and cell by cell
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if (workSheet.Name == "FRM005")
                        {
                            while (x < (TempGV.Rows.Count - 2))
                            {
                                if (TempGV.Rows[x].Cells[0].Value.ToString() != "")
                                {
                                    for (int j = 0; j < 24; j++)
                                    {
                                        MyCom.CommandText = "INSERT INTO [DetailFRM005] (TargetMarketDate,PPID,Block,PPType,Hour,Required,Dispatchable,Contribution) VALUES (@tdate,@id,@num,@type,@hour,@required,@dispach,@contribution)";

                                        MyCom.Parameters["@id"].Value = PID;
                                        MyCom.Parameters["@tdate"].Value = date;
                                        MyCom.Parameters["@num"].Value = TempGV.Rows[x].Cells[0].Value.ToString().Trim();
                                        MyCom.Parameters["@type"].Value = type;

                                        MyCom.Parameters["@hour"].Value = j + 1;
                                        if (((Excel.Range)workSheet.Cells[x + 2, j + 3]).Value2 != null)
                                            MyCom.Parameters["@required"].Value = ((Excel.Range)workSheet.Cells[x + 2, j + 3]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@required"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 3, j + 3]).Value2 != null)
                                            MyCom.Parameters["@dispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, j + 3]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@dispach"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2 != null)
                                            MyCom.Parameters["@contribution"].Value = ((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@contribution"].Value = null;
                                        try
                                        {
                                            MyCom.ExecuteNonQuery();
                                        }
                                        catch (Exception exp)
                                        {
                                            string str = exp.Message;
                                        }
                                    }
                                }
                                x++;
                            }
                        }
                    myConnection.Close();
                    book.Close(false, book, Type.Missing);
                    exobj.Workbooks.Close();
                    exobj.Quit();
                    System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                }
                else MessageBox.Show("Selected File Isnot Valid!");
            }
        }
        //---------------------------------------averagePriceToolStripMenuItem_Click-------------------------------------
        private void averagePriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool check = true;
            DialogResult re = openFileDialog1.ShowDialog();
            if (re != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName;
                //Is It a Valid File?
                if (path.Contains("nprice"))
                {
                    //Save AS AveragePrice.xls file
                    //string path = @"c:\data\AveragePrice.xls";
                    Excel.Application exobj1 = new Excel.Application();
                    exobj1.Visible = true;
                    exobj1.UserControl = true;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    Excel.Workbook book1 = null;
                    book1 = exobj1.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book1.Save();
                    book1.Close(true, book1, Type.Missing);
                    exobj1.Workbooks.Close();
                    exobj1.Quit();

                    //read from AveragePrice.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "قيمت ";
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                    objConn.Close();

                    //Insert into DB (AveragePrice)
 
                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@date1", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                    MyCom.Parameters.Add("@Pmin", SqlDbType.Int);
                    MyCom.Parameters.Add("@Pmax", SqlDbType.Int);
                    MyCom.Parameters.Add("@Amin", SqlDbType.Int);
                    MyCom.Parameters.Add("@Amax", SqlDbType.Int);
                    MyCom.Parameters.Add("@Aav", SqlDbType.Int);
                    for (int i = 0; i < 24; i++)
                    {
                        //has Selected file saved before?
                        if (check)
                        {
                            MyCom.CommandText = "INSERT INTO [AveragePrice] (Date,Hour,ProposedMin,ProposedMax,AcceptedMin,AcceptedMax,AcceptedAverage) VALUES (@date1,@hour,@Pmin,@Pmax,@Amin,@Amax,@Aav)";
                            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                            myConnection.Open();

                            MyCom.Connection = myConnection;
                            MyCom.Parameters["@date1"].Value = TempGV.Columns[0].HeaderText.ToString().Trim();
                            MyCom.Parameters["@hour"].Value = TempGV.Rows[i + 3].Cells[0].Value.ToString().Trim();
                            MyCom.Parameters["@Pmin"].Value = TempGV.Rows[i + 3].Cells[1].Value.ToString().Trim();
                            MyCom.Parameters["@Pmax"].Value = TempGV.Rows[i + 3].Cells[2].Value.ToString().Trim();
                            MyCom.Parameters["@Amin"].Value = TempGV.Rows[i + 3].Cells[3].Value.ToString().Trim();
                            MyCom.Parameters["@Amax"].Value = TempGV.Rows[i + 3].Cells[4].Value.ToString().Trim();
                            MyCom.Parameters["@Aav"].Value = TempGV.Rows[i + 3].Cells[5].Value.ToString().Trim();
                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                                if (str.Contains("PRIMARY KEY"))
                                {
                                    MessageBox.Show("Selected File Had been saved Before!");
                                    check = false;
                                }
                            }
                            finally
                            {
                                myConnection.Close();
                            }
                        }
                    }
                    if (check) MessageBox.Show("Selected File Has been saved!");
                    System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                }
                else MessageBox.Show("Selected File Isnot Valid!");
            }
        }
        //-------------------------------loadForecastingToolStripMenuItem_Click------------------------------------
        private void loadForecastingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult re = openFileDialog1.ShowDialog();
            if (re != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName;
                //Is It a Valid File?
                if (path.Contains("load"))
                {
                    //Save AS LoadForecasting.xls file
                    //string path1 = @"c:\data\LoadForecasting.xls";
                    Excel.Application exobj = new Excel.Application();
                    exobj.Visible = true;
                    exobj.UserControl = true;
                    Excel.Workbook book = null;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book.Save();
                    book.Close(true, book, Type.Missing);

                    //read from LoadForecasting.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "Lfoc";
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                    objConn.Close();

                    //read from LoadForecasting.xls into strings
                    string edate = "";
                    string[] date = new string[4];
                    book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if (workSheet.Name == "Lfoc")
                        {
                            edate = ((Excel.Range)workSheet.Cells[3, 4]).Value2.ToString().Trim();
                            date[0] = ((Excel.Range)workSheet.Cells[4, 3]).Value2.ToString().Trim();
                            date[1] = ((Excel.Range)workSheet.Cells[4, 4]).Value2.ToString().Trim();
                            date[2] = ((Excel.Range)workSheet.Cells[4, 5]).Value2.ToString().Trim();
                            date[3] = ((Excel.Range)workSheet.Cells[4, 6]).Value2.ToString().Trim();
                        }
                    book.Close(false, book, Type.Missing);
                    exobj.Workbooks.Close();
                    exobj.Quit();

                    //Insert into DB (LoadForecasting)

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@date11", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@edate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@peak", SqlDbType.Real);
                    MyCom.Parameters.Add("@h1", SqlDbType.Real);
                    MyCom.Parameters.Add("@h2", SqlDbType.Real);
                    MyCom.Parameters.Add("@h3", SqlDbType.Real);
                    MyCom.Parameters.Add("@h4", SqlDbType.Real);
                    MyCom.Parameters.Add("@h5", SqlDbType.Real);
                    MyCom.Parameters.Add("@h6", SqlDbType.Real);
                    MyCom.Parameters.Add("@h7", SqlDbType.Real);
                    MyCom.Parameters.Add("@h8", SqlDbType.Real);
                    MyCom.Parameters.Add("@h9", SqlDbType.Real);
                    MyCom.Parameters.Add("@h10", SqlDbType.Real);
                    MyCom.Parameters.Add("@h11", SqlDbType.Real);
                    MyCom.Parameters.Add("@h12", SqlDbType.Real);
                    MyCom.Parameters.Add("@h13", SqlDbType.Real);
                    MyCom.Parameters.Add("@h14", SqlDbType.Real);
                    MyCom.Parameters.Add("@h15", SqlDbType.Real);
                    MyCom.Parameters.Add("@h16", SqlDbType.Real);
                    MyCom.Parameters.Add("@h17", SqlDbType.Real);
                    MyCom.Parameters.Add("@h18", SqlDbType.Real);
                    MyCom.Parameters.Add("@h19", SqlDbType.Real);
                    MyCom.Parameters.Add("@h20", SqlDbType.Real);
                    MyCom.Parameters.Add("@h21", SqlDbType.Real);
                    MyCom.Parameters.Add("@h22", SqlDbType.Real);
                    MyCom.Parameters.Add("@h23", SqlDbType.Real);
                    MyCom.Parameters.Add("@h24", SqlDbType.Real);

                    for (int z = 0; z < 4; z++)
                    {
                        MyCom.CommandText = "INSERT INTO [LoadForecasting] (Date,DateEstimate,Peak,Hour1,Hour2,Hour3,Hour4," +
                        "Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19" +
                        ",Hour20,Hour21,Hour22,Hour23,Hour24) VALUES (@date11,@edate,@peak,@h1,@h2,@h3,@h4,@h5,@h6,@h7,@h8" +
                        ",@h9,@h10,@h11,@h12,@h13,@h14,@h15,@h16,@h17,@h18,@h19,@h20,@h21,@h22,@h23,@h24)";
                        SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                        myConnection.Open();

                        MyCom.Connection = myConnection;
                        MyCom.Parameters["@date11"].Value = date[z];
                        MyCom.Parameters["@edate"].Value = edate;
                        MyCom.Parameters["@peak"].Value = TempGV.Rows[28].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h1"].Value = TempGV.Rows[4].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h2"].Value = TempGV.Rows[5].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h3"].Value = TempGV.Rows[6].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h4"].Value = TempGV.Rows[7].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h5"].Value = TempGV.Rows[8].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h6"].Value = TempGV.Rows[9].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h7"].Value = TempGV.Rows[10].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h8"].Value = TempGV.Rows[11].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h9"].Value = TempGV.Rows[12].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h10"].Value = TempGV.Rows[13].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h11"].Value = TempGV.Rows[14].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h12"].Value = TempGV.Rows[15].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h13"].Value = TempGV.Rows[16].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h14"].Value = TempGV.Rows[17].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h15"].Value = TempGV.Rows[18].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h16"].Value = TempGV.Rows[19].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h17"].Value = TempGV.Rows[20].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h18"].Value = TempGV.Rows[21].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h19"].Value = TempGV.Rows[22].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h20"].Value = TempGV.Rows[23].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h21"].Value = TempGV.Rows[24].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h22"].Value = TempGV.Rows[25].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h23"].Value = TempGV.Rows[26].Cells[2 + z].Value.ToString().Trim();
                        MyCom.Parameters["@h24"].Value = TempGV.Rows[27].Cells[2 + z].Value.ToString().Trim();

                        try
                        {
                            MyCom.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                        }
                        finally
                        {
                            myConnection.Close();
                        }
                    }
                    MessageBox.Show("Selected File Has been Saved!");
                    System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                }
                else MessageBox.Show("Selected File Isnot Valid!");
            }
        }
        //---------------------------toolStripMenuItem2_Click------------------------------------------------
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            BaseDataForm baseData = new BaseDataForm();
            baseData.ShowDialog();
        }
        //-----------------------------------annualFactorToolStripMenuItem_Click---------------------------------------
        private void annualFactorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool check = true;
            DialogResult re = openFileDialog1.ShowDialog();
            if (re != DialogResult.Cancel)
            {

                string path = openFileDialog1.FileName;
                //Is It a Valid File?
                if (path.Contains("HCPF"))
                {
                    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                    myConnection.Open();

                    //read from HCPF.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "Data";
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                    objConn.Close();

                    //Insert into DB (HCPF)

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@h1", SqlDbType.Real);
                    MyCom.Parameters.Add("@h2", SqlDbType.Real);
                    MyCom.Parameters.Add("@h3", SqlDbType.Real);
                    MyCom.Parameters.Add("@h4", SqlDbType.Real);
                    MyCom.Parameters.Add("@h5", SqlDbType.Real);
                    MyCom.Parameters.Add("@h6", SqlDbType.Real);
                    MyCom.Parameters.Add("@h7", SqlDbType.Real);
                    MyCom.Parameters.Add("@h8", SqlDbType.Real);
                    MyCom.Parameters.Add("@h9", SqlDbType.Real);
                    MyCom.Parameters.Add("@h10", SqlDbType.Real);
                    MyCom.Parameters.Add("@h11", SqlDbType.Real);
                    MyCom.Parameters.Add("@h12", SqlDbType.Real);
                    MyCom.Parameters.Add("@h13", SqlDbType.Real);
                    MyCom.Parameters.Add("@h14", SqlDbType.Real);
                    MyCom.Parameters.Add("@h15", SqlDbType.Real);
                    MyCom.Parameters.Add("@h16", SqlDbType.Real);
                    MyCom.Parameters.Add("@h17", SqlDbType.Real);
                    MyCom.Parameters.Add("@h18", SqlDbType.Real);
                    MyCom.Parameters.Add("@h19", SqlDbType.Real);
                    MyCom.Parameters.Add("@h20", SqlDbType.Real);
                    MyCom.Parameters.Add("@h21", SqlDbType.Real);
                    MyCom.Parameters.Add("@h22", SqlDbType.Real);
                    MyCom.Parameters.Add("@h23", SqlDbType.Real);
                    MyCom.Parameters.Add("@h24", SqlDbType.Real);
                    MyCom.Connection = myConnection;
                    int i = 0;
                    while (i < TempGV.RowCount)
                    {
                        //has Selected file saved before?
                        if ((check) && (TempGV.Rows[i].Cells[0].Value != null) && (TempGV.Rows[i].Cells[0].Value.ToString() != ""))
                        {
                            MyCom.CommandText = "INSERT INTO [HCPF] (Date,H1,H2,H3,H4,H5,H6,H7,H8,H9,H10,H11,H12," +
                            "H13,H14,H15,H16,H17,H18,H19,H20,H21,H22,H23,H24) VALUES (@date,@h1,@h2,@h3,@h4,@h5,@h6" +
                            ",@h7,@h8,@h9,@h10,@h11,@h12,@h13,@h14,@h15,@h16,@h17,@h18,@h19,@h20,@h21,@h22,@h23,@h24)";

                            //Translate Date in the Right Formet 1111/11/11
                            string date1 = TempGV.Rows[i].Cells[0].Value.ToString().Trim();
                            string date2 = date1.Remove(4);
                            date2 += "/";
                            date2 += date1[4];
                            date2 += date1[5];
                            date2 += "/";
                            date2 += date1[6];
                            date2 += date1[7];
                            MyCom.Parameters["@date"].Value = date2;
                            MyCom.Parameters["@h1"].Value = TempGV.Rows[i].Cells[1].Value.ToString().Trim();
                            MyCom.Parameters["@h2"].Value = TempGV.Rows[i].Cells[2].Value.ToString().Trim();
                            MyCom.Parameters["@h3"].Value = TempGV.Rows[i].Cells[3].Value.ToString().Trim();
                            MyCom.Parameters["@h4"].Value = TempGV.Rows[i].Cells[4].Value.ToString().Trim();
                            MyCom.Parameters["@h5"].Value = TempGV.Rows[i].Cells[5].Value.ToString().Trim();
                            MyCom.Parameters["@h6"].Value = TempGV.Rows[i].Cells[6].Value.ToString().Trim();
                            MyCom.Parameters["@h7"].Value = TempGV.Rows[i].Cells[7].Value.ToString().Trim();
                            MyCom.Parameters["@h8"].Value = TempGV.Rows[i].Cells[8].Value.ToString().Trim();
                            MyCom.Parameters["@h9"].Value = TempGV.Rows[i].Cells[9].Value.ToString().Trim();
                            MyCom.Parameters["@h10"].Value = TempGV.Rows[i].Cells[10].Value.ToString().Trim();
                            MyCom.Parameters["@h11"].Value = TempGV.Rows[i].Cells[11].Value.ToString().Trim();
                            MyCom.Parameters["@h12"].Value = TempGV.Rows[i].Cells[12].Value.ToString().Trim();
                            MyCom.Parameters["@h13"].Value = TempGV.Rows[i].Cells[13].Value.ToString().Trim();
                            MyCom.Parameters["@h14"].Value = TempGV.Rows[i].Cells[14].Value.ToString().Trim();
                            MyCom.Parameters["@h15"].Value = TempGV.Rows[i].Cells[15].Value.ToString().Trim();
                            MyCom.Parameters["@h16"].Value = TempGV.Rows[i].Cells[16].Value.ToString().Trim();
                            MyCom.Parameters["@h17"].Value = TempGV.Rows[i].Cells[17].Value.ToString().Trim();
                            MyCom.Parameters["@h18"].Value = TempGV.Rows[i].Cells[18].Value.ToString().Trim();
                            MyCom.Parameters["@h19"].Value = TempGV.Rows[i].Cells[19].Value.ToString().Trim();
                            MyCom.Parameters["@h20"].Value = TempGV.Rows[i].Cells[20].Value.ToString().Trim();
                            MyCom.Parameters["@h21"].Value = TempGV.Rows[i].Cells[21].Value.ToString().Trim();
                            MyCom.Parameters["@h22"].Value = TempGV.Rows[i].Cells[22].Value.ToString().Trim();
                            MyCom.Parameters["@h23"].Value = TempGV.Rows[i].Cells[23].Value.ToString().Trim();
                            MyCom.Parameters["@h24"].Value = TempGV.Rows[i].Cells[24].Value.ToString().Trim();

                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                                if (str.Contains("PRIMARY KEY"))
                                {
                                    //MessageBox.Show("Selected File Had been saved Before!");
                                    check = false;
                                }
                            }
                        }
                        //Selected file has been saved before?
                        if ((!check) && (TempGV.Rows[i].Cells[0].Value != null) && (TempGV.Rows[i].Cells[0].Value.ToString() != ""))
                        {
                            MyCom.CommandText = "UPDATE [HCPF] SET H1=@h1,H2=@h2,H3=@h3,H4=@h4,H5=@h5,H6=@h6," +
                            "H7=@h7,H8=@h8,H9=@h9,H10=@h10,H11=@h11,H12=@h12,H13=@h13,H14=@h14,H15=@h15,H16=@h16," +
                            "H17=@h17,H18=@h18,H19=@h19,H20=@h20,H21=@h21,H22=@h22,H23=@h23,H24=@h24 WHERE Date=@date";

                            //Translate Date in the Right Formet 1111/11/11
                            string date1 = TempGV.Rows[i].Cells[0].Value.ToString().Trim();
                            string date2 = date1.Remove(4);
                            date2 += "/";
                            date2 += date1[4];
                            date2 += date1[5];
                            date2 += "/";
                            date2 += date1[6];
                            date2 += date1[7];
                            MyCom.Parameters["@date"].Value = date2;
                            MyCom.Parameters["@h1"].Value = TempGV.Rows[i].Cells[1].Value.ToString().Trim();
                            MyCom.Parameters["@h2"].Value = TempGV.Rows[i].Cells[2].Value.ToString().Trim();
                            MyCom.Parameters["@h3"].Value = TempGV.Rows[i].Cells[3].Value.ToString().Trim();
                            MyCom.Parameters["@h4"].Value = TempGV.Rows[i].Cells[4].Value.ToString().Trim();
                            MyCom.Parameters["@h5"].Value = TempGV.Rows[i].Cells[5].Value.ToString().Trim();
                            MyCom.Parameters["@h6"].Value = TempGV.Rows[i].Cells[6].Value.ToString().Trim();
                            MyCom.Parameters["@h7"].Value = TempGV.Rows[i].Cells[7].Value.ToString().Trim();
                            MyCom.Parameters["@h8"].Value = TempGV.Rows[i].Cells[8].Value.ToString().Trim();
                            MyCom.Parameters["@h9"].Value = TempGV.Rows[i].Cells[9].Value.ToString().Trim();
                            MyCom.Parameters["@h10"].Value = TempGV.Rows[i].Cells[10].Value.ToString().Trim();
                            MyCom.Parameters["@h11"].Value = TempGV.Rows[i].Cells[11].Value.ToString().Trim();
                            MyCom.Parameters["@h12"].Value = TempGV.Rows[i].Cells[12].Value.ToString().Trim();
                            MyCom.Parameters["@h13"].Value = TempGV.Rows[i].Cells[13].Value.ToString().Trim();
                            MyCom.Parameters["@h14"].Value = TempGV.Rows[i].Cells[14].Value.ToString().Trim();
                            MyCom.Parameters["@h15"].Value = TempGV.Rows[i].Cells[15].Value.ToString().Trim();
                            MyCom.Parameters["@h16"].Value = TempGV.Rows[i].Cells[16].Value.ToString().Trim();
                            MyCom.Parameters["@h17"].Value = TempGV.Rows[i].Cells[17].Value.ToString().Trim();
                            MyCom.Parameters["@h18"].Value = TempGV.Rows[i].Cells[18].Value.ToString().Trim();
                            MyCom.Parameters["@h19"].Value = TempGV.Rows[i].Cells[19].Value.ToString().Trim();
                            MyCom.Parameters["@h20"].Value = TempGV.Rows[i].Cells[20].Value.ToString().Trim();
                            MyCom.Parameters["@h21"].Value = TempGV.Rows[i].Cells[21].Value.ToString().Trim();
                            MyCom.Parameters["@h22"].Value = TempGV.Rows[i].Cells[22].Value.ToString().Trim();
                            MyCom.Parameters["@h23"].Value = TempGV.Rows[i].Cells[23].Value.ToString().Trim();
                            MyCom.Parameters["@h24"].Value = TempGV.Rows[i].Cells[24].Value.ToString().Trim();

                            //try
                            //{
                            MyCom.ExecuteNonQuery();
                            //}
                            //catch (Exception exp)
                            //{
                            //    string str = exp.Message;
                            //}
                        }
                        i++;
                    }
                    myConnection.Close();
                    MessageBox.Show("Selected File Has been saved!");
                }
                else MessageBox.Show("Selected File Isnot Valid!");
            }
        }
        //----------------------------------weekFactorToolStripMenuItem-------------------------------------------
        private void weekFactorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool check = true;
            DialogResult re = openFileDialog1.ShowDialog();
            if (re != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName;
                //Is It a Valid File?
                if (path.Contains("WCPF"))
                {
                    //read from WCPF.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "Data";
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                    objConn.Close();

                    //Insert into DB (WCPF)

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@year", SqlDbType.Int);
                    MyCom.Parameters.Add("@week", SqlDbType.Int);
                    MyCom.Parameters.Add("@value", SqlDbType.Real);
                    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                    myConnection.Open();

                    MyCom.Connection = myConnection;
                    int i = 0;
                    while (i < TempGV.RowCount)
                    {
                        //has Selected file saved before?
                        if ((check) && (TempGV.Rows[i].Cells[0].Value != null) && (TempGV.Rows[i].Cells[0].Value.ToString() != ""))
                        {
                            MyCom.CommandText = "INSERT INTO [WCPF] (Year,Week,Value) VALUES (@year,@week,@value)";
                            MyCom.Parameters["@year"].Value = TempGV.Rows[i].Cells[0].Value.ToString().Trim();
                            MyCom.Parameters["@week"].Value = TempGV.Rows[i].Cells[1].Value.ToString().Trim();
                            MyCom.Parameters["@value"].Value = TempGV.Rows[i].Cells[2].Value.ToString().Trim();
                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                                if (str.Contains("PRIMARY KEY"))
                                {
                                    //MessageBox.Show("Selected File Had been saved Before!");
                                    check = false;
                                }
                            }
                        }
                        if ((!check) && (TempGV.Rows[i].Cells[0].Value != null) && (TempGV.Rows[i].Cells[0].Value.ToString() != ""))
                        {
                            MyCom.CommandText = "UPDATE [WCPF] SET Value=@value WHERE Year=@year AND Week=@week";
                            MyCom.Parameters["@year"].Value = TempGV.Rows[i].Cells[0].Value.ToString().Trim();
                            MyCom.Parameters["@week"].Value = TempGV.Rows[i].Cells[1].Value.ToString().Trim();
                            MyCom.Parameters["@value"].Value = TempGV.Rows[i].Cells[2].Value.ToString().Trim();
                            //try
                            //{
                            MyCom.ExecuteNonQuery();
                            //}
                            //catch (Exception exp)
                            //{
                            //    string str = exp.Message;
                            //}
                        }
                        i++;
                    }
                    myConnection.Close();
                    MessageBox.Show("Selected File Has been saved!");
                }
                else MessageBox.Show("Selected File Isnot Valid!");
            }
        }

        //---------------------------------------------autoPathToolStripMenuItem_Click---------------------------------------
        private void autoPathToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PathForm Path = new PathForm();
            Path.ShowDialog();

        }

        //-------------------------------FRUnitCal_ValueChanged--------------------------------
        private void FRUnitCal_ValueChanged(object sender, EventArgs e)
        {
            string unit = FRUnitLb.Text.Trim();
            string package = FRPackLb.Text.Trim();
            int index = 0;
            if (GDSteamGroup.Text.Contains(package))
                for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                {
                    if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                        index = i;
                }
            else
                for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                {
                    if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                        index = i;
                }
            FillFRUnitRevenue(unit, package, index);
        }


        private void FRUnitAmainTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitAmainTb.Text, 1))
                errorProvider1.SetError(FRUnitAmainTb, "");
            else errorProvider1.SetError(FRUnitAmainTb, "just real number!");
        }

        private void FRUnitBmainTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitBmainTb.Text, 1))
                errorProvider1.SetError(FRUnitBmainTb, "");
            else errorProvider1.SetError(FRUnitBmainTb, "just real number!");

        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainTabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            TreeNode selectedNode = null;
            if (e.GetType() == typeof(TreeNodeMouseClickEventArgs))
            {
                TreeNodeMouseClickEventArgs e2 = (TreeNodeMouseClickEventArgs)e;
                selectedNode = e2.Node;
            }
            else
                selectedNode = treeView1.SelectedNode;

            if (MainTabs.SelectedTab.Name == "BiddingStrategy" )
            {
                if (selectedNode != null)
                {
                    if (selectedNode.Text.Trim() == "Plant" || selectedNode.Text.Trim() == "Trec")
                        lblPlantValue.Text = "All";
                    else if (selectedNode.Parent != null && selectedNode.Parent.Text == "Plant")
                        lblPlantValue.Text = selectedNode.Text.Trim();

                    //LoadBiddingStrategy();
                }
            }
            else if (MainTabs.SelectedTab.Name ==  "maintenance")
            {
                if (selectedNode != null)
                {
                    if (selectedNode.Text.Trim() == "Plant" || selectedNode.Text.Trim() == "Trec")
                        lblMaintPlant.Text = "All";
                    else if (selectedNode.Parent != null && selectedNode.Parent.Text == "Plant")
                        lblMaintPlant.Text = selectedNode.Text.Trim();

                    //LoadBiddingStrategy();
                }
            }
        }

 


        public bool MultiplePackageType(int ppid)
        {
            string query = "select distinct packageType from [unitsdatamain] where ppid=" + ppid.ToString();
            if (utilities.returntbl(query).Rows.Count == 0)
                return false;
            else
                return true;
        }


        private DataSet GetUnits(int ppId)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();


            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT DISTINCT PackageType FROM UnitsDataMain WHERE PPID=@Num", myConnection);
            Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
            Myda.SelectCommand.Parameters["@Num"].Value = ppId;
            Myda.Fill(MyDS, "UnitType");

            foreach (DataRow MyRow in MyDS.Tables["UnitType"].Rows)
            {
                string Utype = MyRow["PackageType"].ToString().Trim();
                Utype = Utype.Trim();
                switch (Utype)
                {
                    case "Steam":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Steam%'",
                            myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, "Steam");

                        DataView sdv = new DataView(MyDS.Tables["Steam"]);
                        sdv.Sort = "UnitCode ASC";
                        break;
                    case "Gas":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Gas%'",
                            myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, "Gas");
                        DataView gdv = new DataView(MyDS.Tables["Gas"]);
                        gdv.Sort = "UnitCode";

                        break;
                    default:

                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM [UnitsDataMain] WHERE PPID=@Num AND PackageType LIKE 'CC%'",
                            myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, "Combined");
                        DataView cdv = new DataView(MyDS.Tables["Combined"]);
                        cdv.Sort = "UnitCode ASC";
                        break;
                }
            }

            MyDS.Dispose();
            Myda.Dispose();

            myConnection.Close();
            return MyDS;

        }








    }
}