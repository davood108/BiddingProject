﻿namespace PowerPlantProject
{
    partial class SaleBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SaleBill));
            this.button1 = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtmaliat = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtsphone = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtscode = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtseco = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtsadd = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtsshahr = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtsostan = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtsname = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.datePickerFrom = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.txttakhfif = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtbname = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtbostan = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtbshahr = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtbadd = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtbeco = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtbcode = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtbphone = new System.Windows.Forms.TextBox();
            this.txtbsabt = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtssabt = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(343, 398);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Export";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtssabt);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.txtsphone);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtscode);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtseco);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtsadd);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtsshahr);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtsostan);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtsname);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(31, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(693, 156);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "مشخصات فروشنده";
            // 
            // txtmaliat
            // 
            this.txtmaliat.Location = new System.Drawing.Point(406, 24);
            this.txtmaliat.Name = "txtmaliat";
            this.txtmaliat.Size = new System.Drawing.Size(112, 20);
            this.txtmaliat.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(330, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "عوارض ماليات";
            // 
            // txtsphone
            // 
            this.txtsphone.Location = new System.Drawing.Point(503, 79);
            this.txtsphone.Name = "txtsphone";
            this.txtsphone.Size = new System.Drawing.Size(112, 20);
            this.txtsphone.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(418, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "شماره تلفن";
            // 
            // txtscode
            // 
            this.txtscode.Location = new System.Drawing.Point(503, 53);
            this.txtscode.Name = "txtscode";
            this.txtscode.Size = new System.Drawing.Size(112, 20);
            this.txtscode.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(383, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "كد پستي 10 رقمي";
            // 
            // txtseco
            // 
            this.txtseco.Location = new System.Drawing.Point(503, 28);
            this.txtseco.Name = "txtseco";
            this.txtseco.Size = new System.Drawing.Size(112, 20);
            this.txtseco.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(396, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "شماره اقتصادي";
            // 
            // txtsadd
            // 
            this.txtsadd.Location = new System.Drawing.Point(176, 130);
            this.txtsadd.Name = "txtsadd";
            this.txtsadd.Size = new System.Drawing.Size(443, 20);
            this.txtsadd.TabIndex = 1;
            this.txtsadd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(97, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "نشاني كامل";
            // 
            // txtsshahr
            // 
            this.txtsshahr.Location = new System.Drawing.Point(172, 81);
            this.txtsshahr.Name = "txtsshahr";
            this.txtsshahr.Size = new System.Drawing.Size(112, 20);
            this.txtsshahr.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(105, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "شهر";
            // 
            // txtsostan
            // 
            this.txtsostan.Location = new System.Drawing.Point(172, 53);
            this.txtsostan.Name = "txtsostan";
            this.txtsostan.Size = new System.Drawing.Size(112, 20);
            this.txtsostan.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(105, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "استان ";
            // 
            // txtsname
            // 
            this.txtsname.Location = new System.Drawing.Point(172, 27);
            this.txtsname.Name = "txtsname";
            this.txtsname.Size = new System.Drawing.Size(112, 20);
            this.txtsname.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(78, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "نام شخص حقوقي    ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtbsabt);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.txtbphone);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtbname);
            this.groupBox2.Controls.Add(this.txtbcode);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.txtbostan);
            this.groupBox2.Controls.Add(this.txtbeco);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtbshahr);
            this.groupBox2.Controls.Add(this.txtbadd);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Location = new System.Drawing.Point(31, 234);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(693, 154);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "مشخصات خريدار";
            // 
            // datePickerFrom
            // 
            this.datePickerFrom.HasButtons = true;
            this.datePickerFrom.Location = new System.Drawing.Point(127, 24);
            this.datePickerFrom.Name = "datePickerFrom";
            this.datePickerFrom.Readonly = true;
            this.datePickerFrom.Size = new System.Drawing.Size(140, 20);
            this.datePickerFrom.TabIndex = 34;
            this.datePickerFrom.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(539, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "درصد تخفيف";
            // 
            // txttakhfif
            // 
            this.txttakhfif.Location = new System.Drawing.Point(608, 24);
            this.txttakhfif.Name = "txttakhfif";
            this.txttakhfif.Size = new System.Drawing.Size(112, 20);
            this.txttakhfif.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(63, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 35;
            this.label10.Text = "تاريخ :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(74, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(99, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "نام شخص حقوقي    ";
            // 
            // txtbname
            // 
            this.txtbname.Location = new System.Drawing.Point(176, 29);
            this.txtbname.Name = "txtbname";
            this.txtbname.Size = new System.Drawing.Size(112, 20);
            this.txtbname.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(109, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "استان ";
            // 
            // txtbostan
            // 
            this.txtbostan.Location = new System.Drawing.Point(176, 55);
            this.txtbostan.Name = "txtbostan";
            this.txtbostan.Size = new System.Drawing.Size(112, 20);
            this.txtbostan.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(109, 87);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "شهر";
            // 
            // txtbshahr
            // 
            this.txtbshahr.Location = new System.Drawing.Point(176, 83);
            this.txtbshahr.Name = "txtbshahr";
            this.txtbshahr.Size = new System.Drawing.Size(112, 20);
            this.txtbshahr.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(97, 132);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "نشاني كامل";
            // 
            // txtbadd
            // 
            this.txtbadd.Location = new System.Drawing.Point(176, 129);
            this.txtbadd.Name = "txtbadd";
            this.txtbadd.Size = new System.Drawing.Size(443, 20);
            this.txtbadd.TabIndex = 1;
            this.txtbadd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(400, 27);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "شماره اقتصادي";
            // 
            // txtbeco
            // 
            this.txtbeco.Location = new System.Drawing.Point(507, 25);
            this.txtbeco.Name = "txtbeco";
            this.txtbeco.Size = new System.Drawing.Size(112, 20);
            this.txtbeco.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(387, 53);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "كد پستي 10 رقمي";
            // 
            // txtbcode
            // 
            this.txtbcode.Location = new System.Drawing.Point(507, 50);
            this.txtbcode.Name = "txtbcode";
            this.txtbcode.Size = new System.Drawing.Size(112, 20);
            this.txtbcode.TabIndex = 1;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(422, 81);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "شماره تلفن";
            // 
            // txtbphone
            // 
            this.txtbphone.Location = new System.Drawing.Point(507, 78);
            this.txtbphone.Name = "txtbphone";
            this.txtbphone.Size = new System.Drawing.Size(112, 20);
            this.txtbphone.TabIndex = 1;
            // 
            // txtbsabt
            // 
            this.txtbsabt.Location = new System.Drawing.Point(507, 104);
            this.txtbsabt.Name = "txtbsabt";
            this.txtbsabt.Size = new System.Drawing.Size(112, 20);
            this.txtbsabt.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(426, 104);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "شماره ثبت";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(422, 104);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "شماره ثبت";
            // 
            // txtssabt
            // 
            this.txtssabt.Location = new System.Drawing.Point(503, 104);
            this.txtssabt.Name = "txtssabt";
            this.txtssabt.Size = new System.Drawing.Size(112, 20);
            this.txtssabt.TabIndex = 3;
            // 
            // SaleBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 429);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txttakhfif);
            this.Controls.Add(this.txtmaliat);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.datePickerFrom);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SaleBill";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SaleBill";
            this.Load += new System.EventHandler(this.SaleBill_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerFrom;
        private System.Windows.Forms.TextBox txtmaliat;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtsphone;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtscode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtseco;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtsadd;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtsshahr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtsostan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtsname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txttakhfif;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtbphone;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtbname;
        private System.Windows.Forms.TextBox txtbcode;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtbostan;
        private System.Windows.Forms.TextBox txtbeco;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtbshahr;
        private System.Windows.Forms.TextBox txtbadd;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtbsabt;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtssabt;
        private System.Windows.Forms.Label label19;
    }
}