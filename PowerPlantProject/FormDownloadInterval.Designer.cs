﻿namespace PowerPlantProject
{
    partial class FormDownloadInterval
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDownloadInterval));
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.datePickerTo = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panel81 = new System.Windows.Forms.Panel();
            this.datePickerFrom = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label17 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.txtdownloadexistfile = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rb002 = new System.Windows.Forms.RadioButton();
            this.rb005 = new System.Windows.Forms.RadioButton();
            this.rbDispatch = new System.Windows.Forms.RadioButton();
            this.rbChartPrice = new System.Windows.Forms.RadioButton();
            this.rblfc = new System.Windows.Forms.RadioButton();
            this.rbinternet = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rdm005ba = new System.Windows.Forms.RadioButton();
            this.rddailybill = new System.Windows.Forms.RadioButton();
            this.rdsaledenergy = new System.Windows.Forms.RadioButton();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.btnDownload = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel81.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(19, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Files :";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.CadetBlue;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.datePickerTo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(224, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(163, 49);
            this.panel1.TabIndex = 12;
            // 
            // datePickerTo
            // 
            this.datePickerTo.HasButtons = true;
            this.datePickerTo.Location = new System.Drawing.Point(10, 24);
            this.datePickerTo.Name = "datePickerTo";
            this.datePickerTo.Readonly = true;
            this.datePickerTo.Size = new System.Drawing.Size(140, 20);
            this.datePickerTo.TabIndex = 33;
            this.datePickerTo.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(7, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "To";
            // 
            // panel81
            // 
            this.panel81.BackColor = System.Drawing.Color.CadetBlue;
            this.panel81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel81.Controls.Add(this.datePickerFrom);
            this.panel81.Controls.Add(this.label17);
            this.panel81.Location = new System.Drawing.Point(32, 53);
            this.panel81.Name = "panel81";
            this.panel81.Size = new System.Drawing.Size(163, 49);
            this.panel81.TabIndex = 11;
            // 
            // datePickerFrom
            // 
            this.datePickerFrom.HasButtons = true;
            this.datePickerFrom.Location = new System.Drawing.Point(10, 24);
            this.datePickerFrom.Name = "datePickerFrom";
            this.datePickerFrom.Readonly = true;
            this.datePickerFrom.Size = new System.Drawing.Size(140, 20);
            this.datePickerFrom.TabIndex = 33;
            this.datePickerFrom.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.datePickerFrom.ValueChanged += new System.EventHandler(this.datePickerFrom_ValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(7, 7);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(34, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "From";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(10, 244);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(394, 23);
            this.progressBar1.TabIndex = 14;
            this.progressBar1.Visible = false;
            // 
            // txtdownloadexistfile
            // 
            this.txtdownloadexistfile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtdownloadexistfile.BackColor = System.Drawing.Color.LemonChiffon;
            this.txtdownloadexistfile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdownloadexistfile.Location = new System.Drawing.Point(435, 47);
            this.txtdownloadexistfile.Multiline = true;
            this.txtdownloadexistfile.Name = "txtdownloadexistfile";
            this.txtdownloadexistfile.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtdownloadexistfile.Size = new System.Drawing.Size(405, 230);
            this.txtdownloadexistfile.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(469, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(185, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Last Downloaded Date of Files:";
            // 
            // rb002
            // 
            this.rb002.AutoSize = true;
            this.rb002.BackColor = System.Drawing.Color.Transparent;
            this.rb002.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rb002.Location = new System.Drawing.Point(30, 34);
            this.rb002.Name = "rb002";
            this.rb002.Size = new System.Drawing.Size(56, 17);
            this.rb002.TabIndex = 17;
            this.rb002.TabStop = true;
            this.rb002.Text = "M002";
            this.rb002.UseVisualStyleBackColor = false;
            this.rb002.CheckedChanged += new System.EventHandler(this.rb002_CheckedChanged);
            // 
            // rb005
            // 
            this.rb005.AutoSize = true;
            this.rb005.BackColor = System.Drawing.Color.Transparent;
            this.rb005.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rb005.Location = new System.Drawing.Point(30, 56);
            this.rb005.Name = "rb005";
            this.rb005.Size = new System.Drawing.Size(56, 17);
            this.rb005.TabIndex = 17;
            this.rb005.TabStop = true;
            this.rb005.Text = "M005";
            this.rb005.UseVisualStyleBackColor = false;
            this.rb005.CheckedChanged += new System.EventHandler(this.rb005_CheckedChanged);
            // 
            // rbDispatch
            // 
            this.rbDispatch.AutoSize = true;
            this.rbDispatch.BackColor = System.Drawing.Color.Transparent;
            this.rbDispatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rbDispatch.Location = new System.Drawing.Point(30, 78);
            this.rbDispatch.Name = "rbDispatch";
            this.rbDispatch.Size = new System.Drawing.Size(74, 17);
            this.rbDispatch.TabIndex = 17;
            this.rbDispatch.TabStop = true;
            this.rbDispatch.Text = "Dispatch";
            this.rbDispatch.UseVisualStyleBackColor = false;
            this.rbDispatch.CheckedChanged += new System.EventHandler(this.rbDispatch_CheckedChanged);
            // 
            // rbChartPrice
            // 
            this.rbChartPrice.AutoSize = true;
            this.rbChartPrice.BackColor = System.Drawing.Color.Transparent;
            this.rbChartPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rbChartPrice.Location = new System.Drawing.Point(127, 32);
            this.rbChartPrice.Name = "rbChartPrice";
            this.rbChartPrice.Size = new System.Drawing.Size(87, 17);
            this.rbChartPrice.TabIndex = 17;
            this.rbChartPrice.TabStop = true;
            this.rbChartPrice.Tag = "AveragePrice";
            this.rbChartPrice.Text = "Chart Price";
            this.rbChartPrice.UseVisualStyleBackColor = false;
            this.rbChartPrice.CheckedChanged += new System.EventHandler(this.rbChartPrice_CheckedChanged);
            // 
            // rblfc
            // 
            this.rblfc.AutoSize = true;
            this.rblfc.BackColor = System.Drawing.Color.Transparent;
            this.rblfc.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rblfc.Location = new System.Drawing.Point(127, 56);
            this.rblfc.Name = "rblfc";
            this.rblfc.Size = new System.Drawing.Size(42, 17);
            this.rblfc.TabIndex = 17;
            this.rblfc.TabStop = true;
            this.rblfc.Tag = "LoadForecasting";
            this.rblfc.Text = "Lfc";
            this.rblfc.UseVisualStyleBackColor = false;
            this.rblfc.CheckedChanged += new System.EventHandler(this.rblfc_CheckedChanged);
            // 
            // rbinternet
            // 
            this.rbinternet.AutoSize = true;
            this.rbinternet.BackColor = System.Drawing.Color.Transparent;
            this.rbinternet.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rbinternet.Location = new System.Drawing.Point(233, 34);
            this.rbinternet.Name = "rbinternet";
            this.rbinternet.Size = new System.Drawing.Size(73, 17);
            this.rbinternet.TabIndex = 17;
            this.rbinternet.TabStop = true;
            this.rbinternet.Text = " Internet";
            this.rbinternet.UseVisualStyleBackColor = false;
            this.rbinternet.CheckedChanged += new System.EventHandler(this.rbinternet_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.CadetBlue;
            this.panel2.Controls.Add(this.rdm005ba);
            this.panel2.Controls.Add(this.rddailybill);
            this.panel2.Controls.Add(this.rdsaledenergy);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.rb002);
            this.panel2.Controls.Add(this.rbinternet);
            this.panel2.Controls.Add(this.rb005);
            this.panel2.Controls.Add(this.rblfc);
            this.panel2.Controls.Add(this.rbDispatch);
            this.panel2.Controls.Add(this.rbChartPrice);
            this.panel2.Location = new System.Drawing.Point(32, 115);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(355, 115);
            this.panel2.TabIndex = 18;
            // 
            // rdm005ba
            // 
            this.rdm005ba.AutoSize = true;
            this.rdm005ba.BackColor = System.Drawing.Color.Transparent;
            this.rdm005ba.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rdm005ba.Location = new System.Drawing.Point(233, 55);
            this.rdm005ba.Name = "rdm005ba";
            this.rdm005ba.Size = new System.Drawing.Size(118, 17);
            this.rdm005ba.TabIndex = 21;
            this.rdm005ba.TabStop = true;
            this.rdm005ba.Text = "M005-withLimmit";
            this.rdm005ba.UseVisualStyleBackColor = false;
            this.rdm005ba.Visible = false;
            this.rdm005ba.CheckedChanged += new System.EventHandler(this.rdm005ba_CheckedChanged);
            // 
            // rddailybill
            // 
            this.rddailybill.AutoSize = true;
            this.rddailybill.Location = new System.Drawing.Point(233, 78);
            this.rddailybill.Name = "rddailybill";
            this.rddailybill.Size = new System.Drawing.Size(70, 17);
            this.rddailybill.TabIndex = 20;
            this.rddailybill.TabStop = true;
            this.rddailybill.Text = "DailyBill";
            this.rddailybill.UseVisualStyleBackColor = true;
            this.rddailybill.Visible = false;
            this.rddailybill.CheckedChanged += new System.EventHandler(this.rddailybill_CheckedChanged);
            // 
            // rdsaledenergy
            // 
            this.rdsaledenergy.AutoSize = true;
            this.rdsaledenergy.BackColor = System.Drawing.Color.Transparent;
            this.rdsaledenergy.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rdsaledenergy.Location = new System.Drawing.Point(127, 78);
            this.rdsaledenergy.Name = "rdsaledenergy";
            this.rdsaledenergy.Size = new System.Drawing.Size(96, 17);
            this.rdsaledenergy.TabIndex = 19;
            this.rdsaledenergy.TabStop = true;
            this.rdsaledenergy.Text = "SaledEnergy";
            this.rdsaledenergy.UseVisualStyleBackColor = false;
            this.rdsaledenergy.CheckedChanged += new System.EventHandler(this.rdsaledenergy_CheckedChanged);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(295, 33);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(133, 13);
            this.linkLabel1.TabIndex = 19;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "http://sccisrep.igmc.ir";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label4.Location = new System.Drawing.Point(9, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(419, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "گزارش تفضيلي-گزارش روزانه انرژي -گزارش بار مناطق و صنايع-گزارش اطلاعات اجزاي شبكه" +
    "";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.linkLabel1);
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(1, -5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(466, 49);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Visible = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BackgroundImage = global::PowerPlantProject.Properties.Resources.internet7_explorer;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel3.Location = new System.Drawing.Point(430, 9);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(33, 33);
            this.panel3.TabIndex = 22;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "save to : ";
            // 
            // btnDownload
            // 
            this.btnDownload.BackColor = System.Drawing.Color.CadetBlue;
            this.btnDownload.BackgroundImage = global::PowerPlantProject.Properties.Resources.load;
            this.btnDownload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnDownload.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDownload.Location = new System.Drawing.Point(149, 251);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(87, 26);
            this.btnDownload.TabIndex = 10;
            this.btnDownload.Text = "Load";
            this.btnDownload.UseVisualStyleBackColor = false;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = global::PowerPlantProject.Properties.Resources.forward;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel4.Location = new System.Drawing.Point(785, 279);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(57, 30);
            this.panel4.TabIndex = 42;
            this.panel4.Click += new System.EventHandler(this.panel4_Click);
            // 
            // FormDownloadInterval
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(856, 311);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtdownloadexistfile);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel81);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btnDownload);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormDownloadInterval";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Load Data";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormDownloadInterval_FormClosed);
            this.Load += new System.EventHandler(this.FormDownloadInterval_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FormDownloadInterval_Paint);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel81.ResumeLayout(false);
            this.panel81.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel81;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerFrom;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox txtdownloadexistfile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rb002;
        private System.Windows.Forms.RadioButton rb005;
        private System.Windows.Forms.RadioButton rbDispatch;
        private System.Windows.Forms.RadioButton rbChartPrice;
        private System.Windows.Forms.RadioButton rblfc;
        private System.Windows.Forms.RadioButton rbinternet;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rdsaledenergy;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton rddailybill;
        private System.Windows.Forms.RadioButton rdm005ba;
        public System.Windows.Forms.Panel panel4;
    }
}