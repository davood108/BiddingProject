﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NRI.SBS.Common;
using System.Data.SqlClient;
using FarsiLibrary.Utils;
using System.Globalization;
namespace PowerPlantProject
{
    public partial class SpecialTimes : Form
    {
        public string Year = "";
        public SpecialTimes()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                /////////////////////////////////grid////////////////////////////////////////

              
                dataGridView1.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                dataGridView1.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;
            }
            catch
            {

            }
        }

        private void SpecialTimes_Load(object sender, EventArgs e)
        {
            datePickerweekend.SelectedDateTime = DateTime.Now;
            datePickerweekend.Text = new PersianDate(System.DateTime.Now).ToString("d");
            fillgrid();
        }
        private void fillgrid()
        {
            try
            {
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                dataGridView1.DataSource = null;

                DataTable dt = Utilities.GetTable("select * from SpecialTime where Date like '" + Year + '%' + "' order by date asc");
                dataGridView1.DataSource = dt;
            }
            catch
            {

            }

        }      
      
       
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                dataGridView1.AllowUserToDeleteRows = true;
                string date = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                string day = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                int index = dataGridView1.CurrentCell.RowIndex;
                DataTable dt = Utilities.GetTable("select Date from SpecialTime where Date like '" + Year + '%' + "' order by date asc");
                string Date = dt.Rows[index][0].ToString().Trim();


                if (dataGridView1.CurrentCell.Value.ToString().Trim() == "Edit")
                {
                    DataTable otable = Utilities.GetTable("update SpecialTime set Date='" + date + "' , DayOfWeekend='" + day + "' where Date='" + Date + "'");
                    fillgrid();
                }
                if (dataGridView1.CurrentCell.Value.ToString().Trim() == "Delete")
                {
                    DataTable otable = Utilities.GetTable("delete from SpecialTime where Date='" + date + "'");
                    fillgrid();
                }
            }
            catch
            {

            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string date = datePickerweekend.Text.Trim();
                string day = PersianDateConverter.ToGregorianDateTime(date).DayOfWeek.ToString().Trim();

                DataTable dt = Utilities.GetTable("insert into SpecialTime (Date , DayOfWeekend) values('" + date + "','" + day + "')");
                fillgrid();
            }
            catch
            {
            }
        }

        private void datePickerweekend_ValueChanged(object sender, EventArgs e)
        {
            string[] tempsplit = datePickerweekend.Text.Split('/');
            Year = tempsplit[0].Trim();
            fillgrid();
        }

       


    }
}
