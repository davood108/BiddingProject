﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NRI.SBS.Common;
using System.Data.SqlClient;
using FarsiLibrary.Utils;


namespace PowerPlantProject
{
    public partial class StatusForm : Form
    {
        int plantnum = 0;
       
        int Package_Num = 0;
        string predate = "";
   

        public StatusForm()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;
                   
                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////

                ////////////////////////////////////datagridview///////////////////////////////////////
              
                                  
                   dataGridinternet.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                   dataGridothers.DefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                   dataGridStatus.DefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                   dataGridStatus.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;


                   dataGridinternet.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                   dataGridothers.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                   dgbill.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                   dataGridStatus.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                ////////////////////////////////////////////////////////////////////////////////////////

            }
            catch
            {

            }
        }

        private void StatusForm_Load(object sender, EventArgs e)
        {
            statusdatePicker.SelectedDateTime = System.DateTime.Now;
            statusdatePicker.Text = new PersianDate(System.DateTime.Now).ToString("d");
            FillGrid(statusdatePicker.Text);
            Buildtree(new PersianDate(System.DateTime.Now).ToString("d"));
           
        }

   
        private void FillGrid(string date)
        {
            dataGridStatus.DataSource = null;
            dataGridinternet.DataSource = null;
            dataGridothers.DataSource = null;
            dgbill.DataSource = null;
            dataGridinternet.AllowUserToDeleteRows = false;
            dataGridStatus.AllowUserToDeleteRows = false;
            dataGridothers.AllowUserToDeleteRows = false;
            dgbill.AllowUserToDeleteRows = false;


            //DateTime dt = PersianDateConverter.ToGregorianDateTime(date).AddDays(-1);
            //string yesterday = new PersianDate(dt).ToString("d");
            
            DataTable ODATA = Utilities.GetTable("select distinct PPID from dbo.PowerPlant");
            plantnum = ODATA.Rows.Count;
            dataGridStatus.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridinternet.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridothers.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgbill.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridStatus.DataSource = ODATA.DefaultView;

            /////////////////////////////for block/////////////////////////////

            DataTable oDataTable = Utilities.GetTable("select UnitCode from UnitsDataMain");
            Package_Num = oDataTable.Rows.Count;

            string[,] blocks002 = new string[plantnum, Package_Num];
            string[,] blocks = new string[plantnum, Package_Num];
            blocks002 = Getblock002();
            blocks = Getblock();

            ////////////////////////////////ppname column/////////////////////////////////

            DataTable nametable = Utilities.GetTable("select distinct PPID,PPName from dbo.PowerPlant order by PPID");
            if (nametable.Rows.Count > 0)
            {
                for (int i = 0; i < nametable.Rows.Count; i++)
                {
                    dataGridStatus.Rows[i].Cells[0].Value = nametable.Rows[i][1].ToString().Trim();


                }
            }
            /////////////////////////////each cell value for each plant////////////////////////////////
            int index = 0;
            int plantindex = 0;
            bool[] checkbefore = new bool[plantnum];
            foreach (DataRow myrow in ODATA.Rows)
            {

                bool enterlimmit = true;
                bool enterm009 = true;
                bool enterUnitData = true;
                bool entergeneral = true;
                DataTable unitstable = Utilities.GetTable("select distinct UnitCode from dbo.UnitsDataMain where PPID='" + myrow[0].ToString().Trim() + "'");
                for (int i = 0; i < unitstable.Rows.Count; i++)
                {

                    DataTable limmitTable = Utilities.GetTable("select * from dbo.PowerLimitedUnit where PPID='" + myrow[0].ToString().Trim() + "'" +
                    "and StartDate='" + date + "'and UnitCode='" + unitstable.Rows[i][0].ToString().Trim() + "'and isEmpty=0");
                    DataTable m009tTable = Utilities.GetTable("select * from dbo.DetailFRM009 where PPID='" + myrow[0].ToString().Trim() + "'" +
                    "and TargetMarketDate='" + date + "' and Block='" + unitstable.Rows[i][0].ToString().Trim() + "'");
                    DataTable UnitDataTable = Utilities.GetTable("select * from dbo.UnitsDataMain where PPID='" + myrow[0].ToString().Trim() + "' and UnitCode='" + unitstable.Rows[i][0].ToString().Trim() + "'");
                    if (enterlimmit)
                    {
                        if (limmitTable.Rows.Count == 0)
                        {
                           dataGridStatus.Rows[index].Cells[1].Value=0;
                            enterlimmit = false;
                        }
                        else
                        {
                            dataGridStatus.Rows[index].Cells[1].Value = 1;
                            checkbefore[plantindex] = true;
                        }
                    }
                    if (enterm009)
                    {
                        if (m009tTable.Rows.Count == 0) { dataGridStatus.Rows[index].Cells[2].Value = 0; enterm009 = false; }
                        else dataGridStatus.Rows[index].Cells[2].Value = 1;
                    }
                    if (enterUnitData)
                    {
                        if (UnitDataTable.Rows.Count == 0) { dataGridStatus.Rows[index].Cells[3].Value = 0; enterUnitData = false; }
                        else dataGridStatus.Rows[index].Cells[3].Value = 1;
                    }
                    if (enterUnitData)
                    {
                        if (UnitDataTable.Rows[0]["SecondaryPowerC"].ToString() == "" || UnitDataTable.Rows[0]["SecondaryPowerB"].ToString() == ""
                           || UnitDataTable.Rows[0]["CostStartWarm"].ToString() == "" || UnitDataTable.Rows[0]["SecondaryPowerA"].ToString() == ""
                           || UnitDataTable.Rows[0]["PrimaryPowerC"].ToString() == "" || UnitDataTable.Rows[0]["PrimaryPowerB"].ToString() == ""
                           || UnitDataTable.Rows[0]["PrimaryPowerA"].ToString() == "" || UnitDataTable.Rows[0]["SecondaryHeatRateC"].ToString() == ""
                           || UnitDataTable.Rows[0]["SecondaryHeatRateB"].ToString() == "" || UnitDataTable.Rows[0]["SecondaryHeatRateA"].ToString() == ""
                           || UnitDataTable.Rows[0]["PrimaryHeatRateA"].ToString() == "" || UnitDataTable.Rows[0]["PrimaryHeatRateB"].ToString() == ""
                           || UnitDataTable.Rows[0]["PrimaryHeatRateC"].ToString() == "" || UnitDataTable.Rows[0]["CostStartHot"].ToString() == ""
                           || UnitDataTable.Rows[0]["CostStartCold"].ToString() == "")
                        {
                            dataGridStatus.Rows[index].Cells[3].Value = 0;
                            enterUnitData = false;
                        }
                        else
                        {
                            dataGridStatus.Rows[index].Cells[3].Value = 1;
                        }
                    }
                    if (entergeneral)
                    {
                        if (UnitDataTable.Rows[0]["PMax"].ToString() == "" || UnitDataTable.Rows[0]["PMin"].ToString() == ""
                           || UnitDataTable.Rows[0]["TUp"].ToString() == "" || UnitDataTable.Rows[0]["TDown"].ToString() == ""
                           || UnitDataTable.Rows[0]["RampUpRate"].ToString() == "" || UnitDataTable.Rows[0]["Capacity"].ToString() == ""
                           || UnitDataTable.Rows[0]["HeatValueSecondaryFuel"].ToString() == "" || UnitDataTable.Rows[0]["HeatValuePrimaryFuel"].ToString() == "")
                        {
                            dataGridStatus.Rows[index].Cells[4].Value = 0;
                            entergeneral = false;
                        }
                        else
                        {
                            dataGridStatus.Rows[index].Cells[4].Value = 1;
                        }


                    }
                }


                index++;
                plantindex++;
            }


            ///////////////////////////////////////block////////////////////////////////////

            int[] Plant_Packages = new int[plantnum];

            DataTable oDataTable2 = Utilities.GetTable("select distinct PPID from dbo.PowerPlant");

            string[] plantid = new string[plantnum];

            for (int i = 0; i < plantnum; i++)
            {
                plantid[i] = oDataTable2.Rows[i][0].ToString().Trim();

            }

            int[] Plant_units_Num = new int[plantnum];
            for (int re = 0; re < plantnum; re++)
            {
                DataTable oDataTable5 = Utilities.GetTable("select Max(PackageCode) from UnitsDataMain where  PPID='" + plantid[re] + "'");
                Plant_Packages[re] = (int)oDataTable5.Rows[0][0];
                DataTable  oD = Utilities.GetTable("select UnitCode from UnitsDataMain where PPID='" + plantid[re] + "'");
                Plant_units_Num[re] = oD.Rows.Count;
            }

            ///////////////////////////////////////fill cell////////////////////////////////
            int blockindex = 0;
            for (int j = 0; j < plantnum; j++)
            {
                bool enter2 = true;
                bool enter5 = true;
                bool baenter5 = true;
                bool enterdispatch = true;
                for (int k = 0; k <Plant_units_Num[j] ; k++)
                {
                    DataTable m002table = Utilities.GetTable("select * from  dbo.DetailFRM002 where PPID='" + plantid[j] + "'and TargetMarketDate='" + date + "'and Block='" + blocks002[j, k] + "'and ( Estimated is null or Estimated = 0)");
                    DataTable bam005table = Utilities.GetTable("select * from  dbo.baDetailFRM005 where PPID='" + plantid[j] + "'and TargetMarketDate='" + date + "'and Block='" + blocks[j, k] + "'");
                    DataTable m005table = Utilities.GetTable("select * from  dbo.DetailFRM005 where PPID='" + plantid[j] + "'and TargetMarketDate='" + date + "'and Block='" + blocks[j, k] + "'");
                    DataTable Dispatch = Utilities.GetTable("select count(*) from dbo.Dispathable WHERE PPID='" + plantid[j] + "' AND StartDate='" + date + "'AND Block='" + blocks002[j, k] + "'");
                    if (enter2)
                    {
                        if (m002table.Rows.Count == 0) { dataGridStatus.Rows[blockindex].Cells[5].Value = 0; enter2 = false; }
                        else dataGridStatus.Rows[blockindex].Cells[5].Value = 1;
                    }
                    if (enter5)
                    {
                        if (m005table.Rows.Count == 0) { dataGridStatus.Rows[blockindex].Cells[6].Value = 0; enter5 = false; }
                        else dataGridStatus.Rows[blockindex].Cells[6].Value = 1;
                    }
                    if (baenter5)
                    {
                        if (bam005table.Rows.Count == 0) { dataGridStatus.Rows[blockindex].Cells[7].Value = 0; baenter5 = false; }
                        else dataGridStatus.Rows[blockindex].Cells[7].Value = 1;
                    }
                    if (enterdispatch)
                    {
                        if (int.Parse(Dispatch.Rows[0][0].ToString()) == 0 && checkbefore[j] == false) { dataGridStatus.Rows[blockindex].Cells[1].Value = 0; enterdispatch = false; }
                        else if (int.Parse(Dispatch.Rows[0][0].ToString()) == 24) dataGridStatus.Rows[blockindex].Cells[1].Value = 1;
                    }

                }
                blockindex++;

            }


            /////////////////////////////////////////////////////////////////

            dataGridStatus.Columns.Remove(dataGridStatus.Columns[8]);

            ////////////////////////////////internet grid and post //////////////////////////////////////////

            DataTable odataunitcomp = Utilities.GetTable("select * from dbo.UnitNetComp where Date='"+date+"'");
            DataTable odatarep12 = Utilities.GetTable("select* from dbo.Rep12Page where Date='" + date + "'");
            DataTable odataregion = Utilities.GetTable("select * from dbo.RegionNetComp where Date='" + date + "'");
            DataTable odataproduced = Utilities.GetTable("select * from dbo.ProducedEnergy where Date='" + date + "'");
            //DataTable odataoutage = utilities.GetTable("select * from dbo.OutageUnits where Date='" + date + "'");
            DataTable odatamanategh = Utilities.GetTable("select * from dbo.Manategh where Date='" + date + "'");
            DataTable odatalinenet = Utilities.GetTable("select * from dbo.LineNetComp where Date='" + date + "'");
            DataTable odatainterchange = Utilities.GetTable("select * from dbo.InterchangedEnergy where Date='" + date + "'");
            DataTable odatapost = Utilities.GetTable("select * from dbo.DetailFRM009Post where TargetMarketDate='" + date + "'");

            if(odataunitcomp.Rows.Count==0) dataGridinternet.Rows[0].Cells[0].Value=0;
            else dataGridinternet.Rows[0].Cells[0].Value=1;

            if(odatalinenet.Rows.Count==0) dataGridinternet.Rows[0].Cells[1].Value=0;
            else dataGridinternet.Rows[0].Cells[1].Value = 1;

            if (odataregion.Rows.Count == 0) dataGridinternet.Rows[0].Cells[2].Value = 0;
            else dataGridinternet.Rows[0].Cells[2].Value = 1;
            
            if (odatainterchange.Rows.Count == 0) dataGridinternet.Rows[0].Cells[3].Value = 0;
            else dataGridinternet.Rows[0].Cells[3].Value = 1;

            if (odatamanategh.Rows.Count == 0) dataGridinternet.Rows[0].Cells[4].Value = 0;
            else dataGridinternet.Rows[0].Cells[4].Value = 1;

            if (odataproduced.Rows.Count == 0) dataGridinternet.Rows[0].Cells[5].Value = 0;
            else dataGridinternet.Rows[0].Cells[5].Value = 1;

            if (odatarep12.Rows.Count == 0) dataGridinternet.Rows[0].Cells[6].Value = 0;
            else dataGridinternet.Rows[0].Cells[6].Value = 1;

            if (odatapost.Rows.Count < 100) dataGridinternet.Rows[0].Cells[7].Value = 0;
            else dataGridinternet.Rows[0].Cells[7].Value = 1;


            //if (odataoutage.Rows.Count == 0) dataGridinternet.Rows[0].Cells[6].Value = 0;
            //else dataGridinternet.Rows[0].Cells[6].Value = 1;

            //if (odatarep12.Rows.Count == 0) dataGridinternet.Rows[0].Cells[7].Value = 0;
            //else dataGridinternet.Rows[0].Cells[7].Value = 1;



           ////////////////////////////////////////others////////////////////////////////////////////////////
            DataTable chart = Utilities.GetTable("select * from  dbo.AveragePrice where Date='"+date+"'");
            DataTable lfc = Utilities.GetTable("select * from dbo.LoadForecasting where DateEstimate='" + date + "'");
            DataTable saled = Utilities.GetTable("select *  from dbo.SaledEnergy where Date='"+date+"'");

            if (chart.Rows.Count < 24) dataGridothers.Rows[0].Cells[0].Value = 0;
            else dataGridothers.Rows[0].Cells[0].Value = 1;

            if (lfc.Rows.Count == 0) dataGridothers.Rows[0].Cells[1].Value = 0;
            else dataGridothers.Rows[0].Cells[1].Value = 1;

            if(saled!=null)
            {
                if (saled.Rows.Count < 15) dataGridothers.Rows[0].Cells[2].Value = 0;
                else dataGridothers.Rows[0].Cells[2].Value = 1;

            }

            //////////////////////////////////////billl///////////////////////////////////////
            DataTable s1 = Utilities.GetTable("select * from  MonthlyBillTotal where month='" + date.Substring(0,7).Trim() + "'");
            DataTable s2 = Utilities.GetTable("select * from MonthlyBillPlant where Date='" + date + "'");
            DataTable s3 = Utilities.GetTable("select *  from MonthlyBillDate where Date='" + date + "'");
            DataTable s4 = Utilities.GetTable("select *  from yearhub where year='" + date.Substring(0, 4).Trim() + "'");
            DataTable s5 = Utilities.GetTable("select *  from  YearTrans where year='" + date.Substring(0, 4).Trim() + "'");

            if (s1.Rows.Count == 0 || s2.Rows.Count == 0 || s3.Rows.Count == 0) dgbill.Rows[0].Cells[0].Value = 0;
            else dgbill.Rows[0].Cells[0].Value = 1;

            if (s4.Rows.Count == 0) dgbill.Rows[0].Cells[1].Value = 0;
            else dgbill.Rows[0].Cells[1].Value = 1;

            if (s5.Rows.Count == 0) dgbill.Rows[0].Cells[2].Value = 0;
            else dgbill.Rows[0].Cells[2].Value = 1;



        }

        private void Buildtree(string date)
        {
            bool enter = true;

            if (predate != "" )
            {
                if (predate.Substring(0, 7) == date.Substring(0, 7) )
                {
                    enter = false;
                }
            }
          

            if (enter)
            {

                treeView1.Nodes.Clear();
                TreeNode MyNode = new TreeNode();
                MyNode.Text = "Date";
                treeView1.Nodes.Add(MyNode);
                TreeNode DateNode = new TreeNode();

                DateTime time = PersianDateConverter.ToGregorianDateTime(date);
                string year = new PersianDate(time).Year.ToString();
                string month = date.Substring(5, 2);

                string primdate = year + "/" + month + "/01";
                DateTime primetime = PersianDateConverter.ToGregorianDateTime(primdate);

                for (int i = 0; i < 31; i++)
                {

                    MyNode.Nodes.Add(new PersianDate(primetime.AddDays(i)).ToString("d"));
                }

                treeView1.ExpandAll();
                predate = "";
            }
            
        }

        private void statusdatePicker_ValueChanged(object sender, EventArgs e)
        {
           FillGrid(statusdatePicker.Text);
           Buildtree(statusdatePicker.Text);
         
          
        }

        private string[,] Getblock()
        {
            DataTable oDataTable = null;

            int[] Plant_Packages_Num = new int[plantnum];
            int[] Plant_unit_Num = new int[plantnum];

            oDataTable = Utilities.GetTable("select distinct PPID from dbo.PowerPlant");

            string[] plantid = new string[plantnum];

            for (int i = 0; i < plantnum; i++)
            {
                plantid[i] = oDataTable.Rows[i][0].ToString().Trim();

            }


            for (int re = 0; re < plantnum; re++)
            {
                DataTable oDataTable5 = Utilities.GetTable("select max(packagecode) from UnitsDataMain where  PPID='" + plantid[re] + "'");
                Plant_Packages_Num[re] = int.Parse(oDataTable5.Rows[0][0].ToString());

            }

            for (int re = 0; re < plantnum; re++)
            {
                DataTable oDataTable5 = Utilities.GetTable("select UnitCode from UnitsDataMain where  PPID='" + plantid[re] + "'");
                Plant_unit_Num[re] = oDataTable5.Rows.Count;

            }
          


            string[,] blocks = new string[plantnum, Package_Num];
            string temp = "";

            for (int j = 0; j < plantnum; j++)
            {
                int packcode = 0;
                for (int k = 0; k < Plant_unit_Num[j]; k++)
                {
                    if (packcode < Plant_Packages_Num[j]) packcode++;
                    
                    oDataTable = Utilities.GetTable("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                        plantid[j] + "'" + " and packageCode='" + packcode + "'");

                    int ppid = int.Parse(plantid[j]);

                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {
                        Initial = "C" + (k + 1).ToString();

                        if (Findccplant(plantid[j]))
                        {
                            ppid++;
                        }


                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plantid[j] + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'");
                        int loopcount = 0;
                        foreach (DataRow mn in oDataTable.Rows)
                        {

                            string ptype = mn[0].ToString().Trim();
                            ptype = ptype.ToLower();
                            ptype = ptype.Replace("cc", "c");
                            string[] sp = ptype.Split('c');
                            ptype = sp[0].Trim() + sp[1].Trim();
                            if (ptype.Contains("gas"))
                            {
                                ptype = ptype.Replace("gas", "G");

                            }
                            else
                            {
                                ptype = ptype.Replace("steam", "S");

                            }
                            ptype = ppid + "-" + ptype;

                            blocks[j, k] = ptype;

                            loopcount++;
                            if (loopcount != oDataTable.Rows.Count) k++;
                        }
                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                       plantid[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;
                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plantid[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;
                    }


                



                }
            }
            return blocks;

        }
        private string[,] Getblock002()
        {
            DataTable oDataTable = null;

            int[] Plant_Packages_Num = new int[plantnum];
            int[] Plant_unit_Num = new int[plantnum];


            oDataTable = Utilities.GetTable("select distinct PPID from dbo.PowerPlant");

            string[] plantid = new string[plantnum];

            for (int i = 0; i < plantnum; i++)
            {
                plantid[i] = oDataTable.Rows[i][0].ToString().Trim();
                
            }

            
                for (int re = 0; re < plantnum; re++)
                {
                    DataTable oDataTable5 = Utilities.GetTable("select max(packagecode) from UnitsDataMain where  PPID='" + plantid[re] + "'");
                    Plant_Packages_Num[re] = int.Parse(oDataTable5.Rows[0][0].ToString());

                }

                for (int re = 0; re < plantnum; re++)
                {
                    DataTable oDataTable5 = Utilities.GetTable("select UnitCode from UnitsDataMain where  PPID='" + plantid[re] + "'");
                    Plant_unit_Num[re] = oDataTable5.Rows.Count;

                }
          
           

            string[,] blocks1 = new string[plantnum, Package_Num];
            string temp = "";

            for (int j = 0; j < plantnum; j++)
            {
                int packcode = 0;
                for (int k = 0; k < Plant_unit_Num[j]; k++)
                {
                    if (packcode < Plant_Packages_Num[j]) packcode++;
                    oDataTable = Utilities.GetTable("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                        plantid[j] + "'" + " and packageCode='" + packcode + "'");

                    int ppid = int.Parse(plantid[j]);

                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plantid[j] + "'" + " and packageCode='" + packcode + "'and PackageType='"+packageType+"'");
                        int loopcount=0;
                        foreach (DataRow mn in oDataTable.Rows)
                        {
                            
                            string ptype = mn[0].ToString().Trim();
                            ptype = ptype.ToLower();
                            ptype = ptype.Replace("cc", "c");
                            string[] sp = ptype.Split('c');
                            ptype = sp[0].Trim() + sp[1].Trim();
                            if (ptype.Contains("gas"))
                                ptype = ptype.Replace("gas", "G");
                            else if (ptype.Contains("steam"))
                                ptype = ptype.Replace("steam", "S");

                            blocks1[j, k] = ptype;

                            loopcount++;
                           if(loopcount!=oDataTable.Rows.Count) k++;
                            
                        }


                        
                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plantid[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                        string blockName1 = Initial;

                        blocks1[j, k] = blockName1;

                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plantid[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                        string blockName1 = Initial;

                        blocks1[j, k] = blockName1;

                    }

                  
                }
            }
            return blocks1;



        }


        //private string[,] Getblock002()
        //{
        //    DataTable oDataTable = null;

        //    int[] Plant_Packages_Num = new int[plantnum];

        //    oDataTable = utilities.GetTable("select distinct PPID from dbo.PowerPlant");

        //    string[] plantid = new string[plantnum];

        //    for (int i = 0; i < plantnum; i++)
        //    {
        //        plantid[i] = oDataTable.Rows[i][0].ToString().Trim();

        //    }


        //    for (int re = 0; re < plantnum; re++)
        //    {
        //        DataTable oDataTable5 = utilities.GetTable("select Max(PackageCode) from UnitsDataMain where  PPID='" + plantid[re] + "'");
        //        Plant_Packages_Num[re] = (int)oDataTable5.Rows[0][0];

        //    }




        //    string[,] blocks1 = new string[plantnum, Package_Num];
        //    string temp = "";

        //    for (int j = 0; j < plantnum; j++)
        //    {
        //        for (int k = 0; k < Plant_Packages_Num[j]; k++)
        //        {

        //            oDataTable = utilities.GetTable("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
        //                plantid[j] + "'" + " and packageCode='" + (k + 1).ToString() + "'");

        //            int ppid = int.Parse(plantid[j]);

        //            DataRow myRow = oDataTable.Rows[0];

        //            string packageType = myRow["PackageType"].ToString().Trim();

        //            string Initial = "";
        //            if (packageType == "CC")
        //            {
        //                Initial = "C" + (k + 1).ToString();
        //            }
        //            else if (packageType == "Gas")
        //            {
        //                oDataTable = utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
        //                plantid[j] + "'" + " and packageCode='" + (k + 1).ToString() + "' and packageType ='Gas'");

        //                Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
        //            }
        //            else if (packageType == "Steam")
        //            {
        //                oDataTable = utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
        //                plantid[j] + "'" + " and packageCode='" + (k + 1).ToString() + "' and packageType ='Steam'");

        //                Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
        //            }

        //            string blockName1 = Initial;

        //            blocks1[j, k] = blockName1;

        //        }
        //    }
        //    return blocks1;



        //}
        public bool Findccplant(string ppid)
        {
            int tr = 0;

            DataTable FindccplantTable = Utilities.GetTable("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < FindccplantTable.Rows.Count; i++)
            {

                if (FindccplantTable.Rows.Count > 1 && FindccplantTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            predate= statusdatePicker.Text;
            
            if (treeView1.SelectedNode != null)
            {
                FillGrid(e.Node.Text);
               
            }

            if(e.Node.Text!="Date")
            statusdatePicker.Text = e.Node.Text;
           
        }

  

     
       

        
    }
}
