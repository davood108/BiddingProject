﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using System.IO;
using Microsoft.Win32;

namespace PowerPlantProject
{
    public partial class SensetivityAnalysis : Form
    {
        public int nday;
      
        List<CMatrix> result;

        int rnum;

        public SensetivityAnalysis()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                dataGridView2.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////grid////////////////////////////////////////

                dataGridView1.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                dataGridView1.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;
                dataGridView2.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                dataGridView2.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;
            }
            catch
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
              string  fileName="out6";

            
              try
              {
                  nday = int.Parse(txtnday.Text.Trim());

                  Sensitive.Sensitiveclass oCore_pf = new Sensitive.Sensitiveclass();
                                    
                  DataTable dt = Utilities.GetTable("select distinct PPID from dbo.UnitsDataMain");
                  string[] parray = new string[dt.Rows.Count];
                  for (int y = 0; y < dt.Rows.Count; y++)
                  {
                      parray[y] = dt.Rows[y][0].ToString().Trim();

                  }
                 

                  //------------------------start-----------------------------------//

                  for (int j = 1; j <= 24; j++)
                  {

                      CMatrix P = produce("Hour" + j);
                      CMatrix x = manategh("Hour" + j);


                      x = x.AddColumns(interchange("Hour" + j));


                      double[,] pend = new double[nday, dt.Rows.Count];
                      double[,] xend = new double[nday, x.Cols];

                      for (int k = 0; k < nday; k++)
                      {
                          for (int n = 0; n < dt.Rows.Count; n++)
                          {
                              pend[k, n] = P[k, n];


                          }
                      }
                      for (int t = 0; t < nday; t++)
                      {
                          for (int n1 = 0; n1 < x.Cols; n1++)
                          {
                              xend[t, n1] = x[t, n1];
                          }
                      }

                      MWArray[] argsOut = oCore_pf.Sensitivity_Analysis(0, (MWArray)DoubleArrayToMWNumericArray(xend), (MWArray)DoubleArrayToMWNumericArray(pend));



                      if (j == 1)
                      {
                          result = new List<CMatrix>();
                          for (int i = 0; i < 24; i++)
                              result.Add(new CMatrix(x.Cols, dt.Rows.Count));
                      }

                      rnum = x.Cols;
                      //ReadExcelOut("out6", x.Cols, dt.Rows.Count, (j - 1));

                      if (fileName != "" && File.Exists(fileName + ".xls"))
                      {
                          string strConnectionString = string.Empty;

                          strConnectionString = getExcelConnectionString(fileName + ".xls");

                          OleDbConnection oleConn = new OleDbConnection(strConnectionString);
                          OleDbDataAdapter oleCommand = new OleDbDataAdapter("SELECT * from [Sheet1$]", oleConn);
                          DataTable dtt = new DataTable();

                          oleCommand.Fill(dtt);



                          //if (dtt.Rows.Count != x.Cols || dtt.Columns.Count != dt.Rows.Count)
                          //{
                          //    MessageBox.Show("Excel Data Is Invalid");
                          //}
                          //else
                          //{
                              for (int i = 0; i < dtt.Rows.Count; i++)
                              {
                                  for (int jj = 0; jj < dtt.Columns.Count; jj++)
                                      result[(j - 1)][i, jj] = MyDoubleParse(dtt.Rows[i][jj].ToString());
                              }
                         // }

                      }
                  }
                  DataTable del = Utilities.GetTable("Delete from SenseAnalysis where Date='" + datePickerCurrent.Text.Trim() + "'");

                  for (int p = 0; p < dt.Rows.Count; p++)
                  {
                      for (int h1 = 0; h1 < 24; h1++)
                      {
                          for (int r = 0; r < rnum; r++)
                          {

                              DataTable insert = Utilities.GetTable("insert into SenseAnalysis (PPID,Hour,Value,Date,RowIndex) values('" + parray[p] + "','" + (h1 + 1) + "','" + result[h1][r, p] + "','" + datePickerCurrent.Text.Trim() + "','" + r + "')");
                          }
                      }
                  }
                  fillgrid(datePickerCurrent.Text,cmbPlant.Text.Trim());
              }
              catch
              {

                  MessageBox.Show("Please Insert Integer Value In Training Days");
              }


           
        }
        private double[,] MWNumericArrayToDoubleArray(MWNumericArray mwNumericArray, int row, int col)
        {
            double[,] doubleArray = new double[row, col];

            for (int i = 0; i < row; i++)
                for (int j = 0; j < col; j++)
                    doubleArray[i, j] = mwNumericArray[i + 1, j + 1].ToScalarDouble();

            return doubleArray;
        }
        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private MWNumericArray DoubleArrayToMWNumericArray(double[,] doubleArray)
        {
            int noX = doubleArray.GetLength(0);
            int noY = doubleArray.GetLength(1);

            MWNumericArray mwNumericArray = new MWNumericArray(MWArrayComplexity.Real, MWNumericType.Double, noX, noY);

            for (int i = 0; i < noX; i++)
                for (int j = 0; j < noY; j++)
                    mwNumericArray[i + 1, j + 1] = doubleArray[i, j];

            return mwNumericArray;
        }

        private void SensetivityAnalysis_Load(object sender, EventArgs e)
        {
           
          
            DataTable dt = Utilities.GetTable("select distinct PPName,PPID from PowerPlant");
             string p1=dt.Rows[0][0].ToString().Trim();
            
            foreach (DataRow mrow in dt.Rows)
            {

                cmbPlant.Items.Add(mrow[0].ToString().Trim());
            }

            if(cmbPlant.Text=="")
            {

                cmbPlant.Text = p1;
            }
            datePickerCurrent.SelectedDateTime = DateTime.Now;

            DataTable dmax = Utilities.GetTable("SELECT MAX(Date) FROM dbo.SenseAnalysis");
            if (dmax.Rows.Count > 0)
            {
                datePickerCurrent.Text = dmax.Rows[0][0].ToString().Trim();
                fillgrid(dmax.Rows[0][0].ToString().Trim(), p1);
               
            }
        }


        private CMatrix produce(string hour)
        {
            DataTable dt = Utilities.GetTable("select distinct PPID from dbo.UnitsDataMain");

           CMatrix Produced = new CMatrix(nday , dt.Rows.Count);

          
            int j = 0;
         
            foreach (DataRow mrow in dt.Rows)
            {
                DateTime pdate = PersianDateConverter.ToGregorianDateTime(datePickerCurrent.Text);

                for (int i = 0; i < nday; i++)
                {
                   pdate =pdate.Subtract(new TimeSpan(1, 0, 0, 0));
                   DataTable data = Utilities.GetTable("select "+hour+" from dbo.ProducedEnergy where PPCode='" + mrow[0].ToString().Trim() + "' and Date='" + new PersianDate(pdate).ToString("d") + "'");
                   if (data.Rows.Count > 0)
                   {
                       if (data.Rows.Count == 1)
                       {

                           Produced[i, j] = MyDoubleParse(data.Rows[0][0].ToString());

                           
                       }
                       else
                       {
                          data = Utilities.GetTable("select sum("+hour+") from dbo.ProducedEnergy  where PPCode='" + mrow[0].ToString().Trim() + "' and Date='" + new PersianDate(pdate).ToString("d") + "'");


                          Produced[i, j] = MyDoubleParse(data.Rows[0][0].ToString());

                         


                       }

                   }
                   else
                   {
                       data = Utilities.GetTable("select "+hour+" from dbo.ProducedEnergy where PPCode='" + mrow[0].ToString().Trim() + "' and Date='"+
                           FindNearProduceDate(new PersianDate(pdate).ToString("d"))+ "'");
                       if (data.Rows.Count > 0)
                       {
                           if (data.Rows.Count == 1)
                           {

                               Produced[i, j] = MyDoubleParse(data.Rows[0][0].ToString());

                               
                           }
                           else
                           {
                               data = Utilities.GetTable("select sum(" + hour + ") from dbo.ProducedEnergy  where PPCode='" + mrow[0].ToString().Trim() + "' and Date='" + new PersianDate(pdate).ToString("d") + "'");


                               Produced[i, j] = MyDoubleParse(data.Rows[0][0].ToString());

                              


                           }
                       }

                   }

                }


                j++;
            }
            return Produced;
           
        }

        private CMatrix manategh(string hour)
        {


            //--------------------------manatehg-----------------------------------------//
                                
            CMatrix manategh = new CMatrix(nday,16);

            DateTime pdate = PersianDateConverter.ToGregorianDateTime(datePickerCurrent.Text);

             for (int i = 0; i < nday; i++)
             {
                 pdate = pdate.Subtract(new TimeSpan(1, 0, 0, 0));

                 DataTable dt = Utilities.GetTable("select "+hour+" from Manategh where date ='" + new PersianDate(pdate).ToString("d") + "'and Title= N'توليد بدون صنايع' order by Code");
                 if (dt.Rows.Count > 0)
                 {
                     int r = 0;
                     foreach (DataRow mrow in dt.Rows)
                     {

                         manategh[i, r] = MyDoubleParse(mrow[0].ToString());
                         r++;

                     }
                 }
                 else
                 {
                     
                      dt = Utilities.GetTable("select " + hour + " from Manategh where date ='" +FindNearManategh(new PersianDate(pdate).ToString("d")) + "'and Title= N'توليد بدون صنايع' order by Code");
                    
                     if (dt.Rows.Count > 0)
                     {
                         int r = 0;
                         foreach (DataRow mrow in dt.Rows)
                         {

                             manategh[i, r] = MyDoubleParse(mrow[0].ToString());
                             r++;
                         }
                     }




                 }


             }

            
             return manategh;

        }
        private CMatrix interchange(string hour)
        {
           
            int lineTypesNo = Enum.GetNames(typeof(Line_codes)).Length;
            CMatrix interchange = new CMatrix(nday, lineTypesNo);

            for (int index = 1; index <= lineTypesNo; index++)
            {
               
                DateTime pdate = PersianDateConverter.ToGregorianDateTime(datePickerCurrent.Text);

                for (int i = 0; i < nday; i++)
                {
                    Line_codes line = (Line_codes)Enum.Parse(typeof(Line_codes), index.ToString());
                    pdate = pdate.Subtract(new TimeSpan(1, 0, 0, 0));
                    DataTable dt = Utilities.GetTable("select "+hour+" from dbo.InterchangedEnergy where date='" + new PersianDate(pdate).ToString("d") + "' and Code='" + line.ToString().Trim() + "'");
                    if (dt.Rows.Count > 0)
                    {

                        interchange[i, (index - 1)] = MyDoubleParse(dt.Rows[0][0].ToString());

                    }
                    else
                    {
                         dt = Utilities.GetTable("select " + hour + " from dbo.InterchangedEnergy where date='" + FindNearInterchange(new PersianDate(pdate).ToString("d")) + "' and Code='" + line.ToString().Trim() + "'");
                        if (dt.Rows.Count > 0)
                        {

                            interchange[i, (index - 1)] = MyDoubleParse(dt.Rows[0][0].ToString());

                        }

                    }

                   
                }
            }
            return interchange;
        }

        private string FindNearProduceDate(string date)
        {
            DataTable odat = Utilities.GetTable("Select distinct Date from dbo.ProducedEnergy where  Date<='" + date + "'order by Date desc");
            return odat.Rows[0][0].ToString().Trim();

        }
        private string FindNearRegionDate(string date)
        {
            DataTable odat = Utilities.GetTable("Select distinct Date from dbo.RegionNetComp where Date<='" + date + "'order by Date desc");
            return odat.Rows[0][0].ToString().Trim();

        }
        private string FindNearManategh(string date)
        {
            DataTable odat = Utilities.GetTable("Select distinct Date from Manategh where Date<='" + date + "' order by Date desc");
            return odat.Rows[0][0].ToString().Trim();
        }
        private string FindNearInterchange(string date)
        {

            DataTable odat = Utilities.GetTable("Select distinct Date FROM [InterchangedEnergy] WHERE Date<='" + date + "'AND Code!='' ORDER BY Date desc ");
            if (odat.Rows.Count > 0)
            {
                return odat.Rows[0][0].ToString().Trim();
            }
        
            return null;
        }
        private List<CMatrix> ReadExcelOut(string fileName, int rowNum, int colNum, int hour)
        {
          
            List<CMatrix> result = new List<CMatrix>();
            for (int i = 0; i < 24; i++)
                result.Add(new CMatrix(rowNum, colNum)); 



            if (fileName != "" && File.Exists(fileName + ".xls"))
            {
                string strConnectionString = string.Empty;
                
                strConnectionString = getExcelConnectionString(fileName + ".xls");

                OleDbConnection oleConn = new OleDbConnection(strConnectionString);
                OleDbDataAdapter oleCommand = new OleDbDataAdapter("SELECT * from [Sheet1$]", oleConn);
                DataTable dt = new DataTable();

                oleCommand.Fill(dt);

                ////// Delets empty rows
                //int count = dt.Rows.Count;
                //for (int i = count - 1; i >= 0; i--)
                //    if (dt.Rows[i][0].ToString() == "" && dt.Rows[i][1].ToString() == "")
                //        dt.Rows[i].Delete();
                //dt.AcceptChanges();
                ////// end of Deletion

                if (dt.Rows.Count != rowNum || dt.Columns.Count != colNum)
                {
                    MessageBox.Show("");
                }
                else
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        for (int j = 0; j < dt.Columns.Count; j++)
                            result[hour][i, j] = MyDoubleParse(dt.Rows[i][j].ToString());
                    }
                }

            }
            return result;
        }
        private string getExcelConnectionString(string fileName)
        {
            string _excelPath = GetComponentPath();
            if (GetMajorVersion(_excelPath) == 11)//2003
                return @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                     "Data Source=" + fileName + ";" +
                     "Extended Properties='Excel 8.0;HDR=No'";
            else if (GetMajorVersion(_excelPath) == 12)//2007
                return @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName +
                        ";Extended Properties='Excel 12.0;HDR=No'";
            else
                return "";


        }

        private string GetComponentPath()
        {
            string RegKey = @"Software\Microsoft\Windows\CurrentVersion\App Paths";

            string toReturn = string.Empty;
            string _key = "excel.exe";

            //looks inside CURRENT_USER:
            RegistryKey _mainKey = Registry.CurrentUser;
            try
            {
                _mainKey = _mainKey.OpenSubKey(RegKey + "\\" + _key, false);
                if (_mainKey != null)
                {
                    toReturn = _mainKey.GetValue(string.Empty).ToString();
                }
            }
            catch
            { }

            //if not found, looks inside LOCAL_MACHINE:
            _mainKey = Registry.LocalMachine;
            if (string.IsNullOrEmpty(toReturn))
            {
                try
                {
                    _mainKey = _mainKey.OpenSubKey(RegKey + "\\" + _key, false);
                    if (_mainKey != null)
                    {
                        toReturn = _mainKey.GetValue(string.Empty).ToString();
                    }
                }
                catch
                {
                }
            }

            //closing the handle:
            if (_mainKey != null)
                _mainKey.Close();

            return toReturn;
        }
        private int GetMajorVersion(string _path)
        {
            int toReturn = 0;
            if (File.Exists(_path))
            {
                try
                {
                    FileVersionInfo _fileVersion = FileVersionInfo.GetVersionInfo(_path);
                    toReturn = _fileVersion.FileMajorPart;
                }
                catch
                { }
            }

            return toReturn;
        }
        private void fillgrid(string date, string  Pname)
        {

            dataGridView1.DataSource = null;
            dataGridView2.DataSource = null;
            dataGridView2.Rows.Clear();
            dataGridView1.Rows.Clear();


            DataTable DTPID = Utilities.GetTable("select PPID from PowerPlant where PPName='" + Pname + "'");
            DataTable DTnum = Utilities.GetTable("select PPID from PowerPlant");
            DataTable dtreg = Utilities.GetTable("select distinct Code from dbo.Manategh order by Code");
            string[] regname = new string[dtreg.Rows.Count];
            for (int i = 0; i < dtreg.Rows.Count; i++)
            {
               DataTable  dtnamereg = Utilities.GetTable("select distinct Name from dbo.Manategh where Code='"+dtreg.Rows[i][0].ToString().Trim()+"'");
               regname[i] = dtnamereg.Rows[0][0].ToString().Trim();

            }

            int plantnum = DTnum.Rows.Count;
            int reg = 16;
            int lineTypesNo = Enum.GetNames(typeof(Line_codes)).Length;

            DataTable dt22 = Utilities.GetTable("select * from SenseAnalysis where Date='" + date + "' and PPID ='" + DTPID.Rows[0][0].ToString().Trim() + "'");
            if (dt22.Rows.Count > 0)
            {
                btnprint.Enabled = true;

            }
            else
            {
                btnprint.Enabled = false;
            }

            DataTable dt = Utilities.GetTable("select * from SenseAnalysis where Date='" + date + "' and PPID ='" + DTPID.Rows[0][0].ToString().Trim() + "' and Hour='" + "1" + "'");
            if (dt.Rows.Count > 0)
            {
                dataGridView1.RowCount = dt.Rows.Count;
                dataGridView1.ColumnCount = 13;
                dataGridView2.RowCount = dt.Rows.Count;
                dataGridView2.ColumnCount = 13;
            }

         
           
                for (int h = 1; h < 14; h++)
                {

                    dt = Utilities.GetTable("select * from SenseAnalysis where Date='" + date + "' and PPID ='" + DTPID.Rows[0][0].ToString().Trim() + "' and Hour='" + h + "'order by RowIndex");
                  
                    int i=0;
                    int finali = 0;
                    foreach(DataRow mr in dt.Rows)
                    {
                        if (h != 13)
                        {
                           
                           
                            if(i<reg)
                            {


                                if (regname[i] == "خوزستان" || regname[i] == "يزد" || regname[i] == "كرمان" || regname[i] == "سيستان و بلوچستان" || regname[i] == "هرمزگان")
                                {
                                    //----------5 row deleted //---------------------------------------------------------------

                                }
                                else
                                {
                                    dataGridView1.Rows[finali].Cells[0].Value = regname[i];
                                    dataGridView1.Rows[finali].Cells[h].Value = mr["Value"].ToString();
                                    finali++;
                                   
                                }

                            }
                            if (i>=reg)
                            {

                                Line_codes line = (Line_codes)Enum.Parse(typeof(Line_codes), (i- (reg)+1).ToString());
                                    dataGridView1.Rows[i-5].Cells[0].Value = line.ToString();
                                    dataGridView1.Rows[i-5].Cells[h].Value = mr["Value"].ToString();
                                    
                            }

                            i++;
                        }
                    }


                   
                }


                for (int h = 13; h <= 24; h++)
                {

                    dt = Utilities.GetTable("select * from SenseAnalysis where Date='" + date + "' and PPID ='" + DTPID.Rows[0][0].ToString().Trim() + "' and Hour='" + h + "'order by RowIndex");

                    int i = 0;
                    int finali = 0;
                    foreach (DataRow mr in dt.Rows)
                    {
                        if (h >=13)
                        {
                           

                            if (i < reg)
                            {



                                if (regname[i] == "خوزستان" || regname[i] == "يزد" || regname[i] == "كرمان" || regname[i] == "سيستان و بلوچستان" || regname[i] == "هرمزگان")
                                {
                                    //----------5 row deleted //---------------------------------------------------------------

                                }
                                else
                                {

                                    dataGridView2.Rows[finali].Cells[0].Value = regname[i];
                                    dataGridView2.Rows[finali].Cells[((h - 13) + 1)].Value = mr["Value"].ToString();

                                    finali++;
                                }

                              

                            }
                            if (i >= reg)
                            {

                                Line_codes line = (Line_codes)Enum.Parse(typeof(Line_codes), (i - (reg) + 1).ToString());
                                dataGridView2.Rows[i-5].Cells[0].Value = line.ToString();
                                dataGridView2.Rows[i-5].Cells[((h - 13) + 1)].Value = mr["Value"].ToString();
                               

                            }
                            i++;
                          
                        }
                    }
                   

                }

            //remove end of rows //-----------------------------------------------

                if (dt.Rows.Count>0)
                {
                    for (int i = (dt.Rows.Count - 2); i > dt.Rows.Count - 6; i--)
                    {
                        dataGridView1.Rows.Remove(dataGridView1.Rows[i]);
                      
                    }

                    for (int i = (dt.Rows.Count - 2); i > dt.Rows.Count - 6; i--)
                    {
                        dataGridView2.Rows.Remove(dataGridView2.Rows[i]);

                    }
                }
        }

        private void datePickerCurrent_ValueChanged(object sender, EventArgs e)
        {
            if (datePickerCurrent.Text == "[Empty Value]" || datePickerCurrent.Text == "")
                datePickerCurrent.Text = new PersianDate(DateTime.Now).ToString("d");

           
                fillgrid(datePickerCurrent.Text,cmbPlant.Text.Trim());
           
        }

        private void cmbPlant_TextChanged(object sender, EventArgs e)
        {

            if (cmbPlant.Text != "" )
            {
                if (datePickerCurrent.Text =="" || datePickerCurrent.Text == "[Empty Value]")
                    datePickerCurrent.Text = new PersianDate(DateTime.Now).ToString("d");

                fillgrid(datePickerCurrent.Text, cmbPlant.Text.Trim());
            }
        }

        private void btnprint_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Item");
                for (int i = 0; i < 24; i++)
                {
                    dt.Columns.Add((i + 1).ToString());
                }
                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    dt.Rows.Add(dataGridView1.Rows[i].Cells[0].Value.ToString());
                }


                for (int h = 0; h < dataGridView1.Rows.Count - 1; h++)
                {
                    for (int j = 1; j <= 24; j++)
                    {
                        if (j <= 12)
                        {
                            if (dataGridView1.Rows[h].Cells[j].Value.ToString().Length > 6)
                            {
                                dt.Rows[h][j] = dataGridView1.Rows[h].Cells[j].Value.ToString().Substring(0, 5);
                            }
                            else
                                dt.Rows[h][j] = dataGridView1.Rows[h].Cells[j].Value.ToString();
                        }
                        else
                        {
                            if (dataGridView2.Rows[h].Cells[(j - 13) + 1].Value.ToString().Length > 6)
                            {

                                dt.Rows[h][j] = dataGridView2.Rows[h].Cells[(j - 13) + 1].Value.ToString().Substring(0, 5);
                            }
                            else
                                dt.Rows[h][j] = dataGridView2.Rows[h].Cells[(j - 13) + 1].Value.ToString();
                        }
                    }

                }



                SensetivityPrint m = new SensetivityPrint(dt, cmbPlant.Text.Trim(), datePickerCurrent.Text.Trim());
                m.Show();
            }
            catch
            {

            }

        }


    }
}
