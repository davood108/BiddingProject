﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using NRI.SBS.Common;

namespace PowerPlantProject
{

    public partial class AddUnitForm : Form
    {
        public string result;
        private string Num;
        public AddUnitForm(string ppid)
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
            PackageTypeValid.Visible = false;

            Num = ppid;
            DataSet dataDS = new DataSet();
            SqlDataAdapter dataDA = new SqlDataAdapter();
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            //dataDA.SelectCommand = new SqlCommand("SELECT PackageType FROM PPUnit WHERE PPID=" + Num, myConnection);
            //dataDA.Fill(dataDS);
            //foreach (DataRow MyRow in dataDS.Tables[0].Rows)
            //{
            //    PackageType.Items.Add(MyRow[0]);
            //}
        }

        private void ODSaveBtn_Click(object sender, EventArgs e)
        {
            bool save=false;
            if ((unitNumTb.Text != "") && (PackageCodeTb.Text != ""))
            {
                save = true;
                if ((PackageType.SelectedItem == null) || (UnitType.SelectedItem == null))
                {
                    //PackageTypeValid.Visible = true;
                    MessageBox.Show("Please Fill the blank!");
                    save = false;
                }
                else
                {
                    if (!PackageType.SelectedItem.ToString().Contains("Combined"))
                        if (PackageType.SelectedItem.ToString().Trim() != UnitType.SelectedItem.ToString().Trim())
                        {
                            MessageBox.Show("Unmatched Package and Unit Type!");
                            save = false;
                        }
                }
            }
            else MessageBox.Show("Please Fill The Blank(s)!");
            if (save)
            {
                    string UnitName = "";
                    if (PackageType.SelectedItem.ToString().Contains("Combined")) UnitName = UnitType.SelectedItem.ToString().Trim() + " cc";
                    else UnitName = PackageType.SelectedItem.ToString().Trim();
                    UnitName += unitNumTb.Text.Trim();
                    result = UnitName + "," + UnitType.SelectedItem.ToString() + "," + PackageCodeTb.Text + "," + PackageType.SelectedItem.ToString();
                    this.DialogResult = DialogResult.OK;
                    this.Close();

            }
        }

        private void unitNameTb_Validated(object sender, EventArgs e)
        {
            try
            {
                int x = int.Parse(unitNumTb.Text);
                errorProvider1.SetError(unitNumTb, "");
            }
            catch
            {
                errorProvider1.SetError(unitNumTb, "Invalid value!");
            }
        }

        private void PackageCodeTb_Validated(object sender, EventArgs e)
        {
            try
            {
                int x = int.Parse(PackageCodeTb.Text);
                errorProvider1.SetError(PackageCodeTb, "");
            }
            catch
            {
                errorProvider1.SetError(PackageCodeTb, "Invalid value!");
            }
        }

        private void unitNumTb_TextChanged(object sender, EventArgs e)
        {
            string package=" ";
            if (PackageType.Text.Contains("Combined")) package = " cc";
            UnitNameTb.Text = UnitType.Text + package + unitNumTb.Text;
        }

        private void UnitType_TextChanged(object sender, EventArgs e)
        {
            string package = " ";
            if (PackageType.Text.Contains("Combined")) package = " cc";
            UnitNameTb.Text = UnitType.Text + package + unitNumTb.Text;
        }

        private void PackageType_TextChanged(object sender, EventArgs e)
        {
            string package = " ";
            if (PackageType.Text.Contains("Combined")) package = " cc";
            UnitNameTb.Text = UnitType.Text + package + unitNumTb.Text;
        }

        private void AddUnitForm_Load(object sender, EventArgs e)
        {
            label5.Text = "";
            DataTable namedata = Utilities.GetTable("select distinct PPName from dbo.PowerPlant where PPID='" + Num + "'");
            label5.Text = "Exist Units  in  " + namedata.Rows[0][0].ToString().Trim() + "  Plant  Are:";
            txtStatusexist.Text = "";
            DataTable odata = Utilities.GetTable("select UnitCode,UnitType,PackageType,PackageCode from dbo.UnitsDataMain where PPID='" + Num + "'order by PackageCode asc");
            foreach (DataRow myrow in odata.Rows)
            {
                txtStatusexist.Text += "  UnitName =   " + myrow[0].ToString().Trim() + "   UnitType =   " + myrow[1].ToString().Trim() + "   PackageType =   " + myrow[2].ToString().Trim() + "   PackageCode =   " + myrow[3].ToString().Trim() + "\r\n\r\n";
            }

        }


    }
}
