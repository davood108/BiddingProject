﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NRI.SBS.Common;
namespace PowerPlantProject
{
    public partial class FinanCialPrint : Form
    {

        DataTable Dg1 = null;
        DataTable DG2 = null;
        string pname;
        string Date;
        string PPID;

        public FinanCialPrint(DataTable dg1, string DATE,string ppid)
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dgv.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;

                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }
                dgv.DefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                dgv.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;
                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
            Dg1 = dg1;
            Date = DATE;
            PPID = ppid;
            DG2 = dg1;
        }

        private void FinanCialPrint_Load(object sender, EventArgs e)
        {
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            dgv.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    
        
            dgv.AllowUserToDeleteRows = false;
            label1.Text = "Financial Plant Report for A week Before Date :  " + Date +" For Plant : "+PPID;

            dgv.DataSource = Dg1;
           // dgv.Columns[0].Width = 19;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            dgv.DataSource = Dg1;
            if (chkland.Checked)
            {
                PrintDGV.landscape = true;
            }
            PrintDGV.info(label1.Text);
            PrintDGV.Print_DataGridView(dgv);

        }

        private void rdunit_CheckedChanged(object sender, EventArgs e)
        {
            //if (rdunit.Checked)
            //{
            //    pname = "Units Report";
            //    Dg1 = utilities.GetTable("SELECT  UnitCode,AvailableCapacity,TotalPower,ULPower,CapacityPayment," +
            //    "EnergyPayment,Income FROM [EconomicUnit] WHERE  Date='" + Date + "' AND PPID=" + PPID);

            //    DataTable dt = utilities.GetTable("SELECT Date,ppid, as PPID, UnitCode AS UnitCode,Benefit as 'سود خالص نيروگاه' ,Income as 'درآمد خالص نيروگاه',Cost as 'هزينه سوخت نيروگاه',AvailableCapacity as 'ميزان آمادگي-مگاوات ساعت',TotalPower as 'ميزان توليد ناخالص-مگاوات ساعت',BidPower as 'ميزان خالص پذيرفته شده باقيمت پيشنهادي فروشنده-مگاوات ساعت'," +
            //     "ULPower as 'ميزان خالص پذيرفته شده با نرخ UL-مگاوات ساعت',IncrementPower as 'ميزان  خالص توليدبيش ازپيشنهادبازارو به دستور مرکز-مگاوات ساعت',DecreasePower as 'ميزان توليدکمترازپيشنهاد بازارو به دستور مرکز-مگاوات ساعت',(Income/TotalPower)AS 'PerMw'   FROM [EconomicPlant] WHERE Date>='" + frdate7 + "' AND date<='" + frdate1 + "'and  PPID='" + PPID + "'order by date asc");




            //    dgv.DataSource = null;
            //    dgv.DataSource = Dg1;
            //    label1.Text = pname + " In Date :  " + Date;
            //}
            //else
            //{
            //    Dg1 = DG2;
            //    dgv.DataSource = null;
            //    dgv.DataSource = Dg1;
            //    pname = "Plant Report";
            //    label1.Text = pname + " In Date :  " + Date;
            //}
        }

      

       
    }
}
