﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using System.IO;
using Microsoft.Win32;

namespace PowerPlantProject
{
    public partial class FuelSetting : Form
    {
        public FuelSetting()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dataGridView2.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void FuelSetting_Load(object sender, EventArgs e)
        {
            try
            {
                datePickerFrom.Text = new PersianDate(DateTime.Now).ToString("d");
                datePickerto.Text = new PersianDate(DateTime.Now.AddDays(1)).ToString("d");
                dataGridView2.DataSource = null;
                DataTable cf = Utilities.GetTable("select * from BillFuel");
                dataGridView2.DataSource = cf;
                DataTable c = Utilities.GetTable("select ppid from powerplant");
                cmbplant.Text = c.Rows[0][0].ToString();
                foreach (DataRow n in c.Rows)
                {
                    cmbplant.Items.Add(n[0].ToString().Trim());

                }
            }
            catch
            {

            }
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            TimeSpan span = PersianDateConverter.ToGregorianDateTime(datePickerto.Text) - PersianDateConverter.ToGregorianDateTime(datePickerFrom.Text);
            int dday = span.Days;
            if (dday < 0)
            {
                MessageBox.Show("Please Select Correct Interval Date!");
                return;
            }
            try
            {
                DataTable del = Utilities.GetTable("delete from billfuel where ppid='" + cmbplant.Text + "'and fromdate ='" + datePickerFrom.Text + "'and todate='" + datePickerto.Text + "'");
                DataTable ins = Utilities.GetTable("insert into billfuel (ppid,fromdate,todate,avc,fueltype)values('" + cmbplant.Text + "','" + datePickerFrom.Text + "','" + datePickerto.Text + "','" + textBox1.Text + "','" + comboBox1.Text + "')");

                dataGridView2.DataSource = null;
                DataTable cf = Utilities.GetTable("select * from BillFuel");
                dataGridView2.DataSource = cf;
            }
            catch
            {

            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                int rowIndex = e.RowIndex;

                string x = dataGridView2.Rows[rowIndex].Cells[e.ColumnIndex].Value.ToString();
                if (x == "System.Drawing.Bitmap")
                {
                    DataTable bb = Utilities.GetTable("delete from billfuel where ppid='" + cmbplant.Text.Trim() + "' and  fromdate='" + dataGridView2.Rows[rowIndex].Cells[2].Value.ToString() + "'and todate='" + dataGridView2.Rows[rowIndex].Cells[3].Value.ToString() + "'");
                }
                dataGridView2.DataSource = null;
                DataTable cf = Utilities.GetTable("select * from BillFuel");
                dataGridView2.DataSource = cf;
            }
            catch
            {
            }
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            this.Close();
           Bourse  m = new Bourse();
            m.panel1.Visible = true;
            m.Show();
        }

  

       
    }
}
