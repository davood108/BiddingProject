﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using NRI.DMS.Common;

namespace PowerPlantProject
{
    public partial class Form3 : Form
    {
        //Form2 secondpage;
        //public Form2 Secondpage
        //{
        //    get { return secondpage; }
        //    set { secondpage = value; }
        //}
        //public Form3(Form2 frm)
        //{
        //    secondpage = frm;
        //    InitializeComponent();
        //    ConStr = System.Configuration.ConfigurationManager.ConnectionStrings["PowerPlantProject"].ConnectionString;
        //    PackageTypeValid.Visible = false;
        //}

        public Form3()
        {
            InitializeComponent();
            //ConStr = System.Configuration.ConfigurationManager.ConnectionStrings["PowerPlantProject"].ConnectionString;
            PackageTypeValid.Visible = false;
        }

        private void ODSaveBtn_Click(object sender, EventArgs e)
        {
            if ((PlanNameTb.Text != "") && (PlantCodeTb.Text != ""))
            {
                if ((PackageType1.SelectedItem == null) && (PackageType2.SelectedItem == null))
                {
                    PackageTypeValid.Visible = true;
                    MessageBox.Show("You Must Detect Type Of Plant's Packege(s)");
                }
                else
                {
                    bool check = true;
                    PackageTypeValid.Visible = false;
                    SqlConnection MyConnection = new SqlConnection(ConnectionManager.ConnectionString);
                    MyConnection.Open();


                    SqlCommand MyCom = new SqlCommand();
                    MyCom.CommandText = "INSERT INTO PowerPlant (PPID,PPName) VALUES (@num,@name)";
                    MyCom.Connection = MyConnection;
                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                    MyCom.Parameters["@num"].Value = PlantCodeTb.Text;
                    MyCom.Parameters.Add("@name", SqlDbType.NChar, 20);
                    MyCom.Parameters["@name"].Value = PlanNameTb.Text;
                    try
                    {
                        MyCom.ExecuteNonQuery();
                    }
                    catch
                    {
                        MessageBox.Show("This PlantCode Exists Now!");
                        check = false;
                    }
                    if (check)
                    {
                        if (PackageType1.SelectedItem != null)
                        {
                            MyCom.CommandText = "INSERT INTO PPUnit (PPID,PackageType) VALUES (@num,@package)";
                            MyCom.Parameters.Add("@package", SqlDbType.NChar, 20);
                            MyCom.Parameters["@package"].Value = PackageType1.SelectedItem.ToString();
                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception er)
                            {
                                string error = er.Message;
                            }
                        }
                        if ((PackageType2.SelectedItem != null) && (PackageType1.SelectedItem.ToString() != PackageType2.SelectedItem.ToString()))
                        {
                            MyCom.CommandText = "INSERT INTO PPUnit (PPID,PackageType) VALUES (@num,@package)";
                            MyCom.Parameters.Add("@package", SqlDbType.NChar, 20);
                            MyCom.Parameters["@package"].Value = PackageType2.SelectedItem.ToString();
                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception er)
                            {
                                string error = er.Message;
                            }
                        }
                        this.DialogResult = DialogResult.OK;
                        MyConnection.Close();
                        this.Close();
                    }
                    MyConnection.Close();
                }
            }
            else MessageBox.Show("Please Fill The Blank(s)!");
        }

        private void Form3_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

        private void PlanNameTb_Validated(object sender, EventArgs e)
        {
            if (PlanNameTb.Text == "")
                errorProvider1.SetError(PlanNameTb, "Fill the blank");
            else errorProvider1.SetError(PlanNameTb, ""); 
        }

        private void PlantCodeTb_Validated(object sender, EventArgs e)
        {
            if (PlantCodeTb.Text != "")
                errorProvider1.SetError(PlantCodeTb, "");
            else errorProvider1.SetError(PlantCodeTb, "Fill the blank");
        }



    }
}
