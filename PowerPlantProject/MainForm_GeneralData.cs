﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
//using Microsoft.Office.Core;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    partial class MainForm
    {
        //----------------------------------------------GDNewBtn_Click-----------------------------------------------
        private void GDNewBtn_Click(object sender, EventArgs e)
        {
            //We Are in GDPlanttPanel
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            if (GDNewBtn.Text.Contains("Add"))
            {
                //IF a Plant is selected
                if ((PlantGV1.Visible) || (PlantGV2.Visible))
                {
                    AddUnitForm newUnit = new AddUnitForm(PPID);
                    DialogResult re = newUnit.ShowDialog();
                    if (re == DialogResult.OK)
                    {
                        string[] UnitResult = newUnit.result.Split(',');
                        string type = UnitResult[3];

                        DataTable IsThere = null;
                        IsThere = Utilities.GetTable("SELECT COUNT(*) FROM PPUnit WHERE PPID='" + PPID + "' AND PackageType='" + type + "'");
                        if (IsThere.Rows[0][0].ToString() == "0")
                        {
                            SqlCommand MyCom = new SqlCommand();
                            MyCom.Connection = myConnection;
                            MyCom.CommandText = "INSERT INTO PPUnit (PPID,PackageType) Values (@num,@ptype)";
                            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                            MyCom.Parameters["@num"].Value = PPID;
                            MyCom.Parameters.Add("@ptype", SqlDbType.NChar, 20);
                            MyCom.Parameters["@ptype"].Value = type;
                            MyCom.ExecuteNonQuery();
                        }
                        IsThere = null;
                        IsThere = Utilities.GetTable("SELECT COUNT(*) FROM UnitsDataMain WHERE PPID='" + PPID + "' AND UnitCode='" + UnitResult[0] + "'");
                        if (type.Contains("Combined")) type = "CC";
                        if (IsThere.Rows[0][0].ToString() == "0")
                        {
                            SqlCommand MyCom = new SqlCommand();
                            MyCom.CommandText = "INSERT INTO UnitsDataMain (PPID,UnitCode,UnitType,PackageCode,PackageType) VALUES (@num,@ucode,@utype,@pcode,@ptype)";
                            MyCom.Connection = myConnection;
                            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                            MyCom.Parameters["@num"].Value = PPID;
                            MyCom.Parameters.Add("@ptype", SqlDbType.NChar, 10);
                            MyCom.Parameters["@ptype"].Value = type;
                            MyCom.Parameters.Add("@ucode", SqlDbType.NChar, 20);
                            MyCom.Parameters["@ucode"].Value = UnitResult[0];
                            MyCom.Parameters.Add("@utype", SqlDbType.NChar, 8);
                            MyCom.Parameters["@utype"].Value = UnitResult[1];
                            MyCom.Parameters.Add("@pcode", SqlDbType.Int);
                            MyCom.Parameters["@pcode"].Value = int.Parse(UnitResult[2]);
                            try
                            {
                                MyCom.ExecuteNonQuery();

                                ///////////////////////////////
                                MyCom = new SqlCommand();
                                MyCom.CommandText = "INSERT INTO MaintenanceGeneralData (PPID,UnitCode,PackageType) " +
                                    "VALUES (" + PPID + ",'" + UnitResult[0] + "','" + type + "')";
                                MyCom.Connection = myConnection;
                                MyCom.ExecuteNonQuery();

                                /////////////////////
                                //string Mypackage = UnitResult[3];
                                //Mypackage = Mypackage.Trim();
                                //MyCom.CommandText = "SELECT @re=COUNT(PPID) FROM PPUnit WHERE PPID=@num AND PackageType=@packagetype";
                                //MyCom.Parameters["@num"].Value = PPID;
                                //MyCom.Parameters.Add("@packagetype", SqlDbType.NChar, 20);
                                //MyCom.Parameters["@packagetype"].Value = Mypackage;
                                //MyCom.Parameters.Add("@re", SqlDbType.Int);
                                //MyCom.Parameters["@re"].Direction = ParameterDirection.Output;

                                //MyCom.ExecuteNonQuery();

                                //int result1;
                                //result1 = (int)MyCom.Parameters["@re"].Value;
                                //if (result1 == 0)
                                //{
                                //    MyCom.CommandText = "INSERT INTO PPUnit (PPID,PackageType) VALUES (@num,@packagetype)";
                                //    MyCom.Parameters["@num"].Value = PPID;
                                //    MyCom.Parameters["@packagetype"].Value = Mypackage;

                                //    MyCom.ExecuteNonQuery();
                                //}
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                            }
                            SetHeader1Plant();
                            buildPPtree(PPID);
                            //TAB : GENERAL DATA
                            FillPlantGrid(PPID);
                            //TAB :MARKETRESULTS
                            FillMRVlues(PPID);
                        }
                        else MessageBox.Show("This Unit Exists Now!");
                    }

                    //    if (PlantGV1.Visible)
                    //    {
                    //        foreach (DataGridViewRow dr in PlantGV1.Rows)
                    //        {
                    //            if ((dr.Cells[3].Value == null) || (dr.Cells[3].Value.ToString() == ""))
                    //                if ((dr.Cells[0].Value != null) && (dr.Cells[1].Value != null) && (dr.Cells[2].Value != null))
                    //                {
                    //                    SqlCommand MyCom = new SqlCommand();
                    //                    MyCom.CommandText = "INSERT INTO UnitsDataMain (PPID,UnitCode,UnitType,PackageCode,PackageType) VALUES (@num,@ucode,@utype,@pcode,@ptype)";
                    //                    MyCom.Connection = MyConnection;
                    //                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                    //                    MyCom.Parameters["@num"].Value = PPID;
                    //                    MyCom.Parameters.Add("@ucode", SqlDbType.NChar, 20);
                    //                    string ucode = dr.Cells[0].Value.ToString();
                    //                    MyCom.Parameters["@ucode"].Value = ucode;
                    //                    MyCom.Parameters.Add("@utype", SqlDbType.NChar, 8);
                    //                    ucode = dr.Cells[2].Value.ToString();
                    //                    ucode = ucode.ToLower();
                    //                    if (ucode.Contains("steam")) ucode = "Steam";
                    //                    else ucode = "Gas";
                    //                    MyCom.Parameters["@utype"].Value = ucode;
                    //                    MyCom.Parameters.Add("@pcode", SqlDbType.Int);
                    //                    MyCom.Parameters["@pcode"].Value = int.Parse(dr.Cells[1].Value.ToString());
                    //                    MyCom.Parameters.Add("@ptype", SqlDbType.NChar, 10);
                    //                    string type = GDGasGroup.Text;
                    //                    if (type.Contains("Combined")) type = "CC";
                    //                    MyCom.Parameters["@ptype"].Value = type;
                    //                    //try
                    //                    //{
                    //                    MyCom.ExecuteNonQuery();
                    //                    //}
                    //                    //catch (Exception exp)
                    //                    //{
                    //                    //MessageBox.Show("This Unit Exists Now!");
                    //                    //  string str = exp.Message;
                    //                    //}
                    //                }
                    //        }
                    //    }
                    //    if (PlantGV2.Visible)
                    //    {
                    //        foreach (DataGridViewRow dr in PlantGV2.Rows)
                    //        {
                    //            if ((dr.Cells[3].Value == null) || (dr.Cells[3].Value.ToString() == ""))
                    //                if ((dr.Cells[0].Value != null) && (dr.Cells[1].Value != null) && (dr.Cells[2].Value != null))
                    //                {
                    //                    SqlCommand MyCom = new SqlCommand();
                    //                    MyCom.CommandText = "INSERT INTO UnitsDataMain (PPID,UnitCode,UnitType,PackageCode,PackageType) VALUES (@num,@ucode,@utype,@pcode,@ptype)";
                    //                    MyCom.Connection = MyConnection;
                    //                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                    //                    MyCom.Parameters["@num"].Value = PPID;
                    //                    MyCom.Parameters.Add("@ucode", SqlDbType.NChar, 20);
                    //                    string ucode = dr.Cells[0].Value.ToString();
                    //                    MyCom.Parameters["@ucode"].Value = ucode;
                    //                    MyCom.Parameters.Add("@utype", SqlDbType.NChar, 8);
                    //                    ucode = dr.Cells[2].Value.ToString();
                    //                    ucode = ucode.ToLower();
                    //                    if (ucode.Contains("steam")) ucode = "Steam";
                    //                    else ucode = "Gas";
                    //                    MyCom.Parameters["@utype"].Value = ucode;
                    //                    MyCom.Parameters.Add("@pcode", SqlDbType.Int);
                    //                    MyCom.Parameters["@pcode"].Value = int.Parse(dr.Cells[1].Value.ToString());
                    //                    MyCom.Parameters.Add("@ptype", SqlDbType.NChar, 10);
                    //                    string type = GDSteamGroup.Text;
                    //                    if (type.Contains("Combined")) type = "CC";
                    //                    MyCom.Parameters["@ptype"].Value = type;
                    //                    //try
                    //                    //{
                    //                    MyCom.ExecuteNonQuery();
                    //                    //}
                    //                    //catch (Exception exp)
                    //                    //{
                    //                    //MessageBox.Show("This Unit Exists Now!");
                    //                    //  string str = exp.Message;
                    //                    //}
                    //                }
                    //        }
                    //    }
                    //    buildPPtree(PPID.ToString());
                    //    //TAB : GENERAL DATA
                    //    FillPlantGrid(PPID.ToString());
                    //    //TAB :MARKETRESULTS
                    //    FillMRVlues(PPID.ToString());
                }

                //IF A Transmission Line is selected
                if ((Grid230.Visible) || (Grid400.Visible))
                {
                    AddLineForm newLine = new AddLineForm();
                    DialogResult re = newLine.ShowDialog();
                    if (re == DialogResult.OK)
                    {
                        string[] UnitResult = newLine.result.Split(',');
                        DataTable isThere = null;
                        isThere = Utilities.GetTable("SELECT COUNT(*) FROM Lines WHERE LineNumber=" + line + " AND LineCode='" + UnitResult[0] + "'");
                        if (isThere.Rows[0][0].ToString() == "0")
                        {
                            SqlCommand MyCom = new SqlCommand();
                            MyCom.CommandText = "INSERT INTO Lines (LineNumber,LineCode,FromBus,ToBus,LineLength," +
                            "Owner_GencoFrom,Owner_GencoTo,RateA) VALUES (@num,@code,@from,@to,@lentgh,@gencofrom," +
                            "@gencoto,@capacity) INSERT INTO TransLine (LineNumber,LineCode,Name) VALUES (@num,@code,@name) ";
                            MyCom.Connection = myConnection;
                            MyCom.Parameters.Add("@num", SqlDbType.Int);
                            MyCom.Parameters["@num"].Value = line;
                            MyCom.Parameters.Add("@code", SqlDbType.NChar, 20);
                            MyCom.Parameters["@code"].Value = UnitResult[0];
                            MyCom.Parameters.Add("@from", SqlDbType.NVarChar, 50);
                            if (UnitResult[1] != "")
                                MyCom.Parameters["@from"].Value = UnitResult[1];
                            else MyCom.Parameters["@from"].Value = "0";
                            MyCom.Parameters.Add("@to", SqlDbType.NVarChar, 50);
                            if (UnitResult[2] != "")
                                MyCom.Parameters["@to"].Value = UnitResult[2];
                            else MyCom.Parameters["@to"].Value = "0";
                            MyCom.Parameters.Add("@lentgh", SqlDbType.Real);
                            if (UnitResult[3] != "")
                                MyCom.Parameters["@lentgh"].Value = double.Parse(UnitResult[3]);
                            else MyCom.Parameters["@lentgh"].Value = 0;
                            MyCom.Parameters.Add("@gencofrom", SqlDbType.NChar, 10);
                            MyCom.Parameters["@gencofrom"].Value = UnitResult[5];
                            MyCom.Parameters.Add("@gencoto", SqlDbType.NChar, 10);
                            MyCom.Parameters["@gencoto"].Value = UnitResult[6];
                            MyCom.Parameters.Add("@capacity", SqlDbType.Real);

                            if (UnitResult[4] != "")
                                MyCom.Parameters["@capacity"].Value = double.Parse(UnitResult[4]);
                            else MyCom.Parameters["@capacity"].Value = 0;
                            MyCom.Parameters.Add("@name", SqlDbType.NChar, 20);
                            string name = "";
                            if ((UnitResult[5] != "") && (UnitResult[6] != ""))
                                name = UnitResult[5] + "-" + UnitResult[6];
                            MyCom.Parameters["@name"].Value = name;
                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                            }
                            buildTRANStree(line.ToString());
                            FillTransmissionGrid(line.ToString());
                        }
                        else MessageBox.Show("This Line Exists Now!");
                    }
                    //foreach (DataGridViewRow dr in Grid400.Rows)
                    //{
                    //    if ((dr.Cells[7].Value == null) || (dr.Cells[7].Value.ToString() == ""))
                    //        if (dr.Cells[0].Value != null)
                    //        {
                    //            SqlCommand MyCom = new SqlCommand();
                    //            MyCom.CommandText = "INSERT INTO Lines (LineNumber,LineCode,FromBus,ToBus,LineLength,"+
                    //            "Owner_GencoFrom,Owner_GencoTo,RateA) VALUES (@num,@code,@from,@to,@lentgh,@gencofrom,"+
                    //            "@gencoto,@capacity) INSERT INTO TransLine (LineNumber,LineCode,Name) VALUES (@num,@code,@name) ";
                    //            MyCom.Connection = MyConnection;
                    //            MyCom.Parameters.Add("@num", SqlDbType.Int);
                    //            MyCom.Parameters["@num"].Value = int.Parse(GDGasGroup.Text);
                    //            MyCom.Parameters.Add("@code", SqlDbType.NChar, 20);
                    //            MyCom.Parameters["@code"].Value = dr.Cells[0].Value.ToString();
                    //            MyCom.Parameters.Add("@from", SqlDbType.SmallInt);
                    //            if ((dr.Cells[1].Value!=null)&&(dr.Cells[1].Value.ToString()!=""))
                    //                MyCom.Parameters["@from"].Value = int.Parse(dr.Cells[1].Value.ToString());
                    //            else MyCom.Parameters["@from"].Value =0;
                    //            MyCom.Parameters.Add("@to", SqlDbType.SmallInt);
                    //            if ((dr.Cells[2].Value!=null)&&(dr.Cells[2].Value.ToString()!=""))
                    //                MyCom.Parameters["@to"].Value = int.Parse(dr.Cells[2].Value.ToString());
                    //            else MyCom.Parameters["@to"].Value =0;
                    //            MyCom.Parameters.Add("@lentgh", SqlDbType.Real);
                    //            if ((dr.Cells[3].Value!=null)&&(dr.Cells[3].Value.ToString()!=""))
                    //                MyCom.Parameters["@lentgh"].Value = double.Parse(dr.Cells[3].Value.ToString());
                    //            else MyCom.Parameters["@lentgh"].Value =0;
                    //            MyCom.Parameters.Add("@gencofrom", SqlDbType.NChar,10);
                    //            if (dr.Cells[4].Value!=null)
                    //                MyCom.Parameters["@gencofrom"].Value = dr.Cells[4].Value.ToString();
                    //            else MyCom.Parameters["@gencofrom"].Value ="";
                    //            MyCom.Parameters.Add("@gencoto", SqlDbType.NChar,10);
                    //            if (dr.Cells[5].Value!=null)
                    //                MyCom.Parameters["@gencoto"].Value = dr.Cells[5].Value.ToString();
                    //            else MyCom.Parameters["@gencoto"].Value ="";
                    //            MyCom.Parameters.Add("@capacity", SqlDbType.Real);
                    //            if ((dr.Cells[6].Value!=null)&&(dr.Cells[6].Value.ToString()!=""))
                    //                MyCom.Parameters["@capacity"].Value = double.Parse(dr.Cells[6].Value.ToString());
                    //            else MyCom.Parameters["@capacity"].Value =0;
                    //            MyCom.Parameters.Add("@name", SqlDbType.NChar,20);
                    //            string name="";
                    //            if ((dr.Cells[4].Value!=null)&&(dr.Cells[5].Value!=null))
                    //                name=dr.Cells[4].Value.ToString()+"-"+dr.Cells[5].Value.ToString();
                    //            MyCom.Parameters["@name"].Value=name;
                    //            //try
                    //            //{
                    //            MyCom.ExecuteNonQuery();
                    //            //}
                    //            //catch (Exception exp)
                    //            //{
                    //            //MessageBox.Show("This Unit Exists Now!");
                    //            //  string str = exp.Message;
                    //            //}
                    //        }
                    //    }
                    //    foreach (DataGridViewRow dr in Grid230.Rows)
                    //    {
                    //    if ((dr.Cells[7].Value == null) || (dr.Cells[7].Value.ToString() == ""))
                    //        if (dr.Cells[0].Value != null)
                    //        {
                    //            SqlCommand MyCom = new SqlCommand();
                    //            MyCom.CommandText = "INSERT INTO Lines (LineNumber,LineCode,FromBus,ToBus,LineLength,"+
                    //            "Owner_GencoFrom,Owner_GencoTo,RateA) VALUES (@num,@code,@from,@to,@lentgh,@gencofrom,"+
                    //            "@gencoto,@capacity) INSERT INTO TransLine (LineNumber,LineCode,Name) VALUES (@num,@code,@name)";
                    //            MyCom.Connection = MyConnection;
                    //            MyCom.Parameters.Add("@num", SqlDbType.Int);
                    //            MyCom.Parameters["@num"].Value = int.Parse(GDSteamGroup.Text);
                    //            MyCom.Parameters.Add("@code", SqlDbType.NChar, 20);
                    //            MyCom.Parameters["@code"].Value = dr.Cells[0].Value.ToString();
                    //            MyCom.Parameters.Add("@from", SqlDbType.SmallInt);
                    //            if ((dr.Cells[1].Value!=null)&&(dr.Cells[1].Value.ToString()!=""))
                    //                MyCom.Parameters["@from"].Value = int.Parse(dr.Cells[1].Value.ToString());
                    //            else MyCom.Parameters["@from"].Value =0;
                    //            MyCom.Parameters.Add("@to", SqlDbType.SmallInt);
                    //            if ((dr.Cells[2].Value!=null)&&(dr.Cells[2].Value.ToString()!=""))
                    //                MyCom.Parameters["@to"].Value = int.Parse(dr.Cells[2].Value.ToString());
                    //            else MyCom.Parameters["@to"].Value =0;
                    //            MyCom.Parameters.Add("@lentgh", SqlDbType.Real);
                    //            if ((dr.Cells[3].Value!=null)&&(dr.Cells[3].Value.ToString()!=""))
                    //                MyCom.Parameters["@lentgh"].Value = double.Parse(dr.Cells[3].Value.ToString());
                    //            else MyCom.Parameters["@lentgh"].Value =0;
                    //            MyCom.Parameters.Add("@gencofrom", SqlDbType.NChar,10);
                    //            if (dr.Cells[4].Value!=null)
                    //                MyCom.Parameters["@gencofrom"].Value = dr.Cells[4].Value.ToString();
                    //            else MyCom.Parameters["@gencofrom"].Value ="";
                    //            MyCom.Parameters.Add("@gencoto", SqlDbType.NChar,10);
                    //            if (dr.Cells[5].Value!=null)
                    //                MyCom.Parameters["@gencoto"].Value = dr.Cells[5].Value.ToString();
                    //            else MyCom.Parameters["@gencoto"].Value ="";
                    //            MyCom.Parameters.Add("@capacity", SqlDbType.Real);
                    //            if ((dr.Cells[6].Value!=null)&&(dr.Cells[6].Value.ToString()!=""))
                    //                MyCom.Parameters["@capacity"].Value = double.Parse(dr.Cells[6].Value.ToString());
                    //            else MyCom.Parameters["@capacity"].Value =0;
                    //            MyCom.Parameters.Add("@name", SqlDbType.NChar, 20);
                    //            string name = "";
                    //            if ((dr.Cells[4].Value != null) && (dr.Cells[5].Value != null))
                    //                name = dr.Cells[4].Value.ToString() + "-" + dr.Cells[5].Value.ToString();
                    //            MyCom.Parameters["@name"].Value = name;
                    //            //try
                    //            //{
                    //            MyCom.ExecuteNonQuery();
                    //            //}
                    //            //catch (Exception exp)
                    //            //{
                    //            //MessageBox.Show("This Unit Exists Now!");
                    //            //  string str = exp.Message;
                    //            //}
                    //        }
                    //}
                }
            }

            //We Are in GDUnitPanel
            else if (GDNewBtn.Text.Contains("Save"))
            {
                //A Unit is selected
                if (Currentgb.Text.Contains("STATE"))
                {
                    if ((errorProvider1.GetError(GDcapacityTB) == "") && (errorProvider1.GetError(GDPmaxTB) == "") &&
                        (errorProvider1.GetError(GDPminTB) == "") && (errorProvider1.GetError(GDTimeUpTB) == "") &&
                        (errorProvider1.GetError(GDTimeDownTB) == "") && (errorProvider1.GetError(GDTimeColdStartTB) == "") &&
                        (errorProvider1.GetError(GDTimeHotStartTB) == "") && (errorProvider1.GetError(GDRampRateTB) == "") &&
                        (errorProvider1.GetError(GDPrimaryFuelTB) == "") && (errorProvider1.GetError(GDSecondaryFuelTB) == "") && (errorProvider1.GetError(GDtxtAvc) == "") &&
                        (errorProvider1.GetError(txtDurabilityHours) == "") && (errorProvider1.GetError(datePickerDurability) == "") &&
                        (errorProvider1.GetError(txtMaintLastHour) == "") && (errorProvider1.GetError(cmbMaintLastMaintType) == "") &&
                        (errorProvider1.GetError(txtMaintFuelFactor) == "") && (errorProvider1.GetError(txtMaintStartUpFactor) == "") &&
                        (errorProvider1.GetError(txtMaintCIHours) == "") && (errorProvider1.GetError(txtMaintCIDur) == "") &&
                        (errorProvider1.GetError(txtMaintHGPHours) == "") && (errorProvider1.GetError(txtMaintHGPDur) == "") &&
                        (errorProvider1.GetError(txtMaintMOHours) == "") && (errorProvider1.GetError(txtMaintMODur) == "") &&
                        (txtDurabilityHours.Text != "") && (datePickerDurability.Text != "") && (txtMaintLastHour.Text != "") &&
                        (cmbMaintLastMaintType.Text != "") && (txtMaintFuelFactor.Text != "") && (txtMaintStartUpFactor.Text != "") &&
                        (txtMaintCIHours.Text != "") && (txtMaintCIDur.Text != "") &&
                        (txtMaintMOHours.Text != "") && (txtMaintMODur.Text != "") &&
                        (GDcapacityTB.Text != "") && (GDPmaxTB.Text != "") && (GDPminTB.Text != "") && (GDTimeUpTB.Text != "") &&
                        (GDTimeDownTB.Text != "") && (GDTimeColdStartTB.Text != "") && (GDtxtAvc.Text != "") && (GDTimeHotStartTB.Text != "") &&
                        (GDRampRateTB.Text != "") && (GDPrimaryFuelTB.Text != "") && (GDSecondaryFuelTB.Text != ""))
                    {
                        SqlCommand MyCom = new SqlCommand();
                        MyCom.CommandText = "UPDATE UnitsDataMain SET Capacity=@cap,PMax=@pmax,PMin=@pmin,TUp=@tup,TDown=@tdown," +
                        "TStartCold=@tcold,TStartHot=@thot,RampUpRate=@ramp,HeatValuePrimaryFuel=@primary, " +
                        "HeatValueSecondaryFuel=@secondary,PowerSerial=@Pserial,ConsumedSerial=@Cserial,Avc=@avcvalue WHERE PPID=@num AND UnitCode=@unit AND PackageType=@package ";
                        MyCom.Connection = myConnection;
                        MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                        MyCom.Parameters["@num"].Value = PPID;
                        MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
                        string unit = GDUnitLb.Text;
                        string package = GDPackLb.Text;
                        MyCom.Parameters["@unit"].Value = unit;
                        MyCom.Parameters.Add("@package", SqlDbType.NChar, 10);
                        if (package.Contains("Combined")) package = "CC";
                        MyCom.Parameters["@package"].Value = package;
                        MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                        //Detect Farsi Date
                        PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
                        string mydate = prDate.ToString("d");

                        MyCom.Parameters["@date"].Value = mydate;
                        MyCom.Parameters.Add("@cap", SqlDbType.Real);
                        if (GDcapacityTB.Text != "")
                            MyCom.Parameters["@cap"].Value = double.Parse(GDcapacityTB.Text);
                        else MyCom.Parameters["@cap"].Value = 0;
                        MyCom.Parameters.Add("@pmax", SqlDbType.Real);
                        if (GDPmaxTB.Text != "")
                            MyCom.Parameters["@pmax"].Value = double.Parse(GDPmaxTB.Text);
                        else MyCom.Parameters["@pmax"].Value = 0;
                        MyCom.Parameters.Add("@pmin", SqlDbType.Real);
                        if (GDPminTB.Text != "")
                            MyCom.Parameters["@pmin"].Value = double.Parse(GDPminTB.Text);
                        else MyCom.Parameters["@pmin"].Value = 0;
                        MyCom.Parameters.Add("@tup", SqlDbType.SmallInt);
                        if (GDTimeUpTB.Text != "")
                            MyCom.Parameters["@tup"].Value = int.Parse(GDTimeUpTB.Text);
                        else MyCom.Parameters["@tup"].Value = 0;
                        MyCom.Parameters.Add("@tdown", SqlDbType.SmallInt);
                        if (GDTimeDownTB.Text != "")
                            MyCom.Parameters["@tdown"].Value = int.Parse(GDTimeDownTB.Text);
                        else MyCom.Parameters["@tdown"].Value = 0;
                        MyCom.Parameters.Add("@tcold", SqlDbType.Real);
                        if (GDTimeColdStartTB.Text != "")
                            MyCom.Parameters["@tcold"].Value = double.Parse(GDTimeColdStartTB.Text);
                        else MyCom.Parameters["@tcold"].Value = 0;
                        MyCom.Parameters.Add("@thot", SqlDbType.Real);
                        if (GDTimeHotStartTB.Text != "")
                            MyCom.Parameters["@thot"].Value = double.Parse(GDTimeHotStartTB.Text);
                        else MyCom.Parameters["@thot"].Value = 0;
                        MyCom.Parameters.Add("@ramp", SqlDbType.SmallInt);
                        if (GDRampRateTB.Text != "")
                            MyCom.Parameters["@ramp"].Value = int.Parse(GDRampRateTB.Text);
                        else MyCom.Parameters["@ramp"].Value = 0;
                        MyCom.Parameters.Add("@primary", SqlDbType.Real);
                        if (GDPrimaryFuelTB.Text != "")
                            MyCom.Parameters["@primary"].Value = double.Parse(GDPrimaryFuelTB.Text);
                        else MyCom.Parameters["@primary"].Value = 0;
                        MyCom.Parameters.Add("@secondary", SqlDbType.Real);
                        if (GDSecondaryFuelTB.Text != "")
                            MyCom.Parameters["@secondary"].Value = double.Parse(GDSecondaryFuelTB.Text);
                        else MyCom.Parameters["@secondary"].Value = 0;

                        MyCom.Parameters.Add("@Pserial", SqlDbType.NChar, 20);
                        MyCom.Parameters["@Pserial"].Value = GDPowerSerialTb.Text;

                        MyCom.Parameters.Add("@Cserial", SqlDbType.NChar, 20);
                        MyCom.Parameters["@Cserial"].Value = GDConsumedSerialTb.Text;


                        MyCom.Parameters.Add("@avcvalue", SqlDbType.Real);
                        if (GDtxtAvc.Text != "")
                            MyCom.Parameters["@avcvalue"].Value = double.Parse(GDtxtAvc.Text);
                        else MyCom.Parameters["@avcvalue"].Value = 0;
                
                        //try
                        //{
                        MyCom.ExecuteNonQuery();

                        ////////////////////////////////////
                        string strDate = "";
                        if (PersianDateConverter.ToPersianDate(datePickerDurability.SelectedDateTime) != null)
                            strDate = PersianDateConverter.ToPersianDate(datePickerDurability.SelectedDateTime).ToString("d");
                        MyCom = new SqlCommand();
                        string strCmd = "UPDATE MaintenanceGeneralData Set ";
                        if (txtMaintCIHours.Text != "")
                            strCmd += " CIHour=" + txtMaintCIHours.Text + ",";
                        if (txtMaintCIDur.Text != "")
                            strCmd += " CIDuration=" + txtMaintCIDur.Text + ",";
                        if (txtMaintHGPHours.Text != "")
                            strCmd += " HGPHour=" + txtMaintHGPHours.Text + ",";
                        if (txtMaintHGPDur.Text != "")
                            strCmd += " HGPDuration=" + txtMaintHGPDur.Text + ",";
                        if (txtMaintMOHours.Text != "")
                            strCmd += " MOHour=" + txtMaintMOHours.Text + ",";
                        if (txtMaintMODur.Text != "")
                            strCmd += " MODuration=" + txtMaintMODur.Text + ",";
                        if (txtDurabilityHours.Text != "")
                            strCmd += " DurabilityHours=" + txtDurabilityHours.Text + ",";
                        if (strDate != "")
                            strCmd += " DurabilityDateTime='" + strDate + "',";
                        if (txtMaintStartUpFactor.Text != "")
                            strCmd += " startupfactor=" + txtMaintStartUpFactor.Text + ",";
                        if (txtMaintFuelFactor.Text != "")
                            strCmd += " fuelfactor=" + txtMaintFuelFactor.Text + ",";
                        if (txtMaintLastHour.Text != "")
                            strCmd += " lastmainthour=" + txtMaintLastHour.Text + ",";
                        if (cmbMaintLastMaintType.SelectedItem.ToString() != "")
                            strCmd += " lastMaintType='" + cmbMaintLastMaintType.SelectedItem.ToString() + "',";

                        strCmd = strCmd.Remove(strCmd.Length - 1);

                        strCmd += " where PPID= " + PPID +
                        " and UnitCode='" + unit + "'" +
                        " and PackageType='" + package + "'";
                        MyCom.CommandText = strCmd;
                        MyCom.Connection = myConnection;
                        MyCom.ExecuteNonQuery();
                        ///////////////////////////////////
                        //}
                        //catch (Exception exp)
                        //{
                        //  string str = exp.Message;
                        //}


                        ///////////////////// MaintenaceGeneralData///////////////

                        //MyCom = new SqlCommand();
                        //MyCom.CommandText = "UPDATE UnitsDataMain SET";

                        ///////////////////// MaintenaceGeneralData///////////////


                        FillPlantGrid(PPID);
                    }
                    else
                    {
                        MessageBox.Show("Please fill all blanks!");
                    }
                }

                //A Transmission Line is selected
                else if (Currentgb.Text.Contains("OWNER"))
                {
                    if ((errorProvider1.GetError(GDcapacityTB) == "") && (errorProvider1.GetError(GDPmaxTB) == "") &&
                    (errorProvider1.GetError(GDPminTB) == "") && (errorProvider1.GetError(GDRampRateTB) == "") &&
                    (errorProvider1.GetError(GDTimeDownTB) == "") && (errorProvider1.GetError(GDTimeColdStartTB) == "") &&
                    (errorProvider1.GetError(GDTimeHotStartTB) == ""))
                    {
                        SqlCommand MyCom = new SqlCommand();
                        MyCom.CommandText = "UPDATE Lines SET RateA=@cap,FromBus=@from,ToBus=@to,CircuitID=@cid,LineR=@lr," +
                        "LineX=@lx,LineSuseptance=@sus,LineLength=@length,PowerSerialtrans=@pstrans WHERE LineNumber=@num AND LineCode=@code";
                        MyCom.Connection = myConnection;
                        MyCom.Parameters.Add("@num", SqlDbType.Int);
                        MyCom.Parameters["@num"].Value = line;
                        MyCom.Parameters.Add("@code", SqlDbType.NChar, 20);
                        MyCom.Parameters["@code"].Value = GDPlantLb.Text;
                        MyCom.Parameters.Add("@cap", SqlDbType.Real);
                        if (GDcapacityTB.Text != "")
                            MyCom.Parameters["@cap"].Value = double.Parse(GDcapacityTB.Text);
                        else MyCom.Parameters["@cap"].Value = 0;
                        MyCom.Parameters.Add("@from", SqlDbType.NVarChar, 50);
                        if (GDPmaxTB.Text != "")
                            MyCom.Parameters["@from"].Value = GDPmaxTB.Text;
                        else MyCom.Parameters["@from"].Value = "0";
                        MyCom.Parameters.Add("@to", SqlDbType.NVarChar, 50);
                        if (GDPminTB.Text != "")
                            MyCom.Parameters["@to"].Value = GDPminTB.Text;
                        else MyCom.Parameters["@to"].Value = "0";
                        MyCom.Parameters.Add("@cid", SqlDbType.NChar, 5);
                        MyCom.Parameters["@cid"].Value = GDTimeUpTB.Text;
                        MyCom.Parameters.Add("@lr", SqlDbType.Real);
                        if (GDTimeDownTB.Text != "")
                            MyCom.Parameters["@lr"].Value = double.Parse(GDTimeDownTB.Text);
                        else MyCom.Parameters["@lr"].Value = 0;
                        MyCom.Parameters.Add("@lx", SqlDbType.Real);
                        if (GDTimeColdStartTB.Text != "")
                            MyCom.Parameters["@lx"].Value = double.Parse(GDTimeColdStartTB.Text);
                        else MyCom.Parameters["@lx"].Value = 0;
                        MyCom.Parameters.Add("@sus", SqlDbType.Real);
                        if (GDTimeHotStartTB.Text != "")
                            MyCom.Parameters["@sus"].Value = double.Parse(GDTimeHotStartTB.Text);
                        else MyCom.Parameters["@sus"].Value = 0;
                        MyCom.Parameters.Add("@length", SqlDbType.Real);
                        if (GDRampRateTB.Text != "")
                            MyCom.Parameters["@length"].Value = double.Parse(GDRampRateTB.Text);
                        else MyCom.Parameters["@length"].Value = 0;

                        MyCom.Parameters.Add("@pstrans", SqlDbType.NChar, 20);
                        if (GDpowertransserialtb.Text != "")
                            MyCom.Parameters["@pstrans"].Value = GDpowertransserialtb.Text;
                        else MyCom.Parameters["@pstrans"].Value = 0;
                        //try
                        //{
                        MyCom.ExecuteNonQuery();
                        //}
                        //catch (Exception exp)
                        //{
                        //  string str = exp.Message;
                        //}

                        //Update OutService GroupBox
                        if (GDOutServiceCheck.Checked)
                        {
                            //if ((GDMainTypeTB.Text != "") && ((!GDStartDateCal.IsNull)||(GDStartDateCal.Text!="")) && ((!GDEndDateCal.IsNull)||(GDEndDateCal.Text!="")))
                            if ((GDMainTypeTB.Text != "") && (!GDStartDateCal.IsNull) && (!GDEndDateCal.IsNull))
                            {
                                DataTable outservice = Utilities.GetTable("UPDATE Lines SET OutService='True', OutServiceType='" + GDMainTypeTB.Text + "', OutServiceStartDate='" + GDStartDateCal.Text + "', OutServiceEndDate='" + GDEndDateCal.Text + "'where LineCode='" + GDPlantLb.Text + "' and LineNumber='" + line + "'");
                                UpdateLineTab(true);
                            }
                            else
                                MessageBox.Show("Please fill OutService Items!");
                        }
                        else
                        {
                            DataTable outservice = Utilities.GetTable("UPDATE Lines SET OutService='False', OutServiceType='', OutServiceStartDate='', OutServiceEndDate=''where linecode='" + GDPlantLb.Text + "' and LineNumber='"+ line +"'");
                            UpdateLineTab(true);
                        }
                    }

                }
                GDcapacityTB.ReadOnly = true;
                GDPmaxTB.ReadOnly = true;
                GDPminTB.ReadOnly = true;
                GDTimeUpTB.ReadOnly = true;
                GDTimeDownTB.ReadOnly = true;
                GDTimeColdStartTB.ReadOnly = true;
                GDTimeHotStartTB.ReadOnly = true;
                GDRampRateTB.ReadOnly = true;
                GDPrimaryFuelTB.ReadOnly = true;
                GDSecondaryFuelTB.ReadOnly = true;
                GDPowerSerialTb.ReadOnly = true;
                GDpowertransserialtb.ReadOnly = true;
                GDConsumedSerialTb.ReadOnly = true;

            }
            myConnection.Close();
        }
        //-------------------------------------------GDDeleteBtn_Click----------------------------------------------
        private void GDDeleteBtn_Click(object sender, EventArgs e)
        {
            //We Are in GDPlantPanel
            if (GDDeleteBtn.Text.Contains("Delete"))
            {
                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                // A Plant is selected
                if ((PlantGV1.Visible) || (PlantGV2.Visible))
                {
                    bool delete = true;
                    int first = 0;
                    if (PlantGV1.Visible)
                        foreach (DataGridViewRow dr in PlantGV1.Rows)
                        {
                            if ((dr.Cells[10].Value != null) && (dr.Cells[10].Value.ToString() == "1"))
                            //Cells[10] Because in 10th cell we have added checkbox          
                            {
                                if (first == 0)
                                {
                                    DialogResult result = MessageBox.Show("Are You Sure to delete Selected Row(s)?", "Warning", MessageBoxButtons.YesNo);
                                    if (result == DialogResult.No) delete = false;
                                }
                                if (delete)
                                {
                                    //delete this row
                                    SqlCommand MyCom = new SqlCommand();
                                    MyCom.CommandText = "DELETE FROM [UnitsDataMain] WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type AND PackageCode=@code " +
                                    "DELETE FROM [MaintenanceGeneralData] WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type";
                                    MyCom.Connection = myConnection;
                                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                                    MyCom.Parameters["@num"].Value = PPID;
                                    MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
                                    MyCom.Parameters["@unit"].Value = dr.Cells[0].Value.ToString().Trim();
                                    MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
                                    string type = GDGasGroup.Text;
                                    if (type.Contains("/")) type = dr.Cells[2].Value.ToString().Trim();
                                    if (type.Contains("Combined")) type = "CC";
                                    MyCom.Parameters["@type"].Value = type;
                                    MyCom.Parameters.Add("@code", SqlDbType.Int);
                                    try
                                    {
                                        MyCom.Parameters["@code"].Value = int.Parse(dr.Cells[1].Value.ToString().Trim());
                                    }
                                    catch
                                    {
                                        MyCom.Parameters["@code"].Value = 0;
                                    }
                                    //try
                                    //{
                                    MyCom.ExecuteNonQuery();
                                    //}
                                    //catch (Exception exp)
                                    //{
                                    //  string str = exp.Message;
                                    //}
                                    DataTable TypeCount = Utilities.GetTable("SELECT COUNT(*) FROM UnitSDataMain WHERE PPID='" + PPID + "' AND PackageType='" + type + "'");
                                    //if (PlantGV1.RowCount <= 2)
                                    if (TypeCount.Rows[0][0].ToString().Trim() == "0")
                                    {
                                        MyCom.CommandText = "DELETE FROM [PPUnit] WHERE PPID=@num AND PackageType=@packagetype";
                                        MyCom.Parameters["@num"].Value = PPID;
                                        MyCom.Parameters.Add("@packagetype", SqlDbType.NChar, 20);
                                        if (type.Contains("CC")) type = "Combined Cycle";
                                        MyCom.Parameters["@packagetype"].Value = type;
                                        //try
                                        //{
                                        MyCom.ExecuteNonQuery();
                                        //}
                                        //catch (Exception exp)
                                        //{
                                        //  string str = exp.Message;
                                        //}

                                    }
                                }
                                first++;
                            }
                        }
                    if (PlantGV2.Visible)
                        foreach (DataGridViewRow dr in PlantGV2.Rows)
                        {
                            if ((dr.Cells[10].Value != null) && (dr.Cells[10].Value.ToString() == "1"))
                            //Cells[10] Because in cell 10th cell we have added checkbox          
                            {
                                if (first == 0)
                                {
                                    DialogResult result = MessageBox.Show("Are You Sure to delete Selected Row(s)?", "Warning", MessageBoxButtons.YesNo);
                                    if (result == DialogResult.No) delete = false;
                                }
                                if (delete)
                                {
                                    //delete this row

                                    SqlCommand MyCom = new SqlCommand();
                                    MyCom.CommandText = "DELETE FROM [UnitsDataMain] WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type AND PackageCode=@code " +
                                    "DELETE FROM [MaintenanceGeneralData] WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type ";
                                    MyCom.Connection = myConnection;
                                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                                    MyCom.Parameters["@num"].Value = PPID;
                                    MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
                                    MyCom.Parameters["@unit"].Value = dr.Cells[0].Value.ToString().Trim();
                                    MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
                                    string type = GDSteamGroup.Text;
                                    if (type.Contains("/")) type = dr.Cells[2].Value.ToString().Trim();
                                    if (type.Contains("Combined")) type = "CC";
                                    MyCom.Parameters["@type"].Value = type;
                                    MyCom.Parameters.Add("@code", SqlDbType.Int);
                                    try
                                    {
                                        MyCom.Parameters["@code"].Value = int.Parse(dr.Cells[1].Value.ToString().Trim());
                                    }
                                    catch
                                    {
                                        MyCom.Parameters["@code"].Value = 0;
                                    }
                                    //try
                                    //{
                                    MyCom.ExecuteNonQuery();
                                    //}
                                    //catch (Exception exp)
                                    //{
                                    //  string str = exp.Message;
                                    //}
                                    DataTable TypeCount = Utilities.GetTable("SELECT COUNT(*) FROM UnitSDataMain WHERE PPID='" + PPID + "' AND PackageType='" + type + "'");
                                    //if (PlantGV2.RowCount <= 2)
                                    if (TypeCount.Rows[0][0].ToString().Trim() == "0")
                                    {
                                        MyCom.CommandText = "DELETE FROM [PPUnit] WHERE PPID=@num AND PackageType=@packagetype2";
                                        MyCom.Parameters["@num"].Value = PPID;
                                        MyCom.Parameters.Add("@packagetype2", SqlDbType.NChar, 20);
                                        if (type.Contains("CC")) type = "Combined Cycle";
                                        MyCom.Parameters["@packagetype2"].Value = type;
                                        //try
                                        //{
                                        MyCom.ExecuteNonQuery();
                                        //}
                                        //catch (Exception exp)
                                        //{
                                        //  string str = exp.Message;
                                        //}

                                    }

                                }
                                first++;
                            }
                        }
                    if (delete)
                    {
                        //PlantGV1.Rows[PlantGV1.RowCount - 1].Cells[PlantGV1.ColumnCount - 1].ReadOnly = true;
                        //PlantGV2.Rows[PlantGV2.RowCount - 1].Cells[PlantGV2.ColumnCount - 1].ReadOnly = true;
                        buildPPtree(PPID);
                        FillPlantGrid(PPID);
                    }
                }
                // A Transmission Line is selected
                if ((Grid230.Visible) || (Grid400.Visible))
                {
                    bool delete = true;
                    int first = 0;
                    if (Grid400.Visible)
                    {
                        foreach (DataGridViewRow dr in Grid400.Rows)
                        {
                            if ((dr.Cells[8].Value != null) && (dr.Cells[8].Value.ToString() == "1"))
                            //Cells[8] Because in cell 8th cell we have added checkbox          
                            {
                                if (first == 0)
                                {
                                    DialogResult result = MessageBox.Show("Are You Sure to delete Selected Row(s)?", "Warning", MessageBoxButtons.YesNo);
                                    if (result == DialogResult.No) delete = false;
                                }
                                if (delete)
                                {
                                    //delete this row
                                    SqlCommand MyCom = new SqlCommand();
                                    MyCom.CommandText = "DELETE FROM [Lines] WHERE LineNumber=@num AND LineCode=@code DELETE FROM [TransLine] WHERE LineNumber=@num AND LineCode=@code ";
                                    MyCom.Connection = myConnection;
                                    MyCom.Parameters.Add("@num", SqlDbType.Int);
                                    MyCom.Parameters["@num"].Value = int.Parse(GDGasGroup.Text);
                                    MyCom.Parameters.Add("@code", SqlDbType.NChar, 20);
                                    MyCom.Parameters["@code"].Value = dr.Cells[0].Value.ToString().Trim();
                                    //try
                                    //{
                                    MyCom.ExecuteNonQuery();
                                    //}
                                    //catch (Exception exp)
                                    //{
                                    //  string str = exp.Message;
                                    //}
                                }
                                first++;
                            }
                        }
                        buildTRANStree(GDGasGroup.Text);
                        FillTransmissionGrid(GDGasGroup.Text);
                    }
                    if (Grid230.Visible)
                    {
                        foreach (DataGridViewRow dr in Grid230.Rows)
                        {
                            if ((dr.Cells[8].Value != null) && (dr.Cells[8].Value.ToString() == "1"))
                            //Cells[8] Because in cell 8th cell we have added checkbox          
                            {
                                if (first == 0)
                                {
                                    DialogResult result = MessageBox.Show("Are You Sure to delete Selected Row(s)?", "Warning", MessageBoxButtons.YesNo);
                                    if (result == DialogResult.No) delete = false;
                                }
                                if (delete)
                                {
                                    //delete this row
                                    SqlCommand MyCom = new SqlCommand();
                                    MyCom.CommandText = "DELETE FROM [Lines] WHERE LineNumber=@num AND LineCode=@code DELETE FROM [TransLine] WHERE LineNumber=@num AND LineCode=@code";
                                    MyCom.Connection = myConnection;
                                    MyCom.Parameters.Add("@num", SqlDbType.Int);
                                    MyCom.Parameters["@num"].Value = int.Parse(GDSteamGroup.Text);
                                    MyCom.Parameters.Add("@code", SqlDbType.NChar, 20);
                                    MyCom.Parameters["@code"].Value = dr.Cells[0].Value.ToString().Trim();
                                    //try
                                    //{
                                    MyCom.ExecuteNonQuery();
                                    //}
                                    //catch (Exception exp)
                                    //{
                                    //  string str = exp.Message;
                                    //}
                                }
                                first++;
                            }
                        }
                        buildTRANStree(GDSteamGroup.Text);
                        FillTransmissionGrid(GDSteamGroup.Text);
                    }
                }
                myConnection.Close();
            }
            //We Are in GDUnitPanel or GDLinePanel
            else if (GDDeleteBtn.Text.Contains("Edit"))
            {
                string unitType = "";
                if (treeView2.SelectedNode.Text.Contains("gas"))
                    unitType = "Gas";
                else if (treeView2.SelectedNode.Text.Contains("steam"))
                    unitType = "Steam";
                DisableUnitTab(false, unitType);
                //For Units
                if (Currentgb.Text.Contains("STATE"))
                {
                    GDPrimaryFuelTB.ReadOnly = false;
                    GDSecondaryFuelTB.ReadOnly = false;
                }
                //For Lines
                else if (Currentgb.Text.Contains("OWNER"))
                {
                    DataTable dt1 = Utilities.GetTable("select VoltageNumber from dbo.PostVoltage where VoltageLevel='" + GDPmaxTB.Text.Trim() + "'");
                    if (dt1.Rows.Count > 0)
                    {
                        GDPmaxTB.Text = dt1.Rows[0][0].ToString().Trim();
                    }


                    DataTable dt2 = Utilities.GetTable("select VoltageNumber from dbo.PostVoltage where VoltageLevel='" + GDPminTB.Text.Trim() + "'");
                    if (dt2.Rows.Count > 0)
                    {
                        GDPminTB.Text = dt2.Rows[0][0].ToString().Trim();
                    }


                    UpdateLineTab(false);
                }
            }
        }

        //------------------------------FillTransmissionGrid--------------------------------------
        private void FillTransmissionGrid(string Num)
        {
            //Clear Grids
            //PlantGV1.DataSource = null;
            //if (PlantGV1.Rows != null) PlantGV1.Rows.Clear();
            //PlantGV2.DataSource = null;
            //if (PlantGV2.Rows != null) PlantGV2.Rows.Clear();

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();

            //detect current date and hour
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            Myda.SelectCommand = new SqlCommand("SELECT LineCode,FromBus,ToBus,LineLength,Owner_GencoFrom,Owner_GencoTo" +
            ",RateA,OutService,OutServiceStartDate,OutServiceEndDate FROM [Lines] WHERE LineNumber=" + Num, myConnection);
            Myda.Fill(MyDS);
            myConnection.Close();

            if (Num.Contains("400"))
            {
                //for (int i = 0; i < 7; i++)
                //Grid400.Rows[Grid400.RowCount - 1].Cells[i].ReadOnly = false;
                Grid400.DataSource = MyDS.Tables[0].DefaultView;
                //Grid400.ReadOnly = true;
                int index = 0;
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    Grid400.Rows[index].Cells[7].Value = "Work";
                    Grid400.Rows[index].Cells[Grid400.ColumnCount - 1].ReadOnly = false;
                    if (MyRow[7].ToString() == "True")
                        if (CheckDate(mydate, MyRow[8].ToString(), MyRow[9].ToString()))
                            Grid400.Rows[index].Cells[7].Value = "OutService";

                    index++;
                }
                Grid400.Rows[Grid400.RowCount - 1].Cells[Grid400.ColumnCount - 1].ReadOnly = true;
            }
            else
            {
                Grid230.DataSource = MyDS.Tables[0].DefaultView;
                //for (int i = 0; i < 7; i++)
                //Grid230.Rows[Grid230.RowCount - 1].Cells[i].ReadOnly = false;
                //Grid230.ReadOnly = true;
                int index = 0;
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    Grid230.Rows[index].Cells[7].Value = "Work";
                    Grid230.Rows[index].Cells[Grid230.ColumnCount - 1].ReadOnly = false;
                    if (MyRow[7].ToString() == "True")
                        if (CheckDate(mydate, MyRow[8].ToString(), MyRow[9].ToString()))
                            Grid230.Rows[index].Cells[7].Value = "OutService";

                    index++;
                }
                Grid230.Rows[Grid230.RowCount - 1].Cells[Grid230.ColumnCount - 1].ReadOnly = true;
            }

        }
        //-------------------------FillPlantGrid---------------------------------------
        private void FillPlantGrid(string Num)
        {
            //Clear Grids
            //PlantGV1.DataSource = null;
            //if (PlantGV1.Rows != null) PlantGV1.Rows.Clear();
            //PlantGV2.DataSource = null;
            //if (PlantGV2.Rows != null) PlantGV2.Rows.Clear();

            Currentgb.Text = "CURRENT STATE";
            //Detect Farsi Date
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");
            int myhour = DateTime.Now.Hour;

            //detect the type of packages(Data GridView Labels)

            DataSet dataDS = new DataSet();
            SqlDataAdapter dataDA = new SqlDataAdapter();
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            dataDA.SelectCommand = new SqlCommand("SELECT PackageType FROM PPUnit WHERE PPID=" + Num, myConnection);
            dataDA.Fill(dataDS);
            int count = 0;
            GDGasGroup.Visible = false;
            GDSteamGroup.Visible = false;
            int countRow = dataDS.Tables[0].Rows.Count;

            if (countRow < 3)
            {
                foreach (DataRow MyRow in dataDS.Tables[0].Rows)
                {
                    if (count == 0)
                    {
                        GDGasGroup.Visible = true;
                        GDGasGroup.Text = MyRow["PackageType"].ToString().Trim();
                        string type = MyRow["PackageType"].ToString().Trim();
                        type = type.Trim();
                        if (type == "Combined Cycle") type = "CC";
                        PlantGV1.Visible = true;
                        DataSet MyDS = new DataSet();
                        SqlDataAdapter Myda = new SqlDataAdapter();
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageCode," +
                        "UnitType,Capacity,PMin,PMax FROM [UnitsDataMain] WHERE PPID=" + Num + "AND (PackageType LIKE '" + type + "%')", myConnection);
                        Myda.Fill(MyDS);
                        PlantGV1.DataSource = MyDS.Tables[0].DefaultView;
                        PlantGV1.Sort(PlantGV1.Columns[1], ListSortDirection.Ascending);
                        FillPlantGV1Remained(mydate, Num, type, myhour);
                        GDSteamGroup.Text = "";
                    }
                    else if (count == 1)
                    {
                        GDSteamGroup.Visible = true;
                        GDSteamGroup.Text = MyRow["PackageType"].ToString().Trim();
                        string type = MyRow["PackageType"].ToString().Trim();
                        if (type == "Combined Cycle") type = "CC";
                        PlantGV2.Visible = true;
                        DataSet SteamDS = new DataSet();
                        SqlDataAdapter Steamda = new SqlDataAdapter();
                        Steamda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageCode," +
                        "UnitType,Capacity,PMin,PMax FROM [UnitsDataMain] WHERE PPID=" + Num + "AND (PackageType LIKE '" + type + "%')", myConnection);
                        Steamda.Fill(SteamDS);
                        PlantGV2.DataSource = SteamDS.Tables[0].DefaultView;
                        PlantGV2.Sort(PlantGV2.Columns[1], ListSortDirection.Ascending);
                        FillPlantGV2Remained(mydate, Num, type, myhour);
                    }
                    count++;
                }
            }
            else
            {
                count = 0;
                while (count < 2)
                {
                    if (count == 0)
                    {
                        GDGasGroup.Visible = true;
                        GDGasGroup.Text = "Steam/Gas";
                        //string type = MyRow["PackageType"].ToString().Trim();
                        //type = type.Trim();
                        //if (type == "Combined Cycle") type = "CC";
                        PlantGV1.Visible = true;
                        DataSet MyDS = new DataSet();
                        SqlDataAdapter Myda = new SqlDataAdapter();
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageCode," +
                        "UnitType,Capacity,PMin,PMax FROM [UnitsDataMain] WHERE PPID=" + Num + "AND (PackageType Not LIKE '%CC%')", myConnection);
                        Myda.Fill(MyDS);
                        PlantGV1.DataSource = MyDS.Tables[0].DefaultView;
                        PlantGV1.Sort(PlantGV1.Columns[1], ListSortDirection.Ascending);
                        FillPlantGV1Remained(mydate, Num, "Steam", myhour);
                        GDSteamGroup.Text = "";
                    }
                    else if (count == 1)
                    {
                        GDSteamGroup.Visible = true;
                        GDSteamGroup.Text = "Combined Cycle";
                        string type = "CC";
                        PlantGV2.Visible = true;
                        DataSet SteamDS = new DataSet();
                        SqlDataAdapter Steamda = new SqlDataAdapter();
                        Steamda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageCode," +
                        "UnitType,Capacity,PMin,PMax FROM [UnitsDataMain] WHERE PPID=" + Num + "AND (PackageType LIKE '" + type + "%')", myConnection);
                        Steamda.Fill(SteamDS);
                        PlantGV2.DataSource = SteamDS.Tables[0].DefaultView;
                        PlantGV2.Sort(PlantGV2.Columns[1], ListSortDirection.Ascending);
                        FillPlantGV2Remained(mydate, Num, type, myhour);
                    }
                    count++;
                }
            }
            myConnection.Close();

            if (count == 1) GDSteamGroup.Visible = false;

            MRLabel1.Text = "On Units";
            MRLabel2.Text = "Power";
            Grid230.Visible = false;
            Grid400.Visible = false;
        }
        //----------------------------FillPlantGV1Remained---------------------------
        private void FillPlantGV1Remained(string mydate, string Num, string type, int myhour)
        {
            PlantGV1.Rows[PlantGV1.RowCount - 1].Cells[PlantGV1.ColumnCount - 1].ReadOnly = true;

            


            //for (int i = 0; i < 3; i++)
            //PlantGV1.Rows[PlantGV1.RowCount - 1].Cells[i].ReadOnly = false;
            for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
            {
                PlantGV1.Rows[i].Cells[PlantGV1.ColumnCount - 1].ReadOnly = false;


                //////////////////////detect unit name//////////////////////////

                string temp = PlantGV1.Rows[i].Cells[0].Value.ToString().Trim();
                temp = temp.ToLower();

                string ptypenum = "0";
                if (temp.Contains("cc") || temp.Contains("CC")) ptypenum = "1";
                if (Findcconetype(PPID)) ptypenum = "0";

                ///////////////////////////////////////////////////////////////////


                SqlCommand MyCom = new SqlCommand();

                MyCom.CommandText = "SELECT  @md1 =MaintenanceStartDate,@md2=MaintenanceEndDate,@mt=MaintenanceType " +
                "FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type SELECT  " +
                "@fd1 =SecondFuelStartDate,@fd2=SecondFuelEndDate FROM [ConditionUnit] WHERE PPID=@num AND " +
                "UnitCode=@uc AND PackageType=@type SELECT  @osd1 =OutServiceStartDate,@osd2=OutServiceEndDate " +
                "FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type SELECT @re=COUNT(PPID) " +
                "FROM [DetailFRM005] WHERE TargetMarketDate=@date AND PPID=@num AND Block=@block and PPType='" + ptypenum + "' AND Hour=@hour AND Required<>0" +
                "SELECT @sf=SecondFuel FROM [UnitsDataMain] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type";

                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                MyCom.Connection = myConnection;

                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = mydate;
                MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                MyCom.Parameters["@num"].Value = Num;
                MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);



               
                ////////////new//////////////////////////////////////////////////////
                //Detect Block For FRM005
               // string temp = unit;
                //temp = temp.ToLower();

                if (type == "CC")
                {
                    int x;
                    try
                    {
                        x = int.Parse(PPID);
                    }
                    catch (Exception e)
                    {
                        x = 0;
                    }
                    if (PPIDArray.Contains(x + 1))
                        if (PPIDType[PPIDArray.IndexOf(x + 1)].ToString() == "virtual") x++;
                    


                    //ccunitbase///////////////////////////////////////////
                    //temp = x + "-" + "C" + packagecode;

                    temp = temp.Replace("cc", "c");
                    string[] sp = temp.Split('c');
                    temp = sp[0].Trim() + sp[1].Trim();
                    if (temp.Contains("gas"))
                    {
                        temp = temp.Replace("gas", "G");

                    }
                    else
                    {
                        temp = temp.Replace("steam", "S");

                    }
                    temp = x + "-" + temp;
                    ///////////////////////////////////////////////////////
                }
                else if (temp.Contains("gas"))
                {
                    temp = temp.Replace("gas", "G");
                    temp = PPID + "-" + temp;
                }
                else
                {
                    temp = temp.Replace("steam", "S");
                    temp = PPID + "-" + temp;
                }


                ///////////////////////////////old///////////////////////////////////////////////
                //if (type == "CC")
                //{
                //    int x = int.Parse(Num);
                //    //is This a Plant with 2 package Type?
                //    //SqlCommand XCom = new SqlCommand();
                //    //XCom.Connection = myConnection;
                //    //XCom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id";
                //    //XCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                //    //XCom.Parameters["@id"].Value = PPID;
                //    //XCom.Parameters.Add("@result1", SqlDbType.Int);
                //    //XCom.Parameters["@result1"].Direction = ParameterDirection.Output;
                //    //XCom.ExecuteNonQuery();
                //    //int result1 = (int)XCom.Parameters["@result1"].Value;
                //    //if (result1 > 1) x++;
                //    if (PPIDArray.Contains(x + 1))
                //        if (PPIDType[PPIDArray.IndexOf(x + 1)].ToString() == "virtual") x++;

                //    //if ((x == 131) || (x == 144)) x++;
                //    temp = x + "-" + "C" + PlantGV1.Rows[i].Cells[1].Value.ToString().Trim();
                //}
                //else if (temp.Contains("gas"))
                //{
                //    temp = temp.Replace("gas", "G");
                //    temp = Num + "-" + temp;
                //}
                //else
                //{
                //    temp = temp.Replace("steam", "S");
                //    temp = Num + "-" + temp;
                //}
                ////////////////////////////////////////////////////////////////////////////



                MyCom.Parameters["@block"].Value = temp;
                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                MyCom.Parameters["@hour"].Value = myhour;
                MyCom.Parameters.Add("@uc", SqlDbType.NChar, 20);
                MyCom.Parameters["@uc"].Value = PlantGV1.Rows[i].Cells[0].Value.ToString().Trim();
                MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
                MyCom.Parameters["@type"].Value = type;

                MyCom.Parameters.Add("@md1", SqlDbType.Char, 10);
                MyCom.Parameters["@md1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@md2", SqlDbType.Char, 10);
                MyCom.Parameters["@md2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@mt", SqlDbType.NChar, 12);
                MyCom.Parameters["@mt"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@fd1", SqlDbType.Char, 10);
                MyCom.Parameters["@fd1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@fd2", SqlDbType.Char, 10);
                MyCom.Parameters["@fd2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@osd1", SqlDbType.Char, 10);
                MyCom.Parameters["@osd1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@osd2", SqlDbType.Char, 10);
                MyCom.Parameters["@osd2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@re", SqlDbType.Int);
                MyCom.Parameters["@re"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@sf", SqlDbType.NChar, 10);
                MyCom.Parameters["@sf"].Direction = ParameterDirection.Output;

                MyCom.ExecuteNonQuery();

                myConnection.Close();

                string md1, md2, fd1, fd2, osd1, osd2, sf, mt;
                md1 = MyCom.Parameters["@md1"].Value.ToString().Trim();
                md2 = MyCom.Parameters["@md2"].Value.ToString().Trim();
                fd1 = MyCom.Parameters["@fd1"].Value.ToString().Trim();
                fd2 = MyCom.Parameters["@fd2"].Value.ToString().Trim();
                osd1 = MyCom.Parameters["@osd1"].Value.ToString().Trim();
                osd2 = MyCom.Parameters["@osd2"].Value.ToString().Trim();
                sf = MyCom.Parameters["@sf"].Value.ToString().Trim();
                mt = MyCom.Parameters["@mt"].Value.ToString().Trim();
                int result = int.Parse(MyCom.Parameters["@re"].Value.ToString().Trim());

                if (result != 0) PlantGV1.Rows[i].Cells[7].Value = "ON";
                else PlantGV1.Rows[i].Cells[7].Value = "OFF";
                if (CheckDate(mydate, fd1, fd2)) PlantGV1.Rows[i].Cells[6].Value = sf;
                else PlantGV1.Rows[i].Cells[6].Value = "Gas";
                if (CheckDate(mydate, md1, md2)) PlantGV1.Rows[i].Cells[8].Value = mt;
                else PlantGV1.Rows[i].Cells[8].Value = "No";
                if (CheckDate(mydate, osd1, osd2)) PlantGV1.Rows[i].Cells[9].Value = "Yes";
                else PlantGV1.Rows[i].Cells[9].Value = "No";
            }

        }
        //----------------------------FillPlantGV2Remained---------------------------
        private void FillPlantGV2Remained(string mydate, string Num, string type, int myhour)
        {
            PlantGV2.Rows[PlantGV2.RowCount - 1].Cells[PlantGV2.ColumnCount - 1].ReadOnly = true;

            //for (int i = 0; i < 3; i++)
            //PlantGV2.Rows[PlantGV2.RowCount - 1].Cells[i].ReadOnly = false;
            for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
            {
                PlantGV2.Rows[i].Cells[PlantGV2.ColumnCount - 1].ReadOnly = false;

                //////////////////////detect unit name//////////////////////////

                string temp = PlantGV2.Rows[i].Cells[0].Value.ToString().Trim();
                temp = temp.ToLower();

                string ptypenum = "0";
                if (temp.Contains("cc") || temp.Contains("CC")) ptypenum = "1";
                if (Findcconetype(PPID)) ptypenum = "0";

                ///////////////////////////////////////////////////////////////////



                SqlCommand MyCom = new SqlCommand();

                MyCom.CommandText = "SELECT  @md1 =MaintenanceStartDate,@md2=MaintenanceEndDate,@mt=MaintenanceType " +
                "FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type SELECT  " +
                "@fd1 =SecondFuelStartDate,@fd2=SecondFuelEndDate FROM [ConditionUnit] WHERE PPID=@num AND " +
                "UnitCode=@uc AND PackageType=@type SELECT  @osd1 =OutServiceStartDate,@osd2=OutServiceEndDate " +
                "FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type SELECT @re=COUNT(PPID) " +
                "FROM [DetailFRM005] WHERE TargetMarketDate=@date AND PPID=@num AND Block=@block and PPType='" + ptypenum + "' AND Hour=@hour AND Required<>0" +
                "SELECT @sf=SecondFuel FROM [UnitsDataMain] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type";
                MyCom.Connection = new SqlConnection(ConnectionManager.ConnectionString);
                MyCom.Connection.Open();

                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = mydate;
                MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                MyCom.Parameters["@num"].Value = Num;
                MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);


                ////////////new//////////////////////////////////////////////////////
                //Detect Block For FRM005
                // string temp = unit;
                //temp = temp.ToLower();
                if (type == "CC")
                {
                    int x;
                    try
                    {
                        x = int.Parse(PPID);
                    }
                    catch (Exception e)
                    {
                        x = 0;
                    }
                    if (PPIDArray.Contains(x + 1))
                        if (PPIDType[PPIDArray.IndexOf(x + 1)].ToString() == "virtual") x++;



                    //ccunitbase///////////////////////////////////////////
                    //temp = x + "-" + "C" + packagecode;

                    temp = temp.Replace("cc", "c");
                    string[] sp = temp.Split('c');
                    temp = sp[0].Trim() + sp[1].Trim();
                    if (temp.Contains("gas"))
                    {
                        temp = temp.Replace("gas", "G");

                    }
                    else
                    {
                        temp = temp.Replace("steam", "S");

                    }
                    temp = x + "-" + temp;
                    ///////////////////////////////////////////////////////
                }
                else if (temp.Contains("gas"))
                {
                    temp = temp.Replace("gas", "G");
                    temp = PPID + "-" + temp;
                }
                else
                {
                    temp = temp.Replace("steam", "S");
                    temp = PPID + "-" + temp;
                }


                ////OLD/////////////////////////////////////////////////////////////////////
                //string temp = PlantGV2.Rows[i].Cells[0].Value.ToString();
                //temp = temp.ToLower();
                //if (type == "CC")
                //{
                //    int x = int.Parse(Num);
                //    //is This a Plant with 2 package Type?
                //    //SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                //    //myConnection.Open();
                //    //SqlCommand XCom = new SqlCommand();
                //    //XCom.Connection = myConnection;
                //    //XCom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id";
                //    //XCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                //    //XCom.Parameters["@id"].Value = PPID;
                //    //XCom.Parameters.Add("@result1", SqlDbType.Int);
                //    //XCom.Parameters["@result1"].Direction = ParameterDirection.Output;
                //    //XCom.ExecuteNonQuery();
                //    //int result1 = (int)XCom.Parameters["@result1"].Value;
                //    //if (result1 > 1) x++;
                //    if (PPIDArray.Contains(x + 1))
                //        if (PPIDType[PPIDArray.IndexOf(x + 1)].ToString() == "virtual") x++;
                //    //if ((x == 131) || (x == 144)) x++;
                //    temp = x + "-" + "C" + PlantGV2.Rows[i].Cells[1].Value.ToString();
                //    //myConnection.Close();
                //}
                //else if (temp.Contains("gas"))
                //{
                //    temp = temp.Replace("gas", "G");
                //    temp = Num + "-" + temp;
                //}
                //else
                //{
                //    temp = temp.Replace("steam", "S");
                //    temp = Num + "-" + temp;
                //}

                MyCom.Parameters["@block"].Value = temp;
                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                MyCom.Parameters["@hour"].Value = myhour;
                MyCom.Parameters.Add("@uc", SqlDbType.NChar, 20);
                MyCom.Parameters["@uc"].Value = PlantGV2.Rows[i].Cells[0].Value.ToString();
                MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
                MyCom.Parameters["@type"].Value = type;

                MyCom.Parameters.Add("@md1", SqlDbType.Char, 10);
                MyCom.Parameters["@md1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@md2", SqlDbType.Char, 10);
                MyCom.Parameters["@md2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@mt", SqlDbType.NChar, 12);
                MyCom.Parameters["@mt"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@fd1", SqlDbType.Char, 10);
                MyCom.Parameters["@fd1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@fd2", SqlDbType.Char, 10);
                MyCom.Parameters["@fd2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@osd1", SqlDbType.Char, 10);
                MyCom.Parameters["@osd1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@osd2", SqlDbType.Char, 10);
                MyCom.Parameters["@osd2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@re", SqlDbType.Int);
                MyCom.Parameters["@re"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@sf", SqlDbType.NChar, 10);
                MyCom.Parameters["@sf"].Direction = ParameterDirection.Output;

                MyCom.ExecuteNonQuery();

                MyCom.Connection.Close();

                string md1, md2, fd1, fd2, osd1, osd2, sf, mt;
                md1 = MyCom.Parameters["@md1"].Value.ToString().Trim();
                md2 = MyCom.Parameters["@md2"].Value.ToString().Trim();
                fd1 = MyCom.Parameters["@fd1"].Value.ToString().Trim();
                fd2 = MyCom.Parameters["@fd2"].Value.ToString().Trim();
                osd1 = MyCom.Parameters["@osd1"].Value.ToString().Trim();
                osd2 = MyCom.Parameters["@osd2"].Value.ToString().Trim();
                sf = MyCom.Parameters["@sf"].Value.ToString().Trim();
                mt = MyCom.Parameters["@mt"].Value.ToString().Trim();
                int result = int.Parse(MyCom.Parameters["@re"].Value.ToString().Trim());

                if (result != 0) PlantGV2.Rows[i].Cells[7].Value = "ON";
                else PlantGV2.Rows[i].Cells[7].Value = "OFF";
                if (CheckDate(mydate, fd1, fd2)) PlantGV2.Rows[i].Cells[6].Value = sf;
                else PlantGV2.Rows[i].Cells[6].Value = "Gas";
                if (CheckDate(mydate, md1, md2)) PlantGV2.Rows[i].Cells[8].Value = mt;
                else PlantGV2.Rows[i].Cells[8].Value = "No";
                if (CheckDate(mydate, osd1, osd2)) PlantGV2.Rows[i].Cells[9].Value = "Yes";
                else PlantGV2.Rows[i].Cells[9].Value = "No";
            }
        }
        //-----------------------------ClearGDUnitTab()------------------------------
        private void ClearGDUnitTab()
        {
            GDcapacityTB.Text = "";
            GDPmaxTB.Text = "";
            GDPminTB.Text = "";
            GDTimeUpTB.Text = "";
            GDTimeDownTB.Text = "";
            GDTimeColdStartTB.Text = "";
            GDTimeHotStartTB.Text = "";
            GDRampRateTB.Text = "";
            GDPrimaryFuelTB.Text = "";
            GDSecondaryFuelTB.Text = "";
            GDStateTB.Text = "";
            GDFuelTB.Text = "";
            GDMainTypeTB.Text = "";
            GDPowerSerialTb.Text = "";
            GDpowertransserialtb.Text = "";
            GDConsumedSerialTb.Text = "";
            GDStartDateCal.IsNull = true;
            GDEndDateCal.IsNull = true;

            txtMaintCIDur.Text = "";
            txtMaintCIHours.Text = "";
            txtMaintHGPDur.Text = "";
            txtMaintHGPHours.Text = "";
            txtMaintMODur.Text = "";
            txtMaintMOHours.Text = "";
            txtDurabilityHours.Text = "";
            datePickerDurability.Text = "";
            cmbMaintLastMaintType.Items.Clear();
        }
        //-------------------------FillGDUnit----------------------------------------
        private void FillGDUnit(string unit, string package, int index)
        {
            txtMaintHGPDur.BackColor = Color.White;
            txtMaintHGPHours.BackColor = Color.White;

            ClearGDUnitTab();
            cmbMaintLastMaintType.Items.Add("");
            cmbMaintLastMaintType.Items.Add("CI");
            if (unit.ToLower().Contains("steam"))
            {
                txtMaintHGPDur.Text = "";
                txtMaintHGPDur.Enabled = false;
                txtMaintHGPDur.BackColor = Color.Gray;

                txtMaintHGPHours.Text = "";
                txtMaintHGPHours.Enabled = false;
                txtMaintHGPHours.BackColor = Color.Gray;
            }
            else
            {
                txtMaintHGPDur.Enabled = true;
                txtMaintHGPHours.Enabled = true;
                cmbMaintLastMaintType.Items.Add("HGP");
            }

            cmbMaintLastMaintType.Items.Add("MO");
            string ABpackage = package;
            if (ABpackage.Contains("Combined"))
                ABpackage = "CC";

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT TUp,TDown,TStartCold,TStartHot," +
            "RampUpRate,HeatValuePrimaryFuel,HeatValueSecondaryFuel,PowerSerial,ConsumedSerial,Avc FROM [UnitsDataMain] WHERE PPID=" + PPID + " AND UnitCode LIKE '" + unit + "%'", myConnection);
            Myda.Fill(MyDS);
            foreach (DataRow MyRow in MyDS.Tables[0].Rows)
            {
                GDTimeUpTB.Text = MyRow[0].ToString().Trim();
                GDTimeDownTB.Text = MyRow[1].ToString().Trim();
                GDTimeColdStartTB.Text = MyRow[2].ToString().Trim();
                GDTimeHotStartTB.Text = MyRow[3].ToString().Trim();
                GDRampRateTB.Text = MyRow[4].ToString().Trim();
                GDPrimaryFuelTB.Text = MyRow[5].ToString().Trim();
                GDSecondaryFuelTB.Text = MyRow[6].ToString().Trim();
                GDPowerSerialTb.Text = MyRow[7].ToString().Trim();
                GDConsumedSerialTb.Text = MyRow[8].ToString().Trim();
                GDtxtAvc.Text = MyRow[9].ToString().Trim();
            }

            //Detect Farsi Date
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");
            int myhour = DateTime.Now.Hour;

            //SqlCommand MyCom = new SqlCommand();
            //MyCom.Connection = MyConnection;
            //MyCom.CommandText = "SELECT @IntCon=InternalConsume FROM EconomicUnit WHERE PPID=@num AND " +
            //"UnitCode=@unit AND Date=@date";
            //MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            //MyCom.Parameters["@num"].Value = PPID;
            //MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
            //string myunit = unit;
            //if (package.Contains("Combined"))
            //{
            //    string packagecode = "";
            //    if (GDSteamGroup.Text.Contains(package))
            //        packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
            //    else packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
            //    myunit = "C" + packagecode;
            //}
            //MyCom.Parameters["@unit"].Value = myunit;
            //MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            //MyCom.Parameters["@date"].Value = mydate;
            //MyCom.Parameters.Add("@IntCon", SqlDbType.Real);
            //MyCom.Parameters["@IntCon"].Direction = ParameterDirection.Output;
            ////try
            ////{
            //MyCom.ExecuteNonQuery();
            ////}
            ////catch (Exception exp)
            ////{
            ////  string str = exp.Message;
            ////}
            if (package.Contains("Combined"))
                package = "Combined Cycle";
            if (GDGasGroup.Text.Contains(package))
            {
                GDcapacityTB.Text = PlantGV1.Rows[index].Cells[3].Value.ToString();
                GDPminTB.Text = PlantGV1.Rows[index].Cells[4].Value.ToString().Trim();
                GDPmaxTB.Text = PlantGV1.Rows[index].Cells[5].Value.ToString().Trim();
                //GDStateTB.Text = PlantGV1.Rows[index].Cells[7].Value.ToString();
                //GDFuelTB.Text = PlantGV1.Rows[index].Cells[6].Value.ToString();
                //GDMainTypeTB.Text = PlantGV1.Rows[index].Cells[8].Value.ToString();
            }
            else if (GDSteamGroup.Text.Contains(package))
            {
                GDcapacityTB.Text = PlantGV2.Rows[index].Cells[3].Value.ToString();
                GDPminTB.Text = PlantGV2.Rows[index].Cells[4].Value.ToString();
                GDPmaxTB.Text = PlantGV2.Rows[index].Cells[5].Value.ToString().Trim();
                GDStateTB.Text = PlantGV2.Rows[index].Cells[7].Value.ToString().Trim();
                //GDFuelTB.Text = PlantGV2.Rows[index].Cells[6].Value.ToString();
                //GDMainTypeTB.Text = PlantGV2.Rows[index].Cells[8].Value.ToString();
            }


            string pType = package;
            if (package.Contains("Combined"))
                pType = "CC";

            string command = "SELECT * FROM [MaintenanceGeneralData] WHERE " +
                "PPID=" + PPID +
            " AND UnitCode='" + unit + "'" +
                " AND PackageType='" + pType + "'";
            DataTable res = Utilities.GetTable(command);
            if (res.Rows.Count > 0)
            {
                DataRow row = res.Rows[0];
                txtMaintCIDur.Text = row["CIDuration"].ToString().Trim();
                txtMaintCIHours.Text = row["CIHour"].ToString().Trim();
                txtMaintHGPDur.Text = row["HGPDuration"].ToString().Trim();
                txtMaintHGPHours.Text = row["HGPHour"].ToString().Trim();
                txtMaintMODur.Text = row["MODuration"].ToString().Trim();
                txtMaintMOHours.Text = row["MOHour"].ToString().Trim();
                txtDurabilityHours.Text = row["DurabilityHours"].ToString().Trim();

                txtMaintFuelFactor.Text = row["FuelFactor"].ToString().Trim();
                txtMaintStartUpFactor.Text = row["startupfactor"].ToString().Trim();
                txtMaintLastHour.Text = row["lastMaintHour"].ToString().Trim();
                cmbMaintLastMaintType.Text = row["lastMaintType"].ToString().Trim();


                try
                {
                    datePickerDurability.SelectedDateTime = PersianDateConverter.ToGregorianDateTime(row["DurabilityDateTime"].ToString());
                }
                catch
                { }
            }
            //////////////////////////////////////////
            ////////////??????????????? Hamidi Nemikhad??????????????
            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            MyCom.CommandText = "SELECT  @m=Maintenance, @md1 =MaintenanceStartDate,@md2=MaintenanceEndDate " +
                "FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@unit AND PackageType=@package";
            MyCom.Parameters.Add("@package", SqlDbType.NChar, 10);
            string type = package;
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPID;
            MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
            string myunit = unit;
            if (package.Contains("Combined"))
            {
                type = "CC";
                string packagecode = "";
                if (GDSteamGroup.Text.Contains(package))
                    packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                else packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                myunit = "C" + packagecode;
            }
            MyCom.Parameters["@unit"].Value = myunit;
            MyCom.Parameters["@package"].Value = type;
            MyCom.Parameters.Add("@m", SqlDbType.Bit);
            MyCom.Parameters["@m"].Direction = ParameterDirection.Output;
            MyCom.Parameters.Add("@md1", SqlDbType.Char, 10);
            MyCom.Parameters["@md1"].Direction = ParameterDirection.Output;
            MyCom.Parameters.Add("@md2", SqlDbType.Char, 10);
            MyCom.Parameters["@md2"].Direction = ParameterDirection.Output;
            //try
            //{
            MyCom.ExecuteNonQuery();
            //}
            //catch (Exception exp)
            //{
            //  string str = exp.Message;
            //}

            if (MyCom.Parameters["@m"].Value.ToString().Trim() != "")
                if (bool.Parse(MyCom.Parameters["@m"].Value.ToString()))
                {
                    GDStartDateCal.Text = MyCom.Parameters["@md1"].Value.ToString().Trim();
                    GDEndDateCal.Text = MyCom.Parameters["@md2"].Value.ToString().Trim();
                }


            myConnection.Close();
            DisableUnitTab(true, "");
        }

        //----------------------------------FillGDLine---------------------------------------
        private void FillGDLine(string TransType, string TransLine)
        {
            ClearGDUnitTab();

           

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT FromBus,ToBus,CircuitID,LineR,LineX,LineSuseptance," +
            "LineLength,OutService,OutServiceStartDate,OutServiceEndDate,Owner_GencoFrom,Owner_GencoTo,PercentOwnerGencoFrom," +
            "PercentOwnerGencoTo,RateA,OutServiceType,PowerSerialtrans FROM [Lines] WHERE LineNumber=@num AND LineCode=@code", myConnection);
            Myda.SelectCommand.Parameters.Add("@num", SqlDbType.Int);
            Myda.SelectCommand.Parameters["@num"].Value = TransType;
            Myda.SelectCommand.Parameters.Add("@code", SqlDbType.NChar, 20);

            Myda.SelectCommand.Parameters["@code"].Value = TransLine;

            Myda.Fill(MyDS);
            myConnection.Close();
            GDOutServiceCheck.Checked = false;
            foreach (DataRow MyRow in MyDS.Tables[0].Rows)
            {
                GDcapacityTB.Text = MyRow[14].ToString().Trim();

                DataTable dt1 = Utilities.GetTable("select VoltageLevel from dbo.PostVoltage where VoltageNumber='" + MyRow[0].ToString().Trim()+ "'");
                if (dt1.Rows.Count > 0)
                {
                    GDPmaxTB.Text = dt1.Rows[0][0].ToString().Trim();
                }
                else
                    GDPmaxTB.Text = MyRow[0].ToString().Trim().Trim();

                DataTable dt2 = Utilities.GetTable("select VoltageLevel from dbo.PostVoltage where VoltageNumber='" + MyRow[1].ToString().Trim() + "'");
                if (dt2.Rows.Count > 0)
                {
                    GDPminTB.Text = dt2.Rows[0][0].ToString().Trim();
                }
                else

                   GDPminTB.Text = MyRow[1].ToString().Trim().Trim();


                GDTimeUpTB.Text = MyRow[2].ToString().Trim();
                GDTimeDownTB.Text = MyRow[3].ToString().Trim();
                GDTimeColdStartTB.Text = MyRow[4].ToString().Trim();
                GDTimeHotStartTB.Text = MyRow[5].ToString().Trim();
                GDRampRateTB.Text = MyRow[6].ToString().Trim();
                GDpowertransserialtb.Text = MyRow[16].ToString().Trim();
                GDPrimaryFuelTB.Text = "Work";
                string temp = MyRow[7].ToString().Trim();
                //IS it OutService?
                if (MyRow[7].ToString() == "True")
                {
                    //Detect Farsi Date

                    PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
                    string mydate = prDate.ToString("d");
                    int myhour = DateTime.Now.Hour;

                    ///////////?????????????????????????
                    GDOutServiceCheck.Checked = true;
                    GDStartDateCal.Text = MyRow[8].ToString().Trim();
                    if (MyRow[8].ToString().Trim() != "")
                        GDStartDateCal.IsNull = false;
                    GDEndDateCal.Text = MyRow[9].ToString().Trim();
                    if (MyRow[9].ToString().Trim() != "")
                        GDEndDateCal.IsNull = false;
                    if (CheckDate(mydate, GDStartDateCal.Text, GDEndDateCal.Text))
                        GDPrimaryFuelTB.Text = "OutService";
                    GDMainTypeTB.Text = MyRow[15].ToString().Trim();
                }
                string from = MyRow[10].ToString().ToLower();
                string to = MyRow[11].ToString().ToLower();
                if (!from.Contains(GenName))
                {
                    GDStateTB.Text = MyRow[10].ToString().Trim();
                    GDFuelTB.Text = MyRow[12].ToString().Trim();
                }
                else if (!to.Contains(GenName))
                {
                    GDStateTB.Text = MyRow[11].ToString().Trim();
                    GDFuelTB.Text = MyRow[13].ToString().Trim();
                }
                else
                {
                    GDStateTB.Text = GenName;
                    GDFuelTB.Text = "100";
                }

            }
            UpdateLineTab(true);
        }

        
        private void GDOutServiceCheck_CheckedChanged(object sender, EventArgs e)
        {
            GDMainTypeTB.ReadOnly = !(GDOutServiceCheck.Checked);
            GDStartDateCal.Enabled = GDOutServiceCheck.Checked;
            GDEndDateCal.Enabled = GDOutServiceCheck.Checked;
            if (!GDOutServiceCheck.Checked)
            {
                GDMainTypeTB.Text = "";
                GDStartDateCal.IsNull = true;
                GDEndDateCal.IsNull = true;
            }
        }

        //--------------------------------UpdateLineTab-------------------------------------
        private void UpdateLineTab(bool Linestatus)
        {
            GDNewBtn.Enabled = !Linestatus;
            GDOutServiceCheck.Enabled = !Linestatus;
            GDPrimaryFuelTB.ReadOnly = true;
        }

        //----------------------------------------------------------------------------------
        private void DeletePostBtn_Click(object sender, EventArgs e)
        {
            DataTable del1 = Utilities.GetTable("delete FROM PostVoltage Where PostNumber='" + postnumlb.Text.Trim() + "' and VoltageLevel ='" + voltagelevellb.Text.Trim() + "' and VoltageNumber='" + VolNumLb.Text.Trim() + "'");
            DataTable del2 = Utilities.GetTable("delete FROM PowerPost Where  VoltageNumber='" + VolNumLb.Text.Trim() + "'");
            DataTable del3 = Utilities.GetTable("delete FROM ConsumedPost Where  VoltageNumber='" + VolNumLb.Text.Trim() + "'");
            buildTreeView1();
            buildPostTree(postnumlb.Text.Trim());
        }

        private void AddConsumBtn_Click(object sender, EventArgs e)
        {

            if (txtselectconsum.Text != "")
            {

                string firstCommand = "insert into ConsumedPost (ConsumedSerial,VoltageNumber)";

                string secondCommand = "values ('" + txtselectconsum.Text.Trim() + "','" + VolNumLb.Text.Trim() + "')";

                DataTable inserttable = Utilities.GetTable(firstCommand + secondCommand);
            }
            else MessageBox.Show("Fill insert Consume");

            DeleteConsumeList.Items.Clear();
            DataTable availableConsume = Utilities.GetTable("Select ConsumedSerial From ConsumedPost where VoltageNumber='" + VolNumLb.Text + "'order by ConsumedSerial asc");
            foreach (DataRow MyRow in availableConsume.Rows)
            {
                DeleteConsumeList.Items.Add(MyRow[0].ToString().Trim());
            }
            txtselectconsum.Text = "";
        }

        private void DeleteConsumBtn_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DeleteConsumeList.Items.Count; i++)
            {
                if (DeleteConsumeList.GetItemChecked(i))
                {
                    DataTable deletetb = Utilities.GetTable("DELETE FROM ConsumedPost Where ConsumedSerial='" + DeleteConsumeList.Items[i].ToString() + "'and VoltageNumber='" + VolNumLb.Text + "'");
                }
            }
            DeleteConsumeList.Items.Clear();
            DataTable availableConsume = Utilities.GetTable("Select ConsumedSerial From ConsumedPost where VoltageNumber='" + VolNumLb.Text + "'order by ConsumedSerial asc");
            foreach (DataRow MyRow in availableConsume.Rows)
            {
                DeleteConsumeList.Items.Add(MyRow[0].ToString().Trim());
            }
        }

        private void AddPowerBtn_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < AddPowerList.Items.Count; i++)
            {
                if (AddPowerList.GetItemChecked(i))
                {
                    string firstCommand = "insert into PowerPost (PowerSerial,VoltageNumber)";

                    string secondCommand = "values ('" + AddPowerList.Items[i].ToString() + "','" + VolNumLb.Text.Trim() + "')";

                    DataTable addtb = Utilities.GetTable(firstCommand + secondCommand);
                }
            }

            DeletePowerList.Items.Clear();
            DataTable availablePower = Utilities.GetTable("Select PowerSerial From PowerPost where VoltageNumber='" + VolNumLb.Text + "'order by PowerSerial asc");
            foreach (DataRow MyRow in availablePower.Rows)
            {
                DeletePowerList.Items.Add(MyRow[0].ToString().Trim());
            }
            AddPowerList.Items.Clear();
            //DataTable PowerPost = utilities.GetTable("select distinct PPCode from dbo.RegionNetComp where Code='R03' and PPCode Not in (Select PowerSerial From PowerPost where VoltageNumber='" + VolNumLb.Text + "')order by PPCode asc");
            //foreach (DataRow MyRow in PowerPost.Rows)
            //{
                
            //   AddPowerList.Items.Add(MyRow[0].ToString().Trim());
                
            //}
            DataTable ss = Utilities.GetTable("select distinct ppcode from dbo.RegionNetComp where code='" + gencode + "'");

            string x = "";
            foreach (DataRow m in ss.Rows)
            {
                if (Findpackplant(m[0].ToString().Trim()))
                {
                    DataTable oDat = Utilities.GetTable("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + m[0].ToString().Trim() + "'");
                    for (int i = 0; i < oDat.Rows.Count; i++)
                    {
                        x += m[0].ToString().Trim() + "-" + oDat.Rows[i][0].ToString().Substring(0, 1).Trim() + ",";

                    }
                }
                else
                {
                    x += m[0].ToString().Trim() + ",";
                }
            }
            string[] name = x.Split(',');
          
            DataTable PowerPost = Utilities.GetTable("Select PowerSerial From PowerPost where VoltageNumber='" + VolNumLb.Text + "'");
            for (int i = 0; i < name.Length - 1; i++)
            {
                if (PowerPost.Rows.Count > 0)
                {
                    bool enter = true;
                    foreach (DataRow MyRow in PowerPost.Rows)
                    {

                        if (name[i] == MyRow[0].ToString().Trim())
                        {
                            enter = false;
                            break;
                        }

                    }
                    if (enter)
                        AddPowerList.Items.Add(name[i]);
                }
                else
                    AddPowerList.Items.Add(name[i]);
            }
        }


        private void DeletePowerBtn_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DeletePowerList.Items.Count; i++)
            {
                if (DeletePowerList.GetItemChecked(i))
                {
                    DataTable deletetb = Utilities.GetTable("DELETE FROM PowerPost Where PowerSerial='" + DeletePowerList.Items[i].ToString() + "'and VoltageNumber='" + VolNumLb.Text.Trim() + "'");
                }
            }
            DeletePowerList.Items.Clear();
            DataTable availablePower = Utilities.GetTable("Select PowerSerial From PowerPost where VoltageNumber='" + VolNumLb.Text + "'order by PowerSerial asc");
            foreach (DataRow MyRow in availablePower.Rows)
            {
                DeletePowerList.Items.Add(MyRow[0].ToString().Trim());
            }
            AddPowerList.Items.Clear();
            //string[] name = new string[4];
            //name[0] = "203";
            //name[1] = "206-G";
            //name[2] = "206-S";
            //name[3] = "232";
            DataTable ss = Utilities.GetTable("select distinct ppcode from dbo.RegionNetComp where code='" + gencode + "'");

            string x = "";
            foreach (DataRow m in ss.Rows)
            {
                if (Findpackplant(m[0].ToString().Trim()))
                {
                    DataTable oDat = Utilities.GetTable("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + m[0].ToString().Trim() + "'");
                    for (int i = 0; i < oDat.Rows.Count; i++)
                    {
                        x += m[0].ToString().Trim() + "-" + oDat.Rows[i][0].ToString().Substring(0, 1).Trim() + ",";

                    }
                }
                else
                {
                    x += m[0].ToString().Trim() + ",";
                }
            }
            string[] name = x.Split(',');

            DataTable PowerPost = Utilities.GetTable("Select PowerSerial From PowerPost where VoltageNumber='" + VolNumLb.Text + "'");
            for (int i = 0; i < name.Length-1; i++)
            {
                if (PowerPost.Rows.Count > 0)
                {
                    bool enter = true;
                    foreach (DataRow MyRow in PowerPost.Rows)
                    {

                        if (name[i] == MyRow[0].ToString().Trim())
                        {
                            enter = false;
                            break;
                        }

                    }
                    if (enter)
                        AddPowerList.Items.Add(name[i]);
                }
                else
                    AddPowerList.Items.Add(name[i]);
            }

        }


        private void radioEditPost_CheckedChanged(object sender, EventArgs e)
        {
            if (radioEditPost.Checked)
            {
                txtaddvoltagenum.Enabled = false;
                txtaddvoltagenum.Text = VolNumLb.Text.Trim();
                txtaddpostnum.Text = "";
            }
        }

        private void radioAddpost_CheckedChanged(object sender, EventArgs e)
        {
            if (radioAddpost.Checked)
            {
                txtaddvoltagenum.Enabled = true;


                int nummax = 0;
                DataTable max = Utilities.GetTable("select VoltageNumber from dbo.PostVoltage");
                foreach (DataRow mrow in max.Rows)
                {

                    if (nummax < int.Parse(mrow[0].ToString()))
                        nummax = int.Parse(mrow[0].ToString());


                }
                txtaddvoltagenum.Text = (nummax + 1).ToString();
                txtaddpostnum.Text = postnumlb.Text.Trim();
            }
        }

        private void BtnOKpost_Click(object sender, EventArgs e)
        {
            if (radioAddpost.Checked)
            {

                if (txtaddvoltagenum.Text != "" && txtaddpostnum.Text != "" && txtpostname.Text != ""
                    && errorProvider1.GetError(txtaddvoltagenum) == "" && errorProvider1.GetError(txtaddpostnum) == "")
                {

                    DataTable check = Utilities.GetTable("select *  FROM PostVoltage Where PostNumber='" + txtaddpostnum.Text.Trim() + "' and VoltageLevel ='" + txtpostname.Text.Trim() + "'");
                    if (check.Rows.Count == 0)
                    {
                        string firstCommand = "insert into PostVoltage (VoltageNumber,PostNumber,VoltageLevel)";

                        string secondCommand = "values ('" + txtaddvoltagenum.Text.Trim() + "','" + txtaddpostnum.Text.Trim() + "','" + txtpostname.Text.Trim() + "')";

                        DataTable inserttable = Utilities.GetTable(firstCommand + secondCommand);
                        if (inserttable != null)
                        {

                            buildTreeView1();
                            buildPostTree(txtaddpostnum.Text.Trim());
                            FillPostVoltage(postnumlb.Text.Trim(), voltagelevellb.Text.Trim());

                        }
                        else
                        {
                            txtaddvoltagenum.Text = "";
                            txtpostname.Text = "";
                            txtaddpostnum.Text = "";
                            MessageBox.Show(" Post Data is Availbale ");

                        }

                    }
                    else
                    {
                        txtaddvoltagenum.Text = "";
                        txtpostname.Text = "";
                        txtaddpostnum.Text = "";
                        MessageBox.Show(" Post Data is Availbale ");

                    }
                    //errorProvider1.Clear();
                }

            }
            else if (radioEditPost.Checked)
            {
                if (txtaddvoltagenum.Text != "" && txtpostname.Text != "" && txtaddpostnum.Text != "" && errorProvider1.GetError(txtaddpostnum) == "")
                {

                    DataTable check = Utilities.GetTable("select *  FROM PostVoltage Where VoltageNumber='" + VolNumLb.Text.Trim() + "'");
                    if (check.Rows.Count == 1)
                    {

                        string firstCommand = "update dbo.PostVoltage set VoltageNumber='" + txtaddvoltagenum.Text.Trim() + "',PostNumber='" + txtaddpostnum.Text.Trim() + "',VoltageLevel='" + txtpostname.Text.Trim() + "'where VoltageNumber='" + VolNumLb.Text.Trim() + "'";

                        DataTable inserttable = Utilities.GetTable(firstCommand);
                        if (inserttable != null)
                        {


                            buildTreeView1();
                            buildPostTree(txtaddpostnum.Text.Trim());
                            FillPostVoltage(txtaddpostnum.Text.Trim(), txtpostname.Text.Trim());
                            txtaddvoltagenum.Text = "";
                            txtpostname.Text = "";
                            txtaddpostnum.Text = "";
                        }
                        else
                        {
                            txtaddvoltagenum.Text = "";
                            txtpostname.Text = "";
                            txtaddpostnum.Text = "";
                            MessageBox.Show(" Post Data is Availbale ");

                        }

                    }
                    else
                    {
                        txtaddvoltagenum.Text = "";
                        txtpostname.Text = "";
                        txtaddpostnum.Text = "";
                        MessageBox.Show(" Post Data is Availbale ");

                    }
                    // errorProvider1.Clear();

                }

                txtaddvoltagenum.ReadOnly = false;


            }

        }

        //////////////////////tool tip unit & line ////////////////////////////////
        private void GDcapacityTB_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(GDcapacityTB, "MW");
        }

        private void GDcapacityTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GDcapacityTB, "");
        }

        private void GDPmaxTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GDPmaxTB, "");
        }

        private void GDPmaxTB_MouseClick(object sender, MouseEventArgs e)
        {
            if (GDPmaxLb.Text == "From Bus")
            {
                toolTip1.SetToolTip(GDPmaxTB, "Name");

            }
            else
            {
                toolTip1.SetToolTip(GDPmaxTB, "MW");

            }
        }

        private void GDPminTB_MouseClick(object sender, MouseEventArgs e)
        {
            if (GDPminLb.Text == "To Bus")
            {

                toolTip1.SetToolTip(GDPminTB, "Name");
            }
            else
            {
                toolTip1.SetToolTip(GDPminTB, "MW");
            }
        }

        private void GDPminTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GDPminTB, "");
        }

        private void GDTimeUpTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GDTimeUpTB, "");
        }

        private void GDTimeUpTB_MouseClick(object sender, MouseEventArgs e)
        {
            if (GDTUpLb.Text == "Circuit ID")
            {
                toolTip1.SetToolTip(GDTimeUpTB, "Number");
            }
            else
            {
                toolTip1.SetToolTip(GDTimeUpTB, "Hour");
            }
        }

        private void GDTimeDownTB_MouseClick(object sender, MouseEventArgs e)
        {
            if (GDTimeDownLb.Text == "    Line-R")
            {
                toolTip1.SetToolTip(GDTimeDownTB, "Ohm/Km");
            }
            else
            {
                toolTip1.SetToolTip(GDTimeDownTB, "Hour");
            }
        }

        private void GDTimeDownTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GDTimeDownTB, "Hour");
        }

        private void GDTimeColdStartTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GDTimeColdStartTB, "");
        }

        private void GDTimeColdStartTB_MouseClick(object sender, MouseEventArgs e)
        {
            if (GDColdLb.Text == "     Line-X")
            {
                toolTip1.SetToolTip(GDTimeColdStartTB, "Ohm/Km");
            }
            else
            {
                toolTip1.SetToolTip(GDTimeColdStartTB, "Hour");
            }
        }

        private void GDTimeHotStartTB_MouseClick(object sender, MouseEventArgs e)
        {
            if (GDHotLb.Text == "Suseptance")
            {
                toolTip1.SetToolTip(GDTimeHotStartTB, "");
            }
            else
            {
                toolTip1.SetToolTip(GDTimeHotStartTB, "Hour");
            }
        }

        private void GDTimeHotStartTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GDTimeHotStartTB, "");
        }

        private void GDRampRateTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GDRampRateTB, "");
        }

        private void GDRampRateTB_MouseClick(object sender, MouseEventArgs e)
        {
            if (GDRampLb.Text == "Line Length")
            {
                toolTip1.SetToolTip(GDRampRateTB, "Km");
            }
            else
            {

                toolTip1.SetToolTip(GDRampRateTB, "MW/MIN");
            }
        }

        private void GDPrimaryFuelTB_MouseClick(object sender, MouseEventArgs e)
        {
            if (GDConsLb.Text == "         Status")
            {
                toolTip1.SetToolTip(GDPrimaryFuelTB, "Name");

            }
            else
            {
                toolTip1.SetToolTip(GDPrimaryFuelTB, "Btu");
            }
        }

        private void GDPrimaryFuelTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GDPrimaryFuelTB, "");
        }

        private void GDSecondaryFuelTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GDSecondaryFuelTB, "");
        }

        private void GDSecondaryFuelTB_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(GDSecondaryFuelTB, "Btu");
        }

        private void GDtxtAvc_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(GDtxtAvc, "Rial/MW");
        }

        private void GDtxtAvc_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GDtxtAvc, "");
        }

        private void txtMaintCIDur_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtMaintCIDur, "");
        }

        private void txtMaintCIDur_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(txtMaintCIDur, "Day");
        }

        private void txtMaintHGPDur_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(txtMaintHGPDur, "Day");
        }

        private void txtMaintHGPDur_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtMaintHGPDur, "");
        }

        private void txtMaintMODur_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtMaintMODur, "");
        }

        private void txtMaintMODur_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(txtMaintMODur, "Day");
        }

        private void GDStateTB_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(GDStateTB, "Name");
        }

        private void GDStateTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GDStateTB, "");
        }

        private void GDFuelTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GDFuelTB, "");
        }

        private void GDFuelTB_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(GDFuelTB, "%");
        }


    }
}