﻿namespace PowerPlantProject
{
    partial class FRPlot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Dundas.Charting.WinControl.ChartArea chartArea1 = new Dundas.Charting.WinControl.ChartArea();
            Dundas.Charting.WinControl.Legend legend1 = new Dundas.Charting.WinControl.Legend();
            Dundas.Charting.WinControl.Series series1 = new Dundas.Charting.WinControl.Series();
            Dundas.Charting.WinControl.Series series2 = new Dundas.Charting.WinControl.Series();
            Dundas.Charting.WinControl.Series series3 = new Dundas.Charting.WinControl.Series();
            Dundas.Charting.WinControl.Title title1 = new Dundas.Charting.WinControl.Title();
            Dundas.Charting.WinControl.ChartArea chartArea2 = new Dundas.Charting.WinControl.ChartArea();
            Dundas.Charting.WinControl.Legend legend2 = new Dundas.Charting.WinControl.Legend();
            Dundas.Charting.WinControl.Series series4 = new Dundas.Charting.WinControl.Series();
            Dundas.Charting.WinControl.Title title2 = new Dundas.Charting.WinControl.Title();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRPlot));
            this.FRChart1 = new Dundas.Charting.WinControl.Chart();
            this.FRChart2 = new Dundas.Charting.WinControl.Chart();
            this.DailyRadio = new System.Windows.Forms.RadioButton();
            this.MonthlyRadio = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.FRChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRChart2)).BeginInit();
            this.SuspendLayout();
            // 
            // FRChart1
            // 
            this.FRChart1.AlwaysRecreateHotregions = true;
            this.FRChart1.BackGradientEndColor = System.Drawing.Color.LightSteelBlue;
            this.FRChart1.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft;
            this.FRChart1.BorderLineColor = System.Drawing.Color.LightSlateGray;
            this.FRChart1.BorderLineStyle = Dundas.Charting.WinControl.ChartDashStyle.Solid;
            this.FRChart1.BorderSkin.FrameBackColor = System.Drawing.Color.SteelBlue;
            this.FRChart1.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.LightBlue;
            this.FRChart1.BorderSkin.FrameBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.FRChart1.BorderSkin.FrameBorderWidth = 2;
            this.FRChart1.BorderSkin.PageColor = System.Drawing.Color.Transparent;
            chartArea1.Area3DStyle.WallWidth = 0;
            chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            chartArea1.AxisX.MinorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            chartArea1.AxisX.MinorTickMark.Size = 2F;
            chartArea1.AxisX.Title = "Financial Parameters";
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            chartArea1.AxisY.MinorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            chartArea1.AxisY.MinorTickMark.Size = 2F;
            chartArea1.AxisY.Title = "Values";
            chartArea1.BackColor = System.Drawing.Color.White;
            chartArea1.BackGradientEndColor = System.Drawing.Color.White;
            chartArea1.BorderColor = System.Drawing.Color.LightSlateGray;
            chartArea1.BorderStyle = Dundas.Charting.WinControl.ChartDashStyle.Solid;
            chartArea1.Name = "Default";
            this.FRChart1.ChartAreas.Add(chartArea1);
            legend1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            legend1.BorderColor = System.Drawing.Color.LightSlateGray;
            legend1.Name = "Default";
            this.FRChart1.Legends.Add(legend1);
            this.FRChart1.Location = new System.Drawing.Point(15, 86);
            this.FRChart1.Name = "FRChart1";
            this.FRChart1.Palette = Dundas.Charting.WinControl.ChartColorPalette.AcidWash;
            series1.BackGradientEndColor = System.Drawing.Color.White;
            series1.BackGradientType = Dundas.Charting.WinControl.GradientType.TopBottom;
            series1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            series1.Color = System.Drawing.Color.Red;
            series1.LegendText = "Income";
            series1.Name = "Series1";
            series1.PaletteCustomColors = new System.Drawing.Color[0];
            series2.BackGradientEndColor = System.Drawing.Color.White;
            series2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            series2.LegendText = "Benefit";
            series2.Name = "Series2";
            series2.PaletteCustomColors = new System.Drawing.Color[0];
            series3.BackGradientEndColor = System.Drawing.Color.White;
            series3.Color = System.Drawing.Color.White;
            series3.LegendText = "-";
            series3.Name = "Series3";
            series3.PaletteCustomColors = new System.Drawing.Color[0];
            this.FRChart1.Series.Add(series1);
            this.FRChart1.Series.Add(series2);
            this.FRChart1.Series.Add(series3);
            this.FRChart1.Size = new System.Drawing.Size(888, 436);
            this.FRChart1.TabIndex = 0;
            this.FRChart1.Text = "chart1";
            title1.Name = "Title1";
            title1.Text = "Daily Plot";
            this.FRChart1.Titles.Add(title1);
            this.FRChart1.UI.Toolbar.BackColor = System.Drawing.Color.Transparent;
            this.FRChart1.UI.Toolbar.BorderSkin.PageColor = System.Drawing.Color.Transparent;
            this.FRChart1.UI.Toolbar.BorderStyle = Dundas.Charting.WinControl.ChartDashStyle.NotSet;
            this.FRChart1.UI.Toolbar.Enabled = true;
            // 
            // FRChart2
            // 
            this.FRChart2.AlwaysRecreateHotregions = true;
            this.FRChart2.BackGradientEndColor = System.Drawing.Color.LightSteelBlue;
            this.FRChart2.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft;
            this.FRChart2.BorderLineColor = System.Drawing.Color.LightSlateGray;
            this.FRChart2.BorderLineStyle = Dundas.Charting.WinControl.ChartDashStyle.Solid;
            this.FRChart2.BorderSkin.FrameBackColor = System.Drawing.Color.SteelBlue;
            this.FRChart2.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.LightBlue;
            this.FRChart2.BorderSkin.FrameBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.FRChart2.BorderSkin.FrameBorderWidth = 2;
            this.FRChart2.BorderSkin.PageColor = System.Drawing.Color.Transparent;
            chartArea2.Area3DStyle.WallWidth = 0;
            chartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            chartArea2.AxisX.MinorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            chartArea2.AxisX.MinorTickMark.Size = 2F;
            chartArea2.AxisX.Title = "Financial Parameters";
            chartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            chartArea2.AxisY.MinorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            chartArea2.AxisY.MinorTickMark.Size = 2F;
            chartArea2.AxisY.Title = "Values";
            chartArea2.BackColor = System.Drawing.Color.White;
            chartArea2.BackGradientEndColor = System.Drawing.Color.White;
            chartArea2.BorderColor = System.Drawing.Color.LightSlateGray;
            chartArea2.BorderStyle = Dundas.Charting.WinControl.ChartDashStyle.Solid;
            chartArea2.Name = "Default";
            this.FRChart2.ChartAreas.Add(chartArea2);
            legend2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            legend2.BorderColor = System.Drawing.Color.LightSlateGray;
            legend2.Name = "Default";
            this.FRChart2.Legends.Add(legend2);
            this.FRChart2.Location = new System.Drawing.Point(12, 528);
            this.FRChart2.Name = "FRChart2";
            this.FRChart2.Palette = Dundas.Charting.WinControl.ChartColorPalette.AcidWash;
            series4.BackGradientEndColor = System.Drawing.Color.White;
            series4.BackGradientType = Dundas.Charting.WinControl.GradientType.TopBottom;
            series4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            series4.Name = "Series1";
            series4.PaletteCustomColors = new System.Drawing.Color[0];
            this.FRChart2.Series.Add(series4);
            this.FRChart2.Size = new System.Drawing.Size(888, 105);
            this.FRChart2.TabIndex = 1;
            this.FRChart2.Text = "chart2";
            title2.Name = "Title1";
            title2.Text = "monthly Plot";
            this.FRChart2.Titles.Add(title2);
            this.FRChart2.UI.Toolbar.BackColor = System.Drawing.Color.Transparent;
            this.FRChart2.UI.Toolbar.BorderSkin.PageColor = System.Drawing.Color.Transparent;
            this.FRChart2.UI.Toolbar.BorderStyle = Dundas.Charting.WinControl.ChartDashStyle.NotSet;
            this.FRChart2.UI.Toolbar.Enabled = true;
            this.FRChart2.Visible = false;
            // 
            // DailyRadio
            // 
            this.DailyRadio.AutoSize = true;
            this.DailyRadio.BackColor = System.Drawing.Color.Transparent;
            this.DailyRadio.Checked = true;
            this.DailyRadio.Location = new System.Drawing.Point(834, 30);
            this.DailyRadio.Name = "DailyRadio";
            this.DailyRadio.Size = new System.Drawing.Size(69, 17);
            this.DailyRadio.TabIndex = 2;
            this.DailyRadio.TabStop = true;
            this.DailyRadio.Text = "Daily Plot";
            this.DailyRadio.UseVisualStyleBackColor = false;
            this.DailyRadio.Visible = false;
            this.DailyRadio.CheckedChanged += new System.EventHandler(this.DailyRadio_CheckedChanged);
            // 
            // MonthlyRadio
            // 
            this.MonthlyRadio.AutoSize = true;
            this.MonthlyRadio.BackColor = System.Drawing.Color.Transparent;
            this.MonthlyRadio.Location = new System.Drawing.Point(727, 30);
            this.MonthlyRadio.Name = "MonthlyRadio";
            this.MonthlyRadio.Size = new System.Drawing.Size(83, 17);
            this.MonthlyRadio.TabIndex = 3;
            this.MonthlyRadio.Text = "Monthly Plot";
            this.MonthlyRadio.UseVisualStyleBackColor = false;
            this.MonthlyRadio.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.Location = new System.Drawing.Point(29, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "label1";
            // 
            // FRPlot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(923, 607);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MonthlyRadio);
            this.Controls.Add(this.DailyRadio);
            this.Controls.Add(this.FRChart2);
            this.Controls.Add(this.FRChart1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FRPlot";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Financial Plot";
            this.Load += new System.EventHandler(this.FRPlot_Load);
            ((System.ComponentModel.ISupportInitialize)(this.FRChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRChart2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Dundas.Charting.WinControl.Chart FRChart1;
        private Dundas.Charting.WinControl.Chart FRChart2;
        private System.Windows.Forms.RadioButton DailyRadio;
        private System.Windows.Forms.RadioButton MonthlyRadio;
        private System.Windows.Forms.Label label1;
    }
}