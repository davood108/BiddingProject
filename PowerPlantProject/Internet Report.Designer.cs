﻿namespace PowerPlantProject
{
    partial class Internet_Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Internet_Report));
            this.datePickerFrom = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdaverageprice = new System.Windows.Forms.RadioButton();
            this.rdproduce = new System.Windows.Forms.RadioButton();
            this.rdinterchane = new System.Windows.Forms.RadioButton();
            this.rdmanategh = new System.Windows.Forms.RadioButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.datePickerTo = new FarsiLibrary.Win.Controls.FADatePicker();
            this.btnprint = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // datePickerFrom
            // 
            this.datePickerFrom.HasButtons = true;
            this.datePickerFrom.Location = new System.Drawing.Point(155, 21);
            this.datePickerFrom.Name = "datePickerFrom";
            this.datePickerFrom.Readonly = true;
            this.datePickerFrom.Size = new System.Drawing.Size(120, 20);
            this.datePickerFrom.TabIndex = 35;
            this.datePickerFrom.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2003;
            this.datePickerFrom.SelectedDateTimeChanged += new System.EventHandler(this.datePickerFrom_SelectedDateTimeChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(113, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "From :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(317, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 36;
            this.label2.Text = "To :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdaverageprice);
            this.groupBox1.Controls.Add(this.rdproduce);
            this.groupBox1.Controls.Add(this.rdinterchane);
            this.groupBox1.Controls.Add(this.rdmanategh);
            this.groupBox1.Location = new System.Drawing.Point(155, 56);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(270, 66);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            // 
            // rdaverageprice
            // 
            this.rdaverageprice.AutoSize = true;
            this.rdaverageprice.Location = new System.Drawing.Point(147, 36);
            this.rdaverageprice.Name = "rdaverageprice";
            this.rdaverageprice.Size = new System.Drawing.Size(92, 17);
            this.rdaverageprice.TabIndex = 0;
            this.rdaverageprice.Text = "Average Price";
            this.rdaverageprice.UseVisualStyleBackColor = true;
            this.rdaverageprice.CheckedChanged += new System.EventHandler(this.rdaverageprice_CheckedChanged);
            // 
            // rdproduce
            // 
            this.rdproduce.AutoSize = true;
            this.rdproduce.Checked = true;
            this.rdproduce.Location = new System.Drawing.Point(6, 13);
            this.rdproduce.Name = "rdproduce";
            this.rdproduce.Size = new System.Drawing.Size(100, 17);
            this.rdproduce.TabIndex = 0;
            this.rdproduce.TabStop = true;
            this.rdproduce.Text = "Produce energy";
            this.rdproduce.UseVisualStyleBackColor = true;
            this.rdproduce.CheckedChanged += new System.EventHandler(this.rdproduce_CheckedChanged);
            // 
            // rdinterchane
            // 
            this.rdinterchane.AutoSize = true;
            this.rdinterchane.Location = new System.Drawing.Point(147, 13);
            this.rdinterchane.Name = "rdinterchane";
            this.rdinterchane.Size = new System.Drawing.Size(117, 17);
            this.rdinterchane.TabIndex = 0;
            this.rdinterchane.Text = "Interchange energy";
            this.rdinterchane.UseVisualStyleBackColor = true;
            this.rdinterchane.CheckedChanged += new System.EventHandler(this.rdinterchane_CheckedChanged);
            // 
            // rdmanategh
            // 
            this.rdmanategh.AutoSize = true;
            this.rdmanategh.Location = new System.Drawing.Point(6, 36);
            this.rdmanategh.Name = "rdmanategh";
            this.rdmanategh.Size = new System.Drawing.Size(73, 17);
            this.rdmanategh.TabIndex = 0;
            this.rdmanategh.Text = "Manategh";
            this.rdmanategh.UseVisualStyleBackColor = true;
            this.rdmanategh.CheckedChanged += new System.EventHandler(this.rdmanategh_CheckedChanged);
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.GridColor = System.Drawing.Color.CadetBlue;
            this.dataGridView1.Location = new System.Drawing.Point(4, 148);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.LemonChiffon;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(577, 487);
            this.dataGridView1.TabIndex = 40;
            // 
            // datePickerTo
            // 
            this.datePickerTo.HasButtons = true;
            this.datePickerTo.Location = new System.Drawing.Point(349, 21);
            this.datePickerTo.Name = "datePickerTo";
            this.datePickerTo.Readonly = true;
            this.datePickerTo.Size = new System.Drawing.Size(120, 20);
            this.datePickerTo.TabIndex = 44;
            this.datePickerTo.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2003;
            this.datePickerTo.SelectedDateTimeChanged += new System.EventHandler(this.datePickerTo_SelectedDateTimeChanged);
            // 
            // btnprint
            // 
            this.btnprint.BackColor = System.Drawing.Color.Linen;
            this.btnprint.BackgroundImage = global::PowerPlantProject.Properties.Resources._1263108050_agt_print;
            this.btnprint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnprint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnprint.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnprint.Location = new System.Drawing.Point(12, 112);
            this.btnprint.Name = "btnprint";
            this.btnprint.Size = new System.Drawing.Size(33, 30);
            this.btnprint.TabIndex = 43;
            this.btnprint.Tag = "Print";
            this.btnprint.UseVisualStyleBackColor = false;
            this.btnprint.Click += new System.EventHandler(this.btnprint_Click);
            // 
            // Internet_Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(583, 647);
            this.Controls.Add(this.datePickerTo);
            this.Controls.Add(this.btnprint);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.datePickerFrom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Internet_Report";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Internet_Report";
            this.Load += new System.EventHandler(this.Internet_Report_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FarsiLibrary.Win.Controls.FADatePicker datePickerFrom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdaverageprice;
        private System.Windows.Forms.RadioButton rdproduce;
        private System.Windows.Forms.RadioButton rdinterchane;
        private System.Windows.Forms.RadioButton rdmanategh;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnprint;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerTo;
    }
}