﻿//namespace PowerPlantProject
//{
//    partial class EconomicAllPlantDailyReportForm
//    {
//        /// <summary>
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.IContainer components = null;

//        /// <summary>
//        /// Clean up any resources being used.
//        /// </summary>
//        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && (components != null))
//            {
//                components.Dispose();
//            }
//            base.Dispose(disposing);
//        }

//        #region Windows Form Designer generated code

//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            this.EconomicAllPlantDailycrystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
//            this.EconomicAllPlantDailyCrystalReportDoc1 = new PowerPlantProject.EconomicAllPlantDailyCrystalReport();
//            this.SuspendLayout();
//            // 
//            // EconomicAllPlantDailycrystalReportViewer1
//            // 
//            this.EconomicAllPlantDailycrystalReportViewer1.ActiveViewIndex = -1;
//            this.EconomicAllPlantDailycrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
//            this.EconomicAllPlantDailycrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.EconomicAllPlantDailycrystalReportViewer1.Location = new System.Drawing.Point(0, 0);
//            this.EconomicAllPlantDailycrystalReportViewer1.Name = "EconomicAllPlantDailycrystalReportViewer1";
//            this.EconomicAllPlantDailycrystalReportViewer1.SelectionFormula = "";
//            this.EconomicAllPlantDailycrystalReportViewer1.Size = new System.Drawing.Size(292, 266);
//            this.EconomicAllPlantDailycrystalReportViewer1.TabIndex = 0;
//            this.EconomicAllPlantDailycrystalReportViewer1.ViewTimeSelectionFormula = "";
//            // 
//            // EconomicAllPlantDailyReportForm
//            // 
//            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
//            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//            this.ClientSize = new System.Drawing.Size(292, 266);
//            this.Controls.Add(this.EconomicAllPlantDailycrystalReportViewer1);
//            this.Name = "EconomicAllPlantDailyReportForm";
//            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
//            this.Text = "EconomicAllPlantDailyReportForm";
//            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
//            this.Load += new System.EventHandler(this.EconomicAllPlantDailyReportForm_Load);
//            this.ResumeLayout(false);

//        }

//        #endregion

//        private CrystalDecisions.Windows.Forms.CrystalReportViewer EconomicAllPlantDailycrystalReportViewer1;
//        private EconomicAllPlantDailyCrystalReport EconomicAllPlantDailyCrystalReportDoc1;
//    }
//}