﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
//using Microsoft.Office.Core;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    partial class MainForm
    {
        DataTable UnitsDataTable = null;
        string MissDates = "";
        string smaxdate = "";
        bool istrue = true;
        string daterror = "";
        double Avc_interval = 240000;
        //---------------------------------FRPlantCal_ValueChanged--------------------------------------------
        private void FRPlantCal_ValueChanged(object sender, EventArgs e)
        {
            FillFRValues(PPID);
        }
        //-------------------------------FRUpdateBtn_Click-------------------------------
        private void FRUpdateBtn_Click(object sender, EventArgs e)
        {

            if (FRUnitPanel.Visible)
            {
                if ((errorProvider1.GetError(FRUnitHotTb) == "") && (errorProvider1.GetError(FRUnitFixedTb) == "")
                && (errorProvider1.GetError(FRUnitVariableTb) == "") && (errorProvider1.GetError(FRUnitAmargTb1) == "")
                && (errorProvider1.GetError(FRUnitBmargTb1) == "") && (errorProvider1.GetError(FRUnitCmargTb1) == "")
                && (errorProvider1.GetError(FRUnitAmargTb2) == "") && (errorProvider1.GetError(FRUnitBmargTb2) == "")
                && (errorProvider1.GetError(FRUnitCmargTb2) == "") && (errorProvider1.GetError(FRUnitColdTb) == "")
                && (errorProvider1.GetError(FRUnitAmainTb) == "") && (errorProvider1.GetError(FRUnitBmainTb) == "")
                && (errorProvider1.GetError(FRUnitQReactiveTb) == "") && (errorProvider1.GetError(FRUnitPActiveTb) == ""))
                {
                    SqlCommand MyCom = new SqlCommand();
                    MyCom.CommandText = "UPDATE UnitsDataMain SET CapitalCost=@Ccost,FixedCost=@Fcost,VariableCost=@Vcost," +
                    "PrimaryFuelAmargin=@Amargin1,PrimaryFuelBmargin=@Bmargin1,PrimaryFuelCmargin=@Cmargin1,SecondFuelAmargin=@Amargin2," +
                    "SecondFuelBmargin=@Bmargin2,SecondFuelCmargin=@Cmargin2,CostStartCold=@cold,CostStartHot=@hot," +
                    "BMaintenance=@am,CMaintenance=@bm,QReactive=@qreactive,PActive=@pactive WHERE PPID=@num AND "+
                    "UnitCode=@unit AND PackageType=@type";

                    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                    myConnection.Open();

                    MyCom.Connection = myConnection;
                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                    MyCom.Parameters["@num"].Value = PPID;
                    MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
                    MyCom.Parameters["@unit"].Value = FRUnitLb.Text;
                    MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
                    string type = FRPackLb.Text;
                    if (type.Contains("Combined")) type = "CC";
                    MyCom.Parameters["@type"].Value = type;
                    MyCom.Parameters.Add("@Ccost", SqlDbType.NChar, 10);
                    MyCom.Parameters["@Ccost"].Value = FRUnitCapitalTb.Text;
                    MyCom.Parameters.Add("@Fcost", SqlDbType.Real);
                    if (FRUnitFixedTb.Text != "")
                        MyCom.Parameters["@Fcost"].Value = FRUnitFixedTb.Text;
                    else MyCom.Parameters["@Fcost"].Value = 0;
                    MyCom.Parameters.Add("@Vcost", SqlDbType.Real);
                    if (FRUnitVariableTb.Text != "")
                        MyCom.Parameters["@Vcost"].Value = FRUnitVariableTb.Text;
                    else MyCom.Parameters["@Vcost"].Value = 0;
                    MyCom.Parameters.Add("@Amargin1", SqlDbType.Real);
                    if (FRUnitAmargTb1.Text != "")
                        MyCom.Parameters["@Amargin1"].Value = FRUnitAmargTb1.Text;
                    else MyCom.Parameters["@Amargin1"].Value = 0;
                    MyCom.Parameters.Add("@Bmargin1", SqlDbType.Real);
                    if (FRUnitBmargTb1.Text != "")
                        MyCom.Parameters["@Bmargin1"].Value = FRUnitBmargTb1.Text;
                    else MyCom.Parameters["@Bmargin1"].Value = 0;
                    MyCom.Parameters.Add("@Cmargin1", SqlDbType.Real);
                    if (FRUnitCmargTb1.Text != "")
                        MyCom.Parameters["@Cmargin1"].Value = FRUnitCmargTb1.Text;
                    else MyCom.Parameters["@Cmargin1"].Value = 0;
                    MyCom.Parameters.Add("@Amargin2", SqlDbType.Real);
                    if (FRUnitAmargTb2.Text != "")
                        MyCom.Parameters["@Amargin2"].Value = FRUnitAmargTb2.Text;
                    else MyCom.Parameters["@Amargin2"].Value = 0;
                    MyCom.Parameters.Add("@Bmargin2", SqlDbType.Real);
                    if (FRUnitBmargTb2.Text != "")
                        MyCom.Parameters["@Bmargin2"].Value = FRUnitBmargTb2.Text;
                    else MyCom.Parameters["@Bmargin2"].Value = 0;
                    MyCom.Parameters.Add("@Cmargin2", SqlDbType.Real);
                    if (FRUnitCmargTb2.Text != "")
                        MyCom.Parameters["@Cmargin2"].Value = FRUnitCmargTb2.Text;
                    else MyCom.Parameters["@Cmargin2"].Value = 0;
                    MyCom.Parameters.Add("@cold", SqlDbType.Real);
                    if (FRUnitColdTb.Text != "")
                        MyCom.Parameters["@cold"].Value = FRUnitColdTb.Text;
                    else MyCom.Parameters["@cold"].Value = 0;
                    MyCom.Parameters.Add("@hot", SqlDbType.Real);
                    if (FRUnitHotTb.Text != "")
                        MyCom.Parameters["@hot"].Value = FRUnitHotTb.Text;
                    else MyCom.Parameters["@hot"].Value = 0;
                    MyCom.Parameters.Add("@qreactive", SqlDbType.Real);
                    if (FRUnitQReactiveTb.Text != "")
                        MyCom.Parameters["@qreactive"].Value = FRUnitQReactiveTb.Text;
                    else MyCom.Parameters["@qreactive"].Value = 0;
                    MyCom.Parameters.Add("@pactive", SqlDbType.Real);
                    if (FRUnitPActiveTb.Text != "")
                        MyCom.Parameters["@pactive"].Value = FRUnitPActiveTb.Text;
                    else MyCom.Parameters["@pactive"].Value = 0;
                    MyCom.Parameters.Add("@am", SqlDbType.NChar, 10);
                    MyCom.Parameters["@am"].Value = FRUnitAmainTb.Text;
                    MyCom.Parameters.Add("@bm", SqlDbType.NChar, 10);
                    MyCom.Parameters["@bm"].Value = FRUnitBmainTb.Text;
                    //try
                    //{
                    MyCom.ExecuteNonQuery();
                    myConnection.Close();
                    //}
                    //catch (Exception exp)
                    //{
                    //  string str = exp.Message;
                    //}
                }
                FRUnitCapitalTb.ReadOnly = true;
                FRUnitFixedTb.ReadOnly = true;
                FRUnitVariableTb.ReadOnly = true;
                FRUnitBmainTb.ReadOnly = true;
                FRUnitAmainTb.ReadOnly = true;
                FRUnitHotTb.ReadOnly = true;
                FRUnitColdTb.ReadOnly = true;
                FRUnitCmargTb2.ReadOnly = true;
                FRUnitCmargTb1.ReadOnly = true;
                FRUnitBmargTb2.ReadOnly = true;
                FRUnitBmargTb1.ReadOnly = true;
                FRUnitAmargTb2.ReadOnly = true;
                FRUnitAmargTb1.ReadOnly = true;
                FRUnitPActiveTb.ReadOnly = true;
                FRUnitQReactiveTb.ReadOnly = true;
                FRUpdateBtn.Enabled = false;
                FROKBtn.Enabled = true;
            }

        }
        //---------------------------------------FROKBtn_Click------------------------------------------
        private void FROKBtn_Click(object sender, EventArgs e)
        {
            if (FRUnitPanel.Visible)
            {
                FRUpdateBtn.Enabled = true;
                FRUnitCapitalTb.ReadOnly = false;
                FRUnitFixedTb.ReadOnly = false;
                FRUnitVariableTb.ReadOnly = false;
                FRUnitAmargTb1.ReadOnly = false;
                FRUnitBmargTb1.ReadOnly = false;
                FRUnitCmargTb1.ReadOnly = false;
                FRUnitAmargTb2.ReadOnly = false;
                FRUnitBmargTb2.ReadOnly = false;
                FRUnitCmargTb2.ReadOnly = false;
                FRUnitAmainTb.ReadOnly = false;
                FRUnitBmainTb.ReadOnly = false;
                FRUnitColdTb.ReadOnly = false;
                FRUnitHotTb.ReadOnly = false;
                FRUnitPActiveTb.ReadOnly = false;
                FRUnitQReactiveTb.ReadOnly = false;
            }
        }

        //---------------------------------------ClearFRPlantTab---------------------------------------------
        private void ClearFRPlantTab()
        {
            FRPlantBenefitTB.Text = "";
            FRPlantIncomeTb.Text = "";
            FRPlantCostTb.Text = "";
         

            FRPlantAvaCapTb.Text = "";
            FRPlantTotalPowerTb.Text = "";
            FRPlantBidPowerTb.Text = "";
            FRPlantULPowerTb.Text = "";
            FRPlantIncPowerTb.Text = "";
            FRPlantDecPowerTb.Text = "";
            FRPlantCapPayTb.Text = "";
            FRPlantEnergyPayTb.Text = "";
            FRPlantBidPayTb.Text = "";
            FRPlantULPayTb.Text = "";
            FRPlantIncPayTb.Text = "";
            FRPlantDecPayTb.Text = "";
        }
        //----------------------------------------FillFRValues()--------------------------------------------------
        private void FillFRValues(string Num)
        {
            dataGridViewECO.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            dataGridViewECO.AutoSizeRowsMode =System.Windows.Forms.DataGridViewAutoSizeRowsMode.None;
         
            string frdate1 = "";
            string frdate2 = "";
            string frdate3 = "";
            string frdate4 = "";
            string frdate5 = "";
            string frdate6 = "";
            string frdate7 = "";

            

            if (FRPlantCal.Text != "[Empty Value]")
            {

                frdate1 = FRPlantCal.Text;
                frdate2 = new PersianDate(PersianDateConverter.ToGregorianDateTime(FRPlantCal.Text).AddDays(-1)).ToString("d");
                frdate3 = new PersianDate(PersianDateConverter.ToGregorianDateTime(FRPlantCal.Text).AddDays(-2)).ToString("d");
                frdate4 = new PersianDate(PersianDateConverter.ToGregorianDateTime(FRPlantCal.Text).AddDays(-3)).ToString("d");
                frdate5 = new PersianDate(PersianDateConverter.ToGregorianDateTime(FRPlantCal.Text).AddDays(-4)).ToString("d");
                frdate6 = new PersianDate(PersianDateConverter.ToGregorianDateTime(FRPlantCal.Text).AddDays(-5)).ToString("d");
                frdate7 = new PersianDate(PersianDateConverter.ToGregorianDateTime(FRPlantCal.Text).AddDays(-6)).ToString("d");
              
            }
            else
            {
                frdate1 = "";
                frdate2 = "";
                frdate3 = "";
                frdate4 = "";
                frdate5 = "";
                frdate6 = "";
                frdate7 = "";
            }
            ClearFRPlantTab();
            //if (!FRPlantCal.IsNull)
            //if (FRPlantCal.Text == "[Empty Value]")
            //{
            if (FRPlantCal.Text != "[Empty Value]")
            {
                DataTable MyDS = null;
                MyDS = Utilities.GetTable("SELECT Date,Benefit as 'سود خالص نيروگاه' ,Income as 'درآمد خالص نيروگاه',Cost as 'هزينه سوخت نيروگاه',AvailableCapacity as 'ميزان آمادگي-مگاوات ساعت',TotalPower as 'ميزان توليد ناخالص-مگاوات ساعت',BidPower as 'ميزان خالص پذيرفته شده در بازار-مگاوات ساعت'," +
                "ULPower as 'ميزان خالص پذيرفته شده با نرخ UL-مگاوات ساعت',IncrementPower as 'ميزان  خالص توليدبيش ازپيشنهادبازارو به دستور مرکز-مگاوات ساعت',DecreasePower as 'ميزان توليدکمترازپيشنهاد بازارو به دستور مرکز-مگاوات ساعت',(Income/nullif(TotalPower,0))AS 'PerMw'   FROM [EconomicPlant] WHERE Date>='" + frdate7 + "' AND date<='" + frdate1 + "'and  PPID='" + PPID + "'order by date asc");
                if (MyDS != null)
                {
                    if (MyDS.Rows.Count == 0)
                    {
                        FRPlot.Enabled = false;
                        btnfrprint.Enabled = false;
                        btnfinanceexcel.Enabled = false;
                    }
                    else
                    {
                        FRPlot.Enabled = true;
                        btnfrprint.Enabled = true;
                        btnfinanceexcel.Enabled = true;
                    }

                }
                dataGridViewECO.DataSource = null;
                dataGridViewECO.DataSource = MyDS;
                //foreach (DataRow MyRow in MyDS.Rows)
                //{
                //    if (MyRow["date"].ToString().Trim() == frdate1.Text.Trim())
                //    {
                //        FRPlantBenefitTB.Text = MyRow["Benefit"].ToString().Trim();
                //        FRPlantIncomeTb.Text = MyRow["Income"].ToString().Trim();
                //        FRPlantCostTb.Text = MyRow["Cost"].ToString().Trim();
                //        FRPlantMWTb.Text = (MyDoubleParse(MyRow["Income"].ToString().Trim()) / MyDoubleParse(MyRow["TotalPower"].ToString().Trim())).ToString();



                //        //FRPlantAvaCapTb.Text = MyRow[3].ToString().Trim();
                //        //FRPlantTotalPowerTb.Text = MyRow[4].ToString().Trim();
                //        //FRPlantBidPowerTb.Text = MyRow[5].ToString().Trim();
                //        //FRPlantULPowerTb.Text = MyRow[6].ToString().Trim();
                //        //FRPlantIncPowerTb.Text = MyRow[7].ToString().Trim();
                //        //FRPlantDecPowerTb.Text = MyRow[8].ToString().Trim();
                //        //FRPlantCapPayTb.Text = MyRow[9].ToString().Trim();
                //        //FRPlantEnergyPayTb.Text = MyRow[10].ToString().Trim();
                //        //FRPlantBidPayTb.Text = MyRow[11].ToString().Trim();
                //        //FRPlantULPayTb.Text = MyRow[12].ToString().Trim();
                //        //FRPlantIncPayTb.Text = MyRow[13].ToString().Trim();
                //        //FRPlantDecPayTb.Text = MyRow[14].ToString().Trim();
                //        //FRRun.Visible = false;
                //    }

                //}
            }

        }
        //-------------------------------------ClearFRUnitTab()--------------------------------
        private void ClearFRUnitTab()
        {
            FRUnitCapitalTb.Text = "";
            FRUnitFixedTb.Text = "";
            FRUnitVariableTb.Text = "";
            FRUnitAmargTb1.Text = "";
            FRUnitAmargTb2.Text = "";
            FRUnitBmargTb1.Text = "";
            FRUnitBmargTb2.Text = "";
            FRUnitCmargTb1.Text = "";
            FRUnitCmargTb2.Text = "";
            FRUnitAmainTb.Text = "";
            FRUnitBmainTb.Text = "";
            FRUnitColdTb.Text = "";
            FRUnitHotTb.Text = "";
            FRUnitCapacityTb.Text = "";
            FRUnitTotalPowerTb.Text = "";
            FRUnitULPowerTb.Text = "";
            FRUnitCapPayTb.Text = "";
            FRUnitEneryPayTb.Text = "";
            FRUnitIncomeTb.Text = "";
        }
        //-----------------------------------FillFRUnit---------------------------------------
        private void FillFRUnit(string unit, string package, int index)
        {
            ClearFRUnitTab();
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            //Fill Cost Function GroupBox
            string type = package;
            if (type.Contains("Combined")) type = "CC";
            DataTable FRDS = null;
            FRDS = Utilities.GetTable("SELECT CapitalCost,FixedCost,VariableCost,PrimaryFuelAmargin," +
            "PrimaryFuelBmargin,PrimaryFuelCmargin,SecondFuelAmargin,SecondFuelBmargin,SecondFuelCmargin," +
            "BMaintenance,CMaintenance,CostStartCold,CostStartHot,QReactive,PActive FROM [UnitsDataMain] "+
            "WHERE PPID='" + PPID + "' AND PackageType='"+type+"' AND UnitCode LIKE '" + unit + "%'");


            foreach (DataRow MyRow in FRDS.Rows)
            {
                FRUnitCapitalTb.Text = MyRow[0].ToString().Trim();
                FRUnitFixedTb.Text = MyRow[1].ToString().Trim();
                FRUnitVariableTb.Text = MyRow[2].ToString().Trim();
                FRUnitAmargTb1.Text = MyRow[3].ToString().Trim();
                FRUnitBmargTb1.Text = MyRow[4].ToString().Trim();
                FRUnitCmargTb1.Text = MyRow[5].ToString().Trim();
                FRUnitAmargTb2.Text = MyRow[6].ToString().Trim();
                FRUnitBmargTb2.Text = MyRow[7].ToString().Trim();
                FRUnitCmargTb2.Text = MyRow[8].ToString().Trim();
                FRUnitAmainTb.Text = MyRow[9].ToString().Trim();
                FRUnitBmainTb.Text = MyRow[10].ToString().Trim();
                FRUnitColdTb.Text = MyRow[11].ToString().Trim();
                FRUnitHotTb.Text = MyRow[12].ToString().Trim();
                FRUnitQReactiveTb.Text = MyRow[13].ToString().Trim();
                FRUnitPActiveTb.Text = MyRow[14].ToString().Trim();
            }

            //Fill Revenue GroupBox
            FillFRUnitRevenue(unit, package, index);
            FuelCostCalculate("primary");
        }
        //-------------------------------------FillFRUnitRevenue()--------------------------------------
        private void FillFRUnitRevenue(string unit, string package, int index)
        {
            if ((!FRUnitCal.IsNull) && (FRUnitLb.Text != ""))
            {
                FRUnitCapacityTb.Text = "";
                FRUnitTotalPowerTb.Text = "";
                FRUnitULPowerTb.Text = "";
                FRUnitCapPayTb.Text = "";
                FRUnitEneryPayTb.Text = "";
                FRUnitIncomeTb.Text = "";

                string Pcode = "";
                if (GDSteamGroup.Text.Contains(package))
                    Pcode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                else Pcode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                string Punit = unit;
                if (package.Contains("Combined"))
                    Punit = "C" + Pcode;
                Punit = Punit.Trim();

                DataTable MyDS = null;
                MyDS = Utilities.GetTable("SELECT AvailableCapacity,TotalPower,ULPower,CapacityPayment," +
                "EnergyPayment,Income FROM [EconomicUnit] WHERE UnitCode='" + unit + "' AND Date='" + FRUnitCal.Text + "' AND PPID=" + PPID);
                

                foreach (DataRow MyRow in MyDS.Rows)
                {
                    FRUnitCapacityTb.Text = MyRow[0].ToString().Trim();
                    FRUnitTotalPowerTb.Text = MyRow[1].ToString().Trim();
                    FRUnitULPowerTb.Text = MyRow[2].ToString().Trim();
                    FRUnitCapPayTb.Text = MyRow[3].ToString().Trim();
                    FRUnitEneryPayTb.Text = MyRow[4].ToString().Trim();
                    FRUnitIncomeTb.Text = MyRow[5].ToString().Trim();
                }

            }
        }
//-------------------------------JustOneTime(string StartDate)-----------------------------------

//-------------------------------ReseTVariables------------------------------------
        private void ResetVariables()
        {
            PAvailableCapacity = 0;
            PTotalPower = 0;
            PULPower = 0;
            PBidPower = 0;
            PIncrementPower = 0;
            PDecreasePower = 0;
            PCapacityPayment = 0;
            PBidPayment = 0;
            PULPayment = 0;
            PIncrementPayment = 0;
            PDecreasePayment = 0;
            PEnergyPayment = 0;
            PIncome = 0;
            PCost = 0;
            PBenefit = 0;

        }
//------------------------------------------AutomaticFillEconomics---------------------------------
        private void AutomaticFillEconomics()
        {
            MissDates = "";
            //detect Yesterday
            System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
            DateTime CurDate = DateTime.Now; //DateTime.Parse("2010/07/16"); //
            int imonth = pr.GetMonth(CurDate);
            int iyear = pr.GetYear(CurDate);
            int iday = pr.GetDayOfMonth(CurDate) - 1;
            if (iday < 1)
            {
                imonth = imonth - 1;
                if (imonth < 1)
                {
                    iyear = iyear - 1;
                    imonth = 12;
                }
                if (imonth < 7) iday = 31;
                else if (imonth < 12) iday = 30;
                //is this year, KABISE?
                else if ((iyear - 1392) % 4 == 0) iday = 30;
                else iday = 29;
            }
            string day = iday.ToString();
            if (MyintParse1(day) < 10) day = "0" + day;
            string month = imonth.ToString();
            if (MyintParse1(month) < 10) month = "0" + month;
            string yesterday = iyear.ToString() + "/" + month + "/" + day;

            //Detect Last Date in Economic Plant
            SqlConnection MyConnection = new SqlConnection(ConnectionManager.ConnectionString);
            MyConnection.Open();
            SqlCommand MyCom = new SqlCommand();
            MyCom.CommandText = "select @result1 = max(EconomicPlant.Date) from EconomicPlant";
            MyCom.Connection = MyConnection;
            MyCom.Parameters.Add("@result1", SqlDbType.Char, 10);
            MyCom.Parameters["@result1"].Direction = ParameterDirection.Output;
            MyCom.ExecuteNonQuery();
            string LastDate = (string)MyCom.Parameters["@result1"].Value;
            //string LastDate = iyear.ToString() + "/" + "01" + "/" + "01";
            MyConnection.Close();

            // REPEAT FOR LastDate TO Yesterday
                if (IsLessDate(LastDate , yesterday))
                {
                    string mydate = NextDate(LastDate);
                  
                    while (IsLessDate(mydate , yesterday))
                    {
                        //MissDates += "<Date :" + mydate + " Plant:";
                        //for (int k = 0; k < PPIDArray.Count; k++)
                        //    if ((PPIDArray[k].ToString() != "0") && (PPIDType[k].ToString() == "real"))
                        //    {
                        //        //Check is there valid data for this Date in 002,005,009 
                        //        if (ThereAreValues(k, mydate))
                        //        {
                        //            MissDates += "  Complete " + "--";
                        //
                       //                        FillEconomicPlant(k, mydate);
                        daterror = mydate;
                        if (istrue)
                        {
                            //autofrdailyvalue(mydate);
                        }
                        else
                        {
                            MessageBox.Show("Incompleted Data In : "+mydate);
                            break;
                        }
                        //        }
                        //        else
                        //        {

                        //            MissDates += PPIDName[k].ToString() + "--";
                        //        }
                        //    }
                        //MissDates += ">";
                        ////MyCon.Close();
                        mydate = NextDate(mydate);
                    } 
                   if(istrue) MessageBox.Show(" Completed Successfully .");
                }

                if (MissDates != "")
                {
                    //DialogResult Result;
                    //Result = MessageBox.Show("Fail to Fill Financial Report Automatically for Some Items! Last Date In DataBase :" + LastDate + ". \r\n For More Information Click OK", "InSufficient Data !", MessageBoxButtons.OKCancel);
                    //if (Result == DialogResult.OK)
                    //   // MessageBox.Show("The Items are: " + MissDates);
                    //    MessageBox.Show(MissDates, " The Items are: ");
                    lblinsufficient.Text = "Fail to Fill Financial Report Automatically for Some Items! Last Date In DataBase :" + LastDate + "! For More Information Click !";

                }
        }
//----------------------------------------FillEconomicPlant----------------------------------------
        private void FillEconomicPlant(int k, string mydate)
        {
            ResetVariables();

            UnitsDataTable = null;
            UnitsDataTable = Utilities.GetTable("SELECT UnitCode,PackageType,PackageCode,Capacity,FixedCost," +
            "VariableCost,Bmaintenance,CMaintenance,PrimaryFuelAmargin,PrimaryFuelBmargin,PrimaryFuelCmargin," +
            "SecondFuelAmargin,SecondFuelBmargin,SecondFuelCmargin,CostStartCold,CostStartHot,PMin,PMax," +
            "HeatValuePrimaryFuel,HeatValueSecondaryFuel,PrimaryHeatRateC,SecondaryHeatRateC,PrimaryPowerC,SecondaryPowerC,CapitalCost " +
            " FROM [UnitsDataMain] WHERE PPID=" + PPIDArray[k]);

            foreach (DataRow MyRow in UnitsDataTable.Rows)
            {
                for (int column = 0; column < UnitsDataTable.Columns.Count; column++)
                    if (MyRow[column].ToString() == "") MyRow[column] = "0";
            }



            float[] UnitCostPayment = new float[UnitsDataTable.Rows.Count];
            //for all Units of selected plant
            for (int index = 0; index < (UnitsDataTable.Rows.Count); index++)
                UnitCostPayment[index] = SetCostPayment(k, mydate, index);

            //for all Blocks of selected plant
            bool[] check = new bool[UnitsDataTable.Rows.Count];
            for (int x = 0; x < UnitsDataTable.Rows.Count; x++) check[x] = false;
            for (int index = 0; index < (UnitsDataTable.Rows.Count); index++)
                if (!check[index])
                {
                    int Pcode = MyintParse1(UnitsDataTable.Rows[index][2].ToString().Trim());
                    //for  CombinedCycle 's Units one time run this loop
                    check[index] = true;
                    int counter = 0;

                    FillEconomicUnit(k, mydate, index, UnitCostPayment);
                }


            //INSERT INTO EconomicPlant Table
            SqlConnection MyConnection = new SqlConnection(ConnectionManager.ConnectionString);
            MyConnection.Open();
            SqlCommand MyCom1 = new SqlCommand();
            MyCom1.Connection = MyConnection;

            MyCom1.CommandText = "INSERT INTO [EconomicPlant] (PPID,Date,AvailableCapacity,TotalPower,BidPower," +
            "ULPower,Cost,CapacityPayment,EnergyPayment,Income,Benefit,IncrementPower,DecreasePower,BidPayment," +
            "ULPayment,IncrementPayment,DecreasePayment) VALUES (@num,@date,@avacap,@tpower,@bpower,@upower," +
            "@cost,@cap,@energy,@income,@benefit,@incpo,@decpo,@bidpay,@upay,@incpay,@decpay)";
            MyCom1.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom1.Parameters["@num"].Value = PPIDArray[k];
            MyCom1.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom1.Parameters["@date"].Value = mydate;
            MyCom1.Parameters.Add("@avacap", SqlDbType.Real);
            MyCom1.Parameters["@avacap"].Value = PAvailableCapacity;
            MyCom1.Parameters.Add("@tpower", SqlDbType.Real);
            MyCom1.Parameters["@tpower"].Value = PTotalPower;
            MyCom1.Parameters.Add("@bpower", SqlDbType.Real);
            MyCom1.Parameters["@bpower"].Value = PBidPower;
            MyCom1.Parameters.Add("@upower", SqlDbType.Real);
            MyCom1.Parameters["@upower"].Value = PULPower;
            MyCom1.Parameters.Add("@cost", SqlDbType.Real);
            MyCom1.Parameters["@cost"].Value = PCost;
            MyCom1.Parameters.Add("@cap", SqlDbType.Real);
            MyCom1.Parameters["@cap"].Value = PCapacityPayment;
            MyCom1.Parameters.Add("@energy", SqlDbType.Real);
            MyCom1.Parameters["@energy"].Value = PEnergyPayment;
            MyCom1.Parameters.Add("@income", SqlDbType.Real);
            MyCom1.Parameters["@income"].Value = PIncome;
            MyCom1.Parameters.Add("@benefit", SqlDbType.Real);
            MyCom1.Parameters["@benefit"].Value = PBenefit;
            MyCom1.Parameters.Add("@incpo", SqlDbType.Real);
            MyCom1.Parameters["@incpo"].Value = PIncrementPower;
            MyCom1.Parameters.Add("@decpo", SqlDbType.Real);
            MyCom1.Parameters["@decpo"].Value = PDecreasePower;
            MyCom1.Parameters.Add("@bidpay", SqlDbType.Real);
            MyCom1.Parameters["@bidpay"].Value = PBidPayment;
            MyCom1.Parameters.Add("@upay", SqlDbType.Real);
            MyCom1.Parameters["@upay"].Value = PULPayment;
            MyCom1.Parameters.Add("@incpay", SqlDbType.Real);
            MyCom1.Parameters["@incpay"].Value = PIncrementPayment;
            MyCom1.Parameters.Add("@decpay", SqlDbType.Real);
            MyCom1.Parameters["@decpay"].Value = PDecreasePayment;

            try
            {
            MyCom1.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                string message = e.Message;
            }
            //Close Connection*******
            MyConnection.Close();
        }
//-----------------------------------------------------------FillEconomicUnit()-----------------------------------
        private void FillEconomicUnit(int k, string mydate, int index, float[] UnitCostPayment)
        {
            double AvailableCapacity = 0, TotalPower = 0, ULPower = 0, CapacityPayment = 0,
            EnergyPayment = 0, Income = 0, Benefit = 0, BidPayment = 0, ULPayment = 0, DecPayment = 0,
            IncPayment = 0, DecPower = 0, IncPower = 0, BidPower = 0;

            string unit = UnitsDataTable.Rows[index][0].ToString().Trim();
            string package = UnitsDataTable.Rows[index][1].ToString().Trim();
            int Pcode = MyintParse1(UnitsDataTable.Rows[index][2].ToString().Trim());

            
           

            //Detect Block For FRM005
            string temp = unit;
            temp = temp.ToLower();
            if (package.Contains("CC"))
            {
                int x = MyintParse1(PPIDArray[k].ToString());

                if (PPIDArray.Contains(x + 1))
                    if (PPIDType[PPIDArray.IndexOf(x + 1)].ToString() == "virtual") x++;
                //if ((x == 131) || (x == 144)) x++;
                //temp = x + "-" + "C" + Pcode;


                //ccunitbase///////////////////////////////////////////
                //temp = x + "-" + "C" + packagecode;
                temp = temp.Replace("cc", "c");
                string[] sp = temp.Split('c');
                temp = sp[0].Trim() + sp[1].Trim();
                if (temp.Contains("gas"))
                {
                    temp = temp.Replace("gas", "G");

                }
                else
                {
                    temp = temp.Replace("steam", "S");

                }
                temp = x + "-" + temp;
            }
            else if (temp.Contains("gas"))
            {
                temp = temp.Replace("gas", "G");
                temp = PPIDArray[k] + "-" + temp;
            }
            else
            {
                temp = temp.Replace("steam", "S");
                temp = PPIDArray[k] + "-" + temp;
            }

            //string ptypenum = "0";
            //if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";
            //if (PPID == "149") ptypenum = "0";


            string ptypenum = "0";
            if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";
            // if (PPID == "232") ptypenum = "0";
            if (Findcconetype(PPID)) ptypenum = "0";
            
            double[] P = new double[24];
            double[] powerS = new double[24];
            double[] Dis = new double[24];
            double[] Economic = new double[24];
            string[] Contribution = new string[24];
            double[] W = new double[24];
            double[] U = new double[24];
            double F4 = 0;

            DataTable FRDS = null;
            FRDS = Utilities.GetTable("SELECT Required,Hour,Dispatchable,Contribution,Economic FROM [DetailFRM005] " +
            "WHERE TargetMarketDate='" + mydate + "' and  PPType='"+ptypenum +"' AND PPID='" + PPIDArray[k] + "' AND Block='" + temp + "' ");


            for (int i = 0; i < 24; i++)
            {
                P[i] = 0;
                Dis[i] = 0;
                Contribution[i] = "N";
            }
            foreach (DataRow MyRow in FRDS.Rows)
            {
                P[MyintParse1(MyRow[1].ToString().Trim()) - 1] = MyDoubleParse1(MyRow[0].ToString().Trim());
                Dis[MyintParse1(MyRow[1].ToString().Trim()) - 1] = MyDoubleParse1(MyRow[2].ToString().Trim());
                Contribution[MyintParse1(MyRow[1].ToString().Trim()) - 1] = MyRow[3].ToString().Trim();
                Economic[MyintParse1(MyRow[1].ToString().Trim()) - 1] = MyDoubleParse1(MyRow[4].ToString().Trim());
            }
            //FRCon.Close();

            //*************************SET PowerS Array
            for (int w = 0; w < 24; w++)
            {
                powerS[w] = P[w];
            }


            SqlConnection MyCon = new SqlConnection(ConnectionManager.ConnectionString);
            MyCon.Open();
            DataSet powerSDS = new DataSet();
            SqlDataAdapter powerSda = new SqlDataAdapter();
            powerSda.SelectCommand = new SqlCommand("SELECT SUM(P),Hour FROM DetailFRM009 WHERE TargetMarketDate=@date AND " +
            "PPID=@num AND PackageCode=" + Pcode + " GROUP BY Hour", MyCon);
            powerSda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
            powerSda.SelectCommand.Parameters["@date"].Value = mydate;
            powerSda.SelectCommand.Parameters.Add("@num", SqlDbType.NChar, 10);
            powerSda.SelectCommand.Parameters["@num"].Value = PPIDArray[k];
            powerSda.Fill(powerSDS);
            foreach (DataRow MyRow in powerSDS.Tables[0].Rows)
                powerS[MyintParse1(MyRow[1].ToString().Trim())] = MyDoubleParse1(MyRow[0].ToString().Trim());
            for (int w = 0; w < 24; w++)
            {
                if (powerS[w] < 1)
                {
                    powerS[w] = 0;
                }
            }
            //*************************

            //string unit = UnitsDataTable.Rows[index][0].ToString().Trim();
            //string package = UnitsDataTable.Rows[index][1].ToString().Trim();
            //int Pcode = MyintParse1(UnitsDataTable.Rows[index][2].ToString().Trim());

            float[] PowerS = new float[24];
            for (int i = 0; i < 24; i++) PowerS[i] = 0;
            //Detect Block For FRM009
            //string temp = unit;
            string temp2 = unit;

            DataTable PowerSDS = null;
            PowerSDS = Utilities.GetTable("SELECT Hour,P FROM [DetailFRM009] WHERE TargetMarketDate='" + mydate + "' AND PPID='" + PPIDArray[k] + "' AND Block='" + temp2 + "'");

            float[] PowerSU = new float[24];
            int px=0;
            try
            {
                foreach (DataRow tt in PowerSDS.Rows)
                {
                    int hour = int.Parse(tt["Hour"].ToString());
                    PowerSU[hour] = float.Parse(tt["P"].ToString());
                    
                }
            }
            catch
            {
            }

            //*************************SET AvailableCapacity/ TotalPower/ ULPower
            AvailableCapacity = SetAvailableCap(k, mydate, index, temp);
            //TotalPower = SetTotalPower(k, mydate, index, temp, P);
            ULPower = SetULPower(k, mydate, index, temp);

            //*************************SET CapacityPayment
            CapacityPayment = SetCapacityPayment(k, mydate, index, Dis);

            //*************************SET EnergyPayment

            
            
            SqlCommand MyCom1 = new SqlCommand();
            MyCom1.Connection = MyCon;

            SqlConnection PowerCon = new SqlConnection(ConnectionManager.ConnectionString);
            PowerCon.Open();
            DataSet PowerDS = new DataSet();
            SqlDataAdapter Powerda = new SqlDataAdapter();
            Powerda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3," +
            "Power4,Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10," +
            "Price10,Hour FROM [DetailFRM002] WHERE Estimated<>1 AND TargetMarketDate=@date AND PPType='"+ptypenum+"' and PPID=@num AND Block=@block", PowerCon);
            Powerda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
            Powerda.SelectCommand.Parameters["@date"].Value = mydate;
            Powerda.SelectCommand.Parameters.Add("@num", SqlDbType.NChar, 10);
            Powerda.SelectCommand.Parameters["@num"].Value = PPIDArray[k];
            Powerda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
            //DETECT Block For FRM002
            string block = unit;
            block = block.ToLower();
            if (package.Contains("CC"))
            {

                //block = "C" + Pcode;

                block = block.Replace("cc", "c");
                string[] sp = block.Split('c');
                block = sp[0].Trim() + sp[1].Trim();
                if (block.Contains("gas"))
                {
                    block = block.Replace("gas", "G");

                }
                else
                {
                    block = block.Replace("steam", "S");

                }
               

            }
            else
            {
                if (block.Contains("gas"))
                    block = block.Replace("gas", "G");
                else if (block.Contains("steam"))
                    block = block.Replace("steam", "S");
            }
            block = block.Trim();
            Powerda.SelectCommand.Parameters["@block"].Value = block;
            Powerda.Fill(PowerDS);

            double[] UnitBP = new double[24];
            double[] UnitULP = new double[24];
            double[] UnitI_Dec = new double[24];
            double[] UnitI_Inc = new double[24];
            double[] UnitI_Dec_Power = new double[24];
            double[] UnitI_Inc_Power = new double[24];
            double[] MaxBid = new double[24];

            //////////////////////////////////avc////////////////////////////////////////////////
            //AVC public shavad 543
            double[] AvcValue = new double[3];

            DataTable davc = Utilities.GetTable("select Avc  from UnitsDataMain where  PPID='" + PPIDArray[k] + "'ORDER BY packagecode,unittype");
                for (int i = 0; i < 3; i++)
                {

                    AvcValue[i] = MyDoubleParse(davc.Rows[i][0].ToString());
                }


         

            foreach (DataRow PRow in PowerDS.Tables[0].Rows)
            {
                int hour = MyintParse1(PRow[20].ToString().Trim()) - 1;
                float[] Power = new float[10];
                float[] price = new float[10];
                for (int z = 0; z < 10; z++) Power[z] = (float)PRow[2 * z];
                for (int z = 0; z < 10; z++) price[z] = (float)PRow[(2 * z) + 1];
                int PCount = 0;
                while ((PCount < 10) && (Power[PCount] != 0)) PCount++;
                if ((PCount > 0) && (Power[PCount - 1] < P[hour])) Power[PCount - 1] = (float)P[hour];

                if (Contribution[hour].Contains("N"))
                {
                    if (P[hour] == PowerSU[hour])
                    {
                        UnitI_Dec[hour] = 0;
                        UnitI_Inc[hour] = 0;
                        int p1 = 0;
                        while (p1 < 10)
                        {
                            float previous = 0;
                            if (p1 > 0) previous = Power[p1 - 1];
                            if (Power[p1] < PowerSU[hour]) UnitBP[hour] += (Power[p1] - previous) * price[p1];
                            else
                            {
                                UnitBP[hour] += ((float)PowerSU[hour] - previous) * price[p1];
                                p1 = 10;
                            }
                            p1++;
                        }
                    }
                    else if (P[hour] < PowerSU[hour])
                    {
                        UnitI_Inc_Power[hour] = PowerSU[hour] - P[hour];
                        UnitI_Dec_Power[hour] = 0;
                        UnitI_Dec[hour] = 0;
                        int p1 = 0;
                        while (p1 < 10)
                        {
                            float previous = 0;
                            if (p1 > 0) previous = Power[p1 - 1];
                            if (Power[p1] < P[hour]) UnitBP[hour] += (Power[p1] - previous) * price[p1];
                            else
                            {
                                UnitBP[hour] += ((float)P[hour] - previous) * price[p1];
                                p1 = 10;
                            }
                            p1++;
                        }
                        float ans2 = 0;
                        p1 = 0;
                        while (p1 < 10)
                        {
                            float previous = 0;
                            if (p1 > 0) previous = Power[p1 - 1];
                            if (Power[p1] < PowerSU[hour]) ans2 += (Power[p1] - previous) * price[p1];
                            else
                            {
                                ans2 += ((float)PowerSU[hour] - previous) * price[p1];
                                p1 = 10;
                            }
                            p1++;
                        }
                        //ans2 = (float)PowerSU[hour];
                        UnitI_Inc[hour] = ans2 ;

                    }
                    if (P[hour] > PowerSU[hour])
                    {
                        UnitI_Dec_Power[hour] = P[hour] - PowerSU[hour];
                        UnitI_Inc_Power[hour] = 0;
                        UnitI_Inc[hour] = 0;
                        int p1 = 0;
                        while (p1 < 10)
                        {
                            float previous = 0;
                            if (p1 > 0) previous = Power[p1 - 1];
                            if (Power[p1] < PowerSU[hour]) UnitBP[hour] += (Power[p1] - previous) * price[p1];
                            else
                            {
                                UnitBP[hour] += ((float)PowerSU[hour] - previous) * price[p1];
                                p1 = 10;
                            }
                            p1++;
                        }
                        float ans2 = 0;
                        p1 = 0;
                        while (p1 < 10)
                        {
                            float previous = 0;
                            if (p1 > 0) previous = Power[p1 - 1];
                            if (Power[p1] < P[hour]) ans2 += (Power[p1] - previous) * price[p1];
                            else
                            {
                                ans2 += ((float)P[hour] - previous) * price[p1];
                                p1 = 10;
                            }
                            p1++;
                        }

                        //-------------------------------------------------------------------
                       
                        double Costmin = 0;
                        DataTable ODataTable = null;
                        // ODataTable = utilities.GetTable("SELECT MarketPriceMin FROM BaseData WHERE Date=(SELECT MAX(Date) FROM BaseData)");
                        ODataTable = Utilities.GetTable("SELECT MarketPriceMin FROM BaseData WHERE Date='" + smaxdate + "'order by BaseID desc");
                        DataTable ddt = Utilities.GetTable("select Avc from dbo.UnitsDataMain where PPID='" + PPIDArray[k] + "' and UnitCode='" + unit + "' and PackageCode='" + Pcode + "'");
                        try
                        {
                            if (ddt.Rows.Count > 0)
                            {
                                if (ddt.Rows[0][0].ToString().Trim() != "")
                                {
                                    Costmin = MyDoubleParse1(ddt.Rows[0][0].ToString());
                                }
                            }
                            else if (ODataTable.Rows.Count > 0)
                            {
                                Costmin = MyDoubleParse1(ODataTable.Rows[0][0].ToString().Trim());

                            }
                        }
                        catch
                        {
                            Costmin = 0;
                        }
                        //DataTable ODataTable = null;
                        //ODataTable = utilities.GetTable("SELECT MarketPriceMin FROM BaseData WHERE Date=(SELECT MAX(Date) FROM BaseData)");


                        //double Costmin;
                        //try { Costmin = double.Parse(ODataTable.Rows[0][0].ToString().Trim()); }
                        //catch { Costmin = 0; }
                        //try { Costmin = double.Parse(MyCom1.Parameters["@Costmin"].Value.ToString().Trim()); }
                        //catch {Costmin=0;}
                        //UnitI_Dec[hour] = (ans2 - UnitBP[hour]) - (Costmin * (P[hour] - PowerSU[hour]));
                        UnitI_Dec[hour] = (ans2 - UnitBP[hour]) - (Costmin * (P[hour] - PowerSU[hour]));
                    }

                }
                else //if (Contribution=="UL")
                {
                    //ULPower+=P[hour];
                    //*************************SET MaxBid When Contribution=UL
                    //DETECT Min Price Form AveragePrice Table
                    DataTable MinTable = null;
                    MinTable = Utilities.GetTable("SELECT AcceptedMin FROM [AveragePrice] WHERE Date='" + mydate + "' AND Hour=" + (hour + 1));

                    double minPrice;
                    try
                    {
                        //minPrice = double.Parse(MyCom1.Parameters["@min"].Value.ToString().Trim()); 
                        minPrice = MyDoubleParse1(MinTable.Rows[0][0].ToString().Trim());

                    }

                    catch
                    {
                        DataTable nearestTable = null;
                        nearestTable = Utilities.GetTable("select max(AveragePrice.Date) from AveragePrice Where Date<'" + mydate + "' AND Hour=" + (hour + 1));

                        string result1 = "";
                        //result1 = MyCom1.Parameters["@result1"].Value.ToString().Trim();
                        result1 = nearestTable.Rows[0][0].ToString().Trim();
                        DataTable AcceptTable = null;
                        AcceptTable = Utilities.GetTable("SELECT AcceptedMin FROM [AveragePrice] WHERE Date='" + result1 + "' AND Hour=" + (hour + 1));

                        try
                        {
                            //minPrice = double.Parse(MyCom1.Parameters["@min"].Value.ToString().Trim()); 
                            minPrice = MyDoubleParse1(AcceptTable.Rows[0][0].ToString().Trim());
                        }
                        catch
                        {
                            minPrice = 25000;
                        }
                    }
                    double TempBid = minPrice * 0.9;
                    if (MaxBid[hour] < TempBid) MaxBid[hour] = TempBid;

                    //SET BidPayment & IncomeDec & IncomeInc
                    if (P[hour] == PowerSU[hour])
                    {
                        UnitULP[hour] = PowerSU[hour] * MaxBid[hour] * 0.9;
                        UnitI_Dec[hour] = 0;
                        UnitI_Inc[hour] = 0;

                    }
                    else if (P[hour] < PowerSU[hour])
                    {
                        UnitI_Inc_Power[hour] = PowerSU[hour] - P[hour];
                        UnitI_Dec_Power[hour] = 0;
                        int p1 = 0;
                        float ans1 = 0, ans2 = 0;
                        while (p1 < 10)
                        {
                            float previous = 0;
                            if (p1 > 0) previous = Power[p1 - 1];
                            if (Power[p1] < PowerSU[hour]) ans1 += (Power[p1] - previous) * price[p1];
                            else
                            {
                                ans1 += ((float)PowerSU[hour] - previous) * price[p1];
                                p1 = 10;
                            }
                            p1++;
                        }
                        p1 = 0;
                        while (p1 < 10)
                        {
                            float previous = 0;
                            if (p1 > 0) previous = Power[p1 - 1];
                            if (Power[p1] < P[hour]) ans2 += (Power[p1] - previous) * price[p1];
                            else
                            {
                                ans2 += ((float)P[hour] - previous) * price[p1];
                                p1 = 10;
                            }
                            p1++;
                        }
                        //while (p1 < 10)
                        //{

                        //    if (Power[p1] < PowerSU[hour]) ans1 += ((float)PowerSU[hour]- Power[p1]) * price[p1];
                        //    if (Power[p1] < P[hour]) ans2 += ((float)P[hour]- Power[p1]) * price[p1];
                        //    p1++;
                        //}
                        if (PowerSU[hour] < (1.05 * P[hour]))
                        {
                            UnitULP[hour] = P[hour] * AvcValue[0];
                            UnitI_Dec[hour] = 0;
                            if (Power[hour] > P[hour])
                            {
                                UnitI_Inc[hour] = ans1 - ans2;
                            }
                            
                        }
                        else if (PowerSU[hour] > (1.05 * P[hour]))
                        {
                            ULPower -= P[hour];
                            UnitI_Dec[hour] = 0;
                            if (PowerSU[hour] > P[hour])
                            {
                                UnitI_Inc[hour] = ans1 - ans2;
                                UnitBP[hour] = ans2;
                            }
                            if (PowerSU[hour] < P[hour])
                            {
                                UnitBP[hour] = ans2;
                            }
                            if (PowerSU[hour] == P[hour])
                            {
                                UnitI_Inc[hour] = 0;
                                UnitBP[hour] = ans2;
                            }
                            
                        }
                    }
                }
            }

            //SET BidPayment,ULPayment, for Unit
            for (int i = 0; i < 24; i++)
            {
                BidPayment += UnitBP[i];
                ULPayment += UnitULP[i];
                //BidPower += PowerSU[i] - UnitI_Inc_Power[i];
                BidPower += P[i];
                DecPayment += UnitI_Dec[i];
                IncPayment += UnitI_Inc[i];
                DecPower += UnitI_Dec_Power[i];
                IncPower += UnitI_Inc_Power[i];
                TotalPower += PowerSU[i];
            }
            //if power-s < required DecPowert=... else IncPower=...
            //if (TotalPower < BidPower) DecPower = BidPower - TotalPower;
            //else IncPower = TotalPower - BidPower;

            PowerCon.Close();

            //***********************SET EnergyPayment
            EnergyPayment = 0;
            for (int x = 0; x < 24; x++)
                EnergyPayment += UnitBP[x] + UnitI_Inc[x] + UnitI_Dec[x];
            EnergyPayment += ULPayment;

            //*************************SET Income
            Income = EnergyPayment + CapacityPayment;

            //*************************SET Benefit
            float CostPayment = 0;
            int counter = 0;
            foreach (DataRow DR in UnitsDataTable.Rows)
            //if (DR.Index < TempGV.RowCount - 1)
            {
                if (MyintParse1(DR[2].ToString().Trim()) == Pcode) CostPayment += UnitCostPayment[counter];
                counter++;
            }
            
            // Movaghatiiiiiiiiiiiiiiii
         
            Benefit = Income;
            // Public shavad
            //Benefit = Income - CostPayment;


            MyCon.Close();

            //INSERT INTO EconomicUnit Table
            SqlConnection MyConnection = new SqlConnection(ConnectionManager.ConnectionString);
            MyConnection.Open();
            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            counter = 0;

            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@PackageType", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@Punit", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@avacap", SqlDbType.Real);
            MyCom.Parameters.Add("@tpower", SqlDbType.Real);
            MyCom.Parameters.Add("@upower", SqlDbType.Real);
            MyCom.Parameters.Add("@cost", SqlDbType.Real);
            MyCom.Parameters.Add("@cap", SqlDbType.Real);
            MyCom.Parameters.Add("@energy", SqlDbType.Real);
            MyCom.Parameters.Add("@income", SqlDbType.Real);
            MyCom.Parameters.Add("@benefit", SqlDbType.Real);
            foreach (DataRow DR in UnitsDataTable.Rows)
            {
                //if (DR.Index < TempGV.RowCount - 1)
                if (MyintParse1(DR[2].ToString().Trim()) == Pcode)
                {
                    string myunit = DR[0].ToString().Trim();
                    DataTable ddel = Utilities.GetTable("delete from EconomicUnit where ppid='" + PPIDArray[k] + "'and UnitCode='" + myunit + "'and PackageType='" + DR[1].ToString().Trim() + "'and Date='" + mydate + "'");
                   
                    MyCom.CommandText = "INSERT INTO [EconomicUnit] (PPID,UnitCode,PackageType,Date,AvailableCapacity,TotalPower," +
                    "ULPower,CostPayment,CapacityPayment,EnergyPayment,Income,Benefit) VALUES (@num,@Punit,@PackageType,@date," +
                    "@avacap,@tpower,@upower,@cost,@cap,@energy,@income,@benefit)";
                    MyCom.Parameters["@num"].Value = PPIDArray[k];
                    MyCom.Parameters["@PackageType"].Value = DR[1].ToString().Trim();
                    MyCom.Parameters["@date"].Value = mydate;
                    MyCom.Parameters["@Punit"].Value = myunit;
                    MyCom.Parameters["@avacap"].Value = AvailableCapacity;
                    MyCom.Parameters["@tpower"].Value = BidPower;
                    MyCom.Parameters["@upower"].Value = ULPower;
                    MyCom.Parameters["@cost"].Value = UnitCostPayment[counter];
                    MyCom.Parameters["@cap"].Value = CapacityPayment;
                    MyCom.Parameters["@energy"].Value = EnergyPayment;
                    MyCom.Parameters["@income"].Value = Income;
                    MyCom.Parameters["@benefit"].Value = Benefit;
                    try
                    {
                    MyCom.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        string message = e.Message;
                    }

                    //SET Some Items Of selected Plant, Incrementally!
                    PCost += UnitCostPayment[counter];

                }
                counter++;
            }
            PBidPayment += BidPayment;
            PBidPower += BidPower;
            PULPayment += ULPayment;
            PCapacityPayment += CapacityPayment;
            PDecreasePayment += DecPayment;
            PIncrementPayment += IncPayment;
            PDecreasePower += DecPower;
            PIncrementPower += IncPower;
            PTotalPower += TotalPower;
            PAvailableCapacity += AvailableCapacity;
            PULPower += ULPower;
            PIncome += Income;
            PBenefit += Benefit;
            PEnergyPayment += EnergyPayment;

            MyConnection.Close();

        }
//--------------------------------------SetCapacityPayment---------------------------------
        private double SetCapacityPayment(int k, string mydate, int index, double[] Dis)
        {
            double CapacityPayment = 0;
            string answer = GetNearestDatetohcpf(mydate);
            double TempCap = 0;
            if ((answer != null) && (answer != ""))
            {
                SqlConnection HourCon = new SqlConnection(ConnectionManager.ConnectionString);
                HourCon.Open();
                DataSet HourDS = new DataSet();
                SqlDataAdapter Hourda = new SqlDataAdapter();
                Hourda.SelectCommand = new SqlCommand("SELECT H1,H2,H3,H4,H5,H6,H7,H8,H9,H10,H11,H12,H13," +
                "H14,H15,H16,H17,H18,H19,H20,H21,H22,H23,H24 FROM [HCPF] WHERE Date=@date", HourCon);
                Hourda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Hourda.SelectCommand.Parameters["@date"].Value = answer;
                Hourda.Fill(HourDS);
                foreach (DataRow HRow in HourDS.Tables[0].Rows)
                {
                    for (int c = 0; c < 24; c++)
                        TempCap += ((float)HRow[c]) * Dis[c];
                }
                HourCon.Close();
            }
            else
            {
                for (int c = 0; c < 24; c++)
                    TempCap += Dis[c];
            }
            //Close Connection**********

            // DETECT The Week Of the Selected day!
            int year = MyintParse1(mydate.Remove(4));
            int day = MyintParse1(mydate.Substring(8, 2));
            int month = MyintParse1(mydate.Substring(5, 2));
            if (month < 7) day = day + ((month - 1) * 31);
            else day = 186 + day + ((month - 7) * 30);
                                   
            //---------------------old devision----------------------------
            //int week = day / 7;
            ///////////////////////////////////////////////////
            int x1 = day / 7;
            int operand1 = day;
            int operand2 = 7;
            double x2 = (double)operand1 / operand2;

            if (x2 >= (x1 + 0.85))
            {
                x1 = MyintParse1(Math.Round((double)day / 7).ToString());
            }
            int week = x1;
            //---------------------------------------------------

            if (((day % 7) != 0) && (week < 52)) week++;

            answer = GetNearestDatetowcpf(year, week);
            DataTable WeekValue = null;
            double myweek = 1;
            if (answer != null)
            {
                WeekValue = Utilities.GetTable("SELECT Value FROM [WCPF] WHERE Week=" + week + " AND Year='" + answer + "'");
                myweek = MyDoubleParse1(WeekValue.Rows[0][0].ToString().Trim());
            }
            SqlConnection MyConnection = new SqlConnection(ConnectionManager.ConnectionString);
            MyConnection.Open();
            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            MyCom.CommandText = "SELECT @mycap=CapacityPayment FROM [BaseData] WHERE Date='" + smaxdate + "'order by BaseID desc";
            MyCom.Parameters.Add("@mycap", SqlDbType.Real);
            MyCom.Parameters["@mycap"].Direction = ParameterDirection.Output;
            //try
            //{
            MyCom.ExecuteNonQuery();
            //}
            //catch (Exception e)
            //{
            //string message = e.Message;
            //}
            double BaseCap;
            try { BaseCap = MyDoubleParse1(MyCom.Parameters["@mycap"].Value.ToString().Trim()); }
            catch { BaseCap = 0; }
            CapacityPayment = myweek * BaseCap * TempCap;
            return (CapacityPayment);
            //Close Connection*******
            MyConnection.Close();
        }
//----------------------------------------SetCostPayment-----------------------------
        private float SetCostPayment(int k, string mydate, int index)
        {
            string unit = UnitsDataTable.Rows[index][0].ToString().Trim();
            string package = UnitsDataTable.Rows[index][1].ToString().Trim();
            int Pcode = MyintParse1(UnitsDataTable.Rows[index][2].ToString().Trim());

            float[] PowerS = new float[24];
            for (int i = 0; i < 24; i++) PowerS[i] = 0;
            //Detect Block For FRM009
            string temp = unit;


            DataTable PowerSDS = null;
            PowerSDS = Utilities.GetTable("SELECT Hour,P FROM [DetailFRM009] WHERE TargetMarketDate='" + mydate + "' AND PPID='" + PPIDArray[k] + "' AND Block='" + temp + "'");


            foreach (DataRow PRow in PowerSDS.Rows)
                PowerS[MyintParse1(PRow[0].ToString().Trim())] = MyfloatParse1(PRow[1].ToString().Trim());



            float CostPayment = 0;
            int[] V = new int[24];
            int[] S = new int[24];
            float[] F1 = new float[24];
            float[] F2 = new float[24];
            float[] F3 = new float[24];
            float[] F4 = new float[24];
            float[] F5 = new float[24];
            float[] FuelCost = new float[24];

            for (int i = 0; i < 24; i++) if (PowerS[i] > 0) V[i] = 1; else V[i] = 0;
            for (int i = 0; i < 23; i++) S[i] = V[i] - V[i + 1];

            //# of Required those are less than zero
            int Minus = 0;
            for (int i = 0; i < 23; i++) if (S[i] < 0) Minus++;

            //Read EfficiencyMarket Table FROM DataBase
            DataTable EfficiencyMarket = null;
            EfficiencyMarket = Utilities.GetTable("SELECT Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8," +
            "Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22" +
            ",Hour23,Hour24,FromDate,ToDate FROM [EfficiencyMarket] WHERE Date=(SELECT MAX(Date) FROM EfficiencyMarket)");


            DataTable BaseData = null;
            BaseData = Utilities.GetTable("SELECT GasPrice,MazutPrice,GasOilPrice,GasSubsidiesPrice,MazutSubsidiesPrice," +
            "GasOilSubsidiesPrice FROM BaseData WHERE Date='" + smaxdate + "'order by BaseID desc");


            //SET FuelCost,F1,F2
            for (int j = 0; j < 24; j++)
            {
                bool SF = isSecondFuel(k, index, j, mydate);
                //SET FuelCost
                string fromDate = EfficiencyMarket.Rows[0][24].ToString().Trim();
                string toDate = EfficiencyMarket.Rows[0][25].ToString().Trim();
                //if ((mydate == fromDate) || (mydate == toDate) || CheckDate(mydate, fromDate, toDate))

                float PAmargin = MyfloatParse1(UnitsDataTable.Rows[index][8].ToString().Trim());
                float PBmargin = MyfloatParse1(UnitsDataTable.Rows[index][9].ToString().Trim());
                float PCmargin = MyfloatParse1(UnitsDataTable.Rows[index][10].ToString().Trim());
                float SAmargin = MyfloatParse1(UnitsDataTable.Rows[index][11].ToString().Trim());
                float SBmargin = MyfloatParse1(UnitsDataTable.Rows[index][12].ToString().Trim());
                float SCmargin = MyfloatParse1(UnitsDataTable.Rows[index][13].ToString().Trim());
                float Pmax = MyfloatParse1(UnitsDataTable.Rows[index][17].ToString().Trim());
                float PHeatValue = MyfloatParse1(UnitsDataTable.Rows[index][18].ToString().Trim());
                float SHeatValue = MyfloatParse1(UnitsDataTable.Rows[index][19].ToString().Trim());
                float PHeatC = MyfloatParse1(UnitsDataTable.Rows[index][20].ToString().Trim());
                float PPowerC = MyfloatParse1(UnitsDataTable.Rows[index][22].ToString().Trim());
                float SHeatC = MyfloatParse1(UnitsDataTable.Rows[index][21].ToString().Trim());
                float SPowerC = MyfloatParse1(UnitsDataTable.Rows[index][23].ToString().Trim());

                float FixedParameter = MyfloatParse1(UnitsDataTable.Rows[index][4].ToString().Trim());
                float VariableParameter = MyfloatParse1(UnitsDataTable.Rows[index][5].ToString().Trim());
                float MaintenanceParameter = MyfloatParse1(UnitsDataTable.Rows[index][7].ToString().Trim());
                float CapitalParameter = MyfloatParse1(UnitsDataTable.Rows[index][24].ToString().Trim());



                float TempMargin = 0;
                // AP2+BP+C //Mousavi Comment
                //if (SF) TempMargin = (((SAmargin * Pmax * Pmax) + SBmargin * Pmax + SCmargin) / Pmax) * SHeatValue;
                //else TempMargin = (((PAmargin * Pmax * Pmax) + PBmargin * Pmax + PCmargin) / Pmax) * PHeatValue;

                double[] P = new double[24];
                float[] Dis = new float[24];

                //Detect Block For FRM005
               
                string block1 = temp;
                block1 = block1.ToLower();
                if (package.Contains("CC"))
                {
                    int x = MyintParse1(PPIDArray[k].ToString());

                    if (PPIDArray.Contains(x + 1))
                        if (PPIDType[PPIDArray.IndexOf(x + 1)].ToString() == "virtual") x++;

                    //block1 = x + "-" + "C" + Pcode;

                    block1 = block1.Replace("cc", "c");
                    string[] sp = block1.Split('c');
                    block1 = sp[0].Trim() + sp[1].Trim();
                    if (block1.Contains("gas"))
                    {
                        block1 = block1.Replace("gas", "G");

                    }
                    else
                    {
                        block1 = block1.Replace("steam", "S");

                    }
                    block1 = x + "-" + block1;


                }
                else if (block1.Contains("gas"))
                {
                    block1 = block1.Replace("gas", "G");
                    block1 = PPIDArray[k] + "-" + block1;
                }
                else
                {
                    block1 = block1.Replace("steam", "S");
                    block1 = PPIDArray[k] + "-" + block1;
                }

                

                DataTable FRDS = null;
                FRDS = Utilities.GetTable("SELECT Required,Hour,Dispatchable,Contribution,Economic FROM [DetailFRM005] " +
                "WHERE TargetMarketDate='" + mydate + "' AND PPID='" + PPIDArray[k] + "' AND Block='" +block1 + "'");


                for (int i = 0; i < 24; i++)
                {
                    P[i] = 0;
                    Dis[i] = 0;
                }
                foreach (DataRow MyRow in FRDS.Rows)
                {
                    P[MyintParse1(MyRow[1].ToString().Trim()) - 1] = MyDoubleParse1(MyRow[0].ToString().Trim());
                    Dis[MyintParse1(MyRow[1].ToString().Trim()) - 1] = MyfloatParse1(MyRow[2].ToString().Trim());
                }

                if (!unit.Contains("Steam cc"))
                {


                    if (SF) TempMargin = (SHeatC / SPowerC) * SHeatValue;
                    else TempMargin = (PHeatC / PPowerC) * PHeatValue;

                    float X = ((float)859000.0 / TempMargin) * 100;
                    float Y = MyfloatParse1(EfficiencyMarket.Rows[0][j].ToString().Trim());
                    if (X > Y)
                    {
                        if (!SF) FuelCost[j] = MyfloatParse1(BaseData.Rows[0][3].ToString().Trim());
                        else
                        {
                            if (unit.Contains("Steam"))
                                FuelCost[j] = MyfloatParse1(BaseData.Rows[0][4].ToString().Trim());
                            else if (unit.Contains("Gas"))
                                FuelCost[j] = MyfloatParse1(BaseData.Rows[0][5].ToString().Trim());
                        }
                    }
                    else
                    {
                        float PriceSubsidies = 0, PriceNoSubsidies = 0;
                        if (!SF)
                        {
                            PriceSubsidies = MyfloatParse1(BaseData.Rows[0][3].ToString().Trim());
                            PriceNoSubsidies = MyfloatParse1(BaseData.Rows[0][0].ToString().Trim());
                        }
                        else
                        {
                            if (unit.Contains("Steam"))
                            {
                                PriceSubsidies = MyfloatParse1(BaseData.Rows[0][4].ToString().Trim());
                                PriceNoSubsidies = MyfloatParse1(BaseData.Rows[0][1].ToString().Trim());
                            }
                            else if (unit.Contains("Gas"))
                            {
                                PriceSubsidies = MyfloatParse1(BaseData.Rows[0][5].ToString().Trim());
                                PriceNoSubsidies = MyfloatParse1(BaseData.Rows[0][2].ToString().Trim());
                            }
                        }
                        FuelCost[j] = (PriceSubsidies * (X / Y)) + (PriceNoSubsidies * ((Y - X) / Y));
                    }

                    //Set F1
                    //AP2+BP+C //Mousavi Comment
                    //if (SF)
                    //    F1[j] = (float.Parse(UnitsDataTable.Rows[index][11].ToString().Trim()) * PowerS[j] * PowerS[j] + float.Parse(UnitsDataTable.Rows[index][12].ToString().Trim()) * PowerS[j] + float.Parse(UnitsDataTable.Rows[index][13].ToString().Trim()) * V[j]) * FuelCost[j];
                    //else
                    //    F1[j] = (float.Parse(UnitsDataTable.Rows[index][8].ToString().Trim()) * PowerS[j] * PowerS[j] + float.Parse(UnitsDataTable.Rows[index][9].ToString().Trim()) * PowerS[j] + float.Parse(UnitsDataTable.Rows[index][10].ToString().Trim()) * V[j]) * FuelCost[j];
                }

                if (SF)
                    F1[j] = ((SHeatC / SPowerC) * PowerS[j]) * FuelCost[j];
                else
                    F1[j] = ((PHeatC / PPowerC) * PowerS[j]) * FuelCost[j];

                if (unit.Contains("cc") && unit.Contains("Steam"))
                {
                    F1[j] = 0;
                }

                //SET F2
                if (V[j] != 0)
                {
                    //if (unit.Contains("Steam")) F2[j] = float.Parse(UnitsDataTable.Rows[index][6].ToString().Trim()) * PowerS[j];
                    //else F2[j] = float.Parse(UnitsDataTable.Rows[index][6].ToString().Trim()) * V[j];
                    F2[j] = (CapitalParameter * 0 * Dis[j] + MaintenanceParameter * PowerS[j] + MyfloatParse1(UnitsDataTable.Rows[index][4].ToString().Trim()) * PowerS[j]);
                }
                else F2[j] = 0;
                // SET F3
                float checking = (MyfloatParse1(UnitsDataTable.Rows[index][16].ToString().Trim()) + MyfloatParse1(UnitsDataTable.Rows[index][17].ToString().Trim())) / 2;
                if ((unit.Contains("Steam cc")) && (checking > PowerS[j]))
                {
                    if (Minus > 1) F3[j] = MyfloatParse1(UnitsDataTable.Rows[index][15].ToString().Trim());
                    else if (Minus == 1) F3[j] = (float)0.5 * MyfloatParse1(UnitsDataTable.Rows[index][14].ToString().Trim());
                    else F3[j] = 0;
                }
                else
                {
                    if (Minus > 1) F3[j] = Minus * (MyfloatParse1(UnitsDataTable.Rows[index][15].ToString().Trim()));
                    else if (Minus == 1) F3[j] = MyfloatParse1(UnitsDataTable.Rows[index][14].ToString().Trim());
                    else F3[j] = 0;

                }
            }

            CostPayment = 0;
            for (int z = 0; z < 24; z++)
                // CostPayment = CostPayment + F1[z] + F2[z] + F3[z] + MyfloatParse1(UnitsDataTable.Rows[index][5].ToString().Trim()) * V[z];
                CostPayment = 0;
               return (CostPayment);
        }
//---------------------------------isSecondFuel-----------------------
        private bool isSecondFuel(int k, int index,int Hour,string date)
        {
            string unit = UnitsDataTable.Rows[index][0].ToString().Trim();
            string package = UnitsDataTable.Rows[index][1].ToString().Trim();
            int Pcode = MyintParse1(UnitsDataTable.Rows[index][2].ToString().Trim());

            SqlConnection MyConnection = new SqlConnection(ConnectionManager.ConnectionString);
            MyConnection.Open();
            DataSet FuelDS = new DataSet();
            SqlDataAdapter Fuelda = new SqlDataAdapter();
            Fuelda.SelectCommand = new SqlCommand("SELECT SecondFuel,SecondFuelStartDate,SecondFuelEndDate,"+
            "SecondFuelStartHour,SecondFuelEndHour FROM [ConditionUnit] WHERE Date=(SELECT MAX(Date) FROM "+
            "EfficiencyMarket) AND PPID=@num AND UnitCode=@unit AND PackageType=@type", MyConnection);
            Fuelda.SelectCommand.Parameters.Add("@num", SqlDbType.NChar, 10);
            Fuelda.SelectCommand.Parameters["@num"].Value = PPIDArray[k];
            Fuelda.SelectCommand.Parameters.Add("@unit", SqlDbType.NChar, 20);
            Fuelda.SelectCommand.Parameters["@unit"].Value = unit;
            Fuelda.SelectCommand.Parameters.Add("@type", SqlDbType.NChar, 10);
            string type = package;
            if (type.Contains("CC")) type = "CC";
            Fuelda.SelectCommand.Parameters["@type"].Value = type;
            Fuelda.Fill(FuelDS);
            MyConnection.Close();

            bool SF=false;
            foreach (DataRow MyRow in FuelDS.Tables[0].Rows)
            {
                if (bool.Parse(MyRow[0].ToString().Trim()))
                {
                    //DateTime CurDate = DateTime.Now;
                    //int myhour=CurDate.Hour;
                    int myhour = Hour;
                    string startDate = MyRow[1].ToString().Trim();
                    string endDate = MyRow[2].ToString().Trim();
                    int startHour = MyintParse1(MyRow[3].ToString().Trim());
                    int endHour = MyintParse1(MyRow[4].ToString().Trim());
                    if (date == startDate)
                        if (myhour < startHour)
                            SF = false;
                        else SF = true;
                    else if (date == endDate)
                        if (myhour > endHour)
                            SF = false;
                        else SF = true;
                    else if (CheckDateSF(date, startDate, endDate))
                        SF = true;
                    else SF = false;
                }
                else SF = false;
            }

          
            return (SF);
        }
//---------------------------------------SetAvailableCap----------------------------
        private double SetAvailableCap(int k, string mydate, int index, string temp)
        {
            double AvailableCapacity = 0;
            string unit = UnitsDataTable.Rows[index][0].ToString().Trim();
            string package = UnitsDataTable.Rows[index][1].ToString().Trim();
            int Pcode = MyintParse1(UnitsDataTable.Rows[index][2].ToString().Trim());

            SqlConnection MyConnection = new SqlConnection(ConnectionManager.ConnectionString);
            MyConnection.Open();
            SqlCommand MyCom = new SqlCommand();
            MyCom.CommandText = "SELECT  @Dispatch =SUM(Dispatchable) FROM [DetailFRM005] WHERE TargetMarketDate=@date " +
            "AND PPID=@num AND Block=@block";
            MyCom.Connection = MyConnection;

            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = mydate;
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPIDArray[k];
            MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
            MyCom.Parameters["@block"].Value = temp;
            MyCom.Parameters.Add("@Dispatch", SqlDbType.Real);
            MyCom.Parameters["@Dispatch"].Direction = ParameterDirection.Output;
            //try
            //{
            MyCom.ExecuteNonQuery();
            //}
            //catch (Exception e)
            //{
            //string message = e.Message;
            //}
            try { AvailableCapacity = MyDoubleParse1(MyCom.Parameters["@Dispatch"].Value.ToString().Trim()); }
            catch { AvailableCapacity = 0; }
            //Close Connection********
            MyConnection.Close();
            return (AvailableCapacity);
        }
//------------------------------------SetTotalPower--------------------------------
        private double SetTotalPower(int k, string mydate, int index, string temp, double[] PS)
        {
            double TotalPower = 0;
            string unit = UnitsDataTable.Rows[index][0].ToString().Trim();
            string package = UnitsDataTable.Rows[index][1].ToString().Trim();
            int Pcode = MyintParse1(UnitsDataTable.Rows[index][2].ToString().Trim());

            for (int i = 0; i < 24; i++)
                TotalPower += PS[i];
                return (TotalPower);
        }
//--------------------------------------SetULPower------------------------------------
        private double SetULPower(int k, string mydate, int index, string temp)
        {
            double ULPower = 0;
            string unit = UnitsDataTable.Rows[index][0].ToString().Trim();
            string package = UnitsDataTable.Rows[index][1].ToString().Trim();
            int Pcode = MyintParse1(UnitsDataTable.Rows[index][2].ToString().Trim());

            SqlConnection MyConnection = new SqlConnection(ConnectionManager.ConnectionString);
            MyConnection.Open();
            SqlCommand MyCom = new SqlCommand();
            MyCom.CommandText = "SELECT  @UL =SUM(Required) FROM [DetailFRM005] WHERE TargetMarketDate=@date " +
            "AND PPID=@num AND Block=@block AND Contribution like '" + "UL" + '%' + "'";
            MyCom.Connection = MyConnection;

            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = mydate;
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPIDArray[k];
            MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
            MyCom.Parameters["@block"].Value = temp;
            //MyCom.Parameters.Add("@cont", SqlDbType.Char, 2);
            //MyCom.Parameters["@cont"].Value = "UL";
            MyCom.Parameters.Add("@UL", SqlDbType.Real);
            MyCom.Parameters["@UL"].Direction = ParameterDirection.Output;
            //tryDate like '" + smaxdate2 + '%' + "'");
            //{
            MyCom.ExecuteNonQuery();
            //}
            //catch (Exception e)
            //{
            //string message = e.Message;
            //}
            try { ULPower = MyDoubleParse1(MyCom.Parameters["@UL"].Value.ToString().Trim()); }
            catch { ULPower = 0; }
            //Close Connection*******
            MyConnection.Close();
            return (ULPower);
        }
//----------------------------AddDate()----------------------------
        private string NextDate(string today)
        {
            int year = MyintParse1(today.Remove(4));
            string temp=today.Remove(0,5);
            int month = MyintParse1(temp.Remove(2));
            int day = MyintParse1(temp.Remove(0, 3));
            if (month <= 6)
            {
                if (day == 31)
                {
                    day = 1;
                    month++;
                }
                else day++;
            }
            else if ((month > 6) && (month < 12))
            {
                if (day == 30)
                {
                    day = 1;
                    month++;
                }
                else day++;
            }
            else if (month==12)
            {
                //is this year KABISE?????
                if ((year - 1383) % 4 == 0)
                {
                    if (day == 30)
                    {
                        day = 1;
                        month = 1;
                        year++;
                    }
                    else day++;
                }
                else if (day == 29)
                {
                    day = 1;
                    month = 1;
                    year++;
                }
                else day++;

            }

            string sday = day.ToString();
            if (MyintParse1(sday) < 10) sday = "0" + sday;
            string smonth = month.ToString();
            if (MyintParse1(smonth) < 10) smonth = "0" + smonth;
            string tomorrow = year.ToString() + "/" + smonth + "/" + sday;

            return tomorrow;
        }
//-----------------IsLessDate()--------------------------
        private bool IsLessDate(string date1, string date2)
        {

            string temp1 = date1.Remove(4);
            string temp2 = date2.Remove(4);
            if (MyintParse1(temp1) > MyintParse1(temp2)) return false;
            else if (MyintParse1(temp1) < MyintParse1(temp2)) return true;
            else
            {
                temp1 = date1.Remove(7);
                temp1 = temp1.Remove(0, temp1.Length - 2);
                temp2 = date2.Remove(7);
                temp2 = temp2.Remove(0, temp2.Length - 2);
                if (MyintParse1(temp1) > MyintParse1(temp2)) return false;
                else if (MyintParse1(temp1) < MyintParse1(temp2)) return true;
                else
                {
                    temp1 = date1.Remove(0, date1.Length - 2);
                    temp2 = date2.Remove(0, date2.Length - 2);
                    if (MyintParse1(temp1) > MyintParse1(temp2)) return false;
                    else return true;
                }
            }
        }
////---------------------------FindNearestDate()------------------------------
//        private string FindNearestDate(string mydate, string result1, string result2)
//        {
//            if (result1 == "") return result2;
//            else if (result2 == "") return result1;
//            else
//            {
//                int Dday1 = 0, Dday2 = 0;
//                int year1 = int.Parse(result1.Remove(4));
//                int year2 = int.Parse(result2.Remove(4));
//                int myyear = int.Parse(mydate.Remove(4));
//                string Smonth1 = result1.Remove(7);
//                int month1 = int.Parse(Smonth1.Remove(0, Smonth1.Length - 2));
//                string Smonth2 = result2.Remove(7);
//                int month2 = int.Parse(Smonth2.Remove(0, Smonth2.Length - 2));
//                string Smymonth = mydate.Remove(7);
//                int mymonth = int.Parse(Smymonth.Remove(0, Smymonth.Length - 2));
//                int day1 = int.Parse(result1.Remove(0, result1.Length - 2));
//                int day2 = int.Parse(result2.Remove(0, result2.Length - 2));
//                int myday = int.Parse(mydate.Remove(0, mydate.Length - 2));

//                if (day1 > myday) 
//                {
//                    if (mymonth < 7) myday += 31; else myday += 30;
//                    mymonth = mymonth - 1;
//                    if (mymonth == 0)
//                    {
//                        myyear = myyear - 1;
//                        mymonth = 12;
//                    }
//                }
//                Dday1 = myday - day1;
//                if (month1 > mymonth)
//                {
//                    myyear = myyear - 1;
//                    mymonth += 12;
//                }
//                Dday1 += ((mymonth - month1) * 30);
//            }
//        }

//---------------------------RUnitPrimaryFuelRb_CheckedChanged----------------------
        private void FRUnitPrimaryFuelRb_CheckedChanged(object sender, EventArgs e)
        {
            if (FRUnitPrimaryFuelRb.Checked)
                FuelCostCalculate("primary");
            else
                FuelCostCalculate("secondary");
        }
//------------------------------FRUnitQReactiveTb_Validated(---------------------------
        private void FRUnitQReactiveTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitQReactiveTb.Text, 1))
                errorProvider1.SetError(FRUnitQReactiveTb, "");
            else errorProvider1.SetError(FRUnitQReactiveTb, "just real number!");

        }
//-------------------------------FRUnitPActiveTb_Validated---------------------------------
        private void FRUnitPActiveTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitPActiveTb.Text, 1))
                errorProvider1.SetError(FRUnitPActiveTb, "");
            else errorProvider1.SetError(FRUnitPActiveTb, "just real number!");
        }
 
        //-------------------------------FuelCostCalculate-----------------------
        private void FuelCostCalculate(string type)
        {
            double resultSub = 0, resultNoSub = 0;
            if (type == "primary")
            {
                double Amargin = 0;
                if (FRUnitAmargTb1.Text != "") Amargin = MyDoubleParse1(FRUnitAmargTb1.Text);
                double Bmargin = 0;
                if (FRUnitBmargTb1.Text != "") Bmargin = MyDoubleParse1(FRUnitBmargTb1.Text);
                double Cmargin = 0;
                if (FRUnitCmargTb1.Text != "") Cmargin = MyDoubleParse1(FRUnitCmargTb1.Text);
                double HeatValue = 0;
                if (GDPrimaryFuelTB.Text != "") HeatValue = MyDoubleParse1(GDPrimaryFuelTB.Text);
                double Pactive = 0;
                if (FRUnitPActiveTb.Text != "") Pactive = MyDoubleParse1(FRUnitPActiveTb.Text);
                double Qreactive = 0;
                if (FRUnitQReactiveTb.Text != "") Qreactive = MyDoubleParse1(FRUnitQReactiveTb.Text);
                DataTable ODataTable = null;
                ODataTable = Utilities.GetTable("select GasSubsidiesPrice,GasPrice from BaseData where Date='" + smaxdate + "'order by BaseID desc");
                double FirstRe = (Amargin * Pactive * Pactive) + (Bmargin * Pactive) + Cmargin;
                if ((FirstRe != 0) && ((Qreactive != 0) || (Pactive != 0)))
                {
                    double SecondRe = (Qreactive * Qreactive) / (Qreactive * Qreactive + Pactive * Pactive);
                    resultSub = FirstRe * (HeatValue - (859000.0 * (Pactive / FirstRe))) * SecondRe * (MyDoubleParse1(ODataTable.Rows[0][0].ToString()) / 859000.0);
                    resultNoSub = FirstRe * (HeatValue - (859000.0 * (Pactive / FirstRe))) * SecondRe * (MyDoubleParse1(ODataTable.Rows[0][1].ToString()) / 859000.0);
                }
            }
            else
            {
                double Amargin = 0;
                if (FRUnitAmargTb2.Text != "") Amargin = MyDoubleParse1(FRUnitAmargTb2.Text);
                double Bmargin = 0;
                if (FRUnitBmargTb2.Text != "") Bmargin = MyDoubleParse1(FRUnitBmargTb2.Text);
                double Cmargin = 0;
                if (FRUnitCmargTb2.Text != "") Cmargin = MyDoubleParse1(FRUnitCmargTb2.Text);
                double HeatValue = 0;
                if (GDSecondaryFuelTB.Text != "") HeatValue = MyDoubleParse1(GDSecondaryFuelTB.Text);
                double Pactive = 0;
                if (FRUnitPActiveTb.Text != "") Pactive = MyDoubleParse1(FRUnitPActiveTb.Text);
                double Qreactive = 0;
                if (FRUnitQReactiveTb.Text != "") Qreactive = MyDoubleParse1(FRUnitQReactiveTb.Text);
                DataTable ODataTable = null;
                ODataTable = Utilities.GetTable("select MazutSubsidiesPrice,MazutPrice,GasOilSubsidiesPrice,GasOilPrice from BaseData where Date='" + smaxdate + "'order by BaseID desc");
                double FirstRe = (Amargin * Pactive * Pactive) + (Bmargin * Pactive) + Cmargin;
                if ((FirstRe != 0) && ((Qreactive != 0) || (Pactive != 0)))
                {
                    double SecondRe = (Qreactive * Qreactive) / (Qreactive * Qreactive + Pactive * Pactive);
                    if (FRUnitLb.Text.Contains("Gas"))
                    {
                        resultSub = FirstRe * (HeatValue - (859000.0 * (Pactive / FirstRe))) * SecondRe * (MyDoubleParse1(ODataTable.Rows[0][2].ToString()) / 859000.0);
                        resultNoSub = FirstRe * (HeatValue - (859000.0 * (Pactive / FirstRe))) * SecondRe * (MyDoubleParse1(ODataTable.Rows[0][3].ToString()) / 859000.0);
                    }
                    else
                    {
                        resultSub = FirstRe * (HeatValue - (859000.0 * (Pactive / FirstRe))) * SecondRe * (MyDoubleParse1(ODataTable.Rows[0][0].ToString()) / 859000.0);
                        resultNoSub = FirstRe * (HeatValue - (859000.0 * (Pactive / FirstRe))) * SecondRe * (MyDoubleParse1(ODataTable.Rows[0][1].ToString()) / 859000.0);
                    }
                }
            }
            FRUnitFuelNoCostTb.Text = resultNoSub.ToString();
            FRUnitFuelCostTb.Text = resultSub.ToString();
        }
//----------------------------CheckDate--------------------------------
        //private bool CheckDate(string date, string start, string end)
        //{
        //    bool result = false;
        //    string temp1 = date.Remove(4);
         
        //    start = start.Trim();
        //    end = end.Trim();
        //    if ((start != "") && (end != ""))
        //    {
        //        string temp2 = start.Remove(4);
        //        if (int.Parse(temp1) > int.Parse(temp2)) result = true;
        //        else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
        //        else
        //        {
        //            temp1 = date.Remove(7);
        //            temp1 = temp1.Remove(0, temp1.Length - 2);
        //            temp2 = start.Remove(7);
        //            temp2 = temp2.Remove(0, temp2.Length - 2);
        //            if (int.Parse(temp1) > int.Parse(temp2)) result = true;
        //            else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
        //            else
        //            {
        //                temp1 = date.Remove(0, date.Length - 2);
        //                temp2 = start.Remove(0, start.Length - 2);
        //                if (int.Parse(temp1) > int.Parse(temp2)) result = true;
        //                else result = false;
        //            }
        //        }
        //        if (!result) return (result);
        //        else
        //        {
        //            temp1 = date.Remove(4);
        //            temp2 = end.Remove(4);
        //            if (int.Parse(temp1) < int.Parse(temp2)) return (true);
        //            else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
        //            else
        //            {
        //                temp1 = date.Remove(7);
        //                temp1 = temp1.Remove(0, temp1.Length - 2);
        //                temp2 = end.Remove(7);
        //                temp2 = temp2.Remove(0, temp2.Length - 2);
        //                if (int.Parse(temp1) < int.Parse(temp2)) return (true);
        //                else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
        //                else
        //                {
        //                    temp1 = date.Remove(0, date.Length - 2);
        //                    temp2 = end.Remove(0, end.Length - 2);
        //                    if (int.Parse(temp1) < int.Parse(temp2)) return (true);
        //                    else return (false);
        //                }
        //            }
        //        }
        //    }
        //    return false;
        //}
//------------------------------GetNearestDatetowcpf---------------------------
        private string GetNearestDatetowcpf(int year, int week)
        {
            string strComd = "select max(Year) from dbo.WCPF where Year<='" + year + "' and Week='" + week + "'";

            DataTable oDatatable = Utilities.GetTable(strComd);
            if (oDatatable != null && oDatatable.Rows.Count > 0 && oDatatable.Rows[0][0].ToString() != "")
                return oDatatable.Rows[0][0].ToString().Trim();

            return null;
        }
//----------------------------GetNearestDatetohcpf------------------------
        private string GetNearestDatetohcpf(string nowdate)
        {
            string datelike = nowdate.Substring(4);
            string date = nowdate;

            string strComd = "select max(Date) from HCPF where Date <='" + date + "'and Date like '" + "%" + datelike + "'";

            DataTable oDatatable = Utilities.GetTable(strComd);
            if (oDatatable != null && oDatatable.Rows.Count > 0 && oDatatable.Rows[0][0].ToString() != "")
                return oDatatable.Rows[0][0].ToString().Trim();

            return null;

        }
//----------------------------CheckDateSF--------------------------------
        private bool CheckDateSF(string date, string start, string end)
        {
            bool result = false;
            string temp1 = date.Remove(4);
            start = start.Trim();
            end = end.Trim();
            if ((start != "") && (end != ""))
            {
                string temp2 = start.Remove(4);
                if (MyintParse1(temp1) > MyintParse1(temp2)) result = true;
                else if (MyintParse1(temp1) < MyintParse1(temp2)) result = false;
                else
                {
                    temp1 = date.Remove(7);
                    temp1 = temp1.Remove(0, temp1.Length - 2);
                    temp2 = start.Remove(7);
                    temp2 = temp2.Remove(0, temp2.Length - 2);
                    if (MyintParse1(temp1) > MyintParse1(temp2)) result = true;
                    else if (MyintParse1(temp1) < MyintParse1(temp2)) result = false;
                    else
                    {
                        temp1 = date.Remove(0, date.Length - 2);
                        temp2 = start.Remove(0, start.Length - 2);
                        if (MyintParse1(temp1) > MyintParse1(temp2)) result = true;
                        else result = false;
                    }
                }
                if (!result) return (result);
                else
                {
                    temp1 = date.Remove(4);
                    temp2 = end.Remove(4);
                    if (MyintParse1(temp1) < MyintParse1(temp2)) return (true);
                    else if (MyintParse1(temp1) > MyintParse1(temp2)) return (false);
                    else
                    {
                        temp1 = date.Remove(7);
                        temp1 = temp1.Remove(0, temp1.Length - 2);
                        temp2 = end.Remove(7);
                        temp2 = temp2.Remove(0, temp2.Length - 2);
                        if (MyintParse1(temp1) < MyintParse1(temp2)) return (true);
                        else if (MyintParse1(temp1) > MyintParse1(temp2)) return (false);
                        else
                        {
                            temp1 = date.Remove(0, date.Length - 2);
                            temp2 = end.Remove(0, end.Length - 2);
                            if (MyintParse1(temp1) < MyintParse1(temp2))// || (temp1 == temp2))
                                return (true);
                            else return (false);
                        }
                    }
                }
            }
            return false;
        }
//----------------------FRPlot_Click-----------------------
        private void FRPlot_Click(object sender, EventArgs e)
        {
            string frdate1 = "";
            string frdate2 = "";
            string frdate3 = "";
            string frdate4 = "";
            string frdate5 = "";
            string frdate6 = "";
            string frdate7 = "";

            

            if (FRPlantCal.Text != "[Empty Value]")
            {

                frdate1 = FRPlantCal.Text;
                frdate2 = new PersianDate(PersianDateConverter.ToGregorianDateTime(FRPlantCal.Text).AddDays(-1)).ToString("d");
                frdate3 = new PersianDate(PersianDateConverter.ToGregorianDateTime(FRPlantCal.Text).AddDays(-2)).ToString("d");
                frdate4 = new PersianDate(PersianDateConverter.ToGregorianDateTime(FRPlantCal.Text).AddDays(-3)).ToString("d");
                frdate5 = new PersianDate(PersianDateConverter.ToGregorianDateTime(FRPlantCal.Text).AddDays(-4)).ToString("d");
                frdate6 = new PersianDate(PersianDateConverter.ToGregorianDateTime(FRPlantCal.Text).AddDays(-5)).ToString("d");
                frdate7 = new PersianDate(PersianDateConverter.ToGregorianDateTime(FRPlantCal.Text).AddDays(-6)).ToString("d");
              
            }
            else
            {
                frdate1 = "";
                frdate2 = "";
                frdate3 = "";
                frdate4 = "";
                frdate5 = "";
                frdate6 = "";
                frdate7 = "";
            }
                    
            if ((!FRUnitPanel.Visible) && (!FRPlantCal.IsNull))
            {
                FRPlot FinancialChart = new FRPlot(PPID, frdate7+"-"+frdate1);
                FinancialChart.Show();
            }

        }
//----------------------FRRun_Click-----------------------
        private void FRRun_Click(object sender, EventArgs e)
        {


           // frdailyvalue();

            FillFRValues(PPID);




            /////////////////kafaei/////////////////////////////////////////////////////////////

            //string mydate = FRPlantCal.Text;
            ////------------------------------------------------------------------------

            //DataTable dtsmaxdate = utilities.GetTable("select * from dbo.BaseData where Date<='" + mydate + "'order by BaseID desc");
            //if (dtsmaxdate.Rows.Count > 0)
            //{
            //    string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
            //    int ib = 0;
            //    foreach (DataRow m in dtsmaxdate.Rows)
            //    {
            //        arrbasedata[ib] = m["Date"].ToString();
            //        ib++;
            //    }
            //    smaxdate = buildmaxdate1(arrbasedata);
            //}
            //else
            //{
            //    dtsmaxdate = utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
            //    smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            //}

            ////--------------------------------------------------------------------------

           
            //int k=PPIDArray.IndexOf(PPID);
            ////detect Yesterday
            //System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
            //DateTime CurDate = DateTime.Now; //DateTime.Parse("2010/07/16"); //
            //int imonth = pr.GetMonth(CurDate);
            //int iyear = pr.GetYear(CurDate);
            //int iday = pr.GetDayOfMonth(CurDate) - 1;
            //if (iday < 1)
            //{
            //    imonth = imonth - 1;
            //    if (imonth < 1)
            //    {
            //        iyear = iyear - 1;
            //        imonth = 12;
            //    }
            //    if (imonth < 7) iday = 31;
            //    else if (imonth < 12) iday = 30;
            //    //is this year, KABISE?
            //    else if ((iyear - 1383) % 4 == 0) iday = 30;
            //    else iday = 29;
            //}
            //string day = iday.ToString();
            //if (MyintParse1(day) < 10) day = "0" + day;
            //string month = imonth.ToString();
            //if (MyintParse1(month) < 10) month = "0" + month;
            //string yesterday = iyear.ToString() + "/" + month + "/" + day;
            //if (string.Compare(yesterday, mydate) >= 0)
            //{
            //    //Delete
            //    DataTable deleteTable = utilities.GetTable("DELETE FROM EconomicUnit WHERE Date='" + mydate + "' AND PPID='" + PPID + "'");
            //    deleteTable = utilities.GetTable("DELETE FROM EconomicPlant WHERE Date='" + mydate + "' AND PPID='" + PPID + "'");
            //    //
            //    FillEconomicPlant(k, mydate);
            //    FillFRValues(PPID);
            //}
            //else MessageBox.Show("You Must Select A Date Before "+new PersianDate(CurDate).ToString("d")+"!");
        }

       
       


      

     
     




//------------------------ThereAreValues(k,mydate)-------------------
        private bool ThereAreValues(int k, string mydate)
        {
            DataTable IsThere =null;
            IsThere = Utilities.GetTable("SELECT * FROM DetailFRM002 WHERE PPID='" + PPIDArray[k].ToString().Trim() + "' AND TargetMarketDate='" + mydate + "'");
            
            if ((IsThere == null)||(IsThere.Rows.Count==0))
                return false;
            else
            {
                IsThere = null;
                IsThere = Utilities.GetTable("SELECT * FROM DetailFRM005 WHERE PPID='" + PPIDArray[k].ToString().Trim() + "' AND TargetMarketDate='" + mydate + "'");
                if ((IsThere == null) || (IsThere.Rows.Count == 0))
                    return false;
                else
                {
                    IsThere = null;
                    IsThere = Utilities.GetTable("SELECT * FROM DetailFRM009 WHERE PPID='" + PPIDArray[k].ToString().Trim() + "' AND TargetMarketDate='" + mydate + "'");
                    if ((IsThere == null) || (IsThere.Rows.Count == 0))
                        return false;
                    else return true;
                }
            }
        }

        private void FinancialReport_Click(object sender, EventArgs e)
        {
           
        }

        private void lblinsufficient_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show(MissDates, " The Items are: ");

        }

        private void FRRunAuto_Click(object sender, EventArgs e)
        {
            string mydate = FRPlantCal.Text;
            //------------------------------------------------------------------------

            //DataTable dtsmaxdate = utilities.GetTable("select * from dbo.BaseData where Date<='" + mydate + "'order by BaseID desc");
            //if (dtsmaxdate.Rows.Count > 0)
            //{
            //    string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
            //    int ib = 0;
            //    foreach (DataRow m in dtsmaxdate.Rows)
            //    {
            //        arrbasedata[ib] = m["Date"].ToString();
            //        ib++;
            //    }
            //    smaxdate = buildmaxdate1(arrbasedata);
            //}
            //else
            //{
            //    dtsmaxdate = utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
            //    smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            //}

            //--------------------------------------------------------------------------
            try
            {
                AutomaticFillEconomics(); 
               
            }
            catch
            {
                MessageBox.Show("  UnSuccessfully in : "+daterror);
            }
        }

 //--------------------------frprint---------------------------------
        private void btnfrprint_Click(object sender, EventArgs e)
        {
            try
            {
                //DataTable dt = utilities.GetTable("SELECT PPID,Benefit,Income,Cost,AvailableCapacity,TotalPower,BidPower," +
                //"ULPower,IncrementPower,DecreasePower,CapacityPayment,EnergyPayment,BidPayment,ULPayment,IncrementPayment," +
                //"DecreasePayment FROM [EconomicPlant] WHERE Date='" + FRPlantCal.Text + "' AND PPID=" + PPID);
                string frdate1 = "";             
                string frdate7 = "";



                if (FRPlantCal.Text != "[Empty Value]")
                {

                    frdate1 = FRPlantCal.Text;                  
                    frdate7 = new PersianDate(PersianDateConverter.ToGregorianDateTime(FRPlantCal.Text).AddDays(-6)).ToString("d");

                }
                else
                {
                    frdate1 = "";                  
                    frdate7 = "";
                }


                DataTable dt = Utilities.GetTable("SELECT Date , round(Benefit,0,0) as 'سود خالص نيروگاه' ,round(Income,0,0) as 'درآمد خالص نيروگاه',round(Cost,0,0) as 'هزينه سوخت نيروگاه',round(AvailableCapacity,0,0) as 'ميزان آمادگي-مگاوات ساعت',round(TotalPower,0,0) as 'ميزان توليد ناخالص-مگاوات ساعت',round(BidPower,0,0) as 'ميزان خالص پذيرفته شده در بازار-مگاوات ساعت'," +
                "round(ULPower,0,0) as 'ميزان خالص پذيرفته شده با نرخ UL-مگاوات ساعت',round(IncrementPower,0,0) as 'ميزان  خالص توليدبيش ازپيشنهادبازارو به دستور مرکز-مگاوات ساعت',round(DecreasePower,0,0) as 'ميزان توليدکمترازپيشنهاد بازارو به دستور مرکز-مگاوات ساعت',round((Income/nullif(TotalPower,0)),0,0)AS 'PerMw'   FROM [EconomicPlant] WHERE Date>='" + frdate7 + "' AND date<='" + frdate1 + "'and  PPID='" + PPID + "'order by date asc");


                FinanCialPrint m = new FinanCialPrint(dt, FRPlantCal.Text.Trim(), PPID);
                m.Show();

               
            }
            catch
            {

            }

        }

     
  //////////////////plant tool tip text //////////////////////////////////////     
        private void btnfinanceexcel_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowNewFolderButton = true;
            DialogResult answer = folderBrowserDialog1.ShowDialog();
            if (answer == DialogResult.OK)
            {
                string path = folderBrowserDialog1.SelectedPath;



                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                Excel.Application oExcel = new Excel.Application();

                oExcel.SheetsInNewWorkbook = 1;
                Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
                Excel.Worksheet wsheet = null;
                wsheet = ((Excel.Worksheet)WB.Worksheets["Sheet1"]);


                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers
                oExcel.DefaultSheetDirection = (int)Excel.Constants.xlRTL;


                wsheet.DisplayRightToLeft = false;

                int sheetIndex = 1;

                wsheet.Name = FRPlantCal.Text.Replace("/", "").Trim();
                Excel.Range range = null;




                try
                {
                    int iX;
                    int iY;
                    for (int i = 0; i < this.dataGridViewECO.Columns.Count; i++)
                    {
                        wsheet.Cells[1, i + 1] = this.dataGridViewECO.Columns[i].HeaderText;

                    }
                    for (int i = 0; i < this.dataGridViewECO.Rows.Count; i++)
                    {
                        DataGridViewRow row = this.dataGridViewECO.Rows[i];
                        for (int j = 0; j < row.Cells.Count; j++)
                        {
                            DataGridViewCell cell = row.Cells[j];
                            try
                            {
                                wsheet.Cells[i + 2, j + 1] = (cell.Value == null) ? "" : cell.Value.ToString();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                    }


                    for (int i = 1; i <= dataGridViewECO.Rows.Count + 1; i++)
                    {

                        for (int j = 1; j <= dataGridViewECO.Columns.Count; j++)
                        {
                            range = (Excel.Range)wsheet.Cells[i, j];
                            range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                            range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;

                            range.Borders.Weight = 2;
                            range.Font.Size = 10;
                            if (i == 1)
                            {
                                range.Font.Bold = true;
                                range.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(204, 255, 255));
                                range.Columns.AutoFit();
                            }
                            range.Borders.LineStyle = Excel.Constants.xlSolid;
                            range.Cells.RowHeight = 18;


                        }
                    }


                    ////////////////////////////////////////////////////

                    range = (Excel.Range)wsheet.Cells[1, 1];
                    range.ColumnWidth = 20;
                    range.Cells.RowHeight = 18;

                    /////////////////////////////////////////////////////



                   // path = path + "\\" + lbplantmarket.Text.Trim() + "-" + FRPlantCal.Text.Trim().Replace("/", "").Trim() + "-FinancialReport.xls";
                    path = path + "\\"+ FRPlantCal.Text.Trim().Replace("/", "").Trim() + "-FinancialReport.xls";
                    WB.SaveAs(path, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                    WB.Close(Missing.Value, Missing.Value, Missing.Value);
                    oExcel.Workbooks.Close();
                    oExcel.Quit();
                    MessageBox.Show(" Export Successfully in " + path, "Export");

                }
                catch (Exception ex1)
                {
                    MessageBox.Show("Export UnSuccessfully ", "Export");
                }





            }
        }

        private void FRPlantBenefitTB_Leave(object sender, EventArgs e)
        {           
          toolTip1.SetToolTip(FRPlantBenefitTB, "");            
        }

        private void FRPlantBenefitTB_MouseClick(object sender, MouseEventArgs e)
        {           
          toolTip1.SetToolTip(FRPlantBenefitTB, "Rial");           
        }
        private void FRPlantIncomeTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRPlantIncomeTb, "");
        }

        private void FRPlantIncomeTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRPlantIncomeTb, "Rial");
        }
        private void FRPlantCostTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRPlantCostTb, "Rial");
        }

        private void FRPlantCostTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRPlantCostTb, "");
        }

        private void FRPlantAvaCapTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRPlantAvaCapTb, "");
        }

        private void FRPlantAvaCapTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRPlantAvaCapTb, "MW");
        }

        private void FRPlantTotalPowerTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRPlantTotalPowerTb, "MW");
        }

        private void FRPlantTotalPowerTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRPlantTotalPowerTb, "");
        }

        private void FRPlantBidPowerTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRPlantBidPowerTb, "");
        }

        private void FRPlantBidPowerTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRPlantBidPowerTb, "MW");
        }

        private void FRPlantULPowerTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRPlantULPowerTb, "MW");
        }

        private void FRPlantULPowerTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRPlantULPowerTb, "");
        }

        private void FRPlantIncPowerTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRPlantIncPowerTb, "");
        }

        private void FRPlantIncPowerTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRPlantIncPowerTb, "MW");
        }

        private void FRPlantDecPowerTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRPlantDecPowerTb, "MW");
        }

        private void FRPlantDecPowerTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRPlantDecPowerTb, "");
        }

        private void FRPlantCapPayTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRPlantCapPayTb, "");
        }

        private void FRPlantCapPayTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRPlantCapPayTb, "Rial");
        }

        private void FRPlantEnergyPayTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRPlantEnergyPayTb, "Rial");
        }

        private void FRPlantEnergyPayTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRPlantEnergyPayTb, "");
        }

        private void FRPlantBidPayTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRPlantBidPayTb, "");
        }

        private void FRPlantBidPayTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRPlantBidPayTb, "Rial");
        }

        private void FRPlantULPayTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRPlantULPayTb, "Rial");
        }

        private void FRPlantULPayTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRPlantULPayTb, "");
        }

        private void FRPlantIncPayTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRPlantIncPayTb, "");
        }

        private void FRPlantIncPayTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRPlantIncPayTb, "Rial");
        }

        private void FRPlantDecPayTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRPlantDecPayTb, "");
        }

        private void FRPlantDecPayTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRPlantDecPayTb, "Rial");
        }
        //////////////////////////////unit tool tip//////////////////////////////////////////////

        private void FRUnitCapitalTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRUnitCapitalTb, "Rial/MW");
        }

        private void FRUnitCapitalTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRUnitCapitalTb, "");
        }

        private void FRUnitFixedTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRUnitFixedTb, "");
        }

        private void FRUnitFixedTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRUnitFixedTb, "Rial/MW");
        }

        private void FRUnitVariableTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRUnitVariableTb, "Rial/MWH");
        }

        private void FRUnitVariableTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRUnitVariableTb, "");
        }

        private void FRUnitColdTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRUnitColdTb, "");
        }

        private void FRUnitColdTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRUnitColdTb, "Rial/Start");
        }

        private void FRUnitHotTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRUnitHotTb, "Rial/Start");
        }

        private void FRUnitHotTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRUnitHotTb, "");
        }

        private void FRUnitQReactiveTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRUnitQReactiveTb, "");
        }

        private void FRUnitQReactiveTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRUnitQReactiveTb, "MVAR");
        }

        private void FRUnitPActiveTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRUnitPActiveTb, "MW");
        }

        private void FRUnitPActiveTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRUnitPActiveTb, "");
        }

        private void FRUnitFuelNoCostTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRUnitFuelNoCostTb, "");
        }

        private void FRUnitFuelNoCostTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRUnitFuelNoCostTb, "Rial/MVAR");
        }

        private void FRUnitFuelCostTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRUnitFuelCostTb, "Rial/MVAR");
        }

        private void FRUnitFuelCostTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRUnitFuelCostTb, "");
        }

        private void FRUnitCapacityTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRUnitCapacityTb, "");
        }

        private void FRUnitCapacityTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRUnitCapacityTb, "MWH");
        }

        private void FRUnitTotalPowerTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRUnitTotalPowerTb, "MWH");
        }

        private void FRUnitTotalPowerTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRUnitTotalPowerTb, "");
        }

        private void FRUnitULPowerTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRUnitULPowerTb, "");
        }

        private void FRUnitULPowerTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRUnitULPowerTb, "MWH");
        }

        private void FRUnitIncomeTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRUnitIncomeTb, "Rial");
        }

        private void FRUnitIncomeTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRUnitIncomeTb, "");
        }

        private void FRUnitCapPayTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRUnitCapPayTb, "");
        }

        private void FRUnitCapPayTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRUnitCapPayTb, "Rial");
        }

        private void FRUnitEneryPayTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(FRUnitEneryPayTb, "Rial");
        }

        private void FRUnitEneryPayTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(FRUnitEneryPayTb, "");
        }

        private string buildmaxdate1(string[] arrmax)
        {
            string maxdate = null;
            for (int fd = 0; fd < arrmax.Length; fd++)
            {
                if (maxdate == null || string.Compare(maxdate, arrmax[fd]) < 0)
                    maxdate = arrmax[fd];
            }
            return maxdate;

        }

        private static double MyDoubleParse1(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private static int MyintParse1(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return int.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private static float MyfloatParse1(string str)
        {
            if (str.Trim() == "")
                return 0.0f;
            else
            {
                try { return float.Parse(str.Trim()); }
                catch { return 0.0f; }
            }
        }
    }
}