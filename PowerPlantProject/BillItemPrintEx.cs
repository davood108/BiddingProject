﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using NRI.SBS.Common;
using FarsiLibrary.Utils;
using System.Collections;
using System.IO;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using Auto_Update_Library;

namespace PowerPlantProject
{
    public partial class BillItemPrintEx : Form
    {
        int modItem;
        string dateTime;
        
        public BillItemPrintEx(int modeItem, string date)
        {
            //datePickerFrom.Visible = false;
            //panel81.Visible = false; 
            modItem = modeItem;
            dateTime = date;

            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }



        public void fillcheck()
        {

          this.checkedListBox1.Items.Clear();
          DataTable bb = Utilities.GetTable("select distinct code,item from dbo.MonthlyBillTotal where month like'%" + datePickerFrom.Text.Substring(0, 7).Trim() + "%' order by code asc");
         
              if (bb.Rows.Count == 0)  
              {
                 bb = Utilities.GetTable("select distinct code,item from dbo.MonthlyBillTotal where month in (select max(month) from MonthlyBillTotal) order by code asc");

               }
            //if (bb.Rows.Count == 0) return;
            for (int y = 0; y < bb.Rows.Count; y++)
            {
                string nn = bb.Rows[y][0].ToString().Trim();
                if (nn.Contains("01") || nn.Contains("02") || nn.Contains("03") || nn.Contains("04") || nn.Contains("05") || nn.Contains("06") || nn.Contains("07") || nn.Contains("08") || nn.Contains("09"))
                    this.checkedListBox1.Items.Add(bb.Rows[y][0].ToString().Replace("0", "").Trim() + "=" + bb.Rows[y][1].ToString().Trim());
                else this.checkedListBox1.Items.Add(bb.Rows[y][0].ToString().Trim() + "=" + bb.Rows[y][1].ToString().Trim());
            }

            //for (int b = 0; b < checkedListBox1.Items.Count; b++)
            //{
            //    this.checkedListBox1.SetItemChecked(b, true);
            //}


            //DataTable d = utilities.GetTable("select * from billitem where  date like'%" + datePickerFrom.Text.Substring(0, 7).Trim() + "%'");
            ////if (d.Rows.Count == 0)
            ////{
            ////    d = utilities.GetTable("select * from billitem where  date in ( select max(date) from billitem ) ");

            ////}
            //if (d.Rows.Count == 0)
            //{
            //    for (int b = 0; b < checkedListBox1.Items.Count; b++)
            //    {
            //        this.checkedListBox1.SetItemChecked(b, false);
            //    }
            //    return;

            //}

            //try
            //{
            //    for (int indexChecked = 0; indexChecked < checkedListBox1.Items.Count; indexChecked++)
            //    {

            //        string c = checkedListBox1.Items[indexChecked].ToString();
            //        string[] v = c.Split('=');

            //        if (v[0].Trim().ToLower() == d.Columns[indexChecked + 1].ToString().ToLower())
            //        {
            //            if (d.Rows[0][indexChecked].ToString().Trim() == "1" || d.Rows[0][indexChecked].ToString().Trim() == "True")
            //                checkedListBox1.SetItemChecked(indexChecked, true);
            //            else checkedListBox1.SetItemChecked(indexChecked, false);

            //            if (v[0].Trim().ToLower() == "s15")
            //                checkedListBox1.SetItemChecked(indexChecked, true);
            //        }
            //    }
            //}
            //catch
            //{

            //}

            ///////////////////////////////////////////////////////////
            for (int b = 0; b < checkedListBox1.Items.Count; b++)
            {


                this.checkedListBox1.SetItemChecked(b, false);
            }


            DataTable d = Utilities.GetTable("select * from ItemSelection");
            if (d.Rows.Count > 0)
            {
                for (int indexChecked = 0; indexChecked < checkedListBox1.Items.Count; indexChecked++)
                {

                    string c = checkedListBox1.Items[indexChecked].ToString();
                    string[] v = c.Split('=');

                    if (v[0].Trim().ToLower() == d.Columns[indexChecked].ToString())
                    {
                        if (d.Rows[0][indexChecked].ToString().Trim() == "1" || d.Rows[0][indexChecked].ToString().Trim() == "True")
                            checkedListBox1.SetItemChecked(indexChecked, true);
                        else checkedListBox1.SetItemChecked(indexChecked, false);

                    }
                }
            }

        }

         //if (modItem == 0)
         //   {
         //       if (checkedListBoxitmslk.CheckedItems.Count == 1)
         //       {
         //           DataTable s = utilities.GetTable("delete from ItemSelection");
         //           string c = checkedListBoxitmslk.CheckedItems[0].ToString();
         //           string[] v = c.Split('=');
         //           s = utilities.GetTable("insert into ItemSelection (" + v[0].Trim() + ")values('1')");

         //           MessageBox.Show("Saved.");
         //           this.Close();
         //       }
         //       else
         //       {
         //           MessageBox.Show("Please Select one item");
         //       }
         //   }
         //   else
         //   {

         //       DataTable s = utilities.GetTable("delete from ItemSelection");
         //       int i = 0;
         //       for (int indexChecked = 0; indexChecked < checkedListBoxitmslk.CheckedItems.Count; indexChecked++)
         //       {

         //           string c = checkedListBoxitmslk.CheckedItems[indexChecked].ToString();

         //           string[] v = c.Split('=');
         //           if (i == 0)
         //           {
         //               s = utilities.GetTable("insert into ItemSelection (" + v[0].Trim() + ")values('1')");
         //           }
         //           else
         //           {
         //               s = utilities.GetTable("update ItemSelection  set " + v[0].Trim() + " ='1'");
         //           }
         //           i++;

                    
         //          // PrintFinancialReportEx ppfrE = new PrintFinancialReportEx();
         //           //ppfrE.textBox9.Text = v[0].Trim();
             

         //       }
         //       MessageBox.Show("Saved.");
         //       this.Close();
         //   }

        private void button1_Click(object sender, EventArgs e)
        {
            //DataTable s = utilities.GetTable("delete from billitem where  date like'%" + datePickerFrom.Text.Substring(0, 7).Trim() + "%'");
            //int i = 0;
            //for (int indexChecked = 0; indexChecked < checkedListBox1.CheckedItems.Count; indexChecked++)
            //{

            //    string c = checkedListBox1.CheckedItems[indexChecked].ToString();

            //    string[] v = c.Split('=');
            //    if (i == 0)
            //    {
            //        s = utilities.GetTable("insert into billitem (" + v[0].Trim() + ",date)values('1','" + datePickerFrom.Text.Trim() + "')");
            //    }
            //    else
            //    {
            //        s = utilities.GetTable("update billitem  set " + v[0].Trim() + " ='1'where date='" + datePickerFrom.Text.Trim() + "'");
            //    }
            //    i++;


            //}


            //MessageBox.Show("Saved.");
            /////////////////////////////////////////////////////////////////
            if (modItem == 0)
            {
                if (checkedListBox1.CheckedItems.Count == 1)
                {
                    DataTable s = Utilities.GetTable("delete from ItemSelection");
                    string c = checkedListBox1.CheckedItems[0].ToString();
                    string[] v = c.Split('=');
                    s = Utilities.GetTable("insert into ItemSelection (" + v[0].Trim() + ")values('1')");

                    MessageBox.Show("Saved.");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Please Select one item");
                }
            }
            else
            {

                DataTable s = Utilities.GetTable("delete from ItemSelection");
                int i = 0;
                for (int indexChecked = 0; indexChecked < checkedListBox1.CheckedItems.Count; indexChecked++)
                {

                    string c = checkedListBox1.CheckedItems[indexChecked].ToString();

                    string[] v = c.Split('=');
                    if (i == 0)
                    {
                        s = Utilities.GetTable("insert into ItemSelection (" + v[0].Trim() + ")values('1')");
                    }
                    else
                    {
                        s = Utilities.GetTable("update ItemSelection  set " + v[0].Trim() + " ='1'");
                    }
                    i++;


                    // PrintFinancialReportEx ppfrE = new PrintFinancialReportEx();
                    //ppfrE.textBox9.Text = v[0].Trim();


                }
                MessageBox.Show("Saved.");
                this.Close();
            }
        }

        //private void button2_Click(object sender, EventArgs e)
        //{
        //    BillName n = new BillName(datePickerFrom.Text.Trim());
        //    n.Show();
        //}

        private void datePickerFrom_ValueChanged(object sender, EventArgs e)
        {
            if (modItem == 0)
            {
                fillcheck();
            }
            else
            {
                if (datePickerFrom.Text.ToString() != dateTime.ToString())
                {
                    MessageBox.Show("Please don't change the date");
                    datePickerFrom.Text = dateTime.ToString();
                }
            }
        }

        private void BillItemPrintEx_Load(object sender, EventArgs e)
        {          


            if (modItem == 0)
            {
                datePickerFrom.Text = new PersianDate(DateTime.Now).ToString("d");
                fillcheck();
            }
            //datePickerFrom.Text = new PersianDate(DateTime.Now).ToString("d");
            datePickerFrom.Text = dateTime.ToString();
            fillcheck();


            /////////////////////////////for clear function/////////////////////////////////////
            if (modItem == 0)
            {
                if (checkedListBox1.CheckedItems.Count > 1)
                {
                    DataTable s = Utilities.GetTable("delete from ItemSelection");
                    for (int b = 0; b < checkedListBox1.Items.Count; b++)
                    {
                        this.checkedListBox1.SetItemChecked(b, false);
                    }
                }
            }


        }
    }
}

