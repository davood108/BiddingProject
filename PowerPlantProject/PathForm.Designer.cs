﻿namespace PowerPlantProject
{
    partial class PathForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PathForm));
            this.CapacityTb = new System.Windows.Forms.TextBox();
            this.LoadTb = new System.Windows.Forms.TextBox();
            this.PriceTb = new System.Windows.Forms.TextBox();
            this.M005Tb = new System.Windows.Forms.TextBox();
            this.M002Tb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.txtProxy = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.M009Tb = new System.Windows.Forms.TextBox();
            this.M0091Tb = new System.Windows.Forms.TextBox();
            this.CounterTb = new System.Windows.Forms.TextBox();
            this.txtInternetUsername = new System.Windows.Forms.TextBox();
            this.lblInternetPassword = new System.Windows.Forms.Label();
            this.txtInternetPassword = new System.Windows.Forms.TextBox();
            this.lblInternetUserName = new System.Windows.Forms.Label();
            this.txtDomain = new System.Windows.Forms.TextBox();
            this.lblDomain = new System.Windows.Forms.Label();
            this.rbFTPPath002 = new System.Windows.Forms.RadioButton();
            this.rbHardPath002 = new System.Windows.Forms.RadioButton();
            this.txtftpip = new System.Windows.Forms.TextBox();
            this.txtftpuser = new System.Windows.Forms.TextBox();
            this.txtftppass = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.grbinternet = new System.Windows.Forms.GroupBox();
            this.grbftp = new System.Windows.Forms.GroupBox();
            this.txtdispatchable = new System.Windows.Forms.TextBox();
            this.rbFTPPath005 = new System.Windows.Forms.RadioButton();
            this.rbHardPath005 = new System.Windows.Forms.RadioButton();
            this.rbFTPPathdispatch = new System.Windows.Forms.RadioButton();
            this.rbHardPathDispatch = new System.Windows.Forms.RadioButton();
            this.rbFTPPath009 = new System.Windows.Forms.RadioButton();
            this.rbHardPath009 = new System.Windows.Forms.RadioButton();
            this.rbFTPPath0091 = new System.Windows.Forms.RadioButton();
            this.rbHardPath0091 = new System.Windows.Forms.RadioButton();
            this.rbFTPPathcounter = new System.Windows.Forms.RadioButton();
            this.rbHardPathcounter = new System.Windows.Forms.RadioButton();
            this.rbFTPPathavg = new System.Windows.Forms.RadioButton();
            this.rbHardPathavg = new System.Windows.Forms.RadioButton();
            this.rbFTPPathload = new System.Windows.Forms.RadioButton();
            this.rbHardPathload = new System.Windows.Forms.RadioButton();
            this.rbFTPPathcapacity = new System.Windows.Forms.RadioButton();
            this.rbHardPathcapacity = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btnformat002 = new System.Windows.Forms.Button();
            this.btnformat005 = new System.Windows.Forms.Button();
            this.btnformatdispatch = new System.Windows.Forms.Button();
            this.btnformatsaled = new System.Windows.Forms.Button();
            this.btnformatcounter = new System.Windows.Forms.Button();
            this.btnformatavgprice = new System.Windows.Forms.Button();
            this.btnformatlfc = new System.Windows.Forms.Button();
            this.btnformatcapacity = new System.Windows.Forms.Button();
            this.btninternet = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnM002 = new System.Windows.Forms.Button();
            this.btnformat005ba = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbHardPath005ba = new System.Windows.Forms.RadioButton();
            this.rbFTPPath005ba = new System.Windows.Forms.RadioButton();
            this.btnm005 = new System.Windows.Forms.Button();
            this.M005Tba = new System.Windows.Forms.TextBox();
            this.grbinternet.SuspendLayout();
            this.grbftp.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // CapacityTb
            // 
            this.CapacityTb.BackColor = System.Drawing.SystemColors.Window;
            this.CapacityTb.Location = new System.Drawing.Point(186, 532);
            this.CapacityTb.Name = "CapacityTb";
            this.CapacityTb.ReadOnly = true;
            this.CapacityTb.Size = new System.Drawing.Size(227, 20);
            this.CapacityTb.TabIndex = 11;
            // 
            // LoadTb
            // 
            this.LoadTb.BackColor = System.Drawing.SystemColors.Window;
            this.LoadTb.Location = new System.Drawing.Point(186, 496);
            this.LoadTb.Name = "LoadTb";
            this.LoadTb.ReadOnly = true;
            this.LoadTb.Size = new System.Drawing.Size(227, 20);
            this.LoadTb.TabIndex = 10;
            // 
            // PriceTb
            // 
            this.PriceTb.BackColor = System.Drawing.SystemColors.Window;
            this.PriceTb.Location = new System.Drawing.Point(186, 460);
            this.PriceTb.Name = "PriceTb";
            this.PriceTb.ReadOnly = true;
            this.PriceTb.Size = new System.Drawing.Size(227, 20);
            this.PriceTb.TabIndex = 9;
            // 
            // M005Tb
            // 
            this.M005Tb.BackColor = System.Drawing.SystemColors.Window;
            this.M005Tb.Location = new System.Drawing.Point(186, 240);
            this.M005Tb.Name = "M005Tb";
            this.M005Tb.ReadOnly = true;
            this.M005Tb.Size = new System.Drawing.Size(227, 20);
            this.M005Tb.TabIndex = 4;
            // 
            // M002Tb
            // 
            this.M002Tb.BackColor = System.Drawing.SystemColors.Window;
            this.M002Tb.Location = new System.Drawing.Point(187, 206);
            this.M002Tb.Name = "M002Tb";
            this.M002Tb.ReadOnly = true;
            this.M002Tb.Size = new System.Drawing.Size(227, 20);
            this.M002Tb.TabIndex = 3;
            this.M002Tb.Tag = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(59, 534);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Market Bill Files :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(44, 499);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 13);
            this.label4.TabIndex = 11;
            this.label4.Tag = "پيش بيني بار روزانه";
            this.label4.Text = "Load Forecasting Files(lfc) :";
            this.toolTip1.SetToolTip(this.label4, "پيش بيني بار روزانه");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label3.Location = new System.Drawing.Point(23, 463);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 13);
            this.label3.TabIndex = 9;
            this.label3.Tag = "قيمت روزانه بازار";
            this.label3.Text = "Average Price Files(chart price) :";
            this.toolTip1.SetToolTip(this.label3, "قيمت روزانه بازار");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(59, 243);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 8;
            this.label2.Tag = "نتايج بازار";
            this.label2.Text = "M005 Files :";
            this.toolTip1.SetToolTip(this.label2, "نتايج بازار");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(60, 209);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 4;
            this.label1.Tag = "پيشنهاد قيمت ";
            this.label1.Text = "M002 Files :";
            this.toolTip1.SetToolTip(this.label1, "پيشنهاد قيمت ");
            // 
            // SaveBtn
            // 
            this.SaveBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.SaveBtn.BackgroundImage = global::PowerPlantProject.Properties.Resources.Savedata;
            this.SaveBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SaveBtn.Location = new System.Drawing.Point(401, 581);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(86, 27);
            this.SaveBtn.TabIndex = 16;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.UseVisualStyleBackColor = false;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(51, 353);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "Temporary/Internet :";
            this.toolTip1.SetToolTip(this.label7, "فايل هاي موقتي و اينترنت");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(51, 391);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 13);
            this.label8.TabIndex = 27;
            this.label8.Tag = "انرژي فروخته شده";
            this.label8.Text = "Saled Energy Files :";
            this.toolTip1.SetToolTip(this.label8, "انرژي فروخته شده");
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(61, 429);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 30;
            this.label9.Tag = "كنتورها";
            this.label9.Text = "Counter Files :";
            this.toolTip1.SetToolTip(this.label9, "كنتورها");
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(60, 315);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 13);
            this.label13.TabIndex = 21;
            this.label13.Tag = "محدوديت توليد";
            this.label13.Text = "Dispatchable :";
            this.toolTip1.SetToolTip(this.label13, "محدوديت توليد");
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(59, 281);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(121, 13);
            this.label14.TabIndex = 67;
            this.label14.Tag = "نتايج بازار";
            this.label14.Text = "M005 With Limmit Files :";
            this.toolTip1.SetToolTip(this.label14, "نتايج بازار");
            // 
            // txtProxy
            // 
            this.txtProxy.BackColor = System.Drawing.SystemColors.Window;
            this.txtProxy.Location = new System.Drawing.Point(179, 33);
            this.txtProxy.Name = "txtProxy";
            this.txtProxy.Size = new System.Drawing.Size(227, 20);
            this.txtProxy.TabIndex = 1;
            this.txtProxy.Tag = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(52, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Server Proxy :";
            // 
            // M009Tb
            // 
            this.M009Tb.BackColor = System.Drawing.SystemColors.Window;
            this.M009Tb.Location = new System.Drawing.Point(186, 350);
            this.M009Tb.Name = "M009Tb";
            this.M009Tb.ReadOnly = true;
            this.M009Tb.Size = new System.Drawing.Size(227, 20);
            this.M009Tb.TabIndex = 6;
            // 
            // M0091Tb
            // 
            this.M0091Tb.BackColor = System.Drawing.SystemColors.Window;
            this.M0091Tb.Location = new System.Drawing.Point(186, 389);
            this.M0091Tb.Name = "M0091Tb";
            this.M0091Tb.ReadOnly = true;
            this.M0091Tb.Size = new System.Drawing.Size(227, 20);
            this.M0091Tb.TabIndex = 7;
            // 
            // CounterTb
            // 
            this.CounterTb.BackColor = System.Drawing.SystemColors.Window;
            this.CounterTb.Location = new System.Drawing.Point(186, 426);
            this.CounterTb.Name = "CounterTb";
            this.CounterTb.ReadOnly = true;
            this.CounterTb.Size = new System.Drawing.Size(227, 20);
            this.CounterTb.TabIndex = 8;
            // 
            // txtInternetUsername
            // 
            this.txtInternetUsername.BackColor = System.Drawing.SystemColors.Window;
            this.txtInternetUsername.Location = new System.Drawing.Point(179, 64);
            this.txtInternetUsername.Name = "txtInternetUsername";
            this.txtInternetUsername.PasswordChar = '*';
            this.txtInternetUsername.Size = new System.Drawing.Size(227, 20);
            this.txtInternetUsername.TabIndex = 2;
            this.txtInternetUsername.Tag = "";
            // 
            // lblInternetPassword
            // 
            this.lblInternetPassword.AutoSize = true;
            this.lblInternetPassword.BackColor = System.Drawing.Color.Transparent;
            this.lblInternetPassword.Location = new System.Drawing.Point(52, 93);
            this.lblInternetPassword.Name = "lblInternetPassword";
            this.lblInternetPassword.Size = new System.Drawing.Size(59, 13);
            this.lblInternetPassword.TabIndex = 33;
            this.lblInternetPassword.Text = "Password :";
            // 
            // txtInternetPassword
            // 
            this.txtInternetPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txtInternetPassword.Location = new System.Drawing.Point(179, 93);
            this.txtInternetPassword.Name = "txtInternetPassword";
            this.txtInternetPassword.PasswordChar = '*';
            this.txtInternetPassword.Size = new System.Drawing.Size(227, 20);
            this.txtInternetPassword.TabIndex = 3;
            this.txtInternetPassword.Tag = "";
            // 
            // lblInternetUserName
            // 
            this.lblInternetUserName.AutoSize = true;
            this.lblInternetUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblInternetUserName.Location = new System.Drawing.Point(53, 64);
            this.lblInternetUserName.Name = "lblInternetUserName";
            this.lblInternetUserName.Size = new System.Drawing.Size(61, 13);
            this.lblInternetUserName.TabIndex = 35;
            this.lblInternetUserName.Text = "Username :";
            // 
            // txtDomain
            // 
            this.txtDomain.BackColor = System.Drawing.SystemColors.Window;
            this.txtDomain.Location = new System.Drawing.Point(179, 120);
            this.txtDomain.Name = "txtDomain";
            this.txtDomain.Size = new System.Drawing.Size(227, 20);
            this.txtDomain.TabIndex = 4;
            this.txtDomain.Tag = "";
            // 
            // lblDomain
            // 
            this.lblDomain.AutoSize = true;
            this.lblDomain.BackColor = System.Drawing.Color.Transparent;
            this.lblDomain.Location = new System.Drawing.Point(52, 121);
            this.lblDomain.Name = "lblDomain";
            this.lblDomain.Size = new System.Drawing.Size(49, 13);
            this.lblDomain.TabIndex = 37;
            this.lblDomain.Text = "Domain :";
            // 
            // rbFTPPath002
            // 
            this.rbFTPPath002.AutoSize = true;
            this.rbFTPPath002.BackColor = System.Drawing.Color.Transparent;
            this.rbFTPPath002.Location = new System.Drawing.Point(132, 6);
            this.rbFTPPath002.Name = "rbFTPPath002";
            this.rbFTPPath002.Size = new System.Drawing.Size(70, 17);
            this.rbFTPPath002.TabIndex = 1;
            this.rbFTPPath002.Text = "FTP Path";
            this.rbFTPPath002.UseVisualStyleBackColor = false;
            this.rbFTPPath002.CheckedChanged += new System.EventHandler(this.rbFTPPath002_CheckedChanged);
            // 
            // rbHardPath002
            // 
            this.rbHardPath002.AutoSize = true;
            this.rbHardPath002.BackColor = System.Drawing.Color.Transparent;
            this.rbHardPath002.Location = new System.Drawing.Point(10, 5);
            this.rbHardPath002.Name = "rbHardPath002";
            this.rbHardPath002.Size = new System.Drawing.Size(73, 17);
            this.rbHardPath002.TabIndex = 0;
            this.rbHardPath002.Text = "Hard Path";
            this.rbHardPath002.UseVisualStyleBackColor = false;
            this.rbHardPath002.CheckedChanged += new System.EventHandler(this.rbHardPath002_CheckedChanged);
            // 
            // txtftpip
            // 
            this.txtftpip.Location = new System.Drawing.Point(141, 34);
            this.txtftpip.Name = "txtftpip";
            this.txtftpip.Size = new System.Drawing.Size(227, 20);
            this.txtftpip.TabIndex = 0;
            // 
            // txtftpuser
            // 
            this.txtftpuser.Location = new System.Drawing.Point(141, 62);
            this.txtftpuser.Name = "txtftpuser";
            this.txtftpuser.PasswordChar = '*';
            this.txtftpuser.Size = new System.Drawing.Size(227, 20);
            this.txtftpuser.TabIndex = 1;
            // 
            // txtftppass
            // 
            this.txtftppass.Location = new System.Drawing.Point(141, 94);
            this.txtftppass.Name = "txtftppass";
            this.txtftppass.PasswordChar = '*';
            this.txtftppass.Size = new System.Drawing.Size(227, 20);
            this.txtftppass.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(55, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 13);
            this.label10.TabIndex = 43;
            this.label10.Text = "FtpServerIP:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(55, 69);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 13);
            this.label11.TabIndex = 43;
            this.label11.Text = "FtpUserName:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(56, 98);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 13);
            this.label12.TabIndex = 43;
            this.label12.Text = "FtpPassword:";
            // 
            // grbinternet
            // 
            this.grbinternet.BackColor = System.Drawing.Color.Transparent;
            this.grbinternet.Controls.Add(this.label6);
            this.grbinternet.Controls.Add(this.txtProxy);
            this.grbinternet.Controls.Add(this.lblInternetPassword);
            this.grbinternet.Controls.Add(this.txtInternetUsername);
            this.grbinternet.Controls.Add(this.lblInternetUserName);
            this.grbinternet.Controls.Add(this.txtInternetPassword);
            this.grbinternet.Controls.Add(this.lblDomain);
            this.grbinternet.Controls.Add(this.txtDomain);
            this.grbinternet.Location = new System.Drawing.Point(24, 31);
            this.grbinternet.Name = "grbinternet";
            this.grbinternet.Size = new System.Drawing.Size(426, 153);
            this.grbinternet.TabIndex = 1;
            this.grbinternet.TabStop = false;
            this.grbinternet.Text = "Internet Setting";
            // 
            // grbftp
            // 
            this.grbftp.BackColor = System.Drawing.Color.Transparent;
            this.grbftp.Controls.Add(this.label10);
            this.grbftp.Controls.Add(this.txtftpip);
            this.grbftp.Controls.Add(this.label12);
            this.grbftp.Controls.Add(this.txtftpuser);
            this.grbftp.Controls.Add(this.label11);
            this.grbftp.Controls.Add(this.txtftppass);
            this.grbftp.Location = new System.Drawing.Point(465, 31);
            this.grbftp.Name = "grbftp";
            this.grbftp.Size = new System.Drawing.Size(404, 153);
            this.grbftp.TabIndex = 2;
            this.grbftp.TabStop = false;
            this.grbftp.Text = "Ftp Setting";
            // 
            // txtdispatchable
            // 
            this.txtdispatchable.BackColor = System.Drawing.SystemColors.Window;
            this.txtdispatchable.Location = new System.Drawing.Point(187, 315);
            this.txtdispatchable.Name = "txtdispatchable";
            this.txtdispatchable.ReadOnly = true;
            this.txtdispatchable.Size = new System.Drawing.Size(227, 20);
            this.txtdispatchable.TabIndex = 5;
            // 
            // rbFTPPath005
            // 
            this.rbFTPPath005.AutoSize = true;
            this.rbFTPPath005.BackColor = System.Drawing.Color.Transparent;
            this.rbFTPPath005.Location = new System.Drawing.Point(132, 3);
            this.rbFTPPath005.Name = "rbFTPPath005";
            this.rbFTPPath005.Size = new System.Drawing.Size(70, 17);
            this.rbFTPPath005.TabIndex = 47;
            this.rbFTPPath005.Text = "FTP Path";
            this.rbFTPPath005.UseVisualStyleBackColor = false;
            this.rbFTPPath005.CheckedChanged += new System.EventHandler(this.rbFTPPath005_CheckedChanged);
            // 
            // rbHardPath005
            // 
            this.rbHardPath005.AutoSize = true;
            this.rbHardPath005.BackColor = System.Drawing.Color.Transparent;
            this.rbHardPath005.Location = new System.Drawing.Point(9, 3);
            this.rbHardPath005.Name = "rbHardPath005";
            this.rbHardPath005.Size = new System.Drawing.Size(73, 17);
            this.rbHardPath005.TabIndex = 46;
            this.rbHardPath005.Text = "Hard Path";
            this.rbHardPath005.UseVisualStyleBackColor = false;
            this.rbHardPath005.CheckedChanged += new System.EventHandler(this.rbHardPath005_CheckedChanged);
            // 
            // rbFTPPathdispatch
            // 
            this.rbFTPPathdispatch.AutoSize = true;
            this.rbFTPPathdispatch.BackColor = System.Drawing.Color.Transparent;
            this.rbFTPPathdispatch.Location = new System.Drawing.Point(133, 5);
            this.rbFTPPathdispatch.Name = "rbFTPPathdispatch";
            this.rbFTPPathdispatch.Size = new System.Drawing.Size(70, 17);
            this.rbFTPPathdispatch.TabIndex = 49;
            this.rbFTPPathdispatch.Text = "FTP Path";
            this.rbFTPPathdispatch.UseVisualStyleBackColor = false;
            this.rbFTPPathdispatch.CheckedChanged += new System.EventHandler(this.rbFTPPathdispatch_CheckedChanged);
            // 
            // rbHardPathDispatch
            // 
            this.rbHardPathDispatch.AutoSize = true;
            this.rbHardPathDispatch.BackColor = System.Drawing.Color.Transparent;
            this.rbHardPathDispatch.Location = new System.Drawing.Point(10, 4);
            this.rbHardPathDispatch.Name = "rbHardPathDispatch";
            this.rbHardPathDispatch.Size = new System.Drawing.Size(73, 17);
            this.rbHardPathDispatch.TabIndex = 48;
            this.rbHardPathDispatch.Text = "Hard Path";
            this.rbHardPathDispatch.UseVisualStyleBackColor = false;
            this.rbHardPathDispatch.CheckedChanged += new System.EventHandler(this.rbHardPathDispatch_CheckedChanged);
            // 
            // rbFTPPath009
            // 
            this.rbFTPPath009.AutoSize = true;
            this.rbFTPPath009.BackColor = System.Drawing.Color.Transparent;
            this.rbFTPPath009.Location = new System.Drawing.Point(131, 3);
            this.rbFTPPath009.Name = "rbFTPPath009";
            this.rbFTPPath009.Size = new System.Drawing.Size(70, 17);
            this.rbFTPPath009.TabIndex = 51;
            this.rbFTPPath009.Text = "Link Path";
            this.rbFTPPath009.UseVisualStyleBackColor = false;
            this.rbFTPPath009.CheckedChanged += new System.EventHandler(this.rbFTPPath009_CheckedChanged);
            // 
            // rbHardPath009
            // 
            this.rbHardPath009.AutoSize = true;
            this.rbHardPath009.BackColor = System.Drawing.Color.Transparent;
            this.rbHardPath009.Location = new System.Drawing.Point(9, 3);
            this.rbHardPath009.Name = "rbHardPath009";
            this.rbHardPath009.Size = new System.Drawing.Size(73, 17);
            this.rbHardPath009.TabIndex = 50;
            this.rbHardPath009.Text = "Hard Path";
            this.rbHardPath009.UseVisualStyleBackColor = false;
            this.rbHardPath009.CheckedChanged += new System.EventHandler(this.rbHardPath009_CheckedChanged);
            // 
            // rbFTPPath0091
            // 
            this.rbFTPPath0091.AutoSize = true;
            this.rbFTPPath0091.BackColor = System.Drawing.Color.Transparent;
            this.rbFTPPath0091.Location = new System.Drawing.Point(131, 4);
            this.rbFTPPath0091.Name = "rbFTPPath0091";
            this.rbFTPPath0091.Size = new System.Drawing.Size(70, 17);
            this.rbFTPPath0091.TabIndex = 53;
            this.rbFTPPath0091.Text = "FTP Path";
            this.rbFTPPath0091.UseVisualStyleBackColor = false;
            this.rbFTPPath0091.Visible = false;
            this.rbFTPPath0091.CheckedChanged += new System.EventHandler(this.rbFTPPath0091_CheckedChanged);
            // 
            // rbHardPath0091
            // 
            this.rbHardPath0091.AutoSize = true;
            this.rbHardPath0091.BackColor = System.Drawing.Color.Transparent;
            this.rbHardPath0091.Location = new System.Drawing.Point(10, 4);
            this.rbHardPath0091.Name = "rbHardPath0091";
            this.rbHardPath0091.Size = new System.Drawing.Size(73, 17);
            this.rbHardPath0091.TabIndex = 52;
            this.rbHardPath0091.Text = "Hard Path";
            this.rbHardPath0091.UseVisualStyleBackColor = false;
            this.rbHardPath0091.CheckedChanged += new System.EventHandler(this.rbHardPath0091_CheckedChanged);
            // 
            // rbFTPPathcounter
            // 
            this.rbFTPPathcounter.AutoSize = true;
            this.rbFTPPathcounter.BackColor = System.Drawing.Color.Transparent;
            this.rbFTPPathcounter.Location = new System.Drawing.Point(130, 3);
            this.rbFTPPathcounter.Name = "rbFTPPathcounter";
            this.rbFTPPathcounter.Size = new System.Drawing.Size(70, 17);
            this.rbFTPPathcounter.TabIndex = 55;
            this.rbFTPPathcounter.Text = "FTP Path";
            this.rbFTPPathcounter.UseVisualStyleBackColor = false;
            this.rbFTPPathcounter.CheckedChanged += new System.EventHandler(this.rbFTPPathcounter_CheckedChanged);
            // 
            // rbHardPathcounter
            // 
            this.rbHardPathcounter.AutoSize = true;
            this.rbHardPathcounter.BackColor = System.Drawing.Color.Transparent;
            this.rbHardPathcounter.Location = new System.Drawing.Point(9, 4);
            this.rbHardPathcounter.Name = "rbHardPathcounter";
            this.rbHardPathcounter.Size = new System.Drawing.Size(73, 17);
            this.rbHardPathcounter.TabIndex = 54;
            this.rbHardPathcounter.Text = "Hard Path";
            this.rbHardPathcounter.UseVisualStyleBackColor = false;
            this.rbHardPathcounter.CheckedChanged += new System.EventHandler(this.rbHardPathcounter_CheckedChanged);
            // 
            // rbFTPPathavg
            // 
            this.rbFTPPathavg.AutoSize = true;
            this.rbFTPPathavg.BackColor = System.Drawing.Color.Transparent;
            this.rbFTPPathavg.Location = new System.Drawing.Point(130, 3);
            this.rbFTPPathavg.Name = "rbFTPPathavg";
            this.rbFTPPathavg.Size = new System.Drawing.Size(70, 17);
            this.rbFTPPathavg.TabIndex = 57;
            this.rbFTPPathavg.Text = "FTP Path";
            this.rbFTPPathavg.UseVisualStyleBackColor = false;
            this.rbFTPPathavg.CheckedChanged += new System.EventHandler(this.rbFTPPathavg_CheckedChanged);
            // 
            // rbHardPathavg
            // 
            this.rbHardPathavg.AutoSize = true;
            this.rbHardPathavg.BackColor = System.Drawing.Color.Transparent;
            this.rbHardPathavg.Location = new System.Drawing.Point(9, 3);
            this.rbHardPathavg.Name = "rbHardPathavg";
            this.rbHardPathavg.Size = new System.Drawing.Size(73, 17);
            this.rbHardPathavg.TabIndex = 56;
            this.rbHardPathavg.Text = "Hard Path";
            this.rbHardPathavg.UseVisualStyleBackColor = false;
            this.rbHardPathavg.CheckedChanged += new System.EventHandler(this.rbHardPathavg_CheckedChanged);
            // 
            // rbFTPPathload
            // 
            this.rbFTPPathload.AutoSize = true;
            this.rbFTPPathload.BackColor = System.Drawing.Color.Transparent;
            this.rbFTPPathload.Location = new System.Drawing.Point(130, 4);
            this.rbFTPPathload.Name = "rbFTPPathload";
            this.rbFTPPathload.Size = new System.Drawing.Size(70, 17);
            this.rbFTPPathload.TabIndex = 59;
            this.rbFTPPathload.Text = "FTP Path";
            this.rbFTPPathload.UseVisualStyleBackColor = false;
            this.rbFTPPathload.CheckedChanged += new System.EventHandler(this.rbFTPPathload_CheckedChanged);
            // 
            // rbHardPathload
            // 
            this.rbHardPathload.AutoSize = true;
            this.rbHardPathload.BackColor = System.Drawing.Color.Transparent;
            this.rbHardPathload.Location = new System.Drawing.Point(10, 3);
            this.rbHardPathload.Name = "rbHardPathload";
            this.rbHardPathload.Size = new System.Drawing.Size(73, 17);
            this.rbHardPathload.TabIndex = 58;
            this.rbHardPathload.Text = "Hard Path";
            this.rbHardPathload.UseVisualStyleBackColor = false;
            this.rbHardPathload.CheckedChanged += new System.EventHandler(this.rbHardPathload_CheckedChanged);
            // 
            // rbFTPPathcapacity
            // 
            this.rbFTPPathcapacity.AutoSize = true;
            this.rbFTPPathcapacity.BackColor = System.Drawing.Color.Transparent;
            this.rbFTPPathcapacity.Location = new System.Drawing.Point(130, 6);
            this.rbFTPPathcapacity.Name = "rbFTPPathcapacity";
            this.rbFTPPathcapacity.Size = new System.Drawing.Size(70, 17);
            this.rbFTPPathcapacity.TabIndex = 61;
            this.rbFTPPathcapacity.Text = "FTP Path";
            this.rbFTPPathcapacity.UseVisualStyleBackColor = false;
            this.rbFTPPathcapacity.CheckedChanged += new System.EventHandler(this.rbFTPPathcapacity_CheckedChanged);
            // 
            // rbHardPathcapacity
            // 
            this.rbHardPathcapacity.AutoSize = true;
            this.rbHardPathcapacity.BackColor = System.Drawing.Color.Transparent;
            this.rbHardPathcapacity.Location = new System.Drawing.Point(10, 6);
            this.rbHardPathcapacity.Name = "rbHardPathcapacity";
            this.rbHardPathcapacity.Size = new System.Drawing.Size(73, 17);
            this.rbHardPathcapacity.TabIndex = 1;
            this.rbHardPathcapacity.Text = "Hard Path";
            this.rbHardPathcapacity.UseVisualStyleBackColor = false;
            this.rbHardPathcapacity.CheckedChanged += new System.EventHandler(this.rbHardPathcapacity_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.CadetBlue;
            this.panel1.Controls.Add(this.rbHardPath002);
            this.panel1.Controls.Add(this.rbFTPPath002);
            this.panel1.Location = new System.Drawing.Point(507, 201);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(218, 26);
            this.panel1.TabIndex = 62;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.CadetBlue;
            this.panel3.Controls.Add(this.rbHardPath005);
            this.panel3.Controls.Add(this.rbFTPPath005);
            this.panel3.Location = new System.Drawing.Point(507, 236);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(218, 26);
            this.panel3.TabIndex = 63;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.CadetBlue;
            this.panel4.Controls.Add(this.rbFTPPathdispatch);
            this.panel4.Controls.Add(this.rbHardPathDispatch);
            this.panel4.Location = new System.Drawing.Point(506, 312);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(219, 26);
            this.panel4.TabIndex = 63;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.SkyBlue;
            this.panel5.Controls.Add(this.radioButton1);
            this.panel5.Controls.Add(this.radioButton2);
            this.panel5.Location = new System.Drawing.Point(1014, 243);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(191, 26);
            this.panel5.TabIndex = 63;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.BackColor = System.Drawing.Color.Transparent;
            this.radioButton1.Location = new System.Drawing.Point(9, 3);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(73, 17);
            this.radioButton1.TabIndex = 46;
            this.radioButton1.Text = "Hard Path";
            this.radioButton1.UseVisualStyleBackColor = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.BackColor = System.Drawing.Color.Transparent;
            this.radioButton2.Location = new System.Drawing.Point(108, 3);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(70, 17);
            this.radioButton2.TabIndex = 47;
            this.radioButton2.Text = "FTP Path";
            this.radioButton2.UseVisualStyleBackColor = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.CadetBlue;
            this.panel6.Controls.Add(this.rbHardPath009);
            this.panel6.Controls.Add(this.rbFTPPath009);
            this.panel6.Location = new System.Drawing.Point(508, 347);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(217, 26);
            this.panel6.TabIndex = 63;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.CadetBlue;
            this.panel7.Controls.Add(this.rbHardPath0091);
            this.panel7.Controls.Add(this.rbFTPPath0091);
            this.panel7.Location = new System.Drawing.Point(508, 383);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(217, 26);
            this.panel7.TabIndex = 63;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.CadetBlue;
            this.panel8.Controls.Add(this.rbHardPathcounter);
            this.panel8.Controls.Add(this.rbFTPPathcounter);
            this.panel8.Location = new System.Drawing.Point(509, 420);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(216, 26);
            this.panel8.TabIndex = 63;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.SkyBlue;
            this.panel9.Controls.Add(this.radioButton3);
            this.panel9.Controls.Add(this.radioButton4);
            this.panel9.Location = new System.Drawing.Point(985, 326);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(191, 26);
            this.panel9.TabIndex = 63;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.BackColor = System.Drawing.Color.Transparent;
            this.radioButton3.Location = new System.Drawing.Point(18, 4);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(73, 17);
            this.radioButton3.TabIndex = 52;
            this.radioButton3.Text = "Hard Path";
            this.radioButton3.UseVisualStyleBackColor = false;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.BackColor = System.Drawing.Color.Transparent;
            this.radioButton4.Location = new System.Drawing.Point(116, 4);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(70, 17);
            this.radioButton4.TabIndex = 53;
            this.radioButton4.Text = "FTP Path";
            this.radioButton4.UseVisualStyleBackColor = false;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.CadetBlue;
            this.panel10.Controls.Add(this.rbHardPathavg);
            this.panel10.Controls.Add(this.rbFTPPathavg);
            this.panel10.Location = new System.Drawing.Point(509, 456);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(216, 26);
            this.panel10.TabIndex = 63;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.CadetBlue;
            this.panel11.Controls.Add(this.rbHardPathload);
            this.panel11.Controls.Add(this.rbFTPPathload);
            this.panel11.Location = new System.Drawing.Point(509, 493);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(216, 26);
            this.panel11.TabIndex = 63;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.CadetBlue;
            this.panel12.Controls.Add(this.rbHardPathcapacity);
            this.panel12.Controls.Add(this.rbFTPPathcapacity);
            this.panel12.Location = new System.Drawing.Point(509, 529);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(216, 26);
            this.panel12.TabIndex = 63;
            // 
            // btnformat002
            // 
            this.btnformat002.BackColor = System.Drawing.Color.CadetBlue;
            this.btnformat002.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnformat002.Location = new System.Drawing.Point(774, 203);
            this.btnformat002.Name = "btnformat002";
            this.btnformat002.Size = new System.Drawing.Size(90, 23);
            this.btnformat002.TabIndex = 64;
            this.btnformat002.Text = "NameFormat";
            this.btnformat002.UseVisualStyleBackColor = false;
            this.btnformat002.Click += new System.EventHandler(this.btnformat002_Click);
            // 
            // btnformat005
            // 
            this.btnformat005.BackColor = System.Drawing.Color.CadetBlue;
            this.btnformat005.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnformat005.Location = new System.Drawing.Point(774, 236);
            this.btnformat005.Name = "btnformat005";
            this.btnformat005.Size = new System.Drawing.Size(90, 23);
            this.btnformat005.TabIndex = 64;
            this.btnformat005.Text = "NameFormat";
            this.btnformat005.UseVisualStyleBackColor = false;
            this.btnformat005.Click += new System.EventHandler(this.btnformat005_Click);
            // 
            // btnformatdispatch
            // 
            this.btnformatdispatch.BackColor = System.Drawing.Color.CadetBlue;
            this.btnformatdispatch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnformatdispatch.Location = new System.Drawing.Point(774, 314);
            this.btnformatdispatch.Name = "btnformatdispatch";
            this.btnformatdispatch.Size = new System.Drawing.Size(90, 23);
            this.btnformatdispatch.TabIndex = 64;
            this.btnformatdispatch.Text = "NameFormat";
            this.btnformatdispatch.UseVisualStyleBackColor = false;
            this.btnformatdispatch.Click += new System.EventHandler(this.btnformatdispatch_Click);
            // 
            // btnformatsaled
            // 
            this.btnformatsaled.BackColor = System.Drawing.Color.CadetBlue;
            this.btnformatsaled.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnformatsaled.Location = new System.Drawing.Point(777, 385);
            this.btnformatsaled.Name = "btnformatsaled";
            this.btnformatsaled.Size = new System.Drawing.Size(87, 23);
            this.btnformatsaled.TabIndex = 64;
            this.btnformatsaled.Text = "NameFormat";
            this.btnformatsaled.UseVisualStyleBackColor = false;
            this.btnformatsaled.Click += new System.EventHandler(this.btnformatsaled_Click);
            // 
            // btnformatcounter
            // 
            this.btnformatcounter.BackColor = System.Drawing.Color.CadetBlue;
            this.btnformatcounter.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnformatcounter.Location = new System.Drawing.Point(777, 421);
            this.btnformatcounter.Name = "btnformatcounter";
            this.btnformatcounter.Size = new System.Drawing.Size(87, 23);
            this.btnformatcounter.TabIndex = 64;
            this.btnformatcounter.Text = "NameFormat";
            this.btnformatcounter.UseVisualStyleBackColor = false;
            this.btnformatcounter.Click += new System.EventHandler(this.btnformatcounter_Click);
            // 
            // btnformatavgprice
            // 
            this.btnformatavgprice.BackColor = System.Drawing.Color.CadetBlue;
            this.btnformatavgprice.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnformatavgprice.Location = new System.Drawing.Point(777, 456);
            this.btnformatavgprice.Name = "btnformatavgprice";
            this.btnformatavgprice.Size = new System.Drawing.Size(87, 23);
            this.btnformatavgprice.TabIndex = 64;
            this.btnformatavgprice.Text = "NameFormat";
            this.btnformatavgprice.UseVisualStyleBackColor = false;
            this.btnformatavgprice.Click += new System.EventHandler(this.btnformatavgprice_Click);
            // 
            // btnformatlfc
            // 
            this.btnformatlfc.BackColor = System.Drawing.Color.CadetBlue;
            this.btnformatlfc.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnformatlfc.Location = new System.Drawing.Point(777, 494);
            this.btnformatlfc.Name = "btnformatlfc";
            this.btnformatlfc.Size = new System.Drawing.Size(87, 23);
            this.btnformatlfc.TabIndex = 64;
            this.btnformatlfc.Text = "NameFormat";
            this.btnformatlfc.UseVisualStyleBackColor = false;
            this.btnformatlfc.Click += new System.EventHandler(this.btnformatlfc_Click);
            // 
            // btnformatcapacity
            // 
            this.btnformatcapacity.BackColor = System.Drawing.Color.CadetBlue;
            this.btnformatcapacity.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnformatcapacity.Location = new System.Drawing.Point(777, 531);
            this.btnformatcapacity.Name = "btnformatcapacity";
            this.btnformatcapacity.Size = new System.Drawing.Size(87, 23);
            this.btnformatcapacity.TabIndex = 64;
            this.btnformatcapacity.Text = "NameFormat";
            this.btnformatcapacity.UseVisualStyleBackColor = false;
            this.btnformatcapacity.Click += new System.EventHandler(this.btnformatcapacity_Click);
            // 
            // btninternet
            // 
            this.btninternet.BackColor = System.Drawing.Color.CadetBlue;
            this.btninternet.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btninternet.Location = new System.Drawing.Point(776, 348);
            this.btninternet.Name = "btninternet";
            this.btninternet.Size = new System.Drawing.Size(87, 23);
            this.btninternet.TabIndex = 65;
            this.btninternet.Text = "InternetAddress";
            this.btninternet.UseVisualStyleBackColor = false;
            this.btninternet.Click += new System.EventHandler(this.btninternet_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button8.Image = global::PowerPlantProject.Properties.Resources.open;
            this.button8.Location = new System.Drawing.Point(420, 313);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(33, 23);
            this.button8.TabIndex = 20;
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button7.Image = global::PowerPlantProject.Properties.Resources.open;
            this.button7.Location = new System.Drawing.Point(420, 423);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(33, 23);
            this.button7.TabIndex = 23;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button6.Image = global::PowerPlantProject.Properties.Resources.open;
            this.button6.Location = new System.Drawing.Point(420, 386);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(33, 23);
            this.button6.TabIndex = 22;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Image = global::PowerPlantProject.Properties.Resources.open;
            this.button1.Location = new System.Drawing.Point(420, 347);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 23);
            this.button1.TabIndex = 21;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Image = global::PowerPlantProject.Properties.Resources.open;
            this.button5.Location = new System.Drawing.Point(420, 529);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(33, 23);
            this.button5.TabIndex = 26;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Image = global::PowerPlantProject.Properties.Resources.open;
            this.button4.Location = new System.Drawing.Point(420, 493);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(33, 23);
            this.button4.TabIndex = 25;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Image = global::PowerPlantProject.Properties.Resources.open;
            this.button3.Location = new System.Drawing.Point(420, 456);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(33, 23);
            this.button3.TabIndex = 24;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Image = global::PowerPlantProject.Properties.Resources.open;
            this.button2.Location = new System.Drawing.Point(419, 237);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(33, 23);
            this.button2.TabIndex = 18;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnM002
            // 
            this.btnM002.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnM002.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnM002.Image = global::PowerPlantProject.Properties.Resources.open;
            this.btnM002.Location = new System.Drawing.Point(419, 204);
            this.btnM002.Name = "btnM002";
            this.btnM002.Size = new System.Drawing.Size(33, 23);
            this.btnM002.TabIndex = 17;
            this.btnM002.UseVisualStyleBackColor = false;
            this.btnM002.Click += new System.EventHandler(this.btnM002_Click);
            // 
            // btnformat005ba
            // 
            this.btnformat005ba.BackColor = System.Drawing.Color.CadetBlue;
            this.btnformat005ba.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnformat005ba.Location = new System.Drawing.Point(774, 274);
            this.btnformat005ba.Name = "btnformat005ba";
            this.btnformat005ba.Size = new System.Drawing.Size(90, 23);
            this.btnformat005ba.TabIndex = 70;
            this.btnformat005ba.Text = "NameFormat";
            this.btnformat005ba.UseVisualStyleBackColor = false;
            this.btnformat005ba.Click += new System.EventHandler(this.btnformat005ba_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.CadetBlue;
            this.panel2.Controls.Add(this.rbHardPath005ba);
            this.panel2.Controls.Add(this.rbFTPPath005ba);
            this.panel2.Location = new System.Drawing.Point(507, 274);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(218, 26);
            this.panel2.TabIndex = 69;
            // 
            // rbHardPath005ba
            // 
            this.rbHardPath005ba.AutoSize = true;
            this.rbHardPath005ba.BackColor = System.Drawing.Color.Transparent;
            this.rbHardPath005ba.Location = new System.Drawing.Point(9, 3);
            this.rbHardPath005ba.Name = "rbHardPath005ba";
            this.rbHardPath005ba.Size = new System.Drawing.Size(73, 17);
            this.rbHardPath005ba.TabIndex = 46;
            this.rbHardPath005ba.Text = "Hard Path";
            this.rbHardPath005ba.UseVisualStyleBackColor = false;
            this.rbHardPath005ba.CheckedChanged += new System.EventHandler(this.rbHardPath005ba_CheckedChanged);
            // 
            // rbFTPPath005ba
            // 
            this.rbFTPPath005ba.AutoSize = true;
            this.rbFTPPath005ba.BackColor = System.Drawing.Color.Transparent;
            this.rbFTPPath005ba.Location = new System.Drawing.Point(132, 3);
            this.rbFTPPath005ba.Name = "rbFTPPath005ba";
            this.rbFTPPath005ba.Size = new System.Drawing.Size(70, 17);
            this.rbFTPPath005ba.TabIndex = 47;
            this.rbFTPPath005ba.Text = "FTP Path";
            this.rbFTPPath005ba.UseVisualStyleBackColor = false;
            this.rbFTPPath005ba.CheckedChanged += new System.EventHandler(this.rbFTPPath005ba_CheckedChanged);
            // 
            // btnm005
            // 
            this.btnm005.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnm005.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnm005.Image = global::PowerPlantProject.Properties.Resources.open;
            this.btnm005.Location = new System.Drawing.Point(419, 275);
            this.btnm005.Name = "btnm005";
            this.btnm005.Size = new System.Drawing.Size(33, 23);
            this.btnm005.TabIndex = 19;
            this.btnm005.UseVisualStyleBackColor = false;
            this.btnm005.Click += new System.EventHandler(this.btnm005_Click);
            // 
            // M005Tba
            // 
            this.M005Tba.BackColor = System.Drawing.SystemColors.Window;
            this.M005Tba.Location = new System.Drawing.Point(186, 278);
            this.M005Tba.Name = "M005Tba";
            this.M005Tba.ReadOnly = true;
            this.M005Tba.Size = new System.Drawing.Size(227, 20);
            this.M005Tba.TabIndex = 66;
            this.M005Tba.MouseClick += new System.Windows.Forms.MouseEventHandler(this.M005Tba_MouseClick);
            // 
            // PathForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(892, 629);
            this.Controls.Add(this.btnformat005ba);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnm005);
            this.Controls.Add(this.M005Tba);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.btninternet);
            this.Controls.Add(this.btnformatcapacity);
            this.Controls.Add(this.btnformatlfc);
            this.Controls.Add(this.btnformatavgprice);
            this.Controls.Add(this.btnformatcounter);
            this.Controls.Add(this.btnformatsaled);
            this.Controls.Add(this.btnformatdispatch);
            this.Controls.Add(this.btnformat005);
            this.Controls.Add(this.btnformat002);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtdispatchable);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.grbftp);
            this.Controls.Add(this.grbinternet);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.CounterTb);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.M0091Tb);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.M009Tb);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnM002);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.CapacityTb);
            this.Controls.Add(this.LoadTb);
            this.Controls.Add(this.PriceTb);
            this.Controls.Add(this.M005Tb);
            this.Controls.Add(this.M002Tb);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PathForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Path";
            this.Load += new System.EventHandler(this.PathForm_Load);
            this.grbinternet.ResumeLayout(false);
            this.grbinternet.PerformLayout();
            this.grbftp.ResumeLayout(false);
            this.grbftp.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox CapacityTb;
        private System.Windows.Forms.TextBox LoadTb;
        private System.Windows.Forms.TextBox PriceTb;
        private System.Windows.Forms.TextBox M005Tb;
        private System.Windows.Forms.TextBox M002Tb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button btnM002;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txtProxy;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox M009Tb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox M0091Tb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.TextBox CounterTb;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtInternetUsername;
        private System.Windows.Forms.Label lblInternetPassword;
        private System.Windows.Forms.TextBox txtInternetPassword;
        private System.Windows.Forms.Label lblInternetUserName;
        private System.Windows.Forms.TextBox txtDomain;
        private System.Windows.Forms.Label lblDomain;
        private System.Windows.Forms.RadioButton rbFTPPath002;
        private System.Windows.Forms.RadioButton rbHardPath002;
        private System.Windows.Forms.TextBox txtftpip;
        private System.Windows.Forms.TextBox txtftpuser;
        private System.Windows.Forms.TextBox txtftppass;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox grbinternet;
        private System.Windows.Forms.GroupBox grbftp;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox txtdispatchable;
        private System.Windows.Forms.RadioButton rbFTPPath005;
        private System.Windows.Forms.RadioButton rbHardPath005;
        private System.Windows.Forms.RadioButton rbFTPPathdispatch;
        private System.Windows.Forms.RadioButton rbHardPathDispatch;
        private System.Windows.Forms.RadioButton rbFTPPath009;
        private System.Windows.Forms.RadioButton rbHardPath009;
        private System.Windows.Forms.RadioButton rbFTPPath0091;
        private System.Windows.Forms.RadioButton rbHardPath0091;
        private System.Windows.Forms.RadioButton rbFTPPathcounter;
        private System.Windows.Forms.RadioButton rbHardPathcounter;
        private System.Windows.Forms.RadioButton rbFTPPathavg;
        private System.Windows.Forms.RadioButton rbHardPathavg;
        private System.Windows.Forms.RadioButton rbFTPPathload;
        private System.Windows.Forms.RadioButton rbHardPathload;
        private System.Windows.Forms.RadioButton rbFTPPathcapacity;
        private System.Windows.Forms.RadioButton rbHardPathcapacity;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button btnformat002;
        private System.Windows.Forms.Button btnformat005;
        private System.Windows.Forms.Button btnformatdispatch;
        private System.Windows.Forms.Button btnformatsaled;
        private System.Windows.Forms.Button btnformatcounter;
        private System.Windows.Forms.Button btnformatavgprice;
        private System.Windows.Forms.Button btnformatlfc;
        private System.Windows.Forms.Button btnformatcapacity;
        private System.Windows.Forms.Button btninternet;
        private System.Windows.Forms.Button btnformat005ba;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rbHardPath005ba;
        private System.Windows.Forms.RadioButton rbFTPPath005ba;
        private System.Windows.Forms.Button btnm005;
        private System.Windows.Forms.TextBox M005Tba;
        private System.Windows.Forms.Label label14;
    }
}