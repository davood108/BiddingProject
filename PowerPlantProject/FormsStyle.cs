﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using NRI.SBS.Common;
namespace PowerPlantProject
{
    public partial class FormsStyle : Form
    {
        public Color resultformback = FormColors.GetColor().Formbackcolor;
        public Color resultbuttonback = FormColors.GetColor().Buttonbackcolor;
        public Color resultpanelback = FormColors.GetColor().Panelbackcolor;
        public Color resulttextback = FormColors.GetColor().Textbackcolor;
        public Color resultgridback = FormColors.GetColor().Gridbackcolor;
        bool b1click=false;
        bool b2click=false;
        bool b5click = false;
        bool b3click = false;
        bool b4click = false;

        public FormsStyle( )
        {
           
            InitializeComponent();
            try
            {
               

                ////////////////////////buttons////////////////////////
                

                button1.BackColor = FormColors.GetColor().Formbackcolor;
                button2.BackColor = FormColors.GetColor().Buttonbackcolor;
                button4.BackColor = FormColors.GetColor().Textbackcolor;
                button6.BackColor = FormColors.GetColor().Panelbackcolor;
                button3.BackColor = FormColors.GetColor().Buttonbackcolor;
                button5.BackColor = FormColors.GetColor().Gridbackcolor;
                groupBox1.BackColor = button1.BackColor;

                //foreach (Control c in this.Controls)
                //{
                //    if (c is Button)
                //        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                //}
                //////////////////////////////panel,groupbox/////////////////////////////;
                //foreach (Control c in this.Controls)
                //{
                //    if (c is Panel || c is GroupBox)
                //        c.BackColor = FormColors.GetColor().Panelbackcolor;

                //}
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void FormsStyle_Load(object sender, EventArgs e)
        {
           
           
        }
      
        private void button1_Click(object sender, EventArgs e)
        {
            b1click = true;

           
            //this.BackColor = Color.FromName(cmbbackform.Text.Trim());
            //this.UpdateFormColor( Color.FromName(cmbbutbackcolor.Text.Trim()));
          
            //try
            //{
            //    BackupForm form = (BackupForm)Application.OpenForms["BackupForm"];
            //    form.BackColor = Color.FromName(cmbbutbackcolor.Text.Trim());
            //}
            //catch
            //{

            //}
          
            //f3 = new AddLineForm();
            //f3.Show();
        
            //foreach (Form form in Application.OpenForms)
            //{        
            //    if (form.Name == "AddLineForm")
            //        f3.UpdateFormColor(Color.FromName(colorDialog1.Color.Name.ToString()));
            //    else if (form.Name == "MainForm")
            //        f3.UpdateFormColor(Color.FromName(colorDialog1.Color.Name.ToString()));
            //}
            //BackupForm b = new BackupForm();
            //b.UpdateFormColor(Color.FromName(colorDialog1.Color.Name.ToString()));


             //FormColors f = FormColors.GetColor();
             //f.Formbackcolor=colorDialog1.Color.Name.ToString();

          
            //int argb = Color.Black.ToArgb();
            // Color color = Color.FromArgb(argb);


        



           if (colorDialog1.ShowDialog() == DialogResult.OK)
           {
              
               groupBox1.BackColor = Color.FromArgb(colorDialog1.Color.A,colorDialog1.Color.R, colorDialog1.Color.G, colorDialog1.Color.B);


               button1.BackColor = Color.FromArgb(colorDialog1.Color.A, colorDialog1.Color.R, colorDialog1.Color.G, colorDialog1.Color.B);
               resultformback = Color.FromArgb(colorDialog1.Color.A, colorDialog1.Color.R, colorDialog1.Color.G, colorDialog1.Color.B);
              
           }

           
        }
       

        private void button2_Click(object sender, EventArgs e)
        {
            b2click = true;



            if (colorDialog2.ShowDialog() == DialogResult.OK)
            {
                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = Color.FromArgb(colorDialog2.Color.A, colorDialog2.Color.R, colorDialog2.Color.G, colorDialog2.Color.B);
                }
                button2.BackColor = Color.FromArgb(colorDialog2.Color.A, colorDialog2.Color.R, colorDialog2.Color.G, colorDialog2.Color.B);


                resultbuttonback = Color.FromArgb(colorDialog2.Color.A, colorDialog2.Color.R, colorDialog2.Color.G, colorDialog2.Color.B);

            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult o=MessageBox.Show("Do You Want To Do These Changes?","Attention",MessageBoxButtons.YesNo );
            if (o == DialogResult.Yes)
            {
                if (b1click || b2click || b5click || b3click || b4click)
                {
                    DataTable del = Utilities.GetTable("delete from colors");
                    DataTable dt = Utilities.GetTable("insert  into  colors (formA,formR,formG,formB,textA,textR,textG,textB,buttonA,buttonR,buttonG,buttonB,panelA,panelR,panelG,panelB,gridA,gridR,gridG,gridB) values ('"
                        + resultformback.A + "','" + resultformback.R + "','" + resultformback.G + "','" + resultformback.B + "','" +
                        resulttextback.A + "','" + resulttextback.R + "','" + resulttextback.G + "','" + resulttextback.B + "','" +
                        resultbuttonback.A + "','" + resultbuttonback.R + "','" + resultbuttonback.G + "','" + resultbuttonback.B + "','" +
                        resultpanelback.A + "','" + resultpanelback.R + "','" + resultpanelback.G + "','" + resultpanelback.B + "','" +
                        resultgridback.A + "','" + resultgridback.R + "','" + resultgridback.G + "','" +resultgridback.B + "')");

                    /////////////////////////////////////////////////////////////////////////
                    dt = Utilities.GetTable("select * from colors");
                    FormColors f = FormColors.GetColor();

                    if (b1click == false) resultformback = f.Formbackcolor;
                    if (b2click == false) resultbuttonback = f.Buttonbackcolor;
                    if (b3click == false) resulttextback = f.Textbackcolor;
                    if (b4click == false) resultgridback = f.Gridbackcolor;
                    if (b5click == false) resultpanelback = f.Panelbackcolor;

                    f.Formbackcolor = resultformback;
                    f.Buttonbackcolor = resultbuttonback;
                    f.Panelbackcolor = resultpanelback;
                    f.Textbackcolor = resulttextback;
                    f.Gridbackcolor = resultgridback;
                    MessageBox.Show("Change Selected Colors Successfully.");

                }
                this.Close();
            }

        }

        private void button6_Click(object sender, EventArgs e)
        {
            b5click = true;

            if (colorDialog5.ShowDialog() == DialogResult.OK)
            {

                foreach (Control ctrl in groupBox1.Controls)
                {
                    if (ctrl is Panel)
                        ctrl.BackColor = Color.FromArgb(colorDialog5.Color.A, colorDialog1.Color.R, colorDialog1.Color.G, colorDialog1.Color.B);
                }

                panel1.BackColor = Color.FromArgb(colorDialog5.Color.A, colorDialog5.Color.R, colorDialog5.Color.G, colorDialog5.Color.B);
                button6.BackColor = Color.FromArgb(colorDialog5.Color.A, colorDialog5.Color.R, colorDialog5.Color.G, colorDialog5.Color.B);
                resultpanelback = Color.FromArgb(colorDialog5.Color.A, colorDialog5.Color.R, colorDialog5.Color.G, colorDialog5.Color.B);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            b3click = true;


            if (colorDialog3.ShowDialog() == DialogResult.OK)
            {

                foreach (Control ctrl in groupBox1.Controls)
                {
                    if (ctrl is TextBox)
                        ctrl.BackColor = Color.FromArgb(colorDialog3.Color.A, colorDialog3.Color.R, colorDialog3.Color.G, colorDialog3.Color.B);
                }

                button4.BackColor = Color.FromArgb(colorDialog3.Color.A, colorDialog3.Color.R, colorDialog3.Color.G, colorDialog3.Color.B);
                resulttextback = Color.FromArgb(colorDialog3.Color.A, colorDialog3.Color.R, colorDialog3.Color.G, colorDialog3.Color.B);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            b4click = true;

            if (colorDialog4.ShowDialog() == DialogResult.OK)
            {

                foreach (Control ctrl in groupBox1.Controls)
                {
                    if (ctrl is TextBox)
                        ctrl.BackColor = Color.FromArgb(colorDialog4.Color.A, colorDialog4.Color.R, colorDialog4.Color.G, colorDialog4.Color.B);
                }

                button5.BackColor = Color.FromArgb(colorDialog4.Color.A, colorDialog4.Color.R, colorDialog4.Color.G, colorDialog4.Color.B);
                resultgridback = Color.FromArgb(colorDialog4.Color.A, colorDialog4.Color.R, colorDialog4.Color.G, colorDialog4.Color.B);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            FormColors f = FormColors.GetColor();

            button1.BackColor = Color.Beige;
            button2.BackColor = Color.CadetBlue;
            button4.BackColor = Color.White;
            button5.BackColor = Color.LemonChiffon;
            button6.BackColor = Color.DarkSeaGreen;

            f.Formbackcolor = Color.Beige;
            f.Buttonbackcolor = Color.CadetBlue;
            f.Panelbackcolor = Color.DarkSeaGreen;
            f.Textbackcolor = Color.White;
            f.Gridbackcolor = Color.LemonChiffon;

            resultformback = Color.Beige;
            resultbuttonback = Color.CadetBlue;
            resulttextback = Color.White;
            resultgridback = Color.LemonChiffon;
            resultpanelback = Color.DarkSeaGreen;


            DataTable del = Utilities.GetTable("delete from colors");
            DataTable dt = Utilities.GetTable("insert  into  colors (formA,formR,formG,formB,textA,textR,textG,textB,buttonA,buttonR,buttonG,buttonB,panelA,panelR,panelG,panelB,gridA,gridR,gridG,gridB) values ('"
                + resultformback.A + "','" + resultformback.R + "','" + resultformback.G + "','" + resultformback.B + "','" +
                resulttextback.A + "','" + resulttextback.R + "','" + resulttextback.G + "','" + resulttextback.B + "','" +
                resultbuttonback.A + "','" + resultbuttonback.R + "','" + resultbuttonback.G + "','" + resultbuttonback.B + "','" +
                resultpanelback.A + "','" + resultpanelback.R + "','" + resultpanelback.G + "','" + resultpanelback.B + "','" +
                resultgridback.A + "','" + resultgridback.R + "','" + resultgridback.G + "','" + resultgridback.B + "')");


            MessageBox.Show("Back To Default Design.");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
    }
}
