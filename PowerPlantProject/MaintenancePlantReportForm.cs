﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NRI.SBS.Common;
using System.Data.SqlClient;

namespace PowerPlantProject
{
    public partial class MaintenancePlantReportForm : Form
    {
        string PPID;
        public MaintenancePlantReportForm(string id)
        {
            PPID = id;
            InitializeComponent();
        }

        private void MaintenancePlantReportForm_Load(object sender, EventArgs e)
        {
            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = new SqlConnection(ConnectionManager.ConnectionString);
            MyCom.Connection.Open();

            //Delete CrystalMaintenance
            MyCom.CommandText = "DELETE FROM CrystalMaintenance";
            MyCom.ExecuteNonQuery();
            DataTable myTemp = Utilities.GetTable("SELECT MaintenanceResult.id,MaintenanceResult.ppid,MaintenanceResult.date,MaintenanceResult.UnitCode,MaintResultWeekly.weekindex FROM MaintenanceResult INNER JOIN MaintResultWeekly ON MaintenanceResult.id=MaintResultWeekly.MaintenanceResultId WHERE MaintenanceResult.ppid='"+PPID+"' AND MaintenanceResult.date=(SELECT Max(MaintenanceResult.date) from MaintenanceResult where ppid='"+PPID+"') AND MaintResultWeekly.value=1 order by UnitCode,weekindex");
            int Rowindex = 0;
            while (Rowindex < myTemp.Rows.Count)
            {
                string date = myTemp.Rows[Rowindex][2].ToString();
                string unit = myTemp.Rows[Rowindex][3].ToString();
                int firstweekindex = int.Parse(myTemp.Rows[Rowindex][4].ToString());
                int weekindex = firstweekindex;
                while ((Rowindex + 1 < myTemp.Rows.Count) && (int.Parse(myTemp.Rows[Rowindex + 1][4].ToString()) == weekindex + 1))
                {
                    Rowindex++;
                    weekindex = int.Parse(myTemp.Rows[Rowindex][4].ToString());
                }
                //query
                string Duration="";
                if (firstweekindex==weekindex) 
                    Duration="Week "+(weekindex+1)+" ";
                else Duration="From Week "+(firstweekindex+1)+" To Week "+ (weekindex+1)+" ";
                MyCom.CommandText = "INSERT INTO CrystalMaintenance (PPID,Date,UnitCode,WeekDur) VALUES ('" + PPID + "','" + date + "','" + unit.Trim() + "','"+Duration + "')";
                MyCom.ExecuteNonQuery();
                if (Rowindex < myTemp.Rows.Count) Rowindex++;
            }
            MyCom.Connection.Close();

           // MaintenanceCrystalReportDoc1.SetDataSource(utilities.GetTable("SELECT * FROM VWCrystalMaintenance"));
           // MaintenancecrystalReportViewer1.ReportSource = MaintenanceCrystalReportDoc1;
        }
    }
}
