﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
//using Microsoft.Office.Core;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using PowerPlantProject.New;

namespace PowerPlantProject
{
    partial class MainForm
    {
        //------------------------------MRCal_ValueChanged-------------------------------------
        private void MRCal_ValueChanged(object sender, EventArgs e)
        {
            if (MRHeaderPanel.Visible)
                FillMRUnitGrid();
            else if (L9.Text.Contains("Transmission"))
                FillMRTransmission(line.ToString().Trim());
            else if (L9.Text.Contains("Line"))
                FillMRLine(line.ToString().Trim(), MRPlantLb.Text.Trim());
            else if (L9.Text == "All Plants")
                FillMRGrecGrid();
            else FillMRPlantGrid();
        }
        //----------------------------FillMRVlues------------------------------------
        private void FillMRVlues(string Num)
        {
            string tname = "[DetailFRM005]";
            if (rdbam005ba.Checked) tname = "[baDetailFRM005]";

            //Clear Market Result Tab
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            MRPlantOnUnitTb.Text = "";
            MRPlantPowerTb.Text = "";

            //detect current date and hour
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");
            int myhour = DateTime.Now.Hour;

            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;


            //set OnUnits & Power TextBox
            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = new SqlConnection(ConnectionManager.ConnectionString);
            MyCom.Connection.Open();

            MyCom.CommandText = "SELECT @re1=COUNT(DISTINCT Block), @re2=SUM (Required) FROM "+tname+" WHERE" +
            "  TargetMarketDate=@date AND PPID=@pid AND Hour=@hour AND Required <> 0";
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = mydate;
            MyCom.Parameters.Add("@pid", SqlDbType.NChar, 10);
            MyCom.Parameters["@pid"].Value = Num;
            MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
            MyCom.Parameters["@hour"].Value = myhour;
            MyCom.Parameters.Add("@re1", SqlDbType.Int);
            MyCom.Parameters["@re1"].Direction = ParameterDirection.Output;
            MyCom.Parameters.Add("@re2", SqlDbType.Real);
            MyCom.Parameters["@re2"].Direction = ParameterDirection.Output;
            MyCom.ExecuteNonQuery();
            MyCom.Connection.Close();

            MRPlantOnUnitTb.Text = MyCom.Parameters["@re1"].Value.ToString().Trim();
            if (MyCom.Parameters["@re2"].Value.ToString() != "")
                MRPlantPowerTb.Text = MyCom.Parameters["@re2"].Value.ToString().Trim();
            else MRPlantPowerTb.Text = "0";

            //Set GridViews
            if (!MRCal.IsNull)
                FillMRPlantGrid();
            //{
            //    DataSet MyDS = new DataSet();
            //    SqlDataAdapter Myda = new SqlDataAdapter();

            //    Myda.SelectCommand = new SqlCommand("SELECT Hour,SUM(Required) AS SR FROM [DetailFRM005] WHERE PPID= "+Num+" AND TargetMarketDate=@date GROUP BY Hour", MyConnection);
            //    Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
            //    Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
            //    Myda.Fill(MyDS);
            //    MRCurGrid1.Rows[0].Cells[0].Value = "Power";
            //    MRCurGrid2.Rows[0].Cells[0].Value = "Power";
            //    foreach (DataRow MyRow in MyDS.Tables[0].Rows)
            //    {
            //        int index=int.Parse(MyRow["Hour"].ToString());
            //        if (index <= 12)
            //            MRCurGrid1.Rows[0].Cells[index].Value = MyRow["SR"];
            //        else
            //            MRCurGrid2.Rows[0].Cells[index - 12].Value = MyRow["SR"];
            //    }
            // }

        }
        //------------------------------FillMRTransmission-------------------------
        private void FillMRTransmission(string TransType)
        {
            string smaxdate = "";
            int b = 0;
            // detect max date in database...................................  
            DataTable oDataTable = Utilities.GetTable("select Date from BaseData");
            int ndate = oDataTable.Rows.Count;
            string[] arrmaxdate1 = new string[ndate];
            for (b = 0; b < ndate; b++)
            {
                arrmaxdate1[b] = oDataTable.Rows[b][0].ToString().Trim();
            }

            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + MRCal.Text.Trim() + "'order by BaseID desc");
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }


            //Clear Market Result Tab
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            MRPlantOnUnitTb.Text = "";
            MRPlantPowerTb.Text = "";
            if (!MRCal.IsNull)
            {
                MRCurGrid1.Rows.Add();
                MRCurGrid2.Rows.Add();

                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                //SET FIRST Row of DataGrid
                MRCurGrid1.Rows[0].Cells[0].Value = "Power";
                MRCurGrid2.Rows[0].Cells[0].Value = "Power";

                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT  SUM((TransLine.Type)*Hour1),SUM((TransLine.Type)*Hour2)," +
                "SUM((TransLine.Type)*Hour3),SUM((TransLine.Type)*Hour4),SUM((TransLine.Type)*Hour5),SUM((TransLine.Type)*Hour6)," +
                "SUM((TransLine.Type)*Hour7),SUM((TransLine.Type)*Hour8),SUM((TransLine.Type)*Hour9),SUM((TransLine.Type)*Hour10)," +
                "SUM((TransLine.Type)*Hour11),SUM((TransLine.Type)*Hour12),SUM((TransLine.Type)*Hour13),SUM((TransLine.Type)*Hour14)," +
                "SUM((TransLine.Type)*Hour15),SUM((TransLine.Type)*Hour16),SUM((TransLine.Type)*Hour17),SUM((TransLine.Type)*Hour18)," +
                "SUM((TransLine.Type)*Hour19),SUM((TransLine.Type)*Hour20),SUM((TransLine.Type)*Hour21),SUM((TransLine.Type)*Hour22)," +
                "SUM((TransLine.Type)*Hour23),SUM((TransLine.Type)*Hour24) FROM InterchangedEnergy INNER JOIN TransLine " +
                "ON InterchangedEnergy.Code = TransLine.LineCode WHERE InterchangedEnergy.Date=@date AND TransLine.LineNumber=@trans", myConnection);
                Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                Myda.SelectCommand.Parameters.Add("@trans", SqlDbType.Int);
                Myda.SelectCommand.Parameters["@trans"].Value = int.Parse(TransType);
                Myda.Fill(MyDS);

                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    for (int i = 0; i < 24; i++)
                        if (i < 12)
                            MRCurGrid1.Rows[0].Cells[i + 1].Value = MyRow[i].ToString().Trim();
                        else
                            MRCurGrid2.Rows[0].Cells[i - 11].Value = MyRow[i].ToString().Trim();
                }

                //Read From BaseData
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = myConnection;
                MyCom.CommandText = "select @lep=LineEnergyPayment,@lcp=LineCapacityPayment From BaseData " +
                "where Date ='" + smaxdate + "'order by BaseID desc";
                MyCom.Parameters.Add("@lep", SqlDbType.Real);
                MyCom.Parameters["@lep"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@lcp", SqlDbType.Real);
                MyCom.Parameters["@lcp"].Direction = ParameterDirection.Output;
                MyCom.ExecuteNonQuery();

                double LineEnergyPayment, LineCapacityPayment;
                try { LineEnergyPayment = double.Parse(MyCom.Parameters["@lep"].Value.ToString()); }
                catch { LineEnergyPayment = 0; }
                try { LineCapacityPayment = double.Parse(MyCom.Parameters["@lcp"].Value.ToString()); }
                catch { LineCapacityPayment = 0; }

                //Detect Lines of This Voltage Level
                DataSet LineDS = new DataSet();
                SqlDataAdapter Lineda = new SqlDataAdapter();
                Lineda.SelectCommand = new SqlCommand("SELECT TransLine.LineCode,TransLine.Type,Lines.LineLength " +
                "FROM Lines INNER JOIN TransLine ON Lines.LineCode = TransLine.LineCode WHERE" +
                " TransLine.LineNumber=" + TransType, myConnection);
                Lineda.Fill(LineDS);
                DataTable LinesTable = LineDS.Tables[0];

                //Read InterchangedEnergy for selected Date
                DataSet InterchangedDS = new DataSet();
                SqlDataAdapter Interchangedda = new SqlDataAdapter();
                Interchangedda.SelectCommand = new SqlCommand("SELECT  Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8," +
                "Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21," +
                "Hour22,Hour23,Hour24,Code FROM InterchangedEnergy INNER JOIN Lines ON InterchangedEnergy.Code=" +
                "Lines.LineCode WHERE Date=@date AND Lines.LineNumber=@trans", myConnection);
                Interchangedda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Interchangedda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                Interchangedda.SelectCommand.Parameters.Add("@trans", SqlDbType.Int);
                Interchangedda.SelectCommand.Parameters["@trans"].Value = TransType;
                Interchangedda.Fill(InterchangedDS);
                DataTable IntercganedTable = InterchangedDS.Tables[0];

                //SET Second Row of DataGrid (EnergyPayment)&& TextBoxes
                MRCurGrid1.Rows[1].Cells[0].Value = "Energy Payment";
                MRCurGrid2.Rows[1].Cells[0].Value = "Energy Payment";
                double[] EnergyPaymentGrid = new double[24];
                double CpacityPayment = 0, EnergyPayment = 0;
                foreach (DataRow MyRow in LinesTable.Rows)
                {
                    string LineCode = MyRow[0].ToString().Trim();
                    int Interchanged_Index = 0;
                    int k = 0;
                    foreach (DataRow InRow in IntercganedTable.Rows)
                    {
                        if (InRow[24].ToString().Trim() == LineCode)
                            Interchanged_Index = k;
                        k++;
                    }
                    double LineLength;
                    int LineType;
                    if (MyRow[2].ToString().Trim() != "")
                        LineLength = MyDoubleParse(MyRow[2].ToString().Trim());
                    else LineLength = 0;
                    if (MyRow[1].ToString().Trim() != "")
                        LineType = int.Parse(MyRow[1].ToString().Trim());
                    else LineType = 0;
                    if (k != 0)
                        for (int i = 0; i < 24; i++)
                            EnergyPaymentGrid[i] += Math.Abs(LineEnergyPayment * MyDoubleParse(IntercganedTable.Rows[Interchanged_Index][i].ToString().Trim()) * LineLength);

                    double ReadytoWork = 0;
                    if (TransType == "230")
                        ReadytoWork = 1600 * 0.230 * LineLength * Math.Pow(3, 0.5);
                    else
                        ReadytoWork = 1600 * 0.400 * LineLength * Math.Pow(3, 0.5);

                    CpacityPayment += 24 * (LineCapacityPayment * ReadytoWork * LineLength);
                }

                for (int i = 0; i < 24; i++)
                {
                    EnergyPayment += EnergyPaymentGrid[i];
                    if (i < 12)
                        MRCurGrid1.Rows[1].Cells[i + 1].Value = EnergyPaymentGrid[i];
                    else
                        MRCurGrid2.Rows[1].Cells[i - 11].Value = EnergyPaymentGrid[i];
                }

                MRPlantOnUnitTb.Text = CpacityPayment.ToString();
                MRPlantPowerTb.Text = EnergyPayment.ToString();

                myConnection.Close();
            }
        }
        //----------------------------FillMRLine------------------------------------
        private void FillMRLine(string TransType, string TransLine)
        {
            double[] ls2value = new double[24];
            string smaxdate = "";
            int b = 0;
            // detect max date in database...................................  
            DataTable oDataTable = Utilities.GetTable("select Date from BaseData");
            int ndate = oDataTable.Rows.Count;
            string[] arrmaxdate1 = new string[ndate];
            for (b = 0; b < ndate; b++)
            {
                arrmaxdate1[b] = oDataTable.Rows[b][0].ToString().Trim();
            }

            //string smaxdate = buildmaxdate(arrmaxdate1);

            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + MRCal.Text.Trim() + "'order by BaseID desc");
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }



            //Clear Market Result Tab
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            MRPlantOnUnitTb.Text = "";
            MRPlantPowerTb.Text = "";
            if (!MRCal.IsNull)
            {
                MRCurGrid1.Rows.Add();
                MRCurGrid2.Rows.Add();

                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = myConnection;

                //Read InterchangedEnergy for selected Date
                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT  Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8," +
                "Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21," +
                "Hour22,Hour23,Hour24 FROM InterchangedEnergy WHERE Date=@date AND Code =@code", myConnection);
                Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                Myda.SelectCommand.Parameters.Add("@code", SqlDbType.NChar, 20);
                Myda.SelectCommand.Parameters["@code"].Value = TransLine;
                Myda.Fill(MyDS);

                //Read TransLine.Type , Lines.LineLengh
                MyCom.CommandText = "SELECT @type=Type FROM TransLine WHERE LineCode=@code SELECT @length=LineLength FROM Lines WHERE LineCode=@code";
                MyCom.Parameters.Add("@code", SqlDbType.NChar, 20);
                MyCom.Parameters["@code"].Value = TransLine;
                MyCom.Parameters.Add("@type", SqlDbType.Int);
                MyCom.Parameters["@type"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@length", SqlDbType.Real);
                MyCom.Parameters["@length"].Direction = ParameterDirection.Output;
                MyCom.ExecuteNonQuery();
                double LineLength;
                int LineType;
                try { LineLength = double.Parse(MyCom.Parameters["@length"].Value.ToString()); }
                catch { LineLength = 0; }
                try { LineType = int.Parse(MyCom.Parameters["@type"].Value.ToString()); }
                catch { LineType = 1; }

                //SET first Row of DataGrid (Energy Payment)
                for (int x = 0; x < 24; x++)
                    if (x < 12)
                    {
                        MRCurGrid1.Rows[0].Cells[x + 1].Value = "0";
                        MRCurGrid1.Rows[1].Cells[x + 1].Value = "0";
                    }
                    else
                    {
                        MRCurGrid2.Rows[0].Cells[x - 11].Value = "0";
                        MRCurGrid2.Rows[1].Cells[x - 11].Value = "0";
                    }

                MRCurGrid1.Rows[0].Cells[0].Value = "Power";
                MRCurGrid2.Rows[0].Cells[0].Value = "Power";

                if (MyDS.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                    {
                        for (int i = 0; i < 24; i++)
                            if (i < 12)
                                MRCurGrid1.Rows[0].Cells[i + 1].Value = LineType * MyDoubleParse(MyRow[i].ToString().Trim());
                            else
                                MRCurGrid2.Rows[0].Cells[i - 11].Value = LineType * MyDoubleParse(MyRow[i].ToString().Trim());
                    }
                }
                else
                {
                    for (int i = 0; i < 24; i++)
                    {
                        DataTable ddls = Utilities.GetTable("select * from dbo.DetailFRM009LINE where TargetMarketDate='" + MRCal.Text.Trim() + "' and LineCode='" + TransLine + "'and Hour='" + i + "'");
                        if (ddls.Rows.Count > 0)
                        {
                            DataRow MyRow = ddls.Rows[0];
                            if (i < 12)
                                MRCurGrid1.Rows[0].Cells[i + 1].Value = MyDoubleParse(MyRow["P"].ToString().Trim()) * MyDoubleParse(MyRow["FlagLine"].ToString().Trim());
                            else
                                MRCurGrid2.Rows[0].Cells[i - 11].Value = MyDoubleParse(MyRow["P"].ToString().Trim()) * MyDoubleParse(MyRow["FlagLine"].ToString().Trim());

                            ls2value[i] = MyDoubleParse(MyRow["P"].ToString().Trim()) * MyDoubleParse(MyRow["FlagLine"].ToString().Trim());
                        }
                    }

                }

                //Read From BaseData
                MyCom.CommandText = "select @lep=LineEnergyPayment,@lcp=LineCapacityPayment From BaseData " +
                "where Date ='" + smaxdate + "'order by BaseID desc";
                MyCom.Parameters.Add("@lep", SqlDbType.Real);
                MyCom.Parameters["@lep"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@lcp", SqlDbType.Real);
                MyCom.Parameters["@lcp"].Direction = ParameterDirection.Output;
                MyCom.ExecuteNonQuery();

                double LineEnergyPayment, LineCapacityPayment;
                try { LineEnergyPayment = double.Parse(MyCom.Parameters["@lep"].Value.ToString()); }
                catch { LineEnergyPayment = 0; }
                try { LineCapacityPayment = double.Parse(MyCom.Parameters["@lcp"].Value.ToString()); }
                catch { LineCapacityPayment = 0; }

                //SET Second Row of DataGrid (Energy Payment)

                MRCurGrid1.Rows[1].Cells[0].Value = "Energy Payment";
                MRCurGrid2.Rows[1].Cells[0].Value = "Energy Payment";

                if (MyDS.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                    {
                        for (int i = 0; i < 24; i++)
                            if (i < 12)
                                MRCurGrid1.Rows[1].Cells[i + 1].Value = Math.Abs(LineEnergyPayment * MyDoubleParse(MyRow[i].ToString().Trim()) * LineLength);
                            else
                                MRCurGrid2.Rows[1].Cells[i - 11].Value = Math.Abs(LineEnergyPayment * MyDoubleParse(MyRow[i].ToString().Trim()) * LineLength);
                    }
                }
                else
                {
                    for (int i = 0; i < 24; i++)
                        if (i < 12)
                            MRCurGrid1.Rows[1].Cells[i + 1].Value = Math.Abs(LineEnergyPayment * ls2value[i] * LineLength);
                        else
                            MRCurGrid2.Rows[1].Cells[i - 11].Value = Math.Abs(LineEnergyPayment * ls2value[i] * LineLength);
                }

                //SET EnergyPayment, CapacityPayment TEXTBOXes 
                double ReadytoWork = 0;
                if (TransType == "230")
                    ReadytoWork = 1600 * 0.230 * LineLength * Math.Pow(3, 0.5);
                else
                    ReadytoWork = 1600 * 0.400 * LineLength * Math.Pow(3, 0.5);

                double EnergyPayment = 0, CapacityPayment = 0;
                CapacityPayment = 24 * (LineCapacityPayment * ReadytoWork);

                for (int i = 0; i < 24; i++)
                    if (i < 12)
                        EnergyPayment += MyDoubleParse(MRCurGrid1.Rows[1].Cells[i + 1].Value.ToString());
                    else
                        EnergyPayment += MyDoubleParse(MRCurGrid2.Rows[1].Cells[i - 11].Value.ToString());

                MRPlantOnUnitTb.Text = CapacityPayment.ToString();
                MRPlantPowerTb.Text = EnergyPayment.ToString();

                myConnection.Close();
            }
        }
        //----------------------------FillMRUnit-------------------------------------
        private void FillMRUnit(string unit, string package, int index)
        {
            //Clear Market Result Tab
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            MRUnitStateTb.Text = "";
            MRUnitPowerTb.Text = "";
            MRUnitMaxBidTb.Text = "";

            //Detect Farsi Date
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");
            int myhour = DateTime.Now.Hour;

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();
            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = myConnection;

            //Detect Block For FRM005
            string temp = unit;
            temp = temp.ToLower();

            string ptypenum = "0";
            if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";
            //if (PPID == "232") ptypenum = "0";
            if (Findcconetype(PPID)) ptypenum = "0";

            if (package.Contains("Combined"))
            {
                int x;
                try
                {
                    x = int.Parse(PPID);
                }
                catch (Exception e)
                {
                    x = 0;
                }
                //is This a Plant with 2 package Type?
                //MyCom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id";
                //MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                //MyCom.Parameters["@id"].Value = PPID;
                //MyCom.Parameters.Add("@result1", SqlDbType.Int);
                //MyCom.Parameters["@result1"].Direction = ParameterDirection.Output;
                //MyCom.ExecuteNonQuery();
                //int result1 = (int)MyCom.Parameters["@result1"].Value;
                //if (result1 > 1) x++;
                if (PPIDArray.Contains(x + 1))
                    if (PPIDType[PPIDArray.IndexOf(x + 1)].ToString() == "virtual") x++;
                //if ((x == 131) || (x == 144)) x++;
                string packagecode = "";
                if (GDSteamGroup.Text.Contains(package))
                    packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                else packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                //temp = x + "-" + "C" + packagecode;

                //ccunitbase///////////////////////////////////////////
                //temp = x + "-" + "C" + packagecode;

                temp = temp.Replace("cc", "c");
                string[] sp = temp.Split('c');
                temp = sp[0].Trim() + sp[1].Trim();
                if (temp.Contains("gas"))
                {
                    temp = temp.Replace("gas", "G");

                }
                else
                {
                    temp = temp.Replace("steam", "S");

                }
                temp = x + "-" + temp;


            }
            else if (temp.Contains("gas"))
            {
                temp = temp.Replace("gas", "G");
                temp = PPID + "-" + temp;
            }
            else
            {
                temp = temp.Replace("steam", "S");
                temp = PPID + "-" + temp;
            }

            MRUnitStateTb.Text = GDStateTB.Text;
            if (MRUnitStateTb.Text == "ON")
            {
                string tname = "[DetailFRM005]";
                if (rdbam005ba.Checked) tname = "[baDetailFRM005]";

                MyCom.CommandText = "SELECT @re=Required FROM "+tname+" WHERE TargetMarketDate=@date " +
                "AND PPID=@num AND Block=@block and PPType='" + ptypenum + "' AND Hour=@hour ";
                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = mydate;
                MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                MyCom.Parameters["@num"].Value = PPID;
                MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                MyCom.Parameters["@block"].Value = temp;
                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                MyCom.Parameters["@hour"].Value = myhour;
                MyCom.Parameters.Add("@re", SqlDbType.Real);
                MyCom.Parameters["@re"].Direction = ParameterDirection.Output;
                //try
                //{
                MyCom.ExecuteNonQuery();
                //}
                //catch (Exception exp)
                //{
                //  string str = exp.Message;
                //}
                MRUnitPowerTb.Text = MyCom.Parameters["@re"].Value.ToString().Trim();

                //Detect MaxBid field
                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10 FROM [DetailFRM002] " +
                "WHERE Estimated<>1 AND TargetMarketDate=@date AND Block=@ptype AND PPType='" + ptypenum + "'and Hour=@hour AND PPID=" + PPID, myConnection);
                Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Myda.SelectCommand.Parameters["@date"].Value = mydate;
                Myda.SelectCommand.Parameters.Add("@hour", SqlDbType.SmallInt);
                Myda.SelectCommand.Parameters["@hour"].Value = myhour;
                //Detect Block For FRM002
                string ptype = unit;
                ptype = ptype.ToLower();
                if (package.Contains("Combined"))
                {
                    //string packagecode = "";
                    //if (GDGasGroup.Text.Contains("Combined"))
                    //    packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                    //else packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                    //ptype = "C" + packagecode;
                    ptype = ptype.Replace("cc", "c");
                    string[] sp = ptype.Split('c');
                    ptype = sp[0].Trim() + sp[1].Trim();
                    if (ptype.Contains("gas"))
                        ptype = ptype.Replace("gas", "G");
                    else if (ptype.Contains("steam"))
                        ptype = ptype.Replace("steam", "S");

                }
                else
                {
                    if (ptype.Contains("gas"))
                        ptype = ptype.Replace("gas", "G");
                    else if (ptype.Contains("steam"))
                        ptype = ptype.Replace("steam", "S");
                }
                ptype = ptype.Trim();
                Myda.SelectCommand.Parameters.Add("@ptype", SqlDbType.NChar, 20);
                Myda.SelectCommand.Parameters["@ptype"].Value = ptype;
                Myda.Fill(MyDS);
               
            }
            myConnection.Close();

            if (!MRCal.IsNull)
                FillMRUnitGrid();
        }
        //--------------------------------------FillMRUnitGrid()------------------------------------------
        private void FillMRUnitGrid()
        {



            MRPlotBtn.Enabled = false;
            mrbtnpprint.Enabled = false;
            btnToExcelMR.Enabled = false;

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            if (!MRCal.IsNull)
            {
                string package = MRPackLb.Text.Trim();
                string unit = MRUnitLb.Text.Trim();


                string ptypenum = "0";
                if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";
                //if (PPID == "232") ptypenum = "0";
                if (Findcconetype(PPID)) ptypenum = "0";


                //Get Index Of Selected Unit(Row) in GridView (In Generad data Tab)
                int index = 0;
                if (GDSteamGroup.Text.Contains(package))
                    for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                    {
                        if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                            index = i;
                    }
                else
                    for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                    {
                        if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                            index = i;
                    }

                //Detect Block For FRM005
                string temp = unit;
                temp = temp.ToLower();
                if (package.Contains("Combined"))
                {
                    int x;
                    try
                    {
                        x = int.Parse(PPID);
                    }
                    catch (Exception e)
                    {
                        x = 0;
                    }
                    if (PPIDArray.Contains(x + 1))
                        if (PPIDType[PPIDArray.IndexOf(x + 1)].ToString() == "virtual") x++;
                    //if ((x == 131) || (x == 144)) x++;
                    string packagecode = "";
                    if (GDSteamGroup.Text.Contains(package))
                        packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                    else packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();




                    //ccunitbase///////////////////////////////////////////
                    //temp = x + "-" + "C" + packagecode;

                    temp = temp.Replace("cc", "c");
                    string[] sp = temp.Split('c');
                    temp = sp[0].Trim() + sp[1].Trim();
                    if (temp.Contains("gas"))
                    {
                        temp = temp.Replace("gas", "G");

                    }
                    else
                    {
                        temp = temp.Replace("steam", "S");

                    }
                    temp = x + "-" + temp;
                    ///////////////////////////////////////////////////////
                }
                else if (temp.Contains("gas"))
                {
                    temp = temp.Replace("gas", "G");
                    temp = PPID + "-" + temp;
                }
                else
                {
                    temp = temp.Replace("steam", "S");
                    temp = PPID + "-" + temp;
                }

                //Read Required,Dispatchable,Contribution From M005
                string tname = "[DetailFRM005]";
                if (rdbam005ba.Checked) tname = "[baDetailFRM005]";

                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT Hour,Required,Dispatchable,Contribution,Economic,StartUL,EndUL,StartOC,EndOC FROM " + tname + "" +
                " WHERE PPID= " + PPID + " AND TargetMarketDate=@date and PPType='" + ptypenum + "'AND Block=@block  SELECT " +
                "Hour,P,QC,QL,Consumed FROM DetailFRM009 WHERE PPID= " + PPID + " AND TargetMarketDate=@date " +
                "AND Block='" + unit + "'", myConnection);
                Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                Myda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
                Myda.SelectCommand.Parameters["@block"].Value = temp;
                Myda.Fill(MyDS);
                /////////////new khorsand//////////////////
                DataTable if005null = Utilities.GetTable("SELECT Hour,Required,Dispatchable,Contribution,Economic,StartUL,EndUL,StartOC,EndOC FROM " +
                  "" + tname + " WHERE PPID= " + PPID + " AND TargetMarketDate='" + MRCal.Text + "'and PPType='" + ptypenum + "'");
                if (if005null.Rows.Count < 1) return;

                MRCurGrid1.DataSource = MyDS.Tables[0].DefaultView;
                MRCurGrid2.DataSource = MyDS.Tables[1].DefaultView;
                MRCurGrid1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                MRCurGrid2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;



                //////////////////////////////////////
                if ((MRCurGrid1.RowCount > 1) || (MRCurGrid2.RowCount > 1))
                {
                    MRPlotBtn.Enabled = true;
                    mrbtnpprint.Enabled = true;
                    btnToExcelMR.Enabled = true;

                    if (MRCurGrid1.RowCount > 1) MRCurGrid2.DataSource = MyDS.Tables[0].DefaultView;
                    if (MRCurGrid2.RowCount > 1) MRCurGrid1.DataSource = MRCurGrid2.DataSource;

                    MRCurGrid1.Rows[0].Cells[0].Value = "Required";
                    MRCurGrid2.Rows[0].Cells[0].Value = "Required";
                    MRCurGrid1.Rows[1].Cells[0].Value = "Dispatchable";
                    MRCurGrid2.Rows[1].Cells[0].Value = "Dispatchable";
                    MRCurGrid1.Rows[2].Cells[0].Value = "Contribution";
                    MRCurGrid2.Rows[2].Cells[0].Value = "Conrtibution";
                    MRCurGrid1.Rows[3].Cells[0].Value = "Price";
                    MRCurGrid2.Rows[3].Cells[0].Value = "Price";
                    MRCurGrid1.Rows[4].Cells[0].Value = "Economic";
                    MRCurGrid2.Rows[4].Cells[0].Value = "Economic";

                    MRCurGrid1.Rows[5].Cells[0].Value = "Power";
                    MRCurGrid2.Rows[5].Cells[0].Value = "Power";
                    MRCurGrid1.Rows[6].Cells[0].Value = "QC";
                    MRCurGrid2.Rows[6].Cells[0].Value = "QC";
                    MRCurGrid1.Rows[7].Cells[0].Value = "QL";
                    MRCurGrid2.Rows[7].Cells[0].Value = "QL";
                    MRCurGrid1.Rows[8].Cells[0].Value = "Consumed";
                    MRCurGrid2.Rows[8].Cells[0].Value = "Consumed";

                    //ADD
                    MRCurGrid1.Rows[9].Cells[0].Value = "StartUL";
                    MRCurGrid2.Rows[9].Cells[0].Value = "StartUL";
                    MRCurGrid1.Rows[10].Cells[0].Value = "EndUL";
                    MRCurGrid2.Rows[10].Cells[0].Value = "EndUL";
                    MRCurGrid1.Rows[11].Cells[0].Value = "StartOC";
                    MRCurGrid2.Rows[11].Cells[0].Value = "StartOC";
                    MRCurGrid1.Rows[12].Cells[0].Value = "EndOC";
                    MRCurGrid2.Rows[12].Cells[0].Value = "EndOC";

                    //





                    //Fill four Rows of DataGridView (Require,Dispachable,Contribution,Economic)
                    foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                    {
                        int x = int.Parse(MyRow["Hour"].ToString().Trim());
                        if (x <= 12)
                        {
                            MRCurGrid1.Rows[0].Cells[x].Value = MyRow["Required"];
                            MRCurGrid1.Rows[1].Cells[x].Value = MyRow["Dispatchable"];
                            MRCurGrid1.Rows[2].Cells[x].Value = MyRow["Contribution"].ToString().Trim();
                            MRCurGrid1.Rows[4].Cells[x].Value = MyRow["Economic"];
                            //ADD
                            MRCurGrid1.Rows[9].Cells[x].Value = MyRow["StartUL"];
                            MRCurGrid1.Rows[10].Cells[x].Value = MyRow["EndUL"];
                            MRCurGrid1.Rows[11].Cells[x].Value = MyRow["StartOC"];
                            MRCurGrid1.Rows[12].Cells[x].Value = MyRow["EndOC"];

                        }
                        else
                        {
                            MRCurGrid2.Rows[0].Cells[x - 12].Value = MyRow["Required"];
                            MRCurGrid2.Rows[1].Cells[x - 12].Value = MyRow["Dispatchable"];
                            MRCurGrid2.Rows[2].Cells[x - 12].Value = MyRow["Contribution"].ToString().Trim();
                            MRCurGrid2.Rows[4].Cells[x - 12].Value = MyRow["Economic"];
                            //ADD
                            MRCurGrid2.Rows[9].Cells[x - 12].Value = MyRow["StartUL"];
                            MRCurGrid2.Rows[10].Cells[x - 12].Value = MyRow["EndUL"];
                            MRCurGrid2.Rows[11].Cells[x - 12].Value = MyRow["StartOC"];
                            MRCurGrid2.Rows[12].Cells[x - 12].Value = MyRow["EndOC"];
                        }
                    }

                    //Fill three Rows of DataGridView (P,QC,QL,Consumed)
                    foreach (DataRow MyRow in MyDS.Tables[1].Rows)
                    {
                        int x = int.Parse(MyRow["Hour"].ToString().Trim());
                        if (x < 12)
                        {
                            MRCurGrid1.Rows[5].Cells[x + 1].Value = MyRow["P"];
                            MRCurGrid1.Rows[6].Cells[x + 1].Value = MyRow["QC"];
                            MRCurGrid1.Rows[7].Cells[x + 1].Value = MyRow["QL"];
                            MRCurGrid1.Rows[8].Cells[x + 1].Value = MyRow["Consumed"];
                        }
                        else
                        {
                            MRCurGrid2.Rows[5].Cells[x - 11].Value = MyRow["P"];
                            MRCurGrid2.Rows[6].Cells[x - 11].Value = MyRow["QC"];
                            MRCurGrid2.Rows[7].Cells[x - 11].Value = MyRow["QL"];
                            MRCurGrid2.Rows[8].Cells[x - 11].Value = MyRow["Consumed"];
                        }
                    }

                    for (int i = (MRCurGrid1.RowCount - 2); i > 12; i--)
                        MRCurGrid1.Rows.Remove(MRCurGrid1.Rows[i]);
                    for (int i = (MRCurGrid2.RowCount - 2); i > 12; i--)
                        MRCurGrid2.Rows.Remove(MRCurGrid2.Rows[i]);

                    //Read Power And Price From M002
                    DataSet MaxDS = new DataSet();
                    SqlDataAdapter Maxda = new SqlDataAdapter();
                    Maxda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                    "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10,Hour FROM [DetailFRM002] " +
                    "WHERE Estimated<>1 AND TargetMarketDate=@date and PPType='" + ptypenum + "' AND Block=@ptype AND PPID=" + PPID, myConnection);
                    Maxda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                    Maxda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                    //Detect Block For FRM002
                    string ptype = unit;
                    ptype = ptype.ToLower();
                    if (package.Contains("Combined"))
                    {
                        //ccunitbase///////////////////////////////////////////
                        //string packagecode = "";
                        //if (GDGasGroup.Text.Contains("Combined"))
                        //    packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                        //else packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                        //ptype = "C" + packagecode;

                        ptype = ptype.Replace("cc", "c");
                        string[] sp = ptype.Split('c');
                        ptype = sp[0].Trim() + sp[1].Trim();
                        if (ptype.Contains("gas"))
                            ptype = ptype.Replace("gas", "G");
                        else if (ptype.Contains("steam"))
                            ptype = ptype.Replace("steam", "S");
                    }
                    else
                    {
                        if (ptype.Contains("gas"))
                            ptype = ptype.Replace("gas", "G");
                        else if (ptype.Contains("steam"))
                            ptype = ptype.Replace("steam", "S");
                    }
                    ptype = ptype.Trim();
                    Maxda.SelectCommand.Parameters.Add("@ptype", SqlDbType.NChar, 20);
                    Maxda.SelectCommand.Parameters["@ptype"].Value = ptype;
                    Maxda.Fill(MaxDS);
                    DataTable PowerPrice = new DataTable();
                    PowerPrice = MaxDS.Tables[0];

                    //Read Previous MaxBid
                    DataSet BidDS = new DataSet();
                    SqlDataAdapter Bidda = new SqlDataAdapter();
                    Bidda.SelectCommand = new SqlCommand("SELECT MaxBid,Hour FROM " + tname + " WHERE TargetMarketDate" +
                    "=@date AND PPID=@num and PPType='" + ptypenum + "' AND Block=@block", myConnection);
                    Bidda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                    Bidda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                    Bidda.SelectCommand.Parameters.Add("@num", SqlDbType.NChar, 10);
                    Bidda.SelectCommand.Parameters["@num"].Value = PPID;
                    Bidda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
                    Bidda.SelectCommand.Parameters["@block"].Value = temp;
                    Bidda.Fill(BidDS);
                    double[] PreMaxBid = new double[24];
                    for (int i = 0; i < 24; i++)
                        PreMaxBid[i] = 0;
                    foreach (DataRow BidRow in BidDS.Tables[0].Rows)
                    {
                        if (BidRow[1].ToString() != "")
                        {
                            int myhour = int.Parse(BidRow[1].ToString()) - 1;
                            if (BidRow[0].ToString() != "")
                                PreMaxBid[myhour] = double.Parse(BidRow[0].ToString());
                        }
                    }

                    //Detect MaxBid field in MRCurGrid1
                    //////////////////////////////////////////////////////////////////////////
                    // new 910820
                    DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + MRCal.Text + "'order by BaseID desc");
                    string staterun = "";
                    if (dtsmaxdate.Rows.Count > 0)
                    {
                        string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                        int ib = 0;
                        foreach (DataRow m in dtsmaxdate.Rows)
                        {
                            arrbasedata[ib] = m["Date"].ToString();
                            ib++;
                        }
                        smaxdate = buildmaxdate(arrbasedata);
                    }
                    else
                    {
                        dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                        smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
                    }
                    DataTable basetabledata = Utilities.GetTable("select ProposalHour from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                    if (basetabledata.Rows.Count > 0)
                    {
                        staterun = basetabledata.Rows[0][0].ToString();
                    }

                    /////////////////////////////////////////////////////////////
                    if (PowerPrice.Rows.Count > 0)
                    {
                        foreach (DataGridViewColumn DC in MRCurGrid1.Columns)
                        {
                            if ((DC.Index > 0) && (MRCurGrid1.Rows[0].Cells[DC.Index].Value != null))//.ToString().Trim()!=""))
                            {

                                double required = MyDoubleParse(MRCurGrid1.Rows[0].Cells[DC.Index].Value.ToString().Trim());
                                double DispatchM005 = MyDoubleParse(MRCurGrid1.Rows[1].Cells[DC.Index].Value.ToString().Trim());
                                double Economic = MyDoubleParse(MRCurGrid1.Rows[4].Cells[DC.Index].Value.ToString().Trim());
                                //////////////////////////////////change require////////////////////////////////////////////
                                try
                                {

                                    DataTable dsd = Utilities.GetTable("select h" + DC.Index + " from consumed where PPID='" + PPID + "' and unit='" + temp + "'and '" + MRCal.Text.Trim() + "'between fromdate and todate");
                                    double hub = 0;
                                    double consume = 0;
                                    if (dsd.Rows.Count > 0)
                                    {
                                        consume = MyDoubleParse(dsd.Rows[0][0].ToString());
                                    }
                                    //////////////////////////
                                    int mn = int.Parse(MRCal.Text.Trim().Substring(5, 2).ToString());
                                    DataTable zx = Utilities.GetTable("select H" + ((DC.Index).ToString()) + " from dbo.bourse where fromdate<='" + MRCal.Text + "' and todate>='" + MRCal.Text + "' order by fromdate,id desc");
                                    DataTable dsd1 = null;
                                    if (zx.Rows.Count > 0)
                                    {
                                        string c = zx.Rows[0][0].ToString().Trim();
                                        if (c == "Medium")
                                        {
                                            dsd1 = Utilities.GetTable("select M" + mn + "mid from yeartrans where year='" + MRCal.Text.Trim().Substring(0, 4).Trim() + "'and ppid='" + PPID + "'");
                                        }
                                        else if (c == "Base")
                                        {
                                            dsd1 = Utilities.GetTable("select M" + mn + "Low from yeartrans where year='" + MRCal.Text.Trim().Substring(0, 4).Trim() + "'and ppid='" + PPID + "'");
                                        }
                                        else if (c == "peak")
                                        {
                                            dsd1 = Utilities.GetTable("select M" + mn + "Peak from yeartrans where year='" + MRCal.Text.Trim().Substring(0, 4).Trim() + "'and ppid='" + PPID + "'");
                                        }
                                        hub = MyDoubleParse(dsd1.Rows[0][0].ToString());

                                    }
                                    if (((1 - (consume / 100)) * (1 - (hub / 100))) > 0)
                                    {
                                        required = (required) * ((1 - (consume / 100)) * (1 - (hub / 100)));
                                        Economic = (Economic) * ((1 - (consume / 100)) * (1 - (hub / 100)));

                                    }
                                }
                                catch
                                {

                                }

                                ////////////////////////////////////////////////////////////////////////////////////////






                                double MaxBid = 0;
                                double sub = 0;

                                double statusquantity = required;
                                if (staterun == "Economic")
                                {
                                    //statusquantity = MyDoubleParse(MRCurGrid1.Rows[4].Cells[DC.Index].Value.ToString().Trim()); 
                                    statusquantity = Economic;
                                }
                                if (staterun == "Maximum")
                                {
                                    //if (MyDoubleParse(MRCurGrid1.Rows[4].Cells[DC.Index].Value.ToString().Trim()) > MyDoubleParse(MRCurGrid1.Rows[0].Cells[DC.Index].Value.ToString().Trim()))
                                    //    statusquantity = MyDoubleParse(MRCurGrid1.Rows[4].Cells[DC.Index].Value.ToString().Trim());
                                    //else
                                    //    statusquantity = MyDoubleParse(MRCurGrid1.Rows[0].Cells[DC.Index].Value.ToString().Trim());
                                    if (Economic > required)
                                    {
                                        statusquantity = Economic;
                                    }
                                    else
                                    {
                                        statusquantity = required;
                                    }
                                }


                                //if (statusquantity != 0)
                                {
                                    /////////// find the last non zero step of power
                                    double power = 0;
                                    int s = 10;
                                    for (s = 18; s > 0; s = s - 2)
                                    {
                                        string temp1 = PowerPrice.Rows[DC.Index - 1][s].ToString().Trim();
                                        if (temp1 != "" && temp1 != "0")
                                        {
                                            power = double.Parse(temp1);
                                            break;
                                        }
                                    }
                                    ////////////////////////////////////////////////
                                    if ((statusquantity > power) && (s > 0) && (statusquantity != 0))
                                    {
                                        sub = 0;
                                    }

                                    DataRow MaxRow = MaxDS.Tables[0].Rows[DC.Index - 1];

                                    //Math.Round((MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero)
                                    int Dsindex = 1;
                                    while ((Dsindex <= 10) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString())  < Math.Ceiling(statusquantity-0.2) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) != 0.0)))
                                        Dsindex++;
                                    if (Dsindex == 11)
                                        MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                                    else if (Dsindex == 1)
                                        MaxBid = MyDoubleParse(MaxRow["price" + Dsindex.ToString()].ToString());
                                    else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) == 0)
                                        MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                                    else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) != 0)
                                        MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex - 0).ToString()].ToString());
                                    else
                                        MaxBid = 0;


                                    ////////////////////////////next price////////////////////////
                                    //if ((Dsindex <= 10) && (Dsindex != 1))
                                    //{
                                    //    if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex - 1).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex).ToString()].ToString()) != 0.0))

                                    //        MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex).ToString()].ToString());
                                    //}
                                    //else if ((Dsindex == 1))
                                    //{
                                    //    if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()].ToString()) != 0.0))
                                    //        MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex + 1).ToString()].ToString());
                                    //}

                                    ////////////////////////////////////////////////////////////////////




                                    //if (statusquantity < MyDoubleParse(MaxDS.Tables[0].Rows[DC.Index - 1]["Power1"].ToString()) && statusquantity > 0.99)
                                    //{
                                    //    MaxBid = MyDoubleParse(MaxDS.Tables[0].Rows[DC.Index - 1]["Price1"].ToString());

                                    //}

                                    //if (MaxBid == 0 && s > 0)
                                    //{

                                    //    MaxBid = MyDoubleParse(MaxDS.Tables[0].Rows[DC.Index - 1]["Price1"].ToString());
                                    //}


                                    ///////////////////////test

                                    MRCurGrid1.Rows[3].Cells[DC.Index].Value = Convert.ToString(MaxBid);


                                    //else
                                    //{
                                    //    /////////// find the last non zero step of power
                                    //    //double power = 0;
                                    //    //int s = 10;
                                    //    //for (s = 18; s > 0; s = s - 2)
                                    //    //{
                                    //    //    string temp1 = PowerPrice.Rows[DC.Index - 1][s].ToString().Trim();
                                    //    //    if (temp1 != "" && temp1 != "0")
                                    //    //    {
                                    //    //        power = double.Parse(temp1);
                                    //    //        break;
                                    //    //    }
                                    //    //}
                                    //    if (required > power && s > 0)
                                    //        MRCurGrid1.Rows[3].Cells[DC.Index].Value = PowerPrice.Rows[DC.Index - 1][s + 1].ToString().Trim();
                                    //    else MRCurGrid1.Rows[3].Cells[DC.Index].Value = "0";

                                    //}

                                    if (MRCurGrid1.Rows[3].Cells[DC.Index].Value.ToString() != "0")
                                        MaxBid = MyDoubleParse(MRCurGrid1.Rows[3].Cells[DC.Index].Value.ToString());
                                }
                                // else MRCurGrid1.Rows[3].Cells[DC.Index].Value = "0";

                                //Insert MaxBid Into DetailFRM005
                                if (MaxBid != PreMaxBid[DC.Index - 1])
                                {
                                    SqlCommand MyCom = new SqlCommand();
                                    MyCom.Connection = myConnection;
                                    MyCom.CommandText = "UPDATE " + tname + " SET MaxBid=@mb WHERE TargetMarketDate=@date AND PPID=@num " +
                                    "AND Block=@block and PPType='" + ptypenum + "' AND Hour=@hour ";
                                    MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                                    MyCom.Parameters["@date"].Value = MRCal.Text;
                                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                                    MyCom.Parameters["@num"].Value = PPID;
                                    MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                                    MyCom.Parameters["@block"].Value = temp;
                                    MyCom.Parameters.Add("@mb", SqlDbType.Real);
                                    MyCom.Parameters["@mb"].Value = MaxBid;
                                    MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                                    MyCom.Parameters["@hour"].Value = DC.Index;

                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception e)
                                    {
                                        string a = e.Message;
                                    }
                                }

                            }
                        }
                    }
                    //Detect MaxBid field in MRCurGrid2

                    if (PowerPrice.Rows.Count > 0)
                    {
                        foreach (DataGridViewColumn DC in MRCurGrid2.Columns)
                        {
                            if ((DC.Index > 0) && (MRCurGrid2.Rows[0].Cells[DC.Index].Value != null))//.ToString().Trim()!=""))
                            {
                                double required = MyDoubleParse(MRCurGrid2.Rows[0].Cells[DC.Index].Value.ToString().Trim());
                                double DispatchM005 = MyDoubleParse(MRCurGrid2.Rows[1].Cells[DC.Index].Value.ToString().Trim());
                                double Economic = MyDoubleParse(MRCurGrid2.Rows[4].Cells[DC.Index].Value.ToString().Trim());
                                //////////////////////////////////change require////////////////////////////////////////////
                                try
                                {

                                    DataTable dsd = Utilities.GetTable("select h" + (DC.Index + 12) + " from consumed where PPID='" + PPID + "' and unit='" + temp + "'and '" + MRCal.Text.Trim() + "'between fromdate and todate");
                                    double hub = 0;
                                    double consume = 0;
                                    if (dsd.Rows.Count > 0)
                                    {
                                        consume = MyDoubleParse(dsd.Rows[0][0].ToString());
                                    }
                                    //////////////////////////
                                    int mn = int.Parse(MRCal.Text.Trim().Substring(5, 2).ToString());
                                    DataTable zx = Utilities.GetTable("select H" + ((DC.Index + 12).ToString()) + " from dbo.bourse where fromdate<='" + MRCal.Text + "' and todate>='" + MRCal.Text + "' order by fromdate,id desc");
                                    DataTable dsd1 = null;
                                    if (zx.Rows.Count > 0)
                                    {
                                        string c = zx.Rows[0][0].ToString().Trim();
                                        if (c == "Medium")
                                        {
                                            dsd1 = Utilities.GetTable("select M" + mn + "mid from yeartrans where year='" + MRCal.Text.Trim().Substring(0, 4).Trim() + "'and ppid='" + PPID + "'");
                                        }
                                        else if (c == "Base")
                                        {
                                            dsd1 = Utilities.GetTable("select M" + mn + "Low from yeartrans where year='" + MRCal.Text.Trim().Substring(0, 4).Trim() + "'and ppid='" + PPID + "'");
                                        }
                                        else if (c == "peak")
                                        {
                                            dsd1 = Utilities.GetTable("select M" + mn + "Peak from yeartrans where year='" + MRCal.Text.Trim().Substring(0, 4).Trim() + "'and ppid='" + PPID + "'");
                                        }
                                        hub = MyDoubleParse(dsd1.Rows[0][0].ToString());

                                    }
                                    if (((1 - (consume / 100)) * (1 - (hub / 100))) > 0)
                                    {
                                        required = (required) * ((1 - (consume / 100)) * (1 - (hub / 100)));
                                        Economic = (Economic) * ((1 - (consume / 100)) * (1 - (hub / 100)));

                                    }
                                }
                                catch
                                {

                                }

                                ////////////////////////////////////////////////////////////////////////////////////////





                                double MaxBid = 0;
                                double sub = 0;
                                double statusquantity = required;
                                if (staterun == "Economic")
                                {
                                    //statusquantity = MyDoubleParse(MRCurGrid1.Rows[4].Cells[DC.Index].Value.ToString().Trim()); 
                                    statusquantity = Economic;
                                }
                                if (staterun == "Maximum")
                                {
                                    //if (MyDoubleParse(MRCurGrid1.Rows[4].Cells[DC.Index].Value.ToString().Trim()) > MyDoubleParse(MRCurGrid1.Rows[0].Cells[DC.Index].Value.ToString().Trim()))
                                    //    statusquantity = MyDoubleParse(MRCurGrid1.Rows[4].Cells[DC.Index].Value.ToString().Trim());
                                    //else
                                    //    statusquantity = MyDoubleParse(MRCurGrid1.Rows[0].Cells[DC.Index].Value.ToString().Trim());
                                    if (Economic > required)
                                    {
                                        statusquantity = Economic;
                                    }
                                    else
                                    {
                                        statusquantity = required;
                                    }
                                }

                                //if (statusquantity != 0)
                                {

                                    /////////// find the last non zero step of power
                                    double power = 0;
                                    int s = 10;
                                    for (s = 18; s > 0; s = s - 2)
                                    {
                                        string temp1 = PowerPrice.Rows[DC.Index + 11][s].ToString().Trim();
                                        if (temp1 != "" && temp1 != "0")
                                        {
                                            power = double.Parse(temp1);
                                            break;
                                        }
                                    }

                                    if ((statusquantity > power) && (s > 0) && (statusquantity != 0))
                                    {
                                        sub = 0;
                                    }


                                    ///////////////////////////////////////////////////////
                                    DataRow MaxRow = MaxDS.Tables[0].Rows[DC.Index + 11];//pmousavi-97/03/31


                                    int Dsindex = 1;
                                    while ((Dsindex <= 10) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString())  < Math.Ceiling(statusquantity-0.2) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) != 0.0)))
                                        Dsindex++;
                                    if (Dsindex == 11)
                                        MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                                    else if (Dsindex == 1)
                                        MaxBid = MyDoubleParse(MaxRow["price" + Dsindex.ToString()].ToString());
                                    else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) == 0)
                                        MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                                    else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) != 0)
                                        MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex - 0).ToString()].ToString());
                                    else
                                        MaxBid = 0;




                                    //////////////////////////////next price////////////////////////
                                    //if ((Dsindex <= 10) && (Dsindex != 1))
                                    //{
                                    //    if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex - 1).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex).ToString()].ToString()) != 0.0))

                                    //        MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex).ToString()].ToString());
                                    //}
                                    //else if ((Dsindex == 1))
                                    //{
                                    //    if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()].ToString()) != 0.0))
                                    //        MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex + 1).ToString()].ToString());
                                    //}

                                    //////////////////////////////////////////////////////////////////////


                                    //if (statusquantity < MyDoubleParse(MaxDS.Tables[0].Rows[DC.Index + 11]["Power1"].ToString()) && statusquantity > 3.0)
                                    //{
                                    //    MaxBid = MyDoubleParse(MaxDS.Tables[0].Rows[DC.Index + 11]["Price1"].ToString());

                                    //}

                                    //if (MaxBid == 0 && s > 0)
                                    //{

                                    //    MaxBid = MyDoubleParse(MaxDS.Tables[0].Rows[DC.Index + 11]["Price1"].ToString());
                                    //}


                                    ///////////////////////test

                                    MRCurGrid2.Rows[3].Cells[DC.Index].Value = Convert.ToString(MaxBid);


                                    //////////////////////////////////////////////////////////////////////


                                    //int Dsindex = 0;
                                    //while ((Dsindex < 20) && (Math.Round(double.Parse(PowerPrice.Rows[DC.Index + 11][Dsindex].ToString().Trim()), 1) < statusquantity)) Dsindex = Dsindex + 2;
                                    //if (Dsindex < 20)
                                    //    MRCurGrid2.Rows[3].Cells[DC.Index].Value = PowerPrice.Rows[DC.Index + 11][Dsindex + 1].ToString().Trim();
                                    //else
                                    //{
                                    //    /////////// find the last non zero step of power
                                    //    double power = 0;
                                    //    int s = 10;
                                    //    for (s = 18; s > 0; s = s - 2)
                                    //    {
                                    //        string temp1 = PowerPrice.Rows[DC.Index + 11][s].ToString().Trim();
                                    //        if (temp1 != "" && temp1 != "0")
                                    //        {
                                    //            power = double.Parse(temp1);
                                    //            break;
                                    //        }
                                    //    }
                                    //    if (required > power && s > 0)
                                    //        MRCurGrid2.Rows[3].Cells[DC.Index].Value = PowerPrice.Rows[DC.Index + 11][s + 1].ToString().Trim();
                                    //    else MRCurGrid2.Rows[3].Cells[DC.Index].Value = "0";

                                    //}

                                    if (MRCurGrid2.Rows[3].Cells[DC.Index].Value.ToString() != "0")
                                        MaxBid = MyDoubleParse(MRCurGrid2.Rows[3].Cells[DC.Index].Value.ToString());
                                }
                                // else MRCurGrid2.Rows[3].Cells[DC.Index].Value = "0";
                                //Insert MaxBid Into DetailFRM005
                                if (MaxBid != PreMaxBid[DC.Index + 11])
                                {
                                    SqlCommand MyCom = new SqlCommand();
                                    MyCom.Connection = myConnection;
                                    MyCom.CommandText = "UPDATE " + tname + " SET MaxBid=@mb WHERE TargetMarketDate=@date AND PPID=@num " +
                                    "AND Block=@block and PPType='" + ptypenum + "' AND Hour=@hour ";
                                    MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                                    MyCom.Parameters["@date"].Value = MRCal.Text;
                                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                                    MyCom.Parameters["@num"].Value = PPID;
                                    MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                                    MyCom.Parameters["@block"].Value = temp;
                                    MyCom.Parameters.Add("@mb", SqlDbType.Real);
                                    MyCom.Parameters["@mb"].Value = MaxBid;
                                    MyCom.Parameters.Add("@hour", SqlDbType.Real);
                                    MyCom.Parameters["@hour"].Value = DC.Index + 12;

                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception e)
                                    {
                                        string a = e.Message;
                                    }
                                }

                            }
                        }
                    }
                }

            }
            myConnection.Close();



             
        }

        //1397//04/03
        private void btnToExcelMR_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.FileName = String.Format("MarketResult_{0}_{1}",MRUnitLb.Text, MRCal.Text.Replace("/", ""));
            saveFileDialog.Filter = "Excel files (*.xls)|";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = saveFileDialog.FileName + ".xls";
                try
                {
                    ExcelUtility.ExportMarketResultGrid(MRCurGrid1, MRCurGrid2, fileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        //---------------------------------FillMRPlantGrid--------------------------------
        private void FillMRPlantGrid()
        {
            string tname = "[DetailFRM005]";
            if (rdbam005ba.Checked) tname = "[baDetailFRM005]";


            if (rbrealplant.Checked)
            {
                MRCurGrid1.DataSource = null;
                MRCurGrid2.DataSource = null;
                if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
                if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
                if (!MRCal.IsNull)
                {

                    DataSet MyDS = new DataSet();
                    SqlDataAdapter Myda = new SqlDataAdapter();
                    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                    myConnection.Open();

                    Myda.SelectCommand = new SqlCommand("SELECT Hour,SUM(Required) AS SR ,SUM(economic)as ec  FROM " + tname + " WHERE PPID= " + PPID + " AND TargetMarketDate=@date GROUP BY Hour SELECT Hour,SUM(P) AS P  FROM DetailFRM009 WHERE PPID= " + PPID + " AND TargetMarketDate=@date GROUP BY Hour", myConnection);
                    Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                    Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                    Myda.Fill(MyDS);
                    myConnection.Close();


                    MRCurGrid1.DataSource = MyDS.Tables[0].DefaultView;
                    MRCurGrid2.DataSource = MyDS.Tables[1].DefaultView;



                    MRCurGrid1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    MRCurGrid2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    if ((MRCurGrid1.RowCount > 1) || (MRCurGrid2.RowCount > 1))
                    {
                        if (MRCurGrid1.RowCount > 1) MRCurGrid2.DataSource = MyDS.Tables[0].DefaultView;
                        if (MRCurGrid2.RowCount > 1) MRCurGrid1.DataSource = MRCurGrid2.DataSource;


                        //MRCurGrid1.Rows[0].Cells[0].Value = "M005";
                        //MRCurGrid2.Rows[0].Cells[0].Value = "M005";

                        MRCurGrid1.Rows[0].Cells[0].Value = "Require";
                        MRCurGrid2.Rows[0].Cells[0].Value = "Require";



                        MRCurGrid1.Rows[1].Cells[0].Value = "Real";
                        MRCurGrid2.Rows[1].Cells[0].Value = "Real";



                        MRCurGrid1.Rows[2].Cells[0].Value = "Economic";
                        MRCurGrid2.Rows[2].Cells[0].Value = "Economic";

                    }

                    foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                    {
                        int index = int.Parse(MyRow["Hour"].ToString().Trim());
                        if (index <= 12)
                            MRCurGrid1.Rows[0].Cells[index].Value = MyRow["SR"];
                        else
                            MRCurGrid2.Rows[0].Cells[index - 12].Value = MyRow["SR"];
                    }


                    foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                    {
                        int index = int.Parse(MyRow["Hour"].ToString().Trim());
                        if (index <= 12)
                            MRCurGrid1.Rows[2].Cells[index].Value = MyRow["ec"];
                        else
                            MRCurGrid2.Rows[2].Cells[index - 12].Value = MyRow["ec"];
                    }

                    foreach (DataRow MyRow in MyDS.Tables[1].Rows)
                    {
                        int index = int.Parse(MyRow["Hour"].ToString().Trim()) + 1;
                        if (index <= 12)
                            MRCurGrid1.Rows[1].Cells[index].Value = MyRow["P"];
                        else
                            MRCurGrid2.Rows[1].Cells[index - 12].Value = MyRow["P"];
                    }

                    for (int i = (MRCurGrid1.RowCount - 2); i > 2; i--)
                        MRCurGrid1.Rows.Remove(MRCurGrid1.Rows[i]);
                    for (int i = (MRCurGrid2.RowCount - 2); i > 2; i--)
                        MRCurGrid2.Rows.Remove(MRCurGrid2.Rows[i]);




                    if (MyDS.Tables[0].Rows.Count == 0)
                        MRPlotBtn.Enabled = false;
                    else
                        MRPlotBtn.Enabled = true;

                }
            }
            else if(rbestimateplant.Checked)
            {

                FillMRPlantGridEstimated();

            }
        }
        private void FillMRPlantGridEstimated()
        {
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            if (!MRCal.IsNull)
            {

                
              DataTable powertable=Utilities.GetTable("select * from dbo.PowerForecast where date='"+MRCal.Text+"' and gencode='"+ PPID +"' order by Id desc");
           

                MRCurGrid1.Rows[0].Cells[0].Value = "Power Forecast";
                MRCurGrid2.Rows[0].Cells[0].Value = "Power Forecast";

                MRCurGrid1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                MRCurGrid2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                if (powertable.Rows.Count>0)
                {
                    for (int i = 3; i <= 14; i++)
                    {                        
                        MRCurGrid1.Rows[0].Cells[i-2].Value = powertable.Rows[0][i];
                        
                    }
                    int index1=15;
                    for (int j = 1; j <=12; j++)
                    {
                       
                        MRCurGrid2.Rows[0].Cells[j].Value = powertable.Rows[0][index1];

                        index1++;
                    }
                }
                if (powertable.Rows.Count == 0)
                    MRPlotBtn.Enabled = false;
                else
                    MRPlotBtn.Enabled = true;

            }
        }
        private void FillMRCombine(string name, string plantname)
        {

            string tname = "[DetailFRM005]";
            if (rdbam005ba.Checked) tname = "[baDetailFRM005]";

            MRPlotBtn.Enabled = false;
            mrbtnpprint.Enabled = false;
            btnToExcelMR.Enabled = false;

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            try
            {
                if (!MRCal.IsNull)
                {


                    //ccunitbase///////////////////////////////////////////
                    ////Detect Block For FRM005
                    string ptypenum = "1";
                    //if (PPID == "232") ptypenum = "0";
                    if (Findcconetype(PPID)) ptypenum = "0";

                    DataTable df = Utilities.GetTable("select * from dbo.PPUnit where PPID=(select PPID from dbo.PowerPlant where PPName='" + plantname + "')");


                    //string temp = name.Replace("Combined Cycle", "C");
                    string bid = "";
                    if (df.Rows.Count > 1)
                    {
                        bid = (int.Parse(PPID.ToString()) + 1).ToString();
                    }
                    else
                    {
                        bid = (int.Parse(PPID.ToString())).ToString();
                    }



                    string packnum = name.Replace("Combined Cycle", "");


                    DataTable dunit = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" + int.Parse(PPID.ToString()) + "' and PackageType='cc' and PackageCode='" + packnum + "'");



                    double[] sumrequ = new double[24];
                    double[] sumdis = new double[24];
                    double[] sumcont = new double[24];
                    double[] sumeco = new double[24];

                    foreach (DataRow m in dunit.Rows)
                    {


                        string temp = m["UnitCode"].ToString().Trim().ToLower();
                        temp = temp.Replace("cc", "c");
                        string[] sp = temp.Split('c');
                        temp = sp[0].Trim() + sp[1].Trim();
                        if (temp.Contains("gas"))
                        {
                            temp = temp.Replace("gas", "G");

                        }
                        else
                        {
                            temp = temp.Replace("steam", "S");

                        }
                        temp = bid + "-" + temp;

                        //Read Required,Dispatchable,Contribution From M005
                        //DataSet MyDS = new DataSet();
                        //SqlDataAdapter Myda = new SqlDataAdapter();
                        //Myda.SelectCommand = new SqlCommand("SELECT Hour,Required,Dispatchable,Contribution,Economic FROM " +
                        //"[DetailFRM005] WHERE PPID= " + PPID + " AND TargetMarketDate=@date AND  Block=@block ", myConnection);
                        //Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                        //Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                        //Myda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
                        //Myda.SelectCommand.Parameters["@block"].Value = temp;
                        //Myda.Fill(MyDS);
                        DataTable dt5 = Utilities.GetTable("SELECT Hour,Required,Dispatchable,Contribution,Economic FROM " +
                        ""+tname+" WHERE PPID= " + PPID + " AND TargetMarketDate='" + MRCal.Text.Trim() + "' AND  Block='" + temp + "' and PPType='" + ptypenum + "'");
                        int m1 = 0;

                        foreach (DataRow mu in dt5.Rows)
                        {
                            if (m1 <= 23)
                            {
                                sumrequ[m1] += MyDoubleParse(mu["Required"].ToString());
                                sumdis[m1] += MyDoubleParse(mu["Dispatchable"].ToString());
                                sumcont[m1] += MyDoubleParse(mu["Contribution"].ToString());
                                sumeco[m1] += MyDoubleParse(mu["Economic"].ToString());
                                m1++;
                            }
                        }
                    }
                    /////////////new khorsand//////////////////
                    DataTable if005null = Utilities.GetTable("SELECT Hour,Required,Dispatchable,Contribution,Economic FROM " +
                      ""+tname+" WHERE PPID= " + PPID + " AND TargetMarketDate='" + MRCal.Text + "'and PPType='" + ptypenum + "'");
                    if (if005null.Rows.Count < 1) return;

                    //MRCurGrid1.DataSource = MyDS.Tables[0].DefaultView;
                    //MRCurGrid2.DataSource = MyDS.Tables[0].DefaultView;
                    MRCurGrid1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    MRCurGrid2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                    MRCurGrid1.RowCount = 24;
                    MRCurGrid2.RowCount = 24;
                    //////////////////////////////////////
                    if ((MRCurGrid1.RowCount > 1) || (MRCurGrid2.RowCount > 1))
                    {


                        MRCurGrid1.Rows[0].Cells[0].Value = "Required";
                        MRCurGrid2.Rows[0].Cells[0].Value = "Required";
                        MRCurGrid1.Rows[1].Cells[0].Value = "Dispatchable";
                        MRCurGrid2.Rows[1].Cells[0].Value = "Dispatchable";
                        MRCurGrid1.Rows[2].Cells[0].Value = "Contribution";
                        MRCurGrid2.Rows[2].Cells[0].Value = "Conrtibution";
                        MRCurGrid1.Rows[3].Cells[0].Value = "Price";
                        MRCurGrid2.Rows[3].Cells[0].Value = "Price";
                        MRCurGrid1.Rows[4].Cells[0].Value = "Economic";
                        MRCurGrid2.Rows[4].Cells[0].Value = "Economic";
                        MRCurGrid1.Rows[5].Cells[0].Value = "Power";
                        MRCurGrid2.Rows[5].Cells[0].Value = "Power";
                        MRCurGrid1.Rows[6].Cells[0].Value = "QC";
                        MRCurGrid2.Rows[6].Cells[0].Value = "QC";
                        MRCurGrid1.Rows[7].Cells[0].Value = "QL";
                        MRCurGrid2.Rows[7].Cells[0].Value = "QL";
                        MRCurGrid1.Rows[8].Cells[0].Value = "Consumed";
                        MRCurGrid2.Rows[8].Cells[0].Value = "Consumed";

                        //Fill four Rows of DataGridView (Require,Dispachable,Contribution,Economic)

                        for (int x = 1; x <= 24; x++)
                        {
                            // int x = int.Parse(MyRow["Hour"].ToString());
                            if (x <= 12)
                            {
                                MRCurGrid1.Rows[0].Cells[x].Value = sumrequ[x - 1];
                                MRCurGrid1.Rows[1].Cells[x].Value = sumdis[x - 1];
                                MRCurGrid1.Rows[2].Cells[x].Value = sumcont[x - 1];
                                MRCurGrid1.Rows[4].Cells[x].Value = sumeco[x - 1];
                            }
                            else
                            {
                                MRCurGrid2.Rows[0].Cells[x - 12].Value = sumrequ[x - 1];
                                MRCurGrid2.Rows[1].Cells[x - 12].Value = sumdis[x - 1];
                                MRCurGrid2.Rows[2].Cells[x - 12].Value = sumcont[x - 1];
                                MRCurGrid2.Rows[4].Cells[x - 12].Value = sumeco[x - 1];
                            }

                        }

                        //Fill three Rows of DataGridView (P,QC,QL,Consumed)
                        for (int h = 0; h < 24; h++)
                        {
                            DataTable dd = Utilities.GetTable("SELECT sum(P)as P,sum(QC)as QC,sum(QL) as QL,sum(Consumed)as Consumed FROM DetailFRM009 WHERE PPID= " + PPID + " AND TargetMarketDate='" + MRCal.Text.Trim() + "' and Hour='" + (h) + "'and Block like '%cc%' and PackageCode='" + packnum + "'");

                            if (h < 12)
                            {
                                MRCurGrid1.Rows[5].Cells[h + 1].Value = dd.Rows[0]["P"].ToString();
                                MRCurGrid1.Rows[6].Cells[h + 1].Value = dd.Rows[0]["QC"].ToString();
                                MRCurGrid1.Rows[7].Cells[h + 1].Value = dd.Rows[0]["QL"].ToString();
                                MRCurGrid1.Rows[8].Cells[h + 1].Value = dd.Rows[0]["Consumed"].ToString();
                            }
                            else
                            {
                                MRCurGrid2.Rows[5].Cells[h - 11].Value = dd.Rows[0]["P"].ToString();
                                MRCurGrid2.Rows[6].Cells[h - 11].Value = dd.Rows[0]["QC"].ToString();
                                MRCurGrid2.Rows[7].Cells[h - 11].Value = dd.Rows[0]["QL"].ToString();
                                MRCurGrid2.Rows[8].Cells[h - 11].Value = dd.Rows[0]["Consumed"].ToString();
                            }

                        }



                        for (int i = (MRCurGrid1.RowCount - 2); i > 8; i--)
                            MRCurGrid1.Rows.Remove(MRCurGrid1.Rows[i]);
                        for (int i = (MRCurGrid2.RowCount - 2); i > 8; i--)
                            MRCurGrid2.Rows.Remove(MRCurGrid2.Rows[i]);

                        //Read Power And Price From M002

                        DataSet MaxDS = new DataSet();
                        SqlDataAdapter Maxda = new SqlDataAdapter();
                        Maxda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                        "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10,Hour FROM [DetailFRM002] " +
                        "WHERE Estimated<>1 AND TargetMarketDate=@date AND Block=@ptype  and PPType ='" + ptypenum + "'AND PPID=" + PPID, myConnection);
                        Maxda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                        Maxda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                        //Detect Block For FRM002

                        //ccunitbase///////////////////////////////////////////
                        string ptype = "Steamcc" + packnum;
                        ptype = ptype.Replace("cc", "c").ToLower();
                        string[] sp = ptype.Split('c');
                        ptype = sp[0].Trim() + sp[1].Trim();
                        if (ptype.Contains("gas"))
                            ptype = ptype.Replace("gas", "G");
                        else if (ptype.Contains("steam"))
                            ptype = ptype.Replace("steam", "S");

                        //----------------------------------------------------------

                        Maxda.SelectCommand.Parameters.Add("@ptype", SqlDbType.NChar, 20);
                        Maxda.SelectCommand.Parameters["@ptype"].Value = ptype;
                        Maxda.Fill(MaxDS);
                        DataTable PowerPrice = new DataTable();
                        PowerPrice = MaxDS.Tables[0];

                        //Read Previous MaxBid
                        DataSet BidDS = new DataSet();
                        SqlDataAdapter Bidda = new SqlDataAdapter();
                        Bidda.SelectCommand = new SqlCommand("SELECT MaxBid,Hour FROM "+tname+" WHERE TargetMarketDate" +
                        "=@date AND PPID=@num and PPType='" + ptypenum + "'AND Block=@block", myConnection);
                        Bidda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                        Bidda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                        Bidda.SelectCommand.Parameters.Add("@num", SqlDbType.NChar, 10);
                        Bidda.SelectCommand.Parameters["@num"].Value = PPID;
                        Bidda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
                        Bidda.SelectCommand.Parameters["@block"].Value = bid + "-" + ptype;
                        Bidda.Fill(BidDS);
                        double[] PreMaxBid = new double[24];
                        for (int i = 0; i < 24; i++)
                            PreMaxBid[i] = 0;
                        foreach (DataRow BidRow in BidDS.Tables[0].Rows)
                        {
                            if (BidRow[1].ToString() != "")
                            {
                                int myhour = int.Parse(BidRow[1].ToString()) - 1;
                                if (BidRow[0].ToString() != "")
                                    PreMaxBid[myhour] = double.Parse(BidRow[0].ToString());
                            }
                        }

                        //Detect MaxBid field in MRCurGrid1
                        //////////////////////////////////////////////////////////////////////////
                        // new 910820
                        DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + MRCal.Text + "'order by BaseID desc");
                        string staterun = "";
                        if (dtsmaxdate.Rows.Count > 0)
                        {
                            string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                            int ib = 0;
                            foreach (DataRow m in dtsmaxdate.Rows)
                            {
                                arrbasedata[ib] = m["Date"].ToString();
                                ib++;
                            }
                            smaxdate = buildmaxdate(arrbasedata);
                        }
                        else
                        {
                            dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                            smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
                        }
                        DataTable basetabledata = Utilities.GetTable("select ProposalHour from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                        if (basetabledata.Rows.Count > 0)
                        {
                            staterun = basetabledata.Rows[0][0].ToString();
                        }
                        // new 910820
                        if (PowerPrice.Rows.Count > 0)
                        {
                            foreach (DataGridViewColumn DC in MRCurGrid1.Columns)
                            {
                                if ((DC.Index > 0) && (MRCurGrid1.Rows[0].Cells[DC.Index].Value != null))//.ToString().Trim()!=""))
                                {

                                    double required = MyDoubleParse(MRCurGrid1.Rows[0].Cells[DC.Index].Value.ToString().Trim());
                                    double DispatchM005 = MyDoubleParse(MRCurGrid1.Rows[1].Cells[DC.Index].Value.ToString().Trim());
                                    double MaxBid = 0;
                                    double sub = 0;
                                    double statusquantity = required;
                                    if (staterun == "Economic")
                                    {
                                        statusquantity = MyDoubleParse(MRCurGrid1.Rows[4].Cells[DC.Index].Value.ToString().Trim());
                                    }
                                    if (staterun == "Maximum")
                                    {
                                        if (MyDoubleParse(MRCurGrid1.Rows[4].Cells[DC.Index].Value.ToString().Trim()) > MyDoubleParse(MRCurGrid1.Rows[0].Cells[DC.Index].Value.ToString().Trim()))
                                            statusquantity = MyDoubleParse(MRCurGrid1.Rows[4].Cells[DC.Index].Value.ToString().Trim());
                                        else
                                            statusquantity = MyDoubleParse(MRCurGrid1.Rows[0].Cells[DC.Index].Value.ToString().Trim());
                                    }
                                    
                                    //if (statusquantity != 0)
                                    {
                                        /////////// find the last non zero step of power
                                        double power = 0;
                                        int s = 10;
                                        for (s = 18; s > 0; s = s - 2)
                                        {
                                            string temp1 = PowerPrice.Rows[DC.Index - 1][s].ToString().Trim();
                                            if (temp1 != "" && temp1 != "0")
                                            {
                                                power = double.Parse(temp1);
                                                break;
                                            }
                                        }
                                        ////////////////////////////////////////////////
                                        if ((statusquantity > power) && (s > 0) && (statusquantity != 0))
                                        {
                                            sub = DispatchM005 - power;
                                        }

                                        DataRow MaxRow = MaxDS.Tables[0].Rows[DC.Index - 1];


                                        int Dsindex = 1;
                                        while ((Dsindex <= 10) &&(MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString())  < Math.Ceiling(statusquantity-0.2) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) != 0.0)))
                                            Dsindex++;
                                        if(Dsindex == 11)                                           
                                            MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                                        else if (Dsindex == 1)
                                            MaxBid = MyDoubleParse(MaxRow["price" + Dsindex.ToString()].ToString());
                                        else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) == 0)
                                            MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());  
                                        else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) != 0)
                                            MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex - 0).ToString()].ToString()); 
                                        else
                                            MaxBid = 0;

                                        ////////////////////////////next price////////////////////////
                                        if ((Dsindex <= 10) && (Dsindex != 1))
                                        {
                                            if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex - 1).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex).ToString()].ToString()) != 0.0))

                                                MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex).ToString()].ToString());
                                        }
                                        else if ((Dsindex == 1))
                                        {
                                            if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()].ToString()) != 0.0))
                                                MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex + 1).ToString()].ToString());
                                        }

                                        ////////////////////////////////////////////////////////////////////


                                        if (statusquantity < MyDoubleParse(MaxDS.Tables[0].Rows[DC.Index - 1]["Power1"].ToString()) && statusquantity > 3.0)
                                        {
                                            MaxBid = MyDoubleParse(MaxDS.Tables[0].Rows[DC.Index - 1]["Price1"].ToString());

                                        }

                                        if (MaxBid == 0 && s > 0)
                                        {

                                            MaxBid = MyDoubleParse(MaxDS.Tables[0].Rows[DC.Index - 1]["Price1"].ToString());
                                        }


                                        ///////////////////////test

                                        MRCurGrid1.Rows[3].Cells[DC.Index].Value = Convert.ToString(MaxBid);




                                        if (MRCurGrid1.Rows[3].Cells[DC.Index].Value.ToString() != "0")
                                            MaxBid = MyDoubleParse(MRCurGrid1.Rows[3].Cells[DC.Index].Value.ToString());
                                    }
                                    //else MRCurGrid1.Rows[3].Cells[DC.Index].Value = "0";

                                }
                            }
                        }
                        //Detect MaxBid field in MRCurGrid2
                        //////////////////////////////////////////////////////////////////////////
                        if (PowerPrice.Rows.Count > 0)
                        {
                            foreach (DataGridViewColumn DC in MRCurGrid2.Columns)
                            {
                                if ((DC.Index > 0) && (MRCurGrid2.Rows[0].Cells[DC.Index].Value != null))//.ToString().Trim()!=""))
                                {
                                    double required = MyDoubleParse(MRCurGrid2.Rows[0].Cells[DC.Index].Value.ToString().Trim());
                                    double DispatchM005 = MyDoubleParse(MRCurGrid2.Rows[1].Cells[DC.Index].Value.ToString().Trim());
                                    double MaxBid = 0;
                                    double sub = 0;
                                    double statusquantity = required;
                                    if (staterun == "Economic")
                                    {
                                        statusquantity = MyDoubleParse(MRCurGrid2.Rows[4].Cells[DC.Index].Value.ToString().Trim());
                                    }
                                    if (staterun == "Maximum")
                                    {
                                        if (MyDoubleParse(MRCurGrid2.Rows[4].Cells[DC.Index].Value.ToString().Trim()) > MyDoubleParse(MRCurGrid2.Rows[0].Cells[DC.Index].Value.ToString().Trim()))
                                            statusquantity = MyDoubleParse(MRCurGrid2.Rows[4].Cells[DC.Index].Value.ToString().Trim());
                                        else
                                            statusquantity = MyDoubleParse(MRCurGrid2.Rows[0].Cells[DC.Index].Value.ToString().Trim());
                                    }
                                    //if (statusquantity != 0)
                                    {

                                        /////////// find the last non zero step of power
                                        double power = 0;
                                        int s = 10;
                                        for (s = 18; s > 0; s = s - 2)
                                        {
                                            string temp1 = PowerPrice.Rows[DC.Index + 11][s].ToString().Trim();
                                            if (temp1 != "" && temp1 != "0")
                                            {
                                                power = double.Parse(temp1);
                                                break;
                                            }
                                        }

                                        if ((statusquantity > power) && (s > 0) && (statusquantity != 0))
                                        {
                                            sub = DispatchM005 - power;
                                        }


                                        ///////////////////////////////////////////////////////
                                        DataRow MaxRow = MaxDS.Tables[0].Rows[DC.Index + 11];


                                            int Dsindex = 1;
                                            while ((Dsindex <= 10) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString())  < Math.Ceiling(statusquantity-0.2)  && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) != 0.0)))
                                            Dsindex++;
                                            if (Dsindex == 11)
                                                MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                                            else if (Dsindex == 1)
                                                MaxBid = MyDoubleParse(MaxRow["price" + Dsindex.ToString()].ToString());
                                            else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) == 0)
                                                MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                                            else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) != 0)
                                                MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex - 0).ToString()].ToString());
                                            else
                                                MaxBid = 0;


                                        ////////////////////////////next price////////////////////////
                                        if ((Dsindex <= 10) && (Dsindex != 1))
                                        {
                                            if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex - 1).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex).ToString()].ToString()) != 0.0))

                                                MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex).ToString()].ToString());
                                        }
                                        else if ((Dsindex == 1))
                                        {
                                            if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()].ToString()) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()].ToString()) != 0.0))
                                                MaxBid = MyDoubleParse(MaxRow["price" + (Dsindex + 1).ToString()].ToString());
                                        }

                                        ////////////////////////////////////////////////////////////////////


                                        if (statusquantity < MyDoubleParse(MaxDS.Tables[0].Rows[DC.Index + 11]["Power1"].ToString()) && statusquantity > 3.0)
                                        {
                                            MaxBid = MyDoubleParse(MaxDS.Tables[0].Rows[DC.Index + 11]["Price1"].ToString());

                                        }

                                        if (MaxBid == 0 && s > 0)
                                        {

                                            MaxBid = MyDoubleParse(MaxDS.Tables[0].Rows[DC.Index + 11]["Price1"].ToString());
                                        }


                                        ///////////////////////test

                                        MRCurGrid2.Rows[3].Cells[DC.Index].Value = Convert.ToString(MaxBid);


                                        //////////////////////////////////////////////////////////////////////




                                        if (MRCurGrid2.Rows[3].Cells[DC.Index].Value.ToString() != "0")
                                            MaxBid = MyDoubleParse(MRCurGrid2.Rows[3].Cells[DC.Index].Value.ToString());
                                    }
                          



                                }
                            }
                        }
                    }

                }
                myConnection.Close();
            }
            catch
            {

            }

        }
        private void FillMRGrecGrid()
        {
            MRPlotBtn.Enabled = false;
            btnex009.Enabled = false;
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            if (!MRCal.IsNull)
            {

                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                //Myda.SelectCommand = new SqlCommand("select sum( P), sum(QC), sum(QL) from dbo.DetailFRM009 where TargetMarketDate='" + MRCal.Text + "'", myConnection);
                Myda.SelectCommand = new SqlCommand("select sum(P) from dbo.DetailFRM009Post where TargetMarketDate='" + MRCal.Text + "'", myConnection);
                Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                Myda.Fill(MyDS);
                myConnection.Close();

               // MRCurGrid1.DataSource = MyDS.Tables[0].DefaultView;
               // MRCurGrid2.DataSource = MyDS.Tables[0].DefaultView;

                MRCurGrid1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                MRCurGrid2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                MRCurGrid1.RowCount = 3;
                MRCurGrid2.RowCount = 3; 
                 

                MRCurGrid1.Rows[0].Cells[0].Value = "Consume-Ls2";
                MRCurGrid2.Rows[0].Cells[0].Value = "Consume-Ls2";

                MRCurGrid1.Rows[1].Cells[0].Value = "Consume-Total";
                MRCurGrid2.Rows[1].Cells[0].Value = "Consume-Total";

                MRCurGrid1.Rows[2].Cells[0].Value = "Power";
                MRCurGrid2.Rows[2].Cells[0].Value = "Power";


                try
                {

                    //////////////////////////////////row 0/////////////////////////////////////////////////////
                    double[] sub = new double[25];


                    for (int s = 1; s < 25; s++)
                    {

                        DataTable dt = Utilities.GetTable("select sum(P) from dbo.DetailFRM009Post where Hour='" + (s - 1) + "'and TargetMarketDate='" + MRCal.Text.Trim() + "'");
                        if (s <= 12)
                            MRCurGrid1.Rows[0].Cells[s].Value = MyDoubleParse(dt.Rows[0][0].ToString());
                        else
                            MRCurGrid2.Rows[0].Cells[s - 12].Value = MyDoubleParse(dt.Rows[0][0].ToString());

                    }
                    ////////////////////////////////////////row 1/////////////////////////////////////////////////
                    double[] line = new double[24];
                    double[] sub1 = new double[25];
                    int lineTypesNo = Enum.GetNames(typeof(Line_codes)).Length+1;
                    int index = 1;

                    double[] flagcode = new double[lineTypesNo];
                    for (int lnum = 1; lnum <= lineTypesNo-1; lnum++)
                    {
                        Line_codes linename = (Line_codes)Enum.Parse(typeof(Line_codes), lnum.ToString());
                        if (linename.ToString().Trim() == "SZ833")
                        {
                            flagcode[lnum] = +1;
                        }
                        else
                        {
                            flagcode[lnum] = 1;
                        }

                    }


                    for (int h1 = 0; h1 < 24; h1++)
                    {

                        DataTable m009line = Utilities.GetTable("select  P,FlagLine,LineCode from DetailFRM009LINE where  TargetMarketDate='" + MRCal.Text + "' and Hour='" + h1 + "'");
                        //if (m009line.Rows.Count > 0)
                        //{
                        //    line[h1] = double.Parse(m009line.Rows[0][0].ToString()) * double.Parse(m009line.Rows[0][1].ToString());
                        //}

                        double sum = 0.0;
                        foreach (DataRow myrow in m009line.Rows)
                        {
                            bool enter = false;
                            for (int lnum = 1; lnum <= lineTypesNo-1; lnum++)
                            {
                                Line_codes linename = (Line_codes)Enum.Parse(typeof(Line_codes), lnum.ToString());
                                if (myrow["LineCode"].ToString().Trim() == linename.ToString().Trim())
                                {
                                    enter = true;
                                    index = lnum;
                                    break;
                                }
                            }
                            if (enter)
                            {                               
                              sum += (MyDoubleParse(myrow[0].ToString()) * MyDoubleParse(myrow[1].ToString())* flagcode[index]);
                                
                            }
                        }

                        line[h1] = sum;
                    }

                    for (int h = 0; h < 24; h++)
                    {
                        
                        DataTable subpower = Utilities.GetTable("select sum( P) from dbo.DetailFRM009 where  TargetMarketDate='" + MRCal.Text + "' and Hour='" + h + "'");
                        DataTable consval = Utilities.GetTable("select sum(Consumed) from dbo.DetailFRM009 where TargetMarketDate='" + MRCal.Text + "' and Hour='" + h + "'");

                        if (subpower.Rows.Count > 0 && subpower.Rows[0][0].ToString() != "" && subpower.Rows[0][0].ToString() != "0")
                        {
                            sub1[(h + 1)] = (MyDoubleParse(subpower.Rows[0][0].ToString()) - MyDoubleParse(consval.Rows[0][0].ToString())) - line[h];
                        }
                    }
                    for (int s = 1; s < 25; s++)
                    {


                        if (s <= 12)
                            MRCurGrid1.Rows[1].Cells[s].Value = sub1[s].ToString().Trim();
                        else
                            MRCurGrid2.Rows[1].Cells[s - 12].Value = sub1[s].ToString().Trim();

                    }
                /////////////////////////////row 2////////////////////////////////////////////

                    
                    for (int s = 1; s < 25; s++)
                    {

                        DataTable subpower = Utilities.GetTable("select sum( P) from dbo.DetailFRM009 where  TargetMarketDate='" + MRCal.Text + "' and Hour='" + (s-1) + "'");
                        if (s <= 12)
                            MRCurGrid1.Rows[2].Cells[s].Value = MyDoubleParse(subpower.Rows[0][0].ToString());
                        else
                            MRCurGrid2.Rows[2].Cells[s - 12].Value = MyDoubleParse(subpower.Rows[0][0].ToString());

                    }

                ///////////////////////////////////////////////////////////////////////////////////


               

                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }


            }


        }





        private void btnex009_Click(object sender, EventArgs e)
        {
            bool success009 = true;
            bool success0091 = true;

            DataTable odata = Utilities.GetTable(" select PNameFarsi FROM PowerPlant WHERE ppid='" + PPID + "'");
      
            folderBrowserDialogmarket.ShowNewFolderButton = true;
            DialogResult answer = folderBrowserDialogmarket.ShowDialog();
            if (answer == DialogResult.OK)
            {
                try
                {
                    string path = folderBrowserDialogmarket.SelectedPath;
                   

                    DataTable checkhour = Utilities.GetTable("select Hour from dbo.DetailFRM009 where PPID='"+int.Parse(PPID)+"' and TargetMarketDate='"+MRCal.Text+"' order by TargetMarketDate desc ");
                    if (checkhour.Rows[0][0].ToString() == "0")
                    {
                       success009=ExportM009Excel(path, int.Parse(PPID), MRCal.Text, odata.Rows[0][0].ToString().Trim());
                       success0091=ExportM0091Excel(path, int.Parse(PPID), MRCal.Text, odata.Rows[0][0].ToString().Trim());

                       MessageBox.Show("Export of M009 file(s) to " + path + " completed" + (success009 ? " successfully" : " Usuccessfully!") + ".");


                       MessageBox.Show("Export of M0091 file(s) to " + path + " completed" + (success0091 ? " successfully" : " Usuccessfully!") + ".");

                    }
                    else
                    {
                        MessageBox.Show("!Export Not Possible , Hours number are not complete.!\r\n Please Complete ls2 data in date:" + MRCal.Text + "..");
                    }

                }
                catch (Exception exp1)
                {
                    string str = exp1.Message;
                    MessageBox.Show("Unable To Export");
                }

            }


        }
        private bool ExportM009Excel(string path, int ppid, string Date, string ppnamefarsi)
        {
            List<string> Packages = new List<string>();

            string cmd = "select distinct PackageType from dbo.UnitsDataMain where PPID='" + ppid.ToString() + "'" +
            " AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%') AND (UnitCode not like 'Gas cc%')))";
            DataTable oDataTable = Utilities.GetTable(cmd);
            foreach (DataRow row in oDataTable.Rows)
            {
                Packages.Add(row["PackageType"].ToString().Trim());
            }

            string[] headers = { "Block/Unit No", "Hour", "P", "QC", 
                                   "QL"};

            string[] firstColValues = { "Form Name :", "Date Of Issue :", "Time Of Issue :", "Target Market Date :",
                                    "Power Plant Name :", "Code :", "Revision :", 
                                    "Filled By :", "Approved By :" };

            for (int packageIndex = 0; packageIndex < Packages.Count; packageIndex++)
            {
                int rowIndex = 0;
                int colIndex = 50;

                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                Excel.Application oExcel = new Excel.Application();

                oExcel.SheetsInNewWorkbook = 1;
                Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
                Excel.Worksheet sheet = null;
                Excel.Range range = null;

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers




                rowIndex = 0;
                colIndex = 0;
                int sheetIndex = 1;
                if (sheet != null)
                    sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
                else
                    sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

                sheet.Name = "Sheet1";

                int ppidToPrint = ppid;
                string ppnameFarsiModified = ppnamefarsi;

                if (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count > 1)
                {
                    ppnameFarsiModified = "سيكل تركيبي" + " " + ppnameFarsiModified;
                    ppidToPrint++;
                }

                string[] secondColValues = { "M009", 
                                          DateTime.Now.ToString("d"), DateTime.Now.ToString("T"),
                                          Date,
                                          ppnameFarsiModified, ppidToPrint.ToString(), 
                                          "0", "", "" };


                for (int i = 0; i < firstColValues.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[i + 1, 1];
                    range.Value2 = firstColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    range = (Excel.Range)sheet.Cells[i + 1, 2];
                    range.Value2 = secondColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


                rowIndex = firstColValues.Length + 2;
                colIndex = 0;
                for (int i = 0; i < headers.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = headers[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
                //for (int i = 1; i <= 10; i++)
                //{
                //    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                //    range.Value2 = "Power_" + i.ToString();
                //    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                //    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                //    range.Value2 = "Price_" + i.ToString();
                //    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                //}
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                //string strCmd = "select distinct  from DetailFRM009 where ppid='" + ppid.ToString() +
                //"' AND TargetMarketDate='" + biddingDate + "'" +
                //" and pptype=" + (Packages[packageIndex] == PackageTypePriority.CC.ToString() ? 1 : 0).ToString();
                //strCmd += " order by block";
                string strCmd;
                if (Packages.Count > 1)
                {
                    if (Packages[packageIndex] != PackageTypePriority.CC.ToString())

                        strCmd = " select distinct Block  from DetailFRM009 where ppid='" + ppid + "' and Block not like '%cc%'  ORDER BY Block";

                    else
                        strCmd = " select distinct Block  from DetailFRM009 where ppid='" + ppid + "' and Block  like '%cc%'  ORDER BY Block";
                }
                else
                    strCmd = " select distinct Block  from DetailFRM009 where ppid='" + ppid + "'ORDER BY Block";


                oDataTable = Utilities.GetTable(strCmd);


                string[] blockstable = new string[oDataTable.Rows.Count];
                string[] blocks = new string[oDataTable.Rows.Count];
                for (int i = 0; i < oDataTable.Rows.Count; i++)
                {
                    blockstable[i] = oDataTable.Rows[i][0].ToString().Trim();
                    blocks[i] = oDataTable.Rows[i][0].ToString().Trim();


                    ////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


                }

                for (int i = 0; i < oDataTable.Rows.Count; i++)
                {
                    strCmd = "select * from DetailFRM009 where ppid='" + ppid.ToString() + "'" +
                          " AND Block='" + blocks[i] + "'" +
                          " AND TargetMarketDate='" + Date + "'" +
                          " order by block, hour";
                    DataTable checkoDataTable = Utilities.GetTable(strCmd);
                    if (checkoDataTable.Rows.Count == 0) return false;

                }




                /////////////
                rowIndex = firstColValues.Length + 3;



                foreach (string blockName in blocks)
                {

                    string blocknameexport = blockName;
                    int firstLine = rowIndex;
                    range = (Excel.Range)sheet.Cells[rowIndex, 1];

                    //if (blocknameexport.Contains("Gas"))
                    //{
                    //    blocknameexport = blocknameexport.Replace("Gas", "G");
                    //    if (blocknameexport.Contains("cc"))
                    //        blocknameexport = blocknameexport.Replace("cc", "");

                    //}
                    //else if (blocknameexport.Contains("Steam"))
                    //{
                    //    blocknameexport = blocknameexport.Replace("Steam", "S");
                    //    if (blocknameexport.Contains("cc"))
                    //        blocknameexport = blocknameexport.Replace("cc", "");

                    //}



                    range.Value2 = blocknameexport;
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    ////////!!!!!!! Peak!!!>???????????????




                    strCmd = "select * from DetailFRM009 where ppid='" + ppid.ToString() + "'" +
                        " AND Block='" + blockName + "'" +
                        " AND TargetMarketDate='" + Date + "'" +

                        " order by block, hour";
                    oDataTable = Utilities.GetTable(strCmd);

                    colIndex = 2;

                    double max = 0;
                    foreach (DataRow row in oDataTable.Rows)
                    {
                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = (int.Parse(row["Hour"].ToString().Trim()) + 1).ToString();
                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["P"].ToString().Trim();


                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["QC"].ToString().Trim();

                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["QL"].ToString().Trim();




                        rowIndex++;
                        colIndex = 2;
                    }

                    //range = (Excel.Range)sheet.Cells[firstLine, 2];
                    //range.Value2 = max.ToString();
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                sheet = ((Excel.Worksheet)WB.Worksheets["Sheet1"]);

                range = (Excel.Range)sheet.Cells[1, 1];
                range = range.EntireColumn;
                range.AutoFit();

                range = (Excel.Range)sheet.Cells[1, 2];
                range = range.EntireColumn;
                range.AutoFit();

                sheet.Activate();
                //oExcel.Visible = true;

                string strDate = Date;
                strDate = strDate.Remove(7, 1);
                strDate = strDate.Remove(4, 1);
                string filePath = path + "\\" + "FRM009_" + ppidToPrint.ToString() + "_" + strDate + ".xls";
                //oExcel.Visible = true;
                WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                WB.Close(Missing.Value, Missing.Value, Missing.Value);
                oExcel.Workbooks.Close();
                oExcel.Quit();

            }
            return true;

        }
        private bool ExportM0091Excel(string path, int ppid, string Date, string ppnamefarsi)
        {
            List<string> Packages = new List<string>();

            string cmd = "select distinct PackageType from dbo.UnitsDataMain where PPID='" + ppid.ToString() + "'" +
            " AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%') AND (UnitCode not like 'Gas cc%')))";
            DataTable oDataTable = Utilities.GetTable(cmd);
            foreach (DataRow row in oDataTable.Rows)
            {
                Packages.Add(row["PackageType"].ToString().Trim());
            }

            string[] headers = { "Block/Unit No", "Hour", "Consumed" };


            string[] firstColValues = { "Form Name :", "Date Of Issue :", "Time Of Issue :", "Target Market Date :",
                                    "Power Plant Name :", "Code :", "Revision :", 
                                    "Filled By :", "Approved By :" };

            for (int packageIndex = 0; packageIndex < Packages.Count; packageIndex++)
            {
                int rowIndex = 0;
                int colIndex = 50;

                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                Excel.Application oExcel = new Excel.Application();

                oExcel.SheetsInNewWorkbook = 1;
                Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
                Excel.Worksheet sheet = null;
                Excel.Range range = null;

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers




                rowIndex = 0;
                colIndex = 0;
                int sheetIndex = 1;
                if (sheet != null)
                    sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
                else
                    sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

                sheet.Name = "Sheet1";

                int ppidToPrint = ppid;
                string ppnameFarsiModified = ppnamefarsi;

                if (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count > 1)
                {
                    ppnameFarsiModified = "سيكل تركيبي" + " " + ppnameFarsiModified;
                    ppidToPrint++;
                }

                string[] secondColValues = { "M0091", 
                                          DateTime.Now.ToString("d"), DateTime.Now.ToString("T"),
                                          Date,
                                          ppnameFarsiModified, ppidToPrint.ToString(), 
                                          "0", "", "" };


                for (int i = 0; i < firstColValues.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[i + 1, 1];
                    range.Value2 = firstColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    range = (Excel.Range)sheet.Cells[i + 1, 2];
                    range.Value2 = secondColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


                rowIndex = firstColValues.Length + 2;
                colIndex = 0;
                for (int i = 0; i < headers.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = headers[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
                //for (int i = 1; i <= 10; i++)
                //{
                //    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                //    range.Value2 = "Power_" + i.ToString();
                //    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                //    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                //    range.Value2 = "Price_" + i.ToString();
                //    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                //}
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                //string strCmd = "select distinct  from DetailFRM009 where ppid='" + ppid.ToString() +
                //"' AND TargetMarketDate='" + biddingDate + "'" +
                //" and pptype=" + (Packages[packageIndex] == PackageTypePriority.CC.ToString() ? 1 : 0).ToString();
                //strCmd += " order by block";
                string strCmd;
                if (Packages.Count > 1)
                {
                    if (Packages[packageIndex] != PackageTypePriority.CC.ToString())

                        strCmd = " select distinct Block  from DetailFRM009 where ppid='" + ppid + "' and Block not like '%cc%'  ORDER BY Block";

                    else
                        strCmd = " select distinct Block  from DetailFRM009 where ppid='" + ppid + "' and Block  like '%cc%'  ORDER BY Block";
                }
                else
                    strCmd = " select distinct Block  from DetailFRM009 where ppid='" + ppid + "'ORDER BY Block";


                oDataTable = Utilities.GetTable(strCmd);


                string[] blockstable = new string[oDataTable.Rows.Count];
                string[] blocks = new string[oDataTable.Rows.Count];
                for (int i = 0; i < oDataTable.Rows.Count; i++)
                {
                    blockstable[i] = oDataTable.Rows[i][0].ToString().Trim();
                    blocks[i] = oDataTable.Rows[i][0].ToString().Trim();


                    ////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


                }

                for (int i = 0; i < oDataTable.Rows.Count; i++)
                {
                    strCmd = "select * from DetailFRM009 where ppid='" + ppid.ToString() + "'" +
                          " AND Block='" + blocks[i] + "'" +
                          " AND TargetMarketDate='" + Date + "'" +
                          " order by block, hour";
                    DataTable checkoDataTable = Utilities.GetTable(strCmd);

                    if (checkoDataTable.Rows.Count == 0) return false;
                    if (checkoDataTable.Rows[i]["Consumed"].ToString() == "") return false;

                }




                /////////////
                rowIndex = firstColValues.Length + 3;



                foreach (string blockName in blocks)
                {

                    string blocknameexport = blockName;
                    int firstLine = rowIndex;
                    range = (Excel.Range)sheet.Cells[rowIndex, 1];

                    //if (blocknameexport.Contains("Gas"))
                    //{
                    //    blocknameexport = blocknameexport.Replace("Gas", "G");
                    //    if (blocknameexport.Contains("cc"))
                    //        blocknameexport = blocknameexport.Replace("cc", "");

                    //}
                    //else if (blocknameexport.Contains("Steam"))
                    //{
                    //    blocknameexport = blocknameexport.Replace("Steam", "S");
                    //    if (blocknameexport.Contains("cc"))
                    //        blocknameexport = blocknameexport.Replace("cc", "");

                    //}



                    range.Value2 = blocknameexport;
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    ////////!!!!!!! Peak!!!>???????????????




                    strCmd = "select * from DetailFRM009 where ppid='" + ppid.ToString() + "'" +
                        " AND Block='" + blockName + "'" +
                        " AND TargetMarketDate='" + Date + "'" +

                        " order by block, hour";
                    oDataTable = Utilities.GetTable(strCmd);

                    colIndex = 2;

                    double max = 0;
                    foreach (DataRow row in oDataTable.Rows)
                    {

                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = (int.Parse(row["Hour"].ToString().Trim()) + 1).ToString();
                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["Consumed"].ToString().Trim();


                        rowIndex++;
                        colIndex = 2;
                    }

                    //range = (Excel.Range)sheet.Cells[firstLine, 2];
                    //range.Value2 = max.ToString();
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                sheet = ((Excel.Worksheet)WB.Worksheets["Sheet1"]);

                range = (Excel.Range)sheet.Cells[1, 1];
                range = range.EntireColumn;
                range.AutoFit();

                range = (Excel.Range)sheet.Cells[1, 2];
                range = range.EntireColumn;
                range.AutoFit();

                sheet.Activate();
                //oExcel.Visible = true;

                string strDate = Date;
                strDate = strDate.Remove(7, 1);
                strDate = strDate.Remove(4, 1);
                string filePath = path + "\\" + "FRM0091_" + ppidToPrint.ToString() + "_" + strDate + ".xls";
                //oExcel.Visible = true;
                WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                WB.Close(Missing.Value, Missing.Value, Missing.Value);
                oExcel.Workbooks.Close();
                oExcel.Quit();

            }
            return true;

        }
        private void rbrealplant_CheckedChanged(object sender, EventArgs e)
        {
            if (rbrealplant.Checked)
            {
                FillMRPlantGrid();
            }
            else
            {
                FillMRPlantGridEstimated();
            }

        }
        private double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private void mrbtnpprint_Click(object sender, EventArgs e)
        {
            string tname = "DetailFRM005";
            if (rdbam005ba.Checked) tname = "baDetailFRM005";

            string package = MRPackLb.Text.Trim();
            string unit = MRUnitLb.Text.Trim();

         
            //Get Index Of Selected Unit(Row) in GridView (In Generad data Tab)
            int index = 0;
            if (GDSteamGroup.Text.Contains(package))
                for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                {
                    if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                        index = i;
                }
            else
                for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                {
                    if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                        index = i;
                }

            //Detect Block For FRM005
            string temp = unit;
            temp = temp.ToLower();
            if (package.Contains("Combined"))
            {
                int x;
                try
                {
                    x = int.Parse(PPID);
                }
                catch (Exception ep)
                {
                    x = 0;
                }
                if (PPIDArray.Contains(x + 1))
                    if (PPIDType[PPIDArray.IndexOf(x + 1)].ToString() == "virtual") x++;
                //if ((x == 131) || (x == 144)) x++;
                string packagecode = "";
                if (GDSteamGroup.Text.Contains(package))
                    packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                else packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                //temp = x + "-" + "C" + packagecode;


                //ccunitbase///////////////////////////////////////////
                //temp = x + "-" + "C" + packagecode;
                temp = temp.Replace("cc", "c");
                string[] sp = temp.Split('c');
                temp = sp[0].Trim() + sp[1].Trim();
                if (temp.Contains("gas"))
                {
                    temp = temp.Replace("gas", "G");

                }
                else
                {
                    temp = temp.Replace("steam", "S");

                }
                temp = x + "-" + temp;


            }
            else if (temp.Contains("gas"))
            {
                temp = temp.Replace("gas", "G");
                temp = PPID + "-" + temp;
            }
            else
            {
                temp = temp.Replace("steam", "S");
                temp = PPID + "-" + temp;
            }




            DataTable dt = Utilities.GetTable("SELECT Hour,Required,Dispatchable,MaxBid AS Price,Contribution,Economic FROM "+tname+" WHERE PPID= " + PPID + " AND TargetMarketDate='" + MRCal.Text + "'and Block='" + temp + "'");
            DataTable dt2 = Utilities.GetTable("SELECT Hour,P as Power,QC,QL,Consumed FROM DetailFRM009 WHERE PPID= " + PPID + " AND TargetMarketDate='" + MRCal.Text + "'AND Block='" + unit + "'");





            dt.Columns.Add("Power");
            dt.Columns.Add("QC");
            dt.Columns.Add("QL");
            dt.Columns.Add("Consumed");


            if (dt2.Rows.Count > 0)
            {

                foreach (DataRow mm in dt2.Rows)
                {
                    if (dt.Rows.Count > 0)
                    {
                        int i = int.Parse(mm["Hour"].ToString());
                        dt.Rows[i]["Power"] = mm["Power"].ToString();
                        dt.Rows[i]["QC"] = mm["QC"].ToString();
                        dt.Rows[i]["QL"] = mm["QL"].ToString();
                        dt.Rows[i]["Consumed"] = mm["Consumed"].ToString();
                    }
                }
            }
            string pname = MRPlantLb.Text.Trim();

            MarketPrint m = new MarketPrint(dt, pname, temp, package, MRCal.Text.Trim());
            m.Show();
        }
    
        private void btnmarketback_Click(object sender, EventArgs e)
        {
            if (MRHeaderPanel.Visible)
                FillMRUnitGrid();
            else if (L9.Text.Contains("Transmission"))
                FillMRTransmission(line.ToString().Trim());
            else if (L9.Text.Contains("Line"))
                FillMRLine(line.ToString().Trim(), MRPlantLb.Text.Trim());
            else if (L9.Text == "All Plants")
                FillMRGrecGrid();
            else FillMRPlantGrid();

            MRCurGrid2.Visible = false;
            MRCurGrid1.Visible = true;
            btnmarketback.Visible = false;
            btnmarketforward.Visible = true;
        }

        private void btnmarketforward_Click(object sender, EventArgs e)
        {
           // FillMRUnitGrid();
            MRCurGrid2.Visible = true;
            MRCurGrid1.Visible = false;
            btnmarketback.Visible = true;
            btnmarketforward.Visible = false;

            if (MRHeaderPanel.Visible)
                FillMRUnitGrid();
            else if (L9.Text.Contains("Transmission"))
                FillMRTransmission(line.ToString().Trim());
            else if (L9.Text.Contains("Line"))
                FillMRLine(line.ToString().Trim(), MRPlantLb.Text.Trim());
            else if (L9.Text == "All Plants")
                FillMRGrecGrid();
            else FillMRPlantGrid();
        }

    }
}