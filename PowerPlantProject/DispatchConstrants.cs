﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NRI.SBS.Common;
using System.Data.SqlClient;
using FarsiLibrary.Utils;
using System.Globalization;

namespace PowerPlantProject
{
    public partial class DispatchConstrants : Form
    {
      
        public DispatchConstrants()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }
                button1.BackColor = FormColors.GetColor().Buttonbackcolor;
                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }

                dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////

                //////////////////////////////////textbox////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                        c.BackColor = FormColors.GetColor().Textbackcolor;

                }
                foreach (Control c in panel1.Controls)
                {
                    if (c is TextBox || c is ListBox)
                        c.BackColor = FormColors.GetColor().Textbackcolor;

                }




                //////////////////////////////grid/////////////////////////////////////


                dataGridView1.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                dataGridView1.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;
            }
            catch
            {

            }
        }


        private void DispatchConstrants_Load(object sender, EventArgs e)
        {
            DataTable dt = Utilities.GetTable("select distinct LineCode from dbo.TransLine");
            foreach (DataRow mrow in dt.Rows)
            {
                cmbline1.Items.Add(mrow[0].ToString().Trim());
                             
            }
            fillgrid();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool enter=true;
            string line1 = cmbline1.Text.Trim();
            string line2 = cmbline2.Text.Trim();
            string line3 = cmbline3.Text.Trim();
            string line4 = cmbline4.Text.Trim();
            string perl1=cmbperline1.Text.Trim();
            string perl2=cmbperline2.Text.Trim();
            string perl3=cmbperline3.Text.Trim();
            string perl4=cmbperline4.Text.Trim();
            string temperature = cmbtemperature.Text.Trim();
            if (temperature == "")
                temperature = "EveryTemperature";
            string outline = cmboutline.Text.Trim();
            if (outline == "")
                outline = "EveryTime";


            DataTable dt = Utilities.GetTable("select * from  dbo.DispatchConstrant");
            bool[,] notagain = new bool[dt.Rows.Count,4];
            int ii = 0;
            foreach (DataRow mrow in dt.Rows)
            {
               
                for (int i = 0; i < 4; i++)
                {
                    if (mrow[i].ToString().Trim() == cmbline1.Text.Trim() || mrow[i].ToString().Trim() == cmbline2.Text.Trim() || mrow[i].ToString().Trim() == cmbline3.Text.Trim() || mrow[i].ToString().Trim() == cmbline4.Text.Trim())
                    {
                        if ((mrow[10].ToString().Trim() == cmboutline.Text.Trim() && mrow[9].ToString().Trim() == cmbtemperature.Text.Trim()) || (cmbtemperature.Text=="" && cmboutline.Text==""))
                            notagain[ii, i] = true;
                    }
                }
                ii++;
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (notagain[i, 0] && notagain[i, 1] && notagain[i, 2] && notagain[i, 3])
                {
                    enter = false;
                    MessageBox.Show("There Is A Same Selection !");
                    break;
                    
                }
            }
            if(enter)
            {
                try
                {
                   double value= double.Parse(txtquantity.Text);
                    if (line1 == "" && line2 == "" && line3 == "" && line4=="")
                    {
                        MessageBox.Show("Select AtLeast One Line");

                    }
                    else
                    {

                        DataTable dt1 = Utilities.GetTable("insert into  dbo.DispatchConstrant (Line1,Line2,Line3,Line4,PercentL1,PercentL2,PercentL3,PercentL4,Quantity,Temperature,OutLine) values ('" + line1 + "','" + line2 + "','" + line3 + "','" + line4 + "','" + perl1 + "','" + perl2 + "','" + perl3 + "','" + perl4 + "','" + txtquantity.Text.Trim() + "','" + temperature +"','"+ outline + "')");
                        fillgrid();

                    }
                   
                    

                }
                catch
                {
                    MessageBox.Show("Please Fill Double Value in Quantity !");

                }
                //--------------delete gui-----------------------------------------//
                cmbtemperature.Text = "";
                cmbperline1.Text = "";
                cmbperline2.Text = "";
                cmbperline3.Text = "";
                cmbperline4.Text = "";
                //cmbline1.Text = "";
                cmbline2.Text = "";
                cmbline3.Text = "";
                cmbline4.Text = "";
                txtquantity.Text = "";
                cmboutline.Text = "";
                //--------------------------------------------------------------------
            }


        }

        private void fillgrid()
        {
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.DataSource = null;
            DataTable dt = Utilities.GetTable("select * from dbo.DispatchConstrant");
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                string line1 = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                string line2 = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                string line3 = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                string line4 = dataGridView1.CurrentRow.Cells[4].Value.ToString();
                string perline1 = dataGridView1.CurrentRow.Cells[5].Value.ToString();
                string perline2 = dataGridView1.CurrentRow.Cells[6].Value.ToString();
                string perline3 = dataGridView1.CurrentRow.Cells[7].Value.ToString();
                string perline4 = dataGridView1.CurrentRow.Cells[8].Value.ToString();
                double  quantity = double.Parse(dataGridView1.CurrentRow.Cells[9].Value.ToString());
                string temp = dataGridView1.CurrentRow.Cells[10].Value.ToString();
                string  outline=dataGridView1.CurrentRow.Cells[11].Value.ToString();

                if (dataGridView1.CurrentCell.Value.ToString().Trim() == "Delete")
                {
                    DataTable dt = Utilities.GetTable("delete from dbo.DispatchConstrant where Line1='" + line1 + "' and line2='"
                        + line2 + "'and line3='" + line3 + "'and line4='"+ line4 +
                        "'and PercentL1='" + perline1 + "'and PercentL2='" + perline2 + "'and PercentL3='"+perline3+
                        "' and PercentL4='" + perline4 + "'and Quantity='" + quantity + "' and Temperature='" + temp + "' and OutLine='"+outline+"'");

                }
                fillgrid();
            }
            catch
            {

            }
        }

        private void cmbline1_TextChanged(object sender, EventArgs e)
        {
            if (cmbline1.Text == "")
            {
                MessageBox.Show("Select AtLeast One Line");

            }
        }

      

        private void cmbline2_MouseClick(object sender, MouseEventArgs e)
        {
            if (cmbline1.Text != "")
            {
                cmbline2.Items.Clear();
                DataTable dt = Utilities.GetTable("select distinct LineCode from dbo.TransLine where LineCode!='" + cmbline1.Text.Trim() + "' and LineCode!='" + cmbline2.Text.Trim() + "' and LineCode!='" + cmbline3.Text.Trim() + "' and LineCode!='" + cmbline4.Text.Trim() + "' and LineCode!='" + cmboutline.Text.Trim() + "'");
                foreach (DataRow mrow in dt.Rows)
                {

                    cmbline2.Items.Add(mrow[0].ToString().Trim());


                }
            }
            else
                MessageBox.Show("Please fill Line1 First!");
        }

        private void cmbline3_MouseClick(object sender, MouseEventArgs e)
        {
            if (cmbline1.Text != "")
            {
                cmbline3.Items.Clear();
                DataTable dt = Utilities.GetTable("select distinct LineCode from dbo.TransLine where LineCode!='" + cmbline1.Text.Trim() + "' and LineCode!='" + cmbline2.Text.Trim() + "' and LineCode!='" + cmbline3.Text.Trim() + "' and LineCode!='" + cmbline4.Text.Trim() + "' and LineCode!='" + cmboutline.Text.Trim()+"'");
                foreach (DataRow mrow in dt.Rows)
                {

                    cmbline3.Items.Add(mrow[0].ToString().Trim());

                }
            }
            else
                MessageBox.Show("Please fill Line1 First!");
        }

        private void cmbline4_MouseClick(object sender, MouseEventArgs e)
        {
            if (cmbline1.Text != "")
            {
                cmbline4.Items.Clear();
                DataTable dt = Utilities.GetTable("select distinct LineCode from dbo.TransLine where LineCode!='" + cmbline1.Text.Trim() + "' and LineCode!='" + cmbline2.Text.Trim() + "' and LineCode!='" + cmbline3.Text.Trim() + "' and LineCode!='" + cmbline4.Text.Trim() + "' and LineCode!='" + cmboutline.Text.Trim() + "'");
                foreach (DataRow mrow in dt.Rows)
                {

                    cmbline4.Items.Add(mrow[0].ToString().Trim());

                }
            }
            else
                MessageBox.Show("Please fill Line1 First!");
        }

        private void cmboutline_MouseClick(object sender, MouseEventArgs e)
        {
            if (cmbline1.Text != "")
            {
                cmboutline.Items.Clear();

                cmboutline.Items.Add("EveryTime");
                DataTable dt = Utilities.GetTable("select distinct LineCode from dbo.TransLine where LineCode!='" + cmbline1.Text.Trim() + "' and LineCode!='" + cmbline2.Text.Trim() + "' and LineCode!='" + cmbline3.Text.Trim() + "' and LineCode!='" + cmbline4.Text.Trim() + "' and LineCode!='" + cmboutline.Text.Trim()+"'");
                foreach (DataRow mrow in dt.Rows)
                {

                    cmboutline.Items.Add(mrow[0].ToString().Trim());

                }
            }
            else
                MessageBox.Show("Please fill Line1 First!");
        }

      

       

       

       
    }
}
