﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    static public class Utility
    {
        private static Dictionary<string, string> _packageTypesDictionary;
        private static DataTable _dtPackageTypes;
        static Utility()
        {
             _dtPackageTypes = Utilities.GetTable("select  distinct ppid, PackageType from dbo.UnitsDataMain");
            _packageTypesDictionary = new Dictionary<string, string>();
            foreach (DataRow row in _dtPackageTypes.Rows)
            {
                _packageTypesDictionary.Add(row["ppid"].ToString().Trim(), row["PackageType"].ToString().Trim());
            }
        }

        public static string GetPackageType(string ppid)
        {
            try
            {
                return _packageTypesDictionary[ppid];
            }
            catch (Exception)
            {

                return null;
            }
           
        }

      public static  DataSet SelectCommand(string query)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter adaptor = new SqlDataAdapter();
            SqlConnection conn = new SqlConnection(ConnectionManager.ConnectionString);
            conn.Open();
            adaptor.SelectCommand = new SqlCommand(query, conn);
            adaptor.Fill(ds);
            conn.Close();
            return ds;
        }

      public static void ExecuteCommand(string query)
      {
                 
          SqlConnection conn = new SqlConnection(ConnectionManager.ConnectionString);
          conn.Open();
          SqlCommand cmd = new SqlCommand(query, conn);
          cmd.ExecuteNonQuery();
          conn.Close();
        
      }

        
    }
}
