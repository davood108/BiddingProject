﻿namespace PowerPlantProject
{
    partial class MarketBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MarketBill));
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.btnReport = new System.Windows.Forms.Button();
            this.radioBtnDaily = new System.Windows.Forms.RadioButton();
            this.radioBtnMonthly = new System.Windows.Forms.RadioButton();
            this.radioBtnCustom = new System.Windows.Forms.RadioButton();
            this.myDateRange1 = new UI.MyControl.MyDateRange();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioBtnUnitHour = new System.Windows.Forms.RadioButton();
            this.radioBtnPlantHour = new System.Windows.Forms.RadioButton();
            this.radioBtnTotal = new System.Windows.Forms.RadioButton();
            this.radioBtnPlantDate = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // radGridView1
            // 
            this.radGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView1.BackColor = System.Drawing.Color.Beige;
            this.radGridView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radGridView1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radGridView1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView1.Location = new System.Drawing.Point(2, 134);
            // 
            // radGridView1
            // 
            this.radGridView1.MasterTemplate.EnablePaging = true;
            this.radGridView1.MasterTemplate.PageSize = 10000;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radGridView1.Size = new System.Drawing.Size(1048, 288);
            this.radGridView1.TabIndex = 2;
            this.radGridView1.Text = "radGridView1";
            this.toolTip1.SetToolTip(this.radGridView1, "Click the column header to sort");
            // 
            // btnReport
            // 
            this.btnReport.BackColor = System.Drawing.Color.Linen;
            this.btnReport.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnReport.Image = global::PowerPlantProject.Properties.Resources.run88;
            this.btnReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReport.Location = new System.Drawing.Point(565, 58);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(111, 30);
            this.btnReport.TabIndex = 1;
            this.btnReport.Tag = "";
            this.btnReport.Text = "   Report";
            this.btnReport.UseVisualStyleBackColor = false;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // radioBtnDaily
            // 
            this.radioBtnDaily.AutoSize = true;
            this.radioBtnDaily.Checked = true;
            this.radioBtnDaily.Location = new System.Drawing.Point(6, 23);
            this.radioBtnDaily.Name = "radioBtnDaily";
            this.radioBtnDaily.Size = new System.Drawing.Size(48, 17);
            this.radioBtnDaily.TabIndex = 0;
            this.radioBtnDaily.TabStop = true;
            this.radioBtnDaily.Text = "Daily";
            this.radioBtnDaily.UseVisualStyleBackColor = true;
            this.radioBtnDaily.CheckedChanged += new System.EventHandler(this.radioBtnCustom_CheckedChanged);
            // 
            // radioBtnMonthly
            // 
            this.radioBtnMonthly.AutoSize = true;
            this.radioBtnMonthly.Location = new System.Drawing.Point(6, 46);
            this.radioBtnMonthly.Name = "radioBtnMonthly";
            this.radioBtnMonthly.Size = new System.Drawing.Size(62, 17);
            this.radioBtnMonthly.TabIndex = 1;
            this.radioBtnMonthly.Text = "Monthly";
            this.radioBtnMonthly.UseVisualStyleBackColor = true;
            this.radioBtnMonthly.CheckedChanged += new System.EventHandler(this.radioBtnCustom_CheckedChanged);
            // 
            // radioBtnCustom
            // 
            this.radioBtnCustom.AutoSize = true;
            this.radioBtnCustom.Location = new System.Drawing.Point(6, 69);
            this.radioBtnCustom.Name = "radioBtnCustom";
            this.radioBtnCustom.Size = new System.Drawing.Size(60, 17);
            this.radioBtnCustom.TabIndex = 2;
            this.radioBtnCustom.Text = "Custom";
            this.radioBtnCustom.UseVisualStyleBackColor = true;
            this.radioBtnCustom.CheckedChanged += new System.EventHandler(this.radioBtnCustom_CheckedChanged);
            // 
            // myDateRange1
            // 
            this.myDateRange1.BackColor = System.Drawing.Color.Transparent;
            this.myDateRange1.EndDate = "";
            this.myDateRange1.Location = new System.Drawing.Point(90, 23);
            this.myDateRange1.Name = "myDateRange1";
            this.myDateRange1.Size = new System.Drawing.Size(169, 63);
            this.myDateRange1.StartDate = "";
            this.myDateRange1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioBtnDaily);
            this.groupBox1.Controls.Add(this.radioBtnMonthly);
            this.groupBox1.Controls.Add(this.myDateRange1);
            this.groupBox1.Controls.Add(this.radioBtnCustom);
            this.groupBox1.Location = new System.Drawing.Point(51, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(330, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.BackColor = System.Drawing.Color.Linen;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnExport.Image = global::PowerPlantProject.Properties.Resources._1263108119_Microsoft_Office_2007_Excel;
            this.btnExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExport.Location = new System.Drawing.Point(1000, 91);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(47, 37);
            this.btnExport.TabIndex = 1;
            this.btnExport.Tag = "";
            this.btnExport.Text = "Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioBtnUnitHour);
            this.groupBox2.Controls.Add(this.radioBtnPlantHour);
            this.groupBox2.Controls.Add(this.radioBtnTotal);
            this.groupBox2.Controls.Add(this.radioBtnPlantDate);
            this.groupBox2.Location = new System.Drawing.Point(397, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(152, 116);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            // 
            // radioBtnUnitHour
            // 
            this.radioBtnUnitHour.AutoSize = true;
            this.radioBtnUnitHour.Checked = true;
            this.radioBtnUnitHour.Location = new System.Drawing.Point(6, 19);
            this.radioBtnUnitHour.Name = "radioBtnUnitHour";
            this.radioBtnUnitHour.Size = new System.Drawing.Size(100, 17);
            this.radioBtnUnitHour.TabIndex = 0;
            this.radioBtnUnitHour.TabStop = true;
            this.radioBtnUnitHour.Text = "Unit/Date/Hour";
            this.radioBtnUnitHour.UseVisualStyleBackColor = true;
            // 
            // radioBtnPlantHour
            // 
            this.radioBtnPlantHour.AutoSize = true;
            this.radioBtnPlantHour.Location = new System.Drawing.Point(6, 42);
            this.radioBtnPlantHour.Name = "radioBtnPlantHour";
            this.radioBtnPlantHour.Size = new System.Drawing.Size(105, 17);
            this.radioBtnPlantHour.TabIndex = 1;
            this.radioBtnPlantHour.Text = "Plant/Date/Hour";
            this.radioBtnPlantHour.UseVisualStyleBackColor = true;
            // 
            // radioBtnTotal
            // 
            this.radioBtnTotal.AutoSize = true;
            this.radioBtnTotal.Enabled = false;
            this.radioBtnTotal.Location = new System.Drawing.Point(6, 89);
            this.radioBtnTotal.Name = "radioBtnTotal";
            this.radioBtnTotal.Size = new System.Drawing.Size(82, 17);
            this.radioBtnTotal.TabIndex = 2;
            this.radioBtnTotal.Text = "Total Month";
            this.radioBtnTotal.UseVisualStyleBackColor = true;
            // 
            // radioBtnPlantDate
            // 
            this.radioBtnPlantDate.AutoSize = true;
            this.radioBtnPlantDate.Location = new System.Drawing.Point(6, 65);
            this.radioBtnPlantDate.Name = "radioBtnPlantDate";
            this.radioBtnPlantDate.Size = new System.Drawing.Size(77, 17);
            this.radioBtnPlantDate.TabIndex = 2;
            this.radioBtnPlantDate.Text = "Plant/Date";
            this.radioBtnPlantDate.UseVisualStyleBackColor = true;
            // 
            // MarketBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.ClientSize = new System.Drawing.Size(1053, 434);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.radGridView1);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnReport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MarketBill";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Market Bill ";
            this.Load += new System.EventHandler(this.MarketInformation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnReport;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private UI.MyControl.MyDateRange myDateRange1;
        private System.Windows.Forms.RadioButton radioBtnDaily;
        private System.Windows.Forms.RadioButton radioBtnMonthly;
        private System.Windows.Forms.RadioButton radioBtnCustom;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioBtnUnitHour;
        private System.Windows.Forms.RadioButton radioBtnPlantHour;
        private System.Windows.Forms.RadioButton radioBtnTotal;
        private System.Windows.Forms.RadioButton radioBtnPlantDate;
    }
}