﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.UI.Export;
using Telerik.WinControls.UI;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using FarsiLibrary.Utils;
using Telerik.WinControls;
using Telerik.Data;
using System.IO;

//*********************************************************
//Add Refrence. Search TelerikData and select Newer Version (2014.1.226.40)
//*********************************************************

namespace PowerPlantProject.New
{
  public  class ExportRadGrid
    {
      public static void RunExportToExcelML(RadGridView radGridView, string fileName, ref bool openExportFile)
        {
          
            ExportToExcelML excelExporter = new ExportToExcelML(radGridView);         
           
            excelExporter.SheetName = "Sheet1";
            
            excelExporter.SummariesExportOption = SummariesOption.ExportAll;
           
            excelExporter.SheetMaxRows = Telerik.WinControls.UI.Export.ExcelMaxRows._1048576;            
            try
            {
                excelExporter.RunExport(fileName);
                RadMessageBox.SetThemeName(radGridView.ThemeName);
                DialogResult dr = RadMessageBox.Show(
                    "The data in the grid was exported successfully. Do you want to open the file?",
                    "Export to Excel", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    openExportFile = true;
                    System.Diagnostics.Process.Start(fileName);
                }
            }
            catch (IOException ex)
            {
                RadMessageBox.SetThemeName(radGridView.ThemeName);
                RadMessageBox.Show(ex.Message, "I/O Error", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }
    }
}
