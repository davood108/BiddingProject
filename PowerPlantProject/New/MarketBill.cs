﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NRI.SBS.Common;
using System.Data.SqlClient;
using FarsiLibrary.Utils;
using Telerik.WinControls.UI;
using PowerPlantProject.New;

namespace PowerPlantProject
{
    public partial class MarketBill : Form
    {
        public MarketBill()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;
                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
              
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////

            }
            catch
            {

            }
        }


        private void fillgrid(string fromDate, string toDate)
        {
            DataTable dt = new DataTable();
            string query = "";
            if (radioBtnUnitHour.Checked)
            {
                query = string.Format("SELECT * FROM [dbo].[DailyBill] Where Date BETWEEN '{0}' AND '{1}' ORDER BY [Date],[HourID],[UnitCode]", fromDate, toDate);
            }
            else if (radioBtnPlantHour.Checked)
            {
                query = string.Format("SELECT * FROM [dbo].[View_DailyBillPlant] Where Date BETWEEN '{0}' AND '{1}' ORDER BY [Date],[HourID]", fromDate, toDate);
            }
            else if (radioBtnPlantDate.Checked)
            {
                query = string.Format("SELECT * FROM [dbo].[View_DailyBillSum] Where Date BETWEEN '{0}' AND '{1}' ORDER BY [Date]", fromDate, toDate);
            }
            else if (radioBtnTotal.Checked)
            {
                query = string.Format("SELECT * FROM [dbo].[View_DailyBillTotal] Where DateMonth = '{0}'", fromDate.Substring(0, 7));
            }

            dt = Utilities.GetTable(query);

            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("There is no data for selected date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            radGridView1.AllowAutoSizeColumns = true;
            radGridView1.DataSource = null;
            radGridView1.DataSource = dt;
        }

        private void MarketInformation_Load(object sender, EventArgs e)
        {            
            myDateRange1.Initialize();          
        }



        private void btnReport_Click(object sender, EventArgs e)
        {

            string fromDate = myDateRange1.StartDate;
            string toDate = myDateRange1.EndDate;
            fillgrid(fromDate, toDate);
                     
        }

        private void radioBtnCustom_CheckedChanged(object sender, EventArgs e)
        {
            if ((sender as RadioButton).Checked)
            {
                if (radioBtnDaily.Checked)
                {
                    myDateRange1.RangeType = UI.MyControl.TimeRangeType.Daily;
                    radioBtnTotal.Enabled = false;
                    if (radioBtnTotal.Checked)
                    {
                        radioBtnUnitHour.Checked = true;
                    }
                }
                else if (radioBtnMonthly.Checked)
                {
                    myDateRange1.RangeType = UI.MyControl.TimeRangeType.Monthly;
                    radioBtnTotal.Enabled = true;
                }
                else if (radioBtnCustom.Checked)
                {
                    myDateRange1.RangeType = UI.MyControl.TimeRangeType.Custom;
                    radioBtnTotal.Enabled = false;
                    if (radioBtnTotal.Checked)
                    {
                        radioBtnUnitHour.Checked = true;
                    }
                }

                
                myDateRange1.Initialize();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Excel (*.xls)|*.xls";
           
            if (saveFileDialog1.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            if (saveFileDialog1.FileName.Equals(String.Empty))
            {
               MessageBox.Show("Please enter a file name.");
                return;
            }
            string fileName = this.saveFileDialog1.FileName;
          
            bool openExportFile = true;

           ExportRadGrid.RunExportToExcelML(radGridView1,fileName, ref openExportFile);
                  
            if (openExportFile)
            {
                try
                {
                    System.Diagnostics.Process.Start(fileName);
                }
                catch (Exception ex)
                {
                    string message = String.Format("The file cannot be opened on your system.\nError message: {0}", ex.Message);
                    MessageBox.Show(message, "Open File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

    


    

    }
}
