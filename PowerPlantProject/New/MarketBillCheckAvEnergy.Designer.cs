﻿namespace PowerPlantProject
{
    partial class MarketBillCheckAvEnergy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn38 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn39 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MarketBillCheckAvEnergy));
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.btnReport = new System.Windows.Forms.Button();
            this.radioBtnDaily = new System.Windows.Forms.RadioButton();
            this.radioBtnMonthly = new System.Windows.Forms.RadioButton();
            this.radioBtnCustom = new System.Windows.Forms.RadioButton();
            this.myDateRange1 = new UI.MyControl.MyDateRange();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // radGridView1
            // 
            this.radGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView1.BackColor = System.Drawing.Color.Beige;
            this.radGridView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radGridView1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radGridView1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView1.Location = new System.Drawing.Point(2, 134);
            // 
            // radGridView1
            // 
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.FieldName = "PowerPlantCode";
            gridViewTextBoxColumn1.HeaderText = "PowerPlantCode";
            gridViewTextBoxColumn1.Name = "columnPlantCode";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 100;
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.FieldName = "UnitCode";
            gridViewTextBoxColumn2.HeaderText = "Unit";
            gridViewTextBoxColumn2.Name = "ColumnUnitCode";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn3.EnableExpressionEditor = false;
            gridViewTextBoxColumn3.FieldName = "Date";
            gridViewTextBoxColumn3.HeaderText = "Date";
            gridViewTextBoxColumn3.Name = "columnDate";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 90;
            gridViewTextBoxColumn4.EnableExpressionEditor = false;
            gridViewTextBoxColumn4.FieldName = "HourID";
            gridViewTextBoxColumn4.HeaderText = "Hour";
            gridViewTextBoxColumn4.Name = "columnHour";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.Width = 40;
            gridViewTextBoxColumn5.EnableExpressionEditor = false;
            gridViewTextBoxColumn5.FieldName = "BillTypeCode";
            gridViewTextBoxColumn5.HeaderText = "Bill Type Code";
            gridViewTextBoxColumn5.Name = "columnBillTypeCode";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.Width = 100;
            gridViewTextBoxColumn6.EnableExpressionEditor = false;
            gridViewTextBoxColumn6.FieldName = "BillRevision";
            gridViewTextBoxColumn6.HeaderText = "Bill Revision";
            gridViewTextBoxColumn6.Name = "columnBillRevision";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.Width = 70;
            gridViewTextBoxColumn7.EnableExpressionEditor = false;
            gridViewTextBoxColumn7.FieldName = "Ps_Bill";
            gridViewTextBoxColumn7.HeaderText = "Ps Bill";
            gridViewTextBoxColumn7.Name = "columnPs_Bill";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn7.Width = 80;
            gridViewTextBoxColumn8.EnableExpressionEditor = false;
            gridViewTextBoxColumn8.FieldName = "Ps_Av";
            gridViewTextBoxColumn8.HeaderText = "Ps Av";
            gridViewTextBoxColumn8.Name = "columnPs_Av";
            gridViewTextBoxColumn8.ReadOnly = true;
            gridViewTextBoxColumn8.Width = 80;
            gridViewTextBoxColumn9.EnableExpressionEditor = false;
            gridViewTextBoxColumn9.FieldName = "Ps_Diff";
            gridViewTextBoxColumn9.HeaderText = "Ps Diff";
            gridViewTextBoxColumn9.Name = "columnPs_Diff";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.Width = 70;
            gridViewTextBoxColumn10.EnableExpressionEditor = false;
            gridViewTextBoxColumn10.FieldName = "PMet_Bill";
            gridViewTextBoxColumn10.HeaderText = "PMet Bill";
            gridViewTextBoxColumn10.Name = "columnPMet_Bill";
            gridViewTextBoxColumn10.ReadOnly = true;
            gridViewTextBoxColumn10.Width = 80;
            gridViewTextBoxColumn11.EnableExpressionEditor = false;
            gridViewTextBoxColumn11.FieldName = "PMet_Av";
            gridViewTextBoxColumn11.HeaderText = "PMet Av";
            gridViewTextBoxColumn11.Name = "columnPMet_Av";
            gridViewTextBoxColumn11.ReadOnly = true;
            gridViewTextBoxColumn11.Width = 80;
            gridViewTextBoxColumn12.EnableExpressionEditor = false;
            gridViewTextBoxColumn12.FieldName = "PMet_Diff";
            gridViewTextBoxColumn12.HeaderText = "PMet Diff";
            gridViewTextBoxColumn12.Name = "columnPMet_Diff";
            gridViewTextBoxColumn12.ReadOnly = true;
            gridViewTextBoxColumn12.Width = 70;
            gridViewTextBoxColumn13.EnableExpressionEditor = false;
            gridViewTextBoxColumn13.FieldName = "AvCapMin_Bill";
            gridViewTextBoxColumn13.HeaderText = "AvCapMin Bill";
            gridViewTextBoxColumn13.Name = "columnAvCapMin_Bill";
            gridViewTextBoxColumn13.ReadOnly = true;
            gridViewTextBoxColumn13.Width = 80;
            gridViewTextBoxColumn14.EnableExpressionEditor = false;
            gridViewTextBoxColumn14.FieldName = "AvCapMin_Av";
            gridViewTextBoxColumn14.HeaderText = "AvCapMin Av";
            gridViewTextBoxColumn14.Name = "columnAvCapMin_Av";
            gridViewTextBoxColumn14.ReadOnly = true;
            gridViewTextBoxColumn14.Width = 80;
            gridViewTextBoxColumn15.EnableExpressionEditor = false;
            gridViewTextBoxColumn15.FieldName = "AvCapMin_Diff";
            gridViewTextBoxColumn15.HeaderText = "AvCapMin Diff";
            gridViewTextBoxColumn15.Name = "columnAvCapMin_Diff";
            gridViewTextBoxColumn15.ReadOnly = true;
            gridViewTextBoxColumn15.Width = 70;
            gridViewTextBoxColumn16.EnableExpressionEditor = false;
            gridViewTextBoxColumn16.FieldName = "AvCapMax_Bill";
            gridViewTextBoxColumn16.HeaderText = "AvCapMax Bill";
            gridViewTextBoxColumn16.Name = "columnAvCapMax_Bill";
            gridViewTextBoxColumn16.ReadOnly = true;
            gridViewTextBoxColumn16.Width = 80;
            gridViewTextBoxColumn17.EnableExpressionEditor = false;
            gridViewTextBoxColumn17.FieldName = "AvCapMax_Av";
            gridViewTextBoxColumn17.HeaderText = "AvCapMax Av";
            gridViewTextBoxColumn17.Name = "columnAvCapMax_Av";
            gridViewTextBoxColumn17.ReadOnly = true;
            gridViewTextBoxColumn17.Width = 80;
            gridViewTextBoxColumn18.EnableExpressionEditor = false;
            gridViewTextBoxColumn18.FieldName = "AvCapMax_Diff";
            gridViewTextBoxColumn18.HeaderText = "AvCapMax Diff";
            gridViewTextBoxColumn18.Name = "columnAvCapMax_Diff";
            gridViewTextBoxColumn18.ReadOnly = true;
            gridViewTextBoxColumn18.Width = 80;
            gridViewTextBoxColumn19.EnableExpressionEditor = false;
            gridViewTextBoxColumn19.FieldName = "DevType2_Bill";
            gridViewTextBoxColumn19.HeaderText = "DevType2 Bill";
            gridViewTextBoxColumn19.Name = "columnDevType2_Bill";
            gridViewTextBoxColumn19.ReadOnly = true;
            gridViewTextBoxColumn19.Width = 80;
            gridViewTextBoxColumn20.EnableExpressionEditor = false;
            gridViewTextBoxColumn20.FieldName = "DevType2_Av";
            gridViewTextBoxColumn20.HeaderText = "DevType2 Av";
            gridViewTextBoxColumn20.Name = "columnDevType2_Av";
            gridViewTextBoxColumn20.ReadOnly = true;
            gridViewTextBoxColumn20.Width = 80;
            gridViewTextBoxColumn21.EnableExpressionEditor = false;
            gridViewTextBoxColumn21.FieldName = "DevType2_Diff";
            gridViewTextBoxColumn21.HeaderText = "DevType2 Diff";
            gridViewTextBoxColumn21.Name = "columnDevType2_Diff";
            gridViewTextBoxColumn21.ReadOnly = true;
            gridViewTextBoxColumn21.Width = 80;
            gridViewTextBoxColumn22.EnableExpressionEditor = false;
            gridViewTextBoxColumn22.FieldName = "DevType3_Bill";
            gridViewTextBoxColumn22.HeaderText = "DevType3 Bill";
            gridViewTextBoxColumn22.Name = "columnDevType3_Bill";
            gridViewTextBoxColumn22.ReadOnly = true;
            gridViewTextBoxColumn22.Width = 80;
            gridViewTextBoxColumn23.EnableExpressionEditor = false;
            gridViewTextBoxColumn23.FieldName = "DevType3_Av";
            gridViewTextBoxColumn23.HeaderText = "DevType3 Av";
            gridViewTextBoxColumn23.Name = "columnDevType3_Av";
            gridViewTextBoxColumn23.ReadOnly = true;
            gridViewTextBoxColumn23.Width = 80;
            gridViewTextBoxColumn24.EnableExpressionEditor = false;
            gridViewTextBoxColumn24.FieldName = "DevType3_Diff";
            gridViewTextBoxColumn24.HeaderText = "DevType3 Diff";
            gridViewTextBoxColumn24.Name = "columnDevType3_Diff";
            gridViewTextBoxColumn24.ReadOnly = true;
            gridViewTextBoxColumn24.Width = 80;
            gridViewTextBoxColumn25.EnableExpressionEditor = false;
            gridViewTextBoxColumn25.FieldName = "DevType4_Bill";
            gridViewTextBoxColumn25.HeaderText = "DevType4 Bill";
            gridViewTextBoxColumn25.Name = "columnDevType4_Bill";
            gridViewTextBoxColumn25.ReadOnly = true;
            gridViewTextBoxColumn25.Width = 80;
            gridViewTextBoxColumn26.EnableExpressionEditor = false;
            gridViewTextBoxColumn26.FieldName = "DevType4_Av";
            gridViewTextBoxColumn26.HeaderText = "DevType4 Av";
            gridViewTextBoxColumn26.Name = "columnDevType4_Av";
            gridViewTextBoxColumn26.ReadOnly = true;
            gridViewTextBoxColumn26.Width = 80;
            gridViewTextBoxColumn27.EnableExpressionEditor = false;
            gridViewTextBoxColumn27.FieldName = "DevType4_Diff";
            gridViewTextBoxColumn27.HeaderText = "DevType4 Diff";
            gridViewTextBoxColumn27.Name = "columnDevType4_Diff";
            gridViewTextBoxColumn27.ReadOnly = true;
            gridViewTextBoxColumn27.Width = 80;
            gridViewTextBoxColumn28.EnableExpressionEditor = false;
            gridViewTextBoxColumn28.FieldName = "DevType5_Bill";
            gridViewTextBoxColumn28.HeaderText = "DevType5 Bill";
            gridViewTextBoxColumn28.Name = "columnDevType5_Bill";
            gridViewTextBoxColumn28.ReadOnly = true;
            gridViewTextBoxColumn28.Width = 80;
            gridViewTextBoxColumn29.EnableExpressionEditor = false;
            gridViewTextBoxColumn29.FieldName = "DevType5_Av";
            gridViewTextBoxColumn29.HeaderText = "DevType5 Av";
            gridViewTextBoxColumn29.Name = "columnDevType5_Av";
            gridViewTextBoxColumn29.ReadOnly = true;
            gridViewTextBoxColumn29.Width = 80;
            gridViewTextBoxColumn30.EnableExpressionEditor = false;
            gridViewTextBoxColumn30.FieldName = "DevType5_Diff";
            gridViewTextBoxColumn30.HeaderText = "DevType5 Diff";
            gridViewTextBoxColumn30.Name = "columnDevType5_Diff";
            gridViewTextBoxColumn30.ReadOnly = true;
            gridViewTextBoxColumn30.Width = 80;
            gridViewTextBoxColumn31.EnableExpressionEditor = false;
            gridViewTextBoxColumn31.FieldName = "DevType6_Bill";
            gridViewTextBoxColumn31.HeaderText = "DevType6 Bill";
            gridViewTextBoxColumn31.Name = "columnDevType6_Bill";
            gridViewTextBoxColumn31.ReadOnly = true;
            gridViewTextBoxColumn31.Width = 80;
            gridViewTextBoxColumn32.EnableExpressionEditor = false;
            gridViewTextBoxColumn32.FieldName = "DevType6_Av";
            gridViewTextBoxColumn32.HeaderText = "DevType6 Av";
            gridViewTextBoxColumn32.Name = "columnDevType6_Av";
            gridViewTextBoxColumn32.ReadOnly = true;
            gridViewTextBoxColumn32.Width = 80;
            gridViewTextBoxColumn33.EnableExpressionEditor = false;
            gridViewTextBoxColumn33.FieldName = "DevType6_Diff";
            gridViewTextBoxColumn33.HeaderText = "DevType6 Diff";
            gridViewTextBoxColumn33.Name = "columnDevType6_Diff";
            gridViewTextBoxColumn33.ReadOnly = true;
            gridViewTextBoxColumn33.Width = 80;
            gridViewTextBoxColumn34.EnableExpressionEditor = false;
            gridViewTextBoxColumn34.FieldName = "DevType7_Bill";
            gridViewTextBoxColumn34.HeaderText = "DevType7 Bill";
            gridViewTextBoxColumn34.Name = "columnDevType7_Bill";
            gridViewTextBoxColumn34.ReadOnly = true;
            gridViewTextBoxColumn34.Width = 80;
            gridViewTextBoxColumn35.EnableExpressionEditor = false;
            gridViewTextBoxColumn35.FieldName = "DevType7_Av";
            gridViewTextBoxColumn35.HeaderText = "DevType7 Av";
            gridViewTextBoxColumn35.Name = "columnDevType7_Av";
            gridViewTextBoxColumn35.ReadOnly = true;
            gridViewTextBoxColumn35.Width = 80;
            gridViewTextBoxColumn36.EnableExpressionEditor = false;
            gridViewTextBoxColumn36.FieldName = "DevType7_Diff";
            gridViewTextBoxColumn36.HeaderText = "DevType7 Diff";
            gridViewTextBoxColumn36.Name = "columnDevType7_Diff";
            gridViewTextBoxColumn36.ReadOnly = true;
            gridViewTextBoxColumn36.Width = 80;
            gridViewTextBoxColumn37.EnableExpressionEditor = false;
            gridViewTextBoxColumn37.FieldName = "DevType8_Bill";
            gridViewTextBoxColumn37.HeaderText = "DevType8 Bill";
            gridViewTextBoxColumn37.Name = "columnDevType8_Bill";
            gridViewTextBoxColumn37.ReadOnly = true;
            gridViewTextBoxColumn37.Width = 80;
            gridViewTextBoxColumn38.EnableExpressionEditor = false;
            gridViewTextBoxColumn38.FieldName = "DevType8_Av";
            gridViewTextBoxColumn38.HeaderText = "DevType8 Av";
            gridViewTextBoxColumn38.Name = "columnDevType8_Av";
            gridViewTextBoxColumn38.ReadOnly = true;
            gridViewTextBoxColumn38.Width = 80;
            gridViewTextBoxColumn39.EnableExpressionEditor = false;
            gridViewTextBoxColumn39.FieldName = "DevType8_Diff";
            gridViewTextBoxColumn39.HeaderText = "DevType8 Diff";
            gridViewTextBoxColumn39.Name = "columnDevType8_Diff";
            gridViewTextBoxColumn39.ReadOnly = true;
            gridViewTextBoxColumn39.Width = 80;
            this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32,
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34,
            gridViewTextBoxColumn35,
            gridViewTextBoxColumn36,
            gridViewTextBoxColumn37,
            gridViewTextBoxColumn38,
            gridViewTextBoxColumn39});
            this.radGridView1.MasterTemplate.EnablePaging = true;
            this.radGridView1.MasterTemplate.PageSize = 10000;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radGridView1.Size = new System.Drawing.Size(1048, 288);
            this.radGridView1.TabIndex = 2;
            this.radGridView1.Text = "radGridView1";
            this.toolTip1.SetToolTip(this.radGridView1, "Click the column header to sort");
            // 
            // btnReport
            // 
            this.btnReport.BackColor = System.Drawing.Color.Linen;
            this.btnReport.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnReport.Image = global::PowerPlantProject.Properties.Resources.run88;
            this.btnReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReport.Location = new System.Drawing.Point(407, 45);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(111, 30);
            this.btnReport.TabIndex = 1;
            this.btnReport.Tag = "";
            this.btnReport.Text = "   Report";
            this.btnReport.UseVisualStyleBackColor = false;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // radioBtnDaily
            // 
            this.radioBtnDaily.AutoSize = true;
            this.radioBtnDaily.Checked = true;
            this.radioBtnDaily.Location = new System.Drawing.Point(6, 23);
            this.radioBtnDaily.Name = "radioBtnDaily";
            this.radioBtnDaily.Size = new System.Drawing.Size(48, 17);
            this.radioBtnDaily.TabIndex = 0;
            this.radioBtnDaily.TabStop = true;
            this.radioBtnDaily.Text = "Daily";
            this.radioBtnDaily.UseVisualStyleBackColor = true;
            this.radioBtnDaily.CheckedChanged += new System.EventHandler(this.radioBtnCustom_CheckedChanged);
            // 
            // radioBtnMonthly
            // 
            this.radioBtnMonthly.AutoSize = true;
            this.radioBtnMonthly.Location = new System.Drawing.Point(6, 46);
            this.radioBtnMonthly.Name = "radioBtnMonthly";
            this.radioBtnMonthly.Size = new System.Drawing.Size(62, 17);
            this.radioBtnMonthly.TabIndex = 1;
            this.radioBtnMonthly.Text = "Monthly";
            this.radioBtnMonthly.UseVisualStyleBackColor = true;
            this.radioBtnMonthly.CheckedChanged += new System.EventHandler(this.radioBtnCustom_CheckedChanged);
            // 
            // radioBtnCustom
            // 
            this.radioBtnCustom.AutoSize = true;
            this.radioBtnCustom.Location = new System.Drawing.Point(6, 69);
            this.radioBtnCustom.Name = "radioBtnCustom";
            this.radioBtnCustom.Size = new System.Drawing.Size(60, 17);
            this.radioBtnCustom.TabIndex = 2;
            this.radioBtnCustom.Text = "Custom";
            this.radioBtnCustom.UseVisualStyleBackColor = true;
            this.radioBtnCustom.CheckedChanged += new System.EventHandler(this.radioBtnCustom_CheckedChanged);
            // 
            // myDateRange1
            // 
            this.myDateRange1.BackColor = System.Drawing.Color.Transparent;
            this.myDateRange1.EndDate = "";
            this.myDateRange1.Location = new System.Drawing.Point(90, 23);
            this.myDateRange1.Name = "myDateRange1";
            this.myDateRange1.Size = new System.Drawing.Size(169, 63);
            this.myDateRange1.StartDate = "";
            this.myDateRange1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioBtnDaily);
            this.groupBox1.Controls.Add(this.radioBtnMonthly);
            this.groupBox1.Controls.Add(this.myDateRange1);
            this.groupBox1.Controls.Add(this.radioBtnCustom);
            this.groupBox1.Location = new System.Drawing.Point(51, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(330, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.BackColor = System.Drawing.Color.Linen;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnExport.Image = global::PowerPlantProject.Properties.Resources._1263108119_Microsoft_Office_2007_Excel;
            this.btnExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExport.Location = new System.Drawing.Point(1000, 91);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(47, 37);
            this.btnExport.TabIndex = 1;
            this.btnExport.Tag = "";
            this.btnExport.Text = "Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // MarketBillCheckAvEnergy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.ClientSize = new System.Drawing.Size(1053, 434);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.radGridView1);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnReport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MarketBillCheckAvEnergy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Market Bill Checking (AvEnergy)";
            this.Load += new System.EventHandler(this.MarketInformation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnReport;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private UI.MyControl.MyDateRange myDateRange1;
        private System.Windows.Forms.RadioButton radioBtnDaily;
        private System.Windows.Forms.RadioButton radioBtnMonthly;
        private System.Windows.Forms.RadioButton radioBtnCustom;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}