﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PowerPlantProject.ServiceReference1;

namespace PowerPlantProject.New
{
  public  class WebServiceData
    {//ref string error, ref string columnsHeaders, ref ArrayOfAnyType arrayData
        public string Error { get; set; }
        public bool HasError
        {
            get
            {
                if (string.IsNullOrEmpty(Error))
                {
                    return false;
                }
                return true;
            }
        }
        public string ColumnsHeaders { get; set; }
        public ArrayOfAnyType ArrayData { get; set; }
    }
}
