﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using NRI.SBS.Common;
using FarsiLibrary.Utils;
using System.Collections;
using System.IO;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using Auto_Update_Library;
using PowerPlantProject.New;


namespace PowerPlantProject
{
    public partial class BillWebServiceForm : Form
    {

        Thread thread;


        public BillWebServiceForm()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void BillWebServiceForm_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.Form.CheckForIllegalCrossThreadCalls = false;
            txtYear.Text = PersianDateTime.Now.Year.ToString();
            txtMonth.Text = PersianDateTime.Now.Month.ToString();

        }





        private void AllTypeInterval_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (thread != null)
            {
                thread.Abort();
            }
        }
        private double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        public void UpdateProgressBar(int value, string description)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<int, string>(UpdateProgressBar), new object[] { value, description });
                return;
            }
            lblCurrentDate.Text = description;

            progressBar1.Value = value;
            Thread.Sleep(1);

        }

        private void button1_Click(object sender, EventArgs e)
        {


            //    progressBar1.Visible = true;

            //    //--------------------max progress------------------------//
            //    PPIDArray.Clear();
            //    PPIDType.Clear();
            //    DataSet MyDS = new DataSet();
            //    SqlDataAdapter Myda = new SqlDataAdapter();
            //    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            //    myConnection.Open();

            //    Myda.SelectCommand = new SqlCommand("SELECT PPID FROM PowerPlant", myConnection);
            //    Myda.Fill(MyDS, "ppid");
            //    foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
            //    {
            //        PPIDArray.Add(MyRow["PPID"].ToString().Trim());
            //        PPIDType.Add("real");
            //    }

            //    for (int i = 0; i < PPIDArray.Count; i++)
            //    {
            //        SqlCommand mycom = new SqlCommand();
            //        mycom.Connection = myConnection;
            //        mycom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id SELECT @result2 =count(PPID) FROM [PPUnit] WHERE PPID=@id AND PackageType LIKE 'Combined Cycle%'";
            //        mycom.Parameters.Add("@id", SqlDbType.NChar, 10);
            //        mycom.Parameters["@id"].Value = PPIDArray[i].ToString();
            //        mycom.Parameters.Add("@result1", SqlDbType.Int);
            //        mycom.Parameters["@result1"].Direction = ParameterDirection.Output;
            //        mycom.Parameters.Add("@result2", SqlDbType.Int);
            //        mycom.Parameters["@result2"].Direction = ParameterDirection.Output;
            //        mycom.ExecuteNonQuery();
            //        int result1 = (int)mycom.Parameters["@result1"].Value;
            //        int result2 = (int)mycom.Parameters["@result2"].Value;
            //        if ((result1 > 1) && (result2 > 0))
            //        {
            //            PPIDArray.Add((int.Parse(PPIDArray[i].ToString())) + 1);
            //            PPIDType.Add("virtual");
            //        }
            //    }

            //    DataTable numpost = Utilities.ls2returntbl("select  count(*) from ConsumedPost");
            //      DataTable numline = Utilities.ls2returntbl("select distinct count(*) from dbo.TransLine");
            //    missingDates = GetDatesBetween();
            //    int max = (missingDates.Count * PPIDArray.Count) + (missingDates.Count * int.Parse(numline.Rows[0][0].ToString())) + (missingDates.Count * int.Parse(numpost.Rows[0][0].ToString()));
            //    progressBar1.Maximum = max;
            //    //------------------------------------------------------------------------
            //    button1.Enabled = false;
            //    label3.Visible = true;
            //    thread = new Thread(LongTask);
            //    thread.IsBackground = true;
            //    thread.Start();



        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            progressBar1.Visible = true;
            btnCancel.Enabled = true;
            btnStart.Enabled = false;
            PersianDateTime pdt;
            try
            {
                pdt = new PersianDateTime(int.Parse(txtYear.Text), int.Parse(txtMonth.Text), 1);
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Year or Month", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            for (int day = 0; day < pdt.MonthDays; day++)
            {
                if (backgroundWorker1.CancellationPending)
                {
                    MessageBox.Show("The process was canceled", "Cancel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                PersianDateTime currentDate = pdt.AddDays(day);
                bool hasData;
                try
                {
                    BillDataInsertion.ReadFromWebServiceAndInsert(currentDate, out hasData);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (!hasData)
                {
                    MessageBox.Show("There is no bill for this month", "Cancel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

               
                backgroundWorker1.ReportProgress((day + 1) * 100 / pdt.MonthDays, currentDate.PersianDateString);

            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            UpdateProgressBar(e.ProgressPercentage, e.UserState.ToString());
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnCancel.Enabled = false;
            btnStart.Enabled = true;
            MessageBox.Show("Finished");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            backgroundWorker1.CancelAsync();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            (new WebServiceSettingForm()).ShowDialog();
        }

        //private void LongTask()
        //{
        //    int p = 1;
        //    //plant//-----------------------------------------------------------------------
        //    label3.Text = "";
        //    label3.Text = "Inserting Ls2 Plant";
        //    PPIDArray.Clear();
        //    PPIDType.Clear();

        //    if (Auto_Update_Library.BaseData.GetInstance().CounterPath != "")
        //    {
        //        DataSet MyDS = new DataSet();
        //        SqlDataAdapter Myda = new SqlDataAdapter();
        //        SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
        //        myConnection.Open();

        //        Myda.SelectCommand = new SqlCommand("SELECT PPID FROM PowerPlant", myConnection);
        //        Myda.Fill(MyDS, "ppid");
        //        foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
        //        {
        //            PPIDArray.Add(MyRow["PPID"].ToString().Trim());
        //            PPIDType.Add("real");
        //        }

        //        for (int i = 0; i < PPIDArray.Count; i++)
        //        {
        //            SqlCommand mycom = new SqlCommand();
        //            mycom.Connection = myConnection;
        //            mycom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id SELECT @result2 =count(PPID) FROM [PPUnit] WHERE PPID=@id AND PackageType LIKE 'Combined Cycle%'";
        //            mycom.Parameters.Add("@id", SqlDbType.NChar, 10);
        //            mycom.Parameters["@id"].Value = PPIDArray[i].ToString();
        //            mycom.Parameters.Add("@result1", SqlDbType.Int);
        //            mycom.Parameters["@result1"].Direction = ParameterDirection.Output;
        //            mycom.Parameters.Add("@result2", SqlDbType.Int);
        //            mycom.Parameters["@result2"].Direction = ParameterDirection.Output;
        //            mycom.ExecuteNonQuery();
        //            int result1 = (int)mycom.Parameters["@result1"].Value;
        //            int result2 = (int)mycom.Parameters["@result2"].Value;
        //            if ((result1 > 1) && (result2 > 0))
        //            {
        //                PPIDArray.Add((int.Parse(PPIDArray[i].ToString())) + 1);
        //                PPIDType.Add("virtual");
        //            }
        //        }


        //        missingDates = GetDatesBetween();

        //        foreach (PersianDate date in missingDates)
        //        {

        //            for (int k = 0; k < PPIDArray.Count; k++)
        //            {
        //                if ((PPIDArray[k].ToString() != "0") && (PPIDType[k].ToString() == "real"))
        //                {
        //                    UpdateProgressBar(p, "Getting Data For");
        //                    ReadLS2Files(k, date);
        //                    p++;
        //                }
        //            }

        //        }


        //        //----------line------------------------------------------------//
        //        label3.Text = "";
        //        label3.Text = "Inserting Ls2 Lines - Post Interval";
        //        missingDates = GetDatesBetween();

        //        foreach (PersianDate date in missingDates)
        //        {

        //            DataTable numline = Utilities.ls2returntbl("select distinct count(*) from dbo.TransLine");
        //            for (int k = 0; k < int.Parse(numline.Rows[0][0].ToString()); k++)
        //            {

        //                if (numline.Rows[0][0].ToString() != "0")
        //                {
        //                    UpdateProgressBar(p, "Getting Data For");
        //                    ReadLS2Trans(k, date, "");
        //                    p++;
        //                }
        //            }

        //        }

        //        //-------------post------------------------------------------//

        //        missingDates = GetDatesBetween();

        //        foreach (PersianDate date in missingDates)
        //        {

        //            DataTable numline = Utilities.ls2returntbl("select  count(*) from ConsumedPost");
        //            for (int k = 0; k < int.Parse(numline.Rows[0][0].ToString()); k++)
        //            {

        //                if (numline.Rows[0][0].ToString() != "0")
        //                {
        //                    UpdateProgressBar(p, "Getting Data For");
        //                    ReadLS2Post(k, date, "");
        //                    p++;
        //                }
        //            }

        //        }

        //        label3.Text = "Download End .";
        //        UpdateProgressBar(progressBar1.Maximum, "");
        //    }
        //    else
        //    {
        //        MessageBox.Show(" Counter Path Is Not Set !");
        //    }

        //}





    }
}
