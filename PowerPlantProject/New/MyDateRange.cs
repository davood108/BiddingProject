﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PowerPlantProject.New;

namespace UI.MyControl
{
    public partial class MyDateRange : UserControl
    {
        public string StartDate
        {
            get { return dateTimeSelectorStart.Text; }
            set
            {
                _startDate = value;
                dateTimeSelectorStart.Text = _startDate;

                //if (_rangeType == TimeRangeType.Daily)
                //{
                //    dateTimeSelectorEnd.Enabled = false;
                //    dateTimeSelectorEnd.Text = _startDate;
                //}
                //else
                //    dateTimeSelectorEnd.Enabled = true;
            }
        }
        public string EndDate
        {
            get { return dateTimeSelectorEnd.Text; }
            set
            {
                _endDate = value;
                dateTimeSelectorEnd.Text = _endDate;
            }
        }

        [Description("Type Of time range"),
         Category("NewProperty"),
         DefaultValue(TimeRangeType.Daily),
         Browsable(true)]
        public TimeRangeType RangeType
        {
            get { return _rangeType; }
            set
            {
                _rangeType = value;
                dateTimeSelectorEnd.Enabled = _rangeType != TimeRangeType.Daily;
            }
        }

        [Description("Raise when value of StartDate or EndDate changed"),
       Category("NewProperty"),
       Browsable(true)]
        public event EventHandler ValueChangedHandler;


        private string _startDate;
        private string _endDate;
        private TimeRangeType _rangeType;

        public MyDateRange()
        {
            InitializeComponent();

        }

        /// <summary>
        /// set StartDate and EndDate based on now time
        /// </summary>
        public void Initialize()
        {
            switch (_rangeType)
            {
                case TimeRangeType.Daily:
                    dateTimeSelectorStart.Value = DateTime.Now;

                    break;
                case TimeRangeType.Monthly:
                    dateTimeSelectorStart.Value = DateTime.Now;
                    buttonPrev.PerformClick();


                    break;
                case TimeRangeType.Yearly:
                    dateTimeSelectorStart.Value = DateTime.Now;
                    buttonPrev.PerformClick();
                    break;
                case TimeRangeType.Custom:
                    dateTimeSelectorStart.Value = DateTime.Now;
                    dateTimeSelectorEnd.Value = DateTime.Now;
                    break;
            }
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
           switch (_rangeType)
            {
                case TimeRangeType.Daily:
                    if (dateTimeSelectorStart.Value.HasValue)
                    {
                        dateTimeSelectorStart.Value = dateTimeSelectorStart.Value.Value.AddDays(1);
                    }
                    break;
                case TimeRangeType.Monthly:
                    if (dateTimeSelectorStart.Value.HasValue)
                    {
                        PersianDateTime startPersianDateTime = new PersianDateTime(StartDate);
                        startPersianDateTime = startPersianDateTime.NextMonth;
                        dateTimeSelectorStart.Text =
                            new PersianDateTime(startPersianDateTime.Year, startPersianDateTime.Month, 1)
                                .PersianDateString;
                        dateTimeSelectorEnd.Text = new PersianDateTime(startPersianDateTime.Year,
                            startPersianDateTime.Month, startPersianDateTime.MonthDays).PersianDateString;

                    }
                    break;
                case TimeRangeType.Yearly:
                    if (dateTimeSelectorStart.Value.HasValue)
                    {
                        PersianDateTime startPersianDateTime = new PersianDateTime(StartDate);
                        dateTimeSelectorStart.Text =new PersianDateTime(startPersianDateTime.Year + 1, 1, 1).PersianDateString;
                        startPersianDateTime = new PersianDateTime(startPersianDateTime.Year + 1, 12, 1);
                        dateTimeSelectorEnd.Text = new PersianDateTime(startPersianDateTime.Year,
                            startPersianDateTime.Month, startPersianDateTime.MonthDays).PersianDateString;
                    }
                    break;
                case TimeRangeType.Custom:
                    if (dateTimeSelectorStart.Value.HasValue)
                    {
                        dateTimeSelectorStart.Value = dateTimeSelectorStart.Value.Value.AddDays(1);
                    }
                    break;
            }
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            switch (_rangeType)
            {
                case TimeRangeType.Daily:
                    if (dateTimeSelectorStart.Value.HasValue)
                    {
                        dateTimeSelectorStart.Value = dateTimeSelectorStart.Value.Value.AddDays(-1);
                    }
                    break;
                case TimeRangeType.Monthly:
                    if (dateTimeSelectorStart.Value.HasValue)
                    {
                        PersianDateTime startPersianDateTime = new PersianDateTime(StartDate);
                        startPersianDateTime = startPersianDateTime.PreviousMonth;
                        dateTimeSelectorStart.Text =
                            new PersianDateTime(startPersianDateTime.Year, startPersianDateTime.Month, 1)
                                .PersianDateString;
                        dateTimeSelectorEnd.Text = new PersianDateTime(startPersianDateTime.Year,
                            startPersianDateTime.Month, startPersianDateTime.MonthDays).PersianDateString;

                    }
                    break;
                case TimeRangeType.Yearly:
                    if (dateTimeSelectorStart.Value.HasValue)
                    {
                        PersianDateTime startPersianDateTime = new PersianDateTime(StartDate);
                        dateTimeSelectorStart.Text = new PersianDateTime(startPersianDateTime.Year - 1, 1, 1).PersianDateString;
                        startPersianDateTime = new PersianDateTime(startPersianDateTime.Year - 1, 12, 1);
                        dateTimeSelectorEnd.Text = new PersianDateTime(startPersianDateTime.Year,
                            startPersianDateTime.Month, startPersianDateTime.MonthDays).PersianDateString;
                    }
                    break;

                case TimeRangeType.Custom:
                    if (dateTimeSelectorStart.Value.HasValue)
                    {
                        dateTimeSelectorStart.Value = dateTimeSelectorStart.Value.Value.AddDays(-1);
                    }
                    break;
            }
        }

        private void dateTimeSelectorStart_ValueChanged(object sender, EventArgs e)
        {
            switch (_rangeType)
            {
                case TimeRangeType.Daily:
                    dateTimeSelectorEnd.Value = dateTimeSelectorStart.Value;
                    break;
                case TimeRangeType.Monthly:
                    PersianDateTime startPersianDateTime = new PersianDateTime(StartDate);
                    dateTimeSelectorEnd.Text = String.Format("{0}/{1}/{2}", startPersianDateTime.Year,
                        startPersianDateTime.Month, startPersianDateTime.MonthDays);
                    //dateTimeSelectorEnd.Value = dateTimeSelectorStart.Value;
                    break;
                case TimeRangeType.Yearly:
                    PersianDateTime startPersianDateTime2 = new PersianDateTime(StartDate);
                    PersianDateTime endPersianDateTime = new PersianDateTime(startPersianDateTime2.Year, 12, 1);
                    dateTimeSelectorEnd.Text = String.Format("{0}/{1}/{2}", endPersianDateTime.Year, 12,
                        endPersianDateTime.MonthDays);
                    break;
                    case TimeRangeType.Custom:
                    if (dateTimeSelectorStart.Value.HasValue && dateTimeSelectorEnd.Value.HasValue)
                    {
                        if (dateTimeSelectorStart.Value>dateTimeSelectorEnd.Value)
                        {
                            dateTimeSelectorEnd.Value = dateTimeSelectorStart.Value;
                        }
                    }
                    break;
            }

            if (ValueChangedHandler != null)
            {
                ValueChangedHandler(this, new EventArgs());
            }
           
        }

        private void dateTimeSelectorEnd_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimeSelectorStart.Value.HasValue && dateTimeSelectorEnd.Value.HasValue)
            {
                if (dateTimeSelectorStart.Value > dateTimeSelectorEnd.Value)
                {
                    dateTimeSelectorStart.Value = dateTimeSelectorEnd.Value;
                }
            }

            if (ValueChangedHandler != null)
            {
                ValueChangedHandler(this, new EventArgs());
            }
        }

    }

    public enum TimeRangeType
    {
        Daily,
        Monthly,
        Yearly,
        Custom //e.g multiple Daily
    }
}
