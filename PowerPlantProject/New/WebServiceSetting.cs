﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using NRI.SBS.Common;

namespace PowerPlantProject.New
{
    class WebServiceSetting
    {
        public string Username { get; set; }
        public string Password { get; set; }
      
        static public WebServiceSetting GetSetting()
        {
            DataSet ds = Utility.SelectCommand("SELECT TOP 1 [Username] ,[Password]  FROM [dbo].[WebServiceInfo]");
            WebServiceSetting webServiceSetting = new WebServiceSetting();
             webServiceSetting.Username ="";
             webServiceSetting.Password = "";
             if (ds.Tables[0].Rows.Count>0)
             {
                 webServiceSetting.Username = RijndaelEncoding.Decrypt(ds.Tables[0].Rows[0][0].ToString());
                 webServiceSetting.Password = RijndaelEncoding.Decrypt(ds.Tables[0].Rows[0][1].ToString());
             }
          
            return webServiceSetting;
        }
        static public void SaveSetting(WebServiceSetting webServiceSetting)
        {
            string username = RijndaelEncoding.Encrypt(webServiceSetting.Username);
            string password = RijndaelEncoding.Encrypt(webServiceSetting.Password);
            string query = string.Format("DELETE [dbo].[WebServiceInfo]; INSERT INTO [dbo].[WebServiceInfo] ([Username],[Password]) VALUES('{0}','{1}')", username, password);
            Utility.ExecuteCommand(query);
        }
    }
}
