﻿namespace UI.MyControl
{
    partial class MyDateRange
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dateTimeSelectorStart = new Atf.UI.DateTimeSelector();
            this.dateTimeSelectorEnd = new Atf.UI.DateTimeSelector();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.buttonNext = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dateTimeSelectorStart
            // 
            this.dateTimeSelectorStart.Location = new System.Drawing.Point(29, 4);
            this.dateTimeSelectorStart.Name = "dateTimeSelectorStart";
            this.dateTimeSelectorStart.Size = new System.Drawing.Size(100, 21);
            this.dateTimeSelectorStart.TabIndex = 0;
            this.dateTimeSelectorStart.UsePersianFormat = true;
            this.dateTimeSelectorStart.ValueChanged += new System.EventHandler(this.dateTimeSelectorStart_ValueChanged);
            // 
            // dateTimeSelectorEnd
            // 
            this.dateTimeSelectorEnd.Enabled = false;
            this.dateTimeSelectorEnd.Location = new System.Drawing.Point(29, 31);
            this.dateTimeSelectorEnd.Name = "dateTimeSelectorEnd";
            this.dateTimeSelectorEnd.Size = new System.Drawing.Size(100, 21);
            this.dateTimeSelectorEnd.TabIndex = 2;
            this.dateTimeSelectorEnd.UsePersianFormat = true;
            this.dateTimeSelectorEnd.ValueChanged += new System.EventHandler(this.dateTimeSelectorEnd_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-4, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "From";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(-4, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "To";
            // 
            // buttonPrev
            // 
            this.buttonPrev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrev.Image = global::PowerPlantProject.Properties.Resources.arrow_down;
            this.buttonPrev.Location = new System.Drawing.Point(135, 30);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(23, 23);
            this.buttonPrev.TabIndex = 3;
            this.toolTip1.SetToolTip(this.buttonPrev, "Previous");
            this.buttonPrev.UseVisualStyleBackColor = true;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNext.Image = global::PowerPlantProject.Properties.Resources.arrow_up;
            this.buttonNext.Location = new System.Drawing.Point(135, 3);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(23, 23);
            this.buttonNext.TabIndex = 1;
            this.toolTip1.SetToolTip(this.buttonNext, "Next");
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // MyDateRange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.buttonPrev);
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimeSelectorEnd);
            this.Controls.Add(this.dateTimeSelectorStart);
            this.Name = "MyDateRange";
            this.Size = new System.Drawing.Size(169, 65);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Atf.UI.DateTimeSelector dateTimeSelectorStart;
        private Atf.UI.DateTimeSelector dateTimeSelectorEnd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrev;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
