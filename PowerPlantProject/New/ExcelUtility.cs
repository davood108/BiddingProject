﻿using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using FlexCel.Core;
using FlexCel.Report;
using FlexCel.XlsAdapter;

namespace PowerPlantProject.New
{
    /// <summary>
    /// Abdollahzadeh
    /// </summary>
    class ExcelUtility
    {
        public DataSet GetData(string excelFilePath, string sheetName)
        {
            //read from FRM002.xls into datagridview
            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelFilePath + ";Extended Properties=Excel 8.0";
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            objConn.Open();

            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + sheetName + "$]", objConn);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
            objAdapter1.SelectCommand = objCmdSelect;
            DataSet objDataset1 = new DataSet();
            try
            {
                objAdapter1.Fill(objDataset1);

                
            }
            catch (Exception ex)
            {
                //IsValid = false;
                throw ex;
            }
            objConn.Close();
            return objDataset1;
        }

        public static void CreateExcelFile(DataSet dataset, string tempFile, string saveFilePath)
        {
            string dataPath;
            FlexCelReport report = Export(dataset, out dataPath);

            File.Delete(saveFilePath);

            //report.SetValue("ReportCaption", reportcaption);

            try
            {
                report.Run(tempFile, saveFilePath);
            }
            catch (Exception ex)
            {
                // Logger.AddToLogAndShow(String.Format("{0},\n TempFile={1} ,\n SaveFile={2}", ex.Message, tempfile, savefile), LogStyle.ErrorStyle);
                throw ex;
            }
        }
        public static void CreateExcelFile(DataSet dataset, string tempfile, string savedir, string savefile)
        {
            string dataPath;
            string savefilename = savedir + savefile;
            CreateSubDir(savedir);
            FlexCelReport report = Export(dataset, out dataPath);

            File.Delete(savefilename);

            //report.SetValue("ReportCaption", reportcaption);

            try
            {
                report.Run(tempfile, savefilename);
            }
            catch (Exception ex)
            {
                // Logger.AddToLogAndShow(String.Format("{0},\n TempFile={1} ,\n SaveFile={2}", ex.Message, tempfile, savefile), LogStyle.ErrorStyle);
                throw ex;
            }
        }


        private static FlexCelReport Export(DataSet dataset, out string DataPath)
        {
            //PersianDate OccuredDate = PersianDateConverter.ToPersianDate
            //    (DateTime.Now.Date);

            //_today = OccuredDate.ToString("d");
            FlexCelReport report = new FlexCelReport
            {
                DeleteEmptyRanges = false,
                ErrorActions = TErrorActions.None
            };

            report.ClearTables();
            report.AddTable(dataset);
            DataPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase).Replace(@"file:\", "") + @"\";
            if (!File.Exists(DataPath + "Generic Reports.template.xls"))
            {
                DataPath = DataPath + @"..\..\";
            }
            return report;
        }


        private static void CreateSubDir(string dir)
        {
            var di = new DirectoryInfo(dir);
            if (!di.Exists)
            {
                di.Create();
            }
        }

        /// <summary>
        ///  1397//04/03
        /// </summary>
        /// <param name="gridView1">First 12 Hours</param>
        /// <param name="gridView2">Second 12 Hours</param>
        public static void ExportMarketResultGrid(DataGridView gridView1, DataGridView gridView2, string saveFilePath)
        {

            XlsFile xls = new XlsFile(true);
            xls.NewFile(1, TExcelFileFormat.v2010);
            xls.AddSheet();
            xls.ActiveSheet = 1;
            xls.SheetName = "Hour1-12";

            //Export To DataTable
            //DataTable dt1 = new DataTable();
            //foreach (DataGridViewColumn col in gridView1.Columns)
            //{
            //     dt1.Columns.Add(col.Name);
            //}
            //foreach (DataGridViewRow row in gridView1.Rows)
            //{
            //    DataRow dRow = dt1.NewRow();
            //    foreach (DataGridViewCell cell in row.Cells)
            //    {
            //        dRow[cell.ColumnIndex] = cell.Value;
            //    }
            //    dt1.Rows.Add(dRow);
            //}



            foreach (DataGridViewColumn col in gridView1.Columns)
            {
                xls.SetCellValue(1, col.Index + 1, col.HeaderText);
            }

            int r = 2;
            foreach (DataGridViewRow row in gridView1.Rows)
            {

                foreach (DataGridViewCell cell in row.Cells)
                {
                    xls.SetCellValue(r, cell.ColumnIndex + 1, cell.Value);
                }
                r++;
            }

            xls.ActiveSheet = 2;
            xls.SheetName = "Hour13-24";

            foreach (DataGridViewColumn col in gridView2.Columns)
            {
                xls.SetCellValue(1, col.Index + 1, col.HeaderText);
            }

            r = 2;
            foreach (DataGridViewRow row in gridView2.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    xls.SetCellValue(r, cell.ColumnIndex + 1, cell.Value);
                }
                r++;
            }
            xls.ActiveSheet = 1;
            xls.Save(saveFilePath);

        }

    }
}
