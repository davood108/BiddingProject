﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PowerPlantProject.ServiceReference1;
using System.Data.SqlClient;
using System.Data;
using NRI.SBS.Common;

namespace PowerPlantProject.New
{
    public class BillDataInsertion
    {
        public static void ReadFromWebServiceAndInsert(PersianDateTime date, out bool hasData)
        {
          

            string webServiceColumnsHeadres = "";
            ArrayOfAnyType webServiceArrayData = new ArrayOfAnyType();
            WebServiceData webServiceData = new WebServiceData();
            EmisData.LoadData(date, webServiceData);
            if (webServiceData.HasError)
            {
                throw new Exception("Error In Web Service. " + webServiceData.Error);
            }
            if (webServiceData.ArrayData.Count == 0)
            {
                hasData = false;
                return;
            }
            hasData = true;
            //Delete old data
            Utility.ExecuteCommand(string.Format("DELETE FROM [dbo].[DailyBill] WHERE Date='{0}'", date.PersianDateString));
            string insertQuery = GetInsertCommand(webServiceData.ColumnsHeaders, webServiceData.ArrayData);
            Utility.ExecuteCommand(insertQuery);
            
        }
        static string GetInsertCommand(string webServiceColumnsHeadres, ArrayOfAnyType webServiceArrayData)
        {
            List<string> tableColumnsList = GetColumnsOfDailyBillTable();

            string[] columns = webServiceColumnsHeadres.Split('|');

            Dictionary<int, bool> keepedIndecies = new Dictionary<int, bool>();
            string tableColumnsString = "";
            for (int i = 0; i < columns.Length; i++)
            {
                bool keeped = tableColumnsList.Contains(columns[i].ToLower());
                keepedIndecies.Add(i, keeped);
                if (keeped)
                {
                    tableColumnsString += columns[i] + ",";
                }
            }
            tableColumnsString = tableColumnsString.Remove(tableColumnsString.Length - 1);//remove last comma

            StringBuilder builder = new StringBuilder();
            foreach (string item in webServiceArrayData)
            {
                string[] values = item.Split('|');
                string valuesString = "";
                for (int j = 0; j < values.Length; j++)
                {
                    if (keepedIndecies[j])
                    {
                        if (values[j].Length > 0 && values[j][0] == '\'')
                        {
                            valuesString += "N";//UniCode
                        }
                        valuesString += values[j] + ",";
                    }
                }
                valuesString = valuesString.Remove(valuesString.Length - 1);//remove last comma
                builder.AppendLine(string.Format(" INSERT DailyBill({0}) \n VALUES ({1}) ", tableColumnsString, valuesString));
            }



            return builder.ToString(); ;
        }

        static List<string> GetColumnsOfDailyBillTable()
        {

            DataSet ds = new DataSet();
            ds = Utility.SelectCommand("SELECT name FROM sys.columns WHERE OBJECT_NAME(object_id) = 'DailyBill'");
            List<string> tableColumnsList = new List<string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                tableColumnsList.Add(row[0].ToString().ToLower());
            }
            return tableColumnsList;

        }
    }
}
