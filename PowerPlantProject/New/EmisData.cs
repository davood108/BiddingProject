﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PowerPlantProject.ServiceReference1;
using System.ServiceModel;


namespace PowerPlantProject.New
{
    public class EmisData
    {

        public static bool LoadData(PersianDateTime date, WebServiceData webServiceData)
        {
            ServiceReference1.RExternalWebServiceSoapClient dwsMain = new RExternalWebServiceSoapClient();


            // dwsMain.Endpoint.Address = new EndpointAddress("https://emis.igmc.ir/rwss/rexternalwebservice.asmx");                

    
            WebServiceSetting webServiceSetting = WebServiceSetting.GetSetting();
            string serviceCode = "EMNP3321";
            string userName = webServiceSetting.Username;//"webpoet";
            string password = webServiceSetting.Password;//"toos@1397@";

            string loginInfo = String.Format("{0}|{1}", userName, password);
            var parameteres = string.Format("'{0}'|'S'|'M'|NULL", date.PersianDateString);
            ArrayOfAnyType arrayData = new ArrayOfAnyType();
            string columnsHeaders = "";
            string error = "";

            bool success = dwsMain.LoadData(serviceCode, loginInfo, parameteres, ref error, ref columnsHeaders, ref arrayData);
            webServiceData.ArrayData = arrayData;
            webServiceData.ColumnsHeaders = columnsHeaders;
            webServiceData.Error = error;
            return success;

        }
    }


}
