﻿namespace PowerPlantProject
{
    partial class MarketBillCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject1 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject2 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject3 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject4 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MarketBillCheck));
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.btnReport = new System.Windows.Forms.Button();
            this.radioBtnDaily = new System.Windows.Forms.RadioButton();
            this.radioBtnMonthly = new System.Windows.Forms.RadioButton();
            this.radioBtnCustom = new System.Windows.Forms.RadioButton();
            this.myDateRange1 = new UI.MyControl.MyDateRange();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // radGridView1
            // 
            this.radGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView1.BackColor = System.Drawing.Color.Beige;
            this.radGridView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radGridView1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radGridView1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView1.Location = new System.Drawing.Point(2, 134);
            // 
            // radGridView1
            // 
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.FieldName = "PowerPlantCode";
            gridViewTextBoxColumn1.HeaderText = "PowerPlantCode";
            gridViewTextBoxColumn1.Name = "columnPlantCode";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 100;
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.FieldName = "UnitCode";
            gridViewTextBoxColumn2.HeaderText = "Unit";
            gridViewTextBoxColumn2.Name = "ColumnUnitCode";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn3.EnableExpressionEditor = false;
            gridViewTextBoxColumn3.FieldName = "Date";
            gridViewTextBoxColumn3.HeaderText = "Date";
            gridViewTextBoxColumn3.Name = "columnDate";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 90;
            gridViewTextBoxColumn4.EnableExpressionEditor = false;
            gridViewTextBoxColumn4.FieldName = "HourID";
            gridViewTextBoxColumn4.HeaderText = "Hour";
            gridViewTextBoxColumn4.Name = "columnHour";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.Width = 40;
            gridViewTextBoxColumn5.EnableExpressionEditor = false;
            gridViewTextBoxColumn5.FieldName = "BillTypeCode";
            gridViewTextBoxColumn5.HeaderText = "Bill Type Code";
            gridViewTextBoxColumn5.Name = "columnBillTypeCode";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.Width = 100;
            gridViewTextBoxColumn6.EnableExpressionEditor = false;
            gridViewTextBoxColumn6.FieldName = "BillRevision";
            gridViewTextBoxColumn6.HeaderText = "Bill Revision";
            gridViewTextBoxColumn6.Name = "columnBillRevision";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.Width = 70;
            gridViewTextBoxColumn7.EnableExpressionEditor = false;
            gridViewTextBoxColumn7.FieldName = "Declare_Bill";
            gridViewTextBoxColumn7.HeaderText = "Declare Bill";
            gridViewTextBoxColumn7.Name = "columnDeclare_Bill";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn7.Width = 80;
            gridViewTextBoxColumn8.EnableExpressionEditor = false;
            gridViewTextBoxColumn8.FieldName = "Declare_002";
            gridViewTextBoxColumn8.HeaderText = "Declare M002";
            gridViewTextBoxColumn8.Name = "columnDeclare_002";
            gridViewTextBoxColumn8.ReadOnly = true;
            gridViewTextBoxColumn8.Width = 80;
            conditionalFormattingObject1.CellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            conditionalFormattingObject1.CellFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            conditionalFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.ConditionType = Telerik.WinControls.UI.ConditionTypes.Between;
            conditionalFormattingObject1.Name = "NewCondition";
            conditionalFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            conditionalFormattingObject1.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.TValue1 = "0";
            conditionalFormattingObject1.TValue2 = "1";
            conditionalFormattingObject2.CellBackColor = System.Drawing.Color.Red;
            conditionalFormattingObject2.CellFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            conditionalFormattingObject2.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.ConditionType = Telerik.WinControls.UI.ConditionTypes.GreaterOrEqual;
            conditionalFormattingObject2.Name = "NewCondition";
            conditionalFormattingObject2.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            conditionalFormattingObject2.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.TValue1 = "1";
            gridViewTextBoxColumn9.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            gridViewTextBoxColumn9.ConditionalFormattingObjectList.Add(conditionalFormattingObject2);
            gridViewTextBoxColumn9.EnableExpressionEditor = false;
            gridViewTextBoxColumn9.FieldName = "Declare_Diff";
            gridViewTextBoxColumn9.HeaderText = "Declare Diff";
            gridViewTextBoxColumn9.Name = "columnDeclare_Diff";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.Width = 70;
            gridViewTextBoxColumn10.EnableExpressionEditor = false;
            gridViewTextBoxColumn10.FieldName = "Energy_Bill";
            gridViewTextBoxColumn10.HeaderText = "Energy Bill";
            gridViewTextBoxColumn10.Name = "columnEnergy_Bill";
            gridViewTextBoxColumn10.ReadOnly = true;
            gridViewTextBoxColumn10.Width = 80;
            gridViewTextBoxColumn11.EnableExpressionEditor = false;
            gridViewTextBoxColumn11.FieldName = "Energy_Met";
            gridViewTextBoxColumn11.HeaderText = "Energy Met";
            gridViewTextBoxColumn11.Name = "columnEnergy_Met";
            gridViewTextBoxColumn11.ReadOnly = true;
            gridViewTextBoxColumn11.Width = 80;
            conditionalFormattingObject3.CellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            conditionalFormattingObject3.CellFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            conditionalFormattingObject3.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.ConditionType = Telerik.WinControls.UI.ConditionTypes.Between;
            conditionalFormattingObject3.Name = "NewCondition";
            conditionalFormattingObject3.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            conditionalFormattingObject3.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.TValue1 = "0";
            conditionalFormattingObject3.TValue2 = "1";
            conditionalFormattingObject4.CellBackColor = System.Drawing.Color.Red;
            conditionalFormattingObject4.CellFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            conditionalFormattingObject4.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.ConditionType = Telerik.WinControls.UI.ConditionTypes.GreaterOrEqual;
            conditionalFormattingObject4.Name = "NewCondition";
            conditionalFormattingObject4.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            conditionalFormattingObject4.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.TValue1 = "1";
            gridViewTextBoxColumn12.ConditionalFormattingObjectList.Add(conditionalFormattingObject3);
            gridViewTextBoxColumn12.ConditionalFormattingObjectList.Add(conditionalFormattingObject4);
            gridViewTextBoxColumn12.EnableExpressionEditor = false;
            gridViewTextBoxColumn12.FieldName = "Energy_Diff";
            gridViewTextBoxColumn12.HeaderText = "Energy Diff";
            gridViewTextBoxColumn12.Name = "columnEnergy_Diff";
            gridViewTextBoxColumn12.ReadOnly = true;
            gridViewTextBoxColumn12.Width = 70;
            this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12});
            this.radGridView1.MasterTemplate.EnablePaging = true;
            this.radGridView1.MasterTemplate.PageSize = 10000;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radGridView1.Size = new System.Drawing.Size(1048, 288);
            this.radGridView1.TabIndex = 2;
            this.radGridView1.Text = "radGridView1";
            this.toolTip1.SetToolTip(this.radGridView1, "Click the column header to sort");
            // 
            // btnReport
            // 
            this.btnReport.BackColor = System.Drawing.Color.Linen;
            this.btnReport.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnReport.Image = global::PowerPlantProject.Properties.Resources.run88;
            this.btnReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReport.Location = new System.Drawing.Point(407, 45);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(111, 30);
            this.btnReport.TabIndex = 1;
            this.btnReport.Tag = "";
            this.btnReport.Text = "   Report";
            this.btnReport.UseVisualStyleBackColor = false;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // radioBtnDaily
            // 
            this.radioBtnDaily.AutoSize = true;
            this.radioBtnDaily.Checked = true;
            this.radioBtnDaily.Location = new System.Drawing.Point(6, 23);
            this.radioBtnDaily.Name = "radioBtnDaily";
            this.radioBtnDaily.Size = new System.Drawing.Size(48, 17);
            this.radioBtnDaily.TabIndex = 0;
            this.radioBtnDaily.TabStop = true;
            this.radioBtnDaily.Text = "Daily";
            this.radioBtnDaily.UseVisualStyleBackColor = true;
            this.radioBtnDaily.CheckedChanged += new System.EventHandler(this.radioBtnCustom_CheckedChanged);
            // 
            // radioBtnMonthly
            // 
            this.radioBtnMonthly.AutoSize = true;
            this.radioBtnMonthly.Location = new System.Drawing.Point(6, 46);
            this.radioBtnMonthly.Name = "radioBtnMonthly";
            this.radioBtnMonthly.Size = new System.Drawing.Size(62, 17);
            this.radioBtnMonthly.TabIndex = 1;
            this.radioBtnMonthly.Text = "Monthly";
            this.radioBtnMonthly.UseVisualStyleBackColor = true;
            this.radioBtnMonthly.CheckedChanged += new System.EventHandler(this.radioBtnCustom_CheckedChanged);
            // 
            // radioBtnCustom
            // 
            this.radioBtnCustom.AutoSize = true;
            this.radioBtnCustom.Location = new System.Drawing.Point(6, 69);
            this.radioBtnCustom.Name = "radioBtnCustom";
            this.radioBtnCustom.Size = new System.Drawing.Size(60, 17);
            this.radioBtnCustom.TabIndex = 2;
            this.radioBtnCustom.Text = "Custom";
            this.radioBtnCustom.UseVisualStyleBackColor = true;
            this.radioBtnCustom.CheckedChanged += new System.EventHandler(this.radioBtnCustom_CheckedChanged);
            // 
            // myDateRange1
            // 
            this.myDateRange1.BackColor = System.Drawing.Color.Transparent;
            this.myDateRange1.EndDate = "";
            this.myDateRange1.Location = new System.Drawing.Point(90, 23);
            this.myDateRange1.Name = "myDateRange1";
            this.myDateRange1.Size = new System.Drawing.Size(169, 63);
            this.myDateRange1.StartDate = "";
            this.myDateRange1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioBtnDaily);
            this.groupBox1.Controls.Add(this.radioBtnMonthly);
            this.groupBox1.Controls.Add(this.myDateRange1);
            this.groupBox1.Controls.Add(this.radioBtnCustom);
            this.groupBox1.Location = new System.Drawing.Point(51, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(330, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.BackColor = System.Drawing.Color.Linen;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnExport.Image = global::PowerPlantProject.Properties.Resources._1263108119_Microsoft_Office_2007_Excel;
            this.btnExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExport.Location = new System.Drawing.Point(1000, 91);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(47, 37);
            this.btnExport.TabIndex = 1;
            this.btnExport.Tag = "";
            this.btnExport.Text = "Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // MarketBillCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.ClientSize = new System.Drawing.Size(1053, 434);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.radGridView1);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnReport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MarketBillCheck";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Market Bill Checking";
            this.Load += new System.EventHandler(this.MarketInformation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnReport;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private UI.MyControl.MyDateRange myDateRange1;
        private System.Windows.Forms.RadioButton radioBtnDaily;
        private System.Windows.Forms.RadioButton radioBtnMonthly;
        private System.Windows.Forms.RadioButton radioBtnCustom;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}