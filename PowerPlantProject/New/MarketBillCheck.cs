﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NRI.SBS.Common;
using System.Data.SqlClient;
using FarsiLibrary.Utils;
using Telerik.WinControls.UI;
using PowerPlantProject.New;

namespace PowerPlantProject
{
    public partial class MarketBillCheck : Form
    {
        public MarketBillCheck()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;
                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
              
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////

            }
            catch
            {

            }
        }

       
        private void fillgrid(string fromDate,string toDate)
        {
           DataTable dt=new DataTable();
           // date = "1397/07/01";
            dt = Utilities.GetTable(string.Format("SELECT [UnitCode],[Date],[HourID],[BillTypeCode],[BillRevision],[PowerPlantCode],[Energy_Bill],[Energy_Met],[Energy_Diff],[Declare_Bill],[Declare_002],[Declare_Diff]"
              + "FROM [dbo].[View_EnergyDeclareCheck] Where Date BETWEEN '{0}' AND '{1}' ORDER BY [Date],[HourID],[UnitCode]", fromDate, toDate));

            if (dt.Rows.Count==0)
            {
                MessageBox.Show("There is no data for selected date","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }
           radGridView1.DataSource = null;
            radGridView1.DataSource=dt;
        }

        private void MarketInformation_Load(object sender, EventArgs e)
        {
            ConditionalFormattingObject c1 = new ConditionalFormattingObject("condition1", ConditionTypes.Between, "0", "1", false) { CellBackColor = Color.SkyBlue, CellForeColor = Color.LightCoral };
            ConditionalFormattingObject c2 = new ConditionalFormattingObject("condition2", ConditionTypes.GreaterOrEqual, "1", "", false) { CellBackColor = Color.SkyBlue, CellForeColor = Color.Red };            
            foreach (GridViewColumn column in radGridView1.Columns)
            {
                if(column.FieldName.EndsWith("_Diff"))
                {
                    column.ConditionalFormattingObjectList.Add(c1);
                    column.ConditionalFormattingObjectList.Add(c2);
                }
            }
            myDateRange1.Initialize();

            // fillgrid("");
        }



        private void btnReport_Click(object sender, EventArgs e)
        {

            string fromDate = myDateRange1.StartDate;
            string toDate = myDateRange1.EndDate;
            fillgrid(fromDate, toDate);
                     
        }

        private void radioBtnCustom_CheckedChanged(object sender, EventArgs e)
        {
            if ((sender as RadioButton).Checked)
            {
                if (radioBtnDaily.Checked)
                {
                    myDateRange1.RangeType = UI.MyControl.TimeRangeType.Daily;
                }
                else if (radioBtnMonthly.Checked)
                {
                    myDateRange1.RangeType = UI.MyControl.TimeRangeType.Monthly;
                }
                else if (radioBtnCustom.Checked)
                {
                    myDateRange1.RangeType = UI.MyControl.TimeRangeType.Custom;
                }
                myDateRange1.Initialize();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Excel (*.xls)|*.xls";
           
            if (saveFileDialog1.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            if (saveFileDialog1.FileName.Equals(String.Empty))
            {
               MessageBox.Show("Please enter a file name.");
                return;
            }
            string fileName = this.saveFileDialog1.FileName;
          
            bool openExportFile = true;

           ExportRadGrid.RunExportToExcelML(radGridView1,fileName, ref openExportFile);
                  
            if (openExportFile)
            {
                try
                {
                    System.Diagnostics.Process.Start(fileName);
                }
                catch (Exception ex)
                {
                    string message = String.Format("The file cannot be opened on your system.\nError message: {0}", ex.Message);
                    MessageBox.Show(message, "Open File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

    


    

    }
}
