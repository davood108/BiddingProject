﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PowerPlantProject
{
    public partial class Edit_Computers : Form
    {
        Sync Sync1;
        public Edit_Computers()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
             
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure the current computer data be stored?", "Save", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Computer c = new Computer();
                c.ComputerName = textBox1.Text;
                c.ServerName = textBox2.Text;
                c.DBName = textBox3.Text;
                c.UserName = textBox4.Text;
                c.Password = textBox5.Text;
                c.Timeout = Convert.ToInt32(textBox6.Text);
                Sync1.SetComputerInfo(comboBox_Source.Text, c);

                int index = comboBox_Source.SelectedIndex;
                comboBox_Source.Items.Clear();
                string[] x = Sync1.GetComputers();
                for (int i = 0; i < x.Length; i++)
                    comboBox_Source.Items.Add(x[i]);
                comboBox_Source.SelectedIndex = index;
                MessageBox.Show("The current computer data saved");
            }
        }

        private void Edit_Computers_Load(object sender, EventArgs e)
        {

          string x1=  System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                "\\SBS\\PcConfigs.xml";

          string x2 = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                "\\SBS\\Tables.xml";

            Sync1 = new Sync(x1, x2);
            string[] x = Sync1.GetComputers();
            for (int i = 0; i < x.Length; i++)
            {
                comboBox_Source.Items.Add(x[i]);
                
            }
            comboBox_Source.SelectedIndex = 0;
        }

        private void comboBox_Source_SelectedIndexChanged(object sender, EventArgs e)
        {
            Computer c = Sync1.GetComputerInfo(comboBox_Source.Text);
            textBox1.Text = c.ComputerName;
            textBox2.Text = c.ServerName;
            textBox3.Text = c.DBName;
            textBox4.Text = c.UserName;
            textBox5.Text = c.Password;
            textBox6.Text = c.Timeout.ToString();

        }
    }
}
