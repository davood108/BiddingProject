﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using NRI.SBS.Common;
using FarsiLibrary.Utils;
using System.Collections;
using System.IO;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using Auto_Update_Library;

namespace PowerPlantProject
{
    public partial class Metering_Internet_Plant : Form
    {
        ArrayList PPIDArray = new ArrayList();
        ArrayList PPIDType = new ArrayList();
        List<PersianDate> missingDates;
        Thread thread;
        public Metering_Internet_Plant()
        {
           
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void Metering_Internet_Plant_Load(object sender, EventArgs e)
        {
            datePickerFrom.Text = new PersianDate(DateTime.Now).ToString("d");
            datePickerTo.Text = new PersianDate(DateTime.Now.AddDays(1)).ToString("d");
            datePickerFrom.SelectedDateTime = DateTime.Now;
            datePickerTo.SelectedDateTime = DateTime.Now.AddDays(1);
        }
        private void datePickerTo_ValueChanged(object sender, EventArgs e)
        {
            lblDate.Visible = true;
            button1.Enabled = true;
            label3.Visible = false;

            if (datePickerFrom.Text != "")
            {

                DateTime showtime = PersianDateConverter.ToGregorianDateTime(datePickerFrom.Text).Date;
                string labletime = showtime.ToString("d");
                labletime = labletime.Replace("/", "-");
                lblDate.Text = " Date - DA :" + labletime;


            }
            if (datePickerTo.Text != "")
            {

                DateTime showtimeend = PersianDateConverter.ToGregorianDateTime(datePickerTo.Text).Date;
                string labletimeend = showtimeend.ToString("d");
                labletimeend = labletimeend.Replace("/", "-");
                labelenddate.Text = " Date - DA : " + labletimeend;
            }


        }

        private void datePickerFrom_ValueChanged(object sender, EventArgs e)
        {
            lblDate.Visible = true;
            button1.Enabled = true;
            label3.Visible = false;

            if (datePickerFrom.Text != "")
            {

                DateTime showtime = PersianDateConverter.ToGregorianDateTime(datePickerFrom.Text).Date;
                string labletime = showtime.ToString("d");
                labletime = labletime.Replace("/", "-");
                lblDate.Text = " Date - DA :" + labletime;


            }
        }

        private void LS2Interval_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (thread != null)
            {
                thread.Abort();
            }
        }

        private void LS2Interval_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (thread != null)
            {
                thread.Abort();
            }
        }

        private double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Visible = true;

            //--------------------max progress------------------------//
            PPIDArray.Clear();
            PPIDType.Clear();
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            Myda.SelectCommand = new SqlCommand("SELECT PPID FROM PowerPlant", myConnection);
            Myda.Fill(MyDS, "ppid");
            foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
            {
                PPIDArray.Add(MyRow["PPID"].ToString().Trim());
                PPIDType.Add("real");
            }

            for (int i = 0; i < PPIDArray.Count; i++)
            {
                SqlCommand mycom = new SqlCommand();
                mycom.Connection = myConnection;
                mycom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id SELECT @result2 =count(PPID) FROM [PPUnit] WHERE PPID=@id AND PackageType LIKE 'Combined Cycle%'";
                mycom.Parameters.Add("@id", SqlDbType.NChar, 10);
                mycom.Parameters["@id"].Value = PPIDArray[i].ToString();
                mycom.Parameters.Add("@result1", SqlDbType.Int);
                mycom.Parameters["@result1"].Direction = ParameterDirection.Output;
                mycom.Parameters.Add("@result2", SqlDbType.Int);
                mycom.Parameters["@result2"].Direction = ParameterDirection.Output;
                mycom.ExecuteNonQuery();
                int result1 = (int)mycom.Parameters["@result1"].Value;
                int result2 = (int)mycom.Parameters["@result2"].Value;
                if ((result1 > 1) && (result2 > 0))
                {
                    PPIDArray.Add((int.Parse(PPIDArray[i].ToString())) + 1);
                    PPIDType.Add("virtual");
                }
            }

            missingDates = GetDatesBetween();
            int max = (missingDates.Count) * (PPIDArray.Count);
            progressBar1.Maximum = max;
            //------------------------------------------------------------------------
            button1.Enabled = false;
            label3.Visible = true;
            thread = new Thread(LongTask);
            thread.IsBackground = true;
            thread.Start();




        }
        public void UpdateProgressBar(int value, string description)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<int, string>(UpdateProgressBar), new object[] { value, description });
                return;
            }
            // lblTitle.Text = description;

            progressBar1.Value = value;
            Thread.Sleep(1);

        }
        private void LongTask()
        {
            try
            {
                PPIDArray.Clear();
                PPIDType.Clear();

                if (Auto_Update_Library.BaseData.GetInstance().CounterPath != "")
                {
                    DataSet MyDS = new DataSet();
                    SqlDataAdapter Myda = new SqlDataAdapter();
                    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                    myConnection.Open();

                    Myda.SelectCommand = new SqlCommand("SELECT PPID FROM PowerPlant", myConnection);
                    Myda.Fill(MyDS, "ppid");
                    foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
                    {
                        PPIDArray.Add(MyRow["PPID"].ToString().Trim());
                        PPIDType.Add("real");
                    }

                    for (int i = 0; i < PPIDArray.Count; i++)
                    {
                        SqlCommand mycom = new SqlCommand();
                        mycom.Connection = myConnection;
                        mycom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id SELECT @result2 =count(PPID) FROM [PPUnit] WHERE PPID=@id AND PackageType LIKE 'Combined Cycle%'";
                        mycom.Parameters.Add("@id", SqlDbType.NChar, 10);
                        mycom.Parameters["@id"].Value = PPIDArray[i].ToString();
                        mycom.Parameters.Add("@result1", SqlDbType.Int);
                        mycom.Parameters["@result1"].Direction = ParameterDirection.Output;
                        mycom.Parameters.Add("@result2", SqlDbType.Int);
                        mycom.Parameters["@result2"].Direction = ParameterDirection.Output;
                        mycom.ExecuteNonQuery();
                        int result1 = (int)mycom.Parameters["@result1"].Value;
                        int result2 = (int)mycom.Parameters["@result2"].Value;
                        if ((result1 > 1) && (result2 > 0))
                        {
                            PPIDArray.Add((int.Parse(PPIDArray[i].ToString())) + 1);
                            PPIDType.Add("virtual");
                        }
                    }

                    string msg = "";
                    missingDates = GetDatesBetween();
                    int p = 1;
                    foreach (PersianDate date in missingDates)
                    {
                        msg = "";
                        for (int k = 0; k < PPIDArray.Count; k++)
                        {
                            if ((PPIDArray[k].ToString() != "0") && (PPIDType[k].ToString() == "real"))
                            {
                                UpdateProgressBar(p, "Getting Data For");
                                msg += ReadLS2Filesactive(k, date) + "\r\n";
                                msg += ReadLS2Filesreactive(k, date) + "\r\n";
                                p++;
                            }
                        }

                    }
                    UpdateProgressBar(progressBar1.Maximum, "");
                    //////////////////errors

                    if (msg.Contains("error") && msg.Length < 4400)
                    {

                        MessageBox.Show(msg.Replace("error", ""));
                    }
                    else if (!msg.Contains("Invalid Path"))
                    {
                        MessageBox.Show("Download Complete");

                    }
                    else
                    {
                        MessageBox.Show("Invalid Path");

                    }
                }
                else
                {
                    MessageBox.Show(" Counter Path Is Not Set !");
                }
            }
            catch
            {

            }
            //
        }
        public string folderFormat(string date, string ppid, string type)
        {

            //Year/2char
            //Year/4char
            //month/2char
            //month/1char
            //day/2char
            //day/1char
            //PlantName/English
            //PlantName/Farsi
            //MonthName/Finglish
            //Year-6MaheAval
            //Year-6MaheDovom
            string farsi = "";
            string english = "";
            DataTable vv = Utilities.GetTable("select * from powerplant where ppid='" + ppid + "'");
            if (vv.Rows.Count > 0)
            {
                farsi = vv.Rows[0]["PNameFarsi"].ToString().Trim();
                english = vv.Rows[0]["PPName"].ToString().Trim();

            }
            string xcmonth = "Year-6MaheAval";
            string year = date.Substring(0, 4).Trim();
            string month = date.Substring(5, 2).Trim();
            string day = date.Substring(8, 2).Trim();
            if (int.Parse(month) > 6) xcmonth = "Year-6MaheDovom";
            string pathname = "";
            DataTable dddd = Utilities.GetTable("select * from FilesNameFormat where filename='" + type + "'");
            if (dddd.Rows.Count > 0 && dddd.Rows[0]["FoldersFormat"].ToString().Trim() != "")
            {

                string[] x = dddd.Rows[0]["FoldersFormat"].ToString().Trim().Split('*');

                for (int i = 0; i < x.Length; i++)
                {

                    if (x[i].Trim() == "Plantname/english")
                    {
                        pathname += @"\" + english;
                    }
                    else if (x[i].Trim() == "Plantname/Farsi")
                    {
                        pathname += @"\" + farsi;
                    }
                    else if (x[i].Trim() == "PlantID")
                    {
                        pathname += @"\" + ppid;
                    }

                    else if (x[i].Trim() == "space")
                    {
                        pathname += @"\" + " ";
                    }
                    else if (x[i].Trim() == "Year/2char")
                    {
                        pathname += @"\" + year.Substring(2, 2).Trim();
                    }
                    else if (x[i].Trim() == "Year/4char")
                    {
                        pathname += @"\" + year.Trim();
                    }
                    else if (x[i].Trim() == "month/2char")
                    {
                        pathname += @"\" + month.Trim();

                    }
                    else if (x[i].Trim() == "month/1char")
                    {
                        //pathname += @"\" + month.Substring(1, 1).Trim();
                        pathname += @"\" + int.Parse(month);
                    }
                    else if (x[i].Trim() == "day/2char")
                    {
                        pathname += @"\" + day.Trim();

                    }
                    else if (x[i].Trim() == "day/1char")
                    {
                        pathname += @"\" + day.Substring(1, 1).Trim();

                    }
                    else if (x[i].Trim() == "MonthName/Finglish")
                    {
                        string Sendmonth = int.Parse(month).ToString().Trim();
                        switch (Sendmonth)
                        {
                            case "1":
                                Sendmonth = "Farvardin";
                                break;
                            case "2":
                                Sendmonth = "Ordibehesht";
                                break;
                            case "3":
                                Sendmonth = "Khordad";
                                break;
                            case "4":
                                Sendmonth = "Tir";
                                break;
                            case "5":
                                Sendmonth = "Mordad";
                                break;
                            case "6":
                                Sendmonth = "Shahrivar";
                                break;
                            case "7":
                                Sendmonth = "Mehr";
                                break;
                            case "8":
                                Sendmonth = "Aban";
                                break;
                            case "9":
                                Sendmonth = "Azar";
                                break;
                            case "10":
                                Sendmonth = "Day";
                                break;
                            case "11":
                                Sendmonth = "Bahman";
                                break;
                            case "12":
                                Sendmonth = "Esfand";
                                break;

                        }
                        pathname += @"\" + Sendmonth.ToString().Trim();
                    }
                    else if (x[i].Trim() == "Date Without Seperator/8char")
                    {
                        string strDate = date;
                        strDate = strDate.Remove(7, 1);
                        strDate = strDate.Remove(4, 1);
                        pathname += @"\" + strDate;
                    }
                    else
                    {
                        pathname += @"\" + x[i];
                    }

                }



                return pathname;
            }
            return pathname;
        }
        public string NameFormat(string date, string ppid, string pnamefarsi, string type)
        {
            //FRM002
            //FRM005
            //FRM0022
            //M002
            //M005
            //M009
            //Plantname/english
            //Plantname/Farsi
            //PlantID
            //Date Without Seperator/8char
            //Date With Seperator/10char
            //(
            //)
            //_
            //-
            //space
            //VersionNumber/max10

            string excelname = "";



            string english = "";
            DataTable dv = Utilities.GetTable("select * from powerplant where ppid='" + ppid + "'");
            if (dv.Rows.Count > 0)
            {
                pnamefarsi = dv.Rows[0]["PNameFarsi"].ToString().Trim();
                english = dv.Rows[0]["PPName"].ToString().Trim();

            }

            string year = date.Substring(0, 4).Trim();
            string month = date.Substring(5, 2).Trim();
            string day = date.Substring(8, 2).Trim();

            DataTable dddd = Utilities.GetTable("select * from FilesNameFormat where filename='" + type + "'");
            if (dddd.Rows.Count > 0 && dddd.Rows[0]["ExcelName"].ToString().Trim() != "")
            {

                string[] x = dddd.Rows[0]["ExcelName"].ToString().Trim().Split('*');

                for (int i = 0; i < x.Length; i++)
                {

                    if (x[i].Trim() == "Plantname/english")
                    {
                        excelname += english;
                    }
                    else if (x[i].Trim() == "Plantname/Farsi")
                    {
                        excelname += pnamefarsi;
                    }
                    else if (x[i].Trim() == "PlantID")
                    {
                        excelname += ppid;
                    }
                    else if (x[i].Trim() == "Date Without Seperator/8char")
                    {
                        string strDate = date;
                        strDate = strDate.Remove(7, 1);
                        strDate = strDate.Remove(4, 1);
                        excelname += strDate;
                    }

                    else if (x[i].Trim() == "Date With Seperator/10char")
                    {
                        excelname += date;
                    }
                    else if (x[i].Trim() == "Year/2char")
                    {
                        excelname += year.Substring(2, 2).Trim();
                    }
                    else if (x[i].Trim() == "Year/4char")
                    {
                        excelname += year.Trim();
                    }
                    else if (x[i].Trim() == "month/2char")
                    {
                        excelname += month.Trim();

                    }
                    else if (x[i].Trim() == "month/1char")
                    {
                        //excelname += month.Substring(1, 1).Trim();
                        excelname += int.Parse(month);
                    }
                    else if (x[i].Trim() == "day/2char")
                    {
                        excelname += day.Trim();

                    }
                    else if (x[i].Trim() == "day/1char")
                    {
                        excelname += day.Substring(1, 1).Trim();

                    }
                    else if (x[i].Trim() == "space")
                    {
                        excelname += " ";
                    }
                    else if (x[i].Trim() == "VersionNumber/max10")
                    {
                        //excelname += "1";
                    }
                    else
                    {
                        excelname += x[i];
                    }

                }



                return excelname;
            }
            return excelname;
        }
        private string ReadLS2Filesactive(int k, PersianDate date)
        {
            if (!Auto_Update_Library.BaseData.GetInstance().Useftpcounter)
            {
                string price = "active";
                string makename = NameFormat(date.ToString("d"), "", "", "Counters");

                DataTable dde = Utilities.GetTable("select * from FilesNameFormat where filename='Counters'");
                if (dde.Rows.Count > 0)
                {
                    if (dde.Rows[0]["SheetName"].ToString().Trim() != "" || dde.Rows[0]["SheetName"].ToString().Trim() != null)
                    {
                        price = dde.Rows[0]["SheetName"].ToString();
                    }

                }

                //////////////////////////////////////////////////////////////////////////////


                DateTime Miladi = PersianDateConverter.ToGregorianDateTime(date).Date;
                string smiladi = Miladi.ToString("d");
                string[] splitmiladi = smiladi.Split('-');

                string yearpath = splitmiladi[0];
                string monthpath = splitmiladi[1];
                string daypath = splitmiladi[2];

                string virtualpath = @"\" + yearpath + @"\" + yearpath + monthpath + daypath;
                string pathname = folderFormat(smiladi, "", "Counters");
                if (pathname != "") virtualpath = pathname;



                DataTable plantname = Utilities.ls2returntbl("select PPName from dbo.PowerPlant where PPID='" + PPIDArray[k] + "'");
                string ppnamek = plantname.Rows[0][0].ToString().Trim();

                string status = "Incomplete Data or Serial Number For Plant <<<<" + ppnamek + " In Date :" + date + ">>>>\r\n";
                int loopcount = 0;
                DataTable UnitsData = null;
                UnitsData = Utilities.ls2returntbl("SELECT UnitCode,PackageCode,PowerSerial,ConsumedSerial,StateConnection,BusNumber FROM [UnitsDataMain] WHERE PPID='" + PPIDArray[k] + "'");
                foreach (DataRow MyRow in UnitsData.Rows)
                {
                    for (int column = 0; column < UnitsData.Columns.Count; column++)
                    {
                        if (MyRow[column].ToString() == "") MyRow[column] = "0";
                        MyRow[4] = "0";
                        MyRow[5] = "0";
                    }
                    int PowerCount = 0, ConsumeCount = 0;
                    foreach (DataRow SecondRow in UnitsData.Rows)
                    {
                        if (SecondRow[2].ToString().Trim() == MyRow[2].ToString().Trim()) PowerCount++;
                        if (SecondRow[3].ToString().Trim() == MyRow[3].ToString().Trim()) ConsumeCount++;
                    }
                    MyRow[4] = PowerCount;
                    MyRow[5] = ConsumeCount;
                }
                //try
                //{
                int index = 0;
                foreach (DataRow MyRow in UnitsData.Rows)
                {
                  // DataTable d = utilities.ls2returntbl("SELECT packagetype FROM [UnitsDataMain] WHERE PPID='" + PPIDArray[k] + "'and unitcode='"+MyRow[0].ToString().Trim()+"'");
                    //for (int Count = 2; Count < 4; Count++)
                    //{
                        string temppath = BaseData.GetInstance().M009Path;
                        //temppath += @"\" + MyRow[Count].ToString().Trim() + ".xls";

                        string Path = Auto_Update_Library.BaseData.GetInstance().CounterPath;
                        Path += virtualpath ;
                        Path += @"\"+makename+".xls";
                        //if (File.Exists(temppath)) File.Delete(temppath);
                        if (File.Exists(Path))
                        {
                            //Save AS ls2file to xls file
                          
                            Excel.Application exobj = new Excel.Application();
                            exobj.Visible = false;
                            exobj.UserControl = true;
                            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                            Excel.Workbook book = (Excel.Workbook)exobj.Workbooks.Add(Path);
                            //Path = Path.Replace("ls2", "xls");
                            //book.SaveAs(Path, Excel.XlFileFormat.xlWorkbookNormal, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                            ///////////////
                            book.Close(false, book, Type.Missing);
                            exobj.Workbooks.Close();
                            exobj.Quit();
                          

                            //////////////////////

                            //Read From Excel File
                            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Path+ ";Extended Properties=Excel 8.0";
                            OleDbConnection objConn = new OleDbConnection(sConnectionString);
                            objConn.Open();
                          
                            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                            objAdapter1.SelectCommand = objCmdSelect;
                            DataSet objDataset1 = new DataSet();
                            objAdapter1.Fill(objDataset1);
                            DataTable TempTable = objDataset1.Tables[0];
                            objConn.Close();

                            //if (Count == 2) //Power Serial Type
                            //{
                              
                                //while (index < TempTable.Rows.Count)
                                //{
                                    if (TempTable.Rows[index][1].ToString().Trim() != "")
                                    {
                                        int finalindex = index;
                                        //Seperate Time
                                        string myDateTime = TempTable.Rows[index][0].ToString().Trim();
                                        string TempTime = myDateTime.Substring(myDateTime.Length - 11, 11).Trim();
                                        string myDate = myDateTime.Remove(myDateTime.Length - 11, 11).Trim();
                                        char[] temp = new char[1];
                                        temp[0] = ':';
                                        int pos1 = TempTime.IndexOfAny(temp);
                                        string myTime = TempTime.Remove(pos1);
                                        if ((TempTime.Contains("PM")) && (myTime != "12"))
                                            myTime = (int.Parse(myTime) + 12).ToString();
                                        if ((TempTime.Contains("AM")) && (myTime == "12"))
                                            myTime = "0";
                                        //Seperate Date
                                        System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
                                        DateTime CurDate = DateTime.Parse(myDate);
                                        int imonth = pr.GetMonth(CurDate);
                                        int iyear = pr.GetYear(CurDate);
                                        int iday = pr.GetDayOfMonth(CurDate);
                                        string day = iday.ToString();
                                        if (int.Parse(day) < 10) day = "0" + day;
                                        string month = imonth.ToString();
                                        if (int.Parse(month) < 10) month = "0" + month;
                                        myDate = iyear.ToString() + "/" + month + "/" + day;
                                        //-------------------------- is6secondarymonth --------------------------------//


                                        //if (int.Parse(month) >= 6)
                                        //{
                                        //    if (TempTable.Rows[finalindex - 1][1].ToString().Trim() != "")
                                        //        finalindex = index - 1;
                                        //}
                                        //---------------------------------------------------------------------------
                                        //Is There any Data for this Plant(and type) at this Date in the MainFRM009 and DetailFRM009?
                                        DataTable IsThere = null;
                                        string bblock = MyRow[0].ToString().Trim();
                                        if (bblock.Contains("cc")) bblock = bblock.Replace(" cc", "").Trim();
                                        if (bblock.Contains("Gas")) bblock = bblock.Replace("Gas", "G").Trim();
                                        if (bblock.Contains("Steam")) bblock = bblock.Replace("Steam", "S").Trim();

                                        for (int h = 0; h < 24; h++)
                                        {
                                            IsThere = Utilities.ls2returntbl("SELECT COUNT(*) FROM DetailFRM009 WHERE PPID='" + PPIDArray[k] + "' AND Block='" + MyRow[0].ToString().Trim() + "' AND TargetMarketDate='" + myDate + "' AND Hour=" + h);
                                            int check = int.Parse(IsThere.Rows[0][0].ToString());

                                            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                                            myConnection.Open();
                                            SqlCommand MyCom = new SqlCommand();
                                            MyCom.Connection = myConnection;
                                            MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                                            MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                                            MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                                            MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                                            MyCom.Parameters.Add("@PCode", SqlDbType.Int);
                                            MyCom.Parameters.Add("@P", SqlDbType.Real);
                                            MyCom.Parameters.Add("@QC", SqlDbType.Real);
                                            MyCom.Parameters.Add("@QL", SqlDbType.Real);

                                            //Insert Into DATABASE
                                            if (check == 0)
                                                MyCom.CommandText = "INSERT INTO [DetailFRM009] (TargetMarketDate,PPID" +
                                                ",Block,PackageCode,Hour,P,QC,QL) VALUES (@tdate,@id,@block,@PCode,@hour,@P,@QC,@QL)";
                                            else
                                                MyCom.CommandText = "UPDATE DetailFRM009 SET P=@P,QC=@QC,QL=@QL WHERE " +
                                                "TargetMarketDate=@tdate AND PPID=@id AND Block=@block AND " +
                                                "PackageCode=@PCode AND Hour=@hour";

                                            MyCom.Parameters["@id"].Value = PPIDArray[k].ToString();
                                            MyCom.Parameters["@tdate"].Value = myDate;
                                            MyCom.Parameters["@block"].Value = MyRow[0].ToString().Trim();
                                            MyCom.Parameters["@PCode"].Value = MyRow[1].ToString().Trim();
                                            MyCom.Parameters["@hour"].Value = h;
                                            if (TempTable.Rows[finalindex][h+2].ToString().Trim() != "0")
                                                MyCom.Parameters["@P"].Value = (MyDoubleParse(TempTable.Rows[finalindex][h+2].ToString().Trim())) ;
                                                                                   
                                                ////////////if all is zero//////////////////////////////////
                                            else
                                                MyCom.Parameters["@P"].Value = 0.0;
                                            /////////////////////////////////////////////////////////////////

                                            //if (TempTable.Rows[finalindex][4].ToString().Trim() != "")
                                            //    MyCom.Parameters["@QC"].Value = MyDoubleParse(TempTable.Rows[finalindex][4].ToString().Trim()) / 1000000.0;
                                             MyCom.Parameters["@QC"].Value = 0;
                                            //if (TempTable.Rows[finalindex][5].ToString().Trim() != "")
                                            //    MyCom.Parameters["@QL"].Value = MyDoubleParse(TempTable.Rows[finalindex][5].ToString().Trim()) / 1000000.0;
                                            MyCom.Parameters["@QL"].Value = 0;

                                            try
                                            {
                                                MyCom.ExecuteNonQuery();
                                            }
                                            catch (Exception exp)
                                            {
                                                string str = exp.Message;
                                            }
                                            myConnection.Close();
                                        }
                                      
                                    }
                           
                            System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                            if (loopcount == 0)
                            {
                                status = status.Replace("Incomplete", "Complete");
                            }
                        }
                        else
                        {                           
                                if (loopcount != 0)
                                {
                                    status = status.Replace("Complete", "Incomplete");
                                }
                                status += "error" + " Unit:  " + MyRow[0].ToString().Trim() + "   ";
                            
                            loopcount++;
                        }
                        index++;               
                 

                }


                return status;
            }
            else
            {
                return "Invalid Path";
            }


        }

        private string ReadLS2Filesreactive(int k, PersianDate date)
        {
            if (!Auto_Update_Library.BaseData.GetInstance().Useftpcounter)
            {

                string price = "reactive";
                string makename ="re"+NameFormat(date.ToString("d"), "", "", "Counters");


                DataTable dde = Utilities.GetTable("select * from FilesNameFormat where filename='Counters'");
                if (dde.Rows.Count > 0)
                {
                    if (dde.Rows[0]["SheetName"].ToString().Trim() != "" || dde.Rows[0]["SheetName"].ToString().Trim() != null)
                    {
                        price ="re"+dde.Rows[0]["SheetName"].ToString();
                    }

                }
                //////////////////////////////////////////////

                DateTime Miladi = PersianDateConverter.ToGregorianDateTime(date).Date;
                string smiladi = Miladi.ToString("d");
                string[] splitmiladi = smiladi.Split('-');

                string yearpath = splitmiladi[0];
                string monthpath = splitmiladi[1];
                string daypath = splitmiladi[2];

                string virtualpath = @"\" + yearpath + @"\" + yearpath + monthpath + daypath;
                string pathname = folderFormat(smiladi, "", "Counters");
                if (pathname != "") virtualpath = pathname;



                DataTable plantname = Utilities.ls2returntbl("select PPName from dbo.PowerPlant where PPID='" + PPIDArray[k] + "'");
                string ppnamek = plantname.Rows[0][0].ToString().Trim();

                string status = "Incomplete Data or Serial Number For Plant <<<<" + ppnamek + " In Date :" + date + ">>>>\r\n";
                int loopcount = 0;
                DataTable UnitsData = null;
                UnitsData = Utilities.ls2returntbl("SELECT UnitCode,PackageCode,PowerSerial,ConsumedSerial,StateConnection,BusNumber FROM [UnitsDataMain] WHERE PPID='" + PPIDArray[k] + "'");
                foreach (DataRow MyRow in UnitsData.Rows)
                {
                    for (int column = 0; column < UnitsData.Columns.Count; column++)
                    {
                        if (MyRow[column].ToString() == "") MyRow[column] = "0";
                        MyRow[4] = "0";
                        MyRow[5] = "0";
                    }
                    int PowerCount = 0, ConsumeCount = 0;
                    foreach (DataRow SecondRow in UnitsData.Rows)
                    {
                        if (SecondRow[2].ToString().Trim() == MyRow[2].ToString().Trim()) PowerCount++;
                        if (SecondRow[3].ToString().Trim() == MyRow[3].ToString().Trim()) ConsumeCount++;
                    }
                    MyRow[4] = PowerCount;
                    MyRow[5] = ConsumeCount;
                }
                //try
                //{
                int index = 0;
               
                    string temppath = BaseData.GetInstance().M009Path;
                   

                    string Path = Auto_Update_Library.BaseData.GetInstance().CounterPath;
                    Path += virtualpath;
                    Path += @"\" + makename+".xls";
                 
                    if (File.Exists(Path))
                    {


                        Excel.Application exobj = new Excel.Application();
                        exobj.Visible = false;
                        exobj.UserControl = true;
                        System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                        //Excel.Workbook book = (Excel.Workbook)exobj.Workbooks.Add(Path);
                        //Path = Path.Replace("ls2", "xls");
                        //book.SaveAs(Path, Excel.XlFileFormat.xlWorkbookNormal, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                        ///////////////
                        //book.Close(false, book, Type.Missing);
                        exobj.Workbooks.Close();
                        exobj.Quit();
                        //////////////////////

                        //Read From Excel File
                        String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Path + ";Extended Properties=Excel 8.0";
                        OleDbConnection objConn = new OleDbConnection(sConnectionString);
                        objConn.Open();
                        //string price = "reactive";
                        OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                        objAdapter1.SelectCommand = objCmdSelect;
                        DataSet objDataset1 = new DataSet();
                        objAdapter1.Fill(objDataset1);
                        DataTable TempTable = objDataset1.Tables[0];
                        objConn.Close();                      
                        if (TempTable.Rows[index][1].ToString().Trim() != "")
                        {
                            int finalindex = index;
                            //Seperate Time
                            string myDateTime = TempTable.Rows[index][0].ToString().Trim();
                            string TempTime = myDateTime.Substring(myDateTime.Length - 11, 11).Trim();
                            string myDate = myDateTime.Remove(myDateTime.Length - 11, 11).Trim();
                            char[] temp = new char[1];
                            temp[0] = ':';
                            int pos1 = TempTime.IndexOfAny(temp);
                            string myTime = TempTime.Remove(pos1);
                            if ((TempTime.Contains("PM")) && (myTime != "12"))
                                myTime = (int.Parse(myTime) + 12).ToString();
                            if ((TempTime.Contains("AM")) && (myTime == "12"))
                                myTime = "0";
                            //Seperate Date
                            System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
                            DateTime CurDate = DateTime.Parse(myDate);
                            int imonth = pr.GetMonth(CurDate);
                            int iyear = pr.GetYear(CurDate);
                            int iday = pr.GetDayOfMonth(CurDate);
                            string day = iday.ToString();
                            if (int.Parse(day) < 10) day = "0" + day;
                            string month = imonth.ToString();
                            if (int.Parse(month) < 10) month = "0" + month;
                            myDate = iyear.ToString() + "/" + month + "/" + day;
                           
                            //Is There any Data for this Plant(and type) at this Date in the MainFRM009 and DetailFRM009?
                            DataTable IsThere = null;

                            foreach (DataRow MyRow in UnitsData.Rows)
                            {
                                for (int h = 0; h < 24; h++)
                                {
                                    IsThere = Utilities.ls2returntbl("SELECT COUNT(*) FROM DetailFRM009 WHERE PPID='" + PPIDArray[k] + "' AND TargetMarketDate='" + myDate + "' AND Hour=" + h);
                                    int check = int.Parse(IsThere.Rows[0][0].ToString());

                                    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                                    myConnection.Open();
                                    SqlCommand MyCom = new SqlCommand();
                                    MyCom.Connection = myConnection;
                                    MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                                    MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                                    MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                                    MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                                    MyCom.Parameters.Add("@PCode", SqlDbType.Int);

                                    MyCom.Parameters.Add("@QC", SqlDbType.Real);
                                    MyCom.Parameters.Add("@QL", SqlDbType.Real);

                                    //Insert Into DATABASE
                                    if (check == 0)
                                        MyCom.CommandText = "INSERT INTO [DetailFRM009] (TargetMarketDate,PPID" +
                                        ",Block,PackageCode,Hour,QC,QL) VALUES (@tdate,@id,@block,@PCode,@hour,@QC,@QL)";
                                    else
                                        MyCom.CommandText = "UPDATE DetailFRM009 SET QC=@QC,QL=@QL WHERE " +
                                        "TargetMarketDate=@tdate AND PPID=@id AND Block=@block AND " +
                                        "PackageCode=@PCode AND Hour=@hour";

                                    MyCom.Parameters["@id"].Value = PPIDArray[k].ToString();
                                    MyCom.Parameters["@tdate"].Value = myDate;
                                    MyCom.Parameters["@block"].Value = MyRow[0].ToString().Trim();
                                    MyCom.Parameters["@PCode"].Value = MyRow[1].ToString().Trim();
                                    MyCom.Parameters["@hour"].Value = h;


                                    if ((h + 1) * 2 <= 48)
                                    {
                                        if (TempTable.Rows[1][(h + 1) * 2].ToString().Trim() != "")
                                            MyCom.Parameters["@QL"].Value = (MyDoubleParse(TempTable.Rows[1][(h + 1) * 2].ToString().Trim()) / UnitsData.Rows.Count) / 1000000.0;
                                        else MyCom.Parameters["@QL"].Value = 0;
                                        if (TempTable.Rows[1][((h + 1) * 2) + 1].ToString().Trim() != "")
                                            MyCom.Parameters["@QC"].Value = (MyDoubleParse(TempTable.Rows[1][((h + 1) * 2) + 1].ToString().Trim()) / UnitsData.Rows.Count) / 1000000.0;
                                        else MyCom.Parameters["@QC"].Value = 0;
                                    }

                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        string str = exp.Message;
                                    }
                                    myConnection.Close();
                                }

                            }

                        }

                       System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                        if (loopcount == 0)
                        {
                            status = status.Replace("Incomplete", "Complete");
                        }
                      
                    }
                    else
                    {
                        if (loopcount != 0)
                        {
                            status = status.Replace("Complete", "Incomplete");
                        }
                        status += "error" ;

                        loopcount++;
                    }
                    index++;



                    

                return status;
            }
            else
            {
                return "Invalid Path";
            }
          

        }

        private List<PersianDate> GetDatesBetween()
        {


            if (datePickerFrom != null)
            {
                List<PersianDate> missingDates = new List<PersianDate>();


                DateTime dateFrom = datePickerFrom.SelectedDateTime.Date;
                DateTime dateTo = datePickerTo.SelectedDateTime.Date;
                missingDates.Add(dateFrom);

                TimeSpan span = dateTo - dateFrom;
                while (span.Days > 0)
                {
                    dateFrom = dateFrom.AddDays(1);
                    missingDates.Add(new PersianDate(dateFrom));
                    span = dateTo - dateFrom;
                }
                return missingDates;
            }
            else
                return null;
        }
    }
}
