﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using NRI.DMS.Common;

namespace PowerPlantProject
{
    public partial class Form6 : Form
    {
        string ConStr;
        public Form6()
        {
            InitializeComponent();
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            UpdateCal.SelectedDateTime = System.DateTime.Now;
            //UpdateCal.IsNull = true;
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            if ((!UpdateCal.IsNull) && (errorProvider1.GetError(PmaxTb) == "") && (errorProvider1.GetError(PminTb) == "") &&
            (errorProvider1.GetError(CapacityTb) == "") && (errorProvider1.GetError(HourTb) == "") &&
            (errorProvider1.GetError(DayTb) == "") && (errorProvider1.GetError(GasTb) == "") && (errorProvider1.GetError(SteamTb) == "") &&
            (errorProvider1.GetError(CCTb) == "") && (errorProvider1.GetError(GasPriceTb) == "") &&
            (errorProvider1.GetError(MazutPriceTb) == "") && (errorProvider1.GetError(GasOilPriceTb) == ""))
            {
                SqlCommand MyCom = new SqlCommand();
                MyCom.CommandText = "INSERT INTO [BaseData] (Date,MarketPriceMax,MarketPriceMin,CapacityPayment,ProposalHour" +
                ",ProposalDay,GasProduction,SteamProduction,CCProduction,GasPrice,MazutPrice,GasOilPrice) VALUES " +
                "(@date,@Pmax,@Pmin,@cap,@hour,@day,@gas,@steam,@cc,@gp,@mp,@gop)";
                MyCom.Connection = new SqlConnection(ConnectionManager.ConnectionString);

                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = UpdateCal.Text;
                MyCom.Parameters.Add("@Pmax", SqlDbType.Real);
                MyCom.Parameters["@Pmax"].Value = double.Parse(PmaxTb.Text);
                MyCom.Parameters.Add("@Pmin", SqlDbType.Real);
                MyCom.Parameters["@Pmin"].Value = double.Parse(PminTb.Text);
                MyCom.Parameters.Add("@cap", SqlDbType.Real);
                MyCom.Parameters["@cap"].Value = double.Parse(CapacityTb.Text);
                MyCom.Parameters.Add("@hour", SqlDbType.Real);
                MyCom.Parameters["@hour"].Value = double.Parse(HourTb.Text);
                MyCom.Parameters.Add("@day", SqlDbType.Real);
                MyCom.Parameters["@day"].Value = double.Parse(DayTb.Text);
                MyCom.Parameters.Add("@gas", SqlDbType.Real);
                MyCom.Parameters["@gas"].Value = double.Parse(GasTb.Text);
                MyCom.Parameters.Add("@steam", SqlDbType.Real);
                MyCom.Parameters["@steam"].Value = double.Parse(SteamTb.Text);
                MyCom.Parameters.Add("@cc", SqlDbType.Real);
                MyCom.Parameters["@cc"].Value = double.Parse(CCTb.Text);
                MyCom.Parameters.Add("@gp", SqlDbType.Real);
                MyCom.Parameters["@gp"].Value = double.Parse(GasPriceTb.Text);
                MyCom.Parameters.Add("@mp", SqlDbType.Real);
                MyCom.Parameters["@mp"].Value = double.Parse(MazutPriceTb.Text);
                MyCom.Parameters.Add("@gop", SqlDbType.Real);
                MyCom.Parameters["@gop"].Value = double.Parse(GasOilPriceTb.Text);

                try
                {
                    MyCom.ExecuteNonQuery();
                    this.Close();
                }
                catch (Exception exp)
                {
                    string str = exp.Message;
                    if (str.Contains("PRIMARY KEY"))
                        MessageBox.Show("Data for selected date has saved before!");
                }
                finally
                {
                    MyCom.Connection.Close();
                }
            }
        }

        private void PmaxTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(PmaxTb.Text);
                errorProvider1.SetError(PmaxTb, "");
            }
            catch
            {
                errorProvider1.SetError(PmaxTb, "Invalid Value!");
            }
        }

        private void PminTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(PminTb.Text);
                errorProvider1.SetError(PminTb, "");
            }
            catch
            {
                errorProvider1.SetError(PminTb, "Invalid Value!");
            }
        }

        private void CapacityTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(CapacityTb.Text);
                errorProvider1.SetError(CapacityTb, "");
            }
            catch
            {
                errorProvider1.SetError(CapacityTb, "Invalid Value!");
            }
        }

        private void HourTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(HourTb.Text);
                errorProvider1.SetError(HourTb, "");
            }
            catch
            {
                errorProvider1.SetError(HourTb, "Invalid Value!");
            }
        }

        private void DayTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(DayTb.Text);
                errorProvider1.SetError(DayTb, "");
            }
            catch
            {
                errorProvider1.SetError(DayTb, "Invalid Value!");
            }
        }

        private void GasTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(GasTb.Text);
                errorProvider1.SetError(GasTb, "");
            }
            catch
            {
                errorProvider1.SetError(GasTb, "Invalid Value!");
            }
        }

        private void SteamTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(SteamTb.Text);
                errorProvider1.SetError(SteamTb, "");
            }
            catch
            {
                errorProvider1.SetError(SteamTb, "Invalid Value!");
            }
        }

        private void CCTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(CCTb.Text);
                errorProvider1.SetError(CCTb, "");
            }
            catch
            {
                errorProvider1.SetError(CCTb, "Invalid Value!");
            }
        }

        private void GasPriceTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(GasPriceTb.Text);
                errorProvider1.SetError(GasPriceTb, "");
            }
            catch
            {
                errorProvider1.SetError(GasPriceTb, "Invalid Value!");
            }

        }

        private void MazutPriceTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(MazutPriceTb.Text);
                errorProvider1.SetError(MazutPriceTb, "");
            }
            catch
            {
                errorProvider1.SetError(MazutPriceTb, "Invalid Value!");
            }

        }

        private void GasOilPriceTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(GasOilPriceTb.Text);
                errorProvider1.SetError(GasOilPriceTb, "");
            }
            catch
            {
                errorProvider1.SetError(GasOilPriceTb, "Invalid Value!");
            }
        }


    }
}
