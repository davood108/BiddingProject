﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using Dundas.Charting.WinControl;
using System.Windows.Forms;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    public partial class PlotChart1 : Form
    {
 
        public string PlantId { get; set; }
        string[] arr ;
        bool IsEstimated;
        public string ppid = "";

        public PlotChart1(int printPlotType, bool estimate,int keyNRI, string strDate, string endDate, string[] itmName,int[] itemInx,string PPID)
        {
          
           // PrintFinancialReportEx pfrE = new PrintFinancialReportEx();
            
             //string strDate=pfrE.faDatePickerbill.Text;
             //string endDate=pfrE.faDatePicker1.Text;
            IsEstimated = estimate;
            InitializeComponent();
            ppid = PPID;
            /// try
            //{
            //    this.BackColor = FormColors.GetColor().Formbackcolor;

            //    //////////////////////////buttons/////////////////////////////

            //    foreach (Control c in this.Controls)
            //    {
            //        if (c is Button)
            //            c.BackColor = FormColors.GetColor().Buttonbackcolor;
            //    }

            //    //////////////////////////////panel,groupbox/////////////////////////////;
            //    foreach (Control c in this.Controls)
            //    {
            //        if (c is Panel || c is GroupBox)
            //            c.BackColor = FormColors.GetColor().Panelbackcolor;

            //    }
            //    //////////////////////////////////////////////////////////////////////////
            ////    ////////////////////////////////textbox /////////////////////////////////////

            //    foreach (Control c in this.Controls)
            //    {
            //        if (c is TextBox || c is ListBox)
            //        {
            //            c.BackColor = FormColors.GetColor().Textbackcolor;
            //        }
            //    }


            //    //////////////////////////////////////////////////////////////////////////////
            //}
            //catch
            //{

            ///}
          //  FillMRPlantDataSet( strDate, endDate);
            if (keyNRI == 0)
            {
                if (printPlotType == 0)
                {
                    int itmIndex = itemInx[0];
                    string itemNm = itmName[itmIndex];
                    FillChartItmBased(strDate, endDate, itemNm, itmIndex,keyNRI);
                }
                else
                {
                    FillChartDateBased(strDate, itmName, itemInx,keyNRI);
                }
            }
            else
            {
                FillChartNRI(strDate, endDate);

            }
        }
        //--------------------------------------

        //-------------------------------------
       
        private void FillChartItmBased(string strDate, string endDate,string itemName,int itemInx,int keyNRI)
        {
            //ChartRepEx.ChartAreas["Default"].AxisX.Margin = false;
            //ChartRepEx.ChartAreas["Default"].AxisX.Minimum = 0;
            //ChartRepEx.ChartAreas["Default"].AxisX.Interval = 1;
            //ChartRepEx.ChartAreas["Default"].AxisX.Maximum = 24;
            //ChartRepEx.Series.Clear();
            // ChartRepEx.Titles.Clear();
            //ChartRepEx.Legends.Clear(); 
            //ChartRepEx.Series[0].DeleteAttribute();
            //ChartRepEx.Series[1].DeleteAttribute();
            ChartRepEx.Series.Clear();
            ChartRepEx.Series.Add(itemName);
            //chart1.ChartAreas["Default"].AxisY.Minimum = 0;
            // chart1.ChartAreas["Default"].AxisY.Interval = 20;
            //double min = 0, max = 0;
            //findMinMaxOfArray(arr, ref min, ref max);

            //if (min > 50)
            //    ChartRepEx.ChartAreas["Default"].AxisY.Minimum = min - 50;
            //else
            //    ChartRepEx.ChartAreas["Default"].AxisY.Minimum = min;
            //ChartRepEx.ChartAreas["Default"].AxisY.Maximum = max + 50;

            //for (int pointIndex = 1; pointIndex <= 24; pointIndex++)
            //{
            //    ChartRepEx.Series["Power"].Points.AddY(arr[pointIndex]);

            //}
            //
            DataTable dt;
            int itemNumbInx = itemInx + 2;
          //  string dtadd = strDate.Text;
          //string[] HU1 = dtadd.Split('/');
          // string HG1 = HU1[0].Trim();
          //string HG2 = HG1 + "/" + HU1[1].Trim();
            string[] HU11;
            string HG11;
            HG11 = "My Item";
            string HG21;
            string HG31;
            string[] HU31;
            string dtadd;
          //DateTime startMonth = Convert.ToDateTime(strDate);
          //DateTime endMonth = Convert.ToDateTime(endDate);

          DateTime startMonth = PersianDateConverter.ToGregorianDateTime(strDate);
          DateTime endMonth = PersianDateConverter.ToGregorianDateTime(endDate);
          


          DateTime cur = startMonth;
          TimeSpan span = endMonth - startMonth;
          int diff = span.Days+1;
          //ChartRepEx.Series[itemName]
         // ChartRepEx.Series[itemName].Points
          //ChartRepEx.ChartAreas[itemName].AxisX.Minimum = Convert.ToDouble(startMonth);
          //ChartRepEx.ChartAreas[itemName].AxisX.Maximum = Convert.ToDouble(endDate);



          for (int i = 0; i < diff; i++)
          {
              dtadd =new PersianDate( startMonth.AddDays(i)).ToString("d");
          
              if (keyNRI == 0)
              {
                  dt = Utilities.GetTable("select * from MonthlyBillDate where Date ='" + dtadd + "'and ppid='"+ppid+"'");
              }
              else
              { dt = Utilities.GetTable("select * from MonthlyBillDate where Date ='" + dtadd + "'and ppid='"+ppid+"'"); }
              if (dt.Rows.Count > 0)
              {
                  ChartRepEx.Series[itemName].Points.AddXY(dtadd, dt.Rows[0][itemNumbInx].ToString());

              }

          }

            ///////////////////////////////khorsand///////////////////////////////////////
          //for (int i = 0; cur <= endMonth; cur = startMonth.AddDays((++i)))
          //{
          //    dtadd = cur.ToString();
          //    HU11 = dtadd.Split('/');
          //    HG11 = HU11[0].Trim();
          //    HG31 = HU11[2].Trim();
          //    HU31= HG31.Split(' ');
          //    HG21 = HG11 + "/" + HU11[1].Trim() + "/" + HU31[0].Trim();
          //    if (keyNRI==0)
          //    {
          //    dt = utilities.GetTable("select * from MonthlyBillDate where Date ='" + HG21 + "'");
          //    }
          //    else
          //    { dt = utilities.GetTable("select * from MonthlyBillDate where Date ='" + HG21 + "'"); }
          //        if(dt.Rows.Count>0)
          //    {
          //        ChartRepEx.Series[itemName].Points.AddXY(HG21, dt.Rows[0][itemNumbInx].ToString());

          //    }
              
          //}
         // listBox1.Items.Add(HG2);
          //// Set series chart type
          ChartRepEx.Series[itemName].Type = SeriesChartType.Line;
          // Set point labels
          //ChartRepEx.Titles[0].Text = "Item: " + itemName + "\n" + HG11;
          //ChartRepEx.ChartAreas["Default"].Area3DStyle.Enable3D = true;
            //ChartRepEx.Series[itemName]
          ChartRepEx.Series[itemName].MarkerStyle = MarkerStyle.Circle;
          ChartRepEx.Series[itemName].MarkerSize = 4;
          //// Show a 30% perspective
          //ChartRepEx.ChartAreas["Default"].Area3DStyle.Perspective = 3;
          // Set chart title font
          ChartRepEx.Titles[0].Font = new Font("Times New Roman", 10, FontStyle.Bold);
         //// ChartRepEx.Series[itemName].ShowLabelAsValue = true;
          ChartRepEx.Series[itemName].Color = Color.DarkBlue;
         // string path = @"E:\Source\Update-CCorumieh-unitbase\PowerPlantProject\PowerPlantProject\Resources";
         // path = path + "DundasImg";

          //string destinationFile = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
          //          "\\SBS\\DundasImg.jpeg";

          //khorsand
          string pathImg = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +"\\SBS";
          ChartRepEx.SaveAsImage( System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +"\\SBS\\DundasImg.jpeg", ChartImageFormat.Jpeg);

          //string pathImg = @"C:\Program Files\SBS";
          //ChartRepEx.SaveAsImage( @"C:\Program Files\SBS\DundasImg.jpeg", ChartImageFormat.Jpeg);


          // Enable X axis margin
          //ChartRepEx.ChartAreas[itemName].AxisX.Margin = true;
          //ChartRepEx.UI.Toolbar.Enabled = true;
            

            //arr[i] = string.Format("{0:n1}", double.Parse(dt.Rows[0][i + 2].ToString()));
            //double[] yval = { 5, 6, 4, 6, 3 };
            
            //// initialize an array of strings for X values
            //string[] xval = { "A", "B", "C", "D", "E" };
            //chart1.Series["Channel 1"].Points.AddXY(t, ch1);
            // bind the arrays to the X and Y values of data points in the "ByArray" series
           // ChartRepEx.Series[itemName].Points.DataBindXY(xval, yval);

            // now iterate through the arrays to add points to the "ByPoint" series,
            //  setting X and Y values
           // for (int i = 0; i < 5; i++)
           // {
          //      ChartRepEx.Series["Power"].Points.AddXY(xval[i], yval[i]);
           // }
            //========================================================================
           //double[] yval = { 2, 6, 4, 5, 3 };

           // //// Initialize an array of string
           //string[] xval = { "Peter", "Andrew", "Julie", "Mary", "Dave" };
            ////=======================================================================

           //for (double t = 0; t <= (2.5 * Math.PI); t += Math.PI / 6)
            //{
            //    double ch1 = Math.Sin(t);
            //    double ch2 = Math.Sin(t - Math.PI / 2);
            //    chart1.Series["Channel 1"].Points.AddXY(t, ch1);
            //    chart1.Series["Channel 2"].Points.AddXY(t, ch2);
            //}
            //===========================================================================
            //            private void AddButton_Click(object sender, System.EventArgs e)
            //{
            //    Random rnd = new Random();

            //    Series series = Chart1.Series.Add("Series 1");
            //    series.ChartArea = "Default";
            //    series.Type = SeriesChartType.Spline;
            //    series.BorderWidth = 2;

            //    int j = 0;
            //    int MaxPoints = 10;
            //    while(j++ < MaxPoints)
            //    {
            //        series.Points.Add(rnd.Next(5,20));
            //    }
            //    ...
            //}

            //private void RemoveButton_Click(object sender, System.EventArgs e)
            //{
            //    // Remove the last series in the list of the series
            //    Chart1.Series.RemoveAt(Chart1.Series.Count-1);
            //}
            //===============================================================================


            // Bind the double array to the Y axis points of the Default data series
            //chart1.Series["Series 1"].Points.DataBindXY(xval, yval);
            //// Set series chart type
            //ChartRepEx.Series["Power"].Type = SeriesChartType.Spline;

            //// Set point labels
            //ChartRepEx.Series["Power"].ShowLabelAsValue = true;
            //ChartRepEx.Series["Power"].Color = Color.Red;


            //// Enable X axis margin
            //ChartRepEx.ChartAreas["Default"].AxisX.Margin = true;
            //ChartRepEx.UI.Toolbar.Enabled = true;
        }

        //-------------------------------------
        private void FillChartNRI(string strDate, string endDate)
        {
            //ChartRepEx.ChartAreas["Default"].AxisX.Margin = false;
            //ChartRepEx.ChartAreas["Default"].AxisX.Minimum = 0;
            //ChartRepEx.ChartAreas["Default"].AxisX.Interval = 1;
            //ChartRepEx.ChartAreas["Default"].AxisX.Maximum = 24;
            //ChartRepEx.Series.Clear();
            // ChartRepEx.Titles.Clear();
            //ChartRepEx.Legends.Clear(); 
            //ChartRepEx.Series[0].DeleteAttribute();
            //ChartRepEx.Series[1].DeleteAttribute();

            string[] colorsD = new string[]{
             "DarkGreen",
             "DarkCyan",
            "Brown",
             "Blue",
             "DeepPink",
            "DeepSkyBlue",
            "Gold",
            "GreenYellow",
            "DarkGray",
             "Khaki",
            "DarkOrange",
            "DarkSalmon",
            "Lime",
            "Magenta",
            "MediumPurple",
            "MediumSlateBlue",
             "DarkRed",
            "Navy"};
            ChartRepEx.Series.Clear();
            string databaseSlk = "EconomicPlant";
            string itemName = "Default";
            DataTable dt2 = Utilities.GetTable("select * from " + databaseSlk + " where Date between '" + strDate + "' and '" + endDate + "'");
            //select * from MonthlyBillPlant where Date between '"+TABLE1.MIN_RANGE+"' and "'+TABLE1.MAX_RANGE+"';
            int rowcountm = dt2.Rows.Count;
            int colcountm = dt2.Columns.Count;
            for (int ijk = 2; ijk < colcountm; ijk++)
            {

                itemName = dt2.Columns[ijk].ColumnName.ToString();
                ChartRepEx.Series.Add(itemName);


                //chart1.ChartAreas["Default"].AxisY.Minimum = 0;
                // chart1.ChartAreas["Default"].AxisY.Interval = 20;
                //double min = 0, max = 0;
                //findMinMaxOfArray(arr, ref min, ref max);

                //if (min > 50)
                //    ChartRepEx.ChartAreas["Default"].AxisY.Minimum = min - 50;
                //else
                //    ChartRepEx.ChartAreas["Default"].AxisY.Minimum = min;
                //ChartRepEx.ChartAreas["Default"].AxisY.Maximum = max + 50;

                //for (int pointIndex = 1; pointIndex <= 24; pointIndex++)
                //{
                //    ChartRepEx.Series["Power"].Points.AddY(arr[pointIndex]);

                //}
                //
                DataTable dt;
                // int itemNumbInx = itemInx + 2;
                //  string dtadd = strDate.Text;
                //string[] HU1 = dtadd.Split('/');
                // string HG1 = HU1[0].Trim();
                //string HG2 = HG1 + "/" + HU1[1].Trim();
                string[] HU11;
                string HG11;
                HG11 = "My Item";
                string HG21;
                string HG31;
                string[] HU31;
                string dtadd;

                //DateTime startMonth = Convert.ToDateTime(strDate);
                //DateTime endMonth = Convert.ToDateTime(endDate);


                DateTime startMonth = PersianDateConverter.ToGregorianDateTime(strDate);
                DateTime endMonth = PersianDateConverter.ToGregorianDateTime(endDate);
               



                DateTime cur = startMonth;
                TimeSpan span = endMonth - startMonth;
                int diff = span.Days + 1;

                //ChartRepEx.Series[itemName]
                // ChartRepEx.Series[itemName].Points
                //ChartRepEx.ChartAreas[itemName].AxisX.Minimum = Convert.ToDouble(startMonth);
                //ChartRepEx.ChartAreas[itemName].AxisX.Maximum = Convert.ToDouble(endDate);
                int j = 0;

                for (int i = 0; i < diff;i++)
                {

                    dtadd = new PersianDate(startMonth.AddDays(i)).ToString("d");


                    //dtadd = startMonth.AddDays(i).ToString("d");
                    //HU11 = dtadd.Split('/');
                    //HG11 = HU11[0].Trim();
                    //HG31 = HU11[2].Trim();
                    //HU31 = HG31.Split(' ');
                    //HG21 = HG11 + "/" + HU11[1].Trim() + "/" + HU31[0].Trim();

                    // dt = utilities.GetTable("select * from " + databaseSlk + " where Date ='" + HG21 + "'"); 
                    if (dt2.Rows.Count > 0)
                    {
                        try
                        {
                            if (dtadd == dt2.Rows[j][1].ToString())
                            {

                                ChartRepEx.Series[itemName].Points.AddXY(dtadd, dt2.Rows[j][ijk].ToString());
                                j++;
                            }
                        }
                        catch
                        {
                        }



                    }

                }

                //khorsand
                //for (int i = 0; cur <= endMonth; cur = startMonth.AddDays((++i)))
                //{
                //    dtadd = cur.ToString();
                //    HU11 = dtadd.Split('/');
                //    HG11 = HU11[0].Trim();
                //    HG31 = HU11[2].Trim();
                //    HU31 = HG31.Split(' ');
                //    HG21 = HG11 + "/" + HU11[1].Trim() + "/" + HU31[0].Trim();

                //    // dt = utilities.GetTable("select * from " + databaseSlk + " where Date ='" + HG21 + "'"); 
                //    if (dt2.Rows.Count > 0)
                //    {
                //        try
                //        {
                //            if (HG21 == dt2.Rows[j][1].ToString())
                //            {

                //                ChartRepEx.Series[itemName].Points.AddXY(HG21, dt2.Rows[j][ijk].ToString());
                //                j++;
                //            }
                //        }
                //        catch
                //        {
                //        }



                //    }

                //}

                // listBox1.Items.Add(HG2);
                //// Set series chart type
                ChartRepEx.Series[itemName].Type = SeriesChartType.Line;
                // Set point labels
                // ChartRepEx.Series[itemName].Type = SeriesChartType.StepLine;
                ChartRepEx.Titles[0].Text = "From " + strDate + " to " + endDate;
                //ChartRepEx.ChartAreas["Default"].Area3DStyle.Enable3D = true;
                //ChartRepEx.Series[itemName]
                ChartRepEx.Series[itemName].MarkerStyle = MarkerStyle.Circle;
                ChartRepEx.Series[itemName].MarkerSize = 2;
                //// Show a 30% perspective
                //ChartRepEx.ChartAreas["Default"].Area3DStyle.Perspective = 3;
                // Set chart title font
                ChartRepEx.Titles[0].Font = new Font("Times New Roman", 10, FontStyle.Bold);
                //// ChartRepEx.Series[itemName].ShowLabelAsValue = true;Color.FromArgb(ijk*2, ijk * 9, ijk * 12, ijk * 11);
                ChartRepEx.Series[itemName].Color = Color.FromName(colorsD[ijk]);
            }
            // string path = @"E:\Source\Update-CCorumieh-unitbase\PowerPlantProject\PowerPlantProject\Resources";
            // path = path + "DundasImg";

            //khorsand
            //string pathImg = @"C:\Program Files\SBS";
            //ChartRepEx.SaveAsImage(@"C:\Program Files\SBS\DundasImg.jpeg", ChartImageFormat.Jpeg);

            string pathImg = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\SBS";
            ChartRepEx.SaveAsImage(System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\SBS\\DundasImg.jpeg", ChartImageFormat.Jpeg);





        }
        //============================================================
        private void FillChartDateBased(string stDate, string[] itmNameA, int[] itemInx, int keyNRI)
        {

            ChartRepEx.Series.Clear();
            ChartRepEx.Series.Add("Def");

            DataTable dt2;
            int itemNumbInx ;

           
            string itemName;
        
            string HG21;
            string HG31;
            string[] HU31;
            string dtadd;
            int itmC = itemInx.Length;
            if (keyNRI == 0)
            {
                dt2 = Utilities.GetTable("select * from MonthlyBillDate where Date ='" + stDate + "'and ppid='"+ppid+"'");
            }
            else
            {
                dt2 = Utilities.GetTable("select * from MonthlyBillDate where Date ='" + stDate + "'and ppid='"+ppid+"'");
            }
                for(int iix=0;iix<itmC;iix++)
            {
                itemName=itmNameA[itemInx[iix]-1];
                itemNumbInx = itemInx[iix] + 2;
                if (dt2.Rows.Count > 0)
                {
                    ChartRepEx.Series["Def"].Points.AddXY(itemName, dt2.Rows[0][itemNumbInx].ToString());
                }
                    }
            // listBox1.Items.Add(HG2);
            //// Set series chart type
            ChartRepEx.Series["Def"].Type = SeriesChartType.Bar;
            // Set point labels
            ChartRepEx.Titles[0].Text = "Items " + "In Date " + stDate.ToString() + "\n";
            //ChartRepEx.ChartAreas["Default"].Area3DStyle.Enable3D = true;
            //ChartRepEx.Series[itemName]
            ChartRepEx.Series["Def"].MarkerStyle = MarkerStyle.Circle;
            ChartRepEx.Series["Def"].MarkerSize = 4;
            //// Show a 30% perspective
            //ChartRepEx.ChartAreas["Default"].Area3DStyle.Perspective = 3;
            // Set chart title font
            ChartRepEx.Titles[0].Font = new Font("Times New Roman", 10, FontStyle.Bold);
            //// ChartRepEx.Series[itemName].ShowLabelAsValue = true;
            ChartRepEx.Series["Def"].Color = Color.Gray;
            // string path = @"E:\Source\Update-CCorumieh-unitbase\PowerPlantProject\PowerPlantProject\Resources";
            // path = path + "DundasImg";


            string pathImg = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\SBS";
            ChartRepEx.SaveAsImage(System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\SBS\\DundasImg.jpeg", ChartImageFormat.Jpeg);

            //khorsand
            //string pathImg = @"C:\Program Files\SBS";
            //ChartRepEx.SaveAsImage(@"C:\Program Files\SBS\DundasImg.jpeg", ChartImageFormat.Jpeg);


            // Enable X axis margin
            //ChartRepEx.ChartAreas[itemName].AxisX.Margin = true;
            //ChartRepEx.UI.Toolbar.Enabled = true;


            //arr[i] = string.Format("{0:n1}", double.Parse(dt.Rows[0][i + 2].ToString()));
            //double[] yval = { 5, 6, 4, 6, 3 };

            //// initialize an array of strings for X values
            //string[] xval = { "A", "B", "C", "D", "E" };
            //chart1.Series["Channel 1"].Points.AddXY(t, ch1);
            // bind the arrays to the X and Y values of data points in the "ByArray" series
            // ChartRepEx.Series[itemName].Points.DataBindXY(xval, yval);

            // now iterate through the arrays to add points to the "ByPoint" series,
            //  setting X and Y values
            // for (int i = 0; i < 5; i++)
            // {
            //      ChartRepEx.Series["Power"].Points.AddXY(xval[i], yval[i]);
            // }
            //========================================================================
            //double[] yval = { 2, 6, 4, 5, 3 };

            // //// Initialize an array of string
            //string[] xval = { "Peter", "Andrew", "Julie", "Mary", "Dave" };
            ////=======================================================================



            //===============================================================================



        }

        //------------------------------------------------------------
        //------------------------------------------------------------
        //------------------------------------------------------------
        private void FillMRPlantDataSet(string strDate,string endDate)
        {
            DataTable dt;
            if (!IsEstimated)
            {
                if ((strDate != null) && (strDate != "[Empty Value]") && (endDate != null) && (endDate != "[Empty Value]"))
                {

                    //DataSet MyDS = new DataSet();
                    dt = Utilities.GetTable("select * from MonthlyBillDate where Date ='" + strDate + "'and ppid='"+ppid+"'");
                   //// where Date between '"+strDateitm+"' and '"+endDateitm+"'"

                    arr = new string[25];
                   // foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                    foreach (DataRow MyRow in dt.Rows)
                    {
                        int index = int.Parse(MyRow["Hour"].ToString().Trim());
                        arr[index] = string.Format("{0:n1}", double.Parse(MyRow["SR"].ToString()));
                    }
                }
            }
            else if (IsEstimated)
            {
                if ((strDate != null) && (strDate != "[Empty Value]") && (endDate != null) && (endDate != "[Empty Value]"))
                {
                    dt = Utilities.GetTable("select * from MonthlyBillDate where Date ='" + strDate + "'and ppid='"+ppid+"'");
                    //DataSet MyDS = new DataSet();
                    //SqlDataAdapter Myda = new SqlDataAdapter();
                    //SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                    //myConnection.Open();

                    //Myda.SelectCommand = new SqlCommand("select * from dbo.PowerForecast where date=@date and gencode='" + PlantId + "' order by Id desc", myConnection);
                    //Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                    //Myda.SelectCommand.Parameters["@date"].Value = Date;
                    //Myda.Fill(MyDS);
                    //myConnection.Close();

                    arr = new string[25];
                   
                    for (int i = 1; i <= 24; i++)
                    {

                        arr[i] = string.Format("{0:n1}", double.Parse(dt.Rows[0][i + 2].ToString()));

                    }
                }



            }
        }

        //--------------------------------------
        private void findMinMaxOfArray(string[] arr, ref double min, ref double max)
        {
            if (arr[1] != null)
            {
                min = double.Parse(arr[1]);
                max = double.Parse(arr[1]);
            }
            try
            {
                for (int i = 2; i < 25; i++)
                {
                    if (arr[i] != null)
                    {
                        if (double.Parse(arr[i]) < min)
                            min = double.Parse(arr[i]);
                        if (double.Parse(arr[i]) > max)
                            max = double.Parse(arr[i]);
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }


        }

    }
}
