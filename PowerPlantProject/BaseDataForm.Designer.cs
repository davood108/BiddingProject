﻿namespace PowerPlantProject
{
    partial class BaseDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseDataForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.PmaxTb = new System.Windows.Forms.TextBox();
            this.PminTb = new System.Windows.Forms.TextBox();
            this.GasTb = new System.Windows.Forms.TextBox();
            this.SteamTb = new System.Windows.Forms.TextBox();
            this.UpdateCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.CCTb = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.GasOilPriceTb = new System.Windows.Forms.TextBox();
            this.MazutPriceTb = new System.Windows.Forms.TextBox();
            this.GasPriceTb = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.LineCapacityPaymentTB = new System.Windows.Forms.TextBox();
            this.LineEnergyPaymentTB = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.GasoilSubTB = new System.Windows.Forms.TextBox();
            this.MazutSubTB = new System.Windows.Forms.TextBox();
            this.GasSubTB = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.comboSelectPlant = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.comboBoxstatus = new System.Windows.Forms.ComboBox();
            this.comMarketRule = new System.Windows.Forms.ComboBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.delete = new System.Windows.Forms.DataGridViewImageColumn();
            this.lbrefer = new System.Windows.Forms.Label();
            this.hbtrans = new System.Windows.Forms.Label();
            this.cmbrefer = new System.Windows.Forms.ComboBox();
            this.cmbtrans = new System.Windows.Forms.ComboBox();
            this.txtdownerror = new System.Windows.Forms.TextBox();
            this.txtuperror = new System.Windows.Forms.TextBox();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comFuel = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(43, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Market Price Maximum :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(43, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Market Price Minimum :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(70, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Fuel Limited";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(74, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Status Run :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(74, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Market Rule :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(43, 246);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(154, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Gas Production Cost Minimum :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(43, 278);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Steam Production Cost Minimum :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(43, 308);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(149, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "CC Production Cost Minimum :";
            // 
            // PmaxTb
            // 
            this.PmaxTb.Location = new System.Drawing.Point(213, 32);
            this.PmaxTb.Name = "PmaxTb";
            this.PmaxTb.Size = new System.Drawing.Size(120, 20);
            this.PmaxTb.TabIndex = 0;
            this.PmaxTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.PmaxTb.Validated += new System.EventHandler(this.PmaxTb_Validated);
            this.PmaxTb.Leave += new System.EventHandler(this.PmaxTb_Leave);
            this.PmaxTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PmaxTb_MouseClick);
            // 
            // PminTb
            // 
            this.PminTb.Location = new System.Drawing.Point(213, 61);
            this.PminTb.Name = "PminTb";
            this.PminTb.Size = new System.Drawing.Size(120, 20);
            this.PminTb.TabIndex = 1;
            this.PminTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.PminTb.Validated += new System.EventHandler(this.PminTb_Validated);
            this.PminTb.Leave += new System.EventHandler(this.PminTb_Leave);
            this.PminTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PminTb_MouseClick);
            // 
            // GasTb
            // 
            this.GasTb.Location = new System.Drawing.Point(213, 243);
            this.GasTb.Name = "GasTb";
            this.GasTb.Size = new System.Drawing.Size(120, 20);
            this.GasTb.TabIndex = 5;
            this.GasTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GasTb.Validated += new System.EventHandler(this.GasTb_Validated);
            this.GasTb.Leave += new System.EventHandler(this.GasTb_Leave);
            this.GasTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GasTb_MouseClick);
            // 
            // SteamTb
            // 
            this.SteamTb.Location = new System.Drawing.Point(213, 275);
            this.SteamTb.Name = "SteamTb";
            this.SteamTb.Size = new System.Drawing.Size(120, 20);
            this.SteamTb.TabIndex = 6;
            this.SteamTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.SteamTb.Validated += new System.EventHandler(this.SteamTb_Validated);
            this.SteamTb.Leave += new System.EventHandler(this.SteamTb_Leave);
            this.SteamTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SteamTb_MouseClick);
            // 
            // UpdateCal
            // 
            this.UpdateCal.HasButtons = true;
            this.UpdateCal.Location = new System.Drawing.Point(519, 346);
            this.UpdateCal.Name = "UpdateCal";
            this.UpdateCal.Readonly = true;
            this.UpdateCal.Size = new System.Drawing.Size(120, 20);
            this.UpdateCal.TabIndex = 19;
            this.UpdateCal.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.UpdateCal.ValueChanged += new System.EventHandler(this.UpdateCal_ValueChanged);
            // 
            // CCTb
            // 
            this.CCTb.Location = new System.Drawing.Point(213, 308);
            this.CCTb.Name = "CCTb";
            this.CCTb.Size = new System.Drawing.Size(120, 20);
            this.CCTb.TabIndex = 7;
            this.CCTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.CCTb.Validated += new System.EventHandler(this.CCTb_Validated);
            this.CCTb.Leave += new System.EventHandler(this.CCTb_Leave);
            this.CCTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CCTb_MouseClick);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(391, 346);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 37;
            this.label9.Text = "Update Date :";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // GasOilPriceTb
            // 
            this.GasOilPriceTb.Location = new System.Drawing.Point(517, 92);
            this.GasOilPriceTb.Name = "GasOilPriceTb";
            this.GasOilPriceTb.Size = new System.Drawing.Size(120, 20);
            this.GasOilPriceTb.TabIndex = 12;
            this.GasOilPriceTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GasOilPriceTb.Validated += new System.EventHandler(this.GasOilPriceTb_Validated);
            this.GasOilPriceTb.Leave += new System.EventHandler(this.GasOilPriceTb_Leave);
            this.GasOilPriceTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GasOilPriceTb_MouseClick);
            // 
            // MazutPriceTb
            // 
            this.MazutPriceTb.Location = new System.Drawing.Point(517, 60);
            this.MazutPriceTb.Name = "MazutPriceTb";
            this.MazutPriceTb.Size = new System.Drawing.Size(120, 20);
            this.MazutPriceTb.TabIndex = 11;
            this.MazutPriceTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.MazutPriceTb.Validated += new System.EventHandler(this.MazutPriceTb_Validated);
            this.MazutPriceTb.Leave += new System.EventHandler(this.MazutPriceTb_Leave);
            this.MazutPriceTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MazutPriceTb_MouseClick);
            // 
            // GasPriceTb
            // 
            this.GasPriceTb.Location = new System.Drawing.Point(517, 31);
            this.GasPriceTb.Name = "GasPriceTb";
            this.GasPriceTb.Size = new System.Drawing.Size(120, 20);
            this.GasPriceTb.TabIndex = 10;
            this.GasPriceTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GasPriceTb.Validated += new System.EventHandler(this.GasPriceTb_Validated);
            this.GasPriceTb.Leave += new System.EventHandler(this.GasPriceTb_Leave);
            this.GasPriceTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GasPriceTb_MouseClick);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(402, 94);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 13);
            this.label10.TabIndex = 42;
            this.label10.Text = "GasOil Price :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(404, 61);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 13);
            this.label11.TabIndex = 39;
            this.label11.Text = "Mazut Price :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(404, 32);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 13);
            this.label12.TabIndex = 38;
            this.label12.Text = "Gas Price :";
            // 
            // LineCapacityPaymentTB
            // 
            this.LineCapacityPaymentTB.Location = new System.Drawing.Point(517, 252);
            this.LineCapacityPaymentTB.Name = "LineCapacityPaymentTB";
            this.LineCapacityPaymentTB.Size = new System.Drawing.Size(120, 20);
            this.LineCapacityPaymentTB.TabIndex = 17;
            this.LineCapacityPaymentTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.LineCapacityPaymentTB.Validated += new System.EventHandler(this.LineCapacityPaymentTB_Validated);
            this.LineCapacityPaymentTB.Leave += new System.EventHandler(this.LineCapacityPaymentTB_Leave);
            this.LineCapacityPaymentTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.LineCapacityPaymentTB_MouseClick);
            // 
            // LineEnergyPaymentTB
            // 
            this.LineEnergyPaymentTB.Location = new System.Drawing.Point(517, 217);
            this.LineEnergyPaymentTB.Name = "LineEnergyPaymentTB";
            this.LineEnergyPaymentTB.Size = new System.Drawing.Size(120, 20);
            this.LineEnergyPaymentTB.TabIndex = 16;
            this.LineEnergyPaymentTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.LineEnergyPaymentTB.Validated += new System.EventHandler(this.LineEnergyPaymentTB_Validated);
            this.LineEnergyPaymentTB.Leave += new System.EventHandler(this.LineEnergyPaymentTB_Leave);
            this.LineEnergyPaymentTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.LineEnergyPaymentTB_MouseClick);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(384, 256);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 13);
            this.label13.TabIndex = 46;
            this.label13.Text = "Line Capacity Payment :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(404, 224);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 44;
            this.label14.Text = "FPure:";
            // 
            // GasoilSubTB
            // 
            this.GasoilSubTB.Location = new System.Drawing.Point(517, 189);
            this.GasoilSubTB.Name = "GasoilSubTB";
            this.GasoilSubTB.Size = new System.Drawing.Size(120, 20);
            this.GasoilSubTB.TabIndex = 15;
            this.GasoilSubTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GasoilSubTB.Validated += new System.EventHandler(this.GasoilSubTB_Validated);
            this.GasoilSubTB.Leave += new System.EventHandler(this.GasoilSubTB_Leave);
            this.GasoilSubTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GasoilSubTB_MouseClick);
            // 
            // MazutSubTB
            // 
            this.MazutSubTB.Location = new System.Drawing.Point(517, 154);
            this.MazutSubTB.Name = "MazutSubTB";
            this.MazutSubTB.Size = new System.Drawing.Size(120, 20);
            this.MazutSubTB.TabIndex = 14;
            this.MazutSubTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.MazutSubTB.Validated += new System.EventHandler(this.MazutSubTB_Validated);
            this.MazutSubTB.Leave += new System.EventHandler(this.MazutSubTB_Leave);
            this.MazutSubTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MazutSubTB_MouseClick);
            // 
            // GasSubTB
            // 
            this.GasSubTB.Location = new System.Drawing.Point(517, 121);
            this.GasSubTB.Name = "GasSubTB";
            this.GasSubTB.Size = new System.Drawing.Size(120, 20);
            this.GasSubTB.TabIndex = 13;
            this.GasSubTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GasSubTB.Validated += new System.EventHandler(this.GasSubTB_Validated);
            this.GasSubTB.Leave += new System.EventHandler(this.GasSubTB_Leave);
            this.GasSubTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GasSubTB_MouseClick);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(384, 193);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(119, 13);
            this.label15.TabIndex = 52;
            this.label15.Text = "GasOil Subsidies Price :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(384, 161);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(117, 13);
            this.label16.TabIndex = 51;
            this.label16.Text = "Mazut Subsidies Price :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(384, 128);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(107, 13);
            this.label17.TabIndex = 50;
            this.label17.Text = "Gas Subsidies Price :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Location = new System.Drawing.Point(87, 339);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(46, 13);
            this.label19.TabIndex = 54;
            this.label19.Text = "Up Error";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Location = new System.Drawing.Point(78, 373);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(63, 13);
            this.label20.TabIndex = 55;
            this.label20.Text = "Down  Error";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.BackColor = System.Drawing.Color.Transparent;
            this.label101.Location = new System.Drawing.Point(386, 299);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(119, 13);
            this.label101.TabIndex = 57;
            this.label101.Text = "Select  PreSolve  Plant:";
            // 
            // comboSelectPlant
            // 
            this.comboSelectPlant.FormattingEnabled = true;
            this.comboSelectPlant.Location = new System.Drawing.Point(517, 296);
            this.comboSelectPlant.Name = "comboSelectPlant";
            this.comboSelectPlant.Size = new System.Drawing.Size(120, 21);
            this.comboSelectPlant.TabIndex = 56;
            // 
            // comboBoxstatus
            // 
            this.comboBoxstatus.FormattingEnabled = true;
            this.comboBoxstatus.Items.AddRange(new object[] {
            "Require",
            "Economic",
            "Maximum"});
            this.comboBoxstatus.Location = new System.Drawing.Point(213, 122);
            this.comboBoxstatus.Name = "comboBoxstatus";
            this.comboBoxstatus.Size = new System.Drawing.Size(121, 21);
            this.comboBoxstatus.TabIndex = 58;
            // 
            // comMarketRule
            // 
            this.comMarketRule.FormattingEnabled = true;
            this.comMarketRule.Items.AddRange(new object[] {
            "Normal",
            "Fuel Limited"});
            this.comMarketRule.Location = new System.Drawing.Point(213, 153);
            this.comMarketRule.Name = "comMarketRule";
            this.comMarketRule.Size = new System.Drawing.Size(121, 21);
            this.comMarketRule.TabIndex = 59;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.linkLabel1.Location = new System.Drawing.Point(303, 456);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(73, 15);
            this.linkLabel1.TabIndex = 60;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Show Grid";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.delete});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.Location = new System.Drawing.Point(12, 477);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.Size = new System.Drawing.Size(657, 111);
            this.dataGridView2.TabIndex = 68;
            this.dataGridView2.Visible = false;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // delete
            // 
            this.delete.HeaderText = "Delete";
            this.delete.Image = global::PowerPlantProject.Properties.Resources.deletered81;
            this.delete.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.delete.Name = "delete";
            // 
            // lbrefer
            // 
            this.lbrefer.AutoSize = true;
            this.lbrefer.BackColor = System.Drawing.Color.Transparent;
            this.lbrefer.Location = new System.Drawing.Point(73, 186);
            this.lbrefer.Name = "lbrefer";
            this.lbrefer.Size = new System.Drawing.Size(63, 13);
            this.lbrefer.TabIndex = 3;
            this.lbrefer.Text = "Reference :";
            // 
            // hbtrans
            // 
            this.hbtrans.AutoSize = true;
            this.hbtrans.BackColor = System.Drawing.Color.Transparent;
            this.hbtrans.Location = new System.Drawing.Point(73, 215);
            this.hbtrans.Name = "hbtrans";
            this.hbtrans.Size = new System.Drawing.Size(74, 13);
            this.hbtrans.TabIndex = 4;
            this.hbtrans.Text = "Transmission :";
            // 
            // cmbrefer
            // 
            this.cmbrefer.FormattingEnabled = true;
            this.cmbrefer.Items.AddRange(new object[] {
            "Hub",
            "Normal"});
            this.cmbrefer.Location = new System.Drawing.Point(212, 180);
            this.cmbrefer.Name = "cmbrefer";
            this.cmbrefer.Size = new System.Drawing.Size(121, 21);
            this.cmbrefer.TabIndex = 58;
            // 
            // cmbtrans
            // 
            this.cmbtrans.FormattingEnabled = true;
            this.cmbtrans.Items.AddRange(new object[] {
            "Lost",
            "Normal"});
            this.cmbtrans.Location = new System.Drawing.Point(212, 211);
            this.cmbtrans.Name = "cmbtrans";
            this.cmbtrans.Size = new System.Drawing.Size(121, 21);
            this.cmbtrans.TabIndex = 59;
            // 
            // txtdownerror
            // 
            this.txtdownerror.Location = new System.Drawing.Point(213, 370);
            this.txtdownerror.Name = "txtdownerror";
            this.txtdownerror.Size = new System.Drawing.Size(120, 20);
            this.txtdownerror.TabIndex = 9;
            this.txtdownerror.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtdownerror.Validated += new System.EventHandler(this.txtGasOilHeatValueAverage_Validated);
            this.txtdownerror.Leave += new System.EventHandler(this.txtGasOilHeatValueAverage_Leave);
            this.txtdownerror.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtGasOilHeatValueAverage_MouseClick);
            // 
            // txtuperror
            // 
            this.txtuperror.Location = new System.Drawing.Point(213, 336);
            this.txtuperror.Name = "txtuperror";
            this.txtuperror.Size = new System.Drawing.Size(120, 20);
            this.txtuperror.TabIndex = 8;
            this.txtuperror.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtuperror.Validated += new System.EventHandler(this.txtGasHeatValueAverage_Validated);
            this.txtuperror.Leave += new System.EventHandler(this.txtGasHeatValueAverage_Leave);
            this.txtuperror.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtGasHeatValueAverage_MouseClick);
            // 
            // SaveBtn
            // 
            this.SaveBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.SaveBtn.BackgroundImage = global::PowerPlantProject.Properties.Resources.Savedata;
            this.SaveBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SaveBtn.Location = new System.Drawing.Point(293, 421);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(86, 27);
            this.SaveBtn.TabIndex = 20;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.UseVisualStyleBackColor = false;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "Delete";
            this.dataGridViewImageColumn1.Image = global::PowerPlantProject.Properties.Resources.deletered81;
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::PowerPlantProject.Properties.Resources.forward;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Location = new System.Drawing.Point(612, 441);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(57, 30);
            this.panel1.TabIndex = 69;
            this.panel1.Visible = false;
            this.panel1.Click += new System.EventHandler(this.panel1_Click);
            // 
            // comFuel
            // 
            this.comFuel.FormattingEnabled = true;
            this.comFuel.Items.AddRange(new object[] {
            "Normal",
            "Elghaee"});
            this.comFuel.Location = new System.Drawing.Point(212, 91);
            this.comFuel.Name = "comFuel";
            this.comFuel.Size = new System.Drawing.Size(121, 21);
            this.comFuel.TabIndex = 70;
            // 
            // BaseDataForm
            // 
            this.AcceptButton = this.SaveBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(681, 595);
            this.Controls.Add(this.comFuel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.cmbtrans);
            this.Controls.Add(this.comMarketRule);
            this.Controls.Add(this.cmbrefer);
            this.Controls.Add(this.comboBoxstatus);
            this.Controls.Add(this.label101);
            this.Controls.Add(this.comboSelectPlant);
            this.Controls.Add(this.txtdownerror);
            this.Controls.Add(this.txtuperror);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.GasoilSubTB);
            this.Controls.Add(this.MazutSubTB);
            this.Controls.Add(this.GasSubTB);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.LineCapacityPaymentTB);
            this.Controls.Add(this.LineEnergyPaymentTB);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.GasOilPriceTb);
            this.Controls.Add(this.MazutPriceTb);
            this.Controls.Add(this.GasPriceTb);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.CCTb);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.UpdateCal);
            this.Controls.Add(this.SteamTb);
            this.Controls.Add(this.GasTb);
            this.Controls.Add(this.PminTb);
            this.Controls.Add(this.PmaxTb);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.hbtrans);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbrefer);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BaseDataForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Form6_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox PmaxTb;
        private System.Windows.Forms.TextBox PminTb;
        private System.Windows.Forms.TextBox GasTb;
        private System.Windows.Forms.TextBox SteamTb;
        private FarsiLibrary.Win.Controls.FADatePicker UpdateCal;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.TextBox CCTb;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.TextBox GasOilPriceTb;
        private System.Windows.Forms.TextBox MazutPriceTb;
        private System.Windows.Forms.TextBox GasPriceTb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox LineCapacityPaymentTB;
        private System.Windows.Forms.TextBox LineEnergyPaymentTB;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox GasoilSubTB;
        private System.Windows.Forms.TextBox MazutSubTB;
        private System.Windows.Forms.TextBox GasSubTB;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.ComboBox comboSelectPlant;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ComboBox comboBoxstatus;
        private System.Windows.Forms.ComboBox comMarketRule;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewImageColumn delete;
        private System.Windows.Forms.ComboBox cmbtrans;
        private System.Windows.Forms.ComboBox cmbrefer;
        private System.Windows.Forms.Label hbtrans;
        private System.Windows.Forms.Label lbrefer;
        private System.Windows.Forms.TextBox txtdownerror;
        private System.Windows.Forms.TextBox txtuperror;
        public System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.ComboBox comFuel;
    }
}