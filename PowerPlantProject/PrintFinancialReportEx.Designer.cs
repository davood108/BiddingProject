﻿namespace PowerPlantProject
{
    partial class PrintFinancialReportEx
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintFinancialReportEx));
            this.tabprintreport = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.radioButton14 = new System.Windows.Forms.RadioButton();
            this.radioButton13 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button6 = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.prntdatedaily = new System.Windows.Forms.Button();
            this.SelectItemsDaily = new System.Windows.Forms.Button();
            this.faDatePicker2 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.prtprwitmdaily = new System.Windows.Forms.Button();
            this.selectItmDaily = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.faDatePicker1 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.faDatePickerbill = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.rdnri = new System.Windows.Forms.RadioButton();
            this.rdtolid = new System.Windows.Forms.RadioButton();
            this.rddeclare = new System.Windows.Forms.RadioButton();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.timedomain = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.radioButton15 = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.selectItmMonthly = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.addDateToListbox = new System.Windows.Forms.Button();
            this.faDatePicker3 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label15 = new System.Windows.Forms.Label();
            this.faDatePicker4 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.faDatePicker5 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btnmontexp = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.prntitmmonthly = new System.Windows.Forms.Button();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.CMBPLANT = new System.Windows.Forms.ComboBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.tabprintreport.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox13.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.SuspendLayout();
            // 
            // tabprintreport
            // 
            this.tabprintreport.Controls.Add(this.tabPage1);
            this.tabprintreport.Controls.Add(this.tabPage2);
            this.tabprintreport.Location = new System.Drawing.Point(12, 35);
            this.tabprintreport.Name = "tabprintreport";
            this.tabprintreport.SelectedIndex = 0;
            this.tabprintreport.Size = new System.Drawing.Size(833, 476);
            this.tabprintreport.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.groupBox8);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(825, 450);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Daily";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboBox2);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(266, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(297, 29);
            this.panel1.TabIndex = 57;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "1",
            "10^1",
            "10^2",
            "10^3",
            "10^4",
            "10^5",
            "10^6",
            "10^7",
            "10^8",
            "10^9"});
            this.comboBox2.Location = new System.Drawing.Point(195, 4);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(61, 21);
            this.comboBox2.TabIndex = 1;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBox1.Location = new System.Drawing.Point(75, 4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(54, 21);
            this.comboBox1.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(135, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Scientific :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Decimal :";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.radioButton14);
            this.groupBox8.Controls.Add(this.radioButton13);
            this.groupBox8.Location = new System.Drawing.Point(9, 0);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(132, 29);
            this.groupBox8.TabIndex = 56;
            this.groupBox8.TabStop = false;
            // 
            // radioButton14
            // 
            this.radioButton14.AutoSize = true;
            this.radioButton14.Location = new System.Drawing.Point(76, 10);
            this.radioButton14.Name = "radioButton14";
            this.radioButton14.Size = new System.Drawing.Size(44, 17);
            this.radioButton14.TabIndex = 1;
            this.radioButton14.Text = "NRI";
            this.radioButton14.UseVisualStyleBackColor = true;
            this.radioButton14.CheckedChanged += new System.EventHandler(this.radioButton14_CheckedChanged);
            // 
            // radioButton13
            // 
            this.radioButton13.AutoSize = true;
            this.radioButton13.Checked = true;
            this.radioButton13.Location = new System.Drawing.Point(6, 9);
            this.radioButton13.Name = "radioButton13";
            this.radioButton13.Size = new System.Drawing.Size(52, 17);
            this.radioButton13.TabIndex = 0;
            this.radioButton13.TabStop = true;
            this.radioButton13.Text = "IGMC";
            this.radioButton13.UseVisualStyleBackColor = true;
            this.radioButton13.CheckedChanged += new System.EventHandler(this.radioButton13_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.groupBox2.Controls.Add(this.button6);
            this.groupBox2.Controls.Add(this.groupBox12);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.groupBox10);
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.prntdatedaily);
            this.groupBox2.Controls.Add(this.SelectItemsDaily);
            this.groupBox2.Controls.Add(this.faDatePicker2);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox2.Location = new System.Drawing.Point(9, 241);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(816, 199);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Date-Based Report";
            // 
            // button6
            // 
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button6.Location = new System.Drawing.Point(741, 150);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(72, 25);
            this.button6.TabIndex = 59;
            this.button6.Text = "Excel";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.textBox3);
            this.groupBox12.Controls.Add(this.label7);
            this.groupBox12.Controls.Add(this.textBox4);
            this.groupBox12.Controls.Add(this.label8);
            this.groupBox12.Location = new System.Drawing.Point(17, 22);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(341, 49);
            this.groupBox12.TabIndex = 57;
            this.groupBox12.TabStop = false;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(253, 19);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(30, 23);
            this.textBox3.TabIndex = 45;
            this.textBox3.Text = "24";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(222, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 17);
            this.label7.TabIndex = 44;
            this.label7.Text = "To";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(186, 19);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(30, 23);
            this.textBox4.TabIndex = 43;
            this.textBox4.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(81, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 17);
            this.label8.TabIndex = 42;
            this.label8.Text = "Hour Range:";
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Location = new System.Drawing.Point(577, 150);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(84, 25);
            this.button3.TabIndex = 56;
            this.button3.Text = "Add Chart";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.radioButton11);
            this.groupBox10.Controls.Add(this.radioButton12);
            this.groupBox10.Location = new System.Drawing.Point(364, 14);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox10.Size = new System.Drawing.Size(207, 100);
            this.groupBox10.TabIndex = 55;
            this.groupBox10.TabStop = false;
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.Location = new System.Drawing.Point(66, 26);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButton11.Size = new System.Drawing.Size(46, 21);
            this.radioButton11.TabIndex = 1;
            this.radioButton11.TabStop = true;
            this.radioButton11.Text = "اوليه";
            this.radioButton11.UseVisualStyleBackColor = true;
            this.radioButton11.Visible = false;
            // 
            // radioButton12
            // 
            this.radioButton12.AutoSize = true;
            this.radioButton12.Checked = true;
            this.radioButton12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.radioButton12.Location = new System.Drawing.Point(2, 64);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButton12.Size = new System.Drawing.Size(201, 17);
            this.radioButton12.TabIndex = 0;
            this.radioButton12.TabStop = true;
            this.radioButton12.Text = "Getting Report Data From monthly Bill";
            this.radioButton12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButton12.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.pictureBox3);
            this.groupBox6.Controls.Add(this.pictureBox4);
            this.groupBox6.Controls.Add(this.radioButton3);
            this.groupBox6.Controls.Add(this.radioButton4);
            this.groupBox6.Location = new System.Drawing.Point(597, 11);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(201, 133);
            this.groupBox6.TabIndex = 54;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Paper Format";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(6, 86);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(86, 42);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(28, 22);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(45, 62);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Checked = true;
            this.radioButton3.Location = new System.Drawing.Point(98, 39);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(72, 21);
            this.radioButton3.TabIndex = 49;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Portrait";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(98, 99);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(96, 21);
            this.radioButton4.TabIndex = 50;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Landscape";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // prntdatedaily
            // 
            this.prntdatedaily.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.prntdatedaily.Location = new System.Drawing.Point(665, 150);
            this.prntdatedaily.Name = "prntdatedaily";
            this.prntdatedaily.Size = new System.Drawing.Size(72, 25);
            this.prntdatedaily.TabIndex = 45;
            this.prntdatedaily.Text = "PDF";
            this.prntdatedaily.UseVisualStyleBackColor = true;
            this.prntdatedaily.Click += new System.EventHandler(this.prntdatedaily_Click);
            // 
            // SelectItemsDaily
            // 
            this.SelectItemsDaily.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SelectItemsDaily.Location = new System.Drawing.Point(173, 132);
            this.SelectItemsDaily.Name = "SelectItemsDaily";
            this.SelectItemsDaily.Size = new System.Drawing.Size(94, 32);
            this.SelectItemsDaily.TabIndex = 43;
            this.SelectItemsDaily.Text = "Select Items";
            this.SelectItemsDaily.UseVisualStyleBackColor = true;
            this.SelectItemsDaily.Click += new System.EventHandler(this.button2_Click);
            // 
            // faDatePicker2
            // 
            this.faDatePicker2.HasButtons = true;
            this.faDatePicker2.Location = new System.Drawing.Point(154, 85);
            this.faDatePicker2.Name = "faDatePicker2";
            this.faDatePicker2.Readonly = true;
            this.faDatePicker2.Size = new System.Drawing.Size(113, 20);
            this.faDatePicker2.TabIndex = 39;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(98, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 17);
            this.label6.TabIndex = 38;
            this.label6.Text = " Date:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.groupBox9);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.prtprwitmdaily);
            this.groupBox1.Controls.Add(this.selectItmDaily);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.faDatePicker1);
            this.groupBox1.Controls.Add(this.faDatePickerbill);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.groupBox13);
            this.groupBox1.Controls.Add(this.groupBox11);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox1.Location = new System.Drawing.Point(9, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(816, 194);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Item-Base Report";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // button5
            // 
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Location = new System.Drawing.Point(738, 158);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(72, 25);
            this.button5.TabIndex = 58;
            this.button5.Text = "Excel";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Location = new System.Drawing.Point(577, 158);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(84, 25);
            this.button2.TabIndex = 55;
            this.button2.Text = "Add Chart";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.radioButton10);
            this.groupBox9.Controls.Add(this.radioButton9);
            this.groupBox9.Location = new System.Drawing.Point(364, 79);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox9.Size = new System.Drawing.Size(207, 73);
            this.groupBox9.TabIndex = 54;
            this.groupBox9.TabStop = false;
            // 
            // radioButton10
            // 
            this.radioButton10.AutoSize = true;
            this.radioButton10.Location = new System.Drawing.Point(155, 18);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButton10.Size = new System.Drawing.Size(46, 21);
            this.radioButton10.TabIndex = 1;
            this.radioButton10.TabStop = true;
            this.radioButton10.Text = "اوليه";
            this.radioButton10.UseVisualStyleBackColor = true;
            this.radioButton10.Visible = false;
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Checked = true;
            this.radioButton9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.radioButton9.Location = new System.Drawing.Point(1, 45);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButton9.Size = new System.Drawing.Size(201, 17);
            this.radioButton9.TabIndex = 0;
            this.radioButton9.TabStop = true;
            this.radioButton9.Text = "Getting Report Data From monthly Bill";
            this.radioButton9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButton9.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.pictureBox2);
            this.groupBox5.Controls.Add(this.pictureBox1);
            this.groupBox5.Controls.Add(this.radioButton1);
            this.groupBox5.Controls.Add(this.radioButton2);
            this.groupBox5.Location = new System.Drawing.Point(577, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(233, 140);
            this.groupBox5.TabIndex = 53;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Paper Format";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(6, 93);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(86, 41);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(28, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(45, 61);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(118, 39);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(72, 21);
            this.radioButton1.TabIndex = 49;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Portrait";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(118, 99);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(96, 21);
            this.radioButton2.TabIndex = 50;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Landscape";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // prtprwitmdaily
            // 
            this.prtprwitmdaily.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.prtprwitmdaily.Location = new System.Drawing.Point(667, 158);
            this.prtprwitmdaily.Name = "prtprwitmdaily";
            this.prtprwitmdaily.Size = new System.Drawing.Size(66, 25);
            this.prtprwitmdaily.TabIndex = 48;
            this.prtprwitmdaily.Text = "PDF";
            this.prtprwitmdaily.UseVisualStyleBackColor = true;
            this.prtprwitmdaily.Click += new System.EventHandler(this.button7_Click);
            // 
            // selectItmDaily
            // 
            this.selectItmDaily.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.selectItmDaily.Location = new System.Drawing.Point(173, 140);
            this.selectItmDaily.Name = "selectItmDaily";
            this.selectItmDaily.Size = new System.Drawing.Size(94, 34);
            this.selectItmDaily.TabIndex = 45;
            this.selectItmDaily.Text = "Select Item";
            this.selectItmDaily.UseVisualStyleBackColor = true;
            this.selectItmDaily.Click += new System.EventHandler(this.button4_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(221, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 17);
            this.label4.TabIndex = 37;
            this.label4.Text = "To";
            // 
            // faDatePicker1
            // 
            this.faDatePicker1.HasButtons = true;
            this.faDatePicker1.Location = new System.Drawing.Point(252, 95);
            this.faDatePicker1.Name = "faDatePicker1";
            this.faDatePicker1.Readonly = true;
            this.faDatePicker1.Size = new System.Drawing.Size(106, 20);
            this.faDatePicker1.TabIndex = 36;
            // 
            // faDatePickerbill
            // 
            this.faDatePickerbill.HasButtons = true;
            this.faDatePickerbill.Location = new System.Drawing.Point(104, 95);
            this.faDatePickerbill.Name = "faDatePickerbill";
            this.faDatePickerbill.Readonly = true;
            this.faDatePickerbill.Size = new System.Drawing.Size(111, 20);
            this.faDatePickerbill.TabIndex = 34;
            this.faDatePickerbill.ValueChanged += new System.EventHandler(this.faDatePickerbill_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "From:";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.rdnri);
            this.groupBox13.Controls.Add(this.rdtolid);
            this.groupBox13.Controls.Add(this.rddeclare);
            this.groupBox13.Location = new System.Drawing.Point(17, 26);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(554, 46);
            this.groupBox13.TabIndex = 57;
            this.groupBox13.TabStop = false;
            this.groupBox13.Visible = false;
            // 
            // rdnri
            // 
            this.rdnri.AutoSize = true;
            this.rdnri.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdnri.Checked = true;
            this.rdnri.Location = new System.Drawing.Point(370, 14);
            this.rdnri.Name = "rdnri";
            this.rdnri.Size = new System.Drawing.Size(140, 21);
            this.rdnri.TabIndex = 1;
            this.rdnri.TabStop = true;
            this.rdnri.Text = "صورتحساب محاسبه شده";
            this.rdnri.UseVisualStyleBackColor = true;
            this.rdnri.Click += new System.EventHandler(this.rdnri_Click);
            // 
            // rdtolid
            // 
            this.rdtolid.AutoSize = true;
            this.rdtolid.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rdtolid.Location = new System.Drawing.Point(175, 13);
            this.rdtolid.Name = "rdtolid";
            this.rdtolid.Size = new System.Drawing.Size(183, 21);
            this.rdtolid.TabIndex = 0;
            this.rdtolid.Text = "توليد خالص ساعات پيك و غير پيك";
            this.rdtolid.UseVisualStyleBackColor = true;
            this.rdtolid.Click += new System.EventHandler(this.rdtolid_Click);
            // 
            // rddeclare
            // 
            this.rddeclare.AutoSize = true;
            this.rddeclare.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rddeclare.Location = new System.Drawing.Point(6, 13);
            this.rddeclare.Name = "rddeclare";
            this.rddeclare.Size = new System.Drawing.Size(156, 21);
            this.rddeclare.TabIndex = 0;
            this.rddeclare.Text = "آمادگي ساعات پيك وغير پيك";
            this.rddeclare.UseVisualStyleBackColor = true;
            this.rddeclare.Click += new System.EventHandler(this.rddeclare_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.textBox7);
            this.groupBox11.Controls.Add(this.textBox2);
            this.groupBox11.Controls.Add(this.label2);
            this.groupBox11.Controls.Add(this.timedomain);
            this.groupBox11.Location = new System.Drawing.Point(17, 22);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(364, 49);
            this.groupBox11.TabIndex = 56;
            this.groupBox11.TabStop = false;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(186, 19);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(30, 23);
            this.textBox7.TabIndex = 45;
            this.textBox7.Text = "1";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(253, 19);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(30, 23);
            this.textBox2.TabIndex = 44;
            this.textBox2.Text = "24";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(222, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 17);
            this.label2.TabIndex = 43;
            this.label2.Text = "To";
            // 
            // timedomain
            // 
            this.timedomain.AutoSize = true;
            this.timedomain.Location = new System.Drawing.Point(81, 19);
            this.timedomain.Name = "timedomain";
            this.timedomain.Size = new System.Drawing.Size(89, 17);
            this.timedomain.TabIndex = 42;
            this.timedomain.Text = "Hour Range:";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.CornflowerBlue;
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Controls.Add(this.radioButton15);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(825, 450);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Monthly";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.comboBox3);
            this.panel2.Controls.Add(this.comboBox4);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Location = new System.Drawing.Point(278, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(273, 29);
            this.panel2.TabIndex = 58;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "1",
            "10^1",
            "10^2",
            "10^3",
            "10^4",
            "10^5",
            "10^6",
            "10^7",
            "10^8",
            "10^9"});
            this.comboBox3.Location = new System.Drawing.Point(73, 4);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(52, 21);
            this.comboBox3.TabIndex = 1;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBox4.Location = new System.Drawing.Point(191, 5);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(60, 21);
            this.comboBox4.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(135, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Scientific :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(21, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Decimal :";
            // 
            // radioButton15
            // 
            this.radioButton15.AutoSize = true;
            this.radioButton15.Checked = true;
            this.radioButton15.Location = new System.Drawing.Point(6, 5);
            this.radioButton15.Name = "radioButton15";
            this.radioButton15.Size = new System.Drawing.Size(52, 17);
            this.radioButton15.TabIndex = 7;
            this.radioButton15.TabStop = true;
            this.radioButton15.Text = "IGMC";
            this.radioButton15.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.PowderBlue;
            this.groupBox4.Controls.Add(this.groupBox3);
            this.groupBox4.Controls.Add(this.groupBox7);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox4.Location = new System.Drawing.Point(6, 36);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(816, 362);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Item-Base Report";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.selectItmMonthly);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.radioButton8);
            this.groupBox3.Controls.Add(this.radioButton7);
            this.groupBox3.Controls.Add(this.addDateToListbox);
            this.groupBox3.Controls.Add(this.faDatePicker3);
            this.groupBox3.Controls.Add(this.listBox1);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.faDatePicker4);
            this.groupBox3.Controls.Add(this.faDatePicker5);
            this.groupBox3.Location = new System.Drawing.Point(6, 33);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(496, 276);
            this.groupBox3.TabIndex = 59;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Select Months";
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button4.Location = new System.Drawing.Point(383, 232);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(104, 38);
            this.button4.TabIndex = 60;
            this.button4.Text = "Remove Selected Item";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // selectItmMonthly
            // 
            this.selectItmMonthly.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.selectItmMonthly.Location = new System.Drawing.Point(163, 189);
            this.selectItmMonthly.Name = "selectItmMonthly";
            this.selectItmMonthly.Size = new System.Drawing.Size(105, 37);
            this.selectItmMonthly.TabIndex = 66;
            this.selectItmMonthly.Text = "Select Items";
            this.selectItmMonthly.UseVisualStyleBackColor = true;
            this.selectItmMonthly.Click += new System.EventHandler(this.selectItmMonthly_Click_1);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(324, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(49, 31);
            this.button1.TabIndex = 71;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Location = new System.Drawing.Point(50, 87);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(83, 21);
            this.radioButton8.TabIndex = 70;
            this.radioButton8.Text = "Selective";
            this.radioButton8.UseVisualStyleBackColor = true;
            this.radioButton8.CheckedChanged += new System.EventHandler(this.radioButton8_CheckedChanged);
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(6, 37);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(72, 21);
            this.radioButton7.TabIndex = 69;
            this.radioButton7.Text = " Range";
            this.radioButton7.UseVisualStyleBackColor = true;
            this.radioButton7.Visible = false;
            this.radioButton7.CheckedChanged += new System.EventHandler(this.radioButton7_CheckedChanged);
            // 
            // addDateToListbox
            // 
            this.addDateToListbox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addDateToListbox.Location = new System.Drawing.Point(283, 87);
            this.addDateToListbox.Name = "addDateToListbox";
            this.addDateToListbox.Size = new System.Drawing.Size(49, 31);
            this.addDateToListbox.TabIndex = 68;
            this.addDateToListbox.Text = "Add";
            this.addDateToListbox.UseVisualStyleBackColor = true;
            this.addDateToListbox.Click += new System.EventHandler(this.addDateToListbox_Click_1);
            // 
            // faDatePicker3
            // 
            this.faDatePicker3.HasButtons = true;
            this.faDatePicker3.Location = new System.Drawing.Point(155, 88);
            this.faDatePicker3.Name = "faDatePicker3";
            this.faDatePicker3.Readonly = true;
            this.faDatePicker3.Size = new System.Drawing.Size(113, 20);
            this.faDatePicker3.TabIndex = 67;
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(383, 22);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(104, 204);
            this.listBox1.TabIndex = 66;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(188, 38);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 17);
            this.label15.TabIndex = 62;
            this.label15.Text = "To";
            this.label15.Visible = false;
            // 
            // faDatePicker4
            // 
            this.faDatePicker4.HasButtons = true;
            this.faDatePicker4.Location = new System.Drawing.Point(219, 38);
            this.faDatePicker4.Name = "faDatePicker4";
            this.faDatePicker4.Readonly = true;
            this.faDatePicker4.Size = new System.Drawing.Size(94, 20);
            this.faDatePicker4.TabIndex = 61;
            this.faDatePicker4.Visible = false;
            // 
            // faDatePicker5
            // 
            this.faDatePicker5.HasButtons = true;
            this.faDatePicker5.Location = new System.Drawing.Point(84, 38);
            this.faDatePicker5.Name = "faDatePicker5";
            this.faDatePicker5.Readonly = true;
            this.faDatePicker5.Size = new System.Drawing.Size(98, 20);
            this.faDatePicker5.TabIndex = 60;
            this.faDatePicker5.Visible = false;
            this.faDatePicker5.ValueChanged += new System.EventHandler(this.faDatePicker5_ValueChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btnmontexp);
            this.groupBox7.Controls.Add(this.pictureBox5);
            this.groupBox7.Controls.Add(this.radioButton5);
            this.groupBox7.Controls.Add(this.prntitmmonthly);
            this.groupBox7.Controls.Add(this.pictureBox6);
            this.groupBox7.Controls.Add(this.radioButton6);
            this.groupBox7.Location = new System.Drawing.Point(561, 22);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(201, 281);
            this.groupBox7.TabIndex = 54;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Paper Format";
            // 
            // btnmontexp
            // 
            this.btnmontexp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnmontexp.Location = new System.Drawing.Point(102, 236);
            this.btnmontexp.Name = "btnmontexp";
            this.btnmontexp.Size = new System.Drawing.Size(79, 30);
            this.btnmontexp.TabIndex = 51;
            this.btnmontexp.Text = "Excel";
            this.btnmontexp.UseVisualStyleBackColor = true;
            this.btnmontexp.Click += new System.EventHandler(this.btnmontexp_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(10, 153);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(86, 51);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Checked = true;
            this.radioButton5.Location = new System.Drawing.Point(102, 71);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(72, 21);
            this.radioButton5.TabIndex = 49;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "Portrait";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // prntitmmonthly
            // 
            this.prntitmmonthly.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.prntitmmonthly.Location = new System.Drawing.Point(19, 236);
            this.prntitmmonthly.Name = "prntitmmonthly";
            this.prntitmmonthly.Size = new System.Drawing.Size(77, 30);
            this.prntitmmonthly.TabIndex = 3;
            this.prntitmmonthly.Text = "PDF";
            this.prntitmmonthly.UseVisualStyleBackColor = true;
            this.prntitmmonthly.Click += new System.EventHandler(this.prntitmmonthly_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(24, 54);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(50, 65);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 0;
            this.pictureBox6.TabStop = false;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(102, 166);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(96, 21);
            this.radioButton6.TabIndex = 50;
            this.radioButton6.Text = "Landscape";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(680, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 59;
            this.label11.Text = "PPID :";
            // 
            // CMBPLANT
            // 
            this.CMBPLANT.FormattingEnabled = true;
            this.CMBPLANT.Location = new System.Drawing.Point(724, 13);
            this.CMBPLANT.Name = "CMBPLANT";
            this.CMBPLANT.Size = new System.Drawing.Size(121, 21);
            this.CMBPLANT.TabIndex = 58;
            // 
            // PrintFinancialReportEx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(857, 530);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tabprintreport);
            this.Controls.Add(this.CMBPLANT);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PrintFinancialReportEx";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Report";
            this.Load += new System.EventHandler(this.PrintFinancialReportEx_Load);
            this.tabprintreport.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabprintreport;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox2;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button SelectItemsDaily;
        private System.Windows.Forms.Button selectItmDaily;
        private System.Windows.Forms.Button prntitmmonthly;
        private System.Windows.Forms.Button prntdatedaily;
        private System.Windows.Forms.Button prtprwitmdaily;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.RadioButton radioButton12;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button addDateToListbox;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker3;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label15;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker4;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker5;
        private System.Windows.Forms.Button selectItmMonthly;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        public FarsiLibrary.Win.Controls.FADatePicker faDatePicker1;
        public FarsiLibrary.Win.Controls.FADatePicker faDatePickerbill;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton radioButton14;
        private System.Windows.Forms.RadioButton radioButton13;
        private System.Windows.Forms.RadioButton radioButton15;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label timedomain;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.RadioButton rdtolid;
        private System.Windows.Forms.RadioButton rddeclare;
        private System.Windows.Forms.RadioButton rdnri;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button btnmontexp;
        public System.Windows.Forms.ComboBox CMBPLANT;
    }
}