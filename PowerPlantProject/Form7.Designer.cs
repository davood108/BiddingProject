﻿namespace PowerPlantProject
{
    partial class Form7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CapacityTb = new System.Windows.Forms.TextBox();
            this.LoadTb = new System.Windows.Forms.TextBox();
            this.PriceTb = new System.Windows.Forms.TextBox();
            this.M005Tb = new System.Windows.Forms.TextBox();
            this.M002Tb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnM002 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.txtProxy = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CapacityTb
            // 
            this.CapacityTb.BackColor = System.Drawing.SystemColors.Window;
            this.CapacityTb.Location = new System.Drawing.Point(158, 206);
            this.CapacityTb.Name = "CapacityTb";
            this.CapacityTb.ReadOnly = true;
            this.CapacityTb.Size = new System.Drawing.Size(227, 20);
            this.CapacityTb.TabIndex = 15;
            this.CapacityTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CapacityTb_MouseClick);
            // 
            // LoadTb
            // 
            this.LoadTb.BackColor = System.Drawing.SystemColors.Window;
            this.LoadTb.Location = new System.Drawing.Point(158, 172);
            this.LoadTb.Name = "LoadTb";
            this.LoadTb.ReadOnly = true;
            this.LoadTb.Size = new System.Drawing.Size(227, 20);
            this.LoadTb.TabIndex = 13;
            this.LoadTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.LoadTb_MouseClick);
            // 
            // PriceTb
            // 
            this.PriceTb.BackColor = System.Drawing.SystemColors.Window;
            this.PriceTb.Location = new System.Drawing.Point(158, 142);
            this.PriceTb.Name = "PriceTb";
            this.PriceTb.ReadOnly = true;
            this.PriceTb.Size = new System.Drawing.Size(227, 20);
            this.PriceTb.TabIndex = 12;
            this.PriceTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PriceTb_MouseClick);
            // 
            // M005Tb
            // 
            this.M005Tb.BackColor = System.Drawing.SystemColors.Window;
            this.M005Tb.Location = new System.Drawing.Point(158, 112);
            this.M005Tb.Name = "M005Tb";
            this.M005Tb.ReadOnly = true;
            this.M005Tb.Size = new System.Drawing.Size(227, 20);
            this.M005Tb.TabIndex = 10;
            this.M005Tb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.M005Tb_MouseClick);
            // 
            // M002Tb
            // 
            this.M002Tb.BackColor = System.Drawing.SystemColors.Window;
            this.M002Tb.Location = new System.Drawing.Point(158, 82);
            this.M002Tb.Name = "M002Tb";
            this.M002Tb.ReadOnly = true;
            this.M002Tb.Size = new System.Drawing.Size(227, 20);
            this.M002Tb.TabIndex = 7;
            this.M002Tb.Tag = "";
            this.M002Tb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.M002Tb_MouseClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.LightCyan;
            this.label5.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label5.Location = new System.Drawing.Point(31, 205);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Capacity Factors :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.LightCyan;
            this.label4.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label4.Location = new System.Drawing.Point(31, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Load Forecasting Files :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.LightCyan;
            this.label3.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label3.Location = new System.Drawing.Point(31, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Average Price Files :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.LightCyan;
            this.label2.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label2.Location = new System.Drawing.Point(31, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "M005 Files :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.LightCyan;
            this.label1.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label1.Location = new System.Drawing.Point(31, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "M002 Files :";
            // 
            // SaveBtn
            // 
            this.SaveBtn.BackColor = System.Drawing.Color.SkyBlue;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SaveBtn.Location = new System.Drawing.Point(310, 256);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(75, 23);
            this.SaveBtn.TabIndex = 16;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.UseVisualStyleBackColor = false;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // btnM002
            // 
            this.btnM002.BackColor = System.Drawing.Color.PowderBlue;
            this.btnM002.Image = global::PowerPlantProject.Properties.Resources.open;
            this.btnM002.Location = new System.Drawing.Point(391, 80);
            this.btnM002.Name = "btnM002";
            this.btnM002.Size = new System.Drawing.Size(33, 23);
            this.btnM002.TabIndex = 17;
            this.btnM002.UseVisualStyleBackColor = false;
            this.btnM002.Click += new System.EventHandler(this.btnM002_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.PowderBlue;
            this.button2.Image = global::PowerPlantProject.Properties.Resources.open;
            this.button2.Location = new System.Drawing.Point(391, 109);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(33, 23);
            this.button2.TabIndex = 18;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.PowderBlue;
            this.button3.Image = global::PowerPlantProject.Properties.Resources.open;
            this.button3.Location = new System.Drawing.Point(391, 139);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(33, 23);
            this.button3.TabIndex = 19;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.PowderBlue;
            this.button4.Image = global::PowerPlantProject.Properties.Resources.open;
            this.button4.Location = new System.Drawing.Point(391, 170);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(33, 23);
            this.button4.TabIndex = 20;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.PowderBlue;
            this.button5.Image = global::PowerPlantProject.Properties.Resources.open;
            this.button5.Location = new System.Drawing.Point(391, 203);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(33, 23);
            this.button5.TabIndex = 21;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // txtProxy
            // 
            this.txtProxy.BackColor = System.Drawing.SystemColors.Window;
            this.txtProxy.Location = new System.Drawing.Point(158, 51);
            this.txtProxy.Name = "txtProxy";
            this.txtProxy.Size = new System.Drawing.Size(227, 20);
            this.txtProxy.TabIndex = 23;
            this.txtProxy.Tag = "";
            this.txtProxy.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtProxy_MouseClick_1);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.LightCyan;
            this.label6.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label6.Location = new System.Drawing.Point(31, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Server Proxy :";
            // 
            // Form7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PowerPlantProject.Properties.Resources._10;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(468, 303);
            this.Controls.Add(this.txtProxy);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnM002);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.CapacityTb);
            this.Controls.Add(this.LoadTb);
            this.Controls.Add(this.PriceTb);
            this.Controls.Add(this.M005Tb);
            this.Controls.Add(this.M002Tb);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "Form7";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Path";
            this.Load += new System.EventHandler(this.Form7_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox CapacityTb;
        private System.Windows.Forms.TextBox LoadTb;
        private System.Windows.Forms.TextBox PriceTb;
        private System.Windows.Forms.TextBox M005Tb;
        private System.Windows.Forms.TextBox M002Tb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button btnM002;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txtProxy;
        private System.Windows.Forms.Label label6;
    }
}