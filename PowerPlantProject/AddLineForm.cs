﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    public partial class AddLineForm : Form
    {
        public string result;
        string ConStr;
        public AddLineForm()
        {
           InitializeComponent();
           try
           {
               this.BackColor =FormColors.GetColor().Formbackcolor;

               //////////////////////////buttons/////////////////////////////

               foreach (Control c in this.Controls)
               {
                   if (c is Button)
                       c.BackColor = FormColors.GetColor().Buttonbackcolor;
               }

               //////////////////////////////panel,groupbox/////////////////////////////;
               foreach (Control c in this.Controls)
               {
                   if (c is Panel || c is GroupBox)
                       c.BackColor = FormColors.GetColor().Panelbackcolor;

               }
               //////////////////////////////////////////////////////////////////////////
               ////////////////////////////////textbox /////////////////////////////////////

               foreach (Control c in this.Controls)
               {
                   if (c is TextBox || c is ListBox)
                   {
                       c.BackColor = FormColors.GetColor().Textbackcolor;
                   }
               }

               //////////////////////////////////////////////////////////////////////////////
           }
           catch
           {

           }
        }

        private void ODSaveBtn_Click(object sender, EventArgs e)
        {
            if ((errorProvider1.GetError(LineCodeTb)=="")&&(errorProvider1.GetError(FromBusTB)=="")&&(errorProvider1.GetError(ToBusTb)=="")&&(errorProvider1.GetError(LineLengthTb)=="")&&(errorProvider1.GetError(CapacityTb)==""))
            {
                result = LineCodeTb.Text+","+FromBusTB.Text+","+ToBusTb.Text+","+LineLengthTb.Text+","+CapacityTb.Text+","+StartTb.Text+","+EndTb.Text;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void LineCodeTb_Validated(object sender, EventArgs e)
        {
            if (LineCodeTb.Text == "")
                errorProvider1.SetError(LineCodeTb, "Insert LineCode!");
            else errorProvider1.SetError(LineCodeTb, "");
        }

        private void FromBusTB_Validated(object sender, EventArgs e)
        {
            try
            {
                if (FromBusTB.Text != "")
                {
                    try
                    {
                        int i = int.Parse(FromBusTB.Text);
                    }
                    catch
                    {
                        string f = (string)FromBusTB.Text;
                    }


                }
                errorProvider1.SetError(FromBusTB, "");
            }
            catch
            {
                errorProvider1.SetError(FromBusTB, "just Integer or string!");
            }
        }

        private void ToBusTb_Validated(object sender, EventArgs e)
        {
            try
            {
                if (ToBusTb.Text != "")
                {
                    try
                    {
                        int i = int.Parse(ToBusTb.Text);
                    }
                    catch
                    {
                        string h = (string)ToBusTb.Text;
                    }
                }
                errorProvider1.SetError(ToBusTb, "");
            }
            catch
            {
                errorProvider1.SetError(ToBusTb, "just Integer or string !");
            }
        }

        private void LineLengthTb_Validated(object sender, EventArgs e)
        {
            try
            {
                if (LineLengthTb.Text != "")
                {
                    double i = double.Parse(LineLengthTb.Text);
                }
                errorProvider1.SetError(LineLengthTb, "");
            }
            catch
            {
                errorProvider1.SetError(LineLengthTb, "just Number!");
            }
        }

        private void CapacityTb_Validated(object sender, EventArgs e)
        {
            try
            {
                if (CapacityTb.Text != "")
                {
                    double i = double.Parse(CapacityTb.Text);
                }
                errorProvider1.SetError(CapacityTb, "");
            }
            catch
            {
                errorProvider1.SetError(CapacityTb, "just Number!");
            }
        }


      
      

   
    }
}
