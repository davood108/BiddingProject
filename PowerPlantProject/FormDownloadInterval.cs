﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Auto_Update_Library;
using FarsiLibrary.Utils;
using System.Data.SqlClient;
using System.Threading;
using NRI.SBS.Common;
using System.Net;
using System.Xml.Serialization;
using System.IO;

namespace PowerPlantProject
{
    public partial class FormDownloadInterval : Form
    {
        Thread thread;
        List<PersianDate> missingDates;
        string optionToDownload = "";
        int progressBarCounter;
        bool finish = true;

        List<string> PPIDArray = new List<string>();
        List<bool> CombinedPlantStatus = new List<bool>();
        bool hardinternet = false;
        public FormDownloadInterval()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void FormDownloadInterval_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.Form.CheckForIllegalCrossThreadCalls = false;
            Resestlist();
            label5.Text += "  " + BaseData.GetInstance().M009Path.ToString().Trim();
            InitializeComponents();
        }
        private void InitializeComponents()
        {

            datePickerFrom.SelectedDateTime = DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0, 0));
            datePickerTo.SelectedDateTime = DateTime.Now;
            //cmbFiles.Items.Add(ItemsToBeDownloaded.M002);
            //cmbFiles.Items.Add(ItemsToBeDownloaded.M005);
            //cmbFiles.Items.Add("Dispatchable");
            
            //cmbFiles.Items.Add(ItemsToBeDownloaded.ChartPrice);
            //cmbFiles.Items.Add(ItemsToBeDownloaded.Lfc);
            //cmbFiles.Items.Add("Internet");

            /////////////////////////////////////////////////////////
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            SqlConnection myConnection = SingleToneConnectionManager.GetInstance().Connection;


            Myda.SelectCommand = new SqlCommand("SELECT PPID FROM PowerPlant", myConnection);
            Myda.Fill(MyDS, "ppid");

            foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
            {
                string strPPID = MyRow["PPID"].ToString().Trim();
                PPIDArray.Add(strPPID);

                //////////////////////////

                SqlCommand mycom = new SqlCommand();
                mycom.Connection = myConnection;
                mycom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id SELECT @result2 =count(PPID) FROM [PPUnit] WHERE PPID=@id AND PackageType LIKE 'Combined Cycle%'";
                mycom.Parameters.Add("@id", SqlDbType.NChar, 10);
                mycom.Parameters["@id"].Value = strPPID;
                mycom.Parameters.Add("@result1", SqlDbType.Int);
                mycom.Parameters["@result1"].Direction = ParameterDirection.Output;
                mycom.Parameters.Add("@result2", SqlDbType.Int);
                mycom.Parameters["@result2"].Direction = ParameterDirection.Output;
                mycom.ExecuteNonQuery();
                int result1 = (int)mycom.Parameters["@result1"].Value;
                int result2 = (int)mycom.Parameters["@result2"].Value;
                if ((result1 > 1) && (result2 > 0))
                    CombinedPlantStatus.Add(true);
                else
                    CombinedPlantStatus.Add(false);
            }

            myConnection.Close();
        }

        private void Resestlist()
        {
            txtdownloadexistfile.Text = "";
            DataTable odata002 = Utilities.GetTable("select max (TargetMarketDate) from dbo.MainFRM002");
            DataTable odata005 = Utilities.GetTable("select max(TargetMarketDate)from dbo.MainFRM005");
            DataTable odata005ba = Utilities.GetTable("select max(TargetMarketDate)from dbo.baMainFRM005");
            DataTable odatavg = Utilities.GetTable("select max(Date) from dbo.AveragePrice");
            DataTable odatalfc = Utilities.GetTable("select max(DateEstimate)from dbo.LoadForecasting");
            DataTable odataunitcomp = Utilities.GetTable("select max(Date) from dbo.UnitNetComp");
            DataTable odatarep12 = Utilities.GetTable("select max(Date)from dbo.Rep12Page");
            DataTable odataregion = Utilities.GetTable("select max(Date) from dbo.RegionNetComp");
            DataTable odataproduced = Utilities.GetTable("select max(Date) from dbo.ProducedEnergy");
            //DataTable odataoutage = utilities.GetTable("select max(Date) from dbo.OutageUnits");
            DataTable odatamanategh = Utilities.GetTable("select max(Date) from dbo.Manategh");
            DataTable odatalinenet = Utilities.GetTable("select max(Date) from dbo.LineNetComp");
            DataTable odatainterchange = Utilities.GetTable("select max(Date) from dbo.InterchangedEnergy");
            DataTable odatadispatchable = Utilities.GetTable("select max(StartDate) from Dispathable");
            //DataTable osaledenergy = utilities.GetTable("select max(date) from dbo.SaledEnergy");
            DataTable osaledenergyfec = Utilities.GetTable("select max(date) from dbo.fuelSaledEnergyec");
            DataTable osaledenergyfuc = Utilities.GetTable("select max(date) from dbo.fuelSaledEnergyuc");
            DataTable osaledenergyUC = Utilities.GetTable("select max(date) from SaledEnergyuc");
            DataTable osaledenergyEC = Utilities.GetTable("select max(date) from SaledEnergyEc");
            DataTable monthlybillplant = Utilities.GetTable("select max(date) from monthlybillplant");
            DataTable monthlybilltotal = Utilities.GetTable("select max(month) from monthlybilltotal");
            DataTable monthlybilldate = Utilities.GetTable("select max(date) from monthlybilldate");





            txtdownloadexistfile.Text = "  M002 Files :  " + odata002.Rows[0][0].ToString().Trim() + "   M005 Files :  " + odata005.Rows[0][0].ToString().Trim() + "           M005 With Limmit Files :   " + odata005ba.Rows[0][0].ToString().Trim() + "\r\n\r\n";
            txtdownloadexistfile.Text += "  chart price Files : " + odatavg.Rows[0][0].ToString().Trim() + "  Lfc Files : " + odatalfc.Rows[0][0].ToString().Trim() + "\r\n\r\n";
            txtdownloadexistfile.Text += "  Dispatchable Files : " + odatadispatchable.Rows[0][0].ToString().Trim() + "\r\n\r\n";

            txtdownloadexistfile.Text += "           <<<.............Internet Files............>>>          " + "\r\n\r\n";

            txtdownloadexistfile.Text += "  UnitNetComp Files : " + odataunitcomp.Rows[0][0].ToString().Trim() + "\r\n\r\n";
            txtdownloadexistfile.Text += "  RegionNetComp Files :  " + odataregion.Rows[0][0].ToString().Trim() + "\r\n\r\n";
           // txtdownloadexistfile.Text += "  Rep12Page : " + odatarep12.Rows[0][0].ToString().Trim() + "\r\n\r\n";

            txtdownloadexistfile.Text += "  ProducedEnergy Files : " + odataproduced.Rows[0][0].ToString().Trim() + "\r\n\r\n";
           // txtdownloadexistfile.Text += "  OutageUnits Files : " + odataoutage.Rows[0][0].ToString().Trim() + "\r\n\r\n";
            txtdownloadexistfile.Text += "  Manategh Files : " + odatamanategh.Rows[0][0].ToString().Trim() + "\r\n\r\n";

            txtdownloadexistfile.Text += "  LineNetComp Files : " + odatalinenet.Rows[0][0].ToString().Trim() + "\r\n\r\n";
            txtdownloadexistfile.Text += "  InterchangedEnergy Files : " + odatainterchange.Rows[0][0].ToString().Trim() + "\r\n\r\n";
            txtdownloadexistfile.Text += "  Rep12Pages Files : " + odatarep12.Rows[0][0].ToString().Trim() + "\r\n\r\n";

            if (osaledenergyfuc != null)
            {
                if (osaledenergyfuc.Rows.Count > 0)
                    txtdownloadexistfile.Text += "  Saled Energy UnitCommitment With Fuel  Files : " + osaledenergyfuc.Rows[0][0].ToString().Trim() + "\r\n\r\n";
            }

            if (osaledenergyfec != null)
            {
                if (osaledenergyfec.Rows.Count > 0)
                    txtdownloadexistfile.Text += "  Saled Energy Economic With Fuel  Files : " + osaledenergyfec.Rows[0][0].ToString().Trim() + "\r\n\r\n";
            }

            if (osaledenergyEC != null)
            {
                if (osaledenergyEC.Rows.Count > 0)
                    txtdownloadexistfile.Text += "  Saled Energy Economic Files : " + osaledenergyEC.Rows[0][0].ToString().Trim() + "\r\n\r\n";
            }
            if (osaledenergyUC != null)
            {
                if (osaledenergyUC.Rows.Count > 0)
                    txtdownloadexistfile.Text += "  Saled Energy  UnitCommitment Files : " + osaledenergyUC.Rows[0][0].ToString().Trim() + "\r\n\r\n";
            }


            if (monthlybillplant != null)
            {
                if (monthlybillplant.Rows.Count > 0)
                    txtdownloadexistfile.Text += "  Monthly Bill Plant Files : " + monthlybillplant.Rows[0][0].ToString() + "\r\n\r\n";

            }
            if (monthlybilltotal != null)
            {
                if (monthlybilltotal.Rows.Count > 0)
                    txtdownloadexistfile.Text += "  Monthly Bill total Files : " + monthlybilltotal.Rows[0][0].ToString() + "\r\n\r\n";

            }
            if (monthlybilldate != null)
            {
                if (monthlybilldate.Rows.Count > 0)
                    txtdownloadexistfile.Text += "  Monthly Bill Date Files : " + monthlybilldate.Rows[0][0].ToString() + "\r\n\r\n";

            }


        }
        private void ResetDownloadClass()
        {
            //DataSet MyDS = new DataSet();
            //SqlDataAdapter Myda = new SqlDataAdapter();
            //string strCmd = "SELECT * FROM [Path]";
            //Myda.SelectCommand = new SqlCommand(strCmd, new SqlConnection(ConnectionManager.ConnectionString));
            //Myda.Fill(MyDS);
            //Myda.SelectCommand.Connection.Close();
            //if (MyDS.Tables[0].Rows.Count > 0)
            //{
            //    DataRow row = MyDS.Tables[0].Rows[0];
            //    Download.GetInstance().ProxyAddress = row["Proxy"].ToString().Trim();
            //}
            //Download.GetInstance().ClearMessages();

            SettingParameters settingParam = new SettingParameters();
            XmlSerializer mySerializer = new XmlSerializer(typeof(SettingParameters));
            try
            {
                string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\SBS\\setting.xml";
                FileStream myFileStream = new FileStream(path, FileMode.Open);

                settingParam = (SettingParameters)mySerializer.Deserialize(myFileStream);
                myFileStream.Close();
            }
            catch (System.IO.FileNotFoundException fex)
            {
                throw fex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            Download.GetInstance().ProxyAddress = settingParam.ServerProxy.Trim();
            Download.GetInstance().domain = settingParam.Domain.Trim();
            Download.GetInstance().Username = RijndaelEncoding.Decrypt(settingParam.Username.Trim());
            Download.GetInstance().password = RijndaelEncoding.Decrypt(settingParam.Password.Trim());

            Download.GetInstance().ftppassword = settingParam.FTPPassword.Trim();
            Download.GetInstance().ftpserverip = settingParam.FTPServer.Trim();
            Download.GetInstance().ftpusername = settingParam.FTPUsername.Trim();

            Download.GetInstance().ClearMessages();
         
        }

        private bool CheckDataSufficiency()
        {
            if (optionToDownload == "")
            {
                MessageBox.Show("You should choose an item to download");
                return false;
                //*****************
            }
           // if (optionToDownload == "Internet")
                ResetDownloadClass();

            switch (optionToDownload)
            {
                case "M002":
                    if (BaseData.GetInstance().M002Path == "")
                    {
                        MessageBox.Show("M002 path is not set...");
                        return false;
                    }
                    break;
                case "M005":
                    if (BaseData.GetInstance().M005Path == "")
                    {
                        MessageBox.Show("M005 path is not set...");
                        return false;
                    }
                    break;

                case "Lfc":
                    if (BaseData.GetInstance().LoadForecasting == "")
                    {
                        MessageBox.Show(" LoadForcasting path is not set...");
                        return false;
                    }
                    break;
                case "ChartPrice":
                    if (BaseData.GetInstance().AveragePrice == "")
                    {
                        MessageBox.Show("AveragePrice path is not set...");
                        return false;
                    }
                    break;

                case "SaledEnergy ":
                    if (BaseData.GetInstance().M0091Path == "")
                    {
                        MessageBox.Show("AveragePrice path is not set...");
                        return false;
                    }
                    break;

                case "Dispatchable":
                    if (BaseData.GetInstance().Dispatchable == "")
                    {
                        MessageBox.Show("Dispatchable path is not set...");
                        return false;
                    }
                    break;

                case "DailyBill":
                    if (BaseData.GetInstance().Capacityfactor == "")
                    {
                        MessageBox.Show("Bill path is not set...");
                        return false;
                    }
                    break;
               case "Internet":

                    if (BaseData.GetInstance().M009Path == "" || BaseData.GetInstance().M009Path==null)
                    {
                        MessageBox.Show("Temporary path is not set...");
                        return false;
                    }
                    break;

                //    if (Download.GetInstance().ProxyAddress == "")
                //    {
                //        MessageBox.Show("Proxy Address is not set...");
                //        return false;
                //    }
                //    try
                //    {
                //        WebProxy proxy = new WebProxy(Download.GetInstance().ProxyAddress);
                //        proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                //        WebClient client = new WebClient();
                //        client.Proxy = proxy;
                //    }
                //    catch (Exception exp)
                //    {
                //        MessageBox.Show(exp.Message);
                //        return false;
                //    }
                //    break;
            }
            return true;

        }
        private void btnDownload_Click(object sender, EventArgs e)
        {
          //  optionToDownload = cmbFiles.Text;
            

            if (CheckDataSufficiency())
            {

                finish = false;
                EnableForm();
                progressBarCounter = 0;

                missingDates = GetDatesBetween();
                progressBar1.Maximum = missingDates.Count;

                if (optionToDownload == "Internet")
                {
                    //progressBar1.Maximum = (progressBar1.Maximum * (Enum.GetNames(typeof(ItemsToBeDownloaded)).Length - 5)) + 2;
                    progressBar1.Maximum = (progressBar1.Maximum * 8) + 2;

                }

                else if ((optionToDownload == "ChartPrice") || (optionToDownload == "Lfc") )
                {
                    progressBar1.Maximum = progressBar1.Maximum;

                }
                else if ((optionToDownload == "SaledEnergy"))
                {
                    progressBar1.Maximum = progressBar1.Maximum*4;

                }
                else if ((optionToDownload == "DailyBill"))
                {
                    progressBar1.Maximum = progressBar1.Maximum * 3;

                }
                else
                    progressBar1.Maximum = progressBar1.Maximum * (PPIDArray.Count);


                thread = new Thread(LongTask);
                thread.IsBackground = true;
                thread.Start();
            }

        }

        private void EnableForm()
        {
            //cmbFiles.Enabled = finish;
            rbinternet.Enabled = finish;
            rbDispatch.Enabled = finish;
            rbChartPrice.Enabled = finish;
            rb005.Enabled = finish;
            rdm005ba.Enabled = finish;
            rb002.Enabled = finish;
            rblfc.Enabled = finish;
            rdsaledenergy.Enabled = finish;
            rddailybill.Enabled = finish;

            datePickerFrom.Enabled = finish;
            datePickerTo.Enabled = finish;
            btnDownload.Visible = finish;
            progressBar1.Visible = !finish;
        }
        private void LongTask()
        {
            Download.GetInstance().ClearMessages();
            switch (optionToDownload)
            {
                //this.Cursor = Cursors.WaitCursor;

                case "Dispatchable":
                    foreach (PersianDate date in missingDates)
                        DispatchInterface("Dispatchable", date);
                    break;


                case "M002":
                    foreach (PersianDate date in missingDates)
                        DownLoadInterface(ItemsToBeDownloaded.M002, date);
                    break;
                case "M005":
                    foreach (PersianDate date in missingDates)
                        DownLoadInterface(ItemsToBeDownloaded.M005, date);

                    break;
                case "M005ba":
                    foreach (PersianDate date in missingDates)
                        DownLoadInterface(ItemsToBeDownloaded.M005ba, date);

                    break;

                case "Lfc":
                    foreach (PersianDate date in missingDates)
                        DownLoadInterface(ItemsToBeDownloaded.Lfc, date);

                    break;


                case "ChartPrice":
                    foreach (PersianDate date in missingDates)
                        DownLoadInterface(ItemsToBeDownloaded.ChartPrice, date);

                    break;

                case "SaledEnergy":
                    foreach (PersianDate date in missingDates)
                        EnergyInterface(date.ToString("d"));

                    break;

                case "DailyBill":
                    foreach (PersianDate date in missingDates)
                        BillInterface(date.ToString("d"));

                    break;

                case "Internet":

                    ////////////////////////////
                    //DownLoadInterface(ItemsToBeDownloaded.LoadForcasting, null);
                    //DownLoadInterface(ItemsToBeDownloaded.AveragePrice, null);
                    //////////////////////////
                    foreach (PersianDate date in missingDates)
                        foreach (ItemsToBeDownloaded item in Enum.GetValues(typeof(ItemsToBeDownloaded)))
                            if (item != ItemsToBeDownloaded.AveragePrice && item != ItemsToBeDownloaded.LoadForcasting &&
                                //item != ItemsToBeDownloaded.Rep12Pages &&
                                item != ItemsToBeDownloaded.M005 && item != ItemsToBeDownloaded.M002 && item != ItemsToBeDownloaded.ChartPrice && item != ItemsToBeDownloaded.Lfc && item != ItemsToBeDownloaded.M005ba)
                                DownLoadInterface(item, date);

                    break;

            }
           
            //this.Cursor = Cursors.Default;
            //?????????????????INA ro be dalile dashtane error hazf kardam(Kafaie) 
            if (Download.GetInstance().Messages != "")
               MessageBoxNRI.ShowMessageList("errors:", Download.GetInstance().Messages);
            else
                MessageBox.Show("Download Completed");
            
            finish = true;

            //EnableForm(true);
            Resestlist();
            EnableForm();
        }

        public void UpdateProgressBar(int i)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<int>(UpdateProgressBar), new object[] { i });
                return;
            }

            progressBar1.Value = i;
            Thread.Sleep(1);
        }

        private List<PersianDate> GetDatesBetween()
        {

            //if (strDateFrom == "")
            //{
            //    if (item == Items.Rep12Pages || item == Items.Outage ||
            //        item == Items.Manategh /*|| item == Items.EN */)
            //        strDateFrom = DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)).ToString("d");
            //}
            if (datePickerFrom != null)
            {
                List<PersianDate> missingDates = new List<PersianDate>();

                //DateTime dateFrom = (DateTime)PersianDateConverter.ToGregorianDateTime(new PersianDate(strDateFrom));
                DateTime dateFrom = datePickerFrom.SelectedDateTime.Date;
                DateTime dateTo = datePickerTo.SelectedDateTime.Date; 
                missingDates.Add(dateFrom);

                TimeSpan span = dateTo - dateFrom;
                while (span.Days > 0)
                {
                    dateFrom = dateFrom.AddDays(1);
                    missingDates.Add(new PersianDate(dateFrom));
                    span = dateTo - dateFrom;
                }
                return missingDates;
            }
            else
                return null;
        }

        private void DownLoadInterface(ItemsToBeDownloaded item, PersianDate date)
        {

            string strDate = "";
            if (date != null)
                strDate = date.ToString("d");

            switch (item)
            {
                case ItemsToBeDownloaded.M005:
                    foreach (string PPID in PPIDArray)
                    {
                        Download.GetInstance().M005(strDate, PPID, false);
                        if (CombinedPlantStatus[PPIDArray.IndexOf(PPID)])
                            Download.GetInstance().M005(strDate, PPID, true);

                         UpdateProgressBar(++progressBarCounter);

                    }
                    //13987/02/01 comment
                    //for saina 
                    //foreach (string PPID in PPIDArray)
                    //{
                    //    Download.GetInstance().M005ba(strDate, PPID, false);
                    //    if (CombinedPlantStatus[PPIDArray.IndexOf(PPID)])
                    //        Download.GetInstance().M005ba(strDate, PPID, true);

                    //    //UpdateProgressBar(++progressBarCounter);

                    //}
                    break;
                 ///////////////////////ba mahdodiat/////////////////////

                case ItemsToBeDownloaded.M005ba:
                    foreach (string PPID in PPIDArray)
                    {
                        Download.GetInstance().M005ba(strDate, PPID, false);
                        if (CombinedPlantStatus[PPIDArray.IndexOf(PPID)])
                            Download.GetInstance().M005ba(strDate, PPID, true);

                        UpdateProgressBar(++progressBarCounter);

                    }


                    break;

                case ItemsToBeDownloaded.M002:
                    foreach (string PPID in PPIDArray)
                    {
                        Download.GetInstance().M002(strDate, PPID, false);
                        if (CombinedPlantStatus[PPIDArray.IndexOf(PPID)])
                            Download.GetInstance().M002(strDate, PPID, true);

                         UpdateProgressBar(++progressBarCounter);

                    }
                    break;

                case ItemsToBeDownloaded.Lfc:
                    Download.GetInstance().Lfc(strDate);


                     UpdateProgressBar(++progressBarCounter);

                    break;


                case ItemsToBeDownloaded.ChartPrice:
                    Download.GetInstance().ChartPrice(strDate);

                     UpdateProgressBar(++progressBarCounter);

                    break;






                case ItemsToBeDownloaded.Rep12Pages:
                    Download.GetInstance().Rep12Page(strDate,hardinternet);
                    UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.AveragePrice:
                    Download.GetInstance().AveragePrice();
                    UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.Manategh:
                    Download.GetInstance().Manategh(strDate,hardinternet);
                    UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.InterchangedEnergy:
                    Download.GetInstance().InterchangedEnergy(strDate,hardinternet);
                    UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.lineNetComp:
                    Download.GetInstance().LineNetComp(strDate,hardinternet);
                     UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.RegionNetComp:
                    Download.GetInstance().RegionNetComp(strDate,hardinternet);
                     UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.UnitNetComp:
                    Download.GetInstance().UnitNetComp(strDate,hardinternet);
                    UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.LoadForcasting:
                    Download.GetInstance().LoadForecasting();
                    UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.Outage:
                    //Download.GetInstance().Outage(strDate);
                     UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.ProducedEenergy:
                    Download.GetInstance().ProducedEnergy(strDate,hardinternet);
                    UpdateProgressBar(++progressBarCounter);
                    break;

            }

        }

        //private void cmbFiles_SelectedIndexChanged(object sender, EventArgs e)
        //{
           
        //    Resestlist();

        //}

        private void FormDownloadInterval_Paint(object sender, PaintEventArgs e)
        {
            EnableForm();
        }
        private void DispatchInterface(string item, PersianDate date)
        {

            string strDate = "";
            if (date != null)
                strDate = date.ToString("d");

            switch (item)
            {

                case "Dispatchable":
                    foreach (string PPID in PPIDArray)
                    {
                        Download.GetInstance().M002Dispatch(strDate, PPID, false);
                        if (CombinedPlantStatus[PPIDArray.IndexOf(PPID)])
                            Download.GetInstance().M002Dispatch(strDate, PPID, true);

                        UpdateProgressBar(++progressBarCounter);

                    }
                    break;

            }

        }

        private void rb002_CheckedChanged(object sender, EventArgs e)
        {
            if (rb002.Checked)
            {
                groupBox1.Visible = false;
                optionToDownload = ItemsToBeDownloaded.M002.ToString().Trim();
            }

        }

        private void rb005_CheckedChanged(object sender, EventArgs e)
        {
            if (rb005.Checked)
            {
                optionToDownload = ItemsToBeDownloaded.M005.ToString().Trim();
                groupBox1.Visible = false;
            }
        }

        private void rbDispatch_CheckedChanged(object sender, EventArgs e)
        {
            if (rbDispatch.Checked)
            {
                groupBox1.Visible = false;
                optionToDownload = "Dispatchable";

            }
        }

        private void rbChartPrice_CheckedChanged(object sender, EventArgs e)
        {
            if (rbChartPrice.Checked)
            {
                optionToDownload = ItemsToBeDownloaded.ChartPrice.ToString().Trim();
                groupBox1.Visible = false;
            }
        }

        private void rblfc_CheckedChanged(object sender, EventArgs e)
        {
            if (rblfc.Checked)
            {
                optionToDownload = ItemsToBeDownloaded.Lfc.ToString().Trim();
                groupBox1.Visible = false;
            }
        }

        private void rbinternet_CheckedChanged(object sender, EventArgs e)
        {
            if (rbinternet.Checked)
            {
                optionToDownload = "Internet";
                if (BaseData.GetInstance().Useftp009 == true)
                {
                    hardinternet = false;
                    groupBox1.Visible = false;

                }
                else
                {                    
                    hardinternet = true;
                    groupBox1.Visible = true;
                }
            }
        }

        private void datePickerFrom_ValueChanged(object sender, EventArgs e)
        {
            datePickerTo.Text = datePickerFrom.Text;
            datePickerTo.SelectedDateTime = datePickerFrom.SelectedDateTime;
            label5.Text = "Save To:  " + BaseData.GetInstance().M009Path.ToString().Trim() + @"\" + new PersianDate(datePickerFrom.SelectedDateTime).Year + @"\" + new PersianDate(datePickerFrom.SelectedDateTime).Month + @"\" + new PersianDate(datePickerFrom.SelectedDateTime).Day;

            Resestlist();
        }

        private void rdsaledenergy_CheckedChanged(object sender, EventArgs e)
        {
            if (rdsaledenergy.Checked)
            {
                optionToDownload = "SaledEnergy";
                
               // groupBox1.Visible = false;
            }

        }
        private void rddailybill_CheckedChanged(object sender, EventArgs e)
        {

            if (rddailybill.Checked)
            {
                optionToDownload = "DailyBill";

            }

        }

        private void BillInterface(string strDate)
        {    
            Download.GetInstance().filldailybillsum(strDate);
            UpdateProgressBar(++progressBarCounter);
            Download.GetInstance().filldailybilltotal(strDate);
            UpdateProgressBar(++progressBarCounter);
            Download.GetInstance().filldailybillplant(strDate);
            UpdateProgressBar(++progressBarCounter);
        }
        private void EnergyInterface(string strDate)
        {

            Download.GetInstance().EnergySaledFEC(strDate);


            UpdateProgressBar(++progressBarCounter);


            Download.GetInstance().EnergySaledFUC(strDate);


            UpdateProgressBar(++progressBarCounter);


            Download.GetInstance().EnergySaledUC(strDate);


            UpdateProgressBar(++progressBarCounter);


            Download.GetInstance().EnergySaledEC(strDate);


            UpdateProgressBar(++progressBarCounter);


        }

        private void FormDownloadInterval_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (thread != null)
            {
                thread.Abort();
            }
        }

       
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            System.Diagnostics.Process.Start("http://sccisrep.igmc.ir");
        }

        private void rdm005ba_CheckedChanged(object sender, EventArgs e)
        {
            if (rdm005ba.Checked)
            {
                optionToDownload = ItemsToBeDownloaded.M005ba.ToString().Trim();
                groupBox1.Visible = false;
            }
        }

        private void panel4_Click(object sender, EventArgs e)
        {
            this.Close();
            BillItem  m = new BillItem();
            m.panel1.Visible = true;
            m.Show();

        }
        
    }
}
