﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using NRI.SBS.Common;
using Dundas.Charting.WinControl.ChartTypes;
using Dundas.Charting.WinControl;
using System.Collections;


namespace PowerPlantProject
{
    public partial class MarketResultUnitPlotForm : Form
    {
        public string Date { get; set; }
        public string package { get; set; }
        public string unit { get; set; }
        public string PPId { get; set; }
        public string unitType { get; set; }

        string[] arrP;
        string[] arrRequired;
        string[] arreconomic;
        string[] arrDispatchable;
        string[] arrPrice;
        bool Limmit;
        SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);

        public MarketResultUnitPlotForm(bool limmit)
        {
            Limmit = limmit;
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void MarketResultUnitPlotForm_Load(object sender, EventArgs e)
        {
            FillMRUnitDataSets();
            FillChart();

        }
        //--------------------------------------
        private void FillChart()
        {
            chart1.ChartAreas["Default"].AxisX.Margin = false;
            chart1.ChartAreas["Default"].AxisX.Minimum = 0;
            chart1.ChartAreas["Default"].AxisX.Interval = 1;
            chart1.ChartAreas["Default"].AxisX.Maximum = 24;
           // chart1.ChartAreas["Default"].AxisY.Interval = 20;
            chart1.Series["Power"].Color = Color.Green; ;
            chart1.Series["Dispatchable"].Color = Color.Blue ;
            chart1.Series["Required"].Color = Color.Red;
            chart1.Series[3].Color = Color.Yellow;
            double minP = 0, maxP = 0,minReq=0,maxReq=0,minDis=0,maxDis=0;
            findMinMaxOfArray(arrRequired, ref minReq, ref maxReq);
            findMinMaxOfArray(arreconomic, ref minReq, ref maxReq);
            findMinMaxOfArray(arrDispatchable, ref minDis,ref maxDis);
            findMinMaxOfArray(arrP, ref minP,ref maxP);
            bool isPEmpty = false, isReqEmpty = false, isDisEmpty = false;
            if (minP == 0 && maxP == 0)
                isPEmpty = true;
            if (minReq == 0 && maxReq == 0)
                isReqEmpty = true;
            if (minDis == 0 && maxDis == 0)
                isDisEmpty = true;
            double min = 0, max = 0;
            if (!isPEmpty)
            {
                if (maxP > max)
                    max = maxP;
            }
               
            if (!isReqEmpty)
            {
                if (maxReq > max)
                    max = maxReq;
            }

            if (!isDisEmpty)
            {
                if (maxDis > max)
                    max = maxDis;
            }
            min = max;
            if (!isPEmpty)
            {
                if (minP < min)
                    min = minP;
            }

            if (!isReqEmpty)
            {
                if (minReq < min)
                    min = minReq;
            }

            if (!isDisEmpty)
            {
                if (minDis < min)
                    min = minDis;
            }

            if (min > 500)
                chart1.ChartAreas["Default"].AxisY.Minimum = min - 200;
            else if ((min<= 500)&&(min>100))
                chart1.ChartAreas["Default"].AxisY.Minimum = min - 80;
            else if ((min <= 100) && (min > 50))
                chart1.ChartAreas["Default"].AxisY.Minimum = min - 30;
            else if ((min <= 50) && (min > 30))
                chart1.ChartAreas["Default"].AxisY.Minimum = min - 20;
            else
                chart1.ChartAreas["Default"].AxisY.Minimum = min;
           chart1.ChartAreas["Default"].AxisY.Maximum = max+200;


            for (int pointIndex = 1; pointIndex <= 24; pointIndex++)
            {
                chart1.Series["Power"].Points.AddY(arrP[pointIndex]);
                chart1.Series["Required"].Points.AddY(arrRequired[pointIndex]);
                chart1.Series["Dispatchable"].Points.AddY(arrDispatchable[pointIndex]);
                chart1.Series[3].Points.AddY(arreconomic[pointIndex]);

            }


            // Set point labels
            chart1.Series["Power"].ShowLabelAsValue = false;
            chart1.Series["Required"].ShowLabelAsValue = false;
            chart1.Series["Dispatchable"].ShowLabelAsValue = false;
            chart1.Series[3].ShowLabelAsValue = false;
            // Enable X axis margin
            chart1.ChartAreas["Default"].AxisX.Margin = true;
            chart1.UI.Toolbar.Enabled = true;


            min = 0; max = 0;
            findMinMaxOfArray(arrPrice,ref min,ref max);

            chart2.ChartAreas["Default"].AxisX.Margin = false;
            chart2.ChartAreas["Default"].AxisX.Minimum = 0;
            chart2.ChartAreas["Default"].AxisX.Interval = 1;
            chart2.ChartAreas["Default"].AxisX.Maximum = 24;
            if(min>1500)
                chart2.ChartAreas["Default"].AxisY.Minimum = min-1500;
            //else if ((min< 1000)&&(min>500))
            //    chart2.ChartAreas["Default"].AxisY.Minimum = min - 500;
            else
                chart2.ChartAreas["Default"].AxisY.Minimum = min;
           // chart2.ChartAreas["Default"].AxisY.Interval = 10000;
            chart2.ChartAreas["Default"].AxisY.Maximum = max+1500;



            for (int pointIndex = 1; pointIndex <= 24; pointIndex++)
            {
                chart2.Series["Price"].Points.AddY(arrPrice[pointIndex]);

            }

            chart2.Series["Price"].Color = Color.Red;
            // Set point labels
            chart2.Series["Price"].ShowLabelAsValue = false;
            // Enable X axis margin
            chart2.ChartAreas["Default"].AxisX.Margin = true;
            chart2.UI.Toolbar.Enabled = true;


        }
        
        //-------------------------------------
       
        private void FillMRUnitDataSets()
        {
            string tname = "DetailFRM005";
            if (Limmit) tname = "baDetailFRM005";
            
                string ptypenum = "0";
                if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";
               // if (PPId == "232") ptypenum = "0";
                if (Findcconetype(PPId)) ptypenum = "0";

            if ((Date != null) && (Date != ""))
            {

                //Detect Block For FRM005
                string block005Code = DetectBlockM005();

                string block002Code = DetectBlockM002();


                //Read Required,Dispatchable,Contribution From M005
                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();

                myConnection.Open();

                Myda.SelectCommand = new SqlCommand("SELECT Hour,Required,Dispatchable,Contribution,Economic FROM "+tname+" WHERE PPID= " +
                    PPId + " AND TargetMarketDate='" + Date + "' AND Block='" + block005Code + "'and PPType='"+ptypenum+"'" +
                     "select Hour, P FROM DetailFRM009 WHERE PPID= " + PPId + " AND TargetMarketDate='" + Date + "'" +
                     "AND Block='" + unit + "'", myConnection);
                Myda.Fill(MyDS);
                myConnection.Close();

                    //Fill four Rows of DataGridView (Require,Dispachable,Contribution,Economic)
                arrRequired = new string[25];
                arrDispatchable=new string[25];
                arreconomic = new string[25];


                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    int index = int.Parse(MyRow["Hour"].ToString().Trim());
                    arrRequired[index] = string.Format("{0:n1}", MyDoubleParse(MyRow["Required"].ToString()));
                    arrDispatchable[index] = string.Format("{0:n1}", MyDoubleParse(MyRow["Dispatchable"].ToString()));
                    arreconomic[index] = string.Format("{0:n1}", MyDoubleParse(MyRow["economic"].ToString()));
                }
                    

                    //Fill three Rows of DataGridView (P,QC,QL,Consumed)
                    arrP=new string[25];



                    DataTable tt = Utilities.GetTable("select distinct(PackageCode) from dbo.UnitsDataMain where UnitCode='"+unit+"'and ppid='"+PPId+"'");
                    int pcode = int.Parse(tt.Rows[0][0].ToString());

                
                string strcom = "";
                DataTable dr = null;
                if (unit.Contains("c"))
                {
                    strcom = " select sum( distinct(DetailFRM009.p) ) from  dbo.DetailFRM009 inner join  dbo.UnitsDataMain on dbo.DetailFRM009.Block=dbo.UnitsDataMain.UnitCode where dbo.DetailFRM009.ppid='" +
                        PPId + "'and DetailFRM009.TargetMarketDate='"+Date+"'and DetailFRM009.packagecode='"+pcode + 
                        "'and DetailFRM009.Block in (select UnitsDataMain.UnitCode from UnitsDataMain where PackageType='CC' and PackageCode='" + pcode + 
                        "' and ppid='" + PPId + "'" + ")";
                  
                }
                else
                {
                    strcom = "select P FROM DetailFRM009 WHERE PPID= " + PPId + " AND TargetMarketDate='" + Date + "'AND Block='" + unit + "'";
                    dr = Utilities.GetTable(strcom);

                }

                //foreach (DataRow MyRow in MyDS.Tables[1].Rows)
                //{
                //    int index = int.Parse(MyRow["Hour"].ToString().Trim());
                //    arrP[index] = string.Format("{0:n1}", double.Parse(MyRow["P"].ToString()));

                //}


                   try
                   {

                       for (int i = 0; i < 24; i++)
                       {
                           if (unit.Contains("c"))
                           {
                               string strcom1 = strcom + "and Hour='" + i + "'";
                               DataTable dr1 = Utilities.GetTable(strcom1);
                               if (dr1.Rows.Count > 0)
                               {
                                   arrP[i+1] = string.Format("{0:n1}", MyDoubleParse(dr1.Rows[0][0].ToString()));
                               }
                           }
                           else
                           {
                               if (dr.Rows.Count > 0)
                               {
                                   arrP[i+1] = string.Format("{0:n1}", MyDoubleParse(dr.Rows[i][0].ToString()));

                               }
                           }

                       }

                   }
                   catch
                   {
                       for (int h = 1; h <= 24; h++)
                       {
                           arrP[h] = "0.0";
                       }
                   }

                    //Read Power And Price From M002
                        DataSet MaxDS = new DataSet();
                        SqlDataAdapter Maxda = new SqlDataAdapter();
                        myConnection.Open();
                        Maxda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                        "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10,Hour FROM [DetailFRM002] " +
                        "WHERE Estimated<>1 AND TargetMarketDate='" + Date + "'and PPType='"+ ptypenum+ "' AND Block='" + block002Code + "' AND PPID=" + PPId, myConnection);
                        Maxda.Fill(MaxDS);
                        DataTable PowerPrice = new DataTable();
                        PowerPrice = MaxDS.Tables[0];

                        //Read Previous MaxBid
                        DataSet BidDS = new DataSet();
                        SqlDataAdapter Bidda = new SqlDataAdapter();
                        Bidda.SelectCommand = new SqlCommand("SELECT MaxBid,Hour FROM "+tname+" WHERE TargetMarketDate" +
                        "='" + Date + "' AND PPID=" + PPId + " and PPType='"+ ptypenum +"'AND Block='" + block005Code + "'", myConnection);
                        Bidda.Fill(BidDS);
                        double[] PreMaxBid = new double[24];
                        for (int i = 0; i < 24; i++)
                            PreMaxBid[i] = 0;
                        foreach (DataRow BidRow in BidDS.Tables[0].Rows)
                        {
                            if (BidRow[1].ToString() != "")
                            {
                                int myhour = int.Parse(BidRow[1].ToString()) - 1;
                                if (BidRow[0].ToString() != "")
                                    PreMaxBid[myhour] = MyDoubleParse(BidRow[0].ToString());
                            }
                        }

                        //Detect MaxBid field in MRCurGrid1
                        arrPrice = new string[25];
                        for (int i = 1; i < 25; i++)
                        {
                            double required = MyDoubleParse(arrRequired[i]);
                            double MaxBid = 0;
                            if (required != 0 &&  PowerPrice.Rows.Count > 0)                  
                            {
                                int Dsindex = 0;

                                while ((Dsindex < 20) && (Math.Round(MyDoubleParse(PowerPrice.Rows[i - 1][Dsindex].ToString().Trim()), 1) < required)) Dsindex = Dsindex + 2;
                                if (Dsindex < 20)
                                    arrPrice[i] = PowerPrice.Rows[i - 1][Dsindex + 1].ToString().Trim();
                                else arrPrice[i] = "0";


                                if (arrPrice[i].ToString() != "0")
                                    MaxBid = MyDoubleParse(arrPrice[i].ToString());
                            }
                            else arrPrice[i] = "0";

                        }


                        myConnection.Close();
                    }


                
               
        }


        //--------------------------------------FillMRUnitGrid()------------------------------------------
        //------------------------------------------------------
        private string DetectBlockM005()
        {
            string temp = unit.Trim();
            temp = temp.ToLower();
            if (package.Contains("Combined"))
            {
                int x;
                x = CCExcelCode();
                //string packagecode = GetPackageCode();
                //temp = x + "-" + "C" + packagecode;
                //ccunitbase///////////////////////////////////////////
                
                temp = temp.Replace("cc", "c");
                string[] sp = temp.Split('c');
                temp = sp[0].Trim() + sp[1].Trim();
                if (temp.Contains("gas"))
                {
                    temp = temp.Replace("gas", "G");

                }
                else
                {
                    temp = temp.Replace("steam", "S");

                }
                temp = x + "-" + temp;


            }
            else if (temp.Contains("gas"))
            {
                temp = temp.Replace("gas", "G");
                temp = PPId.Trim() + "-" + temp;
            }
            else
            {
                temp = temp.Replace("steam", "S");
                temp = PPId.Trim() + "-" + temp;
            }
            return temp;
        }
        //------------------------------------------------------
        private string DetectBlockM002()
        {
            string ptype = unit.Trim(); ;
            ptype = ptype.ToLower();
            if (package.Contains("Combined"))
            {
                int x;
                x = CCExcelCode();
                //string packagecode = GetPackageCode();
                //ptype = "C" + packagecode;

                //ccunitbase///////////////////////////////////////////
                //temp = x + "-" + "C" + packagecode;
                ptype = ptype.Replace("cc", "c");
                string[] sp = ptype.Split('c');
                ptype = sp[0].Trim() + sp[1].Trim();
                if (ptype.Contains("gas"))
                {
                    ptype = ptype.Replace("gas", "G");

                }
                else
                {
                    ptype = ptype.Replace("steam", "S");

                }
            

            }
            else if (ptype.Contains("gas"))
            {
                ptype = ptype.Replace("gas", "G");
            }
            else
            {
                ptype = ptype.Replace("steam", "S");
            }
            return ptype.Trim();
        }
        //--------------------------------------------------------
        private int CCExcelCode()
        {
            ArrayList PPIDArray = new ArrayList();
            ArrayList PPIDType = new ArrayList();

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            myConnection.Open();

            Myda.SelectCommand = new SqlCommand("SELECT PPID FROM PowerPlant", myConnection);
            Myda.Fill(MyDS, "ppid");
            foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
            {
                PPIDArray.Add(MyRow["PPID"].ToString().Trim());
                PPIDType.Add("real");
            }

            for (int i = 0; i < PPIDArray.Count; i++)
            {
                SqlCommand mycom = new SqlCommand();
                mycom.Connection = myConnection;
                mycom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id SELECT @result2 =count(PPID) FROM [PPUnit] WHERE PPID=@id AND PackageType LIKE 'Combined Cycle%'";
                mycom.Parameters.Add("@id", SqlDbType.NChar, 10);
                mycom.Parameters["@id"].Value = PPIDArray[i].ToString();
                mycom.Parameters.Add("@result1", SqlDbType.Int);
                mycom.Parameters["@result1"].Direction = ParameterDirection.Output;
                mycom.Parameters.Add("@result2", SqlDbType.Int);
                mycom.Parameters["@result2"].Direction = ParameterDirection.Output;
                mycom.ExecuteNonQuery();
                int result1 = (int)mycom.Parameters["@result1"].Value;
                int result2 = (int)mycom.Parameters["@result2"].Value;
                if ((result1 > 1) && (result2 > 0))
                {
                    PPIDArray.Add((int.Parse(PPIDArray[i].ToString())) + 1);
                    PPIDType.Add("virtual");
                }
            }
            myConnection.Close();

            int x;
            x = int.Parse(PPId);
            if (PPIDArray.Contains(x + 1))
                if (PPIDType[PPIDArray.IndexOf(x + 1)].ToString() == "virtual") x++;
            return x;

        }
        //--------------------------------------------------------
        private string GetPackageCode()
        {
            DataSet dataDS = new DataSet();
            SqlCommand MyCom = new SqlCommand();
            MyCom.CommandText = "select @pc=PackageCode from dbo.UnitsDataMain where PPID=" + PPId + " and UnitCode='" + unit + "' and UnitType='" + unitType + "'";
            MyCom.Parameters.Add("@pc", SqlDbType.Int);
            MyCom.Parameters["@pc"].Direction = ParameterDirection.Output;
            myConnection.Open();
            MyCom.Connection = myConnection;
            MyCom.ExecuteNonQuery();
            myConnection.Close();
            return MyCom.Parameters["@pc"].Value.ToString();
            


        }
        //--------------------------------------
        private void findMinMaxOfArray(string[] arr,ref double min,ref double max)
        {
            if (arr[1] != null)
            {
                min = MyDoubleParse(arr[1]);
                max = MyDoubleParse(arr[1]);
            }
            try
            {
                for (int i = 2; i < 25; i++)
                {
                    if (arr[i] != null)
                    {
                        if (MyDoubleParse(arr[i]) < min)
                            min = MyDoubleParse(arr[i]);
                        if (MyDoubleParse(arr[i]) > max)
                            max = MyDoubleParse(arr[i]);
                    }
                }


            }
            catch (Exception)
            {
                
                throw;
            }

            min = 0;
        }
        //--------------------------------------
       

        private double MyDoubleParse(string str)
        {
            if (str == "" || str == null)
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }

        public bool Findcconetype(string ppid)
        {
            int tr = 0;

           DataTable  oDataTable = Utilities.GetTable("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count == 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }

      

        
    }
}
