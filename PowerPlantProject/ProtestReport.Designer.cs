﻿namespace PowerPlantProject
{
    partial class ProtestReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProtestReport));
            this.label11 = new System.Windows.Forms.Label();
            this.CMBPLANT = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.faDatePicke2 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.faDatePicker1 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(75, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 67;
            this.label11.Text = "PPID :";
            // 
            // CMBPLANT
            // 
            this.CMBPLANT.FormattingEnabled = true;
            this.CMBPLANT.Location = new System.Drawing.Point(119, 29);
            this.CMBPLANT.Name = "CMBPLANT";
            this.CMBPLANT.Size = new System.Drawing.Size(121, 21);
            this.CMBPLANT.TabIndex = 66;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(175, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 65;
            this.label4.Text = "To";
            // 
            // faDatePicke2
            // 
            this.faDatePicke2.HasButtons = true;
            this.faDatePicke2.Location = new System.Drawing.Point(200, 77);
            this.faDatePicke2.Name = "faDatePicke2";
            this.faDatePicke2.Readonly = true;
            this.faDatePicke2.Size = new System.Drawing.Size(106, 20);
            this.faDatePicke2.TabIndex = 64;
            // 
            // faDatePicker1
            // 
            this.faDatePicker1.HasButtons = true;
            this.faDatePicker1.Location = new System.Drawing.Point(58, 77);
            this.faDatePicker1.Name = "faDatePicker1";
            this.faDatePicker1.Readonly = true;
            this.faDatePicker1.Size = new System.Drawing.Size(111, 20);
            this.faDatePicker1.TabIndex = 63;
            this.faDatePicker1.ValueChanged += new System.EventHandler(this.faDatePicker1_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(-80, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 62;
            this.label3.Text = "From:";
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(135, 126);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 68;
            this.button1.Text = "Export";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 69;
            this.label1.Text = "From:";
            // 
            // ProtestReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 186);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.CMBPLANT);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.faDatePicke2);
            this.Controls.Add(this.faDatePicker1);
            this.Controls.Add(this.label3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ProtestReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ProtestReport";
            this.Load += new System.EventHandler(this.ProtestReport_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox CMBPLANT;
        private System.Windows.Forms.Label label4;
        public FarsiLibrary.Win.Controls.FADatePicker faDatePicke2;
        public FarsiLibrary.Win.Controls.FADatePicker faDatePicker1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}