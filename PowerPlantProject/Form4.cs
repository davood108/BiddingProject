﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using NRI.DMS.Common;

namespace PowerPlantProject
{

    public partial class Form4 : Form
    {
        public string result;
        string ConStr;
        public Form4()
        {
            InitializeComponent();
            PackageTypeValid.Visible = false;
        }

        private void ODSaveBtn_Click(object sender, EventArgs e)
        {
            
            if ((unitNameTb.Text != "") && (PackageCodeTb.Text != ""))
            {
                if ((PackageType.SelectedItem == null) || (UnitType.SelectedItem == null))
                {
                    //PackageTypeValid.Visible = true;
                    MessageBox.Show("Please Fill the blank!");
                }
                else
                {
                    result = unitNameTb.Text + "," + UnitType.SelectedItem.ToString() + "," + PackageCodeTb.Text + "," + PackageType.SelectedItem.ToString();
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            else MessageBox.Show("Please Fill The Blank(s)!");
        }

        private void unitNameTb_Validated(object sender, EventArgs e)
        {
            if (unitNameTb.Text != "")
                errorProvider1.SetError(unitNameTb, "");
            else errorProvider1.SetError(unitNameTb, "Fill the blank");

        }

        private void PackageCodeTb_Validated(object sender, EventArgs e)
        {
            try
            {
                int x = int.Parse(PackageCodeTb.Text);
                errorProvider1.SetError(PackageCodeTb, "");
            }
            catch
            {
                errorProvider1.SetError(PackageCodeTb, "Fill the blank");
            }
        }
    }
}
