﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
//using Microsoft.Office.Core;

using NRI.SBS.Common;
using PowerPlantProject.New;

namespace PowerPlantProject
{
    partial class MainForm
    {
        private DataSet _powerDataSet;
        private string _unit;
        //----------------------------------BDCal_ValueChanged--------------------------------------------
        private void BDCal_ValueChanged(object sender, EventArgs e)
        {
            FillBDUnitGrid();

        }
        //-------------------------------FillBDUnitGrid-----------------------------------
        private void FillBDUnitGrid()
        {


            BDCurGrid.DataSource = null;
            if (BDCurGrid.Rows != null) BDCurGrid.Rows.Clear();

            if (!BDCal.IsNull)
            {
                _unit = BDUnitLb.Text.Trim();
                string temp = _unit;
                string package = BDPackLb.Text.Trim();
                //Get Index Of Selected Unit(Row) in GridView (In Generad data Tab)
                int index = 0;
                if (GDSteamGroup.Text.Contains(package))
                    for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                    {
                        if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(_unit))
                            index = i;
                    }
                else
                    for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                    {
                        if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(_unit))
                            index = i;
                    }

                //Build the Bolck for FRM002

                string ptypenum = "0";
                if (_unit.Contains("cc") || _unit.Contains("CC")) ptypenum = "1";
                //if (PPID == "232") ptypenum = "0";
                if (Findcconetype(PPID)) ptypenum = "0";

                temp = temp.ToLower();
                if (package.Contains("Combined"))
                {
                    string packagecode = "";
                    if (GDGasGroup.Text.Contains("Combined"))
                        packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                    else packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                    // temp = "C" + packagecode;

                    //ccunitbase///////////////////////////////////////////
                    //temp = x + "-" + "C" + packagecode;
                    temp = temp.Replace("cc", "c");
                    string[] sp = temp.Split('c');
                    temp = sp[0].Trim() + sp[1].Trim();
                    if (temp.Contains("gas"))
                    {
                        temp = temp.Replace("gas", "G");

                    }
                    else
                    {
                        temp = temp.Replace("steam", "S");

                    }

                }
                else
                {
                    if (temp.Contains("gas"))
                        temp = temp.Replace("gas", "G");
                    else if (temp.Contains("steam"))
                        temp = temp.Replace("steam", "S");
                }
                temp = temp.Trim();

                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                _powerDataSet = new DataSet();
                SqlDataAdapter Powerda = new SqlDataAdapter();
                if (RealBidCheck.Checked)
                {
                    Powerda.SelectCommand = new SqlCommand("SELECT Hour,Power1,Price1,Power2,Price2,Power3,Price3,Power4," +
                    "Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10,FilledBy,TimeFilled " +
                    "FROM [DetailFRM002] WHERE Estimated<>1 AND TargetMarketDate=@date AND Block=@temp and PPType='" + ptypenum + "' AND  PPID=" + PPID, myConnection);
                   
                }
                else
                {
                    Powerda.SelectCommand = new SqlCommand("SELECT Hour,Power1,Price1,Power2,Price2,Power3,Price3,Power4," +
                    "Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10,FilledBy,TimeFilled " +
                    "FROM [DetailFRM002] WHERE Estimated=1 AND TargetMarketDate=@date and PPType='" + ptypenum + "' AND Block=@temp AND PPID=" + PPID, myConnection);
                   
                }
                Powerda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Powerda.SelectCommand.Parameters["@date"].Value = BDCal.Text;
                Powerda.SelectCommand.Parameters.Add("@temp", SqlDbType.NChar, 20);
                Powerda.SelectCommand.Parameters["@temp"].Value = temp;
                Powerda.Fill(_powerDataSet);
                BDCurGrid.DataSource = _powerDataSet.Tables[0].DefaultView;

                _powerDataSet.Tables[0].TableName = "Bid";
                label113.Text = "";
                try
                {
                    if (_powerDataSet.Tables[0].Rows.Count != 0)
                    {
                        if (_powerDataSet.Tables[0].Rows[0]["FilledBy"].ToString() != "" && _powerDataSet.Tables[0].Rows[0]["TimeFilled"].ToString() != "")
                        {
                            string[] time = _powerDataSet.Tables[0].Rows[0]["TimeFilled"].ToString().Split('|');
                            label113.Text = "Filled By : " + _powerDataSet.Tables[0].Rows[0]["FilledBy"].ToString() + "   At Date : " + time[0] + "  Hour : " + time[1]; 
                        }
                       

                    }
                }
                catch
                {

                }
                //mortazavi
                if (_powerDataSet.Tables[0].Rows.Count == 0)
                {
                    BDPlotBtn.Enabled = false;
                    btnbidprint.Enabled = false;
                }
                else
                {
                    BDPlotBtn.Enabled = true;
                    btnbidprint.Enabled = true;
                }
                //mortazavi
                myConnection.Close();
            }
        }
//---------------------------FillBDUnit---------------------------------------
        private void FillBDUnit(string unit, string package, int index)
        {
            BDStateTb.Text = MRUnitStateTb.Text;
            BDPowerTb.Text = MRUnitPowerTb.Text;
            BDMaxBidTb.Text = MRUnitMaxBidTb.Text;
            label113.Text = "";
            if (!BDCal.IsNull) FillBDUnitGrid();
        }
//---------------------------RealBidCheck_CheckedChanged---------------------------------------
        private void RealBidCheck_CheckedChanged(object sender, EventArgs e)
        {            
            FillBDUnitGrid();
        }
//---------------------------EstimatedBidCheck_CheckedChanged---------------------------------------
        private void EstimatedBidCheck_CheckedChanged(object sender, EventArgs e)
        {           
            FillBDUnitGrid();
        }


        /// <summary>
        /// Export Bid Data To Excel. Abdollahzadeh 1397/03/22
        /// </summary>
        private void btnToExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (!BDCal.IsNull)
                {


                    SaveFileDialog saveFileDialog = new SaveFileDialog();
                    string date = BDCal.Text;
                    saveFileDialog.FileName = String.Format("BidData_{0}_{1}", _unit, date.Replace("/", ""));
                    saveFileDialog.Filter = "Excel files (*.xls)|";

                    if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        string fileName = saveFileDialog.FileName + ".xls";
                        string tempPath = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                                          "\\SBS\\BidData.xls";
                        ExcelUtility.CreateExcelFile(_powerDataSet, tempPath, fileName);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}