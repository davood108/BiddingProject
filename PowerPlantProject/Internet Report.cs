﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Auto_Update_Library;
using FarsiLibrary.Utils;
using System.Data.SqlClient;
using System.Threading;
using NRI.SBS.Common;
using System.Net;
using System.Xml.Serialization;
using System.IO;

namespace PowerPlantProject
{
    public partial class Internet_Report : Form
    {
        List<PersianDate> missingDates;
        string ppid1 = "";
        public Internet_Report()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;
                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////

                /////////////////////////////////////grid//////////////////////////////////////////


                dataGridView1.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                dataGridView1.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;              

                ///////////////////////////////////////////////////////////////////

            }
            catch
            {

            }
        }

        private void Internet_Report_Load(object sender, EventArgs e)
        {
            DataTable dddd = Utilities.GetTable("select * from powerplant");
            ppid1 = dddd.Rows[0][0].ToString().Trim();
            datePickerFrom.SelectedDateTime = DateTime.Now.AddDays(-1);
            datePickerTo.SelectedDateTime = DateTime.Now;

            fillproduce();
            
        }
        public void fillmanategh()
        {
            missingDates = GetDatesBetween();
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            int day = missingDates.Count;

            this.Cursor = Cursors.AppStarting;
            
            double[,] avgpeak = new double[16, 7];

            dataGridView1.RowCount = 112;
            dataGridView1.ColumnCount = 4;

            for (int r = 0; r < 16; r++)
            {
                string region = "R";
                if (r < 9) region += "0" + (r + 1);
                else region += (r + 1);            
               
               int index=1;
               double[] peak = new double[7];
               double[] minf=new double[7];
               double[] minvalue = new double[7];
               for (int i = 0; i < 7; i++)
                   minvalue[i] = 10000;


                foreach (PersianDate date in missingDates)
                {
                   
                   DataTable dt1 = Utilities.GetTable("select * from dbo.Manategh where date='" + date.ToString("d") + "' and Code='" + region + "' and Title= N'ارسالي'");
                   DataTable dt2 = Utilities.GetTable("select * from dbo.Manategh where date='" + date.ToString("d") + "' and Code='" + region + "' and Title= N'توليد بدون صنايع' ");
                   DataTable dt3 = Utilities.GetTable("select * from dbo.Manategh where date='" + date.ToString("d") + "' and Code='" + region + "' and Title= N'خاموشي'");
                   DataTable dt4 = Utilities.GetTable("select * from dbo.Manategh where date='" + date.ToString("d") + "' and Code='" + region + "' and Title= N'دريافتي'");
                   DataTable dt5 = Utilities.GetTable("select * from dbo.Manategh where date='" + date.ToString("d") + "' and Code='" + region + "' and Title= N'دريافتي صنايع'");
                   DataTable dt6 = Utilities.GetTable("select * from dbo.Manategh where date='" + date.ToString("d") + "' and Code='" + region + "' and Title= N'معادل افت فركانس'");
                   DataTable dt7 = Utilities.GetTable("select * from dbo.Manategh where date='" + date.ToString("d") + "' and Code='" + region + "' and Title= N'نياز مصرف'");

                   bool exist = checkentity_manategh("manategh", date.ToString("d"), region);
                    if(exist) 
                    {
                        ///////////////peak///////////////////////////////////////////////////

                        peak[0] += MyDoubleParse(dt1.Rows[0]["Peak"].ToString().Trim());
                        peak[1] += MyDoubleParse(dt2.Rows[0]["Peak"].ToString().Trim());
                        peak[2] += MyDoubleParse(dt3.Rows[0]["Peak"].ToString().Trim());
                        peak[3] += MyDoubleParse(dt4.Rows[0]["Peak"].ToString().Trim());
                        peak[4] += MyDoubleParse(dt5.Rows[0]["Peak"].ToString().Trim());
                        peak[5] += MyDoubleParse(dt6.Rows[0]["Peak"].ToString().Trim());
                        peak[6] += MyDoubleParse(dt7.Rows[0]["Peak"].ToString().Trim());

                        //////////////min////////////////////////////////////////////////////////
                        
                     
                        
                            minf[0] = minpeak(dt1);
                            if ((minf[0] < minvalue[0]) && (minf[0] > 0.4 * MyDoubleParse(dt1.Rows[0]["Peak"].ToString().Trim()))) minvalue[0] = minf[0];
                     
                            minf[1] = minpeak(dt2);
                            if ((minf[1] < minvalue[1]) && (minf[1] > 0.4 * MyDoubleParse(dt2.Rows[0]["Peak"].ToString().Trim()))) minvalue[1] = minf[1];
                       
                            minf[2] = minpeak(dt3);
                            if ((minf[2] < minvalue[2]) && (minf[2] > 0.4 * MyDoubleParse(dt3.Rows[0]["Peak"].ToString().Trim()))) minvalue[2] = minf[2];
                       
                            minf[3] = minpeak(dt4);
                            if ((minf[3] < minvalue[3]) && (minf[3] > 0.4 * MyDoubleParse(dt4.Rows[0]["Peak"].ToString().Trim()))) minvalue[3] = minf[3];
                      
                            minf[4] = minpeak(dt5);
                            if ((minf[4] < minvalue[4]) && (minf[4] > 0.4 * MyDoubleParse(dt5.Rows[0]["Peak"].ToString().Trim()))) minvalue[4] = minf[4];
                     
                            minf[5] = minpeak(dt6);
                            if ((minf[5] < minvalue[5]) && (minf[5] > 0.4 * MyDoubleParse(dt6.Rows[0]["Peak"].ToString().Trim()))) minvalue[5] = minf[5];
                    
                            minf[6] = minpeak(dt7);
                            if ((minf[6] < minvalue[6]) && (minf[6] > 0.4 * MyDoubleParse(dt7.Rows[0]["Peak"].ToString().Trim()))) minvalue[6] = minf[6];


                        //////////////////////////////////////////////////////////////////////////
                        index++;

                    }
                   

                }


                for (int c = 0; c < 7; c++)
                {
                    if (peak[c] != 0 )
                    {
                        avgpeak[r, c] = (peak[c] / (index - 1));
                       
                    }
                }

                //////////////////////////////////fill grid//////////////////////////////////////

                string [] field=new string[7];
                field[0] = "ارسالي";
                field[1] = "توليد بدون صنايع";
                field[2] = "خاموشي";
                field[3] = "دريافتي";
                field[4] = "دريافتي صنايع";
                field[5] = "معادل افت فركانس";
                field[6] = "نياز مصرف";


              

                dataGridView1.Columns[0].Name = "Region";
                dataGridView1.Columns[1].Name = "Title";
                dataGridView1.Columns[2].Name = "Peak Average (MW)";
                dataGridView1.Columns[3].Name = "Min peak (MW)";

                if (index != 1)
                {
                    for (int v = 7; v > 0; v--)
                    {
                        dataGridView1.Rows[((r + 1) * 7) - v].Cells[1].Value = field[(7 - v)];
                        dataGridView1.Rows[((r + 1) * 7) - v].Cells[2].Value = avgpeak[r, (7 - v)];


                       
                        if (minvalue[(7 - v)] != 10000) 
                            dataGridView1.Rows[((r + 1) * 7) - v].Cells[3].Value = minvalue[(7 - v)];
                                              

                        if (minvalue[(7 - v)] == 10000)
                            dataGridView1.Rows[((r + 1) * 7) - v].Cells[3].Value = 0;

                        if (minvalue[(7 - v)] > avgpeak[r, (7 - v)])
                            dataGridView1.Rows[((r + 1) * 7) - v].Cells[3].Value = 0;


                        if (v == 7)
                            dataGridView1.Rows[((r + 1) * 7) - v].Cells[0].Value = region;
                    }
                }



                /////////////////////////////////////////////////////////////////////////////////
            }

            this.Cursor = Cursors.Default;
        }

        public void fillproduce()
        {
            missingDates = GetDatesBetween();
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();

            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            int day = missingDates.Count;

            dataGridView1.RowCount = 1;
            dataGridView1.ColumnCount = 3;

            double value = 0;
            double avg = 0;

            int index = 1;

            foreach (PersianDate date in missingDates)
            {
                DataTable dt1 = Utilities.GetTable("select * from  dbo.ProducedEnergy where Date='" + date.ToString("d") + "' and PPCode='"+ppid1+"'and part='G'");

                if (dt1.Rows.Count == 1)
                {
                    double g = maxproduce(dt1);
                
                    value += (g) ;
                    index++; 
                }


            }

            if (value != 0 && index!=1)
            {
                avg = value / (index-1);
            }

            ////////////////////////fillgrid//////////////////////////////
            

          

            dataGridView1.Columns[0].Name = "Plant";
            dataGridView1.Columns[1].Name = "PlantId";
            dataGridView1.Columns[2].Name = "Produce Average (MW)";

            if (index != 1)
            {
                dataGridView1.Rows[0].Cells[0].Value = "CCOrumieh";
                dataGridView1.Rows[0].Cells[1].Value = ppid1;
                dataGridView1.Rows[0].Cells[2].Value = avg;
            }

        }

        public void fillavgprice()
        {

            missingDates = GetDatesBetween();
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();

            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            int day = missingDates.Count;
            dataGridView1.RowCount = 24;
            dataGridView1.ColumnCount = 2;

            double[] value = new double[24];
            double[] avg =new double[24];
            int index = 1;

            foreach (PersianDate date in missingDates)
            {
                bool exist=checkentity_avgprice(date.ToString("d"));

                if (exist)
                {
                    for (int h = 0; h < 24; h++)
                    {
                        DataTable dt = Utilities.GetTable("select AcceptedAverage from  dbo.AveragePrice where Date='" + date.ToString("d") + "' and  Hour='" + (h + 1) + "'");
                        
                            value[h] += MyDoubleParse(dt.Rows[0][0].ToString());                        
                       
                    }

                    index++;
                }
            }


            for (int i = 0; i < 24; i++)
            {
                if(value[i]!=0 && index!=1)
                avg[i] = value[i] / (index - 1);
            }

            //////////////////////////fillgrid///////////////////////////

          
            dataGridView1.Columns[0].Name = "Hour";
            dataGridView1.Columns[1].Name = "Average Price (Rial)";

            if (index != 1)
            {
                for (int h = 0; h < 24; h++)
                {
                    dataGridView1.Rows[h].Cells[1].Value = avg[h];
                    dataGridView1.Rows[h].Cells[0].Value = (h + 1);

                }
            }
           /////////////////////////////////////////////////////////

        }

        public void fillinterchange()
        {
            missingDates = GetDatesBetween();
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();

           dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            int day = missingDates.Count;
            string[] code = null;
            string[] codename = null;
            double[] maxinter = null;
            double[] avg = null;

            this.Cursor = Cursors.AppStarting;
          
            foreach (PersianDate date in missingDates)
            {
                bool exist = checkentity_interchange(date.ToString("d"));
                if (exist)
                {
                    DataTable dt = Utilities.GetTable("select * from dbo.InterchangedEnergy where Date='" + date.ToString("d") + "'");
                    code= new string[dt.Rows.Count];
                    codename = new string[dt.Rows.Count];
                    maxinter = new double[dt.Rows.Count];
                    avg = new double[dt.Rows.Count];
                    int c=0;
                    foreach (DataRow m in dt.Rows)
                    {
                        code[c] = m["code"].ToString().Trim();
                        codename[c] = m["Name"].ToString().Trim();
                        c++;
                    }
                    break;
                }
            }
           

            int num=0;
            if (code != null) 
            num= code.Length;

            if (num != 0)
            {
                dataGridView1.RowCount = num;
                dataGridView1.ColumnCount = 3;
            }

            for (int m = 0; m < num; m++)
            {
                int index = 1;
                foreach (PersianDate date in missingDates)
                {

                    bool exist = checkentity_interchangeline(date.ToString("d"), code[m]);
                    if (exist)
                    {
                        DataTable dt = Utilities.GetTable("select * from dbo.InterchangedEnergy where Date='" + date.ToString("d") + "'and code='"+code[m]+"'");
                        if (dt.Rows.Count > 0)
                        {
                            double[] value = new double[24];
                            string[] pn = new string[24];

                            for (int b = 0; b < 24; b++)
                            {
                                value[b] = MyDoubleParse(dt.Rows[0]["Hour" + (b + 1)].ToString());
                                if (value[b] < 0) pn[b] = "n";
                                else pn[b] = "p";

                            }

                           string[] s= maxinterchange(value, pn).ToString().Split(':');
                           maxinter[m] += MyDoubleParse(s[0].ToString().Trim()) * MyDoubleParse(s[1].ToString().Trim());
                            

                        }



                        index++;
                    }
                }


               
                /////////////////////avg////////////////////////////////

                    if (maxinter[m] != 0 && index != 1)
                        avg[m] = maxinter[m] / (index - 1);
                

                ////////////////////////fill grid/////////////////////////////
            
                
                    dataGridView1.Columns[1].Name = "Line Name";
                    dataGridView1.Columns[0].Name = "Line Code";
                    dataGridView1.Columns[2].Name = "Line Interchange Average (MW)";

                    if (index != 1)
                    {
                        dataGridView1.Rows[m].Cells[0].Value = code[m];
                        dataGridView1.Rows[m].Cells[1].Value = codename[m];
                        dataGridView1.Rows[m].Cells[2].Value = avg[m];
                    }


                
            }


            this.Cursor = Cursors.Default;


        }

        public string maxinterchange(double[] v, string[] n)
        {
           
            int c=1;
            double[] h = new double[24];
            double max = 0;
            string p = "";

            if (v!=null)
            {
                for (int g = 0; g < 24; g++)
                {
                    if (Math.Abs(v[g]) > max)
                    {
                        max = Math.Abs(v[g]);
                        p = n[g];
                    }
               

                }             

              
            }
            if (p == "n") c = -1;

            return  (max+":"+c) ;

        }
        public bool checkentity_interchange( string date)
        {


            DataTable dt = Utilities.GetTable("select * from dbo.InterchangedEnergy where Date='"+date+"'");
            if (dt.Rows.Count >0)
                return true;
            else return false;


        }
        public bool checkentity_interchangeline(string date,string code )
        {


            DataTable dt = Utilities.GetTable("select * from dbo.InterchangedEnergy where Date='" + date + "' and code='"+code+"'");
            if (dt.Rows.Count > 0)
                return true;
            else return false;


        }
        public bool checkentity_manategh(string item, string  date, string code)
        {


            DataTable dt = Utilities.GetTable("select Peak from dbo.Manategh where date='" + date + "' and Code='" + code + "'");
            if (dt.Rows.Count == 7)
                return true;
            else return false;


        }
        public bool checkentity_avgprice(string date)
        {


            DataTable dt = Utilities.GetTable("select AcceptedAverage from  dbo.AveragePrice where Date='" + date + "'");
            if (dt.Rows.Count == 24)
                return true;
            else return false;


        }
        public double minpeak(DataTable dt)
        {
            ///////////manategh/////////////////
            double result = 0;
            
            double[] h = new double[24];
            if (dt != null)
            {
                for (int g = 0; g < 24; g++)
                {
                    h[g] = MyDoubleParse(dt.Rows[0]["Hour" + (g + 1)].ToString());

                }
                Array.Sort(h);

                result = h[0];
            }
                

            return result;
        }
        public double maxproduce(DataTable dt)
        {
            double result = 0;

            double[] h = new double[24];
            if (dt != null)
            {
                for (int g = 0; g < 24; g++)
                {
                    h[g] = MyDoubleParse(dt.Rows[0]["Hour" + (g + 1)].ToString());

                }
                Array.Sort(h);

                result = h[23];
            }


            return result;

        }
        private List<PersianDate> GetDatesBetween()
        {

          
            if (datePickerFrom != null)
            {
                List<PersianDate> missingDates = new List<PersianDate>();

               
                DateTime dateFrom = datePickerFrom.SelectedDateTime.Date;
                DateTime dateTo = datePickerTo.SelectedDateTime.Date;
                missingDates.Add(dateFrom);

                TimeSpan span = dateTo - dateFrom;
                while (span.Days > 0)
                {
                    dateFrom = dateFrom.AddDays(1);
                    missingDates.Add(new PersianDate(dateFrom));
                    span = dateTo - dateFrom;
                }
                return missingDates;
            }
            else
                return null;
        }

       

        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }

        private void btnprint_Click(object sender, EventArgs e)
        {
            try
            {
                string name = "";
                if (rdmanategh.Checked)
                    name = "ميانگين بار مناطق و صنايع ";
                else if (rdaverageprice.Checked)
                    name = "ميانگين قيمت هر ساعت";
                else if (rdproduce.Checked)
                    name = "ميانگين توليد انرژي";
                else if (rdinterchane.Checked)
                    name = "ميانگين تبادل خطوط";  
                              

                //PrintDGV.info( name + "      From : " + datePickerFrom.Text + "   To : " + datePickerTo.Text);
                //PrintDGV.Print_DataGridView(dataGridView1);

                DataTable dt = new DataTable();
                DataColumn[] dsc = new DataColumn[] { };
                foreach (DataGridViewColumn c in dataGridView1.Columns)
                {
                    DataColumn dc = new DataColumn();
                    dc.ColumnName = c.Name.Trim();
                   // dc.DataType = c.ValueType;
                    dt.Columns.Add(dc);
                }
                foreach (DataGridViewRow r in dataGridView1.Rows)
                {
                    DataRow drow = dt.NewRow();
                    foreach (DataGridViewCell cell in r.Cells)
                    {
                        try
                        {
                            drow[cell.OwningColumn.Name] = cell.Value.ToString().Trim();
                        }
                        catch
                        {
                        }
                    }
                    dt.Rows.Add(drow);

                }




               internetprint m = new internetprint(dt, name, datePickerFrom.Text.Trim(), datePickerTo.Text.Trim());
                m.ShowDialog();

              
            }
            catch
            {

            }
        }

        private void rdmanategh_CheckedChanged(object sender, EventArgs e)
        {
            if (rdmanategh.Checked) fillmanategh();
        }

        private void rdproduce_CheckedChanged(object sender, EventArgs e)
        {
            if (rdproduce.Checked) fillproduce();
        }

        private void rdaverageprice_CheckedChanged(object sender, EventArgs e)
        {
            if (rdaverageprice.Checked) fillavgprice();
        }

        private void datePickerFrom_SelectedDateTimeChanged(object sender, EventArgs e)
        {
            datePickerTo.SelectedDateTime = DateTime.Now.AddDays(-1);
             
        }

        private void rdinterchane_CheckedChanged(object sender, EventArgs e)
        {
            if (rdinterchane.Checked) fillinterchange();
        }

        private void datePickerTo_SelectedDateTimeChanged(object sender, EventArgs e)
        {
            DateTime dateFrom = datePickerFrom.SelectedDateTime.Date;
            DateTime dateTo = datePickerTo.SelectedDateTime.Date;
            TimeSpan span = dateTo - dateFrom;
            if (span.Days < 0)
            {
                datePickerTo.SelectedDateTime = DateTime.Now.AddDays(-1);

            }
        }

       

    }
}
