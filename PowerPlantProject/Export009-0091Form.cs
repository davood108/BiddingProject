﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
//using Microsoft.Office.Core;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using System.Reflection;
using NRI.SBS.Common;


namespace PowerPlantProject
{
    public partial class Export009_0091Form : Form
    {
        public Export009_0091Form()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void Export009_0091Form_Load(object sender, EventArgs e)
        {
            btnexport.Enabled = false;
            DataTable PlantTable = Utilities.GetTable("select distinct PPName,PPID from dbo.PowerPlant order by PPID");
            for (int i = 0; i < PlantTable.Rows.Count; i++)
            {
                comboplant.Items.Add(PlantTable.Rows[i][0].ToString());
            }
              comboplant.Items.Add("All");
           
            comboplant.Text = PlantTable.Rows[0][0].ToString().Trim();
            exportdate.Text = new PersianDate(DateTime.Now).ToString("d");
     
            //////////////////////////////////////////////////////////////

        }


        private bool Export0091(string path, int plant, string date, string ppnamefarsi)
        {
            List<string> Packages = new List<string>();

            string cmd = "select distinct PackageType from dbo.UnitsDataMain where PPID='" +plant + "'" +
            " AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%') AND (UnitCode not like 'Gas cc%')))";
            DataTable oDataTable = Utilities.GetTable(cmd);
            foreach (DataRow row in oDataTable.Rows) 
            {
                Packages.Add(row["PackageType"].ToString().Trim());
            }

            string[] headers = { "Block/Unit No", "Hour", "Consumed" };


            string[] firstColValues = { "Form Name :", "Date Of Issue :", "Time Of Issue :", "Target Market Date :",
                                    "Power Plant Name :", "Code :", "Revision :", 
                                    "Filled By :", "Approved By :" };

            for (int packageIndex = 0; packageIndex < Packages.Count; packageIndex++)
            {
                int rowIndex = 0;
                int colIndex = 50;

                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                Excel.Application oExcel = new Excel.Application();

                oExcel.SheetsInNewWorkbook = 1;
                Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
                Excel.Worksheet sheet = null;
                Excel.Range range = null;

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers




                rowIndex = 0;
                colIndex = 0;
                int sheetIndex = 1;
                if (sheet != null)
                    sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
                else
                    sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

                sheet.Name = "Sheet1";

                int ppidToPrint = plant;
                string ppnameFarsiModified = ppnamefarsi;

                if (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count > 1)
                {
                    ppnameFarsiModified = "سيكل تركيبي" + " " + ppnameFarsiModified;
                    ppidToPrint++;
                }

                string[] secondColValues = { "M0091", 
                                          DateTime.Now.ToString("d"), DateTime.Now.ToString("T"),
                                          date,
                                          ppnameFarsiModified, ppidToPrint.ToString(), 
                                          "0", "", "" };


                for (int i = 0; i < firstColValues.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[i + 1, 1];
                    range.Value2 = firstColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    range = (Excel.Range)sheet.Cells[i + 1, 2];
                    range.Value2 = secondColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


                rowIndex = firstColValues.Length + 2;
                colIndex = 0;
                for (int i = 0; i < headers.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = headers[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
             

      
                string strCmd;
                if (Packages.Count > 1)
                {
                    if (Packages[packageIndex] != PackageTypePriority.CC.ToString())

                        strCmd = " select distinct Block  from DetailFRM009 where ppid='" + plant + "'and Block not like '%cc%' ORDER BY Block";

                    else
                        strCmd = " select distinct Block  from DetailFRM009 where ppid='" + plant + "'and Block  like '%cc%' ORDER BY Block";
                }
                else
                    strCmd = " select distinct Block  from DetailFRM009 where ppid='" + plant + "'ORDER BY Block";


                oDataTable = Utilities.GetTable(strCmd);

               

                string[] blockstable = new string[oDataTable.Rows.Count];
                string[] blocks = new string[oDataTable.Rows.Count];

                for (int i = 0; i < oDataTable.Rows.Count; i++)
                {
                    blockstable[i] = oDataTable.Rows[i][0].ToString().Trim();
                    blocks[i] = oDataTable.Rows[i][0].ToString().Trim();
                   
                }
                /////////////
                for (int i = 0; i < oDataTable.Rows.Count; i++)
                {
                    strCmd = "select * from DetailFRM009 where ppid='" + plant + "'" +
                          " AND Block='" + blocks[i] + "'" +
                          " AND TargetMarketDate='" + date + "'" +
                          " order by block, hour";
                 DataTable  checkoDataTable = Utilities.GetTable(strCmd);
                 if (checkoDataTable.Rows.Count == 0) return false;
                 if (checkoDataTable.Rows[i]["Consumed"].ToString() == "") return false;

                }

                rowIndex = firstColValues.Length + 3;



                foreach (string blockName in blocks)
                {



                    string blocknameexport = blockName;
                    int firstLine = rowIndex;
                    range = (Excel.Range)sheet.Cells[rowIndex, 1];

                    //if (blocknameexport.Contains("Gas"))
                    //{
                    //    blocknameexport = blocknameexport.Replace("Gas", "G");
                    //    if (blocknameexport.Contains("cc"))
                    //        blocknameexport = blocknameexport.Replace("cc", "");

                    //}
                    //else if (blocknameexport.Contains("Steam"))
                    //{
                    //    blocknameexport = blocknameexport.Replace("Steam", "S");
                    //    if (blocknameexport.Contains("cc"))
                    //        blocknameexport = blocknameexport.Replace("cc", "");

                    //}
                    ///////////////////////////////////////////////////////////////////
                    string blockshow = blockName;
                    if (blocknameexport.Contains("Gas"))
                    {
                        blockshow = blockshow.Replace("Gas", "G");
                        if (blocknameexport.Contains("cc"))
                            blockshow = blockshow.Replace("cc", "");

                    }
                    else if (blockshow.Contains("Steam"))
                    {
                        blockshow = blockshow.Replace("Steam", "S");
                        if (blockshow.Contains("cc"))
                            blockshow = blockshow.Replace("cc", "");

                    }
                    /////////////////////////////////////////////////////////////////////
                    
                    range.Value2 = blockshow;
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    strCmd = "select * from DetailFRM009 where ppid='" +plant+ "'" +
                         " AND Block='" + blockName + "'" +
                         " AND TargetMarketDate='" + date + "'" +

                         " order by block, hour";



                    oDataTable = Utilities.GetTable(strCmd);


                   

                    colIndex = 2;

                    double max = 0;
                    foreach (DataRow row in oDataTable.Rows)
                    {

                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = (int.Parse(row["Hour"].ToString().Trim()) + 1).ToString();
                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["Consumed"].ToString().Trim();
                       

                        rowIndex++;
                        colIndex = 2;
                    }

                
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                sheet = ((Excel.Worksheet)WB.Worksheets["Sheet1"]);

                range = (Excel.Range)sheet.Cells[1, 1];
                range = range.EntireColumn;
                range.AutoFit();

                range = (Excel.Range)sheet.Cells[1, 2];
                range = range.EntireColumn;
                range.AutoFit();

                sheet.Activate();
               

                string strDate = date ;
                strDate = strDate.Remove(7, 1);
                strDate = strDate.Remove(4, 1);
                string filePath = path + "\\" + "FRM0091_" + ppidToPrint.ToString() + "_" + strDate + ".xls";
            
                WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                WB.Close(Missing.Value, Missing.Value, Missing.Value);
                oExcel.Workbooks.Close();
                oExcel.Quit();

            }
            return true;

        }
        private bool Export009(string path, int plant, string date, string ppnamefarsi)
        {
            List<string> Packages = new List<string>();

            string cmd = "select distinct PackageType from dbo.UnitsDataMain where PPID='" + plant + "'" +
            " AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%') AND (UnitCode not like 'Gas cc%')))";
            DataTable oDataTable = Utilities.GetTable(cmd);
            foreach (DataRow row in oDataTable.Rows)
            {
                Packages.Add(row["PackageType"].ToString().Trim());
            }

            string[] headers = { "Block/Unit No", "Hour", "P", "QC", 
                                   "QL"};

            string[] firstColValues = { "Form Name :", "Date Of Issue :", "Time Of Issue :", "Target Market Date :",
                                    "Power Plant Name :", "Code :", "Revision :", 
                                    "Filled By :", "Approved By :" };

            for (int packageIndex = 0; packageIndex < Packages.Count; packageIndex++)
            {
                int rowIndex = 0;
                int colIndex = 50;

                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                Excel.Application oExcel = new Excel.Application();

                oExcel.SheetsInNewWorkbook = 1;
                Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
                Excel.Worksheet sheet = null;
                Excel.Range range = null;

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers




                rowIndex = 0;
                colIndex = 0;
                int sheetIndex = 1;
                if (sheet != null)
                    sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
                else
                    sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

                sheet.Name = "Sheet1";

                int ppidToPrint = plant;
                string ppnameFarsiModified = ppnamefarsi;

                if (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count > 1)
                {
                    ppnameFarsiModified = "سيكل تركيبي" + " " + ppnameFarsiModified;
                    ppidToPrint++;
                }

                string[] secondColValues = { "M009", 
                                          DateTime.Now.ToString("d"), DateTime.Now.ToString("T"),
                                          date,
                                          ppnameFarsiModified, ppidToPrint.ToString(), 
                                          "0", "", "" };


                for (int i = 0; i < firstColValues.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[i + 1, 1];
                    range.Value2 = firstColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    range = (Excel.Range)sheet.Cells[i + 1, 2];
                    range.Value2 = secondColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


                rowIndex = firstColValues.Length + 2;
                colIndex = 0;
                for (int i = 0; i < headers.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = headers[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
                
                string strCmd;
                if (Packages.Count > 1)
                {
                    if (Packages[packageIndex] != PackageTypePriority.CC.ToString())

                        strCmd = " select distinct Block  from DetailFRM009 where ppid='" + plant + "' and Block not like '%cc%'  ORDER BY Block";

                    else
                        strCmd = " select distinct Block  from DetailFRM009 where ppid='" + plant + "' and Block  like '%cc%'  ORDER BY Block";
                }
                else
                    strCmd = " select distinct Block  from DetailFRM009 where ppid='" + plant+"'ORDER BY Block";


                oDataTable = Utilities.GetTable(strCmd);
               

                string[] blockstable = new string[oDataTable.Rows.Count];
                string[] blocks = new string[oDataTable.Rows.Count];
                for (int i = 0; i < oDataTable.Rows.Count; i++)
                {
                    blockstable[i] = oDataTable.Rows[i][0].ToString().Trim();
                    blocks[i] = oDataTable.Rows[i][0].ToString().Trim();


                    ////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


                }

                for (int i = 0; i < oDataTable.Rows.Count; i++)
                {
                    strCmd = "select * from DetailFRM009 where ppid='" + plant + "'" +
                          " AND Block='" + blocks[i] + "'" +
                          " AND TargetMarketDate='" + date + "'" +
                          " order by block, hour";
                    DataTable checkoDataTable = Utilities.GetTable(strCmd);
                    if (checkoDataTable.Rows.Count == 0) return false;

                }


                /////////////
                rowIndex = firstColValues.Length + 3;



                foreach (string blockName in blocks)
                {

                    string blocknameexport = blockName;
                    int firstLine = rowIndex;
                    range = (Excel.Range)sheet.Cells[rowIndex, 1];



                    ///////////////////////////////////////////////////////////////////
                    string blockshow = blockName;
                    if (blocknameexport.Contains("Gas"))
                    {
                        blockshow = blockshow.Replace("Gas", "G");
                        if (blocknameexport.Contains("cc"))
                            blockshow = blockshow.Replace("cc", "");

                    }
                    else if (blockshow.Contains("Steam"))
                    {
                        blockshow = blockshow.Replace("Steam", "S");
                        if (blockshow.Contains("cc"))
                            blockshow = blockshow.Replace("cc", "");

                    }
                    /////////////////////////////////////////////////////////////////////


                    range.Value2 = blockshow;

                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    ////////!!!!!!! Peak!!!>???????????????




                    strCmd = "select * from DetailFRM009 where ppid='" + plant + "'" +
                        " AND Block='" + blockName + "'" +
                        " AND TargetMarketDate='" + date + "'" +
                        " order by block, hour";

                    oDataTable = Utilities.GetTable(strCmd);

                  

                    colIndex = 2;

                    double max = 0;
                    foreach (DataRow row in oDataTable.Rows)
                    {
                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = (int.Parse(row["Hour"].ToString().Trim()) + 1).ToString();
                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["P"].ToString().Trim();
                       

                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["QC"].ToString().Trim();


                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["QL"].ToString().Trim();




                        rowIndex++;
                        colIndex = 2;
                    }

                    //range = (Excel.Range)sheet.Cells[firstLine, 2];
                    //range.Value2 = max.ToString();
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                sheet = ((Excel.Worksheet)WB.Worksheets["Sheet1"]);

                range = (Excel.Range)sheet.Cells[1, 1];
                range = range.EntireColumn;
                range.AutoFit();

                range = (Excel.Range)sheet.Cells[1, 2];
                range = range.EntireColumn;
                range.AutoFit();

                sheet.Activate();
               

                string strDate = date;
                strDate = strDate.Remove(7, 1);
                strDate = strDate.Remove(4, 1);
                string filePath = path + "\\" + "FRM009_" + ppidToPrint.ToString() + "_" + strDate + ".xls";
               
                WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                WB.Close(Missing.Value, Missing.Value, Missing.Value);
                oExcel.Workbooks.Close();
                oExcel.Quit();

            }

            return true;
        }

        private void btnexport_Click(object sender, EventArgs e)
        {
            bool success009 = true;
            bool success0091 = true;
            ////............................for all................................/////////////////

            DataTable allpname = Utilities.GetTable("select distinct PPName,PNameFarsi,PPID from dbo.PowerPlant order by PPID");
            int [] allidarray=new int[allpname.Rows.Count];
            string[] allpnamefars = new string[allpname.Rows.Count];
            string[] allplantname = new string[allpname.Rows.Count];

            if (comboplant.Text.Trim() == "All")

            {
                int i=0;
                foreach(DataRow myrow in allpname.Rows)
                {

                 DataTable allppid = Utilities.GetTable("select PPID,PNameFarsi,PPName from dbo.PowerPlant where PPName='" + myrow[0].ToString().Trim() + "'");
                 allidarray[i] =int.Parse(allppid.Rows[0][0].ToString().Trim());
                 allpnamefars[i] = allppid.Rows[0][1].ToString().Trim();
                 allplantname[i] = allppid.Rows[0][2].ToString().Trim();

                 i++;

                }
            }

            /////............................end for all................/////////////////////

            folderBrowserDialog1.ShowNewFolderButton = true;
            DialogResult answer = folderBrowserDialog1.ShowDialog();

            if (answer == DialogResult.OK)
            {
                try
                {
                    string path = folderBrowserDialog1.SelectedPath;

                    /////////////////////one plant///////////////////////////////////

                    if (comboplant.Text.Trim()!= "All")
                    {
                        DataTable ppid = Utilities.GetTable("select PPID from dbo.PowerPlant where PPName='" + comboplant.Text.Trim()+ "'");
                        DataTable odata = Utilities.GetTable(" select PNameFarsi FROM PowerPlant WHERE ppid='" + ppid.Rows[0][0].ToString() + "'");


                        DataTable checkhour = Utilities.GetTable("select Hour from dbo.DetailFRM009 where PPID='" + int.Parse(ppid.Rows[0][0].ToString()) + "' and TargetMarketDate='" + exportdate.Text + "' order by TargetMarketDate desc ");
                        if (checkhour.Rows[0][0].ToString() == "0")
                        {
                            success0091 = Export0091(path, int.Parse(ppid.Rows[0][0].ToString()), exportdate.Text, odata.Rows[0][0].ToString().Trim());
                            success009 = Export009(path, int.Parse(ppid.Rows[0][0].ToString()), exportdate.Text, odata.Rows[0][0].ToString().Trim());

                            
                                MessageBox.Show("Export of M009 file(s) to " + path +" completed"+(success009? " successfully" :" Usuccessfully!")+".");
                            
                           
                                MessageBox.Show("Export of M0091 file(s) to " + path + " completed" + (success0091 ? " successfully" : " Usuccessfully!") + ".");

                           
                            
                        }
                        else
                        {
                            MessageBox.Show("!Export Not Possible , Hours number are not complete.!\r\n Please Complete ls2 data in date:" + exportdate.Text + "..");
                        }
                    }

                     /////////////////////////////////end one plant/////////////////////////////


                     /////////////////////////////for all////////////////////////////////////

                    else
                    {
                        for (int i = 0; i < allidarray.Length; i++)
                        {
                            DataTable checkhour = Utilities.GetTable("select Hour from dbo.DetailFRM009 where PPID='" + allidarray[i] + "' and TargetMarketDate='" + exportdate.Text + "' order by TargetMarketDate desc ");
                            if (checkhour.Rows[0][0].ToString() == "0")
                            {
                                success0091 = Export0091(path, allidarray[i], exportdate.Text, allpnamefars[i]);
                                success009 = Export009(path, allidarray[i], exportdate.Text, allpnamefars[i]);
                               
                                    MessageBox.Show("Export of M009 file(s) to " + path + " for << "+ allplantname[i]+ " >> completed" + (success009 ? " successfully" : "  Usuccessfully!") + ".");
                               
                                    MessageBox.Show("Export of M0091 file(s) to " + path + " for << " + allplantname[i] + " >> completed" + (success0091 ? " successfully" : "  Usuccessfully!") + ".");


                            }
                            else
                            {
                                MessageBox.Show("!Export Not Possible , Hours number are not complete.!\r\n Please Complete ls2 data in date:" + exportdate.Text + "..");
                            }

                        }
                        ///////////////////////////////////end all////////////////////////////////////////////

                    }
                }
                catch (Exception exp1)
                {
                    string str = exp1.Message;
                    MessageBox.Show("Unable To Export");
                }

            }

        
        }
        
       

        private void comboplant_SelectedIndexChanged(object sender, EventArgs e)
        {

            DataTable tableppid = null;
            DataTable table9available = null;
            DataTable pname = null;
            string primarymessage = "there is no data for plant/s :";
            string message = "";


            if (comboplant.Text.Trim() != "All")
            {
                tableppid = Utilities.GetTable("select PPID from dbo.PowerPlant where PPName='" + comboplant.Text.Trim() + "'");
                table9available = Utilities.GetTable("select * from dbo.DetailFRM009 where PPID='" + tableppid.Rows[0][0].ToString().Trim() + "'and TargetMarketDate='" + exportdate.Text.Trim() + "'");
                if (table9available.Rows.Count > 0) btnexport.Enabled = true;
                else btnexport.Enabled = false;

            }
            else
            {
                tableppid = Utilities.GetTable("select PPID from dbo.PowerPlant");
                foreach (DataRow myrow in tableppid.Rows)
                {
                  
                    table9available = Utilities.GetTable("select * from dbo.DetailFRM009 where PPID='" +myrow[0].ToString().Trim() + "'and TargetMarketDate='" + exportdate.Text.Trim() + "'");
                    if (table9available.Rows.Count == 0)
                    {
                        pname = Utilities.GetTable("select PPName from dbo.PowerPlant where PPID='"+ myrow[0].ToString().Trim()+"'");
                        message += "\r\n\r\n" +pname.Rows[0][0].ToString();
                       
                    }
                }
                if(message!="")
                MessageBox.Show(primarymessage+message);
            }


       

        }

        private void exportdate_ValueChanged(object sender, EventArgs e)
        {
            DataTable tableppid=null;
            DataTable table9available = null;

            if (comboplant.Text.Trim() != "All")
            {
                tableppid = Utilities.GetTable("select PPID from dbo.PowerPlant where PPName='" + comboplant.Text.Trim() + "'");
                table9available = Utilities.GetTable("select * from dbo.DetailFRM009 where PPID='" + tableppid.Rows[0][0].ToString().Trim() + "'and TargetMarketDate='" + exportdate.Text.Trim() + "'");
                if (table9available.Rows.Count > 0) btnexport.Enabled = true;
                else btnexport.Enabled = false;
            }
            else

                btnexport.Enabled = true;

           
        }
        
    }
}
