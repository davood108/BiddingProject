﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NRI.SBS.Common;
using System.Data.SqlClient;
using FarsiLibrary.Utils;

namespace PowerPlantProject
{
    public partial class MarketInformation : Form
    {
        public MarketInformation()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;
                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dataGridView3.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                dataGridView2.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////

                /////////////////////////////////////grid//////////////////////////////////////////

                 
                 dataGridView1.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                 dataGridView1.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor; 
                
                 dataGridView2.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                 dataGridView2.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;
               
                ///////////////////////////////////////////////////////////////////
                
            }
            catch
            {

            }
        }

        private void datePickerweekend_ValueChanged(object sender, EventArgs e)
        {
            fillgrid(datePickerweekend.Text);
        }
        private void fillgrid(string date)
        {
            dataGridView1.DataSource = null;
            dataGridView2.DataSource = null;
            dataGridView3.DataSource = null;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            DataTable dd1 = null;
            DataTable dd2 = null;
            DataTable dd3 = null;
            if (rdchart.Checked)
            {
                dd1 = Utilities.GetTable("select * from dbo.AveragePrice where Date='"+date+"'");
            }
            else if (rdload.Checked)
            {
                dd1 = Utilities.GetTable("select SUBSTRING(Date,6,5)as Date,Peak,Hour1 as '1',Hour2 as '2',Hour3 as '3',Hour4 as '4',Hour5 as '5',Hour6 as '6',Hour7 as '7',Hour8 as '8',Hour9 as '9',Hour10 as '10'" +
               ",Hour11 as '11',Hour12 as '12',Hour13 as '13',Hour14 as '14',Hour15 as '15',Hour16 as '16',Hour17 as '17',Hour18 as '18',Hour19 as '19',Hour20 as '20',Hour21 as '21',Hour22 as '22',Hour23 as '23',Hour24 as '24'" +
               "from dbo.LoadForecasting where DateEstimate='" + date + "'order by Date");
            }
            if (rdinter.Checked)
            {
                dd2 = Utilities.GetTable("select Code,Name,Hour1 as '1',Hour2 as '2',Hour3 as '3',Hour4 as '4',Hour5 as '5',Hour6 as '6',Hour7 as '7',Hour8 as '8',Hour9 as '9',Hour10 as '10'" +
               ",Hour11 as '11',Hour12 as '12',Hour13 as '13',Hour14 as '14',Hour15 as '15',Hour16 as '16',Hour17 as '17',Hour18 as '18',Hour19 as '19',Hour20 as '20',Hour21 as '21',Hour22 as '22',Hour23 as '23',Hour24 as '24'" +
               "from InterchangedEnergy where Date='" + date + "'");                
            }
            if (rdpro.Checked)
            {
                dd2 = Utilities.GetTable("select PPCode as 'Code',Part,Hour1 as '1',Hour2 as '2',Hour3 as '3',Hour4 as '4',Hour5 as '5',Hour6 as '6',Hour7 as '7',Hour8 as '8',Hour9 as '9',Hour10 as '10'" +
                ",Hour11 as '11',Hour12 as '12',Hour13 as '13',Hour14 as '14',Hour15 as '15',Hour16 as '16',Hour17 as '17',Hour18 as '18',Hour19 as '19',Hour20 as '20',Hour21 as '21',Hour22 as '22',Hour23 as '23',Hour24 as '24'"+
                "from dbo.ProducedEnergy where Date='"+date+"'");
            }
            ////////
            if (rdpact.Checked)
            {
                dd3 = Utilities.GetTable("select ppid,unit,H1,H2,H3,H4,H5,H6,H7,H8,H9,H10,H11,H12,H13,H14,H15,H16,H17,H18,H19,H20,H21,H22,H23,H24 from pactual where date='"+date+"'order by ppid ,unit");

            }
            if (rdplimm.Checked)
            {
                dd3 = Utilities.GetTable("select ppid,unit,Hour,S,DO,D1,D2 from powerlimmited where date='" + date + "'order by ppid ,unit,hour");

            }
            if (rdtrans.Checked)
            {
                dd3 = Utilities.GetTable("select * from yeartrans where year='" + date.Substring(0, 4).Trim() + "'");
            }
            if (rdhub.Checked)
            {
                dd3 = Utilities.GetTable("select * from yearhub where year='" + date.Substring(0, 4).Trim() + "'");
            }


            ///////////
            if(dd1.Rows.Count>0)
            dataGridView1.DataSource = dd1;
            if (dd2.Rows.Count > 0)
            dataGridView2.DataSource = dd2;
            if (dd3.Rows.Count > 0)
                dataGridView3.DataSource = dd3;

        }

        private void MarketInformation_Load(object sender, EventArgs e)
        {
            datePickerweekend.SelectedDateTime = DateTime.Now;
            datePickerweekend.Text = new PersianDate(System.DateTime.Now).ToString("d");
        }

        private void rdload_CheckedChanged(object sender, EventArgs e)
        {
            fillgrid(datePickerweekend.Text);
        }

        private void rdinter_CheckedChanged(object sender, EventArgs e)
        {
            fillgrid(datePickerweekend.Text);
        }

        private void btnprint_Click(object sender, EventArgs e)
        {

            try
            {
                string name = "";
                if (rdchart.Checked)
                {
                    name = "قيمت روز بازار";
                }
                else if (rdload.Checked)
                {

                    name = "پيش بيني بار روزانه";
                }

                DataTable dt = new DataTable();
                DataColumn[] dsc = new DataColumn[] { };
                foreach (DataGridViewColumn c in dataGridView1.Columns)
                {
                    DataColumn dc = new DataColumn();
                    dc.ColumnName = c.Name.Trim();
                    dc.DataType = c.ValueType;
                    dt.Columns.Add(dc);
                }
                foreach (DataGridViewRow r in dataGridView1.Rows)
                {
                    DataRow drow = dt.NewRow();
                    foreach (DataGridViewCell cell in r.Cells)
                    {
                        try
                        {
                            drow[cell.OwningColumn.Name] = cell.Value.ToString().Trim();
                        }
                        catch
                        {
                        }
                    }
                    dt.Rows.Add(drow);

                }


                //////////////////////////////////////////////               
                this.Hide();
                /////////////////////////////////////////////

                MarketInformationPrint m = new MarketInformationPrint(dt, name, datePickerweekend.Text.Trim());
                m.ShowDialog();

                //////////////////////////////////////////////               
                this.Show();
                /////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void btnprint2_Click(object sender, EventArgs e)
        {
            
            try
            {
                string name = "";
                if (rdinter.Checked)
                {
                    name = "گزارش روزانه انرژي - تبادل";
                }
                else if (rdpro.Checked)
                {

                    name = "گزارش روزانه انرژي - توليد ";
                }

                DataTable dt = new DataTable();
                DataColumn[] dsc = new DataColumn[] { };
                foreach (DataGridViewColumn c in dataGridView2.Columns)
                {
                    DataColumn dc = new DataColumn();
                    if (rdinter.Checked)
                    {
                        if (c.Name.Trim() != "Code")
                        {
                            dc.ColumnName = c.Name.Trim();
                            dc.DataType = c.ValueType;
                            dt.Columns.Add(dc);
                        }
                    }
                    else if (rdpro.Checked)
                    {

                        dc.ColumnName = c.Name.Trim();
                        dc.DataType = c.ValueType;
                        dt.Columns.Add(dc);

                    }

                }
                foreach (DataGridViewRow r in dataGridView2.Rows)
                {
                    DataRow drow = dt.NewRow();
                    foreach (DataGridViewCell cell in r.Cells)
                    {
                        try
                        {
                            drow[cell.OwningColumn.Name] = cell.Value.ToString().Trim();
                        }
                        catch
                        {
                        }
                    }
                    dt.Rows.Add(drow);

                }



                //////////////////////////////////////////////               
                this.Hide();
                /////////////////////////////////////////////

                MarketInformationPrint m = new MarketInformationPrint(dt, name, datePickerweekend.Text.Trim());
                m.ShowDialog();

                //////////////////////////////////////////////               
                this.Show();
                /////////////////////////////////////////////

            }
            catch
            {

            }
        }

        private void rdhub_CheckedChanged(object sender, EventArgs e)
        {
            fillgrid(datePickerweekend.Text);
        }

        private void rdtrans_CheckedChanged(object sender, EventArgs e)
        {
            fillgrid(datePickerweekend.Text);
        }

        private void rdpact_CheckedChanged(object sender, EventArgs e)
        {
            fillgrid(datePickerweekend.Text);
        }

        private void rdplimm_CheckedChanged(object sender, EventArgs e)
        {
            fillgrid(datePickerweekend.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string name = "";
                if (rdhub.Checked)
                {
                    name = "گزارش هزینه دسترسی به هاب برای تزریق انرژی نیروگا‌ه";
                }
                else if (rdtrans.Checked)
                {

                    name = "گزارش تزریق انرژی در پستهای شبکه ";
                }
                else if (rdpact.Checked)
                {

                    name = "گزارش توان اصلاح شده قابل توليد ";
                }
                else if (rdplimm.Checked)
                {

                    name = "گزارش محدوديت توليد ";
                }

                DataTable dt = new DataTable();
                DataColumn[] dsc = new DataColumn[] { };
                foreach (DataGridViewColumn c in dataGridView3.Columns)
                {
                    DataColumn dc = new DataColumn();
                   

                        dc.ColumnName = c.Name.Trim();
                        dc.DataType = c.ValueType;
                        dt.Columns.Add(dc);

                  

                }
                foreach (DataGridViewRow r in dataGridView3.Rows)
                {
                    DataRow drow = dt.NewRow();
                    foreach (DataGridViewCell cell in r.Cells)
                    {
                        try
                        {
                            drow[cell.OwningColumn.Name] = cell.Value.ToString().Trim();
                        }
                        catch
                        {
                        }
                    }
                    dt.Rows.Add(drow);

                }



                //////////////////////////////////////////////               
                this.Hide();
                /////////////////////////////////////////////

                MarketInformationPrint m = new MarketInformationPrint(dt, name, datePickerweekend.Text.Trim());
                m.ShowDialog();

                //////////////////////////////////////////////               
                this.Show();
                /////////////////////////////////////////////

            }
            catch
            {

            }
        }
    }
}
