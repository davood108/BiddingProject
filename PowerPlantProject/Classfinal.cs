﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Common;
using ILOG.Concert;
using ILOG.CPLEX;
using PowerPlantProject;
using FarsiLibrary.Utils;
using NRI.SBS.Common;
using System.Collections;
namespace PowerPlantProject
{

    public class Classfinal
    {
        string[, ,] Unit_Dec;
        string gencocode = "";
        int BiddingStrategySettingId;
        string ConStr = NRI.SBS.Common.ConnectionManager.ConnectionString;
        string biddingDate;
        string currentDate;
        string ppName;
        private SqlConnection oSqlConnection = null;
        private SqlCommand oSqlCommand = null;
        private SqlDataReader oSqlDataReader = null;
        private DataTable oDataTable = null;
        private string[] plant;
        int Plants_Num;
        int Package_Num;
        int[] Plant_Packages_Num;
        int Units_Num = 0;
        int[] Plant_units_Num;
        bool Ispre = false;
        bool IsMcp = false;
        string message = "";
        DateTime predate005 = DateTime.Now;
        DateTime predate002 = DateTime.Now;
        string smaxdate = "";
        bool[,] setparam;
        string regioncode = "";
        string staterun = "";
        string Marketrule = "";
        string tname005 = "";
        public Classfinal(string PPName, string CurrentDate, string BiddingDate, bool PreSolve, bool mcp)
        {
            GC.Collect();
            GC.SuppressFinalize(this);
            GC.WaitForPendingFinalizers();

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();
            biddingDate = BiddingDate;
            currentDate = CurrentDate;
            Ispre = PreSolve;
            IsMcp = mcp;
            ppName = PPName.Trim();

            DataTable dtsmaxdatea = Utilities.GetTable("select * from dbo.BaseData where Date<='" + BiddingDate + "'order by BaseID desc");
            
            if (dtsmaxdatea.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdatea.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdatea.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdatea = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdatea.Rows[0]["Date"].ToString();
            }
            DataTable basetabledata = Utilities.GetTable("select ProposalHour from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetabledata.Rows.Count > 0)
            {
                staterun = basetabledata.Rows[0][0].ToString();
            }



            if (ppName.ToLower().Trim() == "all")
            {
                oDataTable = Utilities.GetTable("select distinct PPID from UnitsDataMain ");
                Plants_Num = oDataTable.Rows.Count;

                plant = new string[Plants_Num];
                for (int il = 0; il < Plants_Num; il++)
                {
                    plant[il] = oDataTable.Rows[il][0].ToString().Trim();

                }
            }
            else
            {
                SqlCommand MyCom1 = new SqlCommand();
                MyCom1.Connection = myConnection;
                MyCom1.CommandText = "SELECT @num=PPID FROM PowerPlant WHERE PPName=@name";
                MyCom1.Parameters.Add("@num", SqlDbType.NChar, 10);
                MyCom1.Parameters.Add("@name", SqlDbType.NChar, 20);
                MyCom1.Parameters["@num"].Direction = ParameterDirection.Output;
                MyCom1.Parameters["@name"].Value = ppName.Trim();
                MyCom1.ExecuteNonQuery();

                Plants_Num = 1;
                plant = new string[1];
                plant[0] = MyCom1.Parameters["@num"].Value.ToString().Trim();


            }
            /////////

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            MyCom.CommandText = "select @id=max(id) from dbo.BiddingStrategySetting " +
                " where plant=@ppname AND  CurrentDate=@curDate AND BiddingDate=@bidDate";
            MyCom.Parameters.Add("@id", SqlDbType.Int);
            MyCom.Parameters.Add("@ppname", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@curDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@bidDate", SqlDbType.Char, 10);
            MyCom.Parameters["@id"].Direction = ParameterDirection.Output;
            MyCom.Parameters["@ppname"].Value = ppName.Trim();
            MyCom.Parameters["@curDate"].Value = CurrentDate;
            MyCom.Parameters["@bidDate"].Value = BiddingDate;
            MyCom.ExecuteNonQuery();
            myConnection.Close();
            BiddingStrategySettingId = int.Parse(MyCom.Parameters["@id"].Value.ToString().Trim());


            /////////////////////////region////////////////////////////

            DataTable dt = Utilities.GetTable("select * from BaseGencoInfo");
            if (dt.Rows.Count > 0)
            {
                gencocode = dt.Rows[0]["GencoCode"].ToString().Trim();

            }
            ///////////////////////////////////////////////////////////////
        }


        private string buildmaxdate(string[] arrmax)
        {
            string maxdate = null;
            for (int fd = 0; fd < arrmax.Length; fd++)
            {
                if (maxdate == null || string.Compare(maxdate, arrmax[fd]) < 0)
                    maxdate = arrmax[fd];
            }
            return maxdate;

        }
        public bool value()
        {

            for (int i = 0; i < plant.Length; i++)
            {
                if (Findexist002and005(plant[i], currentDate) == false)
                {
                    MessageBox.Show("M002 && M005 Files Not Exist For Plant:  " + plant[i] + "  In Date :  " + currentDate);

                }

            }

            DateTime dateFrom = PersianDateConverter.ToGregorianDateTime(biddingDate).Date;
            DateTime dateTo = DateTime.Now.Date;
            TimeSpan span = dateFrom - dateTo;
            int difday = 0;
            if (span.Days > 0)
            {
                difday = span.Days;
            }

            DataTable der = Utilities.GetTable("select distinct GencoCode from dbo.UnitsDataMain where GencoCode!='NULL'");
            //if (der.Rows[0][0].ToString().Trim() == "101") regioncode = "R01";
            //else if (der.Rows[0][0].ToString().Trim() == "200") regioncode = "R03";
            //else if (der.Rows[0][0].ToString().Trim() == "501") regioncode = "R06";
            //else regioncode = "";

            regioncode = gencocode;



            DataTable dts1 = null;
            setparam = new bool[Plants_Num, 8];
            if (ppName != "All")
            {
                dts1 = Utilities.GetTable("select * from BidRunSetting where Plant='" + ppName + "'");
                if (dts1.Rows.Count > 0)
                {
                    setparam[0, 0] = MyboolParse(dts1.Rows[0]["FlagOpf"].ToString());
                    setparam[0, 1] = MyboolParse(dts1.Rows[0]["FlagSensetivity"].ToString());
                    setparam[0, 2] = MyboolParse(dts1.Rows[0]["FlagMarketPower"].ToString());
                    setparam[0, 3] = MyboolParse(dts1.Rows[0]["FlagLossPayment"].ToString());//RISK
                    setparam[0, 4] = MyboolParse(dts1.Rows[0]["FlagPresolve"].ToString());
                    setparam[0, 5] = MyboolParse(dts1.Rows[0]["FlagCostFunction"].ToString());//PMIN-UL
                    setparam[0, 6] = MyboolParse(dts1.Rows[0]["FairPlay"].ToString());
                    setparam[0, 7] = MyboolParse(dts1.Rows[0]["LMP"].ToString());

                }
            }
            else
            {

                DataTable dd = Utilities.GetTable("select PPName from dbo.PowerPlant");
                for (int i = 0; i < Plants_Num; i++)
                {
                    dts1 = Utilities.GetTable("select * from BidRunSetting where Plant='" + dd.Rows[i][0].ToString().Trim() + "'");
                    if (dts1.Rows.Count > 0)
                    {
                        setparam[i, 0] = MyboolParse(dts1.Rows[0]["FlagOpf"].ToString());
                        setparam[i, 1] = MyboolParse(dts1.Rows[0]["FlagSensetivity"].ToString());
                        setparam[i, 2] = MyboolParse(dts1.Rows[0]["FlagMarketPower"].ToString());
                        setparam[i, 3] = MyboolParse(dts1.Rows[0]["FlagLossPayment"].ToString());
                        setparam[i, 4] = MyboolParse(dts1.Rows[0]["FlagPresolve"].ToString());
                        setparam[i, 5] = MyboolParse(dts1.Rows[0]["FlagCostFunction"].ToString());
                        setparam[i, 6] = MyboolParse(dts1.Rows[0]["FairPlay"].ToString());
                        setparam[i, 7] = MyboolParse(dts1.Rows[0]["LMP"].ToString());

                    }
                }
            }

            if (staterun == "Economic")
            {
                for (int i = 0; i < Plants_Num; i++)
                {
                    setparam[i, 6] = true;
                }
            }
            //opf result//------------------------------------------------------------------
         
          
            //----------------------------------yesterday is weekend-----------------------------//

            bool IsWeekEnd = false;
            bool yeterdayweekend = false;
            bool two_yeterdayweekend = false;

            DateTime yestdate = PersianDateConverter.ToGregorianDateTime(biddingDate).AddDays(-1);
            DateTime twoyesterday = PersianDateConverter.ToGregorianDateTime(biddingDate).AddDays(-2);
            string peryesterday = new PersianDate(yestdate).ToString("d");
            string pretwoyesterday = new PersianDate(twoyesterday).ToString("d");


            DataTable dtweekend = Utilities.GetTable("select distinct Date from dbo.WeekEnd");

            foreach (DataRow nrow in dtweekend.Rows)
            {

                if (nrow[0].ToString().Trim() == biddingDate)
                {
                    IsWeekEnd = true;
                    break;
                }
            }

            //----------------------------------yesterday is weekend-----------------------------//

            foreach (DataRow nrow in dtweekend.Rows)
            {
                if (nrow[0].ToString().Trim() == peryesterday.Trim())
                {
                    yeterdayweekend = true;
                    break;
                }
                if (nrow[0].ToString().Trim() == pretwoyesterday.Trim())
                {
                    two_yeterdayweekend = true;
                    break;
                }
            }
            //-----------------------------------------------------------------------------------------      

            string strCmd = "select Max (PackageCode) from UnitsDataMain ";
            if (Plants_Num == 1)
                strCmd += " where ppid=" + plant[0];
            oDataTable = Utilities.GetTable(strCmd);
            Package_Num = (int)oDataTable.Rows[0][0];

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //Unit Number

            Plant_units_Num = new int[Plants_Num];
            for (int re = 0; re < Plants_Num; re++)
            {
                oDataTable = Utilities.GetTable("select UnitCode from UnitsDataMain where PPID='" + plant[re] + "'");

                Plant_units_Num[re] = oDataTable.Rows.Count;
                if (Units_Num <= oDataTable.Rows.Count)
                {
                    Units_Num = oDataTable.Rows.Count;
                }

            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //Fixed Set :
            int CplexPrice = 0;
            int CplexPower = 0;
            int Hour = 24;
            int Dec_Num = 4;
            int Data_Num = 26;
            int Start_Num = 48;
            int Mmax = 2;
            int Step_Num = 10;
            int Xtrain = 201;
            int Power_Num = 4;
            int oldday = 3;
            int Const_selectdays = 5;
            int GastostartSteam = 1;

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Unit_Dec = new string[Plants_Num, Units_Num, Dec_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Dec_Num; k++)
                    {
                        switch (k)
                        {
                            case 0:
                                oDataTable = Utilities.GetTable("select UnitType  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString().Trim();
                                break;
                            case 1:
                                oDataTable = Utilities.GetTable("select PackageType  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString().Trim();
                                break;
                            case 2:
                                Unit_Dec[j, i, k] = "Gas";
                                break;
                            case 3:
                                oDataTable = Utilities.GetTable("select  SecondFuel  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype ASC");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString().Trim();
                                break;
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int b = 0;

            // detect max date in database...................................  
            oDataTable = Utilities.GetTable("select Date from BaseData");
            int ndate = oDataTable.Rows.Count;
            string[] arrmaxdate1 = new string[ndate];
            for (b = 0; b < ndate; b++)
            {
                arrmaxdate1[b] = oDataTable.Rows[b][0].ToString().Trim();
            }

            //string smaxdate = buildmaxdate(arrmaxdate1);

            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + biddingDate + "'order by BaseID desc");
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }

          
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int bb = 0;
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            oDataTable = Utilities.GetTable("select MarketPriceMax from BaseData where Date = '" + smaxdate + "'order by BaseID desc");
            double[] CapPrice = new double[2];

            if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][0].ToString() != "")
            {
                CapPrice[0] = MyDoubleParse(oDataTable.Rows[0][0].ToString());
                CapPrice[0] = Math.Round(CapPrice[0]/1000)*1000;
                CapPrice[1] = 1000;
            }
            else
            {
                CapPrice[0] = 416000;

                CapPrice[1] = 1000;
            }

            //Error bid for distance is accept.
            double[] ErrorBid = new double[1];
            ErrorBid[0] = CapPrice[1] * 2;
            string[] Run = new string[2];
            Run[0] = "Automatic";
            Run[1] = "Customize";


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            Plant_Packages_Num = new int[Plants_Num];

            List<string>[] PackageTypes = new List<string>[Plants_Num];

            for (int re = 0; re < Plants_Num; re++)
            {
                oDataTable = Utilities.GetTable("select Max(PackageCode) from UnitsDataMain where  PPID='" + plant[re] + "'");
                Plant_Packages_Num[re] = (int)oDataTable.Rows[0][0];

                oDataTable = Utilities.GetTable("select distinct PackageType from UnitsDataMain where  PPID='" + plant[re] + "'");
                //Plant_Packages_Num[re] = (int)oDataTable.Rows.count;

                PackageTypes[re] = new List<string>();

                foreach (string typeName in Enum.GetNames(typeof(PackageTypePriority)))
                {
                    //////////// have to make sure later whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                    bool found = false;
                    foreach (DataRow row in oDataTable.Rows)
                        if (row["PackageType"].ToString().Trim().Contains(typeName))
                            found = true;

                    if (found)
                        PackageTypes[re].Add(typeName);
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,] Package = new string[Plants_Num, Package_Num];

            for (int j = 0; j < Plants_Num; j++)
            {
                string cmd = "select PackageType from dbo.UnitsDataMain where PPID=" + plant[j] +
                             " AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%')AND(UnitCode not like 'Gas cc%')))";
                oDataTable = Utilities.GetTable(cmd);

                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    Package[j, k] = oDataTable.Rows[k][0].ToString().Trim();
                }

            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,] Unit = new string[Plants_Num, Units_Num];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    oDataTable = Utilities.GetTable("select UnitCode from UnitsDataMain where PPID='" + plant[j] + "'ORDER BY packagecode,unittype");
                    Unit[j, i] = oDataTable.Rows[i][0].ToString().Trim();
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Toreranceup = new double[Plants_Num, Units_Num, Hour];
            double[, ,] ToreranceDown = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Limit_variance_Down = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Limit_variance = new double[Plants_Num, Units_Num, Hour];

            double Factor_Price = 0.995;
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            double[, ,] Unit_Data = new double[Plants_Num, Units_Num, Data_Num];
            // 0 : Package
            // 1 : RampUp
            // 2 : RampDown
            // 3 : Pmin
            // 4 : pmax
            // 5 : RampStart&shut
            // 6 : MinOn
            // 7 : MinOff
            // 8 :  Am
            // 9 :  Bm
            // 10 : Cm
            // 11 : Bmain
            // 12 : Cmain
            // 13 : Dmain
            // 14 : Variable cost
            // 15 : Fixed Cost
            // 16 : Tcold
            // 17 : Thot 
            // 18 : Cold start cost
            // 19 : Hot start cost===]]]]]]]]]]]]]]]]]]]]]]]]]]]]

            for (int j = 0; j < Plants_Num; j++)
            {
                oDataTable = Utilities.GetTable("select *  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype");
                if (oDataTable.Rows.Count > 0)
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int k = 0; k < Data_Num; k++)
                        {
                            switch (k)
                            {
                                case 0:

                                    Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PackageCode"].ToString());
                                    break;
                                case 1:
                                    if (oDataTable.Rows[i]["RampUpRate"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["RampUpRate"].ToString());
                                        Unit_Data[j, i, k] = 30 * Unit_Data[j, i, k];
                                    }
                                    break;
                                case 2:
                                    Unit_Data[j, i, k] = Unit_Data[j, i, 1];
                                    break;

                                case 3:

                                    Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PMin"].ToString());
                                    break;

                                case 4:

                                    Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PMax"].ToString());
                                    break;
                                case 5:
                                    double pow = Unit_Data[j, i, 1];
                                    if (Unit_Data[j, i, 3] > pow)
                                    {
                                        Unit_Data[j, i, k] = Unit_Data[j, i, 3];
                                    }
                                    else
                                    {
                                        Unit_Data[j, i, k] = pow;
                                    }
                                    break;
                                case 6:
                                    if (oDataTable.Rows[i]["TUp"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TUp"].ToString());
                                    }
                                    else Unit_Data[j, i, k] = 3;

                                    break;
                                case 7:
                                    if (oDataTable.Rows[i]["TDown"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TDown"].ToString());

                                    }
                                    else Unit_Data[j, i, k] = 10;

                                    break;

                                case 8:
                                    if (oDataTable.Rows[i]["PrimaryFuelAmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PrimaryFuelAmargin"].ToString());
                                    }
                                    break;
                                case 9:
                                    if (oDataTable.Rows[i]["PrimaryFuelBmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PrimaryFuelBmargin"].ToString());
                                    }
                                    break;
                                case 10:
                                    if (oDataTable.Rows[i]["PrimaryFuelCmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PrimaryFuelCmargin"].ToString());
                                    }
                                    break;
                                case 11:
                                    if (oDataTable.Rows[i]["BMaintenance"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["BMaintenance"].ToString());
                                    }
                                    break;

                                case 12:
                                    if (oDataTable.Rows[i]["CMaintenance"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["CMaintenance"].ToString());
                                    }
                                    break;
                                case 13:
                                    Unit_Data[j, i, k] = Unit_Data[j, i, 12];

                                    break;
                                case 14:

                                    if (oDataTable.Rows[i]["VariableCost"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["VariableCost"].ToString());
                                    }
                                    break;
                                case 15:
                                    if (oDataTable.Rows[i]["FixedCost"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["FixedCost"].ToString());
                                    }
                                    break;

                                case 16:
                                    if (oDataTable.Rows[i]["TStartCold"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TStartCold"].ToString());
                                    }
                                    break;
                                case 17:

                                    if (oDataTable.Rows[i]["TStartHot"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TStartHot"].ToString());
                                    }

                                    break;
                                case 18:
                                    if (oDataTable.Rows[i]["CostStartHot"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["CostStartHot"].ToString());
                                    }


                                    break;

                                case 19:
                                    if (oDataTable.Rows[i]["CostStartCold"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["CostStartCold"].ToString());
                                    }

                                    break;

                                case 20:
                                    if (oDataTable.Rows[i]["SecondFuelAmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["SecondFuelAmargin"].ToString());
                                    }

                                    break;
                                case 21:
                                    if (oDataTable.Rows[i]["SecondFuelBmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["SecondFuelBmargin"].ToString());
                                    }

                                    break;
                                case 22:
                                    if (oDataTable.Rows[i]["SecondFuelCmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["SecondFuelCmargin"].ToString());
                                    }
                                    break;
                                case 23:
                                    if (oDataTable.Rows[i]["HeatValuePrimaryFuel"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["HeatValuePrimaryFuel"].ToString());
                                    }
                                    break;
                                case 24:
                                    if (oDataTable.Rows[i]["HeatValueSecondaryFuel"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["HeatValueSecondaryFuel"].ToString());
                                    }
                                    break;
                                case 25:
                                    if (oDataTable.Rows[i]["CapitalCost"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["CapitalCost"].ToString());
                                    }
                                    break;
                            }
                        }
                    }
            }
            //Rampe rate is correct :
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Data_Num; k++)
                    {
                        if (Unit_Data[j, i, 1] > (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]))
                        {
                            Unit_Data[j, i, 1] = (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]);
                            Unit_Data[j, i, 2] = (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]);
                        }
                    }
                }
            }
            //Rampe rate Startup is correct :
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Data_Num; k++)
                    {
                        if (Unit_Data[j, i, 5] > (Unit_Data[j, i, 4]))
                        {
                            Unit_Data[j, i, 5] = (Unit_Data[j, i, 4]);
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            // package yeki kam she :
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Unit_Data[j, i, 0] = Unit_Data[j, i, 0] - 1;
                }
            }

            ////////////////////////////khorsand////////////////////////////////
            checkdispatch();

            ///////////////////////////////////plant and hour price///////////////////////////////////////////////

            double[,] defaultprce = new double[Plants_Num, 24];
            for (int y = 0; y < Plants_Num; y++)
            {
                for (int h = 0; h < 24; h++)
                {
                    defaultprce[y, h] = defaultpicefind(plant[y], h);
                }
            }

            ////////////////////////////////////////mustrun-hourly////////////////////////////////////////////////////////
            double[, ,] valmust = new double[Plants_Num, Units_Num, 24];
            double[,] XBourse = new double[Plants_Num,24];
            string [, ,] statusmust = new string [Plants_Num, Units_Num, 24];

            for (int j = 0; j < Plants_Num; j++)
            {               
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        double mustval = MustHourly(plant[j],Unit[j,i],(h+1));
                        double Xchange = powerHourly(plant[j], (h + 1), "Exchange");
                        string status = MustHourlystatus(plant[j], Unit[j, i], (h + 1));
                        ///////////////////////////////////////////////////////////////////
                        valmust[j, i, h] = mustval;
                        statusmust[j, i, h] = status;
                        
                        XBourse[j, h] = Xchange;

                    }
                }
            }
            //////////////////////////////////avc////////////////////////////////////////////////

            double[,] AvcValue = new double[Plants_Num, Units_Num];

            for (int j = 0; j < Plants_Num; j++)
            {
                DataTable davc = Utilities.GetTable("select Avc  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype");
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {

                    AvcValue[j, i] = MyDoubleParse(davc.Rows[i][0].ToString());
                }
            }


            //***********************************************************************************
            MaxPrice(DateTime.Now);

            //***********************************************************************************
            string[] ptimmForPowerLimitedUnit = new string[Plants_Num];

            if (Plants_Num == 1)
            {
                strCmd = " select distinct PPID from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate='" + biddingDate +
                    "' and isEmpty=0 AND PPID=" + plant[0];

                oDataTable = Utilities.GetTable(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ptimmForPowerLimitedUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }
            }
            else
            {
                strCmd = " select distinct PPID from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate='" + biddingDate + "'and isEmpty=0";

                oDataTable = Utilities.GetTable(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ptimmForPowerLimitedUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < plann; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ptimmForPowerLimitedUnit[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ptimmForPowerLimitedUnit[i] = temp[i];
            }
            /////////////////////////////////////////////////
            int[] unitname = new int[Plants_Num];

            strCmd = " select UnitCode from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate='" + biddingDate;


            for (b = 0; b < Plants_Num; b++)
            {
                string strCmd2 = strCmd +
                    "'and PPID='" + ptimmForPowerLimitedUnit[b] + "'and isEmpty=0";
                oDataTable = Utilities.GetTable(strCmd2);
                unitname[b] = oDataTable.Rows.Count;
            }
            //***********************************************************************************
            double[, ,] Limit_Power = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Limit_Dispatch = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Limit_Dispatch_notzero = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Dispatch_Complete = new double[Plants_Num, Units_Num, Hour];
            string[,] DispatchBlock = Getblock002();


            bool[,] DispatchFlag = new bool[Plants_Num, Units_Num];
            bool[,] DispatchFlagweb = new bool[Plants_Num, Units_Num];

            int[, ,] Outservice_Plant_unit = new int[Plants_Num, Units_Num, 24];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Dispatch_Complete[j, i, h] = 0;
                    }
                }
            }
            ///////////////////////////////////////////

            for (int j = 0; j < Plants_Num; j++)
            {
                DataTable checkdate = null;
                DataTable CHECKALL = Utilities.GetTable("SELECT distinct StartDate FROM dbo.PowerLimitedUnit WHERE PPID='" + ptimmForPowerLimitedUnit[j] + "'and StartDate='" + biddingDate + "'and isEmpty=0 order by StartDate desc");
                if (CHECKALL.Rows.Count > 0)
                {
                    checkdate = Utilities.GetTable("SELECT count(*) FROM dbo.PowerLimitedUnit WHERE PPID='" + ptimmForPowerLimitedUnit[j] + "'and StartDate='" + CHECKALL.Rows[0][0].ToString().Trim() + "'and isEmpty=0");

                    if (checkdate.Rows.Count > 0)
                    {
                        if (int.Parse(checkdate.Rows[0][0].ToString()) == Plant_units_Num[j])
                        {
                            ////////////////////////////////////////////////
                            //for (int j = 0; j < Plants_Num; j++)
                            //{

                            for (int i = 0; i < Plant_units_Num[j]; i++)
                            {

                                strCmd = "select Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24" +
                                       " from dbo.PowerLimitedUnit where  PPID='" + ptimmForPowerLimitedUnit[j] + "'and UnitCode='" + Unit[j, i] + "' and StartDate='" + biddingDate + "'and isEmpty=0 order by StartDate desc";

                                oDataTable = Utilities.GetTable(strCmd);


                                for (int h = 0; h < Hour; h++)
                                {

                                    if (oDataTable.Rows.Count > 0)
                                    {
                                        DispatchFlagweb[j, i] = true;
                                        DataRow res = oDataTable.Rows[0];
                                        Limit_Power[j, i, h] = MyDoubleParse(res[h].ToString());
                                        Dispatch_Complete[j, i, h] = 1;
                                        if (res[h].ToString() == "")
                                        {
                                            Dispatch_Complete[j, i, h] = 0;
                                        }
                                    }
                                    else
                                    {
                                        Dispatch_Complete[j, i, h] = 0;
                                        DispatchFlagweb[j, i] = false;
                                    }
                                }
                            }
                        }

                        else
                        {

                            for (int k = 0; k < Plant_units_Num[j]; k++)
                            {
                                DataTable drr = Utilities.GetTable("select PackageType  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype ASC");
                                string pack = drr.Rows[k][0].ToString().Trim();

                                string ptype = "0";
                                if (Unit_Dec[j, k, 1].Contains("cc") || Unit_Dec[j, k, 1].Contains("CC")) ptype = "1";
                              //bbb  if (plant[j] == "149" || plant[j] == "232" || plant[j] == "543") ptype = "0";
                                if (Findcconetype(plant[j].ToString())) ptype = "0";
                                DataTable UseDispatch = Utilities.GetTable("select Hour,DispachableCapacity from dbo.Dispathable where PPID='" + plant[j] + "' and Block='" + DispatchBlock[j, k] + "'and PackageType='" + ptype + "' and StartDate='" + biddingDate + "'order by StartDate desc");

                                if (UseDispatch.Rows.Count > 0)
                                {
                                    for (int h = 0; h < 24; h++)
                                    {
                                        Limit_Dispatch[j, k, h] = MyDoubleParse(UseDispatch.Rows[h][1].ToString());
                                    }
                                    DispatchFlag[j, k] = true;

                                }
                            }
                        }
                    }
                }

                else
                {


                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        DataTable drr = Utilities.GetTable("select PackageType  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype ASC");
                        string pack = drr.Rows[k][0].ToString().Trim();

                        string ptype = "0";
                        if (Unit_Dec[j, k, 1].Contains("cc") || Unit_Dec[j, k, 1].Contains("CC")) ptype = "1";
                        //bbb if (plant[j] == "701" || plant[j] == "980" || plant[j] == "543") ptype = "0";
                        if (Findcconetype(plant[j].ToString())) ptype = "0";
                        DataTable UseDispatch = Utilities.GetTable("select Hour,DispachableCapacity from dbo.Dispathable where PPID='" + plant[j] + "' and Block='" + DispatchBlock[j, k] + "'and PackageType='" + ptype + "' and StartDate='" + biddingDate + "'order by StartDate desc");

                        if (UseDispatch.Rows.Count > 0)
                        {
                            for (int h = 0; h < 24; h++)
                            {
                                Limit_Dispatch[j, k, h] = MyDoubleParse(UseDispatch.Rows[h][1].ToString());
                            }
                            DispatchFlag[j, k] = true;
                        }
                    }
                }
            }




            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~limitdispatch not zero~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {

                    DataTable drr = Utilities.GetTable("select PackageType  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype ASC");
                    string pack = drr.Rows[k][0].ToString().Trim();

                    string ptype = "0";
                    if (Unit_Dec[j, k, 1].Contains("cc") || Unit_Dec[j, k, 1].Contains("CC")) ptype = "1";
                   //bbb  if (plant[j] == "149" || plant[j] == "232" || plant[j] == "543") ptype = "0";
                    if (Findcconetype(plant[j].ToString())) ptype = "0";
                    DataTable UseDispatch = Utilities.GetTable("select max(StartDate) from dbo.Dispathable where PPID='" + plant[j] + "' and Block='" + DispatchBlock[j, k] + "'and PackageType='" + ptype + "' and StartDate<='" + biddingDate + "'group by StartDate having count(*) = '24'order by StartDate desc");

                    if (UseDispatch.Rows.Count > 0)
                    {
                        DataTable dd85 = Utilities.GetTable("select Hour,DispachableCapacity from dbo.Dispathable where PPID='" + plant[j] + "' and Block='" + DispatchBlock[j, k] + "'and PackageType='" + ptype + "' and StartDate='" + UseDispatch.Rows[0][0].ToString().Trim() + "'order by StartDate desc");
                        if (dd85.Rows.Count > 0)
                        {
                            for (int hl = 0; hl < 24; hl++)
                            {
                                Limit_Dispatch_notzero[j, k, hl] = MyDoubleParse(dd85.Rows[hl][1].ToString());
                                if (Limit_Dispatch_notzero[j, k, hl] == 0) Limit_Dispatch_notzero[j, k, hl] = Unit_Data[j, k, 4];
                            }

                        }
                    }
                    else
                    {
                        for (int hl = 0; hl < 24; hl++)
                        {
                            Limit_Dispatch_notzero[j, k, hl] = Unit_Data[j, k, 4];
                        }
                    }

                }
            }
            //---------------------------------------------------------------------------------------                   
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[] ppkForConditionUnit = new string[Plants_Num];

            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID,Date from dbo.ConditionUnit where( ConditionUnit.OutServiceStartDate<='" + biddingDate +
                    "'AND ConditionUnit.OutServiceEndDate>='" + biddingDate +
                    "')or( dbo.ConditionUnit.MaintenanceStartDate<='" + biddingDate +
                    "'and dbo.ConditionUnit.MaintenanceEndDate>='" + biddingDate + "')" +
                    " order by Date Desc";
                oDataTable = Utilities.GetTable(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ppkForConditionUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < plann; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppkForConditionUnit[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }
                for (int i = 0; i < Plants_Num; i++)
                    ppkForConditionUnit[i] = temp[i];
            }
            else
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where ((ConditionUnit.OutServiceStartDate<='" + biddingDate +
                    "'AND ConditionUnit.OutServiceEndDate>='" + biddingDate +
                    "')or (dbo.ConditionUnit.MaintenanceStartDate<='" + biddingDate +
                    "'and dbo.ConditionUnit.MaintenanceEndDate>='" + biddingDate + "'" +
                    " ))AND PPID=" + plant[0];
                //" order by Date Desc";

                oDataTable = Utilities.GetTable(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ppkForConditionUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }
            }

            ///////////////////////
            int[] uukForConditionUnit = new int[Plants_Num];
            strCmd = " select UnitCode from dbo.ConditionUnit where(( ConditionUnit.OutServiceStartDate<='" + biddingDate +
                       "'AND ConditionUnit.OutServiceEndDate>='" + biddingDate +
                       "')or( dbo.ConditionUnit.MaintenanceStartDate<='" + biddingDate +
                       "'and dbo.ConditionUnit.MaintenanceEndDate>='" + biddingDate;


            for (b = 0; b < Plants_Num; b++)
            {
                string strCmd2 = strCmd +
                    "'))and PPID='" + ppkForConditionUnit[b] + "'" + " order by Date Desc"; ;
                oDataTable = Utilities.GetTable(strCmd2);
                ///////j//////////
                if (oDataTable.Rows.Count != 0)
                {
                    uukForConditionUnit[b] = oDataTable.Rows.Count;
                }
            }
            //--------------------------------------------------------------
            bool om = false;
            bool op = false;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < 24; h++)
                    {

                        ////////////////////////////maintenance///////////////////////
                        DataTable secondcondition = Utilities.GetTable("SELECT  Maintenance,MaintenanceStartDate,MaintenanceEndDate," +
                           " MaintenanceStartHour,MaintenanceEndHour FROM [ConditionUnit] where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "'and Date<=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "')order by Date desc");

                        if (secondcondition.Rows.Count > 0)
                        {
                            DataRow MyRow = secondcondition.Rows[0];

                            if (bool.Parse(MyRow[0].ToString().Trim()))
                            {

                                int myhour = h;
                                string startDate = MyRow[1].ToString().Trim();
                                string endDate = MyRow[2].ToString().Trim();
                                int startHour = (int.Parse(MyRow[3].ToString().Trim()) - 1);
                                int endHour = (int.Parse(MyRow[4].ToString().Trim()) - 1);


                                if (startDate == endDate && biddingDate == endDate)
                                {
                                    if (myhour > endHour) om = false;

                                    else om = true;

                                    if (myhour < startHour) om = false;

                                }

                                else if (biddingDate == startDate)
                                    if (myhour < startHour)
                                        om = false;
                                    else om = true;
                                else if (biddingDate == endDate)
                                    if (myhour > endHour)
                                        om = false;
                                    else om = true;
                                else if (CheckDateSF(biddingDate, startDate, endDate))
                                    om = true;
                                else om = false;
                            }

                            else om = false;
                        }
                        else om = false;
                        ///////////////////outservice///////////////////
                        DataTable outcondition = Utilities.GetTable("SELECT  OutService,OutServiceStartDate,OutServiceEndDate," +
                          " OutServiceStartHour,OutServiceEndHour FROM [ConditionUnit] where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "'and Date<=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "')order by Date desc");

                        if (outcondition.Rows.Count > 0)
                        {
                            DataRow MyRow = outcondition.Rows[0];

                            if (bool.Parse(MyRow[0].ToString().Trim()))
                            {

                                int myhour = h;
                                string startDate = MyRow[1].ToString().Trim();
                                string endDate = MyRow[2].ToString().Trim();
                                int startHour = (int.Parse(MyRow[3].ToString().Trim()) - 1);
                                int endHour = (int.Parse(MyRow[4].ToString().Trim()) - 1);

                                if (startDate == endDate && biddingDate == endDate)
                                {
                                    if (myhour > endHour) op = false;
                                    else op = true;

                                    if (myhour < startHour) op = false;

                                }
                                else if (biddingDate == startDate)
                                    if (myhour < startHour)
                                        op = false;
                                    else op = true;
                                else if (biddingDate == endDate)
                                    if (myhour > endHour)
                                        op = false;
                                    else op = true;

                                else if (CheckDateSF(biddingDate, startDate, endDate))
                                    op = true;
                                else op = false;
                            }
                            else op = false;
                        }
                        else op = false;

                        //////////////////////////////////////////////////////////////////////

                        if (om == true || op == true)
                        {
                            Outservice_Plant_unit[j, i, h] = 1;
                        }

                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Pmax_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Pmax_Plant_unit_fix = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Dispatch_Complete_Pack = new double[Plants_Num, Units_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Dispatch_Complete_Pack[j, i, h] =  Dispatch_Complete[j, i, h];

                        Pmax_Plant_unit[j, i, h] = Unit_Data[j, i, 4];
                        Pmax_Plant_unit_fix[j, i, h] = Unit_Data[j, i, 4];
                        if (Dispatch_Complete[j, i, h] == 1)
                        {
                            Pmax_Plant_unit[j, i, h] = Limit_Power[j, i, h];
                        }
                    }
                }
            }

        
            ///////////////////////////////////////////////////////////////////////////////////////

            string[,] nb = Getblock002();

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Pmax_Plant_unit[j, i, h] == 0) && (Dispatch_Complete[j, i, h] == 1))
                        {
                            Outservice_Plant_unit[j, i, h] = 1;
                        }
                        if (Pmax_Plant_unit[j, i, h] == 0)
                        {
                            Pmax_Plant_unit[j, i, h] = Unit_Data[j, i, 4];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if ((DispatchFlag[j, i] == true))
                        {
                            Pmax_Plant_unit[j, i, h] = Limit_Dispatch[j, i, h];
                            if (Pmax_Plant_unit[j, i, h] > 0)
                            {
                                Outservice_Plant_unit[j, i, h] = 0;
                            }
                            else
                            {
                                Outservice_Plant_unit[j, i, h] = 1;
                            }
                        }
                        else if (DispatchFlagweb[j, i] == false)
                        {
                            Pmax_Plant_unit[j, i, h] = Limit_Dispatch_notzero[j, i, h];
                            if (Pmax_Plant_unit[j, i, h] > 0)
                            {
                                Outservice_Plant_unit[j, i, h] = 0;
                            }
                            else
                            {
                                Outservice_Plant_unit[j, i, h] = 1;
                            }
                        }

                        if (Pmax_Plant_unit[j, i, h] == 0)
                        {
                            Pmax_Plant_unit[j, i, h] = Unit_Data[j, i, 4];
                        }
                    }
                }
            }




            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Pmax_Plant_Pack = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Pmax_Plant_Pack_fix = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Pmin_Plant_Pack_fix = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        Pmax_Plant_Pack[j, i, h] = Pmax_Plant_unit[j, i, h];
                        Pmax_Plant_Pack_fix[j, i, h] = Pmax_Plant_unit_fix[j, i, h];
                        Pmin_Plant_Pack_fix[j, i, h] = Unit_Data[j, i, 3];
                    }

                }
            }
            


            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------
                                                              //        Price        //
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------

            string[,] PackageTypesList = new string[Plants_Num, Enum.GetNames(typeof(PackageTypePriority)).Length];

            for (int j = 0; j < Plants_Num; j++)
            {

                string cmd = "select Distinct PackageType from dbo.UnitsDataMain where PPID=" + plant[j];
                //" AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%')AND(UnitCode not like 'Gas cc%')))";
                oDataTable = Utilities.GetTable(cmd);
                for (int k = 0; k < oDataTable.Rows.Count; k++)
                    PackageTypesList[j, k] = oDataTable.Rows[k][0].ToString().Trim();

            }

            double[, ,] PriceForecastings = new double[Plants_Num, Hour, Enum.GetNames(typeof(PackageTypePriority)).Length];

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[, ,] UL_State = new string[Plants_Num, Units_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        UL_State[j, k, h] = "N";
                    }
                }
            }

           

            //*************************************************************************
            double[, ,] Dispatch_Proposal = new double[Plants_Num, Units_Num, Hour];
            //*************************************************************************
            double[, ,] Price_UL = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Price_optimal = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Price_Margin = new double[Plants_Num, Units_Num, Hour];
            //*************************************************************************
            double[, , ,] Price = new double[Plants_Num, Units_Num, Hour, Step_Num];
            double[, , ,] Power = new double[Plants_Num, Units_Num, Hour, Step_Num];
            //*************************************************************************
            double[, ,] Power_memory = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Power_if = new double[Plants_Num, Units_Num, Hour];
            //*************************************************************************
            double[, ,] Power_Error = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Power_Error_Pack = new double[Plants_Num, Units_Num, Hour];
            //*************************************************************************
            int[, , ,] X_select = new int[Plants_Num, Units_Num, Hour, Power_Num];
            //*************************************************************************
            double[, ,] Power_memory_Pack = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Power_if_Pack = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Power_if_CC = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Test = new double[Plants_Num, Units_Num, Hour];
            //**************************************************************************
            int[, ,] CO_steam = new int[Plants_Num, Units_Num, Hour];
            int[, ,] CO_Dispatch = new int[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        CO_steam[j, i, h] = 2;
                        CO_Dispatch[j, i, h] = 2;
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Capacity_Plant_Pack = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        Capacity_Plant_Pack[j, i, h] = Unit_Data[j, i, 4];
                    }
                }
            }
            
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Pmin_Plant_Pack = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam") && (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                        {
                            Pmin_Plant_Pack[j, i, h] =  2 * Unit_Data[j, i, 3];
                            if (Pmin_Plant_Pack[j, i, h] > (0.95 * Pmax_Plant_Pack[j, i, h]))
                            {
                                Pmin_Plant_Pack[j, i, h] = (0.93 * Pmax_Plant_Pack[j, i, h]);
                            }
                        }
                        else if ((Unit_Dec[j, i, 0].ToString().Trim() == "Gas") && (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                        {
                            Pmin_Plant_Pack[j, i, h] =  Unit_Data[j, i, 3];
                            if (Pmin_Plant_Pack[j, i, h] > (0.95 * Pmax_Plant_Pack[j, i, h]))
                            {
                                Pmin_Plant_Pack[j, i, h] = (0.7 * Pmax_Plant_Pack[j, i, h]);
                            }
                        }
                        else
                        {
                            Pmin_Plant_Pack[j, i, h] = Unit_Data[j, i, 3];
                            if (Pmin_Plant_Pack[j, i, h] > (0.95 * Pmax_Plant_Pack[j, i, h]))
                            {
                                Pmin_Plant_Pack[j, i, h] = (0.5 * Pmax_Plant_Pack[j, i, h]);
                            }
                        }
                        if (Pmin_Plant_Pack[j, i, h] == 0)
                        {
                            Pmin_Plant_Pack[j, i, h] = Unit_Data[j, i, 3];
                        }
                    }
                }
            }
           
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Pmin_Plant_Pack_Total_Max = new double[Plants_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if ((Pmin_Plant_Pack[j, i, h] > Pmin_Plant_Pack_Total_Max[j, h]) && (Pmin_Plant_Pack[j, i, h] != 0))
                        {
                            Pmin_Plant_Pack_Total_Max[j, h] = Pmin_Plant_Pack[j, i, h];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Pmin_Plant_Pack_Total = new double[Plants_Num, Hour];
            double[, ,] Pmin_Plant_Pack_Total_Check = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    Pmin_Plant_Pack_Total[j, h] = 1000;
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if ((Pmin_Plant_Pack[j, i, h] < Pmin_Plant_Pack_Total[j, h]) && (Pmin_Plant_Pack[j, i, h] != 0))
                        {
                            Pmin_Plant_Pack_Total[j, h] = Pmin_Plant_Pack[j, i, h];
                        }
                    }
                    if (Pmin_Plant_Pack_Total[j, h] == 1000)
                    {
                        MessageBox.Show("Pmin_Plant_Pack_Total is Wrong");
                    }
                }
            }
           
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] PDiff_Max_Min_Plant_Pack_Check = new double[Plants_Num,  Units_Num, Hour];
            double[,,] PDiff_Max_Min_Plant_Pack = new double[Plants_Num, 3, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if ((Outservice_Plant_unit[j, k, h] == 0))
                        {
                            PDiff_Max_Min_Plant_Pack_Check[j, k, h] = 0.15 * Pmin_Plant_Pack[j, k, h];
                        }
                        {
                            if ((Unit_Dec[j, k, 1] == "Steam") && (PDiff_Max_Min_Plant_Pack_Check[j, k, h] > PDiff_Max_Min_Plant_Pack[j, 2, h])) PDiff_Max_Min_Plant_Pack[j, 2, h] = PDiff_Max_Min_Plant_Pack_Check[j, k, h];
                            if ((Unit_Dec[j, k, 1] == "Gas") && (PDiff_Max_Min_Plant_Pack_Check[j, k, h] > PDiff_Max_Min_Plant_Pack[j, 1, h])) PDiff_Max_Min_Plant_Pack[j, 1, h] = PDiff_Max_Min_Plant_Pack_Check[j, k, h];
                            if ((Unit_Dec[j, k, 1] == "CC") && (PDiff_Max_Min_Plant_Pack_Check[j, k, h] > PDiff_Max_Min_Plant_Pack[j, 0, h])) PDiff_Max_Min_Plant_Pack[j, 0, h] = PDiff_Max_Min_Plant_Pack_Check[j, k, h];
                        }
                    }
                }
            }


            //////////////////////////////////////////////////////////////////////////
            DataTable dtsmaxdatex = Utilities.GetTable("select * from dbo.BaseData where Date<='" + biddingDate + "'order by BaseID desc");
            if (dtsmaxdatex.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdatex.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdatex.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdatex = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdatex.Rows[0]["Date"].ToString();
            }
            DataTable basetable = Utilities.GetTable("select ProposalDay from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetable.Rows.Count > 0)
            {
                Marketrule = basetable.Rows[0][0].ToString();
            }
            //////////////////////////////////////////////////////////////////////////

            tname005 = "dbo.DetailFRM005";
            if (Marketrule == "Fuel Limited") tname005 = "BaDetailFRM005";

            string tnamElgha = "BaDetailFRM005";



            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Number_Count_up = new int[Plants_Num, Units_Num];
            int[,] Number_Count_down = new int[Plants_Num, Units_Num];
            double[, ,] Price_memory = new double[Plants_Num, Units_Num, Hour];
            int[, ,] Eh_memory = new int[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] History_Require = new double[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] History_Economic = new double[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] History_Dispatch = new double[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] History_UL = new string[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ] History_Req_T = new double[Plants_Num, Hour];
            double[, ] History_Dis_T = new double[Plants_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ] History_Req_Y = new double[Plants_Num, Hour];
            double[, ] History_Dis_Y = new double[Plants_Num,  Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] History_Require_EL = new double[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] History_Economic_EL = new double[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] History_Dispatch_EL = new double[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[,] blocks = Getblock();
            DataTable baseplant1 = Utilities.GetTable("select PPName from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            string [] ss1 = baseplant1.Rows[0][0].ToString().Trim().Split('-');
            DataTable idtable1 = Utilities.GetTable("select PPID from dbo.PowerPlant  where PPName='" +ss1[0].Trim() + "'");

            string preplant1 = idtable1.Rows[0][0].ToString().Trim();
            int indexpre = 0;
            string[,] prenameblocks = preblock(preplant1);
            string preonepack = prenameblocks[0, 0];
            DataTable oDatapre = Utilities.GetTable("select Max(PackageCode) from UnitsDataMain where  PPID='" + preplant1 + "'");
            int packprenumber = (int)oDatapre.Rows[0][0];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution,Economic  from " + tnamElgha + " where PPID='" + plant[j] +
                    "'and TargetMarketDate='" + near005table(blocks[j, k], plant[j]) +
                    "'and Block='" + blocks[j, k] + "'");

                    int r = oDataTable.Rows.Count;

                    if (r > 0)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            History_Require_EL[j, k, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                            History_Dispatch_EL[j, k, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                            History_Economic_EL[j, k, h] = MyDoubleParse(oDataTable.Rows[h][3].ToString());
                        }
                    }
                    else
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            if (Plant_units_Num[j] == Plant_units_Num[j])
                            {
                                History_Economic_EL[j, k, h] = Pmax_Plant_Pack[j, k, h];
                                History_Require_EL[j, k, h] = Pmax_Plant_Pack[j, k, h];
                                History_Dispatch_EL[j, k, h] = Pmax_Plant_Pack[j, k, h];
                            }
                        }

                    }

                }
            }
            //-------------------------------------------------------------------------------Elghaee
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution,Economic  from " + tname005 + " where PPID='" + plant[j] +
                    "'and TargetMarketDate='" + near005table(blocks[j, k], plant[j]) +
                    "'and Block='" + blocks[j, k] + "'");

                    int r = oDataTable.Rows.Count;

                    if (r > 0)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            History_Require[j, k, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                            History_Dispatch[j, k, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                            History_UL[j, k, h] = "N";
                            //History_UL[j, k, h] = oDataTable.Rows[h][2].ToString().Trim();
                            History_Economic[j, k, h] = MyDoubleParse(oDataTable.Rows[h][3].ToString());
                        }
                    }
                    else
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            if (Plant_units_Num[j] == Plant_units_Num[j])
                            {
                                History_Economic[j, k, h] = Pmax_Plant_Pack[j, k, h];
                                History_Require[j, k, h] = Pmax_Plant_Pack[j, k, h];
                                History_Dispatch[j, k, h] = Pmax_Plant_Pack[j, k, h];
                                History_UL[j, k, h] = "N";
                            }
                        }

                    }

                }
            }
            //-------------------------------------------------------------------------------

            double[, ,] Capacity_Economic = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Capacity_Require = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Capacity_Dispatch = new double[Plants_Num, Units_Num, Hour];

            double[,] Capacity_Economic_max = new double[Plants_Num, Hour];
            double[,] Capacity_Require_max = new double[Plants_Num, Hour];
            double[,] Capacity_Dispatch_max = new double[Plants_Num, Hour];

            double[,] Capacity_Economic_max_EL = new double[Plants_Num, Hour];
            double[,] Capacity_Require_max_EL = new double[Plants_Num, Hour];
            double[,] Capacity_Dispatch_max_EL = new double[Plants_Num, Hour];

            double[, ,] Capacity_Economicba = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Capacity_Requireba = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Capacity_Dispatchba = new double[Plants_Num, Units_Num, Hour];

            double[, ,] Capacity_Economicbi = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Capacity_Requirebi = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Capacity_Dispatchbi = new double[Plants_Num, Units_Num, Hour];

            string[, ,] History_ULba = new string[Plants_Num, Units_Num, Hour];
            string[, ,] History_ULbi = new string[Plants_Num, Units_Num, Hour];
            




            if (Marketrule == "Normal")
            {
                for (int j = 0; j < Plants_Num; j++)
                {
                    string blockM005 = "";
                    string PlantM005 = "";
                    string selectedDate1 = (near005table(prenameblocks[0, 0], preplant1).Trim());
                    try
                    {
                        string status = valtrain(plant[j], selectedDate1);
                        string[] split = status.Split('-');

                        if (status.Contains("Presolve-Unit"))
                        {
                            PlantM005 = plant[j];
                            DataSet myDs1 = UtilityPowerPlantFunctions.GetdefaultUnits(Convert.ToInt32(PlantM005), split[2].Trim());
                            DataTable[] orderedPackages1 = UtilityPowerPlantFunctions.OrderPackages(myDs1);
                            foreach (DataTable dtPackageType1 in orderedPackages1)
                            {
                                int packageOrder = 0;
                                if (dtPackageType1.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                                orderedPackages1.Length > 1)
                                    packageOrder = 1;

                                foreach (DataRow unitRow in dtPackageType1.Rows)
                                {
                                    string package = unitRow["PackageType"].ToString().Trim();
                                    string unit = unitRow["UnitCode"].ToString().Trim();
                                    string packagecode = unitRow["PackageCode"].ToString().Trim();

                                    string ptypenum = "0";
                                    if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";

                                    if (Findcconetype(plant[j].ToString())) ptypenum = "0";

                                    string temp = unit.ToLower();

                                    string ppidWithPriority = (PlantM005).ToString();

                                    if (package.Contains("CC"))
                                    {
                                        temp = temp.Replace("cc", "c");
                                        string[] sp = temp.Split('c');
                                        temp = sp[0].Trim() + sp[1].Trim();
                                        if (temp.Contains("gas"))
                                        {
                                            temp = temp.Replace("gas", "G");

                                        }
                                        else
                                        {
                                            temp = temp.Replace("steam", "S");

                                        }
                                        temp = ppidWithPriority + "-" + temp;
                                    }
                                    else if (temp.Contains("gas"))
                                    {
                                        temp = ppidWithPriority + "-" + temp.Replace("gas", "G");
                                    }
                                    else
                                    {
                                        temp = ppidWithPriority + "-" + temp.Replace("steam", "S");
                                    }
                                    blockM005 = temp.Trim();
                                }
                            }
                        }
                        else if (status.Contains("Presolve - plant"))
                        {
                            PlantM005 = plant[j];
                            int ppii = int.Parse(split[2].Trim());
                            DataSet myDs1 = UtilityPowerPlantFunctions.GetUnits(ppii);
                            DataTable[] orderedPackages1 = UtilityPowerPlantFunctions.OrderPackages(myDs1);
                            foreach (DataTable dtPackageType1 in orderedPackages1)
                            {
                                int packageOrder = 0;
                                if (dtPackageType1.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                                orderedPackages1.Length > 1)
                                    packageOrder = 1;
                            }
                        }
                        else if (status.Contains("date"))
                        {
                            //string[] s = status.Split(':');
                            //selectedDate1 = PersianDateConverter.ToGregorianDateTime(s[1].Trim());
                        }
                    }
                    catch
                    {

                    }
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if ((blockM005 != "") && (PlantM005 != ""))
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution,Economic  from " + tname005 + " where PPID='" + PlantM005 +
                            "'and TargetMarketDate='" + selectedDate1 +
                            "'and Block='" + blockM005 + "'");
                        }
                        else if (blockM005 != "")
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution,Economic  from " + tname005 + " where PPID='" + PlantM005 +
                            "'and TargetMarketDate='" + selectedDate1 +
                            "'and Block='" + blocks[j, k] + "'");
                        }
                        else if (setparam[j, 4])
                        {
                            if (k < packprenumber)
                            {
                                oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution,Economic  from " + tname005 + " where PPID='" + preplant1 +
                                "'and TargetMarketDate='" + selectedDate1 +
                                "'and Block='" + prenameblocks[0, k] + "'");
                            }
                            else
                            {
                                oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution,Economic  from " + tname005 + " where PPID='" + preplant1 +
                                "'and TargetMarketDate='" + selectedDate1 +
                                "'and Block='" + prenameblocks[0, packprenumber - 1] + "'");
                            }
                        }
                        else
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution,Economic  from " + tname005 + " where PPID='" + plant[j] +
                            "'and TargetMarketDate='" + selectedDate1 +
                            "'and Block='" + blocks[j, k] + "'");
                        }
                        int r0 = oDataTable.Rows.Count;
                        bool indexu = false;
                        if (r0 > 0)
                        {
                            foreach (DataRow m in oDataTable.Rows)
                            {
                                if (staterun == "Required")
                                {
                                    if (m["Required"].ToString().Trim() != "0")
                                    {
                                        indexu = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if (m["Economic"].ToString().Trim() != "0")
                                    {
                                        indexu = true;
                                        break;
                                    }
                                }
                            }
                            if (indexu == true)
                            {
                                for (int kk = 0; kk < Plant_units_Num[j]; kk++)
                                {
                                    for (int h = 0; h < Hour; h++)
                                    {
                                        if ((oDataTable.Rows.Count > 0))
                                        {
                                            Capacity_Economic[j, kk, h] = MyDoubleParse(oDataTable.Rows[h][3].ToString());
                                            Capacity_Require[j, kk, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                                            Capacity_Dispatch[j, kk, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                                            History_UL[j, kk, h] = oDataTable.Rows[h][2].ToString().Trim();
                                        }
                                        else
                                        {
                                            Capacity_Economic[j, kk, h] = History_Economic[j, k, h];
                                            Capacity_Require[j, kk, h] = History_Require[j, k, h];
                                            Capacity_Dispatch[j, kk, h] = History_Dispatch[j, k, h];
                                            History_UL[j, kk, h] = History_UL[j, k, h];
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                for (int j = 0; j < Plants_Num; j++)
                {
                    string blockM005 = "";
                    string PlantM005 = "";
                    //DateTime selectedDate1 = PersianDateConverter.ToGregorianDateTime(near005table(prenameblocks[0, 0], preplant1).Trim());
                    string selectedDate1 = near005table(prenameblocks[0, 0], preplant1).Trim();


                    try
                    {
                        string status = valtrain(plant[j], near005table(blocks[j, 0], plant[j]));
                        string[] split = status.Split('-');
                        if (status.Contains("Presolve-Unit"))
                        {
                            PlantM005 = preplant1;
                            DataSet myDs1 = UtilityPowerPlantFunctions.GetdefaultUnits(Convert.ToInt32(PlantM005), split[2].Trim());
                            DataTable[] orderedPackages1 = UtilityPowerPlantFunctions.OrderPackages(myDs1);
                            foreach (DataTable dtPackageType1 in orderedPackages1)
                            {
                                int packageOrder = 0;
                                if (dtPackageType1.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                                orderedPackages1.Length > 1)
                                    packageOrder = 1;

                                foreach (DataRow unitRow in dtPackageType1.Rows)
                                {
                                    string package = unitRow["PackageType"].ToString().Trim();
                                    string unit = unitRow["UnitCode"].ToString().Trim();
                                    string packagecode = unitRow["PackageCode"].ToString().Trim();

                                    string ptypenum = "0";
                                    if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";

                                    if (Findcconetype(plant[j].ToString())) ptypenum = "0";

                                    string temp = unit.ToLower();

                                    string ppidWithPriority = (PlantM005).ToString();

                                    if (package.Contains("CC"))
                                    {
                                        temp = temp.Replace("cc", "c");
                                        string[] sp = temp.Split('c');
                                        temp = sp[0].Trim() + sp[1].Trim();
                                        if (temp.Contains("gas"))
                                        {
                                            temp = temp.Replace("gas", "G");

                                        }
                                        else
                                        {
                                            temp = temp.Replace("steam", "S");

                                        }
                                        temp = ppidWithPriority + "-" + temp;
                                    }
                                    else if (temp.Contains("gas"))
                                    {
                                        temp = ppidWithPriority + "-" + temp.Replace("gas", "G");
                                    }
                                    else
                                    {
                                        temp = ppidWithPriority + "-" + temp.Replace("steam", "S");
                                    }
                                    blockM005 = temp.Trim();
                                }
                            }
                        }
                        else if (status.Contains("Presolve - plant"))
                        {
                            PlantM005 = plant[j];
                            int ppii = int.Parse(split[2].Trim());
                            DataSet myDs1 = UtilityPowerPlantFunctions.GetUnits(ppii);
                            DataTable[] orderedPackages1 = UtilityPowerPlantFunctions.OrderPackages(myDs1);
                            foreach (DataTable dtPackageType1 in orderedPackages1)
                            {
                                int packageOrder = 0;
                                if (dtPackageType1.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                                orderedPackages1.Length > 1)
                                    packageOrder = 1;
                            }
                        }
                        else if (status.Contains("date"))
                        {
                            //string[] s = status.Split(':');
                            //selectedDate1 = PersianDateConverter.ToGregorianDateTime(s[1].Trim());
                        }
                    }
                    catch
                    {

                    }
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if ((blockM005 != "") && (PlantM005 != ""))
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution,Economic  from " + tname005 + " where PPID='" + PlantM005 +
                            "'and TargetMarketDate='" + selectedDate1 +
                            "'and Block='" + blockM005 + "'");
                        }
                        else if (blockM005 != "")
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution,Economic  from " + tname005 + " where PPID='" + PlantM005 +
                            "'and TargetMarketDate='" + selectedDate1 +
                            "'and Block='" + blocks[j, k] + "'");
                        }
                        else if (setparam[j, 4])
                        {
                            if (k < packprenumber)
                            {
                                oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution,Economic  from " + tname005 + " where PPID='" + preplant1 +
                                "'and TargetMarketDate='" + selectedDate1 +
                                "'and Block='" + prenameblocks[0, k] + "'");
                            }
                            else
                            {
                                oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution,Economic  from " + tname005 + " where PPID='" + preplant1 +
                                "'and TargetMarketDate='" + selectedDate1 +
                                "'and Block='" + prenameblocks[0, packprenumber - 1] + "'");
                            }
                        }
                        else
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution,Economic  from " + tname005 + " where PPID='" + plant[j] +
                            "'and TargetMarketDate='" + selectedDate1 +
                            "'and Block='" + blocks[j, k] + "'");
                        }
                        int r0 = oDataTable.Rows.Count;
                        bool indexu = false;
                        if (r0 > 0)
                        {
                            foreach (DataRow m in oDataTable.Rows)
                            {
                                if (staterun == "Required")
                                {
                                    if (m["Required"].ToString().Trim() != "0")
                                    {
                                        indexu = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if (m["Economic"].ToString().Trim() != "0")
                                    {
                                        indexu = true;
                                        break;
                                    }
                                }
                            }
                            if (indexu == true)
                            {
                                for (int kk = 0; kk < Plant_units_Num[j]; kk++)
                                {
                                    for (int h = 0; h < Hour; h++)
                                    {
                                        if ((oDataTable.Rows.Count > 0))
                                        {
                                            Capacity_Economic[j, kk, h] = MyDoubleParse(oDataTable.Rows[h][3].ToString());
                                            Capacity_Require[j, kk, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                                            Capacity_Dispatch[j, kk, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                                            History_UL[j, kk, h] = oDataTable.Rows[h][2].ToString().Trim();
                                        }
                                        else
                                        {
                                            Capacity_Economic[j, kk, h] = History_Economic[j, k, h];
                                            Capacity_Require[j, kk, h] = History_Require[j, k, h];
                                            Capacity_Dispatch[j, kk, h] = History_Dispatch[j, k, h];
                                            History_UL[j, kk, h] = History_UL[j, k, h];
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        
                        {
                            if (Capacity_Economic_max[j, h] <= History_Economic[j, k, h])
                            {
                                Capacity_Economic_max[j, h] = History_Economic[j, k, h];
                            }
                            if (Capacity_Require_max[j, h] <= History_Require[j, k, h])
                            {
                                Capacity_Require_max[j, h] = History_Require[j, k, h];
                            }
                            if (Capacity_Dispatch_max[j, h] <= History_Dispatch[j, k, h])
                            {
                                Capacity_Dispatch_max[j, h] = History_Dispatch[j, k, h];
                            }
                        }
                    }
                }
            }

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {

                        {
                            if (Capacity_Economic_max_EL[j, h] <= History_Economic_EL[j, k, h])
                            {
                                Capacity_Economic_max_EL[j, h] = History_Economic_EL[j, k, h];
                            }
                            if (Capacity_Require_max_EL[j, h] <= History_Require_EL[j, k, h])
                            {
                                Capacity_Require_max_EL[j, h] = History_Require_EL[j, k, h];
                            }
                            if (Capacity_Require_max_EL[j, h] >=Capacity_Economic_max_EL[j, h])
                            {
                                Capacity_Economic_max_EL[j, h] = Capacity_Require_max_EL[j, h];
                            }
                        }
                    }
                }
            }
            
                     
            for (int j = 0; j < Plants_Num; j++)
            {
                string selectedDate2 = (near005table(prenameblocks[0, 0], preplant1).Trim());
                string status2 = valtrain(plant[j], selectedDate2);
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (status2.Contains("Presolve-Unit"))
                        {

                        }
                        else
                        {
                            if (Capacity_Economic_max[j, h] >= Capacity_Require_max[j, h])
                            {
                                Capacity_Economic[j, k, h] = Capacity_Economic_max[j, h];
                            }
                            else
                            {
                                Capacity_Economic[j, k, h] = Capacity_Require_max[j, h];
                            }

                            Capacity_Require[j, k, h] = Capacity_Require_max[j, h];
                            Capacity_Dispatch[j, k, h] = Capacity_Dispatch_max[j, h];
                            History_UL[j, k, h] = "N";
                        }
                    }
                }
            }
            
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int test_n = 0;
            int[, ,] OneGas = new int[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (History_UL[j, k, h] != null)
                        {
                            if (History_UL[j, k, h].Contains("UL"))
                            {
                                UL_State[j, k, h] = "UL";
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        test_n = 0;
                            for (int h = 1; h < Hour; h++)
                            {
                                if ((History_Require[j, k, Hour - h] > 0) & (test_n == 0))
                                {
                                    Number_Count_up[j, i] = h;
                                }
                                else
                                {
                                    test_n = 1;
                                }
                            }

                        if (Unit_Dec[j,k,1] == "CC")
                        {
                            if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                            {
                                Number_Count_up[j, i] = 23;
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        test_n = 0;

                        for (int h = 1; h < Hour; h++)
                        {
                            if ((History_Require[j, k, Hour - h] == 0) & (test_n == 0))
                            {
                                Number_Count_down[j, i] = h;
                            }
                            else
                            {
                                test_n = 1;
                            }
                        }

                        if (Unit_Dec[j, k, 1] == "CC")
                        {
                            if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                            {
                                Number_Count_down[j, i] = 0;
                            }
                        }
                    }
                }
            }
            
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if ((Number_Count_down[j, i] > 0) & (Number_Count_up[j, i] > 0))
                    {
                        Number_Count_down[j, i] = 0;
                    }
                    if (Outservice_Plant_unit[j, i, 0] == 1)
                    {
                        Number_Count_down[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] hisstart_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_up[j, i] < GastostartSteam)
                    {
                        hisstart_Plant_unit[j, i] = Number_Count_up[j, i];
                    }
                    if (Number_Count_up[j, i] >= GastostartSteam)
                    {
                        hisstart_Plant_unit[j, i] = GastostartSteam - 1;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] HisShut_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_down[j, i] < Unit_Data[j, i, 16])
                    {
                        HisShut_Plant_unit[j, i] = Number_Count_down[j, i];
                    }
                    if (Number_Count_down[j, i] >= Unit_Data[j, i, 16])
                    {
                        HisShut_Plant_unit[j, i] = (int)(Unit_Data[j, i, 16] - 1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Powerhistory_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_up[j, i] >= 0)
                    {
                        Powerhistory_Plant_unit[j, i] = ((int)Unit_Data[j, i, 3]);
                    }
                    if (Number_Count_down[j, i] > 0)
                    {
                        Powerhistory_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] V_history_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_up[j, i] >= 0)
                    {
                        V_history_Plant_unit[j, i] = 1;
                    }
                    if (Number_Count_down[j, i] > 0)
                    {
                        V_history_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Hisup_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if ((Number_Count_up[j, i] < Unit_Data[j, i, 6]) & (Number_Count_up[j, i] != 0))
                    {
                        Hisup_Plant_unit[j, i] = (int)Unit_Data[j, i, 6] - Number_Count_up[j, i];
                    }
                    if (Number_Count_up[j, i] >= Unit_Data[j, i, 6])
                    {
                        Hisup_Plant_unit[j, i] = 0;
                    }
                    if (Number_Count_up[j, i] == 0)
                    {
                        Hisup_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Hisdown_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if ((Number_Count_down[j, i] < Unit_Data[j, i, 7]) & (Number_Count_down[j, i] > 0))
                    {
                        Hisdown_Plant_unit[j, i] = (int)Unit_Data[j, i, 7] - Number_Count_down[j, i];
                    }
                    if (Number_Count_down[j, i] >= Unit_Data[j, i, 7])
                    {
                        Hisdown_Plant_unit[j, i] = 0;
                    }
                    if (Number_Count_down[j, i] == 0)
                    {
                        Hisdown_Plant_unit[j, i] = 0;
                    }
                }
            }
                      
            
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Hour_Cap_Automatic = new string[Plants_Num, Units_Num, Hour];
            string[, ,] Hour_Cap_customize = new string[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Hour_State_Automatic = new string[Plants_Num, Units_Num, Hour];
            string[, ,] Hour_State_customize = new string[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] PriceForecasting = new double[Plants_Num, Units_Num, Hour];


            strCmd = "select * from BiddingStrategySetting where id=" + BiddingStrategySettingId.ToString();
            oDataTable = Utilities.GetTable(strCmd);

            string runSetting = oDataTable.Rows[0]["RunSetting"].ToString().Trim();
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[,] Run_State_user = new string[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    Run_State_user[j, k] = runSetting.Trim();
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            if (runSetting == "Automatic")
            {
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Hour_Cap_Automatic[j, k, h] = oDataTable.Rows[0]["PeakBid"].ToString().Trim();
                            Hour_State_Automatic[j, k, h] = oDataTable.Rows[0]["StrategyBidding"].ToString().Trim();
                        }
                    }
                }
            }
            else
            {
                strCmd = "select * from BiddingStrategyCustomized where FkBiddingStrategySettingId=" + BiddingStrategySettingId.ToString() + " order by hour";
                oDataTable = Utilities.GetTable(strCmd);

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        for (int h = 0; h < 24; h++)
                        {
                            Hour_Cap_customize[j, k, h] = oDataTable.Rows[h]["Peak"].ToString().Trim();
                            Hour_State_customize[j, k, h] = oDataTable.Rows[h]["Strategy"].ToString().Trim();
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Hour_Cap_User = new string[Hour, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Hour_Cap_User[j, k, h] = "NO";
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Run_State_user[j, k] == "Automatic")
                        {
                            Hour_Cap_User[j, k, h] = Hour_Cap_Automatic[j, k, h];
                        }
                        if (Run_State_user[j, k] == "Customize" && Hour_Cap_Automatic[j, k, h] != "")
                        {
                            Hour_Cap_User[j, k, h] = Hour_Cap_customize[j, k, h];
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
           
            bool[] UseNearestMaxBid = new bool[Plants_Num];
            double[, , ,] maxs = new double[Plants_Num, Const_selectdays, 24,3];
            double[,] maxs_Hub = new double[Plants_Num, 24];
            string[] dayofmaxs_Package = new string[Const_selectdays];
            for (int j = 0; j < Plants_Num; j++)
            {
                DataSet myDs = UtilityPowerPlantFunctions.GetUnits(int.Parse(plant[j]));
                PersianDate test1 = new PersianDate(currentDate);
                DateTime Now = PersianDateConverter.ToGregorianDateTime(test1);

                DataTable[] orderedPackages = UtilityPowerPlantFunctions.OrderPackages(myDs);

                //////////////////////////////////////////////////////////////////////////////
                DataTable[] Packagesindex = orderedPackages;
                ///////////////////////////////for presolve/////////////////////////////////////
                string finalplant = plant[j];

                DataTable baseplantfor7day = Utilities.GetTable("select PPName from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                string[] ss2 = baseplantfor7day.Rows[0][0].ToString().Trim().Split('-'); 
                DataTable idtable7day = Utilities.GetTable("select PPID from dbo.PowerPlant  where PPName='" + ss2[0].Trim() + "'");

                string preplant7day = idtable7day.Rows[0][0].ToString().Trim();

                DataTable packagetable7day = Utilities.GetTable("SELECT DISTINCT  PackageType  FROM dbo.PPUnit WHERE PPID='" + preplant7day.Trim() + "'");

                string packtype7day = "";
                try
                {
                   packtype7day = ss2[1].Trim();
                }
                catch
                {

                }

                if (packtype7day == "Combined Cycle") packtype7day = "CC";
                if (packtype7day == "" || packtype7day == null)
                {
                    for (int i = 0; i < packagetable7day.Rows.Count; i++)
                    {
                        if (packagetable7day.Rows[i][0].ToString().Trim() == "Steam")
                        {
                            packtype7day = "Steam";
                            break;
                        }
                        else if (packagetable7day.Rows[i][0].ToString().Trim() == "Combined Cycle") packtype7day = "CC";

                    }
                }
                DataSet myDs7 = UtilityPowerPlantFunctions.GetUnits(int.Parse(preplant7day));
                DataTable[] orderedPackages7 = OrderPackages7(myDs7);
                foreach (DataTable dtPackageType in orderedPackages)
                {
                    foreach (DataTable dtPackageType7 in orderedPackages7)
                    {
                        if ((setparam[j, 4] == true))
                        {                           
                                //orderedPackages = orderedPackages7;
                                //finalplant = preplant7day;
                                //break;
                        }
                    }

                }

                int Packnum = 0;
                int Packnum_Index = 0;
                foreach (DataTable dtPackageType in orderedPackages)
                {
                    for (int daysbefore = 0; daysbefore < Const_selectdays; daysbefore++)
                    {
                        DateTime selectedDate = Now.Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                        DateTime mine = selectedDate;
                        bool isspedate = false;
                            if (isspecial(new PersianDate(selectedDate).ToString("d")))
                            {
                            }
                        
                        dayofmaxs_Package[daysbefore] = selectedDate.DayOfWeek.ToString();
                        ////////////////////////////allplants maxbid//////////////////////
                        double[] allmax = UtilityPowerPlantFunctions.CALLPlantPowerPlantMaxBid(selectedDate,biddingDate);

                        ////////////check 002 and 005 not empty////////////////////////////

                        //if (FindCommon002and005(selectedDate, finalplant) == "nocontinue" || FindCommon002and005(selectedDate, finalplant) == "")
                        //{
                           
                        //    return false;
                        //}
                        //else
                        //{
                            if (!isspedate)
                            {
                                
                                if (FindCommon002and005(selectedDate, finalplant) == "nocontinue" || FindCommon002and005(selectedDate, finalplant) == "")
                                {
                                    return false;
                                }
                                else
                                    selectedDate = PersianDateConverter.ToGregorianDateTime(FindCommon002and005(selectedDate, finalplant));

                            }
                            else
                            {
                                
                                if (FindCom002and005special(selectedDate, finalplant) == "nocontinue" || FindCom002and005special(selectedDate, finalplant) == "")
                                {
                                    return false;
                                }
                                else
                                    selectedDate = PersianDateConverter.ToGregorianDateTime(FindCom002and005special(selectedDate, finalplant));
                            }
                       // }
                        ///////////////////////////////////////////////////////////////////
                        int packageOrder = 0;
                        if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                        orderedPackages.Length > 1)
                            packageOrder = 1;

                        CMatrix NSelectPrice = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid
                               (Convert.ToInt32(finalplant), dtPackageType, selectedDate, packageOrder,biddingDate);

                        CMatrix NSelectPrice_Hub = NSelectPrice;

                        CMatrix NSelectPrice_Hub1 = NSelectPrice;

                        CMatrix NSelectPrice_Hub2 = NSelectPrice;

                        CMatrix NSelectPrice_Hub3 = NSelectPrice;

                        ///////////////////////////////////Test///////////////////////////////////
                        try
                        {
                            int attemp = 0;
                            int maxAttemp = 6;


                            while ((NSelectPrice == null || NSelectPrice.Zero)
                                    && selectedDate.Date <= Now.Date && attemp < maxAttemp)
                            {
                                attemp++;
                                DateTime TestselectedDate = PersianDateConverter.ToGregorianDateTime(selectedDate).Subtract(new TimeSpan(attemp, 0, 0, 0));
                                //date = new PersianDate(selectedDate).ToString("d");
                                // oneDayPrice = CalculatePowerPlantMaxBid(ppId, dtPackageType, selectedDate, packagePriorityIndex);
                                NSelectPrice = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid
                                   (Convert.ToInt32(finalplant), dtPackageType, TestselectedDate, packageOrder,biddingDate);
                                UseNearestMaxBid[j] = true;
                            }
                        }
                        catch
                        {

                            NSelectPrice = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid
                               (Convert.ToInt32(finalplant), dtPackageType, selectedDate, packageOrder,biddingDate);
                            UseNearestMaxBid[j] = false;

                        }

                        //////////////////////////////////////End Test/////////////////////////////////////
                        /////////////////////////////STATUS/////////////////////////////////////////////////////////////////////////
                        try
                        {
                            string status = valtrain(finalplant, new PersianDate(mine).ToString("d"));
                            string[] split = status.Split('-');
                            if (status.Contains("Presolve-Unit"))
                            {
                                DataSet myDs1 = UtilityPowerPlantFunctions.GetdefaultUnits(Convert.ToInt32(finalplant), split[2].Trim());
                                DataTable[] orderedPackages1 = UtilityPowerPlantFunctions.OrderPackages(myDs1);
                                foreach (DataTable dtPackageType1 in orderedPackages1)
                                {
                                    NSelectPrice = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid(Convert.ToInt32(finalplant), dtPackageType1, mine, packageOrder, biddingDate);
                                }

                                if ((Convert.ToInt32(finalplant) == 980) || (Convert.ToInt32(finalplant) == 701))
                                {
                                    string status1 = "Presolve-Unit- Steam cc1";
                                    string[] split1 = status1.Split('-');
                                    DataSet myDs2 = UtilityPowerPlantFunctions.GetdefaultUnits(Convert.ToInt32(finalplant), split1[2].Trim());
                                    DataTable[] orderedPackages2 = UtilityPowerPlantFunctions.OrderPackages(myDs2);
                                    foreach (DataTable dtPackageType11 in orderedPackages2)
                                    {
                                        NSelectPrice_Hub1 = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid(Convert.ToInt32(finalplant), dtPackageType11, mine, packageOrder, biddingDate);
                                    }

                                    string status2 = "Presolve-Unit- Steam cc2";
                                    string[] split2 = status2.Split('-');
                                    DataSet myDs3 = UtilityPowerPlantFunctions.GetdefaultUnits(Convert.ToInt32(finalplant), split2[2].Trim());
                                    DataTable[] orderedPackages3 = UtilityPowerPlantFunctions.OrderPackages(myDs3);
                                    foreach (DataTable dtPackageType12 in orderedPackages3)
                                    {
                                        NSelectPrice_Hub2 = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid(Convert.ToInt32(finalplant), dtPackageType12, mine, packageOrder, biddingDate);
                                    }

                                    string status3 = "Presolve-Unit- Steam cc3";
                                    string[] split3 = status3.Split('-');
                                    DataSet myDs4 = UtilityPowerPlantFunctions.GetdefaultUnits(Convert.ToInt32(finalplant), split3[2].Trim());
                                    DataTable[] orderedPackages4 = UtilityPowerPlantFunctions.OrderPackages(myDs4);
                                    foreach (DataTable dtPackageType13 in orderedPackages3)
                                    {
                                        NSelectPrice_Hub3 = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid(Convert.ToInt32(finalplant), dtPackageType13, mine, packageOrder, biddingDate);
                                    }

                                    for (int h = 0; h < 24; h++)
                                    {
                                        NSelectPrice_Hub[0, h] = CapPrice[0];

                                        if ((NSelectPrice_Hub [0, h] > NSelectPrice_Hub1[0, h])||(NSelectPrice_Hub1[0, h] > 0))
                                        {
                                            NSelectPrice_Hub [0, h] = NSelectPrice_Hub1[0, h];
                                        }

                                        if ((NSelectPrice_Hub[0, h] > NSelectPrice_Hub2[0, h]) || (NSelectPrice_Hub2[0, h] > 0))
                                        {
                                            NSelectPrice_Hub[0, h] = NSelectPrice_Hub2[0, h];
                                        }

                                        if ((NSelectPrice_Hub[0, h] > NSelectPrice_Hub3[0, h]) || (NSelectPrice_Hub3[0, h] > 0))
                                        {
                                            NSelectPrice_Hub[0, h] = NSelectPrice_Hub3[0, h];
                                        }

                                        if (NSelectPrice_Hub[0, h] < NSelectPrice[0, h])
                                        {
                                            NSelectPrice_Hub[0, h] = NSelectPrice[0, h]+ 15000;
                                        }

                                    }

                                }


                                if (NSelectPrice.Zero)
                                {

                                    if (Findexist002and005(plant[j], new PersianDate(PersianDateConverter.ToGregorianDateTime(currentDate).AddDays(-1)).ToString("d")))
                                    {

                                        foreach (DataTable dtPackageType1 in orderedPackages1)
                                        {
                                            NSelectPrice = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid(Convert.ToInt32(finalplant), dtPackageType1, PersianDateConverter.ToGregorianDateTime(currentDate).AddDays(-1), packageOrder, biddingDate);
                                        }
                                    }
                                    else if (Findexist002and005(plant[j], new PersianDate(PersianDateConverter.ToGregorianDateTime(currentDate).AddDays(-2)).ToString("d")))
                                    {
                                        foreach (DataTable dtPackageType1 in orderedPackages1)
                                        {
                                            NSelectPrice = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid(Convert.ToInt32(finalplant), dtPackageType1, PersianDateConverter.ToGregorianDateTime(currentDate).AddDays(-2), packageOrder, biddingDate);
                                        }

                                    }
                                }

                            }
                            else if (status.Contains("Presolve - plant"))
                            {
                                int ppii = int.Parse(split[2].Trim());
                                DataSet myDs1 = UtilityPowerPlantFunctions.GetUnits(ppii);
                                DataTable[] orderedPackages1 = UtilityPowerPlantFunctions.OrderPackages(myDs1);
                                foreach (DataTable dtPackageType1 in orderedPackages1)
                                {
                                   
                                    NSelectPrice = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid(ppii, dtPackageType1, mine, packageOrder, biddingDate);
                                }
                            }
                            else if (status.Contains("date"))
                            {
                                string[] s = status.Split(':');
                                DateTime selectedDate1 = PersianDateConverter.ToGregorianDateTime(s[1].Trim());
                                NSelectPrice = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid(Convert.ToInt32(finalplant), dtPackageType, selectedDate1, packageOrder, biddingDate);
                            }
                            else if (status == "defaultprice")
                            {

                                Random rand = new Random();
                                for (int h = 0; h < 24; h++)
                                {
                                    double vc = defaultpicefind(finalplant, h);
                                    if (vc > 0)
                                    {
                                        NSelectPrice[0, h] = vc + (10 * rand.NextDouble());
                                    }

                                }
                            }

                            if (NSelectPrice.Zero)
                            {
                                Random rand = new Random();
                                for (int h = 0; h < 24; h++)
                                {
                                    double vc = defaultpicefind(finalplant, h);
                                    if (vc > 0)
                                    {
                                        NSelectPrice[0, h] = vc + (10 * rand.NextDouble());
                                    }

                                }

                            }
                        }
                        catch
                        {

                        }
                        /////////////////////////////////////ENDSTATUS////////////////////////////////////////////////////////////////

                        
                        if (setparam[j, 4])
                        {
                            foreach (DataTable dtindex in Packagesindex)
                            {

                                if (dtindex.ToString().Trim() == "Steam")
                                {
                                    Packnum_Index = 0;
                                }
                                if (dtindex.ToString().Trim() == "CC")
                                {
                                    Packnum_Index = 1;
                                }
                                if (dtindex.ToString().Trim() == "Gas")
                                {
                                    Packnum_Index = 2;
                                }

                                for (int k = 0; k < 24; k++)
                                {
                                    maxs[j, daysbefore, k, Packnum_Index] = NSelectPrice[0, k];

                                    if ((plant[j] == "701") || (plant[j] == "980"))
                                    {
                                        maxs[j, daysbefore, k, 0] = NSelectPrice_Hub[0, k];
                                    }
                                }

                            }
                        }
                        else
                        {
                            if (dtPackageType.ToString().Trim() == "Steam")
                            {
                                Packnum_Index = 0;
                            }
                            if (dtPackageType.ToString().Trim() == "CC")
                            {
                                Packnum_Index = 1;
                            }
                            if (dtPackageType.ToString().Trim() == "Gas")
                            {
                                Packnum_Index = 2;
                            }
                            for (int k = 0; k < 24; k++)
                            {
                                maxs[j, daysbefore, k, Packnum_Index] = NSelectPrice[0, k];
                                if ((plant[j] == "701") || (plant[j] == "980"))
                                {
                                    maxs[j, daysbefore, k, 0] = NSelectPrice_Hub[0, k];
                                }
                            }
                        }
                    }
                }
            }

            if (message != "" && message.Length<400)
            {
               // MessageBox.Show(message, "There Is No Data For :", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            message = "";

            ///////////////////////delete maxbidall///////////////////////////////////
            DataTable del4 = Utilities.GetTable("delete from MaxBidALL");

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //----------------------------------------plus old --------------------------------------------------
            DataTable vbaseplant = Utilities.GetTable("select PPName from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            string[] ss3 = vbaseplant.Rows[0][0].ToString().Trim().Split('-');
            DataTable vidtable = Utilities.GetTable("select PPID from dbo.PowerPlant  where PPName='" + ss3[0].Trim() + "'");
            string vpreplant = vidtable.Rows[0][0].ToString().Trim();
            int plusold = 0;

            for (int d = 1; d < 5; d++)
            {

                DateTime dc = PersianDateConverter.ToGregorianDateTime(currentDate);
                dc = dc.AddDays(-d);
                string cuper = new PersianDate(dc).ToString("d");


                DataTable dru2 = Utilities.GetTable("select count(*) from dbo.DetailFRM002 where TargetMarketDate='" + cuper + "' and PPID='" + vpreplant + "'and Estimated=0");
                DataTable dru5 = Utilities.GetTable("select count(*) from "+tname005+" where TargetMarketDate='" + cuper + "' and PPID='" + vpreplant + "'");

                if (MyDoubleParse(dru2.Rows[0][0].ToString()) >= 24 && MyDoubleParse(dru5.Rows[0][0].ToString()) >= 24)
                {
                    plusold = d;
                    break;

                }

            }
            
            //------------------------------------------------------------------------------------------------

            int OldDay_Strategy = 3 + plusold;
            int Hour_Base = 8;
            double[, , ,] maxs_Package = new double[Plants_Num, Const_selectdays, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int d = 0; d < Const_selectdays; d++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            if (Unit_Dec[j, k, 1].ToString().Trim() == "CC")
                            {
                                maxs_Package[j, d, k, h] = maxs[j, d, h, 1];
                            }
                            if (Unit_Dec[j, k, 1].ToString().Trim() == "Steam")
                            {
                                maxs_Package[j, d, k, h] = maxs[j, d, h, 0];
                            }
                            if (Unit_Dec[j, k, 1].ToString().Trim() == "Gas")
                            {
                                maxs_Package[j, d, k, h] = maxs[j, d, h, 2];
                            }

                        }
                    }
                }
            }

            DateTime Week_day = PersianDateConverter.ToGregorianDateTime(biddingDate);
            DateTime Week_Holid = PersianDateConverter.ToGregorianDateTime(biddingDate);
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int d = 1; d < OldDay_Strategy; d++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            DateTime S_Week_day = Week_day;
                            S_Week_day = S_Week_day.AddDays(-d - 1);
                            if ((islocalweek(new PersianDate(S_Week_day).ToString("d")) == true) && (S_Week_day.DayOfWeek.ToString() != "Friday"))
                            {
                                DateTime H_Week_day = Week_Holid;
                                for (int u = 1; u < 4; u++)
                                {
                                    H_Week_day = Week_Holid.AddDays(-u);
                                    if ((islocalweek(new PersianDate(H_Week_day).ToString("d")) == false) && (H_Week_day.DayOfWeek.ToString() != "Friday"))
                                    {
                                        maxs_Package[j, d, k, h] = maxs_Package[j, u - 1, k, h];
                                        break;
                                    }
                                }
                            }
                            if ((S_Week_day.DayOfWeek.ToString() == "Friday"))
                            {
                                DateTime F_Week_day = Week_Holid;
                                for (int u = 1; u < 5; u++)
                                {
                                    F_Week_day = Week_Holid.AddDays(-u);
                                    if ((islocalweek(new PersianDate(F_Week_day).ToString("d")) == false) && (F_Week_day.DayOfWeek.ToString() != "Friday"))
                                    {
                                        maxs_Package[j, d, k, h] = maxs_Package[j, u - 1, k, h];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[, ,] Nday = new int[Plants_Num, Units_Num, Hour];
            int[, ,] Nday_count = new int[Plants_Num, Units_Num, Hour];
            double[, ,] Nday_min = new double[Plants_Num, Units_Num, Hour]; 

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Nday[j, k, h] = 4 + plusold;
                        Nday_min[j, k, h] = maxs_Package[j, 0, k, h];
                        Nday_count[j, k, h] = Nday_count[j, k, h] + 1;
                        for (int r = 0; r < plusold; r++)
                        {
                            if (maxs_Package[j, 3 + r, k, h] < 0.98 * Price_UL[j, k, h])
                            {
                                if ((Nday_min[j, k, h] >= maxs_Package[j, 3 + r, k, h]) && (Nday_count[j, k, h] > 0))
                                {
                                    Nday[j, k, h] = 3 + r;
                                    Nday_min[j, k, h] = maxs_Package[j, 3 + r, k, h];
                                }
                                if (Nday_count[j, k, h] == 0)
                                {
                                    Nday[j, k, h] = 3 + r;
                                    Nday_min[j, k, h] = maxs_Package[j, 3 + r, k, h];
                                }
                                Nday_count[j, k, h] = Nday_count[j, k, h] + 1;
                            }
                        }
                    }
                }
            }
            int[, ,] Day_clear = new int[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Day_clear[j, k, h] = 3 + plusold ;
                        if (Nday[j, k, h] != 4+ plusold)
                        {
                            Day_clear[j, k, h] = 2+ plusold;
                        }
                    }
                }
            }
            //Base : 3 - 8
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price_Base = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int d = 0; d < OldDay_Strategy; d++)
                    {
                        for (int h = 2; h < Hour_Base; h++)
                        {
                            Old_price_Base[j, k, d, h] = maxs_Package[j, d, k, h];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_sum_Base = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 2; h < Hour_Base; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_sum_Base[j, k, h] = Old_price_sum_Base[j, k, h] + Old_price_Base[j, k, d, h];
                            }
                         }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_Ave_Base = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 2; h < Hour_Base; h++)
                    {
                        Old_price_Ave_Base[j, k, h] = Old_price_sum_Base[j, k, h] * Math.Pow(Day_clear[j, k, h], -1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price_diff_Base = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour_Base; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_diff_Base[j, k, d, h] = Old_price_Base[j, k, d, h] - Old_price_Ave_Base[j, k, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price_pow_Base = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_pow_Base[j, k, d, h] = Math.Pow(Old_price_diff_Base[j, k, d, h], 2);
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_pow_sum_Base = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 2; h < Hour_Base; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_pow_sum_Base[j, k, h] = Old_price_pow_sum_Base[j, k, h] + Old_price_pow_Base[j, k, d, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_pow_Ave_Base = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 2; h < Hour_Base; h++)
                    {
                        Old_price_pow_Ave_Base[j, k, h] = Old_price_pow_sum_Base[j, k, h] * Math.Pow(Day_clear[j, k, h], -1);
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Price_delta_Base = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 2; h < Hour_Base; h++)
                    {
                        Price_delta_Base[j, k, h] = Math.Pow(Old_price_pow_Ave_Base[j, k, h], 0.5);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Price_delta_sum_Base = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 2; h < Hour_Base; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Price_delta_sum_Base[j, k] = Math.Abs(Old_price_diff_Base[j, k, d, h]) + Price_delta_sum_Base[j, k];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Price_delta_Ave_Base = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 2; h < Hour_Base; h++)
                    {
                        Price_delta_Ave_Base[j, k] = Price_delta_sum_Base[j, k] * Math.Pow(Day_clear[j, k, h], -1) * Math.Pow((Hour_Base-2), -1);
                    }
                }
            }
            //Hour_Peak_N : 10 - 14
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int Hour_Peak_N = 5;
            double[, , ,] Old_price_Peak_N = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int d = 0; d < OldDay_Strategy; d++)
                    {
                        for (int h = (Hour_Base + 1); h < (Hour_Peak_N + Hour_Base+1); h++)
                        {
                            Old_price_Peak_N[j, k, d, h] = maxs_Package[j, d, k, h];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_sum_Peak_N = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_sum_Peak_N[j, k, h] = Old_price_sum_Peak_N[j, k, h] + Old_price_Peak_N[j, k, d, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_Ave_Peak_N = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_Ave_Peak_N[j, k, h] = Old_price_sum_Peak_N[j, k, h] * Math.Pow(Day_clear[j, k, h], -1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price_diff_Peak_N = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_diff_Peak_N[j, k, d, h] = Old_price_Peak_N[j, k, d, h] - Old_price_Ave_Peak_N[j, k, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price_pow_Peak_N = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_pow_Peak_N[j, k, d, h] = Math.Pow(Old_price_diff_Peak_N[j, k, d, h], 2);
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_pow_sum_Peak_N = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_pow_sum_Peak_N[j, k, h] = Old_price_pow_sum_Peak_N[j, k, h] + Old_price_pow_Peak_N[j, k, d, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_pow_Ave_Peak_N = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_pow_Ave_Peak_N[j, k, h] = Old_price_pow_sum_Peak_N[j, k, h] * Math.Pow(Day_clear[j, k, h], -1);
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Price_delta_Peak_N = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Price_delta_Peak_N[j, k, h] = Math.Pow(Old_price_pow_Ave_Peak_N[j, k, h], 0.5);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Price_delta_sum_Peak_N = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Price_delta_sum_Peak_N[j, k] = Math.Abs(Old_price_diff_Peak_N[j, k, d, h]) + Price_delta_sum_Peak_N[j, k];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Price_delta_Ave_Peak_N = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Price_delta_Ave_Peak_N[j, k] = Price_delta_sum_Peak_N[j, k] * Math.Pow(Day_clear[j, k, h], -1) * Math.Pow(Hour_Peak_N, -1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //Hour_Peak_A : 15 - 18
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int Hour_Peak_A = 4;
            double[, , ,] Old_price_Peak_A = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int d = 0; d < OldDay_Strategy; d++)
                    {
                        for (int h = (Hour_Peak_N + Hour_Base + 1); h < (Hour_Peak_A + Hour_Base + Hour_Peak_N); h++)
                        {
                            Old_price_Peak_A[j, k, d, h] = maxs_Package[j, d, k, h];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_sum_Peak_A = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_sum_Peak_A[j, k, h] = Old_price_sum_Peak_A[j, k, h] + Old_price_Peak_A[j, k, d, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_Ave_Peak_A = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_Ave_Peak_A[j, k, h] = Old_price_sum_Peak_A[j, k, h] * Math.Pow(Day_clear[j, k, h], -1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price_diff_Peak_A = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_diff_Peak_A[j, k, d, h] = Old_price_Peak_A[j, k, d, h] - Old_price_Ave_Peak_A[j, k, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price_pow_Peak_A = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_pow_Peak_A[j, k, d, h] = Math.Pow(Old_price_diff_Peak_A[j, k, d, h], 2);
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_pow_sum_Peak_A = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_pow_sum_Peak_A[j, k, h] = Old_price_pow_sum_Peak_A[j, k, h] + Old_price_pow_Peak_A[j, k, d, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_pow_Ave_Peak_A = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_pow_Ave_Peak_A[j, k, h] = Old_price_pow_sum_Peak_A[j, k, h] * Math.Pow(Day_clear[j, k, h], -1);
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Price_delta_Peak_A = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Price_delta_Peak_A[j, k, h] = Math.Pow(Old_price_pow_Ave_Peak_A[j, k, h], 0.5);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Price_delta_sum_Peak_A = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Price_delta_sum_Peak_A[j, k] = Math.Abs(Old_price_diff_Peak_A[j, k, d, h]) + Price_delta_sum_Peak_A[j, k];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Price_delta_Ave_Peak_A = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Price_delta_Ave_Peak_A[j, k] = Price_delta_sum_Peak_A[j, k] * Math.Pow(Day_clear[j, k, h], -1) * Math.Pow(Hour_Peak_A, -1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //Hour_Peak_NI : 19 - 23
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int Hour_Peak_NI = 5;
            double[, , ,] Old_price_Peak_NI = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int d = 0; d < OldDay_Strategy; d++)
                    {
                        for (int h = (Hour_Peak_N + Hour_Base + Hour_Peak_A + 1); h < (Hour_Peak_N + Hour_Base + Hour_Peak_A + Hour_Peak_NI + 1); h++)
                        {
                            Old_price_Peak_NI[j, k, d, h] = maxs_Package[j, d, k, h];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_sum_Peak_NI = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_sum_Peak_NI[j, k, h] = Old_price_sum_Peak_NI[j, k, h] + Old_price_Peak_NI[j, k, d, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_Ave_Peak_NI = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_Ave_Peak_NI[j, k, h] = Old_price_sum_Peak_NI[j, k, h] * Math.Pow(Day_clear[j, k, h], -1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price_diff_Peak_NI = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_diff_Peak_NI[j, k, d, h] = Old_price_Peak_NI[j, k, d, h] - Old_price_Ave_Peak_NI[j, k, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price_pow_Peak_NI = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_pow_Peak_NI[j, k, d, h] = Math.Pow(Old_price_diff_Peak_NI[j, k, d, h], 2);
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_pow_sum_Peak_NI = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_pow_sum_Peak_NI[j, k, h] = Old_price_pow_sum_Peak_NI[j, k, h] + Old_price_pow_Peak_NI[j, k, d, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_pow_Ave_Peak_NI = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_pow_Ave_Peak_NI[j, k, h] = Old_price_pow_sum_Peak_NI[j, k, h] * Math.Pow(Day_clear[j, k, h], -1);
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Price_delta_Peak_NI = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Price_delta_Peak_NI[j, k, h] = Math.Pow(Old_price_pow_Ave_Peak_NI[j, k, h], 0.5);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Price_delta_sum_Peak_NI = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Price_delta_sum_Peak_NI[j, k] = Math.Abs(Old_price_diff_Peak_NI[j, k, d, h]) + Price_delta_sum_Peak_NI[j, k];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Price_delta_Ave_Peak_NI = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Price_delta_Ave_Peak_NI[j, k] = Price_delta_sum_Peak_NI[j, k] * Math.Pow(Day_clear[j, k, h], -1) * Math.Pow(Hour_Peak_NI, -1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int Hour_night = 4;
            double[, , ,] Old_price_night = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int d = 0; d < OldDay_Strategy; d++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            if ((h == 0) || (h == 23) || (h == 1) || (h == 8))
                            {
                                Old_price_night[j, k, d, h] = maxs_Package[j, d, k, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_sum_night = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_sum_night[j, k, h] = Old_price_sum_night[j, k, h] + Old_price_night[j, k, d, h];
                            }

                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_Ave_night = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_Ave_night[j, k, h] = Old_price_sum_night[j, k, h] * Math.Pow(Day_clear[j, k, h], -1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price_diff_night = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_diff_night[j, k, d, h] = Old_price_night[j, k, d, h] - Old_price_Ave_night[j, k, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price_pow_night = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_pow_night[j, k, d, h] = Math.Pow(Old_price_diff_night[j, k, d, h], 2);
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_pow_sum_night = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_pow_sum_night[j, k, h] = 0;
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_pow_sum_night[j, k, h] = Old_price_pow_sum_night[j, k, h] + Old_price_pow_night[j, k, d, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_pow_Ave_night = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_pow_Ave_night[j, k, h] = Old_price_pow_sum_night[j, k, h] * Math.Pow(Day_clear[j, k, h], -1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Price_delta_night = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Price_delta_night[j, k, h] = Math.Pow(Old_price_pow_Ave_night[j, k, h], 0.5);
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Price_delta_sum_night = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Price_delta_sum_night[j, k] = Math.Abs(Old_price_diff_night[j, k, d, h]) + Price_delta_sum_night[j, k];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Price_delta_Ave_night = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Price_delta_Ave_night[j, k] = Price_delta_sum_night[j, k] * Math.Pow(Day_clear[j, k, h], -1) * Math.Pow(Hour_night, -1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            oldday = 2+ plusold;
            DateTime curDate = PersianDateConverter.ToGregorianDateTime(currentDate);
            DateTime bidDate = PersianDateConverter.ToGregorianDateTime(biddingDate);
            TimeSpan duration = bidDate - curDate;
            int DurationInt = duration.Days;

                        
            PersianDate t1 = new PersianDate(biddingDate);
            DateTime bidNow = PersianDateConverter.ToGregorianDateTime(t1);
            string dayofbidding = bidNow.DayOfWeek.ToString();

            ///////////////////////////////////////////////////////////////////////////////////////
    
           double[,] zaribweekend = new double[Plants_Num, 24];
           if ((dayofbidding == "Friday") && (IsWeekEnd==false))
            {
               DateTime  dtsat = PersianDateConverter.ToGregorianDateTime(biddingDate).AddDays(-8);
               DateTime  dtfri = PersianDateConverter.ToGregorianDateTime(biddingDate).AddDays(-7);
               if (islocalweek(new PersianDate(dtsat).ToString("d")))
               {
                   for (int h = 1; h < 30; h++)
                   {
                       DateTime sadtsat = dtsat;
                       sadtsat = sadtsat.AddDays(-h);
                       if ((islocalweek(new PersianDate(sadtsat).ToString("d")) == false) && (sadtsat.DayOfWeek.ToString() != "Friday"))
                       {
                           dtsat = sadtsat;
                           break;
                       }
                   }
               }
                double[,] v1 = MaxPrice(dtfri);
                double[,] v2 = MaxPrice(dtsat);
                if (check1(v1) == false || check1(v2) == false)
                {
                    for (int d = 7; d < 365; d+=7)
                    {
                        DateTime sastfriday = dtfri;
                        sastfriday = sastfriday.Subtract(new TimeSpan(d, 0, 0, 0));
                        double[,] v3 = MaxPrice(sastfriday);
                        if (check1(v3))
                        {
                            dtfri = sastfriday;
                            break;
                        }
                    }

                   v1 = MaxPrice(dtfri);
                   dtsat = dtfri.AddDays(-1);
                   if (islocalweek(new PersianDate(dtsat).ToString("d")))
                   {
                       for (int i = 2; i < 6; i++)
                       {
                           if ((islocalweek(new PersianDate(dtfri.AddDays(-i)).ToString("d")) == false) && (dtfri.AddDays(-i).DayOfWeek.ToString() !="Friday"))
                           {
                             
                               dtsat = dtfri.AddDays(-i);
                               break;
                           }

                       }

                   }
                    
                   v2 = MaxPrice(dtsat);
                }
               
                
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int h = 0; h < 24; h++)
                    {
                        try
                        {

                            zaribweekend[j, h] = v1[j, h] / v2[j, h];
                            if (zaribweekend[j, h] >= 1 || zaribweekend[j, h] == 0) zaribweekend[j, h] = 1;
                            if ((zaribweekend[j, h] > 1))
                            {
                                zaribweekend[j, h] = 1;
                            }
                            if (zaribweekend[j, h] == 0)
                            {
                                zaribweekend[j, h] = 1;
                            }
                        }
                        catch
                        {
                            zaribweekend[j, h] = 1;
                        }
                    }
                }
                for (int j = 0; j < Plants_Num; j++)
                {
                   
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if (setparam[j, 4])
                        {
                            if (k < packprenumber)
                            {
                                oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + preplant1 +
                                "'and TargetMarketDate='" + new PersianDate(dtfri).ToString("d") +
                                "'and Block='" + prenameblocks[0, k] + "'");
                            }
                            else
                            {
                                oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + preplant1 +
                                "'and TargetMarketDate='" + new PersianDate(dtfri).ToString("d") +
                                "'and Block='" + prenameblocks[0, packprenumber - 1] + "'");
                            }
                        }
                        else
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + plant[j] +
                            "'and TargetMarketDate='" + new PersianDate(dtfri).ToString("d") +
                            "'and Block='" + blocks[j, k] + "'");
                        }
                        int r1 = oDataTable.Rows.Count;
                        bool indexa = false;
                        if (r1 > 0)
                        {
                            foreach (DataRow m in oDataTable.Rows)
                            {
                                if (m["Required"].ToString().Trim() == "0")
                                {
                                    indexa = true;
                                    break;
                                }

                            }
                            if (indexa == false)
                            {
                                for (int h = 0; h < Hour; h++)
                                {
                                    History_Req_T[j, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                                    History_Dis_T[j, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                                }

                                break;
                            }
                        }
                    }
                    
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if (setparam[j, 4])
                        {
                            if (k < packprenumber)
                            {
                                oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + preplant1 +
                                "'and TargetMarketDate='" + new PersianDate(dtsat).ToString("d") +
                                "'and Block='" + prenameblocks[0, k] + "'");
                            }
                            else
                            {
                                oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + preplant1 +
                                "'and TargetMarketDate='" + new PersianDate(dtsat).ToString("d") +
                                "'and Block='" + prenameblocks[0, packprenumber - 1] + "'");
                            }
                        }
                        else
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + plant[j] +
                            "'and TargetMarketDate='" + new PersianDate(dtsat).ToString("d") +
                            "'and Block='" + blocks[j, k] + "'");
                        }

                        int r1 = oDataTable.Rows.Count;
                        bool indexb = false;
                        if (r1 > 0)
                        {
                            foreach (DataRow m in oDataTable.Rows)
                            {
                                if (m["Required"].ToString().Trim() == "0")
                                {
                                    indexb = true;
                                    break;
                                }

                            }
                            if (indexb == false)
                            {
                                for (int h = 0; h < Hour; h++)
                                {
                                    History_Req_Y[j, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                                    History_Dis_Y[j, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                                }

                                break;
                            }
                        }
                    }
                }
            
            }

            if (IsWeekEnd)
            {
                
                string nearend=nearestislocalweek(biddingDate);
                DateTime dtfri = PersianDateConverter.ToGregorianDateTime(nearend);
                DateTime dtsat = dtfri.AddDays(-1);
                for (int i = 1; i < 365; i++)
                {
                    if ((dtfri.AddDays(-i).DayOfWeek.ToString() != "Friday") && (islocalweek(new PersianDate(dtfri.AddDays(-i)).ToString("d")) == false))
                    {
                        dtsat = dtfri.AddDays(-i);
                        break;
                    }

                }
                  
                double[,] v1 = MaxPrice(dtfri);
                double[,] v2 = MaxPrice(dtsat);
                if (check1(v1) == false || check1(v2) == false)
                {
                    for (int d = 2; d < 365; d++)
                    {
                        DateTime d5 = dtfri;
                        d5 = d5.AddDays(-d);
                        if (check1(MaxPrice(d5)))
                        {

                            dtfri = d5;
                            break;
                        }

                      //string dater=nearestislocalweek(new PersianDate(dtfri).ToString("d"));
                       //DateTime sastfriday = dtfri;
                        //sastfriday = sastfriday.Subtract(new TimeSpan(d, 0, 0, 0));
                        //double[,] v3 = MaxPrice(PersianDateConverter.ToGregorianDateTime(dater));
                        //if (check1(v3))
                        //{
                        //    dtfri = d;
                        //    break;
                        //}
                    }

                    v1 = MaxPrice(dtfri);
                    dtsat = dtfri.AddDays(-1);
                    if (islocalweek(new PersianDate(dtsat).ToString("d")))
                    {
                        for (int i = 2; i < 6; i++)
                        {
                            if ((islocalweek(new PersianDate(dtfri.AddDays(-i)).ToString("d")) == false )&& (dtfri.AddDays(-i).DayOfWeek.ToString()!="Friday"))
                            {

                                dtsat = dtfri.AddDays(-i);
                                break;
                            }

                        }

                    }

                    v2 = MaxPrice(dtsat);
                }

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int h = 0; h < 24; h++)
                    {
                        try
                        {

                            zaribweekend[j, h] = v1[j, h] / v2[j, h];
                            if (zaribweekend[j, h] >= 1 || zaribweekend[j, h] == 0) zaribweekend[j, h] = 1;
                            if ((zaribweekend[j, h] < 0.98) || (zaribweekend[j, h] > 1))
                            {
                                zaribweekend[j, h] = 1;
                            }
                        }
                        catch
                        {
                            zaribweekend[j, h] = 1;
                        }
                        
                    }
                }
                for (int j = 0; j < Plants_Num; j++)
                {
                    
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if (setparam[j, 4])
                        {
                            if (k < packprenumber)
                            {
                                oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + preplant1 +
                                "'and TargetMarketDate='" + new PersianDate(dtfri).ToString("d") +
                                "'and Block='" + prenameblocks[0, k] + "'");
                            }
                            else
                            {
                                oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + preplant1 +
                                "'and TargetMarketDate='" + new PersianDate(dtfri).ToString("d") +
                                "'and Block='" + prenameblocks[0, packprenumber - 1] + "'");
                            }
                        }
                        else
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + plant[j] +
                            "'and TargetMarketDate='" + new PersianDate(dtfri).ToString("d") +
                            "'and Block='" + blocks[j, k] + "'");
                        }

                        int r1 = oDataTable.Rows.Count;
                        bool indexa = false;
                        if (r1 > 0)
                        {
                            foreach (DataRow m in oDataTable.Rows)
                            {
                                if (m["Required"].ToString().Trim() == "0")
                                {
                                    indexa = true;
                                    break;
                                }

                            }
                            if (indexa == false)
                            {
                                for (int h = 0; h < Hour; h++)
                                {
                                    History_Req_T[j, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                                    History_Dis_T[j, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                                }

                                break;
                            }
                        }
                    }
                   
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if (setparam[j, 4])
                        {
                            if (k < packprenumber)
                            {
                                oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + preplant1 +
                                "'and TargetMarketDate='" + new PersianDate(dtsat).ToString("d") +
                                "'and Block='" + prenameblocks[0, k] + "'");
                            }
                            else
                            {
                                oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + preplant1 +
                                "'and TargetMarketDate='" + new PersianDate(dtsat).ToString("d") +
                                "'and Block='" + prenameblocks[0, packprenumber - 1] + "'");
                            }
                        }
                        else
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + plant[j] +
                            "'and TargetMarketDate='" + new PersianDate(dtsat).ToString("d") +
                            "'and Block='" + blocks[j, k] + "'");
                        }

                        int r1 = oDataTable.Rows.Count;
                        bool indexb = false;
                        if (r1 > 0)
                        {
                            foreach (DataRow m in oDataTable.Rows)
                            {
                                if (m["Required"].ToString().Trim() == "0")
                                {
                                    indexb = true;
                                    break;
                                }

                            }
                            if (indexb == false)
                            {
                                for (int h = 0; h < Hour; h++)
                                {
                                    History_Req_Y[j, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                                    History_Dis_Y[j, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                                }

                                break;
                            }
                        }
                    }
                }

            }
            //------------------------------------tommorowholiprice-------------------------------------------------

            double[,] History_Req_tom = new double[Plants_Num, Hour];
            double[,] History_Dis_tom = new double[Plants_Num, Hour];
            double[,] History_Req_tomx = new double[Plants_Num, Hour];
            double[,] History_Dis_tomx = new double[Plants_Num, Hour];    
            double[,] zaribtomprice = new double[Plants_Num, 24];
            DateTime dtsattopri = PersianDateConverter.ToGregorianDateTime(biddingDate);
            DateTime dtfritopri = PersianDateConverter.ToGregorianDateTime(biddingDate);
            double[,] vs=null;
            double[,] vf=null;

                for (int h = 1; h < 365; h++)
                {
                    DateTime sadtsat = dtsattopri;
                    sadtsat = sadtsat.AddDays(-h);

                    double[,] v21 = MaxPrice(sadtsat);
                    if ((islocalweek(new PersianDate(sadtsat).ToString("d"))==false) && (sadtsat.DayOfWeek.ToString()== "Saturday") && check1(v21))
                    {

                        double[,] v11 = MaxPrice(sadtsat.AddDays(-2));
                        if ((islocalweek(new PersianDate(sadtsat.AddDays(-2)).ToString("d")) == false) && (sadtsat.AddDays(-2).DayOfWeek.ToString() == "Thursday") && check1(v11))
                        {
                            dtfritopri = sadtsat.AddDays(-2);
                            dtsattopri = sadtsat;
                            vs = v21;
                            vf = v11;
                            break;
                        }
                    }
                }
          

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < 24; h++)
                {
                    try
                    {

                        zaribtomprice[j, h] = vs[j, h] / vf[j, h];
                        if (zaribtomprice[j, h] >= 1 || zaribtomprice[j, h] == 0) zaribtomprice[j, h] = 1;
                        if ((zaribtomprice[j, h] > 1))
                        {
                            zaribtomprice[j, h] = 1;
                        }
                    }
                    catch
                    {
                        zaribtomprice[j, h] = 1;
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {

                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    if (setparam[j, 4])
                    {
                        if (k < packprenumber)
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + preplant1 +
                            "'and TargetMarketDate='" + new PersianDate(dtfritopri).ToString("d") +
                            "'and Block='" + prenameblocks[0, k] + "'");
                        }
                        else
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + preplant1 +
                            "'and TargetMarketDate='" + new PersianDate(dtfritopri).ToString("d") +
                            "'and Block='" + prenameblocks[0, packprenumber - 1] + "'");
                        }
                    }
                    else
                    {
                        oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + plant[j] +
                        "'and TargetMarketDate='" + new PersianDate(dtfritopri).ToString("d") +
                        "'and Block='" + blocks[j, k] + "'");
                    }
                    int r1 = oDataTable.Rows.Count;
                    bool indexa = false;
                    if (r1 > 0)
                    {
                        foreach (DataRow m in oDataTable.Rows)
                        {
                            if (m["Required"].ToString().Trim() == "0")
                            {
                                indexa = true;
                                break;
                            }

                        }
                        if (indexa == false)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                History_Req_tom[j, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                                History_Dis_tom[j, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                            }

                            break;
                        }
                    }
                }

                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    if (setparam[j, 4])
                    {
                        if (k < packprenumber)
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + preplant1 +
                            "'and TargetMarketDate='" + new PersianDate(dtsattopri).ToString("d") +
                            "'and Block='" + prenameblocks[0, k] + "'");
                        }
                        else
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + preplant1 +
                            "'and TargetMarketDate='" + new PersianDate(dtsattopri).ToString("d") +
                            "'and Block='" + prenameblocks[0, packprenumber - 1] + "'");
                        }
                    }
                    else
                    {
                        oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + plant[j] +
                        "'and TargetMarketDate='" + new PersianDate(dtsattopri).ToString("d") +
                        "'and Block='" + blocks[j, k] + "'");
                    }

                    int r1 = oDataTable.Rows.Count;
                    bool indexb = false;
                    if (r1 > 0)
                    {
                        foreach (DataRow m in oDataTable.Rows)
                        {
                            if (m["Required"].ToString().Trim() == "0")
                            {
                                indexb = true;
                                break;
                            }

                        }
                        if (indexb == false)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                History_Req_tomx[j, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                                History_Dis_tomx[j, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                            }

                            break;
                        }
                    }
                }
            }

            //------------------------------------------------------------------------------------------------------
            //------------------------------------------weekendfindprice-----------------------------------------------------------
            double[,] History_Req_week = new double[Plants_Num, Hour];
            double[,] History_Dis_week = new double[Plants_Num, Hour];
            double[,] History_Req_weekx = new double[Plants_Num, Hour];
            double[,] History_Dis_weekx = new double[Plants_Num, Hour];
            double[,] zaribwfinprice = new double[Plants_Num, 24];
            DateTime dtsatwepri = PersianDateConverter.ToGregorianDateTime(biddingDate);
            DateTime dtfriwepri = PersianDateConverter.ToGregorianDateTime(biddingDate);
            bool cantfind = true;
            double[,] vs1 = null;
            double[,] vf2 = null;

            for (int h = 1; h < 30; h++)
            {
                DateTime sadtsat = dtsatwepri;
                sadtsat = sadtsat.AddDays(-h);

              
                if (islocalweek(new PersianDate(sadtsat).ToString("d")))
                {
                    double[,] v1day = MaxPrice(sadtsat.AddDays(-1));
                    double[,] v2day = MaxPrice(sadtsat.AddDays(1));

                    if ((sadtsat.DayOfWeek.ToString() != "Friday") && (check1(v1day))
                    && (check1(v2day)) && (islocalweek(new PersianDate(sadtsat.AddDays(-1)).ToString("d")) == false) && (islocalweek(new PersianDate(sadtsat.AddDays(1)).ToString("d")) == false)
                    && (sadtsat.AddDays(-1).DayOfWeek.ToString() != "Friday") && (sadtsat.AddDays(1).DayOfWeek.ToString() != "Friday"))
                    {
                        dtsatwepri = sadtsat.AddDays(-1);
                        dtfriwepri = sadtsat.AddDays(1);
                        vs1 = v1day;
                        vf2 = v2day;
                        cantfind = false;
                        break;
                    }
                }
            }


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < 24; h++)
                {
                    try
                    {
                        if (cantfind==false)
                        {

                            zaribwfinprice[j, h] = vs1[j, h] / vf2[j, h];
                            if (zaribwfinprice[j, h] >= 1 || zaribwfinprice[j, h] == 0) zaribwfinprice[j, h] = 1;
                            if ((zaribwfinprice[j, h] > 1))
                            {
                                zaribwfinprice[j, h] = 1;
                            }
                        }
                        else zaribwfinprice[j, h] = zaribtomprice[j, h];
                    }
                    catch
                    {
                        zaribwfinprice[j, h] = 1;
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {

                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    if (setparam[j, 4])
                    {
                        if (k < packprenumber)
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + preplant1 +
                            "'and TargetMarketDate='" + new PersianDate(dtfriwepri).ToString("d") +
                            "'and Block='" + prenameblocks[0, k] + "'");
                        }
                        else
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + preplant1 +
                            "'and TargetMarketDate='" + new PersianDate(dtfriwepri).ToString("d") +
                            "'and Block='" + prenameblocks[0, packprenumber - 1] + "'");
                        }
                    }
                    else
                    {
                        oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + plant[j] +
                        "'and TargetMarketDate='" + new PersianDate(dtfriwepri).ToString("d") +
                        "'and Block='" + blocks[j, k] + "'");
                    }
                    int r1 = oDataTable.Rows.Count;
                    bool indexa = false;
                    if (r1 > 0)
                    {
                        foreach (DataRow m in oDataTable.Rows)
                        {
                            if (m["Required"].ToString().Trim() == "0")
                            {
                                indexa = true;
                                break;
                            }

                        }
                        if (indexa == false)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                if (cantfind == false)
                                {

                                    History_Req_week[j, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                                    History_Dis_week[j, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                                }
                                else
                                {
                                    History_Req_week[j, h] = History_Req_tom[j, h];
                                    History_Dis_week[j, h] = History_Dis_tom[j, h];
                                }
                            }

                            break;
                        }
                    }
                }

                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    if (setparam[j, 4])
                    {
                        if (k < packprenumber)
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from "+tname005+" where PPID='" + preplant1 +
                            "'and TargetMarketDate='" + new PersianDate(dtsatwepri).ToString("d") +
                            "'and Block='" + prenameblocks[0, k] + "'");
                        }
                        else
                        {
                            oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from " + tname005 + " where PPID='" + preplant1 +
                            "'and TargetMarketDate='" + new PersianDate(dtsatwepri).ToString("d") +
                            "'and Block='" + prenameblocks[0, packprenumber - 1] + "'");
                        }
                    }
                    else
                    {
                        oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from " + tname005 + " where PPID='" + plant[j] +
                        "'and TargetMarketDate='" + new PersianDate(dtsatwepri).ToString("d") +
                        "'and Block='" + blocks[j, k] + "'");
                    }

                    int r1 = oDataTable.Rows.Count;
                    bool indexb = false;
                    if (r1 > 0)
                    {
                        foreach (DataRow m in oDataTable.Rows)
                        {
                            if (m["Required"].ToString().Trim() == "0")
                            {
                                indexb = true;
                                break;
                            }

                        }
                        if (indexb == false)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                if (cantfind == false)
                                {
                                    History_Req_weekx[j, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                                    History_Dis_weekx[j, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                                }
                                else
                                {
                                    History_Req_weekx[j, h] = History_Req_tomx[j, h];
                                    History_Dis_weekx[j, h] = History_Dis_tomx[j, h];
                                }
                            }

                            break;
                        }
                    }
                }
            }



            //-----------------------------------------------------------------------------------------------------------
            //-------------------------------------acceptavg tommarow----------------------------------------------------------
      
            double[] zaribavgtom = new double[Hour];
            DateTime dtsattoavg = PersianDateConverter.ToGregorianDateTime(biddingDate);
            DateTime dtfritoavg = PersianDateConverter.ToGregorianDateTime(biddingDate);

            DataTable acavgfrida1 = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(dtfritoavg).ToString("d") + "'");
            DataTable acavgsat1 = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(dtsattoavg).ToString("d") + "'");
                          

            for (int h = 1; h < 365; h++)
            {
                DateTime sadtsat = dtsattoavg;
                sadtsat = sadtsat.AddDays(-h);

               
                acavgsat1 = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(sadtsat).ToString("d") + "'");
                          

                if ((islocalweek(new PersianDate(sadtsat).ToString("d")) == false) && (sadtsat.DayOfWeek.ToString() == "Saturday") && (acavgsat1.Rows.Count==24))
                {

                    acavgfrida1 = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(sadtsat.AddDays(-2)).ToString("d") + "'");
                    if ((islocalweek(new PersianDate(sadtsat.AddDays(-2)).ToString("d")) == false) && (sadtsat.AddDays(-2).DayOfWeek.ToString() == "Thursday") && (acavgfrida1.Rows.Count==24))
                    {
                        dtfritoavg = sadtsat.AddDays(-2);
                        dtsattoavg = sadtsat;
                       
                        break;
                    }
                }
            }
            for (int h = 0; h < 24; h++)
            {
                if (acavgfrida1.Rows.Count > 0 && acavgsat1.Rows.Count > 0)
                {
                    zaribavgtom[h] = (MyDoubleParse(acavgfrida1.Rows[h][0].ToString()) / MyDoubleParse(acavgsat1.Rows[h][0].ToString()));
                    if (zaribavgtom[h] >= 1 || zaribavgtom[h] == 0 || (zaribavgtom[h] > 1)) zaribavgtom[h] = 1;

                }
                else
                {
                    zaribavgtom[h] = 1;
                }
            }


            //-----------------------------------------acceptavg find weekend------------------------------------------------
            double[] zaribwfinavg = new double[Hour];
            DateTime dtsatweavg = PersianDateConverter.ToGregorianDateTime(biddingDate);
            DateTime dtfriweavg = PersianDateConverter.ToGregorianDateTime(biddingDate);
            bool cannotfindavg = true;
            DataTable acavgfrida2 = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(dtfriweavg).ToString("d") + "'");
            DataTable acavgsat2 = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(dtsatweavg).ToString("d") + "'");
                 

            for (int h = 1; h < 30; h++)
            {
                DateTime sadtsat = dtsatwepri;
                sadtsat = sadtsat.AddDays(-h);

                acavgsat2 = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(sadtsat).ToString("d") + "'");
                acavgfrida2 = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(sadtsat.AddDays(-2)).ToString("d") + "'");

                if (islocalweek(new PersianDate(sadtsat).ToString("d")))
                {
                    if ((sadtsat.DayOfWeek.ToString() != "Friday") && (acavgsat2.Rows.Count == 24)
                     && (acavgfrida2.Rows.Count == 24) && (islocalweek(new PersianDate(sadtsat.AddDays(-1)).ToString("d")) == false) && (islocalweek(new PersianDate(sadtsat.AddDays(1)).ToString("d")) == false)
                     && (sadtsat.AddDays(-1).DayOfWeek.ToString() != "Friday") && (sadtsat.AddDays(1).DayOfWeek.ToString() != "Friday"))
                    {
                        dtsatweavg = sadtsat.AddDays(-1);
                        dtfriweavg = sadtsat.AddDays(1);
                        cannotfindavg = false;
                        break;

                    }
                }
            }
            for (int h = 0; h < 24; h++)
            {
                if (acavgfrida2.Rows.Count > 0 && acavgsat2.Rows.Count > 0)
                {
                    if (cannotfindavg == false)
                    {
                        zaribwfinavg[h] = (MyDoubleParse(acavgfrida2.Rows[h][0].ToString()) / MyDoubleParse(acavgsat2.Rows[h][0].ToString()));
                        if (zaribwfinavg[h] >= 1 || zaribwfinavg[h] == 0 || (zaribwfinavg[h] > 1)) zaribwfinavg[h] = 1;
                    }
                    else
                        zaribwfinavg[h] = zaribavgtom[h];
                }
                else
                {
                      zaribwfinavg[h] = 1;
                }
            }
            //-------------------------------------------------------------------------------------------------------------
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < 24; h++)
                {
                    if ((History_Req_tom[j, h] != 0) && (History_Dis_tom[j, h] != 0) && (History_Req_tomx[j, h] != 0) && (History_Dis_tomx[j, h] != 0))
                    {
                        if ((History_Req_tomx[j, h] / History_Dis_tomx[j, h]) > 0.8)
                        {
                            if ((History_Req_tomx[j, h] / History_Dis_tomx[j, h]) == 1)
                            {
                                if (zaribtomprice[j, h] > zaribavgtom[h])
                                {
                                    if ((History_Req_tom[j, h] / History_Dis_tom[j, h]) > 0.8)
                                    {
                                        zaribtomprice[j, h] = zaribtomprice[j, h];
                                    }
                                    else
                                    {
                                        zaribtomprice[j, h] = (zaribtomprice[j, h] + zaribavgtom[h]) * 0.5;
                                    }
                                }
                                else
                                {
                                    zaribtomprice[j, h] = (zaribtomprice[j, h] + zaribavgtom[h]) * 0.5;
                                }
                            }
                            else
                            {
                                if (zaribtomprice[j, h] > zaribavgtom[h])
                                {
                                    zaribtomprice[j, h] = (zaribtomprice[j, h] + zaribavgtom[h]) * 0.5;
                                }
                                else
                                {
                                    zaribtomprice[j, h] = zaribtomprice[j, h];
                                }
                            }
                        }
                        else
                        {
                            if (zaribtomprice[j, h] > zaribavgtom[h])
                            {
                                zaribtomprice[j, h] = zaribavgtom[h];
                            }
                            else
                            {
                                zaribtomprice[j, h] = 0.97 * zaribtomprice[j, h];
                            }
                        }
                        if (zaribtomprice[j, h] == 0)
                        {
                            zaribtomprice[j, h] = 1;
                        }
                    }
                    else
                    {
                        zaribtomprice[j, h] = zaribavgtom[h];
                    }
                }
            }
            double[,] Zaribsaturday = new double[Plants_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < 24; h++)
                {
                    Zaribsaturday[j, h] = zaribtomprice[j, h];
                    if ((Zaribsaturday[j, h] < 0.98) && (h < 9))
                    {
                        Zaribsaturday[j, h] = 0.98;
                    }
                    if ((Zaribsaturday[j, h] < 0.98))
                    {
                        Zaribsaturday[j, h] = 1;
                    }
                    if ((Zaribsaturday[j, h] > 1))
                    {
                        Zaribsaturday[j, h] = 1;
                    }
                }
            }
            //---------------------------------accepted average ----------------------------------------
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < 24; h++)
                {
                    if ((History_Req_week[j, h] != 0) && (History_Dis_week[j, h] != 0) && (History_Req_weekx[j, h] != 0) && (History_Dis_weekx[j, h] != 0))
                    {
                        if ((History_Req_weekx[j, h] / History_Dis_weekx[j, h]) > 0.8)
                        {
                            if ((History_Req_weekx[j, h] / History_Dis_weekx[j, h]) == 1)
                            {
                                if (zaribtomprice[j, h] > zaribwfinavg[h])
                                {
                                    if ((History_Req_tom[j, h] / History_Dis_tom[j, h]) > 0.8)
                                    {
                                        zaribwfinprice[j, h] = zaribwfinprice[j, h];
                                    }
                                    else
                                    {
                                        zaribwfinprice[j, h] = (zaribwfinprice[j, h] + zaribwfinavg[h]) * 0.5;
                                    }
                                }
                                else
                                {
                                    zaribwfinprice[j, h] = (zaribwfinprice[j, h] + zaribwfinavg[h]) * 0.5;
                                }
                            }
                            else
                            {
                                if (zaribwfinprice[j, h] > zaribwfinavg[h])
                                {
                                    zaribwfinprice[j, h] = (zaribwfinprice[j, h] + zaribwfinavg[h]) * 0.5;
                                }
                                else
                                {
                                    zaribwfinprice[j, h] = zaribwfinprice[j, h];
                                }
                            }
                        }
                        else
                        {
                            if (zaribwfinprice[j, h] > zaribwfinavg[h])
                            {
                                zaribwfinprice[j, h] = zaribwfinavg[h];
                            }
                            else
                            {
                                zaribwfinprice[j, h] = 0.97 * zaribwfinprice[j, h];
                            }
                        }
                        if (zaribwfinprice[j, h] == 0)
                        {
                            zaribwfinprice[j, h] = 1;
                        }
                    }
                    else
                    {
                        zaribwfinprice[j, h] = zaribwfinavg[h];
                    }
                }
            }
            double[,] ZaribafterHoliday = new double[Plants_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < 24; h++)
                {
                    ZaribafterHoliday[j, h] = zaribwfinprice[j, h];
                    if ((ZaribafterHoliday[j, h] < 0.975) && (h < 9))
                    {
                        ZaribafterHoliday[j, h] = 0.975;
                    }
                    else if (ZaribafterHoliday[j, h] < 0.985)
                    {
                        ZaribafterHoliday[j, h] = 0.985;
                    }
                    if ((ZaribafterHoliday[j, h] < 0.98))
                    {
                        ZaribafterHoliday[j, h] = 1;
                    }
                    if ((ZaribafterHoliday[j, h] > 1))
                    {
                        ZaribafterHoliday[j, h] = 1;
                    }
                }
            }
            //-------------------------------------------------------------------------------------------------------------
            double[] zaribavgprice = new double[Hour];
            if ((dayofbidding == "Friday")&& (IsWeekEnd==false))
            {
                DateTime dtsat = PersianDateConverter.ToGregorianDateTime(biddingDate).AddDays(-8);

                DateTime dtfri = PersianDateConverter.ToGregorianDateTime(biddingDate).AddDays(-7);
                
                
                DataTable acavgfrida = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(dtfri).ToString("d") + "'");
                DataTable acavgsat = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(dtsat).ToString("d") + "'");
                 

                    if ((islocalweek(new PersianDate(dtsat).ToString("d"))) && (acavgfrida.Rows.Count == 24) && (acavgsat.Rows.Count ==24))
                    {
                        for (int h = 1; h < 30; h++)
                        {
                            DateTime sadsat = dtsat;
                            sadsat = sadsat.AddDays(-h);
                            if ((islocalweek(new PersianDate(sadsat).ToString("d")) == false) && (sadsat.DayOfWeek.ToString()!="Friday"))
                            {
                                dtsat = sadsat;
                                break;
                            }
                        }
                    }
               

                if (acavgfrida.Rows.Count >0 || acavgsat.Rows.Count >0)
                {

                    for (int d = 7; d < 365; d += 7)
                    {
                        DateTime sadtfri = dtfri;
                        sadtfri = sadtfri.Subtract(new TimeSpan(d, 0, 0, 0));
                        acavgfrida = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(sadtfri).ToString("d") + "'");

                        if (acavgfrida.Rows.Count == 24)
                        {
                            dtfri = sadtfri;
                            break;
                        }
                    }


                    dtsat = dtfri.AddDays(-1);
                    acavgsat = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(dtsat).ToString("d") + "'");
                    if (acavgsat.Rows.Count == 0)
                    {
                        for (int i = 1; i < 365; i++)
                        {
                            DateTime sam = dtsat;
                            sam = sam.AddDays(-1);
                            acavgsat = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(sam).ToString("d") + "'");
                            if ((acavgsat.Rows.Count == 24) && (islocalweek(new PersianDate(sam).ToString("d")) == false) && (sam.DayOfWeek.ToString() != "Friday"))
                            {
                                dtsat = sam;
                                break;
                            }

                        }

                    }

                    if (islocalweek(new PersianDate(dtsat).ToString("d")))
                    {
                        for (int i = 2; i < 6; i++)
                        {
                            if ((islocalweek(new PersianDate(dtfri.AddDays(-i)).ToString("d")) == false) && (dtfri.AddDays(-i).DayOfWeek.ToString() != "Friday"))
                            {
                                dtsat = dtfri.AddDays(-i);
                                acavgsat = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(dtsat).ToString("d") + "'");
                                break;
                            }
                        }
                    }
                    else
                        acavgsat = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(dtsat).ToString("d") + "'");
                }
               
                //------------------------------------------------------------------
                for (int h = 0; h < 24; h++)
                {
                    if (acavgfrida.Rows.Count > 0 && acavgsat.Rows.Count > 0)
                    {
                        zaribavgprice[h] = (MyDoubleParse(acavgfrida.Rows[h][0].ToString()) / MyDoubleParse(acavgsat.Rows[h][0].ToString()));
                        if (zaribavgprice[h] >= 1 || zaribavgprice[h] == 0  || (zaribavgprice[h] > 1)) zaribavgprice[h] = 1;
                            
                    }
                    else
                        zaribavgprice[h] = 1;
                }

                
            }
            if (IsWeekEnd )
            {
                string nearend = nearestislocalweekzaribavg(biddingDate);
                DateTime dtfri = PersianDateConverter.ToGregorianDateTime(nearend);
                DateTime dtsat = dtfri.AddDays(-1);
                for (int i = 1; i < 365; i++)
                {
                    if ((dtfri.AddDays(-i).DayOfWeek.ToString() != "Friday") && (islocalweek(new PersianDate(dtfri.AddDays(-i)).ToString("d"))==false))
                    {
                        dtsat = dtfri.AddDays(-i);
                        break;
                    }

                }


                DataTable acavgfrida = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(dtfri).ToString("d") + "'");
                DataTable acavgsat = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(dtsat).ToString("d") + "'");
                if (acavgsat.Rows.Count == 0)
                {

                    for (int i = 1; i < 365; i++)
                    {
                        DateTime ddavg = dtsat;
                        ddavg = ddavg.AddDays(-i);
                        acavgsat = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(ddavg).ToString("d") + "'");
                        if ((acavgsat.Rows.Count==24) && (islocalweek(new PersianDate(ddavg).ToString("d"))) && (ddavg.DayOfWeek.ToString() != "Friday"))
                        {
                            dtsat = ddavg;
                            break;
                        }
                    }

                }

                acavgsat = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(dtsat).ToString("d") + "'");
                for (int h = 0; h < 24; h++)
                {
                    if (acavgfrida.Rows.Count > 0 && acavgsat.Rows.Count > 0)
                    {
                        zaribavgprice[h] = (MyDoubleParse(acavgfrida.Rows[h][0].ToString()) / MyDoubleParse(acavgsat.Rows[h][0].ToString()));
                        if (zaribavgprice[h] >= 1 || zaribavgprice[h] == 0  || (zaribavgprice[h] > 1)) zaribavgprice[h] = 1;
                    }
                    else
                        zaribavgprice[h] = 1;
                }
            }
            //---------------------------------------------------------------------------------
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < 24; h++)
                {
                    if ((History_Req_Y[j, h] != 0) && (History_Dis_Y[j, h] != 0) && (History_Req_T[j, h] != 0) && (History_Dis_T[j, h] != 0))
                    {
                        if ((History_Req_T[j, h] / History_Dis_T[j, h]) > 0.8)
                        {
                            if ((History_Req_T[j, h] / History_Dis_T[j, h]) == 1)
                            {
                                if (zaribweekend[j, h] > zaribavgprice[h])
                                {
                                    if ((History_Req_Y[j, h] / History_Dis_Y[j, h]) > 0.8)
                                    {
                                        zaribweekend[j, h] = zaribweekend[j, h];
                                    }
                                    else
                                    {
                                        zaribweekend[j, h] = (zaribweekend[j, h] + zaribavgprice[h]) * 0.5;
                                    }
                                }
                                else
                                {
                                    zaribweekend[j, h] = (zaribweekend[j, h] + zaribavgprice[h]) * 0.5;
                                }
                            }
                            else
                            {
                                if (zaribweekend[j, h] > zaribavgprice[h])
                                {
                                    zaribweekend[j, h] = (zaribweekend[j, h] + zaribavgprice[h]) * 0.5;
                                }
                                else
                                {
                                    zaribweekend[j, h] = zaribweekend[j, h];
                                }
                            }
                        }
                        else
                        {
                            if (zaribweekend[j, h] > zaribavgprice[h])
                            {
                                zaribweekend[j, h] = zaribavgprice[h];
                            }
                            else
                            {
                                zaribweekend[j, h] = 0.97 * zaribweekend[j, h];
                            }
                        }
                        if (zaribweekend[j, h] == 0)
                        {
                            zaribweekend[j, h] = 1;
                        }
                    }
                    else
                    {
                        zaribweekend[j, h] = zaribavgprice[h];
                    }
                }
            }


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ zaribaverageprice~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[] zaribaverageprice = new double[24];
            double[] zaribavyes = new double[24];
            double[] zaribav2yes = new double[24];
            double[] zaribavnearest = new double[24];

            string bidyesteday = new PersianDate(PersianDateConverter.ToGregorianDateTime(biddingDate).AddDays(-1)).ToString("d");
            string bidyesteday2 = new PersianDate(PersianDateConverter.ToGregorianDateTime(biddingDate).AddDays(-2)).ToString("d");

            bool yesterdayfriday = false;
            bool yester2friday = false;
            if (PersianDateConverter.ToGregorianDateTime(bidyesteday).DayOfWeek.ToString() == "Friday") yesterdayfriday = true;
            if (PersianDateConverter.ToGregorianDateTime(bidyesteday2).DayOfWeek.ToString() == "Friday") yester2friday = true;
            string nearestweekend = biddingDate;
            for (int d = 2; d < 365; d++)
            {
                string date=new PersianDate(PersianDateConverter.ToGregorianDateTime(biddingDate).AddDays(-d)).ToString("d");
                if (islocalweek(date) == false)
                    nearestweekend = date;
                break;
            }
            

            DataTable dtavnearest = Utilities.GetTable("select *  from AveragePrice where Date='"+nearestweekend+"'");                     
            DataTable dtavyesterday = Utilities.GetTable("select  *  from AveragePrice where Date='" + bidyesteday + "'");
            DataTable dtavyesterday2 = Utilities.GetTable("select *  from AveragePrice where Date='" + bidyesteday2 + "'");
            
            foreach (DataRow m in dtavyesterday.Rows)
            {
                int hour = int.Parse(m["Hour"].ToString().Trim());
                double value = MyDoubleParse(m["AcceptedAverage"].ToString());
                zaribavyes[hour-1] = value;
            }
            foreach (DataRow m in dtavyesterday2.Rows)
            {
                int hour = int.Parse(m["Hour"].ToString().Trim());
                double value = MyDoubleParse(m["AcceptedAverage"].ToString());
                zaribav2yes[hour-1] = value;
            }
            foreach (DataRow m in dtavnearest.Rows)
            {
                int hour = int.Parse(m["Hour"].ToString().Trim());
                double value = MyDoubleParse(m["AcceptedAverage"].ToString());
                zaribavnearest[hour-1] = value;
            }

            for (int h = 0; h < 24; h++)
            {
                try
                {
                    if (yesterdayfriday || yeterdayweekend || zaribavyes[h] == 0)
                    {
                            zaribaverageprice[h] = 1;
                    }
                    else if (two_yeterdayweekend || yester2friday)
                    {
                        if (zaribavyes[h] != 0 && zaribavnearest[h] != 0)
                        {
                            zaribaverageprice[h] = zaribavyes[h] / zaribavnearest[h];
                        }
                        else
                            zaribaverageprice[h] = 1;

                    }
                    else
                    {
                        if (zaribavyes[h] != 0 && zaribav2yes[h] != 0)
                        {
                            zaribaverageprice[h] = zaribavyes[h] / zaribav2yes[h];
                        }
                        else
                            zaribaverageprice[h] = 1;
                    }
                }
                catch
                {
                    zaribaverageprice[h] = 1;
                }
                if (zaribaverageprice[h] > 1)
                {
                    zaribaverageprice[h] = 1;
                }

            }
           
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~zaribaid~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] zaribaid = new double[Plants_Num,24];
            double[, ,] zariMarket = new double[Plants_Num, Units_Num, 24];
            double[,] zaribbidyes = MaxPrice(PersianDateConverter.ToGregorianDateTime(bidyesteday));
            double[,] zaribid2yes = MaxPrice(PersianDateConverter.ToGregorianDateTime(bidyesteday2));
            double[,] zaribbidnearest = MaxPrice(PersianDateConverter.ToGregorianDateTime(nearestweekend));


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < 24; h++)
                {
                    try
                    {
                        if (yesterdayfriday || yeterdayweekend || zaribbidyes[j, h] == 0)
                        {
                            zaribaid[j, h] = 1;
                        }
                        if (two_yeterdayweekend || yester2friday)
                        {
                            if (zaribbidyes[j, h] != 0 && zaribbidnearest[j, h] != 0)
                            {
                                zaribaid[j, h] = zaribbidyes[j, h] / zaribbidnearest[j, h];
                            }
                            else
                                zaribaid[j, h] = 1;

                        }
                        else
                        {
                            if (zaribbidyes[j, h] != 0 && zaribid2yes[j, h] != 0)
                            {
                                zaribaid[j, h] = zaribbidyes[j, h] / zaribid2yes[j, h];
                            }
                            else
                                zaribaid[j, h] = 1;
                        }
                    }
                    catch
                    {
                        zaribaid[j, h] = 1;
                    }
                     if (zaribaid[j, h] > 1)
                    {
                        zaribaid[j, h] = 1;
                    }
                }

            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~historydate~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\
            double[, ,] History_Date_req_y = new double[Plants_Num, Units_Num, 24];
            double[, ,] History_Date_req_2y = new double[Plants_Num, Units_Num, 24];
            double[, ,] History_Date_dispatch_y = new double[Plants_Num, Units_Num, 24];
            double[, ,] History_Date_dispatch_2y = new double[Plants_Num, Units_Num, 24];
            

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {                   
                      for (int h = 0; h < 24; h++)
                      {
                          DataTable oTableyester = Utilities.GetTable("select Required,Dispatchable  from " + tname005 + " where PPID='" + plant[j] +
                         "'and TargetMarketDate='" + bidyesteday +
                         "'and Block='" + blocks[j, i] + "'and Hour='" + (h+1) + "'");
                          DataTable oTableyester2 = Utilities.GetTable("select Required,Dispatchable  from " + tname005 + " where PPID='" + plant[j] +
                          "'and TargetMarketDate='" + nearestweekend +
                          "'and Block='" + blocks[j, i] + "'and Hour='" + (h+1) + "'");

                          if (oTableyester.Rows.Count > 0)
                          {
                              History_Date_req_y[j, i, h] = MyDoubleParse(oTableyester.Rows[0]["Required"].ToString());

                              History_Date_dispatch_y[j, i, h] = MyDoubleParse(oTableyester.Rows[0]["Dispatchable"].ToString());
                          }
                         
                          if (oTableyester2.Rows.Count > 0)
                          {
                              History_Date_req_2y[j, i, h] = MyDoubleParse(oTableyester2.Rows[0]["Required"].ToString());

                              History_Date_dispatch_2y[j, i, h] = MyDoubleParse(oTableyester2.Rows[0]["Dispatchable"].ToString());
                          }
                      }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < 24; h++)
                    {
                        zariMarket[j, i, h] = 1;
                        if ((History_Date_req_y[j, i, h] != History_Date_dispatch_y[j, i, h]) && (History_Date_req_y[j, i, h] != 0))
                        {
                            if (zaribaid[j, h] > zaribaverageprice[h])
                            {
                                zariMarket[j, i, h] = (zaribaid[j, h] + zaribaverageprice[h]) * 0.5;
                            }
                        }
                        if ((h > 0) && (h < 8))
                        {
                            if (zariMarket[j, i, h] < 0.97)
                            {
                                zariMarket[j, i, h] = 0.97;
                            }
                        }
                        else
                        {
                            if (zariMarket[j, i, h] < 0.985)
                            {
                                zariMarket[j, i, h] = 0.985;
                            }
                        }

                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            Old_price[j, k, d, h] = maxs_Package[j, d, k, h];

                            if (Old_price[j, k, d, h] == 0)
                            {
                                if ((d > 1) && (Old_price[j, k, d-1, h]!=0))
                                {
                                    Old_price[j, k, d, h] = Old_price[j, k, d-1, h];
                                }
                                else if ((d < OldDay_Strategy-1) && (Old_price[j, k, d + 1, h] != 0))
                                {
                                    Old_price[j, k, d, h] = Old_price[j, k, d - 1, h];
                                }
                            }
                            
                            if (useInitial.GetInstance().IsInitial)
                            {
                                Old_price[j, k, d, h] = defaultprce[j, h];
                            }
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_sum = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_sum[j, k, h] = Old_price_sum[j, k, h] + Old_price[j, k, d, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_Ave = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_Ave[j, k, h] = Old_price_sum[j, k, h] * Math.Pow(Day_clear[j, k, h], -1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price_diff = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_diff[j, k, d, h] = Old_price[j, k, d, h] - Old_price_Ave[j, k, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price_pow = new double[Plants_Num, Units_Num, OldDay_Strategy, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_pow[j, k, d, h] = Math.Pow(Old_price_diff[j, k, d, h], 2);
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_pow_sum = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Old_price_pow_sum[j, k, h] = Old_price_pow_sum[j, k, h] + Old_price_pow[j, k, d, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_pow_Ave = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_pow_Ave[j, k, h] = Old_price_pow_sum[j, k, h] * Math.Pow(Day_clear[j, k, h], -1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_delta = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                       Old_price_delta[j, k, h] = Math.Pow(Old_price_pow_Ave[j, k, h], 0.5);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] price_diff = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        price_diff[j, k, h] = Math.Abs(Old_price_Ave[j, k, h] - Old_price[j, k, 0, h]);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Price_delta = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Price_delta[j, k, h] = Old_price_delta[j, k, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Price_delta_sum = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int d = 0; d < OldDay_Strategy; d++)
                        {
                            if (Nday[j, k, h] != d)
                            {
                                Price_delta_sum[j, k] = Math.Abs(Old_price_diff[j, k, d, h]) + Price_delta_sum[j, k];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Price_delta_Ave = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Price_delta_Ave[j, k] = Price_delta_sum[j, k] * Math.Pow(Day_clear[j, k, h], -1) * Math.Pow(Hour, -1);
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Select_Hour_Cap = new string[Hour, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Old_price_Ave[j, k, h] > (Factor_Price * CapPrice[0])))
                        {
                            Select_Hour_Cap[j, k, h] = "Yes";
                        }
                        else
                        {
                            Select_Hour_Cap[j, k, h] = "NO";
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Hour_State_User = new string[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Hour_State_User[j, k, h] = "Medium";
                        if (Run_State_user[j, k] == "Automatic")
                        {
                            Hour_State_User[j, k, h] = Hour_State_Automatic[j, k, h];
                        }
                        if (Run_State_user[j, k] == "Customize" && Hour_State_customize[j, k, h] != "")
                        {
                            Hour_State_User[j, k, h] = Hour_State_customize[j, k, h];
                        }
                    }
                }
            }


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        {
                            if (h > 1 && h < Hour_Base)
                            {
                                if ((1.5 * Price_delta_Ave_Base[j, k] < Price_delta[j, k, h]))
                                {
                                    Price_delta[j, k, h] = 1.5 * Price_delta_Ave_Base[j, k];
                                }
                                if ((Price_delta_Ave_Base[j, k] > Price_delta[j, k, h]))
                                {
                                    Price_delta[j, k, h] = Price_delta_Ave_Base[j, k];
                                }
                            }
                            if ((h >= (Hour_Base + 1)) && (h < (Hour_Base + Hour_Peak_N + 1)))
                            {
                                if ((1.5* Price_delta_Ave_Peak_N[j, k] < Price_delta[j, k, h]))
                                {
                                    Price_delta[j, k, h] = 1.5 * Price_delta_Ave_Peak_N[j, k];
                                }
                                if ((0.5 * Price_delta_Ave_Peak_N[j, k] > Price_delta[j, k, h]))
                                {
                                    Price_delta[j, k, h] = 0.5 * Price_delta_Ave_Peak_N[j, k];
                                }
                            }
                            if ((h >= Hour_Base + Hour_Peak_N + 1) && (h < (Hour_Base + Hour_Peak_N + Hour_Peak_A + 1)))
                            {
                                if ((1.5 * Price_delta_Ave_Peak_A[j, k] < Price_delta[j, k, h]))
                                {
                                    Price_delta[j, k, h] = 1.5 * Price_delta_Ave_Peak_A[j, k];
                                }
                                if ((0.5 * Price_delta_Ave_Peak_A[j, k] > Price_delta[j, k, h]))
                                {
                                    Price_delta[j, k, h] = 0.5 * Price_delta_Ave_Peak_A[j, k];
                                }
                            }
                            if ((h >= Hour_Base + Hour_Peak_N + Hour_Peak_A + 1) && (h < (Hour_Base + Hour_Peak_N + Hour_Peak_A + Hour_Peak_NI + 1)))
                            {
                                if ((1.5 * Price_delta_Ave_Peak_NI[j, k] < Price_delta[j, k, h]))
                                {
                                    Price_delta[j, k, h] =1.5 * Price_delta_Ave_Peak_NI[j, k];
                                }
                                if ((0.5 * Price_delta_Ave_Peak_NI[j, k] > Price_delta[j, k, h]))
                                {
                                    Price_delta[j, k, h] = 0.5 * Price_delta_Ave_Peak_NI[j, k];
                                }
                            }
                            if ((h == 0) || (h == 1) || (h == 23) || (h == 8))
                            {
                                if ((1.5* Price_delta_Ave_night[j, k] < Price_delta[j, k, h]))
                                {
                                    Price_delta[j, k, h] = 1.5 * Price_delta_Ave_night[j, k];
                                }
                                if ((0.5 * Price_delta_Ave_night[j, k] > Price_delta[j, k, h]))
                                {
                                    Price_delta[j, k, h] = 0.5 * Price_delta_Ave_night[j, k];
                                }
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Hour_State = new string[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Hour_State[j, k, h] = "O-F";
                    }
                }
            }
           
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[] Priceaverageyesterday = new double[Hour];
            for (int h = 0; h < Hour; h++)
            {
                Priceaverageyesterday[h] = zaribavyes[h];
                if (Priceaverageyesterday[h] == 0)
                {
                    Priceaverageyesterday[h] = zaribav2yes[h];
                }
                if (Priceaverageyesterday[h] == 0)
                {
                    Priceaverageyesterday[h] = zaribavnearest[h];
                }
            }



            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            DateTime Date11 = PersianDateConverter.ToGregorianDateTime(currentDate);
            double[] factor_manategh = new double[Hour];
            string datecurfactor = currentDate;
            string nearloadbid = nearestlfc();
            string nearcurload = new PersianDate(PersianDateConverter.ToGregorianDateTime(nearloadbid).AddDays(-1)).ToString("d");
            DataTable checkload1 = Utilities.GetTable("select *  from dbo.LoadForecasting where Date='" + nearcurload + "' and DateEstimate in(select max(DateEstimate) from dbo.LoadForecasting where Date='" + nearcurload + "')");
            if (checkload1.Rows.Count == 0)
            {
                for (int i = 1; i < 365; i++)
                {
                    DateTime c1 = PersianDateConverter.ToGregorianDateTime(nearcurload);
                    c1 = c1.AddDays(-i);
                    checkload1 = Utilities.GetTable("select *  from dbo.LoadForecasting where Date='" + new PersianDate(c1).ToString("d") + "' and DateEstimate in(select max(DateEstimate) from dbo.LoadForecasting where Date='" + new PersianDate(c1).ToString("d") + "')");
                    bool com0205available = common0205entity(new PersianDate(c1).ToString("d"));
                    if ((checkload1.Rows.Count > 0) && (islocalweek(new PersianDate(c1).ToString("d")) == false) && (c1.DayOfWeek.ToString() != "Friday") && (com0205available))
                    {
                        nearcurload = new PersianDate(c1).ToString("d");
                        break;

                    }
                }
            }
            else if ((islocalweek(nearcurload) == true) || (PersianDateConverter.ToGregorianDateTime(nearcurload).DayOfWeek.ToString() == "Friday"))
            {
                for (int i = 1; i < 365; i++)
                {
                    DateTime c3 = PersianDateConverter.ToGregorianDateTime(nearcurload);
                    DateTime samplecur = c3;
                    samplecur = samplecur.AddDays(-i);
                    checkload1 = Utilities.GetTable("select *  from dbo.LoadForecasting where Date='" + new PersianDate(samplecur).ToString("d") + "' and DateEstimate in(select max(DateEstimate) from dbo.LoadForecasting where Date='" + new PersianDate(samplecur).ToString("d") + "')");
                    bool com0205available = common0205entity(new PersianDate(samplecur).ToString("d"));
                    if ((checkload1.Rows.Count > 0) && (islocalweek(new PersianDate(samplecur).ToString("d")) == false) && (samplecur.DayOfWeek.ToString() != "Friday") && (com0205available))
                    {
                        nearcurload = new PersianDate(samplecur).ToString("d");
                        break;

                    }
                }
            }
            else
            {
                if (common0205entity(nearcurload) == false)
                {
                    for (int i = 1; i < 365; i++)
                    {
                        DateTime c3 = PersianDateConverter.ToGregorianDateTime(nearcurload);
                        DateTime samplecur = c3;
                        samplecur = samplecur.AddDays(-i);
                        if (common0205entity(new PersianDate(samplecur).ToString("d")))
                        {
                            nearcurload = new PersianDate(samplecur).ToString("d");
                            break;
                        }
                    }

                }

            }


            if (nearloadbid == "" || nearloadbid == null) nearloadbid = biddingDate;
            if (nearcurload == "" || nearcurload == null) nearcurload = currentDate;


            DataTable dloadbidding = Utilities.GetTable("select *  from dbo.LoadForecasting where Date='" + nearloadbid + "' and DateEstimate in(select max(DateEstimate) from dbo.LoadForecasting where Date='" + nearloadbid + "')");
            DataTable dloadcurrent = Utilities.GetTable("select *  from dbo.LoadForecasting where Date='" + nearcurload + "' and DateEstimate in(select max(DateEstimate) from dbo.LoadForecasting where Date='" + nearcurload + "')");
                    
            if(dloadbidding.Rows.Count>0 && dloadcurrent.Rows.Count>0)
            {
                for (int h = 1; h <= 24; h++)
                {
                    double tbid = MyDoubleParse(dloadbidding.Rows[0]["Hour" + h].ToString());
                    double tcur = MyDoubleParse(dloadcurrent.Rows[0]["Hour" + h].ToString());
                    if (tbid != 0 && tcur != 0)
                    {
                        factor_manategh[h - 1] = (tcur / tbid) - 1;
                        if (factor_manategh[h - 1] < 0)
                        {
                            factor_manategh[h - 1] = 0;
                        }
                    }
                }
            }
            //-------------------------------------------------------
            
            if (IsWeekEnd)
            {
                dayofbidding = "Friday";
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~saled energy~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            ///////////////////////////////////////////////////////////////////////////////////

            double PracticalPower = 0;
            double ExpectedPower = 0;
            double Producable = 0;
            double Produced = 0;
            DataTable rep12 = Utilities.GetTable("select * from dbo.Rep12Page where Date<='" + biddingDate + "'order by  date desc");

            if (rep12.Rows.Count > 0)
            {
                string daterep = rep12.Rows[0]["Date"].ToString().Trim();
                rep12 = Utilities.GetTable("select sum(PracticalPower),sum(ExpectedPower),sum(Producable),sum(Produced) from dbo.Rep12Page where Date='" + daterep + "'");
                PracticalPower = MyDoubleParse(rep12.Rows[0][0].ToString());
                ExpectedPower = MyDoubleParse(rep12.Rows[0][1].ToString());
                Producable = MyDoubleParse(rep12.Rows[0][2].ToString());
                Produced = MyDoubleParse(rep12.Rows[0][3].ToString());

            }

            ///////////////////////////////////////////////////////////////////////////////////
            double[,] pricenergy = new double[19, 2];
            double[,] hourenergyEc = new double[19, Hour];
            double[,] hourenergyUc = new double[19, Hour];
            double[] Over_ul = new double[Hour];
            double[,] Over_off = new double[19, Hour];
            double[] Price_Ec = new double[Hour];
            double[] avgsale = new double[Hour];
            try
            {
                string twoweek = new PersianDate(PersianDateConverter.ToGregorianDateTime(biddingDate).AddDays(-14)).ToString("d");
                DataTable dtenergy = Utilities.GetTable("select * from dbo.SaledEnergyuc where date<='" + biddingDate + "' and date >= '" + twoweek + "' order by date desc,rowindex asc");

                /////////////////////////////avg-sale-Uc//////////////////////////////////

                string saledateUc = "";
                if (dtenergy.Rows.Count > 0)
                {
                    saledateUc = dtenergy.Rows[0]["Date"].ToString().Trim();
                }

                if (saledateUc != "")
                {
                    DataTable davgsale = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date<='" + saledateUc + "'order by date desc,hour asc");


                    if (davgsale.Rows.Count > 0)
                    {
                        for (int i = 0; i < 24; i++)
                        {
                            avgsale[i] = MyDoubleParse(davgsale.Rows[i]["AcceptedAverage"].ToString());

                        }

                    }
                }

                /////////////////////////////////////avg-sal/////////////////////////////////////////////
                int index = 0;
                foreach (DataRow mm in dtenergy.Rows)
                {

                    if (MyDoubleParse(mm["rowindex"].ToString()) >= 19)
                    {
                        break;
                    }
                    else
                    {
                        pricenergy[index, 0] = MyDoubleParse(mm["PriceMin"].ToString());
                        pricenergy[index, 1] = MyDoubleParse(mm["PriceMax"].ToString());

                        for (int h = 0; h < 24; h++)
                        {
                            hourenergyUc[index, h] = MyDoubleParse(mm["Hour" + (h + 1)].ToString());

                        }
                        index++;
                    }
                }
            }
            catch
            {

            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            for (int h = 0; h < Hour; h++)
            {
                for (int ix = 1; ix < 19; ix++)
                {
                    if (pricenergy[18 - ix, 0] > Old_price[0, 0, 0, h])
                    {
                        Over_off[ix, h] = Over_off[ix - 1, h] + 1.4 * hourenergyUc[18 - ix, h];
                    }
                    else
                    {
                        Over_off[ix, h] = Over_off[ix - 1, h] + hourenergyUc[18 - ix, h];
                    }
                }
            }
            DataTable Load_saled = Utilities.GetTable("select *  from dbo.LoadForecasting where Date='" + nearcurload + "' and DateEstimate in(select max(DateEstimate) from dbo.LoadForecasting where Date='" + nearcurload + "')");

            for (int h = 1; h <= Hour; h++)
            {
                if (Load_saled.Rows.Count != 0)
                {
                    double Load_saled_Q = MyDoubleParse(Load_saled.Rows[0]["Hour" + h].ToString());
                    for (int ix = 1; ix < 18; ix++)
                    {
                        if ((Producable != 0) && (Load_saled_Q != 0))
                        {
                            if (Over_off[ix, h - 1] > 1.1 * (Producable - Load_saled_Q))
                            {
                                Over_ul[h - 1] = pricenergy[17 - ix, 0];
                                break;
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~SaledEc
            try
            {
                string twoweek = new PersianDate(PersianDateConverter.ToGregorianDateTime(biddingDate).AddDays(-14)).ToString("d");
                DataTable Edtenergy = Utilities.GetTable("select * from dbo.SaledEnergyEc where date<='" + biddingDate + "' and date >= '" + twoweek + "' order by date desc,rowindex asc");

                /////////////////////////////avg-sale//////////////////////////////////

                string saledateEc = "";
                if (Edtenergy.Rows.Count > 0)
                {
                    saledateEc = Edtenergy.Rows[0]["Date"].ToString().Trim();
                }

                if (saledateEc != "")
                {
                    DataTable davgsale = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date<='" + saledateEc + "'order by date desc,hour asc");


                    if (davgsale.Rows.Count > 0)
                    {
                        for (int i = 0; i < 24; i++)
                        {
                            avgsale[i] = MyDoubleParse(davgsale.Rows[i]["AcceptedAverage"].ToString());

                        }
                    }
                }

                /////////////////////////////////////avg-sal/////////////////////////////////////////////
                int index = 0;
                foreach (DataRow mm in Edtenergy.Rows)
                {

                    if (MyDoubleParse(mm["rowindex"].ToString()) >= 19)
                    {
                        break;
                    }
                    else
                    {
                        pricenergy[index, 0] = MyDoubleParse(mm["PriceMin"].ToString());
                        pricenergy[index, 1] = MyDoubleParse(mm["PriceMax"].ToString());

                        for (int h = 0; h < 24; h++)
                        {
                            hourenergyEc[index, h] = MyDoubleParse(mm["Hour" + (h + 1)].ToString());

                        }
                        index++;
                    }
                }
            }
            catch
            {

            }

            for (int h = 0; h < Hour; h++)
            {
                for (int ix = 1; ix < 19; ix++)
                {
                    if ((hourenergyEc[18 - ix, h] >= hourenergyUc[18 - ix, h]) || (hourenergyEc[18 - ix, h] > 1500))
                    {
                        Price_Ec[h] = pricenergy[18 - ix, 0];
                        break;
                    }
                }
                if (Price_Ec[h] < avgsale[h])
                {
                    Price_Ec[h] = avgsale[h];
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int ttype = 3;
            double[, ,] PowerAverage = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Over = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Profit_Average = new double[Plants_Num, ttype, Hour];
            double[] PriceAverageMarket = new double[Hour];
            double[] Price_Ec_saled = new double[Hour];
            double[, ,] Profit_Powerforecast = new double[Plants_Num, ttype, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int h = 0; h < Hour; h++)
            {
                if ((Price_Ec[h] != 0) && (Priceaverageyesterday[h] != 0) && (avgsale[h] != 0))
                {
                    Price_Ec_saled[h] = Price_Ec[h] * (Priceaverageyesterday[h] / avgsale[h]);
                }
                if ((Price_Ec_saled[h] != 0) && (Price_Ec_saled[h] > Old_price[0, 0, 0, h]))
                {
                    Price_Ec_saled[h] = (Price_Ec_saled[h] + Old_price[0, 0, 0, h]) * 0.5;
                }
                if (Price_Ec_saled[h] == 0)
                {
                    Price_Ec_saled[h] = avgsale[h];
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int h = 0; h < Hour; h++)
            {
                if ((Over_ul[h] != 0) && (Priceaverageyesterday[h] != 0) && (avgsale[h] != 0))
                {
                    PriceAverageMarket[h] = Over_ul[h] * (Priceaverageyesterday[h] / avgsale[h]);
                }
                if (PriceAverageMarket[h] == 0)
                {
                    PriceAverageMarket[h] = Old_price[0, 0, 0, h];
                }
            }
            
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] price_hist = new double[Plants_Num, Units_Num, Hour];
            double[, ,] PMarket_yest = new double[Plants_Num, Units_Num, Hour];
            double[, ,] price_Steam = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        price_hist[j, k, h] = Math.Round(Old_price[j, k, 0, h] / 1000) * 1000; 
                        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                        if (price_hist[j, k, h] == 0)
                        {
                            for (int d = 0; d < OldDay_Strategy; d++)
                            {
                                if (Old_price[j, k, d, h] != 0)
                                {
                                    price_hist[j, k, h] = Math.Round(Old_price[j, k, d, h] / 1000) * 1000; 
                                    break;
                                }
                            }
                        }

                        if (plant[j] == "701")
                        {
                            AvcValue[j, k] = 165000;
                            if (Unit_Dec[j, k, 0] == "Steam")
                            {
                               // price_hist[j, k, h] = price_hist[j, k, h] * 1.036;
                                price_hist[j, k, h] = price_hist[j, k, h] * 1;
                                for (int kk = 0; kk < Plant_units_Num[j]; kk++)
                                {
                                    price_Steam[j, kk, h] = price_hist[j, k, h];
                                }
                            }
                        }

                        if (plant[j] == "980") 
                        {
                            AvcValue[j, k] = 165000;
                            if (Unit_Dec[j, k, 0] == "Steam")
                            {
                                //price_hist[j, k, h] = price_hist[j, k, h] * 1.033;
                                price_hist[j, k, h] = price_hist[j, k, h] *1;
                                for (int kk = 0; kk < Plant_units_Num[j]; kk++)
                                {
                                    price_Steam[j, kk, h] = price_hist[j, k, h];
                                }
                            }
                        }
                        if (plant[j] == "707")
                        {
                            AvcValue[j, k] = 203000;
                            price_hist[j, k, h] = price_hist[j, k, h] * 1;
                            for (int kk = 0; kk < Plant_units_Num[j]; kk++)
                            {
                                price_Steam[j, kk, h] = price_hist[j, k, h];
                            }
                        }



                        //if (price_hist[j, k, h] == 0)
                        //{
                        //    price_hist[j, k, h] = Price_Ec_saled[h];
                        //}

                        //if ((Capacity_Require[j, k, h] < (0.75 * Capacity_Dispatch[j, k, h])) && (price_hist[j, k, h] > 0.997 * CapPrice[0]) && (setparam[j, 6] == false))
                        //{
                        //    PMarket_yest[j, k, h] = 1;
                        //    for (int d = 0; d < OldDay_Strategy; d++)
                        //    {
                        //        if ((Old_price[j, k, d, h] < 0.99 * CapPrice[0]) && (Old_price[j, k, d, h] > 0))
                        //        {
                        //            price_hist[j, k, h] = Old_price[j, k, d, h];
                        //            break;
                        //        }
                        //    }
                        //}

                        //if ((Capacity_Economic[j, k, h] < (0.75 * Capacity_Dispatch[j, k, h])) && (price_hist[j, k, h] > 0.997 * CapPrice[0]) && (setparam[j, 6] == true))
                        //{
                        //    PMarket_yest[j, k, h] = 1;
                        //    for (int d = 0; d < OldDay_Strategy; d++)
                        //    {
                        //        if ((Old_price[j, k, d, h] < 0.99 * CapPrice[0]) && (Old_price[j, k, d, h] > 0))
                        //        {
                        //            price_hist[j, k, h] = Old_price[j, k, d, h];
                        //            break;
                        //        }
                        //    }
                        //}

                        if (dayofbidding == "Friday")
                        {

                            //if ((ZaribafterHoliday[j, h] > 0.97) && (ZaribafterHoliday[j, h] < 1))
                            //{
                            //    price_hist[j, k, h] = (Old_price[j, k, 0, h]) * zaribweekend[j, h];
                            //}
                            //else if (ZaribafterHoliday[j, h] <= 0.97)
                            //{
                            //    price_hist[j, k, h] = (Old_price[j, k, 0, h]) * 0.97;
                            //}
                            //else
                            //{
                            //    if (h < 3)
                            //    {
                            //        price_hist[j, k, h] = Old_price[j, k, 0, h] * 1;
                            //    }

                            //    else if ((h == 7) || (h == 8) || (h == 9) || (h == 10))
                            //    {
                            //        price_hist[j, k, h] = Old_price[j, k, 0, h] * 0.98;
                            //    }

                            //    else if ((h == 11) || (h == 12) || (h == 13) || (h == 14))
                            //    {
                            //        price_hist[j, k, h] = Old_price[j, k, 0, h] * 0.985;
                            //    }

                            //    else
                            //    {
                            //        price_hist[j, k, h] = Old_price[j, k, 0, h] * 0.99;
                            //    }
                            //}
                        }
                        if (dayofbidding == "Saturday")
                        {
                            //if ((Zaribsaturday[j, h] != 1) && (h < 8))
                            //{
                            //    //price_hist[j, k, h] = Old_price[j, k, 1, h] * Zaribsaturday[j, h];
                            //}
                            //else
                            {
                                //if ((h == 2) && (h == 3) && (h == 4) && (h == 5) && (h == 6) && (h == 7))
                                //{
                                //    price_hist[j, k, h] = Old_price[j, k, 0, h] * 0.995;
                                //}

                                //else
                                //{
                                //    price_hist[j, k, h] = Old_price[j, k, 0, h] * 1.005;
                                //}
                            }
                        }
                        if (dayofbidding == "Thursday")
                        {
                            //{
                            //    if (h < 8)
                            //    {
                            //        price_hist[j, k, h] = Old_price[j, k, 0, h] * 0.99;
                            //    }

                            //    else
                            //    {
                            //        price_hist[j, k, h] = Old_price[j, k, 0, h] * 0.99;
                            //    }
                            //}
                        }
                        if ((yeterdayweekend) && (dayofbidding != "Friday"))
                        {
                            if (plant[j] == "503")
                            {
                                //if ((ZaribafterHoliday[j, h] > 0.98) && (ZaribafterHoliday[j, h] < 1))
                                //{
                                //    price_hist[j, k, h] = Old_price[j, k, 0, h] * ZaribafterHoliday[j, h];
                                //}
                                //else if (ZaribafterHoliday[j, h] <= 0.98)
                                //{
                                //    price_hist[j, k, h] = Old_price[j, k, 0, h] * 0.98;
                                //}
                                //else
                                if (h < 8)
                                {
                                    price_hist[j, k, h] = Math.Round(Old_price[j, k, 0, h] * 0.99 / 1000) * 1000; 
                                }

                                else
                                {
                                    price_hist[j, k, h] = Math.Round(Old_price[j, k, 0, h] * 0.98 / 1000) * 1000;
                                }
                            }
                        }

                         if (difday >= 2)
                        {

                        }
                        //else
                        //{
                        //    price_hist[j, k, h] = price_hist[j, k, h] * zariMarket[j, k, h];
                        //}
                        if (price_hist[j, k, h] < 1)
                        {
                            price_hist[j, k, h] = CapPrice[0];
                        }
                        PriceForecasting[j, k, h] = price_hist[j, k, h];
                    }
                }
            }



            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //for (int j = 0; j < Plants_Num; j++)
            //{
            //    for (int k = 0; k < Plant_units_Num[j]; k++)
            //    {
            //        for (int h = 0; h < Hour; h++)
            //        {
            //            if (setparam[j, 6])
            //            {
            //                if ((Over_ul[h] > price_hist[j, k, h]) && (avgsale[h] != 0) && (Priceaverageyesterday[h] != 0))
            //                {
            //                    price_hist[j, k, h] = Over_ul[h] * (Priceaverageyesterday[h] / avgsale[h]);
            //                }
            //                if ((Over_ul[h] > price_hist[j, k, h]) && ((avgsale[h] == 0) || (Priceaverageyesterday[h] == 0)))
            //                {
            //                    price_hist[j, k, h] = Over_ul[h];
            //                }
            //            }
            //        }
            //    }
            //}
           //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[, ,] Check_Dec = new int[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Check_Dec[j, k, h] = 0;
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Old_price_Ave[j, k, h] > (price_hist[j, k, h] + 10 * CapPrice[1]))
                        {
                            Check_Dec[j, k, h] = 1;
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[, ,] Check_Inc = new int[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Check_Inc[j, k, h] = 0;
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Old_price_Ave[j, k, h] < (price_hist[j, k, h] - 10 * CapPrice[1]))
                        {
                            Check_Inc[j, k, h] = 1;
                        }
                    }
                }
            }

            //////////////////////////////Market power////////////////////////////////
            double[,,] Power_Plant = new double[Plants_Num,3, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if (Outservice_Plant_unit[j, k, h] == 0)
                        {
                            if (Unit_Dec[j, k, 1] == "Steam") Power_Plant[j,2, h] = Power_Plant[j,2,h] + Pmax_Plant_Pack[j, k, h];
                            if (Unit_Dec[j, k, 1] == "Gas") Power_Plant[j, 1, h] = Power_Plant[j,1, h] + Pmax_Plant_Pack[j, k, h];
                            if (Unit_Dec[j, k, 1] == "CC") Power_Plant[j, 0, h] = Power_Plant[j,0, h] + Pmax_Plant_Pack[j, k, h];
                        }

                        if (statusmust[j, k, h] == "MO")
                        {
                            if (Unit_Dec[j, k, 1] == "Steam") Power_Plant[j, 2, h] = Power_Plant[j, 2, h] - Pmax_Plant_Pack[j, k, h] + valmust[j, k, h];
                            if (Unit_Dec[j, k, 1] == "Gas") Power_Plant[j, 1, h] = Power_Plant[j, 1, h] - Pmax_Plant_Pack[j, k, h] + valmust[j, k, h];
                            if (Unit_Dec[j, k, 1] == "CC") Power_Plant[j, 0, h] = Power_Plant[j, 0, h] - Pmax_Plant_Pack[j, k, h] + valmust[j, k, h];
                        }
                    }
                }
            }
            /////////////////////////////////with power///////////////////////////////////////////
            /////////////////////////Produce energy/////////////////////////
            int daysbefore_power = 5;
            double[, ,] Produce7day = new double[Plants_Num, daysbefore_power, 24];
            string[] datepro = new string[daysbefore_power];
            DataTable dt7 = null;
            try
            {

                DateTime curtime1 = PersianDateConverter.ToGregorianDateTime(currentDate);
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int daysbefore = 0; daysbefore < daysbefore_power; daysbefore++)
                    {

                        DateTime selectedDate = curtime1.Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                        string perpro = new PersianDate(selectedDate).ToString("d");
                        datepro[daysbefore] = perpro;
                        ////////////////////////////////////////////
                        int dualpid = int.Parse(plant[j]) + 1;
                        bool isdual = false;
                        DataTable dualtypeid = Utilities.GetTable("select ppid,PackageType from dbo.PPUnit where PPID='" + plant[j] + "'");
                        if (dualtypeid.Rows.Count > 1)
                        {
                            foreach (DataRow myrow in dualtypeid.Rows)
                            {

                                if (myrow[1].ToString().Trim() == "Combined Cycle")
                                {
                                    isdual = true;
                                    break;
                                }

                            }

                        }
                        ///////////////////////////////////////////
                        if (isdual)
                        {
                            dt7 = Utilities.GetTable("select sum(Hour1),sum(Hour2),sum(Hour3),sum(Hour4),sum(Hour5)," +
                            "sum(Hour6),sum(Hour7),sum(Hour8),sum(Hour9)," +
                            "sum(Hour10),sum(Hour11),sum(Hour12),sum(Hour13),sum(Hour14)," +
                            "sum(Hour15),sum(Hour16),sum(Hour17),sum(Hour18),sum(Hour19),sum(Hour20)" +
                            ",sum(Hour21),sum(Hour22),sum(Hour23),sum(Hour24)" +
                            " from dbo.ProducedEnergy where Date='" + perpro + "' and (PPCode='" + plant[j] + "' or PPCode='" + dualpid + "')");
                        }
                        if (!isdual)
                        {

                            dt7 = Utilities.GetTable("select sum(Hour1),sum(Hour2),sum(Hour3),sum(Hour4),sum(Hour5)," +
                            "sum(Hour6),sum(Hour7),sum(Hour8),sum(Hour9)," +
                            "sum(Hour10),sum(Hour11),sum(Hour12),sum(Hour13),sum(Hour14)," +
                            "sum(Hour15),sum(Hour16),sum(Hour17),sum(Hour18),sum(Hour19),sum(Hour20)" +
                            ",sum(Hour21),sum(Hour22),sum(Hour23),sum(Hour24)" +
                            " from dbo.ProducedEnergy where Date='" + perpro + "' and PPCode='" + plant[j] + "'");

                        }

                        if (dt7.Rows.Count > 0)
                        {

                            for (int co = 0; co < 24; co++)
                            {
                                Produce7day[j, daysbefore, co] = MyDoubleParse(dt7.Rows[0][co].ToString());

                            }
                        }
                    }
                }
            }

            catch (System.Exception r)
            {
                string v3 = r.ToString();

            }


            //////////////////////////////end produce energy//////////////////////////////////////
            int OldDay_Power = daysbefore_power;
            /////////////////////////Produce energy/////////////////////////
            //double[,] Power_Plant_min = new double[Plants_Num, Hour];
            //for (int j = 0; j < Plants_Num; j++)
            //{
            //    for (int h = 0; h < Hour; h++)
            //    {
            //        for (int k = 0; k < Plant_units_Num[j]; k++)
            //        {
            //            if (Outservice_Plant_unit[j, k, h] == 0)
            //            {
            //                Power_Plant_min[j, h] = Power_Plant_min[j, h] + Pmin_Plant_Pack[j, k, h];
            //            }
            //        }
            //    }
            //}


            //---------------------------------newwwwww----------------------------------------
            double[, ,] Power_Plant_min = new double[Plants_Num, 3, Hour];

            //Steam 2
            // Gas 1
            // CC  0 


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if (Outservice_Plant_unit[j, k, h] == 0)
                        {
                            if (Unit_Dec[j, k, 1] == "Steam") Power_Plant_min[j, 2, h] = Power_Plant_min[j, 2, h] + Pmin_Plant_Pack[j, k, h];
                            if (Unit_Dec[j, k, 1] == "Gas") Power_Plant_min[j, 1, h] = Power_Plant_min[j, 1, h] + Pmin_Plant_Pack[j, k, h];
                            if (Unit_Dec[j, k, 1] == "CC") Power_Plant_min[j, 0, h] = Power_Plant_min[j, 0, h] + Pmin_Plant_Pack[j, k, h];
                        }
                        if ((statusmust[j, k, h] == "MO") && (valmust[j, k, h]==0))
                        {
                            if (Unit_Dec[j, k, 1] == "Steam") Power_Plant_min[j, 2, h] = Power_Plant_min[j, 2, h] - Pmin_Plant_Pack[j, k, h];
                            if (Unit_Dec[j, k, 1] == "Gas") Power_Plant_min[j, 1, h] = Power_Plant_min[j, 1, h] - Pmin_Plant_Pack[j, k, h];
                            if (Unit_Dec[j, k, 1] == "CC") Power_Plant_min[j, 0, h] = Power_Plant_min[j, 0, h] - Pmin_Plant_Pack[j, k, h];
                        }
                    }
                }
            }

            int[,] Ptypepackage = new int[Plants_Num, Units_Num ];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        {
                            if (Unit_Dec[j, k, 1] == "Steam") Ptypepackage[j, k] = 2 ;
                            if (Unit_Dec[j, k, 1] == "Gas") Ptypepackage[j, k] = 1;
                            if (Unit_Dec[j, k, 1] == "CC") Ptypepackage[j, k] = 0;
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_power = new double[Plants_Num, OldDay_Power, Hour];
            double[,] Old_power_hist = new double[Plants_Num, Hour];
            int[,] p_hist = new int[Plants_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    p_hist[j, h] = 0;
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int d = 0; d < OldDay_Power; d++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_power[j, d, h] = Produce7day[j, d, h];
                        if ((Old_power[j, d, h] != 0) && (p_hist[j, h] == 0))
                        {
                            Old_power_hist[j, h] = Old_power[j, d, h];
                            p_hist[j, h] = 1;
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int d = 0; d < OldDay_Power; d++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Old_power[j, d, h] == 0)
                        {
                            Old_power[j, d, h] = Old_power_hist[j, h];
                        }
                    }
                }
            }
            //--------------------------------------check if null---------------------------------------------
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int daysbefore = 0; daysbefore < daysbefore_power; daysbefore++)
                {
                    for (int h = 0; h < 24; h++)
                    {
                        if (Old_power[j, daysbefore, h] == 0)
                        {
                            string date = datepro[daysbefore];
                            for (int i = 1; i < 30; i++)
                            {
                                DateTime ff = PersianDateConverter.ToGregorianDateTime(date);
                                ff = ff.AddDays(-i);
                                DataTable dt = Utilities.GetTable("select sum(p) from dbo.DetailFRM009 where TargetMarketDate='" + new PersianDate(ff).ToString("d") + "' and PPID='" + plant[j] + "' and Hour='" + h + "'");
                                if (dt.Rows[0][0].ToString() != "0" && dt.Rows[0][0].ToString() != "")
                                {
                                    Old_power[j, daysbefore, h] = MyDoubleParse(dt.Rows[0][0].ToString());
                                    break;
                                }
                            }
                        }

                    }
                }
            }


            //---------------------------------------newwwwww---------------------------------------------------------
          
            double[, ,] capacity2 = capacity();
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int daysbefore = 0; daysbefore < daysbefore_power; daysbefore++)
                {
                    for (int h = 0; h < 24; h++)
                    {
                        if (capacity2[j, daysbefore, h] != 0)
                        {
                            Old_power[j, daysbefore, h] = Old_power[j, daysbefore, h] / capacity2[j, daysbefore, h];
                        }
                        else
                        {
                            {
                                Old_power[j, daysbefore, h] = Old_power[j, daysbefore, h] / (Power_Plant[j, 2, h] + Power_Plant[j, 1, h] + Power_Plant[j, 0, h]);
                            }
                        }
                    } 
                } 
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~newwwwwwww~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Old_power_sum = new double[Plants_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    Old_power_sum[j, h] = 0;
                    for (int d = 0; d < OldDay_Power; d++)
                    {
                        Old_power_sum[j, h] = Old_power_sum[j, h] + Old_power[j, d, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Old_power_Ave = new double[Plants_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    Old_power_Ave[j, h] = Old_power_sum[j, h] * Math.Pow(OldDay_Power, -1);
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_power_diff = new double[Plants_Num, OldDay_Power, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int d = 0; d < OldDay_Power; d++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_power_diff[j, d, h] = Old_power[j, d, h] - Old_power_Ave[j, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_power_pow = new double[Plants_Num, OldDay_Power, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int d = 0; d < OldDay_Power; d++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_power_pow[j, d, h] = Math.Pow(Old_power_diff[j, d, h], 2);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Old_power_pow_sum = new double[Plants_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    Old_power_pow_sum[j, h] = 0;
                    for (int d = 0; d < OldDay_Power; d++)
                    {
                        Old_power_pow_sum[j, h] = Old_power_pow_sum[j, h] + Old_power_pow[j, d, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Old_power_pow_Ave = new double[Plants_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    Old_power_pow_Ave[j, h] = Old_power_pow_sum[j, h] * Math.Pow(OldDay_Power, -1);
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            double[,] power_delta = new double[Plants_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    power_delta[j, h] = Math.Pow(Old_power_pow_Ave[j, h], 0.5);
                }
            }

            /////////////////////////////////////////check POWER forecast////////////////////////////
            Random rdd = new Random();

            //for (int j = 0; j < Plants_Num; j++)
            //{
            //    double[] poewercheck = new double[24];

            //    DataTable checkpowerres = utilities.GetTable("SELECT * FROM dbo.PowerForecast WHERE gencode like '" + plant[j] + '%' + "' AND date='" + biddingDate + "' ORDER BY Id DESC");
             

            //    if (checkpowerres.Rows.Count > 0)
            //    {
            //        string ptype = checkpowerres.Rows[0]["gencode"].ToString().Remove(0, 4).Trim();
            //        int inttype = 4;
            //        if (ptype == "CC") inttype = 0;
            //        if (ptype == "Gas") inttype = 1;
            //        if (ptype == "Steam") inttype = 2;


            //        for (int h = 0; h < Hour; h++)
            //        {
            //            poewercheck[h] = MyDoubleParse(checkpowerres.Rows[0][h + 3].ToString());
            //            if (Old_power_Ave[j, h] != 0)
            //            {
            //                if (poewercheck[h] > (Old_power_Ave[j, h] + power_delta[j, h]))
            //                {
            //                    poewercheck[h] = (Old_power_Ave[j, h] + power_delta[j, h]);
            //                }
            //                if (poewercheck[h] < (Old_power_Ave[j, h] - power_delta[j, h]))
            //                {
            //                    poewercheck[h] = (Old_power_Ave[j, h] - power_delta[j, h]);
            //                }
            //            }
            //            if (poewercheck[h] > 1)
            //            {
            //                poewercheck[h] = 1;
            //            }
            //        }
            //        savepowerchecked(plant[j].Trim(), poewercheck, ptype);
            //    }
            //}

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            double[] factor_Power = new double[Hour];
            for (int h = 0; h < 24; h++)
            {
                factor_Power[h] = 1 - factor_manategh[h];
                if (factor_Power[h] < 0)
                {
                    factor_Power[h] = 1;
                }
            }

            //-------------------------------------------------------
            double[,,] PowerForecast_pu = new double[Plants_Num,3, 24];
            //for (int j = 0; j < Plants_Num; j++)
            //{
            //    DataTable powertable = utilities.GetTable("select * from dbo.PowerForecast where date='" + biddingDate + "'and gencode LIKE'" + plant[j] + "%' order by Id desc");
               
            //    if (powertable.Rows.Count > 0)
            //    {
            //        string ptype = powertable.Rows[0]["gencode"].ToString().Remove(0, 4).Trim();
            //        for (int h = 0; h < 24; h++)
            //        {
            //            if (ptype == "CC") PowerForecast_pu[j, 0, h] = factor_Power[h] * MyDoubleParse(powertable.Rows[0]["Hour" + (h + 1)].ToString());
            //            if (ptype == "Gas") PowerForecast_pu[j, 1, h] = factor_Power[h] * MyDoubleParse(powertable.Rows[0]["Hour" + (h + 1)].ToString());
            //            if (ptype == "Steam") PowerForecast_pu[j,2, h] = factor_Power[h] * MyDoubleParse(powertable.Rows[0]["Hour" + (h + 1)].ToString());
            //        }
            //    }
            //}


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < 24; h++)
                {
                    for (int ki = 0; ki < 3; ki++)
                    {
                        PowerForecast_pu[j, ki, h] = (Old_power_Ave[j, h]);
                    }
                }
            }
        

            DataTable leveltable = Utilities.GetTable("select CostLevel,id from dbo.BiddingStrategySetting where id='" + BiddingStrategySettingId.ToString() + "'");
            string CostLevel = leveltable.Rows[0][0].ToString().Trim();
            //-------------------------------------------------------
            double[, ,] A = new double[Plants_Num, Units_Num, Hour];
            int check_Friday = 0;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        check_Friday = 0;
                        A[j, k, h] = 4;
                        {
                            if (Hour_State_User[j, k, h] == "Low")
                            {
                                Toreranceup[j, k, h] = -1.5 * Price_delta[j, k, h];
                                ToreranceDown[j, k, h] = 2 * Price_delta[j, k, h];
                            }
                            if (Hour_State_User[j, k, h] == "High")
                            {
                                Toreranceup[j, k, h] = -0.5 * Price_delta[j, k, h];
                                ToreranceDown[j, k, h] = 1 * Price_delta[j, k, h];

                            }
                            if (Hour_State_User[j, k, h] == "Medium")
                            {
                                Toreranceup[j, k, h] = -1 * Price_delta[j, k, h];
                                ToreranceDown[j, k, h] = 1.5 * Price_delta[j, k, h];
                            }
                            Limit_variance_Down[j, k, h] = 1 * CapPrice[1];
                            Limit_variance[j, k, h] = 10 * CapPrice[1];

                        }

                        //:**********************************************************************************************************************************
                        // UL-State :*************************************************************************************************************************
                        //if (CostLevel == "Software")
                        
                        if((Hour_State_User[j, k, h] == "High")&&((CostLevel == "Software"))) //pmousavi1397/03/06
                        {
                            if ((h == 0) || (h == 7) || (h == 8))
                            {
                                Toreranceup[j, k, h] = -4000;
                                ToreranceDown[j, k, h] = 6000;
                            }
                            if ((h == 1) || (h == 2) || (h == 3) || (h == 4) || (h == 5) || (h == 6))
                            {
                                Toreranceup[j, k, h] = -6000;
                                ToreranceDown[j, k, h] = 8000;
                            }
                            else
                            {
                                Toreranceup[j, k, h] = -4000;
                                ToreranceDown[j, k, h] = 6000;
                            }
                        }

                        if ((Hour_State_User[j, k, h] == "Medium") && ((CostLevel == "Software")))
                        {
                            if ((h == 0) || (h == 1) || (h == 2) || (h == 3) || (h == 4) || (h == 5) || (h == 6) || (h == 7) || (h == 8))
                            {
                                Toreranceup[j, k, h] = -6000;
                                ToreranceDown[j, k, h] = 8000;
                            }
                            else
                            {
                                Toreranceup[j, k, h] = -6000;
                                ToreranceDown[j, k, h] = 8000;
                            }
                        }

                        if ((Hour_State_User[j, k, h] == "Low") && ((CostLevel == "Software")))
                        {
                            if ((h == 0) || (h == 1) || (h == 2) || (h == 3) || (h == 4) || (h == 5) || (h == 6) || (h == 7) || (h == 8))
                            {
                                    Toreranceup[j, k, h] = -8000;
                                    ToreranceDown[j, k, h] = 10000;
                            }
                            else
                            {
                                Toreranceup[j, k, h] = -8000;
                                ToreranceDown[j, k, h] = 10000;
                            }
                        }

                        if ((Hour_State_User[j, k, h] == "Low") && ((CostLevel == "Manual")))
                        {
                            if (Toreranceup[j, k, h] > -8000)
                            {
                                Toreranceup[j, k, h] = -8000;
                            }
                            if (ToreranceDown[j, k, h] > 10000)
                            {
                                ToreranceDown[j, k, h] = 10000;
                            }
                        }

                        if ((Hour_State_User[j, k, h] == "Medium") && ((CostLevel == "Manual")))
                        {
                            if (Toreranceup[j, k, h] > -6000)
                            {
                                Toreranceup[j, k, h] = -6000;
                            }
                            if (ToreranceDown[j, k, h] > 8000)
                            {
                                ToreranceDown[j, k, h] = 8000;
                            }
                        }

                        if ((Hour_State_User[j, k, h] == "High") && ((CostLevel == "Manual")))
                        {
                            if (Toreranceup[j, k, h] > -4000)
                            {
                                Toreranceup[j, k, h] = -4000;
                            }
                            if (ToreranceDown[j, k, h] > 6000)
                            {
                                ToreranceDown[j, k, h] = 6000;
                            }
                        }

                        if ((dayofbidding == "Friday") && (Hour_State_User[j, k, h] != "High"))
                        {
                            if ((h == 0) || (h == 1) || (h == 2) || (h == 3) || (h == 4) || (h == 5) || (h == 6))
                            {
                                if (Toreranceup[j, k, h] > -6000)
                                {
                                    Toreranceup[j, k, h] = -6000;
                                }
                                if (ToreranceDown[j, k, h] < 8000)
                                {
                                    ToreranceDown[j, k, h] = 8000;
                                }
                            }
                            if ((h == 9) || (h == 10) || (h == 11) || (h == 12))
                            {
                                if (Toreranceup[j, k, h] > -12000)
                                {
                                    Toreranceup[j, k, h] = -12000;
                                }
                                if (ToreranceDown[j, k, h] < 14000)
                                {
                                    ToreranceDown[j, k, h] = 14000;
                                }
                            }
                            if ((h == 13) || (h == 14) || (h == 15) || (h == 16) || (h == 8))
                            {
                                if (Toreranceup[j, k, h] > -10000)
                                {
                                    Toreranceup[j, k, h] = -10000;
                                }
                                if (ToreranceDown[j, k, h] < 12000)
                                {
                                    ToreranceDown[j, k, h] = 12000;
                                }
                            }
                            if ((h == 17) || (h == 18) || (h == 7))
                            {
                                if (Toreranceup[j, k, h] > -8000)
                                {
                                    Toreranceup[j, k, h] = -8000;
                                }
                                if (ToreranceDown[j, k, h] < 10000)
                                {
                                    ToreranceDown[j, k, h] = 10000;
                                }
                            }
                            else
                            {
                                if (Toreranceup[j, k, h] > -8000)
                                {
                                    Toreranceup[j, k, h] = -8000;
                                }
                                if (ToreranceDown[j, k, h] < 10000)
                                {
                                    ToreranceDown[j, k, h] = 10000;
                                }
                            }
                        }

                        if ((dayofbidding == "Saturday")&& (Hour_State_User[j, k, h] != "High"))
                        {
                            if ((h == 0) || (h == 1) || (h == 2) || (h == 3) || (h == 4) || (h == 5) )
                            {
                                if (Toreranceup[j, k, h] > -6000)
                                {
                                    Toreranceup[j, k, h] = -6000;
                                }
                                if (ToreranceDown[j, k, h] < 8000)
                                {
                                    ToreranceDown[j, k, h] = 8000;
                                }
                            }
                            if ((h == 9) || (h == 10) || (h == 11) || (h == 12))
                            {
                                if (Toreranceup[j, k, h] > +2000)
                                {
                                    Toreranceup[j, k, h] = +2000;
                                }
                                if (ToreranceDown[j, k, h] < -1000)
                                {
                                    ToreranceDown[j, k, h] = -1000;
                                }
                            }
                            if ((h == 13) || (h == 14) || (h == 15) || (h == 16) || (h == 8))
                            {
                                if (Toreranceup[j, k, h] > +1000)
                                {
                                    Toreranceup[j, k, h] = +1000;
                                }
                                if (ToreranceDown[j, k, h] < 2000)
                                {
                                    ToreranceDown[j, k, h] = 2000;
                                }
                            }
                            if ((h == 17) || (h == 18) || (h == 7))
                            {
                                if (Toreranceup[j, k, h] > -1000)
                                {
                                    Toreranceup[j, k, h] = -1000;
                                }
                                if (ToreranceDown[j, k, h] < 2000)
                                {
                                    ToreranceDown[j, k, h] = 2000;
                                }
                            }
                            else
                            {
                                if (Toreranceup[j, k, h] > -1000)
                                {
                                    Toreranceup[j, k, h] = -1000;
                                }
                                if (ToreranceDown[j, k, h] < 2000)
                                {
                                    ToreranceDown[j, k, h] = 2000;
                                }
                            }
                        }                       

                        if (Toreranceup[j, k, h] < -12000)
                        {
                            Toreranceup[j, k, h] = -12000;
                        }

                        if (ToreranceDown[j, k, h] >14000)
                        {
                            ToreranceDown[j, k, h] = 14000;
                        }

                        if (Toreranceup[j, k, h] < 0)
                        {
                            //if (ToreranceDown[j, k, h] <= -Toreranceup[j, k, h] - 2 * CapPrice[1])
                            //{
                            //    ToreranceDown[j, k, h] = -1 * Toreranceup[j, k, h] - 2 * CapPrice[1];
                            //}
                        }
                        if (ToreranceDown[j, k, h] < CapPrice[1])
                        {
                            ToreranceDown[j, k, h] = CapPrice[1];
                        }
                        //:**********************************************************************************************************************************
                        //if (PMarket_yest[j, k, h] == 1)
                        //{
                        //    Toreranceup[j, k, h] = -4.5 * CapPrice[1];
                        //    ToreranceDown[j, k, h] = +9 * CapPrice[1];
                        //}
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            double[, ,] Margin_up = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        //////////////////////////////////////////////

                        DataTable dacvg = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date<='" + biddingDate + "'and hour='" + (h + 1) + "'order by date desc");
                        double abv=MyDoubleParse(dacvg.Rows[0][0].ToString());
                        if (price_hist[j, k, h] < abv && (plant[j]=="206" || plant[j]=="215")) price_hist[j, k, h] = 1.04*abv;

                        //////////////////////////////////////////////////////

                        Margin_up[j, k, h] = price_hist[j, k, h] + Toreranceup[j, k, h];
                        //PriceForecasting[j, k, h] = price_hist[j, k, h] + Toreranceup[j, k, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Margin_down = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Margin_down[j, k, h] = price_hist[j, k, h] - ToreranceDown[j, k, h];
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            double Erorr = 1000;

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   
            double[, ,] PowerMarket = new double[Plants_Num, Units_Num, Hour];
            double[, ,] PowerMarket_Gas = new double[Plants_Num, Units_Num, Hour];
            double[, ,] PowerMarket_2step_Gas = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Gas_OFF = new double[Plants_Num, Units_Num, Hour];
            double[,] SumPowerMarket = new double[Plants_Num, Units_Num];
            double[,,] Quantity_PowerMarket = new double[Plants_Num,ttype, Hour];
            double[, ,] counter_PowerMarket = new double[Plants_Num, ttype, Hour];
            double[,] RampRate = new double[Plants_Num, Units_Num];
            double[,] SPowerMarket = new double[Plants_Num, Units_Num];
            bool[] Market18 = new bool[Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //Dispatch_Proposal[j, k, h]
            double[, ,] Dispatch_ptype = new double[Plants_Num, 3, 24];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < 24; h++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        Dispatch_ptype[j, Ptypepackage[j, k], h] = Dispatch_ptype[j, Ptypepackage[j, k], h] + Pmax_Plant_unit[j, k, h];
                    }
                }
            }
            double[, ,] PowerForecast = new double[Plants_Num, 3, 24];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < 24; h++)
                {
                    for (int ki = 0; ki < 3; ki++)
                    {
                        PowerForecast[j, ki, h] = PowerForecast_pu[j, ki, h] * Dispatch_ptype[j, ki, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int ki = 0; ki < 3; ki++)
                    {
                        Profit_Average[j, ki, h] = PowerForecast[j, ki, h] * (PriceAverageMarket[h] - AvcValue[j, ki]);
                    }
                }
            }


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int ki = 0; ki < 3; ki++)
                    {
                        if ((Power_Plant[j, ki, h] > PowerForecast[j, ki, h]))
                        {
                            Profit_Powerforecast[j, ki, h] = (Power_Plant[j, ki, h] - PowerForecast[j, ki, h]) * (price_hist[j, 0, h] - AvcValue[j, ki]) + PowerForecast[j, ki, h] * (price_hist[j, 0, h] - AvcValue[j, ki]);
                        }
                        else
                        {
                            Profit_Powerforecast[j, ki, h] = PowerForecast[j, ki, h] * (price_hist[j, 0, h] - AvcValue[j, ki]);
                        }

                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        Over[j, k, h] = 0;
                        {
                            if ((Unit_Dec[j, k, 1] == "Steam") && (Profit_Powerforecast[j, 2, h] < Profit_Average[j, 2, h])) Over[j, k, h] = 1;
                            if ((Unit_Dec[j, k, 1] == "Gas") && (Profit_Powerforecast[j, 1, h] < Profit_Average[j, 1, h])) Over[j, k, h] = 1;
                            if ((Unit_Dec[j, k, 1] == "CC") && (Profit_Powerforecast[j, 0, h] < Profit_Average[j, 0, h])) Over[j, k, h] = 1;
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int ki = 0; ki < 3; ki++)
                    {
                        if (PowerForecast[j, ki, h] > 1 * Power_Plant_min[j, ki, h])
                        {
                            Quantity_PowerMarket[j, ki, h] = PowerForecast[j, ki, h] - Power_Plant_min[j, ki, h];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,,] Profit_MarketPower = new double[Plants_Num,3, Hour];
            double[,,] Profit_MarketPower_Gas= new double[Plants_Num,2, Hour];
             double[,] TSProfit_MarketPower = new double[Plants_Num,3];
            double[,] TSProfit_Powerforecast = new double[Plants_Num,3];
            double[,,] Plant_units_ON = new double[Plants_Num, 3, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if ((Outservice_Plant_unit[j, k, h] == 0))
                        {

                            if (Unit_Dec[j, k, 1] == "Steam") Plant_units_ON[j, 2, h] = Plant_units_ON[j, 2, h]+1;
                            if (Unit_Dec[j, k, 1] == "Gas") Plant_units_ON[j, 1, h] = Plant_units_ON[j, 1, h] + 1;
                            if (Unit_Dec[j, k, 1] == "CC") Plant_units_ON[j, 0, h] = Plant_units_ON[j, 0, h] + 1;

                        }
                        for (int ki = 0; ki < 3; ki++)
                        {
                            if (Plant_units_ON[j, ki, h] < 0)
                            {
                                Plant_units_ON[j, ki, h] = 1;
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~     

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int ki = 0; ki < 3; ki++)
                    {
                        if (PDiff_Max_Min_Plant_Pack[j,ki, h] != 0)
                        {
                            counter_PowerMarket[j,ki, h] = Math.Round((Quantity_PowerMarket[j, ki, h] / PDiff_Max_Min_Plant_Pack[j,ki, h]), 0, MidpointRounding.AwayFromZero);
                        }
                        if (counter_PowerMarket[j,ki, h] > Plant_units_ON[j,ki, h])
                        {
                            counter_PowerMarket[j,ki, h] = Plant_units_ON[j,ki, h];
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] MarketPower_El = new double[Plants_Num, Hour];
            string Fuelrule= "Normal";
            DataTable basetableFuel = Utilities.GetTable("select CapacityPayment from dbo.BaseData where Date <='" + biddingDate + "'order by BaseID desc");
            if (basetable.Rows.Count > 0)
            {
                if (basetableFuel.Rows.Count > 0)
                {
                  Fuelrule = basetableFuel.Rows[0][0].ToString();
                }
            }
            //for (int j = 0; j < Plants_Num; j++)
            //{
            //    for (int h = 0; h < Hour; h++)
            //    {
            //        if (Fuelrule == "Elghaee")
            //        {
            //            if (Capacity_Economic_max_EL[j, h] != 0)
            //                MarketPower_El[j, h] = Capacity_Economic[j, 0, h] / Capacity_Economic_max_EL[j, h];
            //            else
            //                MarketPower_El[j, h] = 1;
            //        }
            //        else
            //            MarketPower_El[j, h] = 1;
            //        if (MarketPower_El[j, h] > 1)
            //            MarketPower_El[j, h] = 1;
            //    }
            //}

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int ki = 0; ki < 3; ki++)
                    {
                        if ((PowerForecast[j, ki, h] > 1 * Power_Plant_min[j, ki, h]))
                        {
                            Profit_MarketPower[j, ki, h] = ((Plant_units_ON[j, ki, h] - counter_PowerMarket[j, ki, h]) * Pmin_Plant_Pack_Total_Max[j, h] * AvcValue[j, ki]) + (PowerForecast[j, ki, h] - ((Plant_units_ON[j, ki, h] - counter_PowerMarket[j, ki, h]) * Pmin_Plant_Pack_Total_Max[j, h])) * (CapPrice[0] - AvcValue[j, ki]);
                        }
                        if ((PowerForecast[j, ki, h] > 1.075 * Power_Plant_min[j, ki, h]))
                        {
                            Profit_MarketPower[j, ki, h] = PowerForecast[j, ki, h] * CapPrice[0] - AvcValue[j,ki];
                        }
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    {
                        if (Quantity_PowerMarket[j, 1, h] > 0)
                        {
                            Profit_MarketPower_Gas[j, 1, h] = Power_Plant_min[j, 1, h] * price_hist[j, 0, h] + Quantity_PowerMarket[j, 1, h] * CapPrice[0];
                        }
                        if (Quantity_PowerMarket[j, 1, h] <= 0)
                        {
                            Profit_MarketPower_Gas[j, 1, h] = PowerForecast[j, 1, h] *(price_hist[j, 0, h]- AvcValue[j, 1]);
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Gas_Profit_MarketPower = new double[Plants_Num,2];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    if (h < 10)
                    {
                        Gas_Profit_MarketPower[j, 0] = Gas_Profit_MarketPower[j, 0] + Profit_MarketPower[j, 1, h];
                    }
                    else if (h < 22)
                    {
                        Gas_Profit_MarketPower[j, 1] = Gas_Profit_MarketPower[j, 1] + Profit_MarketPower[j, 1, h];
                    }
                }
            }
        
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Gas_Profit_Powerforecast = new double[Plants_Num, 2];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    if (h < 10)
                    {
                        Gas_Profit_Powerforecast[j, 0] = Gas_Profit_Powerforecast[j, 0] + Profit_Powerforecast[j, 1, h];
                    }
                    else if (h < 22)
                    {
                        Gas_Profit_Powerforecast[j, 1] = Gas_Profit_Powerforecast[j, 1] + Profit_Powerforecast[j, 1, h];
                    }
                }
            }
            
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Gas_Costbase = new double[Plants_Num, 2];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    if (h < 10)
                    {
                        Gas_Costbase[j, 0] = Gas_Costbase[j, 0] + PowerForecast[j, 1, h] * AvcValue[j, 1];
                    }
                    else if (h < 22)
                    {
                        Gas_Costbase[j, 1] = Gas_Costbase[j, 1] + PowerForecast[j, 1, h] * AvcValue[j, 1];
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if ((h < 10) && (0 > Gas_Profit_Powerforecast[j, 0]))
                        {
                            Gas_OFF[j, k, h] = 1;
                        }
                        if ((h < 24) && (0 > Gas_Profit_Powerforecast[j, 1]))
                        {
                            Gas_OFF[j, k, h] = 1;
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        {
                            if ((Unit_Dec[j, k, 1] == "Steam") && (Profit_Powerforecast[j, 2, h] < Profit_MarketPower[j,2, h])) PowerMarket[j, k, h] = 1; ;
                            if ((Unit_Dec[j, k, 1] == "Gas") && (Profit_Powerforecast[j, 1, h] < Profit_MarketPower[j, 1, h])) PowerMarket[j, k, h] = 1;
                            if ((Unit_Dec[j, k, 1] == "CC") && (Profit_Powerforecast[j, 0, h] < Profit_MarketPower[j, 0, h])) PowerMarket[j, k, h] = 1;
                        }
                        SumPowerMarket[j, k] = SumPowerMarket[j, k] + PowerMarket[j, k, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < 10; h++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        {
                            if ((Unit_Dec[j, k, 1] == "Gas") && (Profit_MarketPower_Gas[j, 1, h] > Profit_Powerforecast[j, 1, h])) PowerMarket_2step_Gas[j, k, h] = 1;
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {

                        if ((PowerForecast[j, Ptypepackage[j, k], h] < 1.06 * Power_Plant_min[j, Ptypepackage[j, k], h]))
                        {
                            PowerMarket[j, k, h] = 0;
                        }

                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    RampRate[j, i] = RampRate[j, i] + Unit_Data[j, i, 1];
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (UL_State[j, k, h].Contains("UL") && (setparam[j, 2]!=false))
                        {
                            Hour_State[j, k, h] = "U-O";
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Run_State = new string[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Run_State[j, k, h] = Hour_State[j, k, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Cap_Bid = new string[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Cap_Bid[j, k, h] = Hour_Cap_User[j, k, h];
                    }
                }
            }
            //------------------------------------------------------------------------------------------------------------------------
            double[, ,] Tbid_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Tbid_Plant_unit[j, i, h] = (Pmax_Plant_unit[j, i, h] - Unit_Data[j, i, 3]) / Mmax;
                    }
                }
            }

            
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[, ,] Price_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k))
                            {
                                Price_Plant_unit[j, i, h] = PriceForecasting[j, k, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[, ,] VI_Plant_unit = new int[Plants_Num, Units_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        VI_Plant_unit[j, i, h] = 1;

                        strCmd = "select MaintenanceStartDate , MaintenanceEndDate,MaintenanceStartHour, MaintenanceEndHour" +
                            " from ConditionUnit" +
                            " where PPID= '" + plant[j] + "'" +
                            " and UnitCode = '" + Unit[j, i] + "'" +
                            " and Maintenance= 1" +
                            " and (MaintenanceStartDate ='" + biddingDate + "' or MaintenanceEndDate= '" + biddingDate + "')" +
                            " order by Date Desc";
                        oDataTable = Utilities.GetTable(strCmd);

                        if (oDataTable.Rows.Count > 0)
                        {
                            DataRow row = oDataTable.Rows[0];
                            if (biddingDate == row["MaintenanceStartDate"].ToString().Trim())
                            {
                                if (row["MaintenanceStartHour"].ToString().Trim() == (h + 1).ToString())
                                    for (int g = h; g < Hour; g++)
                                        VI_Plant_unit[j, i, g] = 0;
                            }
                            else if (biddingDate == row["MaintenanceEndDate"].ToString().Trim())
                            {
                                if (row["MaintenanceEndHour"].ToString().Trim() == (h + 1).ToString())
                                    for (int g = 0; g <= h; g++)
                                        VI_Plant_unit[j, i, g] = 0;
                            }
                        }

                    }

                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Error_e = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            Error_e[j, i, h] = 0;
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Pminchek = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Pminchek[j, k, h] = 0;
                    }
                }
            }
            //*******************************************************************************************************

            try
            {
                Cplex cplex = new Cplex();


                // define variable :
                IIntVar[, ,] V_Plant_unit = new IIntVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            V_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                IIntVar[, ,] X_Plant_unit = new IIntVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if (Unit_Dec[j, i, 1].ToString().Trim() == "CC")
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                X_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                IIntVar[, ,] Y_Plant_unit = new IIntVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Y_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                IIntVar[, ,] Z_Plant_unit = new IIntVar[Plants_Num, Units_Num, 24];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < 24; h++)
                        {
                            Z_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddLe(V_Plant_unit[j, i, h], VI_Plant_unit[j, i, h]);
                        }
                    }
                }

                ////////////////////////////////////////////////////////////////////////////
                // define power(i,h):
                INumVar[, ,] Power_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Power_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Reserve(i,h):
                INumVar[, ,] Pres_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Pres_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define delta(i,h,m):
                INumVar[, , ,] delta_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour, Mmax];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int m = 0; m < Mmax; m++)
                            {
                                delta_Plant_unit[j, i, h, m] = cplex.NumVar(0.0, double.MaxValue);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Cost(i,h):
                INumVar[, ,] Cost_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Cost_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Benefit(i,h):
                INumVar[, ,] Income_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Income_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define CostStart(i,h):
                INumVar[, ,] Benefit_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Benefit_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[, ,] CostStart_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            CostStart_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }

                //**************************************************************************************************
                //  define Sum_Cost_day :
                ILinearNumExpr Sum_Cost_day = cplex.LinearNumExpr();

                double _Sum_Cost_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Cost_day.AddTerm(_Sum_Cost_day, Cost_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //  define Sum_CostStart_day :
                ILinearNumExpr Sum_CostStart_day = cplex.LinearNumExpr();

                double _Sum_CostStart_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_CostStart_day.AddTerm(_Sum_CostStart_day, Cost_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //  define Sum_Power_day :
                ILinearNumExpr Sum_Power_day = cplex.LinearNumExpr();
                double _Sum_Power_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Power_day.AddTerm(_Sum_Power_day, Power_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //  define Sum_Income_day :
                ILinearNumExpr Sum_Income_day = cplex.LinearNumExpr();

                double _Sum_Income_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Income_day.AddTerm(_Sum_Income_day, Income_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //  define Sum_Benefit_day :
                ILinearNumExpr Sum_Benefit_day = cplex.LinearNumExpr();

                double _Sum_Benefit_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Benefit_day.AddTerm(_Sum_Benefit_day, Benefit_Plant_unit[j, i, h]);
                        }
                    }
                }
                //*******************************************************************************************************
                //define Equation_(h):Income

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddEq(Income_Plant_unit[j, i, h], cplex.Prod((Price_Plant_unit[j, i, h]), Power_Plant_unit[j, i, h]));
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //define Equation_(h):Outservice
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddEq(Z_Plant_unit[j, i, h], Outservice_Plant_unit[j, i, h]);
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //define Equation_(h):Maintenance
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddLe(cplex.Sum(V_Plant_unit[j, i, h], Z_Plant_unit[j, i, h]), 1);
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //define Equation_(h):Profit
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddEq(Benefit_Plant_unit[j, i, h], cplex.Diff(Income_Plant_unit[j, i, h], cplex.Sum(CostStart_Plant_unit[j, i, h], Cost_Plant_unit[j, i, h])));
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //define Equation_(h):Pmin,Pmax
                //for (int j = 0; j < Plants_Num; j++)
                //{
                //    for (int i = 0; i < Plant_units_Num[j]; i++)
                //    {
                //        for (int h = 0; h < Hour; h++)
                //        {
                //            cplex.AddGe(Power_Plant_unit[j, i, h], cplex.Prod(Unit_Data[j, i, 3], V_Plant_unit[j, i, h]));
                //        }
                //    }
                //};

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddLe(Power_Plant_unit[j, i, h], cplex.Prod(Pmax_Plant_unit[j, i, h], V_Plant_unit[j, i, h]));
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):Delta
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int m = 0; m < Mmax; m++)
                            {
                                cplex.AddLe(delta_Plant_unit[j, i, h, m], Tbid_Plant_unit[j, i, h]);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):P_delta
                double _Sum_Delta_m = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            ILinearNumExpr Sum_Delta_m = cplex.LinearNumExpr();
                            for (int m = 0; m < Mmax; m++)
                            {
                                Sum_Delta_m.AddTerm(_Sum_Delta_m, delta_Plant_unit[j, i, h, m]);
                            }
                            cplex.AddEq(Power_Plant_unit[j, i, h], cplex.Sum(cplex.Prod(Unit_Data[j, i, 3], V_Plant_unit[j, i, h]), Sum_Delta_m));
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):Cost
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            //ILinearNumExpr DeltaBid_m = cplex.LinearNumExpr();
                            //for (int m = 0; m < Mmax; m++)
                            //{
                            //    DeltaBid_m.AddTerm(Fbid_Plant_unit[j, i, h, m], delta_Plant_unit[j, i, h, m]);
                            //}
                            //cplex.AddEq(Cost_Plant_unit[j, i, h], cplex.Sum(DeltaBid_m, cplex.Prod(Fmin_Plant_unit[j, i, h], V_Plant_unit[j, i, h])));
                            cplex.AddEq(Cost_Plant_unit[j, i, h], 0);
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^uncomment^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //define Equation_(h):Minupdown_1
                //for (int j = 0; j < Plants_Num; j++)
                //{
                //    for (int i = 0; i < Plant_units_Num[j]; i++)
                //    {
                //        for (int h = 0; h < Hisup_Plant_unit[j, i]; h++)
                //        {
                //            cplex.AddEq(V_Plant_unit[j, i, h], 1);
                //        }
                //    }
                //};

                ////^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //// define Equation_(h):Minupdown_2
                double _Minup_2 = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h <= (Hour - Unit_Data[j, i, 6]); h++)
                        {
                            ILinearNumExpr v_up = cplex.LinearNumExpr();
                            for (int hh = h; hh <= (h + Unit_Data[j, i, 6] - 1); hh++)
                            {
                                v_up.AddTerm(_Minup_2, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(v_up, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(V_Plant_unit[j, i, h], V_Plant_unit[j, i, h - 1])));
                            }
                            else
                            {
                                cplex.AddGe(v_up, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(V_Plant_unit[j, i, h], V_history_Plant_unit[j, i])));
                            }
                        }
                    }
                }
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                double _Mindown_2 = -1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h <= (Hour - (int)Unit_Data[j, i, 7]); h++)
                        {
                            ILinearNumExpr v_down = cplex.LinearNumExpr();
                            for (int hh = h; hh <= (h + Unit_Data[j, i, 7] - 1); hh++)
                            {
                                v_down.AddTerm(_Mindown_2, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(v_down, cplex.Prod(Unit_Data[j, i, 7], cplex.Diff(cplex.Diff(V_Plant_unit[j, i, h - 1], V_Plant_unit[j, i, h]), 1)));
                            }
                            else
                            {
                                cplex.AddGe(v_down, cplex.Prod(Unit_Data[j, i, 7], cplex.Diff(cplex.Diff(V_history_Plant_unit[j, i], V_Plant_unit[j, i, h]), 1)));
                            }
                        }
                    }
                }

                ////^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //// define Equation_(h):Minupdown_3
                double _Minup_h = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = (Hour - (int)Unit_Data[j, i, 6] + 1); h < Hour; h++)
                        {
                            ILinearNumExpr vh_up = cplex.LinearNumExpr();
                            for (int hh = h; hh < Hour; hh++)
                            {
                                vh_up.AddTerm(_Minup_h, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(cplex.Diff(vh_up, cplex.Diff(cplex.Prod((Hour - h), V_Plant_unit[j, i, h]), cplex.Prod((Hour - h), V_Plant_unit[j, i, h - 1]))), 0);
                            }
                            else
                            {
                                cplex.AddGe(cplex.Diff(vh_up, cplex.Diff(cplex.Prod((Hour - h), V_Plant_unit[j, i, h]), ((Hour - h) * V_history_Plant_unit[j, i]))), 0);
                            }
                        }
                    }
                }
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                double _Mindown_h = -1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = (Hour - (int)Unit_Data[j, i, 6] + 1); h < Hour; h++)
                        {
                            ILinearNumExpr vh_down = cplex.LinearNumExpr();
                            for (int hh = h; hh < Hour; hh++)
                            {
                                vh_down.AddTerm(_Mindown_h, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(cplex.Sum((Hour - h), cplex.Diff(vh_down, cplex.Diff(cplex.Prod((Hour - h), V_Plant_unit[j, i, h - 1]), cplex.Prod((Hour - h), V_Plant_unit[j, i, h])))), 0);
                            }
                            else
                            {
                                cplex.AddGe(cplex.Sum((Hour - h), cplex.Diff(vh_down, cplex.Diff(((Hour - h) * (1 - V_history_Plant_unit[j, i])), cplex.Prod((Hour - h), V_Plant_unit[j, i, h])))), 0);
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):Ramp Rate
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h < Hour; h++)
                        {
                            if (h < 23)
                            {
                                cplex.AddLe(Pres_Plant_unit[j, i, h], cplex.Sum(cplex.Prod(Pmax_Plant_unit[j, i, h] * 10, V_Plant_unit[j, i, h + 1]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h], V_Plant_unit[j, i, h + 1]))));
                            }

                            if ((h > 0))
                            {
                                cplex.AddLe(Power_Plant_unit[j, i, h], cplex.Sum(Power_Plant_unit[j, i, h - 1], cplex.Sum(cplex.Prod(Unit_Data[j, i, 1], V_Plant_unit[j, i, h]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h], V_Plant_unit[j, i, h - 1])), cplex.Prod(Pmax_Plant_unit[j, i, h] * 10, cplex.Diff(1, V_Plant_unit[j, i, h])))));
                                cplex.AddLe(cplex.Diff(Power_Plant_unit[j, i, h - 1], Power_Plant_unit[j, i, h]), cplex.Sum(cplex.Prod(Unit_Data[j, i, 2], V_Plant_unit[j, i, h]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h - 1], V_Plant_unit[j, i, h])), cplex.Diff(Pmax_Plant_unit[j, i, h] * 10, cplex.Diff(1, V_Plant_unit[j, i, h - 1]))));
                            }
                            if (h == 0)
                            {
                                cplex.AddLe(Power_Plant_unit[j, i, h], cplex.Sum(Powerhistory_Plant_unit[j, i], cplex.Sum(cplex.Prod(Unit_Data[j, i, 1], V_Plant_unit[j, i, h]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h], V_history_Plant_unit[j, i])), cplex.Prod(Pmax_Plant_unit[j, i, h] * 10, cplex.Diff(1, V_Plant_unit[j, i, h])))));
                                cplex.AddLe(cplex.Diff(Powerhistory_Plant_unit[j, i], Power_Plant_unit[j, i, h]), cplex.Sum(cplex.Prod(Unit_Data[j, i, 2], V_Plant_unit[j, i, h]), cplex.Sum(cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_history_Plant_unit[j, i], V_Plant_unit[j, i, h])), (Pmax_Plant_unit[j, i, h] * 10 * (1 - V_history_Plant_unit[j, i])))));
                            }
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):CC_power
                double _Unit_Gas_CC = 0.5;
                for (int h = 0; h < Hour; h++)
                {
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            if (Package[j, k].ToString().Trim() == "CC")
                            {
                                ILinearNumExpr Unit_Gas_CC = cplex.LinearNumExpr();
                                {
                                    for (int i = 0; i < Plant_units_Num[j]; i++)
                                    {
                                        if ((Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Data[j, i, 0] == k) & (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                                        {
                                            Unit_Gas_CC.AddTerm(_Unit_Gas_CC, Power_Plant_unit[j, i, h]);
                                        }
                                    }
                                }
                                for (int i = 0; i < Plant_units_Num[j]; i++)
                                {
                                    if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam") & (Unit_Data[j, i, 0] == k) & (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                                    {
                                        cplex.AddGe(Unit_Gas_CC, cplex.Diff(Power_Plant_unit[j, i, h], cplex.Prod(cplex.Diff(1, V_Plant_unit[j, i, h]), Pmax_Plant_unit[j, i, h])));
                                    }
                                }
                            }
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):CC_X
                double _X_1 = 1;
                double _X_2 = 1;

                int[] i_Start = new int[1];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                        {
                            i_Start[0] = GastostartSteam;
                        }
                        if (i_Start[0] == 0)
                        {
                            i_Start[0] = GastostartSteam;
                        }
                    }
                }
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                double[] _X_Start = new double[1];
                double _Pmax_Steam = 0;
                _X_Start[0] = Math.Pow(i_Start[0], -1);
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if (Unit_Dec[j, k, 1].ToString().Trim() == "CC")
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                for (int i = 0; i < Plant_units_Num[j]; i++)
                                {
                                    if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Data[j, i, 0] == (k)))
                                    {
                                        if (h < (i_Start[0] + 1))
                                        {
                                            ILinearNumExpr X_1 = cplex.LinearNumExpr();
                                            for (int hh = 0; hh <= h; hh++)
                                            {
                                                X_1.AddTerm(_X_1, V_Plant_unit[j, i, hh]);
                                            }
                                            cplex.AddLe(cplex.Diff(cplex.Prod(_X_Start[0], cplex.Sum(X_1, hisstart_Plant_unit[j, i])), 1), X_Plant_unit[j, i, h]);
                                            cplex.AddLe(X_Plant_unit[j, i, h], cplex.Prod(_X_Start[0], cplex.Sum(X_1, hisstart_Plant_unit[j, i])));

                                        }
                                        if (h > (i_Start[0]))
                                        {
                                            ILinearNumExpr X_2 = cplex.LinearNumExpr();
                                            for (int hh = h - i_Start[0]; hh <= h; hh++)
                                            {

                                                X_2.AddTerm(_X_2, V_Plant_unit[j, i, hh]);
                                            }

                                            cplex.AddLe(cplex.Diff(cplex.Prod(_X_Start[0], X_2), 1), X_Plant_unit[j, i, h]);
                                            cplex.AddLe(X_Plant_unit[j, i, h], cplex.Prod(_X_Start[0], X_2));
                                        }
                                    }
                                }

                                for (int ii = 0; ii < Plant_units_Num[j]; ii++)
                                {
                                    if ((Unit_Dec[j, ii, 1].ToString().Trim() == "CC") & (Unit_Dec[j, ii, 0].ToString().Trim() == "Steam") & (Unit_Data[j, ii, 0] == (k)))
                                    {
                                        _Pmax_Steam = Pmax_Plant_unit[j, ii, h];
                                        ILinearNumExpr X_CC = cplex.LinearNumExpr();
                                        for (int i = 0; i < Plant_units_Num[j]; i++)
                                        {
                                            if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Data[j, i, 0] == (k)))
                                            {
                                                X_CC.AddTerm(_Pmax_Steam * 0.5, X_Plant_unit[j, i, h]);
                                            }
                                        }
                                        cplex.AddGe(X_CC, Power_Plant_unit[j, ii, h]);
                                    }
                                }
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):Start up
                //double _V_Start1 = 1;
                //double _V_Start2 = 1;

                //for (int j = 0; j < Plants_Num; j++)
                //{
                //    for (int i = 0; i < Plant_units_Num[j]; i++)
                //    {
                //        for (int h = 0; h < Hour; h++)
                //        {
                //            for (int t = 0; t <= h; t++)
                //            {
                //                if ((t < h) & (t <= Unit_Data[j, i, 16]))
                //                {
                //                    ILinearNumExpr V_Start1 = cplex.LinearNumExpr();
                //                    for (int tt = 0; tt <= t; tt++)
                //                    {
                //                        V_Start1.AddTerm(_V_Start1, V_Plant_unit[j, i, (h - tt - 1)]);
                //                    }
                //                    cplex.AddGe(CostStart_Plant_unit[j, i, h], cplex.Prod(Fstart_Plant_unit[j, i, t], cplex.Diff(V_Plant_unit[j, i, h], V_Start1)));
                //                }
                //                if ((t == h) & (V_history_Plant_unit[j, i] == 0) & (t <= Unit_Data[j, i, 16]))
                //                {
                //                    ILinearNumExpr V_Start2 = cplex.LinearNumExpr();
                //                    for (int tt = 0; tt < (t + 1); tt++)
                //                    {
                //                        V_Start2.AddTerm(_V_Start2, V_Plant_unit[j, i, (h - tt)]);
                //                    }
                //                    cplex.AddGe(CostStart_Plant_unit[j, i, h], cplex.Prod(Fstart_Plant_unit[j, i, (t + HisShut_Plant_unit[j, i])], cplex.Diff(V_Plant_unit[j, i, h], V_Start2)));
                //                }
                //            }
                //        }
                //    }
                //}

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):Inc_Dec_Ramp
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            if (h > 0)
                            {
                                cplex.AddLe(Y_Plant_unit[j, i, h], cplex.Sum(1, cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Power_Plant_unit[j, i, h - 1]))));
                                cplex.AddGe(Y_Plant_unit[j, i, h], cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Power_Plant_unit[j, i, h - 1])));
                            }
                            if (h == 0)
                            {
                                cplex.AddLe(Y_Plant_unit[j, i, h], cplex.Sum(1, cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Powerhistory_Plant_unit[j, i]))));
                                cplex.AddGe(Y_Plant_unit[j, i, h], cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Powerhistory_Plant_unit[j, i])));
                            }
                        }
                    }
                }
                ////^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //// define Equation_(h):Inc_Dec_Ramp-2
                double _Inc_Ramp_2 = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < (Hour - (int)Unit_Data[j, i, 6]); h++)
                        {
                            ILinearNumExpr Y_up = cplex.LinearNumExpr();
                            for (int hh = h + 1; hh <= (h + Unit_Data[j, i, 6]); hh++)
                            {
                                Y_up.AddTerm(_Inc_Ramp_2, Y_Plant_unit[j, i, hh]);
                            }
                            if (h < 23)
                            {
                                cplex.AddGe(Y_up, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(Y_Plant_unit[j, i, h + 1], Y_Plant_unit[j, i, h])));
                            }
                        }
                    }
                }

                ////^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //// define Equation_(h):Inc_Dec_Ramp_3
                double _Dec_Ramp_2 = -1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h < (Hour - (int)Unit_Data[j, i, 6]); h++)
                        {
                            ILinearNumExpr Y_down = cplex.LinearNumExpr();
                            for (int hh = (h + 1); hh <= (h + Unit_Data[j, i, 6]); hh++)
                            {
                                Y_down.AddTerm(_Dec_Ramp_2, Y_Plant_unit[j, i, hh]);
                            }
                            if (h < 23)
                            {
                                cplex.AddGe(Y_down, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(cplex.Diff(Y_Plant_unit[j, i, h], Y_Plant_unit[j, i, (h + 1)]), 1)));
                            }
                        }
                    }
                }
                ////^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _Inc_Ramp_3 = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = (Hour - (int)Unit_Data[j, i, 6]); h < Hour; h++)
                        {
                            ILinearNumExpr YY_up = cplex.LinearNumExpr();
                            for (int hh = (h + 1); hh < Hour; hh++)
                            {
                                YY_up.AddTerm(_Inc_Ramp_3, Y_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(cplex.Diff(YY_up, cplex.Diff(cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, h]), cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, h - 1]))), 0);
                            }
                        }
                    }
                }

                double _Dec_Ramp_3 = -1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = (Hour - (int)Unit_Data[j, i, 6]); h < Hour; h++)
                        {
                            ILinearNumExpr Yh_down = cplex.LinearNumExpr();
                            for (int hh = (h + 1); hh < Hour; hh++)
                            {
                                Yh_down.AddTerm(_Dec_Ramp_3, Y_Plant_unit[j, i, hh]);
                            }
                            if (h < 23)
                            {
                                cplex.AddGe(cplex.Sum((Hour - h - 1), cplex.Diff(Yh_down, cplex.Diff(cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, h]), cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, (h + 1)])))), 0);
                            }
                        }
                    }
                }
                //*****************************************uncomment * *********************************************************
                cplex.SetParam(Cplex.DoubleParam.EpGap, 0.01);
                cplex.SetParam(Cplex.DoubleParam.EpOpt, 0.01);
                cplex.SetParam(Cplex.DoubleParam.EpAGap, 0.01);
                cplex.SetParam(Cplex.DoubleParam.TiLim, 1000000);
                cplex.SetParam(Cplex.IntParam.ItLim, 1000000);
                cplex.SetParam(Cplex.DoubleParam.WorkMem, 3000.0);
                cplex.SetParam(Cplex.StringParam.WorkDir, "C:\\ILOG");
                GC.Collect();

                cplex.AddMaximize(Sum_Benefit_day);
                //*******************************************************************************************************
                //Export

                if (cplex.Solve())
                {
                    CplexPower = 1;
                    //MessageBox.Show(" Value =  Calculate : PBUC    " + cplex.ObjValue + "   " + cplex.GetStatus());
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                Power_Error[j, i, h] = 0;
                                Power_memory[j, i, h] = cplex.GetValue(Power_Plant_unit[j, i, h]);

                                if (Power_memory[j, i, h] < 2)
                                {
                                    Power_Error[j, i, h] = 1;
                                }
                            }
                        }
                    }

                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                    for (int j = 0; j < Plants_Num; j++)
                    {

                        for (int h = 0; h < Hour; h++)
                        {
                            for (int i = 0; i < Plant_units_Num[j]; i++)
                            {
                                if ((Power_Error[j, i, h] == 0))
                                {
                                    Power_if_Pack[j, i, h] = Pmin_Plant_Pack[j, i, h];
                                    Power_memory_Pack[j, i, h] = Power_memory[j, i, h];
                                    Dispatch_Proposal[j, i, h] = Pmax_Plant_unit[j, i, h];
                                }
                                if ((Power_Error[j, i, h] == 1))
                                {
                                    Power_if_Pack[j, i, h] = Pmin_Plant_Pack[j, i, h];
                                    Dispatch_Proposal[j, i, h] = Pmax_Plant_unit[j, i, h];
                                }
                            }
                        }
                    }
                    


                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_units_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                Power_Error_Pack[j, k, h] = 0;
                                if (Power_memory_Pack[j, k, h] < 3)
                                {
                                    Power_Error_Pack[j, k, h] = 1;
                                }
                            }
                        }
                    }
                }
                cplex.End();
            }
            catch (ILOG.CPLEX.Cplex.UnknownObjectException e1)
            {
                MessageBox.Show("concert exception" + e1.ToString() + "caught");
                return false;
            }
            catch (System.Exception ek)
            {
                MessageBox.Show(ek.Message.ToString());
                return false;
            }
          //  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            DataTable ddd = Utilities.GetTable("select ppid from powerplant");
            double[,] valhub1 = new double[ddd.Rows.Count, 24];
            double[,] valtrans1 = new double[ddd.Rows.Count, 24];

            double[, ,] valhub = new double[ddd.Rows.Count, 12, 3];
            double[, ,] valtrans = new double[ddd.Rows.Count, 12, 3];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        ///////////////////////////////////////////*NEW////////////////////////////////////////////
                        /////////////////////////////////hub-trans//////////////////////////////////////////////////

                        string refer = "";
                        string trans = "";
                        DataTable basetabledata2 = Utilities.GetTable("select Reference,transmission from dbo.BaseData where Date ='" + biddingDate + "'order by BaseID desc");
                        string year2 = biddingDate.Substring(0, 4).Trim();
                        DataTable xx2 = null;

                        ///////////////////////                          


                        string month = biddingDate.Substring(5, 2).Trim();
                        xx2 = Utilities.GetTable("select * from yeartrans where year='" + year2 + "' and ppid='" + plant[j] + "'");


                        if (MyDoubleParse(month) < 10)
                        {
                            month = MyDoubleParse(month).ToString();
                        }

                        for (int u = 0; u < 12; u++)
                        {
                            if (xx2 != null && xx2.Rows.Count > 0)
                            {
                                valtrans[j, u, 0] = MyDoubleParse(xx2.Rows[0]["M" + (u + 1) + "Low"].ToString());
                                valtrans[j, u, 1] = MyDoubleParse(xx2.Rows[0]["M" + (u + 1) + "Mid"].ToString());
                                valtrans[j, u, 2] = MyDoubleParse(xx2.Rows[0]["M" + (u + 1) + "Peak"].ToString());
                            }
                        }


                        DataTable zx = Utilities.GetTable("select H" + ((h + 1).ToString()) + " from dbo.bourse where fromdate<='" + biddingDate + "' and todate>='" + biddingDate + "' order by fromdate,id desc");
                        if (zx.Rows.Count == 0)
                        {
                            zx = Utilities.GetTable("select H" + ((h + 1).ToString()) + " from dbo.bourse where fromdate<='" + biddingDate + "'");
                        }
                        string c = "";
                        if (zx.Rows.Count > 0)
                        {
                            c = zx.Rows[0][0].ToString().Trim();
                            if (c == "Medium")
                            {
                                valtrans1[j, h] = 0.01 * valtrans[j, int.Parse(month) - 1, 1];
                            }
                            else if (c == "Base")
                            {
                                valtrans1[j, h] = 0.01 * valtrans[j, int.Parse(month) - 1, 0];
                            }
                            else if (c == "peak")
                            {
                                valtrans1[j, h] = 0.01 * valtrans[j, int.Parse(month) - 1, 2];
                            }
                        }

                      //if (Power_memory_Pack[j, k, h] > Pmax_Plant_Pack[j, k, h])
                      //{
                      //   Power_memory_Pack[j, k, h] = Pmax_Plant_Pack[j, k, h];
                      //}

                      //if (Power_memory_Pack[j, k, h] > 5)
                      //{
                      //    Power_memory_Pack[j, k, h] = (Power_memory_Pack[j, k, h] * (1 - valtrans1[j, h]) - 5);
                      //}

                      if (Power_memory_Pack[j, k, h] > Pmax_Plant_Pack[j, k, h])
                      {
                          Power_memory_Pack[j, k, h] = Pmax_Plant_Pack[j, k, h];
                      }
                      if (Dispatch_Proposal[j, k, h] > Pmax_Plant_Pack[j, k, h])
                      {
                          Dispatch_Proposal[j, k, h] = Pmax_Plant_Pack[j, k, h];
                      }
                      if (Power_if_Pack[j, k, h] > 0.8 * Pmax_Plant_Pack[j, k, h])
                      {
                          Power_if_Pack[j, k, h] = 0.75 * Pmax_Plant_Pack[j, k, h];
                      }
                      if (Power_if_Pack[j, k, h] < Pmin_Plant_Pack[j, k, h])
                      {
                          Power_if_Pack[j, k, h] = 0.75 * Pmax_Plant_Pack[j, k, h];
                      }
                  }
              }
          }
            
            //*******************************************************************************************************
            //Export
            // MessageBox.Show(" Value =   Power:   ");
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
            int[, ,] checklist = new int[Plants_Num, Units_Num, Hour];
            double[,] Value_ON = new double[Plants_Num, Hour];
            double[, ,] Dec_Fuel = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        checklist[j, i, h] = 0;
                        Dec_Fuel[j, i, h] = 0;
                        Value_ON[j, h] = CapPrice[0];
                    }
                }
            }


            try
            {
                double[,] riskplant = new double[Plants_Num, 24];
                for (int j = 0; j < Plants_Num; j++)
                {
                    DataTable drisk = Utilities.GetTable("select * from risk where ppid='" + plant[j] + "'order by hour asc");
                    for (int h = 0; h < 24; h++)
                    {
                        if ((drisk.Rows.Count == 0) || (setparam[0, 3]== false))
                        {
                            riskplant[j, h] = -1;
                        }
                        else
                        {
                            riskplant[j, h] = 77 *MyDoubleParse(drisk.Rows[h][2].ToString());
                        }
                        for (int k = 0; k < Plant_units_Num[j]; k++)
                        {
                            if ((riskplant[j, h] <= -1))
                            {
                                for (int kk = 0; kk < Plant_units_Num[j]; kk++)
                                {
                                    //checklist[j, kk, h] = 1;
                                    //Dec_Fuel[j, kk, h] = -riskplant[j, h] - 1;
                                }
                                PriceForecasting[j, k, h] = Math.Round((price_hist[j, k, h] + Toreranceup[j, k, h]) / 1000) * 1000;
                                price_Steam[j, k, h] = Math.Round((price_Steam[j, k, h] + Toreranceup[j, k, h]) / 1000) * 1000;
                            }
                            else if ((riskplant[j, h] < 1) && (riskplant[j, h] > -1))
                            {
                                PriceForecasting[j, k, h] = Math.Round((price_hist[j, k, h] - riskplant[j, h] * 100000) / 1000) * 1000;
                                price_Steam[j, k, h] = Math.Round((price_Steam[j, k, h] - riskplant[j, h] * 100000) / 1000) * 1000;
                            }
                            else if ((riskplant[j, h] >= 1))
                            {
                                PriceForecasting[j, k, h] = Math.Round((CapPrice[0] - (riskplant[j, h]  - 1) * 100000) / 1000) * 1000;
                                price_Steam[j, k, h] = Math.Round((CapPrice[0] - (riskplant[j, h]  - 1) * 100000) / 1000) * 1000;
                            }
                            else
                            {
                                PriceForecasting[j, k, h] = Math.Round((price_hist[j, k, h] + Toreranceup[j, k, h]) / 1000) * 1000;
                                price_Steam[j, k, h] = Math.Round((price_Steam[j, k, h] + Toreranceup[j, k, h]) / 1000) * 1000;
                            }
                        }
                    }
                }
            }
            catch
            {

            }

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Dispatch_Proposal[j, i, h] = Pmax_Plant_unit[j, i, h] - Dec_Fuel[j, i, h];
                    }
                }
            }

            double[,] Presolve_unit = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                string blockM005 = "";
                string PlantM005 = "";
                string selectedDate1 = biddingDate;
                try
                {
                    string status = valtrain(plant[j], selectedDate1);
                    string[] split = status.Split('-');

                    if (status.Contains("Presolve-Unit"))
                    {
                        PlantM005 = plant[j];
                        DataSet myDs1 = UtilityPowerPlantFunctions.GetdefaultUnits(Convert.ToInt32(PlantM005), split[2].Trim());
                        DataTable[] orderedPackages1 = UtilityPowerPlantFunctions.OrderPackages(myDs1);
                        foreach (DataTable dtPackageType1 in orderedPackages1)
                        {
                            int packageOrder = 0;
                            if (dtPackageType1.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                            orderedPackages1.Length > 1)
                                packageOrder = 1;

                            foreach (DataRow unitRow in dtPackageType1.Rows)
                            {
                                string package = unitRow["PackageType"].ToString().Trim();
                                string unit = unitRow["UnitCode"].ToString().Trim();
                                string packagecode = unitRow["PackageCode"].ToString().Trim();
                                for (int k = 0; k < Plant_units_Num[j]; k++)
                                {
                                    if (Unit[j, k] == unit)
                                    {
                                        Presolve_unit[j, k] = 1;
                                    }
                                }
                            }
                        }
                    }
                    else if (status.Contains("Presolve - plant"))
                    {
                        for (int k = 0; k < Plant_units_Num[j]; k++)
                        {
                                Presolve_unit[j, k] = 1;
                        }
                    }
                    else if (status.Contains("date"))
                    {
                        for (int k = 0; k < Plant_units_Num[j]; k++)
                        {
                            Presolve_unit[j, k] = 1;
                        }
                    }
                    else
                    {
                        for (int k = 0; k < Plant_units_Num[j]; k++)
                        {
                            Presolve_unit[j, k] = 1;
                        }
                    }

                }
                catch
                {

                }
            }


            int[] Ktest = new int[Plants_Num];


           
           // Gilan

            int test_Zero = 0;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        test_Zero = 0;

                        for (int ss = 0; ss < Step_Num; ss++)
                        {
                            if (Power[j, k, h, ss] > 2.9)
                            {
                                test_Zero = 1;
                            }
                        }
                        if (test_Zero == 0)
                        {
                            for (int ss = 0; ss < Step_Num; ss++)
                            {
                                Price[j, k, h, ss] = 0;
                                Power[j, k, h, ss] = 0;
                            }
                        }
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (checklist[j, i, h] == 1)
                        {
                            price_Steam[j, i, h] =  1000*Math.Round(Value_ON[j, h]/1000);
                        }
                        if (price_Steam[j, i, h] > CapPrice[0])
                        {
                            price_Steam[j, i, h] =  1000*Math.Round(CapPrice[0]/1000);
                        }
                    }
                }
            }
 
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
            double[, ,] Dispatch_Clear = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Dispatch_Clear[j, i, h] = 0;
                        if ((Outservice_Plant_unit[j, i, h] == 0))
                        {
                            Dispatch_Clear[j, i, h] =  Pmax_Plant_unit[j, i, h] - Dec_Fuel[j, i, h];
                        }
                    }
                }
            }
            //////////////////////////////////////////////////////////////////////
            double Ul_Fact = 5;

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int s = 0; s < Step_Num; s++)
                        {
                            if (plant[j] == "206")
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] - Ul_Fact * ErrorBid[0];
                                    Power[j, k, h, s] = 1;
                                }
                                if ((s >= 1) && (s < 9))
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] + (s + 0) * 3 * ErrorBid[0];
                                    Power[j, k, h, s] = Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h])- 10 + s;
                                }
                                if (s==9)
                                {
                                    Price[j, k, h, s] =CapPrice[0];
                                    Power[j, k, h, s] = Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) ;
                                }
                            }
                        }
                    }
                }
            }
            // Saina - 14 Feb - Elghaeeeeeee

             
            //for (int j = 0; j < Plants_Num; j++)
            //{
            //    for (int k = 0; k < Plant_units_Num[j]; k++)
            //    {
            //        for (int h = 0; h < Hour; h++)
            //        {
            //            if (k != 1)
            //            {
            //                Presolve_unit[j, k] = 0;
            //                Presolve_unit[j, 1] = 1;
            //            }
            //        }
            //    }
            //}
            double[, ,] xchange_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    if ((Presolve_unit[j, 1] != 0) && (Dispatch_Clear[j, k, 15] > 0))
                    {
                        for (int hh = 0; hh < Hour; hh++)
                        {
                            xchange_unit[j, k, hh] = XBourse[j, hh];
                        }
                        break;
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
            double[] Distance = new double[Hour]; //pmousavi-1397/02/09

            for (int h = 0; h < Hour; h++)
            {
                Distance[h] = 3000;
                if (PriceForecasting[0, 0, h]> 405000)
                {
                    Distance[h] = 1000;
                }
                else if (PriceForecasting[0, 0, h] > 390000)
                {
                    Distance[h] = 2000;
                }
            }

            //////////////////////////////////////////////////////////////////////
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int s = 0; s < Step_Num; s++)
                        {
                            if (plant[j] == "707")
                            {
                                if (Presolve_unit[j, k] == 1)
                                {
                                    if ((setparam[0, 2] == true) && (price_hist[j, k, h] > CapPrice[0] - 6000))
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] - 6000;
                                            Power[j, k, h, s] = 1;
                                        }
                                        if ((s > 0) && (s < 5))
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 1) * Distance[h];
                                            Power[j, k, h, s] = 1 + s;
                                        }
                                        if (s == 5)
                                        {
                                            Price[j, k, h, s] = Math.Round(CapPrice[0]);
                                            Power[j, k, h, s] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                        }
                                        if (s > 5)
                                        {
                                            Price[j, k, h, s] = 0;
                                            Power[j, k, h, s] = 0;
                                        }
                                    }
                                    else
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] - 6000;
                                            Power[j, k, h, s] = 1;
                                        }
                                        if ((setparam[0, 5] == true) && (setparam[0, 2] == false))
                                        {
                                            if (s == 1)
                                            {
                                                Price[j, k, h, s] = PriceForecasting[j, k, h] - 1000;
                                                Power[j, k, h, s] = Math.Round(Pmin_Plant_Pack[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                            }
                                            if ((s > 1) && (s < 10))
                                            {
                                                Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 2) * Distance[h];
                                                Power[j, k, h, s] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050)) - 9 + s;
                                            }
                                        }
                                        else
                                        {
                                            if ((s > 1) && (s < 10))
                                            {
                                                Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 2) * Distance[h];
                                                Power[j, k, h, s] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050)) - 9 + s;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (xchange_unit[j, k, h] > 0)
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] - 6000;
                                            Power[j, k, h, s] = xchange_unit[j, k, h];
                                        }
                                        if (s == 1)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] - 4000;
                                            Power[j, k, h, s] = xchange_unit[j, k, h]+1;
                                        }
                                        if ((setparam[0, 5] == false) && (setparam[0, 2] == false))
                                        {
                                            if (s == 2)
                                            {
                                                Price[j, k, h, s] = PriceForecasting[j, k, h] - 1000;
                                                Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                            }
                                            if (s > 2)
                                            {
                                                Price[j, k, h, s] = 0;
                                                Power[j, k, h, s] = 0;
                                            }
                                        }
                                        else if ((setparam[0, 5] == true) && (setparam[0, 2] == false))
                                        {
                                            if (s == 2)
                                            {
                                                Price[j, k, h, s] = PriceForecasting[j, k, h] - 1000;
                                                Power[j, k, h, s] =  Math.Max((Math.Round(Pmin_Plant_Pack[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050))),xchange_unit[j, k, h]+2);
                                            }
                                            if (s == 3)
                                            {
                                                Price[j, k, h, s] = PriceForecasting[j, k, h] ;
                                                Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                            }
                                            if (s > 3)
                                            {
                                                Price[j, k, h, s] = 0;
                                                Power[j, k, h, s] = 0;
                                            }
                                        }
                                        else if ((setparam[0, 2] == true) && (price_hist[j, k, h] > CapPrice[0] - 6000))
                                        {
                                            if (s == 2)
                                            {
                                                Price[j, k, h, s] = Math.Round(CapPrice[0]) - 1000;
                                                Power[j, k, h, s] = Math.Max((Math.Round(Pmin_Plant_Pack[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050))),xchange_unit[j, k, h]+2);
                                            }
                                            if (s == 3)
                                            {
                                                Price[j, k, h, s] = Math.Round(CapPrice[0]);
                                                Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                            }
                                            if (s > 3)
                                            {
                                                Price[j, k, h, s] = 0;
                                                Power[j, k, h, s] = 0;
                                            }
                                        }
                                        else
                                        {
                                            if (s == 2)
                                            {
                                                Price[j, k, h, s] = PriceForecasting[j, k, h] - 1000;
                                                Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                            }
                                            if (s > 2)
                                            {
                                                Price[j, k, h, s] = 0;
                                                Power[j, k, h, s] = 0;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] - 4000;
                                            Power[j, k, h, s] = 1;
                                        }
                                        if ((setparam[0, 5] == false) && (setparam[0, 2] == false))
                                        {
                                            if (s == 1)
                                            {
                                                Price[j, k, h, s] = PriceForecasting[j, k, h] - 1000;
                                                Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                            }
                                            if (s > 1)
                                            {
                                                Price[j, k, h, s] = 0;
                                                Power[j, k, h, s] = 0;
                                            }
                                        }
                                        else if ((setparam[0, 5] == true) && (setparam[0, 2] == false))
                                        {
                                            if (s == 1)
                                            {
                                                Price[j, k, h, s] = PriceForecasting[j, k, h] - 1000;
                                                Power[j, k, h, s] = Math.Round(Pmin_Plant_Pack[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                            }
                                            if (s == 2)
                                            {
                                                Price[j, k, h, s] = PriceForecasting[j, k, h];
                                                Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                            }
                                            if (s > 2)
                                            {
                                                Price[j, k, h, s] = 0;
                                                Power[j, k, h, s] = 0;
                                            }
                                        }
                                        else if ((setparam[0, 2] == true) && (price_hist[j, k, h] > CapPrice[0] - 6000))
                                        {
                                            if (s == 1)
                                            {
                                                Price[j, k, h, s] = Math.Round(CapPrice[0]) - 1000;
                                                Power[j, k, h, s] = Math.Round(Pmin_Plant_Pack[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                            }
                                            if (s == 2)
                                            {
                                                Price[j, k, h, s] = Math.Round(CapPrice[0]);
                                                Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                            }
                                            if (s > 2)
                                            {
                                                Price[j, k, h, s] = 0;
                                                Power[j, k, h, s] = 0;
                                            }
                                        }
                                        else
                                        {
                                            if (s == 1)
                                            {
                                                Price[j, k, h, s] = PriceForecasting[j, k, h] - 1000;
                                                Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                            }
                                            if (s > 1)
                                            {
                                                Price[j, k, h, s] = 0;
                                                Power[j, k, h, s] = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // Saina - 14 Feb - Elghaeeeeeee

           //  {
            //for (int j = 0; j < Plants_Num; j++)
            //{
            //    for (int k = 0; k < Plant_units_Num[j]; k++)
            //    {
            //        for (int h = 0; h < Hour; h++)
            //        {
            //            if (k != 1)
            //            {
            //                Presolve_unit[j, k] = 0;
            //                Presolve_unit[j, 1] = 1;
            //            }
            //        }
            //    }
            //}


            //risk added////////////////////////////////////////////////////////////////////////


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int s = 0; s < Step_Num; s++)
                        {
                            if (((plant[j] == "701") || (plant[j] == "980")) && (Unit_Dec[j, k, 0] == "Gas"))
                            {
                                if (Presolve_unit[j, k] == 1)
                                {
                                    if (checklist[j, k, h] == 1)
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] - 7000 ;
                                            Power[j, k, h, s] = 1;
                                        }
                                        if ((s > 0) && (s < 8))
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 1) * 3 * 1000 ;
                                            Power[j, k, h, s] = 1 + 1* s;
                                        }
                                        if (s == 8)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h]- 1000 ;
                                            Power[j, k, h, s] = 84;
                                        }
                                        if (s == 9)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] + 1000 ;
                                            Power[j, k, h, s] = Pmax_Plant_unit[j, k, h] ;
                                        }

                                    }
                                    else
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] - 7000 ;
                                            Power[j, k, h, s] = 1;
                                        }
                                        if (s == 1)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] - 3000;
                                            Power[j, k, h, s] = 2;
                                        }
                                        if (s == 2)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] -1000 ;
                                            Power[j, k, h, s] = 84;
                                        }
                                        if (s == 3)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] ;
                                            Power[j, k, h, s] =Math.Round (Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.015)) - 9 + s;
                                        }
                                        if ((s > 3) && (s < 10))
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 3) * 2 *1000 ;
                                            Power[j, k, h, s] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.015)) - 9 + s;
                                        }
                                    }
                                }
                                else
                                {
                                    if (checklist[j, k, h] == 1)
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] - 11000;
                                            Power[j, k, h, s] = 1;
                                        }
                                        if (s == 1)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] - 1000*k;
                                            Power[j, k, h, s] = 84;
                                        }
                                        if (s == 2)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h];
                                            Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h]);
                                        }

                                        if (s > 2)
                                        {
                                            Price[j, k, h, s] = 0;
                                            Power[j, k, h, s] = 0;
                                        }
                                    }
                                    else
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] - 7000;
                                            Power[j, k, h, s] = 1;
                                        }
                                        if (s == 1)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] - 1000;
                                            Power[j, k, h, s] = 84;
                                        }
                                        if (s == 2)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] ;
                                            Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h]);
                                        }
                                        if (s > 2)
                                        {
                                            Price[j, k, h, s] = 0;
                                            Power[j, k, h, s] = 0;
                                        }
                                    }
                                }
                            }
                            if (((plant[j] == "701") || (plant[j] == "980")) && (Unit_Dec[j, k, 0] == "Steam"))
                            {
                                if (checklist[j, k, h] == 1)
                                {
                                    if ((Dispatch_Clear[j, k, h] > 30) && (Dispatch_Clear[j, k, h] < 70))
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] - 11000 ;
                                            Power[j, k, h, s] = 1;
                                        }
                                        if (s == 1)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] -1000*k;
                                            Power[j, k, h, s] = 40;
                                        }
                                        if (s == 2)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] ;
                                            Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h]);
                                        }
                                        if (s > 2)
                                        {
                                            Price[j, k, h, s] = 0;
                                            Power[j, k, h, s] = 0;
                                        }
                                    }
                                    else
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] -11000 ;
                                            Power[j, k, h, s] = 1;
                                        }
                                        if (s == 1)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] - 1000*k;
                                            Power[j, k, h, s] = 84;
                                        }
                                        if (s == 2)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] ;
                                            Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h]) ;
                                        }
                                        if (s > 2)
                                        {
                                            Price[j, k, h, s] = 0;
                                            Power[j, k, h, s] = 0;
                                        }
                                    }
                                }
                                else
                                {
                                    if ((Dispatch_Clear[j, k, h] > 30) && (Dispatch_Clear[j, k, h] < 70))
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] - 7000 ;
                                            Power[j, k, h, s] = 1;
                                        }
                                        if (s == 1)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] -1000;
                                            Power[j, k, h, s] = 40;
                                        }
                                        if (s == 2)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] ;
                                            Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h]);
                                        }
                                        if (s > 2)
                                        {
                                            Price[j, k, h, s] = 0;
                                            Power[j, k, h, s] = 0;
                                        }
                                    }
                                    else
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] - 7000 ;
                                            Power[j, k, h, s] = 1;
                                        }
                                        if (s == 1)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] -1000;
                                            Power[j, k, h, s] = 84;
                                        }
                                        if (s == 2)
                                        {
                                            Price[j, k, h, s] = price_Steam[j, k, h] ;
                                            Power[j, k, h, s] =  Math.Round(Pmax_Plant_unit[j, k, h]);
                                        }
                                        if (s > 2)
                                        {
                                            Price[j, k, h, s] = 0;
                                            Power[j, k, h, s] = 0;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int s = 1; s < Step_Num; s++)
                        {
                            if ((Power[j, k, h, s] == 0) & (Power[j, k, h, s - 1] > 0) & (Math.Round(Power[j, k, h, s - 1]) < Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050))))
                            {
                                Power[j, k, h, s] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                Price[j, k, h, s] = Price[j, k, h, s - 1];
                                for (int ss = 0; ss < s; ss++)
                                {
                                    Price[j, k, h, ss] = Price[j, k, h, ss] - 1 * ErrorBid[0];
                                }
                                if (Price[j, k, h, s] > CapPrice[0])
                                {
                                    Price[j, k, h, s] = CapPrice[0];
                                }
                            }
                            if ((s == Step_Num - 1) & (Math.Round(Power[j, k, h, s]) < Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050))) & (Power[j, k, h, s] > 0))
                            {
                                Power[j, k, h, s] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                Price[j, k, h, s] = Price[j, k, h, s];
                                if (Price[j, k, h, s] > CapPrice[0])
                                {
                                    Price[j, k, h, s] = CapPrice[0];
                                }
                            }
                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            int test_cap = 0;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        test_cap = 0;

                        if ((Dispatch_Proposal[j, k, h] > 0) & ((Power[j, k, h, 0] == 0) | (Price[j, k, h, 0] == 0)))
                        {
                            {
                                for (int s = 0; s < Step_Num; s++)
                                {
                                    if (s == 0)
                                    {
                                        Price[j, k, h, s] = price_Steam[j, k, h];
                                        Power[j, k, h, s] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                    }
                                    {
                                        if (s > 0)
                                        {
                                            Price[j, k, h, s] = 0;
                                            Power[j, k, h, s] = 0;
                                        }
                              
                                    }
                                }
                            }

                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        test_cap = 0;
                        for (int s = 0; s < Step_Num; s++)
                        {
                            if ((Price[j, k, h, s] > CapPrice[0]) & (test_cap == 0))
                            {
                                Price[j, k, h, s] = (CapPrice[0]);
                                Power[j, k, h, s] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                if (s > 0)
                                {
                                    for (int ss = 0; ss < s; ss++)
                                    {
                                        Price[j, k, h, ss] = Price[j, k, h, ss] - 1 * ErrorBid[0];
                                    }
                                }
                                test_cap = 1;
                                if (s < 9)
                                {
                                    for (int ss = (s + 1); ss < Step_Num; ss++)
                                    {
                                        Price[j, k, h, ss] = 0;
                                        Power[j, k, h, ss] = 0;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //////////////////////////////////////////NEW//////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////
            if (CplexPower == 0)
            {
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int s = 0; s < Step_Num; s++)
                            {
                                Price[j, k, h, s] = 0;
                                Power[j, k, h, s] = 0;
                            }
                        }
                    }
                }
                MessageBox.Show("Operational Information is not Correct");
                return false;
            }
            for (int h = 0; h < Hour; h++)
            {
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if ((Cap_Bid[j, k, h] == "Yes"))
                        {
                            if ((Unit_Dec[j, k, 1].ToString().Trim() != "Gas"))
                            {
                                for (int s = 0; s < Step_Num; s++)
                                {
                                    if (s == 0)
                                    {
                                        Price[j, k, h, s] = CapPrice[0];
                                        Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                    }
                                    if (s > 0)
                                    {
                                        Price[j, k, h, s] = 0;
                                        Power[j, k, h, s] = 0;
                                    }
                                }
                            }
                            if ((Unit_Dec[j, k, 1].ToString().Trim() == "Gas"))
                            {
                                for (int s = 0; s < Step_Num; s++)
                                {
                                    if (s == 0)
                                    {
                                        Price[j, k, h, s] = CapPrice[0];
                                        Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                    }
                                    if (s > 0)
                                    {
                                        Price[j, k, h, s] = 0;
                                        Power[j, k, h, s] = 0;
                                    }
                                }
                            }

                        }
                    }
                }
            }



            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int s = 0; s < Step_Num; s++)
                        {
                            if (Outservice_Plant_unit[j, k, h] == 1)
                            {
                                if ((Unit_Dec[j, k, 1].ToString().Trim() == "Steam"))
                                {
                                    if (s == 0)
                                    {
                                        Price[j, k, h, s] = CapPrice[0];
                                        Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                    }
                                    if (s > 0)
                                    {
                                        Price[j, k, h, s] = 0;
                                        Power[j, k, h, s] = 0;
                                    }
                                }
                                if ((Unit_Dec[j, k, 1].ToString().Trim() == "CC"))
                                {
                                    if (s == 0)
                                    {
                                        Price[j, k, h, s] = CapPrice[0];
                                        Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                    }
                                    if (s > 0)
                                    {
                                        Price[j, k, h, s] = 0;
                                        Power[j, k, h, s] = 0;
                                    }
                                }
                                if ((Unit_Dec[j, k, 1].ToString().Trim() == "Gas"))
                                {
                                    if (s == 0)
                                    {
                                        Price[j, k, h, s] = CapPrice[0];
                                        Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                    }
                                    if (s > 0)
                                    {
                                        Price[j, k, h, s] = 0;
                                        Power[j, k, h, s] = 0;
                                    }
                                }
                               //Dispatch_Proposal[j, k, h] = 0;
                               Dispatch_Clear[j, k, h] = 0;
                            }
                        }
                    }
                }
            }


            //////////////////////////////////////////NEW////////////////////////////////////////////
            double[, , ,] Hisprice = new double[Plants_Num, Units_Num, Hour, Step_Num];
            for (int h = 0; h < Hour; h++)
            {
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        for (int s = 1; s < Step_Num; s++)
                        {
                            if ((Price[j, k, h, s - 1] == Price[j, k, h, s]) && (Price[j, k, h, s] != 0))
                            {
                                for (int ss = s; ss < Step_Num; ss++)
                                {
                                    Price[j, k, h, ss] = Price[j, k, h, ss] + ss * 1000;
                                }
                            }
                            if ((Price[j, k, h, s - 1] > Price[j, k, h, s]-1) && (Price[j, k, h, s] != 0))
                            {
                                Price[j, k, h, s] = Price[j, k, h, s - 1] + ErrorBid[0];
                            }
                            if ((Power[j, k, h, s - 1] > Power[j, k, h, s]) && (Power[j, k, h, s] != 0))
                            {
                                Hisprice[j, k, h, s] = Power[j, k, h, s - 1];
                                Power[j, k, h, s - 1] = Power[j, k, h, s];
                                Power[j, k, h, s] = Hisprice[j, k, h, s];
                                if (s > 1)
                                {
                                    for (int sa = 1; sa < s; sa++)
                                    {
                                        if ((Power[j, k, h, sa - 1] > Power[j, k, h, sa]) && (Power[j, k, h, sa] != 0))
                                        {
                                            Hisprice[j, k, h, sa] = Power[j, k, h, sa - 1];
                                            Power[j, k, h, sa - 1] = Power[j, k, h, sa];
                                            Power[j, k, h, sa] = Hisprice[j, k, h, sa];
                                        }
                                    }
                                    for (int sb = 1; sb < s; sb++)
                                    {
                                        if ((Power[j, k, h, sb - 1] > Power[j, k, h, sb]) && (Power[j, k, h, sb] != 0))
                                        {
                                            Hisprice[j, k, h, sb] = Power[j, k, h, sb - 1];
                                            Power[j, k, h, sb - 1] = Power[j, k, h, sb];
                                            Power[j, k, h, sb] = Hisprice[j, k, h, sb];
                                        }
                                    }
                                }

                            }
                            if ((Price[j, k, h, s] == 0) && (Power[j, k, h, s] != 0))
                            {
                                Power[j, k, h, s] = 0;
                            }
                            if ((Price[j, k, h, s] != 0) && (Power[j, k, h, s] == 0))
                            {
                                Price[j, k, h, s] = 0;
                            }
                        }
                    }
                }
            }

            for (int h = 0; h < Hour; h++)
            {
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        for (int s = 1; s < Step_Num; s++)
                        {
                            if ((Price[j, k, h, s] >= ( CapPrice[0])))
                            {
                                Power[j, k, h, s] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050)); 
                                if (s < 9)
                                {
                                    for (int ss = s + 1; ss < Step_Num; ss++)
                                    {
                                        Price[j, k, h, ss] = 0;
                                        Power[j, k, h, ss] = 0;
                                    }
                                }
                            }
           
                        }
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int s = 0; s < (Step_Num - 1); s++)
                        {
                            if ((Power[j, k, h, s + 1] == 0) & (Power[j, k, h, s] > 0) & (Power[j, k, h, s] < Dispatch_Proposal[j, k, h]))
                            {
                                Power[j, k, h, s] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050)); 
                            }
                        }
                        if ((Power[j, k, h, (Step_Num - 1)] < Math.Round(Dispatch_Proposal[j, k, h])) & (Power[j, k, h, (Step_Num - 1)] > 0))
                        {
                            //Power[j, k, h, (Step_Num - 1)] = Dispatch_Proposal[j, k, h];
                            //Price[j, k, h, (Step_Num - 1)] = Price[j, k, h, (Step_Num - 1)];
                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int s = 0; s < Step_Num; s++)
                        {
                            if ((statusmust[j, k, h] == "MO") && (valmust[j, k, h] == 0))
                            {
                                Power[j, k, h, 0] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                Price[j, k, h, 0] = CapPrice[0];
                                if (s > 0)
                                {
                                    Power[j, k, h, s] = 0;
                                    Price[j, k, h, s] = 0;
                                }
                            }
                            if ((statusmust[j, k, h] == "MN") && (PowerMarket_Gas[j, k, h] == 1))
                            {
                                Power[j, k, h, 0] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                Price[j, k, h, 0] = CapPrice[0];
                                if (s > 0)
                                {
                                    Power[j, k, h, s] = 0;
                                    Price[j, k, h, s] = 0;
                                }
                            }
                            if ((statusmust[j, k, h] == "MO") && (valmust[j, k, h] != 0))
                            {
                                if (s > 0 && s < 9)
                                {
                                    if ((Power[j, k, h, s] < valmust[j, k, h]) && (Power[j, k, h, s + 1] > valmust[j, k, h]) && (Price[j, k, h, s] < CapPrice[0] - 10))
                                    {
                                        Power[j, k, h, s] = valmust[j, k, h];
                                        Price[j, k, h, s] = CapPrice[0] -1000;
                                        Power[j, k, h, s + 1] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                        Price[j, k, h, s + 1] = CapPrice[0];
                                        for (int ss = s + 2; ss < Step_Num; ss++)
                                        {
                                            Power[j, k, h, ss] = 0;
                                            Price[j, k, h, ss] = 0;
                                        }
                                    }
                                }
                            }

                            if (Price[j, k, h, s] > CapPrice[0])
                            {
                                Price[j, k, h, s] = CapPrice[0];
                            }
                        }
                    }
                }
            }




            /////////////////////////////////////
            for (int h = 0; h < Hour; h++)
            {
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if ((Cap_Bid[j, k, h] == "Yes"))
                        {
                            if ((Unit_Dec[j, k, 1].ToString().Trim() != "Gas"))
                            {
                                for (int s = 0; s < Step_Num; s++)
                                {
                                    if (s == 0)
                                    {
                                        Price[j, k, h, s] = CapPrice[0];
                                        Power[j, k, h, s] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050)); 
                                    }
                                    if (s > 0)
                                    {
                                        Price[j, k, h, s] = 0;
                                        Power[j, k, h, s] = 0;
                                    }
                                }
                            }
                            if ((Unit_Dec[j, k, 1].ToString().Trim() == "Gas"))
                            {
                                for (int s = 0; s < Step_Num; s++)
                                {
                                    if (s == 0)
                                    {
                                        Price[j, k, h, s] = CapPrice[0];
                                        Power[j, k, h, s] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                    }
                                    if (s > 0)
                                    {
                                        Price[j, k, h, s] = 0;
                                        Power[j, k, h, s] = 0;
                                    }
                                }
                            }

                        }
                    }
                }
            }


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int s = 0; s < Step_Num; s++)
                        {
                            if (Outservice_Plant_unit[j, k, h] == 1)
                            {
                                if ((Unit_Dec[j, k, 1].ToString().Trim() == "Steam"))
                                {
                                    if (s == 0)
                                    {
                                        Price[j, k, h, s] = CapPrice[0];
                                        Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                    }
                                    if (s > 0)
                                    {
                                        Price[j, k, h, s] = 0;
                                        Power[j, k, h, s] = 0;
                                    }
                                }
                                if ((Unit_Dec[j, k, 1].ToString().Trim() == "CC"))
                                {
                                    if (s == 0)
                                    {
                                        Price[j, k, h, s] = CapPrice[0];
                                        Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                    }
                                    if (s > 0)
                                    {
                                        Price[j, k, h, s] = 0;
                                        Power[j, k, h, s] = 0;
                                    }
                                }
                                if ((Unit_Dec[j, k, 1].ToString().Trim() == "Gas"))
                                {
                                    if (s == 0)
                                    {
                                        Price[j, k, h, s] = CapPrice[0];
                                        Power[j, k, h, s] = Math.Round(Pmax_Plant_unit[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                    }
                                    if (s > 0)
                                    {
                                        Price[j, k, h, s] = 0;
                                        Power[j, k, h, s] = 0;
                                    }
                                }
                                //Dispatch_Proposal[j, k, h] = 0;
                                Dispatch_Clear[j, k, h] = 0;
                            }
                        }
                    }
                }
            }


            //*************************AG******************************************
            //salam khanum khorsand
            // felan farz konid har vahed ye block tajmih shode ino to view dorost konid har block 1 vahed masal AG=G11..AG2=G12....
           
            // int AG =
            //lotfan inja tedad vahed haie tajmi az Data base bekhonid.
           // int[] AG_Num = new int [Plants_Num];

           // //inja name vahede sar goroh har block tajmih begirid masalan AG_NAME[0,1]= "G11"...AG_NAME[0,2]= "G12"  
           // string[,] AG_NAME = new string[Plants_Num,AG_Num];

           // //inja code vahede sar goroh har block tajmih begirid masalan AG_Code[0,1]= 1...AG_Code[0,1]= 2  
           //double[,] AG_Code = new double[Plants_Num, AG_Num];

           ////inja vahedehaee ke zir majmoeie on block tajmi hastan  add 1 bezariid baghie sefr masalan AG_Unit[0,1,1]= 1...AG_Unit[0,1,2]= 0  
           //double[,,] AG_Unit = new double[Plants_Num, AG_Num,Units_Num];

           //// Price_AG man tarif mikonam
           //double[, , , ,] Price_AG = new double[Plants_Num, AG_Num, Units_Num, Hour, Step_Num];
           //double[, , , ,] Power_AG = new double[Plants_Num, AG_Num, Units_Num, Hour, Step_Num];

           //for (int j = 0; j < Plants_Num; j++)
           //{
           //    for (int AG = 0; AG < AG_Num[j]; AG++)
           //    {
           //        for (int ii = 0; ii < Plant_units_Num[j]; ii++)
           //        {
           //            if (AG_Code(j, AG) == i)
           //            {
           //                for (int i = 0; i < Plant_units_Num[j]; i++)
           //                {
           //                    if (AG_Unit(j, AG ,ii) == 1)
           //                    {
           //                        for (int h = 0; h < Hour; h++)
           //                        {
           //                            for (int s = 0; s < Step_Num; s++)
           //                            {
           //                                Price_AG(j, AG, i, h, s) = Price[j, i, h, s];
           //                                Power_AG(j, AG, i, h, s) = Price[j, i, h, s];
           //                            }
           //                        }
           //                    }
           //                }
           //            }
           //        }
           //    }
           //}
           //// Pass har vahed ye price_AG dare ye Power_AG daree ye Dispatch_Clear ham ke az ghbl dasht hame ra bar hasb vahed (ii) berizid mesle ghabl to M002
           //double[, , , ] M005_AG_EC = new double[Plants_Num, AG_Num, Units_Num, Hour];
           //double[, , , ] M005_AG_Dis = new double[Plants_Num, AG_Num, Units_Num, Hour];

           //for (int j = 0; j < Plants_Num; j++)
           //{
           //    for (int AG = 0; AG < AG_Num[j]; AG++)
           //    {
           //        for (int i = 0; i < Plant_units_Num[j]; i++)
           //        {
           //            if (AG_Code(j, AG) == i)
           //            {
           //                for (int h = 0; h < Hour; h++)
           //                {
           //                    //M005_AG_EC(j, AG, i, h) =  Economic in M005;
           //                    //M005_AG_Dis(j, AG, i, h) = Dispatch in M005;
           //                }
           //            }
           //        }
           //    }
           //}
           // // to in marhale ham az file M005 ba format alan ba formol bala maghadi khonde mishe

           // // nahaiat ham ye file MOO2 va ye file M005 ba dade zir dorost mishe

           //double[, , ,] M002_AG_Req = new double[Plants_Num, AG_Num, Units_Num, Hour];
           //double[, , ,] M002_AG_Dis = new double[Plants_Num, AG_Num, Units_Num, Hour];

           //for (int j = 0; j < Plants_Num; j++)
           //{
           //    for (int AG = 0; AG < AG_Num[j]; AG++)
           //    {
           //        for (int i = 0; i < Plant_units_Num[j]; i++)
           //        {
           //                for (int h = 0; h < Hour; h++)
           //                {
           //                    //M0085_AG_Req(j, AG, i, h) =Power[j, k, h, 0];
           //                    //M005_AG_Dis(j, AG, i, h) = Dispatch_Clear in M005;
           //                }
           //            }
           //        }
           //    }
           //}
        // to file m002 power 1 =M002_AG_Req va dispatch=M002_AG_Dis to file M005 req=M002_AG_Req va dispatch=M002_AG_Dis
        //daste shoma dar nakoneeeee :)



            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int s = 1; s < Step_Num; s++)
                        {
                            if (((Price[j, k, h, s - 1] > Price[j, k, h, s]) || (Power[j, k, h, s - 1] > Power[j, k, h, s])) && (Price[j, k, h, s] != 0))
                            {
                                DataTable erroeplant = Utilities.GetTable("select PPName from dbo.PowerPlant where PPID='" + plant[j] + "'");
                                // System.Windows.Forms.MessageBox.Show("   Error_1 : Answer is Fail.   " + "  Plant  " + j.ToString().Trim() + "  Unit  " + k.ToString() + "  Hour  " + h.ToString() + "  Step  " + s.ToString());
                                System.Windows.Forms.MessageBox.Show(" Error in  Plant : " + erroeplant.Rows[0][0].ToString().Trim() + " - Package  " + (k + 1).ToString() + " - Hour  " + h.ToString() + " - Step  " + s.ToString() + " !\r\n Notice That Result of This Plant Is NOT Valid!");

                                return false;
                            }
                        }
                    }
                }
            }
            //////////////////////////////////////////NEW////////////////////////////////////////////
            //////////////////////////////////////////NEW////////////////////////////////////////////


            int pptype = 0;
            // string block = "";


            string[,] blocks002 = Getblock002();
            double[, ,] minprice = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        minprice[j, k, h] = CapPrice[0];
                        for (int hh = 0; hh < 24; hh++)
                        {
                            if (minprice[j, k, h] > Price[j, k, hh, 0] && Price[j, k, hh, 0] > 0)
                            {
                                minprice[j, k, h] = Price[j, k, hh, 0];
                            }
                        }
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                strCmd = "delete from DetailFRM002 where TargetMarketDate ='" + biddingDate + "'" +
             " and ppid='" + plant[j].Trim() + "'and Estimated=1";
                oDataTable = Utilities.GetTable(strCmd);


                /////////////////////))))))))))))))))))))))

                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    pptype = 0;
                    if (Unit_Dec[j, k, 1] == PackageTypePriority.CC.ToString() && PackageTypes[j].Count > 1)
                        pptype = 1;

                    //oDataTable = utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                    //    plant[j] + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'");


                    for (int h = 0; h < Hour; h++)
                    {

                        string firstCommand = "insert into DetailFRM002 (Estimated,TargetMarketDate, PPID,PPType,Block, Hour,DeclaredCapacity,DispachableCapacity ";

                        string secondCommand = "values (1,'" + biddingDate + "','" + plant[j] + "','" +
                            pptype.ToString() + "','" + blocks002[j, k] + "'," +
                            (h + 1).ToString() + "," +
                            Dispatch_Clear[j, k, h].ToString() + ","
                            + Dispatch_Clear[j, k, h].ToString().Trim();


                        for (int s = 0; s < Step_Num; s++)
                        {
                            //~~~~~~~~~~~~~~~~~~~~for negative price value~~~~~~~~~~~~~~~~~~~``
                            if (Price[j, k, h, s] < 0)
                            {
                                Power[j, k, h, s] = Math.Round(Dispatch_Clear[j, k, h] * (1 - valtrans1[j, h]) * (1 - 0.06050));
                                Price[j, k, h, s] = CapPrice[0];;
                                for (int ss = (s + 1); ss < Step_Num; ss++)
                                {
                                    Power[j, k, h, ss] = 0;
                                    Price[j, k, h, ss] = 0;
                                }
                            }

                            //~~~~~~~~~~~~~~~~~~~~~for negative price value~~~~~~~~~~~~~~~~~~~~~~



                            firstCommand += ",Power" + (s + 1).ToString().Trim();
                            secondCommand += "," + Power[j, k, h, s].ToString().Trim();

                            firstCommand += ",Price" + (s + 1).ToString().Trim();
                            secondCommand += "," + Price[j, k, h, s].ToString().Trim();


                        }
                        firstCommand += " ) ";
                        secondCommand += " ) ";
                        //for (int s = 0; s < Step_Num; s++)
                        //{
                        //oDataTable = utilities.GetTable("insert into Table_1 values('" + plant[j].ToString() + "','" + k.ToString() + "','" + h.ToString() + "','" + Pmax_Plant_Pack[j, k, h].ToString() + "','" + Power[j, k, h, s].ToString() + "','" + Price[j, k, h, s].ToString() + "','" + s.ToString() + "' )");
                        oDataTable = Utilities.GetTable(firstCommand + secondCommand);
                        //}
                    }
                }
            }


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int s = 0; s < Step_Num; s++)
                        {
                            if (0 > Price[j, k, h, s])
                            {
                                System.Windows.Forms.MessageBox.Show(" Error in  Plant : " + " - Package  " + (k + 1).ToString() + " - Hour  " + h.ToString() + " - Step  " + s.ToString() + " !\r\n Notice That Result of This Plant Is NOT Valid!");
                            }
                            if (0 > Power[j, k, h, s])
                            {
                                System.Windows.Forms.MessageBox.Show(" Error in  Plant : " + " - Package  " + (k + 1).ToString() + " - Hour  " + h.ToString() + " - Step  " + s.ToString() + " !\r\n Notice That Result of This Plant Is NOT Valid!");
                            }
                        }
                    }
                }
            }


            return true;
            ////////////////////////////////////////////////////////////////////////////////////
            //l1: return false;
            ////////////////////////////////////////
        }
       
      
      
   
      
       
     
        private string[,] Getblock()
        {

            string[,] blocks = new string[Plants_Num, Units_Num];
            DataTable oDataTable = null;


            oDataTable = Utilities.GetTable("select distinct PPID from dbo.PowerPlant");




            for (int j = 0; j < Plants_Num; j++)
            {
                int packcode = 0;
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    if (packcode < Plant_Packages_Num[j]) packcode++;

                    oDataTable = Utilities.GetTable("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "'");

                    int ppid = int.Parse(plant[j]);

                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {
                        Initial = "C" + (k + 1).ToString();

                        if (Findccplant(plant[j]))
                        {
                            ppid++;
                        }


                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY UnitType ASC");
                        int loopcount = 0;
                        foreach (DataRow mn in oDataTable.Rows)
                        {

                            string ptype = mn[0].ToString().Trim();
                            ptype = ptype.ToLower();
                            ptype = ptype.Replace("cc", "c");
                            string[] sp = ptype.Split('c');
                            ptype = sp[0].Trim() + sp[1].Trim();
                            if (ptype.Contains("gas"))
                            {
                                ptype = ptype.Replace("gas", "G");

                            }
                            else
                            {
                                ptype = ptype.Replace("steam", "S");

                            }
                            ptype = ppid + "-" + ptype;

                            blocks[j, k] = ptype;

                            loopcount++;
                            if (loopcount != oDataTable.Rows.Count) k++;
                        }
                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                       plant[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'ORDER BY UnitType ASC");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;
                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;
                    }


                }
            }
            return blocks;

        }

        ////********************************************** Get 7 day
        DateTime b;
        DateTime c;

        private string[,] Getblock002()
        {
            string[,] blocks1 = new string[Plants_Num, Units_Num];

            DataTable oDataTable = null;



            oDataTable = Utilities.GetTable("select distinct PPID from dbo.PowerPlant");





            for (int j = 0; j < Plants_Num; j++)
            {

                int packcode = 0;
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    if (packcode < Plant_Packages_Num[j]) packcode++;
                    oDataTable = Utilities.GetTable("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                       plant[j] + "'" + " and packageCode='" + packcode + "'");


                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY UnitType ASC");
                        int loopcount = 0;
                        foreach (DataRow mn in oDataTable.Rows)
                        {

                            string ptype = mn[0].ToString().Trim();
                            ptype = ptype.ToLower();
                            ptype = ptype.Replace("cc", "c");
                            string[] sp = ptype.Split('c');
                            ptype = sp[0].Trim() + sp[1].Trim();
                            if (ptype.Contains("gas"))
                                ptype = ptype.Replace("gas", "G");
                            else if (ptype.Contains("steam"))
                                ptype = ptype.Replace("steam", "S");

                            blocks1[j, k] = ptype;

                            loopcount++;
                            if (loopcount != oDataTable.Rows.Count) k++;

                        }



                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'ORDER BY UnitType ASC");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                        string blockName1 = Initial;

                        blocks1[j, k] = blockName1;

                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'ORDER BY UnitType ASC");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                        string blockName1 = Initial;

                        blocks1[j, k] = blockName1;

                    }


                }
            }
            return blocks1;







        }
        public bool Findccplant(string ppid)
        {
            int tr = 0;

            oDataTable = Utilities.GetTable("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count > 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }
        private bool CheckDateSF(string date, string start, string end)
        {
            bool result = false;
            string temp1 = date.Remove(4);
            start = start.Trim();
            end = end.Trim();
            if ((start != "") && (end != ""))
            {
                string temp2 = start.Remove(4);
                if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
                else
                {
                    temp1 = date.Remove(7);
                    temp1 = temp1.Remove(0, temp1.Length - 2);
                    temp2 = start.Remove(7);
                    temp2 = temp2.Remove(0, temp2.Length - 2);
                    if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                    else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
                    else
                    {
                        temp1 = date.Remove(0, date.Length - 2);
                        temp2 = start.Remove(0, start.Length - 2);
                        if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                        else result = false;
                    }
                }
                if (!result) return (result);
                else
                {
                    temp1 = date.Remove(4);
                    temp2 = end.Remove(4);
                    if (int.Parse(temp1) < int.Parse(temp2)) return (true);
                    else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
                    else
                    {
                        temp1 = date.Remove(7);
                        temp1 = temp1.Remove(0, temp1.Length - 2);
                        temp2 = end.Remove(7);
                        temp2 = temp2.Remove(0, temp2.Length - 2);
                        if (int.Parse(temp1) < int.Parse(temp2)) return (true);
                        else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
                        else
                        {
                            temp1 = date.Remove(0, date.Length - 2);
                            temp2 = end.Remove(0, end.Length - 2);
                            if (int.Parse(temp1) < int.Parse(temp2))// || (temp1 == temp2))
                                return (true);
                            else return (false);
                        }
                    }
                }
            }
            return false;
        }
        private string FindNearRegionDate(string date)
        {
            DataTable odat = Utilities.GetTable("Select distinct Date from dbo.RegionNetComp where Date<='" + date + "'AND Code='" + regioncode + "' order by Date desc");
            return odat.Rows[0][0].ToString().Trim();

        }
        private string FindNearProduceDate(string date)
        {
            DataTable odat = Utilities.GetTable("Select distinct Date from dbo.ProducedEnergy where Date<='" + date + "'order by Date desc");
            return odat.Rows[0][0].ToString().Trim();

        }
        private string FindCommon002and005(DateTime selectedDate, string plant)
        {

            string tname = "dbo.DetailFRM005";
            if (Marketrule == "Fuel Limited") tname = "BaDetailFRM005";

            string common = "";

            ///////////////check 002 and 005 not empty////////////////////////////
            DataTable name = Utilities.GetTable("select distinct(PPName) from dbo.PowerPlant where PPID='" + plant + "'");

            DataTable omoo5 = Utilities.GetTable("select * from  " + tname + "  where TargetMarketDate='" + new PersianDate(selectedDate).ToString("d") + "'and PPID='" + plant + "'");
            DataTable omoo2 = Utilities.GetTable("select * from dbo.DetailFRM002 where TargetMarketDate='" + new PersianDate(selectedDate).ToString("d") + "'and PPID='" + plant + "'and Estimated=0");

            if (omoo2.Rows.Count == 0 || omoo5.Rows.Count == 0)
            {
                PersianDate test1 = new PersianDate(currentDate);
                DateTime Now = PersianDateConverter.ToGregorianDateTime(test1);

                DateTime ENDDate = Now.Subtract(new TimeSpan(6, 0, 0, 0));
                for (int daysbefore = 0; daysbefore < 7; daysbefore++)
                {

                    DateTime SAMPLEDate = Now.Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                    if (SAMPLEDate != ENDDate)
                    {

                        string date5 = "";
                        string date2 = "";

                        if (omoo2.Rows.Count == 0)
                        {

                            ///////////////
                            if (predate002 != selectedDate)
                            {

                                message += "M002 File" + " Date :" + new PersianDate(selectedDate).ToString("d") + "   Plant :" + name.Rows[0][0].ToString().Trim() + "\r\n";
                                predate002 = selectedDate;
                            }


                            /////////////
                            DataTable nearomoo2 = Utilities.GetTable("select distinct TargetMarketDate from dbo.DetailFRM002 where TargetMarketDate<='" + new PersianDate(SAMPLEDate).ToString("d") + "'and PPID='" + plant + "'and Estimated=0 order by TargetMarketDate desc");

                            date2 = nearomoo2.Rows[0][0].ToString();
                            bool inweek = finddatein7daym002(date2, plant);
                            if (inweek == false)
                            {
                                //DialogResult result = MessageBox.Show("M002 AND M005 Files In Previous week Is Not Existed!. \r\n", "Bid Allocation End Unsuccesssfully!", MessageBoxButtons.OK);
                                // if (result == DialogResult.OK)
                                {
                                    // return "nocontinue";

                                }
                            }

                        }
                        if (omoo5.Rows.Count == 0)
                        {
                            if (predate005 != selectedDate)
                            {
                                message += "M005 File" + " Date :" + new PersianDate(selectedDate).ToString("d") + "   Plant :" + name.Rows[0][0].ToString().Trim() + "\r\n";
                                predate005 = selectedDate;
                            }

                            DataTable nearomoo5 = Utilities.GetTable("select distinct TargetMarketDate from " + tname + " where TargetMarketDate<='" + new PersianDate(SAMPLEDate).ToString("d") + "'and PPID='" + plant + "'order by TargetMarketDate desc");
                            if (nearomoo5.Rows.Count > 0)
                            {
                                date5 = nearomoo5.Rows[0][0].ToString();
                            }
                            bool inweek1 = finddatein7daym005(date5, plant);
                            if (inweek1 == false)
                            {
                                //DialogResult result1 = MessageBox.Show("M002 AND M005 Files In Previous week Is Not Existed!. \r\n", "Bid Allocation End Unsuccesssfully!", MessageBoxButtons.OK);
                                //if (result1 == DialogResult.OK)
                                //{
                                //    return "nocontinue";

                                //}
                            }
                        }

                        if ((date2 == "" && date5 != ""))
                        {
                            date2 = date5;
                        }

                        if ((date5 == "" && date2 != ""))
                        {
                            date5 = date2;
                        }

                        if (string.Compare(date2, date5) < 0)
                        {
                            date5 = date2;
                        }
                        if (string.Compare(date5, date2) < 0)
                        {
                            date2 = date5;
                        }

                        if (date5 == date2)
                        {
                            common = date2;

                            break;
                        }
                    }
                }
            }

            else
                common = new PersianDate(selectedDate).ToString("d");
            return common;
        }

        private string FindCom002and005special(DateTime selectedDate, string plant)
        {
            string tname = "dbo.DetailFRM005";
            if (Marketrule == "Fuel Limited") tname = "BaDetailFRM005";

            string common = "";

            ///////////////check 002 and 005 not empty////////////////////////////
            DataTable name = Utilities.GetTable("select distinct(PPName) from dbo.PowerPlant where PPID='" + plant + "'");

            DataTable omoo5 = Utilities.GetTable("select * from " + tname + " where TargetMarketDate='" + new PersianDate(selectedDate).ToString("d") + "'and PPID='" + plant + "'");
            DataTable omoo2 = Utilities.GetTable("select * from dbo.DetailFRM002 where TargetMarketDate='" + new PersianDate(selectedDate).ToString("d") + "'and PPID='" + plant + "'and Estimated=0");

            if (omoo2.Rows.Count == 0 || omoo5.Rows.Count == 0)
            {
                PersianDate test1 = new PersianDate(new PersianDate(selectedDate).ToString("d"));
                DateTime Now = PersianDateConverter.ToGregorianDateTime(test1);

                DateTime ENDDate = Now.Subtract(new TimeSpan(6, 0, 0, 0));
                for (int daysbefore = 0; daysbefore < 7; daysbefore++)
                {

                    DateTime SAMPLEDate = Now.Subtract(new TimeSpan(daysbefore, 0, 0, 0));

                    if (isspecial(new PersianDate(SAMPLEDate).ToString("d")))
                    {
                        for (int m = 1; m < 6; m++)
                        {
                            if (isspecial(new PersianDate(SAMPLEDate.AddDays(-m)).ToString("d")) == false)
                            {
                                SAMPLEDate = SAMPLEDate.AddDays(-m);

                                break;
                            }
                        }

                    }


                    if (SAMPLEDate != ENDDate)
                    {

                        string date5 = "";
                        string date2 = "";

                        if (omoo2.Rows.Count == 0)
                        {

                            ///////////////
                            if (predate002 != selectedDate)
                            {

                                message += "M002 File" + " Date :" + new PersianDate(selectedDate).ToString("d") + "   Plant :" + name.Rows[0][0].ToString().Trim() + "\r\n";
                                predate002 = selectedDate;
                            }


                            /////////////
                            DataTable nearomoo2 = Utilities.GetTable("select distinct TargetMarketDate from dbo.DetailFRM002 where TargetMarketDate<='" + new PersianDate(SAMPLEDate).ToString("d") + "'and PPID='" + plant + "'and Estimated=0 order by TargetMarketDate desc");

                            date2 = nearomoo2.Rows[0][0].ToString();
                            bool inweek = finddatein7daym002(date2, plant);
                            if (inweek == false)
                            {
                                DialogResult result = MessageBox.Show("M002 AND M005 Files In Previous week Is Not Existed!. \r\n", "Bid Allocation End Unsuccesssfully!", MessageBoxButtons.OK);
                                if (result == DialogResult.OK)
                                {
                                    return "nocontinue";

                                }
                            }

                        }
                        if (omoo5.Rows.Count == 0)
                        {
                            if (predate005 != selectedDate)
                            {
                                message += "M005 File" + " Date :" + new PersianDate(selectedDate).ToString("d") + "   Plant :" + name.Rows[0][0].ToString().Trim() + "\r\n";
                                predate005 = selectedDate;
                            }

                            DataTable nearomoo5 = Utilities.GetTable("select distinct TargetMarketDate from " + tname + " where TargetMarketDate<='" + new PersianDate(SAMPLEDate).ToString("d") + "'and PPID='" + plant + "'order by TargetMarketDate desc");
                            date5 = nearomoo5.Rows[0][0].ToString();
                            bool inweek1 = finddatein7daym005(date5, plant);
                            if (inweek1 == false)
                            {
                                DialogResult result1 = MessageBox.Show("M002 AND M005 Files In Previous week Is Not Existed!. \r\n", "Bid Allocation End Unsuccesssfully!", MessageBoxButtons.OK);
                                if (result1 == DialogResult.OK)
                                {
                                    return "nocontinue";

                                }
                            }
                        }

                        if ((date2 == "" && date5 != ""))
                        {
                            date2 = date5;
                        }

                        if ((date5 == "" && date2 != ""))
                        {
                            date5 = date2;
                        }

                        if (string.Compare(date2, date5) < 0)
                        {
                            date5 = date2;
                        }
                        if (string.Compare(date5, date2) < 0)
                        {
                            date2 = date5;
                        }

                        if (date5 == date2)
                        {
                            common = date2;

                            break;
                        }
                    }
                }
            }

            else
                common = new PersianDate(selectedDate).ToString("d");
            return common;
        }

        private bool finddatein7daym002(string date, string plant)
        {

            PersianDate test2 = new PersianDate(currentDate);
            DateTime Now1 = PersianDateConverter.ToGregorianDateTime(test2);


            for (int daysbefore = 0; daysbefore < 7; daysbefore++)
            {
                DateTime ENDDate = Now1.Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                DataTable nearomoo2 = Utilities.GetTable("select distinct TargetMarketDate from dbo.DetailFRM002 where TargetMarketDate='" + new PersianDate(ENDDate).ToString("d") + "'and PPID='" + plant + "'and Estimated=0 order by TargetMarketDate desc");
                if (nearomoo2.Rows.Count > 0)
                {
                    if (date == nearomoo2.Rows[0][0].ToString())
                    {
                        return true;
                        break;
                    }
                }
            }
            return false;

        }
        private bool finddatein7daym005(string date, string plant)
        {


            string tname = "dbo.DetailFRM005";
            if (Marketrule == "Fuel Limited") tname = "BaDetailFRM005";

            PersianDate test2 = new PersianDate(currentDate);
            DateTime Now1 = PersianDateConverter.ToGregorianDateTime(test2);


            for (int daysbefore = 0; daysbefore < 7; daysbefore++)
            {
                DateTime ENDDate = Now1.Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                DataTable nearomoo5 = Utilities.GetTable("select distinct TargetMarketDate from " + tname + " where TargetMarketDate='" + new PersianDate(ENDDate).ToString("d") + "'and PPID='" + plant + "'order by TargetMarketDate desc");
                if (nearomoo5.Rows.Count > 0)
                {
                    if (date == nearomoo5.Rows[0][0].ToString())
                    {
                        return true;
                        break;
                    }
                }
            }
            return false;

        }



      

        ////////////////////////////////////////////savepowerchecked method////////////////////////

        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private void savepowerchecked(string ppid, double[] power, string type)
        {
            DataTable del = Utilities.GetTable("delete from PowerForecast where date='" + biddingDate + "'and gencode like '" + (ppid + "-" + type) + '%' + "'");

            // Insert into FinalForcast table
            string strCom1 = "insert INTO [PowerForecast] (Gencode,date";
            string strCom2 = "VALUES ('" + (ppid + "-" + type) + "','" + biddingDate + "'";
            for (int i = 1; i <= 24; i++)
            {
                if (power[i - 1] > 0)
                {
                    strCom1 += ",Hour" + i.ToString();
                    strCom2 += "," + power[i - 1].ToString();
                }
                else
                {
                    strCom1 += ",Hour" + i.ToString();
                    strCom2 += "," + "0";
                }

            }

            DataTable insert = Utilities.GetTable(strCom1 + " ) " + strCom2 + ")");

        }
        private void checkdispatch()
        {
            int allindex = 0;
            string message1 = "";
            string[,] DisBlock = Getblock002();


            for (int j = 0; j < Plants_Num; j++)
            {
                bool enter = true;
                DataTable name = Utilities.GetTable("select distinct(PPName) from dbo.PowerPlant where PPID='" + plant[j] + "'");
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    if (enter)
                    {
                        for (int h = 1; h <= 24; h++)
                        {
                            DataTable dt = Utilities.GetTable("select distinct(count (*)) as result from dbo.Dispathable where StartDate='" + biddingDate + "' and PPID='" + plant[j] + "' and Block='" + DisBlock[j, k] + "' and hour='" + h + "'");
                            if (int.Parse(dt.Rows[0][0].ToString()) == 0)
                            {
                                allindex++;
                                message1 += " For Plant : " + name.Rows[0][0].ToString().Trim() + "  In Date :" + biddingDate + "   Not Exist !" + "\r\n";
                                enter = false;
                                break;
                            }

                        }
                    }
                }
            }
            if (message1 != "")
            {
                if (allindex < 30 && allindex != 0)
                {
                    MessageBox.Show(message1, " Dispatchable Data Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (allindex != 0)
                {
                    MessageBox.Show("Please Fill Dispatchable Data For All Plants .", " Dispatchable Data Not Exist !", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }


        private static bool MyboolParse(string str)
        {
            if (str.Trim() == "")
                return false;
            else
            {
                try { return bool.Parse(str.Trim()); }
                catch { return false; }
            }
        }
        private string near005table(string block, string plant)
        {

            string tname = "dbo.DetailFRM005";
            if (Marketrule == "Fuel Limited") tname = "BaDetailFRM005";

            string date1 = currentDate;
            DateTime date = PersianDateConverter.ToGregorianDateTime(date1);
            //DataTable oDataTable = utilities.GetTable("select Required,Dispatchable,Contribution  from dbo.DetailFRM005 where PPID='" + plant +
            //                "'and TargetMarketDate='" + date + "'and Block='" + block + "'");
            DataTable oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from " + tname + " where PPID='" + plant +
                               "'and TargetMarketDate='" + date1 +
                               "'and Block='" + block + "'");
            if (oDataTable.Rows.Count == 0)
            {
                for (int d = 1; d < 100; d++)
                {
                    DateTime saddate = date;
                    saddate = saddate.AddDays(-d);
                    oDataTable = Utilities.GetTable("select Required,Dispatchable,Contribution  from " + tname + " where PPID='" + plant +
                               "'and TargetMarketDate='" + new PersianDate(saddate).ToString("d") +
                               "'and Block='" + block + "'");
                    if (oDataTable.Rows.Count > 0)
                    {
                        date1 = new PersianDate(saddate).ToString("d");
                        break;
                    }
                }
            }
            return date1;
        }
        private double[,] MaxPrice(DateTime date)
        {
            double[,] maxs = new double[Plants_Num, 24];

            for (int j = 0; j < Plants_Num; j++)
            {
                DataSet myDs = UtilityPowerPlantFunctions.GetUnits(int.Parse(plant[j]));

                DataTable[] orderedPackages = UtilityPowerPlantFunctions.OrderPackages(myDs);

                ///////////////////////////////////////////////////////////////////////////
                string finalplant = plant[j];

                DataTable baseplantfor7day = Utilities.GetTable("select PPName from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                string[] s = baseplantfor7day.Rows[0][0].ToString().Trim().Split('-');
                DataTable idtable7day = Utilities.GetTable("select PPID from dbo.PowerPlant  where PPName='" + s[0].Trim() + "'");

                string preplant7day = idtable7day.Rows[0][0].ToString().Trim();

                DataTable packagetable7day = Utilities.GetTable("SELECT DISTINCT  PackageType  FROM dbo.PPUnit WHERE PPID='" + preplant7day.Trim() + "'");

                string packtype7day = "";
                try
                {
                    packtype7day = s[1].Trim();
                }
                catch
                {

                }

                if (packtype7day == "Combined Cycle") packtype7day = "CC";
                if (packtype7day == "" || packtype7day == null)
                {
                    for (int i = 0; i < packagetable7day.Rows.Count; i++)
                    {
                        if (packagetable7day.Rows[i][0].ToString().Trim() == "Steam")
                        {
                            packtype7day = "Steam";
                            break;
                        }
                        else if (packagetable7day.Rows[i][0].ToString().Trim() == "Combined Cycle") packtype7day = "CC";

                    }
                }
                DataSet myDs7 = UtilityPowerPlantFunctions.GetUnits(int.Parse(preplant7day));
                DataTable[] orderedPackages7 = OrderPackages7(myDs7);
                foreach (DataTable dtPackageType in orderedPackages)
                {
                    foreach (DataTable dtPackageType7 in orderedPackages7)
                    {
                        if ((setparam[j, 4] == true))
                        {
                            orderedPackages = orderedPackages7;
                            finalplant = preplant7day;
                            break;

                        }
                    }

                }


                ////////////////////////////////////////////////////////////////////////////

                foreach (DataTable dtPackageType in orderedPackages)
                {

                    int packageOrder = 0;
                    if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                    orderedPackages.Length > 1)
                        packageOrder = 1;
                    CMatrix NSelectPrice = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid
                           (Convert.ToInt32(finalplant), dtPackageType, date, packageOrder, biddingDate);
                    if (!NSelectPrice.Zero || NSelectPrice != null)
                    {
                        for (int h = 0; h < 24; h++)
                        {
                            maxs[j, h] = NSelectPrice[0, h];
                        }
                        break;
                    }
                }

            }
            return maxs;
        }


        private string[,] preblock(string ppidpre)
        {
            DataTable oDatapre = Utilities.GetTable("select unitcode from UnitsDataMain where  PPID='" + ppidpre + "'");
            int packprenumber = oDatapre.Rows.Count;

            DataTable oDatapre1 = Utilities.GetTable("select max(packagecode) from UnitsDataMain where  PPID='" + ppidpre + "'");
            int pack = (int)oDatapre1.Rows[0][0];

            string[,] blocks = new string[1, packprenumber];
            string temp = "";


            for (int j = 0; j < 1; j++)
            {
                int packcode = 0;
                for (int k = 0; k < oDatapre.Rows.Count; k++)
                {
                    if (packcode < pack) packcode++;

                    oDataTable = Utilities.GetTable("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                        ppidpre + "'" + " and packageCode='" + packcode + "'");

                    int ppid = int.Parse(ppidpre);

                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {

                        if (Findccplant(ppidpre))
                        {
                            ppid++;
                        }

                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        ppidpre + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY UnitType ASC");
                        int loopcount = 0;
                        foreach (DataRow mn in oDataTable.Rows)
                        {

                            string ptype = mn[0].ToString().Trim();
                            ptype = ptype.ToLower();
                            ptype = ptype.Replace("cc", "c");
                            string[] sp = ptype.Split('c');
                            ptype = sp[0].Trim() + sp[1].Trim();
                            if (ptype.Contains("gas"))
                            {
                                ptype = ptype.Replace("gas", "G");

                            }
                            else
                            {
                                ptype = ptype.Replace("steam", "S");

                            }
                            ptype = ppid + "-" + ptype;

                            blocks[j, k] = ptype;

                            loopcount++;
                            if (loopcount != oDataTable.Rows.Count) k++;
                        }
                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                       ppidpre + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'ORDER BY UnitType ASC");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;
                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        ppidpre + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'ORDER BY UnitType ASC");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;
                    }


                }
            }
            return blocks;


        }
        private string commonforlfc()
        {
            string tname = "dbo.DetailFRM005";
            if (Marketrule == "Fuel Limited") tname = "BaDetailFRM005";

            string common = "";
            DataTable baseplantfor7day = Utilities.GetTable("select PPName from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            string[] s = baseplantfor7day.Rows[0][0].ToString().Trim().Split('-');
            DataTable idtable7day = Utilities.GetTable("select PPID from dbo.PowerPlant  where PPName='" + s[0].Trim() + "'");

            string preplant = idtable7day.Rows[0][0].ToString().Trim();

            try
            {
                DataTable dt1 = Utilities.GetTable("select distinct TargetMarketDate from dbo.DetailFRM002 where TargetMarketDate in (select max(TargetMarketDate) from dbo.DetailFRM002 where PPID='" + preplant + "' and Hour=24) and Estimated=0 and PPID='" + preplant + "'");

                DataTable dt2 = Utilities.GetTable("select distinct TargetMarketDate from " + tname + " where TargetMarketDate in (select  max(TargetMarketDate) from " + tname + " where PPID='" + preplant + "' and Hour=24) and PPID='" + preplant + "'");

                string[] ar1 = new string[2];

                ar1[0] = dt1.Rows[0][0].ToString().Trim();
                ar1[1] = dt2.Rows[0][0].ToString().Trim();

                string maxdate = buildmaxdate(ar1);
                string mindate = "";
                if (ar1[0] == maxdate)
                {
                    mindate = ar1[1];
                }
                else
                {
                    mindate = ar1[0];
                }

                for (int daysbefore = 0; daysbefore < 50; daysbefore++)
                {

                    DateTime selectedDate = PersianDateConverter.ToGregorianDateTime(mindate).Subtract(new TimeSpan(daysbefore, 0, 0, 0));

                    dt1 = Utilities.GetTable("select distinct TargetMarketDate from dbo.DetailFRM002 where TargetMarketDate in (select max(TargetMarketDate) from dbo.DetailFRM002 where PPID='" + preplant + "'and TargetMarketDate='" + new PersianDate(selectedDate).ToString("d") + "' and Hour=24) and Estimated=0 and PPID='" + preplant + "'");

                    dt2 = Utilities.GetTable("select distinct TargetMarketDate from " + tname + " where TargetMarketDate in (select max(TargetMarketDate) from " + tname + " where PPID='" + preplant + "'and TargetMarketDate='" + new PersianDate(selectedDate).ToString("d") + "' and Hour=24) and PPID='" + preplant + "'");

                    if (dt1.Rows.Count > 0 && dt2.Rows.Count > 0)
                    {
                        if (dt1.Rows[0][0].ToString().Trim() == dt2.Rows[0][0].ToString().Trim())
                        {
                            common = dt1.Rows[0][0].ToString().Trim();
                            break;
                        }
                    }


                }
            }
            catch
            {

                common = null;
            }

            return common;
        }

        private string nearestlfc()
        {
            string finaldate = biddingDate;
            DateTime dtime = PersianDateConverter.ToGregorianDateTime(biddingDate);
            DataTable dloadbidding1 = Utilities.GetTable("select *  from dbo.LoadForecasting where Date='" + biddingDate + "' and DateEstimate in(select max(DateEstimate) from dbo.LoadForecasting where Date='" + biddingDate + "')");
            if (dloadbidding1.Rows.Count == 0)
            {
                if (islocalweek(biddingDate) == false)
                {
                    for (int i = 7; i < 365; i += 7)
                    {
                        DateTime samdtime = dtime;
                        samdtime = samdtime.AddDays(-i);
                        dloadbidding1 = Utilities.GetTable("select *  from dbo.LoadForecasting where Date='" + new PersianDate(samdtime).ToString("d") + "' and DateEstimate in(select max(DateEstimate) from dbo.LoadForecasting where Date='" + new PersianDate(samdtime).ToString("d") + "')");
                        if ((dloadbidding1.Rows.Count > 0) && (islocalweek(new PersianDate(samdtime).ToString("d")) == false))
                        {
                            finaldate = new PersianDate(samdtime).ToString("d");
                            break;

                        }
                    }
                }
                else
                {
                    string date = nearestislocalweeklfc(biddingDate);
                    dloadbidding1 = Utilities.GetTable("select *  from dbo.LoadForecasting where Date='" + date + "' and DateEstimate in(select max(DateEstimate) from dbo.LoadForecasting where Date='" + date + "')");
                    if ((dloadbidding1.Rows.Count > 0))
                    {
                        finaldate = date;
                    }

                }


            }

            else if (dloadbidding1.Rows.Count > 0)
            {
                finaldate = biddingDate;
            }



            return finaldate;
        }

        //-----------------zaribweekend-------------------------------------------
        private bool check1(double[,] value)
        {

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < 24; h++)
                {
                    if (value[j, h] == 0)
                    {
                        return false;
                    }
                }
            }
            return true;

        }
        private bool islocalweek(string date)
        {
            DataTable dtweekend1 = Utilities.GetTable("select distinct Date from dbo.WeekEnd");
            foreach (DataRow nrow in dtweekend1.Rows)
            {

                if (nrow[0].ToString().Trim() == date)
                {

                    return true;
                    break;

                }


            }
            return false;
        }
        private bool isspecial(string date)
        {

            try
            {
                DataTable dspecial = Utilities.GetTable("select distinct Date from dbo.SpecialTime");
                foreach (DataRow nrow in dspecial.Rows)
                {

                    if (nrow[0].ToString().Trim() == date)
                    {

                        return true;
                        break;

                    }


                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        private string nearestislocalweek(string date)
        {
            string finddate = date;
            DataTable dtweekend1 = Utilities.GetTable("select distinct Date from dbo.WeekEnd where Date<'" + date + "' order by Date desc ");
            foreach (DataRow nrow in dtweekend1.Rows)
            {
                DateTime d = PersianDateConverter.ToGregorianDateTime(nrow[0].ToString().Trim());
                string day = d.DayOfWeek.ToString();
                if (check1(MaxPrice(d)))
                {
                    finddate = nrow[0].ToString().Trim();

                    break;

                }


            }
            return finddate;
        }
        private string nearestislocalweekzaribavg(string date)
        {
            string finddate = date;
            DataTable dtweekend1 = Utilities.GetTable("select distinct Date from dbo.WeekEnd where Date<'" + date + "' order by Date desc ");
            foreach (DataRow nrow in dtweekend1.Rows)
            {
                DateTime d = PersianDateConverter.ToGregorianDateTime(nrow[0].ToString().Trim());
                string day = d.DayOfWeek.ToString();
                DataTable acavgfrida = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date='" + new PersianDate(d).ToString("d") + "'");

                if (acavgfrida.Rows.Count > 0)
                {
                    finddate = nrow[0].ToString().Trim();

                    break;

                }


            }
            return finddate;
        }
        private string nearestislocalweeklfc(string date)
        {
            string finddate = date;
            DataTable dtweekend1 = Utilities.GetTable("select distinct Date from dbo.WeekEnd where Date<'" + date + "' order by Date desc ");
            foreach (DataRow nrow in dtweekend1.Rows)
            {
                DateTime d = PersianDateConverter.ToGregorianDateTime(nrow[0].ToString().Trim());
                string day = d.DayOfWeek.ToString();
                DataTable dloadbidding1 = Utilities.GetTable("select *  from dbo.LoadForecasting where Date='" + new PersianDate(d).ToString("d") + "' and DateEstimate in(select max(DateEstimate) from dbo.LoadForecasting where Date='" + new PersianDate(d).ToString("d") + "')");
                if (dloadbidding1.Rows.Count > 0)
                {
                    finddate = nrow[0].ToString().Trim();

                    break;

                }


            }
            return finddate;
        }
        public bool common0205entity(string date)
        {
            string tname = "dbo.DetailFRM005";
            if (Marketrule == "Fuel Limited") tname = "BaDetailFRM005";

            DataTable baseplant = Utilities.GetTable("select PPName from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            string[] s = baseplant.Rows[0][0].ToString().Trim().Split('-');
            DataTable idtable = Utilities.GetTable("select PPID from dbo.PowerPlant  where PPName='" + s[0].Trim() + "'");

            string preplant = idtable.Rows[0][0].ToString().Trim();
            DataTable dd2 = Utilities.GetTable("select * from dbo.DetailFRM002 where ppid='" + preplant + "' and Estimated=0 and TargetMarketDate='" + date + "'");
            DataTable dd5 = Utilities.GetTable("select * from " + tname + " where ppid='" + preplant + "'and TargetMarketDate='" + date + "'");
            if (dd2.Rows.Count >= 24 && dd5.Rows.Count >= 24)
                return true;
            else
                return false;



        }
        public DataTable[] OrderPackages7(DataSet myDs)
        {
            DataTable[] orderedPackages = new DataTable[1];
            int count = 0;


            DataTable baseplant = Utilities.GetTable("select PPName from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            string[] s = baseplant.Rows[0][0].ToString().Trim().Split('-');
            DataTable idtable = Utilities.GetTable("select PPID from dbo.PowerPlant  where PPName='" + s[0].Trim() + "'");

            string preplant = idtable.Rows[0][0].ToString().Trim();

            string packtype = "";
            try
            {
                packtype = s[1].Trim();
            }
            catch
            {

            }

            if (packtype == "Combined Cycle") packtype = "CC";

            if (packtype == "" || packtype == null)
            {
                foreach (string typeName in Enum.GetNames(typeof(PackageTypePriority)))
                {
                    //////////// have to make sure later whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                    if (myDs.Tables.Contains(typeName))
                        orderedPackages[0] = myDs.Tables[myDs.Tables.IndexOf(typeName)];
                }
            }
            else
            {
                //////////// have to make sure later whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                //DataTable dd = new DataTable(packtype);
                //string oo = packtype;
                //orderedPackages[0] = dd;

                foreach (string typeName in Enum.GetNames(typeof(PackageTypePriority)))
                {
                    //////////// have to make sure later whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                    if (typeName == packtype)
                        orderedPackages[0] = myDs.Tables[myDs.Tables.IndexOf(typeName)];
                }


            }

            return orderedPackages;
        }

        public double[, ,] capacity()
        {

            string tname = "dbo.DetailFRM005";
            if (Marketrule == "Fuel Limited") tname = "BaDetailFRM005";

            double[, ,] vv = new double[Plants_Num, 5, 24];
            ///////////////////////////////////////////////////////////////////////////////////////////////////

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int d = 0; d < 5; d++)
                {
                    string date = new PersianDate(PersianDateConverter.ToGregorianDateTime(biddingDate).AddDays(-d)).ToString("d");
                    for (int h = 0; h < 24; h++)
                    {
                        DataTable dp1 = Utilities.GetTable("select sum (Dispatchable) from " + tname + " where TargetMarketDate='" + date + "' AND PPID='" + plant[j] + "'and hour='" + (h + 1) + "'");

                        vv[j, d, h] = MyDoubleParse(dp1.Rows[0][0].ToString());


                    }

                }

            }
            return vv;
        }

        public int[] convertor(int jindex, string ptype)
        {
            int[] v = new int[Plant_units_Num[jindex]];
            int r = 0;
            for (int i = 0; i < Plant_units_Num[jindex]; i++)
            {
                if (Unit_Dec[jindex, i, 1] == ptype)
                {
                    v[r] = i;
                    r++;
                }

            }

            return v;
        }

        public double defaultpicefind(string ppid, int h)
        {
            string vv = "H" + (h + 1);
            DataTable v = Utilities.GetTable("select " + vv + " from defaultprice where ppid='" + ppid + "'");
            double defaultprice = 0;
            //defaultprice = MyDoubleParse(v.Rows[0][0].ToString());
            defaultprice = 382890;

            return defaultprice;
        }

        public double MustHourly(string ppid, string unit, int h)
        {
            double val = 0;
            DataTable x = Utilities.GetTable("select H" + h + " from dbo.MustHourly where fromdate<='" + biddingDate + "'and todate>='" + biddingDate + "'and unit='" + unit + "'and ppid='" + ppid + "'ORDER BY ID DESC");
            if (x.Rows.Count > 0 && x.Rows[0][0].ToString() != "")
            {
                string[] cc = x.Rows[0][0].ToString().Split('-');
                val = MyDoubleParse(cc[0]);
            }
            return val;

        }

        public string MustHourlystatus(string ppid, string unit, int h)
        {
            string val = "";
            DataTable x = Utilities.GetTable("select H" + h + " from dbo.MustHourly where fromdate<='" + biddingDate + "'and todate>='" + biddingDate + "'and unit='" + unit + "'and ppid='" + ppid + "'ORDER BY ID DESC");
            if (x.Rows.Count > 0 && x.Rows[0][0].ToString() != "")
            {
                string[] cc = x.Rows[0][0].ToString().Split('-');
                val = cc[1];
            }
            return val;

        }

        public double powerHourly(string ppid, int h, string type)
        {

            double val = 0;
            try
            {
                DataTable x = Utilities.GetTable("select H" + h + " from powersetting where fromdate<='" + biddingDate + "'and todate>='" + biddingDate + "'and ppid='" + ppid + "'and type='" + type + "'ORDER BY  fromdate,ID DESC");// "'and unit='" + unit
                if (x.Rows.Count > 0 && x.Rows[0][0].ToString() != "")
                {
                    val = MyDoubleParse(x.Rows[0][0].ToString());
                }
            }
            catch
            {

            }
            return val;

        }

        public static bool Findcconetype(string ppid)
        {
            int tr = 0;

            DataTable oDataTable = Utilities.GetTable("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count == 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }

        public string valtrain(string ppid, string date)
        {
            string val = "";
            try
            {
                DataTable dd = Utilities.GetTable("select * from trainingprice where date='" + date + "'and ppid='" + ppid + "'");
                string plantpre = "";
                string unitpre = "";
                string defaultprice = "";
                string defaultdate = "";

                if (dd.Rows[0]["plantpresolve"].ToString().Trim() != "")
                {
                    val = dd.Rows[0]["plantpresolve"].ToString().Trim();
                }
                else if (dd.Rows[0]["defaultprice"].ToString().Trim() == "True")
                {
                    val = "defaultprice";
                }
                else if (dd.Rows[0]["unitpresolve"].ToString().Trim() != "")
                {
                    val = dd.Rows[0]["unitpresolve"].ToString().Trim();
                }
                else if (dd.Rows[0]["defaultdate"].ToString().Trim() != "")
                {
                    val = "date:" + dd.Rows[0]["defaultdate"].ToString().Trim();
                }
            }
            catch
            {
            }

            return val;
        }

        public string BourseHourly(int h)
        {
            string val = "";
            try
            {

                DataTable x = Utilities.GetTable("select H" + h + " from Bourse where fromdate<='" + biddingDate + "'and todate>='" + biddingDate + "'ORDER BY ID DESC");
                if (x.Rows.Count > 0 && x.Rows[0][0].ToString() != "")
                {
                    val = x.Rows[0][0].ToString();
                }
            }
            catch
            {
                val = "";
            }
            return val;

        }


        private bool Findexist002and005(string plant, string Date)
        {
            bool exist = true;
            string tname = "dbo.DetailFRM005";
            if (Marketrule == "Fuel Limited") tname = "BaDetailFRM005";

            string common = "";

            ///////////////check 002 and 005 not empty////////////////////////////
            DataTable name = Utilities.GetTable("select distinct(PPName) from dbo.PowerPlant where PPID='" + plant + "'");

            DataTable omoo5 = Utilities.GetTable("select * from  " + tname + "  where TargetMarketDate='" + Date + "'and PPID='" + plant + "'");
            DataTable omoo2 = Utilities.GetTable("select * from dbo.DetailFRM002 where TargetMarketDate='" + Date + "'and PPID='" + plant + "'and Estimated=0");

            if (omoo2.Rows.Count == 0 || omoo5.Rows.Count == 0)
            {
                exist = false;
            }


            return exist;
        }
    }

}
