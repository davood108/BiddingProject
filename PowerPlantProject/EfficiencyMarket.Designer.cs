﻿namespace PowerPlantProject
{
    partial class EfficiencyMarketForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EfficiencyMarketForm));
            this.label9 = new System.Windows.Forms.Label();
            this.H8Tb = new System.Windows.Forms.TextBox();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.FromDateCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.H7Tb = new System.Windows.Forms.TextBox();
            this.H6Tb = new System.Windows.Forms.TextBox();
            this.H5Tb = new System.Windows.Forms.TextBox();
            this.H4Tb = new System.Windows.Forms.TextBox();
            this.H3Tb = new System.Windows.Forms.TextBox();
            this.H2Tb = new System.Windows.Forms.TextBox();
            this.H1Tb = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.ToDateCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.H16Tb = new System.Windows.Forms.TextBox();
            this.H15Tb = new System.Windows.Forms.TextBox();
            this.H14Tb = new System.Windows.Forms.TextBox();
            this.H13Tb = new System.Windows.Forms.TextBox();
            this.H12Tb = new System.Windows.Forms.TextBox();
            this.H11Tb = new System.Windows.Forms.TextBox();
            this.H10Tb = new System.Windows.Forms.TextBox();
            this.H9Tb = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.H24Tb = new System.Windows.Forms.TextBox();
            this.H23Tb = new System.Windows.Forms.TextBox();
            this.H22Tb = new System.Windows.Forms.TextBox();
            this.H21Tb = new System.Windows.Forms.TextBox();
            this.H20Tb = new System.Windows.Forms.TextBox();
            this.H19Tb = new System.Windows.Forms.TextBox();
            this.H18Tb = new System.Windows.Forms.TextBox();
            this.H17Tb = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.FromValidate = new System.Windows.Forms.Label();
            this.ToValidate = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(38, 301);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 79;
            this.label9.Text = "From Date :";
            // 
            // H8Tb
            // 
            this.H8Tb.Location = new System.Drawing.Point(72, 249);
            this.H8Tb.Name = "H8Tb";
            this.H8Tb.Size = new System.Drawing.Size(73, 20);
            this.H8Tb.TabIndex = 7;
            this.H8Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H8Tb.Validated += new System.EventHandler(this.H8Tb_Validated);
            // 
            // SaveBtn
            // 
            this.SaveBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.SaveBtn.BackgroundImage = global::PowerPlantProject.Properties.Resources.Savedata;
            this.SaveBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SaveBtn.Location = new System.Drawing.Point(228, 352);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(86, 27);
            this.SaveBtn.TabIndex = 26;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.UseVisualStyleBackColor = false;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // FromDateCal
            // 
            this.FromDateCal.HasButtons = true;
            this.FromDateCal.Location = new System.Drawing.Point(106, 301);
            this.FromDateCal.Name = "FromDateCal";
            this.FromDateCal.Readonly = true;
            this.FromDateCal.Size = new System.Drawing.Size(120, 20);
            this.FromDateCal.TabIndex = 24;
            this.FromDateCal.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // H7Tb
            // 
            this.H7Tb.Location = new System.Drawing.Point(72, 217);
            this.H7Tb.Name = "H7Tb";
            this.H7Tb.Size = new System.Drawing.Size(73, 20);
            this.H7Tb.TabIndex = 6;
            this.H7Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H7Tb.Validated += new System.EventHandler(this.H7Tb_Validated);
            // 
            // H6Tb
            // 
            this.H6Tb.Location = new System.Drawing.Point(72, 186);
            this.H6Tb.Name = "H6Tb";
            this.H6Tb.Size = new System.Drawing.Size(73, 20);
            this.H6Tb.TabIndex = 5;
            this.H6Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H6Tb.Validated += new System.EventHandler(this.H6Tb_Validated);
            // 
            // H5Tb
            // 
            this.H5Tb.Location = new System.Drawing.Point(72, 154);
            this.H5Tb.Name = "H5Tb";
            this.H5Tb.Size = new System.Drawing.Size(73, 20);
            this.H5Tb.TabIndex = 4;
            this.H5Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H5Tb.Validated += new System.EventHandler(this.H5Tb_Validated);
            // 
            // H4Tb
            // 
            this.H4Tb.Location = new System.Drawing.Point(72, 122);
            this.H4Tb.Name = "H4Tb";
            this.H4Tb.Size = new System.Drawing.Size(73, 20);
            this.H4Tb.TabIndex = 3;
            this.H4Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H4Tb.Validated += new System.EventHandler(this.H4Tb_Validated);
            // 
            // H3Tb
            // 
            this.H3Tb.Location = new System.Drawing.Point(72, 89);
            this.H3Tb.Name = "H3Tb";
            this.H3Tb.Size = new System.Drawing.Size(73, 20);
            this.H3Tb.TabIndex = 2;
            this.H3Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H3Tb.Validated += new System.EventHandler(this.H3Tb_Validated);
            // 
            // H2Tb
            // 
            this.H2Tb.Location = new System.Drawing.Point(72, 60);
            this.H2Tb.Name = "H2Tb";
            this.H2Tb.Size = new System.Drawing.Size(73, 20);
            this.H2Tb.TabIndex = 1;
            this.H2Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H2Tb.Validated += new System.EventHandler(this.H2Tb_Validated);
            // 
            // H1Tb
            // 
            this.H1Tb.Location = new System.Drawing.Point(72, 29);
            this.H1Tb.Name = "H1Tb";
            this.H1Tb.Size = new System.Drawing.Size(73, 20);
            this.H1Tb.TabIndex = 0;
            this.H1Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H1Tb.Validated += new System.EventHandler(this.H1Tb_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(24, 251);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 67;
            this.label8.Text = "Hour8 :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(24, 221);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 65;
            this.label7.Text = "Hour7 :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(24, 189);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 62;
            this.label6.Text = "Hour6 :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(24, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 60;
            this.label5.Text = "Hour5 :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(24, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 58;
            this.label4.Text = "Hour4 :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(24, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 57;
            this.label3.Text = "Hour3 :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(24, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 55;
            this.label2.Text = "Hour2 :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(24, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 53;
            this.label1.Text = "Hour1 :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Location = new System.Drawing.Point(276, 301);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 13);
            this.label18.TabIndex = 89;
            this.label18.Text = "To Date :";
            // 
            // ToDateCal
            // 
            this.ToDateCal.HasButtons = true;
            this.ToDateCal.Location = new System.Drawing.Point(344, 301);
            this.ToDateCal.Name = "ToDateCal";
            this.ToDateCal.Readonly = true;
            this.ToDateCal.Size = new System.Drawing.Size(120, 20);
            this.ToDateCal.TabIndex = 25;
            this.ToDateCal.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // H16Tb
            // 
            this.H16Tb.Location = new System.Drawing.Point(241, 251);
            this.H16Tb.Name = "H16Tb";
            this.H16Tb.Size = new System.Drawing.Size(73, 20);
            this.H16Tb.TabIndex = 15;
            this.H16Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H16Tb.Validated += new System.EventHandler(this.H16Tb_Validated);
            // 
            // H15Tb
            // 
            this.H15Tb.Location = new System.Drawing.Point(241, 219);
            this.H15Tb.Name = "H15Tb";
            this.H15Tb.Size = new System.Drawing.Size(73, 20);
            this.H15Tb.TabIndex = 14;
            this.H15Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H15Tb.Validated += new System.EventHandler(this.H15Tb_Validated);
            // 
            // H14Tb
            // 
            this.H14Tb.Location = new System.Drawing.Point(241, 188);
            this.H14Tb.Name = "H14Tb";
            this.H14Tb.Size = new System.Drawing.Size(73, 20);
            this.H14Tb.TabIndex = 13;
            this.H14Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H14Tb.Validated += new System.EventHandler(this.H14Tb_Validated);
            // 
            // H13Tb
            // 
            this.H13Tb.Location = new System.Drawing.Point(241, 156);
            this.H13Tb.Name = "H13Tb";
            this.H13Tb.Size = new System.Drawing.Size(73, 20);
            this.H13Tb.TabIndex = 12;
            this.H13Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H13Tb.Validated += new System.EventHandler(this.H13Tb_Validated);
            // 
            // H12Tb
            // 
            this.H12Tb.Location = new System.Drawing.Point(241, 124);
            this.H12Tb.Name = "H12Tb";
            this.H12Tb.Size = new System.Drawing.Size(73, 20);
            this.H12Tb.TabIndex = 11;
            this.H12Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H12Tb.Validated += new System.EventHandler(this.H12Tb_Validated);
            // 
            // H11Tb
            // 
            this.H11Tb.Location = new System.Drawing.Point(241, 91);
            this.H11Tb.Name = "H11Tb";
            this.H11Tb.Size = new System.Drawing.Size(73, 20);
            this.H11Tb.TabIndex = 10;
            this.H11Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H11Tb.Validated += new System.EventHandler(this.H11Tb_Validated);
            // 
            // H10Tb
            // 
            this.H10Tb.Location = new System.Drawing.Point(241, 62);
            this.H10Tb.Name = "H10Tb";
            this.H10Tb.Size = new System.Drawing.Size(73, 20);
            this.H10Tb.TabIndex = 9;
            this.H10Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H10Tb.Validated += new System.EventHandler(this.H10Tb_Validated);
            // 
            // H9Tb
            // 
            this.H9Tb.Location = new System.Drawing.Point(241, 31);
            this.H9Tb.Name = "H9Tb";
            this.H9Tb.Size = new System.Drawing.Size(73, 20);
            this.H9Tb.TabIndex = 8;
            this.H9Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H9Tb.Validated += new System.EventHandler(this.H9Tb_Validated);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Location = new System.Drawing.Point(187, 253);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(48, 13);
            this.label19.TabIndex = 104;
            this.label19.Text = "Hour16 :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Location = new System.Drawing.Point(187, 223);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(48, 13);
            this.label20.TabIndex = 102;
            this.label20.Text = "Hour15 :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(187, 191);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(48, 13);
            this.label21.TabIndex = 99;
            this.label21.Text = "Hour14 :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Location = new System.Drawing.Point(187, 159);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(48, 13);
            this.label22.TabIndex = 97;
            this.label22.Text = "Hour13 :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Location = new System.Drawing.Point(187, 127);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(48, 13);
            this.label23.TabIndex = 95;
            this.label23.Text = "Hour12 :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(187, 96);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 13);
            this.label24.TabIndex = 94;
            this.label24.Text = "Hour11 :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Location = new System.Drawing.Point(187, 67);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(48, 13);
            this.label25.TabIndex = 92;
            this.label25.Text = "Hour10 :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Location = new System.Drawing.Point(193, 34);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(42, 13);
            this.label26.TabIndex = 90;
            this.label26.Text = "Hour9 :";
            // 
            // H24Tb
            // 
            this.H24Tb.Location = new System.Drawing.Point(426, 255);
            this.H24Tb.Name = "H24Tb";
            this.H24Tb.Size = new System.Drawing.Size(73, 20);
            this.H24Tb.TabIndex = 23;
            this.H24Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H24Tb.Validated += new System.EventHandler(this.H24Tb_Validated);
            // 
            // H23Tb
            // 
            this.H23Tb.Location = new System.Drawing.Point(426, 223);
            this.H23Tb.Name = "H23Tb";
            this.H23Tb.Size = new System.Drawing.Size(73, 20);
            this.H23Tb.TabIndex = 22;
            this.H23Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H23Tb.Validated += new System.EventHandler(this.H23Tb_Validated);
            // 
            // H22Tb
            // 
            this.H22Tb.Location = new System.Drawing.Point(426, 192);
            this.H22Tb.Name = "H22Tb";
            this.H22Tb.Size = new System.Drawing.Size(73, 20);
            this.H22Tb.TabIndex = 21;
            this.H22Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H22Tb.Validated += new System.EventHandler(this.H22Tb_Validated);
            // 
            // H21Tb
            // 
            this.H21Tb.Location = new System.Drawing.Point(426, 160);
            this.H21Tb.Name = "H21Tb";
            this.H21Tb.Size = new System.Drawing.Size(73, 20);
            this.H21Tb.TabIndex = 20;
            this.H21Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H21Tb.Validated += new System.EventHandler(this.H21Tb_Validated);
            // 
            // H20Tb
            // 
            this.H20Tb.Location = new System.Drawing.Point(426, 128);
            this.H20Tb.Name = "H20Tb";
            this.H20Tb.Size = new System.Drawing.Size(73, 20);
            this.H20Tb.TabIndex = 19;
            this.H20Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H20Tb.Validated += new System.EventHandler(this.H20Tb_Validated);
            // 
            // H19Tb
            // 
            this.H19Tb.Location = new System.Drawing.Point(426, 95);
            this.H19Tb.Name = "H19Tb";
            this.H19Tb.Size = new System.Drawing.Size(73, 20);
            this.H19Tb.TabIndex = 18;
            this.H19Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H19Tb.Validated += new System.EventHandler(this.H19Tb_Validated);
            // 
            // H18Tb
            // 
            this.H18Tb.Location = new System.Drawing.Point(426, 66);
            this.H18Tb.Name = "H18Tb";
            this.H18Tb.Size = new System.Drawing.Size(73, 20);
            this.H18Tb.TabIndex = 17;
            this.H18Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H18Tb.Validated += new System.EventHandler(this.H18Tb_Validated);
            // 
            // H17Tb
            // 
            this.H17Tb.Location = new System.Drawing.Point(426, 35);
            this.H17Tb.Name = "H17Tb";
            this.H17Tb.Size = new System.Drawing.Size(73, 20);
            this.H17Tb.TabIndex = 16;
            this.H17Tb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.H17Tb.Validated += new System.EventHandler(this.H17Tb_Validated);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(373, 257);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 120;
            this.label10.Text = "Hour24 :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(373, 227);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 13);
            this.label11.TabIndex = 118;
            this.label11.Text = "Hour23 :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(373, 195);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 13);
            this.label12.TabIndex = 115;
            this.label12.Text = "Hour22 :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(373, 163);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 113;
            this.label13.Text = "Hour21 :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(373, 131);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 13);
            this.label14.TabIndex = 111;
            this.label14.Text = "Hour20 :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(373, 100);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 13);
            this.label15.TabIndex = 110;
            this.label15.Text = "Hour19 :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(373, 71);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 13);
            this.label16.TabIndex = 108;
            this.label16.Text = "Hour18 :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(373, 38);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 13);
            this.label17.TabIndex = 106;
            this.label17.Text = "Hour17 :";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // FromValidate
            // 
            this.FromValidate.AutoSize = true;
            this.FromValidate.BackColor = System.Drawing.Color.Transparent;
            this.FromValidate.ForeColor = System.Drawing.Color.Red;
            this.FromValidate.Location = new System.Drawing.Point(232, 308);
            this.FromValidate.Name = "FromValidate";
            this.FromValidate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.FromValidate.Size = new System.Drawing.Size(11, 13);
            this.FromValidate.TabIndex = 121;
            this.FromValidate.Text = "*";
            this.FromValidate.Visible = false;
            // 
            // ToValidate
            // 
            this.ToValidate.AutoSize = true;
            this.ToValidate.BackColor = System.Drawing.Color.Transparent;
            this.ToValidate.ForeColor = System.Drawing.Color.Red;
            this.ToValidate.Location = new System.Drawing.Point(470, 308);
            this.ToValidate.Name = "ToValidate";
            this.ToValidate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ToValidate.Size = new System.Drawing.Size(11, 13);
            this.ToValidate.TabIndex = 122;
            this.ToValidate.Text = "*";
            this.ToValidate.Visible = false;
            // 
            // EfficiencyMarketForm
            // 
            this.AcceptButton = this.SaveBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(530, 400);
            this.Controls.Add(this.ToValidate);
            this.Controls.Add(this.FromValidate);
            this.Controls.Add(this.H24Tb);
            this.Controls.Add(this.H23Tb);
            this.Controls.Add(this.H22Tb);
            this.Controls.Add(this.H21Tb);
            this.Controls.Add(this.H20Tb);
            this.Controls.Add(this.H19Tb);
            this.Controls.Add(this.H18Tb);
            this.Controls.Add(this.H17Tb);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.H16Tb);
            this.Controls.Add(this.H15Tb);
            this.Controls.Add(this.H14Tb);
            this.Controls.Add(this.H13Tb);
            this.Controls.Add(this.H12Tb);
            this.Controls.Add(this.H11Tb);
            this.Controls.Add(this.H10Tb);
            this.Controls.Add(this.H9Tb);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.ToDateCal);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.H8Tb);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.FromDateCal);
            this.Controls.Add(this.H7Tb);
            this.Controls.Add(this.H6Tb);
            this.Controls.Add(this.H5Tb);
            this.Controls.Add(this.H4Tb);
            this.Controls.Add(this.H3Tb);
            this.Controls.Add(this.H2Tb);
            this.Controls.Add(this.H1Tb);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "EfficiencyMarketForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Efficiency Market";
            this.Load += new System.EventHandler(this.EfficiencyMarketForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox H8Tb;
        private System.Windows.Forms.Button SaveBtn;
        private FarsiLibrary.Win.Controls.FADatePicker FromDateCal;
        private System.Windows.Forms.TextBox H7Tb;
        private System.Windows.Forms.TextBox H6Tb;
        private System.Windows.Forms.TextBox H5Tb;
        private System.Windows.Forms.TextBox H4Tb;
        private System.Windows.Forms.TextBox H3Tb;
        private System.Windows.Forms.TextBox H2Tb;
        private System.Windows.Forms.TextBox H1Tb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label18;
        private FarsiLibrary.Win.Controls.FADatePicker ToDateCal;
        private System.Windows.Forms.TextBox H16Tb;
        private System.Windows.Forms.TextBox H15Tb;
        private System.Windows.Forms.TextBox H14Tb;
        private System.Windows.Forms.TextBox H13Tb;
        private System.Windows.Forms.TextBox H12Tb;
        private System.Windows.Forms.TextBox H11Tb;
        private System.Windows.Forms.TextBox H10Tb;
        private System.Windows.Forms.TextBox H9Tb;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox H24Tb;
        private System.Windows.Forms.TextBox H23Tb;
        private System.Windows.Forms.TextBox H22Tb;
        private System.Windows.Forms.TextBox H21Tb;
        private System.Windows.Forms.TextBox H20Tb;
        private System.Windows.Forms.TextBox H19Tb;
        private System.Windows.Forms.TextBox H18Tb;
        private System.Windows.Forms.TextBox H17Tb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label ToValidate;
        private System.Windows.Forms.Label FromValidate;
    }
}