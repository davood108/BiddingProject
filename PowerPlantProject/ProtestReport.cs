﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using NRI.SBS.Common;
using FarsiLibrary.Utils;
using System.Collections;
using System.IO;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using Auto_Update_Library;

namespace PowerPlantProject
{
    public partial class ProtestReport : Form
    {
        public ProtestReport()
        {
            InitializeComponent(); 
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
               
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////

            }
            catch
            {

            }
        }

        private void ProtestReport_Load(object sender, EventArgs e)
        {
            DataTable cc = Utilities.GetTable("select distinct ppid from powerplant");
            CMBPLANT.Text = cc.Rows[0][0].ToString().Trim();
            foreach (DataRow n in cc.Rows)
            {
                CMBPLANT.Items.Add(n[0].ToString().Trim());
            }
            faDatePicker1.Text = new PersianDate(DateTime.Now).ToString("d");
            faDatePicke2.Text = new PersianDate(DateTime.Now.AddDays(1)).ToString("d");

           

        }
        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {


            DialogResult answer = folderBrowserDialog1.ShowDialog();

            if (answer == DialogResult.OK)
            {

                try
                {
                    string path = folderBrowserDialog1.SelectedPath;

                  


                  exportitmsBasedDaily(path);
                }
                catch
                {
                    MessageBox.Show("Unsuccess!!!!");

                }
            }
        }
        public void exportitmsBasedDaily(string path)
        {
            string firstdate = faDatePicker1.Text.Substring(0, 4).Trim() + "/01/01";
            DateTime first = new DateTime(PersianDateConverter.ToGregorianDateTime(firstdate).Year, PersianDateConverter.ToGregorianDateTime(firstdate).Month, PersianDateConverter.ToGregorianDateTime(firstdate).Day,0,0,0);
             
          
              string price = "All-Y";            

            int cIk = 7;
            string []casename=new string[cIk];
            casename[0]="s18";
            casename[1]="s19";
            casename[2]="s45";
            casename[3]="s46";
            casename[4]="s47";
            casename[5]="D1";
            casename[6]="D2";          

            TimeSpan span=PersianDateConverter.ToGregorianDateTime(faDatePicke2.Text)-PersianDateConverter.ToGregorianDateTime(faDatePicker1.Text);

            string[,,] val = new string[span.Days+1,24,cIk];
            string[] date = new string[span.Days+1];
            for (int i = 0; i < span.Days+1; i++)
			{
                 date[i]=new PersianDate( PersianDateConverter.ToGregorianDateTime(faDatePicker1.Text).AddDays(i)).ToString("d");
			 for (int h = 0; h < 24; h++)
			   {
                

                     DataTable ddd=Utilities.GetTable("select sum(D1),sum(D2) from dbo.PowerLimmited where date='"+date[i]+"'and hour='"+(h+1)+"' and ppid='"+CMBPLANT.Text.Trim()+"'");
		             DataTable dm=Utilities.GetTable("select sum(s18),sum(s19),sum(s45),sum(s46),sum(s47) from dbo.MonthlyBillPlant where date='"+date[i]+"' and hour='"+(h+1)+"' and ppid='"+CMBPLANT.Text.Trim()+"'");
                     if(ddd.Rows.Count>0 && dm.Rows.Count>0)
                     {
                         val[i,h,0]=dm.Rows[0][0].ToString();
                           val[i,h,1]=dm.Rows[0][1].ToString();
                           val[i,h,2]=dm.Rows[0][2].ToString();
                           val[i,h,3]=dm.Rows[0][3].ToString();
                           val[i,h,4]=dm.Rows[0][4].ToString();
                           val[i,h,5]=Math.Round(MyDoubleParse(ddd.Rows[0][0].ToString()), 1).ToString();
                           val[i, h, 6] = Math.Round(MyDoubleParse(ddd.Rows[0][1].ToString()), 1).ToString();

                     }
                      else
                     {
                       
                    MessageBox.Show("Incpmplete date.");
                    return;

                
                     }

                  
             }
            }
            

            //////////////////////////export//////////////////////////


            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            Excel.Application oExcel = new Excel.Application();

            oExcel.SheetsInNewWorkbook = 1;
            Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
            Excel.Worksheet sheet = null;
            sheet = ((Excel.Worksheet)WB.Worksheets["Sheet1"]);
            Excel.Range range = null;

            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers
            oExcel.DefaultSheetDirection = (int)Excel.Constants.xlLeftToRight;

            sheet.DisplayRightToLeft = true; ;

            int sheetIndex = 1;

            sheet.Name =price ;

            ///////////////////////////////////////////
            Excel.Range range8 = (Excel.Range)sheet.Cells[1, 1];
          
            range8.Value2 = "نيروگاه" ;
            range8.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range8.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range8.Columns.Font.Size = 10;
            range8.Columns.AutoFit();
            range8.Cells.RowHeight = 60;
            range8.ColumnWidth = 6;
            range8.Borders.Weight = 3;
            range8.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));
            //
            Excel.Range range9 = (Excel.Range)sheet.Cells[1, 2];          
            range9.Value2 = "تاريخ" ;
            range9.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range9.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range9.Columns.AutoFit();
            range9.ColumnWidth = 10;
            //range.Borders.Weight = 3;
            range9.Borders.LineStyle = Excel.Constants.xlSolid;
            range9.Cells.RowHeight = 60;
            range9.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));
            ///




            range = (Excel.Range)sheet.Cells[1, 3];
            range.Value2 = "ساعت روز";
            range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range.Columns.AutoFit();
            range.ColumnWidth = 8;
            //range.Borders.Weight = 3;
            range.Borders.LineStyle = Excel.Constants.xlSolid;
            range.Cells.RowHeight = 60;
            range.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));




            Excel.Range range1 = (Excel.Range)sheet.Cells[1, 4];
            range1.Value2 = "ساعت سال";
            range1.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range1.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range1.Columns.AutoFit();
            range1.ColumnWidth = 8;
            //range1.Borders.Weight = 3;
            range1.Borders.LineStyle = Excel.Constants.xlSolid;
            range1.Cells.RowHeight = 60;
            range1.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));

            Excel.Range range2 = (Excel.Range)sheet.Cells[1, 5];
            range2.Value2 = "مقدار جریمه آزمون ناموفق ظرفیت18-صورت‌حساب";
            range2.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range2.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range2.Columns.AutoFit();
             range2.ColumnWidth = 18;
            //range2.Borders.Weight = 3;
            range2.Borders.LineStyle = Excel.Constants.xlSolid;
            range2.Cells.RowHeight = 60;
            range2.WrapText = true;
            range2.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));

            Excel.Range range3 = (Excel.Range)sheet.Cells[1, 6];
            range3.Value2 = "مبلغ جریمه آزمون ناموفق ظرفیت 19-صورت‌حساب";
            range3.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range3.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range3.Columns.AutoFit();
            range3.ColumnWidth = 16;
            //range3.Borders.Weight = 3;
            range3.Borders.LineStyle = Excel.Constants.xlSolid;
            range3.Cells.RowHeight = 60;
            range3.WrapText = true;
            range3.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));


            Excel.Range range4 = (Excel.Range)sheet.Cells[1, 7];
            range4.Value2 = "مبلغ پنالتي 1 آزمون ظرفيت 45-صورت‌حساب";
            range4.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range4.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range4.Columns.AutoFit();
            range4.ColumnWidth = 16;
            //range4.Borders.Weight = 3;
            range4.Borders.LineStyle = Excel.Constants.xlSolid;
            range4.Cells.RowHeight = 60;
            range4.WrapText = true;
            range4.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));

            Excel.Range range5 = (Excel.Range)sheet.Cells[1, 8];
            range5.Value2 = "مبلغ پنالتي 2 آزمون ظرفيت 46-صورت‌حساب";
            range5.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range5.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range5.Columns.AutoFit();
            range5.ColumnWidth = 16;
            //range5.Borders.Weight = 3;
            range5.Borders.LineStyle = Excel.Constants.xlSolid;
            range5.Cells.RowHeight = 60;
            range5.WrapText = true;
            range5.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));

            Excel.Range range6 = (Excel.Range)sheet.Cells[1, 9];
            range6.Value2 = "مبلغ برگشت آمادگي 47-صورت‌حساب";
            range6.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range6.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range6.Columns.AutoFit();
            range6.ColumnWidth = 16;
            //range6.Borders.Weight = 3;
            range6.Borders.LineStyle = Excel.Constants.xlSolid;
            range6.Cells.RowHeight = 60;
            range6.WrapText = true;
            range6.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));

            Excel.Range range7 = (Excel.Range)sheet.Cells[1, 10];
            range7.Value2 ="میزان D1 آزمون ظرفيت آمادگی و آزمون";
            range7.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range7.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range7.Columns.AutoFit();
            range7.ColumnWidth = 16;
            //range7.Borders.Weight = 3;
            range7.Borders.LineStyle = Excel.Constants.xlSolid;
            range7.Cells.RowHeight = 60;
            range7.WrapText = true;
            range7.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));


            Excel.Range rangeU = (Excel.Range)sheet.Cells[1, 11];
            rangeU.Value2 = "میزان D2 آزمون ظرفيت آمادگی و آزمون";
            rangeU.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            rangeU.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            rangeU.Columns.AutoFit();
            rangeU.ColumnWidth = 16;
            //rangeU.Borders.Weight = 3;
            rangeU.Borders.LineStyle = Excel.Constants.xlSolid;
            rangeU.Cells.RowHeight = 60;
            rangeU.WrapText = true;
            rangeU.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));



            Excel.Range rangeUe = (Excel.Range)sheet.Cells[1, 12];
            rangeUe.Value2 = "جریمه تأیید می‌شود (OK/NO)؟";
            rangeUe.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            rangeUe.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            rangeUe.Columns.AutoFit();
            rangeUe.ColumnWidth = 13;
            //rangeU.Borders.Weight = 3;
            rangeUe.Borders.LineStyle = Excel.Constants.xlSolid;
           // rangeUe.Cells.RowHeight = 60;
            rangeUe.WrapText = true;
            rangeUe.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));

            Excel.Range rangeUq = (Excel.Range)sheet.Cells[1, 13];
            rangeUq.Value2 = "علت جریمه";
            rangeUq.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            rangeUq.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            rangeUq.Columns.AutoFit();
            rangeUq.ColumnWidth = 48;
            //rangeU.Borders.Weight = 3;
            rangeUq.Borders.LineStyle = Excel.Constants.xlSolid;
           // rangeUe.Cells.RowHeight = 60;
            rangeUq.WrapText = true;
            rangeUq.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));


            ////////fill////////////////////////////////////////////////////////

            int ip=2;
            for (int y = 0; y < val.GetLength(0); y++)
            {               

                    for (int h = 0; h < val.GetLength(1); h++)
                    {
                        if (val[y, h, 0] != "0" )
                        {
                            if (val[y, h, 2] != "0" || val[y, h, 3] != "0")
                            {
                                range = (Excel.Range)sheet.Cells[ip, 1];
                                range.Value2 = CMBPLANT.Text.Trim()+"    ";
                                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                range.Columns.AutoFit();
                                 range.ColumnWidth = 6;
                                //range.Borders.Weight = 16;
                                range.Borders.LineStyle = Excel.Constants.xlSolid;
                                range.Cells.RowHeight = 20;
                                range.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(255, 255, 0));
                                range.WrapText = true;

                                range = (Excel.Range)sheet.Cells[ip, 2];
                                range.Value2 = date[y];
                                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                range.Columns.AutoFit();
                                // range.ColumnWidth = 16;
                                //range.Borders.Weight = 16;
                                range.Borders.LineStyle = Excel.Constants.xlSolid;
                                range.Cells.RowHeight = 20;
                                range.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(255, 255, 0));
                                range.WrapText = true;


                                range = (Excel.Range)sheet.Cells[ip, 3];
                                range.Value2 = (h + 1)+"    ";
                                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                range.Columns.AutoFit();
                                range.ColumnWidth = 8;
                                //range.Borders.Weight = 16;
                                range.Borders.LineStyle = Excel.Constants.xlSolid;
                                range.Cells.RowHeight = 20;
                                range.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(255, 255, 0));
                                range.WrapText = true;



                                /////////////////////////////////////////////////////////////
                              int nowday = PersianDateConverter.ToGregorianDateTime(date[y]).Day;
                              int nowmonth = PersianDateConverter.ToGregorianDateTime(date[y]).Month;
                              int nowyear = PersianDateConverter.ToGregorianDateTime(date[y]).Year;
                              DateTime now = new DateTime(nowyear, nowmonth, nowday, (h),0,0);
                              TimeSpan drr = now - first;
                              double hourofyear = drr.TotalHours;
                              

                                range = (Excel.Range)sheet.Cells[ip, 4];
                                range.Value2 = (hourofyear+1)+"     ";
                                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                range.Columns.AutoFit();
                                range.ColumnWidth = 8;
                                //range.Borders.Weight = 16;
                                range.Borders.LineStyle = Excel.Constants.xlSolid;
                                range.Cells.RowHeight = 20;
                                range.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(255, 255, 0));
                                range.WrapText = true;
                                ////////////////////////////////////////////////////////////////////

                                range = (Excel.Range)sheet.Cells[ip, 12];
                                range.Value2 = "";
                                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                range.Columns.AutoFit();                           
                                range.Borders.LineStyle = Excel.Constants.xlSolid;
                                range.Cells.RowHeight = 20;
                                range.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(255, 255, 0));
                                range.WrapText = true;

                                range = (Excel.Range)sheet.Cells[ip, 13];
                                range.Value2 = "";
                                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                range.Columns.AutoFit();                               
                                range.Borders.LineStyle = Excel.Constants.xlSolid;
                                range.Cells.RowHeight = 20;
                                range.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(255, 255, 0));
                                range.WrapText = true;



                                for (int x = 0; x < val.GetLength(2); x++)
                                {
                                    
                                    range = (Excel.Range)sheet.Cells[ip, x + 5];
                                    range.Value2 = val[y, h, x];
                                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                    range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                    range.Columns.AutoFit();
                                    range.ColumnWidth = 16;
                                    //range.Borders.Weight = 16;
                                    range.Borders.LineStyle = Excel.Constants.xlSolid;
                                    range.Cells.RowHeight = 20;
                                    range.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(0, 255, 0));
                                    range.WrapText = true;

                                   

                                }
                                ip++;
                            }
                        }
                }
            }


          


            sheet.Activate();



            string filePath = path + "\\" + " Reply PenaltyList_" + CMBPLANT.Text.Trim()+"_"+faDatePicke2.Text.Replace("/", "").Substring(0,6) + ".xls";

            WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            WB.Close(Missing.Value, Missing.Value, Missing.Value);
            oExcel.Workbooks.Close();
            oExcel.Quit();

            /////////////////////////////
            MessageBox.Show("Successfully Export In" + filePath, "Export");
        }

        private void faDatePicker1_ValueChanged(object sender, EventArgs e)
        {
            faDatePicke2.Text = faDatePicker1.Text;
        }
      
    }
}
