﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using NRI.SBS.Common;
using FarsiLibrary.Utils;
using System.Collections;
using System.IO;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using Auto_Update_Library;

namespace PowerPlantProject
{
    public partial class Daramad : Form
    {
        int span = 0;
        string ww = "";
        double basse = 0;
        double peak = 0;
        double medium = 0;
        double holidaybasse = 0;
        double holidaypeak = 0;
        double holidaymedium = 0;


        double tolidbasse = 0;
        double tolidpeak = 0;
        double tolidmedium = 0;
        double tolidholidaybasse = 0;
        double tolidholidaypeak = 0;
        double tolidholidaymedium = 0;



        public Daramad(string type)
        {
         
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {
            }

        }
        public bool isweekend(string date)
        {
            DataTable dtweekend1 = Utilities.GetTable("select distinct Date from dbo.WeekEnd");
            foreach (DataRow nrow in dtweekend1.Rows)
            {

                if (nrow[0].ToString().Trim() == date)
                {

                    return true;
                    break;

                }


            }

            string name = PersianDateConverter.ToGregorianDateTime(date).DayOfWeek.ToString();
            if (name == "Friday" || name == "Thursday")
            {
                return true;
            }
            return false;
        }
        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult m = folderBrowserDialog1.ShowDialog();
            if (m == DialogResult.OK)
            {
                span =( PersianDateConverter.ToGregorianDateTime(faDatePicke2.Text).Date - PersianDateConverter.ToGregorianDateTime(faDatePicker1.Text).Date).Days;
                ExportM002Excel();
            }
           
        }

        private void faDatePickerbill_SelectedDateTimeChanged(object sender, EventArgs e)
        {
            faDatePicke2.Text = new PersianDate(PersianDateConverter.ToGregorianDateTime(faDatePicker1.Text).AddDays(1)).ToString("d");
        }

        private void Daramad_Load(object sender, EventArgs e)
        {
           
            DataTable cc = Utilities.GetTable("select distinct ppid from powerplant");
            CMBPLANT.Text = cc.Rows[0][0].ToString().Trim();
            foreach (DataRow n in cc.Rows)
            {
                CMBPLANT.Items.Add(n[0].ToString().Trim());
            }
            faDatePicker1.Text = new PersianDate(DateTime.Now).ToString("d");
            faDatePicke2.Text = new PersianDate(DateTime.Now.AddDays(1)).ToString("d");
            
        }
        public void holidayval()
        {
            holidaybasse = 0;
            holidaypeak = 0;
            holidaymedium = 0;
            tolidholidaybasse = 0;
            tolidholidaypeak = 0;
            tolidholidaymedium = 0;
           
            DataTable xxx = null;
            string[] bdate = ww.Split(';');
            int lenght = bdate.Length-1;

            for (int rowDay = 0; rowDay < lenght; rowDay++)
            {

                string ssd = bdate[rowDay];
               
                DataTable zx = Utilities.GetTable("select * from dbo.bourse where fromdate<='" + ssd + "' and todate>='" + ssd + "' order by fromdate,id desc");
                DataTable dt31 = Utilities.GetTable("select AvailableCapacity from  economicplant where Date='" + ssd + "'and ppid='" + CMBPLANT.Text.Trim() + "'");

                double sum = 0;
                for (int i = 0; i < 24; i++)
                {

                   // if (TYPE == "declared")
                        xxx = Utilities.GetTable("select sum(dispachablecapacity) from dbo.DetailFRM002 where TargetMarketDate='" + ssd + "'and  Hour='" + (i + 1) + "'and Estimated=0 and ppid='" + CMBPLANT.Text.Trim() + "'");

                        if (zx.Rows[0][i + 3].ToString() == "Base") holidaybasse += MyDoubleParse(xxx.Rows[0][0].ToString());
                        if (zx.Rows[0][i + 3].ToString() == "Medium") holidaymedium += MyDoubleParse(xxx.Rows[0][0].ToString());
                        if (zx.Rows[0][i + 3].ToString() == "peak") holidaypeak += MyDoubleParse(xxx.Rows[0][0].ToString());

                    //else tolid
                        xxx = Utilities.GetTable("select sum(p) from dbo.DetailFRM009 where TargetMarketDate='" + ssd + "'and  Hour='" + (i) + "'and ppid='" + CMBPLANT.Text.Trim() + "'");
                   
                    if (zx.Rows[0][i + 3].ToString() == "Base") tolidholidaybasse += MyDoubleParse(xxx.Rows[0][0].ToString());
                    if (zx.Rows[0][i + 3].ToString() == "Medium") tolidholidaymedium += MyDoubleParse(xxx.Rows[0][0].ToString());
                    if (zx.Rows[0][i + 3].ToString() == "peak") tolidholidaypeak += MyDoubleParse(xxx.Rows[0][0].ToString());


                }

                //string val = "";
             
                try
                {
                  
                }
                catch
                {

                }
            }
        }
        public void val()
        {
            basse = 0;
            peak = 0;
            medium = 0;
            tolidbasse = 0;
            tolidpeak = 0;
            tolidmedium = 0;

            ww = "";
        
            DataTable xxx = null;
            for (int rowDay = 0; rowDay <= span; rowDay++)
            {

                string ssd = new PersianDate(PersianDateConverter.ToGregorianDateTime(faDatePicker1.Text.Trim()).AddDays(rowDay)).ToString("d");
                if (isweekend(ssd))
                {
                    ww += ssd + ";";
                   
                }
                DataTable zx = Utilities.GetTable("select * from dbo.bourse where fromdate<='" + ssd + "' and todate>='" + ssd + "' order by fromdate,id desc");
                DataTable dt31 = Utilities.GetTable("select AvailableCapacity from  economicplant where Date='" + ssd + "'and ppid='" + CMBPLANT.Text.Trim() + "'");
             
                double sum = 0;
                if (isweekend(ssd)==false)
                {
                    for (int i = 0; i < 24; i++)
                    {


                       // if (TYPE == "declared")
                            xxx = Utilities.GetTable("select sum(dispachablecapacity) from dbo.DetailFRM002 where TargetMarketDate='" + ssd + "'and  Hour='" + (i + 1) + "'and Estimated=0 and ppid='" + CMBPLANT.Text.Trim() + "'");
                            if (zx.Rows[0][i + 3].ToString() == "Base") basse += MyDoubleParse(xxx.Rows[0][0].ToString());
                            if (zx.Rows[0][i + 3].ToString() == "Medium") medium += MyDoubleParse(xxx.Rows[0][0].ToString());
                            if (zx.Rows[0][i + 3].ToString() == "peak") peak += MyDoubleParse(xxx.Rows[0][0].ToString());


                       // else
                            xxx = Utilities.GetTable("select sum(p) from dbo.DetailFRM009 where TargetMarketDate='" + ssd + "'and  Hour='" + (i) + "'and ppid='" + CMBPLANT.Text.Trim() + "'");

                            if (zx.Rows[0][i + 3].ToString() == "Base") tolidbasse += MyDoubleParse(xxx.Rows[0][0].ToString());
                            if (zx.Rows[0][i + 3].ToString() == "Medium") tolidmedium += MyDoubleParse(xxx.Rows[0][0].ToString());
                            if (zx.Rows[0][i + 3].ToString() == "peak") tolidpeak += MyDoubleParse(xxx.Rows[0][0].ToString());

                    }
                }

              
                try
                {
                   
                }
                catch
                {

                }
            }
        }
        public void ExportM002Excel()
        {

            try
            {
                val();
                holidayval();
            }
            catch
            {

            }
            if (txtamadegi.Text!="")
            {

                DataTable cc = Utilities.GetTable("select distinct pnamefarsi from powerplant where ppid='"+CMBPLANT.Text.Trim()+"'");
             
               
                string month = "";
                string date = faDatePicker1.Text.Substring(5, 2).Trim();
                if (date == "01") month = "فروردين";
                if (date == "02") month = "ارديبهشت";
                if (date == "03") month = "خرداد";
                if (date == "04") month = "تير";
                if (date == "05") month = "مرداد";
                if (date == "06") month = "شهريور";
                if (date == "07") month = "مهر";
                if (date == "08") month = "ابان";
                if (date == "09") month = "اذر";
                if (date == "10") month = "دي";
                if (date == "11") month = "بهمن";
                if (date == "12") month = "اسفند";


                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                Excel.Application oExcel = new Excel.Application();

                oExcel.SheetsInNewWorkbook = 1;
                Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
                Excel.Worksheet sheet = null;
                Excel.Range range = null;

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers
                oExcel.DefaultSheetDirection = (int)Excel.Constants.xlRTL;

                int sheetIndex = 1;
                if (sheet != null)
                    sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
                else
                    sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

                sheet.Name = (date);



                for (int i = 1; i <= 60; i++)
                {


                    range = (Excel.Range)sheet.Cells[1, i];
                    range.Value2 = "";
                    range.RowHeight = 17;
                    range.ColumnWidth = 5;
                    range.Font.Size = 11;

                }






                range = (Excel.Range)sheet.Cells[1, 20];
                range.Value2 = "";
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;

                range = (Excel.Range)sheet.Cells[2, 1];
                Excel.Range range1 = sheet.get_Range(sheet.Cells[2, 35], sheet.Cells[2, 1]);
                range1.Merge(true);
                range1.Value2 = "نام شركت";
                range1.Font.Bold = true;
                range1.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range1.Font.Size = 12;
                range1.RowHeight = 18;
                range1.ColumnWidth = 5;
     

                range = (Excel.Range)sheet.Cells[4, 1];
                Excel.Range range5 = sheet.get_Range(sheet.Cells[4, 31], sheet.Cells[4, 1]);
                range5.Merge(true);
                range5.Value2 = month + faDatePicker1.Text.Substring(0, 4) + " -" + cc.Rows[0][0].ToString().Trim() + "  صورت وضعيت نيروگاه ";
                range5.Font.Bold = true;
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 12;
                range.RowHeight = 18;
                range.ColumnWidth = 5;
                range.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(204, 255, 255));
                range5.Borders.LineStyle = Excel.Constants.xlSolid;
                range5.Borders.Weight = 2;
               
               
                range = (Excel.Range)sheet.Cells[5, 1];
                range.Value2 = faDatePicker1.Text.Trim() + " از تاريخ ";
                Excel.Range range12 = sheet.get_Range(sheet.Cells[5,5], sheet.Cells[5, 1]);
                range12.Merge(true);
                range12.Font.Bold = true;
                range12.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range12.Font.Size = 11;
                range12.RowHeight = 18;
                range12.ColumnWidth = 5;


             
                range = (Excel.Range)sheet.Cells[5, 6];
                range.Value2 = faDatePicke2.Text.Trim() + "  تا تاريخ  ";
                Excel.Range range14 = sheet.get_Range(sheet.Cells[5, 10], sheet.Cells[5, 6]);
                range14.Merge(true);
                range14.Font.Bold = true;
                range14.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range14.Font.Size = 11;
                range14.RowHeight = 18;
                range14.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[5, 11];
                range.Value2 = "درآمد آمادگي";
                Excel.Range range13 = sheet.get_Range(sheet.Cells[5, 31], sheet.Cells[5, 11]);
                range13.Merge(true);
                range13.Font.Bold = true;
                range13.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range13.Font.Size = 11;
                range13.RowHeight = 18;
                range13.ColumnWidth = 5;
                range13.Borders.LineStyle = Excel.Constants.xlSolid;
                range13.Borders.Weight = 2;
                range13.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(255, 255,0));


                ///////////////////////////////////////////////////////////////////////////

                range = (Excel.Range)sheet.Cells[6, 1];
                range.Value2 = "شرح";
                Excel.Range range16 = sheet.get_Range(sheet.Cells[6, 5], sheet.Cells[6, 1]);
                range16.Merge(true);
                range16.Font.Bold = true;
                range16.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range16.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range16.Font.Size = 11;
                range16.RowHeight = 35;
                range16.ColumnWidth = 5;
                range16.Borders.LineStyle = Excel.Constants.xlSolid;
                range16.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[7, 1];
                range.Value2 = "";
                Excel.Range range16o = sheet.get_Range(sheet.Cells[7, 5], sheet.Cells[7, 1]);
                range16o.Merge(true);
                range16o.Font.Bold = true;
                range16o.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range16o.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range16o.Font.Size = 11;
                range16o.RowHeight = 35;
                range16o.ColumnWidth = 5;
                range16o.Borders.LineStyle = Excel.Constants.xlSolid;
                range16o.Borders.Weight = 2;




                range = (Excel.Range)sheet.Cells[8, 1];
                range.Value2 = "";
                Excel.Range range16l = sheet.get_Range(sheet.Cells[8, 5], sheet.Cells[8, 1]);
                range16l.Merge(true);
                range16l.Font.Bold = true;
                range16l.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range16l.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range16l.Font.Size = 11;
                range16l.RowHeight = 35;
                range16l.ColumnWidth = 5;
                range16l.Borders.LineStyle = Excel.Constants.xlSolid;
                range16l.Borders.Weight = 2;

                
                //////////////////////////////////////////////////////////////////
                range = (Excel.Range)sheet.Cells[9, 1];
                range.Value2 = "ساعت پبك - 4ساعت";
                Excel.Range range161 = sheet.get_Range(sheet.Cells[9, 5], sheet.Cells[9, 1]);
                range161.Merge(true);
                range161.Font.Bold = true;
                range161.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range161.Font.Size = 11;
                range161.RowHeight = 18;
                range161.ColumnWidth = 5;
                range161.Borders.LineStyle = Excel.Constants.xlSolid;
                range161.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[10, 1];
                range.Value2 = "ساعت ميان باري - 12ساعت";
                Excel.Range range162 = sheet.get_Range(sheet.Cells[10, 5], sheet.Cells[10, 1]);
                range162.Merge(true);
                range162.Font.Bold = true;
                range162.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range162.Font.Size = 11;
                range162.RowHeight = 18;
                range162.ColumnWidth = 5;
                range162.Borders.LineStyle = Excel.Constants.xlSolid;
                range162.Borders.Weight = 2;

                range = (Excel.Range)sheet.Cells[11, 1];
                range.Value2 = "ساعت كم باري - 8ساعت";
                Excel.Range range163 = sheet.get_Range(sheet.Cells[11, 5], sheet.Cells[11, 1]);
                range163.Merge(true);
                range163.Font.Bold = true;
                range163.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range163.Font.Size = 11;
                range163.RowHeight = 18;
                range163.ColumnWidth = 5;
                range163.Borders.LineStyle = Excel.Constants.xlSolid;
                range163.Borders.Weight = 2;


                //peak 

                range = (Excel.Range)sheet.Cells[9, 6];
                range.Value2 = peak;
                Excel.Range range168 = sheet.get_Range(sheet.Cells[9, 8], sheet.Cells[9, 6]);
                range168.Merge(true);
                range168.Font.Bold = true;
                range168.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range168.Font.Size = 11;
                range168.RowHeight = 18;
                range168.ColumnWidth = 5;
                range168.Borders.LineStyle = Excel.Constants.xlSolid;
                range168.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[9 ,9];
                range.Value2 = txtpw.Text;
                Excel.Range range169 = sheet.get_Range(sheet.Cells[9, 9], sheet.Cells[9, 9]);
                range169.Merge(true);
                range169.Font.Bold = true;
                range169.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range169.Font.Size = 11;
                range169.RowHeight = 18;
                range169.ColumnWidth = 5;
                range169.Borders.LineStyle = Excel.Constants.xlSolid;
                range169.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[9, 10];
                range.Value2 = holidaypeak;
                Excel.Range range148 = sheet.get_Range(sheet.Cells[9, 12], sheet.Cells[9, 10]);
                range148.Merge(true);
                range148.Font.Bold = true;
                range148.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range148.Font.Size = 11;
                range148.RowHeight = 18;
                range148.ColumnWidth = 5;
                range148.Borders.LineStyle = Excel.Constants.xlSolid;
                range148.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[9, 13];
                range.Value2 = txtph.Text;
                Excel.Range range149 = sheet.get_Range(sheet.Cells[9, 13], sheet.Cells[9, 13]);
                range149.Merge(true);
                range149.Font.Bold = true;
                range149.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range149.Font.Size = 11;
                range149.RowHeight = 18;
                range149.ColumnWidth = 5;
                range149.Borders.LineStyle = Excel.Constants.xlSolid;
                range149.Borders.Weight = 2;

                /////////////////////medium///////////////////////////////

                range = (Excel.Range)sheet.Cells[10, 6];
                range.Value2 = medium;
                Excel.Range range138 = sheet.get_Range(sheet.Cells[10, 8], sheet.Cells[10, 6]);
                range138.Merge(true);
                range138.Font.Bold = true;
                range138.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range138.Font.Size = 11;
                range138.RowHeight = 18;
                range138.ColumnWidth = 5;
                range138.Borders.LineStyle = Excel.Constants.xlSolid;
                range138.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[10, 9];
                range.Value2 = txtmw.Text;
                Excel.Range range139 = sheet.get_Range(sheet.Cells[10, 9], sheet.Cells[10, 9]);
                range139.Merge(true);
                range139.Font.Bold = true;
                range139.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range139.Font.Size = 11;
                range139.RowHeight = 18;
                range139.ColumnWidth = 5;
                range139.Borders.LineStyle = Excel.Constants.xlSolid;
                range139.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[10, 10];
                range.Value2 = holidaymedium;
                Excel.Range range108 = sheet.get_Range(sheet.Cells[10, 12], sheet.Cells[10, 10]);
                range108.Merge(true);
                range108.Font.Bold = true;
                range108.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range108.Font.Size = 11;
                range108.RowHeight = 18;
                range108.ColumnWidth = 5;
                range108.Borders.LineStyle = Excel.Constants.xlSolid;
                range108.Borders.Weight = 2;




                range = (Excel.Range)sheet.Cells[10, 13];
                range.Value2 = txtmh.Text;
                Excel.Range range109 = sheet.get_Range(sheet.Cells[10, 13], sheet.Cells[10, 13]);
                range109.Merge(true);
                range109.Font.Bold = true;
                range109.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range109.Font.Size = 11;
                range109.RowHeight = 18;
                range109.ColumnWidth = 5;
                range109.Borders.LineStyle = Excel.Constants.xlSolid;
                range109.Borders.Weight = 2;



                /////////////////////////////base/////////////////////////////////////



                range = (Excel.Range)sheet.Cells[11, 6];
                range.Value2 =basse;
                Excel.Range range228 = sheet.get_Range(sheet.Cells[11, 8], sheet.Cells[11, 6]);
                range228.Merge(true);
                range228.Font.Bold = true;
                range228.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range228.Font.Size = 11;
                range228.RowHeight = 18;
                range228.ColumnWidth = 5;
                range228.Borders.LineStyle = Excel.Constants.xlSolid;
                range228.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[11, 9];
                range.Value2 = txtbw.Text;
                Excel.Range range219 = sheet.get_Range(sheet.Cells[11, 9], sheet.Cells[11, 9]);
                range219.Merge(true);
                range219.Font.Bold = true;
                range219.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range219.Font.Size = 11;
                range219.RowHeight = 18;
                range219.ColumnWidth = 5;
                range219.Borders.LineStyle = Excel.Constants.xlSolid;
                range219.Borders.Weight = 2;




                range = (Excel.Range)sheet.Cells[11, 10];
                range.Value2 = holidaybasse;
                Excel.Range range128 = sheet.get_Range(sheet.Cells[11, 12], sheet.Cells[11, 10]);
                range128.Merge(true);
                range128.Font.Bold = true;
                range128.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range128.Font.Size = 11;
                range128.RowHeight = 18;
                range128.ColumnWidth = 5;
                range128.Borders.LineStyle = Excel.Constants.xlSolid;
                range128.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[11, 13];
                range.Value2 = txtbh.Text;
                Excel.Range range119 = sheet.get_Range(sheet.Cells[11,13], sheet.Cells[11, 13]);
                range119.Merge(true);
                range119.Font.Bold = true;
                range119.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range119.Font.Size = 11;
                range119.RowHeight = 18;
                range119.ColumnWidth = 5;
                range119.Borders.LineStyle = Excel.Constants.xlSolid;
                range119.Borders.Weight = 2;

                /////////////////////////////////////////////////////////////////////////////////
              

                range = (Excel.Range)sheet.Cells[12, 1];
                range.Value2 = "جمع";
                Excel.Range range164 = sheet.get_Range(sheet.Cells[12, 5], sheet.Cells[12, 1]);
                range164.Merge(true);
                range164.Font.Bold = true;
                range164.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range164.Font.Size = 11;
                range164.RowHeight = 18;
                range164.ColumnWidth = 5;
                range164.Borders.LineStyle = Excel.Constants.xlSolid;
                range164.Borders.Weight = 2;


        



                range = (Excel.Range)sheet.Cells[12, 6];
                range.Value2 =(basse+peak+medium).ToString();
                Excel.Range range101 = sheet.get_Range(sheet.Cells[12, 8], sheet.Cells[12, 6]);
                range101.Merge(true);
                range101.Font.Bold = true;
                range101.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range101.Font.Size = 11;
                range101.RowHeight = 18;
                range101.ColumnWidth = 5;
                range101.Borders.LineStyle = Excel.Constants.xlSolid;
                range101.Borders.Weight = 2;

                range = (Excel.Range)sheet.Cells[12, 10];
                range.Value2 =( holidaybasse+holidaymedium+holidaypeak).ToString();
                Excel.Range range102 = sheet.get_Range(sheet.Cells[12, 12], sheet.Cells[12, 10]);
                range102.Merge(true);
                range102.Font.Bold = true;
                range102.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range102.Font.Size = 11;
                range102.RowHeight = 18;
                range102.ColumnWidth = 5;
                range102.Borders.LineStyle = Excel.Constants.xlSolid;
                range102.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[12, 9];
                range.Value2 = "";
                Excel.Range range190 = sheet.get_Range(sheet.Cells[12, 9], sheet.Cells[12, 9]);
                range190.Merge(true);
                range190.Font.Bold = true;
                range190.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range190.Font.Size = 11;
                range190.RowHeight = 18;
                range190.ColumnWidth = 5;
                range190.Borders.LineStyle = Excel.Constants.xlSolid;
                range190.Borders.Weight = 2;
                /////////////////////////////////////////////////////////////////////

                range = (Excel.Range)sheet.Cells[6, 6];
                range.Value2 = "روزهاي كاري";
                Excel.Range range151 = sheet.get_Range(sheet.Cells[6, 9], sheet.Cells[6, 6]);
                range151.Merge(true);
                range151.Font.Bold = true;
                range151.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range151.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range151.Font.Size = 11;
                range151.RowHeight = 35;
                range151.ColumnWidth = 5;
                range151.Borders.LineStyle = Excel.Constants.xlSolid;
                range151.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[7, 6];
                range.Value2 = "مقدار-MWH";
                Excel.Range range152 = sheet.get_Range(sheet.Cells[7, 8], sheet.Cells[7, 6]);
                range152.Merge(true);
                range152.Font.Bold = true;
                range152.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range152.Font.Size = 11;
                range152.RowHeight = 18;
                range152.ColumnWidth = 5;
                range152.Borders.LineStyle = Excel.Constants.xlSolid;
                range152.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[7, 14];
                range.Value2 = "مقدار-MWH";
                Excel.Range range152u = sheet.get_Range(sheet.Cells[7, 16], sheet.Cells[7, 14]);
                range152u.Merge(true);
                range152u.Font.Bold = true;
                range152u.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range152u.Font.Size = 11;
                range152u.RowHeight = 18;
                range152u.ColumnWidth = 5;
                range152u.Borders.LineStyle = Excel.Constants.xlSolid;
                range152u.Borders.Weight = 2;

                range = (Excel.Range)sheet.Cells[7, 17];
                range.Value2 = "مقدار-MWH";
                Excel.Range range152r = sheet.get_Range(sheet.Cells[7, 19], sheet.Cells[7, 17]);
                range152r.Merge(true);
                range152r.Font.Bold = true;
                range152r.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range152r.Font.Size = 11;
                range152r.RowHeight = 18;
                range152r.ColumnWidth = 5;
                range152r.Borders.LineStyle = Excel.Constants.xlSolid;
                range152r.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[7, 20];
                range.Value2 = "مقدار-MWH";
                Excel.Range range152q = sheet.get_Range(sheet.Cells[7, 22], sheet.Cells[7, 20]);
                range152q.Merge(true);
                range152q.Font.Bold = true;
                range152q.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range152q.Font.Size = 11;
                range152q.RowHeight = 18;
                range152q.ColumnWidth = 5;
                range152q.Borders.LineStyle = Excel.Constants.xlSolid;
                range152q.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[7, 9];
                range.Value2 = "ضريب";
                Excel.Range range153 = sheet.get_Range(sheet.Cells[7, 9], sheet.Cells[7, 9]);
                range153.Merge(true);
                range153.Font.Bold = true;
                range153.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range153.Font.Size = 11;
                range153.RowHeight = 18;
                range153.ColumnWidth = 5;
                range153.Borders.LineStyle = Excel.Constants.xlSolid;
                range153.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[8, 6];
                range.Value2 = "1";
                Excel.Range range159 = sheet.get_Range(sheet.Cells[8, 8], sheet.Cells[8, 6]);
                range159.Merge(true);
                range159.Font.Bold = true;
                range159.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159.Font.Size = 11;
                range159.RowHeight = 18;
                range159.ColumnWidth = 5;
                range159.Borders.LineStyle = Excel.Constants.xlSolid;
                range159.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[8, 9];
                range.Value2 = "2";
                Excel.Range range158 = sheet.get_Range(sheet.Cells[8, 9], sheet.Cells[8, 9]);
                range158.Merge(true);
                range158.Font.Bold = true;
                range158.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range158.Font.Size = 11;
                range158.RowHeight = 18;
                range158.ColumnWidth = 5;
                range158.Borders.LineStyle = Excel.Constants.xlSolid;
                range158.Borders.Weight = 2;


                ///////////////////////////////////////////////////////////////////

                range = (Excel.Range)sheet.Cells[6, 10];
                range.Value2 = "روزهاي تعطيل";
                Excel.Range range171 = sheet.get_Range(sheet.Cells[6, 13], sheet.Cells[6, 10]);
                range171.Merge(true);
                range171.Font.Bold = true;
                range171.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range171.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range171.Font.Size = 11;
                range171.RowHeight = 35;
                range171.ColumnWidth = 5;
                range171.Borders.LineStyle = Excel.Constants.xlSolid;
                range171.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[7, 10];
                range.Value2 = "مقدار-MWH";
                Excel.Range range154 = sheet.get_Range(sheet.Cells[7, 12], sheet.Cells[7, 10]);
                range154.Merge(true);
                range154.Font.Bold = true;
                range154.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range154.Font.Size = 11;
                range154.RowHeight = 18;
                range154.ColumnWidth = 5;
                range154.Borders.LineStyle = Excel.Constants.xlSolid;
                range154.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[7, 13];
                range.Value2 = "ضريب";
                Excel.Range range155 = sheet.get_Range(sheet.Cells[7, 13], sheet.Cells[7, 13]);
                range155.Merge(true);
                range155.Font.Bold = true;
                range155.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;           
                range155.Font.Size = 11;
                range155.RowHeight = 18;
                range155.ColumnWidth = 5;
                range155.Borders.LineStyle = Excel.Constants.xlSolid;
                range155.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[12, 13];
                range.Value2 = "";
                Excel.Range range195 = sheet.get_Range(sheet.Cells[12, 13], sheet.Cells[12, 13]);
                range195.Merge(true);
                range195.Font.Bold = true;
                range195.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range195.Font.Size = 11;
                range195.RowHeight = 18;
                range195.ColumnWidth = 5;
                range195.Borders.LineStyle = Excel.Constants.xlSolid;
                range195.Borders.Weight = 2;








                range = (Excel.Range)sheet.Cells[8, 10];
                range.Value2 = "3";
                Excel.Range range120 = sheet.get_Range(sheet.Cells[8, 12], sheet.Cells[8, 10]);
                range120.Merge(true);
                range120.Font.Bold = true;
                range120.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range120.Font.Size = 11;
                range120.RowHeight = 18;
                range120.ColumnWidth = 5;
                range120.Borders.LineStyle = Excel.Constants.xlSolid;
                range120.Borders.Weight = 2;




                range = (Excel.Range)sheet.Cells[8, 13];
                range.Value2 = "4";
                Excel.Range range121 = sheet.get_Range(sheet.Cells[8, 13], sheet.Cells[8, 13]);
                range121.Merge(true);
                range121.Font.Bold = true;
                range121.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range121.Font.Size = 11;
                range121.RowHeight = 18;
                range121.ColumnWidth = 5;
                range121.Borders.LineStyle = Excel.Constants.xlSolid;
                range121.Borders.Weight = 2;



                ///////////////////////////////////////////////////////////////
                range = (Excel.Range)sheet.Cells[6, 14];
                range.Value2 = "ضريب ماه ";
                Excel.Range range18 = sheet.get_Range(sheet.Cells[6, 16], sheet.Cells[6, 14]);
                range18.Merge(true);
                range18.Font.Bold = true;
                range18.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range18.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range18.Font.Size = 11;
                range18.RowHeight = 35;
                range18.ColumnWidth = 5;
                range18.Borders.LineStyle = Excel.Constants.xlSolid;
                range18.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[7, 14];
                range.Value2 = "";
                Excel.Range range130 = sheet.get_Range(sheet.Cells[7, 16], sheet.Cells[7, 14]);
                range130.Merge(true);
                range130.Font.Bold = true;
                range130.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range130.Font.Size = 11;
                range130.RowHeight = 18;
                range130.ColumnWidth = 5;
            





                range = (Excel.Range)sheet.Cells[8, 14];
                range.Value2 = "5";
                Excel.Range range122 = sheet.get_Range(sheet.Cells[8, 16], sheet.Cells[8, 14]);
                range122.Merge(true);
                range122.Font.Bold = true;
                range122.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range122.Font.Size = 11;
                range122.RowHeight = 18;
                range122.ColumnWidth = 5;
                range122.Borders.LineStyle = Excel.Constants.xlSolid;
                range122.Borders.Weight = 2;


                //range = (Excel.Range)sheet.Cells[8, 14];
                //range.Value2 = "";
                //Excel.Range range300 = sheet.get_Range(sheet.Cells[8, 16], sheet.Cells[8, 14]);
                //range300.Merge(true);
                //range300.Font.Bold = true;
                //range300.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                //range300.Font.Size = 11;
                //range300.RowHeight = 18;
                //range300.ColumnWidth = 5;
                //range300.Borders.LineStyle = Excel.Constants.xlSolid;
                //range300.Borders.Weight = 2;


                
                
                range = (Excel.Range)sheet.Cells[9, 14];
               // range.Value2 = (MyDoubleParse(txtpw.Text) * peak) + (MyDoubleParse(txtph.Text) *holidaypeak);
                range.Value2 = txtmonth.Text.Trim();
                Excel.Range range107 = sheet.get_Range(sheet.Cells[9, 16], sheet.Cells[9, 14]);
                range107.Merge(true);
                range107.Font.Bold = true;
                range107.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range107.Font.Size = 11;
                range107.RowHeight = 18;
                range107.ColumnWidth = 5;
                range107.Borders.LineStyle = Excel.Constants.xlSolid;
                range107.Borders.Weight = 2;
                double x51 =MyDoubleParse(txtmonth.Text) ;

             
                range = (Excel.Range)sheet.Cells[10, 14];
                //range.Value2 = (MyDoubleParse(txtmw.Text) *medium) + (MyDoubleParse(txtmh.Text) * holidaymedium);
                range.Value2 = txtmonth.Text.Trim();
                Excel.Range range302 = sheet.get_Range(sheet.Cells[10, 16], sheet.Cells[10, 14]);
                range302.Merge(true);
                range302.Font.Bold = true;
                range302.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range302.Font.Size = 11;
                range302.RowHeight = 18;
                range302.ColumnWidth = 5;
                range302.Borders.LineStyle = Excel.Constants.xlSolid;
                range302.Borders.Weight = 2;
                double x52 = (MyDoubleParse(txtmw.Text) * medium) + (MyDoubleParse(txtmh.Text) * holidaymedium);


                range = (Excel.Range)sheet.Cells[11, 14];
                range.Value2 = txtmonth.Text.Trim();
                //range.Value2 = (MyDoubleParse(txtbw.Text) * basse) + (MyDoubleParse(txtbh.Text) * holidaybasse);
                Excel.Range range301 = sheet.get_Range(sheet.Cells[11, 16], sheet.Cells[11, 14]);
                range301.Merge(true);
                range301.Font.Bold = true;
                range301.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range301.Font.Size = 11;
                range301.RowHeight = 18;
                range301.ColumnWidth = 5;
                range301.Borders.LineStyle = Excel.Constants.xlSolid;
                range301.Borders.Weight = 2;
              
                  //  (MyDoubleParse(txtbw.Text) * basse) + (MyDoubleParse(txtbh.Text) * holidaybasse);

                range = (Excel.Range)sheet.Cells[12, 14];
                range.Value2 = "";
                Excel.Range range303 = sheet.get_Range(sheet.Cells[12, 16], sheet.Cells[12, 14]);
                range303.Merge(true);
                range303.Font.Bold = true;
                range303.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range303.Font.Size = 11;
                range303.RowHeight = 18;
                range303.ColumnWidth = 5;
                range303.Borders.LineStyle = Excel.Constants.xlSolid;
                range303.Borders.Weight = 2;



              





                ////////////////////////////////////////////

                range = (Excel.Range)sheet.Cells[6, 17];
                range.Value2 = "جمع روز كاري";
                Excel.Range range19 = sheet.get_Range(sheet.Cells[6, 19], sheet.Cells[6, 17]);
                range19.Merge(true);
                range19.Font.Bold = true;
                range19.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range19.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range19.Font.Size = 11;
                range19.RowHeight = 35;
                range19.ColumnWidth = 5;
                range19.Borders.LineStyle = Excel.Constants.xlSolid;
                range19.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[8, 17];
                range.Value2 = "6=1*2*5";
                Excel.Range range123 = sheet.get_Range(sheet.Cells[8, 19], sheet.Cells[8, 17]);
                range123.Merge(true);
                range123.Font.Bold = true;
                range123.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range123.Font.Size = 11;
                range123.RowHeight = 18;
                range123.ColumnWidth = 5;
                range123.Borders.LineStyle = Excel.Constants.xlSolid;
                range123.Borders.Weight = 2;





                range = (Excel.Range)sheet.Cells[9, 17];
                range.Value2 =peak*MyDoubleParse(txtmonth.Text)*MyDoubleParse(txtpw.Text);
                Excel.Range range10 = sheet.get_Range(sheet.Cells[9, 19], sheet.Cells[9, 17]);
                range10.Merge(true);
                range10.Font.Bold = true;
                range10.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range10.Font.Size = 11;
                range10.RowHeight = 18;
                range10.ColumnWidth = 5;
                range10.Borders.LineStyle = Excel.Constants.xlSolid;
                range10.Borders.Weight = 2;
                double x53 = peak * MyDoubleParse(txtmonth.Text) * MyDoubleParse(txtpw.Text);

                range = (Excel.Range)sheet.Cells[10, 17];
                range.Value2 = medium * MyDoubleParse(txtmonth.Text) * MyDoubleParse(txtmw.Text);
                Excel.Range range11 = sheet.get_Range(sheet.Cells[10, 19], sheet.Cells[10, 17]);
                range11.Merge(true);
                range11.Font.Bold = true;
                range11.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range11.Font.Size = 11;
                range11.RowHeight = 18;
                range11.ColumnWidth = 5;
                range11.Borders.LineStyle = Excel.Constants.xlSolid;
                range11.Borders.Weight = 2;
                double x54 = medium * MyDoubleParse(txtmonth.Text) * MyDoubleParse(txtmw.Text);


                range = (Excel.Range)sheet.Cells[11, 17];
                range.Value2 = basse * MyDoubleParse(txtmonth.Text) * MyDoubleParse(txtbw.Text);
                Excel.Range range17 = sheet.get_Range(sheet.Cells[11, 19], sheet.Cells[11, 17]);
                range17.Merge(true);
                range17.Font.Bold = true;
                range17.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range17.Font.Size = 11;
                range17.RowHeight = 18;
                range17.ColumnWidth = 5;
                range17.Borders.LineStyle = Excel.Constants.xlSolid;
                range17.Borders.Weight = 2;
                double x55 = basse * MyDoubleParse(txtmonth.Text) * MyDoubleParse(txtbw.Text);


                range = (Excel.Range)sheet.Cells[12, 17];
                range.Value2 = (x53+x54+x55).ToString();
                Excel.Range range177 = sheet.get_Range(sheet.Cells[12, 19], sheet.Cells[12, 17]);
                range177.Merge(true);
                range177.Font.Bold = true;
                range177.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range177.Font.Size = 11;
                range177.RowHeight = 18;
                range177.ColumnWidth = 5;
                range177.Borders.LineStyle = Excel.Constants.xlSolid;
                range177.Borders.Weight = 2;
                //////////////////////////////
                range = (Excel.Range)sheet.Cells[6, 20];
                range.Value2 = "جمع روز  تعطيل";
                Excel.Range range20 = sheet.get_Range(sheet.Cells[6, 22], sheet.Cells[6, 20]);
                range20.Merge(true);
                range20.Font.Bold = true;
                range20.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range20.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range20.Font.Size = 11;
                range20.RowHeight = 35;
                range20.ColumnWidth = 5;
                range20.Borders.LineStyle = Excel.Constants.xlSolid;
                range20.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[7, 20];
                range.Value2 = "مقدار-MWH"; 
                Excel.Range range20i = sheet.get_Range(sheet.Cells[6, 22], sheet.Cells[7, 20]);
                range20i.Merge(true);
                range20i.Font.Bold = true;
                range20i.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range20i.Font.Size = 11;
                range20i.RowHeight = 18;
                range20i.ColumnWidth = 5;
                range20i.Borders.LineStyle = Excel.Constants.xlSolid;
                range20i.Borders.Weight = 2;





                range = (Excel.Range)sheet.Cells[8, 20];
                range.Value2 = "7=3*4*5";
                Excel.Range range124 = sheet.get_Range(sheet.Cells[8, 22], sheet.Cells[8, 20]);
                range124.Merge(true);
                range124.Font.Bold = true;
                range124.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range124.Font.Size = 11;
                range124.RowHeight = 18;
                range124.ColumnWidth = 5;
                range124.Borders.LineStyle = Excel.Constants.xlSolid;
                range124.Borders.Weight = 2;





                range = (Excel.Range)sheet.Cells[9, 20];
                range.Value2 = holidaypeak*MyDoubleParse(txtmonth.Text)*MyDoubleParse(txtph.Text);
                Excel.Range range305 = sheet.get_Range(sheet.Cells[9, 22], sheet.Cells[9, 20]);
                range305.Merge(true);
                range305.Font.Bold = true;
                range305.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range305.Font.Size = 11;
                range305.RowHeight = 18;
                range305.ColumnWidth = 5;
                range305.Borders.LineStyle = Excel.Constants.xlSolid;
                range305.Borders.Weight = 2;
                double x71 = holidaypeak * MyDoubleParse(txtmonth.Text) * MyDoubleParse(txtph.Text);



                range = (Excel.Range)sheet.Cells[10, 20];
                range.Value2 = holidaymedium * MyDoubleParse(txtmonth.Text) * MyDoubleParse(txtmh.Text);
                Excel.Range range308 = sheet.get_Range(sheet.Cells[10, 22], sheet.Cells[10, 20]);
                range308.Merge(true);
                range308.Font.Bold = true;
                range308.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range308.Font.Size = 11;
                range308.RowHeight = 18;
                range308.ColumnWidth = 5;
                range308.Borders.LineStyle = Excel.Constants.xlSolid;
                range308.Borders.Weight = 2;
                double x72 = holidaymedium * MyDoubleParse(txtmonth.Text) * MyDoubleParse(txtmh.Text);



                range = (Excel.Range)sheet.Cells[11, 20];
                range.Value2 = holidaybasse * MyDoubleParse(txtmonth.Text) * MyDoubleParse(txtbh.Text);
                Excel.Range range306 = sheet.get_Range(sheet.Cells[11, 22], sheet.Cells[11, 20]);
                range306.Merge(true);
                range306.Font.Bold = true;
                range306.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range306.Font.Size = 11;
                range306.RowHeight = 18;
                range306.ColumnWidth = 5;
                range306.Borders.LineStyle = Excel.Constants.xlSolid;
                range306.Borders.Weight = 2;
                double x73 = holidaybasse * MyDoubleParse(txtmonth.Text) * MyDoubleParse(txtbh.Text);



                range = (Excel.Range)sheet.Cells[12, 20];
                range.Value2 = (x72+x73+x71).ToString();
                Excel.Range range307 = sheet.get_Range(sheet.Cells[12, 22], sheet.Cells[12, 20]);
                range307.Merge(true);
                range307.Font.Bold = true;
                range307.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range307.Font.Size = 11;
                range307.RowHeight = 18;
                range307.ColumnWidth = 5;
                range307.Borders.LineStyle = Excel.Constants.xlSolid;
                range307.Borders.Weight = 2;


                //////////////////////////////
                range = (Excel.Range)sheet.Cells[6, 23];
                range.Value2 = "جمع كل امادگي";
                Excel.Range range20a = sheet.get_Range(sheet.Cells[6, 25], sheet.Cells[6, 23]);
                range20a.Merge(true);
                range20a.Font.Bold = true;
                range20a.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range20a.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range20a.Font.Size = 11;
                range20a.RowHeight = 35;
                range20a.ColumnWidth = 5;
                range20a.Borders.LineStyle = Excel.Constants.xlSolid;
                range20a.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[7, 23];
                range.Value2 = "مقدار-MWH"; 
                Excel.Range range20pa = sheet.get_Range(sheet.Cells[7, 25], sheet.Cells[7, 23]);
                range20pa.Merge(true);
                range20pa.Font.Bold = true;
                range20pa.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;         
                range20pa.Font.Size = 11;
                range20pa.RowHeight = 18;
                range20pa.ColumnWidth = 5;
                range20pa.Borders.LineStyle = Excel.Constants.xlSolid;
                range20pa.Borders.Weight = 2;








                range = (Excel.Range)sheet.Cells[8, 23];
                range.Value2 = "8=6+7";
                Excel.Range range124a = sheet.get_Range(sheet.Cells[8, 25], sheet.Cells[8, 23]);
                range124a.Merge(true);
                range124a.Font.Bold = true;
                range124a.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range124a.Font.Size = 11;
                range124a.RowHeight = 18;
                range124a.ColumnWidth = 5;
                range124a.Borders.LineStyle = Excel.Constants.xlSolid;
                range124a.Borders.Weight = 2;





                range = (Excel.Range)sheet.Cells[9, 23];
                range.Value2 = (x71+x53).ToString();
                Excel.Range range305a = sheet.get_Range(sheet.Cells[9, 25], sheet.Cells[9, 23]);
                range305a.Merge(true);
                range305a.Font.Bold = true;
                range305a.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range305a.Font.Size = 11;
                range305a.RowHeight = 18;
                range305a.ColumnWidth = 5;
                range305a.Borders.LineStyle = Excel.Constants.xlSolid;
                range305a.Borders.Weight = 2;
                double x71a =(x71+x53);



                range = (Excel.Range)sheet.Cells[10, 23];
                range.Value2 = (x54+x72).ToString();
                Excel.Range range308a = sheet.get_Range(sheet.Cells[10, 25], sheet.Cells[10, 23]);
                range308a.Merge(true);
                range308a.Font.Bold = true;
                range308a.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range308a.Font.Size = 11;
                range308a.RowHeight = 18;
                range308a.ColumnWidth = 5;
                range308a.Borders.LineStyle = Excel.Constants.xlSolid;
                range308a.Borders.Weight = 2;
                double x72a = (x54+x72);


                range = (Excel.Range)sheet.Cells[11, 23];
                range.Value2 = (x55+x73).ToString();
                Excel.Range range306a = sheet.get_Range(sheet.Cells[11, 25], sheet.Cells[11, 23]);
                range306a.Merge(true);
                range306a.Font.Bold = true;
                range306a.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range306a.Font.Size = 11;
                range306a.RowHeight = 18;
                range306a.ColumnWidth = 5;
                range306a.Borders.LineStyle = Excel.Constants.xlSolid;
                range306a.Borders.Weight = 2;
                double x73a = (x55+x73);



                range = (Excel.Range)sheet.Cells[12, 23];
                range.Value2 =(x71a+x72a+x73a).ToString();
                Excel.Range range307a = sheet.get_Range(sheet.Cells[12, 25], sheet.Cells[12, 23]);
                range307a.Merge(true);
                range307a.Font.Bold = true;
                range307a.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range307a.Font.Size = 11;
                range307a.RowHeight = 18;
                range307a.ColumnWidth = 5;
                range307a.Borders.LineStyle = Excel.Constants.xlSolid;
                range307a.Borders.Weight = 2;



                //////////////////////////////
                range = (Excel.Range)sheet.Cells[6, 26];
                range.Value2 = "بهاي هر مگاوات آمادگي بهره بردار";
                Excel.Range range20b = sheet.get_Range(sheet.Cells[6, 28], sheet.Cells[6, 26]);
                range20b.Merge(true);
                range20b.Font.Bold = true;
                range20b.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range20b.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range20b.Font.Size = 11;
                range20b.RowHeight = 35;
                range20b.ColumnWidth = 5;
                range20b.Borders.LineStyle = Excel.Constants.xlSolid;
                range20b.Borders.Weight = 2;
                range20b.WrapText = true;

                

                range = (Excel.Range)sheet.Cells[7, 26];
                range.Value2 = "ريال";
                Excel.Range range20bs = sheet.get_Range(sheet.Cells[7, 28], sheet.Cells[7, 26]);
                range20bs.Merge(true);
                range20bs.Font.Bold = true;
                range20bs.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range20bs.Font.Size = 11;
                range20bs.RowHeight = 18;
                range20bs.ColumnWidth = 5;
                range20bs.Borders.LineStyle = Excel.Constants.xlSolid;
                range20bs.Borders.Weight = 2;
                range.WrapText = true;

                range = (Excel.Range)sheet.Cells[7, 29];
                range.Value2 = "ريال";
                Excel.Range range20be = sheet.get_Range(sheet.Cells[7, 31], sheet.Cells[7, 29]);
                range20be.Merge(true);
                range20be.Font.Bold = true;
                range20be.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range20be.Font.Size = 11;
                range20be.RowHeight = 18;
                range20be.ColumnWidth = 5;
                range20be.Borders.LineStyle = Excel.Constants.xlSolid;
                range20be.Borders.Weight = 2;
                range.WrapText = true;






                range = (Excel.Range)sheet.Cells[8, 26];
                range.Value2 = "9";
                Excel.Range range124b = sheet.get_Range(sheet.Cells[8, 28], sheet.Cells[8, 26]);
                range124b.Merge(true);
                range124b.Font.Bold = true;
                range124b.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range124b.Font.Size = 11;
                range124b.RowHeight = 18;
                range124b.ColumnWidth = 5;
                range124b.Borders.LineStyle = Excel.Constants.xlSolid;
                range124b.Borders.Weight = 2;





                range = (Excel.Range)sheet.Cells[9, 26];
                range.Value2 = txtamadegi.Text;
                Excel.Range range305b = sheet.get_Range(sheet.Cells[9, 28], sheet.Cells[9, 26]);
                range305b.Merge(true);
                range305b.Font.Bold = true;
                range305b.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range305b.Font.Size = 11;
                range305b.RowHeight = 18;
                range305b.ColumnWidth = 5;
                range305b.Borders.LineStyle = Excel.Constants.xlSolid;
                range305b.Borders.Weight = 2;
                double x71b = x51 * MyDoubleParse(txtamadegi.Text);



                range = (Excel.Range)sheet.Cells[10, 26];
                range.Value2 = txtamadegi.Text;
                Excel.Range range308b = sheet.get_Range(sheet.Cells[10, 28], sheet.Cells[10, 26]);
                range308b.Merge(true);
                range308b.Font.Bold = true;
                range308b.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range308b.Font.Size = 11;
                range308b.RowHeight = 18;
                range308b.ColumnWidth = 5;
                range308b.Borders.LineStyle = Excel.Constants.xlSolid;
                range308b.Borders.Weight = 2;
                double x72b = x52 * MyDoubleParse(txtamadegi.Text);



                range = (Excel.Range)sheet.Cells[11, 26];
                range.Value2 = txtamadegi.Text;
                Excel.Range range306b = sheet.get_Range(sheet.Cells[11, 28], sheet.Cells[11, 26]);
                range306b.Merge(true);
                range306b.Font.Bold = true;
                range306b.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range306b.Font.Size = 11;
                range306b.RowHeight = 18;
                range306b.ColumnWidth = 5;
                range306b.Borders.LineStyle = Excel.Constants.xlSolid;
                range306b.Borders.Weight = 2;
                double x73b = x53 * MyDoubleParse(txtamadegi.Text);



                range = (Excel.Range)sheet.Cells[12, 26];
                range.Value2 = "";
                Excel.Range range307b = sheet.get_Range(sheet.Cells[12, 28], sheet.Cells[12, 26]);
                range307b.Merge(true);
                range307b.Font.Bold = true;
                range307b.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range307b.Font.Size = 11;
                range307b.RowHeight = 18;
                range307b.ColumnWidth = 5;
                range307b.Borders.LineStyle = Excel.Constants.xlSolid;
                range307b.Borders.Weight = 2;



                //////////////////////////////
                range = (Excel.Range)sheet.Cells[6, 29];
                range.Value2 = "درآمد آمادگي";
                Excel.Range range20c = sheet.get_Range(sheet.Cells[6, 31], sheet.Cells[6, 29]);
                range20c.Merge(true);
                range20c.Font.Bold = true;
                range20c.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                 range20c.VerticalAlignment= Excel.XlHAlign.xlHAlignCenter;
                range20c.Font.Size = 11;
                range20c.RowHeight = 35;
                range20c.ColumnWidth = 5;
                range20c.Borders.LineStyle = Excel.Constants.xlSolid;
                range20c.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[8, 29];
                range.Value2 = "10=8*9";
                Excel.Range range124c = sheet.get_Range(sheet.Cells[8, 31], sheet.Cells[8, 29]);
                range124c.Merge(true);
                range124c.Font.Bold = true;
                range124c.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range124c.Font.Size = 11;
                range124c.RowHeight = 18;
                range124c.ColumnWidth = 5;
                range124c.Borders.LineStyle = Excel.Constants.xlSolid;
                range124c.Borders.Weight = 2;





                range = (Excel.Range)sheet.Cells[9, 29];
                range.Value2 =(x71a*(MyDoubleParse (txtamadegi.Text))).ToString();
                Excel.Range range305c = sheet.get_Range(sheet.Cells[9, 31], sheet.Cells[9, 29]);
                range305c.Merge(true);
                range305c.Font.Bold = true;
                range305c.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range305c.Font.Size = 11;
                range305c.RowHeight = 18;
                range305c.ColumnWidth = 5;
                range305c.Borders.LineStyle = Excel.Constants.xlSolid;
                range305c.Borders.Weight = 2;
                double x71c = (x71a * (MyDoubleParse(txtamadegi.Text)));



                range = (Excel.Range)sheet.Cells[10, 29];
                range.Value2 = (x72a * (MyDoubleParse(txtamadegi.Text))).ToString(); ;
                Excel.Range range308c = sheet.get_Range(sheet.Cells[10, 31], sheet.Cells[10, 29]);
                range308c.Merge(true);
                range308c.Font.Bold = true;
                range308c.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range308c.Font.Size = 11;
                range308c.RowHeight = 18;
                range308c.ColumnWidth = 5;
                range308c.Borders.LineStyle = Excel.Constants.xlSolid;
                range308c.Borders.Weight = 2;
                double x72c = (x72a * (MyDoubleParse(txtamadegi.Text))) ;



                range = (Excel.Range)sheet.Cells[11, 29];
                range.Value2 = (x73a * (MyDoubleParse(txtamadegi.Text))).ToString(); ;
                Excel.Range range306c = sheet.get_Range(sheet.Cells[11, 31], sheet.Cells[11,29]);
                range306c.Merge(true);
                range306c.Font.Bold = true;
                range306c.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range306c.Font.Size = 11;
                range306c.RowHeight = 18;
                range306c.ColumnWidth = 5;
                range306c.Borders.LineStyle = Excel.Constants.xlSolid;
                range306c.Borders.Weight = 2;
                double x73c = (x73a * (MyDoubleParse(txtamadegi.Text)));


                range = (Excel.Range)sheet.Cells[12, 29];
                range.Value2 = (x71c+x72c+x73c).ToString();
                Excel.Range range307c = sheet.get_Range(sheet.Cells[12, 31], sheet.Cells[12, 29]);
                range307c.Merge(true);
                range307c.Font.Bold = true;
                range307c.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range307c.Font.Size = 11;
                range307c.RowHeight = 18;
                range307c.ColumnWidth = 5;
                range307c.Borders.LineStyle = Excel.Constants.xlSolid;
                range307c.Borders.Weight = 2;


         
                //////////////////////////////////////tolid //////////////////////////////


                range = (Excel.Range)sheet.Cells[13, 1];
                range.Value2 = faDatePicker1.Text.Trim() + " از تاريخ ";
                Excel.Range range12s = sheet.get_Range(sheet.Cells[13, 5], sheet.Cells[13, 1]);
                range12s.Merge(true);
                range12s.Font.Bold = true;
                range12s.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range12s.Font.Size = 11;
                range12s.RowHeight = 18;
                range12s.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[13, 6];
                range.Value2 = faDatePicke2.Text.Trim() + "  تا تاريخ  ";
                Excel.Range range14s = sheet.get_Range(sheet.Cells[13, 10], sheet.Cells[13, 6]);
                range14s.Merge(true);
                range14s.Font.Bold = true;
                range14s.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range14s.Font.Size = 11;
                range14s.RowHeight = 18;
                range14s.ColumnWidth = 5;
                range14s.Borders.LineStyle = Excel.Constants.xlSolid;
                range14s.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[13, 11];
                range.Value2 = "درآمد توليد برق";
                Excel.Range range13s = sheet.get_Range(sheet.Cells[13, 31], sheet.Cells[13, 11]);
                range13s.Merge(true);
                range13s.Font.Bold = true;
                range13s.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;             
                range13s.Font.Size = 11;
                range13s.RowHeight = 18;
                range13s.ColumnWidth = 5;
                range13s.Borders.LineStyle = Excel.Constants.xlSolid;
                range13s.Borders.Weight = 2;
                range13s.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(255, 255, 0));



                range = (Excel.Range)sheet.Cells[14, 1];
                range.Value2 = "شرح";
                Excel.Range range16d = sheet.get_Range(sheet.Cells[14, 5], sheet.Cells[14, 1]);
                range16d.Merge(true);
                range16d.Font.Bold = true;
                range16d.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                 range16d.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range16d.Font.Size = 11;
                range16d.RowHeight = 35;
                range16d.ColumnWidth = 5;
                range16d.Borders.LineStyle = Excel.Constants.xlSolid;
                range16d.Borders.Weight = 2;
                //////////////////////////////////////////////////////////////////

                range = (Excel.Range)sheet.Cells[15, 1];
                range.Value2 = "";
                Excel.Range range161s = sheet.get_Range(sheet.Cells[15, 5], sheet.Cells[15, 1]);
                range161s.Merge(true);
                range161s.Font.Bold = true;
                range161s.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range161s.Font.Size = 11;
                range161s.RowHeight = 18;
                range161s.ColumnWidth = 5;
                range161s.Borders.LineStyle = Excel.Constants.xlSolid;
                range161s.Borders.Weight = 2;


                

                range = (Excel.Range)sheet.Cells[16, 1];
                range.Value2 = "";
                Excel.Range range161j = sheet.get_Range(sheet.Cells[16, 5], sheet.Cells[16, 1]);
                range161j.Merge(true);
                range161j.Font.Bold = true;
                range161j.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range161j.Font.Size = 11;
                range161j.RowHeight = 18;
                range161j.ColumnWidth = 5;
                range161j.Borders.LineStyle = Excel.Constants.xlSolid;
                range161j.Borders.Weight = 2;


                


                range = (Excel.Range)sheet.Cells[17, 1];
                range.Value2 = "ساعت پبك - 4ساعت";
                Excel.Range range161h = sheet.get_Range(sheet.Cells[17, 5], sheet.Cells[17, 1]);
                range161h.Merge(true);
                range161h.Font.Bold = true;
                range161h.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range161h.Font.Size = 11;
                range161h.RowHeight = 18;
                range161h.ColumnWidth = 5;
                range161h.Borders.LineStyle = Excel.Constants.xlSolid;
                range161h.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[18, 1];
                range.Value2 = "ساعت ميان باري - 12ساعت";
                Excel.Range range162r = sheet.get_Range(sheet.Cells[18, 5], sheet.Cells[18, 1]);
                range162r.Merge(true);
                range162r.Font.Bold = true;
                range162r.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range162r.Font.Size = 11;
                range162r.RowHeight = 18;
                range162r.ColumnWidth = 5;
                range162r.Borders.LineStyle = Excel.Constants.xlSolid;
                range162r.Borders.Weight = 2;

                range = (Excel.Range)sheet.Cells[19, 1];
                range.Value2 = "ساعت كم باري - 8ساعت";
                Excel.Range range163w = sheet.get_Range(sheet.Cells[19, 5], sheet.Cells[19, 1]);
                range163w.Merge(true);
                range163w.Font.Bold = true;
                range163w.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range163w.Font.Size = 11;
                range163w.RowHeight = 18;
                range163w.ColumnWidth = 5;
                range163w.Borders.LineStyle = Excel.Constants.xlSolid;
                range163w.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[20, 1];
                range.Value2 = "جمع";
                Excel.Range range164d = sheet.get_Range(sheet.Cells[20, 5], sheet.Cells[20, 1]);
                range164d.Merge(true);
                range164d.Font.Bold = true;
                range164d.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range164d.Font.Size = 11;
                range164d.RowHeight = 18;
                range164d.ColumnWidth = 5;
                range164d.Borders.LineStyle = Excel.Constants.xlSolid;
                range164d.Borders.Weight = 2;





                range = (Excel.Range)sheet.Cells[14, 6];
                range.Value2 = "توليد";
                Excel.Range range151p = sheet.get_Range(sheet.Cells[14, 9], sheet.Cells[14, 6]);
                range151p.Merge(true);
                range151p.Font.Bold = true;
                range151p.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range151p.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range151p.Font.Size = 11;
                range151p.RowHeight = 35;
                range151p.ColumnWidth = 5;
                range151p.Borders.LineStyle = Excel.Constants.xlSolid;
                range151p.Borders.Weight = 2;
                range.WrapText = true;



                range = (Excel.Range)sheet.Cells[15, 6];
                range.Value2 = "مقدار-MWH";
                Excel.Range range152i = sheet.get_Range(sheet.Cells[15, 9], sheet.Cells[15, 6]);
                range152i.Merge(true);
                range152i.Font.Bold = true;
                range152i.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range152i.Font.Size = 11;
                range152i.RowHeight = 18;
                range152i.ColumnWidth = 5;
                range152i.Borders.LineStyle = Excel.Constants.xlSolid;
                range152i.Borders.Weight = 2;
                

                range = (Excel.Range)sheet.Cells[16, 6];
                range.Value2 = "1";
                Excel.Range range159v = sheet.get_Range(sheet.Cells[16, 9], sheet.Cells[16, 6]);
                range159v.Merge(true);
                range159v.Font.Bold = true;
                range159v.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159v.Font.Size = 11;
                range159v.RowHeight = 18;
                range159v.ColumnWidth = 5;
                range159v.Borders.LineStyle = Excel.Constants.xlSolid;
                range159v.Borders.Weight = 2;




               


                range = (Excel.Range)sheet.Cells[14, 10];
                range.Value2 = "بهاي هر مگاوات توليد بهره بردار";
                Excel.Range range151o = sheet.get_Range(sheet.Cells[14, 13], sheet.Cells[14, 10]);
                range151o.Merge(true);
                range151o.Font.Bold = true;
                range151o.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                 range151o.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range151o.Font.Size = 11;
                range151o.RowHeight = 35;
                range151o.ColumnWidth = 5;
                range151o.Borders.LineStyle = Excel.Constants.xlSolid;
                range151o.Borders.Weight = 2;
                range.WrapText = true;



                range = (Excel.Range)sheet.Cells[15, 10];
                range.Value2 = "ريال";
                Excel.Range range152o = sheet.get_Range(sheet.Cells[15, 13], sheet.Cells[15, 10]);
                range152o.Merge(true);
                range152o.Font.Bold = true;
                range152o.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range152o.Font.Size = 11;
                range152o.RowHeight = 18;
                range152o.ColumnWidth = 5;
                range152o.Borders.LineStyle = Excel.Constants.xlSolid;
                range152o.Borders.Weight = 2;

                

                range = (Excel.Range)sheet.Cells[16, 10];
                range.Value2 = "2";
                Excel.Range range159o = sheet.get_Range(sheet.Cells[16, 13], sheet.Cells[16, 10]);
                range159o.Merge(true);
                range159o.Font.Bold = true;
                range159o.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159o.Font.Size = 11;
                range159o.RowHeight = 18;
                range159o.ColumnWidth = 5;
                range159o.Borders.LineStyle = Excel.Constants.xlSolid;
                range159o.Borders.Weight = 2;






               



                range = (Excel.Range)sheet.Cells[14, 14];
                range.Value2 = " درآمد توليد";
                Excel.Range range151v = sheet.get_Range(sheet.Cells[14, 17], sheet.Cells[14, 14]);
                range151v.Merge(true);
                range151v.Font.Bold = true;
                range151v.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                 range151v.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range151v.Font.Size = 11;
                range151v.RowHeight = 35;
                range151v.ColumnWidth = 5;
                range151v.Borders.LineStyle = Excel.Constants.xlSolid;
                range151v.Borders.Weight = 2;
                range.WrapText = true;



                range = (Excel.Range)sheet.Cells[15, 14];
                range.Value2 = "ريال";
                Excel.Range range152v = sheet.get_Range(sheet.Cells[15, 17], sheet.Cells[15, 14]);
                range152v.Merge(true);
                range152v.Font.Bold = true;
                range152v.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range152v.Font.Size = 11;
                range152v.RowHeight = 18;
                range152v.ColumnWidth = 5;
                range152v.Borders.LineStyle = Excel.Constants.xlSolid;
                range152v.Borders.Weight = 2;

                

                range = (Excel.Range)sheet.Cells[16, 14];
                range.Value2 = "3=1*2";
                Excel.Range range159x = sheet.get_Range(sheet.Cells[16, 17], sheet.Cells[16, 14]);
                range159x.Merge(true);
                range159x.Font.Bold = true;
                range159x.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159x.Font.Size = 11;
                range159x.RowHeight = 18;
                range159x.ColumnWidth = 5;
                range159x.Borders.LineStyle = Excel.Constants.xlSolid;
                range159x.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[17, 6];
                range.Value2 = (tolidholidaypeak+tolidpeak).ToString();
                Excel.Range range159i = sheet.get_Range(sheet.Cells[17, 9], sheet.Cells[17, 6]);
                range159i.Merge(true);
                range159i.Font.Bold = true;
                range159i.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159i.Font.Size = 11;
                range159i.RowHeight = 18;
                range159i.ColumnWidth = 5;
                range159i.Borders.LineStyle = Excel.Constants.xlSolid;
                range159i.Borders.Weight = 2;
                double xc1 = (tolidholidaypeak + tolidpeak);

                range = (Excel.Range)sheet.Cells[18, 6];
                range.Value2 = (tolidmedium + tolidholidaymedium).ToString();
                Excel.Range range159t = sheet.get_Range(sheet.Cells[18, 9], sheet.Cells[18, 6]);
                range159t.Merge(true);
                range159t.Font.Bold = true;
                range159t.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159t.Font.Size = 11;
                range159t.RowHeight = 18;
                range159t.ColumnWidth = 5;
                range159t.Borders.LineStyle = Excel.Constants.xlSolid;
                range159t.Borders.Weight = 2;
                double xc2 = (tolidmedium + tolidholidaymedium);

                range = (Excel.Range)sheet.Cells[19, 6];
                range.Value2 = (tolidbasse+tolidholidaybasse).ToString();
                Excel.Range range159w = sheet.get_Range(sheet.Cells[19, 9], sheet.Cells[19, 6]);
                range159w.Merge(true);
                range159w.Font.Bold = true;
                range159w.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159w.Font.Size = 11;
                range159w.RowHeight = 18;
                range159w.ColumnWidth = 5;
                range159w.Borders.LineStyle = Excel.Constants.xlSolid;
                range159w.Borders.Weight = 2;
                double xc3 = (tolidbasse + tolidholidaybasse);


                range = (Excel.Range)sheet.Cells[20, 6];
                range.Value2 = (xc1+xc2+xc3).ToString();
                Excel.Range range159q = sheet.get_Range(sheet.Cells[20, 9], sheet.Cells[20, 6]);
                range159q.Merge(true);
                range159q.Font.Bold = true;
                range159q.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159q.Font.Size = 11;
                range159q.RowHeight = 18;
                range159q.ColumnWidth = 5;
                range159q.Borders.LineStyle = Excel.Constants.xlSolid;
                range159q.Borders.Weight = 2;




                range = (Excel.Range)sheet.Cells[17, 10];
                range.Value2 =txtpardakht.Text;
                Excel.Range range159l = sheet.get_Range(sheet.Cells[17, 13], sheet.Cells[17, 10]);
                range159l.Merge(true);
                range159l.Font.Bold = true;
                range159l.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159l.Font.Size = 11;
                range159l.RowHeight = 18;
                range159l.ColumnWidth = 5;
                range159l.Borders.LineStyle = Excel.Constants.xlSolid;
                range159l.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[18, 10];
                range.Value2 = txtpardakht.Text;
                Excel.Range range159s = sheet.get_Range(sheet.Cells[18, 13], sheet.Cells[18, 10]);
                range159s.Merge(true);
                range159s.Font.Bold = true;
                range159s.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159s.Font.Size = 11;
                range159s.RowHeight = 18;
                range159s.ColumnWidth = 5;
                range159s.Borders.LineStyle = Excel.Constants.xlSolid;
                range159s.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[19, 10];
                range.Value2 = txtpardakht.Text;
                Excel.Range range159k = sheet.get_Range(sheet.Cells[19, 13], sheet.Cells[19, 10]);
                range159k.Merge(true);
                range159k.Font.Bold = true;
                range159k.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159k.Font.Size = 11;
                range159k.RowHeight = 18;
                range159k.ColumnWidth = 5;
                range159k.Borders.LineStyle = Excel.Constants.xlSolid;
                range159k.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[17, 14];
                range.Value2 = (xc1*(MyDoubleParse(txtpardakht.Text))).ToString();
                Excel.Range range159b = sheet.get_Range(sheet.Cells[17, 17], sheet.Cells[17, 14]);
                range159b.Merge(true);
                range159b.Font.Bold = true;
                range159b.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159b.Font.Size = 11;
                range159b.RowHeight = 18;
                range159b.ColumnWidth = 5;
                range159b.Borders.LineStyle = Excel.Constants.xlSolid;
                range159b.Borders.Weight = 2;
                double xv1 = xc1 * (MyDoubleParse(txtpardakht.Text));


                range = (Excel.Range)sheet.Cells[18, 14];
                range.Value2 = (xc2 * (MyDoubleParse(txtpardakht.Text))).ToString();
                Excel.Range range159c = sheet.get_Range(sheet.Cells[18, 17], sheet.Cells[18, 14]);
                range159c.Merge(true);
                range159c.Font.Bold = true;
                range159c.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159c.Font.Size = 11;
                range159c.RowHeight = 18;
                range159c.ColumnWidth = 5;
                range159c.Borders.LineStyle = Excel.Constants.xlSolid;
                range159c.Borders.Weight = 2;
                double xv2 = xc2 * (MyDoubleParse(txtpardakht.Text));

                range = (Excel.Range)sheet.Cells[19, 14];
                range.Value2 = (xc3 * (MyDoubleParse(txtpardakht.Text))).ToString();
                Excel.Range range159a = sheet.get_Range(sheet.Cells[19, 17], sheet.Cells[19, 14]);
                range159a.Merge(true);
                range159a.Font.Bold = true;
                range159a.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159a.Font.Size = 11;
                range159a.RowHeight = 18;
                range159a.ColumnWidth = 5;
                range159a.Borders.LineStyle = Excel.Constants.xlSolid;
                range159a.Borders.Weight = 2;
                double xv3 = xc3 * (MyDoubleParse(txtpardakht.Text));


                range = (Excel.Range)sheet.Cells[20 ,14];
                range.Value2 = (xv1+xv2+xv3).ToString();
                Excel.Range range159n = sheet.get_Range(sheet.Cells[20, 17], sheet.Cells[20, 14]);
                range159n.Merge(true);
                range159n.Font.Bold = true;
                range159n.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159n.Font.Size = 11;
                range159n.RowHeight = 18;
                range159n.ColumnWidth = 5;
                range159n.Borders.LineStyle = Excel.Constants.xlSolid;
                range159n.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[20, 10];
                range.Value2 = "";
                Excel.Range range159m = sheet.get_Range(sheet.Cells[20, 13], sheet.Cells[20, 10]);
                range159m.Merge(true);
                range159m.Font.Bold = true;
                range159m.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range159m.Font.Size = 11;
                range159m.RowHeight = 18;
                range159m.ColumnWidth = 5;
                range159m.Borders.LineStyle = Excel.Constants.xlSolid;
                range159m.Borders.Weight = 2;

              ///////////////////////////////////////////////////////////////////////////////////////////

                range = (Excel.Range)sheet.Cells[1, 1];
                range = range.EntireColumn;
                range.AutoFit();

                range = (Excel.Range)sheet.Cells[1, 2];
                range = range.EntireColumn;
                range.AutoFit();
                sheet.Activate();


                string strDate = faDatePicker1.Text;
                strDate = strDate.Remove(7, 1);
                strDate = strDate.Remove(4, 1);
                string filePath = folderBrowserDialog1.SelectedPath + "\\" + "درآمد " + strDate + ".xls";



                //oExcel.Visible = true;
                WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                WB.Close(Missing.Value, Missing.Value, Missing.Value);
                oExcel.Workbooks.Close();
                oExcel.Quit();
                MessageBox.Show("export completed.");
            }
            else
            {

                MessageBox.Show("Monthly Bill Data InCompleted..!!!");
            }


        }
    }
}
