﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
//using Microsoft.Office.Core;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using Auto_Update_Library;
using System.Collections;
using System.IO;

namespace PowerPlantProject
{
    public partial class Opf_Result : Form
    {
        public Opf_Result()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;
                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
             

                /////////////////////////////////////grid//////////////////////////////////////////


                dataGridView1.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                dataGridView1.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;

                dataGridView2.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                dataGridView2.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;

                dataGridView3.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                dataGridView3.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;

                dataGridView4.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                dataGridView4.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;

                dataGridView5.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                dataGridView5.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;

                dataGridView6.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                dataGridView6.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;

                ///////////////////////////////////////////////////////////////////

            }
            catch
            {

            }
        }

        private void Opf_Result_Load(object sender, EventArgs e)
        {
            DataTable dlabel = Utilities.GetTable("select BiddingDate from dbo.BiddingStrategySetting where id=(select max(id) from BiddingStrategySetting)");
            if (dlabel.Rows.Count > 0)
            {
                labeldate.Text = dlabel.Rows[0][0].ToString().Trim();
            }

            buildTreeView1();
          
           
        }
        private void buildTreeView1()
        {
            string GenCo = "";
            DataTable dt = Utilities.GetTable("select * from BaseGencoInfo");
            if (dt.Rows.Count > 0)
            {
                GenCo = dt.Rows[0]["GencoNameEnglish"].ToString().Substring(0, 1).Trim().ToUpper() + "rec";  
             
            }
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            treeView1.Nodes.Clear();
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();

            //Insert Trec
            TreeNode MyNode = new TreeNode();
            MyNode.Text = GenCo;
            treeView1.Nodes.Add(MyNode);

            ////Insert Plants
            //Myda.SelectCommand = new SqlCommand("SELECT PPName FROM PowerPlant order by ppid", myConnection);
            //Myda.Fill(MyDS, "plantname");
            //TreeNode PlantNode = new TreeNode();
            //PlantNode.Text = "Plants";
            //MyNode.Nodes.Add(PlantNode);
            //foreach (DataRow MyRow in MyDS.Tables["plantname"].Rows)
            //{
            //    TreeNode ChildNode = new TreeNode();
            //    ChildNode.Text = MyRow["PPName"].ToString().Trim();
            //    PlantNode.Nodes.Add(ChildNode);
            //}

            //Insert Transmission
            Myda.SelectCommand = new SqlCommand("SELECT DISTINCT LineCode,LineNumber,FromBus,ToBus FROM Lines", myConnection);
            Myda.Fill(MyDS, "transtype");
            myConnection.Close();

            TreeNode TransNode = new TreeNode();
            TransNode.Text = "Lines";
            MyNode.Nodes.Add(TransNode);
            foreach (DataRow MyRow in MyDS.Tables["transtype"].Rows)
            {
                try
                {
                    TreeNode ChildNode = new TreeNode();
                    DataTable dtt = Utilities.GetTable("select VoltageLevel from dbo.PostVoltage where VoltageNumber='" + MyRow["FromBus"].ToString().Trim() + "'");
                    DataTable dxx = Utilities.GetTable("select VoltageLevel from dbo.PostVoltage where VoltageNumber='" + MyRow["ToBus"].ToString().Trim() + "'");
                    ChildNode.Text = dtt.Rows[0][0].ToString().Trim() + "-->" + dxx.Rows[0][0].ToString().Trim();
                    ChildNode.ToolTipText= MyRow["LineCode"].ToString().Trim();
                    TransNode.Nodes.Add(ChildNode);
                }
                catch
                {


                }
            }

            //DataTable dt = utilities.returntbl("select distinct VoltageNumber,VoltageLevel,PostNumber from dbo.PostVoltage");
            //TreeNode postNode = new TreeNode();
            //postNode.Text = "Posts";
            //MyNode.Nodes.Add(postNode);
            //foreach (DataRow MRow in dt.Rows)
            //{
            //    try
            //    {
            //        TreeNode ChildNode = new TreeNode();
            //        ChildNode.Text = MRow["VoltageLevel"].ToString().Trim();
            //        ChildNode.ToolTipText = MRow["PostNumber"].ToString().Trim();
            //        postNode.Nodes.Add(ChildNode);
            //    }
            //    catch
            //    {


            //    }
            //}


           
          // treeView1.ExpandAll();

            
            
        }

        //private void click(string name)
        //{

        //    if (name == "Gilan")
        //    {

        //        panelgilan.BackColor = Color.Yellow;
        //        panelloshan.BackColor = Color.Teal;
               
        //    }
        //    if (name == "Lowshan")
        //    {

        //        panelgilan.BackColor = Color.Teal;
        //        panelloshan.BackColor = Color.Yellow;

        //    }
           
        //}
        //private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        //{
          
        //    panelinfo.Visible = false;

        //    if (e.Node.Parent != null) //be sure the root isnot selected!
        //    {
        //        if (e.Node.Parent.Text == "Plants")
        //        {

        //            click(e.Node.Text.Trim());
        //            panelinfo.Visible = true;
        //            fillgridplant(e.Node.Text.Trim());

        //        }
        //        else if (e.Node.Parent.Text == "Lines")
        //        {
        //            panelinfo.Visible = true;
        //            fillgridline(e.Node.ToolTipText.Trim() + "  - (" + e.Node.Text.Trim()+ ")" );
                    
        //        }
        //    }
        //}

        //private void fillgridline(string name)
        //{
        //    string Fdate = "";
        //    label2.Text = name;
        //    string[] s = name.Split('-');
        //    string mline = s[0].Trim();

        //    dataGridView1.DataSource = null;
        //    dataGridView1.Rows.Clear();
        //    dataGridView2.DataSource = null;
        //    dataGridView2.Rows.Clear();
        //    dataGridView3.DataSource = null;
        //    dataGridView3.Rows.Clear();
        //    dataGridView4.DataSource = null;
        //    dataGridView4.Rows.Clear();
        //    dataGridView5.DataSource = null;
        //    dataGridView5.Rows.Clear();
        //    dataGridView6.DataSource = null;
        //    dataGridView6.Rows.Clear();

        //    dataGridView1.RowCount = 1;
        //    dataGridView1.ColumnCount = 4;

        //    dataGridView2.RowCount = 1;
        //    dataGridView2.ColumnCount = 4;

        //    dataGridView3.RowCount = 1;
        //    dataGridView3.ColumnCount = 4;

        //    dataGridView4.RowCount = 1;
        //    dataGridView4.ColumnCount = 4;

        //    dataGridView5.RowCount = 1;
        //    dataGridView5.ColumnCount = 4;

        //    dataGridView6.RowCount = 1;
        //    dataGridView6.ColumnCount = 4;

        //    DataTable dt21 = utilities.GetTable("select * from dbo.LineFlow where LineCode='" + mline.Trim() + "' and Date=(select BiddingDate from dbo.BiddingStrategySetting where id=(select max(id) from BiddingStrategySetting))");
        //    //DataTable dt22 = utilities.GetTable("select * from dbo.LineFlow where LineCode='" + mline.Trim() + "' and Date=(select max(Date) from dbo.LineFlow)");
        //    if (dt21.Rows.Count > 0)
        //    {
        //        Fdate = dt21.Rows[0][0].ToString();
        //    }
        //    //else if (dt22.Rows.Count > 0)
        //    //{
        //    //    Fdate = dt22.Rows[0][0].ToString();
        //    //}


        //    bool allzero = false;
        //    DataTable dt = null;

        //    if (Fdate != "")
        //    {


        //        dt = utilities.GetTable("select * from dbo.LineFlow where LineCode='" + mline.Trim() + "' and Date='" + Fdate + "'order by Hour ASC");

        //        int count = 0;
        //        foreach (DataRow n in dt.Rows)
        //        {
        //            if (n[3].ToString() == "0") count++;
        //        }
        //        if (count == 24)
        //            allzero = true;
        //    }




        //    // DataTable dt = utilities.GetTable("select * from dbo.LineFlow where LineCode='" + mline.Trim() + "' and Date='"+Fdate+"'order by Hour ASC");
        //    if (!allzero && Fdate != "")
        //    {
        //        if (dt.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < 4; i++)
        //            {
        //                dataGridView1.Rows[0].Cells[i].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 4; i < 8; i++)
        //            {
        //                dataGridView2.Rows[0].Cells[i - 4].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 8; i < 12; i++)
        //            {
        //                dataGridView3.Rows[0].Cells[i - 8].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 12; i < 16; i++)
        //            {
        //                dataGridView4.Rows[0].Cells[i - 12].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 16; i < 20; i++)
        //            {
        //                dataGridView5.Rows[0].Cells[i - 16].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 20; i < 24; i++)
        //            {
        //                dataGridView6.Rows[0].Cells[i - 20].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //        }
        //    }
        //    else
        //    {
        //        DataTable dt22 = utilities.GetTable("select * from dbo.InterchangedEnergy where Code='" + mline.Trim() + "' and Date=(select max(Date) from dbo.InterchangedEnergy where Code='" + mline.Trim() + "')");
        //        if (dt22.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < 4; i++)
        //            {
        //                dataGridView1.Rows[0].Cells[i].Value = Math.Abs( MyDoubleParse(dt22.Rows[0][i + 3].ToString()));
        //            }
        //            for (int i = 4; i < 8; i++)
        //            {
        //                dataGridView2.Rows[0].Cells[i - 4].Value = Math.Abs(MyDoubleParse(dt22.Rows[0][i + 3].ToString()));
        //            }
        //            for (int i = 8; i < 12; i++)
        //            {
        //                dataGridView3.Rows[0].Cells[i - 8].Value = Math.Abs(MyDoubleParse(dt22.Rows[0][i + 3].ToString()));
        //            }
        //            for (int i = 12; i < 16; i++)
        //            {
        //                dataGridView4.Rows[0].Cells[i - 12].Value = Math.Abs(MyDoubleParse(dt22.Rows[0][i + 3].ToString()));
        //            }
        //            for (int i = 16; i < 20; i++)
        //            {
        //                dataGridView5.Rows[0].Cells[i - 16].Value = Math.Abs(MyDoubleParse(dt22.Rows[0][i + 3].ToString()));
        //            }
        //            for (int i = 20; i < 24; i++)
        //            {
        //                dataGridView6.Rows[0].Cells[i - 20].Value = Math.Abs(MyDoubleParse(dt22.Rows[0][i + 3].ToString()));
        //            }
        //        }
        //    }
        //}
        //private void filldualplant(string name)
        //{
        //    string Fdate = "";
        //    label2.Text = name;
        //    dataGridView1.DataSource = null;
        //    dataGridView1.Rows.Clear();
        //    dataGridView2.DataSource = null;
        //    dataGridView2.Rows.Clear();
        //    dataGridView3.DataSource = null;
        //    dataGridView3.Rows.Clear();
        //    dataGridView4.DataSource = null;
        //    dataGridView4.Rows.Clear();
        //    dataGridView5.DataSource = null;
        //    dataGridView5.Rows.Clear();
        //    dataGridView6.DataSource = null;
        //    dataGridView6.Rows.Clear();

        //    dataGridView1.RowCount = 1;
        //    dataGridView1.ColumnCount = 4;

        //    dataGridView2.RowCount = 1;
        //    dataGridView2.ColumnCount = 4;

        //    dataGridView3.RowCount = 1;
        //    dataGridView3.ColumnCount = 4;

        //    dataGridView4.RowCount = 1;
        //    dataGridView4.ColumnCount = 4;

        //    dataGridView5.RowCount = 1;
        //    dataGridView5.ColumnCount = 4;

        //    dataGridView6.RowCount = 1;
        //    dataGridView6.ColumnCount = 4;

        //    DataTable dt21 = utilities.GetTable("select * from dbo.PowerOpf where Date=(select BiddingDate from dbo.BiddingStrategySetting where id=(select max(id) from BiddingStrategySetting))and  PPID='" + name + "'");

        //    //DataTable dt22 = utilities.GetTable("select * from dbo.PowerOpf where Date=(select max(Date) from dbo.PowerOpf) and PPID='" + name + "'");


        //    if (dt21.Rows.Count > 0)
        //    {
        //        Fdate = dt21.Rows[0][0].ToString().Trim();
        //    }
        //    //else if (dt22.Rows.Count > 0)
        //    //{
        //    //    Fdate = dt22.Rows[0][0].ToString().Trim();
        //    //}
        //    DataTable dt = null;
        //    bool allzero = false;
        //    if (Fdate != "")
        //    {

        //        dt = utilities.GetTable("select * from dbo.PowerOpf where Date='" + Fdate + "' and  PPID='" + name + "'order by Hour ASC");

        //        int count = 0;
        //        foreach (DataRow n in dt.Rows)
        //        {
        //            if (n[3].ToString() == "0") count++;
        //        }
        //        if (count == 24)
        //            allzero = true;
        //    }
        //    if (!allzero && Fdate != "")
        //    {
        //        if (dt.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < 4; i++)
        //            {
        //                dataGridView1.Rows[0].Cells[i].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 4; i < 8; i++)
        //            {
        //                dataGridView2.Rows[0].Cells[i - 4].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 8; i < 12; i++)
        //            {
        //                dataGridView3.Rows[0].Cells[i - 8].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 12; i < 16; i++)
        //            {
        //                dataGridView4.Rows[0].Cells[i - 12].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 16; i < 20; i++)
        //            {
        //                dataGridView5.Rows[0].Cells[i - 16].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 20; i < 24; i++)
        //            {
        //                dataGridView6.Rows[0].Cells[i - 20].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //        }
        //    }
        //    else
        //    {
        //        DataTable dt22 = utilities.GetTable("select sum(Hour1),sum(Hour2),sum(Hour3),sum(Hour4),sum(Hour5)," +
        //                        "sum(Hour6),sum(Hour7),sum(Hour8),sum(Hour9)," +
        //                        "sum(Hour10),sum(Hour11),sum(Hour12),sum(Hour13),sum(Hour14)," +
        //                        "sum(Hour15),sum(Hour16),sum(Hour17),sum(Hour18),sum(Hour19),sum(Hour20)" +
        //                        ",sum(Hour21),sum(Hour22),sum(Hour23),sum(Hour24)" +
        //                        "from dbo.ProducedEnergy where Date=(select max(Date) from ProducedEnergy where PPCode='" + name + "')and PPCode='" + name + "'");
        //        if (dt22.Rows.Count > 0 && dt22.Rows[0][0].ToString() != "")
        //        {
        //            for (int i = 0; i < 4; i++)
        //            {
        //                dataGridView1.Rows[0].Cells[i].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //            for (int i = 4; i < 8; i++)
        //            {
        //                dataGridView2.Rows[0].Cells[i - 4].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //            for (int i = 8; i < 12; i++)
        //            {
        //                dataGridView3.Rows[0].Cells[i - 8].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //            for (int i = 12; i < 16; i++)
        //            {
        //                dataGridView4.Rows[0].Cells[i - 12].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //            for (int i = 16; i < 20; i++)
        //            {
        //                dataGridView5.Rows[0].Cells[i - 16].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //            for (int i = 20; i < 24; i++)
        //            {
        //                dataGridView6.Rows[0].Cells[i - 20].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //        }
        //    }
        //}
        //private void fillgridplant(string name)
        //{
        //    string Fdate = "";
        //    label2.Text = name;
        //    dataGridView1.DataSource = null;
        //    dataGridView1.Rows.Clear();
        //    dataGridView2.DataSource = null;
        //    dataGridView2.Rows.Clear();
        //    dataGridView3.DataSource = null;
        //    dataGridView3.Rows.Clear();
        //    dataGridView4.DataSource = null;
        //    dataGridView4.Rows.Clear();
        //    dataGridView5.DataSource = null;
        //    dataGridView5.Rows.Clear();
        //    dataGridView6.DataSource = null;
        //    dataGridView6.Rows.Clear();

        //    dataGridView1.RowCount = 1;
        //    dataGridView1.ColumnCount = 4;

        //    dataGridView2.RowCount = 1;
        //    dataGridView2.ColumnCount = 4;

        //    dataGridView3.RowCount = 1;
        //    dataGridView3.ColumnCount = 4;

        //    dataGridView4.RowCount = 1;
        //    dataGridView4.ColumnCount = 4;

        //    dataGridView5.RowCount = 1;
        //    dataGridView5.ColumnCount = 4;

        //    dataGridView6.RowCount = 1;
        //    dataGridView6.ColumnCount = 4;

        //    DataTable dt21 = utilities.GetTable("select * from dbo.PowerOpf where Date=(select BiddingDate from dbo.BiddingStrategySetting where id=(select max(id) from BiddingStrategySetting))and  PPID=(select distinct PPID from dbo.PowerPlant where PPName='" + name + "')");

        //    //DataTable dt22 = utilities.GetTable("select max(Date) from dbo.PowerOpf where PPID=(select distinct PPID from dbo.PowerPlant where PPName='" + name + "') ");

        //    if (dt21.Rows.Count > 0)
        //    {
        //        Fdate = dt21.Rows[0][0].ToString();

        //    }
        //    //else if (dt22.Rows.Count > 0)
        //    //{
        //    //    Fdate = dt22.Rows[0][0].ToString();
        //    //}
        //    bool allzero = false;
        //    DataTable dt = null;

        //    if (Fdate != "")
        //    {


        //        dt = utilities.GetTable("select * from dbo.PowerOpf where Date='" + Fdate + "' and  PPID=(select distinct PPID from dbo.PowerPlant where PPName='" + name + "')order by Hour ASC");

        //        int count = 0;
        //        foreach (DataRow n in dt.Rows)
        //        {
        //            if (n[3].ToString() == "0") count++;
        //        }
        //        if (count == 24)
        //            allzero = true;
        //    }
        //    if (Fdate != "" && !allzero)
        //    {
        //        if (dt.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < 4; i++)
        //            {
        //                dataGridView1.Rows[0].Cells[i].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 4; i < 8; i++)
        //            {
        //                dataGridView2.Rows[0].Cells[i - 4].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 8; i < 12; i++)
        //            {
        //                dataGridView3.Rows[0].Cells[i - 8].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 12; i < 16; i++)
        //            {
        //                dataGridView4.Rows[0].Cells[i - 12].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 16; i < 20; i++)
        //            {
        //                dataGridView5.Rows[0].Cells[i - 16].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 20; i < 24; i++)
        //            {
        //                dataGridView6.Rows[0].Cells[i - 20].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //        }
        //    }
        //    else
        //    {
        //        DataTable dt22 = utilities.GetTable("select sum(Hour1),sum(Hour2),sum(Hour3),sum(Hour4),sum(Hour5)," +
        //                        "sum(Hour6),sum(Hour7),sum(Hour8),sum(Hour9)," +
        //                        "sum(Hour10),sum(Hour11),sum(Hour12),sum(Hour13),sum(Hour14)," +
        //                        "sum(Hour15),sum(Hour16),sum(Hour17),sum(Hour18),sum(Hour19),sum(Hour20)" +
        //                        ",sum(Hour21),sum(Hour22),sum(Hour23),sum(Hour24)" +
        //                        "from dbo.ProducedEnergy where Date=(select max(Date) from ProducedEnergy where PPCode=(select distinct PPID from dbo.PowerPlant where PPName='" + name + "'))and PPCode=(select distinct PPID from dbo.PowerPlant where PPName='" + name + "')");
        //        if (dt22.Rows.Count > 0 && dt22.Rows[0][0].ToString() != "")
        //        {
        //            for (int i = 0; i < 4; i++)
        //            {
        //                dataGridView1.Rows[0].Cells[i].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //            for (int i = 4; i < 8; i++)
        //            {
        //                dataGridView2.Rows[0].Cells[i - 4].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //            for (int i = 8; i < 12; i++)
        //            {
        //                dataGridView3.Rows[0].Cells[i - 8].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //            for (int i = 12; i < 16; i++)
        //            {
        //                dataGridView4.Rows[0].Cells[i - 12].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //            for (int i = 16; i < 20; i++)
        //            {
        //                dataGridView5.Rows[0].Cells[i - 16].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //            for (int i = 20; i < 24; i++)
        //            {
        //                dataGridView6.Rows[0].Cells[i - 20].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //        }
        //    }
        //}
        //private void fillgridloshan(string name)
        //{
        //    string Fdate = "";
        //    label2.Text = name;
        //    dataGridView1.DataSource = null;
        //    dataGridView1.Rows.Clear();
        //    dataGridView2.DataSource = null;
        //    dataGridView2.Rows.Clear();
        //    dataGridView3.DataSource = null;
        //    dataGridView3.Rows.Clear();
        //    dataGridView4.DataSource = null;
        //    dataGridView4.Rows.Clear();
        //    dataGridView5.DataSource = null;
        //    dataGridView5.Rows.Clear();
        //    dataGridView6.DataSource = null;
        //    dataGridView6.Rows.Clear();

        //    dataGridView1.RowCount = 1;
        //    dataGridView1.ColumnCount = 4;

        //    dataGridView2.RowCount = 1;
        //    dataGridView2.ColumnCount = 4;

        //    dataGridView3.RowCount = 1;
        //    dataGridView3.ColumnCount = 4;

        //    dataGridView4.RowCount = 1;
        //    dataGridView4.ColumnCount = 4;

        //    dataGridView5.RowCount = 1;
        //    dataGridView5.ColumnCount = 4;

        //    dataGridView6.RowCount = 1;
        //    dataGridView6.ColumnCount = 4;
           
        //        string PPID="";
        //        string PART = "";
        //        if (name == "Loshan-Gas")
        //        {
        //            PPID = "206-G";
        //            PART = "G";
        //        }
        //        else
        //        {
        //            PPID = "206-S";
        //            PART = "S";
        //        }

        //    DataTable dt21 = utilities.GetTable("select * from dbo.PowerOpf where Date=(select BiddingDate from dbo.BiddingStrategySetting where id=(select max(id) from BiddingStrategySetting))and  PPID='"+PPID+"'");

        //    //DataTable dt22 = utilities.GetTable("select max(Date) from dbo.PowerOpf where PPID=(select distinct PPID from dbo.PowerPlant where PPName='" + name + "') ");

        //    if (dt21.Rows.Count > 0)
        //    {
        //        Fdate = dt21.Rows[0][0].ToString();

        //    }
        //    //else if (dt22.Rows.Count > 0)
        //    //{
        //    //    Fdate = dt22.Rows[0][0].ToString();
        //    //}
        //    bool allzero = false;
        //    DataTable dt = null;

        //    if (Fdate != "")
        //    {

        //        dt = utilities.GetTable("select * from dbo.PowerOpf where Date='" + Fdate + "' and  PPID='"+PPID+"'order by Hour ASC");

        //        int count = 0;
        //        foreach (DataRow n in dt.Rows)
        //        {
        //            if (n[3].ToString() == "0") count++;
        //        }
        //        if (count == 24)
        //            allzero = true;
        //    }
        //    if (Fdate != "" && !allzero)
        //    {
        //        if (dt.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < 4; i++)
        //            {
        //                dataGridView1.Rows[0].Cells[i].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 4; i < 8; i++)
        //            {
        //                dataGridView2.Rows[0].Cells[i - 4].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 8; i < 12; i++)
        //            {
        //                dataGridView3.Rows[0].Cells[i - 8].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 12; i < 16; i++)
        //            {
        //                dataGridView4.Rows[0].Cells[i - 12].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 16; i < 20; i++)
        //            {
        //                dataGridView5.Rows[0].Cells[i - 16].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //            for (int i = 20; i < 24; i++)
        //            {
        //                dataGridView6.Rows[0].Cells[i - 20].Value = MyDoubleParse(dt.Rows[i][3].ToString());
        //            }
        //        }
        //    }
        //    else
        //    {
        //        DataTable dt22 = utilities.GetTable("select Hour1,Hour2,Hour3,Hour4,Hour5," +
        //                        "Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14," +
        //                        "Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24  from dbo.ProducedEnergy where Date=(select max(Date) from ProducedEnergy where PPCode='206')and PPCode='206' and Part='"+PART+"'");
        //        if (dt22.Rows.Count > 0 && dt22.Rows[0][0].ToString() != "")
        //        {
        //            for (int i = 0; i < 4; i++)
        //            {
        //                dataGridView1.Rows[0].Cells[i].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //            for (int i = 4; i < 8; i++)
        //            {
        //                dataGridView2.Rows[0].Cells[i - 4].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //            for (int i = 8; i < 12; i++)
        //            {
        //                dataGridView3.Rows[0].Cells[i - 8].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //            for (int i = 12; i < 16; i++)
        //            {
        //                dataGridView4.Rows[0].Cells[i - 12].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //            for (int i = 16; i < 20; i++)
        //            {
        //                dataGridView5.Rows[0].Cells[i - 16].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //            for (int i = 20; i < 24; i++)
        //            {
        //                dataGridView6.Rows[0].Cells[i - 20].Value = MyDoubleParse(dt22.Rows[0][i].ToString());
        //            }
        //        }
        //    }
        //}      
        //private void button1_Click(object sender, EventArgs e)
        //{
        //    panelinfo.Visible = false;
        //}
        //private double MyDoubleParse(string str)
        //{
        //    if (str.Trim() == "")
        //        return 0;
        //    else
        //    {
        //        try { return double.Parse(str.Trim()); }
        //        catch { return 0; }
        //    }
        //}
        //private void fillgridoutline(string name)
        //{
        //    string Fdate = "";
        //    label2.Text = name;

        //    string mline = name;
        //    dataGridView1.DataSource = null;
        //    dataGridView1.Rows.Clear();
        //    dataGridView2.DataSource = null;
        //    dataGridView2.Rows.Clear();
        //    dataGridView3.DataSource = null;
        //    dataGridView3.Rows.Clear();
        //    dataGridView4.DataSource = null;
        //    dataGridView4.Rows.Clear();
        //    dataGridView5.DataSource = null;
        //    dataGridView5.Rows.Clear();
        //    dataGridView6.DataSource = null;
        //    dataGridView6.Rows.Clear();

        //    dataGridView1.RowCount = 1;
        //    dataGridView1.ColumnCount = 4;

        //    dataGridView2.RowCount = 1;
        //    dataGridView2.ColumnCount = 4;

        //    dataGridView3.RowCount = 1;
        //    dataGridView3.ColumnCount = 4;

        //    dataGridView4.RowCount = 1;
        //    dataGridView4.ColumnCount = 4;

        //    dataGridView5.RowCount = 1;
        //    dataGridView5.ColumnCount = 4;

        //    dataGridView6.RowCount = 1;
        //    dataGridView6.ColumnCount = 4;

        //    DataTable dt21 = utilities.GetTable("select * from dbo.InterchangedEnergy where Code='" + name + "' and Date=(select CurrentDate from dbo.BiddingStrategySetting where id=(select max(id) from BiddingStrategySetting))");
        //    DataTable dt22 = utilities.GetTable("select * from dbo.InterchangedEnergy where Code='" + name + "' and Date=(select max(Date) from dbo.InterchangedEnergy where Code='" + name + "')");
        //    if (dt21.Rows.Count > 0)
        //    {
        //        Fdate = dt21.Rows[0][0].ToString();
        //    }
        //    else if (dt22.Rows.Count > 0)
        //    {
        //        Fdate = dt22.Rows[0][0].ToString();
        //    }


        //    DataTable dt = utilities.GetTable("select * from dbo.InterchangedEnergy where Code='" + name + "' and Date='" + Fdate + "'");

        //    if (dt.Rows.Count > 0)
        //    {

        //        for (int i = 0; i < 4; i++)
        //        {
        //            dataGridView1.Rows[0].Cells[i].Value = Math.Abs(MyDoubleParse(dt.Rows[0][i + 3].ToString()));
        //        }
        //        for (int i = 4; i < 8; i++)
        //        {
        //            dataGridView2.Rows[0].Cells[i - 4].Value = Math.Abs(MyDoubleParse(dt.Rows[0][i + 3].ToString()));
        //        }
        //        for (int i = 8; i < 12; i++)
        //        {
        //            dataGridView3.Rows[0].Cells[i - 8].Value = Math.Abs(MyDoubleParse(dt.Rows[0][i + 3].ToString()));
        //        }
        //        for (int i = 12; i < 16; i++)
        //        {
        //            dataGridView4.Rows[0].Cells[i - 12].Value = Math.Abs(MyDoubleParse(dt.Rows[0][i + 3].ToString()));
        //        }
        //        for (int i = 16; i < 20; i++)
        //        {
        //            dataGridView5.Rows[0].Cells[i - 16].Value = Math.Abs(MyDoubleParse(dt.Rows[0][i + 3].ToString()));
        //        }
        //        for (int i = 20; i < 24; i++)
        //        {
        //            dataGridView6.Rows[0].Cells[i - 20].Value = Math.Abs(MyDoubleParse(dt.Rows[0][i + 3].ToString()));
        //        }
        //    }
        //}
        //private void panelgilan_Click(object sender, EventArgs e)
        //{
        //    panelgilan.BackColor =Color.Yellow;
        //    panelloshan.BackColor = Color.Teal;
        //    panelinfo.Visible = true;
        //    fillgridplant("Gilan");
        //}

        //private void panelloshan_Click(object sender, EventArgs e)
        //{
        //    panelgilan.BackColor = Color.Teal;
        //    panelgasloshan.BackColor = Color.Teal;
        //    panelloshan.BackColor = Color.Yellow;
        //    panelinfo.Visible = true;
        //    fillgridloshan("Loshan-Steam");
        //}

        //private void panelgasloshan_Click(object sender, EventArgs e)
        //{
        //    panelgilan.BackColor = Color.Teal;
        //    panelloshan.BackColor = Color.Teal;
        //    panelgasloshan.BackColor = Color.Yellow;
        //    panelinfo.Visible = true;
        //   fillgridloshan("Loshan-Gas");
        //}

        //private void paneldize_Click(object sender, EventArgs e)
        //{
        //    panelinfo.Visible = true;
        //    fillgridoutline("SZ833");
        //}

        //private void panelazar_Click(object sender, EventArgs e)
        //{
        //    panelinfo.Visible = true;
        //    fillgridoutline("SY700");
        //}

        //private void paneldanial_Click(object sender, EventArgs e)
        //{
        //    panelinfo.Visible = true;
        //    fillgridoutline("DM818");
        //}

        //private void paneltonekabon_Click(object sender, EventArgs e)
        //{
        //    panelinfo.Visible = true;
        //    fillgridoutline("SB0305");
        //}

        //private void panelramsar_Click(object sender, EventArgs e)
        //{
        //    panelinfo.Visible = true;
        //    fillgridoutline("SB0302");
        //}

        //private void panelghazvin_Click(object sender, EventArgs e)
        //{
        //    panelinfo.Visible = true;
        //    fillgridoutline("GL813");
        //}

        //private void panelalborz_Click(object sender, EventArgs e)
        //{
        //    panelinfo.Visible = true;
        //    fillgridoutline("BL812");
        //}

        //private void panelzanjan_Click(object sender, EventArgs e)
        //{
        //    panelinfo.Visible = true;
        //    fillgridoutline("LQ808");
        //}

        //private void panelrajaie_Click(object sender, EventArgs e)
        //{
        //    panelinfo.Visible = true;
        //    fillgridoutline("AE900");
        //}

        //private void panelghayati_Click(object sender, EventArgs e)
        //{
        //    panelinfo.Visible = true;
        //    fillgridoutline("AQ903");
        //}

        //private void panelponel_Click(object sender, EventArgs e)
        //{
        //    panelinfo.Visible = true;
        //    fillgridoutline("BP806");
        //}

        private void btnPrint_Click(object sender, EventArgs e)
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("Hours");
            dt.Columns.Add("Value");

            for (int i = 0; i < 24; i++)
            {
                dt.Rows.Add("Hour" + (i + 1));
                dt.Rows[i][0] = "Hour" + (i + 1);
            }

            for (int i = 0; i < 4; i++)
            {
                if (dataGridView1.Rows[0].Cells[i].Value != null)
                {
                    dt.Rows[i][1] = dataGridView1.Rows[0].Cells[i].Value.ToString();
                }
            }
            for (int i = 4; i < 8; i++)
            {
                if (dataGridView2.Rows[0].Cells[i - 4].Value != null)
                {
                    dt.Rows[i][1] = dataGridView2.Rows[0].Cells[i - 4].Value.ToString();
                }
            }
            for (int i = 8; i < 12; i++)
            {
                if (dataGridView3.Rows[0].Cells[i - 8].Value != null)
                {
                    dt.Rows[i][1] = dataGridView3.Rows[0].Cells[i - 8].Value.ToString();
                }
            }
            for (int i = 12; i < 16; i++)
            {
                if (dataGridView4.Rows[0].Cells[i - 12].Value != null)
                {
                    dt.Rows[i][1] = dataGridView4.Rows[0].Cells[i - 12].Value.ToString();
                }
            }
            for (int i = 16; i < 20; i++)
            {
                if (dataGridView5.Rows[0].Cells[i - 16].Value != null)
                {
                    dt.Rows[i][1] = dataGridView5.Rows[0].Cells[i - 16].Value.ToString();
                }
            }
            for (int i = 20; i < 24; i++)
            {
                if (dataGridView6.Rows[0].Cells[i - 20].Value != null)
                {
                    dt.Rows[i][1] = dataGridView6.Rows[0].Cells[i - 20].Value.ToString();
                }
            }

            OpfPrint m = new OpfPrint(dt, label2.Text.Trim(), labeldate.Text.Trim());
            m.Show();
        }

       

       

    }
}
