﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Common;
using ILOG.Concert;
using ILOG.CPLEX;
using PowerPlantProject;
using FarsiLibrary.Utils;
using NRI.SBS.Common;
using System.Collections;
namespace PowerPlantProject
{
    public partial class GencoForm : Form
    {
        public GencoForm()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;
                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void GencoForm_Load(object sender, EventArgs e)
        {
            DataTable dt = Utilities.GetTable("select * from BaseGencoInfo");
            if (dt.Rows.Count > 0)
            {
                txtgenenglish.Text = dt.Rows[0]["GencoNameEnglish"].ToString().Trim();
                cmbgencode.Text = dt.Rows[0]["GencoCode"].ToString().Trim();
                cmbgenpersian.Text = dt.Rows[0]["GencoNamePersian"].ToString().Trim();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable DD = Utilities.GetTable("DELETE FROM BaseGencoInfo");

                DataTable dt = Utilities.GetTable("insert into BaseGencoInfo (GencoNameEnglish,GencoNamePersian,GencoCode)  values ('" + txtgenenglish.Text.Trim() + "',N'" + cmbgenpersian.Text.Trim() + "','" + cmbgencode.Text.Trim() + "')");

                MessageBox.Show("Info Save Successfully");
            }
            catch
            {

                MessageBox.Show("There is a Problem.");
            }
        }
    }
}
