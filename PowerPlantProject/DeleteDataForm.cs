﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FarsiLibrary.Utils;
using NRI.SBS.Common;
namespace PowerPlantProject
{
    public partial class DeleteDataForm : Form
    {
        List<PersianDate> missingDates;
        bool success;
        public DeleteDataForm()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                groupBox1.BackColor = FormColors.GetColor().Panelbackcolor;
                groupBox2.BackColor = FormColors.GetColor().Panelbackcolor;
                groupBox3.BackColor = FormColors.GetColor().Panelbackcolor;
                groupBox4.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            missingDates = GetDatesBetween();
            success = true;

            if (rdOptional.Checked)
            {
                if ((rb002.Checked) || (rb005.Checked) || (rdm005ba.Checked)||(rbdispatch.Checked) || (rbManategh.Checked)

                     || (rbProducedEnergy.Checked) || (rbInterchangedEnergy.Checked) || (rbLineNetComp.Checked)

                     || (rbLoadForecasting.Checked) || (rbOutageUnits.Checked) || (rbUnitNetComp.Checked)

                     || (rbSaledEnergy.Checked) || (rbAveragePrice.Checked) || (rbRegionNetComp.Checked) || (rbForeCasting.Checked)||(rbrep12.Checked)||(rbfinance.Checked)||(rbmarketbill.Checked))
                {

                    DialogResult d = MessageBox.Show("Are You Sure You Want To Delete Files From DataBase !", "Attention", MessageBoxButtons.YesNo);

                    if (d == DialogResult.Yes)
                    {

                        foreach (PersianDate date in missingDates)
                        {
                            delete(date.ToString("d"));
                        }

                        if (success)
                        {
                            MessageBox.Show("Delete Selected Files Successfully.");
                        }
                        else
                        {
                            MessageBox.Show("Delete Selected Files UnSuccessfully");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please select A Item !");

                }
            }
            else if (rdAll.Checked)
            {
                DialogResult d = MessageBox.Show("Are You Sure You Want To Delete Files From DataBase !", "Attention", MessageBoxButtons.YesNo);

                if (d == DialogResult.Yes)
                {

                    foreach (PersianDate date in missingDates)
                    {
                        delete(date.ToString("d"));
                    }

                    if (success)
                    {
                        MessageBox.Show("Delete Selected Files Successfully.");
                    }
                    else
                    {
                        MessageBox.Show("Delete Selected Files UnSuccessfully");
                    }
                }

            }

        }

        private List<PersianDate> GetDatesBetween()
        {           
            if (datePickerFrom != null)
            {
                List<PersianDate> missingDates = new List<PersianDate>();

                //DateTime dateFrom = (DateTime)PersianDateConverter.ToGregorianDateTime(new PersianDate(strDateFrom));
                DateTime dateFrom = datePickerFrom.SelectedDateTime.Date;
                DateTime dateTo = datePickerTo.SelectedDateTime.Date;



                missingDates.Add(dateFrom);

                TimeSpan span = dateTo - dateFrom;
                while (span.Days > 0)
                {
                    dateFrom = dateFrom.AddDays(1);
                    missingDates.Add(new PersianDate(dateFrom));
                    span = dateTo - dateFrom;
                }
                return missingDates;
            }
            else
                return null;
        }

        private void delete(string date)
        {
            if (rdOptional.Checked)
            {
                string strm00 = "";

                try
                {
                    //-------------------00----------------------
                    if (rb002.Checked)
                        strm00 = "delete from DetailFRM002 where TargetMarketDate='" + date + "'and Estimated=0"
                        + "delete from dbo.BlockFRM002 where TargetMarketDate='" + date + "'"
                        + "delete from dbo.MainFRM002  where TargetMarketDate='" + date + "'";
                    else if (rb005.Checked)
                        strm00 = "delete from DetailFRM005 where TargetMarketDate='" + date + "'"
                       + "delete from dbo.BlockFRM005 where TargetMarketDate='" + date + "'"
                       + "delete from dbo.MainFRM005  where TargetMarketDate='" + date + "'";
                    else if (rdm005ba.Checked)
                        strm00 = "delete from baDetailFRM005 where TargetMarketDate='" + date + "'"
                       + "delete from baBlockFRM005 where TargetMarketDate='" + date + "'"
                       + "delete from baMainFRM005 where TargetMarketDate='" + date + "'";
                    else if (rbdispatch.Checked)
                        strm00 = "delete from dbo.Dispathable where StartDate='" + date + "'";

                    if (strm00 != "")
                    {
                        DataTable del = Utilities.GetTable(strm00);
                        if (del == null) success = false;
                    }

                    //---------------------internet--------------------------
                    string item = "";

                    if (rbManategh.Checked)
                        item = "Manategh";
                    if (rbProducedEnergy.Checked)
                        item = "ProducedEnergy";
                    if (rbInterchangedEnergy.Checked)
                        item = "InterchangedEnergy";
                    if (rbLineNetComp.Checked)
                        item = "LineNetComp";
                    if (rbLoadForecasting.Checked)
                        item = "LoadForecasting";
                    if (rbOutageUnits.Checked)
                        item = "OutageUnits";
                    if (rbUnitNetComp.Checked)
                        item = "UnitNetComp";
                    if (rbSaledEnergy.Checked)
                        item = "SaledEnergy";
                    if (rbAveragePrice.Checked)
                        item = "AveragePrice";
                    if (rbRegionNetComp.Checked)
                        item = "RegionNetComp";
                    if (rbrep12.Checked)
                        item = "Rep12Page";

                    if (item != "")
                    {

                        DataTable d1 = null;

                        if (item == "LoadForecasting")
                        {
                            string[] date1 = new string[4];
                            date1[0] = date;
                            date1[1] = new PersianDate(PersianDateConverter.ToGregorianDateTime(date).AddDays(1)).ToString("d");
                            date1[2] = new PersianDate(PersianDateConverter.ToGregorianDateTime(date).AddDays(2)).ToString("d");
                            date1[3] = new PersianDate(PersianDateConverter.ToGregorianDateTime(date).AddDays(3)).ToString("d");
                            for (int i = 0; i < 4; i++)
                            {
                                /////////////////////////////////////////deleting//////////////////////////////////////////////
                                d1 = Utilities.GetTable("delete from " + item + " where DateEstimate='" + date + "' AND Date='" + date1[i] + "'");
                            }

                        }
                        else if (item == "SaledEnergy")
                        {
                            d1 = Utilities.GetTable("delete from SaledEnergyEc where date='" + date + "'");
                            d1 = Utilities.GetTable("delete from SaledEnergyuc where date='" + date + "'");
                            d1 = Utilities.GetTable("delete from fuelSaledEnergyec where date='" + date + "'");
                            d1 = Utilities.GetTable("delete from fuelSaledEnergyuc where date='" + date + "'");

                        }
                        else
                        {
                             d1 = Utilities.GetTable("delete from " + item + " where date='" + date + "'");

                        }

                        if (d1 == null) success = false;

                      
                    }


                    //----------------------forecast---------------------------
                    if (rbForeCasting.Checked)
                    {
                        DataTable df = Utilities.GetTable("delete from dbo.FinalForecast201Item where fk_ffHourly in( select FinalForecastHourly.id from dbo.FinalForecastHourly "

                             + "inner join dbo.FinalForecast on FinalForecast.id = FinalForecastHourly.fk_FinalForecastId where FinalForecast.date ='" + date + "')"

                             + "delete from FinalForecastHourly where fk_FinalForecastId in(select id from dbo.FinalForecast where FinalForecast.date ='" + date + "')"

                             + "delete from FinalForecast where FinalForecast.date ='" + date + "'");

                        if (df == null) success = false;
                    }


                    ///finance///////////////////////////////////////////

                    if (rbfinance.Checked)
                    {
                        DataTable df2 = Utilities.GetTable("delete from EconomicPlant where date='"+date+"'");

                        if (df2 == null) success = false;
                    }

                    if (rbmarketbill.Checked)
                    {
                        DataTable df3 = Utilities.GetTable("delete from MonthlyBillTotal where month like'%"+date.Substring(0,7).Trim()+"%'");
                        DataTable df4 = Utilities.GetTable("delete from MonthlyBillPlant where date like'%" + date.Substring(0, 7).Trim() + "%'");
                        DataTable df5 = Utilities.GetTable("delete from MonthlyBillDate where date like'%" + date.Substring(0, 7).Trim() + "%'");

                        if (df3 == null || df4 == null || df5 == null) success = false;
                    }


                }
                catch
                {
                    success = false;
                }

                ////////////////////////////////optional////////////////////////////////
                
            }
                ///////////////////////////////all//////////////////////////////////////
            else if (rdAll.Checked)
            {
                string strm00 = "";

                try
                {
                    //-------------------00----------------------
                    DataTable ddel = null;
                    
                        ddel=Utilities.GetTable("delete from DetailFRM002 where TargetMarketDate='" + date + "'and Estimated=0"
                        + "delete from dbo.BlockFRM002 where TargetMarketDate='" + date + "'"
                        + "delete from dbo.MainFRM002  where TargetMarketDate='" + date + "'");

                        if (ddel == null) success = false;

                         ddel=Utilities.GetTable("delete from DetailFRM005 where TargetMarketDate='" + date + "'"
                       + "delete from dbo.BlockFRM005 where TargetMarketDate='" + date + "'"
                       + "delete from dbo.MainFRM005  where TargetMarketDate='" + date + "'");

                        if (ddel == null) success = false;

                        ddel=Utilities.GetTable("delete from dbo.Dispathable where StartDate='" + date + "'");

                   
                        if (ddel == null) success = false;

                        ddel = Utilities.GetTable("delete from baDetailFRM005 where TargetMarketDate='" + date + "'"
                        + "delete from baBlockFRM005 where TargetMarketDate='" + date + "'"
                        + "delete from baMainFRM005  where TargetMarketDate='" + date + "'");
                        if (ddel == null) success = false;

                    //---------------------internet--------------------------
                    string[] item = new string[11];

                    
                        item[0] = "Manategh";                  
                        item[1] = "ProducedEnergy";                 
                        item[2] = "InterchangedEnergy";                
                        item[3] = "LineNetComp";                 
                        item[4] = "LoadForecasting";                  
                        item[5] = "OutageUnits";                  
                        item[6] = "UnitNetComp";                  
                        item[7] = "SaledEnergy";                  
                        item[8] = "AveragePrice";             
                        item[9] = "RegionNetComp";
                        item[10] = "Rep12Page";

                        foreach (string x in item)
                        {
                            DataTable d1 = null;
                          
                            if (x == "LoadForecasting")
                            {

                                string[] date1 = new string[4];
                                date1[0] = date;
                                date1[1] = new PersianDate(PersianDateConverter.ToGregorianDateTime(date).AddDays(1)).ToString("d");
                                date1[2] = new PersianDate(PersianDateConverter.ToGregorianDateTime(date).AddDays(2)).ToString("d");
                                date1[3] = new PersianDate(PersianDateConverter.ToGregorianDateTime(date).AddDays(3)).ToString("d");
                                for (int i = 0; i < 4; i++)
                                {
                                    /////////////////////////////////////////deleting//////////////////////////////////////////////
                                    d1 = Utilities.GetTable("delete from " + x + " where DateEstimate='" + date + "' AND Date='" + date1[i] + "'");
                                }
                            }
                            else if (x== "SaledEnergy")
                            {
                                d1 = Utilities.GetTable("delete from SaledEnergyEc where date='" + date + "'");
                                d1 = Utilities.GetTable("delete from SaledEnergyuc where date='" + date + "'");
                                d1 = Utilities.GetTable("delete from fuelSaledEnergyec where date='" + date + "'");
                                d1 = Utilities.GetTable("delete from fuelSaledEnergyuc where date='" + date + "'");

                            }
                            else
                            {
                                d1 = Utilities.GetTable("delete from " + x + " where date='" + date + "'");

                            }
                         
                            if (d1 == null) success = false;

                        }


                             

                    //----------------------forecast---------------------------
                   
                        DataTable df = Utilities.GetTable("delete from dbo.FinalForecast201Item where fk_ffHourly in( select FinalForecastHourly.id from dbo.FinalForecastHourly "

                             + "inner join dbo.FinalForecast on FinalForecast.id = FinalForecastHourly.fk_FinalForecastId where FinalForecast.date ='" + date + "')"

                             + "delete from FinalForecastHourly where fk_FinalForecastId in(select id from dbo.FinalForecast where FinalForecast.date ='" + date + "')"

                             + "delete from FinalForecast where FinalForecast.date ='" + date + "'");

                        if (df == null) success = false;



                       /////finance//////////////////////////////////////////////////////

                       
                       DataTable df2 = Utilities.GetTable("delete from EconomicPlant where date='" + date + "'");

                        if (df2 == null) success = false;

                    //////////////////////////////////////////////////////////////////////
                        
                            DataTable df3 = Utilities.GetTable("delete from MonthlyBillTotal where month like'%" + date.Substring(0, 7).Trim() + "%'");
                            DataTable df4 = Utilities.GetTable("delete from MonthlyBillPlant where date like'%" + date.Substring(0, 7).Trim() + "%'");
                            DataTable df5 = Utilities.GetTable("delete from MonthlyBillDate where date like'%" + date.Substring(0, 7).Trim() + "%'");

                            if (df3 == null || df4 == null || df5 == null) success = false;
                      
                    ///////////////////////////////////////////////////////////////////////

                }
                catch
                {
                    success = false;
                }

            }
        }

        private void DeleteDataForm_Load(object sender, EventArgs e)
        {
            datePickerFrom.SelectedDateTime = DateTime.Now.AddDays(-31);
            datePickerTo.SelectedDateTime = DateTime.Now.AddDays(-30);
        }

        private void datePickerFrom_ValueChanged(object sender, EventArgs e)
        {
            datePickerTo.SelectedDateTime = datePickerFrom.SelectedDateTime.AddDays(1);
        }

        private void rdOptional_CheckedChanged(object sender, EventArgs e)
        {
            if (rdOptional.Checked)
                panel1.Visible = true;
            else if (rdAll.Checked)           
                panel1.Visible = false;
           
        }
       
    }
}
