﻿namespace PowerPlantProject
{
    partial class EconomicAllPlantMonthlyReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.EconomicAllPlantMonthlycrystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            //this.EconomicAllPlantMonthlyCrystalReportDoc1 = new PowerPlantProject.EconomicAllPlantMonthlyCrystalReport();
            //this.SuspendLayout();
            // 
            // EconomicAllPlantMonthlycrystalReportViewer1
            // 
            //this.EconomicAllPlantMonthlycrystalReportViewer1.ActiveViewIndex = -1;
            //this.EconomicAllPlantMonthlycrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            //this.EconomicAllPlantMonthlycrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            //this.EconomicAllPlantMonthlycrystalReportViewer1.Location = new System.Drawing.Point(0, 0);
            //this.EconomicAllPlantMonthlycrystalReportViewer1.Name = "EconomicAllPlantMonthlycrystalReportViewer1";
            //this.EconomicAllPlantMonthlycrystalReportViewer1.SelectionFormula = "";
            //this.EconomicAllPlantMonthlycrystalReportViewer1.Size = new System.Drawing.Size(292, 266);
            //this.EconomicAllPlantMonthlycrystalReportViewer1.TabIndex = 0;
            //this.EconomicAllPlantMonthlycrystalReportViewer1.ViewTimeSelectionFormula = "";
            // 
            // EconomicAllPlantMonthlyReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
           // this.Controls.Add(this.EconomicAllPlantMonthlycrystalReportViewer1);
            this.Name = "EconomicAllPlantMonthlyReportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EconomicAllPlantMonthlyReportFrom";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.EconomicAllPlantMonthlyReportForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

       // private CrystalDecisions.Windows.Forms.CrystalReportViewer EconomicAllPlantMonthlycrystalReportViewer1;
      //  private EconomicAllPlantMonthlyCrystalReport EconomicAllPlantMonthlyCrystalReportDoc1;
    }
}