﻿namespace PowerPlantProject
{
    partial class AddUserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddUserForm));
            this.label6 = new System.Windows.Forms.Label();
            this.lbPassWord = new System.Windows.Forms.Label();
            this.lbrole = new System.Windows.Forms.Label();
            this.lblastname = new System.Windows.Forms.Label();
            this.lbfirstname = new System.Windows.Forms.Label();
            this.btnsave = new System.Windows.Forms.Button();
            this.cmbrole = new System.Windows.Forms.ComboBox();
            this.txtusername = new System.Windows.Forms.TextBox();
            this.txtpass = new System.Windows.Forms.TextBox();
            this.txtfirstname = new System.Windows.Forms.TextBox();
            this.txtlastname = new System.Windows.Forms.TextBox();
            this.lblMsg = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(65, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "UserName :";
            // 
            // lbPassWord
            // 
            this.lbPassWord.AutoSize = true;
            this.lbPassWord.BackColor = System.Drawing.Color.Transparent;
            this.lbPassWord.Location = new System.Drawing.Point(234, 179);
            this.lbPassWord.Name = "lbPassWord";
            this.lbPassWord.Size = new System.Drawing.Size(62, 13);
            this.lbPassWord.TabIndex = 24;
            this.lbPassWord.Text = "PassWord :";
            // 
            // lbrole
            // 
            this.lbrole.AutoSize = true;
            this.lbrole.BackColor = System.Drawing.Color.Transparent;
            this.lbrole.Location = new System.Drawing.Point(165, 105);
            this.lbrole.Name = "lbrole";
            this.lbrole.Size = new System.Drawing.Size(35, 13);
            this.lbrole.TabIndex = 26;
            this.lbrole.Text = "Role :";
            // 
            // lblastname
            // 
            this.lblastname.AutoSize = true;
            this.lblastname.BackColor = System.Drawing.Color.Transparent;
            this.lblastname.Location = new System.Drawing.Point(234, 28);
            this.lblastname.Name = "lblastname";
            this.lblastname.Size = new System.Drawing.Size(61, 13);
            this.lblastname.TabIndex = 25;
            this.lblastname.Text = "LastName :";
            // 
            // lbfirstname
            // 
            this.lbfirstname.AutoSize = true;
            this.lbfirstname.BackColor = System.Drawing.Color.Transparent;
            this.lbfirstname.Location = new System.Drawing.Point(65, 28);
            this.lbfirstname.Name = "lbfirstname";
            this.lbfirstname.Size = new System.Drawing.Size(60, 13);
            this.lbfirstname.TabIndex = 21;
            this.lbfirstname.Text = "FirstName :";
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.Color.CadetBlue;
            this.btnsave.BackgroundImage = global::PowerPlantProject.Properties.Resources.Savedata;
            this.btnsave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnsave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsave.Location = new System.Drawing.Point(141, 275);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(86, 27);
            this.btnsave.TabIndex = 9;
            this.btnsave.Text = "Save";
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // cmbrole
            // 
            this.cmbrole.FormattingEnabled = true;
            this.cmbrole.Items.AddRange(new object[] {
            " Administrator        ",
            " NoMaintenance",
            " NoBidding",
            " NoMarketBill",
            " ExceptComputational"});
            this.cmbrole.Location = new System.Drawing.Point(124, 131);
            this.cmbrole.Name = "cmbrole";
            this.cmbrole.Size = new System.Drawing.Size(121, 21);
            this.cmbrole.TabIndex = 6;
            // 
            // txtusername
            // 
            this.txtusername.Location = new System.Drawing.Point(38, 210);
            this.txtusername.Name = "txtusername";
            this.txtusername.Size = new System.Drawing.Size(120, 20);
            this.txtusername.TabIndex = 7;
            this.txtusername.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtusername.Leave += new System.EventHandler(this.txtusername_Leave);
            // 
            // txtpass
            // 
            this.txtpass.Location = new System.Drawing.Point(209, 210);
            this.txtpass.Name = "txtpass";
            this.txtpass.Size = new System.Drawing.Size(120, 20);
            this.txtpass.TabIndex = 8;
            this.txtpass.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtpass.UseSystemPasswordChar = true;
            this.txtpass.Leave += new System.EventHandler(this.txtpass_Leave);
            // 
            // txtfirstname
            // 
            this.txtfirstname.Location = new System.Drawing.Point(38, 60);
            this.txtfirstname.Name = "txtfirstname";
            this.txtfirstname.Size = new System.Drawing.Size(120, 20);
            this.txtfirstname.TabIndex = 4;
            this.txtfirstname.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtlastname
            // 
            this.txtlastname.Location = new System.Drawing.Point(209, 60);
            this.txtlastname.Name = "txtlastname";
            this.txtlastname.Size = new System.Drawing.Size(120, 20);
            this.txtlastname.TabIndex = 5;
            this.txtlastname.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.BackColor = System.Drawing.Color.Transparent;
            this.lblMsg.ForeColor = System.Drawing.Color.Red;
            this.lblMsg.Location = new System.Drawing.Point(14, 375);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(0, 13);
            this.lblMsg.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.Location = new System.Drawing.Point(14, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 28;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.btnsave);
            this.groupBox1.Controls.Add(this.txtlastname);
            this.groupBox1.Controls.Add(this.txtfirstname);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtpass);
            this.groupBox1.Controls.Add(this.lbPassWord);
            this.groupBox1.Controls.Add(this.txtusername);
            this.groupBox1.Controls.Add(this.lbrole);
            this.groupBox1.Controls.Add(this.cmbrole);
            this.groupBox1.Controls.Add(this.lblastname);
            this.groupBox1.Controls.Add(this.lbfirstname);
            this.groupBox1.Location = new System.Drawing.Point(12, 53);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(378, 317);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            // 
            // AddUserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(404, 411);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblMsg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AddUserForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddUserForm";
            this.Load += new System.EventHandler(this.AddUserForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbPassWord;
        private System.Windows.Forms.Label lbrole;
        private System.Windows.Forms.Label lblastname;
        private System.Windows.Forms.Label lbfirstname;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.ComboBox cmbrole;
        private System.Windows.Forms.TextBox txtusername;
        private System.Windows.Forms.TextBox txtpass;
        private System.Windows.Forms.TextBox txtfirstname;
        private System.Windows.Forms.TextBox txtlastname;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;


    }
}