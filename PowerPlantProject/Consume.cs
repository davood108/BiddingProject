﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using System.IO;
using Microsoft.Win32;


namespace PowerPlantProject
{
    public partial class Consume : Form
    {
        public Consume()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dataGridView2.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void Consume_Load(object sender, EventArgs e)
        {
            //textBox1.Text = "1";
            dataGridView2.Columns[0].Width = 17;
            dataGridView2.Columns[0].HeaderText = "";
            datePickerFrom.Text = new PersianDate(DateTime.Now).ToString("d");
            datePickerto.Text = new PersianDate(DateTime.Now.AddDays(1)).ToString("d");
            DataTable c = Utilities.GetTable("select ppid from powerplant");
            cmbplant.Text = c.Rows[0][0].ToString();
            foreach (DataRow n in c.Rows)
            {
                cmbplant.Items.Add(n[0].ToString().Trim());

            }
            //DataTable v = utilities.GetTable("select unitcode from UnitsDataMain where ppid='" + cmbplant.Text.Trim() + "'order by unitcode asc");
            //foreach (DataRow x in v.Rows)
            //{
            //    cmbunit.Items.Add(x[0].ToString().Trim());

            //}


            DataTable dp = Utilities.GetTable("SELECT    UnitCode,PackageType   FROM dbo.UnitsDataMain WHERE PPID='"+cmbplant.Text.Trim()+"'");
                string blockM005 = "";

                foreach (DataRow m in dp.Rows)
                {

                    int ppidWithPriority =int.Parse( cmbplant.Text.Trim());
                    //if (isdual) ppidWithPriority++;
                    string unit = m["UnitCode"].ToString().Trim();
                    string package = m["PackageType"].ToString().Trim();


                    string ptypenum = "0";
                    if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";
                    if (Findcconetype(cmbplant.Text.Trim())) ptypenum = "0";
                    string temp = unit.ToLower();
                    if (package.Contains("CC"))
                    {
                        temp = temp.Replace("cc", "c");
                        string[] sp = temp.Split('c');
                        temp = sp[0].Trim() + sp[1].Trim();
                        if (temp.Contains("gas"))
                        {
                            temp = temp.Replace("gas", "G");
                        }
                        else
                        {
                            temp = temp.Replace("steam", "S");
                        }
                        temp = ppidWithPriority + "-" + temp;
                    }
                    else if (temp.Contains("gas"))
                    {
                        temp = ppidWithPriority + "-" + temp.Replace("gas", "G");
                    }
                    else if (temp.Contains("h"))
                    {
                        temp = ppidWithPriority + "-" + temp.Replace("h", "H");
                    }
                    else
                    {
                        temp = ppidWithPriority + "-" + temp.Replace("steam", "S");
                    }

                    blockM005 = temp.Trim();
                    cmbunit.Items.Add(blockM005);

                }









            cmbunit.Text = "All Units";
            cmbunit.Items.Add("All Units");


            cmbfhour.Text = "1";
            cmbthour.Text = "24";
            fillallunit();
        }
        public static bool Findcconetype(string ppid)
        {
            int tr = 0;

            DataTable oDataTable = Utilities.GetTable("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count == 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }
        private void btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                double c = MyDoubleParse(textBox1.Text);
            }
            catch
            {
                textBox1.Text = "";
            }
            int fromh = int.Parse(cmbfhour.Text.Trim());
            int thour = int.Parse(cmbthour.Text.Trim());
            string status = "";
          

            
                for (int h = fromh; h <= thour; h++)
                {
                    dataGridView1.Rows[0].Cells[h - 1].Value = textBox1.Text ;

                }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {

            dataGridView1.Rows.Clear();
        }
        public void fillallunit()
        {
            dataGridView2.DataSource = null;
            DataTable dd = Utilities.GetTable("select PPID,unit,fromdate,todate,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15,h16,h17,h18,h19,h20,h21,h22,h23,h24 from consumed order by PPID,unit asc");
            dataGridView2.DataSource = dd;
        }
        public void fill()
        {
            string annd = "";
            if (cmbunit.Text.Trim() != "All Units")
            {
                annd = "and unit='"+cmbunit.Text.Trim()+"'";
            }
            DataTable x = Utilities.GetTable("select * from consumed where fromdate='" + datePickerFrom.Text.Trim() + "'and todate='" + datePickerto.Text.Trim() + "'and ppid='" + cmbplant.Text.Trim() + "'"+annd+"");
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            if (x.Rows.Count > 0)
            {
                for (int i = 0; i < 24; i++)
                {
                    string r = x.Rows[0][i + 4].ToString().Trim();
                    string[] val = r.Split('-');
                    dataGridView1.Rows[0].Cells[i].Value = r.ToString();
                    if (r == "") dataGridView1.Rows[0].Cells[i].Style.BackColor = Color.Gray;
               



                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            TimeSpan span = PersianDateConverter.ToGregorianDateTime(datePickerto.Text) - PersianDateConverter.ToGregorianDateTime(datePickerFrom.Text);
            int dday = span.Days;
            if (dday < 0)
            {
                MessageBox.Show("Please Select Correct Interval Date!");
                return;
            }

            if (cmbunit.Text == "All Units" && textBox1.Text != "" && cmbunit.Text != "")
            {
                ///////////////////////////////////////////////////////
                for (int yn = 0; yn < cmbunit.Items.Count - 1; yn++)
                {
                    string uname = cmbunit.Items[yn].ToString();
                    string st1 = "insert into consumed (ppid,unit,fromdate,todate,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15,h16,h17,h18,h19,h20,h21,h22,h23,h24)values('" + cmbplant.Text.Trim() + "','" + uname + "','" + datePickerFrom.Text.Trim() + "','" + datePickerto.Text.Trim();
                    DataTable d = Utilities.GetTable("delete from consumed where unit='" + uname + "'and ppid='" + cmbplant.Text.Trim() + "'and fromdate='" + datePickerFrom.Text + "'and todate='" + datePickerto.Text + "'");
                    string st2 = "";
                    string v = "";
                    for (int i = 0; i < 24; i++)
                    {
                        try
                        {
                            v = dataGridView1.Rows[0].Cells[i].Value.ToString().Trim();
                        }
                        catch
                        {
                            v = "";
                        }


                        st2 += ("','" + v);
                        if (i == 23) st2 += "')";
                    }
                    DataTable ind = Utilities.GetTable(st1 + st2);


                }
                MessageBox.Show("Saved.");
                fillallunit();
                fill();
            }
            else if (textBox1.Text != "" && cmbunit.Text != "")
            {
                string st1 = "insert into consumed (ppid,unit,fromdate,todate,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15,h16,h17,h18,h19,h20,h21,h22,h23,h24)values('" + cmbplant.Text.Trim() + "','" + cmbunit.Text.Trim() + "','" + datePickerFrom.Text.Trim() + "','" + datePickerto.Text.Trim();
                DataTable d = Utilities.GetTable("delete from consumed where unit='" + cmbunit.Text.Trim() + "'and ppid='" + cmbplant.Text.Trim() + "'and fromdate='" + datePickerFrom.Text + "'and todate='" + datePickerto.Text + "'");
                string st2 = "";
                string v = "";
                for (int i = 0; i < 24; i++)
                {
                    try
                    {
                        v = dataGridView1.Rows[0].Cells[i].Value.ToString().Trim();
                    }
                    catch
                    {
                        v = "";
                    }


                    st2 += ("','" + v);
                    if (i == 23) st2 += "')";
                }
                DataTable ind = Utilities.GetTable(st1 + st2);
                MessageBox.Show("Saved.");
                fillallunit();
                fill();
            }
            else
            {
                MessageBox.Show("Fill Completely.");
            }

        }

        private void cmbunit_SelectedValueChanged(object sender, EventArgs e)
        {
            fill();
        }

        private void cmbplant_SelectedValueChanged(object sender, EventArgs e)
        {
            cmbunit.Items.Clear();
            DataTable dp = Utilities.GetTable("SELECT    UnitCode,PackageType   FROM dbo.UnitsDataMain WHERE PPID='" + cmbplant.Text.Trim() + "'");
            string blockM005 = "";

            foreach (DataRow m in dp.Rows)
            {

                int ppidWithPriority = int.Parse(cmbplant.Text.Trim());
                //if (isdual) ppidWithPriority++;
                string unit = m["UnitCode"].ToString().Trim();
                string package = m["PackageType"].ToString().Trim();


                string ptypenum = "0";
                if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";
                if (Findcconetype(cmbplant.Text.Trim())) ptypenum = "0";
                string temp = unit.ToLower();
                if (package.Contains("CC"))
                {
                    temp = temp.Replace("cc", "c");
                    string[] sp = temp.Split('c');
                    temp = sp[0].Trim() + sp[1].Trim();
                    if (temp.Contains("gas"))
                    {
                        temp = temp.Replace("gas", "G");
                    }
                    else
                    {
                        temp = temp.Replace("steam", "S");
                    }
                    temp = ppidWithPriority + "-" + temp;
                }
                else if (temp.Contains("gas"))
                {
                    temp = ppidWithPriority + "-" + temp.Replace("gas", "G");
                }
                else if (temp.Contains("h"))
                {
                    temp = ppidWithPriority + "-" + temp.Replace("h", "H");
                }
                else
                {
                    temp = ppidWithPriority + "-" + temp.Replace("steam", "S");
                }

                blockM005 = temp.Trim();
                cmbunit.Items.Add(blockM005);

            }

            

            cmbunit.Text = "All Units";
            cmbunit.Items.Add("All Units");
            fill();
        }
        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "" || str.Trim() == null)
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }

        private void datePickerFrom_ValueChanged(object sender, EventArgs e)
        {
            fill();
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;

            string x = dataGridView2.Rows[rowIndex].Cells[e.ColumnIndex].Value.ToString();
            if (x == "System.Drawing.Bitmap")
            {
                DataTable bb = Utilities.GetTable("delete from consumed where ppid='" + dataGridView2.Rows[rowIndex].Cells[1].Value.ToString() + "' and  unit='" + dataGridView2.Rows[rowIndex].Cells[2].Value.ToString() + "'and fromdate='" + dataGridView2.Rows[rowIndex].Cells[3].Value.ToString() + "'and todate='" + dataGridView2.Rows[rowIndex].Cells[4].Value.ToString() + "'");
            }
            fillallunit();
        }
    }
}
