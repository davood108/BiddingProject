﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FarsiLibrary.Utils;
using NRI.SBS.Common;
using System.Data.SqlClient;

namespace PowerPlantProject
{
    public partial class Profile : Form
    {

        string ppid1 = "";
        public Profile()
        {
            InitializeComponent();

            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;
                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

               
            }
            catch
            {

            }
        }

        private void Profile_Load(object sender, EventArgs e)
        {
            DataTable dddd = Utilities.GetTable("select * from powerplant");
            ppid1 = dddd.Rows[0][0].ToString().Trim();
            ////////////////////////////////////timer/////////////////////////////////////////// 


            System.Globalization.PersianCalendar p = new System.Globalization.PersianCalendar();

            label42.Text =p.GetSecond(DateTime.Now) + " : " + p.GetMinute(DateTime.Now) + " : " + p.GetHour(DateTime.Now)+"     "+ new PersianDate(DateTime.Now).ToString("d");
            timer1.Start();

            try
            {
                fillunits();
                filldata();
            }
            catch
            {

            }

        }

      

        private void timer1_Tick(object sender, EventArgs e)
        {
            System.Globalization.PersianCalendar p = new System.Globalization.PersianCalendar();
            label42.Text = p.GetSecond(DateTime.Now) + " : " + p.GetMinute(DateTime.Now) + " : " + p.GetHour(DateTime.Now)+"      "+ new PersianDate(DateTime.Now).ToString("d");
        }

        public void fillunits()
        {

            string today=new PersianDate(DateTime.Now).ToString("d");

            DataTable dts = Utilities.GetTable("select * from dbo.UnitsDataMain where PPID='" + ppid1 + "'and UnitCode='Gas11'");
            DataTable dtg1 = Utilities.GetTable("select * from dbo.UnitsDataMain where PPID='" + ppid1 + "'and UnitCode='Gas12'");
            DataTable dtg2 = Utilities.GetTable("select * from dbo.UnitsDataMain where PPID='" + ppid1 + "'and UnitCode='Gas13'");
            DataTable dtg3 = Utilities.GetTable("select * from dbo.UnitsDataMain where PPID='" + ppid1 + "'and UnitCode='Gas14'");
            DataTable dtg4 = Utilities.GetTable("select * from dbo.UnitsDataMain where PPID='" + ppid1 + "'and UnitCode='Gas15'");
            DataTable dtg5 = Utilities.GetTable("select * from dbo.UnitsDataMain where PPID='" + ppid1 + "'and UnitCode='Gas16'");

            foreach (DataRow m in dtg3.Rows)
            {                
                txG14avc.Text = m["Avc"].ToString().Trim();
                txtG14fuel.Text = "Gas /"+m["SecondFuel"].ToString().Trim();
                txtG14pmax.Text = m["PMax"].ToString().Trim();
                txtG14pmin.Text = m["PMin"].ToString().Trim();                
                txtG14status.Text = m["Status"].ToString().Trim();
                if (txtG14status.Text == "0") txtG14status.Text = "OFF";

            }
            foreach (DataRow m in dts.Rows)
            {
                txtg11avc.Text = m["Avc"].ToString().Trim();
                txtg11fuel.Text = "Gas/" + m["SecondFuel"].ToString().Trim();
                txtg11pmax.Text = m["PMax"].ToString().Trim();
                txtg11pmin.Text = m["PMin"].ToString().Trim();
                txtg11status.Text = m["Status"].ToString().Trim();
                if (txtg11status.Text == "0") txtg11status.Text = "OFF";

            }
            foreach (DataRow m in dtg2.Rows)
            {
                txtg13avc.Text= m["Avc"].ToString().Trim();
                txtg13fuel.Text = "Gas/" + m["SecondFuel"].ToString().Trim();
                txtg13pmax.Text = m["PMax"].ToString().Trim();
                txtg13pmin.Text = m["PMin"].ToString().Trim();
                txtg13status.Text = m["Status"].ToString().Trim();
                if (txtg13status.Text == "0") txtg13status.Text = "OFF";

            }

            foreach (DataRow m in dtg1.Rows)
            {
                txt12AVC.Text = m["Avc"].ToString().Trim();
               tXT12FUEL.Text = "Gas/" + m["SecondFuel"].ToString().Trim();
                tXT12PMAX.Text = m["PMax"].ToString().Trim();
                txt12pmin.Text = m["PMin"].ToString().Trim();
                tXT12STATUS.Text = m["Status"].ToString().Trim();
                if (tXT12STATUS.Text == "0") tXT12STATUS.Text = "OFF";

            }
            foreach (DataRow m in dtg4.Rows)
            {
                txt15avc.Text = m["Avc"].ToString().Trim();
                txt15fuel.Text = "Gas/" + m["SecondFuel"].ToString().Trim();
                txt15pmax.Text= m["PMax"].ToString().Trim();
                TXT15PMIN.Text = m["PMin"].ToString().Trim();
                txt15sta.Text = m["Status"].ToString().Trim();
                if (txt15sta.Text == "0") txt15sta.Text = "OFF";


            }
            foreach (DataRow m in dtg5.Rows)
            {
               txt16avc.Text = m["Avc"].ToString().Trim();
               txt16fuel.Text = "Gas/" + m["SecondFuel"].ToString().Trim();
               txt16pmax.Text = m["PMax"].ToString().Trim();
               txt16pmin.Text = m["PMin"].ToString().Trim();
               txt16status.Text = m["Status"].ToString().Trim();
               if (txt16status.Text == "0") txt16status.Text = "OFF";

            }



            DataTable dtoutg1 = Utilities.GetTable("select * from dbo.Dispathable where PPID='" + ppid1 + "' and StartDate='" + today + "'and Block='G11'");
            DataTable dtoutg2 = Utilities.GetTable("select * from dbo.Dispathable where PPID='" + ppid1 + "' and StartDate='" + today + "'and Block='G12'");
            DataTable dtoutg3 = Utilities.GetTable("select * from dbo.Dispathable where PPID='" + ppid1 + "' and StartDate='" + today + "'and Block='G13'");
            DataTable dtoutg4 = Utilities.GetTable("select * from dbo.Dispathable where PPID='" + ppid1 + "' and StartDate='" + today + "'and Block='G14'");
            DataTable dtoutg5 = Utilities.GetTable("select * from dbo.Dispathable where PPID='" + ppid1 + "' and StartDate='" + today + "'and Block='G15'");
            DataTable dtoutg6 = Utilities.GetTable("select * from dbo.Dispathable where PPID='" + ppid1 + "' and StartDate='" + today + "'and Block='G16'");
           txtG14out.Text = "No";
           txtg11out.Text = "No";
           txtg13out.Text = "No";
           txt15out.Text = "No";
           tXT12OUT.Text = "No";
           txt16out.Text = "No";
           foreach (DataRow m in dtoutg3.Rows)
           {
             
               if (m["DeclaredCapacity"].ToString().Trim() == "0" || m["DispachableCapacity"].ToString().Trim() == "0")
               {
                   txtg13out.Text = "Yes";
                   break;
               }               
                   
           }
           foreach (DataRow m in dtoutg1.Rows)
           {
              
               if (m["DeclaredCapacity"].ToString().Trim() == "0" || m["DispachableCapacity"].ToString().Trim() == "0")
               {
                   txtg11out.Text = "Yes";
                   break;
               }

           }
           foreach (DataRow m in dtoutg2.Rows)
           {
             
               if (m["DeclaredCapacity"].ToString().Trim() == "0" || m["DispachableCapacity"].ToString().Trim() == "0")
               {
                   tXT12OUT.Text = "Yes";
                   break;
               }

           }
           foreach (DataRow m in dtoutg4.Rows)
           {

               if (m["DeclaredCapacity"].ToString().Trim() == "0" || m["DispachableCapacity"].ToString().Trim() == "0")
               {
                   txtG14out.Text = "Yes";
                   break;
               }

           }
           foreach (DataRow m in dtoutg5.Rows)
           {

               if (m["DeclaredCapacity"].ToString().Trim() == "0" || m["DispachableCapacity"].ToString().Trim() == "0")
               {
                   txt15out.Text = "Yes";
                   break;
               }

           }
           foreach (DataRow m in dtoutg6.Rows)
           {

               if (m["DeclaredCapacity"].ToString().Trim() == "0" || m["DispachableCapacity"].ToString().Trim() == "0")
               {
                   txt16out.Text = "Yes";
                   break;
               }

           }



           DataTable dtBASE = Utilities.GetTable("SELECT *from dbo.BaseData where Date<='" + today + "' and BaseID=(select max(BaseID) from dbo.BaseData where Date<='" + today + "')");
           if (dtBASE.Rows.Count > 0)
           {
               LBLMaxprice.Text = dtBASE.Rows[0]["MarketPriceMax"].ToString().Trim() +" R";
               lblminprice.Text = dtBASE.Rows[0]["MarketPriceMin"].ToString().Trim() +" R";
           }


           DataTable dbid = Utilities.GetTable("SELECT * FROM dbo.BiddingStrategySetting where Plant='CCOrumieh' and BiddingDate=(select max(BiddingDate)from dbo.BiddingStrategySetting )order by id desc");
           DataTable dtbid = Utilities.GetTable("select * from dbo.BidRunSetting where Plant='CCOrumieh'order by id desc");

           if (dbid.Rows.Count > 0) lbldate.Text = dbid.Rows[0]["BiddingDate"].ToString().Trim();
           if (dtbid.Rows.Count > 0)
           {
               if (dtbid.Rows[0]["FlagMarketPower"].ToString() == "True") lblmarketpower.Text = "Yes";
               if (dtbid.Rows[0]["FlagLossPayment"].ToString() == "True")lbllosspayment.Text = "Yes";
               if (dtbid.Rows[0]["FairPlay"].ToString() == "True") lblminpower.Text = "Yes";
               if (dtbid.Rows[0]["FlagCostFunction"].ToString() == "True") lblcostpayment.Text = "Yes";
               if (dtbid.Rows[0]["LMP"].ToString() == "True") lblcapbid.Text = "Yes";




           }

            ////////////////////////////////detect status ////////////////////////////////////////////////
           DataTable dstatusg11 = Utilities.GetTable(" select  COUNT(PPID) FROM [DetailFRM005] WHERE TargetMarketDate='" + today + "' AND PPID='" + ppid1 + "' AND Block='515-G11' and PPType='0' AND Hour='" + DateTime.Now.Hour + "' AND Required<>0");
           DataTable dstatusg12 = Utilities.GetTable(" select  COUNT(PPID) FROM [DetailFRM005] WHERE TargetMarketDate='" + today + "' AND PPID='" + ppid1 + "' AND Block='515-G12' and PPType='0' AND Hour='" + DateTime.Now.Hour + "' AND Required<>0");
           DataTable dstatusg13 = Utilities.GetTable(" select  COUNT(PPID) FROM [DetailFRM005] WHERE TargetMarketDate='" + today + "' AND PPID='" + ppid1 + "' AND Block='515-G13' and PPType='0' AND Hour='" + DateTime.Now.Hour + "' AND Required<>0");

           DataTable dstatusg14 = Utilities.GetTable(" select  COUNT(PPID) FROM [DetailFRM005] WHERE TargetMarketDate='" + today + "' AND PPID='" + ppid1 + "' AND Block='515-G14' and PPType='0' AND Hour='" + DateTime.Now.Hour + "' AND Required<>0");
           DataTable dstatusg15 = Utilities.GetTable(" select  COUNT(PPID) FROM [DetailFRM005] WHERE TargetMarketDate='" + today + "' AND PPID='" + ppid1 + "' AND Block='515-G15' and PPType='0' AND Hour='" + DateTime.Now.Hour + "' AND Required<>0");
           DataTable dstatusg16 = Utilities.GetTable(" select  COUNT(PPID) FROM [DetailFRM005] WHERE TargetMarketDate='" + today + "' AND PPID='" + ppid1 + "' AND Block='515-G16' and PPType='0' AND Hour='" + DateTime.Now.Hour + "' AND Required<>0");



           if (dstatusg11.Rows[0][0].ToString() != "0") txtg11status.Text = "ON";
           if (dstatusg12.Rows[0][0].ToString() != "0") tXT12STATUS.Text = "ON";
           if (dstatusg13.Rows[0][0].ToString() != "0") txtg13status.Text = "ON";
           if (dstatusg14.Rows[0][0].ToString() != "0") txtG14status.Text = "ON";
           if (dstatusg15.Rows[0][0].ToString() != "0") txt15sta.Text = "ON";
           if (dstatusg16.Rows[0][0].ToString() != "0") txt16status.Text = "ON";

            ////////////////////////////////////////////////////////////////////////////////////////////

        }
        public void filldata()
        {
            string today = new PersianDate(DateTime.Now).ToString("d");

            //////////////////////////////////////////002,005,dispatch////////////////////////////////////

            DataTable m002table = Utilities.GetTable("select * from  dbo.DetailFRM002 where PPID='" + ppid1 + "'and TargetMarketDate='" + today + "'and ( Estimated is null or Estimated = 0)");
            DataTable m005table = Utilities.GetTable("select * from  dbo.DetailFRM005 where PPID='" + ppid1 + "'and TargetMarketDate='" + today + "'");
            DataTable Dispatch = Utilities.GetTable("select count(*) from dbo.Dispathable WHERE PPID='" + ppid1 + "' AND StartDate='" + today + "'");

            if (m002table.Rows.Count <= 23)
            {
                m002cancel.Visible = true;
                m002ok.Visible = false;
            }
            else
            {
                m002cancel.Visible = false;
                m002ok.Visible = true;
            }



            if (m005table.Rows.Count <= 23)
            {
                m005cancel.Visible = true;
                m005ok.Visible = false;
            }
            else
            {
                m005cancel.Visible = false;
                m005ok.Visible = true;
            }



            if (int.Parse(Dispatch.Rows[0][0].ToString()) <= 23)
            {
                dispatchcancel.Visible = true;
                dispatchok.Visible = false;


            }
            else
            {
                dispatchcancel.Visible = false;
                dispatchok.Visible = true;
            }
            //////////////////////////////////ls2 and finance/////////////////////////////////////////
            string yesterday = new PersianDate(DateTime.Now.AddDays(-1)).ToString("d");
            DataTable econo = Utilities.GetTable("select * from economicplant where ppid='" + ppid1 + "'and date='" + yesterday + "'");
            DataTable m009tTable = Utilities.GetTable("select * from dbo.DetailFRM009 where PPID='" + ppid1 + "'and TargetMarketDate='" + today + "'");
            if (m009tTable.Rows.Count <= 23)
            {
                ls2cancel.Visible = true;
                ls2ok.Visible = false;
            }
            else
            {
                ls2cancel.Visible = false;
                ls2ok.Visible = true;
            }

            if (econo.Rows.Count == 0)
            {
                financecancel.Visible = true;
                financok.Visible = false;

            }
            else
            {
                financecancel.Visible = false;
                financok.Visible = true;
            }

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Internet_Report v = new Internet_Report();
            v.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            panel11.Visible = true;
        }

        

       

       

        
    }
}
