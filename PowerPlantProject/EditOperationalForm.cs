﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using FarsiLibrary.Win;
using System.Diagnostics;
using System.Reflection;
using NRI.SBS.Common;
using System.Collections;
using System.IO;

namespace PowerPlantProject
{
    public partial class EditOperationalForm : Form
    {
        string ppid = "";
        string unitcode = "";
        string type = "";

        public EditOperationalForm(string id,string unit,string btype)
        {
            ppid = id;
            unitcode = unit;
            type = btype;

            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void EditOperationalForm_Load(object sender, EventArgs e)
        {
            filleditoperational(type);
        }
        private void filleditoperational(string type)
        {
            editoperationalgrid.DataSource = null;
            //editoperationalgrid.AutoSize = true;
            
           
         
            DataTable odata = null;
            if (type == "outservice")
            {
                
                odata = Utilities.GetTable("select Date,OutService,OutServiceStartDate,OutServiceEndDate,OutServiceStartHour,OutServiceEndHour from dbo.ConditionUnit where PPID='"+ppid+"' and UnitCode='"+unitcode +"' order by  Date asc");
                editoperationalgrid.DataSource = odata.DefaultView;
                editoperationalgrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
             

            }
            if (type == "secondfuel")
            {
                odata = Utilities.GetTable("select Date,SecondFuel,SecondFuelStartDate,SecondFuelEndDate,SecondFuelStartHour,SecondFuelEndHour  from dbo.ConditionUnit where PPID='" + ppid + "' and UnitCode='" + unitcode + "' order by  Date asc");
                editoperationalgrid.DataSource = odata.DefaultView;
                editoperationalgrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            }

            if (type == "maintenance")
            {

                odata = Utilities.GetTable("select Date,Maintenance,MaintenanceStartDate,MaintenanceEndDate,MaintenanceStartHour,MaintenanceEndHour from dbo.ConditionUnit where PPID='"+ppid+"' and UnitCode='"+unitcode +"'order by  Date asc");
                editoperationalgrid.DataSource = odata.DefaultView;
                editoperationalgrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            }
        }

        private void editoperationalgrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (editoperationalgrid.CurrentCell.Value.ToString().Trim() == "Delete")
           {
               if (type == "outservice")
               {
                   editoperationalgrid.AllowUserToDeleteRows = true;
                   string sn = editoperationalgrid.CurrentRow.Cells[2].Value.ToString();

                   DataTable otable = Utilities.GetTable("update dbo.ConditionUnit set OutService='0' , OutServiceStartDate='', OutServiceEndDate='',OutServiceStartHour='0',OutServiceEndHour='0' WHERE PPID='" + ppid + "' AND UNITCODE='" + unitcode + "' AND DATE='" + sn + "'");
                   filleditoperational(type);
                  
               }

               if (type == "maintenance")
               {
                   editoperationalgrid.AllowUserToDeleteRows = true;
                   string sn = editoperationalgrid.CurrentRow.Cells[2].Value.ToString();

                   DataTable otable = Utilities.GetTable("update dbo.ConditionUnit set Maintenance='0' , MaintenanceStartDate='', MaintenanceEndDate='',MaintenanceStartHour='0',MaintenanceEndHour ='0' WHERE PPID='" + ppid + "' AND UNITCODE='" + unitcode + "' AND DATE='" + sn + "'");
                   filleditoperational(type);

               }
               if (type == "secondfuel")
               {
                   editoperationalgrid.AllowUserToDeleteRows = true;
                   string sn = editoperationalgrid.CurrentRow.Cells[2].Value.ToString();

                   DataTable otable = Utilities.GetTable("update dbo.ConditionUnit set SecondFuel='0' , SecondFuelStartDate='',SecondFuelEndDate='',SecondFuelStartHour='0',SecondFuelEndHour='0' WHERE PPID='" + ppid + "' AND UNITCODE='" + unitcode + "' AND DATE='" + sn + "'");
                   filleditoperational(type);

               }
                
           }
            if (editoperationalgrid.CurrentCell.Value.ToString().Trim() == "Edit")
            {
                if (type == "outservice")
                {
                    editoperationalgrid.AllowUserToDeleteRows = true;
                    string sn = editoperationalgrid.CurrentRow.Cells[2].Value.ToString();
                    int index=editoperationalgrid.CurrentCell.RowIndex;
                    if (editoperationalgrid.Rows[index].Cells[3].Value.ToString() == "True")
                    {
                        DataTable otable = Utilities.GetTable("update dbo.ConditionUnit set OutService='" + editoperationalgrid.Rows[index].Cells[3].Value.ToString() + "' , OutServiceStartDate='" + editoperationalgrid.Rows[index].Cells[4].Value.ToString() + "', OutServiceEndDate='" + editoperationalgrid.Rows[index].Cells[5].Value.ToString() + "',OutServiceStartHour='" + editoperationalgrid.Rows[index].Cells[6].Value.ToString() + "',OutServiceEndHour='" + editoperationalgrid.Rows[index].Cells[7].Value.ToString() + "' WHERE PPID='" + ppid + "' AND UNITCODE='" + unitcode + "' AND DATE='" + sn + "'");


                        filleditoperational(type);
                    }
                    else
                        MessageBox.Show("check outservice!");


                }
                if (type == "maintenance")
                {
                    editoperationalgrid.AllowUserToDeleteRows = true;
                    string sn = editoperationalgrid.CurrentRow.Cells[2].Value.ToString();
                    int index = editoperationalgrid.CurrentCell.RowIndex;
                    if (editoperationalgrid.Rows[index].Cells[3].Value.ToString() == "True")
                    {
                        DataTable otable = Utilities.GetTable("update dbo.ConditionUnit set Maintenance='" + editoperationalgrid.Rows[index].Cells[3].Value.ToString() + "' , MaintenanceStartDate='" + editoperationalgrid.Rows[index].Cells[4].Value.ToString() + "', MaintenanceEndDate='" + editoperationalgrid.Rows[index].Cells[5].Value.ToString() + "',MaintenanceStartHour='" + editoperationalgrid.Rows[index].Cells[6].Value.ToString() + "',MaintenanceEndHour='" + editoperationalgrid.Rows[index].Cells[7].Value.ToString() + "' WHERE PPID='" + ppid + "' AND UNITCODE='" + unitcode + "' AND DATE='" + sn + "'");


                        filleditoperational(type);
                    }
                    else
                        MessageBox.Show("check maintenance!");


                }
                if (type == "secondfuel")
                {
                    editoperationalgrid.AllowUserToDeleteRows = true;
                    string sn = editoperationalgrid.CurrentRow.Cells[2].Value.ToString();
                    int index = editoperationalgrid.CurrentCell.RowIndex;
                    if (editoperationalgrid.Rows[index].Cells[3].Value.ToString() == "True")
                    {
                        DataTable otable = Utilities.GetTable("update dbo.ConditionUnit set SecondFuel='" + editoperationalgrid.Rows[index].Cells[3].Value.ToString() + "' , SecondFuelStartDate='" + editoperationalgrid.Rows[index].Cells[4].Value.ToString() + "', SecondFuelEndDate='" + editoperationalgrid.Rows[index].Cells[5].Value.ToString() + "',SecondFuelStartHour='" + editoperationalgrid.Rows[index].Cells[6].Value.ToString() + "',SecondFuelEndHour='" + editoperationalgrid.Rows[index].Cells[7].Value.ToString() + "' WHERE PPID='" + ppid + "' AND UNITCODE='" + unitcode + "' AND DATE='" + sn + "'");


                        filleditoperational(type);
                    }
                    else
                        MessageBox.Show("check secondfuel!");
               

                }
               

            }

        }

       

       
    }
}
