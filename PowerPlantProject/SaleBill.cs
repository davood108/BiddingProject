﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    public partial class SaleBill : Form
    {
        public SaleBill()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }


        }
        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "" || str.Trim() == null)
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {

            string  maliat = txtmaliat.Text.Trim();
            string takhfif = txttakhfif.Text.Trim();


            string badd = txtbadd.Text.Trim();
            string bcode = txtbcode.Text.Trim();
            string beco = txtbeco.Text.Trim();
            string bname = txtbname.Text.Trim();
            string bostan = txtbostan.Text.Trim();
            string bphone = txtbphone.Text.Trim();
            string bshahr = txtbshahr.Text.Trim();
            string bsabt = txtbsabt.Text.Trim();


            string sadd = txtsadd.Text.Trim();
            string scode = txtscode.Text.Trim();
            string seco = txtseco.Text.Trim();
            string sname = txtsname.Text.Trim();
            string sostan = txtsostan.Text.Trim();
            string sphone = txtsphone.Text.Trim();
            string sshahr = txtsshahr.Text.Trim();
            string ssabt = txtssabt.Text.Trim();


            DataTable dele = Utilities.GetTable("delete from salebill");
            DataTable gg = Utilities.GetTable("insert into salebill (maliat,takhfif,salename,buyname,saleshahr,buyshahr,buyostan,saleostan,buyphone,salephone,buyeco,saleeco,buycode,salecode,buyaddre,saleaddre,salesabt,buysabt)"+
            "values('"+maliat+"','"+takhfif+"',N'"+sname+"',N'"+bname+"',N'"+sshahr+"',N'"+bshahr+"',N'"+bostan+"',N'"+sostan+"','"+bphone+"','"+sphone+"','"+beco+"','"+seco+"','"+bcode+"','"+scode+"',N'"+badd+"',N'"+sadd+"','"+ssabt+"','"+bsabt+"')");


           DialogResult m= folderBrowserDialog1.ShowDialog();
           if (m == DialogResult.OK)
           {
               ExportM002Excel();
           }
        }
       public  void ExportM002Excel()
        {
            DataTable d = Utilities.GetTable("select * from salebill");
            DataTable ctolid = Utilities.GetTable("select value from monthlybilltotal where month='"+datePickerFrom.Text.Substring(0,7).Trim()+"'and code='s03'");
            DataTable camadegi = Utilities.GetTable("select value from monthlybilltotal where month='" + datePickerFrom.Text.Substring(0, 7).Trim() + "'and code='s17'");
            DataTable poweramadegi = Utilities.GetTable("select value from monthlybilltotal where month='" + datePickerFrom.Text.Substring(0, 7).Trim() + "'and code='s16'");
            DataTable cferekans = Utilities.GetTable("select value from monthlybilltotal where month='" + datePickerFrom.Text.Substring(0, 7).Trim() + "'and code='s22'");
            DataTable aractive = Utilities.GetTable("select value from monthlybilltotal where month='" + datePickerFrom.Text.Substring(0, 7).Trim() + "'and code='s24'");
            DataTable eractive = Utilities.GetTable("select value from monthlybilltotal where month='" + datePickerFrom.Text.Substring(0, 7).Trim() + "'and code='s23'");
            DataTable pricetolid = Utilities.GetTable("select value from monthlybilltotal where month='" + datePickerFrom.Text.Substring(0, 7).Trim() + "'and code='s15'");
       
            if (d.Rows.Count > 0 && ctolid.Rows.Count > 0 && camadegi.Rows.Count > 0)
            {


                double maliat = MyDoubleParse(d.Rows[0]["maliat"].ToString().Trim());
                double takhfif = MyDoubleParse(d.Rows[0]["takhfif"].ToString().Trim());
                string badd = d.Rows[0]["buyaddre"].ToString().Trim();
                string bcode = d.Rows[0]["buycode"].ToString().Trim();
                string beco = d.Rows[0]["buyeco"].ToString().Trim();
                string bname = d.Rows[0]["buyname"].ToString().Trim();
                string bostan = d.Rows[0]["buyostan"].ToString().Trim();
                string bphone = d.Rows[0]["buyphone"].ToString().Trim();
                string bshahr = d.Rows[0]["buyshahr"].ToString().Trim();
                string sadd = d.Rows[0]["saleaddre"].ToString().Trim();
                string scode = d.Rows[0]["salecode"].ToString().Trim();
                string seco = d.Rows[0]["saleeco"].ToString().Trim();
                string sname = d.Rows[0]["salename"].ToString().Trim();
                string sostan = d.Rows[0]["saleostan"].ToString().Trim();
                string sphone = d.Rows[0]["salephone"].ToString().Trim();
                string sshahr = d.Rows[0]["saleshahr"].ToString().Trim();
                string ssabt = d.Rows[0]["salesabt"].ToString().Trim();
                string bsabt = d.Rows[0]["buysabt"].ToString().Trim();

                double tolid = MyDoubleParse(ctolid.Rows[0][0].ToString());
                double amadegi = MyDoubleParse(camadegi.Rows[0][0].ToString());
                double powera = MyDoubleParse(poweramadegi.Rows[0][0].ToString());
                double energyr = MyDoubleParse(eractive.Rows[0][0].ToString());
                double amadegir = MyDoubleParse(aractive.Rows[0][0].ToString());
                double ferk = MyDoubleParse(cferekans.Rows[0][0].ToString());
                double pptolid = MyDoubleParse(pricetolid.Rows[0][0].ToString());

                string month = "";
                string date = datePickerFrom.Text.Substring(5, 2).Trim();
                if (date == "01") month = "فروردين";
                if (date == "02") month = "ارديبهشت";
                if (date == "03") month = "خرداد";
                if (date == "04") month = "تير";
                if (date == "05") month = "مرداد";
                if (date == "06") month = "شهريور";
                if (date == "07") month = "مهر";
                if (date == "08") month = "ابان";
                if (date == "09") month = "اذر";
                if (date == "10") month = "دي";
                if (date == "11") month = "بهمن";
                if (date == "12") month = "اسفند";


                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                Excel.Application oExcel = new Excel.Application();

                oExcel.SheetsInNewWorkbook = 1;
                Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
                Excel.Worksheet sheet = null;
                Excel.Range range = null;

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers
                oExcel.DefaultSheetDirection = (int)Excel.Constants.xlRTL;

                int sheetIndex = 1;
                if (sheet != null)
                    sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
                else
                    sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

                sheet.Name = (date);



                for (int i = 1; i <= 60; i++)
                {


                    range = (Excel.Range)sheet.Cells[1, i];
                    range.Value2 = "";
                    range.RowHeight = 17;
                    range.ColumnWidth = 5;
                    range.Font.Size = 11;

                }






                range = (Excel.Range)sheet.Cells[1, 20];
                range.Value2 = datePickerFrom.Text.ToString();
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;




                range = (Excel.Range)sheet.Cells[4, 1];
                Excel.Range range5 = sheet.get_Range(sheet.Cells[4, 25], sheet.Cells[4, 1]);
                range5.Merge(true);
                range5.Value2 = "مشخصات فروشنده";
                range5.Font.Bold = true;
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 12;
                range.RowHeight = 18;
                range.ColumnWidth = 5;
                range.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(204, 255, 255));
                range5.Borders.LineStyle = Excel.Constants.xlSolid;
                range5.Borders.Weight = 2;

               
                range = (Excel.Range)sheet.Cells[5, 1];
                range.Value2 = ":نام شخص حقوقي";
                Excel.Range range12 = sheet.get_Range(sheet.Cells[5, 3], sheet.Cells[5, 1]);
                range12.Merge(true);
                range12.Font.Bold = true;
                range12.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range12.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;


                range = (Excel.Range)sheet.Cells[5, 4];
                range.Value2 = sname;
                Excel.Range range13 = sheet.get_Range(sheet.Cells[5, 9], sheet.Cells[5, 4]);
                range13.Merge(true);
                range13.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range13.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;




                range = (Excel.Range)sheet.Cells[5, 10];
                range.Value2 = " : شماره اقتصادي ";
                range.Font.Bold = true;
                Excel.Range range14 = sheet.get_Range(sheet.Cells[5, 12], sheet.Cells[5, 10]);
                range14.Merge(true);
                range14.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range14.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;




                range = (Excel.Range)sheet.Cells[5, 13];
                range.Value2 = seco;
                range.NumberFormat = "#";
                Excel.Range range15 = sheet.get_Range(sheet.Cells[5, 17], sheet.Cells[5, 13]);
                range15.Merge(true);
                range15.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range15.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[5, 18];
                range.Value2 = ": شماره ثبت";
                range.Font.Bold = true;
                Excel.Range range16 = sheet.get_Range(sheet.Cells[5, 19], sheet.Cells[5, 18]);
                range16.Merge(true);
                range16.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                range16.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[5, 20];
                range.Value2 = ssabt;
                range.NumberFormat = "#";
                Excel.Range range17 = sheet.get_Range(sheet.Cells[5, 23], sheet.Cells[5, 20]);
                range17.Merge(true);
                range17.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                range17.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;


                ////
                range = (Excel.Range)sheet.Cells[6, 1];
                range.Value2 = ":نشاني كامل ";
                Excel.Range range18 = sheet.get_Range(sheet.Cells[6, 2], sheet.Cells[6, 1]);
                range18.Merge(true);
                range18.Font.Bold = true;
                range18.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range18.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;


                range = (Excel.Range)sheet.Cells[6, 3];
                range.Value2 = "";
                Excel.Range range19 = sheet.get_Range(sheet.Cells[6, 3], sheet.Cells[6, 3]);
                range19.Merge(true);
                range19.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range19.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;




                range = (Excel.Range)sheet.Cells[6, 4];
                range.Value2 = " :استان ";
                range.Font.Bold = true;
                Excel.Range range20 = sheet.get_Range(sheet.Cells[6, 5], sheet.Cells[6, 4]);
                range20.Merge(true);
                range20.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range20.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;




                range = (Excel.Range)sheet.Cells[6, 6];
                range.Value2 = sostan;
                Excel.Range range22 = sheet.get_Range(sheet.Cells[6, 9], sheet.Cells[6, 6]);
                range22.Merge(true);
                range22.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                range22.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[6, 10];
                range.Value2 = ": كدپستي 10 رقمي";
                range.Font.Bold = true;
                Excel.Range range21 = sheet.get_Range(sheet.Cells[6, 12], sheet.Cells[6, 10]);
                range21.Merge(true);
                range21.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range21.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[6, 13];
                range.Value2 = scode;
                range.NumberFormat = "#";
                Excel.Range range25 = sheet.get_Range(sheet.Cells[6, 17], sheet.Cells[6, 13]);
                range25.Merge(true);
                range25.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range25.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[6, 18];
                range.Value2 = ":شهر";
                range.Font.Bold = true;
                Excel.Range range23 = sheet.get_Range(sheet.Cells[6, 18], sheet.Cells[6, 18]);
                range23.Merge(true);
                range23.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range23.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[6, 19];
                range.Value2 = sshahr;
                Excel.Range range26 = sheet.get_Range(sheet.Cells[6, 20], sheet.Cells[6, 19]);
                range26.Merge(true);
                range26.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range26.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;





                range = (Excel.Range)sheet.Cells[7, 1];
                range.Value2 = sadd;
                Excel.Range range27 = sheet.get_Range(sheet.Cells[7, 17], sheet.Cells[7, 1]);
                range27.Merge(true);
                range27.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                range27.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;




                range = (Excel.Range)sheet.Cells[7, 18];
                range.Value2 = ":شماره تلفن";
                range.Font.Bold = true;
                Excel.Range range28 = sheet.get_Range(sheet.Cells[7, 19], sheet.Cells[7, 18]);
                range28.Merge(true);
                range28.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range28.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[7, 20];
                range.Value2 = sphone;
                Excel.Range range29 = sheet.get_Range(sheet.Cells[7, 22], sheet.Cells[7, 20]);
                range29.Merge(true);
                range29.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range29.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;


                //     ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                range = (Excel.Range)sheet.Cells[8, 1];
                Excel.Range range31 = sheet.get_Range(sheet.Cells[8, 25], sheet.Cells[8, 1]);
                range31.Merge(true);
                range31.Value2 = "مشخصات خريدار";
                range31.Font.Bold = true;
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 12;
                range.RowHeight = 18;
                range.ColumnWidth = 5;
                range.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(204, 255, 255));
                range31.Borders.LineStyle = Excel.Constants.xlSolid;
                range31.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[9, 1];
                range.Value2 = ":نام شخص حقوقي";
                Excel.Range range32 = sheet.get_Range(sheet.Cells[9, 3], sheet.Cells[9, 1]);
                range32.Merge(true);
                range32.Font.Bold = true;
                range32.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range32.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;


                range = (Excel.Range)sheet.Cells[9, 4];
                range.Value2 = bname;
                Excel.Range range33 = sheet.get_Range(sheet.Cells[9, 9], sheet.Cells[9, 4]);
                range33.Merge(true);
                range33.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range33.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;




                range = (Excel.Range)sheet.Cells[9, 10];
                range.Value2 = " : شماره اقتصادي ";
                range.Font.Bold = true;
                Excel.Range range34 = sheet.get_Range(sheet.Cells[9, 12], sheet.Cells[9, 10]);
                range34.Merge(true);
                range34.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range34.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;




                range = (Excel.Range)sheet.Cells[9, 13];
                range.Value2 = beco;
                range.NumberFormat = "#";
                Excel.Range range35 = sheet.get_Range(sheet.Cells[9, 17], sheet.Cells[9, 13]);
                range35.Merge(true);
                range35.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range35.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[9, 18];
                range.Value2 = ": شماره ثبت";
                range.Font.Bold = true;
                Excel.Range range36 = sheet.get_Range(sheet.Cells[9, 19], sheet.Cells[9, 18]);
                range36.Merge(true);
                range36.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                range36.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[9, 20];
                range.Value2 = bsabt;
                range.NumberFormat = "#";
                Excel.Range range37 = sheet.get_Range(sheet.Cells[9, 23], sheet.Cells[9, 20]);
                range37.Merge(true);
                range37.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                range37.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;


                ////
                range = (Excel.Range)sheet.Cells[10, 1];
                range.Value2 = ":نشاني كامل ";
                Excel.Range range38 = sheet.get_Range(sheet.Cells[10, 2], sheet.Cells[10, 1]);
                range38.Merge(true);
                range38.Font.Bold = true;
                range38.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range38.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;


                range = (Excel.Range)sheet.Cells[10, 3];
                range.Value2 = "";
                Excel.Range range39 = sheet.get_Range(sheet.Cells[10, 3], sheet.Cells[10, 3]);
                range39.Merge(true);
                range39.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range39.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;




                range = (Excel.Range)sheet.Cells[10, 4];
                range.Value2 = " :استان ";
                range.Font.Bold = true;
                Excel.Range range40 = sheet.get_Range(sheet.Cells[10, 5], sheet.Cells[10, 4]);
                range40.Merge(true);
                range40.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range40.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;




                range = (Excel.Range)sheet.Cells[10, 6];
                range.Value2 = bostan;
                Excel.Range range41 = sheet.get_Range(sheet.Cells[10, 9], sheet.Cells[10, 6]);
                range41.Merge(true);
                range41.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                range41.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[10, 10];
                range.Value2 = ": كدپستي 10 رقمي";
                range.Font.Bold = true;
                Excel.Range range42 = sheet.get_Range(sheet.Cells[10, 12], sheet.Cells[10, 10]);
                range42.Merge(true);
                range42.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range42.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[10, 13];
                range.Value2 = bcode;
                range.NumberFormat = "#";
                Excel.Range range43 = sheet.get_Range(sheet.Cells[10, 17], sheet.Cells[10, 13]);
                range43.Merge(true);
                range43.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range43.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[10, 18];
                range.Value2 = ":شهر";
                range.Font.Bold = true;
                Excel.Range range44 = sheet.get_Range(sheet.Cells[10, 18], sheet.Cells[10, 18]);
                range44.Merge(true);
                range44.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range44.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[10, 19];
                range.Value2 = bshahr;
                Excel.Range range45 = sheet.get_Range(sheet.Cells[10, 20], sheet.Cells[10, 19]);
                range45.Merge(true);
                range45.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range45.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;





                range = (Excel.Range)sheet.Cells[11, 1];
                range.Value2 = badd;
                Excel.Range range46 = sheet.get_Range(sheet.Cells[11, 17], sheet.Cells[11, 1]);
                range46.Merge(true);
                range46.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                range46.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;




                range = (Excel.Range)sheet.Cells[11, 18];
                range.Value2 = ":شماره تلفن";
                range.Font.Bold = true;
                Excel.Range range47 = sheet.get_Range(sheet.Cells[11, 19], sheet.Cells[11, 18]);
                range47.Merge(true);
                range47.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range47.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[11, 20];
                range.Value2 = bphone;
                Excel.Range range222 = sheet.get_Range(sheet.Cells[11, 22], sheet.Cells[11, 20]);
                range222.Merge(true);
                range222.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range222.Font.Size = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 5;

                ///////////////////////////////////////

                range = (Excel.Range)sheet.Cells[12, 1];
                range.Value2 = "مشخصات كالا يا خدمات مورد معامله";
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                Excel.Range range30 = sheet.get_Range(sheet.Cells[12, 25], sheet.Cells[12, 1]);
                range30.Merge(true);
                range30.Font.Size = 12;
                range30.Font.Bold = true;
                range.RowHeight = 18;
                range.ColumnWidth = 5;
                range.Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(204, 255, 255));



                ///////////////////////////////////////////////////////////////////////////////////
                ////
                range = (Excel.Range)sheet.Cells[13, 1];
                range.Value2 = 1;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Borders.Weight = 2;
                Excel.Range range53 = sheet.get_Range(sheet.Cells[13, 1], sheet.Cells[13, 1]);
                range53.Merge(true);
                range53.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range53.Font.Size = 11;
                range53.RowHeight = 18;
                range53.ColumnWidth = 3;

                range = (Excel.Range)sheet.Cells[14, 1];
                range.Value2 = "   رديف  ";
                range.Font.Bold = true;
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 11;
                range.RowHeight = 30;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Borders.Weight = 2;
                range.ColumnWidth = 3;

                range = (Excel.Range)sheet.Cells[15, 1];
                range.Value2 = "   1  ";
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 11;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Borders.Weight = 2;

                range = (Excel.Range)sheet.Cells[16, 1];
                range.Value2 = "   2  ";
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 11;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Borders.Weight = 2;

                range = (Excel.Range)sheet.Cells[17, 1];
                range.Value2 = "  3   ";
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 11;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[18, 1];
                range.Value2 = "جمع كل";
                range.Font.Bold = true;
                Excel.Range range80 = sheet.get_Range(sheet.Cells[18, 10], sheet.Cells[18, 1]);
                range80.Merge(true);
                range80.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range80.Font.Size = 11;
                range80.RowHeight = 18;
                range80.ColumnWidth = 3;
                range80.Borders.LineStyle = Excel.Constants.xlSolid;
                range80.Borders.Weight = 1;




                range = (Excel.Range)sheet.Cells[19, 1];
                range.Value2 = "شرايط و نحوه فروش : غيرنقدي";
                Excel.Range rangeaa = sheet.get_Range(sheet.Cells[19, 10], sheet.Cells[19, 1]);
                rangeaa.Merge(true);
                rangeaa.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeaa.Font.Size = 11;
                rangeaa.RowHeight = 18;
                rangeaa.ColumnWidth = 3;
                rangeaa.Borders.LineStyle = Excel.Constants.xlSolid;
                rangeaa.Borders.Weight = 2;


                string xxx = " توضيحات : صورتحساب فروش برق " + month + " ماه ";
                range = (Excel.Range)sheet.Cells[20, 1];
                range.Value2 = xxx;
                Excel.Range rangecc = sheet.get_Range(sheet.Cells[20, 10], sheet.Cells[20, 1]);
                rangecc.Merge(true);
                rangecc.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangecc.Font.Size = 11;
                rangecc.RowHeight = 18;
                rangecc.ColumnWidth = 3;
                rangecc.Borders.LineStyle = Excel.Constants.xlSolid;
                rangecc.Borders.Weight = 2;




                range = (Excel.Range)sheet.Cells[21, 1];
                range.Value2 = ": مهر وامضا فروشنده ";
                Excel.Range rangevv = sheet.get_Range(sheet.Cells[21, 10], sheet.Cells[21, 1]);
                rangevv.Merge(true);
                rangevv.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangevv.Font.Size = 11;
                rangevv.RowHeight = 18;
                rangevv.ColumnWidth = 3;


                range = (Excel.Range)sheet.Cells[21, 11];
                range.Value2 = " :مهر وامضا خريدار ";
                Excel.Range rangeae = sheet.get_Range(sheet.Cells[21, 20], sheet.Cells[21, 11]);
                rangeae.Merge(true);
                rangeae.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeae.Font.Size = 11;
                rangeae.RowHeight = 18;
                rangeae.ColumnWidth = 3;





                ////

                range = (Excel.Range)sheet.Cells[13, 2];
                range.Value2 = 2;
                Excel.Range range54 = sheet.get_Range(sheet.Cells[13, 2], sheet.Cells[13, 2]);
                range54.Merge(true);
                range54.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range54.Font.Size = 11;
                range54.RowHeight = 18;
                range54.ColumnWidth = 3;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[14, 2];
                range.Value2 = "   كد كالا   ";
                range.Font.Bold = true;
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 11;
                range.RowHeight = 30;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Borders.Weight = 2;

                range = (Excel.Range)sheet.Cells[15, 2];
                range.Value2 = "   -   ";
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 11;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[16, 2];
                range.Value2 = "   -   ";
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 11;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Borders.Weight = 2;

                range = (Excel.Range)sheet.Cells[17, 2];
                range.Value2 = "   -   ";
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 11;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Borders.Weight = 2;



                ////


                range = (Excel.Range)sheet.Cells[13, 3];
                range.Value2 = 3;
                range.RowHeight = 18;
                range.ColumnWidth = 4;
                Excel.Range ranges = sheet.get_Range(sheet.Cells[13, 5], sheet.Cells[13, 3]);
                ranges.Merge(true);
                ranges.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                ranges.Font.Size = 11;
                ranges.Borders.LineStyle = Excel.Constants.xlSolid;
                ranges.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[14, 3];
                range.Value2 = "شرح كالا يا خدمات";
                range.Font.Bold = true;
                Excel.Range ranget = sheet.get_Range(sheet.Cells[14, 5], sheet.Cells[14, 3]);
                ranget.Merge(true);
                ranget.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                ranget.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                ranget.Font.Size = 11;
                ranget.RowHeight = 30;
                ranget.Borders.LineStyle = Excel.Constants.xlSolid;
                ranget.Borders.Weight = 2;
                ranget.ColumnWidth = 4;


                range = (Excel.Range)sheet.Cells[15, 3];
                range.Value2 = "فروش برق";
                Excel.Range rangeu = sheet.get_Range(sheet.Cells[15, 5], sheet.Cells[15, 3]);
                rangeu.Merge(true);
                rangeu.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeu.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeu.Font.Size = 11;
                rangeu.Borders.LineStyle = Excel.Constants.xlSolid;
                rangeu.Borders.Weight = 2;
                rangeu.ColumnWidth = 4;


                range = (Excel.Range)sheet.Cells[16, 3];
                range.Value2 = "امادگي";
                Excel.Range rangev = sheet.get_Range(sheet.Cells[16, 5], sheet.Cells[16, 3]);
                rangev.Merge(true);
                rangev.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangev.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangev.Font.Size = 11;
                rangev.WrapText = true;
                rangev.Borders.LineStyle = Excel.Constants.xlSolid;
                rangev.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[17, 3];
                range.Value2 = "خدمات جانبي";
                Excel.Range rangex = sheet.get_Range(sheet.Cells[17, 5], sheet.Cells[17, 3]);
                rangex.Merge(true);
                rangex.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangex.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangex.Font.Size = 11;
                rangex.WrapText = true;
                rangex.Borders.LineStyle = Excel.Constants.xlSolid;
                rangex.Borders.Weight = 2;



                ////
                range = (Excel.Range)sheet.Cells[13, 6];
                range.Value2 = 4;
                range.RowHeight = 18;
                range.ColumnWidth = 8;
                Excel.Range range51 = sheet.get_Range(sheet.Cells[13, 6], sheet.Cells[13, 6]);
                range51.Merge(true);
                range51.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range51.Font.Size = 11;
                range51.Borders.LineStyle = Excel.Constants.xlSolid;
                range51.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[14, 6];
                range.Value2 = "مقدار تعداد";
                range.Font.Bold = true;
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 11;
                range.WrapText = true;
                range.RowHeight = 30;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[15, 6];
                range.Value2 = tolid;
                range.NumberFormat = "#";
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 11;
                range.WrapText = true;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[16, 6];
                range.Value2 = powera;
                range.NumberFormat = "#";
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 11;
                range.WrapText = true;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[17, 6];
                range.Value2 = "    ";
                range.NumberFormat = "#";
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Font.Size = 11;
                range.WrapText = true;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Borders.Weight = 2;


                ////

                range = (Excel.Range)sheet.Cells[13, 7];
                range.Value2 = 5;
                range.RowHeight = 18;
                range.ColumnWidth = 5;
                Excel.Range range52 = sheet.get_Range(sheet.Cells[13, 8], sheet.Cells[13, 7]);
                range52.Merge(true);
                range52.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range52.Font.Size = 11;
                range52.Borders.LineStyle = Excel.Constants.xlSolid;
                range52.Borders.Weight = 2;
                range52.RowHeight = 18;
                range52.ColumnWidth = 5;

                range = (Excel.Range)sheet.Cells[14, 7];
                range.Value2 = "واحد اندازه گيري";
                range.Font.Bold = true;
                Excel.Range range56 = sheet.get_Range(sheet.Cells[14, 8], sheet.Cells[14, 7]);
                range56.Merge(true);
                range56.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range56.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range56.Font.Size = 11;
                range56.WrapText = true;
                range56.RowHeight = 30;
                range56.Borders.LineStyle = Excel.Constants.xlSolid;
                range56.Borders.Weight = 2;
                range56.RowHeight = 18;
                range56.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[15, 7];
                range.Value2 = "مگاوات ساعت";
                Excel.Range range70 = sheet.get_Range(sheet.Cells[15, 8], sheet.Cells[15, 7]);
                range70.Merge(true);
                range70.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range70.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range70.Font.Size = 11;
                range70.WrapText = true;
                range70.Borders.LineStyle = Excel.Constants.xlSolid;
                range70.Borders.Weight = 2;
                range70.ColumnWidth = 5;



                range = (Excel.Range)sheet.Cells[16, 7];
                range.Value2 = "مگاوات ساعت";
                Excel.Range rangea = sheet.get_Range(sheet.Cells[16, 8], sheet.Cells[16, 7]);
                rangea.Merge(true);
                rangea.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangea.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangea.Font.Size = 11;
                rangea.WrapText = true;
                rangea.Borders.LineStyle = Excel.Constants.xlSolid;
                rangea.Borders.Weight = 2;




                range = (Excel.Range)sheet.Cells[17, 7];
                range.Value2 = "مگاوات ساعت";
                Excel.Range rangeb = sheet.get_Range(sheet.Cells[17, 8], sheet.Cells[17, 7]);
                rangeb.Merge(true);
                rangeb.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeb.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeb.Font.Size = 11;
                rangeb.WrapText = true;
                rangeb.Borders.LineStyle = Excel.Constants.xlSolid;
                rangeb.Borders.Weight = 2;



                //


                range = (Excel.Range)sheet.Cells[13, 9];
                range.Value2 = 6;
                range.RowHeight = 18;
                range.ColumnWidth = 4;
                Excel.Range range58 = sheet.get_Range(sheet.Cells[13, 10], sheet.Cells[13, 9]);
                range58.Merge(true);
                range58.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range58.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range58.Font.Size = 11;
                range58.Borders.LineStyle = Excel.Constants.xlSolid;
                range58.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[14, 9];
                range.Value2 = "مبلغ واحد-ريال";
                range.Font.Bold = true;
                Excel.Range range59 = sheet.get_Range(sheet.Cells[14, 10], sheet.Cells[14, 9]);
                range59.Merge(true);
                range59.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range59.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range59.Font.Size = 11;
                range59.RowHeight = 30;
                range59.Borders.LineStyle = Excel.Constants.xlSolid;
                range59.Borders.Weight = 2;
                range59.WrapText = true;


                range = (Excel.Range)sheet.Cells[15, 9];
                range.Value2 = "متفاوت";
                Excel.Range range71 = sheet.get_Range(sheet.Cells[15, 10], sheet.Cells[15, 9]);
                range71.Merge(true);
                range71.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range71.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range71.Font.Size = 11;
                range71.Borders.LineStyle = Excel.Constants.xlSolid;
                range71.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[16, 9];
                range.Value2 = "متفاوت";
                Excel.Range rangec = sheet.get_Range(sheet.Cells[16, 10], sheet.Cells[16, 9]);
                rangec.Merge(true);
                rangec.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangec.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangec.Font.Size = 11;
                rangec.Borders.LineStyle = Excel.Constants.xlSolid;
                rangec.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[17, 9];
                range.Value2 = "متفاوت";
                Excel.Range ranged = sheet.get_Range(sheet.Cells[17, 10], sheet.Cells[17, 9]);
                ranged.Merge(true);
                ranged.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                ranged.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                ranged.Font.Size = 11;
                ranged.Borders.LineStyle = Excel.Constants.xlSolid;
                ranged.Borders.Weight = 2;




                //

                range = (Excel.Range)sheet.Cells[13, 11];
                range.Value2 = 7;
                range.RowHeight = 18;
                range.ColumnWidth = 5;
                Excel.Range range60 = sheet.get_Range(sheet.Cells[13, 13], sheet.Cells[13, 11]);
                range60.Merge(true);
                range60.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range60.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range60.Font.Size = 11;
                range60.Borders.LineStyle = Excel.Constants.xlSolid;
                range60.Borders.Weight = 2;


                double x7 =  pptolid;
                double z1x7 = amadegi;
                double z2x7 = amadegir + ferk + energyr;

                range = (Excel.Range)sheet.Cells[14, 11];
                range.Value2 = "مبلغ كل-ريال";
                range.Font.Bold = true;
                Excel.Range range61 = sheet.get_Range(sheet.Cells[14, 13], sheet.Cells[14, 11]);
                range61.Merge(true);
                range61.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range61.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range61.Font.Size = 11;
                range61.RowHeight = 30;
                range61.ColumnWidth = 5;
                range61.Borders.LineStyle = Excel.Constants.xlSolid;
                range61.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[15, 11];
                range.Value2 = x7;
                range.NumberFormat = "#";
                Excel.Range range72 = sheet.get_Range(sheet.Cells[15, 13], sheet.Cells[15, 11]);
                range72.Merge(true);
                range72.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range72.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range72.Font.Size = 11;
                range72.RowHeight = 18;
                range72.ColumnWidth = 5;
                range72.Borders.LineStyle = Excel.Constants.xlSolid;
                range72.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[16, 11];
                range.Value2 = z1x7;
                range.NumberFormat = "#";
                Excel.Range rangef = sheet.get_Range(sheet.Cells[16, 13], sheet.Cells[16, 11]);
                rangef.Merge(true);
                rangef.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangef.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangef.Font.Size = 11;
                rangef.RowHeight = 18;
                rangef.ColumnWidth = 5;
                rangef.Borders.LineStyle = Excel.Constants.xlSolid;
                rangef.Borders.Weight = 2;




                range = (Excel.Range)sheet.Cells[17, 11];
                range.Value2 = z2x7;
                range.NumberFormat = "#";
                Excel.Range rangeg = sheet.get_Range(sheet.Cells[17, 13], sheet.Cells[17, 11]);
                rangeg.Merge(true);
                rangeg.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeg.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeg.Font.Size = 11;
                rangeg.RowHeight = 18;
                rangeg.ColumnWidth = 5;
                rangeg.Borders.LineStyle = Excel.Constants.xlSolid;
                rangeg.Borders.Weight = 2;




                range = (Excel.Range)sheet.Cells[18, 11];
                range.Value2 = z1x7+x7+z2x7;
                range.NumberFormat = "#";
                Excel.Range rangegd = sheet.get_Range(sheet.Cells[18, 13], sheet.Cells[18, 11]);
                rangegd.Merge(true);
                rangegd.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangegd.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangegd.Font.Size = 11;
                rangegd.RowHeight = 18;
                rangegd.ColumnWidth = 5;
                rangegd.Borders.LineStyle = Excel.Constants.xlSolid;
                rangegd.Borders.Weight = 2;



                //
                range = (Excel.Range)sheet.Cells[13, 14];
                range.Value2 = 8;
                range.RowHeight = 18;
                range.ColumnWidth = 4;
                Excel.Range range62 = sheet.get_Range(sheet.Cells[13, 16], sheet.Cells[13, 14]);
                range62.Merge(true);
                range62.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range62.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range62.Font.Size = 11;
                range62.Borders.LineStyle = Excel.Constants.xlSolid;
                range62.Borders.Weight = 2;


                double x8 = (pptolid) * (takhfif);
                double z1x8 = (amadegi) * (takhfif);
                double z2x8 = (amadegir+energyr+ferk) * (takhfif);

                range = (Excel.Range)sheet.Cells[14, 14];
                range.Value2 = "مبلغ تخفيف-ريال";
                range.Font.Bold = true;
                Excel.Range range63 = sheet.get_Range(sheet.Cells[14, 16], sheet.Cells[14, 14]);
                range63.Merge(true);
                range63.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range63.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range63.Font.Size = 11;
                range63.RowHeight = 30;
                range63.ColumnWidth = 5;
                range63.Borders.LineStyle = Excel.Constants.xlSolid;
                range63.Borders.Weight = 2;




                range = (Excel.Range)sheet.Cells[15, 14];
                range.Value2 = x8;
                range.NumberFormat = "#";
                Excel.Range range73 = sheet.get_Range(sheet.Cells[15, 16], sheet.Cells[15, 14]);
                range73.Merge(true);
                range73.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range73.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range73.Font.Size = 11;
                range73.RowHeight = 18;
                range73.ColumnWidth = 5;
                range73.Borders.LineStyle = Excel.Constants.xlSolid;
                range73.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[16, 14];
                range.Value2 = z1x8;
                range.NumberFormat = "#";
                Excel.Range rangeh = sheet.get_Range(sheet.Cells[16, 16], sheet.Cells[16, 14]);
                rangeh.Merge(true);
                rangeh.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeh.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeh.Font.Size = 11;
                rangeh.RowHeight = 18;
                rangeh.ColumnWidth = 5;
                rangeh.Borders.LineStyle = Excel.Constants.xlSolid;
                rangeh.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[17, 14];
                range.Value2 = z2x8;
                range.NumberFormat = "#";
                Excel.Range rangek = sheet.get_Range(sheet.Cells[17, 16], sheet.Cells[17, 14]);
                rangek.Merge(true);
                rangek.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangek.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangek.Font.Size = 11;
                rangek.RowHeight = 18;
                rangek.ColumnWidth = 5;
                rangek.Borders.LineStyle = Excel.Constants.xlSolid;
                rangek.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[18, 14];
                range.Value2 = z2x8 + z1x8 + x8;
                range.NumberFormat = "#";
                Excel.Range rangekd = sheet.get_Range(sheet.Cells[18, 16], sheet.Cells[18, 14]);
                rangekd.Merge(true);
                rangekd.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangekd.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangekd.Font.Size = 11;
                rangekd.RowHeight = 18;
                rangekd.ColumnWidth = 5;
                rangekd.Borders.LineStyle = Excel.Constants.xlSolid;
                rangekd.Borders.Weight = 2;

                //

                //
                range = (Excel.Range)sheet.Cells[13, 17];
                range.Value2 = 9;
                range.RowHeight = 18;
                range.ColumnWidth = 5;
                Excel.Range range64 = sheet.get_Range(sheet.Cells[13, 19], sheet.Cells[13, 17]);
                range64.Merge(true);
                range64.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range64.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range64.Font.Size = 11;
                range64.Borders.LineStyle = Excel.Constants.xlSolid;
                range64.Borders.Weight = 2;



                double x9 = x7 + x8;
                double z1x9 = z1x7 + z1x8;
                double z2x9 = z2x7 + z2x8;

                range = (Excel.Range)sheet.Cells[14, 17];
                range.Value2 = "مبلغ كل پس از تخفيف-ريال";
                range.Font.Bold = true;
                Excel.Range range65 = sheet.get_Range(sheet.Cells[14, 19], sheet.Cells[14, 17]);
                range65.Merge(true);
                range65.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range65.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range65.Font.Size = 11;
                range65.RowHeight = 30;
                range65.ColumnWidth = 6;
                range65.Borders.LineStyle = Excel.Constants.xlSolid;
                range65.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[15, 17];
                range.Value2 = x9;
                range.NumberFormat = "#";
                Excel.Range range74 = sheet.get_Range(sheet.Cells[15, 19], sheet.Cells[15, 17]);
                range74.Merge(true);
                range74.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range74.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range74.Font.Size = 11;
                range74.RowHeight = 18;
                range74.ColumnWidth = 6;
                range74.Borders.LineStyle = Excel.Constants.xlSolid;
                range74.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[16, 17];
                range.Value2 = z1x9;
                range.NumberFormat = "#";
                Excel.Range rangel = sheet.get_Range(sheet.Cells[16, 19], sheet.Cells[16, 17]);
                rangel.Merge(true);
                rangel.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangel.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangel.Font.Size = 11;
                rangel.RowHeight = 18;
                rangel.ColumnWidth = 6;
                rangel.Borders.LineStyle = Excel.Constants.xlSolid;
                rangel.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[17, 17];
                range.Value2 = z2x9;
                range.NumberFormat = "#";
                Excel.Range rangem = sheet.get_Range(sheet.Cells[17, 19], sheet.Cells[17, 17]);
                rangem.Merge(true);
                rangem.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangem.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangem.Font.Size = 11;
                rangem.RowHeight = 18;
                rangem.ColumnWidth = 6;
                rangem.Borders.LineStyle = Excel.Constants.xlSolid;
                rangem.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[18, 17];
                range.Value2 = x9 + z1x9 + z2x9;
                range.NumberFormat = "#";
                Excel.Range rangemk = sheet.get_Range(sheet.Cells[18, 19], sheet.Cells[18, 17]);
                rangemk.Merge(true);
                rangemk.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangemk.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangemk.Font.Size = 11;
                rangemk.RowHeight = 18;
                rangemk.ColumnWidth = 6;
                rangemk.Borders.LineStyle = Excel.Constants.xlSolid;
                rangemk.Borders.Weight = 2;

                //
                //
                range = (Excel.Range)sheet.Cells[13, 20];
                range.Value2 = 10;
                range.RowHeight = 18;
                range.ColumnWidth = 6;
                Excel.Range range66 = sheet.get_Range(sheet.Cells[13, 22], sheet.Cells[13, 20]);
                range66.Merge(true);
                range66.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range66.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range66.Font.Size = 11;
                range66.Borders.LineStyle = Excel.Constants.xlSolid;
                range66.Borders.Weight = 2;


                double x10 = x9 * maliat;
                double z1x10 = z1x9 * maliat;
                double z2x10 = z2x9 * maliat;



                range = (Excel.Range)sheet.Cells[14, 20];
                range.Value2 = "جمع ماليات وعوارض-ريال";
                range.Font.Bold = true;
                Excel.Range range67 = sheet.get_Range(sheet.Cells[14, 22], sheet.Cells[14, 20]);
                range67.Merge(true);
                range67.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range67.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range67.Font.Size = 11;
                range67.RowHeight = 30;
                range67.ColumnWidth = 6;
                range67.Borders.LineStyle = Excel.Constants.xlSolid;
                range67.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[15, 20];
                range.Value2 = x10;
                range.NumberFormat = "#";
                Excel.Range range75 = sheet.get_Range(sheet.Cells[15, 22], sheet.Cells[15, 20]);
                range75.Merge(true);
                range75.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range75.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range75.Font.Size = 11;
                range75.RowHeight = 18;
                range75.ColumnWidth = 6;
                range75.Borders.LineStyle = Excel.Constants.xlSolid;
                range75.Borders.Weight = 2;

                range = (Excel.Range)sheet.Cells[16, 20];
                range.Value2 = z1x10;
                range.NumberFormat = "#";
                Excel.Range rangen = sheet.get_Range(sheet.Cells[16, 22], sheet.Cells[16, 20]);
                rangen.Merge(true);
                rangen.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangen.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangen.Font.Size = 11;
                rangen.RowHeight = 18;
                rangen.ColumnWidth = 6;
                rangen.Borders.LineStyle = Excel.Constants.xlSolid;
                rangen.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[17, 20];
                range.Value2 = z2x10;
                range.NumberFormat = "#";
                Excel.Range rangeo = sheet.get_Range(sheet.Cells[17, 22], sheet.Cells[17, 20]);
                rangeo.Merge(true);
                rangeo.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeo.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeo.Font.Size = 11;
                rangeo.RowHeight = 18;
                rangeo.ColumnWidth = 6;
                rangeo.Borders.LineStyle = Excel.Constants.xlSolid;
                rangeo.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[18, 20];
                range.Value2 = x10 + z2x10 + z1x10;
                range.NumberFormat = "#";
                Excel.Range rangeod = sheet.get_Range(sheet.Cells[18, 22], sheet.Cells[18, 20]);
                rangeod.Merge(true);
                rangeod.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeod.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeod.Font.Size = 11;
                rangeod.RowHeight = 18;
                rangeod.ColumnWidth = 6;
                rangeod.Borders.LineStyle = Excel.Constants.xlSolid;
                rangeod.Borders.Weight = 2;

                //

                //
                range = (Excel.Range)sheet.Cells[13, 23];
                range.Value2 = 11;
                range.RowHeight = 18;
                range.ColumnWidth = 6;
                Excel.Range range68 = sheet.get_Range(sheet.Cells[13, 25], sheet.Cells[13, 23]);
                range68.Merge(true);
                range68.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range68.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range68.Font.Size = 11;
                range68.Borders.LineStyle = Excel.Constants.xlSolid;
                range68.Borders.Weight = 2;



                double x11 = x9 + x10;
                double z1x11 = z1x9 + z1x10;
                double z2x11 = z2x9 + z2x10;



                range = (Excel.Range)sheet.Cells[14, 23];
                range.Value2 = "جمع مبلغ كل بعلاوه جمع ماليات وعوارض-ريال";
                range.Font.Bold = true;
                Excel.Range range69 = sheet.get_Range(sheet.Cells[14, 25], sheet.Cells[14, 23]);
                range69.Merge(true);
                range69.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range69.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range69.Font.Size = 11;
                range69.RowHeight = 30;
                range69.ColumnWidth = 6;
                range69.Borders.LineStyle = Excel.Constants.xlSolid;
                range69.Borders.Weight = 2;
                range69.WrapText = true;


                range = (Excel.Range)sheet.Cells[15, 23];
                range.Value2 = x11;
                range.NumberFormat = "#";
                Excel.Range range76 = sheet.get_Range(sheet.Cells[15, 25], sheet.Cells[15, 23]);
                range76.Merge(true);
                range76.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range76.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range76.Font.Size = 11;
                range76.RowHeight = 18;
                range76.ColumnWidth = 6;
                range76.Borders.LineStyle = Excel.Constants.xlSolid;
                range76.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[16, 23];
                range.Value2 = z1x11;
                range.NumberFormat = "#";
                Excel.Range rangep = sheet.get_Range(sheet.Cells[16, 25], sheet.Cells[16, 23]);
                rangep.Merge(true);
                rangep.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangep.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangep.Font.Size = 11;
                rangep.RowHeight = 18;
                rangep.ColumnWidth = 6;
                rangep.Borders.LineStyle = Excel.Constants.xlSolid;
                rangep.Borders.Weight = 2;


                range = (Excel.Range)sheet.Cells[17, 23];
                range.Value2 = z2x11;
                range.NumberFormat = "#";
                Excel.Range rangeq = sheet.get_Range(sheet.Cells[17, 25], sheet.Cells[17, 23]);
                rangeq.Merge(true);
                rangeq.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeq.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeq.Font.Size = 11;
                rangeq.RowHeight = 18;
                rangeq.ColumnWidth = 6;
                rangeq.Borders.LineStyle = Excel.Constants.xlSolid;
                rangeq.Borders.Weight = 2;



                range = (Excel.Range)sheet.Cells[18, 23];
                range.Value2 = x11 + z2x11 + z1x11;
                range.NumberFormat = "#";
                Excel.Range rangeqd = sheet.get_Range(sheet.Cells[18, 25], sheet.Cells[18, 23]);
                rangeqd.Merge(true);
                rangeqd.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeqd.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                rangeqd.Font.Size = 11;
                rangeqd.RowHeight = 18;
                rangeqd.ColumnWidth = 6;
                rangeqd.Borders.LineStyle = Excel.Constants.xlSolid;
                rangeqd.Borders.Weight = 2;

                //     



                /////////////////////////////////////////////////


                //sheet = ((Excel.Worksheet)WB.Worksheets["sheet1"]);

                range = (Excel.Range)sheet.Cells[1, 1];
                range = range.EntireColumn;
                range.AutoFit();

                range = (Excel.Range)sheet.Cells[1, 2];
                range = range.EntireColumn;
                range.AutoFit();
                sheet.Activate();


                string strDate = datePickerFrom.Text;
                strDate = strDate.Remove(7, 1);
                strDate = strDate.Remove(4, 1);
                string filePath = folderBrowserDialog1.SelectedPath + "\\" + "صورتحساب فروش-" + strDate + ".xls";



                //oExcel.Visible = true;
                WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                WB.Close(Missing.Value, Missing.Value, Missing.Value);
                oExcel.Workbooks.Close();
                oExcel.Quit();
                MessageBox.Show("export completed.");
            }
            else
            {

                MessageBox.Show("Monthly Bill Data InCompleted..!!!");
            }

         
        }

       private void SaleBill_Load(object sender, EventArgs e)
       {
           datePickerFrom.Text = new PersianDate(DateTime.Now).ToString("d");
           fill();
       }

       public void fill()
       {
           DataTable d = Utilities.GetTable("select * from salebill");
           if(d.Rows.Count>0)
           {

               txtmaliat.Text = d.Rows[0]["takhfif"].ToString();
               txttakhfif.Text = d.Rows[0]["maliat"].ToString();


               txtbsabt.Text = d.Rows[0]["buysabt"].ToString();
               txtbadd.Text = d.Rows[0]["buyaddre"].ToString();
               txtbcode.Text = d.Rows[0]["buycode"].ToString();
               txtbeco.Text = d.Rows[0]["buyeco"].ToString();
               txtbname.Text = d.Rows[0]["buyname"].ToString();
               txtbostan.Text = d.Rows[0]["buyostan"].ToString();
               txtbphone.Text = d.Rows[0]["buyphone"].ToString();
               txtbshahr.Text = d.Rows[0]["buyshahr"].ToString();


               txtsadd.Text = d.Rows[0]["saleaddre"].ToString();
               txtscode.Text = d.Rows[0]["salecode"].ToString();
               txtseco.Text = d.Rows[0]["saleeco"].ToString();
               txtsname.Text = d.Rows[0]["salename"].ToString();
               txtsostan.Text = d.Rows[0]["saleostan"].ToString();
               txtsphone.Text = d.Rows[0]["salephone"].ToString();
               txtsshahr.Text = d.Rows[0]["saleshahr"].ToString();
               txtssabt.Text = d.Rows[0]["salesabt"].ToString();
           }
       }

      
    }
}
