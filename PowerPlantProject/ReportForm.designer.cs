﻿namespace PowerPlantProject
{
    partial class ReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportForm));
            this.PlantCombo = new System.Windows.Forms.ComboBox();
            this.PlantName = new System.Windows.Forms.Label();
            this.FromDateCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.FromDateLb = new System.Windows.Forms.Label();
            this.ToDateLb = new System.Windows.Forms.Label();
            this.ToDateCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.OKBtn = new System.Windows.Forms.Button();
            this.FinancialRadio = new System.Windows.Forms.RadioButton();
            this.MaintenanceRadio = new System.Windows.Forms.RadioButton();
            this.MonthlyRadio = new System.Windows.Forms.RadioButton();
            this.DailyRadio = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // PlantCombo
            // 
            this.PlantCombo.FormattingEnabled = true;
            this.PlantCombo.Location = new System.Drawing.Point(102, 24);
            this.PlantCombo.Name = "PlantCombo";
            this.PlantCombo.Size = new System.Drawing.Size(121, 21);
            this.PlantCombo.TabIndex = 0;
            // 
            // PlantName
            // 
            this.PlantName.AutoSize = true;
            this.PlantName.BackColor = System.Drawing.Color.Transparent;
            this.PlantName.Location = new System.Drawing.Point(28, 27);
            this.PlantName.Name = "PlantName";
            this.PlantName.Size = new System.Drawing.Size(68, 13);
            this.PlantName.TabIndex = 1;
            this.PlantName.Text = "Plant Name :";
            // 
            // FromDateCal
            // 
            this.FromDateCal.HasButtons = true;
            this.FromDateCal.Location = new System.Drawing.Point(103, 71);
            this.FromDateCal.Name = "FromDateCal";
            this.FromDateCal.Size = new System.Drawing.Size(120, 20);
            this.FromDateCal.TabIndex = 2;
            this.FromDateCal.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // FromDateLb
            // 
            this.FromDateLb.AutoSize = true;
            this.FromDateLb.BackColor = System.Drawing.Color.Transparent;
            this.FromDateLb.Location = new System.Drawing.Point(28, 71);
            this.FromDateLb.Name = "FromDateLb";
            this.FromDateLb.Size = new System.Drawing.Size(62, 13);
            this.FromDateLb.TabIndex = 3;
            this.FromDateLb.Text = "From Date :";
            // 
            // ToDateLb
            // 
            this.ToDateLb.AutoSize = true;
            this.ToDateLb.BackColor = System.Drawing.Color.Transparent;
            this.ToDateLb.Location = new System.Drawing.Point(28, 113);
            this.ToDateLb.Name = "ToDateLb";
            this.ToDateLb.Size = new System.Drawing.Size(52, 13);
            this.ToDateLb.TabIndex = 5;
            this.ToDateLb.Text = "To Date :";
            // 
            // ToDateCal
            // 
            this.ToDateCal.HasButtons = true;
            this.ToDateCal.Location = new System.Drawing.Point(103, 113);
            this.ToDateCal.Name = "ToDateCal";
            this.ToDateCal.Size = new System.Drawing.Size(120, 20);
            this.ToDateCal.TabIndex = 4;
            this.ToDateCal.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // OKBtn
            // 
            this.OKBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.OKBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.OKBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.OKBtn.Location = new System.Drawing.Point(81, 219);
            this.OKBtn.Name = "OKBtn";
            this.OKBtn.Size = new System.Drawing.Size(85, 23);
            this.OKBtn.TabIndex = 29;
            this.OKBtn.Text = "Show Report";
            this.OKBtn.UseVisualStyleBackColor = false;
            this.OKBtn.Click += new System.EventHandler(this.OKBtn_Click);
            // 
            // FinancialRadio
            // 
            this.FinancialRadio.AutoSize = true;
            this.FinancialRadio.BackColor = System.Drawing.Color.Transparent;
            this.FinancialRadio.Checked = true;
            this.FinancialRadio.Location = new System.Drawing.Point(3, 3);
            this.FinancialRadio.Name = "FinancialRadio";
            this.FinancialRadio.Size = new System.Drawing.Size(67, 17);
            this.FinancialRadio.TabIndex = 30;
            this.FinancialRadio.TabStop = true;
            this.FinancialRadio.Text = "Financial";
            this.FinancialRadio.UseVisualStyleBackColor = false;
            this.FinancialRadio.CheckedChanged += new System.EventHandler(this.FinancialRadio_CheckedChanged);
            // 
            // MaintenanceRadio
            // 
            this.MaintenanceRadio.AutoSize = true;
            this.MaintenanceRadio.BackColor = System.Drawing.Color.Transparent;
            this.MaintenanceRadio.Location = new System.Drawing.Point(3, 26);
            this.MaintenanceRadio.Name = "MaintenanceRadio";
            this.MaintenanceRadio.Size = new System.Drawing.Size(87, 17);
            this.MaintenanceRadio.TabIndex = 31;
            this.MaintenanceRadio.Text = "Maintenance";
            this.MaintenanceRadio.UseVisualStyleBackColor = false;
            // 
            // MonthlyRadio
            // 
            this.MonthlyRadio.AutoSize = true;
            this.MonthlyRadio.BackColor = System.Drawing.Color.Transparent;
            this.MonthlyRadio.Location = new System.Drawing.Point(16, 26);
            this.MonthlyRadio.Name = "MonthlyRadio";
            this.MonthlyRadio.Size = new System.Drawing.Size(62, 17);
            this.MonthlyRadio.TabIndex = 33;
            this.MonthlyRadio.Text = "Monthly";
            this.MonthlyRadio.UseVisualStyleBackColor = false;
            // 
            // DailyRadio
            // 
            this.DailyRadio.AutoSize = true;
            this.DailyRadio.BackColor = System.Drawing.Color.Transparent;
            this.DailyRadio.Checked = true;
            this.DailyRadio.Location = new System.Drawing.Point(16, 3);
            this.DailyRadio.Name = "DailyRadio";
            this.DailyRadio.Size = new System.Drawing.Size(48, 17);
            this.DailyRadio.TabIndex = 32;
            this.DailyRadio.TabStop = true;
            this.DailyRadio.Text = "Daily";
            this.DailyRadio.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.FinancialRadio);
            this.panel1.Controls.Add(this.MaintenanceRadio);
            this.panel1.Location = new System.Drawing.Point(13, 148);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(103, 56);
            this.panel1.TabIndex = 34;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.DailyRadio);
            this.panel2.Controls.Add(this.MonthlyRadio);
            this.panel2.Location = new System.Drawing.Point(148, 148);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(103, 56);
            this.panel2.TabIndex = 35;
            // 
            // ReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(260, 254);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.OKBtn);
            this.Controls.Add(this.ToDateLb);
            this.Controls.Add(this.ToDateCal);
            this.Controls.Add(this.FromDateLb);
            this.Controls.Add(this.FromDateCal);
            this.Controls.Add(this.PlantName);
            this.Controls.Add(this.PlantCombo);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ReportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ReportForm";
            this.Load += new System.EventHandler(this.ReportForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox PlantCombo;
        private System.Windows.Forms.Label PlantName;
        private FarsiLibrary.Win.Controls.FADatePicker FromDateCal;
        private System.Windows.Forms.Label FromDateLb;
        private System.Windows.Forms.Label ToDateLb;
        private FarsiLibrary.Win.Controls.FADatePicker ToDateCal;
        private System.Windows.Forms.Button OKBtn;
        private System.Windows.Forms.RadioButton FinancialRadio;
        private System.Windows.Forms.RadioButton MaintenanceRadio;
        private System.Windows.Forms.RadioButton MonthlyRadio;
        private System.Windows.Forms.RadioButton DailyRadio;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}