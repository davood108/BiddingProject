﻿
//----------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using NRI.SBS.Common;
using FarsiLibrary.Utils;
using System.Collections;
using System.IO;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using Auto_Update_Library;

namespace PowerPlantProject
{
    
    public partial class BillItemPrintExOld : Form
    {
        int modItem;
        public BillItemPrintExOld(int modeItem)
        {
            modItem = modeItem;
            InitializeComponent();
            //CreateDatabase();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void BillItemEx_Load(object sender, EventArgs e)
        {

           // CreateDatabase();
            for (int b = 0; b < checkedListBoxitmslk.Items.Count; b++)
            {
             
             
                this.checkedListBoxitmslk.SetItemChecked(b, false);
            }


            DataTable d = Utilities.GetTable("select * from ItemSelection");

            for (int indexChecked = 0; indexChecked < checkedListBoxitmslk.Items.Count; indexChecked++)
            {
                
                    string c = checkedListBoxitmslk.Items[indexChecked].ToString();
                    string[] v = c.Split('=');

                    if (v[0].Trim() == d.Columns[indexChecked].ToString())
                    {
                        if (d.Rows[0][indexChecked].ToString().Trim() == "1" || d.Rows[0][indexChecked].ToString().Trim() == "True")
                            checkedListBoxitmslk.SetItemChecked(indexChecked, true);
                        else checkedListBoxitmslk.SetItemChecked(indexChecked, false);

                    }               
            }

        }


        private void btnSaveItemslk_Click_1(object sender, EventArgs e)
        {
            if (modItem == 0)
            {
                if (checkedListBoxitmslk.CheckedItems.Count == 1)
                {
                    DataTable s = Utilities.GetTable("delete from ItemSelection");
                    string c = checkedListBoxitmslk.CheckedItems[0].ToString();
                    string[] v = c.Split('=');
                    s = Utilities.GetTable("insert into ItemSelection (" + v[0].Trim() + ")values('1')");

                    MessageBox.Show("Saved.");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Please Select one item");
                }
            }
            else
            {

                DataTable s = Utilities.GetTable("delete from ItemSelection");
                int i = 0;
                for (int indexChecked = 0; indexChecked < checkedListBoxitmslk.CheckedItems.Count; indexChecked++)
                {

                    string c = checkedListBoxitmslk.CheckedItems[indexChecked].ToString();

                    string[] v = c.Split('=');
                    if (i == 0)
                    {
                        s = Utilities.GetTable("insert into ItemSelection (" + v[0].Trim() + ")values('1')");
                    }
                    else
                    {
                        s = Utilities.GetTable("update ItemSelection  set " + v[0].Trim() + " ='1'");
                    }
                    i++;

                    
                   // PrintFinancialReportEx ppfrE = new PrintFinancialReportEx();
                    //ppfrE.textBox9.Text = v[0].Trim();
             

                }
                MessageBox.Show("Saved.");
                this.Close();
            }


        }


    ////=========================Create DB ============================
        private void CreateDatabase()
        {
             String str;
        SqlConnection myConn = new SqlConnection ("Server=localhost;Integrated security=SSPI;database=master");

     
            string sqlCreateDBQuery;
            //tmpConn = new SqlConnection();
            myConn.ConnectionString = "SERVER = LKHORSAND; DATABASE = master; User ID = sa; Pwd = 1";
            sqlCreateDBQuery = " CREATE DATABASE BillselectItem ON PRIMARY "

                               + " (NAME = BillSelect, "
                               + " FILENAME = 'BillselectItem', "
                               + " SIZE = 2MB,"
                               + " FILEGROWTH = 10%) "
                               + " LOG ON (NAME =billitem_log, "
                               + " FILENAME = '', "
                               + " SIZE = 1MB, "
                               + " FILEGROWTH =10%) ";
            SqlCommand myCommand = new SqlCommand(sqlCreateDBQuery, myConn);
            try
            {
                myConn.Open();
                MessageBox.Show(sqlCreateDBQuery);
                myCommand.ExecuteNonQuery();
                MessageBox.Show("Database has been created successfully!",
                                  "Create Database", MessageBoxButtons.OK,
                                              MessageBoxIcon.Information);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Create Database",
                                            MessageBoxButtons.OK,
                                     MessageBoxIcon.Information);
            }
            finally
            {
                myConn.Close();
            }
            return;
        }
    }
}
