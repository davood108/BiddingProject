﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace PowerPlantProject
{
    public partial class UpdateDatbase : Form
    {
        Sync Sync1;
        public UpdateDatbase()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
               
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

       

        private void button2_Click(object sender, EventArgs e)
        {
         
            
        }

      
        private void Form1_Load(object sender, EventArgs e)
        {
            string x1 = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
             "\\SBS\\PcConfigs.xml";

            string x2 = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                  "\\SBS\\Tables.xml";

            Sync1 = new Sync(x1,x2);
            string[] x = Sync1.GetComputers();
            for (int i = 0; i < x.Length; i++)
            {
                comboBox_Source.Items.Add(x[i]);
                comboBox_Dest.Items.Add(x[i]);
            }
            comboBox_Dest.SelectedIndex = 0;
            comboBox_Source.SelectedIndex = 1;
        }

      

        private void button2_Click_1(object sender, EventArgs e)
        {
            
            Edit_Computers EditForm = new Edit_Computers();
            EditForm.StartPosition = FormStartPosition.Manual;
            EditForm.Location = this.Location;
            EditForm.ShowDialog();
            this.OnLoad(e);
        }

        private void button3_Click(object sender, EventArgs e)
        {

                DialogResult d = MessageBox.Show("Are You Sure You Want To Update  "+comboBox_Source.Text.Trim()+"  Files From DataBase : <<<"+comboBox_Dest.Text.Trim()+ ">>> ?", "Attention !!", MessageBoxButtons.YesNo);

                if (d == DialogResult.Yes)
                {

                    Sync1.SetConnections(comboBox_Source.Text, comboBox_Dest.Text);
                    Sync1.OnChangeProgress += new EventHandler(Sync1_OnChangeProgress);
                    int no = 0;
                    string name = "";
                    string status = "";
                    richTextBox1.Text = "";


                    int i = 1;
                    int count = Sync1.TablesCount;
                    Sync1.open();
                    while (Sync1.UpdateNextTable(ref name, ref status, ref no))
                    {
                        richTextBox1.AppendText(name + "  ,  " + status + "  , Updated Rows : " + no.ToString() + "\n");
                        Application.DoEvents();
                        i++;
                    }
                    Sync1.close();
                    MessageBox.Show("Completed Successfully");  
                }
        }

        void Sync1_OnChangeProgress(object sender, EventArgs e)
        {
            progressBar1.Value = (sender as Sync).Progress;
        }

       

  

     
      

         
    }
}
