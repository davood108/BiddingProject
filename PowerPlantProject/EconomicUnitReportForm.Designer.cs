﻿namespace PowerPlantProject
{
    partial class EconomicUnitReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.EconomicUnitcrystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            //this.EconomicUnitCrystalReportDoc1 = new PowerPlantProject.EconomicUnitCrystalReport();
            this.SuspendLayout();
            // 
            // EconomicUnitcrystalReportViewer1
            // 
            //this.EconomicUnitcrystalReportViewer1.ActiveViewIndex = -1;
            //this.EconomicUnitcrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            //this.EconomicUnitcrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            //this.EconomicUnitcrystalReportViewer1.Location = new System.Drawing.Point(0, 0);
            //this.EconomicUnitcrystalReportViewer1.Name = "EconomicUnitcrystalReportViewer1";
            //this.EconomicUnitcrystalReportViewer1.SelectionFormula = "";
            //this.EconomicUnitcrystalReportViewer1.Size = new System.Drawing.Size(292, 266);
            //this.EconomicUnitcrystalReportViewer1.TabIndex = 0;
            //this.EconomicUnitcrystalReportViewer1.ViewTimeSelectionFormula = "";
            // 
            // EconomicUnitReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
           // this.Controls.Add(this.EconomicUnitcrystalReportViewer1);
            this.Name = "EconomicUnitReportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EconomicUnitReportForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.EconomicUnitReportForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

       // private CrystalDecisions.Windows.Forms.CrystalReportViewer EconomicUnitcrystalReportViewer1;
      //  private EconomicUnitCrystalReport EconomicUnitCrystalReportDoc1;
    }
}