﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using System.IO;
using Microsoft.Win32;

namespace PowerPlantProject
{
    public partial class MustHourly : Form
    {
        public MustHourly()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dataGridView2.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void MustHourly_Load(object sender, EventArgs e)
        {
            textBox1.Text = "0";
            dataGridView2.Columns[0].Width = 17;
            dataGridView2.Columns[0].HeaderText="";
            datePickerFrom.Text = new PersianDate(DateTime.Now).ToString("d");
           datePickerto.Text= new PersianDate(DateTime.Now.AddDays(1)).ToString("d");
            DataTable c = Utilities.GetTable("select ppid from powerplant");
            cmbplant.Text = c.Rows[0][0].ToString();
            foreach (DataRow n in c.Rows)
            {
                cmbplant.Items.Add(n[0].ToString().Trim());
              
            }
            DataTable v = Utilities.GetTable("select unitcode from UnitsDataMain where ppid='" + cmbplant.Text.Trim() + "'order by unitcode asc");
            foreach (DataRow x in v.Rows)
            {
                cmbunit.Items.Add(x[0].ToString().Trim());
                
            }

            cmbunit.Text = "All Units";
            cmbunit.Items.Add("All Units");

            cmbfhour.Text = "1";
            cmbthour.Text = "24";
            fillallunit();
        }
        public void fillallunit()
        {
            dataGridView2.DataSource = null;
            DataTable dd = Utilities.GetTable("select PPID,unit,fromdate,todate,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15,h16,h17,h18,h19,h20,h21,h22,h23,h24 from dbo.MustHourly order by PPID,unit asc");
            dataGridView2.DataSource = dd;
        }

        private void cmbplant_SelectedValueChanged(object sender, EventArgs e)
        {
            cmbunit.Items.Clear();
            DataTable v = Utilities.GetTable("select unitcode from UnitsDataMain where ppid='" + cmbplant.Text.Trim() + "'order by unitcode asc");
            foreach (DataRow x in v.Rows)
            {
                cmbunit.Items.Add(x[0].ToString().Trim());
            }

            cmbunit.Text = "All Units";
            cmbunit.Items.Add("All Units");


            fill();
        }

        private void cmbunit_SelectedValueChanged(object sender, EventArgs e)
        {
            fill();
        }
        public void fill()
        {



            DataTable x = Utilities.GetTable("select * from dbo.MustHourly where fromdate='" + datePickerFrom.Text.Trim() + "'and todate='"+datePickerto.Text.Trim()+"'and unit='" + cmbunit.Text.Trim() + "'and ppid='" + cmbplant.Text.Trim() + "'ORDER BY id DESC");
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            if (x.Rows.Count > 0)
            {
                for (int i = 0; i < 24; i++)
                {
                    string r = x.Rows[0][i + 5].ToString().Trim();
                    string[] val = r.Split('-');
                    dataGridView1.Rows[0].Cells[i].Value = r.ToString();
                    if (r == "") dataGridView1.Rows[0].Cells[i].Style.BackColor = Color.Gray;
                    else if (val[1] == "MN") dataGridView1.Rows[0].Cells[i].Style.BackColor = Color.Orange;
                    else if (val[1] == "MR") dataGridView1.Rows[0].Cells[i].Style.BackColor = Color.Green;
                    else if (val[1] == "MO") dataGridView1.Rows[0].Cells[i].Style.BackColor = Color.Red;
                
                   

                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            TimeSpan span = PersianDateConverter.ToGregorianDateTime(datePickerto.Text) - PersianDateConverter.ToGregorianDateTime(datePickerFrom.Text);
            int dday = span.Days;
            if (dday < 0)
            {
                MessageBox.Show("Please Select Correct Interval Date!");
                return;
            }


            if (cmbunit.Text == "All Units" && textBox1.Text != "" && cmbunit.Text != "")
            {
                ///////////////////////////////////////////////////////
                for (int yn = 0; yn < cmbunit.Items.Count - 1; yn++)
                {
                    string uname = cmbunit.Items[yn].ToString();
                    string st1 = "insert into musthourly (ppid,unit,fromdate,todate,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15,h16,h17,h18,h19,h20,h21,h22,h23,h24)values('" + cmbplant.Text.Trim() + "','" + uname + "','" + datePickerFrom.Text.Trim() + "','" + datePickerto.Text.Trim();
                    DataTable d = Utilities.GetTable("delete from musthourly where unit='" + uname + "'and ppid='" + cmbplant.Text.Trim() + "'and fromdate='" + datePickerFrom.Text + "'and todate='" + datePickerto.Text + "'");
                    string st2 = "";
                    string v = "";
                    for (int i = 0; i < 24; i++)
                    {
                        try
                        {
                            v = dataGridView1.Rows[0].Cells[i].Value.ToString().Trim();
                        }
                        catch
                        {
                            v = "";
                        }


                        st2 += ("','" + v);
                        if (i == 23) st2 += "')";
                    }
                    DataTable ind = Utilities.GetTable(st1 + st2);


                }
                MessageBox.Show("Saved.");
                fillallunit();
                fill();
            }


            else if (textBox1.Text != "" && cmbunit.Text != "")
            {
                string st1 = "insert into musthourly (ppid,unit,fromdate,todate,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15,h16,h17,h18,h19,h20,h21,h22,h23,h24)values('" + cmbplant.Text.Trim() + "','" + cmbunit.Text.Trim() + "','" + datePickerFrom.Text.Trim() + "','" + datePickerto.Text.Trim();
                DataTable d = Utilities.GetTable("delete from musthourly where unit='" + cmbunit.Text.Trim() + "'and ppid='" + cmbplant.Text.Trim() + "'and fromdate='" + datePickerFrom.Text + "'and todate='" + datePickerto.Text + "'");
                string st2 = "";
                string v = "";
                for (int i = 0; i < 24; i++)
                {
                    try
                    {
                        v = dataGridView1.Rows[0].Cells[i].Value.ToString().Trim();
                    }
                    catch
                    {
                        v = "";
                    }


                    st2 += ("','" + v);
                    if (i == 23) st2 += "')";
                }
                DataTable ind = Utilities.GetTable(st1 + st2);
                MessageBox.Show("Saved.");
                fillallunit();
                fill();
            }
            else
            {
                MessageBox.Show("Fill Completely.");
            }
        }
        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "" || str.Trim() == null)
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private void btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                double c = MyDoubleParse(textBox1.Text);
            }
            catch
            {
                textBox1.Text = "";
            }
            int fromh =int.Parse(cmbfhour.Text.Trim());
            int thour = int.Parse(cmbthour.Text.Trim());
            string status = "";
            if (rdMustNet.Checked) status = "MN";
            else if (rdmustrun.Checked) status = "MR";
            else if (rdMustOff.Checked) status = "MO";
       
            if (status != "")
            {
                for (int h = fromh; h <= thour; h++)
                {
                    dataGridView1.Rows[0].Cells[h - 1].Value = textBox1.Text + "-" + status;

                }
            }
        }

        private void datePickerFrom_SelectedDateTimeChanged(object sender, EventArgs e)
        {
            fill();
        }

        private void datePickerFrom_ValueChanged(object sender, EventArgs e)
        {
            fill();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            //DataTable d = utilities.GetTable("delete from musthourly where fromdate='" + datePickerFrom.Text.Trim() + "'and todate='"+datePickerto.Text.Trim()+"' and unit='" + cmbunit.Text.Trim() + "'and ppid='" + cmbplant.Text.Trim() + "'");
            //fill();
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            try
            {
                DataTable v = Utilities.GetTable("select pmax from UnitsDataMain where ppid='" + cmbplant.Text.Trim() + "'and unitcode='" + cmbunit.Text.Trim() + "'order by unitcode asc");
                double pmax = MyDoubleParse(v.Rows[0][0].ToString());
                if (MyDoubleParse(textBox1.Text) > pmax || MyDoubleParse(textBox1.Text) < 0)
                {
                    textBox1.Text = "";
                    MessageBox.Show("Please insert  0< value<=pmax");

                }
            }
            catch
            {

            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;

          string x= dataGridView2.Rows[rowIndex].Cells[e.ColumnIndex].Value.ToString();
          if (x == "System.Drawing.Bitmap")
          {
              DataTable bb = Utilities.GetTable("delete from musthourly where ppid='" + dataGridView2.Rows[rowIndex].Cells[1].Value.ToString() + "' and  unit='" + dataGridView2.Rows[rowIndex].Cells[2].Value.ToString() + "'and fromdate='" + dataGridView2.Rows[rowIndex].Cells[3].Value.ToString() + "'and todate='" + dataGridView2.Rows[rowIndex].Cells[4].Value.ToString() + "'");
          }
          fillallunit();
        }

       


       

       
    }
}
