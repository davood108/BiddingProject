﻿namespace PowerPlantProject
{
    partial class RunSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RunSetting));
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.chKlmp = new System.Windows.Forms.CheckBox();
            this.chkfairplay = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPlantValue = new System.Windows.Forms.Label();
            this.chkCost = new System.Windows.Forms.CheckBox();
            this.chkloospay = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdsensetivity = new System.Windows.Forms.RadioButton();
            this.rdopf = new System.Windows.Forms.RadioButton();
            this.btnSave = new System.Windows.Forms.Button();
            this.chkmarketpower = new System.Windows.Forms.CheckBox();
            this.checkPreSolve = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.groupBox18.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox18
            // 
            this.groupBox18.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox18.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.groupBox18.Controls.Add(this.chKlmp);
            this.groupBox18.Controls.Add(this.chkfairplay);
            this.groupBox18.Controls.Add(this.label1);
            this.groupBox18.Controls.Add(this.lblPlantValue);
            this.groupBox18.Controls.Add(this.chkCost);
            this.groupBox18.Controls.Add(this.chkloospay);
            this.groupBox18.Controls.Add(this.groupBox1);
            this.groupBox18.Controls.Add(this.btnSave);
            this.groupBox18.Controls.Add(this.chkmarketpower);
            this.groupBox18.Controls.Add(this.checkPreSolve);
            this.groupBox18.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox18.ForeColor = System.Drawing.Color.Black;
            this.groupBox18.Location = new System.Drawing.Point(218, 12);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox18.Size = new System.Drawing.Size(522, 259);
            this.groupBox18.TabIndex = 46;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Setting";
            // 
            // chKlmp
            // 
            this.chKlmp.AutoSize = true;
            this.chKlmp.Location = new System.Drawing.Point(36, 117);
            this.chKlmp.Name = "chKlmp";
            this.chKlmp.Size = new System.Drawing.Size(66, 17);
            this.chKlmp.TabIndex = 55;
            this.chKlmp.Text = "CapBid";
            this.chKlmp.UseVisualStyleBackColor = true;
            // 
            // chkfairplay
            // 
            this.chkfairplay.AutoSize = true;
            this.chkfairplay.Location = new System.Drawing.Point(363, 117);
            this.chkfairplay.Name = "chkfairplay";
            this.chkfairplay.Size = new System.Drawing.Size(81, 17);
            this.chkfairplay.TabIndex = 54;
            this.chkfairplay.Text = "MinPower";
            this.chkfairplay.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(186, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 52;
            this.label1.Text = "Plant :";
            // 
            // lblPlantValue
            // 
            this.lblPlantValue.AutoSize = true;
            this.lblPlantValue.Location = new System.Drawing.Point(232, 43);
            this.lblPlantValue.Name = "lblPlantValue";
            this.lblPlantValue.Size = new System.Drawing.Size(41, 13);
            this.lblPlantValue.TabIndex = 52;
            this.lblPlantValue.Text = "label1";
            // 
            // chkCost
            // 
            this.chkCost.AutoSize = true;
            this.chkCost.Location = new System.Drawing.Point(364, 156);
            this.chkCost.Name = "chkCost";
            this.chkCost.Size = new System.Drawing.Size(104, 17);
            this.chkCost.TabIndex = 51;
            this.chkCost.Text = "UL Reduction";
            this.chkCost.UseVisualStyleBackColor = true;
            // 
            // chkloospay
            // 
            this.chkloospay.AutoSize = true;
            this.chkloospay.Location = new System.Drawing.Point(362, 81);
            this.chkloospay.Name = "chkloospay";
            this.chkloospay.Size = new System.Drawing.Size(51, 17);
            this.chkloospay.TabIndex = 51;
            this.chkloospay.Text = "Risk";
            this.chkloospay.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdsensetivity);
            this.groupBox1.Controls.Add(this.rdopf);
            this.groupBox1.Location = new System.Drawing.Point(150, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(170, 102);
            this.groupBox1.TabIndex = 50;
            this.groupBox1.TabStop = false;
            // 
            // rdsensetivity
            // 
            this.rdsensetivity.AutoSize = true;
            this.rdsensetivity.Location = new System.Drawing.Point(23, 57);
            this.rdsensetivity.Name = "rdsensetivity";
            this.rdsensetivity.Size = new System.Drawing.Size(133, 17);
            this.rdsensetivity.TabIndex = 55;
            this.rdsensetivity.TabStop = true;
            this.rdsensetivity.Text = "SensetivityAnalysis";
            this.rdsensetivity.UseVisualStyleBackColor = true;
            // 
            // rdopf
            // 
            this.rdopf.AutoSize = true;
            this.rdopf.Location = new System.Drawing.Point(23, 21);
            this.rdopf.Name = "rdopf";
            this.rdopf.Size = new System.Drawing.Size(45, 17);
            this.rdopf.TabIndex = 55;
            this.rdopf.TabStop = true;
            this.rdopf.Text = "Opf";
            this.rdopf.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.CadetBlue;
            this.btnSave.BackgroundImage = global::PowerPlantProject.Properties.Resources.Savedata;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnSave.Location = new System.Drawing.Point(198, 209);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(86, 27);
            this.btnSave.TabIndex = 49;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // chkmarketpower
            // 
            this.chkmarketpower.AutoSize = true;
            this.chkmarketpower.Location = new System.Drawing.Point(36, 156);
            this.chkmarketpower.Name = "chkmarketpower";
            this.chkmarketpower.Size = new System.Drawing.Size(100, 17);
            this.chkmarketpower.TabIndex = 47;
            this.chkmarketpower.Text = "MarketPower";
            this.chkmarketpower.UseVisualStyleBackColor = true;
            // 
            // checkPreSolve
            // 
            this.checkPreSolve.AutoSize = true;
            this.checkPreSolve.Location = new System.Drawing.Point(36, 81);
            this.checkPreSolve.Name = "checkPreSolve";
            this.checkPreSolve.Size = new System.Drawing.Size(77, 17);
            this.checkPreSolve.TabIndex = 46;
            this.checkPreSolve.Text = "PreSolve";
            this.checkPreSolve.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.treeView1);
            this.panel1.Location = new System.Drawing.Point(2, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(210, 259);
            this.panel1.TabIndex = 47;
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(17, 14);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(179, 227);
            this.treeView1.TabIndex = 0;
            this.treeView1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
            // 
            // RunSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(752, 284);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox18);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RunSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RunSetting";
            this.Load += new System.EventHandler(this.RunSetting_Load);
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.CheckBox chkmarketpower;
        private System.Windows.Forms.CheckBox checkPreSolve;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.CheckBox chkCost;
        private System.Windows.Forms.CheckBox chkloospay;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblPlantValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rdopf;
        private System.Windows.Forms.CheckBox chkfairplay;
        private System.Windows.Forms.RadioButton rdsensetivity;
        private System.Windows.Forms.CheckBox chKlmp;

    }
}