﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net;
using System.Web;
namespace PowerPlantProject
{
    public partial class Email : Form
    {
        public Email()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                panel4.BackColor = FormColors.GetColor().Formbackcolor;
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////

                         

            }
            catch
            {

            }
        }

        private void Email_Load(object sender, EventArgs e)
        {
            groupBox3.Hide();
            
         
            System.Globalization.PersianCalendar p = new System.Globalization.PersianCalendar();

            label6.Text = p.GetSecond(DateTime.Now) + " : " + p.GetMinute(DateTime.Now) + " : " + p.GetHour(DateTime.Now);
            timer1.Start();


            label7.Text = p.GetDayOfMonth(DateTime.Now) + " / " + p.GetMonth(DateTime.Now) + " / " + p.GetYear(DateTime.Now);
        }

          
        private void button2_Click(object sender, EventArgs e)
        {
           

            using (OpenFileDialog op = new OpenFileDialog())
            {
                op.Title = "";
                op.CheckFileExists = true;
                op.CheckPathExists = true;
                op.Filter = "";
                if (op.ShowDialog() == DialogResult.OK)
                {
                    textAttachments.Text = op.FileName.Trim();

                }



            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            System.Diagnostics.Process.Start("winword.exe");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if ((errorProvider1.GetError(textBox2)) == "" && (errorProvider1.GetError(textBox3) == "") && (errorProvider1.GetError(textBox4) == ""))
                {

                    OpenFileDialog op = new OpenFileDialog();
                    System.Net.Mail.MailMessage message = new MailMessage(textBox2.Text, textBox3.Text, textBox5.Text, textBox1.Text);
                    System.Net.Mail.SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                    message.Body = textBox1.Text;
                    smtp.Timeout = 90000000;

                    message.Priority = MailPriority.High;
                    // System.Net.Mail.Attachment attach;
                    // If My.Computer.FileSystem.FileExists(txtAttachment.Text) Then message.Attachments.Add(attach)
                    // .Attachments.Add(new Attachment(textBox1.Text.Trim()));
                    if (checkBox1.Checked == true)
                    {


                        message.Attachments.Add(new Attachment(textAttachments.Text.Trim()));

                    }



                    smtp.EnableSsl = true;
                    smtp.Credentials = new System.Net.NetworkCredential(textBox2.Text, textBox4.Text);

                    try
                    {


                        smtp.Send(message);
                        MessageBox.Show("Well, the mail message appears to have been a success!", " Successful?", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    //catch (SmtpException exc)
                    //{
                    catch (Exception ex)
                    {
                        //System.Windows.Forms.MessageBox.Show("Test\n" + ex.Message);
                        MessageBox.Show("Could not send message: " + ex.Message,
                       "Problem sending message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {

                    MessageBox.Show("Fill Blanks Completely");
                }
            }
            catch
            {
                MessageBox.Show("Problem sending message");
            }
        }

      

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(linkLabel3.Text);
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
                SendKeys.Send("{tab}");
        }

       

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                groupBox3.Show();
            }
            else
                groupBox3.Hide();
        }

       

        private void timer1_Tick(object sender, EventArgs e)
        {
            System.Globalization.PersianCalendar p = new System.Globalization.PersianCalendar();
            label6.Text = p.GetSecond(DateTime.Now) + " : " + p.GetMinute(DateTime.Now) + " : " + p.GetHour(DateTime.Now);
        }

        private void linkLabel3_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(linkLabel3.Text);
        }

        private void textBox2_Validated(object sender, EventArgs e)
        {
            if (textBox2.Text=="")
                errorProvider1.SetError(textBox2,"Fill Completely");
            else errorProvider1.SetError(textBox2, "");
        }
        private void textBox3_Validated(object sender, EventArgs e)
        {
            if (textBox3.Text == "")
                errorProvider1.SetError(textBox3, "Fill Completely");
            else errorProvider1.SetError(textBox3, "");
        }

        private void textBox4_Validated(object sender, EventArgs e)
        {
            if (textBox4.Text == "")
                errorProvider1.SetError(textBox4, "Fill Completely");
            else errorProvider1.SetError(textBox4, "");
        }

        private void button4_Click(object sender, EventArgs e)
        {
           
            System.Diagnostics.Process.Start("Excel.exe");
        }

        private void button5_Click(object sender, EventArgs e)
        {
           System.Diagnostics.Process.Start("iexplore.exe");
        }

    }
}
