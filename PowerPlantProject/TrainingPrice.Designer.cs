﻿namespace PowerPlantProject
{
    partial class TrainingPrice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrainingPrice));
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbplant = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.faDatePicker12 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.faDatePicker2 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.faDatePicker11 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.faDatePicker10 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.faDatePicker9 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.faDatePicker8 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.faDatePicker7 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.faDatePicker6 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.faDatePicker5 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.faDatePicker4 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.faDatePicker3 = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBox12 = new System.Windows.Forms.ComboBox();
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Delete = new System.Windows.Forms.DataGridViewImageColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 50;
            this.label2.Text = "Plant";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 51;
            this.label3.Text = "label1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 51;
            this.label4.Text = "label1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 51;
            this.label5.Text = "label1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 117);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 51;
            this.label6.Text = "label1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 146);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 51;
            this.label7.Text = "label1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 173);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 51;
            this.label8.Text = "label1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 201);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 51;
            this.label9.Text = "label1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 233);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 51;
            this.label10.Text = "label1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 266);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 51;
            this.label11.Text = "label1";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "DefaultPrice",
            "DefaultDate"});
            this.comboBox2.Location = new System.Drawing.Point(77, 24);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(152, 21);
            this.comboBox2.TabIndex = 49;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "DefaultPrice",
            "DefaultDate"});
            this.comboBox3.Location = new System.Drawing.Point(77, 55);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(152, 21);
            this.comboBox3.TabIndex = 49;
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "DefaultPrice",
            "DefaultDate"});
            this.comboBox4.Location = new System.Drawing.Point(77, 84);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(152, 21);
            this.comboBox4.TabIndex = 49;
            this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "DefaultPrice",
            "DefaultDate"});
            this.comboBox5.Location = new System.Drawing.Point(77, 114);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(152, 21);
            this.comboBox5.TabIndex = 49;
            this.comboBox5.SelectedIndexChanged += new System.EventHandler(this.comboBox5_SelectedIndexChanged);
            // 
            // comboBox6
            // 
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Items.AddRange(new object[] {
            "DefaultPrice",
            "DefaultDate"});
            this.comboBox6.Location = new System.Drawing.Point(77, 141);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(152, 21);
            this.comboBox6.TabIndex = 49;
            this.comboBox6.SelectedIndexChanged += new System.EventHandler(this.comboBox6_SelectedIndexChanged);
            // 
            // comboBox7
            // 
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Items.AddRange(new object[] {
            "DefaultPrice",
            "DefaultDate"});
            this.comboBox7.Location = new System.Drawing.Point(77, 170);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(152, 21);
            this.comboBox7.TabIndex = 49;
            this.comboBox7.SelectedIndexChanged += new System.EventHandler(this.comboBox7_SelectedIndexChanged);
            // 
            // comboBox8
            // 
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Items.AddRange(new object[] {
            "DefaultPrice",
            "DefaultDate"});
            this.comboBox8.Location = new System.Drawing.Point(77, 198);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(152, 21);
            this.comboBox8.TabIndex = 49;
            this.comboBox8.SelectedIndexChanged += new System.EventHandler(this.comboBox8_SelectedIndexChanged);
            // 
            // comboBox9
            // 
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Items.AddRange(new object[] {
            "DefaultPrice",
            "DefaultDate"});
            this.comboBox9.Location = new System.Drawing.Point(77, 229);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(152, 21);
            this.comboBox9.TabIndex = 49;
            this.comboBox9.SelectedIndexChanged += new System.EventHandler(this.comboBox9_SelectedIndexChanged);
            // 
            // comboBox10
            // 
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Items.AddRange(new object[] {
            "DefaultPrice",
            "DefaultDate"});
            this.comboBox10.Location = new System.Drawing.Point(77, 264);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(152, 21);
            this.comboBox10.TabIndex = 49;
            this.comboBox10.SelectedIndexChanged += new System.EventHandler(this.comboBox10_SelectedIndexChanged);
            this.comboBox10.Enter += new System.EventHandler(this.comboBox10_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::PowerPlantProject.Properties.Resources.savemaeium;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(166, 407);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(36, 24);
            this.button1.TabIndex = 54;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(877, 53);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(65, 13);
            this.linkLabel1.TabIndex = 55;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "DefaultPrice";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbplant);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(390, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(210, 39);
            this.groupBox1.TabIndex = 56;
            this.groupBox1.TabStop = false;
            // 
            // cmbplant
            // 
            this.cmbplant.FormattingEnabled = true;
            this.cmbplant.Location = new System.Drawing.Point(71, 12);
            this.cmbplant.Name = "cmbplant";
            this.cmbplant.Size = new System.Drawing.Size(121, 21);
            this.cmbplant.TabIndex = 49;
            this.cmbplant.SelectedIndexChanged += new System.EventHandler(this.cmbplant_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.faDatePicker12);
            this.groupBox2.Controls.Add(this.faDatePicker2);
            this.groupBox2.Controls.Add(this.faDatePicker11);
            this.groupBox2.Controls.Add(this.faDatePicker10);
            this.groupBox2.Controls.Add(this.faDatePicker9);
            this.groupBox2.Controls.Add(this.faDatePicker8);
            this.groupBox2.Controls.Add(this.faDatePicker7);
            this.groupBox2.Controls.Add(this.faDatePicker6);
            this.groupBox2.Controls.Add(this.faDatePicker5);
            this.groupBox2.Controls.Add(this.faDatePicker4);
            this.groupBox2.Controls.Add(this.faDatePicker3);
            this.groupBox2.Controls.Add(this.comboBox2);
            this.groupBox2.Controls.Add(this.comboBox3);
            this.groupBox2.Controls.Add(this.comboBox4);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.comboBox5);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.comboBox6);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.comboBox7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.comboBox8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.comboBox9);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.comboBox12);
            this.groupBox2.Controls.Add(this.comboBox11);
            this.groupBox2.Controls.Add(this.comboBox10);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(6, 24);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(377, 376);
            this.groupBox2.TabIndex = 57;
            this.groupBox2.TabStop = false;
            // 
            // faDatePicker12
            // 
            this.faDatePicker12.Enabled = false;
            this.faDatePicker12.HasButtons = true;
            this.faDatePicker12.Location = new System.Drawing.Point(235, 336);
            this.faDatePicker12.Name = "faDatePicker12";
            this.faDatePicker12.Readonly = true;
            this.faDatePicker12.Size = new System.Drawing.Size(120, 20);
            this.faDatePicker12.TabIndex = 51;
            // 
            // faDatePicker2
            // 
            this.faDatePicker2.Enabled = false;
            this.faDatePicker2.HasButtons = true;
            this.faDatePicker2.Location = new System.Drawing.Point(235, 25);
            this.faDatePicker2.Name = "faDatePicker2";
            this.faDatePicker2.Readonly = true;
            this.faDatePicker2.Size = new System.Drawing.Size(120, 20);
            this.faDatePicker2.TabIndex = 51;
            // 
            // faDatePicker11
            // 
            this.faDatePicker11.Enabled = false;
            this.faDatePicker11.HasButtons = true;
            this.faDatePicker11.Location = new System.Drawing.Point(235, 303);
            this.faDatePicker11.Name = "faDatePicker11";
            this.faDatePicker11.Readonly = true;
            this.faDatePicker11.Size = new System.Drawing.Size(120, 20);
            this.faDatePicker11.TabIndex = 51;
            // 
            // faDatePicker10
            // 
            this.faDatePicker10.Enabled = false;
            this.faDatePicker10.HasButtons = true;
            this.faDatePicker10.Location = new System.Drawing.Point(235, 266);
            this.faDatePicker10.Name = "faDatePicker10";
            this.faDatePicker10.Readonly = true;
            this.faDatePicker10.Size = new System.Drawing.Size(120, 20);
            this.faDatePicker10.TabIndex = 51;
            // 
            // faDatePicker9
            // 
            this.faDatePicker9.Enabled = false;
            this.faDatePicker9.HasButtons = true;
            this.faDatePicker9.Location = new System.Drawing.Point(235, 231);
            this.faDatePicker9.Name = "faDatePicker9";
            this.faDatePicker9.Readonly = true;
            this.faDatePicker9.Size = new System.Drawing.Size(120, 20);
            this.faDatePicker9.TabIndex = 51;
            // 
            // faDatePicker8
            // 
            this.faDatePicker8.Enabled = false;
            this.faDatePicker8.HasButtons = true;
            this.faDatePicker8.Location = new System.Drawing.Point(235, 198);
            this.faDatePicker8.Name = "faDatePicker8";
            this.faDatePicker8.Readonly = true;
            this.faDatePicker8.Size = new System.Drawing.Size(120, 20);
            this.faDatePicker8.TabIndex = 51;
            // 
            // faDatePicker7
            // 
            this.faDatePicker7.Enabled = false;
            this.faDatePicker7.HasButtons = true;
            this.faDatePicker7.Location = new System.Drawing.Point(235, 171);
            this.faDatePicker7.Name = "faDatePicker7";
            this.faDatePicker7.Readonly = true;
            this.faDatePicker7.Size = new System.Drawing.Size(120, 20);
            this.faDatePicker7.TabIndex = 51;
            // 
            // faDatePicker6
            // 
            this.faDatePicker6.Enabled = false;
            this.faDatePicker6.HasButtons = true;
            this.faDatePicker6.Location = new System.Drawing.Point(235, 142);
            this.faDatePicker6.Name = "faDatePicker6";
            this.faDatePicker6.Readonly = true;
            this.faDatePicker6.Size = new System.Drawing.Size(120, 20);
            this.faDatePicker6.TabIndex = 51;
            // 
            // faDatePicker5
            // 
            this.faDatePicker5.Enabled = false;
            this.faDatePicker5.HasButtons = true;
            this.faDatePicker5.Location = new System.Drawing.Point(235, 114);
            this.faDatePicker5.Name = "faDatePicker5";
            this.faDatePicker5.Readonly = true;
            this.faDatePicker5.Size = new System.Drawing.Size(120, 20);
            this.faDatePicker5.TabIndex = 51;
            // 
            // faDatePicker4
            // 
            this.faDatePicker4.Enabled = false;
            this.faDatePicker4.HasButtons = true;
            this.faDatePicker4.Location = new System.Drawing.Point(235, 84);
            this.faDatePicker4.Name = "faDatePicker4";
            this.faDatePicker4.Readonly = true;
            this.faDatePicker4.Size = new System.Drawing.Size(120, 20);
            this.faDatePicker4.TabIndex = 51;
            // 
            // faDatePicker3
            // 
            this.faDatePicker3.Enabled = false;
            this.faDatePicker3.HasButtons = true;
            this.faDatePicker3.Location = new System.Drawing.Point(235, 56);
            this.faDatePicker3.Name = "faDatePicker3";
            this.faDatePicker3.Readonly = true;
            this.faDatePicker3.Size = new System.Drawing.Size(120, 20);
            this.faDatePicker3.TabIndex = 51;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 338);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 13);
            this.label14.TabIndex = 51;
            this.label14.Text = "label1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 304);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 51;
            this.label13.Text = "label1";
            // 
            // comboBox12
            // 
            this.comboBox12.FormattingEnabled = true;
            this.comboBox12.Items.AddRange(new object[] {
            "DefaultPrice",
            "DefaultDate"});
            this.comboBox12.Location = new System.Drawing.Point(77, 335);
            this.comboBox12.Name = "comboBox12";
            this.comboBox12.Size = new System.Drawing.Size(152, 21);
            this.comboBox12.TabIndex = 49;
            this.comboBox12.SelectedIndexChanged += new System.EventHandler(this.comboBox12_SelectedIndexChanged);
            this.comboBox12.Enter += new System.EventHandler(this.comboBox12_SelectedIndexChanged);
            // 
            // comboBox11
            // 
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Items.AddRange(new object[] {
            "DefaultPrice",
            "DefaultDate"});
            this.comboBox11.Location = new System.Drawing.Point(77, 301);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(152, 21);
            this.comboBox11.TabIndex = 49;
            this.comboBox11.SelectedIndexChanged += new System.EventHandler(this.comboBox11_SelectedIndexChanged);
            this.comboBox11.Enter += new System.EventHandler(this.comboBox11_SelectedIndexChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Delete});
            this.dataGridView1.Location = new System.Drawing.Point(389, 76);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(553, 324);
            this.dataGridView1.TabIndex = 58;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // Delete
            // 
            this.Delete.HeaderText = "Delete";
            this.Delete.Image = global::PowerPlantProject.Properties.Resources.deletered8;
            this.Delete.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.Delete.Name = "Delete";
            // 
            // TrainingPrice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 443);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "TrainingPrice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TrainingPrice";
            this.Load += new System.EventHandler(this.TrainingPrice_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbplant;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comboBox12;
        private System.Windows.Forms.ComboBox comboBox11;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker2;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker11;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker10;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker9;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker8;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker7;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker6;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker5;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker4;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker3;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePicker12;
        private System.Windows.Forms.DataGridViewImageColumn Delete;
    }
}