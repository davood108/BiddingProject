﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.DMS.Common;

namespace PowerPlantProject
{
    partial class MainForm
    {
        private bool ValidateBiddingDates()
        {
            string msg = "";
            if (lblPlantValue.Text.Trim() == "")
            {
                msg = "Please select the plant(s)";
            }
            else if (datePickerCurrent.IsNull || datePickerBidding.IsNull)
            {
                msg = "Please select current and bidding dates";
            }
            else if (datePickerCurrent.SelectedDateTime > datePickerBidding.SelectedDateTime)
            {
                msg = "Bidding date should be ahead of current date";
            }
            if (msg == "")
                return true;
            else
            {
                MessageBox.Show(msg);
                return false;
            }
        }

        private void btnRunBidding_Click(object sender, EventArgs e)
        {

            if (!ValidateBiddingDates())
                return;

            BiddingStrategyProgressForm frm = new BiddingStrategyProgressForm(this);
            frm.BiddingStrategySettingId = SaveBiddingStrategy();
            frm.Show();

        }



        private void cmbRunSetting_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbRunSetting.SelectedItem.ToString() == "Customize")
                grBoxCustomize.Enabled = true;
            else
                grBoxCustomize.Enabled = false;

        }

        private void btnSaveBidding_Click(object sender, EventArgs e)
        {
            SaveBiddingStrategy();
        }

        private int SaveBiddingStrategy()
        {
            SqlCommand MyCom = new SqlCommand();
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            MyCom.Connection = myConnection;

            //////// Delete previous results
            int BiddingStrategySettingId = 0;

            MyCom.CommandText = "select @id=max(id) from [BiddingStrategySetting] " +
            " where Plant=@Plant AND CurrentDate=@CurrentDate AND BiddingDate=@BiddingDate";

            MyCom.Parameters.Add("@Plant", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@CurrentDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@BiddingDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@id", SqlDbType.Int);
            MyCom.Parameters["@Plant"].Value = lblPlantValue.Text.Trim();
            MyCom.Parameters["@CurrentDate"].Value = new PersianDate(datePickerCurrent.SelectedDateTime).ToString("d"); ;
            MyCom.Parameters["@BiddingDate"].Value = new PersianDate(datePickerBidding.SelectedDateTime).ToString("d"); ; ;
            MyCom.Parameters["@id"].Direction = ParameterDirection.Output;

            MyCom.ExecuteNonQuery();

            try
            {
                BiddingStrategySettingId = int.Parse(MyCom.Parameters["@id"].Value.ToString());
            }
            catch { }

            if (BiddingStrategySettingId != 0)
            {
                MyCom = new SqlCommand();
                MyCom.Connection = myConnection;
                MyCom.CommandText = "Delete from BiddingStrategySetting where id=" + BiddingStrategySettingId.ToString();
                MyCom.ExecuteNonQuery();

                MyCom = new SqlCommand();
                MyCom.Connection = myConnection;
                MyCom.CommandText = "Delete from BiddingStrategyCustomized where FkBiddingStrategySettingId=" + BiddingStrategySettingId.ToString();
                MyCom.ExecuteNonQuery();
            }

            // Insert into BiddingStrategySetting table
            MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            MyCom.CommandText = "insert INTO [BiddingStrategySetting] " +
                "(Plant, CurrentDate, BiddingDate, RunSetting, StrategyBidding, TrainingDays, CostLevel, PeakBid, Status, FlagForecastingPrice, FlagBidAllocation, FlagStrategyBidding)"
                        + " VALUES (@Plant, @CurrentDate, @BiddingDate, @RunSetting, @StrategyBidding, @TrainingDays, @CostLevel, @PeakBid, @Status, @FlagForecastingPrice, @FlagBidAllocation, @FlagStrategyBidding)";

            MyCom.Parameters.Add("@Plant", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@CurrentDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@BiddingDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@RunSetting", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@StrategyBidding", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@PeakBid", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@TrainingDays", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@CostLevel", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@Status", SqlDbType.VarChar);
            MyCom.Parameters.Add("@FlagForecastingPrice", SqlDbType.Bit);
            MyCom.Parameters.Add("@FlagBidAllocation", SqlDbType.Bit);
            MyCom.Parameters.Add("@FlagStrategyBidding", SqlDbType.Bit);

            MyCom.Parameters["@Plant"].Value = lblPlantValue.Text;
            MyCom.Parameters["@CurrentDate"].Value = new PersianDate(datePickerCurrent.SelectedDateTime).ToString("d"); ;
            MyCom.Parameters["@BiddingDate"].Value = new PersianDate(datePickerBidding.SelectedDateTime).ToString("d"); ; ;
            MyCom.Parameters["@RunSetting"].Value = cmbRunSetting.Text;
            MyCom.Parameters["@StrategyBidding"].Value = cmbStrategyBidding.Text;
            MyCom.Parameters["@TrainingDays"].Value = txtTraining.Text;
            MyCom.Parameters["@CostLevel"].Value = cmbCostLevel.Text;
            MyCom.Parameters["@Status"].Value = lstStatus.Text;
            MyCom.Parameters["@PeakBid"].Value = cmbPeakBid.Text;
            MyCom.Parameters["@FlagForecastingPrice"].Value = rdForecastingPrice.Checked;
            MyCom.Parameters["@FlagBidAllocation"].Value = rdBidAllocation.Checked;
            MyCom.Parameters["@FlagStrategyBidding"].Value = rdStrategyBidding.Checked;
            MyCom.ExecuteNonQuery();

            // retrieve the newly inserted id in FinalForcast

            int maxId = 0;

            MyCom.CommandText = "select max(id) from [BiddingStrategySetting]";
            maxId = (int)MyCom.ExecuteScalar();
            if (cmbRunSetting.Text == "Customize")
            {
                for (int j = 1; j <= 24; j++)
                {
                    // Insert into FinalForcastHourly table
                    MyCom = new SqlCommand();
                    MyCom.Connection = myConnection;
                    MyCom.CommandText = "insert INTO [BiddingStrategyCustomized] (FkBiddingStrategySettingId,hour,Strategy, Peak)"
                                + " VALUES (@FkBiddingStrategySettingId,@hour,@Strategy, @Peak)";

                    MyCom.Parameters.Add("@FkBiddingStrategySettingId", SqlDbType.Int);
                    MyCom.Parameters.Add("@hour", SqlDbType.Int);
                    MyCom.Parameters.Add("@Strategy", SqlDbType.NChar, 10);
                    MyCom.Parameters.Add("@Peak", SqlDbType.NChar, 10);

                    ToolStripComboBox cmbStrategy = FindBiddingComboBox("cmbStrategy", j);
                    ToolStripComboBox cmbPeak = FindBiddingComboBox("cmbPeak", j);

                    MyCom.Parameters["@FkBiddingStrategySettingId"].Value = maxId;
                    MyCom.Parameters["@hour"].Value = j;
                    MyCom.Parameters["@Strategy"].Value = cmbStrategy.Text;
                    MyCom.Parameters["@Peak"].Value = cmbPeak.Text;
                    MyCom.ExecuteNonQuery();

                }
            }
            myConnection.Close();
            return maxId;

        }

        private void LoadBiddingStrategy()
        {
            datePickerCurrent.SelectedDateTime = DateTime.Now;
            datePickerBidding.SelectedDateTime = DateTime.Now.AddDays(1);
            lblGencoValue.Text = "Tehran";
            lblPlantValue.Text = "All";



            try
            {
                SqlCommand MyCom = new SqlCommand();
                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                MyCom.Connection = myConnection;
                // Insert into FinalForcast table
                MyCom.CommandText = "select max(id) from [BiddingStrategySetting]";

                int maxId = (int)MyCom.ExecuteScalar();


                MyCom.CommandText = "select * from [BiddingStrategySetting] " +
                    "where id=" + maxId.ToString();

                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;
                Maxda.Fill(MaxDS);

                DataTable dt = MaxDS.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    lblPlantValue.Text = row["Plant"].ToString();
                    datePickerCurrent.SelectedDateTime = PersianDateConverter.ToGregorianDateTime(new PersianDate(row["CurrentDate"].ToString()));
                    datePickerBidding.SelectedDateTime = PersianDateConverter.ToGregorianDateTime(new PersianDate(row["BiddingDate"].ToString()));
                    cmbRunSetting.Text = row["RunSetting"].ToString().Trim();
                    cmbStrategyBidding.Text = row["StrategyBidding"].ToString().Trim();
                    txtTraining.Text = row["TrainingDays"].ToString().Trim();
                    cmbCostLevel.Text = row["CostLevel"].ToString().Trim();
                    cmbPeakBid.Text = row["PeakBid"].ToString().Trim();
                    //lstStatus.Text = row["Status"].ToString().Trim();
                    string strBool = row["FlagForecastingPrice"].ToString().Trim().ToLower();
                    rdForecastingPrice.Checked = (strBool == "true" ? true : false);

                    strBool = row["FlagBidAllocation"].ToString().Trim().ToLower();
                    rdBidAllocation.Checked = (strBool == "true" ? true : false);

                    strBool = row["FlagStrategyBidding"].ToString().Trim().ToLower();
                    rdStrategyBidding.Checked = (strBool == "true" ? true : false);


                    MyCom.CommandText = "select * from [BiddingStrategyCustomized] " +
                        "where FkBiddingStrategySettingId = " + maxId.ToString();
                    MaxDS = new DataSet();
                    Maxda = new SqlDataAdapter();
                    Maxda.SelectCommand = MyCom;
                    Maxda.Fill(MaxDS);

                    dt = MaxDS.Tables[0];
                    foreach (DataRow row2 in dt.Rows)
                    {
                        int hour = Int16.Parse(row2["hour"].ToString().Trim());

                        ToolStripComboBox cmbStrategy = FindBiddingComboBox("cmbStrategy", hour);
                        ToolStripComboBox cmbPeak = FindBiddingComboBox("cmbPeak", hour);

                        cmbStrategy.Text = row2["Strategy"].ToString().Trim();
                        cmbPeak.Text = row2["Peak"].ToString().Trim();
                    }
                }
                myConnection.Close();

            }
            catch
            {
            }

        }

        private ToolStripComboBox FindBiddingComboBox(string type, int index)
        {
            ToolStripComboBox cmb = null;
            cmb = FindBiddingComboBox_2(this.menuStrip1, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip2, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip3, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip4, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip5, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip6, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip7, type, index);
            if (cmb != null)
                return cmb;
            return FindBiddingComboBox_2(this.menuStrip8, type, index);
        }

        private ToolStripComboBox FindBiddingComboBox_2(MenuStrip menuStrip, string type, int index)
        {
            foreach (ToolStripItem item in menuStrip.Items)
            {
                if (item.GetType() == typeof(ToolStripComboBox))
                {
                    ToolStripComboBox cmb = (ToolStripComboBox)item;
                    if (item.Name.ToLower() == type.ToLower() + index.ToString())
                    {
                        return cmb;
                    }

                }
            }
            return null;
        }

        private void betToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormDownloadInterval frm = new FormDownloadInterval();
            frm.Show();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowNewFolderButton = true;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                string path = folderBrowserDialog1.SelectedPath;

                if (lblPlantValue.Text.ToLower() != "all")
                {
                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Connection = new SqlConnection(ConnectionManager.ConnectionString);
                    MyCom.Connection.Open();

                    MyCom.CommandText = "SELECT @num=PPID, @nameFarsi=PNameFarsi FROM PowerPlant WHERE PPName=@name";
                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                    MyCom.Parameters.Add("@name", SqlDbType.NChar, 20);
                    MyCom.Parameters["@num"].Direction = ParameterDirection.Output;
                    MyCom.Parameters["@name"].Value = lblPlantValue.Text.Trim();
                    MyCom.Parameters.Add("@nameFarsi", SqlDbType.NVarChar, 50);
                    MyCom.Parameters["@nameFarsi"].Direction = ParameterDirection.Output;
                    MyCom.ExecuteNonQuery();
                    MyCom.Connection.Close();
                    ExportM002Excel(path,
                        int.Parse(MyCom.Parameters["@num"].Value.ToString().Trim()),
                        MyCom.Parameters["@nameFarsi"].Value.ToString().Trim());
                }
                else
                {
                    DataTable dt = utilities.returntbl("select * from powerplant");
                    foreach (DataRow row in dt.Rows)
                    {
                        ExportM002Excel(path,
                            int.Parse(row["PPID"].ToString().Trim()),
                            row["PNameFarsi"].ToString().Trim());
                    }
                }
            }



            /////////////////
            //Excel.Application exobj1 = new Excel.Application();
            //exobj1.Visible = true;
            //exobj1.UserControl = true;
            //System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            //Excel.Workbook book1 = null;
            //book1 = exobj1.Workbooks.Open("C://test.xls", 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            //book1.Save();
            //book1.Close(true, book1, Type.Missing);
            //exobj1.Workbooks.Close();
            //exobj1.Quit();


            //////////////////


        }

        private void ExportToExcel(string filename, System.Data.DataTable DT, string[] colNames)
        {
            int MaxRowsPerSheet = 1000;
            int rowIndex = 1000;
            int colIndex = 50;
            int sheetIndex = 0;
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            Excel.Application oExcel = new Excel.Application();

            oExcel.SheetsInNewWorkbook = 1;
            Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
            Excel.Worksheet sheet = null;
            Excel.Range range = null;

            foreach (DataRow row in DT.Rows)
            {
                if (rowIndex == MaxRowsPerSheet)
                {
                    rowIndex = 1;
                    colIndex = 1;
                    sheetIndex++;
                    if (sheet != null)
                        sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
                    else
                        sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

                    if (colNames == null)
                    {
                        foreach (DataColumn col in DT.Columns)
                        {
                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 = col.ColumnName.ToString();
                            range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                        }
                    }
                    else
                    {
                        foreach (string str in colNames)
                        {
                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 = str.ToString();
                            range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                        }
                    }
                    rowIndex++;
                    sheet.Name = "Page " + (sheetIndex);
                }
                colIndex = 1;
                foreach (object item in row.ItemArray)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, colIndex];
                    range.Value2 = item.ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    if (item.ToString().Length > 11 && range.NumberFormat != "#")
                    {
                        bool IsPhoneNumber = true;
                        string strItem = item.ToString();
                        for (int i = 0; i < 100000000000; i++)
                        {
                            if (!Char.IsDigit(strItem, i))
                            {
                                IsPhoneNumber = false;
                                break;
                            }
                        }
                        if (IsPhoneNumber)
                            range.EntireColumn.NumberFormat = "#";
                    }
                    colIndex++;
                }
                rowIndex++;
            }
            for (int i = 1; i <= WB.Worksheets.Count; i++)
            {
                sheet = (Excel.Worksheet)WB.Worksheets["Page " + i];
                int num_cols = DT.Columns.Count;
                for (int j = 1; j <= num_cols; j++)
                {
                    range = (Excel.Range)sheet.Cells[1, j];
                    range = range.EntireColumn;
                    range.AutoFit();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
            }
            sheet = ((Excel.Worksheet)WB.Worksheets["Page 1"]);
            sheet.Activate();
            ////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //string filename = page.Server.MapPath("/APP_TPH_local/temp") + "\\" + page.Session.SessionID + ".tmp";
            oExcel.Visible = true;
            //WB.SaveAs(filename, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            //WB.Close(Missing.Value, Missing.Value, Missing.Value);
            //oExcel.Workbooks.Close();
            //oExcel.Quit();

            /////////////////////

            //System.IO.FileStream input = new System.IO.FileStream(filename, System.IO.FileMode.Open);
            //System.IO.Stream output = res.OutputStream;
            //int len = (int)input.Length;
            //byte[] buffer = new byte[len];
            //input.Read(buffer, 0, len);
            //output.Write(buffer, 0, len);
            //input.Close();
            //output.Close();
            //System.IO.File.Delete(filename);
            //DT.Dispose();
            //DT = null;
            //System.Runtime.InteropServices.Marshal.ReleaseComObject(range);
            //System.Runtime.InteropServices.Marshal.ReleaseComObject(sheet);
            //System.Runtime.InteropServices.Marshal.ReleaseComObject(WB);
            //while (System.Runtime.InteropServices.Marshal.ReleaseComObject(oExcel) > 0) ;

            ////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //sheet = null;
            //WB = null;
            //oExcel = null;
            //range = null;
            //GC.Collect();

        }

        private void ExportM002Excel(string path, int ppid, string ppnameFarsi)
        {
            MixUnits mixUnitStatus = MixUnits.All;
            if (ppid == 131 || ppid == 144)
            
            ////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!///
            //if (MultiplePackageType(ppid))
                mixUnitStatus = MixUnits.ExeptCombined;

            string[] headers = { "Block/Unit No", "Peak", "Max. Daily Generation", "Hour", 
                                   "Declared Capacity", "Dispatchable Capacity"};

            string[] firstColValues = { "Form Name :", "Date Of Issue :", "Time Of Issue :", "Target Market Date :",
                                    "Power Plant Name :", "Code :", "Revision :", 
                                    "Filled By :", "Approved By :" };

            do
            {
                int rowIndex = 0;
                int colIndex = 50;

                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                Excel.Application oExcel = new Excel.Application();

                oExcel.SheetsInNewWorkbook = 1;
                Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
                Excel.Worksheet sheet = null;
                Excel.Range range = null;

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers
                rowIndex = 0;
                colIndex = 0;
                int sheetIndex = 1;
                if (sheet != null)
                    sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
                else
                    sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

                sheet.Name = "Page " + (sheetIndex);

                int ppidToPrint = ppid;
                if (mixUnitStatus == MixUnits.JustCombined)// the second one
                    ppidToPrint++;
                string[] secondColValues = { "M0022", 
                                          DateTime.Now.ToString("d"), DateTime.Now.ToString("T"),
                                          datePickerBidding.Text,
                                          ppnameFarsi, ppidToPrint.ToString(), 
                                          "0", "", "" };


                for (int i = 0; i < firstColValues.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[i + 1, 1];
                    range.Value2 = firstColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    range = (Excel.Range)sheet.Cells[i + 1, 2];
                    range.Value2 = secondColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


                rowIndex = firstColValues.Length + 2;
                colIndex = 0;
                for (int i = 0; i < headers.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = headers[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
                for (int i = 1; i <= 10; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = "Power_" + i.ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = "Price_" + i.ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                string strCmd = "select distinct block from DetailFRM002 where ppid='" + ppid.ToString() +
                "' AND TargetMarketDate='" + datePickerBidding.Text + "'";
                if (mixUnitStatus == MixUnits.ExeptCombined)
                    strCmd += " AND block like 'S%'";
                else if (mixUnitStatus == MixUnits.JustCombined)
                    strCmd += " AND block like 'C%'";

                DataTable oDataTable = utilities.returntbl(strCmd);

                string[] blocks = new string[oDataTable.Rows.Count];
                for (int i = 0; i < oDataTable.Rows.Count; i++)
                    blocks[i] = oDataTable.Rows[i][0].ToString().Trim();

                //strCmd = "select * from DetailFRM002 where ppid='" + ppid.ToString() +
                //    "' AND TargetMarketDate='" + datePickerBidding.Text + "' order by block, hour";
                //oDataTable = utilities.returntbl(strCmd);

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                rowIndex = firstColValues.Length + 3;
                foreach (string blockName in blocks)
                {
                    int firstLine = rowIndex;
                    range = (Excel.Range)sheet.Cells[rowIndex, 1];
                    range.Value2 = blockName;
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    ////////!!!!!!! Peak!!!>???????????????

                    strCmd = "select * from DetailFRM002 where ppid='" + ppid.ToString() +
                        "' AND Block='" + blockName +
                        "' AND TargetMarketDate='" + datePickerBidding.Text + "'" +
                        " order by block, hour";
                    oDataTable = utilities.returntbl(strCmd);

                    colIndex = 4;

                    double max = 0;
                    foreach (DataRow row in oDataTable.Rows)
                    {
                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = (int.Parse(row["Hour"].ToString().Trim())).ToString();
                        //range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["DeclaredCapacity"].ToString().Trim();


                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["DispachableCapacity"].ToString().Trim();
                        double dC = double.Parse(row["DispachableCapacity"].ToString().Trim());

                        if (dC > max)
                            max = dC;

                        for (int i = 1; i <= 10; i++)
                        {
                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 = row["Power" + i.ToString()].ToString().Trim();

                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 = row["Price" + i.ToString()].ToString().Trim();
                        }

                        rowIndex++;
                        colIndex = 4;
                    }

                    range = (Excel.Range)sheet.Cells[firstLine, 2];
                    range.Value2 = max.ToString();
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                sheet = ((Excel.Worksheet)WB.Worksheets["Page 1"]);

                range = (Excel.Range)sheet.Cells[1, 1];
                range = range.EntireColumn;
                range.AutoFit();

                range = (Excel.Range)sheet.Cells[1, 2];
                range = range.EntireColumn;
                range.AutoFit();

                sheet.Activate();
                //oExcel.Visible = true;

                string strDate = datePickerBidding.Text;
                strDate = strDate.Remove(7, 1);
                strDate = strDate.Remove(4, 1);
                string filePath = path + "\\" + ppidToPrint.ToString() + "-" + strDate + ".xls";
                //oExcel.Visible = true;
                WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                WB.Close(Missing.Value, Missing.Value, Missing.Value);
                oExcel.Workbooks.Close();
                oExcel.Quit();

                if (mixUnitStatus == MixUnits.ExeptCombined)
                    mixUnitStatus = MixUnits.JustCombined;
                else
                    mixUnitStatus = MixUnits.All;

            } while (mixUnitStatus != MixUnits.All);

        }

        private void UpdateBiddingTab()
        {
            m002Generated = false;
            btnExport.Enabled = m002Generated;
            FillStatusBox();

        }

        private void FillStatusBox()
        {
            PersianDate persianDate = datePickerBidding.SelectedDateTime;
            if (persianDate != null)
            {
                string strDate = PersianDateConverter.ToPersianDate(persianDate).ToString("d");

                lstStatus.Text = "";
                if (lblPlantValue.Text.Trim() != "All" && strDate != "")
                {
                    CheckInfoAvailability();
                    CheckFuelAvailabiliyty();

                }
            }
        }

        private void CheckInfoAvailability()
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);

            myConnection.Open();

            string strDate = PersianDateConverter.ToPersianDate(datePickerCurrent.SelectedDateTime).ToString("d");

            string[] M002Tables = { "MainFRM002", "DetailFRM002", "BlockFRM002" };
            string[] M005Tables = { "MainFRM005", "DetailFRM005", "BlockFRM005" };

            string strCmdFirst = "SELECT @count=count(*) from ";
            string strCmdEnd = " as table1 inner join dbo.PowerPlant on PowerPlant.PPID=table1.PPID" +
                " where table1.TargetMarketDate ='" + strDate + "' AND PowerPlant.PPName='" + lblPlantValue.Text.Trim() + "'";

            bool M002Available = true, M005Available = true;
            for (int i = 0; i < 3 && (M002Available || M005Available); i++)
            {
                SqlCommand command = new SqlCommand(strCmdFirst + M002Tables[i] + strCmdEnd, myConnection);
                command.Parameters.Add("@count", SqlDbType.Int);
                command.Parameters["@count"].Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                int count = int.Parse(command.Parameters["@count"].Value.ToString());
                if (count == 0)
                    M002Available = false;

                command = new SqlCommand(strCmdFirst + M005Tables[i] + strCmdEnd, myConnection);
                command.Parameters.Add("@count", SqlDbType.Int);
                command.Parameters["@count"].Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                count = int.Parse(command.Parameters["@count"].Value.ToString());
                if (count == 0)
                    M005Available = false;

            }
            myConnection.Close();
            if (M002Available)
                lstStatus.Text += "M002 Data Available...\r\n";
            else
                lstStatus.Text += "M002 Data Not Available...\r\n";

            if (M005Available)
                lstStatus.Text += "M005 Data Available...\r\n";
            else
                lstStatus.Text += "M005 Data Not Available...\r\n";

        }

        private void CheckFuelAvailabiliyty()
        {
            string strDate = PersianDateConverter.ToPersianDate(datePickerBidding.SelectedDateTime).ToString("d");

            string strCmd = "select UnitCode,SecondFuel,SecondFuelStartDate,SecondFuelEndDate " +
                " from ConditionUnit inner join PowerPlant on PowerPlant.PPID=ConditionUnit.PPID" +
                " where PowerPlant.ppname='" + lblPlantValue.Text.Trim() + "'" +
                " and SecondFuelStartDate<'" + strDate + "' and '" + strDate + "'<SecondFuelEndDate";

            DataTable oDataTable = utilities.returntbl(strCmd);
            foreach (DataRow myrow in oDataTable.Rows)
            {
                if (myrow["SecondFuel"].ToString() != "" && bool.Parse(myrow["SecondFuel"].ToString()) == true)
                {
                    if (oDataTable.Rows.Count != 0)
                    {
                        //   MessageBox.Show("Test");
                        lstStatus.Text += "*********************units of plant that have second fuel*****************\r\n";
                        lstStatus.Text += myrow["UnitCode"].ToString() + "\r\n";

                        lstStatus.Text += "***************dates of second fuel <<< " + myrow["UnitCode"].ToString().Trim() + " >>> ********************\r\n";
                        lstStatus.Text += myrow["SecondFuelStartDate"].ToString() + "\r\n";

                        lstStatus.Text += myrow["SecondFuelEndDate"].ToString() + "\r\n";
                    }
                }
            }
            ////////////////////
            //lstStatus.Text += "\r\n";
            //lstStatus.Text += "\r\n";

            strCmd = "select UnitCode,Maintenance,MaintenanceStartDate,MaintenanceEndDate " +
                " from ConditionUnit inner join PowerPlant on PowerPlant.PPID=ConditionUnit.PPID" +
                " where PowerPlant.ppname='" + lblPlantValue.Text.Trim() + "'" +
                " and MaintenanceStartDate<'" + strDate + "' and '" + strDate + "'<MaintenanceEndDate";

            oDataTable = utilities.returntbl(strCmd);

            string[] mm = new string[oDataTable.Rows.Count];
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {
                mm[i] = oDataTable.Rows[i][0].ToString();
            }

            foreach (DataRow myrow1 in oDataTable.Rows)
            {

                if (myrow1["Maintenance"].ToString() != "" && bool.Parse(myrow1["Maintenance"].ToString()) == true)
                {
                    lstStatus.Text += "*********************units of plant that have maintenance***************\r\n";
                    lstStatus.Text += myrow1["UnitCode"].ToString() + "\r\n"; ;

                    lstStatus.Text += "***************dates of maintenance <<<" + myrow1["UnitCode"].ToString().Trim() + " >>>*****************\r\n";
                    lstStatus.Text += myrow1["MaintenanceStartDate"].ToString() + "\r\n"; ;

                    lstStatus.Text += myrow1["MaintenanceEndDate"].ToString() + "\r\n"; ;


                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            lstStatus.Text += "\r\n";
            lstStatus.Text += "\r\n";

            strCmd = "select UnitCode,OutService,OutServiceStartDate,OutServiceEndDate" +
                " from ConditionUnit inner join PowerPlant on PowerPlant.PPID=ConditionUnit.PPID" +
                " where PowerPlant.ppname='" + lblPlantValue.Text.Trim() + "'" +
                " and OutServiceStartDate<'" + strDate + "' and '" + strDate + "'<OutServiceEndDate";

            oDataTable = utilities.returntbl(strCmd);

            foreach (DataRow myrow1 in oDataTable.Rows)
            {

                if (bool.Parse(myrow1["OutService"].ToString()) == true)
                {
                    lstStatus.Text += "*********************units of plant that have outservice***************\r\n";
                    lstStatus.Text += myrow1["UnitCode"].ToString() + "\r\n";

                    lstStatus.Text += "***************dates of outservice <<<" + myrow1["UnitCode"].ToString().Trim() + " >>>*****************\r\n";
                    lstStatus.Text += myrow1["OutServiceStartDate"].ToString() + "\r\n";

                    lstStatus.Text += myrow1["OutServiceEndDate"].ToString() + "\r\n";


                }
            }
        }

        private void datePickerBidding_SelectedDateTimeChanged(object sender, EventArgs e)
        {
            UpdateBiddingTab();
        }

        public void EnableBiddingStrategyTab(bool enable)
        {
            grBoxCustomize.Enabled = enable;
            groupBoxBiddingStrategy1.Enabled = enable;
            groupBoxBiddingStrategy2.Enabled = enable;
            groupBoxBiddingStrategy3.Enabled = enable;
        }

        public void EnableBiddingStrategyExportButton(bool enable)
        {
            btnExport.Enabled = enable;
        }
    }
}