﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Common;
using ILOG.Concert;
using ILOG.CPLEX;
using PowerPlantProject;
using FarsiLibrary.Utils;
using NRI.DMS.Common;
namespace PowerPlantProject
{

    public class Classfinal
    {
        int BiddingStrategySettingId;
        string ConStr = "Data Source=.;Initial Catalog=PowerPalntDB;Integrated Security=True";
        string biddingDate;
        string currentDate;
        string ppName;
        private SqlConnection oSqlConnection = null;
        private SqlCommand oSqlCommand = null;
        private SqlDataReader oSqlDataReader = null;
        private DataTable oDataTable = null;
        private string[] plant;
        int Plants_Num;
        int Package_Num;
        int[] Plant_Packages_Num;
        public Classfinal(string PPName, string CurrentDate, string BiddingDate)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();
            biddingDate = BiddingDate;
            currentDate = CurrentDate;
            ppName = PPName.Trim();

            if (ppName.ToLower().Trim() == "all")
            {
                oDataTable = utilities.returntbl("select distinct PPID from UnitsDataMain ");
                Plants_Num = oDataTable.Rows.Count;

                plant = new string[Plants_Num];
                for (int il = 0; il < Plants_Num; il++)
                {
                    plant[il] = oDataTable.Rows[il][0].ToString().Trim();

                }
            }
            else
            {
                SqlCommand MyCom1 = new SqlCommand();
                MyCom1.Connection = myConnection;
                MyCom1.CommandText = "SELECT @num=PPID FROM PowerPlant WHERE PPName=@name";
                MyCom1.Parameters.Add("@num", SqlDbType.NChar, 10);
                MyCom1.Parameters.Add("@name", SqlDbType.NChar, 20);
                MyCom1.Parameters["@num"].Direction = ParameterDirection.Output;
                MyCom1.Parameters["@name"].Value = ppName.Trim();
                MyCom1.ExecuteNonQuery();
               
                Plants_Num = 1;
                plant = new string[1];
                plant[0] = MyCom1.Parameters["@num"].Value.ToString().Trim();


            }
            /////////

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            MyCom.CommandText = "select @id=max(id) from dbo.BiddingStrategySetting " +
                " where plant=@ppname AND  CurrentDate=@curDate AND BiddingDate=@bidDate";
            MyCom.Parameters.Add("@id", SqlDbType.Int);
            MyCom.Parameters.Add("@ppname", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@curDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@bidDate", SqlDbType.Char, 10);
            MyCom.Parameters["@id"].Direction = ParameterDirection.Output;
            MyCom.Parameters["@ppname"].Value = ppName.Trim();
            MyCom.Parameters["@curDate"].Value = CurrentDate;
            MyCom.Parameters["@bidDate"].Value = BiddingDate;
            MyCom.ExecuteNonQuery();
            myConnection.Close();
            BiddingStrategySettingId = int.Parse(MyCom.Parameters["@id"].Value.ToString().Trim());
        }



        //public double[, , ,] powval;
        //public double[, , ,] priceval;
        //public double[, , ] valpmax;

        // public double tot;

        private string buildmaxdate(string[] arrmax)
        {
            string maxdate = null;
            for (int fd = 0; fd < arrmax.Length; fd++)
            {
                if (maxdate == null || string.Compare(maxdate, arrmax[fd]) < 0)
                    maxdate = arrmax[fd];
            }
            return maxdate;

        }

        public void value()
        {

            string strCmd = "select Max (PackageCode) from UnitsDataMain ";
            if (Plants_Num == 1)
                strCmd += " where ppid=" + plant[0];
            oDataTable = utilities.returntbl(strCmd);
             Package_Num = (int)oDataTable.Rows[0][0];


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //Unit Number
            int Units_Num = 0;

            int[] Plant_units_Num = new int[Plants_Num];
            for (int re = 0; re < Plants_Num; re++)
            {
                oDataTable = utilities.returntbl("select UnitCode from UnitsDataMain where PPID='" + plant[re] + "'");

                Plant_units_Num[re] = oDataTable.Rows.Count;
                if (Units_Num <= oDataTable.Rows.Count)
                {
                    Units_Num = oDataTable.Rows.Count;
                }

            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //Fixed Set :
            int Hour = 24;
            int Dec_Num = 4;
            int Data_Num = 20;
            int Start_Num = 48;
            int Mmax = 3;
            int Step_Num = 10;
            int Xtrain = 201;
            int Power_Num = 4;
            int Power_W = 4;
            //powval = new double[Plants_Num, Package_Num, Hour, Step_Num];
            //priceval = new double[Plants_Num, Package_Num, Hour, Step_Num];
            //valpmax = new double[Plants_Num, Package_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            int b = 0;

            // detect max date in database...................................  
            oDataTable = utilities.returntbl("select Date from BaseData");
            int ndate = oDataTable.Rows.Count;
            string[] arrmaxdate1 = new string[ndate];
            for (b = 0; b < ndate; b++)
            {
                arrmaxdate1[b] = oDataTable.Rows[b][0].ToString().Trim();
            }

            string smaxdate = buildmaxdate(arrmaxdate1);

            //end.................................................

            //double[] FuelPrice = new double[3];
            //FuelPrice[0] = 49000;
            //FuelPrice[1] = 43000;
            //FuelPrice[2] = 640000;


            oDataTable = utilities.returntbl("select GasPrice,MazutPrice,GasOilPrice from BaseData where Date like '" + smaxdate + '%' + "'");
            double[] FuelPrice = new double[3];
            for (b = 0; b < 3; b++)
            {
                FuelPrice[b] = double.Parse(oDataTable.Rows[0][b].ToString());
            }

            //double[] CapPrice = new double[1];
            //CapPrice[0] = 110000;

            oDataTable = utilities.returntbl("select max(MarketPriceMax) from BaseData where Date like '" + smaxdate + '%' + "'");
            double[] CapPrice = new double[oDataTable.Rows.Count];
            for (b = 0; b < oDataTable.Rows.Count; b++)
            {

                CapPrice[b] = double.Parse(oDataTable.Rows[b][0].ToString());

            }

            double[] ErrorBid = new double[oDataTable.Rows.Count];
            ErrorBid[0] = CapPrice[0] / 440;
            for (b = 0; b < oDataTable.Rows.Count; b++)
            {
                ErrorBid[b] = CapPrice[b] / 440;

            }


            string[] Run = new string[2];
            Run[0] = "Automatic";
            Run[1] = "customize";


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

             Plant_Packages_Num = new int[Plants_Num];
            for (int re = 0; re < Plants_Num; re++)
            {
                oDataTable = utilities.returntbl("select Max(PackageCode) from UnitsDataMain where  PPID='" + plant[re] + "'");
                Plant_Packages_Num[re] = (int)oDataTable.Rows[0][0];
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,] Package = new string[Plants_Num, Package_Num];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    string cmd = "select PackageType from dbo.UnitsDataMain where PPID=" + plant[j] +
                    " AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%')AND(UnitCode not like 'Gas cc%')))";
                    oDataTable = utilities.returntbl(cmd);
                    Package[j, k] = oDataTable.Rows[k][0].ToString().Trim();
                }

            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,] Unit = new string[Plants_Num, Units_Num];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    oDataTable = utilities.returntbl("select UnitCode from UnitsDataMain where  PPID='" + plant[j] + "'");
                    Unit[j, i] = oDataTable.Rows[i][0].ToString().Trim();

                }

            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[, ,] Unit_Dec = new string[Plants_Num, Units_Num, Dec_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Dec_Num; k++)
                    {
                        switch (k)
                        {
                            case 0:
                                oDataTable = utilities.returntbl("select UnitType  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString().Trim();
                                break;
                            case 1:
                                oDataTable = utilities.returntbl("select PackageType  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString().Trim();
                                break;
                            case 2:
                                Unit_Dec[j, i, k] = "Gas";
                                break;
                            case 3:
                                oDataTable = utilities.returntbl("select  SecondFuel  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString().Trim();
                                break;
                        }
                    }

                }


            }


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Unit_Data = new double[Plants_Num, Units_Num, Data_Num];
            // 0 : Package
            // 1 : RampUp
            // 2 : RampDown
            // 3 : Pmin
            // 4 : pmax
            // 5 : RampStart&shut
            // 6 : MinOn
            // 7 : MinOff
            // 8 :  Am
            // 9 :  Bm
            // 10 : Cm
            // 11 : Bmain
            // 12 : Cmain
            // 13 : Dmain
            // 14 : Variable cost
            // 15 : Fixed Cost
            // 16 : Tcold
            // 17 : Thot
            // 18 : Cold start cost
            // 19 : Hot start cost

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Data_Num; k++)
                    {
                        switch (k)
                        {
                            case 0:
                                oDataTable = utilities.returntbl("select PackageCode  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 1:
                                oDataTable = utilities.returntbl("select RampUpRate  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                Unit_Data[j, i, k] = 60 * Unit_Data[j, i, k];
                                break;
                            case 2:
                                Unit_Data[j, i, k] = Unit_Data[j, i, 1];
                                break;

                            case 3:
                                oDataTable = utilities.returntbl("select PMin  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;

                            case 4:
                                oDataTable = utilities.returntbl("select PMax  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 5:
                                double pow = Unit_Data[j, i, 1];
                                if (Unit_Data[j, i, 3] > pow)
                                {
                                    Unit_Data[j, i, k] = Unit_Data[j, i, 3];
                                }
                                else
                                {
                                    Unit_Data[j, i, k] = pow;
                                }
                                break;
                            case 6:

                                oDataTable = utilities.returntbl("select TUp  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 7:
                                oDataTable = utilities.returntbl("select TDown  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;

                            case 8:

                                oDataTable = utilities.returntbl("select PrimaryFuelAmargin from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 9:
                                oDataTable = utilities.returntbl("select PrimaryFuelBmargin  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 10:

                                oDataTable = utilities.returntbl("select PrimaryFuelCmargin  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 11:
                                oDataTable = utilities.returntbl("select BMaintenance  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString()); ;
                                break;

                            case 12:
                                oDataTable = utilities.returntbl("select CMaintenance  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 13:
                                oDataTable = utilities.returntbl("select DMaintenance  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = Unit_Data[j, i, 12];
                                break;
                            case 14:

                                oDataTable = utilities.returntbl("select VariableCost  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 15:
                                oDataTable = utilities.returntbl("select FixedCost  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;

                            case 16:
                                oDataTable = utilities.returntbl("select TStartCold  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 17:

                                oDataTable = utilities.returntbl("select TStartHot  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 18:
                                oDataTable = utilities.returntbl("select StartUpCostA  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                // Unit_Data[j, i, k] = 20000;
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());

                                break;

                            case 19:
                                oDataTable = utilities.returntbl("select StartUpCostB  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                //  Unit_Data[j, i, k] = 10000;
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;

                        }
                    }

                }

            }

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Data_Num; k++)
                    {
                        if(Unit_Data[j, i, 1]>(Unit_Data[j, i, 4]-Unit_Data[j, i, 3]))
                        {
                            Unit_Data[j, i, 1]=(Unit_Data[j, i, 4]-Unit_Data[j, i, 3]);
                            Unit_Data[j, i, 2] = (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]);
                        }
                    }
                }
            }

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Data_Num; k++)
                    {
                        if (Unit_Data[j, i, 5] > (Unit_Data[j, i, 4]))
                        {
                            Unit_Data[j, i, 5] = (Unit_Data[j, i, 4]);
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Unit_Data[j, i, 0] = Unit_Data[j, i, 0] - 1;
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


            string[] ptimmForPowerLimitedUnit = new string[Plants_Num];

            if (Plants_Num == 1)
            {
                strCmd = " select distinct PPID from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate<'" + biddingDate +
                    "'AND PowerLimitedUnit.EndDate>'" + biddingDate + "'" +
                    " AND PPID=" + plant[0];
                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ptimmForPowerLimitedUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }
            }
            else
            {
                strCmd = " select distinct PPID from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate<'" + biddingDate +
                    "'AND PowerLimitedUnit.EndDate>'" + biddingDate + "'";
                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ptimmForPowerLimitedUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < plann; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ptimmForPowerLimitedUnit[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ptimmForPowerLimitedUnit[i] = temp[i];
            }



            /////////////////////////////////////////////////

            int[] unitname = new int[Plants_Num];

            strCmd = " select UnitCode from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate<'" + biddingDate +
                "'AND PowerLimitedUnit.EndDate>'" + biddingDate;

            for (b = 0; b < Plants_Num; b++)
            {
                string strCmd2 = strCmd +
                    "'and PPID='" + ptimmForPowerLimitedUnit[b] + "'";
                oDataTable = utilities.returntbl(strCmd2);
                unitname[b] = oDataTable.Rows.Count;
            }


            //double[, ,] Limit_Power = new double[AllPlantsNum, maxAllUnits, Hour];
            double[, ,] Limit_Power = new double[Plants_Num, Units_Num, Hour];


            for (int j = 0; j < Plants_Num; j++)
            //for (int j = 0; j < AllPlantsNum; j++)
            {

                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        strCmd = " select Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24" +
                            "  from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate<'" + biddingDate +
                            "' AND PowerLimitedUnit.EndDate>'" + biddingDate +
                            "' and PPID='" + ptimmForPowerLimitedUnit[j] + "'" +
                        " and UnitCode='" + Unit[j, i] + "'";

                        oDataTable = utilities.returntbl(strCmd);
                        foreach (DataRow res in oDataTable.Rows)
                        {
                            Limit_Power[j, i, h] = double.Parse(res[h].ToString());
                        }

                        //if (oDataTable.Rows.Count == 0)
                        //{
                        //    Limit_Power[j, i, h] = 0;
                        //}
                        //else
                        //{
                        //    Limit_Power[j, i, h] = double.Parse(oDataTable.Rows[i][h].ToString());
                        //}
                    }
                }
            }
            ////////////////////////////////

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Limit_Power[j, i, h] > Unit_Data[j, i, 4])
                       {
                           Limit_Power[j, i, h] = (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]);
                       }
                    }
                }
            }

            double[, ,] Pmax_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Pmax_Plant_unit[j, i, h] = (Unit_Data[j, i, 4] - Limit_Power[j, i, h]);
                    }

                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Outservice_Plant_unit = new int[Plants_Num, Units_Num];
            string[] ppkForConditionUnit = new string[Plants_Num];

            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where( ConditionUnit.OutServiceStartDate<'" + biddingDate +
                    "'AND ConditionUnit.OutServiceEndDate>'" + biddingDate +
                    "')or( dbo.ConditionUnit.MaintenanceStartDate<'" + biddingDate +
                    "'and dbo.ConditionUnit.MaintenanceEndDate>'" + biddingDate + "')";
                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ppkForConditionUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < plann; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppkForConditionUnit[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ppkForConditionUnit[i] = temp[i];
            }
            else
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where ((ConditionUnit.OutServiceStartDate<'" + biddingDate +
                    "'AND ConditionUnit.OutServiceEndDate>'" + biddingDate +
                    "')or (dbo.ConditionUnit.MaintenanceStartDate<'" + biddingDate +
                    "'and dbo.ConditionUnit.MaintenanceEndDate>'" + biddingDate + "'" +
                    " ))AND PPID=" + plant[0];
                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ppkForConditionUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }
            }

            ///////////////////////
            int[] uukForConditionUnit = new int[Plants_Num];
            strCmd = " select UnitCode from dbo.ConditionUnit where(( ConditionUnit.OutServiceStartDate<'" + biddingDate +
                       "'AND ConditionUnit.OutServiceEndDate>'" + biddingDate +
                       "')or( dbo.ConditionUnit.MaintenanceStartDate<'" + biddingDate +
                       "'and dbo.ConditionUnit.MaintenanceEndDate>'" + biddingDate;

            for (b = 0; b < Plants_Num; b++)
            {
                string strCmd2 = strCmd +
                    "'))and PPID='" + ppkForConditionUnit[b] + "'";
                oDataTable = utilities.returntbl(strCmd2);
                uukForConditionUnit[b] = oDataTable.Rows.Count;
            }


            //----------------------------------------------------------------------





            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    strCmd = " select Maintenance,OutService from dbo.ConditionUnit where ((ConditionUnit.OutServiceStartDate<'" + biddingDate +
                         "'AND ConditionUnit.OutServiceEndDate>'" + biddingDate + "') or (dbo.ConditionUnit.MaintenanceStartDate<'" + biddingDate +
                         "'and dbo.ConditionUnit.MaintenanceEndDate>'" + biddingDate +
                         "')) and PPID='" + ppkForConditionUnit[j] + "'" +
                         " and UnitCode='" + Unit[j, i] + "'";
                    oDataTable = utilities.returntbl(strCmd);
                    foreach (DataRow res in oDataTable.Rows)
                    {
                        if (bool.Parse(res[0].ToString()) || bool.Parse(res[1].ToString()))
                        {
                            Outservice_Plant_unit[j, i] = 1;
                        }
                    }

                }
            }



            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Outservice = new int[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if (((int)Unit_Data[j, i, 0] == k))
                        {
                            Outservice[j, k] = Outservice[j, k] + Outservice_Plant_unit[j, i];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] service_Max = new double[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if (((int)Unit_Data[j, i, 0] == k))
                        {
                            if (Outservice_Plant_unit[j, i] == 1)
                            {
                                service_Max[j, k] = service_Max[j, k] + Pmax_Plant_unit[j, i, 20];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,] Outservice_Plant_Pack = new string[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    Outservice_Plant_Pack[j, k] = "No";
                    if ((Package[j, k].ToString().Trim() == "CC") & (Outservice[j, k] > 2))
                    {
                        Outservice_Plant_Pack[j, k] = "Yes";
                    }
                    if ((Package[j, k].ToString().Trim() != "CC") & (Outservice[j, k] > 0))
                    {
                        Outservice_Plant_Pack[j, k] = "Yes";
                    }
                }
            }




            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] SecondFuel_Plant_unit = new int[Plants_Num, Units_Num];
            string[] ppkForSecondFuel = new string[Plants_Num];

            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'";
                oDataTable = utilities.returntbl(strCmd);

                int ppkn1 = oDataTable.Rows.Count;

                for (b = 0; b < oDataTable.Rows.Count; b++)
                    ppkForSecondFuel[b] = oDataTable.Rows[b][0].ToString().Trim();

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < ppkn1; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppkForSecondFuel[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ppkForSecondFuel[i] = temp[i];

            }
            else
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'" +
                    " AND PPID=" + plant[0];
                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                {
                    ppkForSecondFuel[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

            }

            /////////////////////////////////////

            int[] uukForSecondFuel = new int[Plants_Num];
            strCmd = " select UnitCode  from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate;

            for (b = 0; b < Plants_Num; b++)
            {
                string strCmd2 = strCmd + "'and PPID='" + ppkForSecondFuel[b] + "'";
                oDataTable = utilities.returntbl(strCmd2);
                uukForSecondFuel[b] = oDataTable.Rows.Count;

            }



            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    strCmd = " select SecondFuel  from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                        "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate +
                        "'and PPID='" + ppkForSecondFuel[j] + "'" +
                        " and UnitCode='" + Unit[j, i] + "'";

                    oDataTable = utilities.returntbl(strCmd);
                    foreach (DataRow res in oDataTable.Rows)
                    {
                        if (bool.Parse(res[0].ToString()))
                        {

                            SecondFuel_Plant_unit[j, i] = 1;
                        }
                    }

                    

                    //oDataTable = utilities.returntbl(strCmd);
                    //if (oDataTable.Rows.Count == 0)
                    //{
                    //    SecondFuel_Plant_unit[j, i] = 0;
                    //}
                    //else
                    //{
                    //    if (bool.Parse(oDataTable.Rows[i][0].ToString()))
                    //    {
                    //        SecondFuel_Plant_unit[j, i] = 1;
                    //    }
                    //    else SecondFuel_Plant_unit[j, i] = 0;
                    //}
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] FuelQuantity_Plant_unit = new double[Plants_Num, Units_Num];
            string[] ppForQuantity = new string[Plants_Num];

            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'";
                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                    ppForQuantity[b] = oDataTable.Rows[b][0].ToString().Trim();

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < oDataTable.Rows.Count; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppForQuantity[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ppForQuantity[i] = temp[i];


            }
            else
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                         "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'" +
                         " AND PPID = " + plant[0];
                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                    ppForQuantity[b] = oDataTable.Rows[b][0].ToString().Trim();
            }


            int[] uuForQuantity = new int[Plants_Num];
            for (b = 0; b < Plants_Num; b++)
            {
                strCmd = " select UnitCode from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate +
                    "'and PPID='" + ppForQuantity[b] + "'";

                oDataTable = utilities.returntbl(strCmd);
                uuForQuantity[b] = oDataTable.Rows.Count;
            }


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    strCmd = " select FuelForOneDay from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                        "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate +
                        "'and PPID='" + ppForQuantity[j] + "'" +
                         " and UnitCode='" + Unit[j, i] + "'";
                    oDataTable = utilities.returntbl(strCmd);

                    foreach (DataRow res in oDataTable.Rows)
                    {

                        FuelQuantity_Plant_unit[j, i] = double.Parse(res[0].ToString());

                    }





                    //oDataTable = utilities.returntbl(strCmd);
                    //if (oDataTable.Rows.Count == 0)
                    //{
                    //    FuelQuantity_Plant_unit[j, i] = 0;
                    //}

                    //else
                    //    FuelQuantity_Plant_unit[j, i] = double.Parse(oDataTable.Rows[i][0].ToString());
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            
            string[] ppFuelQuantity = new string[Plants_Num];

            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID from dbo.ConditionPlant where ConditionPlant.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionPlant.SecondFuelEndDate>'" + biddingDate + "'";
                oDataTable = utilities.returntbl(strCmd);
                for (b = 0; b < oDataTable.Rows.Count; b++)
                {
                    ppFuelQuantity[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < oDataTable.Rows.Count; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppFuelQuantity[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ppFuelQuantity[i] = temp[i];

            }
            else
            {
                strCmd = " select distinct PPID from dbo.ConditionPlant where ConditionPlant.SecondFuelStartDate<'" + biddingDate +
                        "'AND ConditionPlant.SecondFuelEndDate>'" + biddingDate + "'" + 
                        " AND PPID=" + plant[0];
                oDataTable = utilities.returntbl(strCmd);
                for (b = 0; b < oDataTable.Rows.Count; b++)
                {
                    ppFuelQuantity[b] = oDataTable.Rows[b][0].ToString().Trim();
                }
            }

            double[] FuelQuantity_Plant = new double[Plants_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                strCmd = " select FuelQuantity from dbo.ConditionPlant where ConditionPlant.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionPlant.SecondFuelEndDate>'" + biddingDate +
                    "'AND PPID='" + ppFuelQuantity[j] + "'";
                oDataTable = utilities.returntbl(strCmd);
              
              if (oDataTable.Rows.Count>0) 
                  FuelQuantity_Plant[j] = double.Parse(oDataTable.Rows[0][0].ToString());
                

            }


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            double[] LoadForecasting = GetNearestLoadForecastingDateVlues(biddingDate);

            double[] LoadAverage = new double[Hour];
            LoadAverage[0] = LoadForecasting[1];
            for (int h = 1; h < Hour; h++)
            {
                LoadAverage[h] = LoadForecasting[h] + LoadAverage[(h - 1)];
            }

            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------
            //           Price        //
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------

            //**************************************************************************
            //int row = 0;
            //Price forecasting :
            double[, ,] PriceForecastings = new double[Plants_Num, Hour, 2];

            bool[] forcatingAvailable = new bool[Plants_Num];

            for (int j = 0; j < Plants_Num; j++)
            {
                
                //row = 0;
                if (ppName=="Montazer Qaem" || ppName=="Shahid Rajayi" || ppName=="Montazer Qaem" || ppName=="Shahid Rajayi" || j == 2 || j == 5)
                {
                    string strCommand = "select FinalForecastHourly.forecast from FinalForecastHourly " +
                            " inner join FinalForecast " +
                            " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id  " +
                            " where FinalForecast.PPId =  " + plant[j] +
                            " AND FinalForecast.date = '" + biddingDate + "'";

                    oDataTable = utilities.returntbl(strCommand + " AND MixUnitStatus = 'JustCombined' ORDER BY FinalForecastHourly.hour");
                    PriceForecastings = InsertToPriceForecasting(oDataTable, PriceForecastings, j, 0);

                    oDataTable = utilities.returntbl(strCommand + " AND MixUnitStatus = 'ExeptCombined' ORDER BY FinalForecastHourly.hour");
                    PriceForecastings = InsertToPriceForecasting(oDataTable, PriceForecastings, j, 1);
                    // Add to row PriceForecastings[j,hour,1];
                }
                else
                {
                    string strCommand = "select FinalForecastHourly.forecast from FinalForecastHourly " +
                            "inner join FinalForecast " +
                            "on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id  " +
                            "where FinalForecast.PPId =  " + plant[j] +
                            "AND FinalForecast.date = '" + biddingDate + "'" +
                            " ORDER BY FinalForecastHourly.hour";
                    oDataTable = utilities.returntbl(strCommand);
                    PriceForecastings = InsertToPriceForecasting(oDataTable, PriceForecastings, j, 0);
                    // Add to row PriceForecastings[j,hour,0];
                }

                forcatingAvailable[j] = (oDataTable.Rows.Count > 0 ? true : false);

                for (int h = 0; h < Hour; h++)
                {
                    for (int row = 0; row < 2; row++)
                        if (PriceForecastings[j, h, row] > CapPrice[0])
                            PriceForecastings[j, h, row] = CapPrice[0];
                }
            }


            double[, ,] PriceForecasting = new double[Plants_Num, Package_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ( ppName=="Montazer Qaem" || ppName=="Shahid Rajayi" || j == 2 || j == 5)
                        {
                            if (Package[j, k] == "CC")
                            {
                                PriceForecasting[j, k, h] = PriceForecastings[j, h, 0];
                            }
                            else
                                PriceForecasting[j, k, h] = PriceForecastings[j, h, 1];
                        }
                        else
                            PriceForecasting[j, k, h] = PriceForecastings[j, h, 0];
                    }

                }
            }
            //**************************************************************************
            //**************************************************************************
            //Price forecasting: pd_range :
            //int row1;

            double[, , ,] pd_ranges = new double[Plants_Num, Hour, Xtrain, 2]; // form hamidi
            double[, , ,] pd_range = new double[Plants_Num, Package_Num, Hour, Xtrain]; // to Khorsand

            for (int j = 0; j < Plants_Num; j++)
            {
                if (ppName == "Montazer Qaem" || ppName == "Shahid Rajayi" || j == 2 || j == 5)
                {
                    double[,] result = Get_pd_Range(plant[j], MixUnits.JustCombined, biddingDate);
                    for (int h = 0; h < 24; h++)
                        for (int k = 0; k < Xtrain; k++)
                            pd_ranges[j, h, k, 0] = result[h, k];

                    result = Get_pd_Range(plant[j], MixUnits.ExeptCombined, biddingDate);
                    for (int h = 0; h < 24; h++)
                        for (int k = 0; k < Xtrain; k++)
                            pd_ranges[j, h, k, 1] = result[h, k];

                }
                else
                {
                    double[,] result = Get_pd_Range(plant[j], MixUnits.All, biddingDate);
                    for (int h = 0; h < 24; h++)
                        for (int k = 0; k < Xtrain; k++)
                            pd_ranges[j, h, k, 0] = result[h, k];
                }

            }



            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            if (ppName == "Montazer Qaem" || ppName == "Shahid Rajayi" || j == 2 || j == 5)
                            {
                                if (Package[j, k] == "CC")
                                {
                                    pd_range[j, k, h, x] = pd_ranges[j, h, x, 0];
                                }
                                else
                                    pd_range[j, k, h, x] = pd_ranges[j, h, x, 1];
                            }
                            else
                                pd_range[j, k, h, x] = pd_ranges[j, h, x, 0];

                        }
                    }

                }
            }
            //**************************************************************************
            //**************************************************************************
            //Price forecasting : pdist:

            double[, , ,] pdists = new double[Plants_Num, Hour, Xtrain, 2]; // from hamidi
            double[, , ,] pdist = new double[Plants_Num, Package_Num, Hour, Xtrain]; // to Khorsand

            for (int j = 0; j < Plants_Num; j++)
            {
                if (ppName == "Montazer Qaem" || ppName == "Shahid Rajayi" || j == 2 || j == 5)
                {
                    double[,] result = Get_pdist(plant[j], MixUnits.JustCombined, biddingDate);
                    for (int h = 0; h < 24; h++)
                        for (int k = 0; k < Xtrain; k++)
                            pdists[j, h, k, 0] = result[h, k];

                    result = Get_pdist(plant[j], MixUnits.ExeptCombined, biddingDate);
                    for (int h = 0; h < 24; h++)
                        for (int k = 0; k < Xtrain; k++)
                            pdists[j, h, k, 1] = result[h, k];

                }
                else
                {
                    double[,] result = Get_pdist(plant[j], MixUnits.All, biddingDate);
                    for (int h = 0; h < 24; h++)
                        for (int k = 0; k < Xtrain; k++)
                            pdists[j, h, k, 0] = result[h, k];
                }

            }


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            if (ppName == "Montazer Qaem" || ppName == "Shahid Rajayi" || j == 2 || j == 5)
                            {
                                if (Package[j, k] == "CC")
                                {
                                    pdist[j, k, h, x] = pdists[j, h, x, 0];
                                }
                                else
                                    pdist[j, k, h, x] = pdists[j, h, x, 1];
                            }
                            else
                                pdist[j, k, h, x] = pdists[j, h, x, 0];

                        }
                    }

                }
            }

            ////////////////////////////////////////////

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] UL_State = new string[Plants_Num, Package_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        UL_State[j, k, h] = "N";
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            double[, ,] variances = new double[Plants_Num, Hour, 2]; // from hamidi
            double[, ,] Variance = new double[Plants_Num, Package_Num, Hour]; // to Khorsand

            for (int j = 0; j < Plants_Num; j++)
            {
                if (ppName=="Montazer Qaem" || ppName=="Shahid Rajayi" || j == 2 || j == 5)
                {
                    double[] result = GetVariance(plant[j], MixUnits.JustCombined, biddingDate);
                    for (int h = 0; h < 24; h++)
                        variances[j, h, 0] = result[h];

                    result = GetVariance(plant[j], MixUnits.ExeptCombined, biddingDate);
                    for (int h = 0; h < 24; h++)
                        variances[j, h, 1] = result[h];

                }
                else
                {
                    double[] result = GetVariance(plant[j], MixUnits.All, biddingDate);
                    for (int h = 0; h < 24; h++)
                        variances[j, h, 0] = result[h];
                }

            }


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (ppName=="Montazer Qaem" || ppName=="Shahid Rajayi" || j == 2 || j == 5)
                        {
                            if (Package[j, k] == "CC")
                            {
                                Variance[j, k, h] = variances[j, h, 0];
                            }
                            else
                                Variance[j, k, h] = variances[j, h, 1];
                        }
                        else
                            Variance[j, k, h] = variances[j, h, 0];


                    }

                }
            }

            /////////////////////////////////////////////

            double[,] Sum_Variance = new double[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Sum_Variance[j, k] = Sum_Variance[j, k] + Variance[j, k, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Average_Variance = new double[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    Average_Variance[j, k] = (Sum_Variance[j, k] / 24);
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Max_Variance = new double[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Max_Variance[j, k] < Variance[j, k, h])
                        {
                            Max_Variance[j, k] = Variance[j, k, h];
                        }
                    }
                }
            }
            //*************************************************************************
            double[, ,] Dispatch_Proposal = new double[Plants_Num, Package_Num, Hour];
            //*************************************************************************
            double[, ,] Price_UL = new double[Plants_Num, Package_Num, Hour];
            double[, ,] Price_optimal = new double[Plants_Num, Package_Num, Hour];
            double[, ,] Price_Margin = new double[Plants_Num, Package_Num, Hour];
            //*************************************************************************
            double[, , ,] Price = new double[Plants_Num, Package_Num, Hour, Step_Num];
            double[, , ,] Power = new double[Plants_Num, Package_Num, Hour, Step_Num];
            //*************************************************************************
            double[, ,] Power_memory = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Power_if = new double[Plants_Num, Units_Num, Hour];
            //*************************************************************************
            double[, ,] Power_Error = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Power_Error_Pack = new double[Plants_Num, Package_Num, Hour];
            //*************************************************************************
            int[, , ,] X_select = new int[Plants_Num, Package_Num, Hour, Power_Num];
            //*************************************************************************
            double[, ,] Power_memory_Pack = new double[Plants_Num, Package_Num, Hour];
            double[, ,] Power_if_Pack = new double[Plants_Num, Package_Num, Hour];
            double[, ,] Power_if_CC = new double[Plants_Num, Package_Num, Hour];
            double[, ,] Test = new double[Plants_Num, Units_Num, Hour];
            //**************************************************************************
            int[,,] CO_steam = new int[Plants_Num, Units_Num,Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        CO_steam[j, i, h] = 1;
                        if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam") && (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                        {
                            CO_steam[j, i, h] = 2;
                        }
                    }
                }
            }

            //**************************************************************************
            int[, ,] CO_Dispatch = new int[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        CO_Dispatch[j, i, h] = 2;
                    }
                }
            }

            //**************************************************************************
            string[] Level = new string[Plants_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                Level[j] = "Level1";
                if (plant[j] == "104")
                {
                    Level[j] = "Level2";
                }
            }

            int[] Cost_Level = new int[Plants_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                Cost_Level[j] = 0;
                if (Level[j].ToString().TrimEnd() == "Level1")
                {
                    Cost_Level[j] = 0;
                }
                if (Level[j].ToString().TrimEnd() == "Level2")
                {
                    Cost_Level[j] = 50;
                }
                if (Level[j].ToString().TrimEnd() == "Level3")
                {
                    Cost_Level[j] = 75;
                }
                if (Level[j].ToString().TrimEnd() == "Level4")
                {
                    Cost_Level[j] = 100;
                }
                if (Level[j].ToString().TrimEnd() == "Level5")
                {
                    Cost_Level[j] = 125;
                }
                if (Level[j].ToString().TrimEnd() == "Level6")
                {
                    Cost_Level[j] = 150;
                }
                if (Level[j].ToString().TrimEnd() == "Level7")
                {
                    Cost_Level[j] = 175;
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Pmax_Plant_Pack = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k) && (Outservice_Plant_unit[j, i] == 0))
                            {
                                Pmax_Plant_Pack[j, k, h] = Pmax_Plant_Pack[j, k, h] + Pmax_Plant_unit[j, i, h];
                            }
                        }
                    }
                }
            }
            //////////////////////////////////////////////////////////////////////////////////////////////
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Number_Count_up = new int[Plants_Num, Units_Num];
            int[,] Number_Count_down = new int[Plants_Num, Units_Num];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] History_Require = new double[Plants_Num, Package_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] History_Dispatch = new double[Plants_Num, Package_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] History_UL = new string[Plants_Num, Package_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


            string[,] blocks = Getblock();

            for (int j = 0; j < Plants_Num; j++)
            {


                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    oDataTable = utilities.returntbl("select Required,Dispatchable,Contribution  from dbo.DetailFRM005 where PPID='" + plant[j] +
                    "'and TargetMarketDate='" + currentDate +
                    "'and Block='" + blocks[j,k] + "'");
                    int r = oDataTable.Rows.Count;
                    for (int h = 0; h < Hour; h++)
                    {
                        History_Require[j, k, h] = double.Parse(oDataTable.Rows[h][0].ToString());
                        History_Dispatch[j, k, h] = double.Parse(oDataTable.Rows[h][1].ToString());
                        History_UL[j, k, h] = oDataTable.Rows[h][2].ToString().Trim();
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (History_UL[j, k, h] == "UL")
                        {
                           UL_State[j, k, h] = "UL";
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            int test_n = 0;
            int[, ,] OneGas = new int[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        OneGas[j, k, h] = 0;
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        if (((int)Unit_Data[j, i, 0] == k))
                        {
                            test_n = 0;
                            if (Package[j, k].ToString().Trim() != "CC")
                            {
                                for (int h = 1; h < Hour; h++)
                                {
                                    if ((History_Require[j, k, Hour - h] > 0) & (test_n == 0))
                                    {
                                        Number_Count_up[j, i] = h;
                                    }
                                    else
                                    {
                                        test_n = 1;
                                    }
                                }
                            }
                            if (Package[j, k].ToString().Trim() == "CC")
                            {
                                for (int h = 1; h < Hour; h++)
                                {
                                    if ((History_Require[j, k, Hour - h] > 0) & (test_n == 0) & (History_Dispatch[j, k, Hour - h] < (Pmax_Plant_Pack[j, k, Hour - h]) / (1.8)) & (OneGas[j, k, h] == 0))
                                    {
                                        OneGas[j, k, h] = 1;
                                        Number_Count_up[j, i] = h;
                                    }
                                    if ((History_Require[j, k, Hour - h] > 0) & (test_n == 0) & (History_Dispatch[j, k, Hour - h] > (Pmax_Plant_Pack[j, k, Hour - h]) / (1.8)))
                                    {
                                        Number_Count_up[j, i] = h;
                                    }
                                    if ((History_Require[j, k, Hour - h] == 0))
                                    {
                                        test_n = 1;
                                    }
                                }
                                if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                                {
                                    Number_Count_up[j, i] = 23;
                                }
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        OneGas[j, k, h] = 0;
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if (((int)Unit_Data[j, i, 0] == k))
                        {
                            test_n = 0;
                            if (Package[j, k].ToString().Trim() != "CC")
                            {
                                for (int h = 1; h < Hour; h++)
                                {
                                    if ((History_Require[j, k, Hour - h] == 0) & (test_n == 0))
                                    {
                                        Number_Count_down[j, Plant_units_Num[j] - i - 1] = h;
                                    }
                                    else
                                    {
                                        test_n = 1;
                                    }
                                }
                            }
                            if (Package[j, k].ToString().Trim() == "CC")
                            {
                                for (int h = 1; h < Hour; h++)
                                {
                                    if ((History_Require[j, k, Hour - h] > 0) & (test_n == 0) & (History_Dispatch[j, k, Hour - h] < (Pmax_Plant_Pack[j, k, Hour - h]) / (2.1)) & (OneGas[j, k, h] == 0))
                                    {
                                        OneGas[j, k, h] = 1;
                                        Number_Count_down[j, Plant_units_Num[j] - i - 1] = h;
                                    }
                                    if ((History_Require[j, k, Hour - h] == 0) & (test_n == 0))
                                    {
                                        Number_Count_down[j, Plant_units_Num[j] - i - 1] = h;
                                    }
                                    if ((History_Dispatch[j, k, Hour - h] > (Pmax_Plant_Pack[j, k, Hour - h]) / (2.1)))
                                    {
                                        test_n = 1;
                                    }
                                }
                                if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                                {
                                    Number_Count_down[j, i] = 0;
                                }
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if ((Number_Count_down[j, i] > 0) & (Number_Count_up[j, i] > 0))
                    {
                        Number_Count_down[j, i] = 0;
                    }
                }
            }

            int[,] hisstart_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_up[j, i] < Unit_Data[j, i, 16])
                    {
                        hisstart_Plant_unit[j, i] = Number_Count_up[j, i];
                    }
                    if (Number_Count_up[j, i] >= Unit_Data[j, i, 16])
                    {
                        hisstart_Plant_unit[j, i] = (int)(Unit_Data[j, i, 16] - 1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] HisShut_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_down[j, i] < Unit_Data[j, i, 16])
                    {
                        HisShut_Plant_unit[j, i] = Number_Count_down[j, i];
                    }
                    if (Number_Count_down[j, i] >= Unit_Data[j, i, 16])
                    {
                        HisShut_Plant_unit[j, i] = (int)(Unit_Data[j, i, 16] - 1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Powerhistory_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_up[j, i] >= 0)
                    {
                        Powerhistory_Plant_unit[j, i] = (int)Unit_Data[j, i, 3];
                    }
                    if (Number_Count_down[j, i] > 0)
                    {
                        Powerhistory_Plant_unit[j, i] = 0;
                    }
                }
            }
            int[,] V_history_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_up[j, i] >= 0)
                    {
                        V_history_Plant_unit[j, i] = 1;
                    }
                    if (Number_Count_down[j, i] > 0)
                    {
                        V_history_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Hisup_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if ((Number_Count_up[j, i] < Unit_Data[j, i, 6]) & (Number_Count_up[j, i] != 0))
                    {
                        Hisup_Plant_unit[j, i] = (int)Unit_Data[j, i, 6] - Number_Count_up[j, i];
                    }
                    if (Number_Count_up[j, i] >= Unit_Data[j, i, 6])
                    {
                        Hisup_Plant_unit[j, i] = 0;
                    }
                    if (Number_Count_up[j, i] == 0)
                    {
                        Hisup_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Hisdown_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if ((Number_Count_down[j, i] < Unit_Data[j, i, 7]) & (Number_Count_down[j, i] > 0))
                    {
                        Hisdown_Plant_unit[j, i] = (int)Unit_Data[j, i, 7] - Number_Count_down[j, i];
                    }
                    if (Number_Count_down[j, i] >= Unit_Data[j, i, 7])
                    {
                        Hisdown_Plant_unit[j, i] = 0;
                    }
                    if (Number_Count_down[j, i] == 0)
                    {
                        Hisdown_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[] ppMinPower = new string[Plants_Num];
            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID from dbo.ConditionPlant where ConditionPlant.OutServiceStartDate<'" + biddingDate +
                    "'AND ConditionPlant.OutServiceEndDate>'" + biddingDate + "'";
                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                {
                    ppMinPower[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < oDataTable.Rows.Count; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppMinPower[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ppMinPower[i] = temp[i];
            }

            else
            {
                strCmd = " select distinct PPID from dbo.ConditionPlant where ConditionPlant.OutServiceStartDate<'" + biddingDate +
                        "'AND ConditionPlant.OutServiceEndDate>'" + biddingDate + "'" + 
                        " AND PPID=" + plant[0];
                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                {
                    ppMinPower[b] = oDataTable.Rows[b][0].ToString().Trim();
                }
            }


            double[,] Min_Power = new double[Plants_Num, Hour];


            for (int j = 0; j < Plants_Num; j++)
            {
               strCmd = " select PowerMinHour1,PowerMinHour2,PowerMinHour3,PowerMinHour4,PowerMinHour5,PowerMinHour6,PowerMinHour7,PowerMinHour8,PowerMinHour9,PowerMinHour10,PowerMinHour11,PowerMinHour12,PowerMinHour13,PowerMinHour14,PowerMinHour15,PowerMinHour16,PowerMinHour17,PowerMinHour18,PowerMinHour19,PowerMinHour20,PowerMinHour21,PowerMinHour22,PowerMinHour23,PowerMinHour24 from dbo.ConditionPlant" + 
                    " where ConditionPlant.PowerMinStartDate<'" + biddingDate +
                    "'AND ConditionPlant.PowerMinEndDate>'" + biddingDate +
                    "'and PPID='" + ppMinPower[j] + "'";
               oDataTable = utilities.returntbl(strCmd);

                if (oDataTable.Rows.Count >0)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Min_Power[j, h] = double.Parse(oDataTable.Rows[0][h].ToString());
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[] Price_Min = new double[Hour];
            ///////////////////////

            string CommandText5 = "select Date,AcceptedMin from AveragePrice where Date<='" + biddingDate.Trim() + "'";
            oDataTable = utilities.returntbl(CommandText5);


            DateTime dtDate = PersianDateConverter.ToGregorianDateTime(biddingDate);
            DataRow rowNearest = null;
            TimeSpan minSpan = new TimeSpan();

            foreach (DataRow row in oDataTable.Rows)
            {
                DateTime tempDate = PersianDateConverter.ToGregorianDateTime
                    (row["Date"].ToString().Trim());
                TimeSpan tempSpan = dtDate - tempDate;
                if (rowNearest == null || minSpan > tempSpan)
                {
                    minSpan = tempSpan;
                    rowNearest = row;
                }
            }

            if (rowNearest != null)
            {
                string date2 = rowNearest["Date"].ToString().Trim();

                //////////////////////////////////////////



                string commandtext6 = "select AcceptedMin from AveragePrice where Date='" + date2.ToString().Trim() + "'";
                oDataTable = utilities.returntbl(commandtext6);


                for (int h = 0; h < 24 && h < oDataTable.Rows.Count; h++)
                {

                    Price_Min[h] = double.Parse(oDataTable.Rows[h][0].ToString());
                }

            }


            ///////////////////////
            //strCmd = " select AcceptedMin from AveragePrice where Date='" + biddingDate + "'";
            //oDataTable = utilities.returntbl(strCmd);

            //for (int h = 0; h < Hour && h < oDataTable.Rows.Count; h++)
            //{
            //    Price_Min[h] = double.Parse(oDataTable.Rows[h][0].ToString().Trim());
            //}
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
           
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
           

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Select_Hour_Cap = new string[Hour, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((PriceForecasting[j, k, h] > (CapPrice[0] - 500) | (LoadForecasting[h] > (LoadAverage[23] / 24))))
                        {
                            Select_Hour_Cap[j, k, h] = "Yes";
                        }
                        else
                        {
                            Select_Hour_Cap[j, k, h] = "NO";
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Hour_Cap_Automatic = new string[Plants_Num, Package_Num, Hour];
            string[, ,] Hour_Cap_customize = new string[Plants_Num, Package_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Hour_State_Automatic = new string[Plants_Num, Package_Num, Hour];
            string[, ,] Hour_State_customize = new string[Plants_Num, Package_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

           
            strCmd = "select * from BiddingStrategySetting where id=" + BiddingStrategySettingId.ToString();
            oDataTable = utilities.returntbl(strCmd);

            string runSetting = oDataTable.Rows[0]["RunSetting"].ToString().Trim();
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,] Run_State_user = new string[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {

                    Run_State_user[j, k] = runSetting.Trim();
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            if (runSetting == "Automatic")
            {
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Hour_Cap_Automatic[j, k, h] = oDataTable.Rows[0]["PeakBid"].ToString().Trim();
                            Hour_State_Automatic[j, k, h] = oDataTable.Rows[0]["StrategyBidding"].ToString().Trim();

                        }
                    }
                }
            }
            else
            {
                strCmd = "select * from BiddingStrategyCustomized where FkBiddingStrategySettingId=" + BiddingStrategySettingId.ToString() + " order by hour";
                oDataTable = utilities.returntbl(strCmd);

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h=0; h<24; h++)
                        {
                            Hour_Cap_customize[j,k,h] = oDataTable.Rows[h]["Peak"].ToString().Trim();
                            Hour_State_customize[j, k, h] = oDataTable.Rows[h]["Strategy"].ToString().Trim();
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[, ,] Hour_Cap_User = new string[Hour, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Hour_Cap_User[j, k, h] = "NO";
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Run_State_user[j,k] == "Automatic")
                        {
                            Hour_Cap_User[j, k, h] = Hour_Cap_Automatic[j, k, h];
                        }
                        if (Run_State_user[j,k] == "customize")
                        {
                            Hour_Cap_User[j, k, h] = Hour_Cap_customize[j, k, h];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[, ,] Hour_State = new string[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Variance[j, k, h] <= Average_Variance[j, k])
                        {
                            Hour_State[j, k, h] = "U-O-F";
                        }
                        if ((Variance[j, k, h] > Average_Variance[j, k]) & (Variance[j, k, h] <= Max_Variance[j, k]))
                        {
                            Hour_State[j, k, h] = "O-F";
                        }
                        if (Variance[j, k, h] > Max_Variance[j, k])
                        {
                            Hour_State[j, k, h] = "F-M";
                        }
                        if (Select_Hour_Cap[j, k, h] == "Yes")
                        {
                            Hour_State[j, k, h] = "F-M";
                        }
                        if (h == 7)
                        {
                            Hour_State[j, k, h] = "O-F";
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[, ,] Hour_State_User = new string[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Hour_State_User[j, k, h] = "Medium";
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Run_State_user[j,k] == "Automatic")
                        {
                            Hour_State_User[j, k, h] = Hour_State_Automatic[j, k, h];
                        }
                        if (Run_State_user[j,k] == "customize")
                        {
                            Hour_State_User[j, k, h] = Hour_State_customize[j, k, h];
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "O-F"))
                        {
                            Hour_State[j, k, h] = "U-O";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "F-M"))
                        {
                            Hour_State[j, k, h] = "O-F";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "U-F"))
                        {
                            Hour_State[j, k, h] = "U-O";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "U-M"))
                        {
                            Hour_State[j, k, h] = "U-F";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "O-M"))
                        {
                            Hour_State[j, k, h] = "U-F";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "U-O-F"))
                        {
                            Hour_State[j, k, h] = "U-O";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "U-O-M"))
                        {
                            Hour_State[j, k, h] = "U-O-F";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "U-F-M"))
                        {
                            Hour_State[j, k, h] = "U-O-F";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "O-F-M"))
                        {
                            Hour_State[j, k, h] = "U-O-F";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "U-O-F-M"))
                        {
                            Hour_State[j, k, h] = "U-O-F";
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "O-F"))
                        {
                            Hour_State[j, k, h] = "F-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "U-O"))
                        {
                            Hour_State[j, k, h] = "O-F";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "U-F"))
                        {
                            Hour_State[j, k, h] = "O-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "U-M"))
                        {
                            Hour_State[j, k, h] = "O-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "O-M"))
                        {
                            Hour_State[j, k, h] = "F-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "U-O-F"))
                        {
                            Hour_State[j, k, h] = "O-F-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "U-O-M"))
                        {
                            Hour_State[j, k, h] = "O-F-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "U-F-M"))
                        {
                            Hour_State[j, k, h] = "O-F-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "O-F-M"))
                        {
                            Hour_State[j, k, h] = "F-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "U-O-F-M"))
                        {
                            Hour_State[j, k, h] = "O-F-M";
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (UL_State[j, k, h] == "UL")
                        {
                            Hour_State[j, k, h] = "U-O";
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Run_State = new string[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Run_State[j, k, h] = Hour_State[j, k, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Cap_Bid = new string[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {

                        Cap_Bid[j, k, h] = Hour_Cap_User[j, k, h];

                        if (ppName == "Rey" || j == 4)
                        {
                            Cap_Bid[j, k, h] = "Yes";
                        }
                    }
                }
            }
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------

            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------

            double[, , ,] Price_lost_initial = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Price_lost_initial[j, k, h, 0] = 0;
                        for (int x = 1; x < Xtrain; x++)
                        {
                            Price_lost_initial[j, k, h, x] = Math.Abs(pd_range[j, k, h, x] - pd_range[j, k, h, (x - 1)]) * pdist[j, k, h, x] + Price_lost_initial[j, k, h, (x - 1)];
                        }
                    }
                }

            }

            //**************************************************************************



            double[, , ,] Price_lost = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Price_lost[j, k, h, x] = Price_lost_initial[j, k, h, x] + Math.Abs(1 - Price_lost_initial[j, k, h, (Xtrain - 1)]) / 2;
                        }
                    }
                }

            }
            //**************************************************************************


            double[, , ,] Price_win = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Price_win[j, k, h, x] = 1 - Price_lost[j, k, h, x];
                        }
                    }
                }
            }

            //**************************************************************************


            double[, , ,] Price_win_price = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Price_win_price[j, k, h, x] = Price_win[j, k, h, x] * pdist[j, k, h, x];
                        }
                    }
                }

            }

            //**************************************************************************


            double[, , ,] Price_lost_price = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Price_lost_price[j, k, h, x] = Price_lost[j, k, h, x] * pdist[j, k, h, x];
                        }
                    }
                }
            }

            //**************************************************************************


            double[, , ,] Profit_win_lost = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Profit_win_lost[j, k, h, x] = Price_win_price[j, k, h, x] * pd_range[j, k, h, x] - Price_lost_price[j, k, h, x] * (pd_range[j, k, h, x] - Price_Min[h]);
                        }
                    }
                }
            }


            //**************************************************************************

            double[, , ,] Profit_win = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Profit_win[j, k, h, x] = Price_win_price[j, k, h, x] * pd_range[j, k, h, x];
                        }
                    }
                }

            }
            //**************************************************************************

            double[, , ,] Profit_Margin = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Profit_Margin[j, k, h, x] = Price_lost_price[j, k, h, x] * pd_range[j, k, h, x];
                        }
                    }
                }
            }

            //**************************************************************************
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            try
            {
                Cplex cplex = new Cplex();

                // define variable :

                INumVar[, ,] Point_UL = new INumVar[Plants_Num, Package_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Point_UL[j, k, h] = cplex.NumVar(0, double.MaxValue);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
                INumVar[, ,] Point_Opimal = new INumVar[Plants_Num, Package_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Point_Opimal[j, k, h] = cplex.NumVar(-double.MaxValue, double.MaxValue);
                        }
                    }
                }


                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
                INumVar[, ,] Point_Margin = new INumVar[Plants_Num, Package_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Point_Margin[j, k, h] = cplex.NumVar(-double.MaxValue, double.MaxValue);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
                ILinearNumExpr Sum_Point_Margin = cplex.LinearNumExpr();
                double _Sum_Point_Margin = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Point_Margin.AddTerm(_Sum_Point_Margin, Point_Margin[j, k, h]);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                ILinearNumExpr Sum_Point_UL = cplex.LinearNumExpr();
                double _Sum_Point_UL = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Point_UL.AddTerm(_Sum_Point_UL, Point_UL[j, k, h]);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                ILinearNumExpr Sum_Point_Opimal = cplex.LinearNumExpr();
                double _Sum_Point_Opimal = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Point_Opimal.AddTerm(_Sum_Point_Opimal, Point_Opimal[j, k, h]);
                        }
                    }
                }


                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int x = 0; x < Xtrain; x++)
                            {
                                cplex.AddGe(Point_UL[j, k, h], Profit_win_lost[j, k, h, x]);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int x = 0; x < Xtrain; x++)
                            {
                                cplex.AddGe(Point_Opimal[j, k, h], Profit_win[j, k, h, x]);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int x = 0; x < Xtrain; x++)
                            {
                                cplex.AddGe(Point_Margin[j, k, h], Profit_Margin[j, k, h, x]);
                            }
                        }
                    }
                }
                // define Main Function :
                //*******************************************************************************************************
                cplex.AddMinimize(cplex.Sum(Sum_Point_Opimal, cplex.Sum(Sum_Point_Margin, Sum_Point_UL)));
                //*******************************************************************************************************
                //Export
                //*******************************************************************************************************
                if (cplex.Solve())
                {
                   // System.Windows.Forms.MessageBox.Show(" Value =   Price:   " + cplex.ObjValue + "   " + cplex.GetStatus());
                    int[, ,] Bench = new int[Plants_Num, Package_Num, Hour];
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                Bench[j, k, h] = 0;
                                for (int x = 0; x < Xtrain; x++)
                                {
                                    if (Profit_win[j, k, h, x] == cplex.GetValue(Point_Opimal[j, k, h]))
                                    {
                                        Price_optimal[j, k, h] = pd_range[j, k, h, x];
                                        X_select[j, k, h, 1] = x;
                                    }
                                    if (Profit_win_lost[j, k, h, x] == cplex.GetValue(Point_UL[j, k, h]))
                                    {
                                        Price_UL[j, k, h] = pd_range[j, k, h, x];
                                        X_select[j, k, h, 0] = x;
                                    }
                                    if ((PriceForecasting[j, k, h] < pd_range[j, k, h, x]) & (Bench[j, k, h] == 0))
                                    {
                                        Bench[j, k, h] = 1;
                                    }
                                    if (Bench[j, k, h] == 1)
                                    {
                                        X_select[j, k, h, 2] = x;
                                        Bench[j, k, h] = 2;
                                    }
                                    if (Profit_Margin[j, k, h, x] == cplex.GetValue(Point_Margin[j, k, h]))
                                    {
                                        Price_Margin[j, k, h] = pd_range[j, k, h, x];
                                        X_select[j, k, h, 3] = x;
                                    }
                                    {
                                        Price_Margin[j, k, h] = Price_Margin[j, k, h] + Cost_Level[j];
                                        PriceForecasting[j, k, h] = PriceForecasting[j, k, h] + Cost_Level[j];
                                        Price_optimal[j, k, h] = Price_optimal[j, k, h] + Cost_Level[j];
                                        Price_UL[j, k, h] = Price_UL[j, k, h] + Cost_Level[j];
                                    }

                                }
                            }
                        }
                    }
                }

                cplex.End();
            }
            catch (ILOG.CPLEX.CouldNotInstallColumnException e1)
            {
                System.Console.WriteLine("concert exception" + e1 + "caught");
            }

            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------
            //Program-2 : Calculate PBUC //
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------

            double[,] Tbid_Plant_unit = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Tbid_Plant_unit[j, i] = (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]) / Mmax;
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[, ,] Fbid_Plant_unit = new double[Plants_Num, Units_Num, Mmax];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int m = 0; m < Mmax; m++)
                    {
                        Fbid_Plant_unit[j, i, m] = Unit_Data[j, i, 9] + ((2 * (m + 1)) - 1) * Unit_Data[j, i, 8] * Tbid_Plant_unit[j, i] + (2 * Unit_Data[j, i, 8] * Unit_Data[j, i, 3]);
                    }

                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[,] Fmin_Plant_unit = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Fmin_Plant_unit[j, i] = Unit_Data[j, i, 8] * Unit_Data[j, i, 3] * Unit_Data[j, i, 3] + Unit_Data[j, i, 9] * Unit_Data[j, i, 3] + Unit_Data[j, i, 10];
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[, ,] Fstart_Plant_unit = new double[Plants_Num, Units_Num, Start_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int t = 0; t < Start_Num; t++)
                    {
                        if (t < (Unit_Data[j, i, 17]))
                        {
                            Fstart_Plant_unit[j, i, t] = Unit_Data[j, i, 19];
                        }
                        else
                        {
                            Fstart_Plant_unit[j, i, t] = Unit_Data[j, i, 18];
                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[, ,] Price_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k))
                            {
                                Price_Plant_unit[j, i, h] = PriceForecasting[j, k, h];
                            }
                        }
                    }
                }
            }

            //*******************************************************************************************************

            try
            {
                Cplex cplex = new Cplex();

                // define variable :
                IIntVar[, ,] V_Plant_unit = new IIntVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            V_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                        }

                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                IIntVar[, ,] X_Plant_unit = new IIntVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if (Unit_Dec[j, i, 1].ToString().Trim() == "CC")
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                X_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                IIntVar[, ,] Y_Plant_unit = new IIntVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Y_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                        }

                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                IIntVar[,] Z_Plant_unit = new IIntVar[Plants_Num, Units_Num];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        Z_Plant_unit[j, i] = cplex.IntVar(0, 1);

                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[, ,] Power_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Power_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }

                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 
                INumVar[, ,] Pres_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Pres_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[, , ,] delta_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour, Mmax];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int m = 0; m < Mmax; m++)
                            {
                                delta_Plant_unit[j, i, h, m] = cplex.NumVar(0.0, double.MaxValue);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Cost(i,h):
                INumVar[, ,] Cost_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Cost_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[, ,] Income_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Income_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[, ,] Benefit_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Benefit_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[, ,] CostStart_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            CostStart_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }

                //**************************************************************************************************


                ILinearNumExpr Sum_Cost_day = cplex.LinearNumExpr();

                double _Sum_Cost_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Cost_day.AddTerm(_Sum_Cost_day, Cost_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                ILinearNumExpr Sum_CostStart_day = cplex.LinearNumExpr();

                double _Sum_CostStart_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_CostStart_day.AddTerm(_Sum_CostStart_day, Cost_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                ILinearNumExpr Sum_Power_day = cplex.LinearNumExpr();
                double _Sum_Power_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Power_day.AddTerm(_Sum_Power_day, Power_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                ILinearNumExpr Sum_Income_day = cplex.LinearNumExpr();

                double _Sum_Income_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Income_day.AddTerm(_Sum_Income_day, Income_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                ILinearNumExpr Sum_Benefit_day = cplex.LinearNumExpr();

                double _Sum_Benefit_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Benefit_day.AddTerm(_Sum_Benefit_day, Benefit_Plant_unit[j, i, h]);
                        }
                    }
                }
                //*******************************************************************************************************



                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddEq(Income_Plant_unit[j, i, h], cplex.Prod(Price_Plant_unit[j, i, h], Power_Plant_unit[j, i, h]));
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddEq(Z_Plant_unit[j, i], Outservice_Plant_unit[j, i]);
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddLe(cplex.Sum(V_Plant_unit[j, i, h], Z_Plant_unit[j, i]), 1);
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddEq(Benefit_Plant_unit[j, i, h], cplex.Diff(Income_Plant_unit[j, i, h], cplex.Sum(CostStart_Plant_unit[j, i, h], Cost_Plant_unit[j, i, h])));
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddGe(Power_Plant_unit[j, i, h], cplex.Prod(Unit_Data[j, i, 3], V_Plant_unit[j, i, h]));
                        }
                    }
                };

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddLe(Power_Plant_unit[j, i, h], cplex.Prod(Pmax_Plant_unit[j, i, h], V_Plant_unit[j, i, h]));
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int m = 0; m < Mmax; m++)
                            {
                                cplex.AddLe(delta_Plant_unit[j, i, h, m], Tbid_Plant_unit[j, i]);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _Sum_Delta_m = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            ILinearNumExpr Sum_Delta_m = cplex.LinearNumExpr();
                            for (int m = 0; m < Mmax; m++)
                            {
                                Sum_Delta_m.AddTerm(_Sum_Delta_m, delta_Plant_unit[j, i, h, m]);
                            }
                            cplex.AddEq(Power_Plant_unit[j, i, h], cplex.Sum(cplex.Prod(Unit_Data[j, i, 3], V_Plant_unit[j, i, h]), Sum_Delta_m));
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            ILinearNumExpr DeltaBid_m = cplex.LinearNumExpr();
                            for (int m = 0; m < Mmax; m++)
                            {
                                DeltaBid_m.AddTerm(Fbid_Plant_unit[j, i, m], delta_Plant_unit[j, i, h, m]);
                            }
                            cplex.AddEq(Cost_Plant_unit[j, i, h], cplex.Sum(DeltaBid_m, cplex.Prod(Fmin_Plant_unit[j, i], V_Plant_unit[j, i, h])));
                        }
                    }
                };
                ////^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hisup_Plant_unit[j, i]; h++)
                        {
                            cplex.AddEq(V_Plant_unit[j, i, h], 1);
                        }
                    }
                };

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hisdown_Plant_unit[j, i]; h++)
                        {
                            cplex.AddEq(V_Plant_unit[j, i, h], 0);
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _Minup_2 = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h <= (Hour - Unit_Data[j, i, 6]); h++)
                        {
                            ILinearNumExpr v_up = cplex.LinearNumExpr();
                            for (int hh = h; hh <= (h + Unit_Data[j, i, 6] - 1); hh++)
                            {
                                v_up.AddTerm(_Minup_2, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(v_up, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(V_Plant_unit[j, i, h], V_Plant_unit[j, i, h - 1])));
                            }
                            else
                            {
                                cplex.AddGe(v_up, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(V_Plant_unit[j, i, h], V_history_Plant_unit[j, i])));
                            }
                        }
                    }
                }

                double _Mindown_2 = -1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h <= (Hour - (int)Unit_Data[j, i, 7]); h++)
                        {
                            ILinearNumExpr v_down = cplex.LinearNumExpr();
                            for (int hh = h; hh <= (h + Unit_Data[j, i, 7] - 1); hh++)
                            {
                                v_down.AddTerm(_Mindown_2, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(v_down, cplex.Prod(Unit_Data[j, i, 7], cplex.Diff(cplex.Diff(V_Plant_unit[j, i, h - 1], V_Plant_unit[j, i, h]), 1)));
                            }
                            else
                            {
                                cplex.AddGe(v_down, cplex.Prod(Unit_Data[j, i, 7], cplex.Diff(cplex.Diff(V_history_Plant_unit[j, i], V_Plant_unit[j, i, h]), 1)));
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _Minup_h = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = (Hour - (int)Unit_Data[j, i, 6] + 1); h < Hour; h++)
                        {
                            ILinearNumExpr vh_up = cplex.LinearNumExpr();
                            for (int hh = h; hh < Hour; hh++)
                            {
                                vh_up.AddTerm(_Minup_h, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(cplex.Diff(vh_up, cplex.Diff(cplex.Prod((Hour - h), V_Plant_unit[j, i, h]), cplex.Prod((Hour - h), V_Plant_unit[j, i, h - 1]))), 0);
                            }
                            else
                            {
                                cplex.AddGe(cplex.Diff(vh_up, cplex.Diff(cplex.Prod((Hour - h), V_Plant_unit[j, i, h]), ((Hour - h) * V_history_Plant_unit[j, i]))), 0);
                            }
                        }
                    }
                }

                double _Mindown_h = -1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = (Hour - (int)Unit_Data[j, i, 6] + 1); h < Hour; h++)
                        {
                            ILinearNumExpr vh_down = cplex.LinearNumExpr();
                            for (int hh = h; hh < Hour; hh++)
                            {
                                vh_down.AddTerm(_Mindown_h, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(cplex.Sum((Hour - h), cplex.Diff(vh_down, cplex.Diff(cplex.Prod((Hour - h), V_Plant_unit[j, i, h - 1]), cplex.Prod((Hour - h), V_Plant_unit[j, i, h])))), 0);
                            }
                            else
                            {
                                cplex.AddGe(cplex.Sum((Hour - h), cplex.Diff(vh_down, cplex.Diff(((Hour - h) * (1 - V_history_Plant_unit[j, i])), cplex.Prod((Hour - h), V_Plant_unit[j, i, h])))), 0);
                            }
                        }
                    }
                }

                //////^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h < Hour; h++)
                        {
                            if (h < 23)
                            {
                                cplex.AddLe(Pres_Plant_unit[j, i, h], cplex.Sum(cplex.Prod(Pmax_Plant_unit[j, i, h] * 10, V_Plant_unit[j, i, h + 1]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h], V_Plant_unit[j, i, h + 1]))));
                            }

                            if ((h > 0))
                            {
                                cplex.AddLe(Power_Plant_unit[j, i, h], cplex.Sum(Power_Plant_unit[j, i, h - 1], cplex.Sum(cplex.Prod(Unit_Data[j, i, 1], V_Plant_unit[j, i, h]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h], V_Plant_unit[j, i, h - 1])), cplex.Prod(Pmax_Plant_unit[j, i, h] * 10, cplex.Diff(1, V_Plant_unit[j, i, h])))));
                                cplex.AddLe(cplex.Diff(Power_Plant_unit[j, i, h - 1], Power_Plant_unit[j, i, h]), cplex.Sum(cplex.Prod(Unit_Data[j, i, 2], V_Plant_unit[j, i, h]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h - 1], V_Plant_unit[j, i, h])), cplex.Diff(Pmax_Plant_unit[j, i, h] * 10, cplex.Diff(1, V_Plant_unit[j, i, h - 1]))));
                            }
                            if (h == 0)
                            {
                                cplex.AddLe(Power_Plant_unit[j, i, h], cplex.Sum(Powerhistory_Plant_unit[j, i], cplex.Sum(cplex.Prod(Unit_Data[j, i, 1], V_Plant_unit[j, i, h]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h], V_history_Plant_unit[j, i])), cplex.Prod(Pmax_Plant_unit[j, i, h] * 10, cplex.Diff(1, V_Plant_unit[j, i, h])))));
                                cplex.AddLe(cplex.Diff(Powerhistory_Plant_unit[j, i], Power_Plant_unit[j, i, h]), cplex.Sum(cplex.Prod(Unit_Data[j, i, 2], V_Plant_unit[j, i, h]), cplex.Sum(cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_history_Plant_unit[j, i], V_Plant_unit[j, i, h])), (Pmax_Plant_unit[j, i, h] * 10 * (1 - V_history_Plant_unit[j, i])))));
                            }
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _Unit_Gas_CC = 0.5;
                for (int h = 0; h < Hour; h++)
                {
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            if (Package[j, k].ToString().Trim() == "CC")
                            {
                                ILinearNumExpr Unit_Gas_CC = cplex.LinearNumExpr();
                                {
                                    for (int i = 0; i < Plant_units_Num[j]; i++)
                                    {
                                        if ((Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Data[j, i, 0] == k) & (Unit_Dec[j, i, 1].ToString().Trim() == "CC")) ;
                                        {
                                            Unit_Gas_CC.AddTerm(_Unit_Gas_CC, Power_Plant_unit[j, i, h]);
                                        }
                                    }
                                }
                                for (int i = 0; i < Plant_units_Num[j]; i++)
                                {
                                    if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam") & (Unit_Data[j, i, 0] == k) & (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                                    {
                                        cplex.AddGe(Unit_Gas_CC, cplex.Diff(Power_Plant_unit[j, i, h], cplex.Prod(cplex.Diff(1, V_Plant_unit[j, i, h]), Pmax_Plant_unit[j, i, h])));
                                    }
                                }
                            }
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _X_1 = 1;
                double _X_2 = 1;

                int[] i_Start = new int[1];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                        {
                            i_Start[0] = ((int)Unit_Data[j, i, 16]);
                        }
                        if (i_Start[0] == 0)
                        {
                            i_Start[0] = 8;
                        }
                    }
                }

                double[] _X_Start = new double[1];
                double _Pmax_Steam = 0;
                _X_Start[0] = Math.Pow(i_Start[0], -1);
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        if (Package[j, k].ToString().Trim() == "CC")
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                for (int i = 0; i < Plant_units_Num[j]; i++)
                                {
                                    if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Data[j, i, 0] == (k)))
                                    {
                                        if (h < (i_Start[0] + 1))
                                        {
                                            ILinearNumExpr X_1 = cplex.LinearNumExpr();
                                            for (int hh = 0; hh <= h; hh++)
                                            {
                                                X_1.AddTerm(_X_1, V_Plant_unit[j, i, hh]);
                                            }
                                            cplex.AddLe(cplex.Diff(cplex.Prod(_X_Start[0], cplex.Sum(X_1, hisstart_Plant_unit[j, i])), 1), X_Plant_unit[j, i, h]);
                                            cplex.AddLe(X_Plant_unit[j, i, h], cplex.Prod(_X_Start[0], cplex.Sum(X_1, hisstart_Plant_unit[j, i])));

                                        }
                                        if (h > (i_Start[0]))
                                        {
                                            ILinearNumExpr X_2 = cplex.LinearNumExpr();
                                            for (int hh = h - i_Start[0]; hh <= h; hh++)
                                            {

                                                X_2.AddTerm(_X_2, V_Plant_unit[j, i, hh]);
                                            }

                                            cplex.AddLe(cplex.Diff(cplex.Prod(_X_Start[0], X_2), 1), X_Plant_unit[j, i, h]);
                                            cplex.AddLe(X_Plant_unit[j, i, h], cplex.Prod(_X_Start[0], X_2));
                                        }
                                    }
                                }

                                for (int ii = 0; ii < Plant_units_Num[j]; ii++)
                                {
                                    if ((Unit_Dec[j, ii, 1].ToString().Trim() == "CC") & (Unit_Dec[j, ii, 0].ToString().Trim() == "Steam") & (Unit_Data[j, ii, 0] == (k)))
                                    {
                                        _Pmax_Steam = Pmax_Plant_unit[j, ii, h];
                                        ILinearNumExpr X_CC = cplex.LinearNumExpr();
                                        for (int i = 0; i < Plant_units_Num[j]; i++)
                                        {
                                            if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Data[j, i, 0] == (k)))
                                            {
                                                X_CC.AddTerm(_Pmax_Steam * 0.5, X_Plant_unit[j, i, h]);
                                            }
                                        }
                                        cplex.AddGe(X_CC, Power_Plant_unit[j, ii, h]);
                                    }
                                }
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _V_Start1 = 1;
                double _V_Start2 = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int t = 0; t <= h; t++)
                            {
                                if ((t < h) & (t <= Unit_Data[j, i, 16]))
                                {
                                    ILinearNumExpr V_Start1 = cplex.LinearNumExpr();
                                    for (int tt = 0; tt <= t; tt++)
                                    {
                                        V_Start1.AddTerm(_V_Start1, V_Plant_unit[j, i, (h - tt - 1)]);
                                    }
                                    cplex.AddGe(CostStart_Plant_unit[j, i, h], cplex.Prod(Fstart_Plant_unit[j, i, t], cplex.Diff(V_Plant_unit[j, i, h], V_Start1)));
                                }
                                if ((t == h) & (V_history_Plant_unit[j, i] == 0) & (t <= Unit_Data[j, i, 16]))
                                {
                                    ILinearNumExpr V_Start2 = cplex.LinearNumExpr();
                                    for (int tt = 0; tt < (t+1); tt++)
                                    {
                                        V_Start2.AddTerm(_V_Start2, V_Plant_unit[j, i, (h - tt)]);
                                    }
                                    cplex.AddGe(CostStart_Plant_unit[j, i, h], cplex.Prod(Fstart_Plant_unit[j, i, (t + HisShut_Plant_unit[j, i])], cplex.Diff(V_Plant_unit[j, i, h], V_Start2)));
                                }
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h < Hour; h++)
                        {
                            if (h > 0)
                            {
                                cplex.AddLe(Y_Plant_unit[j, i, h], cplex.Sum(1, cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Power_Plant_unit[j, i, h - 1]))));
                                cplex.AddGe(Y_Plant_unit[j, i, h], cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Power_Plant_unit[j, i, h - 1])));
                            }
                            if (h == 0)
                            {
                                cplex.AddLe(Y_Plant_unit[j, i, h], cplex.Sum(1, cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Powerhistory_Plant_unit[j, i]))));
                                cplex.AddGe(Y_Plant_unit[j, i, h], cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Powerhistory_Plant_unit[j, i])));
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _Inc_Ramp_2 = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < (Hour - (int)Unit_Data[j, i, 6]); h++)
                        {
                            ILinearNumExpr Y_up = cplex.LinearNumExpr();
                            for (int hh = h + 1; hh <= (h + Unit_Data[j, i, 6]); hh++)
                            {
                                Y_up.AddTerm(_Inc_Ramp_2, Y_Plant_unit[j, i, hh]);
                            }
                            if (h < 23)
                            {
                                cplex.AddGe(Y_up, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(Y_Plant_unit[j, i, h + 1], Y_Plant_unit[j, i, h])));
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                double _Dec_Ramp_2 = -1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h < (Hour - (int)Unit_Data[j, i, 6]); h++)
                        {
                            ILinearNumExpr Y_down = cplex.LinearNumExpr();
                            for (int hh = (h + 1); hh <= (h + Unit_Data[j, i, 6]); hh++)
                            {
                                Y_down.AddTerm(_Dec_Ramp_2, Y_Plant_unit[j, i, hh]);
                            }
                            if (h < 23)
                            {
                                cplex.AddGe(Y_down, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(cplex.Diff(Y_Plant_unit[j, i, h], Y_Plant_unit[j, i, (h + 1)]), 1)));
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _Inc_Ramp_3 = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = (Hour - (int)Unit_Data[j, i, 6]); h < Hour; h++)
                        {
                            ILinearNumExpr YY_up = cplex.LinearNumExpr();
                            for (int hh = (h + 1); hh < Hour; hh++)
                            {
                                YY_up.AddTerm(_Inc_Ramp_3, Y_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(cplex.Diff(YY_up, cplex.Diff(cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, h]), cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, h - 1]))), 0);
                            }
                        }
                    }
                }

                double _Dec_Ramp_3 = -1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = (Hour - (int)Unit_Data[j, i, 6]); h < Hour; h++)
                        {
                            ILinearNumExpr Yh_down = cplex.LinearNumExpr();
                            for (int hh = (h + 1); hh < Hour; hh++)
                            {
                                Yh_down.AddTerm(_Dec_Ramp_3, Y_Plant_unit[j, i, hh]);
                            }
                            if (h < 23)
                            {
                                cplex.AddGe(cplex.Sum((Hour - h - 1), cplex.Diff(Yh_down, cplex.Diff(cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, h]), cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, (h + 1)])))), 0);
                            }
                        }
                    }
                }
                //*******************************************************************************************************
                // define Main Function :
                cplex.AddMaximize(Sum_Benefit_day);
                //*******************************************************************************************************
                //Export

                if (cplex.Solve())
                {
                    //MessageBox.Show(" Value =  Calculate : PBUC    " + cplex.ObjValue + "   " + cplex.GetStatus());
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                Power_memory[j, i, h] = cplex.GetValue(Power_Plant_unit[j, i, h]);
                                Test[j, i, h] = Power_memory[j, i, h];
                                Power_Error[j, i, h] = 0;
                                if (Power_memory[j, i, h] < 2)
                                {
                                    Power_Error[j, i, h] = 1;
                                }
                            }
                        }
                    }

                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                for (int i = 0; i < Plant_units_Num[j]; i++)
                                {
                                    if (((int)Unit_Data[j, i, 0] == (k)))
                                    {
                                        if ((Power_Error[j, i, h] == 0) & (Package[j, k].ToString().Trim() != "CC"))
                                        {
                                            Power_if_Pack[j, k, h] = Unit_Data[j, i, 3];
                                            Power_memory_Pack[j, k, h] = Power_memory[j, i, h];
                                            Power_if_CC[j, k, h] = Unit_Data[j, i, 3];
                                            Dispatch_Proposal[j, k, h] = Pmax_Plant_unit[j, i, h];
                                        }
                                        if ((Power_memory[j, i, h] < (Pmax_Plant_unit[j, i, h])) && (Package[j, k].ToString().Trim() == "CC") && (Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                                        {
                                            CO_steam[j, i, h] = 1;
                                            CO_Dispatch[j, i, h] = 1;
                                        }
                                        if ((Power_Error[j, i, h] == 0) && (Package[j, k].ToString().Trim() == "CC") && (Outservice_Plant_unit[j, i] == 0))
                                        {
                                            Power_memory_Pack[j, k, h] = Power_memory_Pack[j, k, h] + Power_memory[j, i, h];
                                            Power_if_Pack[j, k, h] = Power_if_Pack[j, k, h] + CO_steam[j, i, h] * Unit_Data[j, i, 3];
                                            Dispatch_Proposal[j, k, h] = Dispatch_Proposal[j, k, h] + CO_Dispatch [j, i, h] * Pmax_Plant_unit[j, i, h]/2;
                                        }
                                        if ((Power_Error[j, i, h] == 1) & (Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Gas"))
                                        {
                                            Power_if_CC[j, k, h] = Unit_Data[j, i, 3];
                                        }
                                    }
                                }
                            }
                        }
                    }
                                

                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                Power_Error_Pack[j, k, h] = 0;
                                if (Power_memory_Pack[j, k, h] < 3)
                                {
                                    Power_Error_Pack[j, k, h] = 1;
                                }
                                if (Power_memory_Pack[j, k, h] < 3)
                                {
                                    Power_if_Pack[j, k, h] = Power_if_CC[j, k, h];
                                    Power_memory_Pack[j, k, h] = Power_if_CC[j, k, h] + 0;
                                }
                            }
                        }
                    }
                }
                cplex.End();
            }
            catch (ILOG.CPLEX.Cplex.UnknownObjectException e1)
            {
                MessageBox.Show("concert exception" + e1.ToString() + "caught");
            }
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------
            //Program-3 : Calculate Bid Power //
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------

            double[, , ,] Weight_Lost = new double[Plants_Num, Package_Num, Hour, Power_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int n = 0; n < Power_Num; n++)
                        {
                            Weight_Lost[j, k, h, n] = Math.Pow(Price_lost[j, k, h, X_select[j, k, h, n]] * pd_range[j, k, h, X_select[j, k, h, n]], -1);
                        }
                    }
                }
            }

            double[, , , ,] Weight_Lost_per = new double[Plants_Num, Package_Num, Hour, Power_Num, Power_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int w = 0; w < Power_Num; w++)
                        {
                            if (w == 0)
                            {
                                Weight_Lost_per[j, k, h, 1, w] = Weight_Lost[j, k, h, 1] / (Weight_Lost[j, k, h, 1] + Weight_Lost[j, k, h, 2]);
                                Weight_Lost_per[j, k, h, 2, w] = Weight_Lost[j, k, h, 2] / (Weight_Lost[j, k, h, 1] + Weight_Lost[j, k, h, 2]);
                            }
                            if (w == 1)
                            {
                                Weight_Lost_per[j, k, h, 1, w] = Weight_Lost[j, k, h, 1] / (Weight_Lost[j, k, h, 1] + Weight_Lost[j, k, h, 3]);
                                Weight_Lost_per[j, k, h, 3, w] = Weight_Lost[j, k, h, 3] / (Weight_Lost[j, k, h, 1] + Weight_Lost[j, k, h, 3]);
                            }
                            if (w == 2)
                            {
                                Weight_Lost_per[j, k, h, 2, w] = Weight_Lost[j, k, h, 2] / (Weight_Lost[j, k, h, 3] + Weight_Lost[j, k, h, 2]);
                                Weight_Lost_per[j, k, h, 3, w] = Weight_Lost[j, k, h, 3] / (Weight_Lost[j, k, h, 3] + Weight_Lost[j, k, h, 2]);
                            }
                            if (w == 3)
                            {
                                Weight_Lost_per[j, k, h, 1, w] = Weight_Lost[j, k, h, 1] / (Weight_Lost[j, k, h, 1] + Weight_Lost[j, k, h, 2] + Weight_Lost[j, k, h, 3]);
                                Weight_Lost_per[j, k, h, 2, w] = Weight_Lost[j, k, h, 2] / (Weight_Lost[j, k, h, 1] + Weight_Lost[j, k, h, 2] + Weight_Lost[j, k, h, 3]);
                                Weight_Lost_per[j, k, h, 3, w] = Weight_Lost[j, k, h, 3] / (Weight_Lost[j, k, h, 1] + Weight_Lost[j, k, h, 2] + Weight_Lost[j, k, h, 3]);
                            }
                        }
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[, , , ,] Power_bid_Plant_Pack = new double[Plants_Num, Package_Num, Hour, Power_Num, Power_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int w = 0; w < Power_Num; w++)
                        {
                            if (w == 0)
                            {
                                Power_bid_Plant_Pack[j, k, h, 0, w] = Power_if_Pack[j, k, h];
                                Power_bid_Plant_Pack[j, k, h, 1, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 1, w]);
                                Power_bid_Plant_Pack[j, k, h, 2, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 2, w]);
                                Power_bid_Plant_Pack[j, k, h, 3, w] = 0;

                            }
                            if (w == 1)
                            {
                                Power_bid_Plant_Pack[j, k, h, 0, w] = Power_if_Pack[j, k, h];
                                Power_bid_Plant_Pack[j, k, h, 1, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 1, w]);
                                Power_bid_Plant_Pack[j, k, h, 2, w] = 0;
                                Power_bid_Plant_Pack[j, k, h, 3, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 3, w]);
                            }
                            if (w == 2)
                            {
                                Power_bid_Plant_Pack[j, k, h, 0, w] = Power_if_Pack[j, k, h];
                                Power_bid_Plant_Pack[j, k, h, 1, w] = 0;
                                Power_bid_Plant_Pack[j, k, h, 2, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 2, w]);
                                Power_bid_Plant_Pack[j, k, h, 3, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 3, w]);
                            }
                            if (w == 3)
                            {
                                Power_bid_Plant_Pack[j, k, h, 0, w] = Power_if_Pack[j, k, h];
                                Power_bid_Plant_Pack[j, k, h, 1, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 1, w]);
                                Power_bid_Plant_Pack[j, k, h, 2, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 2, w]);
                                Power_bid_Plant_Pack[j, k, h, 3, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 3, w]);
                            }
                        }

                    }
                }
            }
            ;

            //*******************************************************************************************************
            //Export
           // MessageBox.Show(" Value =   Power:   ");
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int s = 0; s < Step_Num; s++)
                        {
                            if ((Run_State[j, k, h] == "U-O") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if ((s < 3) & (s > 0))
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h] + s * (Price_optimal[j, k, h] - Price_UL[j, k, h]) / 3;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 3)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                }
                                if (s > 3)
                                {

                                    Price[j, k, h, s] = Price_optimal[j, k, h] + (s - 3) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "U-F") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if ((s < 3) & (s > 0))
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h] + s * (PriceForecasting[j, k, h] - Price_UL[j, k, h]) / 3;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 3)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                }
                                if (s > 3)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 3) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "U-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if ((s < 3) & (s > 0))
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h] + s * (Price_Margin[j, k, h] - Price_UL[j, k, h]) / 3;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 3)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                }
                                if (s > 3)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 3) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "O-F") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if ((s < 3) & (s > 0))
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h] + s * (PriceForecasting[j, k, h] - Price_optimal[j, k, h]) / 3;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 3)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                }
                                if (s > 3)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 3) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "O-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if ((s < 3) & (s > 0))
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h] + s * (Price_Margin[j, k, h] - Price_optimal[j, k, h]) / 3;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 3)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                }
                                if (s > 3)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 3) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "F-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if ((s < 3) & (s > 0))
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] + s * (Price_Margin[j, k, h] - PriceForecasting[j, k, h]) / 3;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 3)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                }
                                if (s > 3)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 3) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "U-O-F") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if (s == 1)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h] + (Price_optimal[j, k, h] - Price_UL[j, k, h]) / 2;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 2)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + Power_bid_Plant_Pack[j, k, h, 1, 0]);
                                }
                                if ((s < 5) & (s > 2))
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h] + (s - 2) * (PriceForecasting[j, k, h] - Price_optimal[j, k, h]) / 3;
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + Power_bid_Plant_Pack[j, k, h, 1, 0]) + 0.2 * (s - 2);
                                }
                                if (s == 5)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 0]) + (Power_bid_Plant_Pack[j, k, h, 2, 0])) - 4 * 0.2;
                                }
                                if ((s < 10) & (s > 5))
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 5) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 0]) + (Power_bid_Plant_Pack[j, k, h, 2, 0])) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "U-O-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if (s == 1)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h] + (Price_optimal[j, k, h] - Price_UL[j, k, h]) / 2;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 2)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 1]));
                                }
                                if ((s < 5) & (s > 2))
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h] + (s - 2) * (Price_Margin[j, k, h] - Price_optimal[j, k, h]) / 3;
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 1])) + 0.2 * (s - 2);
                                }
                                if (s == 5)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 1]) + (Power_bid_Plant_Pack[j, k, h, 3, 1])) - 4 * 0.2;
                                }
                                if ((s < 10) & (s > 5))
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 5) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 1]) + (Power_bid_Plant_Pack[j, k, h, 3, 1])) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "U-F-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if (s == 1)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h] + (PriceForecasting[j, k, h] - Price_UL[j, k, h]) / 2;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 2)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2]));
                                }
                                if ((s < 5) & (s > 2))
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 2) * (Price_Margin[j, k, h] - PriceForecasting[j, k, h]) / 3;
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2])) + 0.2 * (s - 2);
                                }
                                if (s == 5)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2]) + (Power_bid_Plant_Pack[j, k, h, 3, 2])) - 4 * 0.2;
                                }
                                if ((s < 10) & (s > 5))
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 5) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2]) + (Power_bid_Plant_Pack[j, k, h, 3, 2])) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "O-F-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if (s == 1)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h] + (PriceForecasting[j, k, h] - Price_optimal[j, k, h]) / 2;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 2)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2]));
                                }
                                if ((s < 5) & (s > 2))
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 2) * (Price_Margin[j, k, h] - PriceForecasting[j, k, h]) / 3;
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2])) + 0.2 * (s - 2);
                                }
                                if (s == 5)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2]) + (Power_bid_Plant_Pack[j, k, h, 3, 2])) - 4 * 0.2;
                                }
                                if ((s < 10) & (s > 5))
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 5) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2]) + (Power_bid_Plant_Pack[j, k, h, 3, 2])) - (9 - s) * 0.2;
                                }
                            }

                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "U-O-F-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if (s == 1)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h] + (Price_optimal[j, k, h] - Price_UL[j, k, h]) / 2;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 2)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 3]));
                                }
                                if (s == 3)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h] + (PriceForecasting[j, k, h] - Price_optimal[j, k, h]) / 2;
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 3])) + 0.2;
                                }
                                if (s == 4)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 3]) + (Power_bid_Plant_Pack[j, k, h, 2, 3]));
                                }
                                if (s == 5)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] + (Price_Margin[j, k, h] - PriceForecasting[j, k, h]) / 2;
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 3]) + (Power_bid_Plant_Pack[j, k, h, 2, 3]) + 0.2);
                                }
                                if (s == 6)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 3]) + (Power_bid_Plant_Pack[j, k, h, 2, 3]) + (Power_bid_Plant_Pack[j, k, h, 3, 3])) - 3 * 0.2;
                                }
                                if ((s < 10) & (s > 6))
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 6) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 3]) + (Power_bid_Plant_Pack[j, k, h, 2, 3]) + (Power_bid_Plant_Pack[j, k, h, 3, 3])) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                    
                            if ((Cap_Bid[j, k, h] == "yes") & (Power_memory_Pack[j, k, h] > 3.2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = CapPrice[0] * 0.999;
                                    Power[j, k, h, s] = Dispatch_Proposal[j, k, h] - 0.2;
                                }
                                if (s == 1)
                                {
                                    Price[j, k, h, s] = CapPrice[0] * 0.9993;
                                    Power[j, k, h, s] = Dispatch_Proposal[j, k, h] - 0.1;
                                }
                                if (s == 2)
                                {
                                    Price[j, k, h, s] = CapPrice[0] * 0.9996;
                                    Power[j, k, h, s] = Dispatch_Proposal[j, k, h];
                                }
                                if (s > 2)
                                {
                                    Price[j, k, h, s] = 0;
                                    Power[j, k, h, s] = 0;
                                }
                            }
                        }
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            int test_Zero = 0;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        test_Zero = 0;

                        for (int ss = 0; ss < Step_Num; ss++)
                        {
                            if (Power[j, k, h, ss] > 3.2)
                            {
                                test_Zero = 1;
                            }
                        }
                        if (test_Zero == 0)
                        {
                            for (int ss = 0; ss < Step_Num; ss++)
                            {
                                Price[j, k, h, ss] = 0;
                                Power[j, k, h, ss] = 0;
                            }
                        }
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {

                        if ((Power_memory_Pack[j, k, h] > 3.2) & (Power_memory_Pack[j, k, h] < Pmax_Plant_Pack[j, k, h]) & (Package[j, k].ToString().Trim() != "CC"))
                        {
                            Price[j, k, h, 9] = CapPrice[0];
                            Power[j, k, h, 9] = Dispatch_Proposal[j, k, h];
                        }
                        if ((Power_Error_Pack[j, k, h] == 1))
                        {
                            Price[j, k, h, 1] = CapPrice[0];
                            Power[j, k, h, 1] = Dispatch_Proposal[j, k, h];

                            for (int s = 2; s < Step_Num; s++)
                            {
                                Price[j, k, h, 1] = 0;
                                Power[j, k, h, 1] = 0;
                            }
                        }
                        if ((Power_Error_Pack[j, k, h] == 1) & (service_Max[j, k] == Pmax_Plant_Pack[j, k, h]))
                        {
                            Price[j, k, h, 0] = 0;
                            Power[j, k, h, 0] = 0;

                            for (int s = 1; s < Step_Num; s++)
                            {
                                Price[j, k, h, s] = 0;
                                Power[j, k, h, s] = 0;
                            }
                        }
                        if ((Power_Error_Pack[j, k, h] == 1) & (service_Max[j, k] < Pmax_Plant_Pack[j, k, h]) && (service_Max[j, k] > 0))
                        {
                            Price[j, k, h, 1] = CapPrice[0];
                            Power[j, k, h, 1] = Dispatch_Proposal[j, k, h];

                            for (int s = 2; s < Step_Num; s++)
                            {
                                Price[j, k, h, 1] = 0;
                                Power[j, k, h, 1] = 0;
                            }
                        }
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  
            int test_cap = 0;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        test_cap = 0;
                        for (int s = 0; s < Step_Num; s++)
                        {
                            if ((Price[j, k, h, s] >= CapPrice[0]) & (test_cap == 0))
                            {
                                Price[j, k, h, s] = (CapPrice[0]);
                                Power[j, k, h, s] = Dispatch_Proposal[j, k, h];
                                test_cap = 1;
                                if (s < 9)
                                {
                                    for (int ss = (s + 1); ss < Step_Num; ss++)
                                    {
                                        Price[j, k, h, ss] = 0;
                                        Power[j, k, h, ss] = 0;

                                    }
                                }
                            }
                        }
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        test_cap = 0;

                        if (Outservice_Plant_Pack[j, k] == "Yes")
                        {
                            for (int ss = 0; ss < Step_Num; ss++)
                            {
                                Price[j, k, h, ss] = 0;
                                Power[j, k, h, ss] = 0;
                            }
                            Dispatch_Proposal[j, k, h] = 0;
                        }
                    }
                }
            }

                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                for (int s = 1; s < Step_Num; s++)
                                {
                                    if ((Price[j, k, h, s - 1] > Price[j, k, h, s - 1]) | (Power[j, k, h, s - 1] > Power[j, k, h, s - 1]))
                                    {
                                        System.Windows.Forms.MessageBox.Show("   Error_1 : Answer is Fail.   " + "  Plant  " + j.ToString() + "  Unit  " + k.ToString() + "  Hour  " + h.ToString() + "  Step  " + s.ToString());
                                    }
                                }
                            }
                        }
                    }
             

                    //////////////////////////////////////////NEW////////////////////////////////////////////
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {

                                if (forcatingAvailable[j]==false ||
                                    (Price_Margin[j, k, h] == 0 && PriceForecasting[j, k, h] == 0 && Price_optimal[j, k, h] == 0 && Price_UL[j, k, h] == 0)
                                    )
                                {
                                    System.Windows.Forms.MessageBox.Show("Import Data is Invalid!!! \r\nPlease run PriceForecasting Again... ");
                                    return;
                                }
                                //if (Price_Margin[j, k, h] <= PriceForecasting[j, k, h] && PriceForecasting[j, k, h] <= Price_optimal[j, k, h] && Price_optimal[j, k, h] <= Price_UL[j, k, h])
                                //{
                                //    System.Windows.Forms.MessageBox.Show("The Process Has Been Canceled");
                                //    return;
                                //}
                            }
                        }
                    }
                    //////////////////////////////////////////NEW//////////////////////////////////////////
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {

                                if (Price[j, k, h, 0] == 0 && Power[j, k, h, 0] == 0)
                                {

                                    Dispatch_Proposal[j, k, h] = 0;

                                }

                            }

                        }
                    }

            
              

            /////////////////////////////////////////////////////////////////////////////////////


                    int pptype = 0;
                   // string block = "";


                    string[,] blocks002 = Getblock002();

                    for (int j = 0; j < Plants_Num; j++)
                    {
                        strCmd = "delete from DetailFRM002 where TargetMarketDate ='" + biddingDate + "'" +
                     " and ppid='" + plant[j].Trim() + "'";
                        oDataTable = utilities.returntbl(strCmd);

                       
                        /////////////////////))))))))))))))))))))))
                      
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                                                      
                            if (Package[j, k] == "CC")
                                 pptype = 1;
                            else pptype = 0;



                            for (int h = 0; h < Hour; h++)
                            {

                                string firstCommand = "insert into DetailFRM002 (Estimated,TargetMarketDate, PPID,PPType,Block, Hour,DeclaredCapacity,DispachableCapacity ";

                                string secondCommand = "values (1,'" + biddingDate + "','" + plant[j] + "','" +
                                    pptype.ToString() + "','" + blocks002[j,k] + "'," +
                                    (h + 1).ToString() + "," +
                                    Dispatch_Proposal[j, k, h].ToString() + ","
                                    + Dispatch_Proposal[j, k, h].ToString().Trim();
                                for (int s = 0; s < Step_Num; s++)
                                {
                                    firstCommand += ",Power" + (s + 1).ToString().Trim();
                                    secondCommand += "," + Power[j, k, h, s].ToString().Trim();

                                    firstCommand += ",Price" + (s + 1).ToString().Trim();
                                    secondCommand += "," + Price[j, k, h, s].ToString().Trim();

                                   
                                }
                                firstCommand += " ) ";
                                secondCommand += " ) ";
                                //for (int s = 0; s < Step_Num; s++)
                                //{
                                //oDataTable = utilities.returntbl("insert into Table_1 values('" + plant[j].ToString() + "','" + k.ToString() + "','" + h.ToString() + "','" + Pmax_Plant_Pack[j, k, h].ToString() + "','" + Power[j, k, h, s].ToString() + "','" + Price[j, k, h, s].ToString() + "','" + s.ToString() + "' )");
                                oDataTable = utilities.returntbl(firstCommand + secondCommand);
                                //}
                            }
                        }
                    }


            ///////////////////////////////////////end///////////////////////////////////////////
                    System.Windows.Forms.MessageBox.Show(" Bidding Strategy Process Completed.");
            ////////////////////////////////////////////////////////////////////////////////////

                }
                    
    

        private double[] GetForcast(string ppId, MixUnits mixUnitStatus, string date)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText = "select FinalForecastHourly.hour, FinalForecastHourly.forecast" +
                    " from FinalForecastHourly inner join FinalForecast" +
                    " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id" +
                    " where FinalForecast.PPId=@ppID AND FinalForecast.MixUnitStatus=@mixUnitStatus AND FinalForecast.date=@date" +
                    " order by FinalForecastHourly.hour";

            MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
            MyCom.Parameters["@ppID"].Value = ppId.ToString().Trim();
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = date;
            MyCom.Parameters.Add("@mixUnitStatus", SqlDbType.Char, 20);
            MyCom.Parameters["@mixUnitStatus"].Value = mixUnitStatus.ToString().Trim();
            Maxda.Fill(MaxDS);

            double[] forecasts = new double[24];
            if (MaxDS.Tables[0].Rows.Count > 0)
                for (int j = 0; j < 24; j++)
                    forecasts[j] = Double.Parse(MaxDS.Tables[0].Rows[j]["forecast"].ToString());
            return forecasts;
        }

        //private double[] GetForcast(string date)
        //{
        //    double[] forecasts = new double[9,24];

        //    SqlConnection MyConnection = new SqlConnection();
        //    MyConnection.ConnectionString = ConStr;
        //    MyConnection.Open();

        //    SqlCommand MyCom = new SqlCommand();
        //    MyCom.Connection = MyConnection;
        //    DataSet MaxDS = new DataSet();
        //    SqlDataAdapter Maxda = new SqlDataAdapter();
        //    Maxda.SelectCommand = MyCom;

        //    // Insert into FinalForcast table
        //    MyCom.CommandText = "select FinalForecastHourly.forecast" +
        //            " from FinalForecastHourly inner join FinalForecast" +
        //            " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id" +
        //            " where FinalForecast.PPId=@ppID AND FinalForecast.MixUnitStatus=@mixUnitStatus AND FinalForecast.date=@date" +
        //            " order by FinalForecastHourly.hour";

        //    MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
        //    MyCom.Parameters["@ppID"].Value = ppId.ToString().Trim();
        //    MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
        //    MyCom.Parameters["@date"].Value = date;
        //    MyCom.Parameters.Add("@mixUnitStatus", SqlDbType.Char, 20);
        //    MyCom.Parameters["@mixUnitStatus"].Value = mixUnitStatus.ToString().Trim();
        //    Maxda.Fill(MaxDS);

        //    if (MaxDS.Tables[0].Rows.Count > 0)
        //        for (int j = 0; j < 24; j++)
        //            forecasts[j] = Double.Parse(MaxDS.Tables[0].Rows[j][0].ToString());
        //    return forecasts;
        //}

        private double[] GetVariance(string ppId, MixUnits mixUnitStatus, string date)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText = "select FinalForecastHourly.hour, FinalForecastHourly.vr" +
                    " from FinalForecastHourly inner join FinalForecast" +
                    " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id" +
                    " where FinalForecast.PPId=@ppID AND FinalForecast.MixUnitStatus=@mixUnitStatus AND FinalForecast.date=@date" +
                    " order by FinalForecastHourly.hour";

            MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
            MyCom.Parameters["@ppID"].Value = ppId.ToString().Trim();
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = date;
            MyCom.Parameters.Add("@mixUnitStatus", SqlDbType.Char, 20);
            MyCom.Parameters["@mixUnitStatus"].Value = mixUnitStatus.ToString().Trim();
            Maxda.Fill(MaxDS);

            double[] variance = new double[24];
            if (MaxDS.Tables[0].Rows.Count > 0)
                for (int j = 0; j < 24; j++)
                    variance[j] = Double.Parse(MaxDS.Tables[0].Rows[j]["vr"].ToString());
            return variance;
        }

        private double[,] Get_pd_Range(string ppId, MixUnits mixUnitStatus, string date)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            double[,] pd_Range = new double[24, 201];

            for (int j = 0; j < 24; j++)
            {
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;

                // Insert into FinalForcast table
                MyCom.CommandText =
                    "select FinalForecast201Item.[index], FinalForecast201Item.ff" +
                    " from FinalForecast201Item inner join FinalForecastHourly " +
                    "on FinalForecastHourly.id = FinalForecast201Item.fk_ffHourly " +
                    "inner join FinalForecast " +
                    "on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id " +
                    "where FinalForecast.PPId=@PPId AND FinalForecast.MixUnitStatus=@mixunit " +
                    "AND FinalForecast.date=@date AND FinalForecastHourly.hour=@hour " +
                    "order by FinalForecast201Item.[index]";


                MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                MyCom.Parameters["@ppID"].Value = ppId.ToString().Trim();
                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = date;
                MyCom.Parameters.Add("@mixunit", SqlDbType.Char, 20);
                MyCom.Parameters["@mixunit"].Value = mixUnitStatus.ToString().Trim();
                MyCom.Parameters.Add("@hour", SqlDbType.Int);
                MyCom.Parameters["@hour"].Value = j + 1;
                Maxda.Fill(MaxDS);

                if (MaxDS.Tables[0].Rows.Count > 0)
                    for (int k = 0; k < 201; k++)
                        pd_Range[j, k] = Double.Parse(MaxDS.Tables[0].Rows[k]["ff"].ToString());
            }
            return pd_Range;
        }

        private double[,] Get_pdist(string ppId, MixUnits mixUnitStatus, string date)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            double[,] pdist = new double[24, 201];

            for (int j = 0; j < 24; j++)
            {
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;

                // Insert into FinalForcast table
                MyCom.CommandText =
                    "select FinalForecast201Item.pdist" +
                    " from FinalForecast201Item inner join FinalForecastHourly " +
                    "on FinalForecastHourly.id = FinalForecast201Item.fk_ffHourly " +
                    "inner join FinalForecast " +
                    "on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id " +
                    "where FinalForecast.PPId=@PPId AND FinalForecast.MixUnitStatus=@mixunit " +
                    "AND FinalForecast.date=@date AND FinalForecastHourly.hour=@hour " +
                    "order by FinalForecast201Item.[index]";


                MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                MyCom.Parameters["@ppID"].Value = ppId.ToString().Trim();
                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = date;
                MyCom.Parameters.Add("@mixunit", SqlDbType.Char, 20);
                MyCom.Parameters["@mixunit"].Value = mixUnitStatus.ToString().Trim();
                MyCom.Parameters.Add("@hour", SqlDbType.Int);
                MyCom.Parameters["@hour"].Value = j + 1;
                Maxda.Fill(MaxDS);

                if (MaxDS.Tables[0].Rows.Count > 0)
                    for (int k = 0; k < 201; k++)
                        pdist[j, k] = Double.Parse(MaxDS.Tables[0].Rows[k][0].ToString());
            }
            return pdist;
        }

        private double[] Get_HourCap(string ppId, string date)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            double[] hourCap = new double[24];

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText =
                "select BiddingStrategyCustomized.hour, BiddingStrategyCustomized.Peak " +
            "from BiddingStrategyCustomized inner join BiddingStrategySetting " +
            "on BiddingStrategyCustomized.FkBiddingStrategySettingId = BiddingStrategySetting.id " +
            "where BiddingStrategySetting.BiddingDate= @date AND BiddingStrategySetting.Plant=@ppID " +
            "order by BiddingStrategyCustomized.hour";

            MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
            MyCom.Parameters["@ppID"].Value = ppId.ToString().Trim();
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = date;

            Maxda.Fill(MaxDS);

            if (MaxDS.Tables[0].Rows.Count > 0)
                for (int k = 0; k < 24; k++)
                    hourCap[k] = Double.Parse(MaxDS.Tables[0].Rows[k]["Peak"].ToString());

            return hourCap;
        }

        private string GetDaysBefore(string strDate, int days)
        {
            DateTime date = PersianDateConverter.ToGregorianDateTime(strDate);
            date = date.Subtract(new TimeSpan(days, 0, 0, 0));
            return new PersianDate(date).ToString("d");
        }

        private double[] GetNearestLoadForecastingDateVlues(string biddingDate)
        {
            double[] result = new double[24];

            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

           // double[] hourCap = new double[24];

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText =
                "select Date,DateEstimate from LoadForecasting where Date<='" + biddingDate.Trim() + "'";
            Maxda.Fill(MaxDS);


            DateTime dtDate = PersianDateConverter.ToGregorianDateTime(biddingDate);
            DataRow rowNearest = null;
            TimeSpan minSpan = new TimeSpan();
            //if (MaxDS.Tables[0].Rows.Count>0)
            //    minSpan = 
            foreach (DataRow row in MaxDS.Tables[0].Rows)
            {
                DateTime tempDate = PersianDateConverter.ToGregorianDateTime
                    (row["Date"].ToString().Trim());
                TimeSpan tempSpan = dtDate - tempDate;
                if (rowNearest == null || minSpan > tempSpan)
                {
                    minSpan = tempSpan;
                    rowNearest = row;
                }
            }

            if (rowNearest != null)
            {
                string date2 = rowNearest["Date"].ToString().Trim();

                //////////////////////////////////////////
                // in case there are more than one date, find the one with the maxmimum DateEstimate
                MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                MaxDS = new DataSet();
                Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;

                // Insert into FinalForcast table

                MyCom.CommandText =
                    "select LoadForecasting.* from LoadForecasting where Date='" + date2.ToString().Trim() + "'";
                Maxda.Fill(MaxDS);


                DataRow maxDateRow = null;
                string maxDate = null;

                foreach (DataRow row in MaxDS.Tables[0].Rows)
                {
                    string temp = row["DateEstimate"].ToString().Trim();
                    if (maxDate == null || String.Compare(maxDate, temp) < 0)
                    {
                        maxDate = temp;
                        maxDateRow = row;
                    }
                }

                for (int h = 0; h < 24; h++)
                {
                    string colName = "Hour" + (h + 1).ToString().Trim();
                    result[h] = double.Parse(maxDateRow[colName].ToString());
                }
            }

            return result;
        }

        private double[, ,] InsertToPriceForecasting(DataTable dt, double[, ,] PriceForecastings, int plantIndex, int row)
        {
            for (int hour = 0; hour < dt.Rows.Count; hour++)
                PriceForecastings[plantIndex, hour, row] = double.Parse(dt.Rows[hour]["forecast"].ToString().Trim());
            return PriceForecastings;
        }

        private string[,] Getblock()
        {

            string[,] blocks = new string[Plants_Num, Package_Num];
            string temp="";

            for (int j = 0; j < Plants_Num; j++)
            {
              //  int x=int.Parse(plant[j]);
               // oDataTable = utilities.returntbl("select  PackageType,UnitCode,PackageCode from dbo.UnitsDataMain where PPID='" + plant[j] + "'order by UnitCode");
                
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {

                    

                    oDataTable = utilities.returntbl("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + (k + 1).ToString() + "'");

                    int ppid = int.Parse(plant[j]);

                    DataRow myRow = oDataTable.Rows[0];
                    
                    string packageType = myRow["PackageType"].ToString().Trim();
                 
                    string Initial = "";
                    if (packageType == "CC")
                    {
                        Initial = "C"+ (k + 1).ToString();

                        if (ppid == 131 || ppid == 144)
                        {
                            ppid++;
                        }
                    }

                    else if (packageType == "Gas")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
    plant[j] + "'" + " and packageCode='" + (k + 1).ToString() + "' and packageType ='Gas'");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
plant[j] + "'" + " and packageCode='" + (k + 1).ToString() + "' and packageType ='Steam'");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                    }


                    string blockName = ppid.ToString() + "-" + Initial;

                    blocks[j, k] = blockName;

                   
                   
                }
            }
            return blocks;

        }


       private string[,] Getblock002()
        {
            string[,] blocks1 = new string[Plants_Num, Package_Num];
            string temp = "";

            for (int j = 0; j < Plants_Num; j++)
            {
                

                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {

                    oDataTable = utilities.returntbl("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + (k + 1).ToString() + "'");

                    int ppid = int.Parse(plant[j]);

                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {
                        Initial = "C" + (k + 1).ToString();
                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
    plant[j] + "'" + " and packageCode='" + (k + 1).ToString() + "' and packageType ='Gas'");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
plant[j] + "'" + " and packageCode='" + (k + 1).ToString() + "' and packageType ='Steam'");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                    }

                    string blockName1 = Initial;

                    blocks1[j, k] = blockName1;

                }
            }
            return blocks1;
         
 
        
        }







    }
}
