using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.DMS.Common;

namespace PowerPlantProject
{
    public partial class BiddingStrategyProgressForm : Form
    {
        int biddingStrategySettingId;
        string PlantlblValue;
        string CurrentDateValue;
        string BiddingDateValue;
        int TrainingDays=40;
        bool firstDll, secondDll;
        MainForm parentForm;
        bool btnExportEnable=false;
        string titleForm = "";
        /// <summary>
        /// 
        /// </summary>


        public int BiddingStrategySettingId
        {

            get { return biddingStrategySettingId; }
            set { biddingStrategySettingId = value; }
        }

        public BiddingStrategyProgressForm(MainForm form2)
        {
            parentForm = form2;
            InitializeComponent();
        }

        private void CLoadFlowProgressForm_Load(object sender, EventArgs e)
        {
            this.FormClosing += new FormClosingEventHandler(CLoadFlowProgressForm_OnClosing);
            loadDataFromDB();
            LongTaskBegin();

        }
        private void loadDataFromDB()
        {
            try
            {
                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();


                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = myConnection;
                MyCom.CommandText = "select * from [BiddingStrategySetting] " +
                    "where id=" + biddingStrategySettingId;

                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;
                Maxda.Fill(MaxDS);

                DataTable dt = MaxDS.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    PlantlblValue = row["Plant"].ToString().Trim();
                    CurrentDateValue = row["CurrentDate"].ToString().Trim();
                    BiddingDateValue = row["BiddingDate"].ToString().Trim();
                    TrainingDays = Convert.ToInt32(row["TrainingDays"].ToString().Trim());
                    firstDll = Convert.ToBoolean(row["FlagForecastingPrice"].ToString().Trim());
                    secondDll = Convert.ToBoolean(row["FlagBidAllocation"].ToString().Trim());
                    if (Convert.ToBoolean(row["FlagStrategyBidding"].ToString().Trim()))
                    {
                        secondDll = true;
                        firstDll = true;
                    }

                }
                myConnection.Close();
            }
            catch
            {
            }

        }


        private void LongTaskBegin()
        {
            progressBar1.Maximum = 24+2;
            parentForm.EnableBiddingStrategyTab(false);
            Thread thread = new Thread(RunBidding);
            thread.IsBackground = true;
            thread.Start();
        }

        public void UpdateProgressBar(int i, string description)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<int,string>(UpdateProgressBar), new object[] { i, description });
                return;
            }
            lblTitle.Text = description;
            this.Text = titleForm;
            progressBar1.Value = i;
            Thread.Sleep(1);
            //if (cancel)
            //    Thread.CurrentThread.Abort();
        }

        private void RunBidding()
        {

            string strCmd = "select * from powerplant";

            if (PlantlblValue.ToLower() != "all")
            {
                strCmd += " WHERE PPName= '" + PlantlblValue + "'";
            }
            DataTable dtPlants = utilities.returntbl(strCmd);
            int plants_Num = dtPlants.Rows.Count;
            bool[] firstDLLResults = new bool[dtPlants.Rows.Count];


            if (firstDll)
            {
                try
                {
                    int i = 0;

                    foreach (DataRow row in dtPlants.Rows)
                    {
                        this.titleForm = "  Price Forecasting for " + 
                            row["PPName"].ToString().Trim() + "...";


                        firstDLLResults[i++] = CalculateBidding(
                                PersianDateConverter.ToGregorianDateTime(CurrentDateValue),
                                PersianDateConverter.ToGregorianDateTime(BiddingDateValue),
                                int.Parse(row["PPID"].ToString().Trim()),
                            row["PPName"].ToString().Trim());
                    }
                }
                catch (DllNotFoundException ex1)
                {
                    MessageBox.Show("A problem occured in Matlab RunTime Component!\r\nPlease inform the administrator...");
                }
                catch (TypeInitializationException ex2)
                {
                    MessageBox.Show("A problem occured in Matlab RunTime Component!\r\nPlease inform the administrator...");
                }
                catch (Exception exgeneral)
                {
                    MessageBox.Show("A problem has occured!\r\nPlease Try Later...");
                    Log(exgeneral.Message);
                }
            }

            if (secondDll)
            {
                bool canRunSecondDll = true;

                if (firstDll == true)
                {
                    for (int i = 0; i < plants_Num; i++)
                        if (firstDLLResults[i] == false)
                            canRunSecondDll = false;
                }
                if (canRunSecondDll)
                {
                    //try
                    //{
                        Classfinal classFinal = 
                            new Classfinal(PlantlblValue,CurrentDateValue, BiddingDateValue );
                        string plantname = PlantlblValue;
                        if (plantname.ToLower() == "all")
                            plantname += " plants";
                        this.titleForm = "  Bid Allocation for " + plantname + "...";
                        UpdateProgressBar(26, "" );
                        classFinal.value();
                        ///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        btnExportEnable = true;
                    //}
                    //catch (DllNotFoundException ex)
                    //{
                    //    MessageBox.Show("A problem occured in CPlex Registration!\r\nPlease inform the administrator...");
                    //}
                    //catch (Exception exgeneral)
                    //{
                    //    MessageBox.Show("A problem has occured!\r\nPlease Try Later...");
                    //    Log(exgeneral.Message);
                    //}
                }
                else
                {
                    string str = "Due to unsuccessfull Price Forcasting, bid allocation is not possible...";
                    MessageBox.Show(str);
                }
            }
        }

        private double[] Get40Day_PowerPlantPrices(int ppId, DataSet dsUnits, DateTime baseDate, DateTime dateTime, MixUnits mixUnitStatus)
        {
            int maxAttemp = 3;

            int maxCount = TrainingDays * 24;
            double[] prices = new double[maxCount];

            for (int daysbefore = 0; daysbefore < TrainingDays; daysbefore++)
            {
                //Application.DoEvents();
                DateTime selectedDate = PersianDateConverter.ToGregorianDateTime(dateTime).Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                string date = new PersianDate(selectedDate).ToString("d");

                //CMatrix oneDayPrice = GetOneDayPrices(date);
                CMatrix oneDayPrice = CalculatePowerPlantMaxBid(ppId, dsUnits, selectedDate, mixUnitStatus);

                // in case price is null we iterate three times;
                int attemp = 0;
                while ((oneDayPrice == null || oneDayPrice.Zero)
                        && selectedDate <= baseDate && attemp < maxAttemp)
                {
                    attemp++;
                    selectedDate = PersianDateConverter.ToGregorianDateTime(dateTime).Subtract(new TimeSpan(daysbefore + attemp, 0, 0, 0));
                    date = new PersianDate(selectedDate).ToString("d");
                    oneDayPrice = CalculatePowerPlantMaxBid(ppId, dsUnits, selectedDate, mixUnitStatus);
                }
                if ((oneDayPrice == null || oneDayPrice.Zero) && selectedDate > baseDate)
                {
                    // can read from forcast
                    DataTable forcast = GetForcast(ppId, mixUnitStatus, date);
                    if (forcast.Rows.Count > 0)
                    {
                        oneDayPrice = new CMatrix(1, 24);
                        foreach (DataRow row in forcast.Rows)
                        {
                            int hour = Int32.Parse(row["hour"].ToString());
                            double value = double.Parse(row["forecast"].ToString());
                            oneDayPrice[0, hour - 1] = value;
                        }
                    }
                }
                if (oneDayPrice != null)
                {
                    for (int hour = 0; hour < 24; hour++)
                        prices[maxCount - (24 * daysbefore) - 24 + hour] = oneDayPrice[0, hour];
                }
                else
                {
                    MessageBox.Show("No Data for selected Date!");

                }

            }// end loop for days

            //for (int l=0; l<maxCount;l++)
            //    prices[l]=456;
            return prices;

        }

        private CMatrix GetOneDayPricesForEachUnit(int ppid, string date, string blockM005, string blockM002)
        {
            blockM005 = blockM005.Trim();
            blockM002 = blockM002.Trim();

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            //DataTable oDataTable = utilities.returntbl("select max(PMax) from UnitsDataMain where ppid='" + ppid.ToString() + "'");
            //double pMaxUnitDataMain = 0;
            //try
            //{
            //    pMaxUnitDataMain = double.Parse(oDataTable.Rows[0][0].ToString());
            //}
            //catch
            //{
            //}

            CMatrix price = new CMatrix(1, 24);

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();

            Myda.SelectCommand = new SqlCommand("SELECT Hour,Required FROM [DetailFRM005] WHERE PPID= '" + ppid.ToString() + "' AND TargetMarketDate=@date AND Block=@block",
                myConnection);
            Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
            Myda.SelectCommand.Parameters["@date"].Value = date;
            Myda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
            Myda.SelectCommand.Parameters["@block"].Value = blockM005;
            Myda.Fill(MyDS);
            DataTable dt = MyDS.Tables[0];
            Myda.SelectCommand.Connection.Close();

            double[] requiredTable = new double[24];

            // if price exist
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow MyRow in dt.Rows)
                {
                    int x = int.Parse(MyRow["Hour"].ToString());
                    requiredTable[x - 1] = double.Parse(MyRow["Required"].ToString());
                }

                /////////// calculate price
                double[] maxBid = new double[24];
                //Detect MaxBid field in MRCurGrid1



                for (int index = 0; index < 24; index++)
                {

                    DataSet MaxDS = new DataSet();
                    SqlDataAdapter Maxda = new SqlDataAdapter();
                    string strCmd = "SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                        "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10,DispachableCapacity FROM [DetailFRM002] " +
                        "WHERE TargetMarketDate=@date AND Block=@block AND Hour=@hour AND PPID='" + ppid.ToString() + "'" +
                        " and ( Estimated is null or Estimated = 0)";
                    Maxda.SelectCommand = new SqlCommand(strCmd, myConnection);
                    Maxda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                    Maxda.SelectCommand.Parameters["@date"].Value = date;
                    Maxda.SelectCommand.Parameters.Add("@hour", SqlDbType.SmallInt);
                    Maxda.SelectCommand.Parameters["@hour"].Value = index + 1;
                    Maxda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
                    Maxda.SelectCommand.Parameters["@block"].Value = blockM002;

                    Maxda.Fill(MaxDS);
                    Myda.SelectCommand.Connection.Close();

                    double required = requiredTable[index];
                    double DispachableCapacity = 0;
                    try
                    {
                        DispachableCapacity = double.Parse(MaxDS.Tables[0].Rows[0]["DispachableCapacity"].ToString());
                    }
                    catch { }
                    /////~~~~~~~~~~~~~~~~ BEGIN  FOR PARAND!!!!!!!!!!!!!    ~~~~~~~~~~~~~~~~~~~~~~~~~~
                    if (DispachableCapacity < required && DispachableCapacity > 0)
                    {
                        maxBid[index] = 0;
                        string str = "select price1,price2,price3,price4,price5,price6,price7,price8,price9,price10" +
                            " from dbo.DetailFRM002 where ppid='" + ppid.ToString() + "'" +
                            " and TargetMarketDate='" + date + "'" +
                            " AND Hour='" + (index + 1).ToString() + "'" +
                            " AND block='" + blockM002 + "'";

                        DataTable oDataTable = utilities.returntbl(str);
                        double max = 0;
                        if (oDataTable.Rows.Count > 0)
                            for (int i = 0; i < 10; i++)
                            {
                                if (double.Parse(oDataTable.Rows[0][i].ToString()) > max)
                                    max = double.Parse(oDataTable.Rows[0][i].ToString());
                            }

                        maxBid[index] = max;
                    }
                    /////~~~~~~~~~~~~~~~~ END  FOR PARAND!!!!!!!!!!!!!    ~~~~~~~~~~~~~~~~~~~~~~~~~~

                    else
                    {
                        foreach (DataRow MaxRow in MaxDS.Tables[0].Rows)
                        {
                            int Dsindex = 0;
                            while ((Dsindex < 20) && (Math.Round(double.Parse(MaxRow[Dsindex].ToString()), 1) < required))
                                Dsindex = Dsindex + 2;
                            if (Dsindex < 20)
                                maxBid[index] = double.Parse(MaxRow[Dsindex + 1].ToString());
                            else
                                maxBid[index] = 0;

                        }
                    }

                    price[0, index] = maxBid[index];

                }

            }
            else
                price = null;

            myConnection.Close();
            return price;

        }

        private DataTable GetForcast(int ppId, MixUnits mixUnitStatus, string date)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText = "select FinalForecastHourly.hour, FinalForecastHourly.forecast" +
                    " from FinalForecastHourly inner join FinalForecast" +
                    " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id" +
                    " where FinalForecast.PPId=@ppID AND FinalForecast.MixUnitStatus=@mixUnitStatus AND FinalForecast.date=@date" +
                    " order by FinalForecastHourly.hour";

            MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
            MyCom.Parameters["@ppID"].Value = ppId.ToString();
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = date;
            MyCom.Parameters.Add("@mixUnitStatus", SqlDbType.Char, 20);
            MyCom.Parameters["@mixUnitStatus"].Value = mixUnitStatus.ToString();
            Maxda.Fill(MaxDS);

            myConnection.Close();


            return MaxDS.Tables[0];
        }

        private DataTable[] OrderPackages(DataSet myDs)
        {
            DataTable[] orderedPackages = new DataTable[myDs.Tables[0].Rows.Count];
            int count = 0;
            foreach (string typeName in Enum.GetNames(typeof(PackageTypePriority)))
            {
                //////////// have to make sure l;ater whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                if (myDs.Tables.Contains(typeName))
                    orderedPackages[count++] = myDs.Tables[myDs.Tables.IndexOf(typeName)];
            }
            return orderedPackages;
        }

        public bool CalculateBidding(DateTime baseDate, DateTime fututeDate, int ppId, string ppname)
        {

            DataSet myDs = GetUnits(ppId);

            DataTable[] orderedPackages = OrderPackages(myDs);

            MixUnits mixUnitsStatus = (ppId == 131 || ppId == 144 ? MixUnits.JustCombined : MixUnits.All);

            int forcastDaysAhead = (new DateTime(fututeDate.Year, fututeDate.Month, fututeDate.Day, 0, 0, 0) - new DateTime(baseDate.Year, baseDate.Month, baseDate.Day, 0, 0, 0)).Days;

            /** !!!!!!!!!!! log */
            DateTime logTimebase = DateTime.Now;
            Core_pf_Dll.Core_pf_Class oCore_pf = new Core_pf_Class();
            DateTime logTimeCur = DateTime.Now;
            Log("Core_pf_Dll instance", logTimeCur - logTimebase);
            /** !!!!!!!!!!!!!!! */

            do
            {

                int loc_n = 24 * TrainingDays + 1 /*   */;

                for (int k = 1; k <= forcastDaysAhead; k++)
                {

                    string strDayCount = " Day " + k.ToString() + " (of " + forcastDaysAhead.ToString() + " days) ...";
                    //Log("POWER PLANT " + PPID.ToString() + ", DAY " + k.ToString());
                    DateTime curDate = baseDate.AddDays(k);

                    UpdateProgressBar(1, "Getting Data For" + strDayCount);

                    /** !!!!!!!!!!! log */
                    logTimebase = DateTime.Now;
                    double[] price = Get40Day_PowerPlantPrices(ppId, myDs, baseDate, curDate, mixUnitsStatus);
                    logTimeCur = DateTime.Now;
                    Log("Get40Day PowerPlantPrices", logTimeCur - logTimebase);
                    /** !!!!!!!!!!!!!!! */
                    bool noData = true;

                    for (int i = 0; i < 24 * TrainingDays && noData; i++)
                        if (price[i] != 0)
                            noData = false;

                    if (noData)
                    {
                        string str = "Due to lack of Data, Price Forecasting is not possible for " + ppname + " righ now." +
                            "\r\nYou need to load M002 Data for this plant...";

                        MessageBox.Show(str);
                        return false;
                    }

                    int Test = 0;
                    int[] Error_price = new int[24 * TrainingDays];
                    for (int x = 72; x < 24 * TrainingDays; x++)
                    {
                        Test = 0;
                        if ((price[x] == 0))
                        {
                            if (price[x - 24] != 0)
                            {
                                price[x] = price[x - 24];
                                Test = 1;
                            }
                            else if (x - 48 >= 0 && (price[x - 48] != 0) && (Test == 0))
                            {
                                price[x] = price[x - 48];
                                Test = 1;
                            }
                            else if (x - 72 >= 0 && (price[x - 72] != 0) && (Test == 0))
                            {
                                price[x] = price[x - 72];
                                Test = 1;
                            }
                            if ((price[x] == 0))
                            {
                                Error_price[x] = 1;
                            }
                        }
                    }

                    /** !!!!!!!!!!! log */
                    UpdateProgressBar(2, "Init Processing" + strDayCount);
                    
                    logTimebase = DateTime.Now;
                    CMatrix LC_H_N = Get40Day_LC_HValue(curDate);
                    logTimeCur = DateTime.Now;
                    Log("Get40Day Line Interchange Value", logTimeCur - logTimebase);
                    /** !!!!!!!!!!!!!!! */
                    ///**************************************************/


                    double[] final_Forecast = new double[24];
                    double[] vr = new double[24];

                    CMatrix ff = new CMatrix(0, 201); // a new row gets added in each j loop
                    CMatrix pdist = new CMatrix(0, 201); // a new row gets added in each j loop

                    int[,] Co = new int[2, TrainingDays - 7];
                    for (int i = 0; i <= TrainingDays - 8; i++)
                    {
                        Co[0, i] = 0;
                        Co[1, i] = 1;
                    }
                    Co[0, 1] = 2;
                    Co[1, 1] = 2;

                    double[,] Ee = new double[1, TrainingDays - 7];
                    for (int i = 0; i <= TrainingDays - 8; i++)
                    {
                        Ee[0, i] = 0;
                    }
                    Ee[0, 2] = 0.00000001;

                    string description = "Processing" + strDayCount;

                    for (int j = 1; j <= 24; j++)
                    {
                        UpdateProgressBar(j + 2, description);

                        CMatrix P = new CMatrix(43, TrainingDays - 4);
                        CMatrix T = new CMatrix(1, TrainingDays - 5);

                        /** !!!!!!!!!!! log */
                        logTimebase = DateTime.Now;
                        /** !!!!!!!!!!!!!!! */

                        for (int i = 1; i <= TrainingDays - 8; i++)
                        {
                            int e = 24;

                            int temp1 = loc_n - (24 * i) + j;
                            int temp2 = loc_n - (24 * i) - e + j;

                            P[0, i - 1] = (price[temp1 - 2] - price[temp2 - 2] + Ee[0, i]) / price[temp2 - 2];
                            P[1, i - 1] = (price[temp1 - 2] - price[temp2 - 3] + Ee[0, i]) / price[temp2 - 3];
                            P[2, i - 1] = (price[temp1 - 2] - price[temp2 - 1] + Ee[0, i]) / price[temp2 - 1];
                            P[3, i - 1] = (price[temp1 - 2] - price[temp2 - 4] + Ee[0, i]) / price[temp2 - 4];
                            P[4, i - 1] = (price[temp1 - 2] - price[temp2] + Ee[0, i]) / price[temp2];

                            P[5, i - 1] = (price[temp1 - 3] - price[temp2 - 3] + Ee[0, i]) / price[temp2 - 3];
                            P[6, i - 1] = (price[temp1 - Co[1, i]] - price[temp2 - 1] + Ee[0, i]) / price[temp2 - 1];
                            P[7, i - 1] = (price[temp1 - 4] - price[temp2 - 4] + Ee[0, i]) / price[temp2 - 4];
                            P[8, i - 1] = (price[temp1 - Co[0, i]] - price[temp2] + Ee[0, i]) / price[temp2];

                            e = 48;
                            temp2 = loc_n - 24 * i - e + j;
                            P[9, i - 1] = (price[temp1 - 2] - price[temp2 - 2] + Ee[0, i]) / price[temp2 - 2];
                            P[10, i - 1] = (price[temp1 - 2] - price[temp2 - 3] + Ee[0, i]) / price[temp2 - 3];
                            P[11, i - 1] = (price[temp1 - 2] - price[temp2 - 1] + Ee[0, i]) / price[temp2 - 1];
                            P[12, i - 1] = (price[temp1 - 2] - price[temp2 - 4] + Ee[0, i]) / price[temp2 - 4];
                            P[13, i - 1] = (price[temp1 - 2] - price[temp2] + Ee[0, i]) / price[temp2];

                            P[14, i - 1] = (price[temp1 - 3] - price[temp2 - 3] + Ee[0, i]) / price[temp2 - 3];
                            P[15, i - 1] = (price[temp1 - Co[1, i]] - price[temp2 - 1] + Ee[0, i]) / price[temp2 - 1];
                            P[16, i - 1] = (price[temp1 - 4] - price[temp2 - 4] + Ee[0, i]) / price[temp2 - 4];
                            P[17, i - 1] = (price[temp1 - Co[0, i]] - price[temp2] + Ee[0, i]) / price[temp2];

                            e = 168;
                            temp2 = loc_n - 24 * i - e + j;
                            P[18, i - 1] = (price[temp1 - 2] - price[temp2 - 2] + Ee[0, i]) / price[temp2 - 2];
                            P[19, i - 1] = (price[temp1 - 2] - price[temp2 - 3] + Ee[0, i]) / price[temp2 - 3];
                            P[20, i - 1] = (price[temp1 - 2] - price[temp2 - 1] + Ee[0, i]) / price[temp2 - 1];
                            P[21, i - 1] = (price[temp1 - 2] - price[temp2 - 4] + Ee[0, i]) / price[temp2 - 4];
                            P[22, i - 1] = (price[temp1 - 2] - price[temp2] + Ee[0, i]) / price[temp2];

                            P[23, i - 1] = (price[temp1 - 3] - price[temp2 - 3] + Ee[0, i]) / price[temp2 - 3];
                            P[24, i - 1] = (price[temp1 - Co[1, i]] - price[temp2 - 1] + Ee[0, i]) / price[temp2 - 1];
                            P[25, i - 1] = (price[temp1 - 4] - price[temp2 - 4] + Ee[0, i]) / price[temp2 - 4];
                            P[26, i - 1] = (price[temp1 - Co[0, i]] - price[temp2] + Ee[0, i]) / price[temp2];

                            //P[27, i-1] = A(temp1 - 1, 4);
                            string strDayOfWeek =
                                curDate.Subtract(new TimeSpan(temp1 - 1, 0, 0)).
                                    DayOfWeek.ToString();

                            int dayIndex;
                            for (dayIndex = 1; dayIndex <= 7; dayIndex++)
                            {
                                Days curDay = (Days)Enum.Parse(typeof(Days), dayIndex.ToString());
                                if (curDay.ToString() == strDayOfWeek)
                                    break;
                            }
                            P[27, i - 1] = dayIndex;

                            for (int b = 0; b < 15; b++)
                            {
                                try
                                {
                                    P[b + 28, i - 1] = LC_H_N[b, temp1 - 2] + Ee[0, i];
                                }
                                catch (Exception)
                                {
                                    throw;
                                }
                            }

                            if (i - 2 >= 0)
                                T[0, i - 2] = ((price[temp1 + 24 - 2] - price[temp1 - 2]) + Ee[0, i])
                                                / price[temp1 - 2];
                        }

                        /** !!!!!!!!!!! log */
                        logTimeCur = DateTime.Now;
                        Log("40 loop for each hour", logTimeCur - logTimebase);
                        /** !!!!!!!!!!!!!!! */

                        CMatrix XInput = P.GetColumn(0);
                        P = P.RemoveAtColumn(0);

                        ///////////


                        //if (!P.Zero && !XInput.Zero && !T.Zero)
                        //{
                        /** !!!!!!!!!!! log */
                        logTimebase = DateTime.Now;
                        /** !!!!!!!!!!!!!!! */
                        //Application.DoEvents();
                        MWArray[] argsOut = oCore_pf.core_pf(4,
                            (MWArray)DoubleArrayToMWNumericArray(P.GetDoubleMatrix()),
                            (MWArray)DoubleArrayToMWNumericArray(XInput.GetDoubleMatrix()),
                            (MWArray)DoubleArrayToMWNumericArray(T.GetDoubleMatrix()));
                        //Application.DoEvents();
                        MWNumericArray temp = new MWNumericArray();
                        temp = (MWNumericArray)argsOut[0];
                        CMatrix q = new CMatrix(MWNumericArrayToDoubleArray(temp, 1, 1));

                        temp = (MWNumericArray)argsOut[1];
                        CMatrix xOut = new CMatrix(MWNumericArrayToDoubleArray(temp, 1, 201));

                        temp = (MWNumericArray)argsOut[2];
                        CMatrix pdist_n = new CMatrix(MWNumericArrayToDoubleArray(temp, 400, 201));

                        temp = (MWNumericArray)argsOut[3];
                        CMatrix s = new CMatrix(MWNumericArrayToDoubleArray(temp, 1, 1));
                        //Application.DoEvents();
                        /** !!!!!!!!!!! log */
                        logTimeCur = DateTime.Now;
                        Log("Call DLL", logTimeCur - logTimebase);
                        /** !!!!!!!!!!!!!!! */

                        double gainNum = price[loc_n - 24 + j - 2];

                        final_Forecast[j - 1] = gainNum * (1 + q[0, 0]); ///  ????????? 

                        CMatrix gainMatrix = new CMatrix(1, 201);
                        for (int colj = 0; colj < 201; colj++)
                            gainMatrix[0, colj] = gainNum;

                        ff = ff.AddRow((gainNum * xOut) + gainMatrix);
                        CMatrix PdistLarge = pdist_n / gainNum;
                        vr[j - 1] = gainNum * s[0, 0] / 20;
                        pdist = pdist.AddRow(PdistLarge.GetColumnMean());
                        //}
                        //else
                        //{

                        //}

                    }
                    /** !!!!!!!!!!! log */
                    logTimebase = DateTime.Now;
                    //Application.DoEvents();
                    UpdateProgressBar(26, "Save Results for" + strDayCount);
                    SaveForcastResults(ppId, new PersianDate(curDate), final_Forecast, vr, ff, pdist, mixUnitsStatus);
                    //Application.DoEvents();
                    logTimeCur = DateTime.Now;
                    Log("SaveForcastResults", logTimeCur - logTimebase);
                    /** !!!!!!!!!!!!!!! */

                    //loc_n += 24;

                    

                }
                if (mixUnitsStatus == MixUnits.JustCombined)
                    mixUnitsStatus = MixUnits.ExeptCombined;
                else
                    // just to break the loop
                    mixUnitsStatus = MixUnits.All;


            } while (mixUnitsStatus != MixUnits.All);
            return true;
        }

        public static void Log(string txt, TimeSpan time)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter("Log.txt", true);
            sw.WriteLine(time.ToString() + ":    " + txt);
            sw.Close();
        }

        public static void Log(string txt)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter("Log.txt", true);
            //sw.WriteLine("");
            //sw.WriteLine("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            sw.WriteLine(txt);
            sw.Close();
        }

        public void SaveForcastResults(int ppID, PersianDate date, double[] final_Forecast, double[] vr, CMatrix ff, CMatrix pdist, MixUnits mixunitStatus)
        {

            //////// DELETE
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            string whereCmd = " where FinalForecast.PPId='" + ppID.ToString() +
                "' AND FinalForecast.date = '" + date.ToString("d") +
                "' and FinalForecast.MixUnitStatus='" + mixunitStatus.ToString() + "'";

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            MyCom.CommandText = "delete from dbo.FinalForecast201Item where fk_ffHourly in(" +
                        " select FinalForecastHourly.id from dbo.FinalForecastHourly" +
                        " inner join dbo.FinalForecast on FinalForecast.id = FinalForecastHourly.fk_FinalForecastId" +
                        whereCmd + ")";
            MyCom.ExecuteNonQuery();
            

            MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            MyCom.CommandText = "delete from FinalForecastHourly where fk_FinalForecastId in(" +
                " select id from dbo.FinalForecast" +
                    whereCmd + ")";
            MyCom.ExecuteNonQuery();

            MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            MyCom.CommandText = "delete from FinalForecast" + whereCmd;
            MyCom.ExecuteNonQuery();

            /////////////   INSERT
            MyCom = new SqlCommand();
            MyCom.Connection = myConnection;

            // Insert into FinalForcast table
            MyCom.CommandText = "insert INTO [FinalForecast] (PPId,date,MixUnitStatus)"
                        + " VALUES (@ppid,@date,@mixUnitStatus)";
            MyCom.Parameters.Add("@ppid", SqlDbType.NChar, 10);
            MyCom.Parameters["@ppid"].Value = ppID.ToString();
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = date.ToString("d");
            MyCom.Parameters.Add("@mixUnitStatus", SqlDbType.Char, 20);
            MyCom.Parameters["@mixUnitStatus"].Value = mixunitStatus.ToString();
            MyCom.ExecuteNonQuery();

            // retrieve the newly inserted id in FinalForcast
            MyCom.CommandText = "select max(id) from [FinalForecast]";
            int FinalForcastId = (int)MyCom.ExecuteScalar();

            for (int j = 0; j < 24; j++)
            {
                // Insert into FinalForcastHourly table
                MyCom = new SqlCommand();
                MyCom.Connection = myConnection;
                MyCom.CommandText = "insert INTO [FinalForecastHourly] (fk_FinalForecastId,hour,forecast, vr)"
                            + " VALUES (@finalForcastId,@hour,@forecast, @vr)";

                MyCom.Parameters.Add("@finalForcastId", SqlDbType.Int);
                MyCom.Parameters["@finalForcastId"].Value = FinalForcastId;
                MyCom.Parameters.Add("@hour", SqlDbType.Int);
                MyCom.Parameters["@hour"].Value = j + 1;
                MyCom.Parameters.Add("@forecast", SqlDbType.Float);
                MyCom.Parameters["@forecast"].Value = final_Forecast[j];
                MyCom.Parameters.Add("@vr", SqlDbType.Float);
                MyCom.Parameters["@vr"].Value = vr[j];
                MyCom.ExecuteNonQuery();

                // retrieve the newly inserted id in FinalForcastHourly
                MyCom.CommandText = "select max(id) from [FinalForecastHourly]";
                int FinalForcastHourlyId = (int)MyCom.ExecuteScalar();


                for (int k = 0; k < 201; k++)
                {
                    // Insert into FinalForecast201Item table
                    MyCom = new SqlCommand();
                    MyCom.CommandText = "insert INTO [FinalForecast201Item] (fk_ffHourly,[index],pdist, ff)"
                                + " VALUES (@fk_ffHourly,@index,@pdist, @ff)";
                    MyCom.Connection = myConnection;
                    MyCom.Parameters.Add("@fk_ffHourly", SqlDbType.Int);
                    MyCom.Parameters["@fk_ffHourly"].Value = FinalForcastHourlyId;
                    MyCom.Parameters.Add("@index", SqlDbType.Int);
                    MyCom.Parameters["@index"].Value = k + 1;
                    MyCom.Parameters.Add("@pdist", SqlDbType.Float);
                    MyCom.Parameters["@pdist"].Value = pdist[j, k];
                    MyCom.Parameters.Add("@ff", SqlDbType.Float);
                    MyCom.Parameters["@ff"].Value = ff[j, k];
                    MyCom.ExecuteNonQuery();

                }
            }
            //ConnectionManager.GetInstance().Connection.Close();
            myConnection.Close();
        }
        
        private CMatrix Get40Day_LC_HValue(DateTime date)
        {
            //Application.DoEvents();
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();


            int maxCount = TrainingDays * 24;
            int lineTypesNo = Enum.GetNames(typeof(Line_codes)).Length;
            CMatrix LC_H = new CMatrix(lineTypesNo, maxCount);

            string endDate = new PersianDate(date).ToString("d");
            string startDate = new PersianDate(date.Subtract(new TimeSpan(TrainingDays, 0, 0, 0))).ToString("d");

            for (int index = 1; index <= lineTypesNo; index++)
            {
                Line_codes line = (Line_codes)Enum.Parse(typeof(Line_codes), index.ToString());

                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = new SqlCommand("SELECT * FROM [InterchangedEnergy] " +
                "WHERE Date>=@startdate AND Date<=@enddate AND Code=@code ORDER BY Date", myConnection);
                Maxda.SelectCommand.Parameters.Add("@startdate", SqlDbType.Char, 10);
                Maxda.SelectCommand.Parameters["@startdate"].Value = startDate;
                Maxda.SelectCommand.Parameters.Add("@enddate", SqlDbType.Char, 10);
                Maxda.SelectCommand.Parameters["@enddate"].Value = endDate;
                Maxda.SelectCommand.Parameters.Add("@code", SqlDbType.NChar, 20);
                Maxda.SelectCommand.Parameters["@code"].Value = line.ToString();
                Maxda.Fill(MaxDS);

                DataTable dt = MaxDS.Tables[0];

                for (int daysbefore = TrainingDays - 1; daysbefore >= 0; daysbefore--)
                {
                    DateTime selectedDate = date.Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                    string strDate = new PersianDate(selectedDate).ToString("d");

                    DataRow selectedRow = null;
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["Date"].ToString().Trim() == strDate)
                            selectedRow = row;
                    }
                    if (selectedRow != null)
                    {
                        for (int j = 0; j < 24; j++)
                        {
                            string colName = "Hour" + (j + 1).ToString();
                            int col = maxCount - (24 * daysbefore) - 24 + j;
                            LC_H[index - 1, col] = double.Parse(selectedRow[colName].ToString());
                        }
                    }
                    else
                    {
                        // it is by default equal to zero
                    }
                }

            }
            myConnection.Close();
            return LC_H;

        }

        private double[,] MWNumericArrayToDoubleArray(MWNumericArray mwNumericArray, int row, int col)
        {
            double[,] doubleArray = new double[row, col];

            for (int i = 0; i < row; i++)
                for (int j = 0; j < col; j++)
                    doubleArray[i, j] = mwNumericArray[i + 1, j + 1].ToScalarDouble();

            return doubleArray;
        }

        private MWNumericArray DoubleArrayToMWNumericArray(double[,] doubleArray)
        {
            int noX = doubleArray.GetLength(0);
            int noY = doubleArray.GetLength(1);

            //MWNumericArray mwNumericArray = MWNumericArray.MakeSparse(noX, noY, MWArrayComplexity.Real, 1);
            MWNumericArray mwNumericArray = new MWNumericArray(MWArrayComplexity.Real, MWNumericType.Double, noX, noY);
            //mwNumericArray = (MWNumericArray)doubleArray;

            for (int i = 0; i < noX; i++)
                for (int j = 0; j < noY; j++)
                    mwNumericArray[i + 1, j + 1] = doubleArray[i, j];

            return mwNumericArray;
        }

        private CMatrix CalculatePowerPlantMaxBid(int ppid, DataSet dsUnits, DateTime date, MixUnits mixUnitStatus)
        {
            //Application.DoEvents();
            CMatrix maxBids = new CMatrix(1, 24);
            for (int unitType = 1; unitType < dsUnits.Tables.Count; unitType++)
            {
                DataTable myDt = dsUnits.Tables[unitType];
                if (mixUnitStatus == MixUnits.All ||
                    (mixUnitStatus == MixUnits.JustCombined && myDt.TableName == "Combined") ||
                    (mixUnitStatus == MixUnits.ExeptCombined && (myDt.TableName == "Steam" || myDt.TableName == "Gas"))
                    )
                {
                    foreach (DataRow unitRow in myDt.Rows)
                    {
                        string package = unitRow["PackageType"].ToString().Trim();
                        string unit = unitRow["UnitCode"].ToString().Trim();
                        string packagecode = unitRow["PackageCode"].ToString().Trim();

                        string temp = unit;
                        temp = temp.ToLower();
                        if (package.Contains("CC"))
                        {
                            int x = ppid;
                            if ((x == 131) || (x == 144)) x++;
                            temp = x + "-" + "C" + packagecode;
                        }
                        else if (temp.Contains("gas"))
                        {
                            temp = temp.Replace("gas", "G");
                            temp = ppid + "-" + temp;
                        }
                        else
                        {
                            temp = temp.Replace("steam", "S");
                            temp = ppid + "-" + temp;
                        }
                        string blockM005 = temp.Trim();

                        /////////////////////////////////////

                        string blockM002 = unit.ToLower();
                        if (package.Contains("CC"))
                        {

                            blockM002 = "C" + packagecode;
                        }
                        else
                        {
                            if (blockM002.Contains("gas"))
                                blockM002 = blockM002.Replace("gas", "G");
                            else if (blockM002.Contains("steam"))
                                blockM002 = blockM002.Replace("steam", "S");
                        }
                        blockM002 = blockM002.Trim();

                        string strDate = new PersianDate(date).ToString("d");

                        CMatrix currentMaxBid = GetOneDayPricesForEachUnit(ppid, strDate, blockM005, blockM002);
                        //SaveMaxBid(currentMaxBid, ppid, blockM005, strDate);

                        if (currentMaxBid != null)
                            for (int p = 0; p < 24; p++)
                                if (maxBids[0, p] < currentMaxBid[0, p])
                                    maxBids[0, p] = currentMaxBid[0, p];
                    }
                }
            }

            return maxBids;

        }

        private DataSet GetUnits(int ppId)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();


            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT DISTINCT PackageType FROM UnitsDataMain WHERE PPID=@Num", myConnection);
            Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
            Myda.SelectCommand.Parameters["@Num"].Value = ppId;
            Myda.Fill(MyDS, "UnitType");

            foreach (DataRow MyRow in MyDS.Tables["UnitType"].Rows)
            {
                string Utype = MyRow["PackageType"].ToString();
                Utype = Utype.Trim();
                switch (Utype)
                {
                    case "Steam":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Steam%'",
                            myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, "Steam");

                        DataView sdv = new DataView(MyDS.Tables["Steam"]);
                        sdv.Sort = "UnitCode ASC";
                        break;
                    case "Gas":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Gas%'",
                            myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, "Gas");
                        DataView gdv = new DataView(MyDS.Tables["Gas"]);
                        gdv.Sort = "UnitCode";

                        break;
                    default:

                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM [UnitsDataMain] WHERE PPID=@Num AND PackageType LIKE 'CC%'",
                            myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, "Combined");
                        DataView cdv = new DataView(MyDS.Tables["Combined"]);
                        cdv.Sort = "UnitCode ASC";
                        break;
                }
            }

            MyDS.Dispose();
            Myda.Dispose();

            myConnection.Close();
            return MyDS;

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CLoadFlowProgressForm_OnClosing(object sender, EventArgs e)
        {
            parentForm.EnableBiddingStrategyTab(true);
            parentForm.EnableBiddingStrategyExportButton(btnExportEnable);
        }

    }
}