﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.DMS.Common;

namespace PowerPlantProject
{
    partial class MainForm
    {
        //---------------------------------------ClearODPlantTab()---------------------------------
        private void ClearODPlantTab()
        {
            OutServiceCheck.Checked = false;
            NoOnCheck.Checked = false;
            NoOffCheck.Checked = false;
            ODServiceStartDate.IsNull = true;
            ODServiceEndDate.IsNull = true;
            SecondFuelCheck.Checked = false;
            ODFuelStartDate.IsNull = true;
            ODFuelEndDate.IsNull = true;
            ODFuelQuantityTB.Text = "";
            ODPowerMinCheck.Checked = false;
            ODPowerStartDate.IsNull = true;
            ODPowerEndDate.IsNull = true;
            ODPowerGrid1.DataSource = null;
            if (ODPowerGrid1.Rows != null) ODPowerGrid1.Rows.Clear();
            ODPowerGrid2.DataSource = null;
            if (ODPowerGrid2.Rows != null) ODPowerGrid2.Rows.Clear();
        }
        //-----------------------------------FillODValues------------------------------------------
        private void FillODValues(string Num)
        {
            ClearODPlantTab();
            //detect current date and hour
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");
            int myhour = DateTime.Now.Hour;

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            //Is There a Row in DB for this Plant?
            SqlCommand MyCom = new SqlCommand();
            MyCom.CommandText = "SELECT  @result1 =count(PPID) FROM [ConditionPlant] WHERE PPID=@num";
            MyCom.Connection = myConnection;
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPID;
            MyCom.Parameters.Add("@result1", SqlDbType.Int);
            MyCom.Parameters["@result1"].Direction = ParameterDirection.Output;
            //try
            // {
            MyCom.ExecuteNonQuery();
            // }
            //catch()
            //{
            //}
            int result1;
            result1 = (int)MyCom.Parameters["@result1"].Value;
            if (result1 != 0)
            {
                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT OutService,OutServiceStartDate,OutServiceEndDate,NoOn,NoOff," +
                "SecondFuel,SecondFuelStartDate,SecondFuelEndDate,FuelQuantity,PowerMin,PowerMinStartDate,PowerMinEndDate," +
                "PowerMinHour1,PowerMinHour2,PowerMinHour3,PowerMinHour4,PowerMinHour5,PowerMinHour6,PowerMinHour7,PowerMinHour8," +
                "PowerMinHour9,PowerMinHour10,PowerMinHour11,PowerMinHour12,PowerMinHour13,PowerMinHour14,PowerMinHour15," +
                "PowerMinHour16,PowerMinHour17,PowerMinHour18,PowerMinHour19,PowerMinHour20,PowerMinHour21,PowerMinHour22," +
                "PowerMinHour23,PowerMinHour24 FROM ConditionPlant WHERE PPID=" + PPID, myConnection);
                Myda.Fill(MyDS);
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    if (bool.Parse(MyRow[0].ToString()))
                        if (CheckDate(mydate, MyRow[1].ToString(), MyRow[2].ToString()))
                        {
                            OutServiceCheck.Checked = true;
                            ODServiceStartDate.IsNull = false;
                            ODServiceEndDate.IsNull = false;
                            ODServiceStartDate.Text = MyRow[1].ToString();
                            ODServiceEndDate.Text = MyRow[2].ToString();
                        }
                    if (bool.Parse(MyRow[3].ToString()))
                        NoOnCheck.Checked = true;
                    if (bool.Parse(MyRow[4].ToString()))
                        NoOffCheck.Checked = true;
                    if (bool.Parse(MyRow[5].ToString()))
                        if (CheckDate(mydate, MyRow[6].ToString(), MyRow[7].ToString()))
                        {
                            SecondFuelCheck.Checked = true;
                            ODFuelStartDate.IsNull = false;
                            ODFuelEndDate.IsNull = false;
                            ODFuelStartDate.Text = MyRow[6].ToString();
                            ODFuelEndDate.Text = MyRow[7].ToString();
                            ODFuelQuantityTB.Text = MyRow[8].ToString();
                        }
                    if (bool.Parse(MyRow[9].ToString()))
                        if (CheckDate(mydate, MyRow[10].ToString(), MyRow[11].ToString()))
                        {
                            ODPowerMinCheck.Checked = true;
                            ODPowerStartDate.IsNull = false;
                            ODPowerEndDate.IsNull = false;
                            ODPowerStartDate.Text = MyRow[10].ToString();
                            ODPowerEndDate.Text = MyRow[11].ToString();
                            ODPowerGrid1.Rows[0].Cells[0].Value = MyRow[12].ToString();
                            ODPowerGrid1.Rows[0].Cells[1].Value = MyRow[13].ToString();
                            ODPowerGrid1.Rows[0].Cells[2].Value = MyRow[14].ToString();
                            ODPowerGrid1.Rows[0].Cells[3].Value = MyRow[15].ToString();
                            ODPowerGrid1.Rows[0].Cells[4].Value = MyRow[16].ToString();
                            ODPowerGrid1.Rows[0].Cells[5].Value = MyRow[17].ToString();
                            ODPowerGrid1.Rows[0].Cells[6].Value = MyRow[18].ToString();
                            ODPowerGrid1.Rows[0].Cells[7].Value = MyRow[19].ToString();
                            ODPowerGrid1.Rows[0].Cells[8].Value = MyRow[20].ToString();
                            ODPowerGrid1.Rows[0].Cells[9].Value = MyRow[21].ToString();
                            ODPowerGrid1.Rows[0].Cells[10].Value = MyRow[22].ToString();
                            ODPowerGrid1.Rows[0].Cells[11].Value = MyRow[23].ToString();
                            ODPowerGrid2.Rows[0].Cells[0].Value = MyRow[24].ToString();
                            ODPowerGrid2.Rows[0].Cells[1].Value = MyRow[25].ToString();
                            ODPowerGrid2.Rows[0].Cells[2].Value = MyRow[26].ToString();
                            ODPowerGrid2.Rows[0].Cells[3].Value = MyRow[27].ToString();
                            ODPowerGrid2.Rows[0].Cells[4].Value = MyRow[28].ToString();
                            ODPowerGrid2.Rows[0].Cells[5].Value = MyRow[29].ToString();
                            ODPowerGrid2.Rows[0].Cells[6].Value = MyRow[30].ToString();
                            ODPowerGrid2.Rows[0].Cells[7].Value = MyRow[31].ToString();
                            ODPowerGrid2.Rows[0].Cells[8].Value = MyRow[32].ToString();
                            ODPowerGrid2.Rows[0].Cells[9].Value = MyRow[33].ToString();
                            ODPowerGrid2.Rows[0].Cells[10].Value = MyRow[34].ToString();
                            ODPowerGrid2.Rows[0].Cells[11].Value = MyRow[35].ToString();
                        }
                }
            }
            myConnection.Close();
        }
        //------------------------------------ClearODUnitTab()---------------------------------
        private void ClearODUnitTab()
        {
            ODUnitOutCheck.Checked = false;
            ODUnitNoOnCheck.Checked = false;
            ODUnitNoOffCheck.Checked = false;
            ODUnitServiceStartDate.IsNull = true;
            ODUnitServiceEndDate.IsNull = true;
            ODUnitMainCheck.Checked = false;
            ODUnitMainStartDate.IsNull = true;
            ODUnitMainEndDate.IsNull = true;
            ODUnitFuelCheck.Checked = false;
            ODUnitFuelStartDate.IsNull = true;
            ODUnitFuelEndDate.IsNull = true;
            ODUnitFuelTB.Text = "";
            ODUnitPowerCheck.Checked = false;
            ODUnitPowerStartDate.IsNull = true;
            ODUnitPowerEndDate.IsNull = true;
            ODUnitPowerGrid1.DataSource = null;
            if (ODUnitPowerGrid1.Rows != null) ODUnitPowerGrid1.Rows.Clear();
            ODUnitPowerGrid2.DataSource = null;
            if (ODUnitPowerGrid2.Rows != null) ODUnitPowerGrid2.Rows.Clear();
        }
        //----------------------------------FillODUnit--------------------------------------------
        private void FillODUnit(string unit, string package, int index)
        {
            ClearODUnitTab();
            //detect current date and hour
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");
            int myhour = DateTime.Now.Hour;


            //Is There a Row in DB for this Unit?
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            SqlCommand MyCom = new SqlCommand();
            MyCom.CommandText = "SELECT  @result1 =count(PPID) FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@unit " +
            "AND PackageType=@type SELECT @result2=COUNT(PPID) FROM PowerLimitedUnit WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type";
            MyCom.Connection = myConnection;
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPID;
            MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
            MyCom.Parameters["@unit"].Value = unit;
            string type = package;
            if (type.Contains("Combined")) type = "CC";
            MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
            MyCom.Parameters["@type"].Value = type;
            MyCom.Parameters.Add("@result1", SqlDbType.Int);
            MyCom.Parameters["@result1"].Direction = ParameterDirection.Output;
            MyCom.Parameters.Add("@result2", SqlDbType.Int);
            MyCom.Parameters["@result2"].Direction = ParameterDirection.Output;
            //try
            // {
            MyCom.ExecuteNonQuery();
            // }
            //catch()
            //{
            //}
            int result1;
            result1 = (int)MyCom.Parameters["@result1"].Value;
            int result2;
            result2 = (int)MyCom.Parameters["@result2"].Value;
            if (result1 != 0)
            {
                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT OutService,OutServiceStartDate,OutServiceEndDate,NoOn,NoOff," +
                "SecondFuel,SecondFuelStartDate,SecondFuelEndDate,FuelForOneDay,Maintenance,MaintenanceStartDate," +
                "MaintenanceEndDate FROM ConditionUnit WHERE UnitCode=@unit AND PackageType=@type AND PPID=" + PPID, myConnection);
                Myda.SelectCommand.Parameters.Add("@unit", SqlDbType.NChar, 20);
                Myda.SelectCommand.Parameters["@unit"].Value = unit;
                Myda.SelectCommand.Parameters.Add("@type", SqlDbType.NChar, 10);
                Myda.SelectCommand.Parameters["@type"].Value = type;
                Myda.Fill(MyDS);
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    if (bool.Parse(MyRow[0].ToString()))
                        if (CheckDate(mydate, MyRow[1].ToString(), MyRow[2].ToString()))
                        {
                            ODUnitOutCheck.Checked = true;
                            ODUnitServiceStartDate.IsNull = false;
                            ODUnitServiceEndDate.IsNull = false;
                            ODUnitServiceStartDate.Text = MyRow[1].ToString();
                            ODUnitServiceEndDate.Text = MyRow[2].ToString();
                        }
                    if (bool.Parse(MyRow[3].ToString()))
                        ODUnitNoOnCheck.Checked = true;
                    if (bool.Parse(MyRow[4].ToString()))
                        ODUnitNoOffCheck.Checked = true;
                    if (bool.Parse(MyRow[9].ToString()))
                        if (CheckDate(mydate, MyRow[10].ToString(), MyRow[11].ToString()))
                        {
                            ODUnitMainCheck.Checked = true;
                            ODUnitMainStartDate.IsNull = false;
                            ODUnitMainEndDate.IsNull = false;
                            ODUnitMainStartDate.Text = MyRow[10].ToString();
                            ODUnitMainEndDate.Text = MyRow[11].ToString();
                        }
                    if (bool.Parse(MyRow[5].ToString()))
                        if (CheckDate(mydate, MyRow[6].ToString(), MyRow[7].ToString()))
                        {
                            ODUnitFuelCheck.Checked = true;
                            ODUnitFuelStartDate.IsNull = false;
                            ODUnitFuelEndDate.IsNull = false;
                            ODUnitFuelStartDate.Text = MyRow[6].ToString();
                            ODUnitFuelEndDate.Text = MyRow[7].ToString();
                            ODUnitFuelTB.Text = MyRow[8].ToString();
                        }
                }
            }
            if (result2 != 0)
            {
                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT StartDate,EndDate,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6," +
                "Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21," +
                "Hour22,Hour23,Hour24 FROM PowerLimitedUnit WHERE UnitCode=@unit AND PackageType=@type AND PPID=" + PPID, myConnection);
                Myda.SelectCommand.Parameters.Add("@unit", SqlDbType.NChar, 20);
                Myda.SelectCommand.Parameters["@unit"].Value = unit;
                Myda.SelectCommand.Parameters.Add("@type", SqlDbType.NChar, 10);
                Myda.SelectCommand.Parameters["@type"].Value = type;
                Myda.Fill(MyDS);
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    if (CheckDate(mydate, MyRow[0].ToString(), MyRow[1].ToString()))
                    {
                        ODUnitPowerCheck.Checked = true;
                        ODUnitPowerStartDate.IsNull = false;
                        ODUnitPowerEndDate.IsNull = false;
                        ODUnitPowerStartDate.Text = MyRow[0].ToString();
                        ODUnitPowerEndDate.Text = MyRow[1].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[0].Value = MyRow[2].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[1].Value = MyRow[3].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[2].Value = MyRow[4].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[3].Value = MyRow[5].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[4].Value = MyRow[6].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[5].Value = MyRow[7].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[6].Value = MyRow[8].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[7].Value = MyRow[9].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[8].Value = MyRow[10].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[9].Value = MyRow[11].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[10].Value = MyRow[12].ToString();
                        ODUnitPowerGrid1.Rows[0].Cells[11].Value = MyRow[13].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[0].Value = MyRow[14].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[1].Value = MyRow[15].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[2].Value = MyRow[16].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[3].Value = MyRow[17].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[4].Value = MyRow[18].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[5].Value = MyRow[19].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[6].Value = MyRow[20].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[7].Value = MyRow[21].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[8].Value = MyRow[22].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[9].Value = MyRow[23].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[10].Value = MyRow[24].ToString();
                        ODUnitPowerGrid2.Rows[0].Cells[11].Value = MyRow[25].ToString();
                    }
                }
            }
            myConnection.Close();
        }

    }
}