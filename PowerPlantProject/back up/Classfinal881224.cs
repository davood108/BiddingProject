﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Common;
using ILOG.Concert;
using ILOG.CPLEX;
using PowerPlantProject;
using FarsiLibrary.Utils;

namespace ending_class
{
    
  public class Classfinal
    {

      public Classfinal()
      {
      }
      public Classfinal(int ppID, MixUnits mixStatus, string strDate)
      {
          ppID = ppID;
          mixUnitStatus = mixStatus;
          date = strDate;
      }
      int strategyBiddingId;

      public int StrategyBiddingId
      {
          get { return strategyBiddingId; }
          set { strategyBiddingId = value; }
      }

      int ppID;

      public int PPID
      {
          get { return ppID; }
          set { ppID = value; }
      }

      MixUnits mixUnitStatus;

      public MixUnits MixUnitStatus
      {
          get { return mixUnitStatus; }
          set { mixUnitStatus = value; }
      }

      string date;

      public string Date
      {
          get { return date; }
          set { date = value; }
      }

      //int PPIDIndex;
      public SqlConnection oSqlConnection = null;
        public SqlCommand oSqlCommand = null;
        public SqlDataReader oSqlDataReader = null;
        public DataTable oDataTable = null;


        //public double[, , ,] powval;
        //public double[, , ,] priceval;
        //public double[, , ] valpmax;

        public double tot;

        private string buildmaxdate(string[] arrmax)
        {
            string maxdate = null;
            for (int fd = 0; fd < arrmax.Length; fd++)
            {
                if (maxdate == null || string.Compare(maxdate, arrmax[fd]) < 0)
                    maxdate = arrmax[fd];
            }
            return maxdate;
            
        }

        public double value()
        {
            int Plants_Num;

            oDataTable = utilities.returntbl("select distinct PPID from UnitsDataMain ");
            Plants_Num = oDataTable.Rows.Count;

            string[] plant = new string[Plants_Num];
            for (int il = 0; il < Plants_Num; il++)
            {

                plant[il] = oDataTable.Rows[il][0].ToString();

            }

            oDataTable = utilities.returntbl("select Max (PackageCode) from UnitsDataMain ");

            int Package_Num = (int)oDataTable.Rows[0][0];


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //Unit Number
            int Units_Num = 0;

            int[] Plant_units_Num = new int[Plants_Num];
            for (int re = 0; re < Plants_Num; re++)
            {
                oDataTable = utilities.returntbl("select   UnitCode from UnitsDataMain where PPID='" + plant[re] + "'");

                Plant_units_Num[re] = oDataTable.Rows.Count;
                if (Units_Num <= oDataTable.Rows.Count)
                {
                    Units_Num = oDataTable.Rows.Count;
                }

            }
            
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //Fixed Set :
            int Hour = 24;   
            int Dec_Num = 4;
            int Data_Num = 20;
            int Start_Num = 48;   
            int Mmax = 3;          
            int Step_Num = 10;
            int Xtrain = 201;
            int Power_Num = 4;
            int Power_W = 4;
            //powval = new double[Plants_Num, Package_Num, Hour, Step_Num];
            //priceval = new double[Plants_Num, Package_Num, Hour, Step_Num];
            //valpmax = new double[Plants_Num, Package_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            int b = 0;
            
            // detect max date in database...................................  
            oDataTable = utilities.returntbl("select Date from BaseData");
             int ndate = oDataTable.Rows.Count;
            string[] arrmaxdate1=new string[ndate];
            for (b = 0; b < ndate; b++)
            {
                arrmaxdate1[b]=oDataTable.Rows[b][0].ToString();
            }
        
            string smaxdate = buildmaxdate(arrmaxdate1);
                        
            //end.................................................

            //double[] FuelPrice = new double[3];
            //FuelPrice[0] = 49000;
            //FuelPrice[1] = 43000;
            //FuelPrice[2] = 640000;

            
            oDataTable = utilities.returntbl("select GasPrice,MazutPrice,GasOilPrice  from BaseData where Date like '" + smaxdate +'%'+ "'");
            double[] FuelPrice = new double[3];
            for (b = 0; b < 3; b++)
            {
                FuelPrice[b] = double.Parse(oDataTable.Rows[0][b].ToString());
              

            }

            
            //double[] CapPrice = new double[1];
            //CapPrice[0] = 110000;
           
            oDataTable = utilities.returntbl("select max(MarketPriceMax)  from BaseData where Date like '" + smaxdate + '%' + "'");
            double[] CapPrice = new double[oDataTable.Rows.Count];
            for (b = 0; b < oDataTable.Rows.Count; b++)
            {
               
                  CapPrice[b] = double.Parse(oDataTable.Rows[b][0].ToString());
               
            }
            
            double[] ErrorBid = new double[oDataTable.Rows.Count];
            ErrorBid[0] = CapPrice[0] / 440;
            for (b = 0; b < oDataTable.Rows.Count; b++)
            {
                ErrorBid[b] = CapPrice[b] / 440;
                 
            }


            string[] Run = new string[2];
            Run[0] = "Automatic";
            Run[1] = "customize";


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            int[] Plant_Packages_Num = new int[Plants_Num];
            for (int re = 0; re < Plants_Num; re++)
            {
                oDataTable = utilities.returntbl("select Max(PackageCode)  from UnitsDataMain where  PPID='" + plant[re] + "'");
                Plant_Packages_Num[re] = (int)oDataTable.Rows[0][0];
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,] Package = new string[Plants_Num, Package_Num];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    oDataTable = utilities.returntbl("select PackageType  from UnitsDataMain where  PPID='" + plant[j] + "'");
                    Package[j, i] = oDataTable.Rows[i][0].ToString();
                }

            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,] Unit = new string[Plants_Num, Package_Num];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    oDataTable = utilities.returntbl("select UnitCode from UnitsDataMain where  PPID='" + plant[j] + "'");
                    Unit[j, i] = oDataTable.Rows[i][0].ToString();
                }

            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[, ,] Unit_Dec = new string[Plants_Num, Units_Num, Dec_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Dec_Num; k++)
                    {
                        switch (k)
                        {
                            case 0:
                                oDataTable = utilities.returntbl("select UnitType  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString();
                                break;
                            case 1:
                                oDataTable = utilities.returntbl("select PackageType  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString();
                                break;
                            case 2:
                                Unit_Dec[j, i, k] = "Gas";
                                break;
                            case 3:
                                oDataTable = utilities.returntbl("select  SecondFuel  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString();
                                break;
                        }
                    }

                }


            }


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Unit_Data = new double[Plants_Num, Units_Num, Data_Num];
            // 0 : Package
            // 1 : RampUp
            // 2 : RampDown
            // 3 : Pmin
            // 4 : pmax
            // 5 : RampStart&shut
            // 6 : MinOn
            // 7 : MinOff
            // 8 :  Am
            // 9 :  Bm
            // 10 : Cm
            // 11 : Bmain
            // 12 : Cmain
            // 13 : Dmain
            // 14 : Variable cost
            // 15 : Fixed Cost
            // 16 : Tcold
            // 17 : Thot
            // 18 : Cold start cost
            // 19 : Hot start cost

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Data_Num; k++)
                    {
                        switch (k)
                        {
                            case 0:
                                oDataTable = utilities.returntbl("select PackageCode  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 1:
                                oDataTable = utilities.returntbl("select RampUpRate  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                Unit_Data[j, i, k] = 60 * Unit_Data[j, i, k];
                                break;
                            case 2:
                                Unit_Data[j, i, k] = Unit_Data[j, i, 1];
                                break;

                            case 3:
                                oDataTable = utilities.returntbl("select PMin  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;

                            case 4:
                                oDataTable = utilities.returntbl("select PMax  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 5:
                                double pow = Unit_Data[j, i, 1];
                                if (Unit_Data[j, i, 3] > pow)
                                {
                                    Unit_Data[j, i, k] = Unit_Data[j, i, 3];
                                }
                                else
                                {
                                    Unit_Data[j, i, k] = pow;
                                }
                                break;
                            case 6:

                                oDataTable = utilities.returntbl("select TUp  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 7:
                                oDataTable = utilities.returntbl("select TDown  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;

                            case 8:

                                oDataTable = utilities.returntbl("select PrimaryFuelAmargin from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 9:
                                oDataTable = utilities.returntbl("select PrimaryFuelBmargin  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 10:

                                oDataTable = utilities.returntbl("select PrimaryFuelCmargin  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 11:
                                oDataTable = utilities.returntbl("select BMaintenance  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString()); ;
                                break;

                            case 12:
                                oDataTable = utilities.returntbl("select CMaintenance  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 13:
                                oDataTable = utilities.returntbl("select DMaintenance  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = Unit_Data[j, i, 12];
                                break;
                            case 14:

                                oDataTable = utilities.returntbl("select VariableCost  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 15:
                                oDataTable = utilities.returntbl("select FixedCost  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;

                            case 16:
                                oDataTable = utilities.returntbl("select TStartCold  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 17:

                                oDataTable = utilities.returntbl("select TStartHot  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i][0].ToString());
                                break;
                            case 18:

                                Unit_Data[j, i, k] = 20000;
                                break;

                            case 19:

                                Unit_Data[j, i, k] = 10000;
                                break;

                        }
                    }

                }

            }


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Unit_Data[j, i, 0] = Unit_Data[j, i, 0] - 1;
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            int[,] hisstart_Plant_unit = new int[Plants_Num, Units_Num];  //hisstart_Plant_unit<2*Maximum Tstart
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    hisstart_Plant_unit[j, i] = 0;
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] HisShut_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    HisShut_Plant_unit[j, i] = 0;
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Powerhistory_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Powerhistory_Plant_unit[j, i] = 0;
                }
            }
            int[,] V_history_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    V_history_Plant_unit[j, i] = 0;
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Hisup_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Hisup_Plant_unit[j, i] = 0;
                    if (Unit_Data[j, i, 6] < hisstart_Plant_unit[j, i])
                    {
                        Hisup_Plant_unit[j, i] = (int)Unit_Data[j, i, 6] - hisstart_Plant_unit[j, i];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Hisdown_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Hisdown_Plant_unit[j, i] = 0;
                    if (Unit_Data[j, i, 7] < HisShut_Plant_unit[j, i])
                    {
                        Hisdown_Plant_unit[j, i] = (int)Unit_Data[j, i, 7] - HisShut_Plant_unit[j, i];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Outservice_Plant_unit = new int[Plants_Num, Units_Num];
            int ppkn = 0;
           
          //if(CheckDate(date,
            oDataTable = utilities.returntbl("select distinct PPID from ConditionUnit where OutServiceStartDate='1388/10/27'");
          
            /*****************************************************/
//            select * from dbo.ConditionUnit
//where 
            //ConditionUnit.OutServiceStartDate<@date AND
//ConditionUnit.OutServiceEndDate>@date

            string [] ppk=new string[Plants_Num];


            ppkn = oDataTable.Rows.Count;

            for (b = 0; b < ppkn; b++)
            {
                ppk[b] = oDataTable.Rows[b][0].ToString();
            }
            for (int an2 = 0; an2 < Plants_Num; an2++)
            {
                for (int ax2 = 0; ax2 < ppkn; ax2++)
                {

                    if (ppk[ax2] == plant[an2])
                    {
                        if (ppk[ax2] == plant[ax2])
                        {
                            break;
                        }
                        ppk[an2] = plant[an2];

                        ppk[ax2] = null;
                    }

                }

            }

                      
            int[] uuk = new int[Units_Num];
            for (b = 0; b < Plants_Num; b++)
            {
                oDataTable = utilities.returntbl("select UnitCode from ConditionUnit where OutServiceStartDate='1388/10/27'and PPID='" + ppk[b] + "'");

                if (oDataTable.Rows.Count == 0)
                {
                    uuk[b] = 0;
                }
                else
                {
                    uuk[b] = oDataTable.Rows.Count;
                }
            }
            string[] outserviced = new string[Units_Num];
            //for (b = 0; b < Plants_Num; b++)
            //{
            //    oDataTable = utilities.returntbl("select OutServiceStartDate,OutServiceEndDate from ConditionUnit where OutServiceStartDate='1388/10/27'and PPID='" + ppk[b] + "'");
              
            //   if (oDataTable.Rows.Count == 0)
            //   {
            //       outserviced[b] = 0;
            //   }
            //   else
            //   {
            //       outserviced[b] = oDataTable.Rows[b][0].ToString();
            //   }

            //}

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < uuk[j]; i++)
                {
                    oDataTable = utilities.returntbl("select Maintenance,OutService from ConditionUnit where OutServiceStartDate='1388/10/27'AND  PPID='" + ppk[j] + "'");
                    if (oDataTable.Rows.Count == 0)
                    {
                        Outservice_Plant_unit[j, i] = 0;
                    }
                    else
                    {
                        if (bool.Parse(oDataTable.Rows[i][0].ToString()) || bool.Parse(oDataTable.Rows[i][1].ToString()))
                        {
                            Outservice_Plant_unit[j, i] = 1;
                        }
                        else Outservice_Plant_unit[j, i] = 0;
                    }
                }
            }
                   
                 
                                    
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Outservice = new int[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if (((int)Unit_Data[j, i, 0] == k))
                        {
                            Outservice[j, k] = Outservice[j, k] + Outservice_Plant_unit[j, i];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[,] Outservice_Plant_Pack = new string[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    Outservice_Plant_Pack[j, k] = "No";
                    if ((Package[j, k].ToString().Trim() == "CC") & (Outservice[j, k] > 2))
                    {
                        Outservice_Plant_Pack[j, k] = "Yes";
                    }
                    if ((Package[j, k].ToString().Trim() != "CC") & (Outservice[j, k] > 0))
                    {
                        Outservice_Plant_Pack[j, k] = "Yes";
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] SecondFuel_Plant_unit = new int[Plants_Num, Units_Num];
                                   

            oDataTable = utilities.returntbl("select distinct PPID from ConditionUnit where OutServiceStartDate='1388/10/27'");
            int ppkn1 = oDataTable.Rows.Count;
            string[] ppk1 = new string[Plants_Num];

            for (b = 0; b < oDataTable.Rows.Count; b++)
            {

                ppk1[b] = oDataTable.Rows[b][0].ToString();
               

            }

            for (int an3 = 0; an3 < Plants_Num; an3++)
            {
                for (int ax3 = 0; ax3 < ppkn1; ax3++)
                {

                    if (ppk1[ax3] == plant[an3])
                    {
                        if (ppk1[ax3] == plant[ax3])
                        {
                            break;
                        }
                        ppk1[an3] = plant[an3];

                        ppk1[ax3] = null;
                    }

                }

            }

            
            int[] uuk1 = new int[Units_Num];
            for (b = 0; b <Plants_Num; b++)
            {

                oDataTable = utilities.returntbl("select UnitCode from ConditionUnit where OutServiceStartDate='1388/10/27'and PPID='" + ppk1[b] + "'");
                if (oDataTable.Rows.Count == 0)
                {
                    uuk1[b] = 0;
                }

                else
                uuk1[b] = oDataTable.Rows.Count;
                                
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < uuk1[j]; i++)
                {
                    oDataTable = utilities.returntbl("select SecondFuel from ConditionUnit where OutServiceStartDate='1388/10/27'AND  PPID='" + ppk1[j] + "'");
                    if (oDataTable.Rows.Count == 0)
                    {
                        SecondFuel_Plant_unit[j, i] = 0;
                    }
                    else
                    {
                        if (bool.Parse(oDataTable.Rows[i][0].ToString()))
                        {
                            SecondFuel_Plant_unit[j, i] = 1;
                        }
                        else SecondFuel_Plant_unit[j, i] = 0;
                    }
                }
            }
                         
            
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] FuelQuantity_Plant_unit = new double[Plants_Num, Units_Num];
            
            oDataTable = utilities.returntbl("select distinct PPID from ConditionUnit where OutServiceStartDate='1388/10/27'");
            int fpp = oDataTable.Rows.Count;
            string [] ppf=new string[Plants_Num];
            for (b = 0; b < fpp; b++)
            {

                ppf[b]=oDataTable.Rows[b][0].ToString();

            }

            for (int an4 = 0; an4 < Plants_Num; an4++)
            {
                for (int ax4 = 0; ax4 < ppkn1; ax4++)
                {

                    if (ppf[ax4] == plant[an4])
                    {
                        if (ppf[ax4] == plant[ax4])
                        {
                            break;
                        }
                        ppf[an4] = plant[an4];

                        ppf[ax4] = null;
                    }

                }

            }
                     
            
            int[] uff = new int[Units_Num];
            for (b = 0; b < Plants_Num; b++)
            {
                oDataTable = utilities.returntbl("select UnitCode from ConditionUnit where OutServiceStartDate='1388/10/27'and PPID='" + ppf[b] + "'");

                if (oDataTable.Rows.Count == 0)
                {
                    uff[b] = 0;
                }
                else
                uff[b] = oDataTable.Rows.Count;
            }
            
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < uff[j]; i++)
                {
                    oDataTable = utilities.returntbl("select FuelForOneDay from ConditionUnit where OutServiceStartDate='1388/10/27'and PPID='" + ppf[j] + "'");
                    if (oDataTable.Rows.Count == 0)
                    {
                        FuelQuantity_Plant_unit[j, i] = 0;
                    }

                    else
                    FuelQuantity_Plant_unit[j, i] = double.Parse(oDataTable.Rows[i][0].ToString());
                }
            }
            

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            
            oDataTable = utilities.returntbl("select distinct PPID from ConditionPlant where OutServiceStartDate='1388/11/12'");
            int ppo=oDataTable.Rows.Count;
            string[]pp=new string[Plants_Num];


            for (b = 0; b < ppo; b++)
            {
                pp[b]=oDataTable.Rows[b][0].ToString();
            }
            for (int an = 0; an < Plants_Num; an++)
            {
                for (int ax = 0; ax < ppo; ax++)
                {

                    if (pp[ax] == plant[an])
                    {
                        if (pp[ax] == plant[ax])
                        {
                            break;
                        }
                        pp[an] = plant[an];

                        pp[ax] = null;
                    }

                }

            }
            
            
           double[] FuelQuantity_Plant = new double[Plants_Num];
            for (int j = 0; j <Plants_Num; j++)
            {
                oDataTable = utilities.returntbl("select  FuelQuantity from ConditionPlant where OutServiceStartDate='1388/11/12' and PPID='"+pp[j]+"'");
                if (oDataTable.Rows.Count == 0)
                {
                    FuelQuantity_Plant[j] = 0;
                }
                else
                {
                    
                    FuelQuantity_Plant[j] = double.Parse(oDataTable.Rows[0][0].ToString());
                }

            }
            

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            
            oDataTable = utilities.returntbl("select DateEstimate from LoadForecasting");
            int ad = oDataTable.Rows.Count;
            string[] arrmax1=new string[ad];
            for (b = 0; b < ad; b++)
            {


                arrmax1[b]=oDataTable.Rows[b][0].ToString();

            }
           
             smaxdate=buildmaxdate(arrmax1);                    
            
                        
              oDataTable = utilities.returntbl("select Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24 from LoadForecasting where  Date like '1388/12/05'");


            double[] LoadForecasting = new double[Hour];
            for (int h = 0; h < Hour; h++)
            {
                LoadForecasting[h] = double.Parse(oDataTable.Rows[0][h].ToString());
            }
                        
            double[] LoadAverage = new double[Hour];
            LoadAverage[0] = LoadForecasting[1];
            for (int h = 1; h < Hour; h++)
            {
                LoadAverage[h] = LoadForecasting[h] + LoadAverage[(h - 1)];
            }

            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------
                                                          //           Price        //
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------

            //**************************************************************************
            int row = 0;
            //Price forecasting :
            double[,] PriceForecastings = new double[Plants_Num,Hour,row];

            //for (int h = 0; h < Hour; h++)
            //{
            //    PriceForecastings[h] = double.Parse(ds.Tables["price"].Rows[1][h].ToString());
            //    if (PriceForecastings[h] > CapPrice[0])
            //    {
            //        PriceForecastings[h] = CapPrice[0]
            //    }
            //}

            //double[] forecasts = GetForcast( mixUnitStatus, date);
            //int temp=0;
            //oDataTable = utilities.returntbl("select forecast from FinalForecastHourly where fk_FinalForecastId='9'");
            //for (int f = 0; f < Plants_Num; f++)
            //{
            //    if (ppID = plant[f])
            //        temp = f;
                
            //}
            for (int j = 0; j < Plants_Num; j++)
            {
                row = 0;
                if (j == 2 || j == 5)
                {
                    string strCommand = "select FinalForecastHourly.forecast from FinalForecastHourly " +
                            " inner join FinalForecast " +
                            " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id  " +
                            " where FinalForecast.PPId =  " + plant[j] +
                            " AND FinalForecast.date = " + date;
                            
                    oDataTable = utilities.returntbl(strCommand + " AND MixUnitStatus = JustCombined ORDER BY FinalForecastHourly.hour");
                    // Add to row PriceForecastings[j,hour,0];

                    oDataTable = utilities.returntbl(strCommand + " AND MixUnitStatus = ExeptCombined ORDER BY FinalForecastHourly.hour");
                    // Add to row PriceForecastings[j,hour,1];
                }
                else
                {
                    string strCommand = "select FinalForecastHourly.forecast from FinalForecastHourly " +
                            "inner join FinalForecast " +
                            "on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id  " +
                            "where FinalForecast.PPId =  " + plant[j] +
                            "AND FinalForecast.date = " + date + 
                            " ORDER BY FinalForecastHourly.hour";
                    oDataTable = utilities.returntbl(strCommand);
                    // Add to row PriceForecastings[j,hour,0];
                }

            }
              for (int h = 0; h < Hour; h++)
              {

                  PriceForecastings[j, h] = double.Parse(oDataTable.Rows[h][0].ToString());
                  if (PriceForecastings[j, h,row] > CapPrice[0])
                      PriceForecastings[j, h,row] = CapPrice[0];
              }
                
            
            double[, ,] PriceForecasting = new double[Plants_Num, Package_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    if (j == 2 || j == 5)
                    {
                       if(Package[j,k]=="CC" )
                           oDataTable = utilities.returntbl("select forecast from FinalForecastHourly where PPID='" + plant[j] + "'");
                        else
                           oDataTable = utilities.returntbl("select forecast from FinalForecastHourly where PPID='" + plant[j] + "'");
                      }
                    for (int h = 0; h < Hour; h++)
                    {
                        PriceForecasting[j, k, h] = PriceForecastings[j, h, row];
                    }

                }
            }
            //**************************************************************************
            //**************************************************************************
            //Price forecasting: pd_range :
            double[,] pd_ranges = new double[Hour, Xtrain];
            
            //oDataTable = utilities.returntbl("select ff from FinalForecast201Item where fk_ffHourly='30'");
            double[,] pdrange=Get_pd_Range(ppID, mixUnitStatus, date);
            //for (int j = 0; j < Plants_Num; j++)
            //{

                for (int h = 0; h < Hour; h++)
                {

                    for (int x = 0; x < 200; x++)
                    {
                        pd_ranges[h, x] =pdrange[h,x];
                    }
                }

            //}
            double[, , ,] pd_range = new double[Plants_Num, Package_Num, Hour, Xtrain];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < 200; x++)
                        {
                            pd_range[j, k, h, x] = pd_ranges[j ,h, x];
                        }
                    }

                }
            }
            //**************************************************************************
            //**************************************************************************
            //Price forecasting : pdist:
            oDataTable = utilities.returntbl("select pdist from FinalForecast201Item where fk_ffHourly='30'");
           double[,] pdistr= Get_pdist(ppID, mixUnitStatus, date);

            double[,,] pdists = new double[Plants_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int x = 0; x < 200; x++)
                    {
                        pdists[j, h, x] = pdistr[h,x];

                    }

                }
            }

            double[, , ,] pdist = new double[Plants_Num, Package_Num, Hour, Xtrain];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    if (Package[j, k] == "CC")
                    {





                    }
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < 200; x++)
                        {
                            pdist[j, k, h, x] = pdists[j, h, x];
                        }
                    }

                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] UL_State = new string[Plants_Num, Package_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        UL_State[j, k, h] = "N";
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[] Variance = new double[  Hour];

            double[] var = GetVariance(ppID, mixUnitStatus, date);

            //oDataTable = utilities.returntbl("select forecast from FinalForecastHourly where fk_FinalForecastId='9'");

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {

                        Variance[j, k, h] = var[h];
                    }
                }
            }

            double[,] Sum_Variance = new double[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Sum_Variance[j, k] = Sum_Variance[j, k] + Variance[j, k, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Average_Variance = new double[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    Average_Variance[j, k] = (Sum_Variance[j, k] / 24);
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Max_Variance = new double[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Max_Variance[j, k] < Variance[j, k, h])
                        {
                            Max_Variance[j, k] = Variance[j, k, h];
                        }
                    }
                }
            }

            //*************************************************************************
            double[, ,] Price_UL = new double[Plants_Num, Package_Num, Hour];
            double[, ,] Price_optimal = new double[Plants_Num, Package_Num, Hour];
            double[, ,] Price_Margin = new double[Plants_Num, Package_Num, Hour];
            //*************************************************************************
            double[, , ,] Price = new double[Plants_Num, Package_Num, Hour, Step_Num];
            double[, , ,] Power = new double[Plants_Num, Package_Num, Hour, Step_Num];
            //*************************************************************************
            double[, ,] Power_memory = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Power_if = new double[Plants_Num, Units_Num, Hour];
            //*************************************************************************
            double[, ,] Power_Error = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Power_Error_Pack = new double[Plants_Num, Package_Num, Hour];
            //*************************************************************************
            int[, , ,] X_select = new int[Plants_Num, Package_Num, Hour, Power_Num];
            //*************************************************************************
            double[, ,] Power_memory_Pack = new double[Plants_Num, Package_Num, Hour];
            double[, ,] Power_if_Pack = new double[Plants_Num, Package_Num, Hour];
            double[, ,] Power_if_CC = new double[Plants_Num, Package_Num, Hour];
            double[, ,] Test = new double[Plants_Num, Units_Num, Hour];
            //**************************************************************************

            oDataTable = utilities.returntbl("select distinct PPID from PowerLimitedUnit where StartDate='1388/12/13'");
            int plann = oDataTable.Rows.Count;

            string[] ptimm = new string[Plants_Num];
            for (b = 0; b < plann; b++)
            {

                ptimm[b] = oDataTable.Rows[b][0].ToString();

            }

            for (int an6 = 0; an6 < Plants_Num; an6++)
            {
                for (int ax6 = 0; ax6 < plann; ax6++)
                {

                    if (ptimm[ax6] == plant[an6])
                    {
                        if (ptimm[ax6] == plant[ax6])
                        {
                            break;
                        }
                        ptimm[an6] = plant[an6];

                        ptimm[ax6] = null;
                    }

                }

            }

            int[] unitname = new int[Units_Num];

            for (b = 0; b < Plants_Num; b++)
            {

                oDataTable = utilities.returntbl("select UnitCode from PowerLimitedUnit where PPID='" + ptimm[b] + "'and StartDate='1388/12/13'");
                if (oDataTable.Rows.Count == 0)
                {
                    unitname[b] = 0;
                }
                else unitname[b] = oDataTable.Rows.Count;


            }


            double[, ,] Limit_Power = new double[Plants_Num, Units_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {

                for (int i = 0; i < unitname[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        oDataTable = utilities.returntbl("select Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24 from PowerLimitedUnit where StartDate='1388/12/13' and PPID='" + ptimm[j] + "'");
                        if (oDataTable.Rows.Count == 0)
                        {
                            Limit_Power[j, i, h] = 0;
                        }
                        else
                        {

                            Limit_Power[j, i, h] = double.Parse(oDataTable.Rows[i][h].ToString());
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Pmax_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Pmax_Plant_unit[j, i, h] = (Unit_Data[j, i, 4] - Limit_Power[j, i, h]);
                    }

                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Pmax_Plant_Pack = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k))
                            {
                                Pmax_Plant_Pack[j, k, h] = Pmax_Plant_Pack[j, k, h] + Pmax_Plant_unit[j, i, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            oDataTable = utilities.returntbl("select distinct PPID from ConditionPlant where OutServiceStartDate='1388/11/12'");
            int ppo4 = oDataTable.Rows.Count;
            string[] pp4 = new string[Plants_Num];

            for (b = 0; b < ppo4; b++)
            {
                pp4[b] = oDataTable.Rows[b][0].ToString();
            }
            for (int an1 = 0; an1 < Plants_Num; an1++)
            {
                for (int ax1 = 0; ax1 < ppo4; ax1++)
                {

                    if (pp4[ax1] == plant[an1])
                    {
                        if (pp4[ax1] == plant[ax1])
                        {
                            break;
                        }
                        pp4[an1] = plant[an1];

                        pp4[ax1] = null;
                    }

                }

            }

            

            double[,] Min_Power = new double[Plants_Num, Hour];
            

            for (int j = 0; j < Plants_Num; j++)
            {

                oDataTable = utilities.returntbl("select PowerMinHour1,PowerMinHour2,PowerMinHour3,PowerMinHour4,PowerMinHour5,PowerMinHour6,PowerMinHour7,PowerMinHour8,PowerMinHour9,PowerMinHour10,PowerMinHour11,PowerMinHour12,PowerMinHour13,PowerMinHour14,PowerMinHour15,PowerMinHour16,PowerMinHour17,PowerMinHour18,PowerMinHour19,PowerMinHour20,PowerMinHour21,PowerMinHour22,PowerMinHour23,PowerMinHour24 from ConditionPlant where OutServiceStartDate='1388/11/12'and PPID='" + pp4[j] + "'");
                if (oDataTable.Rows.Count == 0)
                {
                    Min_Power[j, 0] = 0;
                }
                else
                {
                    for (int h = 0; h < Hour; h++)
                    {

                        Min_Power[j, h] = double.Parse(oDataTable.Rows[0][h].ToString());
                    }
                }

            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[] Price_Min = new double[Hour];


            oDataTable = utilities.returntbl("select AcceptedMin from AveragePrice where Date='1388/08/17'");

            for (int h = 0; h < Hour; h++)
            {


                Price_Min[h] = double.Parse(oDataTable.Rows[h][0].ToString());
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Select_Hour_Cap = new string[Hour, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((PriceForecasting[j, k, h] > (CapPrice[0] - 500) | (LoadForecasting[h] >= (LoadAverage[23] / 24))))
                        {
                            Select_Hour_Cap[j, k, h] = "Yes";
                        }
                        else
                        {
                            Select_Hour_Cap[j, k, h] = "NO";
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Hour_Cap = new string[Plants_Num, Package_Num, Hour];
            string[] hourcap = Get_HourCap(ppID, mixUnitStatus, date);

            oDataTable = utilities.returntbl("select Peak from BiddingStrategyCustomized where FkBiddingStrategySettingId='6'");
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Hour_Cap[j, k, h] = hourcap[h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //string[] Hour_State_back = new string[Hour];
            //oDataTable = utilities.returntbl("select Strategy from BiddingStrategyCustomized where FkBiddingStrategySettingId='6'");
            //for (int h = 0; h < Hour; h++)
            //{
            //    Hour_State_back[h]=oDataTable.Rows[h][0].ToString();

            //}
            string[, ,] Hour_State = new string[Plants_Num, Package_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Variance[j, k, h] <= Average_Variance[j, k])
                        {
                            Hour_State[j, k, h] = "U-O-F";
                        }
                        if ((Variance[j, k, h] > Average_Variance[j, k]) & (Variance[j, k, h] <= Max_Variance[j, k]))
                        {
                            Hour_State[j, k, h] = "O-F";
                        }
                        if (Variance[j, k, h] > Max_Variance[j, k])
                        {
                            Hour_State[j, k, h] = "F-M";
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (UL_State[j, k, h] == "UL")
                        {
                            Hour_State[j, k, h] = "U-O";
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Run_State = new string[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Run[0] == "Automatic")
                        {
                            Run_State[j, k, h] = Hour_State[j, k, h];
                        }
                        if (Run[0] == "customize")
                        {
                            Run_State[j, k, h] = "";
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Cap_Bid = new string[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Run[0] == "General")
                        {
                            Cap_Bid[j, k, h] = Hour_Cap[j, k, h];
                        }

                        if (Run[0] == "customize")
                        {
                            Cap_Bid[j, k, h] = "";
                        }
                    }
                }
            }
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------
           
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------


            double[, , ,] Price_lost_initial = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Price_lost_initial[j, k, h, 0] = 0;
                        for (int x = 1; x < Xtrain; x++)
                        {
                            Price_lost_initial[j, k, h, x] = Math.Abs(pd_range[j, k, h, x] - pd_range[j, k, h, (x - 1)]) * pdist[j, k, h, x] + Price_lost_initial[j, k, h, (x - 1)];
                        }
                    }
                }

            }

            //**************************************************************************



            double[, , ,] Price_lost = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Price_lost[j, k, h, x] = Price_lost_initial[j, k, h, x] + Math.Abs(1 - Price_lost_initial[j, k, h, (Xtrain - 1)]) / 2;
                        }
                    }
                }

            }

            //**************************************************************************



            double[, , ,] Price_win = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Price_win[j, k, h, x] = 1 - Price_lost[j, k, h, x];
                        }
                    }
                }
            }

            //**************************************************************************


            double[, , ,] Price_win_price = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Price_win_price[j, k, h, x] = Price_win[j, k, h, x] * pdist[j, k, h, x];
                        }
                    }
                }

            }

            //**************************************************************************


            double[, , ,] Price_lost_price = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Price_lost_price[j, k, h, x] = Price_lost[j, k, h, x] * pdist[j, k, h, x];
                        }
                    }
                }
            }

            //**************************************************************************


            double[, , ,] Profit_win_lost = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Profit_win_lost[j, k, h, x] = Price_win_price[j, k, h, x] * pd_range[j, k, h, x] - Price_lost_price[j, k, h, x] * (pd_range[j, k, h, x] - Price_Min[h]);
                        }
                    }
                }
            }

            //**************************************************************************

            double[, , ,] Profit_win = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Profit_win[j, k, h, x] = Price_win_price[j, k, h, x] * pd_range[j, k, h, x];
                        }
                    }
                }

            }
            //**************************************************************************

            double[, , ,] Profit_Margin = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Profit_Margin[j, k, h, x] = Price_lost_price[j, k, h, x] * pd_range[j, k, h, x];
                        }
                    }
                }
            }

            //**************************************************************************
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            try
            {

                Cplex cplex = new Cplex();



                INumVar[, ,] Point_UL = new INumVar[Plants_Num, Package_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Point_UL[j, k, h] = cplex.NumVar(0, double.MaxValue);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
                INumVar[, ,] Point_Opimal = new INumVar[Plants_Num, Package_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Point_Opimal[j, k, h] = cplex.NumVar(-double.MaxValue, double.MaxValue);
                        }
                    }
                }


                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
                INumVar[, ,] Point_Margin = new INumVar[Plants_Num, Package_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Point_Margin[j, k, h] = cplex.NumVar(-double.MaxValue, double.MaxValue);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
                ILinearNumExpr Sum_Point_Margin = cplex.LinearNumExpr();
                double _Sum_Point_Margin = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Point_Margin.AddTerm(_Sum_Point_Margin, Point_Margin[j, k, h]);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                ILinearNumExpr Sum_Point_UL = cplex.LinearNumExpr();
                double _Sum_Point_UL = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Point_UL.AddTerm(_Sum_Point_UL, Point_UL[j, k, h]);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                ILinearNumExpr Sum_Point_Opimal = cplex.LinearNumExpr();
                double _Sum_Point_Opimal = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Point_Opimal.AddTerm(_Sum_Point_Opimal, Point_Opimal[j, k, h]);
                        }
                    }
                }


                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int x = 0; x < Xtrain; x++)
                            {
                                cplex.AddGe(Point_UL[j, k, h], Profit_win_lost[j, k, h, x]);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int x = 0; x < Xtrain; x++)
                            {
                                cplex.AddGe(Point_Opimal[j, k, h], Profit_win[j, k, h, x]);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int x = 0; x < Xtrain; x++)
                            {
                                cplex.AddGe(Point_Margin[j, k, h], Profit_Margin[j, k, h, x]);
                            }
                        }
                    }
                }

                //*******************************************************************************************************
                cplex.AddMinimize(cplex.Sum(Sum_Point_Opimal, cplex.Sum(Sum_Point_Margin, Sum_Point_UL)));
                //*******************************************************************************************************


                //*******************************************************************************************************
                if (cplex.Solve())
                {
                    System.Windows.Forms.MessageBox.Show(" Value =   Price:   " + cplex.ObjValue + "   " + cplex.GetStatus());
                    int[, ,] Bench = new int[Plants_Num, Package_Num, Hour];
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                Bench[j, k, h] = 0;
                                for (int x = 0; x < Xtrain; x++)
                                {
                                    if (Profit_win[j, k, h, x] == cplex.GetValue(Point_Opimal[j, k, h]))
                                    {
                                        Price_optimal[j, k, h] = pd_range[j, k, h, x];
                                        X_select[j, k, h, 1] = x;
                                    }
                                    if (Profit_win_lost[j, k, h, x] == cplex.GetValue(Point_UL[j, k, h]))
                                    {
                                        Price_UL[j, k, h] = pd_range[j, k, h, x];
                                        X_select[j, k, h, 0] = x;
                                    }
                                    if ((PriceForecasting[j, k, h] < pd_range[j, k, h, x]) & (Bench[j, k, h] == 0))
                                    {
                                        Bench[j, k, h] = 1;
                                    }
                                    if (Bench[j, k, h] == 1)
                                    {
                                        X_select[j, k, h, 2] = x;
                                        Bench[j, k, h] = 2;
                                    }
                                    if (Profit_Margin[j, k, h, x] == cplex.GetValue(Point_Margin[j, k, h]))
                                    {
                                        Price_Margin[j, k, h] = pd_range[j, k, h, x];
                                        X_select[j, k, h, 3] = x;
                                    }
                                }
                                //MessageBox.Show("  pack=   " + k.ToString() +"  hour = " + (h + 1).ToString() + "\n  Point_UL=   " + Price_UL[j, k, h] + "    Point_Opimal=  " + Price_optimal[j, k, h] + "\n   Price = " + PriceForecasting[j, k, h] + " Point_Margin=   " + Price_Margin[j, k, h]);

                            }
                        }
                    }
                }

                cplex.End();
            }
            catch (ILOG.CPLEX.CouldNotInstallColumnException e1)
            {
                System.Console.WriteLine("concert exception" + e1 + "caught");
            }

            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------
           
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------

            double[,] Tbid_Plant_unit = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Tbid_Plant_unit[j, i] = (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]) / Mmax;
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[, ,] Fbid_Plant_unit = new double[Plants_Num, Units_Num, Mmax];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int m = 0; m < Mmax; m++)
                    {
                        Fbid_Plant_unit[j, i, m] = Unit_Data[j, i, 9] + ((2 * (m + 1)) - 1) * Unit_Data[j, i, 8] * Tbid_Plant_unit[j, i] + (2 * Unit_Data[j, i, 8] * Unit_Data[j, i, 3]);
                    }

                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[,] Fmin_Plant_unit = new double[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Fmin_Plant_unit[j, i] = Unit_Data[j, i, 8] * Unit_Data[j, i, 3] * Unit_Data[j, i, 3] + Unit_Data[j, i, 9] * Unit_Data[j, i, 3] + Unit_Data[j, i, 10];
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[, ,] Fstart_Plant_unit = new double[Plants_Num, Units_Num, Start_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int t = 0; t < Start_Num; t++)
                    {
                        if (t < (Unit_Data[j, i, 17]))
                        {
                            Fstart_Plant_unit[j, i, t] = Unit_Data[j, i, 19];
                        }
                        else
                        {
                            Fstart_Plant_unit[j, i, t] = Unit_Data[j, i, 18];
                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[, ,] Price_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k))
                            {
                                Price_Plant_unit[j, i, h] = PriceForecasting[j, k, h];
                            }
                        }
                    }
                }
            }

            //*******************************************************************************************************

            try
            {
                Cplex cplex = new Cplex();


                IIntVar[, ,] V_Plant_unit = new IIntVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            V_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                        }

                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                IIntVar[, ,] X_Plant_unit = new IIntVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if (Unit_Dec[j, i, 1].ToString().Trim() == "CC")
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                X_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                            }
                        }

                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                IIntVar[, ,] Y_Plant_unit = new IIntVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Y_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                        }

                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                IIntVar[,] Z_Plant_unit = new IIntVar[Plants_Num, Units_Num];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        Z_Plant_unit[j, i] = cplex.IntVar(0, 1);

                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[, ,] Power_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Power_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }

                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[, ,] Pres_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Pres_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }

                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[, , ,] delta_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour, Mmax];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int m = 0; m < Mmax; m++)
                            {
                                delta_Plant_unit[j, i, h, m] = cplex.NumVar(0.0, double.MaxValue);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[, ,] Cost_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Cost_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[, ,] Income_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Income_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[, ,] Benefit_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Benefit_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[, ,] CostStart_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            CostStart_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }

                //**************************************************************************************************


                ILinearNumExpr Sum_Cost_day = cplex.LinearNumExpr();

                double _Sum_Cost_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Cost_day.AddTerm(_Sum_Cost_day, Cost_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                ILinearNumExpr Sum_CostStart_day = cplex.LinearNumExpr();

                double _Sum_CostStart_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_CostStart_day.AddTerm(_Sum_CostStart_day, Cost_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                ILinearNumExpr Sum_Power_day = cplex.LinearNumExpr();
                double _Sum_Power_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Power_day.AddTerm(_Sum_Power_day, Power_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                ILinearNumExpr Sum_Income_day = cplex.LinearNumExpr();

                double _Sum_Income_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Income_day.AddTerm(_Sum_Income_day, Income_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                ILinearNumExpr Sum_Benefit_day = cplex.LinearNumExpr();

                double _Sum_Benefit_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Benefit_day.AddTerm(_Sum_Benefit_day, Benefit_Plant_unit[j, i, h]);
                        }
                    }
                }
                //*******************************************************************************************************


                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddEq(Income_Plant_unit[j, i, h], cplex.Prod(Price_Plant_unit[j, i, h], Power_Plant_unit[j, i, h]));
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddEq(Z_Plant_unit[j, i], Outservice_Plant_unit[j, i]);
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddLe(cplex.Sum(V_Plant_unit[j, i, h], Z_Plant_unit[j, i]), 1);
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddEq(Benefit_Plant_unit[j, i, h], cplex.Diff(Income_Plant_unit[j, i, h], cplex.Sum(CostStart_Plant_unit[j, i, h], Cost_Plant_unit[j, i, h])));
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddGe(Power_Plant_unit[j, i, h], cplex.Prod(Unit_Data[j, i, 3], V_Plant_unit[j, i, h]));
                        }
                    }
                };

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddLe(Power_Plant_unit[j, i, h], cplex.Prod(Unit_Data[j, i, 4], V_Plant_unit[j, i, h]));
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int m = 0; m < Mmax; m++)
                            {
                                cplex.AddLe(delta_Plant_unit[j, i, h, m], Tbid_Plant_unit[j, i]);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _Sum_Delta_m = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            ILinearNumExpr Sum_Delta_m = cplex.LinearNumExpr();
                            for (int m = 0; m < Mmax; m++)
                            {
                                Sum_Delta_m.AddTerm(_Sum_Delta_m, delta_Plant_unit[j, i, h, m]);
                            }
                            cplex.AddEq(Power_Plant_unit[j, i, h], cplex.Sum(cplex.Prod(Unit_Data[j, i, 3], V_Plant_unit[j, i, h]), Sum_Delta_m));
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            ILinearNumExpr DeltaBid_m = cplex.LinearNumExpr();
                            for (int m = 0; m < Mmax; m++)
                            {
                                DeltaBid_m.AddTerm(Fbid_Plant_unit[j, i, m], delta_Plant_unit[j, i, h, m]);
                            }
                            cplex.AddEq(Cost_Plant_unit[j, i, h], cplex.Sum(DeltaBid_m, cplex.Prod(Fmin_Plant_unit[j, i], V_Plant_unit[j, i, h])));
                        }
                    }
                };
                ////^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hisup_Plant_unit[j, i]; h++)
                        {
                            cplex.AddEq(V_Plant_unit[j, i, h], 1);
                        }
                    }
                };

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hisdown_Plant_unit[j, i]; h++)
                        {
                            cplex.AddEq(V_Plant_unit[j, i, h], 0);
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _Minup_2 = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h <= (Hour - Unit_Data[j, i, 6]); h++)
                        {
                            ILinearNumExpr v_up = cplex.LinearNumExpr();
                            for (int hh = h; hh <= (h + Unit_Data[j, i, 6] - 1); hh++)
                            {
                                v_up.AddTerm(_Minup_2, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(v_up, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(V_Plant_unit[j, i, h], V_Plant_unit[j, i, h - 1])));
                            }
                            else
                            {
                                cplex.AddGe(v_up, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(V_Plant_unit[j, i, h], V_history_Plant_unit[j, i])));
                            }
                        }
                    }
                }

                double _Mindown_2 = -1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h <= (Hour - (int)Unit_Data[j, i, 7]); h++)
                        {
                            ILinearNumExpr v_down = cplex.LinearNumExpr();
                            for (int hh = h; hh <= (h + Unit_Data[j, i, 7] - 1); hh++)
                            {
                                v_down.AddTerm(_Mindown_2, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(v_down, cplex.Prod(Unit_Data[j, i, 7], cplex.Diff(cplex.Diff(V_Plant_unit[j, i, h - 1], V_Plant_unit[j, i, h]), 1)));
                            }
                            else
                            {
                                cplex.AddGe(v_down, cplex.Prod(Unit_Data[j, i, 7], cplex.Diff(cplex.Diff(V_history_Plant_unit[j, i], V_Plant_unit[j, i, h]), 1)));
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _Minup_h = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = (Hour - (int)Unit_Data[j, i, 6] + 1); h < Hour; h++)
                        {
                            ILinearNumExpr vh_up = cplex.LinearNumExpr();
                            for (int hh = h; hh < Hour; hh++)
                            {
                                vh_up.AddTerm(_Minup_h, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(cplex.Diff(vh_up, cplex.Diff(cplex.Prod((Hour - h), V_Plant_unit[j, i, h]), cplex.Prod((Hour - h), V_Plant_unit[j, i, h - 1]))), 0);
                            }
                            else
                            {
                                cplex.AddGe(cplex.Diff(vh_up, cplex.Diff(cplex.Prod((Hour - h), V_Plant_unit[j, i, h]), ((Hour - h) * V_history_Plant_unit[j, i]))), 0);
                            }
                        }
                    }
                }

                double _Mindown_h = -1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = (Hour - (int)Unit_Data[j, i, 6] + 1); h < Hour; h++)
                        {
                            ILinearNumExpr vh_down = cplex.LinearNumExpr();
                            for (int hh = h; hh < Hour; hh++)
                            {
                                vh_down.AddTerm(_Mindown_h, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(cplex.Sum((Hour - h), cplex.Diff(vh_down, cplex.Diff(cplex.Prod((Hour - h), V_Plant_unit[j, i, h - 1]), cplex.Prod((Hour - h), V_Plant_unit[j, i, h])))), 0);
                            }
                            else
                            {
                                cplex.AddGe(cplex.Sum((Hour - h), cplex.Diff(vh_down, cplex.Diff(((Hour - h) * (1 - V_history_Plant_unit[j, i])), cplex.Prod((Hour - h), V_Plant_unit[j, i, h])))), 0);
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h < Hour; h++)
                        {
                            if (h < 23)
                            {
                                cplex.AddLe(Pres_Plant_unit[j, i, h], cplex.Sum(cplex.Prod(Pmax_Plant_unit[j, i, h] * 10, V_Plant_unit[j, i, h + 1]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h], V_Plant_unit[j, i, h + 1]))));
                            }

                            if ((h > 0))
                            {
                                cplex.AddLe(Power_Plant_unit[j, i, h], cplex.Sum(Power_Plant_unit[j, i, h - 1], cplex.Sum(cplex.Prod(Unit_Data[j, i, 1], V_Plant_unit[j, i, h]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h], V_Plant_unit[j, i, h - 1])), cplex.Prod(Pmax_Plant_unit[j, i, h] * 10, cplex.Diff(1, V_Plant_unit[j, i, h])))));
                                cplex.AddLe(cplex.Diff(Power_Plant_unit[j, i, h - 1], Power_Plant_unit[j, i, h]), cplex.Sum(cplex.Prod(Unit_Data[j, i, 2], V_Plant_unit[j, i, h]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h - 1], V_Plant_unit[j, i, h])), cplex.Diff(Pmax_Plant_unit[j, i, h] * 10, cplex.Diff(1, V_Plant_unit[j, i, h - 1]))));
                            }
                            if (h == 0)
                            {
                                cplex.AddLe(Power_Plant_unit[j, i, h], cplex.Sum(Powerhistory_Plant_unit[j, i], cplex.Sum(cplex.Prod(Unit_Data[j, i, 1], V_Plant_unit[j, i, h]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h], V_history_Plant_unit[j, i])), cplex.Prod(Pmax_Plant_unit[j, i, h] * 10, cplex.Diff(1, V_Plant_unit[j, i, h])))));
                                cplex.AddLe(cplex.Diff(Powerhistory_Plant_unit[j, i], Power_Plant_unit[j, i, h]), cplex.Sum(cplex.Prod(Unit_Data[j, i, 2], V_Plant_unit[j, i, h]), cplex.Sum(cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_history_Plant_unit[j, i], V_Plant_unit[j, i, h])), (Pmax_Plant_unit[j, i, h] * 10 - (1 - V_history_Plant_unit[j, i])))));
                            }
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _Unit_Gas_CC = 0.5;
                for (int h = 0; h < Hour; h++)
                {
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            if (Package[j, k].ToString().Trim() == "CC")
                            {
                                ILinearNumExpr Unit_Gas_CC = cplex.LinearNumExpr();
                                {
                                    for (int i = 0; i < Plant_units_Num[j]; i++)
                                    {
                                        if ((Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Data[j, i, 0] == k) & (Unit_Dec[j, i, 1].ToString().Trim() == "CC")) ;
                                        {
                                            Unit_Gas_CC.AddTerm(_Unit_Gas_CC, Power_Plant_unit[j, i, h]);
                                        }
                                    }
                                }
                                for (int i = 0; i < Plant_units_Num[j]; i++)
                                {
                                    if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam") & (Unit_Data[j, i, 0] == k) & (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                                    {
                                        cplex.AddGe(Unit_Gas_CC, cplex.Diff(Power_Plant_unit[j, i, h], cplex.Prod(cplex.Diff(1, V_Plant_unit[j, i, h]), Pmax_Plant_unit[j, i, h])));
                                    }
                                }
                            }
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _X_1 = 1;
                double _X_2 = 1;

                int[] i_Start = new int[1];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                        {
                            i_Start[0] = ((int)Unit_Data[j, i, 16]);
                        }
                        if (i_Start[0] == 0)
                        {
                            i_Start[0] = 8;
                        }
                    }
                }

                double[] _X_Start = new double[1];
                _X_Start[0] = Math.Pow(i_Start[0], -1);
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        if (Package[j, k].ToString().Trim() == "CC")
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                for (int i = 0; i < Plant_units_Num[j]; i++)
                                {
                                    if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Data[j, i, 0] == (k)))
                                    {
                                        if (h < (i_Start[0] + 1))
                                        {
                                            ILinearNumExpr X_1 = cplex.LinearNumExpr();
                                            for (int hh = 0; hh <= h; hh++)
                                            {
                                                X_1.AddTerm(_X_1, V_Plant_unit[j, i, hh]);
                                            }
                                            cplex.AddLe(cplex.Diff(cplex.Prod(_X_Start[0], cplex.Sum(X_1, hisstart_Plant_unit[j, i])), 1), X_Plant_unit[j, i, h]);
                                            cplex.AddLe(X_Plant_unit[j, i, h], cplex.Prod(_X_Start[0], cplex.Sum(X_1, hisstart_Plant_unit[j, i])));

                                        }
                                        if (h > (i_Start[0]))
                                        {
                                            ILinearNumExpr X_2 = cplex.LinearNumExpr();
                                            for (int hh = h - i_Start[0]; hh <= h; hh++)
                                            {

                                                X_2.AddTerm(_X_2, V_Plant_unit[j, i, hh]);
                                            }

                                            cplex.AddLe(cplex.Diff(cplex.Prod(_X_Start[0], X_2), 1), X_Plant_unit[j, i, h]);
                                            cplex.AddLe(X_Plant_unit[j, i, h], cplex.Prod(_X_Start[0], X_2));
                                        }
                                    }
                                }

                                for (int ii = 0; ii < Plant_units_Num[j]; ii++)
                                {
                                    if ((Unit_Dec[j, ii, 1].ToString().Trim() == "CC") & (Unit_Dec[j, ii, 0].ToString().Trim() == "Steam") & (Unit_Data[j, ii, 0] == (k)))
                                    {
                                        ILinearNumExpr X_CC = cplex.LinearNumExpr();
                                        for (int i = 0; i < Plant_units_Num[j]; i++)
                                        {
                                            if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Data[j, i, 0] == (k)))
                                            {
                                                X_CC.AddTerm(Pmax_Plant_unit[j, i, h] * 0.5, X_Plant_unit[j, i, h]);
                                            }
                                        }
                                        cplex.AddGe(X_CC, Power_Plant_unit[j, ii, h]);

                                    }
                                }
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _V_Start1 = 1;
                double _V_Start2 = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int t = 0; t <= h; t++)
                            {
                                if ((t < h) & (t <= Unit_Data[j, i, 16]))
                                {
                                    ILinearNumExpr V_Start1 = cplex.LinearNumExpr();
                                    for (int tt = 0; tt <= t; tt++)
                                    {
                                        V_Start1.AddTerm(_V_Start1, V_Plant_unit[j, i, (h - tt - 1)]);
                                    }
                                    cplex.AddGe(CostStart_Plant_unit[j, i, h], cplex.Prod(Fstart_Plant_unit[j, i, t], cplex.Diff(V_Plant_unit[j, i, h], V_Start1)));


                                }
                                if ((t == h) & (V_history_Plant_unit[j, i] == 0) & (t <= Unit_Data[j, i, 16]))
                                {
                                    ILinearNumExpr V_Start2 = cplex.LinearNumExpr();
                                    for (int tt = 0; tt < t; tt++)
                                    {
                                        V_Start2.AddTerm(_V_Start2, V_Plant_unit[j, i, (h - tt)]);
                                    }
                                    cplex.AddGe(CostStart_Plant_unit[j, i, h], cplex.Prod(Fstart_Plant_unit[j, i, (t + HisShut_Plant_unit[j, i])], cplex.Diff(V_Plant_unit[j, i, h], V_Start2)));
                                }
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h < Hour; h++)
                        {
                            if (h > 0)
                            {
                                cplex.AddLe(Y_Plant_unit[j, i, h], cplex.Sum(1, cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Power_Plant_unit[j, i, h - 1]))));
                                cplex.AddGe(Y_Plant_unit[j, i, h], cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Power_Plant_unit[j, i, h - 1])));
                            }
                            if (h == 0)
                            {
                                cplex.AddLe(Y_Plant_unit[j, i, h], cplex.Sum(1, cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Powerhistory_Plant_unit[j, i]))));
                                cplex.AddGe(Y_Plant_unit[j, i, h], cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Powerhistory_Plant_unit[j, i])));
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _Inc_Ramp_2 = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < (Hour - (int)Unit_Data[j, i, 6]); h++)
                        {
                            ILinearNumExpr Y_up = cplex.LinearNumExpr();
                            for (int hh = h + 1; hh <= (h + Unit_Data[j, i, 6]); hh++)
                            {
                                Y_up.AddTerm(_Inc_Ramp_2, Y_Plant_unit[j, i, hh]);
                            }
                            if (h < 23)
                            {
                                cplex.AddGe(Y_up, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(Y_Plant_unit[j, i, h + 1], Y_Plant_unit[j, i, h])));
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                double _Dec_Ramp_2 = -1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h < (Hour - (int)Unit_Data[j, i, 6]); h++)
                        {
                            ILinearNumExpr Y_down = cplex.LinearNumExpr();
                            for (int hh = (h + 1); hh <= (h + Unit_Data[j, i, 6]); hh++)
                            {
                                Y_down.AddTerm(_Dec_Ramp_2, Y_Plant_unit[j, i, hh]);
                            }
                            if (h < 23)
                            {
                                cplex.AddGe(Y_down, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(cplex.Diff(Y_Plant_unit[j, i, h], Y_Plant_unit[j, i, (h + 1)]), 1)));
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _Inc_Ramp_3 = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = (Hour - (int)Unit_Data[j, i, 6]); h < Hour; h++)
                        {
                            ILinearNumExpr YY_up = cplex.LinearNumExpr();
                            for (int hh = (h + 1); hh < Hour; hh++)
                            {
                                YY_up.AddTerm(_Inc_Ramp_3, Y_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(cplex.Diff(YY_up, cplex.Diff(cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, h]), cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, h - 1]))), 0);
                            }
                        }
                    }
                }

                double _Dec_Ramp_3 = -1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = (Hour - (int)Unit_Data[j, i, 6]); h < Hour; h++)
                        {
                            ILinearNumExpr Yh_down = cplex.LinearNumExpr();
                            for (int hh = (h + 1); hh < Hour; hh++)
                            {
                                Yh_down.AddTerm(_Dec_Ramp_3, Y_Plant_unit[j, i, hh]);
                            }
                            if (h < 23)
                            {
                                cplex.AddGe(cplex.Sum((Hour - h - 1), cplex.Diff(Yh_down, cplex.Diff(cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, h]), cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, (h + 1)])))), 0);
                            }
                        }
                    }
                }
                //*******************************************************************************************************

                cplex.AddMaximize(Sum_Benefit_day);
                //*******************************************************************************************************


                if (cplex.Solve())
                {
                    System.Windows.Forms.MessageBox.Show(" Value =  Calculate : PBUC    " + cplex.ObjValue + "   " + cplex.GetStatus());
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                Power_memory[j, i, h] = cplex.GetValue(Power_Plant_unit[j, i, h]);
                                Test[j, i, h] = Power_memory[j, i, h];
                                Power_Error[j, i, h] = 0;
                                if (Power_memory[j, i, h] < 2)
                                {
                                    Power_Error[j, i, h] = 1;
                                }
                            }
                        }
                    }

                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                for (int i = 0; i < Plant_units_Num[j]; i++)
                                {
                                    if (((int)Unit_Data[j, i, 0] == (k)))
                                    {
                                        if ((Power_Error[j, i, h] == 0) & (Package[j, k].ToString().Trim() != "CC"))
                                        {
                                            Power_if_Pack[j, k, h] = Unit_Data[j, i, 3];
                                            Power_memory_Pack[j, k, h] = Power_memory[j, i, h];
                                            Power_if_CC[j, k, h] = Unit_Data[j, i, 3];
                                        }
                                        if ((Power_Error[j, i, h] == 0) & (Package[j, k].ToString().Trim() == "CC"))
                                        {
                                            Power_memory_Pack[j, k, h] = Power_memory_Pack[j, k, h] + Power_memory[j, i, h];
                                            Power_if_Pack[j, k, h] = Power_if_Pack[j, k, h] + Unit_Data[j, i, 3];
                                        }
                                        if ((Power_Error[j, i, h] == 0) & (Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Gas"))
                                        {
                                            Power_if_CC[j, k, h] = Unit_Data[j, i, 3];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                Power_Error_Pack[j, k, h] = 0;
                                if (Power_memory_Pack[j, k, h] < 2)
                                {
                                    Power_Error_Pack[j, k, h] = 1;
                                }
                                if (Power_memory_Pack[j, k, h] < 3)
                                {
                                    Power_if_Pack[j, k, h] = Power_if_CC[j, k, h];
                                    Power_memory_Pack[j, k, h] = Power_if_CC[j, k, h] + 3;
                                }
                            }
                        }
                    }
                }
                cplex.End();
            }
            catch (ILOG.CPLEX.Cplex.UnknownObjectException e1)
            {
                MessageBox.Show("concert exception" + e1.ToString() + "caught");
            }
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------
           
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------

            double[, , ,] Weight_Lost = new double[Plants_Num, Package_Num, Hour, Power_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int n = 0; n < Power_Num; n++)
                        {
                            Weight_Lost[j, k, h, n] = Price_lost[j, k, h, X_select[j, k, h, n]] * pd_range[j, k, h, X_select[j, k, h, n]];
                        }
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            try
            {
                Cplex cplex = new Cplex();

                INumVar[, , , ,] Power_bid_Plant_Pack = new INumVar[Plants_Num, Package_Num, Hour, Power_Num, Power_W];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int n = 0; n < Power_Num; n++)
                            {
                                for (int w = 0; w < Power_W; w++)
                                {
                                    Power_bid_Plant_Pack[j, k, h, n, w] = (cplex.NumVar(0.0, double.MaxValue));
                                }
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumExpr[, , ,] Value_Step_Plant_Pack = new INumExpr[Plants_Num, Package_Num, Hour, Power_Num];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int w = 0; w < Power_W; w++)
                            {
                                Value_Step_Plant_Pack[j, k, h, w] = (cplex.NumExpr());
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                double _Sum_Power_bid = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int w = 0; w < Power_W; w++)
                            {
                                ILinearNumExpr Sum_Power_bid = cplex.LinearNumExpr();
                                for (int n = 0; n < Power_Num; n++)
                                {
                                    Sum_Power_bid.AddTerm(_Sum_Power_bid, Power_bid_Plant_Pack[j, k, h, n, w]);
                                }
                                cplex.AddEq(Sum_Power_bid, Power_memory_Pack[j, k, h]);
                            }

                        }

                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                INumExpr[, , , ,] Prod_Power_Plant_Pack = new INumExpr[Plants_Num, Package_Num, Hour, Power_Num, Power_Num];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int w = 0; w < Power_W; w++)
                            {
                                for (int n = 0; n < Power_Num; n++)
                                {
                                    Prod_Power_Plant_Pack[j, k, h, n, w] = cplex.Prod(1, cplex.Square(Power_bid_Plant_Pack[j, k, h, n, w]));
                                }
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int w = 0; w < Power_Num; w++)
                            {
                                if (w == 0)
                                {
                                    Value_Step_Plant_Pack[j, k, h, w] = cplex.Sum(cplex.Prod(Prod_Power_Plant_Pack[j, k, h, 1, w], Math.Pow(Weight_Lost[j, k, h, 1], 2)),
                                    cplex.Prod(Prod_Power_Plant_Pack[j, k, h, 2, w], Math.Pow(Weight_Lost[j, k, h, 2], 2)), cplex.Prod(Power_bid_Plant_Pack[j, k, h, 1, w], Power_bid_Plant_Pack[j, k, h, 2, w], (2 * Weight_Lost[j, k, h, 1])));
                                    cplex.AddEq(Power_bid_Plant_Pack[j, k, h, 3, w], 0);
                                    cplex.AddGe(Power_bid_Plant_Pack[j, k, h, 2, w], 1);
                                    cplex.AddGe(Power_bid_Plant_Pack[j, k, h, 1, w], 1);
                                    cplex.AddEq(Power_bid_Plant_Pack[j, k, h, 0, w], Power_if_Pack[j, k, h]);
                                }
                                if (w == 1)
                                {
                                    Value_Step_Plant_Pack[j, k, h, w] = cplex.Sum(cplex.Prod(Prod_Power_Plant_Pack[j, k, h, 1, w], Math.Pow(Weight_Lost[j, k, h, 1], 2)),
                                    cplex.Prod(Prod_Power_Plant_Pack[j, k, h, 3, w], Math.Pow(Weight_Lost[j, k, h, 3], 2)), cplex.Prod(Power_bid_Plant_Pack[j, k, h, 1, w], Power_bid_Plant_Pack[j, k, h, 3, w], (2 * Weight_Lost[j, k, h, 1])));
                                    cplex.AddEq(Power_bid_Plant_Pack[j, k, h, 2, w], 0);
                                    cplex.AddGe(Power_bid_Plant_Pack[j, k, h, 3, w], 1);
                                    cplex.AddGe(Power_bid_Plant_Pack[j, k, h, 1, w], 1);
                                    cplex.AddEq(Power_bid_Plant_Pack[j, k, h, 0, w], Power_if_Pack[j, k, h]);
                                }
                                if (w == 2)
                                {
                                    Value_Step_Plant_Pack[j, k, h, w] = cplex.Sum(cplex.Prod(Prod_Power_Plant_Pack[j, k, h, 2, w], Math.Pow(Weight_Lost[j, k, h, 2], 2)),
                                    cplex.Prod(Prod_Power_Plant_Pack[j, k, h, 3, w], Math.Pow(Weight_Lost[j, k, h, 3], 2)), cplex.Prod(Power_bid_Plant_Pack[j, k, h, 2, w], Power_bid_Plant_Pack[j, k, h, 3, w], (2 * Weight_Lost[j, k, h, 2])));
                                    cplex.AddEq(Power_bid_Plant_Pack[j, k, h, 1, w], 0);
                                    cplex.AddGe(Power_bid_Plant_Pack[j, k, h, 3, w], 1);
                                    cplex.AddGe(Power_bid_Plant_Pack[j, k, h, 2, w], 1);
                                    cplex.AddEq(Power_bid_Plant_Pack[j, k, h, 0, w], Power_if_Pack[j, k, h]);
                                }
                                if (w == 3)
                                {
                                    Value_Step_Plant_Pack[j, k, h, w] = cplex.Sum(cplex.Sum(cplex.Prod(Prod_Power_Plant_Pack[j, k, h, 2, w], Math.Pow(Weight_Lost[j, k, h, 2], 2)),
                                    cplex.Prod(Prod_Power_Plant_Pack[j, k, h, 1, w], Math.Pow(Weight_Lost[j, k, h, 1], 2)), cplex.Prod(Power_bid_Plant_Pack[j, k, h, 1, w], Power_bid_Plant_Pack[j, k, h, 3, w], (2 * Weight_Lost[j, k, h, 1]))),
                                    cplex.Prod(Prod_Power_Plant_Pack[j, k, h, 3, w], Math.Pow(Weight_Lost[j, k, h, 3], 2)), cplex.Sum(cplex.Prod(Power_bid_Plant_Pack[j, k, h, 2, w], Power_bid_Plant_Pack[j, k, h, 3, w], (2 * Weight_Lost[j, k, h, 2])),
                                    cplex.Prod(Power_bid_Plant_Pack[j, k, h, 2, w], Power_bid_Plant_Pack[j, k, h, 1, w], (2 * Weight_Lost[j, k, h, 1]))));
                                    cplex.AddGe(Power_bid_Plant_Pack[j, k, h, 3, w], 1);
                                    cplex.AddGe(Power_bid_Plant_Pack[j, k, h, 2, w], 1);
                                    cplex.AddGe(Power_bid_Plant_Pack[j, k, h, 1, w], 1);
                                    cplex.AddEq(Power_bid_Plant_Pack[j, k, h, 0, w], Power_if_Pack[j, k, h]);
                                }
                            }

                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                INumExpr Sum_Value_Step = cplex.NumExpr();
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int w = 0; w < Power_Num; w++)
                            {
                                Sum_Value_Step = cplex.Sum(Sum_Value_Step, Value_Step_Plant_Pack[j, k, h, w]);
                            }
                        }

                    }
                }


                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


                cplex.AddMinimize(Sum_Value_Step);

                //*******************************************************************************************************

                if (cplex.Solve())
                {
                    System.Windows.Forms.MessageBox.Show(" Value =   Power:   " + cplex.ObjValue + "   " + cplex.GetStatus());
                    //tot = cplex.ObjValue;
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                for (int s = 0; s < Step_Num; s++)
                                {
                                    if ((Run_State[j, k, h] == "U-O") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = Price_UL[j, k, h];
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                        }
                                        if ((s < 3) & (s > 0))
                                        {
                                            Price[j, k, h, s] = Price_UL[j, k, h] + s * (Price_optimal[j, k, h] - Price_UL[j, k, h]) / 3;
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                        }
                                        if (s == 3)
                                        {
                                            Price[j, k, h, s] = Price_optimal[j, k, h];
                                            Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                        }
                                        if (s > 3)
                                        {

                                            Price[j, k, h, s] = Price_optimal[j, k, h] + (s - 3) * ErrorBid[0];
                                            Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                        }
                                    }
                                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    if ((Run_State[j, k, h] == "U-F") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = Price_UL[j, k, h];
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                        }
                                        if ((s < 3) & (s > 0))
                                        {
                                            Price[j, k, h, s] = Price_UL[j, k, h] + s * (PriceForecasting[j, k, h] - Price_UL[j, k, h]) / 3;
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                        }
                                        if (s == 3)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h];
                                            Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                        }
                                        if (s > 3)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 3) * ErrorBid[0];
                                            Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                        }
                                    }
                                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    if ((Run_State[j, k, h] == "U-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = Price_UL[j, k, h];
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                        }
                                        if ((s < 3) & (s > 0))
                                        {
                                            Price[j, k, h, s] = Price_UL[j, k, h] + s * (Price_Margin[j, k, h] - Price_UL[j, k, h]) / 3;
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                        }
                                        if (s == 3)
                                        {
                                            Price[j, k, h, s] = Price_Margin[j, k, h];
                                            Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                        }
                                        if (s > 3)
                                        {
                                            Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 3) * ErrorBid[0];
                                            Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                        }
                                    }
                                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    if ((Run_State[j, k, h] == "O-F") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = Price_optimal[j, k, h];
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                        }
                                        if ((s < 3) & (s > 0))
                                        {
                                            Price[j, k, h, s] = Price_optimal[j, k, h] + s * (PriceForecasting[j, k, h] - Price_optimal[j, k, h]) / 3;
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                        }
                                        if (s == 3)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h];
                                            Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                        }
                                        if (s > 3)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 3) * ErrorBid[0];
                                            Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                        }
                                    }
                                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    if ((Run_State[j, k, h] == "O-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = Price_optimal[j, k, h];
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                        }
                                        if ((s < 3) & (s > 0))
                                        {
                                            Price[j, k, h, s] = Price_optimal[j, k, h] + s * (Price_Margin[j, k, h] - Price_optimal[j, k, h]) / 3;
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                        }
                                        if (s == 3)
                                        {
                                            Price[j, k, h, s] = Price_Margin[j, k, h];
                                            Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                        }
                                        if (s > 3)
                                        {
                                            Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 3) * ErrorBid[0];
                                            Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                        }
                                    }
                                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    if ((Run_State[j, k, h] == "F-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h];
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                        }
                                        if ((s < 3) & (s > 0))
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] + s * (Price_Margin[j, k, h] - PriceForecasting[j, k, h]) / 3;
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                        }
                                        if (s == 3)
                                        {
                                            Price[j, k, h, s] = Price_Margin[j, k, h];
                                            Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                        }
                                        if (s > 3)
                                        {
                                            Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 3) * ErrorBid[0];
                                            Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                        }
                                    }
                                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    if ((Run_State[j, k, h] == "U-O-F") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = Price_UL[j, k, h];
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                        }
                                        if (s == 1)
                                        {
                                            Price[j, k, h, s] = Price_UL[j, k, h] + (Price_optimal[j, k, h] - Price_UL[j, k, h]) / 2;
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                        }
                                        if (s == 2)
                                        {
                                            Price[j, k, h, s] = Price_optimal[j, k, h];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 0]));
                                        }
                                        if ((s < 5) & (s > 2))
                                        {
                                            Price[j, k, h, s] = Price_optimal[j, k, h] + (s - 2) * (PriceForecasting[j, k, h] - Price_optimal[j, k, h]) / 3;
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 0])) + 0.2 * (s - 2);
                                        }
                                        if (s == 5)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 0]) + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 2, 0])) - 4 * 0.2;
                                        }
                                        if ((s < 10) & (s > 5))
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 5) * ErrorBid[0];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 0]) + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 2, 0])) - (9 - s) * 0.2;
                                        }
                                    }
                                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    if ((Run_State[j, k, h] == "U-O-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = Price_UL[j, k, h];
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                        }
                                        if (s == 1)
                                        {
                                            Price[j, k, h, s] = Price_UL[j, k, h] + (Price_optimal[j, k, h] - Price_UL[j, k, h]) / 2;
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                        }
                                        if (s == 2)
                                        {
                                            Price[j, k, h, s] = Price_optimal[j, k, h];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 1]));
                                        }
                                        if ((s < 5) & (s > 2))
                                        {
                                            Price[j, k, h, s] = Price_optimal[j, k, h] + (s - 2) * (Price_Margin[j, k, h] - Price_optimal[j, k, h]) / 3;
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 1])) + 0.2 * (s - 2);
                                        }
                                        if (s == 5)
                                        {
                                            Price[j, k, h, s] = Price_Margin[j, k, h];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 1]) + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 2, 1])) - 4 * 0.2;
                                        }
                                        if ((s < 10) & (s > 5))
                                        {
                                            Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 5) * ErrorBid[0];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 1]) + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 2, 1])) - (9 - s) * 0.2;
                                        }
                                    }
                                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    if ((Run_State[j, k, h] == "U-F-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = Price_UL[j, k, h];
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                        }
                                        if (s == 1)
                                        {
                                            Price[j, k, h, s] = Price_UL[j, k, h] + (PriceForecasting[j, k, h] - Price_UL[j, k, h]) / 2;
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                        }
                                        if (s == 2)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 2]));
                                        }
                                        if ((s < 5) & (s > 2))
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 2) * (Price_Margin[j, k, h] - PriceForecasting[j, k, h]) / 3;
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 2])) + 0.2 * (s - 2);
                                        }
                                        if (s == 5)
                                        {
                                            Price[j, k, h, s] = Price_Margin[j, k, h];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 2]) + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 2, 2])) - 4 * 0.2;
                                        }
                                        if ((s < 10) & (s > 5))
                                        {
                                            Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 5) * ErrorBid[0];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 2]) + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 2, 21])) - (9 - s) * 0.2;
                                        }
                                    }
                                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    if ((Run_State[j, k, h] == "O-F-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = Price_optimal[j, k, h];
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                        }
                                        if (s == 1)
                                        {
                                            Price[j, k, h, s] = Price_optimal[j, k, h] + (PriceForecasting[j, k, h] - Price_optimal[j, k, h]) / 2;
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                        }
                                        if (s == 2)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 2]));
                                        }
                                        if ((s < 5) & (s > 2))
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 2) * (Price_Margin[j, k, h] - PriceForecasting[j, k, h]) / 3;
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 2])) + 0.2 * (s - 2);
                                        }
                                        if (s == 5)
                                        {
                                            Price[j, k, h, s] = Price_Margin[j, k, h];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 2]) + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 2, 2])) - 4 * 0.2;
                                        }
                                        if ((s < 10) & (s > 5))
                                        {
                                            Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 5) * ErrorBid[0];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 2]) + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 2, 21])) - (9 - s) * 0.2;
                                        }
                                    }

                                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                    if ((Run_State[j, k, h] == "U-O-F-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = Price_UL[j, k, h];
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                        }
                                        if (s == 1)
                                        {
                                            Price[j, k, h, s] = Price_UL[j, k, h] + (Price_optimal[j, k, h] - Price_UL[j, k, h]) / 2;
                                            Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                        }
                                        if (s == 2)
                                        {
                                            Price[j, k, h, s] = Price_optimal[j, k, h];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 3]));
                                        }
                                        if (s == 3)
                                        {
                                            Price[j, k, h, s] = Price_optimal[j, k, h] + (PriceForecasting[j, k, h] - Price_optimal[j, k, h]) / 2;
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 3])) + 0.2;
                                        }
                                        if (s == 4)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 3]) + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 2, 3]));
                                        }
                                        if (s == 5)
                                        {
                                            Price[j, k, h, s] = PriceForecasting[j, k, h] + (Price_Margin[j, k, h] - PriceForecasting[j, k, h]) / 2;
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 3]) + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 2, 3]) + 0.2);
                                        }
                                        if (s == 6)
                                        {
                                            Price[j, k, h, s] = Price_Margin[j, k, h];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 3]) + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 2, 3]) + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 3, 3])) - 3 * 0.2;
                                        }
                                        if ((s < 10) & (s > 6))
                                        {
                                            Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 6) * ErrorBid[0];
                                            Power[j, k, h, s] = (Power_if_Pack[j, k, h] + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 1, 3]) + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 2, 3]) + cplex.GetValue(Power_bid_Plant_Pack[j, k, h, 3, 3])) - (9 - s) * 0.2;
                                        }
                                    }

                                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                    
                                    if ((Cap_Bid[j, k, h] == "yes"))
                                    {
                                        if (s == 0)
                                        {
                                            Price[j, k, h, s] = CapPrice[0] * 0.999;
                                            Power[j, k, h, s] = Pmax_Plant_Pack[j, k, h] - 0.2;
                                        }
                                        if (s == 1)
                                        {
                                            Price[j, k, h, s] = CapPrice[0] * 0.9993;
                                            Power[j, k, h, s] = Pmax_Plant_Pack[j, k, h] - 0.1;
                                        }
                                        if (s == 2)
                                        {
                                            Price[j, k, h, s] = CapPrice[0] * 0.9996;
                                            Power[j, k, h, s] = Pmax_Plant_Pack[j, k, h];
                                        }
                                        if (s > 2)
                                        {
                                            Price[j, k, h, s] = 0;
                                            Power[j, k, h, s] = 0;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {

                                if ((Power_memory_Pack[j, k, h] < Pmax_Plant_Pack[j, k, h]) & (Package[j, k].ToString().Trim() != "CC"))
                                {
                                    Price[j, k, h, 9] = CapPrice[0];
                                    Power[j, k, h, 9] = Pmax_Plant_Pack[j, k, h];
                                }
                                if (Power_memory_Pack[j, k, h] == Power_if_CC[j, k, h] + 3)
                                {
                                    Price[j, k, h, 1] = CapPrice[0];
                                    Power[j, k, h, 1] = Pmax_Plant_Pack[j, k, h];
                                    for (int s = 2; s < Step_Num; s++)
                                    {
                                        Price[j, k, h, 1] = 0;
                                        Power[j, k, h, 1] = 0;
                                    }
                                }
                                if (Power_Error_Pack[j, k, h] == 1)
                                {
                                    Price[j, k, h, 0] = CapPrice[0];
                                    Power[j, k, h, 0] = Pmax_Plant_unit[j, k, h];
                                    for (int s = 1; s < Step_Num; s++)
                                    {
                                        Price[j, k, h, 1] = 0;
                                        Power[j, k, h, 1] = 0;
                                    }
                                }
                            }
                        }
                    }
                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  
                    int test_cap = 0;
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                test_cap = 0;
                                for (int s = 0; s < Step_Num; s++)
                                {
                                    if ((Price[j, k, h, s] > CapPrice[0]) & (test_cap == 0))
                                    {
                                        Price[j, k, h, s] = CapPrice[0];
                                        Power[j, k, h, s] = Pmax_Plant_Pack[j, k, h];
                                        test_cap = 1;
                                        if (s < 9)
                                        {
                                            for (int ss = (s + 1); ss < Step_Num; ss++)
                                            {
                                                Price[j, k, h, ss] = 0;
                                                Power[j, k, h, ss] = 0;

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                test_cap = 0;

                                if (Outservice_Plant_Pack[j, k] == "Yes")
                                {
                                    for (int ss = 0; ss < Step_Num; ss++)
                                    {
                                        Price[j, k, h, ss] = 0;
                                        Power[j, k, h, ss] = 0;
                                    }
                                }
                            }
                        }
                    }

                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                for (int s = 1; s < Step_Num; s++)
                                {
                                    if ((Price[j, k, h, s - 1] > Price[j, k, h, s - 1]) | (Power[j, k, h, s - 1] > Power[j, k, h, s - 1]))
                                    {
                                      System.Windows.Forms.MessageBox.Show("   Error_1   " + "  Plant  " + j.ToString() + "  Unit  " + k.ToString() + "  Hour  " + h.ToString() + "  Step  " + s.ToString());
                                    }
                                }
                            }
                        }
                    }
                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                for (int s = 0; s < Step_Num; s++)
                                {
                                    oDataTable = utilities.returntbl("insert into Table_1 values('" + plant[j].ToString() + "','" + k.ToString() + "','" + h.ToString() + "','" + Pmax_Plant_Pack[j, k, h].ToString() + "','" + Power[j, k, h, s].ToString() + "','" + Price[j, k, h, s].ToString() + "','" + s.ToString() + "' )");
                                   
                                                                       
                                }
                            }
                        }
                    }
                }
                cplex.End();
                MessageBox.Show("       End       ");
            }
            catch (ILOG.CPLEX.Cplex.UnknownObjectException e1)
            {
                MessageBox.Show("concert exception" + e1.ToString() + "caught");
            }
            return tot;     

                   
        }

        private double[] GetForcast(int ppId, MixUnits mixUnitStatus, string date)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText = "select FinalForecastHourly.hour, FinalForecastHourly.forecast" +
                    " from FinalForecastHourly inner join FinalForecast" +
                    " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id" +
                    " where FinalForecast.PPId=@ppID AND FinalForecast.MixUnitStatus=@mixUnitStatus AND FinalForecast.date=@date" +
                    " order by FinalForecastHourly.hour";

            MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
            MyCom.Parameters["@ppID"].Value = ppId.ToString();
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = date;
            MyCom.Parameters.Add("@mixUnitStatus", SqlDbType.Char, 10);
            MyCom.Parameters["@mixUnitStatus"].Value = mixUnitStatus.ToString();
            Maxda.Fill(MaxDS);

            double[] forecasts = new double[24];
            if (MaxDS.Tables[0].Rows>0)
                for (int j = 0; j < 24; j++)
                   forecasts[j] = Double.Parse(MaxDS.Tables[0].Rows["forecast"].ToString());
            return forecasts;
        }

        private double[,] GetForcast(string date)
        {
            double[,] forecasts = new double[9,24];
            
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText = "select FinalForecastHourly.hour, FinalForecastHourly.forecast" +
                    " from FinalForecastHourly inner join FinalForecast" +
                    " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id" +
                    " where FinalForecast.PPId=@ppID AND FinalForecast.MixUnitStatus=@mixUnitStatus AND FinalForecast.date=@date" +
                    " order by FinalForecastHourly.hour";

            MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
            MyCom.Parameters["@ppID"].Value = ppId.ToString();
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = date;
            MyCom.Parameters.Add("@mixUnitStatus", SqlDbType.Char, 10);
            MyCom.Parameters["@mixUnitStatus"].Value = mixUnitStatus.ToString();
            Maxda.Fill(MaxDS);

            if (MaxDS.Tables[0].Rows > 0)
                for (int j = 0; j < 24; j++)
                    forecasts[j] = Double.Parse(MaxDS.Tables[0].Rows["forecast"].ToString());
            return forecasts;
        }

        private double[] GetVariance(int ppId, MixUnits mixUnitStatus, string date)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText = "select FinalForecastHourly.hour, FinalForecastHourly.vr" +
                    " from FinalForecastHourly inner join FinalForecast" +
                    " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id" +
                    " where FinalForecast.PPId=@ppID AND FinalForecast.MixUnitStatus=@mixUnitStatus AND FinalForecast.date=@date" +
                    " order by FinalForecastHourly.hour";

            MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
            MyCom.Parameters["@ppID"].Value = ppId.ToString();
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = date;
            MyCom.Parameters.Add("@mixUnitStatus", SqlDbType.Char, 10);
            MyCom.Parameters["@mixUnitStatus"].Value = mixUnitStatus.ToString();
            Maxda.Fill(MaxDS);

            double[] variance = new double[24];
            if (MaxDS.Tables[0].Rows > 0)
                for (int j = 0; j < 24; j++)
                    forecasts[j] = Double.Parse(MaxDS.Tables[0].Rows["vr"].ToString());
            return variance;
        }

        private double[,] Get_pd_Range(int ppId, MixUnits mixUnitStatus, string date)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            double[,] pd_Range = new double[24, 201];

            for (int j = 0; j < 24; j++)
            {
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;

                // Insert into FinalForcast table
                MyCom.CommandText =
                    "select FinalForecast201Item.[index], FinalForecast201Item.ff" +
                    " from FinalForecast201Item inner join FinalForecastHourly " +
                    "on FinalForecastHourly.id = FinalForecast201Item.fk_ffHourly " +
                    "inner join FinalForecast " +
                    "on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id " +
                    "where FinalForecast.PPId=@PPId AND FinalForecast.MixUnitStatus=@mixunit " +
                    "AND FinalForecast.date=@date AND FinalForecastHourly.hour=@hour " +
                    "order by FinalForecast201Item.[index]";


                MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                MyCom.Parameters["@ppID"].Value = ppId.ToString();
                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = date;
                MyCom.Parameters.Add("@mixUnitStatus", SqlDbType.Char, 10);
                MyCom.Parameters["@mixUnitStatus"].Value = mixUnitStatus.ToString();
                MyCom.Parameters.Add("@hour", SqlDbType.Int);
                MyCom.Parameters["@hour"].Value =j+1;
                Maxda.Fill(MaxDS);

                if (MaxDS.Tables[0].Rows > 0)
                    for (int k = 0; k < 201; k++)
                        pd_Range[j,k] = Double.Parse(MaxDS.Tables[0].Rows["ff"].ToString());
            }
            return pd_Range;
        }

        private double[,] Get_pdist(int ppId, MixUnits mixUnitStatus, string date)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            double[,] pdist = new double[24, 201];

            for (int j = 0; j < 24; j++)
            {
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;

                // Insert into FinalForcast table
                MyCom.CommandText =
                    "select FinalForecast201Item.[index], FinalForecast201Item.pdist" +
                    " from FinalForecast201Item inner join FinalForecastHourly " +
                    "on FinalForecastHourly.id = FinalForecast201Item.fk_ffHourly " +
                    "inner join FinalForecast " +
                    "on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id " +
                    "where FinalForecast.PPId=@PPId AND FinalForecast.MixUnitStatus=@mixunit " +
                    "AND FinalForecast.date=@date AND FinalForecastHourly.hour=@hour " +
                    "order by FinalForecast201Item.[index]";


                MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                MyCom.Parameters["@ppID"].Value = ppId.ToString();
                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = date;
                MyCom.Parameters.Add("@mixUnitStatus", SqlDbType.Char, 10);
                MyCom.Parameters["@mixUnitStatus"].Value = mixUnitStatus.ToString();
                MyCom.Parameters.Add("@hour", SqlDbType.Int);
                MyCom.Parameters["@hour"].Value = j + 1;
                Maxda.Fill(MaxDS);

                if (MaxDS.Tables[0].Rows > 0)
                    for (int k = 0; k < 201; k++)
                        pdist[j, k] = Double.Parse(MaxDS.Tables[0].Rows["pdist"].ToString());
            }
            return pdist;
        }

        private double[] Get_HourCap(int ppId, MixUnits mixUnitStatus, string date)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            double[] hourCap = new double[24];

                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;

                // Insert into FinalForcast table
                MyCom.CommandText =
                    "select BiddingStrategyCustomized.hour, BiddingStrategyCustomized.Peak " +
                "from BiddingStrategyCustomized inner join BiddingStrategySetting " +
                "on BiddingStrategyCustomized.FkBiddingStrategySettingId = BiddingStrategySetting.id " +
                "where BiddingStrategySetting.BiddingDate= @date AND BiddingStrategySetting.Plant=@ppID " +
                "order by BiddingStrategyCustomized.hour";

                MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                MyCom.Parameters["@ppID"].Value = ppId.ToString();
                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = date;

                Maxda.Fill(MaxDS);

                if (MaxDS.Tables[0].Rows > 0)
                    for (int k = 0; k < 201; k++)
                        hourCap[k] = Double.Parse(MaxDS.Tables[0].Rows["hour"].ToString());
            
            return hourCap[k];
        }

        private string GetDaysBefore(string strDate, int days)
        {
            string strResult = "";
            DateTime date = PersianDateConverter.ToGregorianDateTime(strDate);
            date = date.Subtract(new TimeSpan(days, 0, 0, 0));
            return new PersianDate(date).ToString("d");
        }
        private string GetNearestLoadForecastingDate(string currentDate)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            double[] hourCap = new double[24];

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText =
                "select Date,DateEstimate from LoadForecasting where Date<=" + currentDate.Trim();
            Maxda.Fill(MaxDS);

            DateTime dtDate = PersianDateConverter.ToGregorianDateTime(currentDate);
            DataRow rowNearest = null;
            TimeSpan minSpan = null;

            foreach (DataRow row in MaxDS.Tables[0])
            {
                DateTime tempDate = PersianDateConverter.ToGregorianDateTime
                    (row["Date"].ToString().Trim());
                TimeSpan tempSpan = currentDate - tempDate;
                if (minSpan == null || minSpan > tempSpan)
                {
                    minSpan = tempSpan;
                    rowNearest = row;
                }              
            }

            if (rowNearest != null)
            {
                string date2 = rowNearest["Date"];

                //////////////////////////////////////////

                MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                MaxDS = new DataSet();
                Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;

                // Insert into FinalForcast table
                MyCom.CommandText =
                    "select Date,DateEstimate from LoadForecasting where Date=" + date2.ToString().Trim();
                Maxda.Fill(MaxDS);


                DataRow maxDateRow = null;
                string maxDate = null;

                foreach (DataRow row in MaxDS.Tables[0])
                {
                    string temp = row["DateEstimate"].ToString().Trim();
                    if (maxDate == null || maxDate < temp)
                    {
                        maxDate = temo;
                        maxDateRow = row;
                    }
                }

                return maxDateRow["Date"].ToString().Trim();
            }

            return "";
        }
   
    }
}
