﻿namespace PowerPlantProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.errormessage = new System.Windows.Forms.Label();
            this.powerPalntDBDataSet11 = new PowerPlantProject.PowerPalntDBDataSet1();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.UserTxt = new System.Windows.Forms.TextBox();
            this.PassTxt = new System.Windows.Forms.TextBox();
            this.LoginBtn = new System.Windows.Forms.Button();
            this.ValidUser = new System.Windows.Forms.Label();
            this.ValidPass = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.powerPalntDBDataSet11)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // errormessage
            // 
            this.errormessage.AutoSize = true;
            this.errormessage.BackColor = System.Drawing.Color.White;
            this.errormessage.ForeColor = System.Drawing.Color.Red;
            this.errormessage.Location = new System.Drawing.Point(115, 235);
            this.errormessage.Name = "errormessage";
            this.errormessage.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.errormessage.Size = new System.Drawing.Size(0, 13);
            this.errormessage.TabIndex = 1;
            // 
            // powerPalntDBDataSet11
            // 
            this.powerPalntDBDataSet11.DataSetName = "PowerPalntDBDataSet1";
            this.powerPalntDBDataSet11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Azure;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 21);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(77, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(25, 63);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(74, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Password :";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // UserTxt
            // 
            this.UserTxt.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.UserTxt.Location = new System.Drawing.Point(109, 20);
            this.UserTxt.Name = "UserTxt";
            this.UserTxt.Size = new System.Drawing.Size(100, 20);
            this.UserTxt.TabIndex = 1;
            // 
            // PassTxt
            // 
            this.PassTxt.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.PassTxt.Location = new System.Drawing.Point(109, 58);
            this.PassTxt.Name = "PassTxt";
            this.PassTxt.Size = new System.Drawing.Size(100, 20);
            this.PassTxt.TabIndex = 2;
            this.PassTxt.UseSystemPasswordChar = true;
            // 
            // LoginBtn
            // 
            this.LoginBtn.BackColor = System.Drawing.Color.Azure;
            this.LoginBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LoginBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoginBtn.Location = new System.Drawing.Point(91, 105);
            this.LoginBtn.Name = "LoginBtn";
            this.LoginBtn.Size = new System.Drawing.Size(75, 29);
            this.LoginBtn.TabIndex = 3;
            this.LoginBtn.Text = "Login";
            this.LoginBtn.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LoginBtn.UseVisualStyleBackColor = false;
            this.LoginBtn.Click += new System.EventHandler(this.LoginBtn_Click);
            // 
            // ValidUser
            // 
            this.ValidUser.AutoSize = true;
            this.ValidUser.BackColor = System.Drawing.Color.Azure;
            this.ValidUser.ForeColor = System.Drawing.Color.Red;
            this.ValidUser.Location = new System.Drawing.Point(215, 23);
            this.ValidUser.Name = "ValidUser";
            this.ValidUser.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ValidUser.Size = new System.Drawing.Size(11, 13);
            this.ValidUser.TabIndex = 4;
            this.ValidUser.Text = "*";
            this.ValidUser.Visible = false;
            // 
            // ValidPass
            // 
            this.ValidPass.AutoSize = true;
            this.ValidPass.BackColor = System.Drawing.Color.Azure;
            this.ValidPass.ForeColor = System.Drawing.Color.Red;
            this.ValidPass.Location = new System.Drawing.Point(215, 65);
            this.ValidPass.Name = "ValidPass";
            this.ValidPass.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ValidPass.Size = new System.Drawing.Size(11, 13);
            this.ValidPass.TabIndex = 5;
            this.ValidPass.Text = "*";
            this.ValidPass.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Azure;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.ValidPass);
            this.panel1.Controls.Add(this.ValidUser);
            this.panel1.Controls.Add(this.LoginBtn);
            this.panel1.Controls.Add(this.PassTxt);
            this.panel1.Controls.Add(this.UserTxt);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.panel1.Location = new System.Drawing.Point(95, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(247, 147);
            this.panel1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AcceptButton = this.LoginBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PowerPlantProject.Properties.Resources._10;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(443, 294);
            this.Controls.Add(this.errormessage);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(200, 200);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.powerPalntDBDataSet11)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label errormessage;
        private PowerPalntDBDataSet1 powerPalntDBDataSet11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox UserTxt;
        private System.Windows.Forms.TextBox PassTxt;
        private System.Windows.Forms.Button LoginBtn;
        private System.Windows.Forms.Label ValidUser;
        private System.Windows.Forms.Label ValidPass;
        private System.Windows.Forms.Panel panel1;
    }
}

