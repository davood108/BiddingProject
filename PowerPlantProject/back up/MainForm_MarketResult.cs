﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.DMS.Common;

namespace PowerPlantProject
{
    partial class MainForm
    {
        //----------------------------FillMRVlues------------------------------------
        private void FillMRVlues(string Num)
        {
            //Clear Market Result Tab
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            MRPlantOnUnitTb.Text = "";
            MRPlantPowerTb.Text = "";

            //detect current date and hour
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");
            int myhour = DateTime.Now.Hour;

            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;


            //set OnUnits & Power TextBox
            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = new SqlConnection(ConnectionManager.ConnectionString);
            MyCom.Connection.Open();

            MyCom.CommandText = "SELECT @re1=COUNT(DISTINCT Block), @re2=SUM (Required) FROM [DetailFRM005] WHERE" +
            "  TargetMarketDate=@date AND PPID=@pid AND Hour=@hour AND Required <> 0";
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = mydate;
            MyCom.Parameters.Add("@pid", SqlDbType.NChar, 10);
            MyCom.Parameters["@pid"].Value = Num;
            MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
            MyCom.Parameters["@hour"].Value = myhour;
            MyCom.Parameters.Add("@re1", SqlDbType.Int);
            MyCom.Parameters["@re1"].Direction = ParameterDirection.Output;
            MyCom.Parameters.Add("@re2", SqlDbType.Real);
            MyCom.Parameters["@re2"].Direction = ParameterDirection.Output;
            MyCom.ExecuteNonQuery();
            MyCom.Connection.Close();

            MRPlantOnUnitTb.Text = MyCom.Parameters["@re1"].Value.ToString();
            if (MyCom.Parameters["@re2"].Value.ToString() != "")
                MRPlantPowerTb.Text = MyCom.Parameters["@re2"].Value.ToString();
            else MRPlantPowerTb.Text = "0";

            //Set GridViews
            if (!MRCal.IsNull)
                FillMRPlantGrid();
            //{
            //    DataSet MyDS = new DataSet();
            //    SqlDataAdapter Myda = new SqlDataAdapter();

            //    Myda.SelectCommand = new SqlCommand("SELECT Hour,SUM(Required) AS SR FROM [DetailFRM005] WHERE PPID= "+Num+" AND TargetMarketDate=@date GROUP BY Hour", MyConnection);
            //    Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
            //    Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
            //    Myda.Fill(MyDS);
            //    MRCurGrid1.Rows[0].Cells[0].Value = "Power";
            //    MRCurGrid2.Rows[0].Cells[0].Value = "Power";
            //    foreach (DataRow MyRow in MyDS.Tables[0].Rows)
            //    {
            //        int index=int.Parse(MyRow["Hour"].ToString());
            //        if (index <= 12)
            //            MRCurGrid1.Rows[0].Cells[index].Value = MyRow["SR"];
            //        else
            //            MRCurGrid2.Rows[0].Cells[index - 12].Value = MyRow["SR"];
            //    }
            // }

        }
        //------------------------------FillMRTransmission-------------------------
        private void FillMRTransmission(string TransType)
        {
            //Clear Market Result Tab
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            MRPlantOnUnitTb.Text = "";
            MRPlantPowerTb.Text = "";
            if (!MRCal.IsNull)
            {
                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                SqlCommand MyCom = new SqlCommand();
                MyCom.CommandText = "SELECT  @result1=SUM(Hour1+Hour2+Hour3+Hour4+Hour5+Hour6+Hour7+Hour8+Hour9+Hour10+" +
                "Hour11+Hour12+Hour13+Hour14+Hour15+Hour16+Hour17+Hour18+Hour19+Hour20+Hour21+Hour22+Hour23+Hour24) " +
                "FROM InterchangedEnergy INNER JOIN TransLine ON InterchangedEnergy.Code = TransLine.LineCode WHERE " +
                "InterchangedEnergy.Date=@date AND TransLine.LineNumber=@trans AND TransLine.Type=-1" +
                "SELECT  @result2=SUM(Hour1+Hour2+Hour3+Hour4+Hour5+Hour6+Hour7+Hour8+Hour9+Hour10+" +
                "Hour11+Hour12+Hour13+Hour14+Hour15+Hour16+Hour17+Hour18+Hour19+Hour20+Hour21+Hour22+Hour23+Hour24) " +
                "FROM InterchangedEnergy INNER JOIN TransLine ON InterchangedEnergy.Code = TransLine.LineCode WHERE " +
                "InterchangedEnergy.Date=@date AND TransLine.LineNumber=@trans AND TransLine.Type=1";
                MyCom.Connection = myConnection;

                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = MRCal.Text;
                MyCom.Parameters.Add("@trans", SqlDbType.Int);
                MyCom.Parameters["@trans"].Value = int.Parse(TransType);

                MyCom.Parameters.Add("@result1", SqlDbType.Real);
                MyCom.Parameters["@result1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@result2", SqlDbType.Real);
                MyCom.Parameters["@result2"].Direction = ParameterDirection.Output;
                //try
                //{
                MyCom.ExecuteNonQuery();
                //}
                //catch(Exception e)
                //{
                //}
                int thereis = 0;
                try
                {
                    double re1 = double.Parse(MyCom.Parameters["@result1"].Value.ToString());
                    if (re1 < 0) re1 = re1 * (-1);
                    MRPlantOnUnitTb.Text = re1.ToString();
                }
                catch
                {
                    thereis++;
                    MRPlantOnUnitTb.Text = "0";
                }
                try
                {
                    double re2 = double.Parse(MyCom.Parameters["@result2"].Value.ToString());
                    if (re2 < 0) re2 = re2 * (-1);
                    MRPlantPowerTb.Text = re2.ToString();
                }
                catch
                {
                    MRPlantPowerTb.Text = "0";
                    thereis++;
                }
                //if (thereis == 2)
                ////    if MRCal.s
                // MessageBox.Show("There is no Data for selected Date!");
                //else
                if (thereis != 2)
                {
                    DataSet MyDS = new DataSet();
                    SqlDataAdapter Myda = new SqlDataAdapter();
                    Myda.SelectCommand = new SqlCommand("SELECT  SUM((TransLine.Type)*Hour1),SUM((TransLine.Type)*Hour2)," +
                    "SUM((TransLine.Type)*Hour3),SUM((TransLine.Type)*Hour4),SUM((TransLine.Type)*Hour5),SUM((TransLine.Type)*Hour6)," +
                    "SUM((TransLine.Type)*Hour7),SUM((TransLine.Type)*Hour8),SUM((TransLine.Type)*Hour9),SUM((TransLine.Type)*Hour10)," +
                    "SUM((TransLine.Type)*Hour11),SUM((TransLine.Type)*Hour12),SUM((TransLine.Type)*Hour13),SUM((TransLine.Type)*Hour14)," +
                    "SUM((TransLine.Type)*Hour15),SUM((TransLine.Type)*Hour16),SUM((TransLine.Type)*Hour17),SUM((TransLine.Type)*Hour18)," +
                    "SUM((TransLine.Type)*Hour19),SUM((TransLine.Type)*Hour20),SUM((TransLine.Type)*Hour21),SUM((TransLine.Type)*Hour22)," +
                    "SUM((TransLine.Type)*Hour23),SUM((TransLine.Type)*Hour24) FROM InterchangedEnergy INNER JOIN TransLine " +
                    "ON InterchangedEnergy.Code = TransLine.LineCode WHERE InterchangedEnergy.Date=@date AND TransLine.LineNumber=@trans", myConnection);
                    Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                    Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                    Myda.SelectCommand.Parameters.Add("@trans", SqlDbType.Int);
                    Myda.SelectCommand.Parameters["@trans"].Value = int.Parse(TransType);
                    Myda.Fill(MyDS);

                    MRCurGrid1.Rows[0].Cells[0].Value = "Power";
                    MRCurGrid2.Rows[0].Cells[0].Value = "Power";
                    foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                    {
                        for (int i = 0; i < 24; i++)
                            if (i < 12)
                                MRCurGrid1.Rows[0].Cells[i + 1].Value = MyRow[i].ToString();
                            else
                                MRCurGrid2.Rows[0].Cells[i - 11].Value = MyRow[i].ToString();
                    }

                }
                myConnection.Close();
            }
        }
        //----------------------------FillMRLine------------------------------------
        private void FillMRLine(string TransType, string TransLine)
        {
            //Clear Market Result Tab
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            MRPlantOnUnitTb.Text = "";
            MRPlantPowerTb.Text = "";
            if (!MRCal.IsNull)
            {
                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                SqlCommand MyCom = new SqlCommand();
                MyCom.CommandText = "SELECT  @result=(Hour1+Hour2+Hour3+Hour4+Hour5+Hour6+Hour7+Hour8+Hour9+Hour10+" +
                "Hour11+Hour12+Hour13+Hour14+Hour15+Hour16+Hour17+Hour18+Hour19+Hour20+Hour21+Hour22+Hour23+Hour24), " +
                "@type=TransLine.Type FROM InterchangedEnergy INNER JOIN TransLine ON InterchangedEnergy.Code = " +
                "TransLine.LineCode WHERE InterchangedEnergy.Date=@date AND TransLine.LineNumber=@trans AND " +
                "InterchangedEnergy.Code =@code";
                MyCom.Connection = myConnection;

                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = MRCal.Text;
                MyCom.Parameters.Add("@trans", SqlDbType.Int);
                MyCom.Parameters["@trans"].Value = int.Parse(TransType);
                MyCom.Parameters.Add("@code", SqlDbType.NChar, 20);
                MyCom.Parameters["@code"].Value = TransLine;

                MyCom.Parameters.Add("@result", SqlDbType.Real);
                MyCom.Parameters["@result"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@type", SqlDbType.Int);
                MyCom.Parameters["@type"].Direction = ParameterDirection.Output;
                //try
                //{
                MyCom.ExecuteNonQuery();
                //}
                //catch(Exception e)
                //{
                //}
                int thereis = 0, type = 0;
                try
                {
                    type = int.Parse(MyCom.Parameters["@type"].Value.ToString());
                }
                catch
                {
                    thereis++;
                    //MessageBox.Show("There is no Data for selected Date!");
                }
                if (thereis == 0)
                {
                    try
                    {
                        double re = double.Parse(MyCom.Parameters["@result"].Value.ToString());
                        if (re < 0) re = re * (-1);
                        if (type == -1)
                            MRPlantOnUnitTb.Text = re.ToString();
                        else
                            MRPlantPowerTb.Text = re.ToString();
                    }
                    catch
                    {
                        MRPlantPowerTb.Text = "0";
                        MRPlantOnUnitTb.Text = "0";
                    }

                    DataSet MyDS = new DataSet();
                    SqlDataAdapter Myda = new SqlDataAdapter();
                    Myda.SelectCommand = new SqlCommand("SELECT  (TransLine.Type)*Hour1,(TransLine.Type)*Hour2," +
                    "(TransLine.Type)*Hour3,(TransLine.Type)*Hour4,(TransLine.Type)*Hour5,(TransLine.Type)*Hour6," +
                    "(TransLine.Type)*Hour7,(TransLine.Type)*Hour8,(TransLine.Type)*Hour9,(TransLine.Type)*Hour10," +
                    "(TransLine.Type)*Hour11,(TransLine.Type)*Hour12,(TransLine.Type)*Hour13,(TransLine.Type)*Hour14," +
                    "(TransLine.Type)*Hour15,(TransLine.Type)*Hour16,(TransLine.Type)*Hour17,(TransLine.Type)*Hour18," +
                    "(TransLine.Type)*Hour19,(TransLine.Type)*Hour20,(TransLine.Type)*Hour21,(TransLine.Type)*Hour22," +
                    "(TransLine.Type)*Hour23,(TransLine.Type)*Hour24 FROM InterchangedEnergy INNER JOIN TransLine " +
                    "ON InterchangedEnergy.Code = TransLine.LineCode WHERE InterchangedEnergy.Date=@date AND " +
                    "TransLine.LineNumber=@trans AND InterchangedEnergy.Code =@code", myConnection);
                    Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                    Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                    Myda.SelectCommand.Parameters.Add("@trans", SqlDbType.Int);
                    Myda.SelectCommand.Parameters["@trans"].Value = int.Parse(TransType);
                    Myda.SelectCommand.Parameters.Add("@code", SqlDbType.NChar, 20);
                    Myda.SelectCommand.Parameters["@code"].Value = TransLine;
                    Myda.Fill(MyDS);

                    MRCurGrid1.Rows[0].Cells[0].Value = "Power";
                    MRCurGrid2.Rows[0].Cells[0].Value = "Power";
                    foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                    {
                        for (int i = 0; i < 24; i++)
                            if (i < 12)
                                MRCurGrid1.Rows[0].Cells[i + 1].Value = MyRow[i].ToString();
                            else
                                MRCurGrid2.Rows[0].Cells[i - 11].Value = MyRow[i].ToString();
                    }
                }
                myConnection.Close();
            }
        }
        //----------------------------FillMRUnit-------------------------------------
        private void FillMRUnit(string unit, string package, int index)
        {
            //Clear Market Result Tab
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            MRUnitStateTb.Text = "";
            MRUnitPowerTb.Text = "";
            MRUnitMaxBidTb.Text = "";

            //Detect Farsi Date
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");
            int myhour = DateTime.Now.Hour;

            //Detect Block For FRM005
            string temp = unit;
            temp = temp.ToLower();
            if (package.Contains("Combined"))
            {
                int x = PPID;
                if ((x == 131) || (x == 144)) x++;
                string packagecode = "";
                if (GDSteamGroup.Text.Contains(package))
                    packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                else packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                temp = x + "-" + "C" + packagecode;
            }
            else if (temp.Contains("gas"))
            {
                temp = temp.Replace("gas", "G");
                temp = PPID + "-" + temp;
            }
            else
            {
                temp = temp.Replace("steam", "S");
                temp = PPID + "-" + temp;
            }

            MRUnitStateTb.Text = GDStateTB.Text;
            if (MRUnitStateTb.Text == "ON")
            {
                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = myConnection;
                MyCom.CommandText = "SELECT @re=Required FROM [DetailFRM005] WHERE TargetMarketDate=@date " +
                "AND PPID=@num AND Block=@block AND Hour=@hour ";
                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = mydate;
                MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                MyCom.Parameters["@num"].Value = PPID;
                MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                MyCom.Parameters["@block"].Value = temp;
                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                MyCom.Parameters["@hour"].Value = myhour;
                MyCom.Parameters.Add("@re", SqlDbType.Real);
                MyCom.Parameters["@re"].Direction = ParameterDirection.Output;
                //try
                //{
                MyCom.ExecuteNonQuery();
                //}
                //catch (Exception exp)
                //{
                //  string str = exp.Message;
                //}
                MRUnitPowerTb.Text = MyCom.Parameters["@re"].Value.ToString();

                //Detect MaxBid field
                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10 FROM [DetailFRM002] " +
                "WHERE TargetMarketDate=@date AND Block=@ptype AND Hour=@hour AND PPID=" + PPID, myConnection);
                Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Myda.SelectCommand.Parameters["@date"].Value = mydate;
                Myda.SelectCommand.Parameters.Add("@hour", SqlDbType.SmallInt);
                Myda.SelectCommand.Parameters["@hour"].Value = myhour;
                //Detect Block For FRM002
                string ptype = unit;
                ptype = ptype.ToLower();
                if (package.Contains("Combined"))
                {
                    string packagecode = "";
                    if (GDGasGroup.Text.Contains("Combined"))
                        packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                    else packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                    ptype = "C" + packagecode;
                }
                else
                {
                    if (ptype.Contains("gas"))
                        ptype = ptype.Replace("gas", "G");
                    else if (ptype.Contains("steam"))
                        ptype = ptype.Replace("steam", "S");
                }
                ptype = ptype.Trim();
                Myda.SelectCommand.Parameters.Add("@ptype", SqlDbType.NChar, 20);
                Myda.SelectCommand.Parameters["@ptype"].Value = ptype;
                Myda.Fill(MyDS);
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    double required = double.Parse(MRUnitPowerTb.Text);
                    int Dsindex = 0;
                    while ((Dsindex < 20) && (double.Parse(MyRow[Dsindex].ToString()) < required))
                        Dsindex = Dsindex + 2;
                    if (Dsindex < 20)
                        MRUnitMaxBidTb.Text = MyRow[Dsindex + 1].ToString();
                    else MRUnitMaxBidTb.Text = "0";
                }
                MyCom.CommandText = "UPDATE DetailFRM005 SET MaxBid=@maxbid WHERE TargetMarketDate=@date AND " +
                "PPID=@num AND Block=@block AND Hour=@hour";
                MyCom.Parameters.Add("@maxbid", SqlDbType.Real);
                MyCom.Parameters["@maxbid"].Value = double.Parse(MRUnitMaxBidTb.Text);
                //try
                //{
                MyCom.ExecuteNonQuery();
                //}
                //catch (Exception exp)
                //{
                //  string str = exp.Message;
                //}

                myConnection.Close();
            }


            if (!MRCal.IsNull)
                FillMRUnitGrid();
        }
        //--------------------------------------FillMRUnitGrid()------------------------------------------
        private void FillMRUnitGrid()
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            if (!MRCal.IsNull)
            {
                string package = MRPackLb.Text.Trim();
                string unit = MRUnitLb.Text.Trim();
                //Get Index Of Selected Unit(Row) in GridView (In Generad data Tab)
                int index = 0;
                if (GDSteamGroup.Text.Contains(package))
                    for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                    {
                        if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                            index = i;
                    }
                else
                    for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                    {
                        if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                            index = i;
                    }

                //Detect Block For FRM005
                string temp = unit;
                temp = temp.ToLower();
                if (package.Contains("Combined"))
                {
                    int x = PPID;
                    if ((x == 131) || (x == 144)) x++;
                    string packagecode = "";
                    if (GDSteamGroup.Text.Contains(package))
                        packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                    else packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                    temp = x + "-" + "C" + packagecode;
                }
                else if (temp.Contains("gas"))
                {
                    temp = temp.Replace("gas", "G");
                    temp = PPID + "-" + temp;
                }
                else
                {
                    temp = temp.Replace("steam", "S");
                    temp = PPID + "-" + temp;
                }

                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();

                Myda.SelectCommand = new SqlCommand("SELECT Hour,Required,Dispatchable,Contribution FROM [DetailFRM005] WHERE PPID= " + PPID + " AND TargetMarketDate=@date AND Block=@block", myConnection);
                Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                Myda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
                Myda.SelectCommand.Parameters["@block"].Value = temp;
                Myda.Fill(MyDS);
                MRCurGrid1.DataSource = MyDS.Tables[0].DefaultView;
                MRCurGrid2.DataSource = MyDS.Tables[0].DefaultView;
                MRCurGrid1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                MRCurGrid2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                if (MRCurGrid1.RowCount > 1)
                {
                    MRCurGrid1.Rows[0].Cells[0].Value = "Required";
                    MRCurGrid2.Rows[0].Cells[0].Value = "Required";
                    MRCurGrid1.Rows[1].Cells[0].Value = "Dispatchable";
                    MRCurGrid2.Rows[1].Cells[0].Value = "Dispatchable";
                    MRCurGrid1.Rows[2].Cells[0].Value = "Contribution";
                    MRCurGrid2.Rows[2].Cells[0].Value = "Conrtibution";
                    MRCurGrid1.Rows[3].Cells[0].Value = "Price";
                    MRCurGrid2.Rows[3].Cells[0].Value = "Price";
                    //???????????????Price????????????????????
                    foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                    {
                        int x = int.Parse(MyRow["Hour"].ToString());
                        if (x <= 12)
                        {
                            MRCurGrid1.Rows[0].Cells[x].Value = MyRow["Required"];
                            MRCurGrid1.Rows[1].Cells[x].Value = MyRow["Dispatchable"];
                            MRCurGrid1.Rows[2].Cells[x].Value = MyRow["Contribution"];
                        }
                        else
                        {
                            MRCurGrid2.Rows[0].Cells[x - 12].Value = MyRow["Required"];
                            MRCurGrid2.Rows[1].Cells[x - 12].Value = MyRow["Dispatchable"];
                            MRCurGrid2.Rows[2].Cells[x - 12].Value = MyRow["Contribution"];
                        }
                    }
                    for (int i = (MRCurGrid1.RowCount - 2); i > 3; i--)
                        MRCurGrid1.Rows.Remove(MRCurGrid1.Rows[i]);
                    for (int i = (MRCurGrid2.RowCount - 2); i > 3; i--)
                        MRCurGrid2.Rows.Remove(MRCurGrid2.Rows[i]);

                    //Detect MaxBid field in MRCurGrid1
                    foreach (DataGridViewColumn DC in MRCurGrid1.Columns)
                    {
                        if (DC.Index > 0)
                        {

                            DataSet MaxDS = new DataSet();
                            SqlDataAdapter Maxda = new SqlDataAdapter();
                            Maxda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                            "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10 FROM [DetailFRM002] " +
                            "WHERE TargetMarketDate=@date AND Block=@ptype AND Hour=@hour AND PPID=" + PPID, myConnection);
                            Maxda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                            Maxda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                            Maxda.SelectCommand.Parameters.Add("@hour", SqlDbType.SmallInt);
                            Maxda.SelectCommand.Parameters["@hour"].Value = DC.Index;
                            //Detect Block For FRM002
                            string ptype = unit;
                            ptype = ptype.ToLower();
                            if (package.Contains("Combined"))
                            {
                                string packagecode = "";
                                if (GDGasGroup.Text.Contains("Combined"))
                                    packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                                else packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                                ptype = "C" + packagecode;
                            }
                            else
                            {
                                if (ptype.Contains("gas"))
                                    ptype = ptype.Replace("gas", "G");
                                else if (ptype.Contains("steam"))
                                    ptype = ptype.Replace("steam", "S");
                            }
                            ptype = ptype.Trim();
                            Maxda.SelectCommand.Parameters.Add("@ptype", SqlDbType.NChar, 20);
                            Maxda.SelectCommand.Parameters["@ptype"].Value = ptype;
                            Maxda.Fill(MaxDS);

                            SqlCommand MyCom = new SqlCommand();
                            MyCom.Connection = myConnection;
                            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                            MyCom.Parameters["@date"].Value = MRCal.Text;
                            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                            MyCom.Parameters["@num"].Value = PPID.ToString();
                            MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                            MyCom.Parameters["@block"].Value = temp;
                            MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                            MyCom.Parameters.Add("@mb", SqlDbType.Real);

                            foreach (DataRow MaxRow in MaxDS.Tables[0].Rows)
                            {
                                double required = double.Parse(MRCurGrid1.Rows[0].Cells[DC.Index].Value.ToString());
                                int Dsindex = 0;
                                while ((Dsindex < 20) && (double.Parse(MaxRow[Dsindex].ToString()) < required)) Dsindex = Dsindex + 2;
                                if (Dsindex < 20)
                                    MRCurGrid1.Rows[3].Cells[DC.Index].Value = MaxRow[Dsindex + 1].ToString();
                                else MRCurGrid1.Rows[3].Cells[DC.Index].Value = "";

                                //Insert MaxBid Into DetailFRM005
                                MyCom.CommandText = "UPDATE DetailFRM005 SET MaxBid=@mb WHERE TargetMarketDate=@date AND PPID=@num " +
                                "AND Block=@block AND Hour=@hour";
                                MyCom.Parameters["@hour"].Value = DC.Index;
                                try
                                {
                                    MyCom.Parameters["@mb"].Value = double.Parse(MRCurGrid1.Rows[3].Cells[DC.Index].Value.ToString());
                                }
                                catch (Exception e)
                                {
                                    MyCom.Parameters["@mb"].Value = "0";
                                }
                                try
                                {
                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception e)
                                {
                                    string a = e.Message;
                                }
                            }
                        }
                    }

                    //Detect MaxBid field in MRCurGrid2
                    foreach (DataGridViewColumn DC in MRCurGrid2.Columns)
                    {
                        if (DC.Index > 0)
                        {
                            DataSet MaxDS = new DataSet();
                            SqlDataAdapter Maxda = new SqlDataAdapter();
                            Maxda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                            "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10 FROM [DetailFRM002] " +
                            "WHERE TargetMarketDate=@date AND Block=@ptype AND Hour=@hour AND PPID=" + PPID, myConnection);
                            Maxda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                            Maxda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                            Maxda.SelectCommand.Parameters.Add("@hour", SqlDbType.SmallInt);
                            Maxda.SelectCommand.Parameters["@hour"].Value = DC.Index + 12;
                            //Detect Block For FRM002
                            string ptype = unit;
                            ptype = ptype.ToLower();
                            if (package.Contains("Combined"))
                            {
                                string packagecode = "";
                                if (GDGasGroup.Text.Contains("Combined"))
                                    packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                                else packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                                ptype = "C" + packagecode;
                            }
                            else
                            {
                                if (ptype.Contains("gas"))
                                    ptype = ptype.Replace("gas", "G");
                                else if (ptype.Contains("steam"))
                                    ptype = ptype.Replace("steam", "S");
                            }
                            ptype = ptype.Trim();
                            Maxda.SelectCommand.Parameters.Add("@ptype", SqlDbType.NChar, 20);
                            Maxda.SelectCommand.Parameters["@ptype"].Value = ptype;
                            Maxda.Fill(MaxDS);

                            SqlCommand MyCom = new SqlCommand();
                            MyCom.Connection = myConnection;
                            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                            MyCom.Parameters["@date"].Value = MRCal.Text;
                            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                            MyCom.Parameters["@num"].Value = PPID.ToString();
                            MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                            MyCom.Parameters["@block"].Value = temp;
                            MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                            MyCom.Parameters.Add("@mb", SqlDbType.Real);

                            foreach (DataRow MaxRow in MaxDS.Tables[0].Rows)
                            {
                                double required = double.Parse(MRCurGrid2.Rows[0].Cells[DC.Index].Value.ToString());
                                int Dsindex = 0;
                                while ((Dsindex < 20) && (double.Parse(MaxRow[Dsindex].ToString()) < required)) Dsindex = Dsindex + 2;
                                if (Dsindex < 20)
                                    MRCurGrid2.Rows[3].Cells[DC.Index].Value = MaxRow[Dsindex + 1].ToString();
                                else MRCurGrid2.Rows[3].Cells[DC.Index].Value = "";

                                //Insert MaxBid Into DetailFRM005
                                MyCom.CommandText = "UPDATE DetailFRM005 SET MaxBid=@mb WHERE TargetMarketDate=@date AND PPID=@num " +
                                "AND Block=@block AND Hour=@hour";
                                MyCom.Parameters["@hour"].Value = DC.Index + 12;
                                try
                                {
                                    MyCom.Parameters["@mb"].Value = double.Parse(MRCurGrid2.Rows[3].Cells[DC.Index].Value.ToString());
                                }
                                catch (Exception e)
                                {
                                    MyCom.Parameters["@mb"].Value = "0";
                                }
                                try
                                {
                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception e)
                                {
                                    string a = e.Message;
                                }
                            }
                        }
                    }
                }
                //else MessageBox.Show("No Data for selected Date!");
            }
            myConnection.Close();
        }
        //---------------------------------FillMRPlantGrid--------------------------------
        private void FillMRPlantGrid()
        {
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            if (!MRCal.IsNull)
            {

                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                Myda.SelectCommand = new SqlCommand("SELECT Hour,SUM(Required) AS SR FROM [DetailFRM005] WHERE PPID= " + PPID + " AND TargetMarketDate=@date GROUP BY Hour", myConnection);
                Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Myda.SelectCommand.Parameters["@date"].Value = MRCal.Text;
                Myda.Fill(MyDS);
                myConnection.Close();

                MRCurGrid1.Rows[0].Cells[0].Value = "Power";
                MRCurGrid2.Rows[0].Cells[0].Value = "Power";
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    int index = int.Parse(MyRow["Hour"].ToString());
                    if (index <= 12)
                        MRCurGrid1.Rows[0].Cells[index].Value = MyRow["SR"];
                    else
                        MRCurGrid2.Rows[0].Cells[index - 12].Value = MyRow["SR"];
                }
            }
        }


    }
}