﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using NRI.DMS.Common;
using System.Security.Cryptography;

namespace PowerPlantProject
{
    public partial class Form1 : Form
    {
        MainForm secondpage = new MainForm();
        public Form1()
        {
            InitializeComponent();
            CenterForm();
        }
        private void CenterForm()
        {
            Point center = new Point();
            center.X = (Screen.PrimaryScreen.WorkingArea.Width - Width) / 2;
            center.Y = (Screen.PrimaryScreen.WorkingArea.Height - Height) / 2;

            Location = center;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void LoginBtn_Click(object sender, EventArgs e)
        {
            string caption = "ورود به سيستم";
            string message = "";
            if ((UserTxt.Text != "") && (PassTxt.Text != ""))
            {
                errormessage.Text = "Please Fill the Blank!";
                if (UserTxt.Text == "") ValidUser.Visible = true; else ValidUser.Visible = false;
                if (PassTxt.Text == "") ValidPass.Visible = true; else ValidPass.Visible = false;

            }

            else
            {
                bool success = VerifyUser();
                if (success)
                {
                    secondpage.Show();
                    this.Hide();
                }
            }
            return;

        }


        private bool VerifyUser()
        {

  
            ValidUser.Visible = false;
            ValidPass.Visible = false;
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            SqlCommand MyCom = new SqlCommand();
            MyCom.CommandText = "SELECT  @result1 =count(Username) FROM [login] WHERE Username=@username AND Password=@password SELECT  @result2=count(Username) FROM [login] WHERE Username=@username AND  Password!=@password";
            MyCom.Connection = myConnection;

            MyCom.Parameters.Add("@username", SqlDbType.NVarChar, 50);
            MyCom.Parameters["@username"].Value = UserTxt.Text;

            MyCom.Parameters.Add("@password", SqlDbType.NVarChar, 50);
            MyCom.Parameters["@password"].Value = EncryptParrword();

            MyCom.Parameters.Add("@result1", SqlDbType.Int);
            MyCom.Parameters["@result1"].Direction = ParameterDirection.Output;

            MyCom.Parameters.Add("@result2", SqlDbType.Int);
            MyCom.Parameters["@result2"].Direction = ParameterDirection.Output;

            MyCom.ExecuteNonQuery();
            myConnection.Close();


            int result1, result2;
            result1 = (int)MyCom.Parameters["@result1"].Value;
            result2 = (int)MyCom.Parameters["@result2"].Value;

            if (result1 == 1)
            {
                return true;

            }
            else
            {
                if (result2 == 1)
                {
                    errormessage.Text = "Invalid Password!";
                }
                else
                    errormessage.Text = "Invalid Username!";
                return false;
            }

        }

        private string EncryptParrword()
        {
            SHA1Managed sh = new SHA1Managed();
            byte[] hash = sh.ComputeHash(System.Text.UnicodeEncoding.Unicode.GetBytes(PassTxt.Text));

            string hashedPassword = "";
            foreach (byte b in hash)
                hashedPassword += b.ToString();
            return hashedPassword;
        }

        private void Exit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Application.Exit();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

 
    }
}
