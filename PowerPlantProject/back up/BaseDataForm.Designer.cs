﻿namespace PowerPlantProject
{
    partial class BaseDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.PmaxTb = new System.Windows.Forms.TextBox();
            this.PminTb = new System.Windows.Forms.TextBox();
            this.CapacityTb = new System.Windows.Forms.TextBox();
            this.HourTb = new System.Windows.Forms.TextBox();
            this.DayTb = new System.Windows.Forms.TextBox();
            this.GasTb = new System.Windows.Forms.TextBox();
            this.SteamTb = new System.Windows.Forms.TextBox();
            this.UpdateCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.CCTb = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.GasOilPriceTb = new System.Windows.Forms.TextBox();
            this.MazutPriceTb = new System.Windows.Forms.TextBox();
            this.GasPriceTb = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.LightCyan;
            this.label1.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label1.Location = new System.Drawing.Point(65, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Market Price Maximum :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.LightCyan;
            this.label2.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label2.Location = new System.Drawing.Point(65, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Market Price Minimum :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.LightCyan;
            this.label3.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label3.Location = new System.Drawing.Point(65, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Capacity Payment Per MW :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.LightCyan;
            this.label4.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label4.Location = new System.Drawing.Point(65, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Proposal Hour To Market :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.LightCyan;
            this.label5.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label5.Location = new System.Drawing.Point(65, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(158, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Proposal Day Delay To Market :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.LightCyan;
            this.label6.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label6.Location = new System.Drawing.Point(65, 177);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(154, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Gas Production Cost Minimum :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.LightCyan;
            this.label7.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label7.Location = new System.Drawing.Point(65, 209);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Steam Production Cost Minimum :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.LightCyan;
            this.label8.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label8.Location = new System.Drawing.Point(65, 239);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(149, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "CC Production Cost Minimum :";
            // 
            // PmaxTb
            // 
            this.PmaxTb.Location = new System.Drawing.Point(235, 20);
            this.PmaxTb.Name = "PmaxTb";
            this.PmaxTb.Size = new System.Drawing.Size(120, 20);
            this.PmaxTb.TabIndex = 1;
            this.PmaxTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.PmaxTb.Validated += new System.EventHandler(this.PmaxTb_Validated);
            // 
            // PminTb
            // 
            this.PminTb.Location = new System.Drawing.Point(235, 49);
            this.PminTb.Name = "PminTb";
            this.PminTb.Size = new System.Drawing.Size(120, 20);
            this.PminTb.TabIndex = 2;
            this.PminTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.PminTb.Validated += new System.EventHandler(this.PminTb_Validated);
            // 
            // CapacityTb
            // 
            this.CapacityTb.Location = new System.Drawing.Point(235, 79);
            this.CapacityTb.Name = "CapacityTb";
            this.CapacityTb.Size = new System.Drawing.Size(120, 20);
            this.CapacityTb.TabIndex = 3;
            this.CapacityTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.CapacityTb.Validated += new System.EventHandler(this.CapacityTb_Validated);
            // 
            // HourTb
            // 
            this.HourTb.Location = new System.Drawing.Point(235, 110);
            this.HourTb.Name = "HourTb";
            this.HourTb.Size = new System.Drawing.Size(120, 20);
            this.HourTb.TabIndex = 4;
            this.HourTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.HourTb.Validated += new System.EventHandler(this.HourTb_Validated);
            // 
            // DayTb
            // 
            this.DayTb.Location = new System.Drawing.Point(235, 142);
            this.DayTb.Name = "DayTb";
            this.DayTb.Size = new System.Drawing.Size(120, 20);
            this.DayTb.TabIndex = 5;
            this.DayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.DayTb.Validated += new System.EventHandler(this.DayTb_Validated);
            // 
            // GasTb
            // 
            this.GasTb.Location = new System.Drawing.Point(235, 174);
            this.GasTb.Name = "GasTb";
            this.GasTb.Size = new System.Drawing.Size(120, 20);
            this.GasTb.TabIndex = 6;
            this.GasTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GasTb.Validated += new System.EventHandler(this.GasTb_Validated);
            // 
            // SteamTb
            // 
            this.SteamTb.Location = new System.Drawing.Point(235, 206);
            this.SteamTb.Name = "SteamTb";
            this.SteamTb.Size = new System.Drawing.Size(120, 20);
            this.SteamTb.TabIndex = 7;
            this.SteamTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.SteamTb.Validated += new System.EventHandler(this.SteamTb_Validated);
            // 
            // UpdateCal
            // 
            this.UpdateCal.HasButtons = true;
            this.UpdateCal.Location = new System.Drawing.Point(235, 377);
            this.UpdateCal.Name = "UpdateCal";
            this.UpdateCal.Readonly = true;
            this.UpdateCal.Size = new System.Drawing.Size(120, 20);
            this.UpdateCal.TabIndex = 9;
            this.UpdateCal.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            // 
            // SaveBtn
            // 
            this.SaveBtn.BackColor = System.Drawing.Color.SkyBlue;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SaveBtn.Location = new System.Drawing.Point(148, 418);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(75, 23);
            this.SaveBtn.TabIndex = 10;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.UseVisualStyleBackColor = false;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // CCTb
            // 
            this.CCTb.Location = new System.Drawing.Point(235, 239);
            this.CCTb.Name = "CCTb";
            this.CCTb.Size = new System.Drawing.Size(120, 20);
            this.CCTb.TabIndex = 8;
            this.CCTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.CCTb.Validated += new System.EventHandler(this.CCTb_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.LightCyan;
            this.label9.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label9.Location = new System.Drawing.Point(65, 377);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 37;
            this.label9.Text = "Update Date :";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // GasOilPriceTb
            // 
            this.GasOilPriceTb.Location = new System.Drawing.Point(234, 339);
            this.GasOilPriceTb.Name = "GasOilPriceTb";
            this.GasOilPriceTb.Size = new System.Drawing.Size(120, 20);
            this.GasOilPriceTb.TabIndex = 43;
            this.GasOilPriceTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GasOilPriceTb.Validated += new System.EventHandler(this.GasOilPriceTb_Validated);
            // 
            // MazutPriceTb
            // 
            this.MazutPriceTb.Location = new System.Drawing.Point(234, 304);
            this.MazutPriceTb.Name = "MazutPriceTb";
            this.MazutPriceTb.Size = new System.Drawing.Size(120, 20);
            this.MazutPriceTb.TabIndex = 41;
            this.MazutPriceTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.MazutPriceTb.Validated += new System.EventHandler(this.MazutPriceTb_Validated);
            // 
            // GasPriceTb
            // 
            this.GasPriceTb.Location = new System.Drawing.Point(234, 271);
            this.GasPriceTb.Name = "GasPriceTb";
            this.GasPriceTb.Size = new System.Drawing.Size(120, 20);
            this.GasPriceTb.TabIndex = 40;
            this.GasPriceTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GasPriceTb.Validated += new System.EventHandler(this.GasPriceTb_Validated);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.LightCyan;
            this.label10.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label10.Location = new System.Drawing.Point(64, 339);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 13);
            this.label10.TabIndex = 42;
            this.label10.Text = "GasOil Price :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.LightCyan;
            this.label11.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label11.Location = new System.Drawing.Point(64, 307);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 13);
            this.label11.TabIndex = 39;
            this.label11.Text = "Mazut Price :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.LightCyan;
            this.label12.Image = global::PowerPlantProject.Properties.Resources._10;
            this.label12.Location = new System.Drawing.Point(64, 274);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 13);
            this.label12.TabIndex = 38;
            this.label12.Text = "Gas Price :";
            // 
            // Form6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PowerPlantProject.Properties.Resources._10;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(432, 452);
            this.Controls.Add(this.GasOilPriceTb);
            this.Controls.Add(this.MazutPriceTb);
            this.Controls.Add(this.GasPriceTb);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.CCTb);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.UpdateCal);
            this.Controls.Add(this.SteamTb);
            this.Controls.Add(this.GasTb);
            this.Controls.Add(this.DayTb);
            this.Controls.Add(this.HourTb);
            this.Controls.Add(this.CapacityTb);
            this.Controls.Add(this.PminTb);
            this.Controls.Add(this.PmaxTb);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "Form6";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Base Data";
            this.Load += new System.EventHandler(this.Form6_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox PmaxTb;
        private System.Windows.Forms.TextBox PminTb;
        private System.Windows.Forms.TextBox CapacityTb;
        private System.Windows.Forms.TextBox HourTb;
        private System.Windows.Forms.TextBox DayTb;
        private System.Windows.Forms.TextBox GasTb;
        private System.Windows.Forms.TextBox SteamTb;
        private FarsiLibrary.Win.Controls.FADatePicker UpdateCal;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.TextBox CCTb;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.TextBox GasOilPriceTb;
        private System.Windows.Forms.TextBox MazutPriceTb;
        private System.Windows.Forms.TextBox GasPriceTb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
    }
}