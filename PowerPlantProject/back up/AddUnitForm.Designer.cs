﻿namespace PowerPlantProject
{
    partial class AddUnitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PackageTypeValid = new System.Windows.Forms.Label();
            this.ODSaveBtn = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.PackageCodeTb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.PackageType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.UnitType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.unitNameTb = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // PackageTypeValid
            // 
            this.PackageTypeValid.AutoSize = true;
            this.PackageTypeValid.BackColor = System.Drawing.Color.PaleTurquoise;
            this.PackageTypeValid.ForeColor = System.Drawing.Color.Red;
            this.PackageTypeValid.Location = new System.Drawing.Point(5, 125);
            this.PackageTypeValid.Name = "PackageTypeValid";
            this.PackageTypeValid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PackageTypeValid.Size = new System.Drawing.Size(11, 13);
            this.PackageTypeValid.TabIndex = 33;
            this.PackageTypeValid.Text = "*";
            this.PackageTypeValid.Visible = false;
            // 
            // ODSaveBtn
            // 
            this.ODSaveBtn.BackColor = System.Drawing.Color.SkyBlue;
            this.ODSaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ODSaveBtn.Location = new System.Drawing.Point(102, 168);
            this.ODSaveBtn.Name = "ODSaveBtn";
            this.ODSaveBtn.Size = new System.Drawing.Size(75, 23);
            this.ODSaveBtn.TabIndex = 5;
            this.ODSaveBtn.Text = "Save";
            this.ODSaveBtn.UseVisualStyleBackColor = false;
            this.ODSaveBtn.Click += new System.EventHandler(this.ODSaveBtn_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SkyBlue;
            this.panel3.Controls.Add(this.PackageCodeTb);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(155, 100);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(118, 49);
            this.panel3.TabIndex = 31;
            // 
            // PackageCodeTb
            // 
            this.PackageCodeTb.BackColor = System.Drawing.Color.White;
            this.PackageCodeTb.Location = new System.Drawing.Point(6, 24);
            this.PackageCodeTb.Name = "PackageCodeTb";
            this.PackageCodeTb.Size = new System.Drawing.Size(99, 20);
            this.PackageCodeTb.TabIndex = 4;
            this.PackageCodeTb.Validated += new System.EventHandler(this.PackageCodeTb_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Window;
            this.label3.Location = new System.Drawing.Point(8, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Package Code";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Controls.Add(this.PackageType);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(17, 100);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(122, 49);
            this.panel2.TabIndex = 30;
            // 
            // PackageType
            // 
            this.PackageType.FormattingEnabled = true;
            this.PackageType.Items.AddRange(new object[] {
            "Combined Cycle",
            "Gas",
            "Steam"});
            this.PackageType.Location = new System.Drawing.Point(5, 23);
            this.PackageType.Name = "PackageType";
            this.PackageType.Size = new System.Drawing.Size(102, 21);
            this.PackageType.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Window;
            this.label2.Location = new System.Drawing.Point(7, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Package Type";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SkyBlue;
            this.panel1.Controls.Add(this.UnitType);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(155, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(118, 49);
            this.panel1.TabIndex = 29;
            // 
            // UnitType
            // 
            this.UnitType.FormattingEnabled = true;
            this.UnitType.Items.AddRange(new object[] {
            "Gas",
            "Steam"});
            this.UnitType.Location = new System.Drawing.Point(4, 21);
            this.UnitType.Name = "UnitType";
            this.UnitType.Size = new System.Drawing.Size(102, 21);
            this.UnitType.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Window;
            this.label1.Location = new System.Drawing.Point(22, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Unit Type";
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.SkyBlue;
            this.panel27.Controls.Add(this.unitNameTb);
            this.panel27.Controls.Add(this.label31);
            this.panel27.Location = new System.Drawing.Point(17, 30);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(122, 49);
            this.panel27.TabIndex = 28;
            // 
            // unitNameTb
            // 
            this.unitNameTb.BackColor = System.Drawing.Color.White;
            this.unitNameTb.Location = new System.Drawing.Point(6, 22);
            this.unitNameTb.Name = "unitNameTb";
            this.unitNameTb.Size = new System.Drawing.Size(99, 20);
            this.unitNameTb.TabIndex = 1;
            this.unitNameTb.Validated += new System.EventHandler(this.unitNameTb_Validated);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.SystemColors.Window;
            this.label31.Location = new System.Drawing.Point(22, 5);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(66, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "Unit Name";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PowerPlantProject.Properties.Resources._10;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(277, 206);
            this.Controls.Add(this.PackageTypeValid);
            this.Controls.Add(this.ODSaveBtn);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel27);
            this.MaximizeBox = false;
            this.Name = "Form4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add New Unit";
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label PackageTypeValid;
        private System.Windows.Forms.Button ODSaveBtn;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox PackageType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.TextBox unitNameTb;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox PackageCodeTb;
        private System.Windows.Forms.ComboBox UnitType;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}