﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using NRI.DMS.Common;

namespace PowerPlantProject
{
    public partial class PathForm : Form
    {
        string ConStr;
        public PathForm()
        {
            InitializeComponent();
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            if ((M002Tb.Text != "") || (M005Tb.Text != "") || (PriceTb.Text != "") || (LoadTb.Text != "") || (CapacityTb.Text != ""))
            {
                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                string command = "delete from path";
                SqlCommand MyCom = new SqlCommand(command, myConnection);
                MyCom.ExecuteNonQuery();

                command = "insert into path (M002,M005,AveragePrice,LoadForecasting,CapacityFactor,Proxy)" +
                    " Values (" +
                    "'" + M002Tb.Text.Trim()+"', " +
                    "'" + M005Tb.Text.Trim()+"', " +
                    "'" + PriceTb.Text.Trim()+"', " +
                    "'" + LoadTb.Text.Trim()+"', " +
                    "'" + CapacityTb.Text.Trim()+"', " +
                    "'" + M002Tb.Text.Trim()+"') ";

                MyCom = new SqlCommand(command, myConnection);
                MyCom.ExecuteNonQuery();

                //SqlCommand MyCom = new SqlCommand();
                //MyCom.Connection = MyConnection;
                //MyCom.CommandText = "SELECT @m2=M002,@m5=M005,@price=AveragePrice,@load=LoadForecasting,@capacity=CapacityFactor FROM [Path]";
                //MyCom.Parameters.Add("@m2", SqlDbType.NChar, 50);
                //MyCom.Parameters["@m2"].Direction = ParameterDirection.Output;
                //MyCom.Parameters.Add("@m5", SqlDbType.NChar, 50);
                //MyCom.Parameters["@m5"].Direction = ParameterDirection.Output;
                //MyCom.Parameters.Add("@price", SqlDbType.NChar, 50);
                //MyCom.Parameters["@price"].Direction = ParameterDirection.Output;
                //MyCom.Parameters.Add("@load", SqlDbType.NChar, 50);
                //MyCom.Parameters["@load"].Direction = ParameterDirection.Output;
                //MyCom.Parameters.Add("@capacity", SqlDbType.NChar, 50);
                //MyCom.Parameters["@capacity"].Direction = ParameterDirection.Output;
                //MyCom.ExecuteNonQuery();


                //string m2, m5, price, load1, capacity;
                //m2 = MyCom.Parameters["@m2"].Value.ToString();
                //m5 = MyCom.Parameters["@m5"].Value.ToString();
                //price = MyCom.Parameters["@price"].Value.ToString();
                //load1 = MyCom.Parameters["@load"].Value.ToString();
                //capacity = MyCom.Parameters["@capacity"].Value.ToString();
                //if (M002Tb.Text != "") m2 = M002Tb.Text;
                //if (M005Tb.Text != "") m5 = M005Tb.Text;
                //if (PriceTb.Text != "") price = PriceTb.Text;
                //if (LoadTb.Text != "") load1 = LoadTb.Text;
                //if (CapacityTb.Text != "") capacity = CapacityTb.Text;
                //MyCom.CommandText = "UPDATE Path SET M002=@m002,M005=@m005,AveragePrice=@price1,LoadForecasting=@load1,CapacityFactor=@capacity1";
                //MyCom.Parameters.Add("@m002", SqlDbType.NChar, 50);
                //MyCom.Parameters["@m002"].Value = m2;
                //MyCom.Parameters.Add("@m005", SqlDbType.NChar, 50);
                //MyCom.Parameters["@m005"].Value = m5;
                //MyCom.Parameters.Add("@price1", SqlDbType.NChar, 50);
                //MyCom.Parameters["@price1"].Value = price;
                //MyCom.Parameters.Add("@load1", SqlDbType.NChar, 50);
                //MyCom.Parameters["@load1"].Value = load1;
                //MyCom.Parameters.Add("@capacity1", SqlDbType.NChar, 50);
                //MyCom.Parameters["@capacity1"].Value = capacity;

                //MyCom.ExecuteNonQuery();
                //this.Close();
                myConnection.Close();
                this.Close();

            }
            else 
                MessageBox.Show("Fill atLeast One Field!");
        }

        private void M002Tb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(M002Tb, @"Example: c:\data\1 ");
        }

        private void M005Tb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(M005Tb, @"Example: c:\data\1 ");
        }

        private void PriceTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(PriceTb, @"Example: c:\data\1 ");
        }

        private void LoadTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(LoadTb, @"Example: c:\data\1 ");
        }

        private void CapacityTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(CapacityTb, @"Example: c:\data\1 ");
        }

        private void btnM002_Click(object sender, EventArgs e)
        {
            string path = getPath();
            M002Tb.Text = (path != "" ? path : M002Tb.Text);
        }
        private string getPath()
        {
            DialogResult result = this.folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                return folderBrowserDialog1.SelectedPath;
            }
            return "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string path = getPath();
            M005Tb.Text = (path != "" ? path : M005Tb.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string path = getPath();
            PriceTb.Text = (path != "" ? path : PriceTb.Text);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string path = getPath();
            LoadTb.Text = (path != "" ? path : LoadTb.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string path = getPath();
            CapacityTb.Text = (path != "" ? path : CapacityTb.Text);
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            PopulateFields();
        }

        private void PopulateFields()
        {

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            string strCmd = "SELECT * FROM [Path]";
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            Myda.SelectCommand = new SqlCommand(strCmd, myConnection);
            Myda.Fill(MyDS);

            myConnection.Close();
            if (MyDS.Tables[0].Rows.Count>0)
            {

                DataRow row = MyDS.Tables[0].Rows[0];
                M002Tb.Text = row["M002"].ToString().Trim();
                M005Tb.Text = row["M005"].ToString().Trim();
                CapacityTb.Text = row["CapacityFactor"].ToString().Trim();
                LoadTb.Text = row["LoadForecasting"].ToString().Trim();
                PriceTb.Text = row["AveragePrice"].ToString().Trim();
                txtProxy.Text = row["Proxy"].ToString().Trim();
            }
            
        }

        private void txtProxy_MouseClick_1(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(txtProxy, @"Example: http://172.16.1.100:8080");
        }
    }
}
