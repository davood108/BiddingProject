﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.DMS.Common;

namespace PowerPlantProject
{
    partial class MainForm
    {
        //---------------------------------------ClearFRPlantTab---------------------------------------------
        private void ClearFRPlantTab()
        {
            FRPlantBenefitTB.Text = "";
            FRPlantIncomeTb.Text = "";
            FRPlantCostTb.Text = "";
            FRPlantAvaCapTb.Text = "";
            FRPlantTotalPowerTb.Text = "";
            FRPlantBidPowerTb.Text = "";
            FRPlantULPowerTb.Text = "";
            FRPlantIncPowerTb.Text = "";
            FRPlantDecPowerTb.Text = "";
            FRPlantCapPayTb.Text = "";
            FRPlantEnergyPayTb.Text = "";
            FRPlantBidPayTb.Text = "";
            FRPlantULPayTb.Text = "";
            FRPlantIncPayTb.Text = "";
            FRPlantDecPayTb.Text = "";
        }
        //----------------------------------------FillFRValues()--------------------------------------------------
        private void FillFRValues(string Num)
        {
            ClearFRPlantTab();
            if (!FRPlantCal.IsNull)
            {
                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT Benefit,Income,Cost,AvailableCapacity,TotalPower,BidPower," +
                "ULPower,IncrementPower,DecreasePower,CapacityPayment,EnergyPayment,BidPayment,ULPayment,IncrementPayment," +
                "DecreasePayment FROM [EconomicPlant] WHERE Date=@date AND PPID=" + PPID, myConnection);
                Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Myda.SelectCommand.Parameters["@date"].Value = FRPlantCal.Text;
                Myda.Fill(MyDS);
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    FRPlantBenefitTB.Text = MyRow[0].ToString();
                    FRPlantIncomeTb.Text = MyRow[1].ToString();
                    FRPlantCostTb.Text = MyRow[2].ToString();
                    FRPlantAvaCapTb.Text = MyRow[3].ToString();
                    FRPlantTotalPowerTb.Text = MyRow[4].ToString();
                    FRPlantBidPowerTb.Text = MyRow[5].ToString();
                    FRPlantULPowerTb.Text = MyRow[6].ToString();
                    FRPlantIncPowerTb.Text = MyRow[7].ToString();
                    FRPlantDecPowerTb.Text = MyRow[8].ToString();
                    FRPlantCapPayTb.Text = MyRow[9].ToString();
                    FRPlantEnergyPayTb.Text = MyRow[10].ToString();
                    FRPlantBidPayTb.Text = MyRow[11].ToString();
                    FRPlantULPayTb.Text = MyRow[12].ToString();
                    FRPlantIncPayTb.Text = MyRow[13].ToString();
                    FRPlantDecPayTb.Text = MyRow[14].ToString();
                }
                myConnection.Close();
            }

        }
        //-------------------------------------ClearFRUnitTab()--------------------------------
        private void ClearFRUnitTab()
        {
            FRUnitCapitalTb.Text = "";
            FRUnitFixedTb.Text = "";
            FRUnitVariableTb.Text = "";
            FRUnitAmargTb1.Text = "";
            FRUnitAmargTb2.Text = "";
            FRUnitBmargTb1.Text = "";
            FRUnitBmargTb2.Text = "";
            FRUnitCmargTb1.Text = "";
            FRUnitCmargTb2.Text = "";
            FRUnitAmainTb.Text = "";
            FRUnitBmainTb.Text = "";
            FRUnitColdTb.Text = "";
            FRUnitHotTb.Text = "";
            FRUnitCapacityTb.Text = "";
            FRUnitTotalPowerTb.Text = "";
            FRUnitULPowerTb.Text = "";
            FRUnitCapPayTb.Text = "";
            FRUnitEneryPayTb.Text = "";
            FRUnitIncomeTb.Text = "";
        }
        //-----------------------------------FillFRUnit---------------------------------------
        private void FillFRUnit(string unit, string package, int index)
        {
            ClearFRUnitTab();
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            //Fill Cost Function GroupBox
            DataSet FRDS = new DataSet();
            SqlDataAdapter FRda = new SqlDataAdapter();
            FRda.SelectCommand = new SqlCommand("SELECT CapitalCost,FixedCost,VariableCost,PrimaryFuelAmargin," +
            "PrimaryFuelBmargin,PrimaryFuelCmargin,SecondFuelAmargin,SecondFuelBmargin,SecondFuelCmargin," +
            "BMaintenance,CMaintenance,CostStartCold,CostStartHot FROM [UnitsDataMain] WHERE PPID=" + PPID +
            " AND PackageType=@type AND UnitCode LIKE '" + unit + "%'", myConnection);
            FRda.SelectCommand.Parameters.Add("@type", SqlDbType.NChar, 10);
            string type = package;
            if (type.Contains("Combined")) type = "CC";
            FRda.SelectCommand.Parameters["@type"].Value = type;
            FRda.Fill(FRDS);
            myConnection.Close();
            foreach (DataRow MyRow in FRDS.Tables[0].Rows)
            {
                FRUnitCapitalTb.Text = MyRow[0].ToString().Trim();
                FRUnitFixedTb.Text = MyRow[1].ToString();
                FRUnitVariableTb.Text = MyRow[2].ToString();
                FRUnitAmargTb1.Text = MyRow[3].ToString();
                FRUnitBmargTb1.Text = MyRow[4].ToString();
                FRUnitCmargTb1.Text = MyRow[5].ToString();
                FRUnitAmargTb2.Text = MyRow[6].ToString();
                FRUnitBmargTb2.Text = MyRow[7].ToString();
                FRUnitCmargTb2.Text = MyRow[8].ToString();
                FRUnitAmainTb.Text = MyRow[9].ToString();
                FRUnitBmainTb.Text = MyRow[10].ToString();
                FRUnitColdTb.Text = MyRow[11].ToString();
                FRUnitHotTb.Text = MyRow[12].ToString();
            }

            //Fill Revenue GroupBox
            FillFRUnitRevenue(unit, package, index);

        }
        //-------------------------------------FillFRUnitRevenue()--------------------------------------
        private void FillFRUnitRevenue(string unit, string package, int index)
        {
            if ((!FRUnitCal.IsNull) && (FRUnitLb.Text != ""))
            {
                FRUnitCapacityTb.Text = "";
                FRUnitTotalPowerTb.Text = "";
                FRUnitULPowerTb.Text = "";
                FRUnitCapPayTb.Text = "";
                FRUnitEneryPayTb.Text = "";
                FRUnitIncomeTb.Text = "";

                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT AvailableCapacity,TotalPower,ULPower," +
                "CapacityPayment,EnergyPayment,Income FROM [EconomicUnit] WHERE UnitCode=@Punit AND Date=@date AND PPID=" + PPID, myConnection);
                Myda.SelectCommand.Parameters.Add("@Punit", SqlDbType.NChar, 20);
                string Pcode = "";
                if (GDSteamGroup.Text.Contains(package))
                    Pcode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                else Pcode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                string Punit = unit;
                if (package.Contains("Combined"))
                    Punit = "C" + Pcode;
                Punit = Punit.Trim();
                Myda.SelectCommand.Parameters["@Punit"].Value = Punit;
                Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Myda.SelectCommand.Parameters["@date"].Value = FRUnitCal.Text;
                Myda.Fill(MyDS);

                myConnection.Close();

                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    FRUnitCapacityTb.Text = MyRow[0].ToString();
                    FRUnitTotalPowerTb.Text = MyRow[1].ToString();
                    FRUnitULPowerTb.Text = MyRow[2].ToString();
                    FRUnitCapPayTb.Text = MyRow[3].ToString();
                    FRUnitEneryPayTb.Text = MyRow[4].ToString();
                    FRUnitIncomeTb.Text = MyRow[5].ToString();
                }

            }
        }

        //private void JustOneTime(string StartDate)
        //{
        //    //detect Yesterday
        //    System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
        //    DateTime CurDate = DateTime.Now;
        //    int imonth = pr.GetMonth(CurDate);
        //    int iyear = pr.GetYear(CurDate);
        //    int iday = pr.GetDayOfMonth(CurDate) - 1;
        //    if (iday < 1)
        //    {
        //        imonth = imonth - 1;
        //        if (imonth < 1)
        //        {
        //            iyear = iyear - 1;
        //            imonth = 12;
        //        }
        //        if (imonth < 7) iday = 31;
        //        else if (imonth < 12) iday = 30;
        //        else iday = 29;
        //    }
        //    string day = iday.ToString();
        //    if (int.Parse(day) < 10) day = "0" + day;
        //    string mydate = iyear.ToString() + "/" + imonth.ToString() + "/" + day;

        //    while (StartDate != mydate)
        //    {
        //        SqlConnection MyCon = new SqlConnection();
        //        MyCon.ConnectionString = ConStr;
        //        MyCon.Open();
        //        SqlCommand MyCom1 = new SqlCommand();
        //        MyCom1.Connection = MyCon;

        //        //is there any data for this date on Database?
        //        MyCom1.CommandText = "SELECT @result=COUNT(Date) FROM EconomicPlant WHERE Date=@date";
        //        MyCom1.Parameters.Add("@date", SqlDbType.Char, 10);
        //        MyCom1.Parameters["@date"].Value = StartDate;
        //        MyCom1.Parameters.Add("@result", SqlDbType.Int);
        //        MyCom1.Parameters["@result"].Direction = ParameterDirection.Output;
        //        //try
        //        //{
        //        MyCom1.ExecuteNonQuery();
        //        //}
        //        //catch (Exception e)
        //        //{
        //        //string message = e.Message;
        //        //}
        //        int result = int.Parse(MyCom1.Parameters["@result"].Value.ToString());
        //        if (result == 0)
        //        {
        //            //???????????????SET DATE??????????????
        //            //string mydate = FRUnitCal.Text;
        //            //mydate = "1388/08/01";
        //            //for all palnts
        //            MyCom1.CommandText = "SELECT @updatedate=Date FROM BaseData WHERE BaseID=IDENT_CURRENT('BaseData')";
        //            MyCom1.Parameters.Add("@updatedate", SqlDbType.Char, 10);
        //            MyCom1.Parameters["@updatedate"].Direction = ParameterDirection.Output;
        //            //try
        //            //{
        //            MyCom1.ExecuteNonQuery();
        //            //}
        //            //catch (Exception e)
        //            //{
        //            //string message = e.Message;
        //            //}
        //            //detect date of last inserted row in BaseData 
        //            string UpdateDate = MyCom1.Parameters["@updatedate"].Value.ToString();
        //            for (int k = 0; k < NumPPID; k++)
        //                if ((PPIDArray[k] != "0") && (PPIDArray[k] != "132") && (PPIDArray[k] != "145")) FillEconomicPlant(k, UpdateDate, StartDate);
        //        }
        //        MyCon.Close();

        //        //Build Next day
        //        string TempDate = StartDate;
        //        int Tempday = int.Parse(TempDate.Substring(8));
        //        int TempMonth = int.Parse(TempDate.Substring(5, 2));
        //        int Tempyear = int.Parse(TempDate.Substring(0, 4));
        //        Tempday = Tempday + 1;
        //        if (TempMonth < 7)
        //        {
        //            if (Tempday > 31)
        //            {
        //                Tempday = 1;
        //                TempMonth++;
        //            }
        //        }
        //        else if (TempMonth < 12)
        //        {
        //            if (Tempday > 30)
        //            {
        //                Tempday = 1;
        //                TempMonth++;
        //            }
        //        }
        //        else if (TempMonth == 12)
        //            if (Tempday > 29)
        //            {
        //                Tempday = 1;
        //                TempMonth = 1;
        //                Tempyear++;
        //            }
        //        string tempday = "";
        //        string tempmonth = "";
        //        if (Tempday < 10) tempday = "0" + Tempday.ToString();
        //        if (TempMonth < 10) tempmonth = "0" + TempMonth.ToString();
        //        StartDate = Tempyear + "/" + tempmonth + "/" + tempday;
        //    }

        //}
        //------------------------------------------AutomaticFillEconomics---------------------------------
        //private void AutomaticFillEconomics()
        //{
        //    //detect Yesterday
        //    System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
        //    DateTime CurDate = DateTime.Now;
        //    int imonth = pr.GetMonth(CurDate);
        //    int iyear = pr.GetYear(CurDate);
        //    int iday = pr.GetDayOfMonth(CurDate) - 1;
        //    if (iday < 1)
        //    {
        //        imonth = imonth - 1;
        //        if (imonth < 1)
        //        {
        //            iyear = iyear - 1;
        //            imonth = 12;
        //        }
        //        if (imonth < 7) iday = 31;
        //        else if (imonth < 12) iday = 30;
        //        else iday = 29;
        //    }
        //    string day = iday.ToString();
        //    if (int.Parse(day) < 10) day = "0" + day;
        //    string mydate = iyear.ToString() + "/" + imonth.ToString() + "/" + day;

        //    SqlConnection MyCon = new SqlConnection(ConnectionManager.Connection);
        //    SqlCommand MyCom1 = new SqlCommand();
        //    MyCom1.Connection = MyCon;

        //    //is there any data for this date on Database?
        //    MyCom1.CommandText = "SELECT @result=COUNT(Date) FROM EconomicPlant WHERE Date=@date";
        //    MyCom1.Parameters.Add("@date", SqlDbType.Char, 10);
        //    MyCom1.Parameters["@date"].Value = mydate;
        //    MyCom1.Parameters.Add("@result", SqlDbType.Int);
        //    MyCom1.Parameters["@result"].Direction = ParameterDirection.Output;
        //    //try
        //    //{
        //    MyCom1.ExecuteNonQuery();
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //string message = e.Message;
        //    //}
        //    int result = int.Parse(MyCom1.Parameters["@result"].Value.ToString());
        //    if (result == 0)
        //    {
        //        //???????????????SET DATE??????????????
        //        //string mydate = FRUnitCal.Text;
        //        //mydate = "1388/08/01";
        //        //for all palnts
        //        MyCom1.CommandText = "SELECT @updatedate=Date FROM BaseData WHERE BaseID=IDENT_CURRENT('BaseData')";
        //        MyCom1.Parameters.Add("@updatedate", SqlDbType.Char, 10);
        //        MyCom1.Parameters["@updatedate"].Direction = ParameterDirection.Output;
        //        //try
        //        //{
        //        MyCom1.ExecuteNonQuery();
        //        //}
        //        //catch (Exception e)
        //        //{
        //        //string message = e.Message;
        //        //}
        //        //detect date of last inserted row in BaseData 
        //        string UpdateDate = MyCom1.Parameters["@updatedate"].Value.ToString();
        //        for (int k = 0; k < NumPPID; k++)
        //            if ((PPIDArray[k] != "0") && (PPIDArray[k] != "132") && (PPIDArray[k] != "145")) FillEconomicPlant(k, UpdateDate, mydate);
        //    }
        //    MyCon.Close();
        //}
        //----------------------------------------FillEconomicPlant----------------------------------------
        //private void FillEconomicPlant(int k, string UpdateDate, string mydate)
        //{
        //    ResetVariables();

        //    for (int z = 0; z < 24; z++)
        //    {
        //        NoCCMaxBid[z] = 0;
        //        CCMaxBid[z] = 0;
        //    }

        //    SqlConnection PlantCon = ConnectionManager.GetInstance().Connection;
        //    DataSet PlantDS = new DataSet();
        //    SqlDataAdapter Plantda = new SqlDataAdapter();
        //    Plantda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode,Capacity,FixedCost," +
        //    "VariableCost,Bmaintenance,CMaintenance,PrimaryFuelAmargin,PrimaryFuelBmargin,PrimaryFuelCmargin," +
        //    "SecondFuelAmargin,SecondFuelBmargin,SecondFuelCmargin,CostStartCold,CostStartHot FROM [UnitsDataMain]" +
        //    "WHERE PPID=@num", PlantCon);
        //    Plantda.SelectCommand.Parameters.Add("@num", SqlDbType.NChar, 10);
        //    Plantda.SelectCommand.Parameters["@num"].Value = PPIDArray[k].ToString();
        //    Plantda.Fill(PlantDS);
        //    TempGV.DataSource = PlantDS.Tables[0].DefaultView;


        //    //for all Units of selected plant
        //    bool[] check = new bool[TempGV.RowCount - 1];
        //    for (int x = 0; x < TempGV.RowCount - 1; x++) check[x] = false;
        //    for (int index = 0; index < (TempGV.RowCount - 1); index++)
        //        if (!check[index])
        //        {
        //            int Pcode = int.Parse(TempGV.Rows[index].Cells[2].Value.ToString());
        //            //for  CombinedCycle 's Units one time run this loop
        //            check[index] = true;
        //            foreach (DataGridViewRow DR in TempGV.Rows)
        //                if (DR.Index < TempGV.RowCount - 1)
        //                    if ((int.Parse(DR.Cells[2].Value.ToString()) == Pcode) && (DR.Cells[1].Value.ToString().Contains("CC")))
        //                        check[DR.Index] = true;

        //            FillEconomicUnit(k, UpdateDate, mydate, index);
        //        }

        //    //FILL EconomicPlant For EVERY Plant!

        //    //****************PTotalPower*****************
        //    SqlConnection MyCon = ConnectionManager.GetInstance().Connection;
        //    SqlCommand MyCom1 = new SqlCommand();
        //    MyCom1.Connection = MyCon;
        //    MyCom1.CommandText = "SELECT @re=SUM(Hour1+Hour2+Hour3+Hour4+Hour5+Hour6+Hour7+Hour8+Hour9+Hour10+" +
        //    "Hour11+Hour12+Hour13+Hour14+Hour15+Hour16+Hour17+Hour18+Hour19+Hour20+Hour21+Hour22+Hour23+Hour24)" +
        //    " FROM ProducedEnergy WHERE PPCode=@num AND Date=@mdate";
        //    MyCom1.Parameters.Add("@mdate", SqlDbType.Char, 10);
        //    MyCom1.Parameters["@mdate"].Value = mydate;
        //    MyCom1.Parameters.Add("@num", SqlDbType.NChar, 10);
        //    MyCom1.Parameters["@num"].Value = PPIDArray[k];
        //    MyCom1.Parameters.Add("@re", SqlDbType.Real);
        //    MyCom1.Parameters["@re"].Direction = ParameterDirection.Output;
        //    //try
        //    //{
        //    MyCom1.ExecuteNonQuery();
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //string message = e.Message;
        //    //}
        //    try { NoCCTotalPower = double.Parse(MyCom1.Parameters["@re"].Value.ToString()); }
        //    catch { NoCCTotalPower = 0; }
        //    int CCID = int.Parse(PPIDArray[k]);
        //    if ((CCID == 131) || (CCID == 144))
        //    {
        //        CCID++;
        //        MyCom1.CommandText = "SELECT @CCre=SUM(Hour1+Hour2+Hour3+Hour4+Hour5+Hour6+Hour7+Hour8+Hour9+Hour10+" +
        //        "Hour11+Hour12+Hour13+Hour14+Hour15+Hour16+Hour17+Hour18+Hour19+Hour20+Hour21+Hour22+Hour23+Hour24)" +
        //        " FROM ProducedEnergy WHERE PPCode=@CCnum AND Date=@mdate";
        //        MyCom1.Parameters.Add("@CCnum", SqlDbType.NChar, 10);
        //        MyCom1.Parameters["@CCnum"].Value = CCID;
        //        MyCom1.Parameters.Add("@CCre", SqlDbType.Real);
        //        MyCom1.Parameters["@CCre"].Direction = ParameterDirection.Output;
        //        //try
        //        //{
        //        MyCom1.ExecuteNonQuery();
        //        //}
        //        //catch (Exception e)
        //        //{
        //        //string message = e.Message;
        //        //}
        //        try { CCTotalPower = double.Parse(MyCom1.Parameters["@CCre"].Value.ToString()); }
        //        catch { CCTotalPower = 0; }
        //    }
        //    else CCTotalPower = 0;
        //    PTotalPower = NoCCTotalPower + CCTotalPower;

        //    //****************IncrementPower & DecreasePower
        //    NoCCIncPower = 0;
        //    NoCCDecPower = 0;
        //    if ((NoCCTotalPower - NoCCUnitPower) > 0)
        //    {
        //        NoCCIncPower = NoCCTotalPower - NoCCUnitPower;
        //        NoCCDecPower = 0;
        //    }
        //    else if ((NoCCTotalPower - NoCCUnitPower) < 0)
        //    {
        //        NoCCIncPower = 0;
        //        NoCCDecPower = NoCCUnitPower - NoCCTotalPower;
        //    }
        //    if ((int.Parse(PPIDArray[k]) == 131) || (int.Parse(PPIDArray[k]) == 144))
        //    {
        //        if ((CCTotalPower - CCUnitPower) > 0)
        //        {
        //            CCIncPower = CCTotalPower - CCUnitPower;
        //            CCDecPower = 0;
        //        }
        //        else if ((CCTotalPower - CCUnitPower) < 0)
        //        {
        //            CCIncPower = 0;
        //            CCDecPower = CCUnitPower - CCTotalPower;
        //        }

        //    }
        //    else
        //    {
        //        CCIncPower = 0;
        //        CCDecPower = 0;
        //    }

        //    PIncrementPower = NoCCIncPower + CCIncPower;
        //    PDecreasePower = NoCCDecPower + CCDecPower;
        //    //*************************BidPower
        //    PBidPower = PBidPower - PULPower;

        //    //*************************IncrementPayment/DecreasePayment
        //    PIncrementPayment = 0;
        //    PDecreasePayment = 0;
        //    double NoCCDecPayment = 0;
        //    // Detect Hazineye tolid
        //    switch (PPIDArray[k])
        //    {
        //        case "101":
        //            MyCom1.CommandText = "SELECT @NoCC=SteamProduction FROM BaseData WHERE Date=@update";
        //            break;
        //        case "131":
        //            MyCom1.CommandText = "SELECT @NoCC=SteamProduction,@CC=CCProduction FROM BaseData WHERE Date=@update";
        //            break;
        //        case "104":
        //            MyCom1.CommandText = "SELECT @NoCC=GasProduction FROM BaseData WHERE Date=@update";
        //            break;
        //        case "133":
        //            MyCom1.CommandText = "SELECT @NoCC=SteamProduction FROM BaseData WHERE Date=@update";
        //            break;
        //        case "138":
        //            MyCom1.CommandText = "SELECT @NoCC=GasProduction FROM BaseData WHERE Date=@update";
        //            break;
        //        case "144":
        //            MyCom1.CommandText = "SELECT @NoCC=SteamProduction,@CC=CCProduction FROM BaseData WHERE Date=@update";
        //            break;
        //        case "149":
        //            MyCom1.CommandText = "SELECT @NoCC=CCProduction FROM BaseData WHERE Date=@update";
        //            break;
        //    }
        //    MyCom1.Parameters.Add("@update", SqlDbType.Char, 10);
        //    MyCom1.Parameters["@update"].Value = UpdateDate;
        //    MyCom1.Parameters.Add("@CC", SqlDbType.Real);
        //    MyCom1.Parameters["@CC"].Direction = ParameterDirection.Output;
        //    MyCom1.Parameters.Add("@NoCC", SqlDbType.Real);
        //    MyCom1.Parameters["@NoCC"].Direction = ParameterDirection.Output;
        //    //try
        //    //{
        //    MyCom1.ExecuteNonQuery();
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //string message = e.Message;
        //    //}
        //    double NoCC, CC = 0;
        //    NoCC = double.Parse(MyCom1.Parameters["@NoCC"].Value.ToString());
        //    if ((int.Parse(PPIDArray[k]) == 131) || (int.Parse(PPIDArray[k]) == 144))
        //        CC = double.Parse(MyCom1.Parameters["@CC"].Value.ToString());

        //    double mycost = NoCC;
        //    double[] myMaxbid = new double[24];
        //    for (int w = 0; w < 24; w++) myMaxbid[w] = NoCCMaxBid[w];
        //    int repeatIndex = 0;
        //    bool repeat = true;
        //    while ((repeat) && (repeatIndex < 2))
        //    {
        //        double[] re = new double[24];
        //        double[] produce = new double[24];
        //        int type = 0;
        //        string num = PPIDArray[k];

        //        SqlConnection FRCon = ConnectionManager.GetInstance().Connection;
        //        DataSet FRDS = new DataSet();
        //        SqlDataAdapter FRda = new SqlDataAdapter();
        //        FRda.SelectCommand = new SqlCommand("SELECT SUM(Required),Hour FROM " +
        //        "[DetailFRM005] WHERE TargetMarketDate=@date AND PPID=@num AND PPType=@type GROUP BY Hour ", FRCon);
        //        FRda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
        //        FRda.SelectCommand.Parameters["@date"].Value = mydate;
        //        FRda.SelectCommand.Parameters.Add("@num", SqlDbType.NChar, 10);
        //        FRda.SelectCommand.Parameters["@num"].Value = num;
        //        FRda.SelectCommand.Parameters.Add("@type", SqlDbType.SmallInt);
        //        FRda.SelectCommand.Parameters["@type"].Value = type;
        //        FRda.Fill(FRDS);
        //        foreach (DataRow MyRow in FRDS.Tables[0].Rows)
        //            re[int.Parse(MyRow[1].ToString()) - 1] = double.Parse(MyRow[0].ToString());

        //        SqlConnection PCon = ConnectionManager.GetInstance().Connection;
        //        DataSet PDS = new DataSet();
        //        SqlDataAdapter Pda = new SqlDataAdapter();
        //        Pda.SelectCommand = new SqlCommand("SELECT SUM(Hour1),SUM(Hour2),SUM(Hour3),SUM(Hour4),SUM(Hour5),SUM(Hour6)" +
        //        ",SUM(Hour7),SUM(Hour8),SUM(Hour9),SUM(Hour10),SUM(Hour11),SUM(Hour12),SUM(Hour13),SUM(Hour14),SUM(Hour15)," +
        //        "SUM(Hour16),SUM(Hour17),SUM(Hour18),SUM(Hour19),SUM(Hour20),SUM(Hour21),SUM(Hour22),SUM(Hour23),SUM(Hour24) " +
        //        "FROM [ProducedEnergy] WHERE Date=@date AND PPCode=@num", PCon);
        //        Pda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
        //        Pda.SelectCommand.Parameters["@date"].Value = mydate;
        //        Pda.SelectCommand.Parameters.Add("@num", SqlDbType.NChar, 10);
        //        Pda.SelectCommand.Parameters["@num"].Value = num;
        //        Pda.Fill(PDS);
        //        foreach (DataRow MyRow in PDS.Tables[0].Rows)
        //            for (int i = 0; i < 24; i++)
        //            {
        //                //produce[int.Parse(MyRow[1].ToString())] = double.Parse(MyRow[0].ToString());
        //                try { produce[i] = double.Parse(MyRow[i].ToString()); }
        //                catch { produce[i] = 0; }
        //            }

        //        double[] tempIncPower = new double[24];
        //        double[] tempDecPower = new double[24];
        //        double[] tempIncPay = new double[24];
        //        double[] tempDecPay = new double[24];

        //        for (int q = 0; q < 24; q++)
        //        {

        //            if (produce[q] > re[q])
        //            {
        //                tempIncPower[q] = produce[q] - re[q];
        //                tempDecPower[q] = 0;
        //            }
        //            else
        //            {
        //                tempIncPower[q] = 0;
        //                tempDecPower[q] = re[q] - produce[q];
        //            }
        //            tempIncPay[q] = tempIncPower[q] * myMaxbid[q];
        //            tempDecPay[q] = tempDecPower[q] * (myMaxbid[q] - mycost);

        //            PIncrementPayment += tempIncPay[q];
        //            PDecreasePayment += tempDecPay[q];
        //        }
        //        if (repeatIndex == 0)
        //            NoCCDecPayment = PDecreasePayment;
        //        if ((int.Parse(PPIDArray[k]) == 131) || (int.Parse(PPIDArray[k]) == 144))
        //        {
        //            repeat = true;
        //            num = (int.Parse(PPIDArray[k]) + 1).ToString(); ;
        //            type = 1;
        //            for (int w = 0; w < 24; w++) myMaxbid[w] = CCMaxBid[w];
        //            mycost = CC;
        //        }
        //        else repeat = false;
        //        repeatIndex++;
        //    }

        //    //*************************EnergyPayment
        //    PEnergyPayment = PBidPayment + PULPayment + PIncrementPayment - (NoCCDecPayment * NoCC) - ((PDecreasePayment - NoCCDecPayment) * CC);

        //    //*************************Income
        //    PIncome = PEnergyPayment + PCapacityPayment;

        //    //*************************Benefit
        //    PBenefit = PIncome - PCost;

        //    //INSERT INTO EconomicPlant Table
        //    MyCom1.CommandText = "INSERT INTO [EconomicPlant] (PPID,Date,AvailableCapacity,TotalPower,BidPower," +
        //    "ULPower,Cost,CapacityPayment,EnergyPayment,Income,Benefit,IncrementPower,DecreasePower,BidPayment," +
        //    "ULPayment,IncrementPayment,DecreasePayment) VALUES (@num,@date,@avacap,@tpower,@bpower,@upower," +
        //    "@cost,@cap,@energy,@income,@benefit,@incpo,@decpo,@bidpay,@upay,@incpay,@decpay)";
        //    MyCom1.Parameters.Add("@date", SqlDbType.Char, 10);
        //    MyCom1.Parameters["@date"].Value = mydate;
        //    MyCom1.Parameters.Add("@avacap", SqlDbType.Real);
        //    MyCom1.Parameters["@avacap"].Value = PAvailableCapacity;
        //    MyCom1.Parameters.Add("@tpower", SqlDbType.Real);
        //    MyCom1.Parameters["@tpower"].Value = PTotalPower;
        //    MyCom1.Parameters.Add("@bpower", SqlDbType.Real);
        //    MyCom1.Parameters["@bpower"].Value = PBidPower;
        //    MyCom1.Parameters.Add("@upower", SqlDbType.Real);
        //    MyCom1.Parameters["@upower"].Value = PULPower;
        //    MyCom1.Parameters.Add("@cost", SqlDbType.Real);
        //    MyCom1.Parameters["@cost"].Value = PCost;
        //    MyCom1.Parameters.Add("@cap", SqlDbType.Real);
        //    MyCom1.Parameters["@cap"].Value = PCapacityPayment;
        //    MyCom1.Parameters.Add("@energy", SqlDbType.Real);
        //    MyCom1.Parameters["@energy"].Value = PEnergyPayment;
        //    MyCom1.Parameters.Add("@income", SqlDbType.Real);
        //    MyCom1.Parameters["@income"].Value = PIncome;
        //    MyCom1.Parameters.Add("@benefit", SqlDbType.Real);
        //    MyCom1.Parameters["@benefit"].Value = PBenefit;
        //    MyCom1.Parameters.Add("@incpo", SqlDbType.Real);
        //    MyCom1.Parameters["@incpo"].Value = PIncrementPower;
        //    MyCom1.Parameters.Add("@decpo", SqlDbType.Real);
        //    MyCom1.Parameters["@decpo"].Value = PDecreasePower;
        //    MyCom1.Parameters.Add("@bidpay", SqlDbType.Real);
        //    MyCom1.Parameters["@bidpay"].Value = PBidPayment;
        //    MyCom1.Parameters.Add("@upay", SqlDbType.Real);
        //    MyCom1.Parameters["@upay"].Value = PULPayment;
        //    MyCom1.Parameters.Add("@incpay", SqlDbType.Real);
        //    MyCom1.Parameters["@incpay"].Value = PIncrementPayment;
        //    MyCom1.Parameters.Add("@decpay", SqlDbType.Real);
        //    MyCom1.Parameters["@decpay"].Value = PDecreasePayment;

        //    //try
        //    //{
        //    MyCom1.ExecuteNonQuery();
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //string message = e.Message;
        //    //}
        //}
        ////-----------------------------------------------------------FillEconomicUnit()-----------------------------------
        //private void FillEconomicUnit(int k, string UpdateDate, string mydate, int index)
        //{
        //    double AvailableCapacity = 0, TotalPower = 0, ULPower = 0, CostPayment = 0, CapacityPayment = 0,
        //    EnergyPayment = 0, Income = 0, Benefit = 0;

        //    string unit = TempGV.Rows[index].Cells[0].Value.ToString().Trim();
        //    string package = TempGV.Rows[index].Cells[1].Value.ToString().Trim();
        //    int Pcode = int.Parse(TempGV.Rows[index].Cells[2].Value.ToString());

        //    SqlCommand MyCom = new SqlCommand();
        //    MyCom.Connection = MyConnection;

        //    //Detect Block For FRM005
        //    string temp = unit;
        //    temp = temp.ToLower();
        //    if (package.Contains("CC"))
        //    {
        //        int x = int.Parse(PPIDArray[k]);
        //        if ((x == 131) || (x == 144)) x++;
        //        temp = x + "-" + "C" + Pcode;
        //    }
        //    else if (temp.Contains("gas"))
        //    {
        //        temp = temp.Replace("gas", "G");
        //        temp = PPIDArray[k] + "-" + temp;
        //    }
        //    else
        //    {
        //        temp = temp.Replace("steam", "S");
        //        temp = PPIDArray[k] + "-" + temp;
        //    }
        //    //*************************SET AvailableCapacity/ TotalPower/ ULPower
        //    AvailableCapacity = SetAvailableCap(k, mydate, index, temp);
        //    TotalPower = SetTotalPower(k, mydate, index, temp);
        //    ULPower = SetULPower(k, mydate, index, temp);

        //    //*************************************SET CostPayment
        //    double[] P = new double[24];
        //    double[] Dis = new double[24];
        //    string[] Contribution = new string[24];
        //    double[] W = new double[24];
        //    double[] U = new double[24];
        //    double F4 = 0;
        //    SqlConnection FRCon = ConnectionManager.GetInstance().Connection;
        //    DataSet FRDS = new DataSet();
        //    SqlDataAdapter FRda = new SqlDataAdapter();
        //    FRda.SelectCommand = new SqlCommand("SELECT Required,Hour,Dispatchable,Contribution FROM " +
        //    "[DetailFRM005] WHERE TargetMarketDate=@date AND PPID=@num AND Block=@block", FRCon);
        //    FRda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
        //    FRda.SelectCommand.Parameters["@date"].Value = mydate;
        //    FRda.SelectCommand.Parameters.Add("@num", SqlDbType.NChar, 10);
        //    FRda.SelectCommand.Parameters["@num"].Value = PPIDArray[k];
        //    FRda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
        //    FRda.SelectCommand.Parameters["@block"].Value = temp;
        //    FRda.Fill(FRDS);
        //    for (int i = 0; i < 24; i++)
        //    {
        //        P[i] = 0;
        //        Dis[i] = 0;
        //        Contribution[i] = "N";
        //    }
        //    foreach (DataRow MyRow in FRDS.Tables[0].Rows)
        //    {
        //        P[int.Parse(MyRow[1].ToString()) - 1] = double.Parse(MyRow[0].ToString());
        //        Dis[int.Parse(MyRow[1].ToString()) - 1] = double.Parse(MyRow[2].ToString());
        //        Contribution[int.Parse(MyRow[1].ToString()) - 1] = MyRow[3].ToString();
        //    }

        //    CostPayment = SetCostPayment(k, index, P, Dis);
        //    //*************************SET CapacityPayment
        //    CapacityPayment = SetCapacityPayment(k, UpdateDate, mydate, index, Dis);

        //    //*************************SET EnergyPayment
        //    SqlConnection PowerCon = ConnectionManager.GetInstance().Connection;
        //    DataSet PowerDS = new DataSet();
        //    SqlDataAdapter Powerda = new SqlDataAdapter();
        //    Powerda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3," +
        //    "Power4,Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10," +
        //    "Price10,Hour FROM [DetailFRM002] WHERE TargetMarketDate=@date AND PPID=@num AND Block=@block", FRCon);
        //    Powerda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
        //    Powerda.SelectCommand.Parameters["@date"].Value = mydate;
        //    Powerda.SelectCommand.Parameters.Add("@num", SqlDbType.NChar, 10);
        //    Powerda.SelectCommand.Parameters["@num"].Value = PPIDArray[k];
        //    Powerda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
        //    //DETECT Block For FRM002
        //    string block = unit;
        //    block = block.ToLower();
        //    if (package.Contains("CC"))
        //        block = "C" + Pcode;
        //    else
        //    {
        //        if (block.Contains("gas"))
        //            block = block.Replace("gas", "G");
        //        else if (block.Contains("steam"))
        //            block = block.Replace("steam", "S");
        //    }
        //    block = block.Trim();
        //    Powerda.SelectCommand.Parameters["@block"].Value = block;
        //    Powerda.Fill(PowerDS);

        //    //SET Total Of EnergyPayment for all of those Contribution=="UL" or Contribution=="N" 
        //    double N_EPUnit = 0, UL_EPUnit = 0;

        //    double[] EP = new double[24];
        //    foreach (DataRow PRow in PowerDS.Tables[0].Rows)
        //    {
        //        int hour = int.Parse(PRow[20].ToString()) - 1;
        //        EP[hour] = 0;
        //        double[] Power = new double[10];
        //        double[] price = new double[10];
        //        for (int z = 0; z < 10; z++) Power[z] = double.Parse(PRow[2 * z].ToString());
        //        for (int z = 0; z < 10; z++) price[z] = double.Parse(PRow[(2 * z) + 1].ToString());
        //        if (Contribution[hour].Contains("N"))
        //        {
        //            int p1 = 0;
        //            while (p1 < 10)
        //            {
        //                if (Power[p1] <= P[hour]) EP[hour] += Power[p1] * price[p1];
        //                else if ((Power[p1] > P[hour]))
        //                {
        //                    if (p1 > 0) EP[hour] += (P[hour] - Power[p1 - 1]) * price[p1];
        //                    else EP[hour] += P[hour] * price[p1];
        //                    p1 = 10;
        //                }
        //                p1++;
        //            }
        //            N_EPUnit += EP[hour];
        //        }
        //        //if (Contribution[hour].Contains("UL"))
        //        else
        //        {
        //            //DETECT Min Price Form AveragePrice Table
        //            SqlConnection MyCon = ConnectionManager.GetInstance().Connection;
        //            SqlCommand MyCom1 = new SqlCommand();
        //            MyCom1.Connection = MyCon;

        //            MyCom1.CommandText = "SELECT @min=AcceptedMin FROM [AveragePrice] WHERE Date=@date " +
        //            "AND Hour=@hour";
        //            MyCom1.Parameters.Add("@date", SqlDbType.Char, 10);
        //            MyCom1.Parameters["@date"].Value = mydate;
        //            MyCom1.Parameters.Add("@hour", SqlDbType.SmallInt);
        //            MyCom1.Parameters["@hour"].Value = hour + 1;
        //            MyCom1.Parameters.Add("@min", SqlDbType.Real);
        //            MyCom1.Parameters["@min"].Direction = ParameterDirection.Output;
        //            //try
        //            //{
        //            MyCom1.ExecuteNonQuery();
        //            //}
        //            //catch (Exception e)
        //            //{
        //            //string message = e.Message;
        //            //}
        //            double minPrice;
        //            try { minPrice = double.Parse(MyCom1.Parameters["@min"].Value.ToString()); }
        //            catch { minPrice = 0; }
        //            EP[hour] = Power[0] * minPrice * 0.9;
        //            UL_EPUnit += EP[hour];
        //        }

        //        //*************************SET MaxBid
        //        double TempBid = 0;
        //        int x = 0;
        //        while ((x < 10) && (Power[x] < P[hour])) x++;
        //        if ((x < 10) && (Power[x] >= P[hour])) TempBid = price[x];
        //        if ((int.Parse(PPIDArray[k]) == 131) || (int.Parse(PPIDArray[k]) == 144))
        //        {
        //            if ((package.Contains("CC")) && (CCMaxBid[hour] < TempBid)) CCMaxBid[hour] = TempBid;
        //            else if ((!(package.Contains("CC"))) && (NoCCMaxBid[hour] < TempBid)) NoCCMaxBid[hour] = TempBid;
        //        }
        //        //if ((!(package.Contains("Combined"))) && (NoCCMaxBid < TempBid)) 
        //        else if (NoCCMaxBid[hour] < TempBid)
        //            NoCCMaxBid[hour] = TempBid;
        //    }

        //    EnergyPayment = 0;
        //    for (int x = 0; x < 24; x++)
        //        EnergyPayment += EP[x];

        //    //*************************SET Income
        //    Income = EnergyPayment + CapacityPayment;

        //    //*************************SET Benefit
        //    Benefit = Income - CostPayment;


        //    //INSERT INTO EconomicUnit Table
        //    MyCom.CommandText = "INSERT INTO [EconomicUnit] (PPID,UnitCode,Date,AvailableCapacity,TotalPower," +
        //    "ULPower,CostPayment,CapacityPayment,EnergyPayment,Income,Benefit) VALUES (@num,@Punit,@date," +
        //    "@avacap,@tpower,@upower,@cost,@cap,@energy,@income,@benefit)";
        //    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
        //    MyCom.Parameters["@num"].Value = PPIDArray[k];
        //    MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
        //    MyCom.Parameters["@date"].Value = mydate;
        //    MyCom.Parameters.Add("@Punit", SqlDbType.NChar, 20);
        //    string Punit = unit;
        //    if (package.Contains("CC"))
        //        Punit = "C" + Pcode;
        //    Punit = Punit.Trim();
        //    MyCom.Parameters["@Punit"].Value = Punit;
        //    MyCom.Parameters.Add("@avacap", SqlDbType.Real);
        //    MyCom.Parameters["@avacap"].Value = AvailableCapacity;
        //    MyCom.Parameters.Add("@tpower", SqlDbType.Real);
        //    MyCom.Parameters["@tpower"].Value = TotalPower;
        //    MyCom.Parameters.Add("@upower", SqlDbType.Real);
        //    MyCom.Parameters["@upower"].Value = ULPower;
        //    MyCom.Parameters.Add("@cost", SqlDbType.Real);
        //    MyCom.Parameters["@cost"].Value = CostPayment;
        //    MyCom.Parameters.Add("@cap", SqlDbType.Real);
        //    MyCom.Parameters["@cap"].Value = CapacityPayment;
        //    MyCom.Parameters.Add("@energy", SqlDbType.Real);
        //    MyCom.Parameters["@energy"].Value = EnergyPayment;
        //    MyCom.Parameters.Add("@income", SqlDbType.Real);
        //    MyCom.Parameters["@income"].Value = Income;
        //    MyCom.Parameters.Add("@benefit", SqlDbType.Real);
        //    MyCom.Parameters["@benefit"].Value = Benefit;
        //    //try
        //    //{
        //    MyCom.ExecuteNonQuery();
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //string message = e.Message;
        //    //}

        //    //SET Some Items Of selected Plant, Incrementally!
        //    PAvailableCapacity += AvailableCapacity;
        //    PULPower += ULPower;
        //    PBidPower += TotalPower;
        //    PCapacityPayment += CapacityPayment;
        //    PBidPayment += N_EPUnit;
        //    PULPayment += UL_EPUnit;
        //    PCost += CostPayment;

        //}
        //--------------------------------------SetCapacityPayment---------------------------------
        //private double SetCapacityPayment(int k, string UpdateDate, string mydate, int index, double[] Dis)
        //{
        //    double CapacityPayment = 0;

        //    SqlConnection HourCon = ConnectionManager.GetInstance().Connection;
        //    DataSet HourDS = new DataSet();
        //    SqlDataAdapter Hourda = new SqlDataAdapter();
        //    Hourda.SelectCommand = new SqlCommand("SELECT H1,H2,H3,H4,H5,H6,H7,H8,H9,H10,H11,H12,H13," +
        //    "H14,H15,H16,H17,H18,H19,H20,H21,H22,H23,H24 FROM [HCPF] WHERE Date=@date", HourCon);
        //    Hourda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
        //    Hourda.SelectCommand.Parameters["@date"].Value = mydate;
        //    Hourda.Fill(HourDS);
        //    double TempCap = 0;
        //    foreach (DataRow HRow in HourDS.Tables[0].Rows)
        //    {
        //        for (int c = 0; c < 24; c++)
        //            TempCap += double.Parse(HRow[c].ToString()) * Dis[c];
        //    }

        //    // DETECT The Week Of the Selected day!
        //    int year = int.Parse(mydate.Remove(4));
        //    int day = int.Parse(mydate.Substring(8, 2));
        //    int month = int.Parse(mydate.Substring(5, 2));
        //    if (month < 7) day = day + ((month - 1) * 31);
        //    else day = 186 + day + ((month - 7) * 30);
        //    int week = day / 7;
        //    if (((day % 7) != 0) && (week < 52)) week++;

        //    SqlCommand MyCom = new SqlCommand();
        //    MyCom.Connection = MyConnection;
        //    MyCom.CommandText = "SELECT @value=Value FROM [WCPF] WHERE Week=@week AND Year=@year " +
        //    "SELECT @mycap=CapacityPayment FROM [BaseData] WHERE Date=@udate";
        //    MyCom.Parameters.Add("@year", SqlDbType.Int);
        //    MyCom.Parameters["@year"].Value = year;
        //    MyCom.Parameters.Add("@week", SqlDbType.Int);
        //    MyCom.Parameters["@week"].Value = week;
        //    MyCom.Parameters.Add("@udate", SqlDbType.Char, 10);
        //    MyCom.Parameters["@udate"].Value = UpdateDate;
        //    MyCom.Parameters.Add("@value", SqlDbType.Real);
        //    MyCom.Parameters["@value"].Direction = ParameterDirection.Output;
        //    MyCom.Parameters.Add("@mycap", SqlDbType.Real);
        //    MyCom.Parameters["@mycap"].Direction = ParameterDirection.Output;
        //    //try
        //    //{
        //    MyCom.ExecuteNonQuery();
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //string message = e.Message;
        //    //}
        //    double myweek;
        //    try { myweek = double.Parse(MyCom.Parameters["@value"].Value.ToString()); }
        //    catch { myweek = 0; }
        //    double BaseCap;
        //    try { BaseCap = double.Parse(MyCom.Parameters["@mycap"].Value.ToString()); }
        //    catch { BaseCap = 0; }
        //    CapacityPayment = myweek * BaseCap * TempCap;
        //    return (CapacityPayment);

        //}
        ////----------------------------------------SetCostPayment-----------------------------
        //private double SetCostPayment(int k, int index, double[] P, double[] Dis)
        //{
        //    double CostPayment = 0;
        //    int[] V = new int[24];
        //    int[] S = new int[24];
        //    double[] F1 = new double[24];
        //    double[] F2 = new double[24];
        //    double[] F3 = new double[24];
        //    double[] F5 = new double[24];
        //    double[] W = new double[24];
        //    double[] U = new double[24];
        //    double F4 = 0;

        //    string unit = TempGV.Rows[index].Cells[0].Value.ToString().Trim();
        //    string package = TempGV.Rows[index].Cells[1].Value.ToString().Trim();
        //    int Pcode = int.Parse(TempGV.Rows[index].Cells[2].Value.ToString());

        //    bool SF = isSecondFuel(k, index);

        //    for (int i = 0; i < 24; i++) if (P[i] > 0) V[i] = 1; else V[i] = 0;
        //    for (int i = 0; i < 23; i++) S[i] = V[i] - V[i + 1];

        //    //# of Required are less than zero
        //    int Minus = 0;
        //    for (int i = 0; i < 23; i++) if (S[i] < 0) Minus++;

        //    //IF selected unit is CombinedCycle
        //    if (package.Contains("CC"))
        //    {
        //        int GasIndex = index;
        //        int SteamIndex = index;
        //        foreach (DataGridViewRow DR in TempGV.Rows)
        //        {
        //            if (DR.Index < (TempGV.RowCount - 1))
        //                if ((int.Parse(DR.Cells[2].Value.ToString()) == Pcode) && (DR.Cells[1].Value.ToString().Contains("CC")))
        //                {
        //                    if ((!unit.Contains("Gas")) && (DR.Cells[0].Value.ToString().Contains("Gas"))) GasIndex = DR.Index;
        //                    if (DR.Cells[0].Value.ToString().Contains("Steam")) SteamIndex = DR.Index;
        //                }
        //        }
        //        //SET U & W
        //        double capacity = double.Parse(TempGV.Rows[GasIndex].Cells[3].Value.ToString());
        //        for (int z = 0; z < 24; z++)
        //        {
        //            if (Dis[z] == 0) { W[z] = 0; U[z] = 0; }
        //            else if (Dis[z] <= ((1.5) * capacity)) { W[z] = 0; U[z] = 1; }
        //            else if (Dis[z] <= (3 * capacity)) { W[z] = 1; U[z] = 1; }
        //        }
        //        //SET F1 ,F2,F3,F4,F5
        //        for (int j = 0; j < 24; j++)
        //        {
        //            double x19 = (double)1 / (double)9;
        //            double x13 = (double)1 / (double)3;
        //            if (SF)
        //                F1[j] = (double.Parse(TempGV.Rows[GasIndex].Cells[11].Value.ToString()) * x19 * P[j] * P[j] + x13 * double.Parse(TempGV.Rows[GasIndex].Cells[12].Value.ToString()) * P[j] + double.Parse(TempGV.Rows[GasIndex].Cells[13].Value.ToString())) * (U[j] + W[j]);
        //            else
        //                F1[j] = (double.Parse(TempGV.Rows[GasIndex].Cells[8].Value.ToString()) * x19 * P[j] * P[j] + x13 * double.Parse(TempGV.Rows[GasIndex].Cells[9].Value.ToString()) * P[j] + double.Parse(TempGV.Rows[GasIndex].Cells[10].Value.ToString())) * (U[j] + W[j]);
        //            F2[j] = (double.Parse(TempGV.Rows[SteamIndex].Cells[5].Value.ToString()) * ((U[j] + W[j]) / (double)2)) + (double.Parse(TempGV.Rows[GasIndex].Cells[5].Value.ToString()) * (U[j] + W[j]));
        //            F3[j] = double.Parse(TempGV.Rows[SteamIndex].Cells[4].Value.ToString()) + 2 * double.Parse(TempGV.Rows[GasIndex].Cells[4].Value.ToString());
        //            if (V[j] == 0) F5[j] = 0;
        //            else
        //                F5[j] = (double.Parse(TempGV.Rows[SteamIndex].Cells[6].Value.ToString()) * P[j]) + (double.Parse(TempGV.Rows[GasIndex].Cells[7].Value.ToString()) * (U[j] + W[j]));
        //        }
        //        if (Minus > 1)
        //        {
        //            try { F4 = Minus * double.Parse(TempGV.Rows[GasIndex].Cells[15].Value.ToString()); }
        //            catch { F4 = 0; }
        //        }
        //        else if (Minus == 1)
        //        {
        //            try { F4 = double.Parse(TempGV.Rows[GasIndex].Cells[14].Value.ToString()) + (double.Parse(TempGV.Rows[SteamIndex].Cells[15].Value.ToString()) / (double)2); }
        //            catch { F4 = 0; }
        //        }
        //        else F4 = 0;

        //    }
        //    //IF selected unit IsNOT CombinedCycle
        //    else
        //    {
        //        for (int j = 0; j < 24; j++)
        //        {
        //            if (SF)
        //                F1[j] = double.Parse(TempGV.Rows[index].Cells[11].Value.ToString()) * P[j] * P[j] + double.Parse(TempGV.Rows[index].Cells[12].Value.ToString()) * P[j] + double.Parse(TempGV.Rows[index].Cells[13].Value.ToString()) * V[j];
        //            else
        //                F1[j] = double.Parse(TempGV.Rows[index].Cells[8].Value.ToString()) * P[j] * P[j] + double.Parse(TempGV.Rows[index].Cells[9].Value.ToString()) * P[j] + double.Parse(TempGV.Rows[index].Cells[10].Value.ToString()) * V[j];
        //            F2[j] = double.Parse(TempGV.Rows[index].Cells[5].Value.ToString()) * V[j];
        //            F3[j] = double.Parse(TempGV.Rows[index].Cells[4].Value.ToString());
        //            if (V[j] == 0) F5[j] = 0;
        //            else
        //            {
        //                if (package.Contains("Steam")) F5[j] = double.Parse(TempGV.Rows[index].Cells[6].Value.ToString()) * P[j];
        //                else F5[j] = double.Parse(TempGV.Rows[index].Cells[7].Value.ToString()) * V[j];
        //            }
        //        }
        //        if (Minus > 1)
        //        {
        //            try { F4 = Minus * double.Parse(TempGV.Rows[index].Cells[15].Value.ToString()); }
        //            catch { F4 = 0; }
        //        }
        //        else if (Minus == 1)
        //        {
        //            try { F4 = double.Parse(TempGV.Rows[index].Cells[14].Value.ToString()); }
        //            catch { F4 = 0; }
        //        }
        //        else F4 = 0;
        //    }
        //    CostPayment = 0;
        //    for (int z = 0; z < 24; z++)
        //        CostPayment = CostPayment + F1[z] + F2[z] + F3[z] + F5[z];
        //    CostPayment = CostPayment + F4;
        //    return (CostPayment);
        //}
        ////---------------------------------isSecondFuel-----------------------
        //private bool isSecondFuel(int k, int index)
        //{
        //    string unit = TempGV.Rows[index].Cells[0].Value.ToString().Trim();
        //    string package = TempGV.Rows[index].Cells[1].Value.ToString().Trim();
        //    int Pcode = int.Parse(TempGV.Rows[index].Cells[2].Value.ToString());

        //    SqlCommand MyCom = new SqlCommand();
        //    MyCom.Connection = MyConnection;
        //    MyCom.CommandText = "SELECT @SF=SecondFuel FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@unit " +
        //    "AND PackageType=@type ";
        //    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
        //    MyCom.Parameters["@num"].Value = PPIDArray[k];
        //    MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
        //    MyCom.Parameters["@unit"].Value = unit;
        //    MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
        //    string type = package;
        //    if (type.Contains("CC")) type = "CC";
        //    MyCom.Parameters["@type"].Value = type;
        //    MyCom.Parameters.Add("@SF", SqlDbType.Bit);
        //    MyCom.Parameters["@SF"].Direction = ParameterDirection.Output;
        //    //try
        //    //{
        //    MyCom.ExecuteNonQuery();
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //string message = e.Message;
        //    //}
        //    bool SF;
        //    try { SF = bool.Parse(MyCom.Parameters["@SF"].Value.ToString()); }
        //    catch { SF = false; }
        //    return (SF);

        //}
        ////---------------------------------------SetAvailableCap----------------------------
        //private double SetAvailableCap(int k, string mydate, int index, string temp)
        //{
        //    double AvailableCapacity = 0;
        //    string unit = TempGV.Rows[index].Cells[0].Value.ToString().Trim();
        //    string package = TempGV.Rows[index].Cells[1].Value.ToString().Trim();
        //    int Pcode = int.Parse(TempGV.Rows[index].Cells[2].Value.ToString());

        //    SqlCommand MyCom = new SqlCommand();
        //    MyCom.CommandText = "SELECT  @Dispatch =SUM(Dispatchable) FROM [DetailFRM005] WHERE TargetMarketDate=@date " +
        //    "AND PPID=@num AND Block=@block";
        //    MyCom.Connection = MyConnection;

        //    MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
        //    MyCom.Parameters["@date"].Value = mydate;
        //    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
        //    MyCom.Parameters["@num"].Value = PPIDArray[k];
        //    MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
        //    MyCom.Parameters["@block"].Value = temp;
        //    MyCom.Parameters.Add("@Dispatch", SqlDbType.Real);
        //    MyCom.Parameters["@Dispatch"].Direction = ParameterDirection.Output;
        //    //try
        //    //{
        //    MyCom.ExecuteNonQuery();
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //string message = e.Message;
        //    //}
        //    try { AvailableCapacity = double.Parse(MyCom.Parameters["@Dispatch"].Value.ToString()); }
        //    catch { AvailableCapacity = 0; }
        //    return (AvailableCapacity);
        //}
        ////------------------------------------SetTotalPower--------------------------------
        //private double SetTotalPower(int k, string mydate, int index, string temp)
        //{
        //    double TotalPower = 0;
        //    string unit = TempGV.Rows[index].Cells[0].Value.ToString().Trim();
        //    string package = TempGV.Rows[index].Cells[1].Value.ToString().Trim();
        //    int Pcode = int.Parse(TempGV.Rows[index].Cells[2].Value.ToString());

        //    SqlCommand MyCom = new SqlCommand();
        //    MyCom.CommandText = "SELECT @Required=SUM(Required) FROM [DetailFRM005] WHERE TargetMarketDate=@date " +
        //    "AND PPID=@num AND Block=@block";
        //    MyCom.Connection = MyConnection;

        //    MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
        //    MyCom.Parameters["@date"].Value = mydate;
        //    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
        //    MyCom.Parameters["@num"].Value = PPIDArray[k];
        //    MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
        //    MyCom.Parameters["@block"].Value = temp;
        //    MyCom.Parameters.Add("@Required", SqlDbType.Real);
        //    MyCom.Parameters["@Required"].Direction = ParameterDirection.Output;
        //    //try
        //    //{
        //    MyCom.ExecuteNonQuery();
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //string message = e.Message;
        //    //}
        //    try { TotalPower = double.Parse(MyCom.Parameters["@Required"].Value.ToString()); }
        //    catch { TotalPower = 0; }
        //    if ((int.Parse(PPIDArray[k]) == 131) || (int.Parse(PPIDArray[k]) == 144))
        //    {
        //        if (package.Contains("CC")) CCUnitPower += TotalPower;
        //        else NoCCUnitPower += TotalPower;
        //    }
        //    else 
        //        NoCCUnitPower += TotalPower;
        //    return (TotalPower);
        //}
        ////--------------------------------------SetULPower------------------------------------
        //private double SetULPower(int k, string mydate, int index, string temp)
        //{
        //    double ULPower = 0;
        //    string unit = TempGV.Rows[index].Cells[0].Value.ToString().Trim();
        //    string package = TempGV.Rows[index].Cells[1].Value.ToString().Trim();
        //    int Pcode = int.Parse(TempGV.Rows[index].Cells[2].Value.ToString());

        //    SqlCommand MyCom = new SqlCommand();
        //    MyCom.CommandText = "SELECT  @UL =SUM(Required) FROM [DetailFRM005] WHERE TargetMarketDate=@date " +
        //    "AND PPID=@num AND Block=@block AND Contribution=@cont";
        //    MyCom.Connection = MyConnection;

        //    MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
        //    MyCom.Parameters["@date"].Value = mydate;
        //    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
        //    MyCom.Parameters["@num"].Value = PPIDArray[k];
        //    MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
        //    MyCom.Parameters["@block"].Value = temp;
        //    MyCom.Parameters.Add("@cont", SqlDbType.Char, 2);
        //    MyCom.Parameters["@cont"].Value = "UL";
        //    MyCom.Parameters.Add("@UL", SqlDbType.Real);
        //    MyCom.Parameters["@UL"].Direction = ParameterDirection.Output;
        //    //try
        //    //{
        //    MyCom.ExecuteNonQuery();
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //string message = e.Message;
        //    //}
        //    try { ULPower = double.Parse(MyCom.Parameters["@UL"].Value.ToString()); }
        //    catch { ULPower = 0; }

        //    return (ULPower);
        //}
    }
}