﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Auto_Update_Library;
using FarsiLibrary.Utils;
using System.Data.SqlClient;
using System.Threading;
using NRI.DMS.Common;
using System.Net;

namespace PowerPlantProject
{
    public partial class FormDownloadInterval : Form
    {
        string[] PPIDArray = new string[9];
        List<PersianDate> missingDates;
        string optionToDownload = "";
        int progressBarCounter;
        bool finish = true;


        public FormDownloadInterval()
        {
            InitializeComponent();
        }

        private void FormDownloadInterval_Load(object sender, EventArgs e)
        {
            InitializeComponents();
        }
        private void InitializeComponents()
        {

            datePickerFrom.SelectedDateTime = DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0, 0));
            datePickerTo.SelectedDateTime = DateTime.Now;
            cmbFiles.Items.Add(ItemsToBeDownloaded.M002);
            cmbFiles.Items.Add(ItemsToBeDownloaded.M005);
            cmbFiles.Items.Add("Internet");

            SqlConnection MyConnection = new SqlConnection(ConnectionManager.ConnectionString);
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT PPID FROM PowerPlant", MyConnection);
            Myda.Fill(MyDS, "ppid");
            MyConnection.Close();
            int i = 0;
            foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
            {
                PPIDArray[i] = MyRow["PPID"].ToString().Trim();
                i++;
            }
            PPIDArray[7] = "132";
            PPIDArray[8] = "145";
            MyConnection.Close();
        }

        private void ResetDownloadClass()
        {
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            string strCmd = "SELECT * FROM [Path]";
            Myda.SelectCommand = new SqlCommand(strCmd, new SqlConnection(ConnectionManager.ConnectionString));
            Myda.Fill(MyDS);
            Myda.SelectCommand.Connection.Close();
            if (MyDS.Tables[0].Rows.Count > 0)
            {
                DataRow row = MyDS.Tables[0].Rows[0];
                Download.GetInstance().ProxyAddress = row["Proxy"].ToString().Trim();
            }
            Download.GetInstance().ClearMessages();
        }

        private bool CheckDataSufficiency()
        {
            if (optionToDownload == "")
            {
                MessageBox.Show("You should choose an item to download");
                return false;
                //*****************
            }
            if (optionToDownload == "Internet")
                ResetDownloadClass();

            string errorMessage = "";

            switch (optionToDownload)
            {
                case "M002":
                    if (BaseData.GetInstance().M002Path == "")
                    {
                        MessageBox.Show("M002 path is not set...");
                        return false;
                    }
                    break;
                case "M005":
                    if (BaseData.GetInstance().M005Path == "")
                    {
                        MessageBox.Show("M005 path is not set...");
                        return false;
                    }
                    break;
                case "Internet":

                    if (Download.GetInstance().ProxyAddress == "")
                    {
                        MessageBox.Show("Proxy Address is not set...");
                        return false;
                    }
                    try
                    {
                        WebProxy proxy = new WebProxy(Download.GetInstance().ProxyAddress);
                        proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        WebClient client = new WebClient();
                        client.Proxy = proxy;
                    }
                    catch (Exception exp)
                    {
                        MessageBox.Show(exp.Message);
                        return false;
                    }
                    break;
            }
            return true;

        }
        private void btnDownload_Click(object sender, EventArgs e)
        {
            optionToDownload = cmbFiles.Text;

            if (CheckDataSufficiency())
            {

                finish = false;
                EnableForm();
                progressBarCounter = 0;

                missingDates = GetDatesBetween();
                progressBar1.Maximum = missingDates.Count;

                if (cmbFiles.Text == "Internet")
                {
                    progressBar1.Maximum = (progressBar1.Maximum * (Enum.GetNames(typeof(ItemsToBeDownloaded)).Length - 5)) + 2;
                }
                else
                    progressBar1.Maximum = progressBar1.Maximum * (PPIDArray.Length + 2);

                Thread thread = new Thread(LongTask);
                thread.IsBackground = true;
                thread.Start();
            }
            
        }

        private void EnableForm()
        {
            cmbFiles.Enabled = finish;
            datePickerFrom.Enabled = finish;
            datePickerTo.Enabled = finish;
            btnDownload.Visible = finish;
            progressBar1.Visible = !finish;
        }
        private void LongTask()
        {

            switch (optionToDownload)
            {
                //this.Cursor = Cursors.WaitCursor;

                case "M002":
                    foreach (PersianDate date in missingDates)
                        DownLoadInterface(ItemsToBeDownloaded.M002, date);
                    break;
                case "M005":
                    foreach (PersianDate date in missingDates)
                        DownLoadInterface(ItemsToBeDownloaded.M005, date);

                    break;

                case "Internet":

                    ////////////////////////////
                    DownLoadInterface(ItemsToBeDownloaded.LoadForcasting, null);
                    DownLoadInterface(ItemsToBeDownloaded.AveragePrice, null);
                    //////////////////////////
                    foreach (PersianDate date in missingDates)
                        foreach (ItemsToBeDownloaded item in Enum.GetValues(typeof(ItemsToBeDownloaded)))
                            if (item != ItemsToBeDownloaded.AveragePrice && item != ItemsToBeDownloaded.LoadForcasting &&
                                item != ItemsToBeDownloaded.Rep12Pages &&
                                item != ItemsToBeDownloaded.M005 && item != ItemsToBeDownloaded.M002)
                                DownLoadInterface(item, date);

                    break;

            }

            //this.Cursor = Cursors.Default;
            if (Download.GetInstance().Messages != "")
                MessageBoxDownload.Show("errors:",Download.GetInstance().Messages);
            else
                MessageBox.Show("Download Completed");
            finish = true;

            //EnableForm(true);

        }

        public void UpdateProgressBar(int i)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<int>(UpdateProgressBar), new object[] { i });
                return;
            }

            progressBar1.Value = i;
            Thread.Sleep(1);
        }

        private List<PersianDate> GetDatesBetween()
        {

            //if (strDateFrom == "")
            //{
            //    if (item == Items.Rep12Pages || item == Items.Outage ||
            //        item == Items.Manategh /*|| item == Items.EN */)
            //        strDateFrom = DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)).ToString("d");
            //}
            if (datePickerFrom != null)
            {
                List<PersianDate> missingDates = new List<PersianDate>();

                //DateTime dateFrom = (DateTime)PersianDateConverter.ToGregorianDateTime(new PersianDate(strDateFrom));
                DateTime dateFrom = datePickerFrom.SelectedDateTime;
                DateTime dateTo = datePickerTo.SelectedDateTime; ;
                missingDates.Add(dateFrom);

                TimeSpan span = dateTo - dateFrom;
                while (span.Days > 0)
                {
                    dateFrom = dateFrom.AddDays(1);
                    missingDates.Add(new PersianDate(dateFrom));
                    span = dateTo - dateFrom;
                }
                return missingDates;
            }
            else
                return null;
        }

        private void DownLoadInterface(ItemsToBeDownloaded item, PersianDate date)
        {

            string strDate = "";
            if (date != null)
                strDate= date.ToString("d");

            switch (item)
            {
                case ItemsToBeDownloaded.M005:
                    foreach (string PPID in PPIDArray)
                    {
                        Download.GetInstance().M005(strDate, PPID, 0);
                        UpdateProgressBar(++progressBarCounter);
                        if (PPID == "131" || PPID == "144")
                        {
                            Download.GetInstance().M005(strDate, PPID, 1);
                            UpdateProgressBar(++progressBarCounter);
                        }
                    }
                    break;

                case ItemsToBeDownloaded.M002:
                    foreach (string PPID in PPIDArray)
                    {
                        Download.GetInstance().M002(strDate, PPID, 0);
                        UpdateProgressBar(++progressBarCounter);
                        if (PPID == "131" || PPID == "144")
                        {
                            Download.GetInstance().M002(strDate, PPID, 1);
                            UpdateProgressBar(++progressBarCounter);
                        }
                    }
                    break;

                case ItemsToBeDownloaded.Rep12Pages:
                    Download.GetInstance().Rep12Page(strDate);
                    UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.AveragePrice:
                    Download.GetInstance().AveragePrice();
                    UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.Manategh:
                    Download.GetInstance().Manategh(strDate);
                    UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.InterchangedEnergy:
                    Download.GetInstance().InterchangedEnergy(strDate);
                    UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.lineNetComp:
                    Download.GetInstance().LineNetComp(strDate);
                    UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.RegionNetComp:
                    Download.GetInstance().RegionNetComp(strDate);
                    UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.UnitNetComp:
                    Download.GetInstance().UnitNetComp(strDate);
                    UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.LoadForcasting:
                    Download.GetInstance().LoadForecasting();
                    UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.Outage:
                    Download.GetInstance().Outage(strDate);
                    UpdateProgressBar(++progressBarCounter);
                    break;

                case ItemsToBeDownloaded.ProducedEenergy:
                    Download.GetInstance().ProducedEnergy(strDate);
                    UpdateProgressBar(++progressBarCounter);
                    break;

            }

        }

        private void cmbFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            //panelProxy.Visible = (cmbFiles.Text== "Internet");

        }

        private void FormDownloadInterval_Paint(object sender, PaintEventArgs e)
        {
            EnableForm();
        }
    }
}
