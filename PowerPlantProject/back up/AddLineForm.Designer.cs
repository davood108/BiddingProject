﻿namespace PowerPlantProject
{
    partial class AddLineForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ODSaveBtn = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ToBusTb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.LineLengthTb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.FromBusTB = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.LineCodeTb = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.CapacityTb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.StartTb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.EndTb = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // ODSaveBtn
            // 
            this.ODSaveBtn.BackColor = System.Drawing.Color.SkyBlue;
            this.ODSaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ODSaveBtn.Location = new System.Drawing.Point(97, 297);
            this.ODSaveBtn.Name = "ODSaveBtn";
            this.ODSaveBtn.Size = new System.Drawing.Size(75, 23);
            this.ODSaveBtn.TabIndex = 8;
            this.ODSaveBtn.Text = "Save";
            this.ODSaveBtn.UseVisualStyleBackColor = false;
            this.ODSaveBtn.Click += new System.EventHandler(this.ODSaveBtn_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SkyBlue;
            this.panel3.Controls.Add(this.ToBusTb);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(149, 82);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(118, 49);
            this.panel3.TabIndex = 37;
            // 
            // ToBusTb
            // 
            this.ToBusTb.BackColor = System.Drawing.Color.White;
            this.ToBusTb.Location = new System.Drawing.Point(9, 24);
            this.ToBusTb.Name = "ToBusTb";
            this.ToBusTb.Size = new System.Drawing.Size(99, 20);
            this.ToBusTb.TabIndex = 3;
            this.ToBusTb.Validated += new System.EventHandler(this.ToBusTb_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Window;
            this.label3.Location = new System.Drawing.Point(33, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "To Bus";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Controls.Add(this.LineLengthTb);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(12, 149);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(118, 49);
            this.panel2.TabIndex = 36;
            // 
            // LineLengthTb
            // 
            this.LineLengthTb.BackColor = System.Drawing.Color.White;
            this.LineLengthTb.Location = new System.Drawing.Point(10, 23);
            this.LineLengthTb.Name = "LineLengthTb";
            this.LineLengthTb.Size = new System.Drawing.Size(99, 20);
            this.LineLengthTb.TabIndex = 4;
            this.LineLengthTb.Validated += new System.EventHandler(this.LineLengthTb_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Window;
            this.label2.Location = new System.Drawing.Point(16, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Line Length";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SkyBlue;
            this.panel1.Controls.Add(this.FromBusTB);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 82);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(118, 49);
            this.panel1.TabIndex = 35;
            // 
            // FromBusTB
            // 
            this.FromBusTB.BackColor = System.Drawing.Color.White;
            this.FromBusTB.Location = new System.Drawing.Point(10, 23);
            this.FromBusTB.Name = "FromBusTB";
            this.FromBusTB.Size = new System.Drawing.Size(99, 20);
            this.FromBusTB.TabIndex = 2;
            this.FromBusTB.Validated += new System.EventHandler(this.FromBusTB_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Window;
            this.label1.Location = new System.Drawing.Point(27, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "From Bus";
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.SkyBlue;
            this.panel27.Controls.Add(this.LineCodeTb);
            this.panel27.Controls.Add(this.label31);
            this.panel27.Location = new System.Drawing.Point(75, 12);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(122, 49);
            this.panel27.TabIndex = 34;
            // 
            // LineCodeTb
            // 
            this.LineCodeTb.BackColor = System.Drawing.Color.White;
            this.LineCodeTb.Location = new System.Drawing.Point(12, 22);
            this.LineCodeTb.Name = "LineCodeTb";
            this.LineCodeTb.Size = new System.Drawing.Size(99, 20);
            this.LineCodeTb.TabIndex = 1;
            this.LineCodeTb.Validated += new System.EventHandler(this.LineCodeTb_Validated);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.SystemColors.Window;
            this.label31.Location = new System.Drawing.Point(33, 4);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(64, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "Line Code";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.SkyBlue;
            this.panel4.Controls.Add(this.CapacityTb);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(149, 149);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(118, 49);
            this.panel4.TabIndex = 40;
            // 
            // CapacityTb
            // 
            this.CapacityTb.BackColor = System.Drawing.Color.White;
            this.CapacityTb.Location = new System.Drawing.Point(9, 24);
            this.CapacityTb.Name = "CapacityTb";
            this.CapacityTb.Size = new System.Drawing.Size(99, 20);
            this.CapacityTb.TabIndex = 5;
            this.CapacityTb.Validated += new System.EventHandler(this.CapacityTb_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Window;
            this.label4.Location = new System.Drawing.Point(29, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Capacity";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.SkyBlue;
            this.panel5.Controls.Add(this.StartTb);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Location = new System.Drawing.Point(12, 216);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(118, 49);
            this.panel5.TabIndex = 41;
            // 
            // StartTb
            // 
            this.StartTb.BackColor = System.Drawing.Color.White;
            this.StartTb.Location = new System.Drawing.Point(9, 24);
            this.StartTb.Name = "StartTb";
            this.StartTb.Size = new System.Drawing.Size(99, 20);
            this.StartTb.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.Window;
            this.label5.Location = new System.Drawing.Point(3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Genco Start Owner";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.SkyBlue;
            this.panel6.Controls.Add(this.EndTb);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Location = new System.Drawing.Point(149, 216);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(118, 49);
            this.panel6.TabIndex = 42;
            // 
            // EndTb
            // 
            this.EndTb.BackColor = System.Drawing.Color.White;
            this.EndTb.Location = new System.Drawing.Point(9, 24);
            this.EndTb.Name = "EndTb";
            this.EndTb.Size = new System.Drawing.Size(99, 20);
            this.EndTb.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.Window;
            this.label6.Location = new System.Drawing.Point(4, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Genco End Owner";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PowerPlantProject.Properties.Resources._10;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(292, 334);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.ODSaveBtn);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel27);
            this.MaximizeBox = false;
            this.Name = "Form5";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add New Line";
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ODSaveBtn;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox ToBusTb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox LineLengthTb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox FromBusTB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.TextBox LineCodeTb;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox CapacityTb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox StartTb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox EndTb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}