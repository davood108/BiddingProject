﻿namespace PowerPlantProject
{
    partial class MainForm
    {
        //-------------------------------ReseTVariables------------------------------------
        private void ResetVariables()
        {
            PAvailableCapacity = 0;
            PTotalPower = 0;
            PULPower = 0;
            PBidPower = 0;
            PIncrementPower = 0;
            PDecreasePower = 0;
            PCapacityPayment = 0;
            PBidPayment = 0;
            PULPayment = 0;
            PIncrementPayment = 0;
            PDecreasePayment = 0;
            PEnergyPayment = 0;
            PIncome = 0;
            PCost = 0;
            PBenefit = 0;
            CCTotalPower = 0;
            NoCCTotalPower = 0;
            CCIncPower = 0;
            NoCCIncPower = 0;
            CCDecPower = 0;
            NoCCDecPower = 0;
            CCUnitPower = 0;
            NoCCUnitPower = 0;
        }
    }
}