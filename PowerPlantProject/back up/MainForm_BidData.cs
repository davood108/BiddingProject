﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.DMS.Common;

namespace PowerPlantProject
{
    partial class MainForm
    {
        //-------------------------------FillBDUnitGrid-----------------------------------
        private void FillBDUnitGrid()
        {
            BDCurGrid.DataSource = null;
            if (BDCurGrid.Rows != null) BDCurGrid.Rows.Clear();

            if (!BDCal.IsNull)
            {
                string unit = BDUnitLb.Text.Trim();
                string temp = unit;
                string package = BDPackLb.Text.Trim();
                //Get Index Of Selected Unit(Row) in GridView (In Generad data Tab)
                int index = 0;
                if (GDSteamGroup.Text.Contains(package))
                    for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                    {
                        if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                            index = i;
                    }
                else
                    for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                    {
                        if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                            index = i;
                    }

                //Build the Bolck for FRM002
                temp = temp.ToLower();
                if (package.Contains("Combined"))
                {
                    string packagecode = "";
                    if (GDGasGroup.Text.Contains("Combined"))
                        packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                    else packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                    temp = "C" + packagecode;
                }
                else
                {
                    if (temp.Contains("gas"))
                        temp = temp.Replace("gas", "G");
                    else if (temp.Contains("steam"))
                        temp = temp.Replace("steam", "S");
                }
                temp = temp.Trim();

                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                DataSet PowerDS = new DataSet();
                SqlDataAdapter Powerda = new SqlDataAdapter();

                Powerda.SelectCommand = new SqlCommand("SELECT Hour,Power1,Price1,Power2,Price2,Power3,Price3,Power4," +
                "Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10 " +
                "FROM [DetailFRM002] WHERE TargetMarketDate=@date AND Block=@temp AND PPID=" + PPID, myConnection);
                Powerda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Powerda.SelectCommand.Parameters["@date"].Value = BDCal.Text;
                Powerda.SelectCommand.Parameters.Add("@temp", SqlDbType.NChar, 20);
                Powerda.SelectCommand.Parameters["@temp"].Value = temp;
                Powerda.Fill(PowerDS);
                BDCurGrid.DataSource = PowerDS.Tables[0].DefaultView;
                myConnection.Close();
            }
        }
        //---------------------------FillBDUnit---------------------------------------
        private void FillBDUnit(string unit, string package, int index)
        {
            BDStateTb.Text = MRUnitStateTb.Text;
            BDPowerTb.Text = MRUnitPowerTb.Text;
            BDMaxBidTb.Text = MRUnitMaxBidTb.Text;

            if (!BDCal.IsNull) FillBDUnitGrid();
        }

    }
}