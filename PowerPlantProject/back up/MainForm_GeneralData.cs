﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.DMS.Common;

namespace PowerPlantProject
{
    partial class MainForm
    {
        //------------------------------FillTransmissionGrid--------------------------------------
        private void FillTransmissionGrid(string Num)
        {
            //Clear Grids
            //PlantGV1.DataSource = null;
            //if (PlantGV1.Rows != null) PlantGV1.Rows.Clear();
            //PlantGV2.DataSource = null;
            //if (PlantGV2.Rows != null) PlantGV2.Rows.Clear();

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();

            //detect current date and hour
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            Myda.SelectCommand = new SqlCommand("SELECT LineCode,FromBus,ToBus,LineLength,Owner_GencoFrom,Owner_GencoTo" +
            ",RateA,OutService,OutServiceStartDate,OutServiceEndDate FROM [Lines] WHERE LineNumber=" + Num, myConnection);
            Myda.Fill(MyDS);
            myConnection.Close();

            if (Num.Contains("400"))
            {
                //for (int i = 0; i < 7; i++)
                //Grid400.Rows[Grid400.RowCount - 1].Cells[i].ReadOnly = false;
                Grid400.DataSource = MyDS.Tables[0].DefaultView;
                //Grid400.ReadOnly = true;
                int index = 0;
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    Grid400.Rows[index].Cells[7].Value = "Work";
                    Grid400.Rows[index].Cells[Grid400.ColumnCount - 1].ReadOnly = false;
                    if (MyRow[7].ToString() == "True")
                        if (CheckDate(mydate, MyRow[8].ToString(), MyRow[9].ToString()))
                            Grid400.Rows[index].Cells[7].Value = "OutService";

                    index++;
                }
                Grid400.Rows[Grid400.RowCount - 1].Cells[Grid400.ColumnCount - 1].ReadOnly = true;
            }
            else
            {
                Grid230.DataSource = MyDS.Tables[0].DefaultView;
                //for (int i = 0; i < 7; i++)
                //Grid230.Rows[Grid230.RowCount - 1].Cells[i].ReadOnly = false;
                //Grid230.ReadOnly = true;
                int index = 0;
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    Grid230.Rows[index].Cells[7].Value = "Work";
                    Grid230.Rows[index].Cells[Grid230.ColumnCount - 1].ReadOnly = false;
                    if (MyRow[7].ToString() == "True")
                        if (CheckDate(mydate, MyRow[8].ToString(), MyRow[9].ToString()))
                            Grid230.Rows[index].Cells[7].Value = "OutService";

                    index++;
                }
                Grid230.Rows[Grid230.RowCount - 1].Cells[Grid230.ColumnCount - 1].ReadOnly = true;
            }

        }
        //-------------------------FillPlantGrid---------------------------------------
        private void FillPlantGrid(string Num)
        {
            //Clear Grids
            //PlantGV1.DataSource = null;
            //if (PlantGV1.Rows != null) PlantGV1.Rows.Clear();
            //PlantGV2.DataSource = null;
            //if (PlantGV2.Rows != null) PlantGV2.Rows.Clear();

            Currentgb.Text = "CURRENT STATE";
            //Detect Farsi Date
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");
            int myhour = DateTime.Now.Hour;

            //detect the type of packages(Data GridView Labels)

            DataSet dataDS = new DataSet();
            SqlDataAdapter dataDA = new SqlDataAdapter();
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            dataDA.SelectCommand = new SqlCommand("SELECT PackageType FROM PPUnit WHERE PPID=" + Num, myConnection);
            dataDA.Fill(dataDS);
            int count = 0;
            GDGasGroup.Visible = false;
            GDSteamGroup.Visible = false;
            foreach (DataRow MyRow in dataDS.Tables[0].Rows)
            {
                if (count == 0)
                {
                    GDGasGroup.Visible = true;
                    GDGasGroup.Text = MyRow["PackageType"].ToString();
                    string type = MyRow["PackageType"].ToString();
                    type = type.Trim();
                    if (type == "Combined Cycle") type = "CC";
                    PlantGV1.Visible = true;
                    DataSet MyDS = new DataSet();
                    SqlDataAdapter Myda = new SqlDataAdapter();
                    Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageCode," +
                    "UnitType,Capacity,PMin,PMax FROM [UnitsDataMain] WHERE PPID=" + Num + "AND (PackageType LIKE '" + type + "%')", myConnection);
                    Myda.Fill(MyDS);
                    PlantGV1.DataSource = MyDS.Tables[0].DefaultView;
                    PlantGV1.Sort(PlantGV1.Columns[1], ListSortDirection.Ascending);
                    FillPlantGV1Remained(mydate, Num, type, myhour);
                    GDSteamGroup.Text = "";
                }
                else if (count == 1)
                {
                    GDSteamGroup.Visible = true;
                    GDSteamGroup.Text = MyRow["PackageType"].ToString();
                    string type = MyRow["PackageType"].ToString();
                    type = type.Trim();
                    if (type == "Combined Cycle") type = "CC";
                    PlantGV2.Visible = true;
                    DataSet SteamDS = new DataSet();
                    SqlDataAdapter Steamda = new SqlDataAdapter();
                    Steamda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageCode," +
                    "UnitType,Capacity,PMin,PMax FROM [UnitsDataMain] WHERE PPID=" + Num + "AND (PackageType LIKE '" + type + "%')", myConnection);
                    Steamda.Fill(SteamDS);
                    PlantGV2.DataSource = SteamDS.Tables[0].DefaultView;
                    PlantGV2.Sort(PlantGV2.Columns[1], ListSortDirection.Ascending);
                    FillPlantGV2Remained(mydate, Num, type, myhour);
                }
                count++;
            }
            myConnection.Close();

            if (count == 1) GDSteamGroup.Visible = false;

            MRLabel1.Text = "On Units";
            MRLabel2.Text = "Power";
            Grid230.Visible = false;
            Grid400.Visible = false;
        }
        //----------------------------FillPlantGV1Remained---------------------------
        private void FillPlantGV1Remained(string mydate, string Num, string type, int myhour)
        {
            PlantGV1.Rows[PlantGV1.RowCount - 1].Cells[PlantGV1.ColumnCount - 1].ReadOnly = true;

            //for (int i = 0; i < 3; i++)
            //PlantGV1.Rows[PlantGV1.RowCount - 1].Cells[i].ReadOnly = false;
            for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
            {
                PlantGV1.Rows[i].Cells[PlantGV1.ColumnCount - 1].ReadOnly = false;
                SqlCommand MyCom = new SqlCommand();

                MyCom.CommandText = "SELECT  @md1 =MaintenanceStartDate,@md2=MaintenanceEndDate,@mt=MaintenanceType " +
                "FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type SELECT  " +
                "@fd1 =SecondFuelStartDate,@fd2=SecondFuelEndDate FROM [ConditionUnit] WHERE PPID=@num AND " +
                "UnitCode=@uc AND PackageType=@type SELECT  @osd1 =OutServiceStartDate,@osd2=OutServiceEndDate " +
                "FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type SELECT @re=COUNT(PPID) " +
                "FROM [DetailFRM005] WHERE TargetMarketDate=@date AND PPID=@num AND Block=@block AND Hour=@hour AND Required<>0" +
                "SELECT @sf=SecondFuel FROM [UnitsDataMain] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type";

                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                MyCom.Connection = myConnection;

                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = mydate;
                MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                MyCom.Parameters["@num"].Value = Num;
                MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                //detect unit name
                string temp = PlantGV1.Rows[i].Cells[0].Value.ToString();
                temp = temp.ToLower();
                if (type == "CC")
                {
                    int x = int.Parse(Num);
                    if ((x == 131) || (x == 144)) x++;
                    temp = x + "-" + "C" + PlantGV1.Rows[i].Cells[1].Value.ToString();
                }
                else if (temp.Contains("gas"))
                {
                    temp = temp.Replace("gas", "G");
                    temp = Num + "-" + temp;
                }
                else
                {
                    temp = temp.Replace("steam", "S");
                    temp = Num + "-" + temp;
                }
                MyCom.Parameters["@block"].Value = temp;
                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                MyCom.Parameters["@hour"].Value = myhour;
                MyCom.Parameters.Add("@uc", SqlDbType.NChar, 20);
                MyCom.Parameters["@uc"].Value = PlantGV1.Rows[i].Cells[0].Value.ToString();
                MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
                MyCom.Parameters["@type"].Value = type;

                MyCom.Parameters.Add("@md1", SqlDbType.Char, 10);
                MyCom.Parameters["@md1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@md2", SqlDbType.Char, 10);
                MyCom.Parameters["@md2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@mt", SqlDbType.NChar, 12);
                MyCom.Parameters["@mt"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@fd1", SqlDbType.Char, 10);
                MyCom.Parameters["@fd1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@fd2", SqlDbType.Char, 10);
                MyCom.Parameters["@fd2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@osd1", SqlDbType.Char, 10);
                MyCom.Parameters["@osd1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@osd2", SqlDbType.Char, 10);
                MyCom.Parameters["@osd2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@re", SqlDbType.Int);
                MyCom.Parameters["@re"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@sf", SqlDbType.NChar, 10);
                MyCom.Parameters["@sf"].Direction = ParameterDirection.Output;

                MyCom.ExecuteNonQuery();

                myConnection.Close();

                string md1, md2, fd1, fd2, osd1, osd2, sf, mt;
                md1 = MyCom.Parameters["@md1"].Value.ToString();
                md2 = MyCom.Parameters["@md2"].Value.ToString();
                fd1 = MyCom.Parameters["@fd1"].Value.ToString();
                fd2 = MyCom.Parameters["@fd2"].Value.ToString();
                osd1 = MyCom.Parameters["@osd1"].Value.ToString();
                osd2 = MyCom.Parameters["@osd2"].Value.ToString();
                sf = MyCom.Parameters["@sf"].Value.ToString();
                mt = MyCom.Parameters["@mt"].Value.ToString();
                int result = int.Parse(MyCom.Parameters["@re"].Value.ToString());

                if (result != 0) PlantGV1.Rows[i].Cells[7].Value = "ON";
                else PlantGV1.Rows[i].Cells[7].Value = "OFF";
                if (CheckDate(mydate, fd1, fd2)) PlantGV1.Rows[i].Cells[6].Value = sf;
                else PlantGV1.Rows[i].Cells[6].Value = "Gas";
                if (CheckDate(mydate, md1, md2)) PlantGV1.Rows[i].Cells[8].Value = mt;
                else PlantGV1.Rows[i].Cells[8].Value = "No";
                if (CheckDate(mydate, osd1, osd2)) PlantGV1.Rows[i].Cells[9].Value = "Yes";
                else PlantGV1.Rows[i].Cells[9].Value = "No";
            }

        }
        //----------------------------FillPlantGV2Remained---------------------------
        private void FillPlantGV2Remained(string mydate, string Num, string type, int myhour)
        {
            PlantGV2.Rows[PlantGV2.RowCount - 1].Cells[PlantGV2.ColumnCount - 1].ReadOnly = true;

            //for (int i = 0; i < 3; i++)
            //PlantGV2.Rows[PlantGV2.RowCount - 1].Cells[i].ReadOnly = false;
            for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
            {
                PlantGV2.Rows[i].Cells[PlantGV2.ColumnCount - 1].ReadOnly = false;
                SqlCommand MyCom = new SqlCommand();

                MyCom.CommandText = "SELECT  @md1 =MaintenanceStartDate,@md2=MaintenanceEndDate,@mt=MaintenanceType " +
                "FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type SELECT  " +
                "@fd1 =SecondFuelStartDate,@fd2=SecondFuelEndDate FROM [ConditionUnit] WHERE PPID=@num AND " +
                "UnitCode=@uc AND PackageType=@type SELECT  @osd1 =OutServiceStartDate,@osd2=OutServiceEndDate " +
                "FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type SELECT @re=COUNT(PPID) " +
                "FROM [DetailFRM005] WHERE TargetMarketDate=@date AND PPID=@num AND Block=@block AND Hour=@hour AND Required<>0" +
                "SELECT @sf=SecondFuel FROM [UnitsDataMain] WHERE PPID=@num AND UnitCode=@uc AND PackageType=@type";
                MyCom.Connection = new SqlConnection(ConnectionManager.ConnectionString);
                MyCom.Connection.Open();

                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = mydate;
                MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                MyCom.Parameters["@num"].Value = Num;
                MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                //detect unit name
                string temp = PlantGV2.Rows[i].Cells[0].Value.ToString();
                temp = temp.ToLower();
                if (type == "CC")
                {
                    int x = int.Parse(Num);
                    if ((x == 131) || (x == 144)) x++;
                    temp = x + "-" + "C" + PlantGV2.Rows[i].Cells[1].Value.ToString();
                }
                else if (temp.Contains("gas"))
                {
                    temp = temp.Replace("gas", "G");
                    temp = Num + "-" + temp;
                }
                else
                {
                    temp = temp.Replace("steam", "S");
                    temp = Num + "-" + temp;
                }

                MyCom.Parameters["@block"].Value = temp;
                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                MyCom.Parameters["@hour"].Value = myhour;
                MyCom.Parameters.Add("@uc", SqlDbType.NChar, 20);
                MyCom.Parameters["@uc"].Value = PlantGV2.Rows[i].Cells[0].Value.ToString();
                MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
                MyCom.Parameters["@type"].Value = type;

                MyCom.Parameters.Add("@md1", SqlDbType.Char, 10);
                MyCom.Parameters["@md1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@md2", SqlDbType.Char, 10);
                MyCom.Parameters["@md2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@mt", SqlDbType.NChar, 12);
                MyCom.Parameters["@mt"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@fd1", SqlDbType.Char, 10);
                MyCom.Parameters["@fd1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@fd2", SqlDbType.Char, 10);
                MyCom.Parameters["@fd2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@osd1", SqlDbType.Char, 10);
                MyCom.Parameters["@osd1"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@osd2", SqlDbType.Char, 10);
                MyCom.Parameters["@osd2"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@re", SqlDbType.Int);
                MyCom.Parameters["@re"].Direction = ParameterDirection.Output;
                MyCom.Parameters.Add("@sf", SqlDbType.NChar, 10);
                MyCom.Parameters["@sf"].Direction = ParameterDirection.Output;

                MyCom.ExecuteNonQuery();

                MyCom.Connection.Close();

                string md1, md2, fd1, fd2, osd1, osd2, sf, mt;
                md1 = MyCom.Parameters["@md1"].Value.ToString();
                md2 = MyCom.Parameters["@md2"].Value.ToString();
                fd1 = MyCom.Parameters["@fd1"].Value.ToString();
                fd2 = MyCom.Parameters["@fd2"].Value.ToString();
                osd1 = MyCom.Parameters["@osd1"].Value.ToString();
                osd2 = MyCom.Parameters["@osd2"].Value.ToString();
                sf = MyCom.Parameters["@sf"].Value.ToString();
                mt = MyCom.Parameters["@mt"].Value.ToString();
                int result = int.Parse(MyCom.Parameters["@re"].Value.ToString());

                if (result != 0) PlantGV2.Rows[i].Cells[7].Value = "ON";
                else PlantGV2.Rows[i].Cells[7].Value = "OFF";
                if (CheckDate(mydate, fd1, fd2)) PlantGV2.Rows[i].Cells[6].Value = sf;
                else PlantGV2.Rows[i].Cells[6].Value = "Gas";
                if (CheckDate(mydate, md1, md2)) PlantGV2.Rows[i].Cells[8].Value = mt;
                else PlantGV2.Rows[i].Cells[8].Value = "No";
                if (CheckDate(mydate, osd1, osd2)) PlantGV2.Rows[i].Cells[9].Value = "Yes";
                else PlantGV2.Rows[i].Cells[9].Value = "No";
            }
        }
        //-----------------------------ClearGDUnitTab()------------------------------
        private void ClearGDUnitTab()
        {
            GDcapacityTB.Text = "";
            GDPmaxTB.Text = "";
            GDPminTB.Text = "";
            GDTimeUpTB.Text = "";
            GDTimeDownTB.Text = "";
            GDTimeColdStartTB.Text = "";
            GDTimeHotStartTB.Text = "";
            GDRampRateTB.Text = "";
            GDIntConsumeTB.Text = "";
            GDStateTB.Text = "";
            GDFuelTB.Text = "";
            GDMainTypeTB.Text = "";
            GDStartDateCal.IsNull = true;
            GDEndDateCal.IsNull = true;
        }
        //-------------------------FillGDUnit----------------------------------------
        private void FillGDUnit(string unit, string package, int index)
        {
            ClearGDUnitTab();
            string ABpackage = package;
            if (ABpackage.Contains("Combined"))
                ABpackage = "CC";

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT TUp,TDown,TStartCold,TStartHot," +
            "RampUpRate,InternalConsume FROM [UnitsDataMain] WHERE PPID=" + PPID + " AND UnitCode LIKE '" + unit + "%'", myConnection);
            Myda.Fill(MyDS);
            foreach (DataRow MyRow in MyDS.Tables[0].Rows)
            {
                GDTimeUpTB.Text = MyRow[0].ToString();
                GDTimeDownTB.Text = MyRow[1].ToString();
                GDTimeColdStartTB.Text = MyRow[2].ToString();
                GDTimeHotStartTB.Text = MyRow[3].ToString();
                GDRampRateTB.Text = MyRow[4].ToString();
                GDIntConsumeTB.Text = MyRow[5].ToString();
            }

            //Detect Farsi Date
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");
            int myhour = DateTime.Now.Hour;

            //SqlCommand MyCom = new SqlCommand();
            //MyCom.Connection = MyConnection;
            //MyCom.CommandText = "SELECT @IntCon=InternalConsume FROM EconomicUnit WHERE PPID=@num AND " +
            //"UnitCode=@unit AND Date=@date";
            //MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            //MyCom.Parameters["@num"].Value = PPID;
            //MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
            //string myunit = unit;
            //if (package.Contains("Combined"))
            //{
            //    string packagecode = "";
            //    if (GDSteamGroup.Text.Contains(package))
            //        packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
            //    else packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
            //    myunit = "C" + packagecode;
            //}
            //MyCom.Parameters["@unit"].Value = myunit;
            //MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            //MyCom.Parameters["@date"].Value = mydate;
            //MyCom.Parameters.Add("@IntCon", SqlDbType.Real);
            //MyCom.Parameters["@IntCon"].Direction = ParameterDirection.Output;
            ////try
            ////{
            //MyCom.ExecuteNonQuery();
            ////}
            ////catch (Exception exp)
            ////{
            ////  string str = exp.Message;
            ////}
            if (package.Contains("Combined")) package = "Combined Cycle";
            if (GDGasGroup.Text.Contains(package))
            {
                GDcapacityTB.Text = PlantGV1.Rows[index].Cells[3].Value.ToString();
                GDPminTB.Text = PlantGV1.Rows[index].Cells[4].Value.ToString();
                GDPmaxTB.Text = PlantGV1.Rows[index].Cells[5].Value.ToString();
                GDStateTB.Text = PlantGV1.Rows[index].Cells[7].Value.ToString();
                GDFuelTB.Text = PlantGV1.Rows[index].Cells[6].Value.ToString();
                GDMainTypeTB.Text = PlantGV1.Rows[index].Cells[8].Value.ToString();
            }
            else if (GDSteamGroup.Text.Contains(package))
            {
                GDcapacityTB.Text = PlantGV2.Rows[index].Cells[3].Value.ToString();
                GDPminTB.Text = PlantGV2.Rows[index].Cells[4].Value.ToString();
                GDPmaxTB.Text = PlantGV2.Rows[index].Cells[5].Value.ToString();
                GDStateTB.Text = PlantGV2.Rows[index].Cells[7].Value.ToString();
                GDFuelTB.Text = PlantGV2.Rows[index].Cells[6].Value.ToString();
                GDMainTypeTB.Text = PlantGV2.Rows[index].Cells[8].Value.ToString();
            }

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            MyCom.CommandText = "SELECT  @m=Maintenance, @md1 =MaintenanceStartDate,@md2=MaintenanceEndDate " +
                "FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@unit AND PackageType=@package";
            MyCom.Parameters.Add("@package", SqlDbType.NChar, 10);
            string type = package;
            if (package.Contains("Combined")) type = "CC";
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPID;
            MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
            string myunit = unit;
            if (package.Contains("Combined"))
            {
                string packagecode = "";
                if (GDSteamGroup.Text.Contains(package))
                    packagecode = PlantGV2.Rows[index].Cells[1].Value.ToString();
                else packagecode = PlantGV1.Rows[index].Cells[1].Value.ToString();
                myunit = "C" + packagecode;
            }
            MyCom.Parameters["@unit"].Value = myunit;
            MyCom.Parameters["@package"].Value = type;
            MyCom.Parameters.Add("@m", SqlDbType.Bit);
            MyCom.Parameters["@m"].Direction = ParameterDirection.Output;
            MyCom.Parameters.Add("@md1", SqlDbType.Char, 10);
            MyCom.Parameters["@md1"].Direction = ParameterDirection.Output;
            MyCom.Parameters.Add("@md2", SqlDbType.Char, 10);
            MyCom.Parameters["@md2"].Direction = ParameterDirection.Output;
            //try
            //{
            MyCom.ExecuteNonQuery();
            //}
            //catch (Exception exp)
            //{
            //  string str = exp.Message;
            //}

            myConnection.Close();
            if (MyCom.Parameters["@m"].Value.ToString() != "")
                if (bool.Parse(MyCom.Parameters["@m"].Value.ToString()))
                {
                    GDStartDateCal.Text = MyCom.Parameters["@md1"].Value.ToString();
                    GDEndDateCal.Text = MyCom.Parameters["@md2"].Value.ToString();
                }
        }
        //----------------------------------FillGDLine---------------------------------------
        private void FillGDLine(string TransType, string TransLine)
        {
            ClearGDUnitTab();

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT FromBus,ToBus,CircuitID,LineR,LineX,LineSuseptance," +
            "LineLength,OutService,OutServiceStartDate,OutServiceEndDate,Owner_GencoFrom,Owner_GencoTo,PercentOwnerGencoFrom," +
            "PercentOwnerGencoTo,RateA,OutServiceType FROM [Lines] WHERE LineNumber=@num AND LineCode=@code", myConnection);
            Myda.SelectCommand.Parameters.Add("@num", SqlDbType.Int);
            Myda.SelectCommand.Parameters["@num"].Value = TransType;
            Myda.SelectCommand.Parameters.Add("@code", SqlDbType.NChar, 20);
            Myda.SelectCommand.Parameters["@code"].Value = TransLine;
            Myda.Fill(MyDS);
            myConnection.Close();

            foreach (DataRow MyRow in MyDS.Tables[0].Rows)
            {
                GDcapacityTB.Text = MyRow[14].ToString();
                GDPmaxTB.Text = MyRow[0].ToString();
                GDPminTB.Text = MyRow[1].ToString();
                GDTimeUpTB.Text = MyRow[2].ToString();
                GDTimeDownTB.Text = MyRow[3].ToString();
                GDTimeColdStartTB.Text = MyRow[4].ToString();
                GDTimeHotStartTB.Text = MyRow[5].ToString();
                GDRampRateTB.Text = MyRow[6].ToString();
                GDIntConsumeTB.Text = "Work";
                string temp = MyRow[7].ToString();
                if (MyRow[7].ToString() == "True")
                {
                    //Detect Farsi Date

                    PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
                    string mydate = prDate.ToString("d");
                    int myhour = DateTime.Now.Hour;

                    GDStartDateCal.Text = MyRow[8].ToString();
                    GDEndDateCal.Text = MyRow[9].ToString();
                    if (CheckDate(mydate, GDStartDateCal.Text, GDEndDateCal.Text))
                        GDIntConsumeTB.Text = "OutService";
                    GDMainTypeTB.Text = MyRow[15].ToString();
                }
                string from = MyRow[10].ToString().ToLower();
                string to = MyRow[11].ToString().ToLower();
                if (!from.Contains("tehran"))
                {
                    GDStateTB.Text = MyRow[10].ToString();
                    GDFuelTB.Text = MyRow[12].ToString();
                }
                else if (!to.Contains("tehran"))
                {
                    GDStateTB.Text = MyRow[11].ToString();
                    GDFuelTB.Text = MyRow[13].ToString();
                }
                else
                {
                    GDStateTB.Text = "Tehran";
                    GDFuelTB.Text = "100";
                }

            }
        }

    }
}