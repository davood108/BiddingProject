﻿namespace PowerPlantProject
{
    partial class AddPlantForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel27 = new System.Windows.Forms.Panel();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.PackageType1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.PackageType2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ODSaveBtn = new System.Windows.Forms.Button();
            this.PackageTypeValid = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtPersianName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel27.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.SkyBlue;
            this.panel27.Controls.Add(this.txtPersianName);
            this.panel27.Controls.Add(this.label31);
            this.panel27.Location = new System.Drawing.Point(114, 10);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(125, 49);
            this.panel27.TabIndex = 6;
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.Color.White;
            this.txtName.Location = new System.Drawing.Point(13, 22);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(99, 20);
            this.txtName.TabIndex = 1;
            this.txtName.Validated += new System.EventHandler(this.PlanNameTb_Validated);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.SystemColors.Window;
            this.label31.Location = new System.Drawing.Point(3, 5);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(118, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "Plant Persian Name";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SkyBlue;
            this.panel1.Controls.Add(this.txtCode);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(183, 71);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(125, 49);
            this.panel1.TabIndex = 7;
            // 
            // txtCode
            // 
            this.txtCode.BackColor = System.Drawing.Color.White;
            this.txtCode.Location = new System.Drawing.Point(13, 22);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(99, 20);
            this.txtCode.TabIndex = 2;
            this.txtCode.Validated += new System.EventHandler(this.PlantCodeTb_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Window;
            this.label1.Location = new System.Drawing.Point(24, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Plant Code";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Controls.Add(this.PackageType1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(44, 132);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(125, 49);
            this.panel2.TabIndex = 8;
            // 
            // PackageType1
            // 
            this.PackageType1.FormattingEnabled = true;
            this.PackageType1.Items.AddRange(new object[] {
            "Combined Cycle",
            "Gas",
            "Steam"});
            this.PackageType1.Location = new System.Drawing.Point(13, 23);
            this.PackageType1.Name = "PackageType1";
            this.PackageType1.Size = new System.Drawing.Size(99, 21);
            this.PackageType1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Window;
            this.label2.Location = new System.Drawing.Point(7, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Package Type1";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SkyBlue;
            this.panel3.Controls.Add(this.PackageType2);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(182, 132);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(125, 49);
            this.panel3.TabIndex = 9;
            // 
            // PackageType2
            // 
            this.PackageType2.FormattingEnabled = true;
            this.PackageType2.Items.AddRange(new object[] {
            "Combined Cycle",
            "Gas",
            "Steam"});
            this.PackageType2.Location = new System.Drawing.Point(13, 22);
            this.PackageType2.Name = "PackageType2";
            this.PackageType2.Size = new System.Drawing.Size(99, 21);
            this.PackageType2.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Window;
            this.label3.Location = new System.Drawing.Point(12, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Package Type2";
            // 
            // ODSaveBtn
            // 
            this.ODSaveBtn.BackColor = System.Drawing.Color.SkyBlue;
            this.ODSaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ODSaveBtn.Location = new System.Drawing.Point(120, 211);
            this.ODSaveBtn.Name = "ODSaveBtn";
            this.ODSaveBtn.Size = new System.Drawing.Size(75, 23);
            this.ODSaveBtn.TabIndex = 5;
            this.ODSaveBtn.Text = "Save";
            this.ODSaveBtn.UseVisualStyleBackColor = false;
            this.ODSaveBtn.Click += new System.EventHandler(this.ODSaveBtn_Click);
            // 
            // PackageTypeValid
            // 
            this.PackageTypeValid.AutoSize = true;
            this.PackageTypeValid.BackColor = System.Drawing.Color.PaleTurquoise;
            this.PackageTypeValid.ForeColor = System.Drawing.Color.Red;
            this.PackageTypeValid.Location = new System.Drawing.Point(18, 157);
            this.PackageTypeValid.Name = "PackageTypeValid";
            this.PackageTypeValid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PackageTypeValid.Size = new System.Drawing.Size(11, 13);
            this.PackageTypeValid.TabIndex = 27;
            this.PackageTypeValid.Text = "*";
            this.PackageTypeValid.Visible = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.SkyBlue;
            this.panel4.Controls.Add(this.txtName);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(44, 69);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(125, 49);
            this.panel4.TabIndex = 7;
            // 
            // txtPersianName
            // 
            this.txtPersianName.BackColor = System.Drawing.Color.White;
            this.txtPersianName.Location = new System.Drawing.Point(13, 22);
            this.txtPersianName.Name = "txtPersianName";
            this.txtPersianName.Size = new System.Drawing.Size(99, 20);
            this.txtPersianName.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Window;
            this.label4.Location = new System.Drawing.Point(22, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Plant Name";
            // 
            // AddPlantForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PowerPlantProject.Properties.Resources._10;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(353, 248);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.PackageTypeValid);
            this.Controls.Add(this.ODSaveBtn);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel27);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(100, 100);
            this.MaximizeBox = false;
            this.Name = "AddPlantForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add New Plant";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form3_FormClosed);
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox PackageType1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox PackageType2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ODSaveBtn;
        private System.Windows.Forms.Label PackageTypeValid;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtPersianName;
        private System.Windows.Forms.Label label4;

    }
}