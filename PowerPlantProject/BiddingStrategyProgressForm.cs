using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using System.IO;
using Microsoft.Win32;

namespace PowerPlantProject
{
    public partial class BiddingStrategyProgressForm : Form
    {
        string peakbid;
        int biddingStrategySettingId;
        string PlantlblValue;
        string CurrentDateValue;
        string BiddingDateValue;
        int TrainingDays=40;
        int TrainingDayspower = 40;
        int TrainingDaysMinus=8;
        bool firstDll, secondDll;
        MainForm parentForm;
        bool Presolveplant = false;
        bool mcp = false;
        bool isopf = false;
        int Count_view = 0;
        int Count_view_pre=0;
        string gencocode = "";
       
        bool btnExportEnable=false;
        string titleForm = "";
        bool commonnearest = false;
        Thread thread;
        /// <summary>
        /// 
        /// </summary>


        public int BiddingStrategySettingId
        {

            get { return biddingStrategySettingId; }
            set { biddingStrategySettingId = value; }
        }

        public BiddingStrategyProgressForm(MainForm form2, bool presolve, bool IsMcp, bool IsOpf)
        {
            parentForm = form2;
            Presolveplant = presolve;
            mcp = IsMcp;
            isopf = IsOpf;
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void CLoadFlowProgressForm_Load(object sender, EventArgs e)
        {
            this.FormClosing += new FormClosingEventHandler(CLoadFlowProgressForm_OnClosing);

          
            DataTable dt = Utilities.GetTable("select * from BaseGencoInfo");
            if (dt.Rows.Count > 0)
            {
                gencocode = dt.Rows[0]["GencoCode"].ToString().Trim();

            }

            loadDataFromDB();

            LongTaskBegin();

        }
        private void loadDataFromDB()
        {
            try
            {
                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();


                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = myConnection;
                MyCom.CommandText = "select * from [BiddingStrategySetting] " +
                    "where id=" + biddingStrategySettingId;

                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;
                Maxda.Fill(MaxDS);

                DataTable dt = MaxDS.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    PlantlblValue = row["Plant"].ToString().Trim();
                    CurrentDateValue = row["CurrentDate"].ToString().Trim();
                    BiddingDateValue = row["BiddingDate"].ToString().Trim();
                    TrainingDays = Convert.ToInt32(row["TrainingDays"].ToString().Trim());
                    TrainingDayspower = Convert.ToInt32(row["TrainingDaysPower"].ToString().Trim());
                    firstDll = Convert.ToBoolean(row["FlagForecastingPrice"].ToString().Trim());
                    secondDll = Convert.ToBoolean(row["FlagBidAllocation"].ToString().Trim());
                    peakbid = row["PeakBid"].ToString().Trim();
                    if (Convert.ToBoolean(row["FlagStrategyBidding"].ToString().Trim()))
                    {
                        secondDll = true;
                        firstDll = true;
                    }

                }
                myConnection.Close();
            }
            catch
            {
            }

        }


        private void LongTaskBegin()
        {
            progressBar1.Maximum = 11;
            parentForm.EnableBiddingStrategyTab(false);
            thread = new Thread(RunBidding);
            thread.IsBackground = true;
            thread.Start();
            
        }

        public void UpdateProgressBar(int value, string description)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<int,string>(UpdateProgressBar), new object[] { value, description });
                return;
            }
            lblTitle.Text = description;
            this.Text = titleForm;
            progressBar1.Value = value;
            Thread.Sleep(1);
           
        }
        public void newUpdateProgressBar(int value, string description)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<int, string>(newUpdateProgressBar), new object[] { value, description });
                return;
            }
            lblTitle.Text = description;
            this.Text = titleForm;
            linkLabel1.Visible = true;
            progressBar1.Value = value;
            Thread.Sleep(1);

        }
        private void RunBidding()
        {

            string smaxdate = "";

            //////////////////////////////////////////////////////////////////////////

            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + BiddingDateValue.Trim() + "'order by BaseID desc");
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }

            ////////////////////////////////////////////////////////////////////////
            DataTable baseplant = Utilities.GetTable("select PPName from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            string[] ss1=baseplant.Rows[0][0].ToString().Trim().Split('-');
            string preplant =ss1[0].Trim();
           

            string strCmd = "select * from powerplant";

            if (PlantlblValue.ToLower() != "all")
            {
                strCmd += " WHERE PPName= '" + PlantlblValue + "'";
            }
            DataTable dtPlants = Utilities.GetTable(strCmd);
            int plants_Num = dtPlants.Rows.Count;
            bool[] firstDLLResults = new bool[dtPlants.Rows.Count];
          

            if (firstDll)
            {
                bool nearinternet = checkselecteddate(PersianDateConverter.ToGregorianDateTime(CurrentDateValue));

                ////////////////////////////////////////////////////////////
                bool enter = true;
                bool onepre = false;
                int c = 0;
                foreach (DataRow row in dtPlants.Rows)
                {
                    DataTable dts1 = Utilities.GetTable("select * from BidRunSetting where Plant='" + row[1].ToString().Trim() + "'");
                    if (dts1.Rows.Count > 0)
                    {
                        if (MyboolParse(dts1.Rows[0]["FlagPresolve"].ToString()))
                        {
                            if (enter)
                            {
                                this.titleForm = " PreSolve Price Forecasting for " + preplant + ".";
                                firstDLLResults[c] = CalculatePreSolve(preplant, nearinternet);
                                onepre = firstDLLResults[c];
                            }
                            enter = false;
                            firstDLLResults[c] = onepre;

                            this.titleForm = " Power Forecasting for " +
                                 row["PPName"].ToString().Trim();

                            CalculateBiddingpowerforecastpre(
                               PersianDateConverter.ToGregorianDateTime(CurrentDateValue),
                               PersianDateConverter.ToGregorianDateTime(BiddingDateValue),
                               int.Parse(row["PPID"].ToString().Trim()),
                           row["PPName"].ToString().Trim(), nearinternet);

                        }
                        else
                        {
                            if (row["PPName"].ToString().Trim() != preplant || enter == true)
                            {
                                this.titleForm = "  Price Forecasting for " +
                                    row["PPName"].ToString().Trim();

                                firstDLLResults[c] = CalculateBidding(PersianDateConverter.ToGregorianDateTime(CurrentDateValue),
                                          PersianDateConverter.ToGregorianDateTime(BiddingDateValue),
                                          int.Parse(row["PPID"].ToString().Trim()),
                                      row["PPName"].ToString().Trim(), nearinternet);
                                ///////////////////////////////////////////////////////////////
                                if (row["PPName"].ToString().Trim() == preplant && firstDLLResults[c] == true)
                                {
                                    onepre = firstDLLResults[c];
                                    enter = false;
                                }
                            }
                            else
                            {
                                firstDLLResults[c] = true;
                            }
                        }

                    }
                    c++;
                }

                //////////////////////////////////////////////////////////////////////////////////////////////
                //if (Presolveplant == true)
                //{
                //    this.titleForm = " PreSolve Price Forecasting for " +preplant + ".";

                //    if (PlantlblValue.ToLower() != "all")
                //    {
                //        firstDLLResults[0] = CalculatePreSolve(preplant, nearinternet);

                //    }
                //    if (PlantlblValue.ToLower() == "all")
                //    {
                //        bool l1 = CalculatePreSolve(preplant, nearinternet);
                //    }
                //}
                //    int i = 0;

                //    foreach (DataRow row in dtPlants.Rows)
                //    {
                //        if (Presolveplant == false || PlantlblValue.ToLower() == "all")
                //        {
                //            this.titleForm = "  Price Forecasting for " +
                //                row["PPName"].ToString().Trim();                           
                //            firstDLLResults[i++] = CalculateBidding(
                //                    PersianDateConverter.ToGregorianDateTime(CurrentDateValue),
                //                    PersianDateConverter.ToGregorianDateTime(BiddingDateValue),
                //                    int.Parse(row["PPID"].ToString().Trim()),
                //                row["PPName"].ToString().Trim(), nearinternet);                          

                //        }
                       
                //    }
                //////////////////////////////////////////////////////////////////////////////////////////

                    string msg = "Price Forecasting Finished..." + (secondDll ? "" : "\t\n\t\nPlease click Close to Return!");

                    UpdateProgressBar(progressBar1.Maximum, msg);                  
                  
                  
            }

            if (secondDll)
            {
                bool canRunSecondDll = true;

                if (firstDll == true)
                {
                    for (int i = 0; i < plants_Num; i++)
                    {
                        if (firstDLLResults[i] == false)
                        {
                            canRunSecondDll = false;
                            break;
                        }
                    }
                }
                if (canRunSecondDll)
                {
                    bool success = false;
                    // COMMENTED!!!!!!!!opf!!!!!!!!!!!!!!!
                    if (isopf)
                    {

                        COPF opf = new COPF(CurrentDateValue, BiddingDateValue, isopf);
                        this.titleForm = " Opf Processing ";
                        UpdateProgressBar(4, " Opf Processing....");
                        bool opfsuccess = opf.value();
                        string msg1 = " OPF Finished " + (opfsuccess ? "Successfully" : "Unsuccessfully") + "...";
                        UpdateProgressBar(6, msg1);

                    }

                    Classfinal classFinal = new Classfinal(PlantlblValue, CurrentDateValue, BiddingDateValue,Presolveplant,mcp);

                    peakClass peakclass = new peakClass(PlantlblValue, CurrentDateValue, BiddingDateValue, Presolveplant, mcp);

                    string plantname = PlantlblValue;
                    if (plantname.ToLower() == "all")
                        plantname += " plants";
                    this.titleForm = "  Bid Allocation for " + plantname;
                    UpdateProgressBar(7, "Processing...");
                    if (peakbid == "Yes")
                    {
                        //success = peakclass.value(); 
                    }
                    else 
                    {
                        success = classFinal.value(); 
                    }

                    string msg = " Bid Allocation Finished "+(success? "Successfully" : "Unsuccessfully" ) + "...";
                    msg += "\t\n\t\nClick Close to Finish!";
                    UpdateProgressBar(progressBar1.Maximum, msg);
                      //System.Windows.Forms.MessageBox.Show("Please click close to finish!");
             
                    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    if (success)
                        btnExportEnable = true;

                    
                }
                else
                {
                    string str = "Due to unsuccessfull Price Forcasting, bid allocation is not possible...";
                    MessageBox.Show(str);
                }
            }

          


        }


        /*
        private static CMatrix GetOneDayPricesForEachUnit(int ppid, string date, string blockM005, string blockM002)
        {
            blockM005 = blockM005.Trim();
            blockM002 = blockM002.Trim();

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            //DataTable oDataTable = utilities.GetTable("select max(PMax) from UnitsDataMain where ppid='" + ppid.ToString() + "'");
            //double pMaxUnitDataMain = 0;
            //try
            //{
            //    pMaxUnitDataMain = double.Parse(oDataTable.Rows[0][0].ToString());
            //}
            //catch
            //{
            //}

            CMatrix price = new CMatrix(1, 24);

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();

            Myda.SelectCommand = new SqlCommand("SELECT Hour,Required FROM [DetailFRM005] WHERE PPID= '" + ppid.ToString() + "' AND TargetMarketDate=@date AND Block=@block",
                myConnection);
            Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
            Myda.SelectCommand.Parameters["@date"].Value = date;
            Myda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
            Myda.SelectCommand.Parameters["@block"].Value = blockM005;
            Myda.Fill(MyDS);
            DataTable dt = MyDS.Tables[0];
            Myda.SelectCommand.Connection.Close();

            double[] requiredTable = new double[24];

            // if price exist
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow MyRow in dt.Rows)
                {
                    int x = int.Parse(MyRow["Hour"].ToString());
                    requiredTable[x - 1] = double.Parse(MyRow["Required"].ToString());
                }

                /////////// calculate price
                double[] maxBid = new double[24];
                //Detect MaxBid field in MRCurGrid1



                for (int index = 0; index < 24; index++)
                {

                    DataSet MaxDS = new DataSet();
                    SqlDataAdapter Maxda = new SqlDataAdapter();
                    string strCmd = "SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                        "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10,DispachableCapacity FROM [DetailFRM002] " +
                        "WHERE TargetMarketDate=@date AND Block=@block AND Hour=@hour AND PPID='" + ppid.ToString() + "'" +
                        " and ( Estimated is null or Estimated = 0)";
                    Maxda.SelectCommand = new SqlCommand(strCmd, myConnection);
                    Maxda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                    Maxda.SelectCommand.Parameters["@date"].Value = date;
                    Maxda.SelectCommand.Parameters.Add("@hour", SqlDbType.SmallInt);
                    Maxda.SelectCommand.Parameters["@hour"].Value = index + 1;
                    Maxda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
                    Maxda.SelectCommand.Parameters["@block"].Value = blockM002;

                    Maxda.Fill(MaxDS);
                    Myda.SelectCommand.Connection.Close();

                    double required = requiredTable[index];
                    double DispachableCapacity = 0;
                    try
                    {
                        DispachableCapacity = double.Parse(MaxDS.Tables[0].Rows[0]["DispachableCapacity"].ToString());
                    }
                    catch { }
                    /////~~~~~~~~~~~~~~~~ BEGIN  FOR PARAND!!!!!!!!!!!!!    ~~~~~~~~~~~~~~~~~~~~~~~~~~
                    if (DispachableCapacity < required && DispachableCapacity > 0)
                    {
                        maxBid[index] = 0;
                        string str = "select price1,price2,price3,price4,price5,price6,price7,price8,price9,price10" +
                            " from dbo.DetailFRM002 where ppid='" + ppid.ToString() + "'" +
                            " and TargetMarketDate='" + date + "'" +
                            " AND Hour='" + (index + 1).ToString() + "'" +
                            " AND block='" + blockM002 + "'";

                        DataTable oDataTable = utilities.GetTable(str);
                        double max = 0;
                        if (oDataTable.Rows.Count > 0)
                            for (int i = 0; i < 10; i++)
                            {
                                if (double.Parse(oDataTable.Rows[0][i].ToString()) > max)
                                    max = double.Parse(oDataTable.Rows[0][i].ToString());
                            }

                        maxBid[index] = max;
                    }
                    /////~~~~~~~~~~~~~~~~ END  FOR PARAND!!!!!!!!!!!!!    ~~~~~~~~~~~~~~~~~~~~~~~~~~

                    else
                    {
                        foreach (DataRow MaxRow in MaxDS.Tables[0].Rows)
                        {
                            int Dsindex = 0;
                            while ((Dsindex < 20) && (Math.Round(double.Parse(MaxRow[Dsindex].ToString()), 1) < required))
                                Dsindex = Dsindex + 2;
                            if (Dsindex < 20)
                                maxBid[index] = double.Parse(MaxRow[Dsindex + 1].ToString());
                            else
                                maxBid[index] = 0;

                        }
                    }

                    price[0, index] = maxBid[index];

                }

            }
            else
                price = null;

            myConnection.Close();
            return price;

        }
        */


        //private double[,] GetAveragePriceInputMatlab(PersianDate date)
        //{
        //    double[,] result = new double[1, 24];

        //    string strComd = "select AcceptedAverage from dbo .AveragePrice" +
        //        " where Date in " +
        //        " (select max (date) from dbo .AveragePrice where date<='" + date.ToString("d") + "')" +
        //        " order by hour";

        //    DataTable dt = utilities.GetTable(strComd);
        //    int index = 0;
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        string str = row[0].ToString();
        //        if (str != "")
        //            result[0, index++] = double.Parse(str);
        //    }
        //    return result;

        //}

        /// <summary>
        /// /////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="baseDate"></param>
        /// <param name="fututeDate"></param>
        /// <param name="ppId"></param>
        /// <param name="ppname"></param>
        /// <returns></returns>

        public static void LogBidding(string txt, TimeSpan time)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter("LogBidding.txt", true);
            sw.WriteLine(time.ToString() + ":    " + txt);
            sw.Close();
        }

        public static void Log(string txt)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter("Log.txt", true);
            //sw.WriteLine("");
            //sw.WriteLine("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            sw.WriteLine(txt);
            sw.Close();
        }
        public bool CalculateBidding(DateTime baseDate, DateTime fututeDate, int ppId, string ppname, bool isnearest)
        {
           
            //bool isnearest = false;

            //isnearest = checkselecteddate(baseDate);

            commonnearest = isnearest;
            BiddingStrategyData oData = null;
            UpdateProgressBar(1, "Initializing");

            LogBidding("/*********************************************/", new TimeSpan());
            LogBidding("/*********************************************/", new TimeSpan());

            DataSet myDs = UtilityPowerPlantFunctions.GetUnits(ppId);

            DataTable[] orderedPackages = UtilityPowerPlantFunctions.OrderPackages(myDs);

            int forcastDaysAhead = (new DateTime(fututeDate.Year, fututeDate.Month, fututeDate.Day, 0, 0, 0) - new DateTime(baseDate.Year, baseDate.Month, baseDate.Day, 0, 0, 0)).Days;

            /** !!!!!!!!!!! log */
            DateTime logTimebase = DateTime.Now;
            Core_pf_Dll.Core_pf_Class oCore_pf = new Core_pf_Class();
            DateTime logTimeCur = DateTime.Now;
            LogBidding("Core_pf_Dll instance", logTimeCur - logTimebase);
            /** !!!!!!!!!!!!!!! */
            bool powerforcastEstimated = false;
            foreach (DataTable dtPackageType in orderedPackages)
            {

                int packageOrder = 0;
                if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                    orderedPackages.Length > 1)
                    packageOrder = 1;

                int loc_n = 24 * TrainingDays + 1 /*   */;
                List<double> prices = new List<double>();

                Count_view = 0;
                for (int k = 1; k <= forcastDaysAhead; k++)
                {
                    //if (ppId == 206 && dtPackageType.TableName == "Gas")
                    //{
                    //    UpdateProgressBar(2, "");
                    //    string strDayCount = " " + dtPackageType.TableName + " packages; " +
                    //    " Day " + k.ToString() + " of " + forcastDaysAhead.ToString() + " ...";
                    //    UpdateProgressBar(9, "Getting Data For" + strDayCount);

                    //}
                    //else
                    {

                        CMatrix P_Power = new CMatrix(37, TrainingDays - 3);

                        DateTime curDate = baseDate.AddDays((k - 1));

                        List<double> average = new List<double>(UtilityPowerPlantFunctions.GetAveragePriceInputMatlab(TrainingDays, curDate));

                        string strDayCount = " " + dtPackageType.TableName + " packages; " +
                            " Day " + k.ToString() + " of " + forcastDaysAhead.ToString() + " ...";
                        UpdateProgressBar(2, "Getting Data For" + strDayCount);


                        /** !!!!!!!!!!! log */
                        logTimebase = DateTime.Now;
                        //string BaseDateDate = "";
                        if (k == 1)
                        {
                            logTimebase = DateTime.Now;
                            //BaseDateDate = checkselecteddate(curDate);
                            oData = new BiddingStrategyData(TrainingDayspower, TrainingDays, TrainingDaysMinus, 2, curDate, isnearest, ppId,dtPackageType.TableName);
                            //timing oData.GetFirstData();
                            logTimeCur = DateTime.Now;
                            LogBidding("first BiddingStrategyData class", logTimeCur - logTimebase);

                            logTimebase = DateTime.Now;
                            prices = new List<double>(UtilityPowerPlantFunctions.Get_N_Days_PowerPlantPrices(TrainingDays, ppId, dtPackageType, baseDate, curDate, packageOrder, new PersianDate(fututeDate).ToString("d")));
                            logTimeCur = DateTime.Now;
                            LogBidding("first prices", logTimeCur - logTimebase);

                        }
                        else
                        {
                            Count_view++;

                            logTimebase = DateTime.Now;


                            double[] oneDayPrice = UtilityPowerPlantFunctions.Get_1_Day_PowerPlantPrice(ppId, dtPackageType, baseDate, curDate, packageOrder, Count_view, new PersianDate(fututeDate).ToString("d"));
                            prices.RemoveRange(0, 24);
                            prices.AddRange(oneDayPrice);
                            logTimeCur = DateTime.Now;
                            LogBidding("next prices", logTimeCur - logTimebase);


                            logTimebase = DateTime.Now;
                           //timing oData.UpdateWithNextDay();
                            logTimeCur = DateTime.Now;
                            LogBidding("next BiddingStrategyData class", logTimeCur - logTimebase);


                        }


                        /** !!!!!!!!!!!!!!! */
                        bool noData = true;

                        for (int i = 0; i < 24 * TrainingDays && noData; i++)
                            if (prices[i] != 0)
                                noData = false;

                        if (noData)
                        {
                            string str = "Due to lack of Data, Price Forecasting is not possible for " + ppname + " right now." +
                                "\r\nYou need to load M002 Data for this plant...";

                            MessageBox.Show(str);
                            return false;
                        }

                        #region MR. Moosavi
                        int Test = 0;
                        int[] Error_price = new int[24 * TrainingDays];
                        for (int x = 72; x < 24 * TrainingDays; x++)
                        {
                            Test = 0;
                            if ((prices[x] == 0))
                            {
                                if (prices[x - 24] != 0)
                                {
                                    prices[x] = prices[x - 24];
                                    Test = 1;
                                }
                                else if (x - 48 >= 0 && (prices[x - 48] != 0) && (Test == 0))
                                {
                                    prices[x] = prices[x - 48];
                                    Test = 1;
                                }
                                else if (x - 72 >= 0 && (prices[x - 72] != 0) && (Test == 0))
                                {
                                    prices[x] = prices[x - 72];
                                    Test = 1;
                                }
                                if ((prices[x] == 0))
                                {
                                    Error_price[x] = 1;
                                }
                            }
                        }

                        for (int x = 48; x < 24 * TrainingDays; x++)
                        {
                            if ((prices[x - 24] != 0) && (prices[x - 48] != 0))
                            {
                                if ((prices[x] < 0.9 * prices[x - 24]) && (prices[x] < 0.9 * prices[x - 48]))
                                {
                                    prices[x] = prices[x - 24];
                                }
                                if ((prices[x] > 1.1 * prices[x - 24]) && (prices[x] < 1.1 * prices[x - 48]))
                                {
                                    prices[x] = prices[x - 24];
                                }
                            }
                        }
                        int[,] Co = new int[2, TrainingDays - 7];
                        for (int i = 0; i <= TrainingDays - 8; i++)
                        {
                            Co[0, i] = 0;
                            Co[1, i] = 1;
                        }
                        Co[0, 1] = 2;
                        Co[1, 1] = 2;

                        double[,] Ee = new double[1, TrainingDays - 7];
                        for (int i = 0; i <= TrainingDays - 8; i++)
                        {
                            Ee[0, i] = 0;
                        }
                        Ee[0, 2] = 0.00000001;
                        #endregion MR.Moosavi

                        /** !!!!!!!!!!! log */
                        UpdateProgressBar(3, "Preparing Data" + strDayCount);


                        /** !!!!!!!!!!!!!!! */
                        ///**************************************************/

                        double[] final_Forecast = new double[24];
                        double[] vr = new double[24];

                        // Two 24*201 Matrix at end!!
                        CMatrix ff_All = new CMatrix(0, 201); // a new row gets added in each j loop
                        CMatrix pdist_All = new CMatrix(0, 201); // a new row gets added in each j loop

                        //string description = "Processing" + strDayCount;
                        int lineTypesNo = Enum.GetNames(typeof(Line_codes)).Length;
                        int ROW_NUM = 28 + lineTypesNo + 1;

                        ////////////////////with power//////////////////////////////////////////////
                        //int revnum = exeptcc(isnearest);
                        //int ChangeRow = (exeptcc(isnearest) + 73);
                        //double[,] P_All = new double[ ChangeRow* 24, TrainingDays - TrainingDaysMinus];
                        //double[,] T_All = new double[1 * 24, TrainingDays - TrainingDaysMinus];
                        //double[,] X_All = new double[ChangeRow * 24, 1];
                        //double[] GainNum_All = new double[24];
                        ///////////////////////////////////////////////////////////////////////////////

                        double[,] P_All = new double[ROW_NUM * 24, TrainingDays - TrainingDaysMinus];
                        double[,] T_All = new double[1 * 24, TrainingDays - TrainingDaysMinus];
                        double[,] X_All = new double[ROW_NUM * 24, 1];
                        double[] GainNum_All = new double[24];




                        //CMatrix D1 = GetUsageNeed(
                        for (int j = 1; j <= 24; j++)
                        {

                            //UpdateProgressBar(j + 2, description);

                            CMatrix P = new CMatrix(ROW_NUM, TrainingDays - TrainingDaysMinus + 1);
                            CMatrix T = new CMatrix(1, TrainingDays - TrainingDaysMinus);

                            /** !!!!!!!!!!! log */
                            logTimebase = DateTime.Now;
                            /** !!!!!!!!!!!!!!! */

                            #region calculation

                            for (int i = 1; i <= TrainingDays - 8; i++)
                            {
                                int e = 24;

                                int temp1 = loc_n - (24 * i) + j;
                                int temp2 = loc_n - (24 * i) - e + j;

                                P[0, i - 1] = (prices[temp1 - 2] - prices[temp2 - 2] + Ee[0, i]) / prices[temp2 - 2];
                                P[1, i - 1] = (prices[temp1 - 2] - prices[temp2 - 3] + Ee[0, i]) / prices[temp2 - 3];
                                P[2, i - 1] = (prices[temp1 - 2] - prices[temp2 - 1] + Ee[0, i]) / prices[temp2 - 1];
                                P[3, i - 1] = (prices[temp1 - 2] - prices[temp2 - 4] + Ee[0, i]) / prices[temp2 - 4];
                                P[4, i - 1] = (prices[temp1 - 2] - prices[temp2] + Ee[0, i]) / prices[temp2];

                                P[5, i - 1] = (prices[temp1 - 3] - prices[temp2 - 3] + Ee[0, i]) / prices[temp2 - 3];
                                P[6, i - 1] = (prices[temp1 - Co[1, i]] - prices[temp2 - 1] + Ee[0, i]) / prices[temp2 - 1];
                                P[7, i - 1] = (prices[temp1 - 4] - prices[temp2 - 4] + Ee[0, i]) / prices[temp2 - 4];
                                P[8, i - 1] = (prices[temp1 - Co[0, i]] - prices[temp2] + Ee[0, i]) / prices[temp2];

                                P[ROW_NUM - 1, i - 1] = (average[temp1 - Co[0, i]] - average[temp2] + Ee[0, i]) / average[temp2];

                                e = 48;
                                temp2 = loc_n - 24 * i - e + j;
                                P[9, i - 1] = (prices[temp1 - 2] - prices[temp2 - 2] + Ee[0, i]) / prices[temp2 - 2];
                                P[10, i - 1] = (prices[temp1 - 2] - prices[temp2 - 3] + Ee[0, i]) / prices[temp2 - 3];
                                P[11, i - 1] = (prices[temp1 - 2] - prices[temp2 - 1] + Ee[0, i]) / prices[temp2 - 1];
                                P[12, i - 1] = (prices[temp1 - 2] - prices[temp2 - 4] + Ee[0, i]) / prices[temp2 - 4];
                                P[13, i - 1] = (prices[temp1 - 2] - prices[temp2] + Ee[0, i]) / prices[temp2];

                                P[14, i - 1] = (prices[temp1 - 3] - prices[temp2 - 3] + Ee[0, i]) / prices[temp2 - 3];
                                P[15, i - 1] = (prices[temp1 - Co[1, i]] - prices[temp2 - 1] + Ee[0, i]) / prices[temp2 - 1];
                                P[16, i - 1] = (prices[temp1 - 4] - prices[temp2 - 4] + Ee[0, i]) / prices[temp2 - 4];
                                P[17, i - 1] = (prices[temp1 - Co[0, i]] - prices[temp2] + Ee[0, i]) / prices[temp2];

                                e = 168;
                                temp2 = loc_n - 24 * i - e + j;
                                P[18, i - 1] = (prices[temp1 - 2] - prices[temp2 - 2] + Ee[0, i]) / prices[temp2 - 2];
                                P[19, i - 1] = (prices[temp1 - 2] - prices[temp2 - 3] + Ee[0, i]) / prices[temp2 - 3];
                                P[20, i - 1] = (prices[temp1 - 2] - prices[temp2 - 1] + Ee[0, i]) / prices[temp2 - 1];
                                P[21, i - 1] = (prices[temp1 - 2] - prices[temp2 - 4] + Ee[0, i]) / prices[temp2 - 4];
                                P[22, i - 1] = (prices[temp1 - 2] - prices[temp2] + Ee[0, i]) / prices[temp2];

                                P[23, i - 1] = (prices[temp1 - 3] - prices[temp2 - 3] + Ee[0, i]) / prices[temp2 - 3];
                                P[24, i - 1] = (prices[temp1 - Co[1, i]] - prices[temp2 - 1] + Ee[0, i]) / prices[temp2 - 1];
                                P[25, i - 1] = (prices[temp1 - 4] - prices[temp2 - 4] + Ee[0, i]) / prices[temp2 - 4];
                                P[26, i - 1] = (prices[temp1 - Co[0, i]] - prices[temp2] + Ee[0, i]) / prices[temp2];

                                //P[27, i-1] = A(temp1 - 1, 4);
                                string strDayOfWeek =
                                    curDate.Subtract(new TimeSpan(temp1 - 1, 0, 0)).
                                        DayOfWeek.ToString();

                                int dayIndex;
                                for (dayIndex = 1; dayIndex <= 7; dayIndex++)
                                {
                                    Days curDay = (Days)Enum.Parse(typeof(Days), dayIndex.ToString());
                                    if (curDay.ToString() == strDayOfWeek)
                                        break;
                                }
                                P[27, i - 1] = dayIndex;

                                //timing
                                //for (int b = 0; b < Enum.GetNames(typeof(Line_codes)).Length; b++)
                                //{
                                //    try
                                //    {
                                //        P[b + 28, i - 1] = oData.LC_H_N[b, temp1 - 2] + Ee[0, i];
                                //    }
                                //    catch (Exception)
                                //    {
                                //        throw;
                                //    }
                                //}
                                //////

                                if (i - 2 >= 0)
                                    T[0, i - 2] = ((prices[temp1 + 24 - 2] - prices[temp1 - 2]) + Ee[0, i])
                                                    / prices[temp1 - 2];


                                /////////////////////////////////////

                                //P.AddRow(temp3);
                                ///////////////// fill 43th to 72th row
                                //P.AddRow(oData.M_PriceForecast_44thRow.GetRow(j - 1));
                                //P.AddRow(oData.L_ProducedRegionsHourly[j - 1]);
                                //P.AddRow(oData.M_PriceForecast_59_72);

                            }
                            ///////////////// fill 43th to 72th row
                            /////////////////44 to 73

                            //timing//////////////
                            //P = P.AddRow(oData.M_PriceForecast_44thRow.GetRow(j - 1));
                            //P = P.AddRow(oData.L_ProducedRegionsHourly[j - 1]);
                            //P = P.AddRow(oData.M_PriceForecast_59_72);
                            ////////////////////


                            ///////////////////////with power//////////////////////
                            //P=P.AddRow(oData.L_Rev_Produced[j - 1]);
                            //////////////////////////////////////////////////////
                            #endregion calculation

                            CMatrix XTemp = P.GetColumn(0);
                            P = P.RemoveAtColumn(0);


                            GainNum_All[j - 1] = prices[loc_n - 24 + j - 2];
                            ////////////////////////with power//////////////////////////////////////
                            //for (int iRow = 0; iRow < ChangeRow; iRow++)
                            //{
                            //    X_All[ChangeRow * (j - 1) + iRow, 0] = XTemp[iRow, 0];
                            //    for (int jCol = 0; jCol < TrainingDays - TrainingDaysMinus; jCol++)
                            //    {
                            //        T_All[j - 1, jCol] = T[0, jCol];
                            //        P_All[ChangeRow * (j - 1) + iRow, jCol] = P[iRow, jCol];
                            //    }
                            //}
                            //////////////////////////////////////////////////////////////////////
                            for (int iRow = 0; iRow < ROW_NUM; iRow++)
                            {
                                X_All[ROW_NUM * (j - 1) + iRow, 0] = XTemp[iRow, 0];
                                for (int jCol = 0; jCol < TrainingDays - TrainingDaysMinus; jCol++)
                                {
                                    T_All[j - 1, jCol] = T[0, jCol];
                                    P_All[ROW_NUM * (j - 1) + iRow, jCol] = P[iRow, jCol];
                                }
                            }




                        }

                        /** !!!!!!!!!!! log */
                        logTimebase = DateTime.Now;
                        /** !!!!!!!!!!!!!!! */


                        MWArray[] argsOut = oCore_pf.core_pf_new_noout_xls(0,
                                        (MWArray)DoubleArrayToMWNumericArray(P_All),
                                        (MWArray)DoubleArrayToMWNumericArray(X_All),
                                        (MWArray)DoubleArrayToMWNumericArray(T_All)
                            //, (MWArray)DoubleArrayToMWNumericArray(AvagaePriceInput)
                                        );

                        logTimeCur = DateTime.Now;
                        LogBidding("Call DLL", logTimeCur - logTimebase);

                        UpdateProgressBar(5, "Getting results" + strDayCount);// two step jump

                        logTimebase = DateTime.Now;
                        CMatrix q_ALL = ReadExcelOut("out1", 1 * 24, 1);
                        logTimeCur = DateTime.Now;
                        LogBidding("out 1 * 24, 1", logTimeCur - logTimebase);

                        logTimebase = DateTime.Now;
                        CMatrix xOut_ALL = ReadExcelOut("out2", 1 * 24, 201);
                        logTimeCur = DateTime.Now;
                        LogBidding("out  1 * 24, 201", logTimeCur - logTimebase);

                        logTimebase = DateTime.Now;
                        CMatrix pdist_n_ALL = ReadExcelOut("out3", 400 * 24, 201);
                        logTimeCur = DateTime.Now;
                        LogBidding("out 400 * 24, 201", logTimeCur - logTimebase);

                        logTimebase = DateTime.Now;
                        CMatrix s_ALL = ReadExcelOut("out4", 1 * 24, 1);
                        logTimeCur = DateTime.Now;
                        LogBidding("out 1 * 24, 1", logTimeCur - logTimebase);

                        //////////////////////////////////////////////

                        UpdateProgressBar(6, "Preparing results" + strDayCount);// two step jump

                        logTimebase = DateTime.Now;
                        for (int hour = 0; hour < 24; hour++)
                        {
                            final_Forecast[hour] = GainNum_All[hour] * (1 + q_ALL[hour, 0]); ///  ????????? 

                            CMatrix gainMatrix = new CMatrix(1, 201);
                            for (int colj = 0; colj < 201; colj++)
                                gainMatrix[0, colj] = GainNum_All[hour];

                            ff_All = ff_All.AddRow((GainNum_All[hour] * xOut_ALL.GetRow(hour)) + gainMatrix);

                            vr[hour] = GainNum_All[hour] * s_ALL[hour, 0] / 20;


                            CMatrix PdistTemp = pdist_n_ALL.GetRows(400 * hour, 400 * (hour + 1) - 1) / GainNum_All[hour];
                            pdist_All = pdist_All.AddRow(PdistTemp.GetColumnMean());
                        }

                        /** !!!!!!!!!!! log */
                        logTimeCur = DateTime.Now;
                        LogBidding("Attatch Arrays ", logTimeCur - logTimebase);
                        /** !!!!!!!!!!!!!!! */


                        /** !!!!!!!!!!! log */
                        logTimebase = DateTime.Now;
                        UpdateProgressBar(7, "Save Bidding Results for" + strDayCount);
                        SavePriceForcastResults(ppId, new PersianDate(baseDate.AddDays(k)), final_Forecast, vr, ff_All, pdist_All, dtPackageType.TableName);
                        logTimeCur = DateTime.Now;
                        LogBidding("SaveForcastResults", logTimeCur - logTimebase);
                        /** !!!!!!!!!!!!!!! */

                        ///////////////////////////with power////////////////////////////////////
                        //if (!powerforcastEstimated)
                        //{

                            UpdateProgressBar(8, "Power Forecasting...");
                            if (k == 1)
                            {
                                oData.GetFirstData();
                            }
                            else
                            {
                                oData.UpdateWithNextDay();
                            }
                            try
                            {
                                CMatrix power_P = oData.M_PowerForecast_1_24;
                                power_P = power_P.AddRow(oData.M_PowerForecast_25thRow);
                               // timing power_P = power_P.AddRow(oData.M_PowerForecast_26_37);
                                // timing power_P = power_P.AddRow(oData.M_PowerForecast_otherGenco);
                                // timing power_P = power_P.AddRow(oData.M_PowerForecast_load);



                                CMatrix power_X = power_P.GetColumn(0);
                                power_P = power_P.RemoveAtColumn(0);

                                CMatrix power_T = oData.M_PowerForecast_T;

                                logTimebase = DateTime.Now;
                                argsOut = oCore_pf.core_pf2(4,
                            (MWArray)DoubleArrayToMWNumericArray(power_P.GetDoubleMatrix()),
                            (MWArray)DoubleArrayToMWNumericArray(power_X.GetDoubleMatrix()),
                            (MWArray)DoubleArrayToMWNumericArray(power_T.GetDoubleMatrix()));
                                logTimeCur = DateTime.Now;
                                LogBidding("Call DLL Power", logTimeCur - logTimebase);
                                UpdateProgressBar(9, "Save Power Forecasting results..");

                                MWNumericArray temp = new MWNumericArray();
                                temp = (MWNumericArray)argsOut[0];
                                CMatrix out_P = new CMatrix(MWNumericArrayToDoubleArray(temp, 24, 1));

                                for (int xx = 0; xx < out_P.Rows; xx++)
                                    for (int yy = 0; yy < out_P.Cols; yy++)
                                        out_P[xx, yy] = 1 + out_P[xx, yy];
                                out_P = out_P & oData.M_PowerForecast_YesterDayPower;

                                SavePowerForcastResults(baseDate.AddDays(k), out_P, ppId.ToString(),dtPackageType.TableName);


                            }
                            catch
                            {
                                CMatrix out_zero = new CMatrix(24, 1);

                                for (int h = 0; h < 24; h++)
                                {
                                    out_zero[h, 0] = 0.0;
                                }

                                SavePowerForcastResults(baseDate.AddDays(k), out_zero, ppId.ToString(),dtPackageType.TableName);

                            }



                        //}

                        if (k != forcastDaysAhead)
                        {
                            ///////////////for not empty//////////////////
                            //UpdateProgressBar(8, "");
                            //UpdateProgressBar(9, "");
                            //////////////////////////////////////////////

                            UpdateProgressBar(progressBar1.Maximum, "");
                        }
                        ///////////////////////////////////////////////////////////////
                    }
                }
                /////////////////////with power///////////////////////////////
                 //powerforcastEstimated = true;
                /////////////////////////////////////////////////////////////
            }
            
            return true;
        }
        public bool CalculateBiddingpowerforecastpre(DateTime baseDate, DateTime fututeDate, int ppId, string ppname, bool isnearest)
        {


            commonnearest = isnearest;
            BiddingStrategyData oData = null;

            DataSet myDs = UtilityPowerPlantFunctions.GetUnits(ppId);

            DataTable[] orderedPackages = UtilityPowerPlantFunctions.OrderPackages(myDs);

            int forcastDaysAhead = (new DateTime(fututeDate.Year, fututeDate.Month, fututeDate.Day, 0, 0, 0) - new DateTime(baseDate.Year, baseDate.Month, baseDate.Day, 0, 0, 0)).Days;


            Core_pf_Dll.Core_pf_Class oCore_pf = new Core_pf_Class();


            bool powerforcastEstimated = false;
            foreach (DataTable dtPackageType in orderedPackages)
            {

                int packageOrder = 0;
                if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                    orderedPackages.Length > 1)
                    packageOrder = 1;

                int loc_n = 24 * TrainingDays + 1 /*   */;


                Count_view = 0;
                for (int k = 1; k <= forcastDaysAhead; k++)
                {

                    CMatrix P_Power = new CMatrix(37, TrainingDays - 3);

                    DateTime curDate = baseDate.AddDays((k - 1));

                    // List<double> average = new List<double>(UtilityPowerPlantFunctions.GetAveragePriceInputMatlab(TrainingDays, curDate));

                    if (k == 1)
                    {
                        oData = new BiddingStrategyData(TrainingDayspower, TrainingDays, TrainingDaysMinus, 2, curDate, isnearest, ppId, dtPackageType.TableName);
                        oData.GetFirstData();


                    }
                    else
                    {
                        Count_view++;


                        oData.UpdateWithNextDay();



                    }

                    ///////////////////////////with power////////////////////////////////////
                    //if (!powerforcastEstimated)
                    //{
                        UpdateProgressBar(8, "Power Forecasting...");
                        try
                        {
                            CMatrix power_P = oData.M_PowerForecast_1_24;
                            power_P = power_P.AddRow(oData.M_PowerForecast_25thRow);
                            // timing  power_P = power_P.AddRow(oData.M_PowerForecast_26_37);
                            // timing  power_P = power_P.AddRow(oData.M_PowerForecast_otherGenco);
                            // timing  power_P = power_P.AddRow(oData.M_PowerForecast_load);

                            CMatrix power_X = power_P.GetColumn(0);
                            power_P = power_P.RemoveAtColumn(0);

                            CMatrix power_T = oData.M_PowerForecast_T;


                            MWArray[] argsOut = oCore_pf.core_pf2(4,
                        (MWArray)DoubleArrayToMWNumericArray(power_P.GetDoubleMatrix()),
                        (MWArray)DoubleArrayToMWNumericArray(power_X.GetDoubleMatrix()),
                        (MWArray)DoubleArrayToMWNumericArray(power_T.GetDoubleMatrix()));


                            UpdateProgressBar(9, "Save Power Forecasting results..");

                            MWNumericArray temp = new MWNumericArray();
                            temp = (MWNumericArray)argsOut[0];
                            CMatrix out_P = new CMatrix(MWNumericArrayToDoubleArray(temp, 24, 1));

                            for (int xx = 0; xx < out_P.Rows; xx++)
                                for (int yy = 0; yy < out_P.Cols; yy++)
                                    out_P[xx, yy] = 1 + out_P[xx, yy];
                            out_P = out_P & oData.M_PowerForecast_YesterDayPower;

                            SavePowerForcastResults(baseDate.AddDays(k), out_P, ppId.ToString(), dtPackageType.TableName);

                        }
                        catch
                        {
                            CMatrix out_zero = new CMatrix(24, 1);

                            for (int h = 0; h < 24; h++)
                            {
                                out_zero[h, 0] = 0.0;
                            }

                            SavePowerForcastResults(baseDate.AddDays(k), out_zero, ppId.ToString(), dtPackageType.TableName);

                        }


                    //}

                    if (k != forcastDaysAhead)
                    {

                        UpdateProgressBar(progressBar1.Maximum, "");
                    }

                }
                /////////////////////with power///////////////////////////////
               // powerforcastEstimated = true;
                /////////////////////////////////////////////////////////////
            }

            return true;
        }
    

        //private CMatrix ReadExcelOut(string fileName, int rowNum, int colNum)
        //{
        //    CMatrix result = new CMatrix(rowNum, colNum);
        //    if (fileName != "" && File.Exists(fileName + ".xls"))
        //    {
        //        string strConnectionString = string.Empty;
        //        strConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
        //            "Data Source=" + fileName + ".xls;" +
        //            "Extended Properties='Excel 8.0;HDR=No'";

        //        OleDbConnection oleConn = new OleDbConnection(strConnectionString);
        //        OleDbDataAdapter oleCommand = new OleDbDataAdapter("SELECT * from [Sheet1$]", oleConn);
        //        DataTable dt = new DataTable();

        //        oleCommand.Fill(dt);

        //        ////// Delets empty rows
        //        int count = dt.Rows.Count;
        //        for (int i = count - 1; i >= 0; i--)
        //            if (dt.Rows[i][0].ToString() == "" && dt.Rows[i][1].ToString() == "")
        //                dt.Rows[i].Delete();
        //        dt.AcceptChanges();
        //        ////// end of Deletion

        //        if (dt.Rows.Count != rowNum || dt.Columns.Count != colNum)
        //        {
        //            MessageBox.Show("");
        //        }
        //        else
        //        {
        //            for (int i = 0; i < dt.Rows.Count; i++)
        //            {
        //                for (int j = 0; j < dt.Columns.Count; j++)
        //                    result[i, j] = double.Parse(dt.Rows[i][j].ToString());
        //            }
        //        }

        //    }
        //    return result;
        //}
        private CMatrix ReadExcelOut(string fileName, int rowNum, int colNum)
        {
            CMatrix result = new CMatrix(rowNum, colNum);
            if (fileName != "" && File.Exists(fileName + ".xls"))
            {
                string strConnectionString = string.Empty;
                //strConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                //    "Data Source=" + fileName + ".xls;" +
                //    "Extended Properties='Excel 8.0;HDR=No'";

                strConnectionString = getExcelConnectionString(fileName + ".xls");

                OleDbConnection oleConn = new OleDbConnection(strConnectionString);
                OleDbDataAdapter oleCommand = new OleDbDataAdapter("SELECT * from [Sheet1$]", oleConn);
                DataTable dt = new DataTable();

                oleCommand.Fill(dt);

                ////// Delets empty rows
                int count = dt.Rows.Count;
                for (int i = count - 1; i >= 0; i--)
                    if (dt.Rows[i][0].ToString() == "" && dt.Rows[i][1].ToString() == "")
                        dt.Rows[i].Delete();
                dt.AcceptChanges();
                ////// end of Deletion

                if (dt.Rows.Count != rowNum || dt.Columns.Count != colNum)
                {
                    MessageBox.Show("please import data from 4 days before bidding date.");
                }
                else
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        for (int j = 0; j < dt.Columns.Count; j++)
                            result[i, j] = MyDoubleParse(dt.Rows[i][j].ToString());
                    }
                }
            }
            return result;
        }
        private string getExcelConnectionString(string fileName)
        {
            string _excelPath = GetComponentPath();
            if (GetMajorVersion(_excelPath) == 11)//2003
                return @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                        "Data Source=" + fileName + ";" +
                        "Extended Properties='Excel 8.0;HDR=no'";
            else if (GetMajorVersion(_excelPath) == 12)//2007
                return @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName +
                        ";Extended Properties='Excel 12.0;HDR=no'";

            else if (GetMajorVersion(_excelPath) == 14)//2010
                return @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName +
                    ";Extended Properties='Excel 12.0 Xml;HDR=no'";

            else  
                return @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + 
                        ";Mode=Share Deny Write;Extended Properties=\"HDR=no';Jet OLEDB:Engine Type=37";

        }

        private string GetComponentPath()
        {
            string RegKey = @"Software\Microsoft\Windows\CurrentVersion\App Paths";

            string toReturn = string.Empty;
            string _key = "excel.exe";

            //looks inside CURRENT_USER:
            RegistryKey _mainKey = Registry.CurrentUser;
            try
            {
                _mainKey = _mainKey.OpenSubKey(RegKey + "\\" + _key, false);
                if (_mainKey != null)
                {
                    toReturn = _mainKey.GetValue(string.Empty).ToString();
                }
            }
            catch
            { }

            //if not found, looks inside LOCAL_MACHINE:
            _mainKey = Registry.LocalMachine;
            if (string.IsNullOrEmpty(toReturn))
            {
                try
                {
                    _mainKey = _mainKey.OpenSubKey(RegKey + "\\" + _key, false);
                    if (_mainKey != null)
                    {
                        toReturn = _mainKey.GetValue(string.Empty).ToString();
                    }
                }
                catch
                {
                }
            }

            //closing the handle:
            if (_mainKey != null)
                _mainKey.Close();

            return toReturn;
        }
        private int GetMajorVersion(string _path)
        {
            int toReturn = 0;
            if (File.Exists(_path))
            {
                try
                {
                    FileVersionInfo _fileVersion = FileVersionInfo.GetVersionInfo(_path);
                    toReturn = _fileVersion.FileMajorPart;
                }
                catch
                { }
            }

            return toReturn;
        }


        
        private CMatrix RandomMatrix(int row, int col)
        {
            Random rand = new Random();
            CMatrix result = new CMatrix(row, col);
            for (int i = 0; i < row; i++)
                for (int j = 0; j < col; j++)
                    result[i, j] = rand.NextDouble();
            return result;
        }


        public void SavePriceForcastResults(int ppID, PersianDate date, double[] final_Forecast, double[] vr, CMatrix ff, CMatrix pdist, string packageType)
        {

            //////// DELETE
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            string whereCmd = " where FinalForecast.PPId='" + ppID.ToString() +
                "' AND FinalForecast.date = '" + date.ToString("d") +
                "' and FinalForecast.packageType='" + packageType + "'";

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            MyCom.CommandText = "delete from dbo.FinalForecast201Item where fk_ffHourly in(" +
                        " select FinalForecastHourly.id from dbo.FinalForecastHourly" +
                        " inner join dbo.FinalForecast on FinalForecast.id = FinalForecastHourly.fk_FinalForecastId" +
                        whereCmd + ")";
            MyCom.ExecuteNonQuery();
            

            MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            MyCom.CommandText = "delete from FinalForecastHourly where fk_FinalForecastId in(" +
                " select id from dbo.FinalForecast" +
                    whereCmd + ")";
            MyCom.ExecuteNonQuery();

            MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            MyCom.CommandText = "delete from FinalForecast" + whereCmd;
            MyCom.ExecuteNonQuery();

            /////////////   INSERT
            MyCom = new SqlCommand();
            MyCom.Connection = myConnection;

            // Insert into FinalForcast table
            MyCom.CommandText = "insert INTO [FinalForecast] (PPId,date,packageType)"
                        + " VALUES (@ppid,@date,@packageType)";
            MyCom.Parameters.Add("@ppid", SqlDbType.NChar, 10);
            MyCom.Parameters["@ppid"].Value = ppID.ToString();
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = date.ToString("d");
            MyCom.Parameters.Add("@packageType", SqlDbType.Char, 20);
            MyCom.Parameters["@packageType"].Value = packageType.ToString();
            MyCom.ExecuteNonQuery();

            // retrieve the newly inserted id in FinalForcast
            MyCom.CommandText = "select max(id) from [FinalForecast]";
            int FinalForcastId = (int)MyCom.ExecuteScalar();

            for (int j = 0; j < 24; j++)
            {
                // Insert into FinalForcastHourly table
                MyCom = new SqlCommand();
                MyCom.Connection = myConnection;
                MyCom.CommandText = "insert INTO [FinalForecastHourly] (fk_FinalForecastId,hour,forecast, vr)"
                            + " VALUES (@finalForcastId,@hour,@forecast, @vr)";

                MyCom.Parameters.Add("@finalForcastId", SqlDbType.Int);
                MyCom.Parameters["@finalForcastId"].Value = FinalForcastId;
                MyCom.Parameters.Add("@hour", SqlDbType.Int);
                MyCom.Parameters["@hour"].Value = j + 1;
                MyCom.Parameters.Add("@forecast", SqlDbType.Float);
                MyCom.Parameters["@forecast"].Value = final_Forecast[j];
                MyCom.Parameters.Add("@vr", SqlDbType.Float);
                MyCom.Parameters["@vr"].Value = vr[j];
                MyCom.ExecuteNonQuery();

                // retrieve the newly inserted id in FinalForcastHourly
                MyCom.CommandText = "select max(id) from [FinalForecastHourly]";
                int FinalForcastHourlyId = (int)MyCom.ExecuteScalar();


                for (int k = 0; k < 201; k++)
                {
                    // Insert into FinalForecast201Item table
                    MyCom = new SqlCommand();
                    MyCom.CommandText = "insert INTO [FinalForecast201Item] (fk_ffHourly,[index],pdist, ff)"
                                + " VALUES (@fk_ffHourly,@index,@pdist, @ff)";
                    MyCom.Connection = myConnection;
                    MyCom.Parameters.Add("@fk_ffHourly", SqlDbType.Int);
                    MyCom.Parameters["@fk_ffHourly"].Value = FinalForcastHourlyId;
                    MyCom.Parameters.Add("@index", SqlDbType.Int);
                    MyCom.Parameters["@index"].Value = k + 1;
                    MyCom.Parameters.Add("@pdist", SqlDbType.Float);
                    MyCom.Parameters["@pdist"].Value = pdist[j, k];
                    MyCom.Parameters.Add("@ff", SqlDbType.Float);
                    MyCom.Parameters["@ff"].Value = ff[j, k];
                    MyCom.ExecuteNonQuery();

                }
            }
            //ConnectionManager.GetInstance().Connection.Close();
            myConnection.Close();
        }

        public void SavePowerForcastResults(DateTime date, CMatrix power, string ppid, string ptype) // 24*1
        {
            PersianDate prDate = new PersianDate(date);

            //////// DELETE
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            MyCom.CommandText = "delete from PowerForecast where date='" + prDate.ToString("d") + "'and gencode like '" + (ppid + "-" + ptype) + '%' + "'";
            MyCom.ExecuteNonQuery();

            /////////////   INSERT
            MyCom = new SqlCommand();
            MyCom.Connection = myConnection;

            // Insert into FinalForcast table
            string strCom1 = "insert INTO [PowerForecast] (Gencode,date";
            string strCom2 = "VALUES ('" + (ppid + "-" + ptype) + "','" + prDate.ToString("d") + "'";
            for (int i = 1; i <= 24; i++)
            {
                if (power[i - 1, 0] > 0)
                {
                    strCom1 += ",Hour" + i.ToString();
                    strCom2 += "," + power[i - 1, 0].ToString();
                }
                else
                {
                    strCom1 += ",Hour" + i.ToString();
                    strCom2 += "," + "0";
                }
            }

            MyCom.CommandText = strCom1 + " ) " + strCom2 + ")";
            MyCom.ExecuteNonQuery();

            myConnection.Close();
        }

        //public void SavePowerForcastResults(DateTime date, CMatrix power , string ppid) // 24*1
        //{
        //    PersianDate prDate = new PersianDate(date);

        //    //////// DELETE
        //    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
        //    myConnection.Open();

        //    SqlCommand MyCom = new SqlCommand();
        //    MyCom.Connection = myConnection;
        //    MyCom.CommandText = "delete from PowerForecast where date='" + prDate.ToString("d") + "'and Gencode='"+ ppid + "'";
        //    MyCom.ExecuteNonQuery();

        //    /////////////   INSERT
        //    MyCom = new SqlCommand();
        //    MyCom.Connection = myConnection;

        //    // Insert into FinalForcast table
        //    string strCom1 = "insert INTO [PowerForecast] (Gencode,date";
        //    string strCom2 = "VALUES ('"+ppid+"','" + prDate.ToString("d") + "'";
        //    for (int i = 1; i <= 24; i++)
        //    {
        //        if (power[i - 1, 0] > 0)
        //        {
        //            strCom1 += ",Hour" + i.ToString();
        //            strCom2 += "," + power[i - 1, 0].ToString();
        //        }
        //        else
        //        {
        //            strCom1 += ",Hour" + i.ToString();
        //            strCom2 += "," + "0";
        //        }
        //    }
            
        //    MyCom.CommandText = strCom1+ " ) " + strCom2 +")";
        //    MyCom.ExecuteNonQuery();

        //    myConnection.Close();
        //}

        //public void SavePowerForcastResultsRival(DateTime date, CMatrix power) // 24*1
        //{
        //    PersianDate prDate = new PersianDate(date);

        //    //////// DELETE
        //    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
        //    myConnection.Open();

        //    SqlCommand MyCom = new SqlCommand();
        //    MyCom.Connection = myConnection;
        //    MyCom.CommandText = "delete from PowerForecastRival where date='" + prDate.ToString("d") + "'";
        //    MyCom.ExecuteNonQuery();

        //    /////////////   INSERT
        //    MyCom = new SqlCommand();
        //    MyCom.Connection = myConnection;

        //    // Insert into FinalForcast table
        //    string strCom1 = "insert INTO [PowerForecastRival] (Gencode,date";
        //    string strCom2 = "VALUES ('Tehran','" + prDate.ToString("d") + "'";
        //    for (int i = 1; i <= 24; i++)
        //    {
        //        strCom1 += ",Hour" + i.ToString();
        //        strCom2 += "," + power[i - 1, 0].ToString();
        //    }

        //    MyCom.CommandText = strCom1 + " ) " + strCom2 + ")";
        //    MyCom.ExecuteNonQuery();

        //    myConnection.Close();
        //}

        private double[,] MWNumericArrayToDoubleArray(MWNumericArray mwNumericArray, int row, int col)
        {
            double[,] doubleArray = new double[row, col];

            for (int i = 0; i < row; i++)
                for (int j = 0; j < col; j++)
                    doubleArray[i, j] = mwNumericArray[i + 1, j + 1].ToScalarDouble();

            return doubleArray;
        }

        private MWNumericArray DoubleArrayToMWNumericArray(double[,] doubleArray)
        {
            int noX = doubleArray.GetLength(0);
            int noY = doubleArray.GetLength(1);

            MWNumericArray mwNumericArray = new MWNumericArray(MWArrayComplexity.Real, MWNumericType.Double, noX, noY);

            for (int i = 0; i < noX; i++)
                for (int j = 0; j < noY; j++)
                    mwNumericArray[i + 1, j + 1] = doubleArray[i, j];

            return mwNumericArray;
        }



        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //private int exeptcc(bool nerest)
        //{

        //    string rivaldate = CurrentDateValue;
        //    if (nerest)
        //    {
        //        rivaldate = FindNearRegionDate(rivaldate);
        //    }

        //    DataTable fdat = utilities.GetTable("SELECT PPID,PackageType FROM dbo.PPUnit ");
        //    string exp = "";
        //    int count = 0;
        //    for (int i = 0; i < fdat.Rows.Count; i++)
        //    {
        //        if (i != (fdat.Rows.Count - 1))
        //        {

        //            if ((fdat.Rows[i][0].ToString().Trim() == fdat.Rows[i + 1][0].ToString().Trim()) && (fdat.Rows[i][1].ToString().Trim() == "Combined Cycle" || fdat.Rows[i + 1][1].ToString().Trim() == "Combined Cycle"))
        //            {
        //                count++;
        //                exp += (int.Parse(fdat.Rows[i + 1][0].ToString()) + 1).ToString() + ":";

        //            }
        //        }
        //    }
        //    string[] exepcode = exp.Split(':');
        //    int lenght = exepcode.Length;

        //    int countrev = 0;

        //    //DataTable findrev = utilities.GetTable("Select distinct PPCode from dbo.RegionNetComp where  Date='" + rivaldate + "'AND Code='R01' AND PPCode NOT IN (SELECT PPID FROM PowerPlant)and Type!='H'");
        //    //if (findrev.Rows.Count > 0)
        //    //{
        //    //    countrev = (findrev.Rows.Count - count) + 1;
        //    //}
        //    int temp = 0;
        //    DataTable findrev = utilities.GetTable("Select distinct PPCode from dbo.RegionNetComp where  Date='" + rivaldate + "'AND Code='R01' AND PPCode NOT IN (SELECT PPID FROM PowerPlant)and Type!='H'");
        //    foreach (DataRow myrow in findrev.Rows)
        //    {
        //        for (int i = 0; i < lenght; i++)
        //        {
        //            if (myrow[0].ToString().Trim() == exepcode[i])
        //            {
        //                temp++;
        //            }
        //        }
        //    }


        //    countrev = (findrev.Rows.Count - temp) + 1;

        //    return countrev;



        //}

        private void CLoadFlowProgressForm_OnClosing(object sender, EventArgs e)
        {
            parentForm.EnableBiddingStrategyTab(true);
            parentForm.EnableBiddingStrategyExportButton(btnExportEnable);
            parentForm.erasecmbcustom();
        }

        //private int  exeptcc()
        //{
        //    DataTable fdat = utilities.GetTable("SELECT PPID,PackageType FROM dbo.PPUnit ");
        //    string exp = "";
        //    int count = 0;
        //    for (int i = 0; i < fdat.Rows.Count; i++)
        //    {
        //        if (i != (fdat.Rows.Count - 1))
        //        {

        //            if (fdat.Rows[i][0].ToString() == fdat.Rows[i + 1][0].ToString())
        //            {
        //                count++;
        //                exp += (int.Parse(fdat.Rows[i + 1][0].ToString()) + 1).ToString() + ":";

        //            }
        //        }
        //    }
        //    string[] exepcode = exp.Split(':');
        //    int lenght = exepcode.Length;
        //    int countrev=0;


        //    DataTable findrev = utilities.GetTable("Select PPCode from dbo.RegionNetComp where  Date='"+ CurrentDateValue+"'AND Code='R01' AND PPCode NOT IN (SELECT PPID FROM PowerPlant)and Type!='H'");
        //    if (findrev.Rows.Count > 0)
        //    {
        //        countrev = (findrev.Rows.Count - count) + 1;
        //    }
        //    else
        //    {
        //        findrev = utilities.GetTable("Select PPCode from dbo.RegionNetComp where  Date='" + FindNearRegionDate(CurrentDateValue) + "'AND Code='R01' AND PPCode NOT IN (SELECT PPID FROM PowerPlant)and Type!='H'");
        //        countrev = (findrev.Rows.Count - count) + 1;
        //    }

        //    return countrev;

        //}
       
        private string FindNearRegionDate(string date)
        {
            string gencocode = "";
            DataTable dt = Utilities.GetTable("select * from BaseGencoInfo");
            if (dt.Rows.Count > 0)
            {
                gencocode = dt.Rows[0]["GencoCode"].ToString().Trim();
               
            }
            DataTable odat = Utilities.GetTable("Select distinct Date from dbo.RegionNetComp where Date<='"+date+"'AND Code='"+gencocode+"' order by Date desc");
            return odat.Rows[0][0].ToString().Trim();

        }
        //public bool CalculatePowerRival(DateTime baseDate, DateTime fututeDate, int ppId, string ppname)
        //{

           
        //    BiddingStrategyData oData = null;
          
        //    DataSet myDs = UtilityPowerPlantFunctions.GetUnits(ppId);

        //    DataTable[] orderedPackages = UtilityPowerPlantFunctions.OrderPackages(myDs);

        //    int forcastDaysAhead = (new DateTime(fututeDate.Year, fututeDate.Month, fututeDate.Day, 0, 0, 0) - new DateTime(baseDate.Year, baseDate.Month, baseDate.Day, 0, 0, 0)).Days;

        //    Core_pf_Dll.Core_pf_Class oCore_pf = new Core_pf_Class();
         
        //    bool powerforcastEstimated = false;
                            

        //        for (int k = 1; k <= forcastDaysAhead; k++)
        //        {

        //           DateTime curDate = baseDate.AddDays((k-1));

        //            if (k == 1)
        //            {

        //                oData = new BiddingStrategyData(TrainingDayspower, TrainingDays, TrainingDaysMinus, 2, curDate,commonnearest);
        //                oData.GetFirstData();
        //            }
        //            else
        //            {
                        
        //              oData.UpdateWithNextDay();
                        
        //            }
                   
        //            if (!powerforcastEstimated)
        //            {

        //                UpdateProgressBar(10, "Rival Analysing Day " + k.ToString() + " of " + forcastDaysAhead.ToString() + " ...");

        //                CMatrix power_P = oData.M_PowerForecast_1_24_Aftercal;


        //                power_P = power_P.AddRow(oData.M_PowerForecast_25thRow);
        //                power_P = power_P.AddRow(oData.M_PowerForecast_26_37);


        //                CMatrix power_X = power_P.GetColumn(0);
        //                power_P = power_P.RemoveAtColumn(0);

        //                CMatrix power_T = oData.M_PowerForecast_T;

        //             MWArray[] argsOut = oCore_pf.core_pf2(4,
        //            (MWArray)DoubleArrayToMWNumericArray(power_P.GetDoubleMatrix()),
        //            (MWArray)DoubleArrayToMWNumericArray(power_X.GetDoubleMatrix()),
        //            (MWArray)DoubleArrayToMWNumericArray(power_T.GetDoubleMatrix()));
                      
                     

        //                MWNumericArray temp = new MWNumericArray();
        //                temp = (MWNumericArray)argsOut[0];
        //                CMatrix out_P = new CMatrix(MWNumericArrayToDoubleArray(temp, 24, 1));

        //                for (int xx = 0; xx < out_P.Rows; xx++)
        //                    for (int yy = 0; yy < out_P.Cols; yy++)
        //                        out_P[xx, yy] = 1 + out_P[xx, yy];
        //                out_P = out_P & oData.M_PowerForecast_YesterDayPower_aftercal;

        //                SavePowerForcastResultsRival(baseDate.AddDays(k), out_P);

        //                if (k == forcastDaysAhead)
        //                {
        //                    UpdateProgressBar(progressBar1.Maximum, "");
        //                }
                       
        //            }
                  
        //        }
        //        powerforcastEstimated = true;
           

        //    return true;
        //}
        private bool checkselecteddate(DateTime current)
        {
            string Date = new PersianDate(current).ToString("d");
            ///////////////////////////////////////////////////////////////////////////
            DateTime tempDate = current.Subtract(new TimeSpan(1, 0, 0, 0));
            string endDate = new PersianDate(tempDate).ToString("d");
            string startDate = new PersianDate(tempDate.Subtract(new TimeSpan(TrainingDays, 0, 0, 0))).ToString("d");
            
            DialogResult Result;
            DataTable findrev = Utilities.GetTable("Select distinct PPCode from dbo.RegionNetComp where  Date='" +Date  + "'AND Code='"+gencocode+"'AND PPCode NOT IN (SELECT PPID FROM PowerPlant)");
            DataTable findproduce = Utilities.GetTable("Select * from ProducedEnergy where Date='"+Date+ "'");
            DataTable findinterchange = Utilities.GetTable("SELECT * FROM [InterchangedEnergy] WHERE Date='"+startDate+"'or Date='"+endDate +"' ORDER BY Date");
            DataTable findmanategh = Utilities.GetTable("select Peak from Manategh where Date='"+Date+"'and Title= N'توليد بدون صنايع' order by Code");


            if (findrev.Rows.Count == 0 || findinterchange.Rows.Count == 0 || findmanategh.Rows.Count==0 || findproduce.Rows.Count==0)
            {

                //Result = MessageBox.Show("Do you Want To Close BiddingStrategy AND Download Internet Data ? \r\n\r\n If Select <No>, Use Nearest Date! \r\n ", "There is No Data for Internet File at Date:" + new PersianDate(current).ToString("d") + " !", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                
                //if (Result == DialogResult.No)
                //{
                    DataTable odat = Utilities.GetTable("Select distinct PPCode,Date from dbo.RegionNetComp where Date='" + FindNearRegionDate(new PersianDate(current).ToString("d")) + "'AND Code='"+gencocode+"'AND PPCode NOT IN (SELECT PPID FROM PowerPlant)");
                    return true;

                //}
                //if (Result == DialogResult.Yes)
                //{

                //    newUpdateProgressBar(progressBar1.Maximum, " End UnSuccessfull For More Information Click Below lable.....");
                                   
                //    thread.Abort();

                   
                //}
            }
            return false;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LackOfInternetDataForm frm = new LackOfInternetDataForm(CurrentDateValue,TrainingDays);
            frm.ShowDialog();
        }
        private bool CalculatePreSolve(string plant,bool isnearest)
        {
           

            string strCmd = "select * from powerplant WHERE PPName='" + plant + "'";

            DataTable dtPlants = Utilities.GetTable(strCmd);
            int plants_Num = dtPlants.Rows.Count;

            bool success = CalculateBiddingPreSolve(PersianDateConverter.ToGregorianDateTime(CurrentDateValue),
                              PersianDateConverter.ToGregorianDateTime(BiddingDateValue),
                              int.Parse(dtPlants.Rows[0]["PPID"].ToString().Trim()),
                              dtPlants.Rows[0]["PPName"].ToString().Trim(),isnearest);


            return success;
          
            


        }
        public bool CalculateBiddingPreSolve(DateTime baseDate, DateTime fututeDate, int ppId, string ppname, bool isnearest)
        {

            //bool isnearest = false;

            //isnearest = checkselecteddate(baseDate);

            commonnearest = isnearest;
            BiddingStrategyData oData = null;
            UpdateProgressBar(1, "Initializing For PreSolve Plant :"+ ppname + ".");

            LogBidding("/*********************************************/", new TimeSpan());
            LogBidding("/*********************************************/", new TimeSpan());

            DataSet myDs = UtilityPowerPlantFunctions.GetUnits(ppId);

            DataTable[] orderedPackages = UtilityPowerPlantFunctions.OrderPackages(myDs);

            int forcastDaysAhead = (new DateTime(fututeDate.Year, fututeDate.Month, fututeDate.Day, 0, 0, 0) - new DateTime(baseDate.Year, baseDate.Month, baseDate.Day, 0, 0, 0)).Days;

            /** !!!!!!!!!!! log */
            DateTime logTimebase = DateTime.Now;
            Core_pf_Dll.Core_pf_Class oCore_pf = new Core_pf_Class();
            DateTime logTimeCur = DateTime.Now;
            LogBidding("PRECore_pf_Dll instance", logTimeCur - logTimebase);
            /** !!!!!!!!!!!!!!! */
        
            foreach (DataTable dtPackageType in orderedPackages)
            {

                int packageOrder = 0;
                if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                    orderedPackages.Length > 1)
                    packageOrder = 1;

                int loc_n = 24 * TrainingDays + 1 /*   */;
                List<double> prices = new List<double>();

                Count_view_pre = 0;

                for (int k = 1; k <= forcastDaysAhead; k++)
                {
                    //if (ppId == 206 && dtPackageType.TableName == "Gas")
                    //{                     
                      
                    //    UpdateProgressBar(9, "");
                    //}
                    //else
                    {
                        CMatrix P_Power = new CMatrix(37, TrainingDays - 3);

                        DateTime curDate = baseDate.AddDays((k - 1));

                        List<double> average = new List<double>(UtilityPowerPlantFunctions.GetAveragePriceInputMatlab(TrainingDays, curDate));

                        string strDayCount = " " + dtPackageType.TableName + " packages; " +
                            " Day " + k.ToString() + " of " + forcastDaysAhead.ToString() + " ...";
                        UpdateProgressBar(2, "Getting Data For" + strDayCount);


                        /** !!!!!!!!!!! log */
                        logTimebase = DateTime.Now;
                        //string BaseDateDate = "";
                        if (k == 1)
                        {
                            logTimebase = DateTime.Now;
                            //BaseDateDate = checkselecteddate(curDate);
                            oData = new BiddingStrategyData(TrainingDayspower, TrainingDays, TrainingDaysMinus, 2, curDate, isnearest, ppId, dtPackageType.TableName);
                            //oData.GetFirstData();
                            logTimeCur = DateTime.Now;
                            LogBidding("first BiddingStrategyData class", logTimeCur - logTimebase);

                            logTimebase = DateTime.Now;
                            prices = new List<double>(UtilityPowerPlantFunctions.Get_N_Days_PowerPlantPrices(TrainingDays, ppId, dtPackageType, baseDate, curDate, packageOrder, new PersianDate(fututeDate).ToString("d")));
                            logTimeCur = DateTime.Now;
                            LogBidding("first prices", logTimeCur - logTimebase);

                        }
                        else
                        {
                            Count_view_pre++;
                            logTimebase = DateTime.Now;
                            double[] oneDayPrice = UtilityPowerPlantFunctions.Get_1_Day_PowerPlantPrice(ppId, dtPackageType, baseDate, curDate, packageOrder, Count_view_pre, new PersianDate(fututeDate).ToString("d"));
                            prices.RemoveRange(0, 24);
                            prices.AddRange(oneDayPrice);
                            logTimeCur = DateTime.Now;
                            LogBidding("next prices", logTimeCur - logTimebase);


                            logTimebase = DateTime.Now;
                          //  oData.UpdateWithNextDay();
                            logTimeCur = DateTime.Now;
                            LogBidding("next BiddingStrategyData class", logTimeCur - logTimebase);


                        }


                        /** !!!!!!!!!!!!!!! */
                        bool noData = true;

                        for (int i = 0; i < 24 * TrainingDays && noData; i++)
                            if (prices[i] != 0)
                                noData = false;

                        if (noData)
                        {
                            //string str = "Due to lack of Data, Price Forecasting is not possible for " + ppname + " righ now." +
                            //    "\r\nYou need to load M002 Data for this plant...";

                            //MessageBox.Show(str);
                            //return false;
                        }

                        #region MR. Moosavi
                        int Test = 0;
                        int[] Error_price = new int[24 * TrainingDays];

                        for (int x = 72; x < 24 * TrainingDays; x++)
                        {
                            Test = 0;
                            if ((prices[x] == 0))
                            {
                                if (prices[x - 24] != 0)
                                {
                                    prices[x] = prices[x - 24];
                                    Test = 1;
                                }
                                else if (x - 48 >= 0 && (prices[x - 48] != 0) && (Test == 0))
                                {
                                    prices[x] = prices[x - 48];
                                    Test = 1;
                                }
                                else if (x - 72 >= 0 && (prices[x - 72] != 0) && (Test == 0))
                                {
                                    prices[x] = prices[x - 72];
                                    Test = 1;
                                }
                                if ((prices[x] == 0))
                                {
                                    Error_price[x] = 1;
                                }
                            }
                        }

                        for (int x = 48; x < 24 * TrainingDays; x++)
                        {
                            if ((prices[x - 24] != 0) && (prices[x - 48] != 0))
                            {
                                if ((prices[x] < 0.9 * prices[x - 24]) && (prices[x] < 0.9 * prices[x - 48]))
                                {
                                    prices[x] = prices[x - 24];
                                }
                                if ((prices[x] > 1.1 * prices[x - 24]) && (prices[x] < 1.1 * prices[x - 48]))
                                {
                                    prices[x] = prices[x - 24];
                                }
                            }
                            prices[x] = 330000;
                        }



                        int[,] Co = new int[2, TrainingDays - 7];
                        for (int i = 0; i <= TrainingDays - 8; i++)
                        {
                            Co[0, i] = 0;
                            Co[1, i] = 1;
                        }
                        Co[0, 1] = 2;
                        Co[1, 1] = 2;

                        double[,] Ee = new double[1, TrainingDays - 7];
                        for (int i = 0; i <= TrainingDays - 8; i++)
                        {
                            Ee[0, i] = 0;
                        }
                        Ee[0, 2] = 0.00000001;
                        #endregion MR.Moosavi

                        /** !!!!!!!!!!! log */
                        UpdateProgressBar(3, "PreSolve Preparing Data" + strDayCount);


                        /** !!!!!!!!!!!!!!! */
                        ///**************************************************/

                        double[] final_Forecast = new double[24];
                        double[] vr = new double[24];

                        // Two 24*201 Matrix at end!!
                        CMatrix ff_All = new CMatrix(0, 201); // a new row gets added in each j loop
                        CMatrix pdist_All = new CMatrix(0, 201); // a new row gets added in each j loop



                        int lineTypesNo = Enum.GetNames(typeof(Line_codes)).Length;
                        int ROW_NUM = 28 + lineTypesNo + 1;

                        ///////////////////////////with power////////////////////////////////
                        //int revnum = exeptcc(isnearest);
                        //int ChangeRow = (exeptcc(isnearest) + 73);
                        //double[,] P_All = new double[ChangeRow * 24, TrainingDays - TrainingDaysMinus];
                        //double[,] T_All = new double[1 * 24, TrainingDays - TrainingDaysMinus];
                        //double[,] X_All = new double[ChangeRow * 24, 1];
                        //double[] GainNum_All = new double[24];
                        ///////////////////////////////////////////////////////////////////////

                        double[,] P_All = new double[ROW_NUM * 24, TrainingDays - TrainingDaysMinus];
                        double[,] T_All = new double[1 * 24, TrainingDays - TrainingDaysMinus];
                        double[,] X_All = new double[ROW_NUM * 24, 1];
                        double[] GainNum_All = new double[24];





                        for (int j = 1; j <= 24; j++)
                        {



                            CMatrix P = new CMatrix(ROW_NUM, TrainingDays - TrainingDaysMinus + 1);
                            CMatrix T = new CMatrix(1, TrainingDays - TrainingDaysMinus);

                            /** !!!!!!!!!!! log */
                            logTimebase = DateTime.Now;
                            /** !!!!!!!!!!!!!!! */

                            #region calculation

                            for (int i = 1; i <= TrainingDays - 8; i++)
                            {
                                int e = 24;

                                int temp1 = loc_n - (24 * i) + j;
                                int temp2 = loc_n - (24 * i) - e + j;

                                P[0, i - 1] = (prices[temp1 - 2] - prices[temp2 - 2] + Ee[0, i]) / prices[temp2 - 2];
                                P[1, i - 1] = (prices[temp1 - 2] - prices[temp2 - 3] + Ee[0, i]) / prices[temp2 - 3];
                                P[2, i - 1] = (prices[temp1 - 2] - prices[temp2 - 1] + Ee[0, i]) / prices[temp2 - 1];
                                P[3, i - 1] = (prices[temp1 - 2] - prices[temp2 - 4] + Ee[0, i]) / prices[temp2 - 4];
                                P[4, i - 1] = (prices[temp1 - 2] - prices[temp2] + Ee[0, i]) / prices[temp2];

                                P[5, i - 1] = (prices[temp1 - 3] - prices[temp2 - 3] + Ee[0, i]) / prices[temp2 - 3];
                                P[6, i - 1] = (prices[temp1 - Co[1, i]] - prices[temp2 - 1] + Ee[0, i]) / prices[temp2 - 1];
                                P[7, i - 1] = (prices[temp1 - 4] - prices[temp2 - 4] + Ee[0, i]) / prices[temp2 - 4];
                                P[8, i - 1] = (prices[temp1 - Co[0, i]] - prices[temp2] + Ee[0, i]) / prices[temp2];

                                P[ROW_NUM - 1, i - 1] = (average[temp1 - Co[0, i]] - average[temp2] + Ee[0, i]) / average[temp2];

                                e = 48;
                                temp2 = loc_n - 24 * i - e + j;
                                P[9, i - 1] = (prices[temp1 - 2] - prices[temp2 - 2] + Ee[0, i]) / prices[temp2 - 2];
                                P[10, i - 1] = (prices[temp1 - 2] - prices[temp2 - 3] + Ee[0, i]) / prices[temp2 - 3];
                                P[11, i - 1] = (prices[temp1 - 2] - prices[temp2 - 1] + Ee[0, i]) / prices[temp2 - 1];
                                P[12, i - 1] = (prices[temp1 - 2] - prices[temp2 - 4] + Ee[0, i]) / prices[temp2 - 4];
                                P[13, i - 1] = (prices[temp1 - 2] - prices[temp2] + Ee[0, i]) / prices[temp2];

                                P[14, i - 1] = (prices[temp1 - 3] - prices[temp2 - 3] + Ee[0, i]) / prices[temp2 - 3];
                                P[15, i - 1] = (prices[temp1 - Co[1, i]] - prices[temp2 - 1] + Ee[0, i]) / prices[temp2 - 1];
                                P[16, i - 1] = (prices[temp1 - 4] - prices[temp2 - 4] + Ee[0, i]) / prices[temp2 - 4];
                                P[17, i - 1] = (prices[temp1 - Co[0, i]] - prices[temp2] + Ee[0, i]) / prices[temp2];

                                e = 168;
                                temp2 = loc_n - 24 * i - e + j;
                                P[18, i - 1] = (prices[temp1 - 2] - prices[temp2 - 2] + Ee[0, i]) / prices[temp2 - 2];
                                P[19, i - 1] = (prices[temp1 - 2] - prices[temp2 - 3] + Ee[0, i]) / prices[temp2 - 3];
                                P[20, i - 1] = (prices[temp1 - 2] - prices[temp2 - 1] + Ee[0, i]) / prices[temp2 - 1];
                                P[21, i - 1] = (prices[temp1 - 2] - prices[temp2 - 4] + Ee[0, i]) / prices[temp2 - 4];
                                P[22, i - 1] = (prices[temp1 - 2] - prices[temp2] + Ee[0, i]) / prices[temp2];

                                P[23, i - 1] = (prices[temp1 - 3] - prices[temp2 - 3] + Ee[0, i]) / prices[temp2 - 3];
                                P[24, i - 1] = (prices[temp1 - Co[1, i]] - prices[temp2 - 1] + Ee[0, i]) / prices[temp2 - 1];
                                P[25, i - 1] = (prices[temp1 - 4] - prices[temp2 - 4] + Ee[0, i]) / prices[temp2 - 4];
                                P[26, i - 1] = (prices[temp1 - Co[0, i]] - prices[temp2] + Ee[0, i]) / prices[temp2];

                                //P[27, i-1] = A(temp1 - 1, 4);
                                string strDayOfWeek =
                                    curDate.Subtract(new TimeSpan(temp1 - 1, 0, 0)).
                                        DayOfWeek.ToString();

                                int dayIndex;
                                for (dayIndex = 1; dayIndex <= 7; dayIndex++)
                                {
                                    Days curDay = (Days)Enum.Parse(typeof(Days), dayIndex.ToString());
                                    if (curDay.ToString() == strDayOfWeek)
                                        break;
                                }
                                P[27, i - 1] = dayIndex;

                                //timing////////////////////
                                //for (int b = 0; b < Enum.GetNames(typeof(Line_codes)).Length; b++)
                                //{
                                //    try
                                //    {
                                //        P[b + 28, i - 1] = oData.LC_H_N[b, temp1 - 2] + Ee[0, i];
                                //    }
                                //    catch (Exception)
                                //    {
                                //        throw;
                                //    }
                                //}


                                if (i - 2 >= 0)
                                    T[0, i - 2] = ((prices[temp1 + 24 - 2] - prices[temp1 - 2]) + Ee[0, i])
                                                    / prices[temp1 - 2];


                                /////////////////////////////////////



                            }
                            ///////////////// fill 43th to 72th row
                            /////////////////44 to 73

                            //timing////////////////////////////////////////////////
                            //P = P.AddRow(oData.M_PriceForecast_44thRow.GetRow(j - 1));
                            //P = P.AddRow(oData.L_ProducedRegionsHourly[j - 1]);
                            //P = P.AddRow(oData.M_PriceForecast_59_72);
                            /////////////////////////////////////////////////////////




                            //////////////////////with power////////////////////////
                            // P = P.AddRow(oData.L_Rev_Produced[j - 1]);
                            ////////////////////////////////////////////////////////
                            #endregion calculation

                            CMatrix XTemp = P.GetColumn(0);
                            P = P.RemoveAtColumn(0);


                            GainNum_All[j - 1] = prices[loc_n - 24 + j - 2];

                            /////////////////////////////////with power///////////////////////
                            //for (int iRow = 0; iRow < ChangeRow; iRow++)
                            //{
                            //    X_All[ChangeRow * (j - 1) + iRow, 0] = XTemp[iRow, 0];
                            //    for (int jCol = 0; jCol < TrainingDays - TrainingDaysMinus; jCol++)
                            //    {
                            //        T_All[j - 1, jCol] = T[0, jCol];
                            //        P_All[ChangeRow * (j - 1) + iRow, jCol] = P[iRow, jCol];
                            //    }
                            //}
                            //////////////////////////////////////////////////////////////////

                            for (int iRow = 0; iRow < ROW_NUM; iRow++)
                            {
                                X_All[ROW_NUM * (j - 1) + iRow, 0] = XTemp[iRow, 0];
                                for (int jCol = 0; jCol < TrainingDays - TrainingDaysMinus; jCol++)
                                {
                                    T_All[j - 1, jCol] = T[0, jCol];
                                    P_All[ROW_NUM * (j - 1) + iRow, jCol] = P[iRow, jCol];
                                }
                            }


                        }

                        logTimebase = DateTime.Now;
                        /** !!!!!!!!!!!!!!! */


                        MWArray[] argsOut = oCore_pf.core_pf_new_noout_xls(0,
                                        (MWArray)DoubleArrayToMWNumericArray(P_All),
                                        (MWArray)DoubleArrayToMWNumericArray(X_All),
                                        (MWArray)DoubleArrayToMWNumericArray(T_All)
                            //, (MWArray)DoubleArrayToMWNumericArray(AvagaePriceInput)
                                        );

                        logTimeCur = DateTime.Now;
                        LogBidding("Call DLL", logTimeCur - logTimebase);

                        UpdateProgressBar(5, "Getting results" + strDayCount);// two step jump

                        logTimebase = DateTime.Now;
                        CMatrix q_ALL = ReadExcelOut("out1", 1 * 24, 1);
                        logTimeCur = DateTime.Now;
                        LogBidding("out 1 * 24, 1", logTimeCur - logTimebase);

                        logTimebase = DateTime.Now;
                        CMatrix xOut_ALL = ReadExcelOut("out2", 1 * 24, 201);
                        logTimeCur = DateTime.Now;
                        LogBidding("out  1 * 24, 201", logTimeCur - logTimebase);

                        logTimebase = DateTime.Now;
                        CMatrix pdist_n_ALL = ReadExcelOut("out3", 400 * 24, 201);
                        logTimeCur = DateTime.Now;
                        LogBidding("out 400 * 24, 201", logTimeCur - logTimebase);

                        logTimebase = DateTime.Now;
                        CMatrix s_ALL = ReadExcelOut("out4", 1 * 24, 1);
                        logTimeCur = DateTime.Now;
                        LogBidding("out 1 * 24, 1", logTimeCur - logTimebase);

                        //////////////////////////////////////////////

                        UpdateProgressBar(6, "PreSolve Preparing results" + strDayCount);// two step jump

                        logTimebase = DateTime.Now;
                        for (int hour = 0; hour < 24; hour++)
                        {
                            final_Forecast[hour] = GainNum_All[hour] * (1 + q_ALL[hour, 0]); ///  ????????? 

                            CMatrix gainMatrix = new CMatrix(1, 201);
                            for (int colj = 0; colj < 201; colj++)
                                gainMatrix[0, colj] = GainNum_All[hour];

                            ff_All = ff_All.AddRow((GainNum_All[hour] * xOut_ALL.GetRow(hour)) + gainMatrix);

                            vr[hour] = GainNum_All[hour] * s_ALL[hour, 0] / 20;


                            CMatrix PdistTemp = pdist_n_ALL.GetRows(400 * hour, 400 * (hour + 1) - 1) / GainNum_All[hour];
                            pdist_All = pdist_All.AddRow(PdistTemp.GetColumnMean());
                        }

                        /** !!!!!!!!!!! log */
                        logTimeCur = DateTime.Now;
                        LogBidding("Attatch Arrays ", logTimeCur - logTimebase);
                        /** !!!!!!!!!!!!!!! */

                        /** !!!!!!!!!!! log */
                        logTimebase = DateTime.Now;
                        UpdateProgressBar(7, "PreSolve Save Bidding Results for" + strDayCount);
                        SavePriceForcastResults(ppId, new PersianDate(baseDate.AddDays(k)), final_Forecast, vr, ff_All, pdist_All, dtPackageType.TableName);
                        logTimeCur = DateTime.Now;
                        LogBidding("SaveForcastResults", logTimeCur - logTimebase);
                        /** !!!!!!!!!!!!!!! */

                        ///////////////////////with power///////////////////////
                        //if (k != forcastDaysAhead)
                        //{
                        ///////////////for not empty//////////////////
                        UpdateProgressBar(8, "");
                        // UpdateProgressBar(9, "");
                        //////////////////////////////////////////////
                        // UpdateProgressBar(progressBar1.Maximum, "");
                        //}
                        ///////////////////////////////////////////////////////
                    }
                }
             
            }

            return true;
        }

        private void BiddingStrategyProgressForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (thread != null)
            {
                thread.Abort();
            }
        }

        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private static bool MyboolParse(string str)
        {
            if (str.Trim() == "")
                return false;
            else
            {
                try { return bool.Parse(str.Trim()); }
                catch { return false; }
            }
        }
        private string buildmaxdate(string[] arrmax)
        {
            string maxdate = null;
            for (int fd = 0; fd < arrmax.Length; fd++)
            {
                if (maxdate == null || string.Compare(maxdate, arrmax[fd]) < 0)
                    maxdate = arrmax[fd];
            }
            return maxdate;

        }   
    }
}