﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using NRI.SBS.Common;
using System.Xml.Serialization;
using System.IO;

namespace PowerPlantProject
{
    public partial class NameFormat : Form
    {
        string Filename;
        public NameFormat(string filename)
        {
            Filename = filename;
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void NameFormat_Load(object sender, EventArgs e)
        {

            ///////////////////////////////////////////////////////////////

            if (Filename == "M002")
            {
                groupBox1.Visible = true;              

                DataTable d = Utilities.GetTable("select * from  emsformat");
                if (d.Rows[0][0].ToString() != "Null")
                {
                    if (d.Rows[0][0].ToString() == "True")
                    {
                        rdEms.Checked = true;
                        rdformatmarket.Checked = false;
                    }
                }              

            }

            ///////////////////////////////////////////////////////////////



            this.Text = "Name Format For  :  "+Filename +"  Files";

            /////////////////////////////////for folders//////////////////////////////////////////

            DataTable dd = Utilities.GetTable("select * from FilesNameFormat where filename='"+ Filename.Trim()+"'");
            if (dd.Rows.Count > 0)
            {
                string x = dd.Rows[0]["FoldersFormat"].ToString().Trim();
                string[] spx=x.Split('*');

               
                    
                    if (0 < spx.Length)
                    {
                        comboBox1.Text = spx[0].ToString().Trim();
                    }
                    if (1 < spx.Length)
                    {
                        comboBox2.Text = spx[1].ToString().Trim();
                    }
                    if (2 < spx.Length)
                    {
                        comboBox3.Text = spx[2].ToString().Trim();
                    }
                    if (3 < spx.Length)
                    {
                        comboBox4.Text = spx[3].ToString().Trim();
                    }
                    if (4 < spx.Length)
                    {
                        comboBox5.Text = spx[4].ToString().Trim();
                    }
                    if (5 < spx.Length)
                    {
                        comboBox6.Text = spx[5].ToString().Trim();
                    }
                    if (6 < spx.Length)
                    {
                        comboBox7.Text = spx[6].ToString().Trim();
                    }
                    if (7 < spx.Length)
                    {
                        comboBox8.Text = spx[7].ToString().Trim();
                    }
                    if (8 < spx.Length)
                    {
                        comboBox9.Text = spx[8].ToString().Trim();
                    }
                    if (9 < spx.Length)
                    {
                        comboBox10.Text = spx[9].ToString().Trim();
                    }
                





             ////////////////////////////////////for excel name////////////////////////////////////

                string x2 = dd.Rows[0]["ExcelName"].ToString().Trim();
                string[] spx2 = x2.Split('*');

                if (0 < spx2.Length)
                {
                    comboBox11.Text = spx2[0].ToString().Trim();
                }
                if (1 < spx2.Length)
                {
                    comboBox12.Text = spx2[1].ToString().Trim();
                }
                if (2 < spx2.Length)
                {
                    comboBox13.Text = spx2[2].ToString().Trim();
                }
                if (3 < spx2.Length)
                {
                    comboBox14.Text = spx2[3].ToString().Trim();
                }
                if (4 < spx2.Length)
                {
                    comboBox15.Text = spx2[4].ToString().Trim();
                }
                if (5 < spx2.Length)
                {
                    comboBox16.Text = spx2[5].ToString().Trim();
                }
                if (6 < spx2.Length)
                {
                    comboBox17.Text = spx2[6].ToString().Trim();
                }
                if (7 < spx2.Length)
                {
                    comboBox18.Text = spx2[7].ToString().Trim();
                }
                if (8 < spx2.Length)
                {
                    comboBox19.Text = spx2[8].ToString().Trim();
                }
                if (9 < spx2.Length)
                {
                    comboBox20.Text = spx2[9].ToString().Trim();
                }
                if (10 < spx2.Length)
                {
                    comboBox21.Text = spx2[10].ToString().Trim();
                }
                if (11 < spx2.Length)
                {
                    comboBox22.Text = spx2[11].ToString().Trim();
                }
                if (12 < spx2.Length)
                {
                    comboBox23.Text = spx2[12].ToString().Trim();
                }
                if (13 < spx2.Length)
                {
                    comboBox24.Text = spx2[13].ToString().Trim();
                }
            }

            //////////////////////////////////for sheet name//////////////////////////////////////
            if (dd.Rows.Count > 0)
            {
                string x3 = dd.Rows[0]["SheetName"].ToString();
                cmbsheet.Text = x3;
            }

        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            ///////////////////////////////////////////////////////////////////////////////////

            if (Filename == "M002")
            {
                if (rdEms.Checked)
                {
                    DataTable d = Utilities.GetTable("delete from  emsformat");
                    DataTable c = Utilities.GetTable("insert into emsformat(m002) values('True')");
                }
                else
                {
                    DataTable d = Utilities.GetTable("delete from  emsformat");
                    DataTable c = Utilities.GetTable("insert into emsformat(m002) values('False')");
                }
            }


            //////////////////////////////////////////////////////////////////////////////////




            string nameformat = "";
            if(comboBox11.Text.Trim()!="")
                nameformat += comboBox11.Text.Trim();
            if (comboBox12.Text.Trim() != "")
                nameformat += "*" + comboBox12.Text.Trim();
            if (comboBox13.Text.Trim() != "")
                nameformat += "*" + comboBox13.Text.Trim();
            if(comboBox14.Text.Trim()!="")
                nameformat += "*" + comboBox14.Text.Trim();
            if (comboBox15.Text.Trim() != "")
                nameformat += "*" + comboBox15.Text.Trim();
            if (comboBox16.Text.Trim() != "")
                nameformat += "*" + comboBox16.Text.Trim();
            if (comboBox17.Text.Trim() != "")
                nameformat += "*" + comboBox17.Text.Trim();
            if (comboBox18.Text.Trim() != "")
                nameformat += "*" + comboBox18.Text.Trim();
            if (comboBox19.Text.Trim() != "")
                nameformat += "*" + comboBox19.Text.Trim();
            if (comboBox20.Text.Trim() != "")
                nameformat += "*" + comboBox20.Text.Trim();
            if (comboBox21.Text.Trim() != "")
                nameformat += "*" + comboBox21.Text.Trim();
            if (comboBox22.Text.Trim() != "")
                nameformat += "*" + comboBox22.Text.Trim();
            if (comboBox23.Text.Trim() != "")
                nameformat += "*" + comboBox23.Text.Trim();
            if (comboBox24.Text.Trim() != "")
                nameformat += "*" + comboBox24.Text.Trim();

            ///////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////

            string pathformat = "";
            if (comboBox1.Text.Trim() != "")
                pathformat +=  comboBox1.Text.Trim();
            if (comboBox2.Text.Trim() != "")
                pathformat += "*" + comboBox2.Text.Trim();
            if (comboBox3.Text.Trim() != "")
                pathformat += "*" + comboBox3.Text.Trim();
            if (comboBox4.Text.Trim() != "")
                pathformat += "*" + comboBox4.Text.Trim();
            if (comboBox5.Text.Trim() != "")
                pathformat += "*" + comboBox5.Text.Trim();
            if (comboBox6.Text.Trim() != "")
                pathformat += "*" + comboBox6.Text.Trim();
            if (comboBox7.Text.Trim() != "")
                pathformat += "*" + comboBox7.Text.Trim();
            if (comboBox8.Text.Trim() != "")
                pathformat += "*" + comboBox8.Text.Trim();
            if (comboBox9.Text.Trim() != "")
                pathformat += "*" + comboBox9.Text.Trim();
            if (comboBox10.Text.Trim() != "")
                pathformat += "*" + comboBox10.Text.Trim();


            try
            {
                DataTable ddel = Utilities.GetTable("delete from FilesNameFormat where filename='" + Filename + "'");
                DataTable dinsert = Utilities.GetTable("insert into  FilesNameFormat (filename,FoldersFormat,ExcelName,SheetName) values('" + Filename + "','" + pathformat.Trim() + "','" + nameformat.Trim() + "',N'" + cmbsheet.Text + "')");
                MessageBox.Show("Save Successfully");
            }
            catch
            {
                MessageBox.Show("Save UnSuccessfully");
            }
        }

       
    }
}
