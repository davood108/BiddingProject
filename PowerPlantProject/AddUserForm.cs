﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NRI.SBS.Common;
using System.Data.SqlClient;
using FarsiLibrary.Utils;
using System.Security.Cryptography;

namespace PowerPlantProject
{
    public partial class AddUserForm : Form
    {
        public AddUserForm()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }
                btnsave.BackColor = FormColors.GetColor().Buttonbackcolor;
                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                //////////////////////////////////textbox////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                        c.BackColor = FormColors.GetColor().Textbackcolor;

                }
                foreach (Control c in groupBox1.Controls)
                {
                    if (c is TextBox || c is ListBox)
                        c.BackColor = FormColors.GetColor().Textbackcolor;

                }




                ///////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
           

            lblMsg.Text = "";
            DataTable dt=Utilities.GetTable("SELECT ID FROM login WHERE Username='" + txtusername.Text.Trim() + "'");
              
            if (dt.Rows.Count > 0)
            {
                lblMsg.Text = "";
                lblMsg.Text = "There is a user with this user name.please choose another one.";
            }
            else
            {
               
                try
                {
                    if (txtusername.Text != "" && txtpass.Text != "" && cmbrole.Text != "" && txtfirstname.Text!="" && txtlastname.Text!="" && validrole(cmbrole.Text.Trim()))
                    {
                        DataTable dt1 = Utilities.GetTable("Insert into login (FirstName,LastName,Username,Password,Role) values ('" + txtfirstname.Text.Trim() + "','" +
                         txtlastname.Text.Trim() + "','" + txtusername.Text.Trim() + "','" + EncryptPassword(txtpass.Text.Trim()) + "','" +
                         cmbrole.Text.Trim() + "')");

                        MessageBox.Show("New Account Add Successfully.");
                        ClearTEXT();
                    }
                    else
                    {
                        if (txtusername.Text == "" || txtpass.Text == "" || cmbrole.Text == "" || txtfirstname.Text == "" || txtlastname.Text == "") MessageBox.Show("Please Fill All Fields .");
                        else if (txtpass.Text == "") MessageBox.Show("Please Fill Minum 4 characters in password !.");
                        else if (validrole(cmbrole.Text.Trim()) == false) MessageBox.Show("Please Select Just Defined Roles!.");
                      

                    }
                }
                catch (Exception ex)
                {
                    lblMsg.Text = "";
                    lblMsg.Text = "There is a problem in inserting data to database";
                }
              
            }
           
        }

        private string EncryptPassword(string Password)
        {
            SHA1Managed sh = new SHA1Managed();
            byte[] hash = sh.ComputeHash(System.Text.UnicodeEncoding.Unicode.GetBytes(Password));

            string hashedPassword = "";
            foreach (byte b in hash)
                hashedPassword += b.ToString();
            return hashedPassword;
        }

        private void AddUserForm_Load(object sender, EventArgs e)
        {
           
            label1.Text = "Dear User (  " + User.getUser().Username + " ):\r\n\r\n To Add New Users Please Fill Items Below And Press Save.";
        }
        private void ClearTEXT()
        {
            foreach (Control ctrl in groupBox1.Controls)
                if (ctrl is TextBox)
                    ctrl.Text = string.Empty;
                else if (ctrl is ComboBox)
                    ctrl.Text = string.Empty;

        }

       

        private void txtusername_Leave(object sender, EventArgs e)
        {
            if (txtusername.Text.Length < 4)
            {
                txtusername.Text = "";
                lblMsg.Text = "Please Enter Minimum 4 character !.";
            }
            else lblMsg.Text = "";

        }

        private void txtpass_Leave(object sender, EventArgs e)
        {
            if (txtpass.Text.Length < 4)
            {
                txtpass.Text = "";
                lblMsg.Text = "Please Enter Minimum 4 character !.";
            }
            else lblMsg.Text = "";
        }

        private bool validrole(string role)
        {

            foreach (string name in Enum.GetNames(typeof(DefinedRoles)))
            {
                if (role == name) return true;

            }
            return false;
        }

       
    }
}
