﻿namespace PowerPlantProject
{
    partial class ManualBidding
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManualBidding));
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.PPID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Block = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PriceMax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Interval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.paneleasy = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnsaveeasy = new System.Windows.Forms.Button();
            this.labelh3 = new System.Windows.Forms.Label();
            this.labelh4 = new System.Windows.Forms.Label();
            this.labelh5 = new System.Windows.Forms.Label();
            this.labelh6 = new System.Windows.Forms.Label();
            this.labelh7 = new System.Windows.Forms.Label();
            this.labelh8 = new System.Windows.Forms.Label();
            this.labelh9 = new System.Windows.Forms.Label();
            this.labelh10 = new System.Windows.Forms.Label();
            this.labelh11 = new System.Windows.Forms.Label();
            this.labelh12 = new System.Windows.Forms.Label();
            this.labelh24 = new System.Windows.Forms.Label();
            this.labelh23 = new System.Windows.Forms.Label();
            this.labelh22 = new System.Windows.Forms.Label();
            this.labelh21 = new System.Windows.Forms.Label();
            this.labelh20 = new System.Windows.Forms.Label();
            this.labelh19 = new System.Windows.Forms.Label();
            this.labelh18 = new System.Windows.Forms.Label();
            this.labelh17 = new System.Windows.Forms.Label();
            this.labelh16 = new System.Windows.Forms.Label();
            this.labelh15 = new System.Windows.Forms.Label();
            this.labelh14 = new System.Windows.Forms.Label();
            this.labelh13 = new System.Windows.Forms.Label();
            this.labelh2 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.labelh1 = new System.Windows.Forms.Label();
            this.numeric70 = new System.Windows.Forms.NumericUpDown();
            this.numeric64 = new System.Windows.Forms.NumericUpDown();
            this.numeric69 = new System.Windows.Forms.NumericUpDown();
            this.numeric63 = new System.Windows.Forms.NumericUpDown();
            this.numeric72 = new System.Windows.Forms.NumericUpDown();
            this.numeric68 = new System.Windows.Forms.NumericUpDown();
            this.numeric66 = new System.Windows.Forms.NumericUpDown();
            this.numeric62 = new System.Windows.Forms.NumericUpDown();
            this.numeric71 = new System.Windows.Forms.NumericUpDown();
            this.numeric67 = new System.Windows.Forms.NumericUpDown();
            this.numeric65 = new System.Windows.Forms.NumericUpDown();
            this.numeric61 = new System.Windows.Forms.NumericUpDown();
            this.numeric58 = new System.Windows.Forms.NumericUpDown();
            this.numeric55 = new System.Windows.Forms.NumericUpDown();
            this.numeric60 = new System.Windows.Forms.NumericUpDown();
            this.numeric54 = new System.Windows.Forms.NumericUpDown();
            this.numeric57 = new System.Windows.Forms.NumericUpDown();
            this.numeric51 = new System.Windows.Forms.NumericUpDown();
            this.numeric59 = new System.Windows.Forms.NumericUpDown();
            this.numeric53 = new System.Windows.Forms.NumericUpDown();
            this.numeric56 = new System.Windows.Forms.NumericUpDown();
            this.numeric50 = new System.Windows.Forms.NumericUpDown();
            this.numeric52 = new System.Windows.Forms.NumericUpDown();
            this.numeric49 = new System.Windows.Forms.NumericUpDown();
            this.numeric46 = new System.Windows.Forms.NumericUpDown();
            this.numeric43 = new System.Windows.Forms.NumericUpDown();
            this.numeric48 = new System.Windows.Forms.NumericUpDown();
            this.numeric42 = new System.Windows.Forms.NumericUpDown();
            this.numeric45 = new System.Windows.Forms.NumericUpDown();
            this.numeric39 = new System.Windows.Forms.NumericUpDown();
            this.numeric47 = new System.Windows.Forms.NumericUpDown();
            this.numeric41 = new System.Windows.Forms.NumericUpDown();
            this.numeric44 = new System.Windows.Forms.NumericUpDown();
            this.numeric38 = new System.Windows.Forms.NumericUpDown();
            this.numeric40 = new System.Windows.Forms.NumericUpDown();
            this.numeric37 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown49 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown51 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown55 = new System.Windows.Forms.NumericUpDown();
            this.numeric34 = new System.Windows.Forms.NumericUpDown();
            this.numeric28 = new System.Windows.Forms.NumericUpDown();
            this.numeric33 = new System.Windows.Forms.NumericUpDown();
            this.numeric27 = new System.Windows.Forms.NumericUpDown();
            this.numeric36 = new System.Windows.Forms.NumericUpDown();
            this.numeric32 = new System.Windows.Forms.NumericUpDown();
            this.numeric30 = new System.Windows.Forms.NumericUpDown();
            this.numeric26 = new System.Windows.Forms.NumericUpDown();
            this.numeric35 = new System.Windows.Forms.NumericUpDown();
            this.numeric31 = new System.Windows.Forms.NumericUpDown();
            this.numeric29 = new System.Windows.Forms.NumericUpDown();
            this.numeric25 = new System.Windows.Forms.NumericUpDown();
            this.numeric22 = new System.Windows.Forms.NumericUpDown();
            this.numeric19 = new System.Windows.Forms.NumericUpDown();
            this.numeric24 = new System.Windows.Forms.NumericUpDown();
            this.numeric18 = new System.Windows.Forms.NumericUpDown();
            this.numeric21 = new System.Windows.Forms.NumericUpDown();
            this.numeric15 = new System.Windows.Forms.NumericUpDown();
            this.numeric23 = new System.Windows.Forms.NumericUpDown();
            this.numeric17 = new System.Windows.Forms.NumericUpDown();
            this.numeric20 = new System.Windows.Forms.NumericUpDown();
            this.numeric14 = new System.Windows.Forms.NumericUpDown();
            this.numeric16 = new System.Windows.Forms.NumericUpDown();
            this.numeric13 = new System.Windows.Forms.NumericUpDown();
            this.numeric10 = new System.Windows.Forms.NumericUpDown();
            this.numeric7 = new System.Windows.Forms.NumericUpDown();
            this.numeric12 = new System.Windows.Forms.NumericUpDown();
            this.numeric6 = new System.Windows.Forms.NumericUpDown();
            this.numeric9 = new System.Windows.Forms.NumericUpDown();
            this.numeric3 = new System.Windows.Forms.NumericUpDown();
            this.numeric11 = new System.Windows.Forms.NumericUpDown();
            this.numeric5 = new System.Windows.Forms.NumericUpDown();
            this.numeric8 = new System.Windows.Forms.NumericUpDown();
            this.numeric2 = new System.Windows.Forms.NumericUpDown();
            this.numeric4 = new System.Windows.Forms.NumericUpDown();
            this.numeric1 = new System.Windows.Forms.NumericUpDown();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnsavesimilar = new System.Windows.Forms.Button();
            this.btnsavelastbid = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnsimilarblock = new System.Windows.Forms.Button();
            this.btncap = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtzarib = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnsavehourblock = new System.Windows.Forms.Button();
            this.panehidebutton = new System.Windows.Forms.Panel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.paneleasy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PPID,
            this.Block,
            this.Hour,
            this.PriceMax,
            this.Interval});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dataGridView1.Location = new System.Drawing.Point(14, 11);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HotTrack;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.RowHeadersVisible = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.LemonChiffon;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(645, 426);
            this.dataGridView1.TabIndex = 0;
            // 
            // PPID
            // 
            this.PPID.HeaderText = "PPID";
            this.PPID.Name = "PPID";
            // 
            // Block
            // 
            this.Block.HeaderText = "Block";
            this.Block.Name = "Block";
            // 
            // Hour
            // 
            this.Hour.HeaderText = "Hour";
            this.Hour.Name = "Hour";
            // 
            // PriceMax
            // 
            this.PriceMax.HeaderText = "PriceMax";
            this.PriceMax.Name = "PriceMax";
            // 
            // Interval
            // 
            this.Interval.HeaderText = "Interval";
            this.Interval.Name = "Interval";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.Location = new System.Drawing.Point(23, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panel1.Controls.Add(this.paneleasy);
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Location = new System.Drawing.Point(12, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(669, 450);
            this.panel1.TabIndex = 3;
            // 
            // paneleasy
            // 
            this.paneleasy.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.paneleasy.Controls.Add(this.button1);
            this.paneleasy.Controls.Add(this.textBox1);
            this.paneleasy.Controls.Add(this.btnsaveeasy);
            this.paneleasy.Controls.Add(this.labelh3);
            this.paneleasy.Controls.Add(this.labelh4);
            this.paneleasy.Controls.Add(this.labelh5);
            this.paneleasy.Controls.Add(this.labelh6);
            this.paneleasy.Controls.Add(this.labelh7);
            this.paneleasy.Controls.Add(this.labelh8);
            this.paneleasy.Controls.Add(this.labelh9);
            this.paneleasy.Controls.Add(this.labelh10);
            this.paneleasy.Controls.Add(this.labelh11);
            this.paneleasy.Controls.Add(this.labelh12);
            this.paneleasy.Controls.Add(this.labelh24);
            this.paneleasy.Controls.Add(this.labelh23);
            this.paneleasy.Controls.Add(this.labelh22);
            this.paneleasy.Controls.Add(this.labelh21);
            this.paneleasy.Controls.Add(this.labelh20);
            this.paneleasy.Controls.Add(this.labelh19);
            this.paneleasy.Controls.Add(this.labelh18);
            this.paneleasy.Controls.Add(this.labelh17);
            this.paneleasy.Controls.Add(this.labelh16);
            this.paneleasy.Controls.Add(this.labelh15);
            this.paneleasy.Controls.Add(this.labelh14);
            this.paneleasy.Controls.Add(this.labelh13);
            this.paneleasy.Controls.Add(this.labelh2);
            this.paneleasy.Controls.Add(this.label32);
            this.paneleasy.Controls.Add(this.label3);
            this.paneleasy.Controls.Add(this.label29);
            this.paneleasy.Controls.Add(this.label31);
            this.paneleasy.Controls.Add(this.label28);
            this.paneleasy.Controls.Add(this.label30);
            this.paneleasy.Controls.Add(this.label27);
            this.paneleasy.Controls.Add(this.labelh1);
            this.paneleasy.Controls.Add(this.numeric70);
            this.paneleasy.Controls.Add(this.numeric64);
            this.paneleasy.Controls.Add(this.numeric69);
            this.paneleasy.Controls.Add(this.numeric63);
            this.paneleasy.Controls.Add(this.numeric72);
            this.paneleasy.Controls.Add(this.numeric68);
            this.paneleasy.Controls.Add(this.numeric66);
            this.paneleasy.Controls.Add(this.numeric62);
            this.paneleasy.Controls.Add(this.numeric71);
            this.paneleasy.Controls.Add(this.numeric67);
            this.paneleasy.Controls.Add(this.numeric65);
            this.paneleasy.Controls.Add(this.numeric61);
            this.paneleasy.Controls.Add(this.numeric58);
            this.paneleasy.Controls.Add(this.numeric55);
            this.paneleasy.Controls.Add(this.numeric60);
            this.paneleasy.Controls.Add(this.numeric54);
            this.paneleasy.Controls.Add(this.numeric57);
            this.paneleasy.Controls.Add(this.numeric51);
            this.paneleasy.Controls.Add(this.numeric59);
            this.paneleasy.Controls.Add(this.numeric53);
            this.paneleasy.Controls.Add(this.numeric56);
            this.paneleasy.Controls.Add(this.numeric50);
            this.paneleasy.Controls.Add(this.numeric52);
            this.paneleasy.Controls.Add(this.numeric49);
            this.paneleasy.Controls.Add(this.numeric46);
            this.paneleasy.Controls.Add(this.numeric43);
            this.paneleasy.Controls.Add(this.numeric48);
            this.paneleasy.Controls.Add(this.numeric42);
            this.paneleasy.Controls.Add(this.numeric45);
            this.paneleasy.Controls.Add(this.numeric39);
            this.paneleasy.Controls.Add(this.numeric47);
            this.paneleasy.Controls.Add(this.numeric41);
            this.paneleasy.Controls.Add(this.numeric44);
            this.paneleasy.Controls.Add(this.numeric38);
            this.paneleasy.Controls.Add(this.numeric40);
            this.paneleasy.Controls.Add(this.numeric37);
            this.paneleasy.Controls.Add(this.numericUpDown49);
            this.paneleasy.Controls.Add(this.numericUpDown51);
            this.paneleasy.Controls.Add(this.numericUpDown55);
            this.paneleasy.Controls.Add(this.numeric34);
            this.paneleasy.Controls.Add(this.numeric28);
            this.paneleasy.Controls.Add(this.numeric33);
            this.paneleasy.Controls.Add(this.numeric27);
            this.paneleasy.Controls.Add(this.numeric36);
            this.paneleasy.Controls.Add(this.numeric32);
            this.paneleasy.Controls.Add(this.numeric30);
            this.paneleasy.Controls.Add(this.numeric26);
            this.paneleasy.Controls.Add(this.numeric35);
            this.paneleasy.Controls.Add(this.numeric31);
            this.paneleasy.Controls.Add(this.numeric29);
            this.paneleasy.Controls.Add(this.numeric25);
            this.paneleasy.Controls.Add(this.numeric22);
            this.paneleasy.Controls.Add(this.numeric19);
            this.paneleasy.Controls.Add(this.numeric24);
            this.paneleasy.Controls.Add(this.numeric18);
            this.paneleasy.Controls.Add(this.numeric21);
            this.paneleasy.Controls.Add(this.numeric15);
            this.paneleasy.Controls.Add(this.numeric23);
            this.paneleasy.Controls.Add(this.numeric17);
            this.paneleasy.Controls.Add(this.numeric20);
            this.paneleasy.Controls.Add(this.numeric14);
            this.paneleasy.Controls.Add(this.numeric16);
            this.paneleasy.Controls.Add(this.numeric13);
            this.paneleasy.Controls.Add(this.numeric10);
            this.paneleasy.Controls.Add(this.numeric7);
            this.paneleasy.Controls.Add(this.numeric12);
            this.paneleasy.Controls.Add(this.numeric6);
            this.paneleasy.Controls.Add(this.numeric9);
            this.paneleasy.Controls.Add(this.numeric3);
            this.paneleasy.Controls.Add(this.numeric11);
            this.paneleasy.Controls.Add(this.numeric5);
            this.paneleasy.Controls.Add(this.numeric8);
            this.paneleasy.Controls.Add(this.numeric2);
            this.paneleasy.Controls.Add(this.numeric4);
            this.paneleasy.Controls.Add(this.numeric1);
            this.paneleasy.Location = new System.Drawing.Point(0, 0);
            this.paneleasy.Name = "paneleasy";
            this.paneleasy.Size = new System.Drawing.Size(669, 444);
            this.paneleasy.TabIndex = 0;
            this.paneleasy.Visible = false;
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(399, 21);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(44, 19);
            this.button1.TabIndex = 98;
            this.button1.Text = "Set";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(293, 20);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 97;
            // 
            // btnsaveeasy
            // 
            this.btnsaveeasy.BackColor = System.Drawing.Color.CadetBlue;
            this.btnsaveeasy.BackgroundImage = global::PowerPlantProject.Properties.Resources.Savedata;
            this.btnsaveeasy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnsaveeasy.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsaveeasy.Location = new System.Drawing.Point(293, 407);
            this.btnsaveeasy.Name = "btnsaveeasy";
            this.btnsaveeasy.Size = new System.Drawing.Size(86, 27);
            this.btnsaveeasy.TabIndex = 74;
            this.btnsaveeasy.Text = "Save ";
            this.btnsaveeasy.UseVisualStyleBackColor = false;
            this.btnsaveeasy.Click += new System.EventHandler(this.btnsaveeasy_Click);
            // 
            // labelh3
            // 
            this.labelh3.AutoSize = true;
            this.labelh3.Location = new System.Drawing.Point(64, 140);
            this.labelh3.Name = "labelh3";
            this.labelh3.Size = new System.Drawing.Size(21, 13);
            this.labelh3.TabIndex = 96;
            this.labelh3.Text = "H3";
            // 
            // labelh4
            // 
            this.labelh4.AutoSize = true;
            this.labelh4.Location = new System.Drawing.Point(64, 166);
            this.labelh4.Name = "labelh4";
            this.labelh4.Size = new System.Drawing.Size(21, 13);
            this.labelh4.TabIndex = 96;
            this.labelh4.Text = "H4";
            // 
            // labelh5
            // 
            this.labelh5.AutoSize = true;
            this.labelh5.Location = new System.Drawing.Point(64, 192);
            this.labelh5.Name = "labelh5";
            this.labelh5.Size = new System.Drawing.Size(21, 13);
            this.labelh5.TabIndex = 96;
            this.labelh5.Text = "H5";
            // 
            // labelh6
            // 
            this.labelh6.AutoSize = true;
            this.labelh6.Location = new System.Drawing.Point(65, 218);
            this.labelh6.Name = "labelh6";
            this.labelh6.Size = new System.Drawing.Size(21, 13);
            this.labelh6.TabIndex = 96;
            this.labelh6.Text = "H6";
            // 
            // labelh7
            // 
            this.labelh7.AutoSize = true;
            this.labelh7.Location = new System.Drawing.Point(64, 243);
            this.labelh7.Name = "labelh7";
            this.labelh7.Size = new System.Drawing.Size(21, 13);
            this.labelh7.TabIndex = 96;
            this.labelh7.Text = "H7";
            // 
            // labelh8
            // 
            this.labelh8.AutoSize = true;
            this.labelh8.Location = new System.Drawing.Point(64, 269);
            this.labelh8.Name = "labelh8";
            this.labelh8.Size = new System.Drawing.Size(21, 13);
            this.labelh8.TabIndex = 96;
            this.labelh8.Text = "H8";
            // 
            // labelh9
            // 
            this.labelh9.AutoSize = true;
            this.labelh9.Location = new System.Drawing.Point(64, 295);
            this.labelh9.Name = "labelh9";
            this.labelh9.Size = new System.Drawing.Size(21, 13);
            this.labelh9.TabIndex = 96;
            this.labelh9.Text = "H9";
            // 
            // labelh10
            // 
            this.labelh10.AutoSize = true;
            this.labelh10.Location = new System.Drawing.Point(64, 320);
            this.labelh10.Name = "labelh10";
            this.labelh10.Size = new System.Drawing.Size(27, 13);
            this.labelh10.TabIndex = 96;
            this.labelh10.Text = "H10";
            // 
            // labelh11
            // 
            this.labelh11.AutoSize = true;
            this.labelh11.Location = new System.Drawing.Point(64, 346);
            this.labelh11.Name = "labelh11";
            this.labelh11.Size = new System.Drawing.Size(27, 13);
            this.labelh11.TabIndex = 96;
            this.labelh11.Text = "H11";
            // 
            // labelh12
            // 
            this.labelh12.AutoSize = true;
            this.labelh12.Location = new System.Drawing.Point(64, 372);
            this.labelh12.Name = "labelh12";
            this.labelh12.Size = new System.Drawing.Size(27, 13);
            this.labelh12.TabIndex = 96;
            this.labelh12.Text = "H12";
            // 
            // labelh24
            // 
            this.labelh24.AutoSize = true;
            this.labelh24.Location = new System.Drawing.Point(335, 377);
            this.labelh24.Name = "labelh24";
            this.labelh24.Size = new System.Drawing.Size(27, 13);
            this.labelh24.TabIndex = 96;
            this.labelh24.Text = "H24";
            // 
            // labelh23
            // 
            this.labelh23.AutoSize = true;
            this.labelh23.Location = new System.Drawing.Point(335, 346);
            this.labelh23.Name = "labelh23";
            this.labelh23.Size = new System.Drawing.Size(27, 13);
            this.labelh23.TabIndex = 96;
            this.labelh23.Text = "H23";
            // 
            // labelh22
            // 
            this.labelh22.AutoSize = true;
            this.labelh22.Location = new System.Drawing.Point(335, 320);
            this.labelh22.Name = "labelh22";
            this.labelh22.Size = new System.Drawing.Size(27, 13);
            this.labelh22.TabIndex = 96;
            this.labelh22.Text = "H22";
            // 
            // labelh21
            // 
            this.labelh21.AutoSize = true;
            this.labelh21.Location = new System.Drawing.Point(335, 295);
            this.labelh21.Name = "labelh21";
            this.labelh21.Size = new System.Drawing.Size(27, 13);
            this.labelh21.TabIndex = 96;
            this.labelh21.Text = "H21";
            // 
            // labelh20
            // 
            this.labelh20.AutoSize = true;
            this.labelh20.Location = new System.Drawing.Point(335, 269);
            this.labelh20.Name = "labelh20";
            this.labelh20.Size = new System.Drawing.Size(27, 13);
            this.labelh20.TabIndex = 96;
            this.labelh20.Text = "H20";
            // 
            // labelh19
            // 
            this.labelh19.AutoSize = true;
            this.labelh19.Location = new System.Drawing.Point(335, 243);
            this.labelh19.Name = "labelh19";
            this.labelh19.Size = new System.Drawing.Size(27, 13);
            this.labelh19.TabIndex = 96;
            this.labelh19.Text = "H19";
            // 
            // labelh18
            // 
            this.labelh18.AutoSize = true;
            this.labelh18.Location = new System.Drawing.Point(335, 218);
            this.labelh18.Name = "labelh18";
            this.labelh18.Size = new System.Drawing.Size(27, 13);
            this.labelh18.TabIndex = 96;
            this.labelh18.Text = "H18";
            // 
            // labelh17
            // 
            this.labelh17.AutoSize = true;
            this.labelh17.Location = new System.Drawing.Point(335, 192);
            this.labelh17.Name = "labelh17";
            this.labelh17.Size = new System.Drawing.Size(27, 13);
            this.labelh17.TabIndex = 96;
            this.labelh17.Text = "H17";
            // 
            // labelh16
            // 
            this.labelh16.AutoSize = true;
            this.labelh16.Location = new System.Drawing.Point(335, 166);
            this.labelh16.Name = "labelh16";
            this.labelh16.Size = new System.Drawing.Size(27, 13);
            this.labelh16.TabIndex = 96;
            this.labelh16.Text = "H16";
            // 
            // labelh15
            // 
            this.labelh15.AutoSize = true;
            this.labelh15.Location = new System.Drawing.Point(335, 140);
            this.labelh15.Name = "labelh15";
            this.labelh15.Size = new System.Drawing.Size(27, 13);
            this.labelh15.TabIndex = 96;
            this.labelh15.Text = "H15";
            // 
            // labelh14
            // 
            this.labelh14.AutoSize = true;
            this.labelh14.Location = new System.Drawing.Point(335, 114);
            this.labelh14.Name = "labelh14";
            this.labelh14.Size = new System.Drawing.Size(27, 13);
            this.labelh14.TabIndex = 96;
            this.labelh14.Text = "H14";
            // 
            // labelh13
            // 
            this.labelh13.AutoSize = true;
            this.labelh13.Location = new System.Drawing.Point(335, 89);
            this.labelh13.Name = "labelh13";
            this.labelh13.Size = new System.Drawing.Size(27, 13);
            this.labelh13.TabIndex = 96;
            this.labelh13.Text = "H13";
            // 
            // labelh2
            // 
            this.labelh2.AutoSize = true;
            this.labelh2.Location = new System.Drawing.Point(64, 115);
            this.labelh2.Name = "labelh2";
            this.labelh2.Size = new System.Drawing.Size(21, 13);
            this.labelh2.TabIndex = 95;
            this.labelh2.Text = "H2";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(511, 65);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(42, 13);
            this.label32.TabIndex = 95;
            this.label32.Text = "Interval";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(245, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 95;
            this.label3.Text = "Interval";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(237, 65);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(42, 13);
            this.label29.TabIndex = 95;
            this.label29.Text = "Interval";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(470, 55);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(51, 13);
            this.label31.TabIndex = 95;
            this.label31.Text = "PriceMax";
            this.label31.Visible = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(177, 55);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(51, 13);
            this.label28.TabIndex = 95;
            this.label28.Text = "PriceMax";
            this.label28.Visible = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(403, 65);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(31, 13);
            this.label30.TabIndex = 95;
            this.label30.Text = "Price";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(120, 65);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(31, 13);
            this.label27.TabIndex = 95;
            this.label27.Text = "Price";
            // 
            // labelh1
            // 
            this.labelh1.AutoSize = true;
            this.labelh1.Location = new System.Drawing.Point(64, 89);
            this.labelh1.Name = "labelh1";
            this.labelh1.Size = new System.Drawing.Size(21, 13);
            this.labelh1.TabIndex = 35;
            this.labelh1.Text = "H1";
            // 
            // numeric70
            // 
            this.numeric70.BackColor = System.Drawing.Color.Beige;
            this.numeric70.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric70.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric70.Location = new System.Drawing.Point(376, 370);
            this.numeric70.Name = "numeric70";
            this.numeric70.Size = new System.Drawing.Size(91, 20);
            this.numeric70.TabIndex = 71;
            this.numeric70.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric64
            // 
            this.numeric64.BackColor = System.Drawing.Color.Beige;
            this.numeric64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric64.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric64.Location = new System.Drawing.Point(376, 318);
            this.numeric64.Name = "numeric64";
            this.numeric64.Size = new System.Drawing.Size(91, 20);
            this.numeric64.TabIndex = 65;
            this.numeric64.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric69
            // 
            this.numeric69.BackColor = System.Drawing.Color.Beige;
            this.numeric69.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric69.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric69.Location = new System.Drawing.Point(494, 344);
            this.numeric69.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric69.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric69.Name = "numeric69";
            this.numeric69.Size = new System.Drawing.Size(91, 20);
            this.numeric69.TabIndex = 70;
            this.numeric69.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric69.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric63
            // 
            this.numeric63.BackColor = System.Drawing.Color.Beige;
            this.numeric63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric63.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric63.Location = new System.Drawing.Point(494, 293);
            this.numeric63.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric63.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric63.Name = "numeric63";
            this.numeric63.Size = new System.Drawing.Size(91, 20);
            this.numeric63.TabIndex = 64;
            this.numeric63.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric63.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric72
            // 
            this.numeric72.BackColor = System.Drawing.Color.Beige;
            this.numeric72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric72.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric72.Location = new System.Drawing.Point(494, 370);
            this.numeric72.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric72.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric72.Name = "numeric72";
            this.numeric72.Size = new System.Drawing.Size(91, 20);
            this.numeric72.TabIndex = 73;
            this.numeric72.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric72.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric68
            // 
            this.numeric68.BackColor = System.Drawing.Color.Beige;
            this.numeric68.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric68.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric68.Location = new System.Drawing.Point(468, 344);
            this.numeric68.Name = "numeric68";
            this.numeric68.Size = new System.Drawing.Size(22, 20);
            this.numeric68.TabIndex = 69;
            this.numeric68.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric68.Visible = false;
            // 
            // numeric66
            // 
            this.numeric66.BackColor = System.Drawing.Color.Beige;
            this.numeric66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric66.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric66.Location = new System.Drawing.Point(494, 318);
            this.numeric66.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric66.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric66.Name = "numeric66";
            this.numeric66.Size = new System.Drawing.Size(91, 20);
            this.numeric66.TabIndex = 67;
            this.numeric66.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric66.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric62
            // 
            this.numeric62.BackColor = System.Drawing.Color.Beige;
            this.numeric62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric62.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric62.Location = new System.Drawing.Point(468, 293);
            this.numeric62.Name = "numeric62";
            this.numeric62.Size = new System.Drawing.Size(22, 20);
            this.numeric62.TabIndex = 63;
            this.numeric62.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric62.Visible = false;
            // 
            // numeric71
            // 
            this.numeric71.BackColor = System.Drawing.Color.Beige;
            this.numeric71.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric71.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric71.Location = new System.Drawing.Point(468, 370);
            this.numeric71.Name = "numeric71";
            this.numeric71.Size = new System.Drawing.Size(22, 20);
            this.numeric71.TabIndex = 72;
            this.numeric71.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric71.Visible = false;
            // 
            // numeric67
            // 
            this.numeric67.BackColor = System.Drawing.Color.Beige;
            this.numeric67.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric67.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric67.Location = new System.Drawing.Point(376, 344);
            this.numeric67.Name = "numeric67";
            this.numeric67.Size = new System.Drawing.Size(91, 20);
            this.numeric67.TabIndex = 68;
            this.numeric67.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric65
            // 
            this.numeric65.BackColor = System.Drawing.Color.Beige;
            this.numeric65.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric65.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric65.Location = new System.Drawing.Point(468, 318);
            this.numeric65.Name = "numeric65";
            this.numeric65.Size = new System.Drawing.Size(22, 20);
            this.numeric65.TabIndex = 66;
            this.numeric65.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric65.Visible = false;
            // 
            // numeric61
            // 
            this.numeric61.BackColor = System.Drawing.Color.Beige;
            this.numeric61.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric61.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric61.Location = new System.Drawing.Point(376, 293);
            this.numeric61.Name = "numeric61";
            this.numeric61.Size = new System.Drawing.Size(91, 20);
            this.numeric61.TabIndex = 62;
            this.numeric61.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric58
            // 
            this.numeric58.BackColor = System.Drawing.Color.Beige;
            this.numeric58.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric58.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric58.Location = new System.Drawing.Point(376, 267);
            this.numeric58.Name = "numeric58";
            this.numeric58.Size = new System.Drawing.Size(91, 20);
            this.numeric58.TabIndex = 59;
            this.numeric58.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric55
            // 
            this.numeric55.BackColor = System.Drawing.Color.Beige;
            this.numeric55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric55.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric55.Location = new System.Drawing.Point(376, 241);
            this.numeric55.Name = "numeric55";
            this.numeric55.Size = new System.Drawing.Size(91, 20);
            this.numeric55.TabIndex = 56;
            this.numeric55.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric60
            // 
            this.numeric60.BackColor = System.Drawing.Color.Beige;
            this.numeric60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric60.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric60.Location = new System.Drawing.Point(494, 267);
            this.numeric60.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric60.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric60.Name = "numeric60";
            this.numeric60.Size = new System.Drawing.Size(91, 20);
            this.numeric60.TabIndex = 61;
            this.numeric60.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric60.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric54
            // 
            this.numeric54.BackColor = System.Drawing.Color.Beige;
            this.numeric54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric54.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric54.Location = new System.Drawing.Point(494, 216);
            this.numeric54.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric54.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric54.Name = "numeric54";
            this.numeric54.Size = new System.Drawing.Size(91, 20);
            this.numeric54.TabIndex = 55;
            this.numeric54.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric54.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric57
            // 
            this.numeric57.BackColor = System.Drawing.Color.Beige;
            this.numeric57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric57.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric57.Location = new System.Drawing.Point(494, 241);
            this.numeric57.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric57.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric57.Name = "numeric57";
            this.numeric57.Size = new System.Drawing.Size(91, 20);
            this.numeric57.TabIndex = 58;
            this.numeric57.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric57.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric51
            // 
            this.numeric51.BackColor = System.Drawing.Color.Beige;
            this.numeric51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric51.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric51.Location = new System.Drawing.Point(494, 190);
            this.numeric51.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric51.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric51.Name = "numeric51";
            this.numeric51.Size = new System.Drawing.Size(91, 20);
            this.numeric51.TabIndex = 52;
            this.numeric51.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric51.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric59
            // 
            this.numeric59.BackColor = System.Drawing.Color.Beige;
            this.numeric59.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric59.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric59.Location = new System.Drawing.Point(468, 267);
            this.numeric59.Name = "numeric59";
            this.numeric59.Size = new System.Drawing.Size(22, 20);
            this.numeric59.TabIndex = 60;
            this.numeric59.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric59.Visible = false;
            // 
            // numeric53
            // 
            this.numeric53.BackColor = System.Drawing.Color.Beige;
            this.numeric53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric53.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric53.Location = new System.Drawing.Point(468, 216);
            this.numeric53.Name = "numeric53";
            this.numeric53.Size = new System.Drawing.Size(22, 20);
            this.numeric53.TabIndex = 54;
            this.numeric53.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric53.Visible = false;
            // 
            // numeric56
            // 
            this.numeric56.BackColor = System.Drawing.Color.Beige;
            this.numeric56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric56.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric56.Location = new System.Drawing.Point(468, 241);
            this.numeric56.Name = "numeric56";
            this.numeric56.Size = new System.Drawing.Size(22, 20);
            this.numeric56.TabIndex = 57;
            this.numeric56.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric56.Visible = false;
            // 
            // numeric50
            // 
            this.numeric50.BackColor = System.Drawing.Color.Beige;
            this.numeric50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric50.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric50.Location = new System.Drawing.Point(468, 190);
            this.numeric50.Name = "numeric50";
            this.numeric50.Size = new System.Drawing.Size(22, 20);
            this.numeric50.TabIndex = 51;
            this.numeric50.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric50.Visible = false;
            // 
            // numeric52
            // 
            this.numeric52.BackColor = System.Drawing.Color.Beige;
            this.numeric52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric52.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric52.Location = new System.Drawing.Point(376, 216);
            this.numeric52.Name = "numeric52";
            this.numeric52.Size = new System.Drawing.Size(91, 20);
            this.numeric52.TabIndex = 53;
            this.numeric52.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric49
            // 
            this.numeric49.BackColor = System.Drawing.Color.Beige;
            this.numeric49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric49.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric49.Location = new System.Drawing.Point(376, 190);
            this.numeric49.Name = "numeric49";
            this.numeric49.Size = new System.Drawing.Size(91, 20);
            this.numeric49.TabIndex = 50;
            this.numeric49.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric46
            // 
            this.numeric46.BackColor = System.Drawing.Color.Beige;
            this.numeric46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric46.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric46.Location = new System.Drawing.Point(376, 164);
            this.numeric46.Name = "numeric46";
            this.numeric46.Size = new System.Drawing.Size(91, 20);
            this.numeric46.TabIndex = 47;
            this.numeric46.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric43
            // 
            this.numeric43.BackColor = System.Drawing.Color.Beige;
            this.numeric43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric43.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric43.Location = new System.Drawing.Point(376, 138);
            this.numeric43.Name = "numeric43";
            this.numeric43.Size = new System.Drawing.Size(91, 20);
            this.numeric43.TabIndex = 44;
            this.numeric43.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric48
            // 
            this.numeric48.BackColor = System.Drawing.Color.Beige;
            this.numeric48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric48.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric48.Location = new System.Drawing.Point(494, 164);
            this.numeric48.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric48.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric48.Name = "numeric48";
            this.numeric48.Size = new System.Drawing.Size(91, 20);
            this.numeric48.TabIndex = 49;
            this.numeric48.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric48.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric42
            // 
            this.numeric42.BackColor = System.Drawing.Color.Beige;
            this.numeric42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric42.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric42.Location = new System.Drawing.Point(494, 113);
            this.numeric42.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric42.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric42.Name = "numeric42";
            this.numeric42.Size = new System.Drawing.Size(91, 20);
            this.numeric42.TabIndex = 43;
            this.numeric42.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric42.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric45
            // 
            this.numeric45.BackColor = System.Drawing.Color.Beige;
            this.numeric45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric45.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric45.Location = new System.Drawing.Point(494, 138);
            this.numeric45.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric45.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric45.Name = "numeric45";
            this.numeric45.Size = new System.Drawing.Size(91, 20);
            this.numeric45.TabIndex = 46;
            this.numeric45.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric45.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric39
            // 
            this.numeric39.BackColor = System.Drawing.Color.Beige;
            this.numeric39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric39.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric39.Location = new System.Drawing.Point(494, 87);
            this.numeric39.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric39.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric39.Name = "numeric39";
            this.numeric39.Size = new System.Drawing.Size(91, 20);
            this.numeric39.TabIndex = 40;
            this.numeric39.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric39.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric47
            // 
            this.numeric47.BackColor = System.Drawing.Color.Beige;
            this.numeric47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric47.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric47.Location = new System.Drawing.Point(468, 164);
            this.numeric47.Name = "numeric47";
            this.numeric47.Size = new System.Drawing.Size(22, 20);
            this.numeric47.TabIndex = 48;
            this.numeric47.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric47.Visible = false;
            // 
            // numeric41
            // 
            this.numeric41.BackColor = System.Drawing.Color.Beige;
            this.numeric41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric41.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric41.Location = new System.Drawing.Point(468, 113);
            this.numeric41.Name = "numeric41";
            this.numeric41.Size = new System.Drawing.Size(22, 20);
            this.numeric41.TabIndex = 42;
            this.numeric41.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric41.Visible = false;
            // 
            // numeric44
            // 
            this.numeric44.BackColor = System.Drawing.Color.Beige;
            this.numeric44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric44.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric44.Location = new System.Drawing.Point(468, 138);
            this.numeric44.Name = "numeric44";
            this.numeric44.Size = new System.Drawing.Size(22, 20);
            this.numeric44.TabIndex = 45;
            this.numeric44.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric44.Visible = false;
            // 
            // numeric38
            // 
            this.numeric38.BackColor = System.Drawing.Color.Beige;
            this.numeric38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric38.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric38.Location = new System.Drawing.Point(468, 87);
            this.numeric38.Name = "numeric38";
            this.numeric38.Size = new System.Drawing.Size(22, 20);
            this.numeric38.TabIndex = 39;
            this.numeric38.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric38.Visible = false;
            // 
            // numeric40
            // 
            this.numeric40.BackColor = System.Drawing.Color.Beige;
            this.numeric40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric40.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric40.Location = new System.Drawing.Point(376, 113);
            this.numeric40.Name = "numeric40";
            this.numeric40.Size = new System.Drawing.Size(91, 20);
            this.numeric40.TabIndex = 41;
            this.numeric40.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric37
            // 
            this.numeric37.BackColor = System.Drawing.Color.Beige;
            this.numeric37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric37.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric37.Location = new System.Drawing.Point(376, 87);
            this.numeric37.Name = "numeric37";
            this.numeric37.Size = new System.Drawing.Size(91, 20);
            this.numeric37.TabIndex = 38;
            this.numeric37.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numericUpDown49
            // 
            this.numericUpDown49.Location = new System.Drawing.Point(28, 499);
            this.numericUpDown49.Name = "numericUpDown49";
            this.numericUpDown49.Size = new System.Drawing.Size(91, 20);
            this.numericUpDown49.TabIndex = 58;
            this.numericUpDown49.UseWaitCursor = true;
            // 
            // numericUpDown51
            // 
            this.numericUpDown51.Location = new System.Drawing.Point(222, 499);
            this.numericUpDown51.Name = "numericUpDown51";
            this.numericUpDown51.Size = new System.Drawing.Size(91, 20);
            this.numericUpDown51.TabIndex = 55;
            this.numericUpDown51.UseWaitCursor = true;
            // 
            // numericUpDown55
            // 
            this.numericUpDown55.Location = new System.Drawing.Point(125, 499);
            this.numericUpDown55.Name = "numericUpDown55";
            this.numericUpDown55.Size = new System.Drawing.Size(91, 20);
            this.numericUpDown55.TabIndex = 51;
            this.numericUpDown55.UseWaitCursor = true;
            // 
            // numeric34
            // 
            this.numeric34.BackColor = System.Drawing.Color.Beige;
            this.numeric34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric34.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric34.Location = new System.Drawing.Point(95, 370);
            this.numeric34.Name = "numeric34";
            this.numeric34.Size = new System.Drawing.Size(91, 20);
            this.numeric34.TabIndex = 35;
            this.numeric34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric28
            // 
            this.numeric28.BackColor = System.Drawing.Color.Beige;
            this.numeric28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric28.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric28.Location = new System.Drawing.Point(95, 318);
            this.numeric28.Name = "numeric28";
            this.numeric28.Size = new System.Drawing.Size(91, 20);
            this.numeric28.TabIndex = 27;
            this.numeric28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric33
            // 
            this.numeric33.BackColor = System.Drawing.Color.Beige;
            this.numeric33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric33.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric33.Location = new System.Drawing.Point(216, 344);
            this.numeric33.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric33.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric33.Name = "numeric33";
            this.numeric33.Size = new System.Drawing.Size(91, 20);
            this.numeric33.TabIndex = 32;
            this.numeric33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric33.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric27
            // 
            this.numeric27.BackColor = System.Drawing.Color.Beige;
            this.numeric27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric27.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric27.Location = new System.Drawing.Point(216, 293);
            this.numeric27.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric27.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric27.Name = "numeric27";
            this.numeric27.Size = new System.Drawing.Size(91, 20);
            this.numeric27.TabIndex = 26;
            this.numeric27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric27.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric36
            // 
            this.numeric36.BackColor = System.Drawing.Color.Beige;
            this.numeric36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric36.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric36.Location = new System.Drawing.Point(216, 370);
            this.numeric36.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric36.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric36.Name = "numeric36";
            this.numeric36.Size = new System.Drawing.Size(91, 20);
            this.numeric36.TabIndex = 37;
            this.numeric36.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric36.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric32
            // 
            this.numeric32.BackColor = System.Drawing.Color.Beige;
            this.numeric32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric32.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric32.Location = new System.Drawing.Point(190, 344);
            this.numeric32.Name = "numeric32";
            this.numeric32.Size = new System.Drawing.Size(26, 20);
            this.numeric32.TabIndex = 31;
            this.numeric32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric32.Visible = false;
            // 
            // numeric30
            // 
            this.numeric30.BackColor = System.Drawing.Color.Beige;
            this.numeric30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric30.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric30.Location = new System.Drawing.Point(216, 318);
            this.numeric30.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric30.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric30.Name = "numeric30";
            this.numeric30.Size = new System.Drawing.Size(91, 20);
            this.numeric30.TabIndex = 29;
            this.numeric30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric30.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric26
            // 
            this.numeric26.BackColor = System.Drawing.Color.Beige;
            this.numeric26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric26.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric26.Location = new System.Drawing.Point(190, 293);
            this.numeric26.Name = "numeric26";
            this.numeric26.Size = new System.Drawing.Size(26, 20);
            this.numeric26.TabIndex = 25;
            this.numeric26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric26.Visible = false;
            // 
            // numeric35
            // 
            this.numeric35.BackColor = System.Drawing.Color.Beige;
            this.numeric35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric35.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric35.Location = new System.Drawing.Point(190, 370);
            this.numeric35.Name = "numeric35";
            this.numeric35.Size = new System.Drawing.Size(26, 20);
            this.numeric35.TabIndex = 36;
            this.numeric35.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric35.Visible = false;
            // 
            // numeric31
            // 
            this.numeric31.BackColor = System.Drawing.Color.Beige;
            this.numeric31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric31.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric31.Location = new System.Drawing.Point(95, 344);
            this.numeric31.Name = "numeric31";
            this.numeric31.Size = new System.Drawing.Size(91, 20);
            this.numeric31.TabIndex = 30;
            this.numeric31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric29
            // 
            this.numeric29.BackColor = System.Drawing.Color.Beige;
            this.numeric29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric29.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric29.Location = new System.Drawing.Point(190, 318);
            this.numeric29.Name = "numeric29";
            this.numeric29.Size = new System.Drawing.Size(26, 20);
            this.numeric29.TabIndex = 28;
            this.numeric29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric29.Visible = false;
            // 
            // numeric25
            // 
            this.numeric25.BackColor = System.Drawing.Color.Beige;
            this.numeric25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric25.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric25.Location = new System.Drawing.Point(95, 293);
            this.numeric25.Name = "numeric25";
            this.numeric25.Size = new System.Drawing.Size(91, 20);
            this.numeric25.TabIndex = 24;
            this.numeric25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric22
            // 
            this.numeric22.BackColor = System.Drawing.Color.Beige;
            this.numeric22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric22.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric22.Location = new System.Drawing.Point(95, 267);
            this.numeric22.Name = "numeric22";
            this.numeric22.Size = new System.Drawing.Size(91, 20);
            this.numeric22.TabIndex = 21;
            this.numeric22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric19
            // 
            this.numeric19.BackColor = System.Drawing.Color.Beige;
            this.numeric19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric19.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric19.Location = new System.Drawing.Point(95, 241);
            this.numeric19.Name = "numeric19";
            this.numeric19.Size = new System.Drawing.Size(91, 20);
            this.numeric19.TabIndex = 18;
            this.numeric19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric24
            // 
            this.numeric24.BackColor = System.Drawing.Color.Beige;
            this.numeric24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric24.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric24.Location = new System.Drawing.Point(216, 267);
            this.numeric24.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric24.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric24.Name = "numeric24";
            this.numeric24.Size = new System.Drawing.Size(91, 20);
            this.numeric24.TabIndex = 23;
            this.numeric24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric24.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric18
            // 
            this.numeric18.BackColor = System.Drawing.Color.Beige;
            this.numeric18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric18.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric18.Location = new System.Drawing.Point(216, 216);
            this.numeric18.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric18.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric18.Name = "numeric18";
            this.numeric18.Size = new System.Drawing.Size(91, 20);
            this.numeric18.TabIndex = 17;
            this.numeric18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric18.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric21
            // 
            this.numeric21.BackColor = System.Drawing.Color.Beige;
            this.numeric21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric21.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric21.Location = new System.Drawing.Point(216, 241);
            this.numeric21.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric21.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric21.Name = "numeric21";
            this.numeric21.Size = new System.Drawing.Size(91, 20);
            this.numeric21.TabIndex = 20;
            this.numeric21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric21.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric15
            // 
            this.numeric15.BackColor = System.Drawing.Color.Beige;
            this.numeric15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric15.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric15.Location = new System.Drawing.Point(216, 190);
            this.numeric15.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric15.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric15.Name = "numeric15";
            this.numeric15.Size = new System.Drawing.Size(91, 20);
            this.numeric15.TabIndex = 14;
            this.numeric15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric15.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric23
            // 
            this.numeric23.BackColor = System.Drawing.Color.Beige;
            this.numeric23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric23.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric23.Location = new System.Drawing.Point(190, 267);
            this.numeric23.Name = "numeric23";
            this.numeric23.Size = new System.Drawing.Size(26, 20);
            this.numeric23.TabIndex = 22;
            this.numeric23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric23.Visible = false;
            // 
            // numeric17
            // 
            this.numeric17.BackColor = System.Drawing.Color.Beige;
            this.numeric17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric17.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric17.Location = new System.Drawing.Point(190, 216);
            this.numeric17.Name = "numeric17";
            this.numeric17.Size = new System.Drawing.Size(26, 20);
            this.numeric17.TabIndex = 16;
            this.numeric17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric17.Visible = false;
            // 
            // numeric20
            // 
            this.numeric20.BackColor = System.Drawing.Color.Beige;
            this.numeric20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric20.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric20.Location = new System.Drawing.Point(190, 241);
            this.numeric20.Name = "numeric20";
            this.numeric20.Size = new System.Drawing.Size(26, 20);
            this.numeric20.TabIndex = 19;
            this.numeric20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric20.Visible = false;
            // 
            // numeric14
            // 
            this.numeric14.BackColor = System.Drawing.Color.Beige;
            this.numeric14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric14.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric14.Location = new System.Drawing.Point(190, 190);
            this.numeric14.Name = "numeric14";
            this.numeric14.Size = new System.Drawing.Size(26, 20);
            this.numeric14.TabIndex = 13;
            this.numeric14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric14.Visible = false;
            // 
            // numeric16
            // 
            this.numeric16.BackColor = System.Drawing.Color.Beige;
            this.numeric16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric16.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric16.Location = new System.Drawing.Point(95, 216);
            this.numeric16.Name = "numeric16";
            this.numeric16.Size = new System.Drawing.Size(91, 20);
            this.numeric16.TabIndex = 15;
            this.numeric16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric13
            // 
            this.numeric13.BackColor = System.Drawing.Color.Beige;
            this.numeric13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric13.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric13.Location = new System.Drawing.Point(95, 190);
            this.numeric13.Name = "numeric13";
            this.numeric13.Size = new System.Drawing.Size(91, 20);
            this.numeric13.TabIndex = 12;
            this.numeric13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric10
            // 
            this.numeric10.BackColor = System.Drawing.Color.Beige;
            this.numeric10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric10.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric10.Location = new System.Drawing.Point(95, 164);
            this.numeric10.Name = "numeric10";
            this.numeric10.Size = new System.Drawing.Size(91, 20);
            this.numeric10.TabIndex = 9;
            this.numeric10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric7
            // 
            this.numeric7.BackColor = System.Drawing.Color.Beige;
            this.numeric7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric7.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric7.Location = new System.Drawing.Point(95, 138);
            this.numeric7.Name = "numeric7";
            this.numeric7.Size = new System.Drawing.Size(91, 20);
            this.numeric7.TabIndex = 6;
            this.numeric7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric12
            // 
            this.numeric12.BackColor = System.Drawing.Color.Beige;
            this.numeric12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric12.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric12.Location = new System.Drawing.Point(216, 164);
            this.numeric12.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric12.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric12.Name = "numeric12";
            this.numeric12.Size = new System.Drawing.Size(91, 20);
            this.numeric12.TabIndex = 11;
            this.numeric12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric12.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric6
            // 
            this.numeric6.BackColor = System.Drawing.Color.Beige;
            this.numeric6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric6.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric6.Location = new System.Drawing.Point(216, 113);
            this.numeric6.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric6.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric6.Name = "numeric6";
            this.numeric6.Size = new System.Drawing.Size(91, 20);
            this.numeric6.TabIndex = 5;
            this.numeric6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric6.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric9
            // 
            this.numeric9.BackColor = System.Drawing.Color.Beige;
            this.numeric9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric9.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric9.Location = new System.Drawing.Point(216, 138);
            this.numeric9.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric9.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric9.Name = "numeric9";
            this.numeric9.Size = new System.Drawing.Size(91, 20);
            this.numeric9.TabIndex = 8;
            this.numeric9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric9.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric3
            // 
            this.numeric3.BackColor = System.Drawing.Color.Beige;
            this.numeric3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric3.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric3.Location = new System.Drawing.Point(216, 87);
            this.numeric3.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric3.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numeric3.Name = "numeric3";
            this.numeric3.Size = new System.Drawing.Size(91, 20);
            this.numeric3.TabIndex = 2;
            this.numeric3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric3.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric11
            // 
            this.numeric11.BackColor = System.Drawing.Color.Beige;
            this.numeric11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric11.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric11.Location = new System.Drawing.Point(190, 164);
            this.numeric11.Name = "numeric11";
            this.numeric11.Size = new System.Drawing.Size(26, 20);
            this.numeric11.TabIndex = 10;
            this.numeric11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric11.Visible = false;
            // 
            // numeric5
            // 
            this.numeric5.BackColor = System.Drawing.Color.Beige;
            this.numeric5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric5.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric5.Location = new System.Drawing.Point(190, 113);
            this.numeric5.Name = "numeric5";
            this.numeric5.Size = new System.Drawing.Size(26, 20);
            this.numeric5.TabIndex = 4;
            this.numeric5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric5.Visible = false;
            // 
            // numeric8
            // 
            this.numeric8.BackColor = System.Drawing.Color.Beige;
            this.numeric8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric8.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric8.Location = new System.Drawing.Point(190, 138);
            this.numeric8.Name = "numeric8";
            this.numeric8.Size = new System.Drawing.Size(26, 20);
            this.numeric8.TabIndex = 7;
            this.numeric8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric8.Visible = false;
            // 
            // numeric2
            // 
            this.numeric2.BackColor = System.Drawing.Color.Beige;
            this.numeric2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric2.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric2.Location = new System.Drawing.Point(190, 87);
            this.numeric2.Name = "numeric2";
            this.numeric2.Size = new System.Drawing.Size(26, 20);
            this.numeric2.TabIndex = 1;
            this.numeric2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric2.Visible = false;
            // 
            // numeric4
            // 
            this.numeric4.BackColor = System.Drawing.Color.Beige;
            this.numeric4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric4.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric4.Location = new System.Drawing.Point(95, 113);
            this.numeric4.Name = "numeric4";
            this.numeric4.Size = new System.Drawing.Size(91, 20);
            this.numeric4.TabIndex = 3;
            this.numeric4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numeric1
            // 
            this.numeric1.BackColor = System.Drawing.Color.Beige;
            this.numeric1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numeric1.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numeric1.Location = new System.Drawing.Point(95, 87);
            this.numeric1.Name = "numeric1";
            this.numeric1.Size = new System.Drawing.Size(91, 20);
            this.numeric1.TabIndex = 0;
            this.numeric1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 504);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(669, 21);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 17;
            this.progressBar1.Visible = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // btnsavesimilar
            // 
            this.btnsavesimilar.BackColor = System.Drawing.Color.CadetBlue;
            this.btnsavesimilar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsavesimilar.Location = new System.Drawing.Point(690, 123);
            this.btnsavesimilar.Name = "btnsavesimilar";
            this.btnsavesimilar.Size = new System.Drawing.Size(98, 23);
            this.btnsavesimilar.TabIndex = 8;
            this.btnsavesimilar.Text = "SaveSimilar";
            this.btnsavesimilar.UseVisualStyleBackColor = false;
            this.btnsavesimilar.Click += new System.EventHandler(this.btnsavesimilar_Click);
            // 
            // btnsavelastbid
            // 
            this.btnsavelastbid.BackColor = System.Drawing.Color.CadetBlue;
            this.btnsavelastbid.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsavelastbid.Location = new System.Drawing.Point(9, 58);
            this.btnsavelastbid.Name = "btnsavelastbid";
            this.btnsavelastbid.Size = new System.Drawing.Size(81, 23);
            this.btnsavelastbid.TabIndex = 9;
            this.btnsavelastbid.Text = "Save Last Bid";
            this.btnsavelastbid.UseVisualStyleBackColor = false;
            this.btnsavelastbid.Click += new System.EventHandler(this.btnsavelastbid_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.Color.Silver;
            this.btnsave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsave.ForeColor = System.Drawing.Color.Black;
            this.btnsave.Location = new System.Drawing.Point(690, 345);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(98, 23);
            this.btnsave.TabIndex = 3;
            this.btnsave.Text = "SaveManual";
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnsimilarblock
            // 
            this.btnsimilarblock.BackColor = System.Drawing.Color.CadetBlue;
            this.btnsimilarblock.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsimilarblock.Location = new System.Drawing.Point(690, 68);
            this.btnsimilarblock.Name = "btnsimilarblock";
            this.btnsimilarblock.Size = new System.Drawing.Size(98, 23);
            this.btnsimilarblock.TabIndex = 10;
            this.btnsimilarblock.Text = "SaveSimilarBlock";
            this.btnsimilarblock.UseVisualStyleBackColor = false;
            this.btnsimilarblock.Click += new System.EventHandler(this.btnsimilarblock_Click);
            // 
            // btncap
            // 
            this.btncap.BackColor = System.Drawing.Color.CadetBlue;
            this.btncap.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btncap.Location = new System.Drawing.Point(7, 50);
            this.btncap.Name = "btncap";
            this.btncap.Size = new System.Drawing.Size(86, 23);
            this.btncap.TabIndex = 14;
            this.btncap.Text = "SaveCapPrice";
            this.btncap.UseVisualStyleBackColor = false;
            this.btncap.Click += new System.EventHandler(this.btncap_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(28, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "index :";
            // 
            // txtzarib
            // 
            this.txtzarib.Location = new System.Drawing.Point(10, 24);
            this.txtzarib.Name = "txtzarib";
            this.txtzarib.Size = new System.Drawing.Size(79, 20);
            this.txtzarib.TabIndex = 12;
            this.txtzarib.TextChanged += new System.EventHandler(this.txtzarib_TextChanged);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panel2.Controls.Add(this.txtzarib);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btncap);
            this.panel2.Location = new System.Drawing.Point(690, 371);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(98, 28);
            this.panel2.TabIndex = 15;
            this.panel2.Visible = false;
            // 
            // btnsavehourblock
            // 
            this.btnsavehourblock.BackColor = System.Drawing.Color.CadetBlue;
            this.btnsavehourblock.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsavehourblock.ForeColor = System.Drawing.Color.Black;
            this.btnsavehourblock.Location = new System.Drawing.Point(690, 293);
            this.btnsavehourblock.Name = "btnsavehourblock";
            this.btnsavehourblock.Size = new System.Drawing.Size(98, 23);
            this.btnsavehourblock.TabIndex = 16;
            this.btnsavehourblock.Text = "SaveHour-Block";
            this.btnsavehourblock.UseVisualStyleBackColor = false;
            this.btnsavehourblock.Click += new System.EventHandler(this.btnsavehourblock_Click);
            // 
            // panehidebutton
            // 
            this.panehidebutton.Location = new System.Drawing.Point(687, 18);
            this.panehidebutton.Name = "panehidebutton";
            this.panehidebutton.Size = new System.Drawing.Size(101, 513);
            this.panehidebutton.TabIndex = 18;
            this.panehidebutton.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(2, 24);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(93, 21);
            this.comboBox1.TabIndex = 19;
            this.comboBox1.TextChanged += new System.EventHandler(this.comboBox1_TextChanged);
            // 
            // BtnDelete
            // 
            this.BtnDelete.BackColor = System.Drawing.Color.CadetBlue;
            this.BtnDelete.BackgroundImage = global::PowerPlantProject.Properties.Resources.deletered8;
            this.BtnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.BtnDelete.Location = new System.Drawing.Point(688, 407);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(98, 23);
            this.BtnDelete.TabIndex = 5;
            this.BtnDelete.Text = "Delete";
            this.BtnDelete.UseVisualStyleBackColor = false;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.CadetBlue;
            this.btnExport.BackgroundImage = global::PowerPlantProject.Properties.Resources.officeexcel2;
            this.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExport.Location = new System.Drawing.Point(687, 464);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(98, 23);
            this.btnExport.TabIndex = 3;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.comboBox1);
            this.panel3.Controls.Add(this.btnsavelastbid);
            this.panel3.Location = new System.Drawing.Point(690, 171);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(98, 89);
            this.panel3.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Default Unit :";
            // 
            // ManualBidding
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(794, 528);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.btnsavehourblock);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.BtnDelete);
            this.Controls.Add(this.btnsimilarblock);
            this.Controls.Add(this.btnsavesimilar);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.panehidebutton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ManualBidding";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ManualBidding";
            this.Load += new System.EventHandler(this.ManualBidding_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ManualBidding_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.paneleasy.ResumeLayout(false);
            this.paneleasy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.NumericUpDown numeric1;
        private System.Windows.Forms.NumericUpDown numeric4;
        private System.Windows.Forms.Panel paneleasy;
        private System.Windows.Forms.Label labelh2;
        private System.Windows.Forms.Label labelh1;
        private System.Windows.Forms.NumericUpDown numeric70;
        private System.Windows.Forms.NumericUpDown numeric64;
        private System.Windows.Forms.NumericUpDown numeric69;
        private System.Windows.Forms.NumericUpDown numeric63;
        private System.Windows.Forms.NumericUpDown numeric72;
        private System.Windows.Forms.NumericUpDown numeric68;
        private System.Windows.Forms.NumericUpDown numeric66;
        private System.Windows.Forms.NumericUpDown numeric62;
        private System.Windows.Forms.NumericUpDown numeric71;
        private System.Windows.Forms.NumericUpDown numeric67;
        private System.Windows.Forms.NumericUpDown numeric65;
        private System.Windows.Forms.NumericUpDown numeric61;
        private System.Windows.Forms.NumericUpDown numeric58;
        private System.Windows.Forms.NumericUpDown numeric55;
        private System.Windows.Forms.NumericUpDown numeric60;
        private System.Windows.Forms.NumericUpDown numeric54;
        private System.Windows.Forms.NumericUpDown numeric57;
        private System.Windows.Forms.NumericUpDown numeric51;
        private System.Windows.Forms.NumericUpDown numeric59;
        private System.Windows.Forms.NumericUpDown numeric53;
        private System.Windows.Forms.NumericUpDown numeric56;
        private System.Windows.Forms.NumericUpDown numeric50;
        private System.Windows.Forms.NumericUpDown numeric52;
        private System.Windows.Forms.NumericUpDown numeric49;
        private System.Windows.Forms.NumericUpDown numeric46;
        private System.Windows.Forms.NumericUpDown numeric43;
        private System.Windows.Forms.NumericUpDown numeric48;
        private System.Windows.Forms.NumericUpDown numeric42;
        private System.Windows.Forms.NumericUpDown numeric45;
        private System.Windows.Forms.NumericUpDown numeric39;
        private System.Windows.Forms.NumericUpDown numeric47;
        private System.Windows.Forms.NumericUpDown numeric41;
        private System.Windows.Forms.NumericUpDown numeric44;
        private System.Windows.Forms.NumericUpDown numeric38;
        private System.Windows.Forms.NumericUpDown numeric40;
        private System.Windows.Forms.NumericUpDown numeric37;
        private System.Windows.Forms.NumericUpDown numericUpDown49;
        private System.Windows.Forms.NumericUpDown numericUpDown51;
        private System.Windows.Forms.NumericUpDown numericUpDown55;
        private System.Windows.Forms.NumericUpDown numeric34;
        private System.Windows.Forms.NumericUpDown numeric28;
        private System.Windows.Forms.NumericUpDown numeric33;
        private System.Windows.Forms.NumericUpDown numeric27;
        private System.Windows.Forms.NumericUpDown numeric36;
        private System.Windows.Forms.NumericUpDown numeric32;
        private System.Windows.Forms.NumericUpDown numeric30;
        private System.Windows.Forms.NumericUpDown numeric26;
        private System.Windows.Forms.NumericUpDown numeric35;
        private System.Windows.Forms.NumericUpDown numeric31;
        private System.Windows.Forms.NumericUpDown numeric29;
        private System.Windows.Forms.NumericUpDown numeric25;
        private System.Windows.Forms.NumericUpDown numeric22;
        private System.Windows.Forms.NumericUpDown numeric19;
        private System.Windows.Forms.NumericUpDown numeric24;
        private System.Windows.Forms.NumericUpDown numeric18;
        private System.Windows.Forms.NumericUpDown numeric21;
        private System.Windows.Forms.NumericUpDown numeric15;
        private System.Windows.Forms.NumericUpDown numeric23;
        private System.Windows.Forms.NumericUpDown numeric17;
        private System.Windows.Forms.NumericUpDown numeric20;
        private System.Windows.Forms.NumericUpDown numeric14;
        private System.Windows.Forms.NumericUpDown numeric16;
        private System.Windows.Forms.NumericUpDown numeric13;
        private System.Windows.Forms.NumericUpDown numeric10;
        private System.Windows.Forms.NumericUpDown numeric7;
        private System.Windows.Forms.NumericUpDown numeric12;
        private System.Windows.Forms.NumericUpDown numeric6;
        private System.Windows.Forms.NumericUpDown numeric9;
        private System.Windows.Forms.NumericUpDown numeric3;
        private System.Windows.Forms.NumericUpDown numeric11;
        private System.Windows.Forms.NumericUpDown numeric5;
        private System.Windows.Forms.NumericUpDown numeric8;
        private System.Windows.Forms.NumericUpDown numeric2;
        private System.Windows.Forms.Button btnsaveeasy;
        private System.Windows.Forms.Label labelh3;
        private System.Windows.Forms.Label labelh4;
        private System.Windows.Forms.Label labelh5;
        private System.Windows.Forms.Label labelh6;
        private System.Windows.Forms.Label labelh7;
        private System.Windows.Forms.Label labelh8;
        private System.Windows.Forms.Label labelh9;
        private System.Windows.Forms.Label labelh10;
        private System.Windows.Forms.Label labelh11;
        private System.Windows.Forms.Label labelh12;
        private System.Windows.Forms.Label labelh24;
        private System.Windows.Forms.Label labelh23;
        private System.Windows.Forms.Label labelh22;
        private System.Windows.Forms.Label labelh21;
        private System.Windows.Forms.Label labelh20;
        private System.Windows.Forms.Label labelh19;
        private System.Windows.Forms.Label labelh18;
        private System.Windows.Forms.Label labelh17;
        private System.Windows.Forms.Label labelh16;
        private System.Windows.Forms.Label labelh15;
        private System.Windows.Forms.Label labelh14;
        private System.Windows.Forms.Label labelh13;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button btnsavehourblock;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtzarib;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btncap;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnsimilarblock;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnsavelastbid;
        private System.Windows.Forms.Button btnsavesimilar;
        private System.Windows.Forms.Panel panehidebutton;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PPID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Block;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriceMax;
        private System.Windows.Forms.DataGridViewTextBoxColumn Interval;
        private System.Windows.Forms.Label label4;
    }
}