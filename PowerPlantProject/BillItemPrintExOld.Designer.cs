﻿namespace PowerPlantProject
{
    partial class BillItemPrintExOld
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BillItemPrintExOld));
            this.btnSaveItemslk = new System.Windows.Forms.Button();
            this.checkedListBoxitmslk = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // btnSaveItemslk
            // 
            this.btnSaveItemslk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSaveItemslk.Location = new System.Drawing.Point(212, 322);
            this.btnSaveItemslk.Name = "btnSaveItemslk";
            this.btnSaveItemslk.Size = new System.Drawing.Size(75, 23);
            this.btnSaveItemslk.TabIndex = 42;
            this.btnSaveItemslk.Text = "Save";
            this.btnSaveItemslk.UseVisualStyleBackColor = true;
            this.btnSaveItemslk.Click += new System.EventHandler(this.btnSaveItemslk_Click_1);
            // 
            // checkedListBoxitmslk
            // 
            this.checkedListBoxitmslk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.checkedListBoxitmslk.Font = new System.Drawing.Font("B Nazanin", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.checkedListBoxitmslk.FormattingEnabled = true;
            this.checkedListBoxitmslk.Items.AddRange(new object[] {
            "s1 = آرایش تولید بازار - مگاوات ساعت",
            "s2 =تولید ناخالص - مگاوات ساعت",
            "s3 = تولید خالص - مگاوات ساعت",
            "s4=مقدار پذیرفته شده با قیمت پیشنهادی فروشنده - مگاوات ساعت",
            "s5 =مقدار پذیرفته شده با 90% قیمت حداقل همان ساعت - مگاوات ساعت",
            "s6 = میزان تولید بیش از پیشنهاد بازار - مگاوات ساعت",
            "s7= مبلغ پذیرفته شده با قیمت پیشنهادی فروشنده - ريال",
            "s8= مبلغ پذیرفته شده با 90% قیمت حداقل همان ساعت - ريال",
            "s9 =مبلغ تولید بیش از پیشنهاد بازار - ريال",
            "s10= جمع مبلغ بابت تولید خالص - ريال",
            "s11=میزان تولید کمتر از پیشنهاد بازار و به دستور مرکز - مگاوات ساعت",
            "s12 =مبلغ خسارت - ريال",
            "s13 = عدم همکاری با مرکز - مگاوات ساعت",
            "s14 = مبلغ جریمه عدم همکاری - ريال",
            "s15 =مبلغ قابل پرداخت - ريال",
            "s16 =آمادگی - مگاوات ساعت",
            "s17= مبلغ آمادگی - ريال",
            "s18 = مقدار مشمول آزمون ناموفق  ظرفیت - مگاوات ساعت",
            "s19 = مبلغ جریمه آزمون ناموفق ظرفیت - ريال",
            "s20 = مقدارآمادگی خودراه انداز - مگاوات ساعت",
            "s21 = مبلغ آمادگی خودراه انداز - ريال",
            "s22 = بهای خدمات کنترل اولیه فرکانس - ريال",
            "s23=بهای انرژی خدمات توان راکتیو - ريال",
            "s24 =بهای آمادگی خدمات توان راکتیو - ريال",
            "s26 = مقدار خالص پذیرفته شده با قیمت پیشنهادی فروشنده",
            "s27 = مقدار خالص پذیرفته شده با 90% قیمت حداقل همان ساعت",
            "s28 = میزان خالص تولید بیش از پیشنهاد بازار",
            "s29 = میزان خالص تولید کمتر از پیشنهاد بازار و به دستور مرکز",
            "s31 = ََمتوسط هزينه متغيير",
            "s36 = پرداخت بابت حسابهاي معوقه",
            "s37 = دريافت بابت حسابهاي معوقه",
            "s38 = ما به التفاوت نرخ سوخت نيروگاهي و آزاد",
            "s39 = جبران هزينه بابت تغيير سوخت نيروگاهي",
            "s40= مبلغ ما به التفاوت نرخ سوخت نيروگاهي و آزاد",
            "s41 = مقدار ما به التفاوت نرخ سوخت نيروگاهي و آزاد",
            "s42 = مقدار جبران هزينه بابت تغيير سوخت نيروگاهي",
            "s43 =مبلغ جبران هزينه بابت تغيير سوخت نيروگاهي"});
            this.checkedListBoxitmslk.Location = new System.Drawing.Point(12, 12);
            this.checkedListBoxitmslk.Name = "checkedListBoxitmslk";
            this.checkedListBoxitmslk.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkedListBoxitmslk.Size = new System.Drawing.Size(472, 288);
            this.checkedListBoxitmslk.TabIndex = 41;
            // 
            // BillItemPrintEx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(498, 356);
            this.Controls.Add(this.btnSaveItemslk);
            this.Controls.Add(this.checkedListBoxitmslk);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BillItemPrintEx";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BillItemPrintEx";
            this.Load += new System.EventHandler(this.BillItemEx_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSaveItemslk;
        internal System.Windows.Forms.CheckedListBox checkedListBoxitmslk;
    }
}