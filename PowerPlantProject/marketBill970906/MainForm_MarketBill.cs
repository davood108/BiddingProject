﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    partial class MainForm
    {            

        int tindex = 0;
         
        string[, ,] DailyBillPlant;
        string[,] DailyBillSum ;
        string[,] DailyBillTotal;
        string[,] MonthlyBillDate;
        string[, ,] MonthlyBillPlant;
        string[,] MonthlyBillTotal;
        //s1
        double[,] out1;
        double[,] out13;
        double[,] out14;
        //s2
        double[,] out2;
        //s4
        double[,] out3;
        //s5
        double[,] out4 ;
        //s6
        double[,] out5;
        //s11
        double[,] out6;
        //s8
        double[,] out8;
        //s7 
        double[,] daramadOUT3;
        //s9
        double[,] daramadOUT5;
        //s12
        double[,] daramadOUT6;
        //s10
        double[,] daramadOUT7;
        //s12
        double[,] daramadOUT8;
        //s15
        double[,] daramadOUT9;

        double[,] daramadOUT10;

        double[,] ddrclared;

        //for hourlydailyprint
      
 
        //s1
        double[,,] printout1;
        double[, ,] printout13;
        double[, ,] printout14;
        //s2,
        double[, ,] printout2;
        //s4
        double[, ,] printout3;
        //s5
        double[, ,] printout4;
        //s6
        double[, ,] printout5;
        //s11,
        double[, ,] printout6;
        //s8
        double[,,] printout8;
        //s7 
        double[, ,] printdaramadOUT3;
        //s9
        double[, ,] printdaramadOUT5;
        //s12
        double[, ,] printdaramadOUT6;
        //s10
        double[, ,] printdaramadOUT7;
        //s12
        double[, ,] printdaramadOUT8;
        //s15
        double[, ,] printdaramadOUT9;

        double[, ,] printdaramadOUT10;

        double[, ,] printddrclared;


        bool Uniti = false;


        public void filldaily()
        {
            dgbill.AllowUserToDeleteRows = false;
           
            if (rdtotal.Checked)
            {
                dgbill.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;           
               
            }
            else
            {
                dgbill.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
               
           
            }
            DataTable d=null;
           
            dgbill.DataSource = null;

            ///////////////////////////////////////////////////////////////////////

            DataTable f = Utilities.GetTable("select * from billitem");
            string item = "";
            for (int y = 0; y < f.Columns.Count; y++)
            {
                if (f.Rows[0][y].ToString().Trim() == "1" || f.Rows[0][y].ToString().Trim() == "True")
                {
                    
                        item +=  ","+ f.Columns[y].ToString() ;
                 

                }
            }
            ///////////////////////////////////////////////////////////////////

            if (rdbillplant.Checked)
            {
                d = Utilities.GetTable("select PlantName,Hour"+item+" from dailybillplant where date='" + faDatePickerbill.Text.Trim() + "'and ppid='"+lbplantmarket.Text.Trim()+"'order by hour asc");
              
                
            }
            else if(rdtotal.Checked)
            {
                d = Utilities.GetTable("select code,item,value from dailybilltotal where date='" + faDatePickerbill.Text.Trim() + "'");
              
            }
            else
            {
                d = Utilities.GetTable("select  PlantName" + item + " from DailyBillsum where date='" + faDatePickerbill.Text.Trim() + "'and ppid='" + lbplantmarket.Text.Trim() + "'");
               
            }
            if (d != null)
            {
                if (d.Rows.Count > 0) dgbill.DataSource = d;
            }


            //dgbill.Columns[0] = 100;
            //dgdailybill2.Columns[0].Width = 100;
            ////////////////////////header/////////////////////////////

            for (int m = 0; m < dgbill.Columns.Count; m++)
            {
                string name = "";
                string hed = dgbill.Columns[m].HeaderText;
                if (hed == "s1") name = "آرایش تولید بازار - مگاوات ساعت";
                else if (hed == "s2") name = "تولید ناخالص - مگاوات ساعت";
                else if (hed == "s3") name = "تولید خالص - مگاوات ساعت";
                else if (hed == "s4") name = "مقدار پذیرفته شده با قیمت پیشنهادی فروشنده - مگاوات ساعت";
                else if (hed == "s5") name = "مقدار پذیرفته شده با 90% قیمت حداقل همان ساعت - مگاوات ساعت";
                else if (hed == "s6") name = "میزان تولید بیش از پیشنهاد بازار - مگاوات ساعت";
                else if (hed == "s7") name = "مبلغ پذیرفته شده با قیمت پیشنهادی فروشنده - ريال";
                else if (hed == "s8") name = "مبلغ پذیرفته شده با 90% قیمت حداقل همان ساعت - ريال";
                else if (hed == "s9") name = "مبلغ تولید بیش از پیشنهاد بازار - ريال";
                else if (hed == "s10") name = "جمع مبلغ بابت تولید خالص - ريال";
                else if (hed == "s11") name = "میزان تولید کمتر از پیشنهاد بازار و به دستور مرکز - مگاوات ساعت";
                else if (hed == "s12") name = "مبلغ خسارت - ريال";
                else if (hed == "s13") name = "عدم همکاری با مرکز - مگاوات ساعت";
                else if (hed == "s14") name = "مبلغ جریمه عدم همکاری - ريال";
                else if (hed == "s15") name = "مبلغ قابل پرداخت - ريال";
                else if (hed == "s16") name = "آمادگی - مگاوات ساعت";
                else if (hed == "s17") name = "مبلغ آمادگی - ريال";
                else if (hed == "s18") name = "مقدار مشمول آزمون ناموفق  ظرفیت - مگاوات ساعت";
                else  if (hed == "s19") name = "مبلغ جریمه آزمون ناموفق ظرفیت - ريال";
                else if (hed == "s20") name = "مقدارآمادگی خودراه انداز - مگاوات ساعت";
                else if (hed == "s21") name = "مبلغ آمادگی خودراه انداز - ريال";
                else if (hed == "s22") name = "بهای خدمات کنترل اولیه فرکانس - ريال";
                else if (hed == "s23") name = "بهای انرژی خدمات توان راکتیو - ريال";
                else if (hed == "s24") name = "بهای آمادگی خدمات توان راکتیو - ريال";
                else if (hed == "s26") name = "مقدار خالص پذیرفته شده با قیمت پیشنهادی فروشنده";
                else if (hed == "s27") name = "مقدار خالص پذیرفته شده با 90% قیمت حداقل همان ساعت";
                else if (hed == "s28") name = "میزان خالص تولید بیش از پیشنهاد بازار";
                else if (hed == "s29") name = "میزان خالص تولید کمتر از پیشنهاد بازار و به دستور مرکز";
                else if (hed == "s31") name = "ََمتوسط هزينه متغيير";
                else if (hed == "s36") name = "پرداخت بابت حسابهاي معوقه";
                else if (hed == "s37") name = "دريافت بابت حسابهاي معوقه";
                else if (hed == "s38") name = "ما به التفاوت نرخ سوخت نيروگاهي و آزاد";
                else if (hed == "s39") name = "جبران هزينه بابت تغيير سوخت نيروگاهي";
                else if (hed == "s40") name = "مبلغ ما به التفاوت نرخ سوخت نيروگاهي و آزاد";
                else if (hed == "s41") name = "مقدار ما به التفاوت نرخ سوخت نيروگاهي و آزاد";
                else if (hed == "s42") name = "مقدار جبران هزينه بابت تغيير سوخت نيروگاهي";
                else if (hed == "s43") name = "مبلغ جبران هزينه بابت تغيير سوخت نيروگاهي";
                
                
                else name = hed;
                dgbill.Columns[m].HeaderText = name;
            }

         
        }

        private void rdbillplant_CheckedChanged(object sender, EventArgs e)
        {
            label118.Text = "";
            if (rddaily.Checked)
            {
                filldaily();
            }
            else
            {
                fillmonthly();
            }
        }

        private void rdtotal_CheckedChanged(object sender, EventArgs e)
        {
            label118.Text = "";

            if (rddaily.Checked)
            {
                filldaily();
            }
            else
            {
                fillmonthly();
            }
        }

        private void rdplantsum_CheckedChanged(object sender, EventArgs e)
        {
            label118.Text = "";
            if (rddaily.Checked)
            {
                filldaily();
            }
            else
            {
                fillmonthly();
            }
        }

        private void faDatePickerbill_ValueChanged(object sender, EventArgs e)
        {
            txtmarketlist.Text = "";
            label118.Text = "";
            if (rddaily.Checked)
            {
                filldaily();
            }
            else
            {
                fillmonthly();
            }
        }

        private void rddaily_CheckedChanged(object sender, EventArgs e)
        {
            txtmarketlist.Text = "";
            label118.Text = "";
            dgbill.DataSource = null;
           
            paneldailybill.Visible = true;
            if (rddaily.Checked)
            {
                filldaily();
            }
            else
            {
                fillmonthly();
            }
        }
        private void rdmonthly_CheckedChanged(object sender, EventArgs e)
        {
            txtmarketlist.Text = "";
            label118.Text = "";
            paneldailybill.Visible = true;
         
            dgbill.DataSource = null;
           
            if (rddaily.Checked)
            {
                filldaily();
            }
            else
            {
                fillmonthly();
            }
        }

        public void oldfillmonthly()
        {
            dgbill.AllowUserToDeleteRows = false;
          
            if (rdtotal.Checked)
            {
                dgbill.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;              
                
            }
            else
            {
                dgbill.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;              
               
            }
            DataTable d = null;
         
            dgbill.DataSource = null;

            ///////////////////////////////////////////////////////////////////////

            //DataTable f = utilities.GetTable("select * from billitem where date  in ( select max(date) from billitem where date<='" + faDatePickerbill.Text.Substring(0, 7).Trim() + "/01" + "') ");
            DataTable f = Utilities.GetTable("select * from billitem where date like'%" + faDatePickerbill.Text.Substring(0,7).Trim()+"%'");
            if (f.Rows.Count == 0)
            {
                if (User.getUser().Username != null)
                {
                  //  MessageBox.Show("Please Check Item In BillItem Form For Selected Month");
                }
                return;
               // f = utilities.GetTable("select * from billitem where date in(select max(date) from billitem )");
            }
            string item = "";
            for (int y = 0; y < f.Columns.Count; y++)
            {
                if (f.Rows[0][y].ToString().Trim() == "1" || f.Rows[0][y].ToString().Trim() == "True")
                {
                  
                        item += ","+ f.Columns[y].ToString() ;
                   

                }
            }
            ///////////////////////////////////////////////////////////////////




            if (rdbillplant.Checked)
            {
                d = Utilities.GetTable("select PlantName,Hour" + item + " from MonthlyBillPlant where date='" + faDatePickerbill.Text.Trim() + "'and ppid='" + lbplantmarket.Text.Trim() + "'order by hour asc");
             

            }
            else if (rdtotal.Checked)
            {
                d = Utilities.GetTable("select code,item,value from MonthlyBillTotal where month ='" + faDatePickerbill.Text.Substring(0, 7).Trim() + "'and ppid='" + lbplantmarket.Text.Trim() + "'");

            }
            else
            {
                d = Utilities.GetTable("select  PlantName" + item + " from MonthlyBillDate where date='" + faDatePickerbill.Text.Trim() + "'and ppid='" + lbplantmarket.Text.Trim() + "'");
               
            }
            if (d != null)
            {
                if (d.Rows.Count > 0) dgbill.DataSource = d;
            }
           


            //dgbill.Columns[0].Width = 100;
            //dgdailybill2.Columns[0].Width = 100;
            ////////////////////////header/////////////////////////////
          
            for (int m = 0; m < dgbill.Columns.Count; m++)
            {

            

                string name = "";
                string hed = dgbill.Columns[m].HeaderText;
                //if (hed == "s1") name = "آرایش تولید بازار - مگاوات ساعت";
                //else if (hed == "s2") name = "تولید ناخالص - مگاوات ساعت";
                //else if (hed == "s3") name = "تولید خالص - مگاوات ساعت";
                //else if (hed == "s4") name = "مقدار پذیرفته شده با قیمت پیشنهادی فروشنده - مگاوات ساعت";
                //else if (hed == "s5") name = "مقدار پذیرفته شده با 90% قیمت حداقل همان ساعت - مگاوات ساعت";
                //else if (hed == "s6") name = "میزان تولید بیش از پیشنهاد بازار - مگاوات ساعت";
                //else if (hed == "s7") name = "مبلغ پذیرفته شده با قیمت پیشنهادی فروشنده - ريال";
                //else if (hed == "s8") name = "مبلغ پذیرفته شده با 90% قیمت حداقل همان ساعت - ريال";
                //else if (hed == "s9") name = "مبلغ تولید بیش از پیشنهاد بازار - ريال";
                //else if (hed == "s10") name = "جمع مبلغ بابت تولید خالص - ريال";
                //else if (hed == "s11") name = "میزان تولید کمتر از پیشنهاد بازار و به دستور مرکز - مگاوات ساعت";
                //else if (hed == "s12") name = "مبلغ خسارت - ريال";
                //else if (hed == "s13") name = "عدم همکاری با مرکز - مگاوات ساعت";
                //else if (hed == "s14") name = "مبلغ جریمه عدم همکاری - ريال";
                //else if (hed == "s15") name = "مبلغ قابل پرداخت - ريال";
                //else if (hed == "s16") name = "آمادگی - مگاوات ساعت";
                //else if (hed == "s17") name = "مبلغ آمادگی - ريال";
                //else if (hed == "s18") name = "مقدار مشمول آزمون ناموفق  ظرفیت - مگاوات ساعت";
                //else if (hed == "s19") name = "مبلغ جریمه آزمون ناموفق ظرفیت - ريال";
                //else if (hed == "s20") name = "مقدارآمادگی خودراه انداز - مگاوات ساعت";
                //else if (hed == "s21") name = "مبلغ آمادگی خودراه انداز - ريال";
                //else if (hed == "s22") name = "بهای خدمات کنترل اولیه فرکانس - ريال";
                //else if (hed == "s23") name = "بهای انرژی خدمات توان راکتیو - ريال";
                //else if (hed == "s24") name = "بهای آمادگی خدمات توان راکتیو - ريال";
                //else if (hed == "s26") name = "مقدار خالص پذیرفته شده با قیمت پیشنهادی فروشنده";
                //else if (hed == "s27") name = "مقدار خالص پذیرفته شده با 90% قیمت حداقل همان ساعت";
                //else if (hed == "s28") name = "میزان خالص تولید بیش از پیشنهاد بازار";
                //else if (hed == "s29") name = "میزان خالص تولید کمتر از پیشنهاد بازار و به دستور مرکز";
                //else if (hed == "s31") name = "ََمتوسط هزينه متغيير";
                //else if (hed == "s36") name = "پرداخت بابت حسابهاي معوقه";
                //else if (hed == "s37") name = "دريافت بابت حسابهاي معوقه";
                //else if (hed == "s38") name = "ما به التفاوت نرخ سوخت نيروگاهي و آزاد";
                //else if (hed == "s39") name = "جبران هزينه بابت تغيير سوخت نيروگاهي";
                //else if (hed == "s40") name = "مبلغ ما به التفاوت نرخ سوخت نيروگاهي و آزاد";
                //else if (hed == "s41") name = "مقدار ما به التفاوت نرخ سوخت نيروگاهي و آزاد";
                //else if (hed == "s42") name = "مقدار جبران هزينه بابت تغيير سوخت نيروگاهي";
                //else if (hed == "s43") name = "مبلغ جبران هزينه بابت تغيير سوخت نيروگاهي";
                //else name = hed;
                if (hed == "s1") hed = "s01";
                if (hed == "s2") hed = "s02";
                if (hed == "s3") hed = "s03";
                if (hed == "s4") hed = "s04";
                if (hed == "s5") hed = "s05";
                if (hed == "s6") hed = "s06";
                if (hed == "s7") hed = "s07";
                if (hed == "s8") hed = "s08";
                if (hed == "s9") hed = "s09";

                 string tool="";
                 DataTable bb = Utilities.GetTable("select distinct code,item,name from dbo.MonthlyBillTotal where code='" + hed.ToUpper() + "'and month like '%" + faDatePickerbill.Text.Substring(0, 7).Trim() + "%'and ppid='" + lbplantmarket.Text.Trim() + "'");
                if(bb.Rows.Count>0)
                {
                name = bb.Rows[0][1].ToString().Trim();
                    tool= bb.Rows[0][2].ToString().Trim();
                }
                else name = hed;
                dgbill.Columns[m].HeaderText = name;
                dgbill.Columns[m].ToolTipText = tool;
            }

        

        }

        public void fillmonthly()
        {




            dgbill.AllowUserToDeleteRows = false;

            if (rdtotal.Checked)
            {
                dgbill.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            }
            else
            {
                dgbill.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;

            }
            DataTable d = null;

            dgbill.DataSource = null;

            ///////////////////////////////////////////////////////////////////////

            //DataTable f = utilities.GetTable("select * from billitem where date  in ( select max(date) from billitem where date<='" + faDatePickerbill.Text.Substring(0, 7).Trim() + "/01" + "') ");
            DataTable f = Utilities.GetTable("select * from billitem where date like'%" + faDatePickerbill.Text.Substring(0, 7).Trim() + "%'");
            if (f.Rows.Count == 0)
            {
                if (User.getUser().Username != null)
                {
                    //  MessageBox.Show("Please Check Item In BillItem Form For Selected Month");
                }
                return;
                // f = utilities.GetTable("select * from billitem where date in(select max(date) from billitem )");
            }
            string item = "";

            string forunit = "";
            if (Uniti)
            {

                if (lbunitmarket.Text.Trim().Contains("Steam "))
                {
                    forunit = " and unit='" + lbunitmarket.Text.Trim().Replace("Steam ", "S").Trim() + "'";
                }
                else  if (lbunitmarket.Text.Trim().Contains("Steam"))
                {
                    forunit = " and unit='" + lbunitmarket.Text.Trim().Replace("Steam", "S").Trim() + "'";
                }

                if (lbunitmarket.Text.Trim().Contains("Gas "))
                {
                    forunit = " and unit='" + lbunitmarket.Text.Trim().Replace("Gas ", "G").Trim() + "'";
                }
                else if (lbunitmarket.Text.Trim().Contains("Gas"))
                {
                    forunit = " and unit='" + lbunitmarket.Text.Trim().Replace("Gas", "G").Trim() + "'";
                }
                /////////////////////////////////////

                if (forunit.Contains("cc"))
                {
                    forunit =forunit.Replace("cc", "").Trim();
                }
              

            }


            for (int y = 0; y < f.Columns.Count; y++)
            {
                if (f.Rows[0][y].ToString().Trim() == "1" || f.Rows[0][y].ToString().Trim() == "True")
                {
                    if (Uniti == false)
                    {
                        item += ",sum(" + f.Columns[y].ToString() + ")as " + f.Columns[y].ToString();
                    }
                    else
                    {

                        item += "," + f.Columns[y].ToString();
                    }


                }
            }

            ///////////////////////////////////////////////////////////////////
            string group = "";
            if (Uniti == false)
            {
                group = " group by PlantName,hour";
            }




            if (rdbillplant.Checked)
            {
                d = Utilities.GetTable("select PlantName,Hour" + item + " from MonthlyBillPlant where date='" + faDatePickerbill.Text.Trim() + "'and ppid='" + lbplantmarket.Text.Trim() + "'" + forunit + group + " order by hour asc");


            }
            else if (rdtotal.Checked)
            {
                d = Utilities.GetTable("select code,item,value from MonthlyBillTotal where month ='" + faDatePickerbill.Text.Substring(0, 7).Trim() + "'and ppid='" + lbplantmarket.Text.Trim() + "'");

            }
            else
            {
                if (group != "")
                {
                    group = " group by PlantName";
                }
                d = Utilities.GetTable("select  PlantName" + item + " from MonthlyBillDate where date='" + faDatePickerbill.Text.Trim() + "'and ppid='" + lbplantmarket.Text.Trim() + "'" + forunit + group);

            }
            if (d != null)
            {
                if (d.Rows.Count > 0) dgbill.DataSource = d;
            }



            //dgbill.Columns[0].Width = 100;
            //dgdailybill2.Columns[0].Width = 100;
            ////////////////////////header/////////////////////////////

            for (int m = 0; m < dgbill.Columns.Count; m++)
            {



                string name = "";
                string hed = dgbill.Columns[m].HeaderText;
                //if (hed == "s1") name = "آرایش تولید بازار - مگاوات ساعت";
                //else if (hed == "s2") name = "تولید ناخالص - مگاوات ساعت";
                //else if (hed == "s3") name = "تولید خالص - مگاوات ساعت";
                //else if (hed == "s4") name = "مقدار پذیرفته شده با قیمت پیشنهادی فروشنده - مگاوات ساعت";
                //else if (hed == "s5") name = "مقدار پذیرفته شده با 90% قیمت حداقل همان ساعت - مگاوات ساعت";
                //else if (hed == "s6") name = "میزان تولید بیش از پیشنهاد بازار - مگاوات ساعت";
                //else if (hed == "s7") name = "مبلغ پذیرفته شده با قیمت پیشنهادی فروشنده - ريال";
                //else if (hed == "s8") name = "مبلغ پذیرفته شده با 90% قیمت حداقل همان ساعت - ريال";
                //else if (hed == "s9") name = "مبلغ تولید بیش از پیشنهاد بازار - ريال";
                //else if (hed == "s10") name = "جمع مبلغ بابت تولید خالص - ريال";
                //else if (hed == "s11") name = "میزان تولید کمتر از پیشنهاد بازار و به دستور مرکز - مگاوات ساعت";
                //else if (hed == "s12") name = "مبلغ خسارت - ريال";
                //else if (hed == "s13") name = "عدم همکاری با مرکز - مگاوات ساعت";
                //else if (hed == "s14") name = "مبلغ جریمه عدم همکاری - ريال";
                //else if (hed == "s15") name = "مبلغ قابل پرداخت - ريال";
                //else if (hed == "s16") name = "آمادگی - مگاوات ساعت";
                //else if (hed == "s17") name = "مبلغ آمادگی - ريال";
                //else if (hed == "s18") name = "مقدار مشمول آزمون ناموفق  ظرفیت - مگاوات ساعت";
                //else if (hed == "s19") name = "مبلغ جریمه آزمون ناموفق ظرفیت - ريال";
                //else if (hed == "s20") name = "مقدارآمادگی خودراه انداز - مگاوات ساعت";
                //else if (hed == "s21") name = "مبلغ آمادگی خودراه انداز - ريال";
                //else if (hed == "s22") name = "بهای خدمات کنترل اولیه فرکانس - ريال";
                //else if (hed == "s23") name = "بهای انرژی خدمات توان راکتیو - ريال";
                //else if (hed == "s24") name = "بهای آمادگی خدمات توان راکتیو - ريال";
                //else if (hed == "s26") name = "مقدار خالص پذیرفته شده با قیمت پیشنهادی فروشنده";
                //else if (hed == "s27") name = "مقدار خالص پذیرفته شده با 90% قیمت حداقل همان ساعت";
                //else if (hed == "s28") name = "میزان خالص تولید بیش از پیشنهاد بازار";
                //else if (hed == "s29") name = "میزان خالص تولید کمتر از پیشنهاد بازار و به دستور مرکز";
                //else if (hed == "s31") name = "ََمتوسط هزينه متغيير";
                //else if (hed == "s36") name = "پرداخت بابت حسابهاي معوقه";
                //else if (hed == "s37") name = "دريافت بابت حسابهاي معوقه";
                //else if (hed == "s38") name = "ما به التفاوت نرخ سوخت نيروگاهي و آزاد";
                //else if (hed == "s39") name = "جبران هزينه بابت تغيير سوخت نيروگاهي";
                //else if (hed == "s40") name = "مبلغ ما به التفاوت نرخ سوخت نيروگاهي و آزاد";
                //else if (hed == "s41") name = "مقدار ما به التفاوت نرخ سوخت نيروگاهي و آزاد";
                //else if (hed == "s42") name = "مقدار جبران هزينه بابت تغيير سوخت نيروگاهي";
                //else if (hed == "s43") name = "مبلغ جبران هزينه بابت تغيير سوخت نيروگاهي";
                //else name = hed;
                if (hed == "s1") hed = "s01";
                if (hed == "s2") hed = "s02";
                if (hed == "s3") hed = "s03";
                if (hed == "s4") hed = "s04";
                if (hed == "s5") hed = "s05";
                if (hed == "s6") hed = "s06";
                if (hed == "s7") hed = "s07";
                if (hed == "s8") hed = "s08";
                if (hed == "s9") hed = "s09";

                string tool = "";
                DataTable bb = Utilities.GetTable("select distinct code,item,name from dbo.MonthlyBillTotal where code='" + hed.ToUpper() + "'and month like '%" + faDatePickerbill.Text.Substring(0, 7).Trim() + "%'and ppid='" + lbplantmarket.Text.Trim() + "'");
                if (bb.Rows.Count > 0)
                {
                    name = bb.Rows[0][1].ToString().Trim();
                    tool = bb.Rows[0][2].ToString().Trim();
                }
                else name = hed;
                dgbill.Columns[m].HeaderText = name;
                dgbill.Columns[m].ToolTipText = tool;
            }



        }


        public void fillnribill()
        {
            double Lmin = 0.95;
            double Lmax = 1.05;
            //////////////////////////////////////////////////////////////////////////
            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + faDatePickerbill.Text.Trim() + "'order by BaseID desc");
            string smaxdate = "";
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }
            DataTable basetable1 = Utilities.GetTable("select GasHeatValueAverage from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetable1.Rows.Count > 0)
            {
                Lmax = MyDoubleParse(basetable1.Rows[0][0].ToString());
                if (Lmax < 0)
                {
                    Lmax = 1.05;
                }
            }
            DataTable basetable2 = Utilities.GetTable("select GasOilHeatValueAverage from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetable2.Rows.Count > 0)
            {
                Lmin = MyDoubleParse(basetable2.Rows[0][0].ToString());
                if (Lmin < 0)
                {
                    Lmin = 0.95;
                }
            }
            /////////////////////////FINANCIAIREPORT-PLANT//////////////////////////////
           
            FRPlantCal.Text = faDatePickerbill.Text.Trim();
            FRPlantBenefitTB.Text = "";
            FRPlantBidPayTb.Text = "";
            FRPlantBidPowerTb.Text = "";
            FRPlantEnergyPayTb.Text = "";
            FRPlantIncomeTb.Text = "";
            FRPlantIncPayTb.Text = "";
            FRPlantDecPowerTb.Text = "";
            FRPlantIncPowerTb.Text = "";
            FRPlantTotalPowerTb.Text = "";
            FRPlantULPayTb.Text = "";
            FRPlantULPowerTb.Text = "";
            FRPlantDecPayTb.Text = "";
            FRPlantCostTb.Text = "";
            FRPlantCapPayTb.Text = "";
            FRPlantAvaCapTb.Text = "";
            //////////////////////////////////////////////////////////
            DataTable dd = Utilities.GetTable("select ppid from powerplant where ppid='" + PPID + "'");
            int jj = dd.Rows.Count;
            int dx =dgbill.Rows.Count - 1;
            
            string[] Blink = new string[dx];
            
            for (int t = 2; t < dgbill.Columns.Count; t++)
            {
                for (int m = 0; m < dgbill.Rows.Count - 1; m++)
                {
                    this.dgbill.ClearSelection();
                    string name = "";

                    string hed = dgbill.Columns[t].HeaderText;
                    string tool = dgbill.Columns[t].ToolTipText;

                    if (rdtotal.Checked)
                    {
                        hed = dgbill.Rows[m].Cells[1].Value.ToString();
                        tool = "";
                    }
                    else if (rdplantsum.Checked)
                    {
                        hed = dgbill.Columns[t - 1].HeaderText;
                        tool = dgbill.Columns[t - 1].ToolTipText;
                    }

                    if ((hed == "مبلغ قابل پرداخت - ريال") || (hed == "بهاي قابل پرداخت-ريال") || (tool == "EnergyPayment"))
                    {
                        Blink[m] = "Y";
                        double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());

                        if (rdbillplant.Checked)
                        {
                            int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                           // double x = daramadOUT9[0, h - 1];
                            double x = daramadOUT9[tindex, h - 1];

                            if (((x >= y * (Lmin)) && (x <= y * (Lmax))) )
                            {

                            }
                            else
                            {
                                Blink[m] = "N";
                            }
                        }
                        // }
                    }
                }
            }
            //////////////////////////////////////////////////////////
            DataTable d = Utilities.GetTable("select ppid from powerplant where ppid='" + PPID + "'");
            int j = d.Rows.Count;

              for (int t = 2; t < dgbill.Columns.Count ;t++)
            {
                for (int m = 0; m < dgbill.Rows.Count-1; m++)
                {
                    this.dgbill.ClearSelection();
                    string name = "";

                    //string hed = dgbill.Columns[t].HeaderText;
                    //if (rdtotal.Checked) hed = dgbill.Rows[m].Cells[1].Value.ToString();
                    //else if (rdplantsum.Checked) hed = dgbill.Columns[t-1].HeaderText;

                    string hed = dgbill.Columns[t].HeaderText;
                    string tool = dgbill.Columns[t].ToolTipText;

                    if (rdtotal.Checked)
                    {
                        hed = dgbill.Rows[m].Cells[1].Value.ToString();
                        tool = "";
                    }
                    else if (rdplantsum.Checked)
                    {
                        hed = dgbill.Columns[t - 1].HeaderText;
                        tool = dgbill.Columns[t - 1].ToolTipText;
                    }


                    if ((hed == "آرایش تولید بازار - مگاوات ساعت") || (hed == "ميزان خالص آرايش توليد بازار -مگاوات ساعت") || (tool == "Market Power"))
                    {
                        //s1
                        double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());
                        //if (rddaily.Checked)
                        //{

                        ////////////////////////////////
                        double ffs = 0;
                        double ffa = 0;
                        for (int n = 0; n < 24; n++)
                        {
                            //ffs += out1[0, n];
                            //ffa += ddrclared[0, n];
                            ffs += out1[tindex, n];
                            ffa += ddrclared[tindex, n];
                        }
                        FRPlantAvaCapTb.Text = ffa.ToString();
                        FRPlantBidPowerTb.Text= ffs.ToString();

                        //////

                        ////////////////////////////////////////////

                            if (rdbillplant.Checked)
                            {
                                int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                                double x = out1[tindex, h - 1];

                                if (((x >= y * (Lmin)) && (x <= y * (Lmax))) ||(Blink[m] != "N"))
                                {

                                }
                                else
                                {
                                    dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                    dgbill.Rows[m].Cells[t].ToolTipText ="OutPut:  "+ x +"  Diff : "+ (y - x).ToString();
                                }
                             
                            }
                            else if (rdtotal.Checked)
                            {
                                double sum = 0;
                                for (int i = 0; i < j; i++)
                                {
                                    for (int s = 0; s < 24; s++)
                                    {
                                        sum += out1[i, s];
                                    }
                                }

                                if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                                {

                                }
                                else
                                {
                                    dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                    dgbill.Rows[m].Cells[t].ToolTipText = "OutPut:  " + sum + "  Diff : " + (y - sum).ToString();
                                }

                            }
                            else if (rdplantsum.Checked)
                            {
                                double sum = 0;
                                for (int i = 0; i < j; i++)
                                {
                                    for (int s = 0; s < 24; s++)
                                    {
                                        sum += out1[i, s];
                                    }
                                    double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t - 1].Value.ToString());

                                    if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                                    {

                                    }
                                    else
                                    {
                                        dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                        dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff : " + (y1 - sum).ToString();
                                    }
                                }

                            }
                        //}

                       
                    }
                    else if ((hed == "تولید ناخالص - مگاوات ساعت") || (hed == "ميزان توليد ناخالص-مگاوات ساعت") || (tool == "TotalPower"))
                    {
                        ////////////////////////////////
                        double ffs = 0;
                        for (int n = 0; n < 24; n++)
                        {
                            ffs += out2[tindex, n];
                        }
                        FRPlantTotalPowerTb.Text = ffs.ToString();
                        ////////////////////////////////

                          double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());
                          
                            if (rdbillplant.Checked)
                            {
                                int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                                double x = out2[tindex, h - 1];

                                if (((x >= y * (Lmin)) && (x <= y * (Lmax))) ||(Blink[m] != "N"))
                                {

                                }
                                else
                                {
                                    dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                    dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                                }
                            }
                            else if (rdtotal.Checked)
                            {
                                double sum = 0;
                                for (int i = 0; i < j; i++)
                                {
                                    for (int s = 0; s < 24; s++)
                                    {
                                        sum += out2[i, s];
                                    }
                                }
                                if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                                {

                                }
                                else
                                {
                                    dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                    dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                                }

                            }
                            else if (rdplantsum.Checked)
                            {
                               double sum = 0;
                                for (int i = 0; i < j; i++)
                                {
                                    for (int s = 0; s < 24; s++)
                                    {                                       
                                        sum += out2[i, s];
                                    }
                                    double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t-1].Value.ToString());

                                    if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                                    {

                                    }
                                    else
                                    {
                                        dgbill.Rows[i].Cells[t-1].Style.ForeColor = Color.Red;
                                        dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                                    }
                                }

                            }
                        //}

                    }
                    else if ((hed == "مقدار پذیرفته شده با قیمت پیشنهادی فروشنده - مگاوات ساعت") || (hed == "ميزان خالص پذيرفته شده با قيمت پيشنهادي فروشنده-مگاوات ساعت") || (tool == "BidPower"))
                    {
                        double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());
                        //if (rddaily.Checked)
                        //{
                        //////////////////////////////////////
                        double ffs = 0;
                        for (int n = 0; n < 24; n++)
                        {
                            ffs += out3[tindex, n];
                        }
                        //FRPlantTotalPowerTb.Text = ffs.ToString();
                        //////////////////////////////////////
                        if (rdbillplant.Checked)
                        {
                            int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                            double x = out3[tindex, h - 1];

                            if (((x >= y * (Lmin)) && (x <= y * (Lmax))) ||(Blink[m] != "N"))
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                            }

                        }
                        else if (rdtotal.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += out3[i, s];
                                }
                            }
                            if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                            }

                        }
                        else if (rdplantsum.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += out3[i, s];
                                }
                                double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t - 1].Value.ToString());
                                if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                                {

                                }
                                else
                                {
                                    dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                    dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                                }
                            }

                        }
                        //}


                    }
                    else if ((hed == "مقدار پذیرفته شده با 90% قیمت حداقل همان ساعت - مگاوات ساعت") || (hed == "ميزان خالص پذيرفته شده با نرخ UL-مگاوات ساعت") || (tool == "ULPower"))
                    {
                        double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());
                        //if (rddaily.Checked)
                        //{
                        //////////////////////////////////////
                        double ffs = 0;
                        for (int n = 0; n < 24; n++)
                        {
                            ffs += out4[tindex, n];
                        }
                        FRPlantULPowerTb.Text = ffs.ToString();
                        //////////////////////////////////////
                        if (rdbillplant.Checked)
                        {
                            int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                            double x = out4[tindex, h - 1];

                            if (((x >= y * (Lmin)) && (x <= y * (Lmax))) ||(Blink[m] != "N"))
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                            }
                       
                        }
                        else if (rdtotal.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += out4[i, s];
                                }
                            }
                            if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                            }

                        }
                        else if (rdplantsum.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += out4[i, s];
                                }
                                double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t - 1].Value.ToString());
                                if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                                {

                                }
                                else
                                {
                                    dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                    dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                                }
                            }

                        }
                        //}
                    }
                    else if ((hed == "میزان تولید بیش از پیشنهاد بازار - مگاوات ساعت") || (hed == "ميزان خالص توليد بيش از پيشنهاد بازارو به دستور مرکز-مگاوات ساعت") || (tool == "IncrementPower"))
                    {
                        double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());
                        //if (rddaily.Checked)
                        //{
                        //////////////////////////////////////
                        double ffs = 0;
                        for (int n = 0; n < 24; n++)
                        {
                            ffs += out5[tindex, n];
                        }
                         FRPlantIncPowerTb.Text = ffs.ToString();
                        //////////////////////////////////////
                        if (rdbillplant.Checked)
                        {
                            int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                            double x = out5[tindex, h - 1];

                            if (((x >= y * (Lmin)) && (x <= y * (Lmax))) ||(Blink[m] != "N"))
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                            }
                      
                        }
                        else if (rdtotal.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += out5[i, s];
                                }
                            }
                            if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                            }

                        }
                        else if (rdplantsum.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += out5[i, s];
                                }
                                double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t - 1].Value.ToString());
                                if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                                {

                                }
                                else
                                {
                                    dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                    dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                                }
                            }

                        }
                        // }

                    }
                    else if ((hed == "مبلغ پذیرفته شده با قیمت پیشنهادی فروشنده - ريال") || (hed == "بهاي پرداختي با قيمت پيشنهادي فروشنده-ريال") || (tool == "BidPayment"))
                    {
                        double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());
                        //if (rddaily.Checked)
                        //{
                        //////////////////////////////////////
                        double ffs = 0;
                        for (int n = 0; n < 24; n++)
                        {
                            ffs += daramadOUT3[tindex, n];
                        }
                        FRPlantBidPayTb.Text = ffs.ToString();
                        //////////////////////////////////////
                        if (rdbillplant.Checked)
                        {
                            int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                            double x = daramadOUT3[tindex, h - 1];

                            if (((x >= y * (Lmin)) && (x <= y * (Lmax))) ||(Blink[m] != "N"))
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                            }
                            
                        }
                        else if (rdtotal.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += daramadOUT3[i, s];
                                }
                            }
                            if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + " Diff :  " + (y - sum).ToString();
                            }

                        }
                        else if (rdplantsum.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += daramadOUT3[i, s];
                                }
                                double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t - 1].Value.ToString());
                                if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                                {

                                }
                                else
                                {
                                    dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                    dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                                }
                            }

                        }
                        // }
                    }
                    else if ((hed == "مبلغ پذیرفته شده با 90% قیمت حداقل همان ساعت - ريال") || (hed == "بهاي پرداختي با نرخ UL-ريال") || (tool == "ULPayment"))
                    {
                        double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());
                        //if (rddaily.Checked)
                        //{
                        //////////////////////////////////////
                        double ffs = 0;
                        for (int n = 0; n < 24; n++)
                        {
                            ffs += out8[tindex, n];
                        }
                         FRPlantULPayTb.Text = ffs.ToString();
                        //////////////////////////////////////
                        if (rdbillplant.Checked)
                        {
                            int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                            double x = out8[tindex, h - 1];

                            if (((x >= y * (Lmin)) && (x <= y * (Lmax))) ||(Blink[m] != "N"))
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                            }
                         
                        }
                        else if (rdtotal.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += out8[i, s];
                                }
                            }
                            if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                            }

                        }
                        else if (rdplantsum.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += out8[i, s];
                                }
                                double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t - 1].Value.ToString());
                                if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                                {

                                }
                                else
                                {
                                    dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                    dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                                }
                            }

                        }
                        // }
                    }
                    else if ((hed == "مبلغ تولید بیش از پیشنهاد بازار - ريال") || (hed == "بهاي پرداختي بابت توليد بيش از پيشنهاد بازار-ريال") || (tool == "IncrementPayment"))
                    {
                        double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());
                        //if (rddaily.Checked)
                        //{
                        //////////////////////////////////////
                        double ffs = 0;
                        for (int n = 0; n < 24; n++)
                        {
                            ffs += daramadOUT5[tindex, n];
                        }
                        FRPlantIncPayTb.Text = ffs.ToString();
                        //////////////////////////////////////
                        if (rdbillplant.Checked)
                        {
                            int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                            double x = daramadOUT5[tindex, h - 1];
                            if (((x >= y * (Lmin)) && (x <= y * (Lmax))) ||(Blink[m] != "N"))
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                            }
                          
                        }
                        else if (rdtotal.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += daramadOUT5[i, s];
                                }
                            }
                            if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                            }

                        }
                        else if (rdplantsum.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += daramadOUT5[i, s];
                                }
                                double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t - 1].Value.ToString());
                                if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                                {

                                }
                                else
                                {
                                    dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                    dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                                }
                            }

                        }
                        //}
                    }
                    else if ((hed == "جمع مبلغ بابت تولید خالص - ريال") || (hed == "جمع بهاي پرداختي بابت توليد خالص-ريال") || (tool == "Pure Energy Payment"))
                    {
                        double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());
                        //if (rddaily.Checked)
                        //{
                        //////////////////////////////////////
                        double ffs = 0;
                        for (int n = 0; n < 24; n++)
                        {
                            ffs += daramadOUT9[tindex, n];
                        }
                        FRPlantCapPayTb.Text = "0";
                        FRPlantEnergyPayTb.Text = ffs.ToString();
                        //////////////////////////////////////
                        if (rdbillplant.Checked)
                        {
                            int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                            double x = daramadOUT7[tindex, h - 1];

                            if (((x >= y * (Lmin)) && (x <= y * (Lmax))) ||(Blink[m] != "N"))
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                            }
                           
                        }
                        else if (rdtotal.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += daramadOUT7[i, s];
                                }
                            }
                            if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :   " + (y - sum).ToString();
                            }

                        }
                        else if (rdplantsum.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += daramadOUT7[i, s];
                                }
                                double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t - 1].Value.ToString());
                                if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                                {

                                }
                                else
                                {
                                    dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                    dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                                }
                            }

                        }
                        //}
                    }
                    else if ((hed == "میزان تولید کمتر از پیشنهاد بازار و به دستور مرکز - مگاوات ساعت") || (hed == "ميزان توليدکمترازپيشنهاد بازارو به دستور مرکز-مگاوات ساعت") || (tool == "DecreasePower"))
                    {
                        double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());
                        //if (rddaily.Checked)
                        //{
                        //////////////////////////////////////
                        double ffs = 0;
                        for (int n = 0; n < 24; n++)
                        {
                            ffs += out6[tindex, n];
                        }
                        FRPlantDecPowerTb.Text =Math.Abs(ffs).ToString();
                        //////////////////////////////////////
                        if (rdbillplant.Checked)
                        {
                            int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                            double x = out6[tindex, h - 1];

                            if (((x >= y * (Lmin)) && (x <= y * (Lmax)))  || (Blink[m] != "N"))
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                            }
                            
                        }
                        else if (rdtotal.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += out6[i, s];
                                }
                            }
                            if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                            }

                        }
                        else if (rdplantsum.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += out6[i, s];
                                }
                                double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t - 1].Value.ToString());
                                if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                                {

                                }
                                else
                                {
                                    dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                    dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                                }
                            }

                        }
                        //}
                    }
                    else if ((hed == "مبلغ خسارت - ريال") || (hed == "بهاي پرداختي بابت سلب فرصت-ريال") || (tool == "DecreasePayment"))
                    {
                        double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());
                        //if (rddaily.Checked)
                        //{
                        //////////////////////////////////////
                        double ffs = 0;
                        for (int n = 0; n < 24; n++)
                        {
                            ffs += daramadOUT8[tindex, n];
                        }
                        FRPlantDecPayTb.Text = Math.Abs(ffs).ToString();
                        //////////////////////////////////////
                        if (rdbillplant.Checked)
                        {
                            int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                            double x = daramadOUT8[tindex, h - 1];

                            if (((x >= y * (Lmin)) && (x <= y * (Lmax))) ||(Blink[m] != "N"))
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                            }
                             
                        }
                        else if (rdtotal.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += daramadOUT8[i, s];
                                }
                            }
                            if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                            {

                            }
                            else
                            {
                                //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                            }

                        }
                        else if (rdplantsum.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += daramadOUT8[i, s];
                                }
                                double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t - 1].Value.ToString());
                                if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                                {

                                }
                                else
                                {
                                    // dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                    // dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                                }
                            }

                        }
                        //}
                    }
                    //else if (hed == "s13") name = "عدم همکاری با مرکز - مگاوات ساعت";
                    //else if (hed == "s14") name = "مبلغ جریمه عدم همکاری - ريال";
                    else if ((hed == "مبلغ قابل پرداخت - ريال") || (hed == "بهاي قابل پرداخت-ريال") || (tool == "EnergyPayment"))
                    {
                        double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());
                        //if (rddaily.Checked)
                        //{

                        if (rdbillplant.Checked)
                        {
                            int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                            double x = daramadOUT9[tindex, h - 1];

                            if (((x >= y * (Lmin)) && (x <= y * (Lmax)))  || (Blink[m] != "N"))
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                            }
                        }
                        else if (rdtotal.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += daramadOUT9[i, s];
                                }
                            }
                            if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                            }

                        }
                        else if (rdplantsum.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += daramadOUT9[i, s];
                                }
                                double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t - 1].Value.ToString());
                                if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                                {

                                }
                                else
                                {
                                    dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                    dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                                }
                            }

                        }
                        
                    }
                    if ((hed == "ميزان آمادگي - مگاوات ساعت") || (tool == "AvailableCapacity"))
                    {
                        //
                        double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());
                        //if (rddaily.Checked)
                        //{

                        ////////////////////////////////
                        //double ffs = 0;
                        //double ffa = 0;
                        //for (int n = 0; n < 24; n++)
                        //{                           
                        //   ffa += ddrclared[tindex, n];
                        //}
                 

                        //////

                        ////////////////////////////////////////////

                        if (rdbillplant.Checked)
                        {
                            int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                            double x = ddrclared[tindex, h - 1];

                            if (((x >= y * (Lmin)) && (x <= y * (Lmax))) || (Blink[m] != "N"))
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut:  " + x + "  Diff : " + (y - x).ToString();
                            }

                        }
                        else if (rdtotal.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += ddrclared[i, s];
                                }
                            }

                            if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))))
                            {

                            }
                            else
                            {
                                dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                                dgbill.Rows[m].Cells[t].ToolTipText = "OutPut:  " + sum + "  Diff : " + (y - sum).ToString();
                            }

                        }
                        else if (rdplantsum.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += ddrclared[i, s];
                                }
                                double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t - 1].Value.ToString());

                                if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                                {

                                }
                                else
                                {
                                    dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                    dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff : " + (y1 - sum).ToString();
                                }
                            }

                        }
                        


                    }
                }
            }
        }
        private void btnbilling_Click(object sender, EventArgs e)
        {
            btnbilling.Enabled = false;
            this.Cursor = Cursors.WaitCursor;
           
            try
            {
                label118.Text = "";
               
                
                if (rdtotal.Checked && rdmonthly.Checked)
                {
                    label118.Text = "Waiting......";
                    fillmonthlyvalue();
                    label118.Text = "Completed......";
                    this.Cursor = Cursors.Default;
                }
                //else if (rdmonthly.Checked)
                //{
                //    filldailyvalue_month();
                //}
                else
                {
                    label118.Text = "Waiting......";
                   
                    filldailyvalue();
                    label118.Text = "Completed......";
                    this.Cursor = Cursors.Default;
                }

            }
            catch
            {
                this.Cursor = Cursors.Default;
            }
            btnbilling.Enabled = true;
        }
        public void fillmonthlyvalue()
        {
            //btnmarketbillprint.Visible = false;
            int wrong = 0;
            int xbreak = 0;
            int tt = new PersianDate(faDatePickerbill.Text).MonthDays;

            string start = faDatePickerbill.Text.Remove(8, 2).ToString() + "01";
            string to = new PersianDate(PersianDateConverter.ToGregorianDateTime(start).AddDays(tt - 1)).ToString("d");
            string todate = to;

            ///////////////////////////////////////////////////////////
            bool kabise = false;
            int year = new PersianDate(start).Year;
            if (year % 4 == 3)
            {
                kabise = true;

            }
            else if (new PersianDate(start).Month == 12)
            {
                string v = start.Substring(8, 2);
                to = start.Remove(8, 2);
                to = start.Replace(v, "29");
                todate = to;
            }

            TimeSpan nnn = PersianDateConverter.ToGregorianDateTime(to) - PersianDateConverter.ToGregorianDateTime(start);
            int day = nnn.Days;


            DataTable d = Utilities.GetTable("select ppid from powerplant where ppid='"+PPID+"'");
            DataTable f = Utilities.GetTable("select * from billitem");
            string[] item = new string[f.Columns.Count];
            int nn = 0;
            for (int y = 0; y < f.Columns.Count; y++)
            {
                if (f.Rows[0][y].ToString().Trim() == "1" || f.Rows[0][y].ToString().Trim() == "True")
                {

                    item[nn] = f.Columns[y].ToString();
                    nn++;
                }
            }
            
       
            //s1
            out1 = new double[d.Rows.Count, 24];
            out13 = new double[d.Rows.Count, 24];
            out14 = new double[d.Rows.Count, 24];
            //s2
            out2 = new double[d.Rows.Count, 24];
            //s4
            out3 = new double[d.Rows.Count, 24];
            //s5
            out4 = new double[d.Rows.Count, 24];
            //s6
            out5 = new double[d.Rows.Count, 24];
            //s11
            out6 = new double[d.Rows.Count, 24];
            //s8
            out8 = new double[d.Rows.Count, 24];
            //s7 
            daramadOUT3 = new double[d.Rows.Count, 24];
            //s9
            daramadOUT5 = new double[d.Rows.Count, 24];
            //s12
            daramadOUT6 = new double[d.Rows.Count, 24];
            //s10
            daramadOUT7 = new double[d.Rows.Count, 24];
            //s12
            daramadOUT8 = new double[d.Rows.Count, 24];
            //s15
            daramadOUT9 = new double[d.Rows.Count, 24];
            bool enter = true;
            ddrclared = new double[d.Rows.Count, 24];


            ////////////////////////printdailyhourly///////////////////////////
            //s1
            printout1 = new double[d.Rows.Count, (day+1), 24];
            printout13 = new double[d.Rows.Count, (day + 1), 24];
            printout14 = new double[d.Rows.Count, (day + 1), 24];
            //s2
            printout2 = new double[d.Rows.Count, (day + 1), 24];
            //s4
            printout3 = new double[d.Rows.Count, (day + 1), 24];
            //s5
            printout4 = new double[d.Rows.Count, (day + 1), 24];
            //s6
            printout5 = new double[d.Rows.Count,(day+1), 24];
            //s11
            printout6 = new double[d.Rows.Count,(day + 1), 24];
            //s8
            printout8 = new double[d.Rows.Count, (day + 1), 24];
            //s7 
            printdaramadOUT3 = new double[d.Rows.Count,(day+1),24];
            //s9
            printdaramadOUT5 = new double[d.Rows.Count, (day + 1), 24];
            //s12
            printdaramadOUT6 = new double[d.Rows.Count, (day + 1), 24];
            //s10
            printdaramadOUT7 = new double[d.Rows.Count,(day+1), 24];
            //s12
            printdaramadOUT8 = new double[d.Rows.Count,(day+1), 24];
            //s15
            printdaramadOUT9 = new double[d.Rows.Count,(day+1), 24];
          
            printddrclared = new double[d.Rows.Count, (day+1),24];

            double[,] valhub1 = new double[d.Rows.Count, 24];
            double[,] valtrans1 = new double[d.Rows.Count, 24];

            double[, ,] valhub = new double[d.Rows.Count, 12, 3];
            double[, ,] valtrans = new double[d.Rows.Count, 12, 3];

            double Fpure = 0.9874299363235;








            ////////////////////////////////////////////////////////////////

            for (int ddd = 0; ddd <= day; ddd++)
            {
                if (wrong == 0)
                {
                    int i = 0;
                    string date = new PersianDate(PersianDateConverter.ToGregorianDateTime(start).AddDays(ddd)).ToString("d");
                    //////////////////////////////////////////////////////////////////////////
                    DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + date + "'order by BaseID desc");
                    string staterun = "";
                    string Marketrule = "";
                    string smaxdate = "";
                    if (dtsmaxdate.Rows.Count > 0)
                    {
                        string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                        int ib = 0;
                        foreach (DataRow m in dtsmaxdate.Rows)
                        {
                            arrbasedata[ib] = m["Date"].ToString();
                            ib++;
                        }
                        smaxdate = buildmaxdate(arrbasedata);
                    }
                    else
                    {
                        dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                        smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
                    }
                    DataTable basetable = Utilities.GetTable("select ProposalDay from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                    if (basetable.Rows.Count > 0)
                    {
                        Marketrule = basetable.Rows[0][0].ToString();
                    }
                    //////////////////////////////////////////////////////////////////////////
                    DataTable basetabledata = Utilities.GetTable("select ProposalHour from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                    if (basetabledata.Rows.Count > 0)
                    {
                        staterun = basetabledata.Rows[0][0].ToString();
                    }
                    //////////////////////////////////////////////////////////////////////////
                    DataTable basetabledata22 = Utilities.GetTable("select LineEnergyPayment from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                    if (basetabledata22.Rows.Count > 0)
                    {
                        Fpure=MyDoubleParse(basetabledata22.Rows[0][0].ToString());
                    }

                    /////////////////////////////////////////////////////////////////////
                    foreach (DataRow n in d.Rows)
                    {

                        //////////////////////////////////////////general data //////////////////////////////////////////////////////

                            for (int h = 0; h < 24; h++)
                            {

                                ///////////////////////////////////////////*NEW////////////////////////////////////////////
                                /////////////////////////////////hub-trans//////////////////////////////////////////////////

                                string refer = "";
                                string trans = "";
                                DataTable basetabledata2 = Utilities.GetTable("select Reference,transmission from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                                if (basetabledata2.Rows.Count > 0)
                                {
                                    refer = basetabledata2.Rows[0][0].ToString();
                                    trans = basetabledata2.Rows[0][1].ToString();
                                }
                                string year2 = date.Substring(0, 4).Trim();
                                DataTable xx1 = null;
                                DataTable xx2 = null;
                                if (refer != "Normal" && refer != "")
                                {
                                    xx1 = Utilities.GetTable("select * from yearhub where year='" + year2 + "'and  ppid='" + n[0].ToString().Trim() + "'");
                                }
                                if (trans != "Normal" && trans != "")
                                {
                                    xx2 = Utilities.GetTable("select * from yeartrans where year='" + year2 + "' and ppid='" + n[0].ToString().Trim() + "'");
                                }
                                ///////////////////////                          

                              
                                string month = date.Substring(5, 2).Trim();
                                if (MyDoubleParse(month) < 10)
                                {
                                    month = MyDoubleParse(month).ToString();
                                }
                              
                                
                                    for (int u = 0; u < 12; u++)
                                    {
                                        if (xx1 != null && xx1.Rows.Count>0)
                                        {
                                            valhub[i, u, 0] = MyDoubleParse(xx1.Rows[0]["M" + (u + 1) + "Low"].ToString());
                                            valhub[i, u, 1] = MyDoubleParse(xx1.Rows[0]["M" + (u + 1) + "Mid"].ToString());
                                            valhub[i, u, 2] = MyDoubleParse(xx1.Rows[0]["M" + (u + 1) + "Peak"].ToString());
                                        }
                                        if (xx2 != null && xx2.Rows.Count > 0)
                                        {
                                            valtrans[i, u, 0] = MyDoubleParse(xx2.Rows[0]["M" + (u + 1) + "Low"].ToString());
                                            valtrans[i, u, 1] = MyDoubleParse(xx2.Rows[0]["M" + (u + 1) + "Mid"].ToString());
                                            valtrans[i, u, 2] = MyDoubleParse(xx2.Rows[0]["M" + (u + 1) + "Peak"].ToString());
                                        }
                                    }


                                    DataTable zx = Utilities.GetTable("select H" + ((h + 1).ToString()) + " from dbo.bourse where fromdate<='" + date + "' and todate>='" + date + "' order by fromdate,id desc");
                                    if (zx.Rows.Count == 0)
                                    {
                                        zx = Utilities.GetTable("select H" + ((h + 1).ToString()) + " from dbo.bourse where fromdate<='" + date + "'");
                                    }
                                string c = "";
                                if (zx.Rows.Count > 0)
                                {
                                    c = zx.Rows[0][0].ToString().Trim();
                                    if (c == "Medium")
                                    {
                                        valhub1[i, h] = 1000 * valhub[i, int.Parse(month) - 1, 1];
                                        valtrans1[i, h] = 0.01 * valtrans[i, int.Parse(month) - 1, 1];
                                    }
                                    else if (c == "Low")
                                    {
                                        valhub1[i, h] = 1000 * valhub[i, int.Parse(month) - 1, 0];
                                        valtrans1[i, h] = 0.01 * valtrans[i, int.Parse(month) - 1, 0];
                                    }
                                    else if (c == "peak")
                                    {
                                        valhub1[i, h] = 1000 * valhub[i, int.Parse(month) - 1, 2];
                                        valtrans1[i, h] = 0.01 * valtrans[i, int.Parse(month) - 1, 2];
                                    }

                                }

                                ////////////////////////////////////*****new****////////////////////////////////////////
                                


                                if (wrong == 0)
                                {
                                double avc = 0;
                                DataTable r1 = Utilities.GetTable("select sum(Required) from dbo.DetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                                if (Marketrule == "Fuel Limited")
                                {
                                    r1 = Utilities.GetTable("select sum(Required) from dbo.BaDetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                                }
                                DataTable r3 = Utilities.GetTable("select sum(Required) from dbo.DetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                                if (Marketrule == "Fuel Limited")
                                {
                                    r3 = Utilities.GetTable("select sum(Required) from dbo.BaDetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                                }
                                DataTable r4 = Utilities.GetTable("select sum(economic) from dbo.DetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                                if (Marketrule == "Fuel Limited")
                                {
                                    r4 = Utilities.GetTable("select sum(economic) from dbo.BaDetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                                }

                                //if ((staterun == "Economic") || (staterun == "Maximum"))
                                //{
                                //    r1 = utilities.GetTable("select sum(economic) from dbo.DetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                                //    if (Marketrule == "Fuel Limited")
                                //    {
                                //        r1 = utilities.GetTable("select sum(economic) from dbo.BaDetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                                //    }
                                //}

                                DataTable r2 = Utilities.GetTable("select sum(p) from dbo.DetailFRM009 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h) + "'");

                                //if ((staterun == "Maximum"))
                                //{
                                //    foreach (DataRow n3 in r3.Rows)
                                //    {
                                //        printout13[i, ddd, h] = MyDoubleParse(n3[0].ToString());
                                //        out13[i, h] += MyDoubleParse(n3[0].ToString());
                                //    }
                                //    foreach (DataRow n4 in r4.Rows)
                                //    {
                                //        printout14[i, ddd, h] = MyDoubleParse(n4[0].ToString());
                                //        out14[i, h] += MyDoubleParse(n4[0].ToString());
                                //    }
                                //    printout1[i, ddd, h] = Math.Max(printout13[i, ddd, h], printout14[i, ddd, h]);
                                //    out1[i, h] = Math.Max(out13[i, h], out14[i, h]);
                                //}
                                //else
                                {
                                    foreach (DataRow n1 in r1.Rows)
                                    {
                                        printout1[i, ddd, h] = MyDoubleParse(n1[0].ToString());
                                        out1[i, h] += MyDoubleParse(n1[0].ToString());
                                    }
                                }
                                foreach (DataRow n2 in r2.Rows)
                                {
                                    printout2[i, ddd, h] = MyDoubleParse(n2[0].ToString());
                                    out2[i, h] += MyDoubleParse(n2[0].ToString());
                                }

                                DataTable p = Utilities.GetTable("select unitcode,packagetype,avc from unitsdatamain where ppid='" + n[0].ToString().Trim() + "'");
                                double Avc_interval = 240000;
                                foreach (DataRow b in p.Rows)
                                {
                                    string type = "";
                                    avc = MyDoubleParse(b[2].ToString());
                                    string FuelType = fuel(n[0].ToString().Trim(), date);
                                    Avc_interval = Avc_date(n[0].ToString().Trim(), date);
                                    string m005 = detectblockm005(b[0].ToString().Trim(), b[1].ToString(), n[0].ToString().Trim());
                                    string m002 = detectblockm002(b[0].ToString().Trim(), b[1].ToString(), n[0].ToString().Trim());
                                    double Pact = 0;
                                    DataTable dd88 = Utilities.GetTable("select * from dbo.DetailFRM002 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'and block='" + m002 + "'and estimated='0'");
                                    DataTable dd = Utilities.GetTable("select economic,Contribution,required from dbo.DetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'and block='" + m005 + "'");

                                    if (Marketrule == "Fuel Limited")
                                    {
                                        dd = Utilities.GetTable("select economic,Contribution,required from dbo.BaDetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'and block='" + m005 + "'");
                                    }

                                    DataTable PactTable = Utilities.GetTable("select H" + ((h + 1).ToString()) + "  from dbo.Pactual where PPID='" + n[0].ToString().Trim() + "' AND Unit='" + m002 + "'AND date='" + date + "'");
                                    if (PactTable == null)
                                    {
                                        Pact = MyDoubleParse(dd88.Rows[0]["dispachablecapacity"].ToString());
                                    }
                                    else
                                    {
                                        if (PactTable.Rows.Count > 0)
                                        {
                                            Pact = MyDoubleParse(PactTable.Rows[0][0].ToString());
                                        }
                                        Pact = MyDoubleParse(dd88.Rows[0]["dispachablecapacity"].ToString());
                                    }

                                    DataTable dd2 = Utilities.GetTable("select p from dbo.DetailFRM009 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h) + "'and block='" + b[0].ToString() + "'");
                                    if (dd.Rows.Count > 0 && dd2.Rows.Count > 0 && dd88.Rows.Count > 0)
                                    {


                                        double req = Fpure * (1 - valtrans1[i, h]) * MyDoubleParse(dd.Rows[0][2].ToString());
                                        if (req > Pact)
                                        {
                                            req = Pact;
                                        }
                                        double eco = Fpure * (1 - valtrans1[i, h]) * MyDoubleParse(dd.Rows[0][0].ToString());
                                        if (eco > Pact)
                                        {
                                            eco = Pact;
                                        }
                                        string contr = dd.Rows[0][1].ToString();
                                        double active = (1 - valtrans1[i, h]) * MyDoubleParse(dd2.Rows[0][0].ToString());
                                        string ULx = "Y";

                                        double req2 = Fpure * MyDoubleParse(dd.Rows[0][2].ToString());
                                        if (req2 > Pact)
                                        {
                                            req2 = Pact;
                                        }
                                        double eco2 = Fpure * MyDoubleParse(dd.Rows[0][0].ToString());
                                        if (eco2 > Pact)
                                        {
                                            eco2 = Pact;
                                        }
                                        string contr2 = dd.Rows[0][1].ToString();
                                        double active2 = MyDoubleParse(dd2.Rows[0][0].ToString());

                                        ddrclared[i, h] += MyDoubleParse(dd88.Rows[0]["dispachablecapacity"].ToString());
                                        printddrclared[i, ddd, h] += MyDoubleParse(dd88.Rows[0]["dispachablecapacity"].ToString());
                                        if (ddrclared[i, h] > Pact)
                                        {
                                            ddrclared[i, h] -= MyDoubleParse(dd88.Rows[0]["dispachablecapacity"].ToString());
                                            ddrclared[i, h] += Pact;
                                            printddrclared[i, ddd, h] -= MyDoubleParse(dd88.Rows[0]["dispachablecapacity"].ToString());
                                            printddrclared[i, ddd, h] += Pact;
                                        }

                                        if (contr.Contains("UL") && (contr.Contains("UL3") == false) && active < (1.05 * req) && (active > 0))
                                        {
                                            printout4[i, ddd, h] += active;
                                            out4[i, h] += active;
                                            ULx = "N";
                                        }
                                        else if ((contr.Contains("UL3")) && active < (1.05 * req) && (active > 0))
                                        {
                                            printout3[i, ddd, h] = (req);
                                            out3[i, h] += (req);
                                            type = "Required";

                                            printdaramadOUT3[i, ddd, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                            daramadOUT3[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                        }
                                        else if (contr.Contains("UL") && active > (1.05 * req) && (active > 0))
                                        {
                                            if (eco > 0)
                                            {
                                                printout3[i, ddd, h] += req;
                                                out3[i, h] += req;
                                                type = "Required";
                                                printdaramadOUT3[i, ddd, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                daramadOUT3[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);


                                                printout5[i, ddd, h] += active - req;
                                                out5[i, h] += active - req;
                                                type = "p";
                                                printdaramadOUT5[i, ddd, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                                daramadOUT5[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);


                                                type = "Required";
                                                printdaramadOUT5[i, ddd, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                daramadOUT5[i, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                            }
                                            else
                                            {
                                                printout5[i, ddd, h] += active;
                                                out5[i, h] += active;
                                                type = "p";
                                                printdaramadOUT5[i, ddd, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                                daramadOUT5[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);

                                            }
                                        }
                                        if ((staterun == "Economic") || (staterun == "Maximum"))
                                        {
                                            if ((Math.Max(req, eco) >= active) && (active > 0) && (ULx != "N"))
                                            {
                                                printout3[i, ddd, h] = active;
                                                out3[i, h] += active;
                                                type = "p";
                                                printdaramadOUT3[i, ddd, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                                daramadOUT3[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                               
                                            }

                                            if ((active > Math.Max(req, eco)) && (contr.Contains("N")) && (ULx != "N"))
                                            {
                                                printout5[i, ddd, h] = (active - Math.Max(req, eco));
                                                out5[i, h] += (active - Math.Max(req, eco));
                                                {
                                                    if (req < eco)
                                                    {
                                                        type = "p";
                                                        printdaramadOUT5[i, ddd, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                                        daramadOUT5[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                                        type = "Economic";
                                                        printdaramadOUT5[i, ddd, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                        daramadOUT5[i, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                    }
                                                    else
                                                    {
                                                        type = "p";
                                                        printdaramadOUT5[i, ddd, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                                        daramadOUT5[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                                        type = "Required";
                                                        printdaramadOUT5[i, ddd, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                        daramadOUT5[i, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                    }
                                                }

                                                printout3[i, ddd, h] += Math.Max(req, eco);
                                                out3[i, h] += Math.Max(req, eco);
                                                if (req < eco)
                                                {
                                                    type = "Economic";
                                                    printdaramadOUT3[i, ddd, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), eco, date);
                                                    daramadOUT3[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(),eco, date);
                                                }
                                                else
                                                {
                                                    type = "Required";
                                                    printdaramadOUT3[i, ddd, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                    daramadOUT3[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if ((req >= active) && (active > 0) && (ULx != "N"))
                                            {
                                                printout3[i, ddd, h] = active;
                                                out3[i, h] += active;
                                                type = "p";
                                                printdaramadOUT3[i, ddd, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                                daramadOUT3[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                            }

                                            if ((active > req) && (contr.Contains("N")) && (ULx != "N"))
                                            {
                                                printout5[i, ddd, h] += (active - req);
                                                out5[i, h] += (active - req);
                                                {
                                                    type = "p";
                                                    printdaramadOUT5[i, ddd, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                                    daramadOUT5[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);

                                                    type = "Required";
                                                    printdaramadOUT5[i, ddd, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                    daramadOUT5[i, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                }

                                                printout3[i, ddd, h] = req;
                                                out3[i, h] += req;
                                                {
                                                    type = "Required";
                                                    printdaramadOUT3[i, ddd, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                    daramadOUT3[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                }

                                            }
                                        }
                                        if ((staterun == "Economic") || (staterun == "Maximum"))
                                        {
                                            if ((Math.Max(req, eco) > active) && (ULx != "N"))
                                            {
                                                printout6[i, ddd, h] = (Math.Max(req, eco) - active);
                                                out6[i, h] += (Math.Max(req, eco) - active);
                                                if (req < eco)
                                                {
                                                    type = "p";
                                                    printdaramadOUT6[i, ddd, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                                    daramadOUT6[i, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                                    daramadOUT6[i, h] += Avc_interval * active2 + valhub1[i, h] * active2;
                                                    printdaramadOUT6[i, ddd, h] += Avc_interval * active2 + valhub1[i, h] * active2;
                                                    type = "Economic";
                                                    printdaramadOUT6[i, ddd, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), eco, date);
                                                    daramadOUT6[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(),eco, date);
                                                    daramadOUT6[i, h] -= Avc_interval * eco2 + valhub1[i, h] * eco2;
                                                    printdaramadOUT6[i, ddd, h] -= Avc_interval * eco2 + valhub1[i, h] * eco2;
                                                }
                                                else
                                                {
                                                    type = "p";
                                                    printdaramadOUT6[i, ddd, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                                    daramadOUT6[i, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                                    daramadOUT6[i, h] += Avc_interval * active2 + valhub1[i, h] * active2;
                                                    printdaramadOUT6[i, ddd, h] += Avc_interval * active2 + valhub1[i, h] * active2;

                                                    type = "Required";
                                                    printdaramadOUT6[i, ddd, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                    daramadOUT6[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                    daramadOUT6[i, h] -= Avc_interval * req2 + valhub1[i, h] * req2;
                                                    printdaramadOUT6[i, ddd, h] -= Avc_interval * req2 + valhub1[i, h] * req2;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if ((req > active) && (ULx != "N"))
                                            {
                                                printout6[i, ddd, h] += (req - active);
                                                out6[i, h] += (req - active);
                                                type = "p";
                                                printdaramadOUT6[i, ddd, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                                daramadOUT6[i, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                                daramadOUT6[i, h] += Avc_interval * active2 + valhub1[i, h] * active2;
                                                printdaramadOUT6[i, ddd, h] += Avc_interval * active2 + valhub1[i, h] * active2;

                                                type = "Required";
                                                printdaramadOUT6[i, ddd, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                daramadOUT6[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                                daramadOUT6[i, h] -= Avc_interval * req2 + valhub1[i, h] * req2;
                                                printdaramadOUT6[i, ddd, h] -= Avc_interval * req2 + valhub1[i, h] * req2;

                                            }
                                        }

                                    }
                                    else
                                    {
                                        enter = false;
                                        if ((dd88.Rows.Count == 0) && (xbreak == 0))
                                        {
                                            MessageBox.Show("Please import Data" + date + "M002");
                                            xbreak = 1;
                                        }
                                        if ((dd.Rows.Count == 0) && (xbreak == 0))
                                        {
                                            MessageBox.Show("Please import Data" + date + "M005");
                                            xbreak = 1;
                                        }
                                        if ((dd2.Rows.Count == 0) && (xbreak == 0))
                                        {
                                            MessageBox.Show("Please import Data" + date + "Metering");
                                            xbreak = 1;
                                        }
                                        wrong = 1;
                                        break;
                                    }

                                }

                                printout8[i, ddd, h] = (printout4[i, ddd, h] * 156600);
                                out8[i, h] = (out4[i, h] * 156600) - out4[i, h] * valhub1[i, h];
                                
                                daramadOUT3[i, h] -= out3[i, h] * valhub1[i, h];
                                daramadOUT5[i, h] -= out5[i, h] * valhub1[i, h];

                                printdaramadOUT3[i, ddd, h] -= out3[i, h] * valhub1[i, h];
                                printdaramadOUT5[i, ddd, h] -= out5[i, h] * valhub1[i, h];


                                daramadOUT7[i, h] = daramadOUT5[i, h] + out8[i, h] + daramadOUT3[i, h];
                                daramadOUT8[i, h] = daramadOUT6[i, h] ;

                                printdaramadOUT7[i, ddd, h] = printdaramadOUT5[i, ddd, h] + printout8[i, ddd, h] + printdaramadOUT3[i, ddd, h];
                                printdaramadOUT8[i, ddd, h] = printdaramadOUT6[i, ddd, h] - ((Avc_interval + valhub1[i, h]) * printout6[i, ddd, h]);

                                if (daramadOUT8[i, h] < 0)
                                {
                                    printdaramadOUT8[i, ddd, h] = 0;
                                    daramadOUT8[i, h] = 0;
                                }
                                printdaramadOUT9[i, ddd, h] = printdaramadOUT7[i, ddd, h] + printdaramadOUT8[i, ddd, h];
                                daramadOUT9[i, h] = daramadOUT7[i, h] + daramadOUT8[i, h];
                            }
                        }

                        i++;
                    }
                }
                /////////////////////////FINANCIAIREPORT-PLANT//////////////////////////////
                FRPlantBenefitTB.Text = "";
                FRPlantBidPayTb.Text = "";
                FRPlantBidPowerTb.Text = "";
                FRPlantEnergyPayTb.Text = "";
                FRPlantIncomeTb.Text = "";
                FRPlantIncPayTb.Text = "";
                FRPlantDecPowerTb.Text = "";
                FRPlantIncPowerTb.Text = "";
                FRPlantTotalPowerTb.Text = "";
                FRPlantULPayTb.Text = "";
                FRPlantULPowerTb.Text = "";
                FRPlantDecPayTb.Text = "";
                FRPlantCostTb.Text = "";
                FRPlantCapPayTb.Text = "";
                FRPlantAvaCapTb.Text = "";
                //////////////////////////////////////////////////////////
              
                    
                //else
                    //MessageBox.Show("InSufficient data!!");
            }

            if (enter && btnmarketbillprint.Visible==false)
            {
                fillnribill();
                //MessageBox.Show("Bill Compare With Information Software");
                if(rdmonthly.Checked==true && rdbillplant.Checked==false)
                MessageBox.Show("Complete Successfully.");
            }
        }
        public void filldailyvalue()
        {
            btnmarketbillprint.Visible =false;
            DataTable d = Utilities.GetTable("select ppid from powerplant where ppid='" + PPID + "'");
            
            DataTable f = Utilities.GetTable("select * from billitem");
            string[] item = new string[f.Columns.Count];
            int nn = 0;
            for (int y = 0; y < f.Columns.Count; y++)
            {
                if (f.Rows[0][y].ToString().Trim() == "1" || f.Rows[0][y].ToString().Trim() == "True")
                {

                    item[nn] = f.Columns[y].ToString();
                    nn++;
                }
            }

            string date = faDatePickerbill.Text.Trim();
            DailyBillPlant = new string[d.Rows.Count, 24, nn];
            DailyBillSum = new string[d.Rows.Count, nn];
            DailyBillTotal = new string[d.Rows.Count, nn];
            MonthlyBillDate = new string[d.Rows.Count, nn];
            MonthlyBillPlant = new string[d.Rows.Count, 24, nn];
            MonthlyBillTotal = new string[d.Rows.Count, nn];
            //s1
            out1 = new double[d.Rows.Count, 24];
            out13 = new double[d.Rows.Count, 24];
            out14 = new double[d.Rows.Count, 24];
            //s2
            out2 = new double[d.Rows.Count, 24];
            //s4
            out3 = new double[d.Rows.Count, 24];
            //s5
            out4 = new double[d.Rows.Count, 24];
            //s6
            out5 = new double[d.Rows.Count, 24];
            //s11
            out6 = new double[d.Rows.Count, 24];
            //s8
            out8 = new double[d.Rows.Count, 24];
            //s7 
            daramadOUT3 = new double[d.Rows.Count, 24];
            //s9
            daramadOUT5 = new double[d.Rows.Count, 24];
            //s12
            daramadOUT6 = new double[d.Rows.Count, 24];
            //s10
            daramadOUT7 = new double[d.Rows.Count, 24];
            //s12
            daramadOUT8 = new double[d.Rows.Count, 24];
            //s15
            daramadOUT9 = new double[d.Rows.Count, 24];

            daramadOUT10 = new double[d.Rows.Count, 24];

            ddrclared=new double[d.Rows.Count, 24];


            double[,] valhub1 = new double[d.Rows.Count, 24];
            double[,] valtrans1 = new double[d.Rows.Count, 24];

            double[, ,] valhub = new double[d.Rows.Count, 12, 3];
            double[, ,] valtrans = new double[d.Rows.Count, 12, 3];

            double Fpure = 0.9874299363235;
            //////////////////////////////////////////////////////////////////////////
            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + date + "'order by BaseID desc");
            string staterun = "";
            string Marketrule = "";
            string smaxdate = "";
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }
            DataTable basetable = Utilities.GetTable("select ProposalDay from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetable.Rows.Count > 0)
            {
                Marketrule = basetable.Rows[0][0].ToString();
            }
            
            //////////////////////////////////////////////////////////////////////////
            DataTable basetabledata = Utilities.GetTable("select ProposalHour from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetabledata.Rows.Count > 0)
            {
                staterun = basetabledata.Rows[0][0].ToString();
            }
            
            ////////////////////////////////////////////////////////////////////////// 
            DataTable df = Utilities.GetTable("select ppid from powerplant");
            Avc_interval = Avc_date(df.Rows[0][0].ToString(), FRPlantCal.Text);
            
            bool enter = true;
            int i = 0;
            foreach (DataRow n in d.Rows)
            {
                if (n[0].ToString().Trim() == PPID) tindex = i;


                ///////////////////////////////sum//////////////////////////////////////////////////////////////

                for (int j = 0; j < nn; j++)
                {
                    DataTable v = Utilities.GetTable("select * from dbo.DailyBillSum where ppid='" + n[0].ToString().Trim() + "'and Date='" + date+ "'");
                    DataTable v2 = Utilities.GetTable("select * from dbo.MonthlyBillDate where ppid='" + n[0].ToString().Trim() + "'and Date='" + date + "'");


                    if (v.Rows.Count > 0 && rddaily.Checked)
                    {
                        string x = v.Rows[0][item[j]].ToString();
                        int index = int.Parse(item[j].Replace("s", "").Trim());
                        DailyBillSum[i, j] = x + "-" + index;
                    }
                    if (v2.Rows.Count > 0 && rdmonthly.Checked)
                    {
                        string x = v2.Rows[0][item[j]].ToString();
                        int index = int.Parse(item[j].Replace("s", "").Trim());
                        MonthlyBillDate[i, j] = x + "-" + index;
                    }

                }

                ////////////////////////////////////////////////////////////////////////////////////////////////

                /////////////////////////////////////////total////////////////////////////////////////////
                DataTable d2 = Utilities.GetTable("select * from dbo.DailyBillTotal where Date='" + date + "'");
                foreach (DataRow v in d2.Rows)
                {
                    if (rddaily.Checked)
                    {
                        for (int j = 0; j < nn; j++)
                        {
                            string x = v[3].ToString();
                            string e8 = v[0].ToString().Trim();
                            if (e8.Contains("S0")) e8 = e8.Replace("S0", "").Trim();
                            int index = int.Parse(e8.Replace("S", "").Trim());
                            int index2 = int.Parse(item[j].Replace("s", "").Trim());
                            if (index == index2)
                            {
                                DailyBillTotal[i, j] = x + "-" + index;
                                break;
                            }
                        }
                    }
                }

                DataTable d3 = Utilities.GetTable("select * from dbo.MonthlyBillTotal where Month='" + date.Substring(0, 7).Trim() + "'and ppid='" + n[0].ToString().Trim() + "'");
                foreach (DataRow v in d3.Rows)
                {
                    if (rdmonthly.Checked)
                    {
                        for (int j = 0; j < nn; j++)
                        {
                            string x = v[3].ToString();
                            string e8 = v[0].ToString().Trim();
                            if (e8.Contains("S0")) e8 = e8.Replace("S0", "").Trim();
                            int index = int.Parse(e8.Replace("S", "").Trim());
                            int index2 = int.Parse(item[j].Replace("s", "").Trim());
                            if (index == index2)
                            {
                                MonthlyBillTotal[i, j] = x + "-" + index;
                                break;
                            }
                        }
                    }

                }

                //////////////////////////////////////////////////////////////////////////////////////////

                ////////////////////////////////////plant-hourly/////////////////////////////////////////////////

                for (int h = 1; h <= 24; h++)
                {
                    for (int j = 0; j < nn; j++)
                    {
                        DataTable v = Utilities.GetTable("select * from dbo.DailyBillPlant where ppid='" + n[0].ToString().Trim() + "'and Date='" + date+ "' and Hour='" + h + "'");
                        DataTable v2 = Utilities.GetTable("select * from dbo.MonthlyBillPlant where ppid='" + n[0].ToString().Trim() + "'and Date='" + date + "'and Hour='" + h + "'");

                        if (v.Rows.Count > 0 && rddaily.Checked)
                        {
                            string x = v.Rows[0][item[j]].ToString();
                            int index = int.Parse(item[j].Replace("s", "").Trim());
                            DailyBillPlant[i, h - 1, j] = x + "-" + index;
                        }
                        if (v2.Rows.Count > 0 && rdmonthly.Checked)
                        {
                            string x = v2.Rows[0][item[j]].ToString();
                            int index = int.Parse(item[j].Replace("s", "").Trim());
                            MonthlyBillPlant[i, h - 1, j] = x + "-" + index;
                        }
                    }
                }
                int xbreak = 0;
                //////////////////////////////////////////general data //////////////////////////////////////////////////////
                for (int h = 0; h < 24; h++)
                {

                    ///////////////////////////////////////////*NEW////////////////////////////////////////////
                    /////////////////////////////////hub-trans//////////////////////////////////////////////////

                    string refer = "";
                    string trans = "";
                    DataTable basetabledata2 = Utilities.GetTable("select Reference,transmission from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                    if (basetabledata2.Rows.Count > 0)
                    {
                        refer = basetabledata2.Rows[0][0].ToString();
                        trans = basetabledata2.Rows[0][1].ToString();
                    }
                    string year2 = date.Substring(0, 4).Trim();
                    DataTable xx1 = null;
                    DataTable xx2 = null;
                    if (refer != "Normal" && refer != "")
                    {
                        xx1 = Utilities.GetTable("select * from yearhub where year='" + year2 + "'and  ppid='" + n[0].ToString().Trim() + "'");
                    }
                    if (trans != "Normal" && trans != "")
                    {
                        xx2 = Utilities.GetTable("select * from yeartrans where year='" + year2 + "' and ppid='" + n[0].ToString().Trim() + "'");
                    }
                    ///////////////////////                          



                    string month = date.Substring(5, 2).Trim();
                    if (MyDoubleParse(month) < 10)
                    {
                        month = MyDoubleParse(month).ToString();
                    }


                    for (int u = 0; u < 12; u++)
                    {
                        if (xx1 != null && xx1.Rows.Count > 0)
                        {
                            valhub[i, u, 0] = MyDoubleParse(xx1.Rows[0]["M" + (u + 1) + "Low"].ToString());
                            valhub[i, u, 1] = MyDoubleParse(xx1.Rows[0]["M" + (u + 1) + "Mid"].ToString());
                            valhub[i, u, 2] = MyDoubleParse(xx1.Rows[0]["M" + (u + 1) + "Peak"].ToString());
                        }
                        if (xx2 != null && xx2.Rows.Count > 0)
                        {
                            valtrans[i, u, 0] = MyDoubleParse(xx2.Rows[0]["M" + (u + 1) + "Low"].ToString());
                            valtrans[i, u, 1] = MyDoubleParse(xx2.Rows[0]["M" + (u + 1) + "Mid"].ToString());
                            valtrans[i, u, 2] = MyDoubleParse(xx2.Rows[0]["M" + (u + 1) + "Peak"].ToString());
                        }
                    }


                    DataTable zx = Utilities.GetTable("select H" + ((h + 1).ToString()) + " from dbo.bourse where fromdate<='" + date + "' and todate>='" + date + "' order by fromdate,id desc");
                    if (zx.Rows.Count == 0)
                    {
                        zx = Utilities.GetTable("select H" + ((h + 1).ToString()) + " from dbo.bourse where fromdate<='" + date + "'");
                    }
                    string c = "";
                    if (zx.Rows.Count > 0)
                    {
                        c = zx.Rows[0][0].ToString().Trim();
                        if (c == "Medium")
                        {
                            valhub1[i, h] = 1000 * valhub[i, int.Parse(month) - 1, 1];
                            valtrans1[i, h] = 0.01 * valtrans[i, int.Parse(month) - 1, 1];
                        }
                        else if (c == "Low")
                        {
                            valhub1[i, h] = 1000 * valhub[i, int.Parse(month) - 1, 0];
                            valtrans1[i, h] = 0.01 * valtrans[i, int.Parse(month) - 1, 0];
                        }
                        else if (c == "peak")
                        {
                            valhub1[i, h] = 1000 * valhub[i, int.Parse(month) - 1, 2];
                            valtrans1[i, h] = 0.01*valtrans[i, int.Parse(month) - 1, 2];
                        }
                        //valhub1[i, h] = 0;
                        //valtrans1[i, h] = 0;

                    }

                    ////////////////////////////////////*****new****////////////////////////////////////////

                    double avc = 0;
                    DataTable r1 = Utilities.GetTable("select sum(Required) from dbo.DetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                    if (Marketrule == "Fuel Limited")
                    {
                        r1 = Utilities.GetTable("select sum(Required) from dbo.BaDetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                    }
                    DataTable r3 = Utilities.GetTable("select sum(Required) from dbo.DetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                    if (Marketrule == "Fuel Limited")
                    {
                        r3 = Utilities.GetTable("select sum(Required) from dbo.BaDetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                    }
                    DataTable r4 = Utilities.GetTable("select sum(economic) from dbo.DetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                    if (Marketrule == "Fuel Limited")
                    {
                        r4 = Utilities.GetTable("select sum(economic) from dbo.BaDetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                    }

                    if ((staterun == "Economic") || (staterun == "Maximum"))
                    {
                        //r1 = utilities.GetTable("select sum(economic) from dbo.DetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                        //if (Marketrule == "Fuel Limited")
                        //{
                        //    r1 = utilities.GetTable("select sum(economic) from dbo.BaDetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'");
                        //}
                    }

                    DataTable r2 = Utilities.GetTable("select sum(p) from dbo.DetailFRM009 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h) + "'");

                   // if ((staterun == "Maximum"))
                    {
                        //foreach (DataRow n3 in r3.Rows)
                        //{
                        //    out13[i, h] += MyDoubleParse(n3[0].ToString());
                        //}
                        //foreach (DataRow n4 in r4.Rows)
                        //{
                        //    out14[i, h] += MyDoubleParse(n4[0].ToString());
                        //}
                        //out1[i, h] = Math.Max(out13[i, h], out14[i, h]);
                    }
                    //else
                    {
                        foreach (DataRow n1 in r1.Rows)
                        {
                            out1[i, h] += Fpure * (1 - valtrans1[i, h]) * MyDoubleParse(n1[0].ToString());
                        }
                    }
                    foreach (DataRow n2 in r2.Rows)
                    {
                        out2[i, h] += (1 - valtrans1[i, h]) * MyDoubleParse(n2[0].ToString());

                    }

                    DataTable p = Utilities.GetTable("select unitcode,packagetype,avc from unitsdatamain where ppid='" + n[0].ToString().Trim() + "'");
                    foreach (DataRow b in p.Rows)
                    {
                        string type = "";
                        avc = MyDoubleParse(b[2].ToString());
                        string FuelType = fuel(n[0].ToString().Trim(), date);
                        Avc_interval = Avc_date(n[0].ToString().Trim(), date);
                        string m005 = detectblockm005(b[0].ToString().Trim(), b[1].ToString(), n[0].ToString().Trim());
                        string m002 = detectblockm002(b[0].ToString().Trim(), b[1].ToString(), n[0].ToString().Trim());
                        double Pact = 0;
                        DataTable dd88 = Utilities.GetTable("select * from dbo.DetailFRM002 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'and block='" + m002 + "'and estimated='0'");
                        DataTable dd = Utilities.GetTable("select economic,Contribution,required from dbo.DetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'and block='" + m005 + "'");

                        if (Marketrule == "Fuel Limited")
                        {
                            dd = Utilities.GetTable("select economic,Contribution,required from dbo.BaDetailFRM005 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h + 1) + "'and block='" + m005 + "'");
                        }
                        
                        DataTable PactTable = Utilities.GetTable("select H" + ((h + 1).ToString()) + "  from dbo.Pactual where PPID='" + n[0].ToString().Trim() + "' AND Unit='" + m002 + "'AND date='" + date + "'");
                        if (PactTable == null)
                        {
                            Pact = MyDoubleParse(dd88.Rows[0]["dispachablecapacity"].ToString());
                        }
                        else
                        {
                            if (PactTable.Rows.Count > 0)
                            {
                                Pact = MyDoubleParse(PactTable.Rows[0][0].ToString());
                            }
                            Pact = MyDoubleParse(dd88.Rows[0]["dispachablecapacity"].ToString());
                        }

                        DataTable dd2 = Utilities.GetTable("select p from dbo.DetailFRM009 where TargetMarketDate='" + date + "'and PPID='" + n[0].ToString().Trim() + "' and Hour='" + (h) + "'and block='" + b[0].ToString() + "'");
                        if (dd.Rows.Count > 0 && dd2.Rows.Count > 0 && dd88.Rows.Count > 0)
                        {
                            

                            double req = Fpure * (1 - valtrans1[i, h]) * MyDoubleParse(dd.Rows[0][2].ToString());
                            if (req > Pact)
                            {
                                req = Pact;
                            }
                            double eco = Fpure * (1 - valtrans1[i, h]) * MyDoubleParse(dd.Rows[0][0].ToString());
                            if (eco > Pact)
                            {
                                eco = Pact;
                            }
                            string contr = dd.Rows[0][1].ToString();
                            double active = (1 - valtrans1[i, h]) * MyDoubleParse(dd2.Rows[0][0].ToString());
                            string ULx = "Y";

                            double req2 = Fpure * MyDoubleParse(dd.Rows[0][2].ToString());
                            if (req2 > Pact)
                            {
                                req2 = Pact;
                            }
                            double eco2 = Fpure * MyDoubleParse(dd.Rows[0][0].ToString());
                            if (eco2 > Pact)
                            {
                                eco2 = Pact;
                            }
                            string contr2 = dd.Rows[0][1].ToString();
                            double active2 = MyDoubleParse(dd2.Rows[0][0].ToString());

                            ddrclared[i, h] += MyDoubleParse(dd88.Rows[0]["dispachablecapacity"].ToString());
                            if (ddrclared[i, h] > Pact)
                            {
                                ddrclared[i, h] -= MyDoubleParse(dd88.Rows[0]["dispachablecapacity"].ToString());
                                ddrclared[i, h] += Pact;
                            }

                            if (contr.Contains("UL") && (contr.Contains("UL3") == false) && active < (1.05 * req) && (active > 0))
                            {
                                out4[i, h] += active;
                                ULx = "N";

                            }
                            else if ((contr.Contains("UL3")) && active < (1.05 * req) && (active > 0))
                            {
                                out3[i, h] += (req);
                                type = "Required";
                                  daramadOUT3[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                            }
                            else if (contr.Contains("UL") && active > (1.05 * req) && (active > 0))
                            {
                                if (eco > 0)
                                {
                                    out3[i, h] += req;
                                    type = "Required";
                                    daramadOUT3[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                    out5[i, h] += active - req;
                                    type = "p";
                                    daramadOUT5[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                    type = "Required";
                                    daramadOUT5[i, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                }
                                else
                                {
                                    out5[i, h] += active;
                                    type = "p";
                                    daramadOUT5[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                }
                            }

                            if ((Math.Max(req, eco) >= active) && (active > 0) && (ULx != "N"))
                            {
                                out3[i, h] += active;
                                type = "p";
                                daramadOUT3[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                            }

                            if ((active > Math.Max(req, eco)) && (contr.Contains("N")) && (ULx != "N"))
                            {
                                out5[i, h] += (active - Math.Max(req, eco));
                                if (req < eco)
                                {
                                    type = "p";
                                    daramadOUT5[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                    type = "Economic";
                                    daramadOUT5[i, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), eco, date);
                                }
                                else
                                {
                                    type = "p";
                                    daramadOUT5[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                    type = "Required";
                                    daramadOUT5[i, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                }

                                out3[i, h] += (Math.Max(req, eco));
                                if (req < eco)
                                {
                                    type = "Economic";
                                    daramadOUT3[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), eco, date);
                                }
                                else
                                {
                                    type = "Required";
                                    daramadOUT3[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                }

                            }
                            if ((staterun == "Economic") || (staterun == "Maximum"))
                            {
                                if ((Math.Max(req, eco) > active) && (ULx != "N"))
                                {
                                    out6[i, h] += (Math.Max(req, eco) - active);
                                    if (req < eco)
                                    {
                                        type = "p";
                                        daramadOUT6[i, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                        daramadOUT6[i, h] += Avc_interval * active2 + valhub1[i, h] * active2;
                                        type = "Economic";
                                        daramadOUT6[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), eco, date);
                                        daramadOUT6[i, h] -= Avc_interval * req2 + valhub1[i, h] * req2;
                                    }
                                    else
                                    {
                                        type = "p";
                                        daramadOUT6[i, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                        daramadOUT6[i, h] += Avc_interval * active2 + valhub1[i, h] * active2;
                                        type = "Required";
                                        daramadOUT6[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                        daramadOUT6[i, h] -= Avc_interval * req2 + valhub1[i, h] * req2;
                                    }
                                }
                            }
                            else
                            {
                                if ((req> active) && (ULx != "N"))
                                {
                                    out6[i, h] += (req- active);
                                    type = "p";
                                    daramadOUT6[i, h] -= daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), active, date);
                                    daramadOUT6[i, h] -= Avc_interval * active2 + valhub1[i, h] * active2;
                                    type = "Required";
                                    daramadOUT6[i, h] += daramadpower(n[0].ToString().Trim(), (h), m002, m005, b[0].ToString().Trim(), req, date);
                                    daramadOUT6[i, h] -= Avc_interval * req2 + valhub1[i, h] * req2;

                                }
                            }

                        }
                        else
                        {
                            enter = false;
                            if ((dd88.Rows.Count == 0) && (xbreak == 0))
                            {
                                MessageBox.Show("Please import Data" + date + "M002");
                                xbreak = 1;
                            }
                            if ((dd.Rows.Count == 0) && (xbreak == 0))
                            {
                                MessageBox.Show("Please import Data" + date + "M005");
                                xbreak = 1;
                            }
                            if ((dd2.Rows.Count == 0) && (xbreak == 0))
                            {
                                MessageBox.Show("Please import Data" + date + "Metering");
                                xbreak = 1;
                            }
                            break;
                        }

                    }
                    out8[i, h] = (out4[i, h] * 156600);
                    daramadOUT3[i, h] -= out3[i, h] * valhub1[i, h];
                    daramadOUT5[i, h] -= out5[i, h] * valhub1[i, h];

                    daramadOUT7[i, h] = daramadOUT5[i, h] + out8[i, h] + daramadOUT3[i, h];
                    daramadOUT8[i, h] = daramadOUT6[i, h];
                   
                    if (daramadOUT8[i, h] < 0)
                    {
                        daramadOUT8[i, h] = 0;
                    }
                    daramadOUT9[i, h] = daramadOUT7[i, h] + daramadOUT8[i, h];
                }
                i++;
            }
            /////////////////////////FINANCIAIREPORT-PLANT//////////////////////////////
            FRPlantBenefitTB.Text = "";
            FRPlantBidPayTb.Text = "";
            FRPlantBidPowerTb.Text = "";
            FRPlantEnergyPayTb.Text = "";
            FRPlantIncomeTb.Text = "";
            FRPlantIncPayTb.Text = "";
            FRPlantDecPowerTb.Text = "";
            FRPlantIncPowerTb.Text = "";
            FRPlantTotalPowerTb.Text = "";
            FRPlantULPayTb.Text = "";
            FRPlantULPowerTb.Text = "";
            FRPlantDecPayTb.Text = "";
            FRPlantCostTb.Text = "";
            FRPlantCapPayTb.Text = "";
            FRPlantAvaCapTb.Text = "";
          
            //////////////////////////////////////////////////////////

            if (enter)
            {
                fillnribill();
                //MessageBox.Show("Bill Compare With Information Software");
                MessageBox.Show("Complete Successfully.");
            }
            else
                MessageBox.Show("InSufficient data!!");
        }
        
        //public bool Findcconetype(string ppid)
        //{
        //    int tr = 0;

        //    DataTable oDataTable = utilities.GetTable("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
        //    for (int i = 0; i < oDataTable.Rows.Count; i++)
        //    {

        //        if (oDataTable.Rows.Count == 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

        //            return true;
        //    }

        //    return false;
        //}
        public string detectblockm002(string unit, string package, string ppid)
        {
            string ptypenum = "0";
            if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";

            if (Findcconetype(ppid.ToString())) ptypenum = "0";

            string temp = unit.ToLower();


            /////////////////////////////////////

            string blockM002 = unit.ToLower();
            if (package.Contains("CC"))
            {
                blockM002 = blockM002.Replace("cc", "c");
                string[] sp = blockM002.Split('c');
                blockM002 = sp[0].Trim() + sp[1].Trim();
                if (blockM002.Contains("gas"))
                    blockM002 = blockM002.Replace("gas", "G");
                else if (blockM002.Contains("steam"))
                    blockM002 = blockM002.Replace("steam", "S");

            }
            else
            {
                if (blockM002.Contains("gas"))
                    blockM002 = blockM002.Replace("gas", "G");
                else if (blockM002.Contains("steam"))
                    blockM002 = blockM002.Replace("steam", "S");
            }
            blockM002 = blockM002.Trim();

            /////////////////////////////////////
            return blockM002;
        }

        public string detectblockm005(string unit,string package,string ppid)
        {
           
         

            string ptypenum = "0";
            if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";
          
            if (Findcconetype(ppid.ToString())) ptypenum = "0";

          


            string temp = unit.ToLower();
            string ppidWithPriority =ppid;
            if (int.Parse(ptypenum) == 1) ppidWithPriority = (MyDoubleParse(ppid) + 1).ToString();

            if (package.Contains("CC"))
            {
                temp = temp.Replace("cc", "c");
                string[] sp = temp.Split('c');
                temp = sp[0].Trim() + sp[1].Trim();
                if (temp.Contains("gas"))
                {
                    temp = temp.Replace("gas", "G");

                }
                else
                {
                    temp = temp.Replace("steam", "S");

                }
                temp = ppidWithPriority + "-" + temp;
            }
            else if (temp.Contains("gas"))
            {
                temp = ppidWithPriority + "-" + temp.Replace("gas", "G");
            }
            else
            {
                temp = ppidWithPriority + "-" + temp.Replace("steam", "S");
            }
            string blockM005 = temp.Trim();

            /////////////////////////////////////
            return blockM005;
        }
        public double daramadpower(string ppid, int h, string block002, string block005, string unit, double PPOWER, string date)
        {
            int i = 0;
            DataTable dx = Utilities.GetTable("select ppid from powerplant");
            double[,] valhub1 = new double[dx.Rows.Count, 24];
            double[,] valtrans1 = new double[dx.Rows.Count, 24];

            double[, ,] valhub = new double[dx.Rows.Count, 12, 3];
            double[, ,] valtrans = new double[dx.Rows.Count, 12, 3];
            //////////////////////////////////////////////////////////////////////////
            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + date + "'order by BaseID desc");
            string staterun = "";
            string Marketrule = "";
            string smaxdate = "";
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }
            DataTable basetable = Utilities.GetTable("select ProposalDay from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetable.Rows.Count > 0)
            {
                Marketrule = basetable.Rows[0][0].ToString();
            }
            //////////////////////////////////////////////////////////////////////////
            DataTable basetabledata = Utilities.GetTable("select ProposalHour from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetabledata.Rows.Count > 0)
            {
                staterun = basetabledata.Rows[0][0].ToString();
            }

            /////////////////////////////////hub-trans/////////////////////////////////////////

            string refer = "";
            string trans = "";
            DataTable basetabledata2 = Utilities.GetTable("select Reference,transmission from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetabledata2.Rows.Count > 0)
            {
                refer = basetabledata2.Rows[0][0].ToString();
                trans = basetabledata2.Rows[0][1].ToString();
            }
            string year = date.Substring(0, 4).Trim();
            DataTable xx1 = null;
            DataTable xx2 = null;
            if (refer != "Normal" && refer != "")
            {
                xx1 = Utilities.GetTable("select * from yearhub where year='" + year + "'");
            }
            if (trans != "Normal" && trans != "")
            {
                xx2 = Utilities.GetTable("select * from yeartrans where year='" + year + "'");
            }

            string month = date.Substring(5, 2).Trim();
            if (MyDoubleParse(month) < 10)
            {
                month = MyDoubleParse(month).ToString();
            }


            for (int u = 0; u < 12; u++)
            {
                if (xx1 != null)
                {
                    valhub[i, u, 0] = MyDoubleParse(xx1.Rows[0]["M" + (u + 1) + "Low"].ToString());
                    valhub[i, u, 1] = MyDoubleParse(xx1.Rows[0]["M" + (u + 1) + "Mid"].ToString());
                    valhub[i, u, 2] = MyDoubleParse(xx1.Rows[0]["M" + (u + 1) + "Peak"].ToString());
                }
                if (xx2 != null)
                {
                    valtrans[i, u, 0] = MyDoubleParse(xx2.Rows[0]["M" + (u + 1) + "Low"].ToString());
                    valtrans[i, u, 1] = MyDoubleParse(xx2.Rows[0]["M" + (u + 1) + "Mid"].ToString());
                    valtrans[i, u, 2] = MyDoubleParse(xx2.Rows[0]["M" + (u + 1) + "Peak"].ToString());
                }
            }


            DataTable zx = Utilities.GetTable("select H" + ((h + 1).ToString()) + " from dbo.bourse where fromdate<='" + date + "' and todate>='" + date + "' order by fromdate,id desc");
            if (zx.Rows.Count == 0)
            {
                zx = Utilities.GetTable("select H" + ((h + 1).ToString()) + " from dbo.bourse where fromdate<='" + date + "'");
            }
            string c = "";
            if (zx.Rows.Count > 0)
            {
                c = zx.Rows[0][0].ToString().Trim();
                if (c == "Medium")
                {
                    valhub1[i, h] = 0.01 * valhub[i, int.Parse(month) - 1, 1];
                    valtrans1[i, h] = 0.01 * valtrans[i, int.Parse(month) - 1, 1];
                }
                else if (c == "Low")
                {
                    valhub1[i, h] = 0.01 * valhub[i, int.Parse(month) - 1, 0];
                    valtrans1[i, h] = 0.01 * valtrans[i, int.Parse(month) - 1, 0];
                }
                else if (c == "peak")
                {
                    valhub1[i, h] = 0.01 * valhub[i, int.Parse(month) - 1, 2];
                    valtrans1[i, h] = 0.01 * valtrans[i, int.Parse(month) - 1, 2];
                }

            }

            int d = h;

            double n005 = PPOWER;
            //if (type == "p")
            //{
            //    DataTable d3 = utilities.GetTable("select p from dbo.DetailFRM009 where TargetMarketDate='" + date + "'and PPID='" + ppid + "' and Hour='" + d + "'and block='" + unit + "'");
            //    n005 = (1 - valtrans1[i, h] - valhub1[i, h]) * MyDoubleParse(d3.Rows[0][0].ToString());
            //}
            //else
            //{
            //    if (type != "p") d = h + 1;
            //    //DataTable d2 = utilities.GetTable("select " + type + " from DetailFRM005 where TargetMarketDate='" + date + "'and ppid='" + ppid + "'and Block='" + block005 + "'and Hour='" +d + "'and Contribution='N'");
            //    DataTable d2 = utilities.GetTable("select " + type + " from DetailFRM005 where TargetMarketDate='" + date + "'and ppid='" + ppid + "'and Block='" + block005 + "'and Hour='" + d + "'");
            //    if (Marketrule == "Fuel Limited")
            //    {
            //        d2 = utilities.GetTable("select " + type + " from BaDetailFRM005 where TargetMarketDate='" + date + "'and ppid='" + ppid + "'and Block='" + block005 + "'and Hour='" + d + "'");
            //    }
            //    try
            //    {
            //        n005 = (1 - valtrans1[i, h] - valhub1[i, h]) * MyDoubleParse(d2.Rows[0][0].ToString());
            //    }
            //    catch
            //    {

            //        n005 = 0;
            //    }
            //}
            // if (type == "p") d = h + 1;
            DataTable d1 = Utilities.GetTable("select * from DetailFRM002 where TargetMarketDate='" + date + "'and ppid='" + ppid + "'and Block='" + block002 + "'and Hour='" + (d + 1) + "'and Estimated=0");
            double[] power1 = new double[10];
            double[] price1 = new double[10];

            int uu = 0;
            for (int b = 8; b < 18; b++)
            {
                int r = 8 + (uu * 2);
                if (d1.Rows.Count > 0)
                {
                    power1[b - 8] = MyDoubleParse(d1.Rows[0][r].ToString());
                    price1[b - 8] = MyDoubleParse(d1.Rows[0][r + 1].ToString());
                }
                uu++;
            }

            double result = 0;
            for (int yy = 0; yy < 10; yy++)
            {
                if (n005 != 0)
                {
                    bool noofound = false;


                    if (!noofound)
                    {
                        double price = 0;
                        double power = 0;


                        double powerpre = 0;
                        double pricepre = 0;

                        int yp = yy;
                        int yl = yy + 1;

                        if (yp != 0) yp = yp - 1;

                        if (yy != 0)
                        {
                            powerpre = power1[yp];
                            pricepre = price1[yp];
                        }


                        price = price1[yy];
                        power = power1[yy];



                        if (n005 > power)
                        {
                            if (power != 0)
                            {

                                if (yy == 9) power = n005 - powerpre;
                                else if (yy != 0) power = power - powerpre;
                                else power = power;
                            }
                            else if (power == 0)
                            {
                                power = (n005 - powerpre);
                                price = pricepre;
                                noofound = true;
                            }


                        }
                        else if (n005 == power)
                        {
                            noofound = true;
                            if (power != 0)
                            {
                                if (yy != 0)
                                    power = power - powerpre;
                                else
                                    power = power;
                            }


                        }
                        else if (power > n005)
                        {
                            noofound = true;
                            power = (n005 - powerpre);

                        }

                        if (power < 0)
                        {
                            power = 0;
                        }


                        //////////////////////////////////////////////////////////////////////////////
                        result += (power * price);
                    }

                }
            }
            return result;
        }

        public double daramad(string ppid, int h, string block002, string block005, string unit, string type,string date)
        {
            int i = 0;
            DataTable dx = Utilities.GetTable("select ppid from powerplant");
            double[,] valhub1 = new double[dx.Rows.Count, 24];
            double[,] valtrans1 = new double[dx.Rows.Count, 24];

            double[, ,] valhub = new double[dx.Rows.Count, 12, 3];
            double[, ,] valtrans = new double[dx.Rows.Count, 12, 3];

            double Fpure = 0.9874299363235;
            //////////////////////////////////////////////////////////////////////////
            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + date + "'order by BaseID desc");
            string staterun = "";
            string Marketrule = "";
            string smaxdate = "";
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }
            DataTable basetable = Utilities.GetTable("select ProposalDay from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetable.Rows.Count > 0)
            {
                Marketrule = basetable.Rows[0][0].ToString();
            }
            //////////////////////////////////////////////////////////////////////////
            DataTable basetabledata = Utilities.GetTable("select ProposalHour from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetabledata.Rows.Count > 0)
            {
                staterun = basetabledata.Rows[0][0].ToString();
            }

            /////////////////////////////////hub-trans/////////////////////////////////////////

            string refer = "";
            string trans = "";
            DataTable basetabledata2 = Utilities.GetTable("select Reference,transmission from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetabledata2.Rows.Count > 0)
            {
                refer = basetabledata2.Rows[0][0].ToString();
                trans = basetabledata2.Rows[0][1].ToString();
            }
            string year = date.Substring(0, 4).Trim();
            DataTable xx1 = null;
            DataTable xx2 = null;
            if (refer != "Normal" && refer != "")
            {
                xx1 = Utilities.GetTable("select * from yearhub where year='" + year + "'and ppid='"+ppid+"'");
            }
            if (trans != "Normal" && trans != "")
            {
                xx2 = Utilities.GetTable("select * from yeartrans where year='" + year + "'and ppid='"+ppid+"'");
            }

            string month = date.Substring(5, 2).Trim();
            if (MyDoubleParse(month) < 10)
            {
                month = MyDoubleParse(month).ToString();
            }


            for (int u = 0; u < 12; u++)
            {
                if (xx1 != null)
                {
                    valhub[i, u, 0] = MyDoubleParse(xx1.Rows[0]["M" + (u + 1) + "Low"].ToString());
                    valhub[i, u, 1] = MyDoubleParse(xx1.Rows[0]["M" + (u + 1) + "Mid"].ToString());
                    valhub[i, u, 2] = MyDoubleParse(xx1.Rows[0]["M" + (u + 1) + "Peak"].ToString());
                }
                if (xx2 != null)
                {
                    valtrans[i, u, 0] = MyDoubleParse(xx2.Rows[0]["M" + (u + 1) + "Low"].ToString());
                    valtrans[i, u, 1] = MyDoubleParse(xx2.Rows[0]["M" + (u + 1) + "Mid"].ToString());
                    valtrans[i, u, 2] = MyDoubleParse(xx2.Rows[0]["M" + (u + 1) + "Peak"].ToString());
                }
            }


            DataTable zx = Utilities.GetTable("select H" + ((h + 1).ToString()) + " from dbo.bourse where fromdate<='" + date + "' and todate>='" + date + "' order by fromdate,id desc");
            if (zx.Rows.Count == 0)
            {
                zx = Utilities.GetTable("select H" + ((h + 1).ToString()) + " from dbo.bourse where fromdate<='" + date + "'");
            }
            string c = "";
            if (zx.Rows.Count > 0)
            {
                c = zx.Rows[0][0].ToString().Trim();
                if (c == "Medium")
                {
                    valhub1[i, h] = 0.01 * valhub[i, int.Parse(month) - 1, 1];
                    valtrans1[i, h] = 0.01 * valtrans[i, int.Parse(month) - 1, 1];
                }
                else if (c == "Low")
                {
                    valhub1[i, h] = 0.01 * valhub[i, int.Parse(month) - 1, 0];
                    valtrans1[i, h] = 0.01 * valtrans[i, int.Parse(month) - 1, 0];
                }
                else if (c == "peak")
                {
                    valhub1[i, h] = 0.01 * valhub[i, int.Parse(month) - 1, 2];
                    valtrans1[i, h] = 0.01 * valtrans[i, int.Parse(month) - 1, 2];
                }

            }
            //////////////////////////////////////////NEW////////////////////////////////////////////
          
            //double[, ,] valhub = new double[1, 12, 3];
            //double[, ,] valtrans = new double[1, 12, 3];        
          

            //string year2 = date.Substring(0, 4).Trim();
            //string month = date.Substring(5, 2).Trim();
            //if (MyDoubleParse(month) < 10)
            //{
            //    month = MyDoubleParse(month).ToString();
            //}
            //for (int j = 0; j < 1; j++)
            //{
            //    DataTable ss = utilities.GetTable("select * from dbo.YearTrans where ppid='" + ppid + "' and year='" + year2 + "'");
            //    DataTable ss2 = utilities.GetTable("select * from dbo.Yearhub where ppid='" + ppid + "' and year='" + year2 + "'");
            //    for (int u = 0; u < 12; u++)
            //    {
            //        valhub[j, u, 0] = MyDoubleParse(ss2.Rows[0]["M" + (u + 1) + "Low"].ToString());
            //        valhub[j, u, 1] = MyDoubleParse(ss2.Rows[0]["M" + (u + 1) + "Mid"].ToString());
            //        valhub[j, u, 2] = MyDoubleParse(ss2.Rows[0]["M" + (u + 1) + "Peak"].ToString());
            //        valtrans[j, u, 0] = MyDoubleParse(ss.Rows[0]["M" + (u + 1) + "Low"].ToString());
            //        valtrans[j, u, 1] = MyDoubleParse(ss.Rows[0]["M" + (u + 1) + "Mid"].ToString());
            //        valtrans[j, u, 2] = MyDoubleParse(ss.Rows[0]["M" + (u + 1) + "Peak"].ToString());
            //    }
            //}

            //////////////////////////////////////////NEW////////////////////////////////////////////
         

            //////////////////////////////////////////////////////////////////////////
            int d=h;          

            double n005 = 0;
            if (type == "p")
            {
                DataTable d3 = Utilities.GetTable("select p from dbo.DetailFRM009 where TargetMarketDate='" + date + "'and PPID='" + ppid + "' and Hour='" + d + "'and block='" + unit + "'");
                n005 = (1 - valtrans1[i, h]) * MyDoubleParse(d3.Rows[0][0].ToString());
            }
            else
            {
                if (type != "p") d = h + 1;
                //DataTable d2 = utilities.GetTable("select " + type + " from DetailFRM005 where TargetMarketDate='" + date + "'and ppid='" + ppid + "'and Block='" + block005 + "'and Hour='" +d + "'and Contribution='N'");
                DataTable d2 = Utilities.GetTable("select " + type + " from DetailFRM005 where TargetMarketDate='" + date + "'and ppid='" + ppid + "'and Block='" + block005 + "'and Hour='" + d + "'");
                if(Marketrule == "Fuel Limited")
                {
                d2 = Utilities.GetTable("select " + type + " from BaDetailFRM005 where TargetMarketDate='" + date + "'and ppid='" + ppid + "'and Block='" + block005 + "'and Hour='" + d + "'");
                }
                    try
                {
                    n005 = Fpure*(1 - valtrans1[i, h]) * MyDoubleParse(d2.Rows[0][0].ToString());
                }
                catch
                {

                    n005 = 0;
                }
            }
            if (type == "p") d = h + 1;
            DataTable d1 = Utilities.GetTable("select * from DetailFRM002 where TargetMarketDate='" + date + "'and ppid='" + ppid + "'and Block='" + block002 + "'and Hour='" + d + "'and Estimated=0");
            double[] power1 = new double[10];
            double[] price1 = new double[10];

            int uu = 0;
            for (int b = 8; b < 18; b++)
            {
                int r = 8 + (uu * 2);
                if (d1.Rows.Count > 0)
                {
                    power1[b - 8] = MyDoubleParse(d1.Rows[0][r].ToString());
                    price1[b - 8] = MyDoubleParse(d1.Rows[0][r + 1].ToString());
                }
                uu++;
            }

            double result = 0;
            for (int yy = 0; yy < 10; yy++)
            {
                if (n005 != 0)
                {
                    bool noofound = false;


                    if (!noofound)
                    {
                        double price = 0;
                        double power = 0;


                        double powerpre = 0;
                        double pricepre = 0;

                        int yp = yy;
                        int yl = yy + 1;

                        if (yp != 0) yp = yp - 1;

                        if (yy != 0)
                        {
                            powerpre = power1[yp];
                            pricepre = price1[yp];
                        }


                        price = price1[yy];
                        power = power1[yy];



                        if (n005 > power)
                        {
                            if (power != 0)
                            {

                                if (yy == 9) power = n005 - powerpre;
                                else if (yy != 0) power = power - powerpre;
                                else power = power;
                            }
                            else if (power == 0)
                            {
                                power = (n005 - powerpre);
                                price = pricepre;
                                noofound = true;
                            }


                        }
                        else if (n005 == power)
                        {
                            noofound = true;
                            if (power != 0)
                            {
                                if (yy != 0)
                                    power = power - powerpre;
                                else
                                    power = power;
                            }


                        }
                        else if (power > n005)
                        {
                            noofound = true;
                            power = (n005 - powerpre);

                        }

                        if (power < 0)
                        {
                            power = 0;
                        }


                        //////////////////////////////////////////////////////////////////////////////
                        result += (power * price);
                    }
                 
                }
            }
            return result;
        }
        public double daramadUL(string ppid, int h, string block002, string block005, string unit, string type,string date)
        {
            //////////////////////////////////////////////////////////////////////////
            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + date + "'order by BaseID desc");
            string staterun = "";
            string Marketrule = "";
            string smaxdate = "";
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }
            DataTable basetable = Utilities.GetTable("select ProposalDay from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetable.Rows.Count > 0)
            {
                Marketrule = basetable.Rows[0][0].ToString();
            }
            //////////////////////////////////////////////////////////////////////////
            DataTable basetabledata = Utilities.GetTable("select ProposalHour from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetabledata.Rows.Count > 0)
            {
                staterun = basetabledata.Rows[0][0].ToString();
            }
            //////////////////////////////////////////////////////////////////////////
            int d =h; 
     
            double n005 = 0;
            if (type == "p")
            {
                DataTable d3 = Utilities.GetTable("select p from dbo.DetailFRM009 where TargetMarketDate='" + date + "'and PPID='" + ppid + "' and Hour='" + d + "'and block='" + unit + "'");
                n005 = MyDoubleParse(d3.Rows[0][0].ToString());
            }
            else
            {
                try
                {
                    if (type != "p") d = h + 1;
                    DataTable d2 = Utilities.GetTable("select " + type + " from DetailFRM005 where TargetMarketDate='" + date + "'and ppid='" + ppid + "'and Block='" + block005 + "'and Hour='" + d + "'and Contribution='UL'");
                    if (Marketrule == "Fuel Limited")
                    {
                        d2 = Utilities.GetTable("select " + type + " from BaDetailFRM005 where TargetMarketDate='" + date + "'and ppid='" + ppid + "'and Block='" + block005 + "'and Hour='" + d + "'and Contribution='UL'");
                    }
                  
                    n005 = MyDoubleParse(d2.Rows[0][0].ToString());
                }

                catch
                {

                    n005 = 0;
                }
            }
            if (type == "p") d = h + 1;
            DataTable d1 = Utilities.GetTable("select * from DetailFRM002 where TargetMarketDate='" + date + "'and ppid='" + ppid + "'and Block='" + block002 + "'and Hour='" + d + "'and Estimated=0");

            double[] power1 = new double[10];
            double[] price1 = new double[10];

            int uu = 0;
            for (int b = 8; b < 18; b++)
            {
                int r = 8 + (uu * 2);
                power1[b - 8] = MyDoubleParse(d1.Rows[0][r].ToString());
                price1[b - 8] = MyDoubleParse(d1.Rows[0][r + 1].ToString());
                uu++;
            }

            double result = 0;
            for (int yy = 0; yy < 10; yy++)
            {
                if (n005 != 0)
                {
                    bool noofound = false;


                    if (!noofound)
                    {
                        double price = 0;
                        double power = 0;


                        double powerpre = 0;
                        double pricepre = 0;

                        int yp = yy;
                        int yl = yy + 1;

                        if (yp != 0) yp = yp - 1;

                        if (yy != 0)
                        {
                            powerpre = power1[yp];
                            pricepre = price1[yp];
                        }


                        price = price1[yy];
                        power = power1[yy];



                        if (n005 > power)
                        {
                            if (power != 0)
                            {

                                if (yy == 9) power = n005 - powerpre;
                                else if (yy != 0) power = power - powerpre;
                                else power = power;
                            }
                            else if (power == 0)
                            {
                                power = (n005 - powerpre);
                                price = pricepre;
                                noofound = true;
                            }


                        }
                        else if (n005 == power)
                        {
                            noofound = true;
                            if (power != 0)
                            {
                                if (yy != 0)
                                    power = power - powerpre;
                                else
                                    power = power;
                            }


                        }
                        else if (power > n005)
                        {
                            noofound = true;
                            power = (n005 - powerpre);

                        }

                        if (power < 0)
                        {
                            power = 0;
                        }


                        //////////////////////////////////////////////////////////////////////////////
                        result += (power * price);
                    }

                }


            }
            return result;
        }

        public string fuel(string ppid, string date)
        {
            string val = "";
            try
            {
                DataTable x = Utilities.GetTable("select * from billfuel where todate>='" + date + "'and fromdate<='" + date + "'and ppid='" + ppid + "'order by todate desc,fromdate desc");
                if (x.Rows.Count > 0 && x.Rows[0][0].ToString() != "")
                {
                    val = (x.Rows[0][4].ToString());
                }
            }
            catch
            {
            }
            return val;
        }

        public double Avc_date(string ppid, string date)
        {
            double val = 240000;
            try
            {
                DataTable x = Utilities.GetTable("select * from billfuel where todate>='" + date + "'and fromdate<='" + date + "'and ppid='" + ppid + "'order by todate desc,fromdate desc");
                if (x.Rows.Count > 0 && x.Rows[0][0].ToString() != "")
                {
                    val = MyDoubleParse(x.Rows[0][3].ToString());
                }
            }
            catch
            {
            }
            return val;
        }

        private void dgbill_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            double Lmin =0.95;
            double Lmax = 1.05;
            //////////////////////////////////////////////////////////////////////////
            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + faDatePickerbill.Text.Trim() + "'order by BaseID desc");
            string smaxdate = "";
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }
            DataTable basetable1 = Utilities.GetTable("select GasHeatValueAverage from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetable1.Rows.Count > 0)
            {
                Lmax = MyDoubleParse(basetable1.Rows[0][0].ToString());
                if (Lmax < 0)
                {
                    Lmax = 1.05;
                }
            }
            DataTable basetable2 = Utilities.GetTable("select GasOilHeatValueAverage from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetable2.Rows.Count > 0)
            {
                Lmin = MyDoubleParse(basetable2.Rows[0][0].ToString());
                if (Lmin < 0)
              {
                  Lmin = 0.95;
              }
            }

            try
            {
             
                txtmarketlist.Text = "";
                string typeday = "";
                if (rddaily.Checked) typeday = "Daily";
                else typeday = "Monthly";

               
                double y = MyDoubleParse(dgbill.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                string c1 = dgbill.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText;
                //////////////////////////////////////////////////////////
                DataTable dd = Utilities.GetTable("select ppid from powerplant");
                int j = dd.Rows.Count;
                int dx = dgbill.Rows.Count - 1;
                int t = e.ColumnIndex;
                int m = e.RowIndex;
                string Blink = "";
                //string hed = dgbill.Columns[e.ColumnIndex].HeaderCell.Value.ToString();
                //for (int t = 2; t < dgbill.Columns.Count; t++)
                //{
                //for (int m = 0; m < dgbill.Rows.Count - 1; m++)
                //{
                this.dgbill.ClearSelection();
                string name = "";

                string hed = dgbill.Columns[t].HeaderText;
                string tool = dgbill.Columns[t].ToolTipText;
                if (rdtotal.Checked) hed = dgbill.Rows[m].Cells[1].Value.ToString();
                //else if (rdplantsum.Checked) hed = dgbill.Columns[t - 1].HeaderText;

                //if (hed == "مبلغ قابل پرداخت - ريال")
                //{

                //if (rdbillplant.Checked)
                //{
                    Blink = "N";
                //    int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                //    double x = daramadOUT9[0, h - 1];

                //    if (((x >= y * (Lmin)) && (x <= y * (Lmax))) )
                //    {

                //    }
                //    else
                //    {
                //        txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "Report OF Plant : " + PPID + "    IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + "   " + typeday + " Report OF Software Is : " + x.ToString();
                //        Blink = "N";
                //    }
                //}

                // }

                //////////////////////////////////////////////////////////


                    if ((hed == "آرایش تولید بازار - مگاوات ساعت") || (hed == "ميزان خالص آرايش توليد بازار -مگاوات ساعت") || (tool == "Market Power"))
                {
                    //s1/

                    ////////////////////////////////////////////

                    if (rdbillplant.Checked)
                    {
                        int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                        double x = out1[0, h - 1];

                        if (((x >= y * (Lmin)) && (x <= y * (Lmax)))  || (Blink != "N"))
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "Report OF Plant :  " + PPID + "    IN    " + faDatePickerbill.Text + "  Is : " + y.ToString() + "  " + typeday + " Report OF Software Is : " + x.ToString();

                        }

                    }
                    else if (rdtotal.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += out1[i, s];
                            }
                        }

                        if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant :  " + PPID + "    IN    " + faDatePickerbill.Text + "  Is :  " + y.ToString() + "  " + typeday + " Report OF Software Is :" + sum.ToString();

                        }

                    }
                    else if (rdplantsum.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += out1[i, s];
                            }
                            double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t].Value.ToString());

                            if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                            {

                            }
                            else
                            {
                                txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "    IN    " + faDatePickerbill.Text + "  Is :  " + y1.ToString() + "  " + typeday + " Report OF Software Is :" + sum.ToString();

                                // dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff : " + (y1 - sum).ToString();
                            }
                        }

                    }


                }
                    else if ((hed == "تولید ناخالص - مگاوات ساعت") || (hed == "ميزان توليد ناخالص-مگاوات ساعت") || (tool == "TotalPower"))
                {


                    // double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());

                    if (rdbillplant.Checked)
                    {
                        int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                        double x = out2[0, h - 1];

                        if (((x >= y * (Lmin)) && (x <= y * (Lmax)))  || (Blink != "N"))
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "  Report OF Plant :  " + PPID + "     IN    " + faDatePickerbill.Text + "  Is : " + y.ToString() + "  " + typeday + "  Report OF Software Is : " + x.ToString();
                            // dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                        }
                    }
                    else if (rdtotal.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += out2[i, s];
                            }
                        }
                        if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant :  " + PPID + "   IN    " + faDatePickerbill.Text + "  Is : " + y.ToString() + "  " + typeday + " Report OF Software Is :" + sum.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                        }

                    }
                    else if (rdplantsum.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += out2[i, s];
                            }
                            double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t].Value.ToString());

                            if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                            {

                            }
                            else
                            {
                                txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "   IN   " + faDatePickerbill.Text + "  Is :  " + y1.ToString() + "  " + typeday + "  Report OF Software Is :" + sum.ToString();

                                // dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                            }
                        }

                    }
                    //}

                }
                else if (hed == "تولید خالص - مگاوات ساعت")
                {

                }
                    else if ((hed == "مقدار پذیرفته شده با قیمت پیشنهادی فروشنده - مگاوات ساعت") || (hed == "ميزان خالص پذيرفته شده با قيمت پيشنهادي فروشنده-مگاوات ساعت") || (tool == "BidPower"))
                {

                    if (rdbillplant.Checked)
                    {
                        int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                        double x = out3[0, h - 1];

                        if (((x >= y * (Lmin)) && (x <= y * (Lmax)))  || (Blink != "N"))
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "  Report OF Plant : " + PPID + "    IN   " + faDatePickerbill.Text + "  Is :  " + y.ToString() + "  " + typeday + " Report OF Software Is : " + x.ToString();
                            // dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                        }

                    }
                    else if (rdtotal.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += out3[i, s];
                            }
                        }
                        if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "   IN   " + faDatePickerbill.Text + "  Is :  " + y.ToString() + "  " + typeday + " Report OF Software Is :" + sum.ToString();
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                        }

                    }
                    else if (rdplantsum.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += out3[i, s];
                            }
                            double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t].Value.ToString());
                            if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                            {

                            }
                            else
                            {
                                txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "  Report OF Plant : " + PPID + "    IN   " + faDatePickerbill.Text + "  Is :  " + y1.ToString() + "  " + typeday + " Report OF Software Is :" + sum.ToString();
                                //dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                //dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                            }
                        }

                    }
                    //}


                }
                    else if ((hed == "مقدار پذیرفته شده با 90% قیمت حداقل همان ساعت - مگاوات ساعت") || (hed == "ميزان خالص پذيرفته شده با نرخ UL-مگاوات ساعت") || (tool == "ULPower"))
                {

                    if (rdbillplant.Checked)
                    {
                        int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                        double x = out4[0, h - 1];

                        if (((x >= y * (Lmin)) && (x <= y * (Lmax)))  || (Blink != "N"))
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "Report OF Plant : " + PPID + "    IN    " + faDatePickerbill.Text + "  Is :  " + y.ToString() + "  " + typeday + "  Report OF Software Is : " + x.ToString();
                            // dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                        }

                    }
                    else if (rdtotal.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += out4[i, s];
                            }
                        }
                        if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "  Report OF Plant :  " + PPID + "   IN   " + faDatePickerbill.Text + "  Is :  " + y.ToString() + "  " + typeday + "  Report OF Software Is :" + sum.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                        }

                    }
                    else if (rdplantsum.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += out4[i, s];
                            }
                            double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t].Value.ToString());
                            if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                            {

                            }
                            else
                            {
                                txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant :  " + PPID + "   IN   " + faDatePickerbill.Text + "  Is :  " + y1.ToString() + "  " + typeday + " Report OF Software Is :" + sum.ToString();
                                //dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                //dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                            }
                        }

                    }
                    //}
                }
                    else if ((hed == "میزان تولید بیش از پیشنهاد بازار - مگاوات ساعت") || (hed == "ميزان خالص توليد بيش از پيشنهاد بازارو به دستور مرکز-مگاوات ساعت") || (tool == "IncrementPower"))
                {

                    if (rdbillplant.Checked)
                    {
                        int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                        double x = out5[0, h - 1];

                        if (((x >= y * (Lmin)) && (x <= y * (Lmax)))  || (Blink != "N"))
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "    IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + "  " + typeday + " Report OF Software Is : " + x.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                        }

                    }
                    else if (rdtotal.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += out5[i, s];
                            }
                        }
                        if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "  Report OF Plant : " + PPID + "    IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + " " + typeday + "  Report OF Software Is :" + sum.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                        }

                    }
                    else if (rdplantsum.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += out5[i, s];
                            }
                            double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t].Value.ToString());
                            if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                            {

                            }
                            else
                            {
                                txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant :" + PPID + "    IN    " + faDatePickerbill.Text + "  Is : " + y1.ToString() + "  " + typeday + " Report OF Software Is :" + sum.ToString();
                                //dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                //dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                            }
                        }

                    }
                    // }

                }
                    else if ((hed == "مبلغ پذیرفته شده با قیمت پیشنهادی فروشنده - ريال") || (hed == "بهاي پرداختي با قيمت پيشنهادي فروشنده-ريال") || (tool == "BidPayment"))
                {

                    if (rdbillplant.Checked)
                    {
                        int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                        double x = daramadOUT3[0, h - 1];

                        if (((x >= y * (Lmin)) && (x <= y * (Lmax)))  || (Blink != "N"))
                        {

                        }
                        else
                        {

                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "   IN   " + faDatePickerbill.Text + "  Is :  " + y.ToString() + "   " + typeday + "  Report OF Software Is : " + x.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                        }

                    }
                    else if (rdtotal.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += daramadOUT3[i, s];
                            }
                        }
                        if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "  Report OF Plant : " + PPID + "   IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + "   " + typeday + "  Report OF Software Is : " + sum.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + " Diff :  " + (y - sum).ToString();
                        }

                    }
                    else if (rdplantsum.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += daramadOUT3[i, s];
                            }
                            double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t].Value.ToString());
                            if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                            {

                            }
                            else
                            {

                                txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "  Report OF Plant :  " + PPID + "    IN   " + faDatePickerbill.Text + "  Is : " + y1.ToString() + "  " + typeday + "  Report OF Software Is : " + sum.ToString();
                                //dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                //dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                            }
                        }

                    }
                    // }
                }
                    else if ((hed == "مبلغ پذیرفته شده با 90% قیمت حداقل همان ساعت - ريال") || (hed == "بهاي پرداختي با نرخ UL-ريال") || (tool == "ULPayment"))
                {

                    if (rdbillplant.Checked)
                    {
                        int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                        double x = out8[0, h - 1];

                        if (((x >= y * (Lmin)) && (x <= y * (Lmax)))  || (Blink != "N"))
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "  Report OF Plant : " + PPID + "    IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + "  " + typeday + " Report OF Software Is : " + x.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                        }

                    }
                    else if (rdtotal.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += out8[i, s];
                            }
                        }
                        if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                        {

                        }
                        else
                        {

                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant :  " + PPID + "   IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + "  " + typeday + " Report OF Software Is : " + sum.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                        }

                    }
                    else if (rdplantsum.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += out8[i, s];
                            }
                            double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t].Value.ToString());
                            if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                            {

                            }
                            else
                            {

                                txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant :  " + PPID + "   IN   " + faDatePickerbill.Text + "  Is : " + y1.ToString() + "  " + typeday + " Report OF Software Is : " + sum.ToString();
                                dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                            }
                        }

                    }
                    // }
                }
                    else if ((hed == "مبلغ تولید بیش از پیشنهاد بازار - ريال") || (hed == "بهاي پرداختي بابت توليد بيش از پيشنهاد بازار-ريال") || (tool == "IncrementPayment"))
                {

                    if (rdbillplant.Checked)
                    {
                        int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                        double x = daramadOUT5[0, h - 1];
                        if (((x >= y * (Lmin)) && (x <= y * (Lmax)))  || (Blink != "N"))
                        {

                        }
                        else
                        {

                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "  Report OF Plant : " + PPID + "    IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + "  " + typeday + "  Report OF Software Is : " + x.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                        }

                    }
                    else if (rdtotal.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += daramadOUT5[i, s];
                            }
                        }
                        if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                        {

                        }
                        else
                        {

                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "  Report OF Plant :" + PPID + "    IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + "  " + typeday + "  Report OF Software Is : " + sum.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                        }

                    }
                    else if (rdplantsum.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += daramadOUT5[i, s];
                            }
                            double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t].Value.ToString());
                            if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                            {

                            }
                            else
                            {
                                txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "  Report OF Plant : " + PPID + "    IN   " + faDatePickerbill.Text + "  Is : " + y1.ToString() + "   " + typeday + " Report OF Software Is : " + sum.ToString();
                                //dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                //dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                            }
                        }

                    }
                    //}
                }
                    else if ((hed == "جمع مبلغ بابت تولید خالص - ريال") || (hed == "جمع بهاي پرداختي بابت توليد خالص-ريال") || (tool == "Pure Energy Payment"))
                {

                    if (rdbillplant.Checked)
                    {
                        int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                        double x = daramadOUT7[0, h - 1];

                        if (((x >= y * (Lmin)) && (x <= y * (Lmax)))  || (Blink != "N"))
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "  Report OF Plant : " + PPID + "   IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + "  " + typeday + " Report OF Software Is : " + x.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                        }

                    }
                    else if (rdtotal.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += daramadOUT7[i, s];
                            }
                        }
                        if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "  Report OF Plant : " + PPID + "    IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + "  " + typeday + " Report OF Software Is : " + sum.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :   " + (y - sum).ToString();
                        }

                    }
                    else if (rdplantsum.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += daramadOUT7[i, s];
                            }
                            double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t].Value.ToString());
                            if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                            {

                            }
                            else
                            {
                                txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "    IN   " + faDatePickerbill.Text + "  Is : " + y1.ToString() + "  " + typeday + " Report OF Software Is : " + sum.ToString();
                                //dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                //dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                            }
                        }

                    }
                    //}
                }
                    else if ((hed == "میزان تولید کمتر از پیشنهاد بازار و به دستور مرکز - مگاوات ساعت") || (hed == "ميزان توليد کمتر از پيشنهاد بازار و به دستور مرکز-مگاوات ساعت") || (tool == "DecreasePower"))
                {

                    if (rdbillplant.Checked)
                    {
                        int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                        double x = out6[0, h - 1];

                        if (((x >= y * (Lmin)) && (x <= y * (Lmax)))  || (Blink != "N"))
                        {

                        }
                        else
                        {

                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "   IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + "  " + typeday + " Report OF Software Is : " + x.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                        }

                    }
                    else if (rdtotal.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += out6[i, s];
                            }
                        }
                        if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "   IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + "  " + typeday + "  Report OF Software Is : " + sum.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                        }

                    }
                    else if (rdplantsum.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += out6[i, s];
                            }
                            double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t].Value.ToString());
                            if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                            {

                            }
                            else
                            {
                                txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "  Report OF Plant : " + PPID + "   IN   " + faDatePickerbill.Text + "  Is : " + y1.ToString() + "  " + typeday + "  Report OF Software Is : " + sum.ToString();
                                //dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                //dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                            }
                        }

                    }
                    //}
                }
                    else if ((hed == "مبلغ خسارت - ريال") || (hed == "بهاي پرداختي بابت سلب فرصت-ريال") || (tool == "DecreasePayment"))
                {


                    if (rdbillplant.Checked)
                    {
                        int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                        double x = daramadOUT8[0, h - 1];

                        if (((x >= y * (Lmin)) && (x <= y * (Lmax)))  || (Blink != "N"))
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + "  Report OF Plant : " + PPID + "   IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + "   " + typeday + "  Report OF Software Is : " + x.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                        }

                    }
                    else if (rdtotal.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += daramadOUT8[i, s];
                            }
                        }
                        if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "    IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + "  " + typeday + "  Report OF Software Is : " + sum.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                        }

                    }
                    else if (rdplantsum.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += daramadOUT8[i, s];
                            }
                            double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t].Value.ToString());
                            if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                            {

                            }
                            else
                            {
                                txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "    IN   " + faDatePickerbill.Text + "  Is : " + y1.ToString() + " " + typeday + "  Report OF Software Is : " + sum.ToString();
                                // dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                // dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                            }
                        }

                    }
                    //}
                }
                //else if (hed == "s13") name = "عدم همکاری با مرکز - مگاوات ساعت";
                //else if (hed == "s14") name = "مبلغ جریمه عدم همکاری - ريال";
                    else if ((hed == "مبلغ قابل پرداخت - ريال") || (hed == "بهاي قابل پرداخت-ريال") || (tool == "EnergyPayment"))
                {


                    if (rdbillplant.Checked)
                    {
                        int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                        double x = daramadOUT9[0, h - 1];

                        if (((x >= y * (Lmin)) && (x <= y * (Lmax)))  || (Blink != "N"))
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "    IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + "  " + typeday + "  Report OF Software Is : " + x.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + x + "  Diff :  " + (y - x).ToString();
                        }
                    }
                    else if (rdtotal.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += daramadOUT9[i, s];
                            }
                        }
                        if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))) )
                        {

                        }
                        else
                        {
                            txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "   IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + " " + typeday + "  Report OF Software Is : " + sum.ToString();
                            //dgbill.Rows[m].Cells[t].Style.ForeColor = Color.Red;
                            //dgbill.Rows[m].Cells[t].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y - sum).ToString();
                        }

                    }
                    else if (rdplantsum.Checked)
                    {
                        double sum = 0;
                        for (int i = 0; i < j; i++)
                        {
                            for (int s = 0; s < 24; s++)
                            {
                                sum += daramadOUT9[i, s];
                            }
                            double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t].Value.ToString());
                            if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                            {

                            }
                            else
                            {
                                txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "   IN   " + faDatePickerbill.Text + "  Is : " + y1.ToString() + "   " + typeday + "  Report OF Software Is : " + sum.ToString();
                                //dgbill.Rows[i].Cells[t - 1].Style.ForeColor = Color.Red;
                                //dgbill.Rows[i].Cells[t - 1].ToolTipText = "OutPut :  " + sum + "  Diff :  " + (y1 - sum).ToString();
                            }
                        }

                    }

                }
                    else  if ((hed == "ميزان آمادگي - مگاوات ساعت") || (tool == "AvailableCapacity"))
                    {
                        //
                       // double y = MyDoubleParse(dgbill.Rows[m].Cells[t].Value.ToString());
                       

                        ////////////////////////////////////////////

                        if (rdbillplant.Checked)
                        {
                            int h = int.Parse(dgbill.Rows[m].Cells[1].Value.ToString());
                            double x = ddrclared[tindex, h - 1];

                            if (((x >= y * (Lmin)) && (x <= y * (Lmax))) || (Blink != "N"))
                            {

                            }
                            else
                            {
                                txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "    IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + "  " + typeday + "  Report OF Software Is : " + x.ToString();
                            }

                        }
                        else if (rdtotal.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += ddrclared[i, s];
                                }
                            }

                            if (((sum >= y * (Lmin)) && (sum <= y * (Lmax))))
                            {

                            }
                            else
                            {
                                txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "   IN   " + faDatePickerbill.Text + "  Is : " + y.ToString() + " " + typeday + "  Report OF Software Is : " + sum.ToString();
                            }

                        }
                        else if (rdplantsum.Checked)
                        {
                            double sum = 0;
                            for (int i = 0; i < j; i++)
                            {
                                for (int s = 0; s < 24; s++)
                                {
                                    sum += ddrclared[i, s];
                                }
                                double y1 = MyDoubleParse(dgbill.Rows[i].Cells[t].Value.ToString());

                                if (((sum >= y1 * (Lmin)) && (sum <= y1 * (Lmax))) || ((sum >= y1 - 10) && (sum <= y1 + 10)))
                                {

                                }
                                else
                                {
                                    txtmarketlist.Text = c1.Trim() + "\r\n\r\n" + typeday + " Report OF Plant : " + PPID + "   IN   " + faDatePickerbill.Text + "  Is : " + y1.ToString() + "   " + typeday + "  Report OF Software Is : " + sum.ToString();
                                }
                            }

                        }



                    }




                //////////////////////////////////////////////////////////////////////////////////////////////
                if (txtmarketlist.Text != "" && rdbillplant.Checked) btnmarketbillprint.Visible = true;
                else btnmarketbillprint.Visible = false;
            }
            catch
            {

            }
        }


        private void btnmarketbillprint_Click(object sender, EventArgs e)
        {

            this.Cursor = Cursors.WaitCursor;
            
            if (rdmonthly.Checked)
            {
                if (rdbillplant.Checked)
                {
                    if (rdbillplant.Checked && rdmonthly.Checked)
                    {
                       fillmonthlyvalue();
                    }
                    btnmarketbillprint.Visible = false;
                  string hed = dgbill.Columns[dgbill.CurrentCell.ColumnIndex].HeaderText;
                  string [] c= fillnriprintmonth();


                   //////////////////////////////lmin/////////////////////////////////////
                  double Lmin = 0.95;
                  double Lmax = 1.05;
                  //////////////////////////////////////////////////////////////////////////
                  DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + faDatePickerbill.Text.Trim() + "'order by BaseID desc");
                  string smaxdate = "";
                  if (dtsmaxdate.Rows.Count > 0)
                  {
                      string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                      int ib = 0;
                      foreach (DataRow m in dtsmaxdate.Rows)
                      {
                          arrbasedata[ib] = m["Date"].ToString();
                          ib++;
                      }
                      smaxdate = buildmaxdate(arrbasedata);
                  }
                  else
                  {
                      dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                      smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
                  }
                  DataTable basetable1 = Utilities.GetTable("select GasHeatValueAverage from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                  if (basetable1.Rows.Count > 0)
                  {
                      Lmax = MyDoubleParse(basetable1.Rows[0][0].ToString());
                      if (Lmax < 0)
                      {
                          Lmax = 1.05;
                      }
                  }
                  DataTable basetable2 = Utilities.GetTable("select GasOilHeatValueAverage from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                  if (basetable2.Rows.Count > 0)
                  {
                      Lmin = MyDoubleParse(basetable2.Rows[0][0].ToString());
                      if (Lmin < 0)
                      {
                          Lmin = 0.95;
                      }
                  }
               ///////////////////////lmin//////////////////////////////////

                  dataGridView1.DataSource = null;
                  int d = c.Length;
                  dataGridView1.RowCount = c.Length;
                  int xc = 0;
                  for (int k = 0; k < d - 1; k++)
                  {
                      string[] sp = c[k].Split(';');
                      if (sp.Length == 4)
                      {
                          //dataGridView1.Rows[k].Cells[0].Value = sp[0];
                          //dataGridView1.Rows[k].Cells[1].Value = sp[1];
                          //dataGridView1.Rows[k].Cells[2].Value = sp[2];
                          //dataGridView1.Rows[k].Cells[3].Value = sp[3];
                          double ddd = MyDoubleParse(sp[3].ToString()) - MyDoubleParse(sp[2].ToString());
                          //if (ddd <= 0) dataGridView1.Rows[k].Cells[4].Value = Math.Abs(ddd);

                          double x = MyDoubleParse(sp[3].ToString());
                          double y = MyDoubleParse(sp[2].ToString());
                          if (((x >= y * (Lmin)) && (x <= y * (Lmax)) ))
                          {
                              //dataGridView1.Rows[k].Cells[0].Value = "";
                              //dataGridView1.Rows[k].Cells[1].Value = "";
                              //dataGridView1.Rows[k].Cells[2].Value = "";
                              //dataGridView1.Rows[k].Cells[3].Value = "";
                              //dataGridView1.Rows[k].Cells[4].Value = "";

                          }
                          else if (ddd <= 0)
                          {
                              dataGridView1.Rows[xc].Cells[0].Value = sp[0];
                              dataGridView1.Rows[xc].Cells[1].Value = sp[1];
                              dataGridView1.Rows[xc].Cells[2].Value = sp[2];
                              dataGridView1.Rows[xc].Cells[3].Value = sp[3];
                              dataGridView1.Rows[xc].Cells[4].Value = Math.Abs(ddd);
                              xc++;
                          }
                      }


                  }


                  /////////////////////////////////////////////
                 DataTable  dt = new DataTable();
                  DataColumn[] dsc = new DataColumn[] { };
                  foreach (DataGridViewColumn c1 in dataGridView1.Columns)
                  {
                      DataColumn dc = new DataColumn();
                      dc.ColumnName = c1.Name.Trim();

                      dt.Columns.Add(dc);
                  }
                  foreach (DataGridViewRow r in dataGridView1.Rows)
                  {
                      DataRow drow = dt.NewRow();
                      foreach (DataGridViewCell cell in r.Cells)
                      {
                          try
                          {
                              drow[cell.OwningColumn.Name] = cell.Value.ToString().Trim();
                          }
                          catch
                          {
                          }
                      }
                      dt.Rows.Add(drow);

                  }

                    ///////////////////////////////////////////////



                  this.Cursor = Cursors.Default;
                  MarketBillPrint v = new MarketBillPrint(dt,hed,PPID);
                  v.ShowDialog();

        
                }

            }

        }

        public string [] fillnriprintmonth()
        {
            string[] split = null;
            try
            {
                txtmarketlist.Text = "";
                string typeday = "";
                if (rddaily.Checked) typeday = "Daily";
                else typeday = "Monthly";
                int tt = new PersianDate(faDatePickerbill.Text).MonthDays;

                string start = faDatePickerbill.Text.Remove(8, 2).ToString() + "01";
                string to = new PersianDate(PersianDateConverter.ToGregorianDateTime(start).AddDays(tt - 1)).ToString("d");
                string todate = to;
               
                //if (rdtotal.Checked) hed = dgbill.Rows[m].Cells[1].Value.ToString();
                ///////////////////////////////////////////////////////////
             
                bool kabise = false;
                int year = new PersianDate(start).Year;
                if (year % 4 == 3)
                {
                    kabise = true;

                }
                else if (new PersianDate(start).Month == 12)
                {
                    string v = start.Substring(8, 2);
                    to = start.Remove(8, 2);
                    to = start.Replace(v, "29");
                    todate = to;
                }

                TimeSpan nnn = PersianDateConverter.ToGregorianDateTime(to) - PersianDateConverter.ToGregorianDateTime(start);
                int day = nnn.Days;


                string hed = dgbill.Columns[dgbill.CurrentCell.ColumnIndex].HeaderText;
                string TOOL = dgbill.Columns[dgbill.CurrentCell.ColumnIndex].ToolTipText;
                //////////////////////////////////////////////////////////
                DataTable dd = Utilities.GetTable("select ppid from powerplant");
                int j = dd.Rows.Count;
                int dx = dgbill.Rows.Count - 1;

                string Blink = "";
                //string hed = dgbill.Columns[e.ColumnIndex].HeaderCell.Value.ToString();
                //for (int t = 2; t < dgbill.Columns.Count; t++)
                //{
                //for (int m = 0; m < dgbill.Rows.Count - 1; m++)
                //{


                Blink = "N";
                string err = "";
                for (int ddd = 0; ddd <= day; ddd++)
                {
                    string date = new PersianDate(PersianDateConverter.ToGregorianDateTime(start).AddDays(ddd)).ToString("d");
                    for (int h = 1; h <= 24; h++)
                    {
                        //////////////////////////////////////////////////////////


                        if ((hed == "آرایش تولید بازار - مگاوات ساعت") || (hed == "ميزان خالص آرايش توليد بازار -مگاوات ساعت") || (TOOL == "Market Power"))
                        {
                            //s1/

                            ////////////////////////////////////////////

                            if (rdbillplant.Checked)
                            {

                                double x = printout1[0, ddd, h - 1];
                                double y = 0;
                                DataTable c = Utilities.GetTable("select s1 from MonthlyBillPlant where date='" + date + "'and ppid='" + lbplantmarket.Text.Trim() + "'and hour ='" + h + "'");
                                if (c.Rows.Count > 0) y = MyDoubleParse(c.Rows[0][0].ToString());
                                if(y!=x)
                                err += date + ";" + h + ";" + y + ";" + x + "-";
                            }
                          


                        }
                        else if ((hed == "تولید ناخالص - مگاوات ساعت") || (hed == "ميزان توليد ناخالص-مگاوات ساعت") || (TOOL == "TotalPower"))
                        {



                            if (rdbillplant.Checked)
                            {

                                double x = printout2[0, ddd, h - 1];
                                double y = 0;
                                DataTable c = Utilities.GetTable("select s2 from MonthlyBillPlant where date='" + date + "'and ppid='" + lbplantmarket.Text.Trim() + "'and hour ='" + h + "'");
                                if (c.Rows.Count > 0) y = MyDoubleParse(c.Rows[0][0].ToString());
                                if (y != x)
                                    err += date + ";" + h + ";" + y + ";" + x + "-";
                            }
                          
                     

                        }
                        else if (hed == "تولید خالص - مگاوات ساعت")
                        {
                            
                        }
                        else if ((hed == "مقدار پذیرفته شده با قیمت پیشنهادی فروشنده - مگاوات ساعت") || (hed == "ميزان خالص پذيرفته شده با قيمت پيشنهادي فروشنده-مگاوات ساعت") || (TOOL == "BidPower"))
                        {

                            if (rdbillplant.Checked)
                            {

                                double x = printout3[0, ddd, h - 1];
                                double y = 0;
                                DataTable c = Utilities.GetTable("select s4 from MonthlyBillPlant where date='" + date + "'and ppid='" + lbplantmarket.Text.Trim() + "'and hour ='" + h + "'");
                                if (c.Rows.Count > 0) y = MyDoubleParse(c.Rows[0][0].ToString());
                                if (y != x)
                                    err += date + ";" + h + ";" + y + ";" + x + "-";
                            }
                          
                            //}


                        }
                        else if ((hed == "مقدار پذیرفته شده با 90% قیمت حداقل همان ساعت - مگاوات ساعت") || (hed == "ميزان خالص پذيرفته شده با نرخ UL-مگاوات ساعت") || (TOOL == "ULPower"))
                        {

                            if (rdbillplant.Checked)
                            {

                                double x = printout4[0, ddd, h - 1];
                                double y = 0;
                                DataTable c = Utilities.GetTable("select s5 from MonthlyBillPlant where date='" + date + "'and ppid='" + lbplantmarket.Text.Trim() + "'and hour ='" + h + "'");
                                if (c.Rows.Count > 0) y = MyDoubleParse(c.Rows[0][0].ToString());
                                if (y != x)
                                    err += date + ";" + h + ";" + y + ";" + x + "-";
                            }
                           
                        }
                        else if ((hed == "میزان تولید بیش از پیشنهاد بازار - مگاوات ساعت") || (hed == "ميزان خالص توليد بيش از پيشنهاد بازارو به دستور مرکز-مگاوات ساعت") || (TOOL == "IncrementPower"))
                        {

                            if (rdbillplant.Checked)
                            {

                                double x = printout5[0, ddd, h - 1];
                                double y = 0;
                                DataTable c = Utilities.GetTable("select s6 from MonthlyBillPlant where date='" + date + "'and ppid='" + lbplantmarket.Text.Trim() + "'and hour ='" + h + "'");
                                if (c.Rows.Count > 0) y = MyDoubleParse(c.Rows[0][0].ToString());
                                if (y != x)
                                    err += date + ";" + h + ";" + y + ";" + x + "-";
                            }
                        }
                        else if ((hed == "مبلغ پذیرفته شده با قیمت پیشنهادی فروشنده - ريال") || (hed == "بهاي پرداختي با قيمت پيشنهادي فروشنده-ريال") || (TOOL == "BidPayment"))
                        {

                            if (rdbillplant.Checked)
                            {

                                double x = printdaramadOUT3[0, ddd, h - 1];
                                double y = 0;
                                DataTable c = Utilities.GetTable("select s7 from MonthlyBillPlant where date='" + date + "'and ppid='" + lbplantmarket.Text.Trim() + "'and hour ='" + h + "'");
                                if (c.Rows.Count > 0) y = MyDoubleParse(c.Rows[0][0].ToString());
                                if (y != x)
                                    err += date + ";" + h + ";" + y + ";" + x + "-";
                            }
                            
                        }
                        else if ((hed == "مبلغ پذیرفته شده با 90% قیمت حداقل همان ساعت - ريال") || (hed == "بهاي پرداختي با نرخ UL-ريال") || (TOOL == "ULPayment"))
                        {
                            if (rdbillplant.Checked)
                            {

                                double x = printout8[0, ddd, h - 1];
                                double y = 0;
                                DataTable c = Utilities.GetTable("select s8 from MonthlyBillPlant where date='" + date + "'and ppid='" + lbplantmarket.Text.Trim() + "'and hour ='" + h + "'");
                                if (c.Rows.Count > 0) y = MyDoubleParse(c.Rows[0][0].ToString());
                                if (y != x)
                                    err += date + ";" + h + ";" + y + ";" + x + "-";
                            }
                            
                           
                        }
                        else if ((hed == "مبلغ تولید بیش از پیشنهاد بازار - ريال") || (hed == "بهاي پرداختي بابت توليد بيش از پيشنهاد بازار-ريال") || (TOOL == "IncrementPayment"))
                        {
                                                      
                            if (rdbillplant.Checked)
                            {

                                double x = printdaramadOUT5[0, ddd, h - 1];
                                double y = 0;
                                DataTable c = Utilities.GetTable("select s9 from MonthlyBillPlant where date='" + date + "'and ppid='" + lbplantmarket.Text.Trim() + "'and hour ='" + h + "'");
                                if (c.Rows.Count > 0) y = MyDoubleParse(c.Rows[0][0].ToString());
                                if (y != x)
                                    err += date + ";" + h + ";" + y + ";" + x + "-";
                            }
                        }
                        else if ((hed == "جمع مبلغ بابت تولید خالص - ريال") || (hed == "جمع بهاي پرداختي بابت توليد خالص-ريال") || (TOOL == "Pure Energy Payment"))
                        {
                            if (rdbillplant.Checked)
                            {

                                double x = printdaramadOUT7[0, ddd, h - 1];
                                double y = 0;
                                DataTable c = Utilities.GetTable("select s10 from MonthlyBillPlant where date='" + date + "'and ppid='" + lbplantmarket.Text.Trim() + "'and hour ='" + h + "'");
                                if (c.Rows.Count > 0) y = MyDoubleParse(c.Rows[0][0].ToString());
                                if (y != x)
                                    err += date + ";" + h + ";" + y + ";" + x + "-";
                            }
                           
                        }
                        else if ((hed == "میزان تولید کمتر از پیشنهاد بازار و به دستور مرکز - مگاوات ساعت") || (hed == "ميزان توليد کمتر از پيشنهاد بازار و به دستور مرکز-مگاوات ساعت") || (TOOL == "DecreasePower"))
                        {

                            if (rdbillplant.Checked)
                            {

                                double x = printout6[0, ddd, h - 1];
                                double y = 0;
                                DataTable c = Utilities.GetTable("select s11 from MonthlyBillPlant where date='" + date + "'and ppid='" + lbplantmarket.Text.Trim() + "'and hour ='" + h + "'");
                                if (c.Rows.Count > 0) y = MyDoubleParse(c.Rows[0][0].ToString());
                                if (y != x)
                                    err += date + ";" + h + ";" + y + ";" + x + "-";
                            }
                          
                        }
                        else if ((hed == "مبلغ خسارت - ريال") || (hed == "بهاي پرداختي بابت سلب فرصت-ريال") || (TOOL == "DecreasePayment"))
                        {

                            if (rdbillplant.Checked)
                            {

                                double x = printdaramadOUT8[0, ddd, h - 1];
                                double y = 0;
                                DataTable c = Utilities.GetTable("select s12 from MonthlyBillPlant where date='" + date + "'and ppid='" + lbplantmarket.Text.Trim() + "'and hour ='" + h + "'");
                                if (c.Rows.Count > 0) y = MyDoubleParse(c.Rows[0][0].ToString());
                                if (y != x)
                                    err += date + ";" + h + ";" + y + ";" + x + "-";
                            }
                           
                           
                        }

                        else if ((hed == "مبلغ قابل پرداخت - ريال") || (hed == "بهاي قابل پرداخت-ريال") || (TOOL == "EnergyPayment"))
                        {

                            if (rdbillplant.Checked)
                            {

                                double x = printdaramadOUT9[0, ddd, h - 1];
                                double y = 0;
                                DataTable c = Utilities.GetTable("select s15 from MonthlyBillPlant where date='" + date + "'and ppid='" + lbplantmarket.Text.Trim() + "'and hour ='" + h + "'");
                                if (c.Rows.Count > 0) y = MyDoubleParse(c.Rows[0][0].ToString());
                                if (y != x)
                                    err += date + ";" + h + ";" + y + ";" + x + "-";
                            }
                        }
                        else if ((hed == "ميزان آمادگي - مگاوات ساعت") || (TOOL == "AvailableCapacity"))
                        {

                            if (rdbillplant.Checked)
                            {

                                double x = printddrclared[0, ddd, h - 1];
                                double y = 0;
                                DataTable c = Utilities.GetTable("select s16 from MonthlyBillPlant where date='" + date + "'and ppid='" + lbplantmarket.Text.Trim() + "'and hour ='" + h + "'");
                                if (c.Rows.Count > 0) y = MyDoubleParse(c.Rows[0][0].ToString());
                                if (y != x)
                                    err += date + ";" + h + ";" + y + ";" + x + "-";
                            }

                        }







                    }
                }

               split = err.Split('-');
            }


            catch
            {

            }
            return split;
        }

        private void lbplantmarket_TextChanged(object sender, EventArgs e)
        {
            Uniti = false;
            txtmarketlist.Text = "";
            label120.Text = "";
            paneldailybill.Visible = true;

            dgbill.DataSource = null;

            if (rddaily.Checked)
            {
                filldaily();
            }
            else
            {
                fillmonthly();
            }
        }
    
    
   
        private void lbunitmarket_TextChanged(object sender, EventArgs e)
        {
            Uniti = true;
            txtmarketlist.Text = "";
            label120.Text = "";
            paneldailybill.Visible = true;

            dgbill.DataSource = null;

            if (rddaily.Checked)
            {
                filldaily();
            }
            else
            {
                fillmonthly();
            }
        }

   
       
    }
}
