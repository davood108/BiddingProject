﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using NRI.SBS.Common;
using FarsiLibrary.Utils;
using System.Collections;
using System.IO;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using Auto_Update_Library;


namespace PowerPlantProject
{
    public partial class BillName : Form
    {
        string Date = "";
        public BillName(string date)
        {
            Date = date;
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void BillName_Load(object sender, EventArgs e)
        {
            
            datePickerFrom.Text = Date;
            fillgrid();

        }
        public void fillgrid()
        {
            string NOTICE = "";
            label1.Text = "";
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.DataSource = null;
            DataTable s = Utilities.GetTable("select distinct code,item,name from MonthlyBillTotal where month like'%"+datePickerFrom.Text.Substring(0,7).Trim()+"%'");
            //if (s.Rows.Count == 0)
            //{
            //    s = utilities.GetTable("select distinct code,item,name from MonthlyBillTotal where month in (select max(month) from MonthlyBillTotal where name is not null )");
            //}
            dataGridView1.DataSource = s;
            try
            {
                if (dataGridView1.Rows.Count > 1)
                {
                    for (int j = 0; j < dataGridView1.RowCount; j++)
                    {
                        if (dataGridView1.Rows[j].Cells[2].Value.ToString() == "" )
                       {
                            NOTICE = "Default Status. Press Save For Selected Month";
                            label1.Text = NOTICE;
                            DataTable s4 = Utilities.GetTable("select distinct name,month from MonthlyBillTotal where month<='" + datePickerFrom.Text.Substring(0, 7).Trim() + "' and code='" + dataGridView1.Rows[j].Cells[0].Value.ToString().Trim() + "'and name!='' order by month desc ");
                            dataGridView1.Rows[j].Cells[2].Value = s4.Rows[0][0].ToString();
                        }

                    }
                }
               
            }
            catch
            {
                
            }
        }

        private void datePickerFrom_ValueChanged(object sender, EventArgs e)
        {
            fillgrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
          
            for (int j = 0; j < dataGridView1.RowCount; j++)
            {

                DataTable s = Utilities.GetTable("update MonthlyBillTotal  set name='" + dataGridView1.Rows[j].Cells[2].Value + "' where month like'%" + datePickerFrom.Text.Substring(0, 7).Trim() + "%'and code='"+dataGridView1.Rows[j].Cells[0].Value+"'");

            }
            MessageBox.Show("Saved.");
            fillgrid();
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            this.Close();
            BaseDataForm m = new BaseDataForm();
            m.panel1.Visible = true;
            m.Show();
        }
    }
}
