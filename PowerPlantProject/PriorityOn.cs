﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using System.IO;
using Microsoft.Win32;

namespace PowerPlantProject
{
    public partial class PriorityOn : Form
    {
        public PriorityOn()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                dataGridView2.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void PriorityOn_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            try
            {
                datePickerFrom.Text = new PersianDate(DateTime.Now).ToString("d");
                datePickerto.Text = new PersianDate(DateTime.Now.AddDays(1)).ToString("d");
                dataGridView2.DataSource = null;
                DataTable cf = Utilities.GetTable("select * from priorityon");
                dataGridView2.DataSource = cf;
                DataTable c = Utilities.GetTable("select ppid from powerplant");
                cmbplant.Text = c.Rows[0][0].ToString();
                foreach (DataRow n in c.Rows)
                {
                    cmbplant.Items.Add(n[0].ToString().Trim());

                }
            }
            catch
            {

            }
            fillunit();
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            try
            {



               
                int rowIndex = e.RowIndex;

                string x = dataGridView2.Rows[rowIndex].Cells[e.ColumnIndex].Value.ToString();
                if (x == "System.Drawing.Bitmap")
                {
                    DataTable bb = Utilities.GetTable("delete from priorityon where ppid='" + cmbplant.Text.Trim() + "' and  unit='" + dataGridView2.Rows[rowIndex].Cells[2].Value.ToString().Trim() + "'and ppid='" + dataGridView2.Rows[rowIndex].Cells[1].Value.ToString().Trim() + "'and fromdate='" + dataGridView2.Rows[rowIndex].Cells[3].Value.ToString().Trim() + "'and todate='" + dataGridView2.Rows[rowIndex].Cells[4].Value.ToString().Trim() + "'");
                }
                dataGridView2.DataSource = null;
                DataTable cf = Utilities.GetTable("select * from priorityon");
                dataGridView2.DataSource = cf;
            }
            catch
            {
            }
        }

        private void cmbplant_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillunit();
        }
        public void fillunit()
        {

            dataGridView1.DataSource = null;
            DataTable v = Utilities.GetTable("select unitcode from UnitsDataMain where ppid='" + cmbplant.Text.Trim() + "'order by unitcode asc");
            dataGridView1.ColumnCount = v.Rows.Count + 1;
            dataGridView1.RowCount = 2;
            dataGridView1.Rows[0].Cells[0].Value = "UnitCode";
            dataGridView1.Rows[1].Cells[0].Value = "PriorityIndex";
            for (int u = 1; u <= v.Rows.Count; u++)
            {
                dataGridView1.Rows[0].Cells[u].Value = v.Rows[u - 1][0].ToString();
            }
        }
        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "" || str.Trim() == null)
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private void btnadd_Click(object sender, EventArgs e)
        {
            TimeSpan span = PersianDateConverter.ToGregorianDateTime(datePickerto.Text) - PersianDateConverter.ToGregorianDateTime(datePickerFrom.Text);
            int dday = span.Days;
            if (dday < 0)
            {
                MessageBox.Show("Please Select Correct Interval Date!");
                return;
            }
            try
            {
               

                for (int j = 1; j <= dataGridView1.ColumnCount - 1; j++)
                {
                    string unit = dataGridView1.Rows[0].Cells[j].Value.ToString().Trim();
                    string  index =MyDoubleParse( dataGridView1.Rows[1].Cells[j].Value.ToString().Trim()).ToString();
                    if (MyDoubleParse(index) > dataGridView1.ColumnCount - 1 || index == "0")
                    {
                        MessageBox.Show("Max Index Is  "+(dataGridView1.ColumnCount - 1).ToString());

                        return;
                    }
                    DataTable del = Utilities.GetTable("delete from priorityon where ppid='" + cmbplant.Text.Trim() + "'and fromdate ='" + datePickerFrom.Text + "'and todate='" + datePickerto.Text + "'and unit='"+unit+"'");
                    DataTable ins = Utilities.GetTable("insert into priorityon (ppid,fromdate,todate,unit,onindex)values('" + cmbplant.Text.Trim() + "','" + datePickerFrom.Text + "','" + datePickerto.Text + "','" + unit + "','" + index + "')");
                }
                dataGridView2.DataSource = null;
                DataTable cf = Utilities.GetTable("select * from priorityon");
                dataGridView2.DataSource = cf;
                MessageBox.Show("Saved");
            }
            catch
            {

            }
        }
    }
}
