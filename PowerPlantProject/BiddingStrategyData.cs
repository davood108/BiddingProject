﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NRI.SBS.Common;
using System.Data;
using System.Data.SqlClient;
using FarsiLibrary.Utils;
using System.Collections;
using System.Windows.Forms;

namespace PowerPlantProject
{
    public class BiddingStrategyData
    {
        string PackageType = "";
        string GenFarsi = "";
        string Gencode = "";
        public string [] otherGenco;
        int countothergenco = 0;
        private int PPID;
        private int trainDays;
        private int trainingdaysprice;
        private int trainDaysMinusPrice;
        private int trainDaysMinusPower;
        private DateTime baseDate;
        private int Regions = 16;
        private bool isnearest = false;
        
        private CMatrix lc_H_N;
        private CMatrix m_DayTypes;
        private List<CMatrix> lst_ProducedRegionsHourly; // list<CMatrix(14, Training Days)>
        private List<CMatrix> lst_Rev_Produced;
        private List<CMatrix> lst_Rev_Produced_Power;


        private CMatrix m_TehranUsageNeed; // CMatrix(24 * Training Days)
        private CMatrix m_RegionsPeak; // CMatrix(16 * trainingDays-trainingDaysMinusPowerForecast+2)
        private CMatrix m_RegionsPeakPower; //////////for power

        ////////////////for end power
        private CMatrix m_otherGencoPeakPower;
        private CMatrix m_loadpower;


        //CMatrix m_Power_RegionsPeak;
    
        private CMatrix m_Power_TehranProduced; // CMatrix(24 * trainDays - trainDaysMinusPower+2)  
        // the last column of m_Power_TehranProduced represents today, and is needed for calculating T, not needed for D1,D2
        private CMatrix m_Power_TehranProduced_final_minuspower;
        private CMatrix m_Power_TehranProduced_rival;
        public CMatrix M_PowerForecast_25thRow
        {
            get { return m_DayTypes; }
        }
        public CMatrix M_PowerForecast_T
        {
            get
            {
                CMatrix temp = this.M_PowerForecast_1_24;
                return temp.RemoveAtColumn(temp.Cols-1); 
            }
        }
        public CMatrix M_PriceForecast_59_72
        {
            get
            {   //CMatrix pricePeaks = m_RegionsPeak.RemoveAtColumn(0); // column 0 is for D4 of Priceforecast

               // CMatrix pricePeaks = m_RegionsPeak.RemoveAtRow(4).RemoveAtRow(3);

                CMatrix pricePeaks = m_RegionsPeak;

                //for (int i = 0; i < trainDaysMinusPrice - trainDaysMinusPower; i++)
                //    pricePeaks = pricePeaks.RemoveAtColumn(0);
                pricePeaks = pricePeaks.RemoveAtColumn(pricePeaks.Cols - 1); // remove today!!
                return pricePeaks;
            }
        }
        public CMatrix M_PriceForecast_44thRow
        {
            get
            {
                CMatrix usageNeediDaysBefor = m_TehranUsageNeed.RemoveAtColumn(0);
                CMatrix usageNeediPlus1DayBefor = m_TehranUsageNeed.RemoveAtColumn(m_TehranUsageNeed.Cols - 1);
                CMatrix temp3 = (usageNeediDaysBefor - usageNeediPlus1DayBefor) % usageNeediPlus1DayBefor;
                return temp3;
            }
        }
        public CMatrix M_PowerForecast_YesterDayPower
        {
            get
            {   
                return m_Power_TehranProduced.GetColumn(m_Power_TehranProduced.Cols-1);
            }
        }
        public CMatrix M_PowerForecast_YesterDayPower_aftercal
        {
            get
            {
                return m_Power_TehranProduced_final_minuspower.GetColumn(m_Power_TehranProduced_final_minuspower.Cols - 1);
            }
        }

        public CMatrix M_PowerForecast_1_24
        {
            get
            {   // D1
                CMatrix TehranProducediDaysBefor = m_Power_TehranProduced.RemoveAtColumn(0);
                // D2
                CMatrix TehranProducediPlus1DayBefor =
                    m_Power_TehranProduced.RemoveAtColumn(m_Power_TehranProduced.Cols - 1);
                //CMatrix temp = (TehranProducediDaysBefor - TehranProducediPlus1DayBefor) % TehranProducediPlus1DayBefor;
                CMatrix temp = (TehranProducediDaysBefor - TehranProducediPlus1DayBefor);
                return temp;
            }
        }
        //public CMatrix M_PowerForecast_1_24_noMInus
        //{
        //    get
        //    {
        //        // D1
        //        CMatrix temp = m_Power_TehranProduced;
             
        //        return temp;
               
               
        //    }
        //}

        public CMatrix M_PowerForecast_1_24_Aftercal
        {
            get
            {
                CMatrix TehranProducediDaysBefor = m_Power_TehranProduced_final_minuspower.RemoveAtColumn(0);
                // D2
                CMatrix TehranProducediPlus1DayBefor =
                    m_Power_TehranProduced.RemoveAtColumn(m_Power_TehranProduced_final_minuspower.Cols - 1);
                //CMatrix temp = (TehranProducediDaysBefor - TehranProducediPlus1DayBefor) % TehranProducediPlus1DayBefor;
                CMatrix temp = (TehranProducediDaysBefor - TehranProducediPlus1DayBefor) ;
                return temp;

            }
        }

        public CMatrix M_PowerForecast_26_37
        {
            get
            {
                CMatrix powerPeaks = m_RegionsPeakPower.RemoveAtColumn(0); // column 0 is for D4 of Priceforecast
                //powerPeaks = powerPeaks.RemoveAtRow(11).RemoveAtRow(4).RemoveAtRow(3).RemoveAtRow(0);

                powerPeaks = powerPeaks.RemoveAtRow(powerPeaks.Rows-1).RemoveAtRow(0);
                CMatrix D3 = powerPeaks.RemoveAtColumn(0);
                CMatrix D4 = powerPeaks.RemoveAtColumn(powerPeaks.Cols - 1);
              
                return (D3 - D4) % D4;
            }
        }
       

        /////////////////add end of power forecast//////////////////////////////////////
        public CMatrix M_PowerForecast_otherGenco
        {
            get
            {
                CMatrix powerPeaks1 = m_otherGencoPeakPower.RemoveAtColumn(0); // column 0 is for D4 of Priceforecast
                //powerPeaks1 = powerPeaks1.RemoveAtRow(powerPeaks1.Rows - 1).RemoveAtRow(0);
                CMatrix D3 = powerPeaks1.RemoveAtColumn(0);
                CMatrix D4 = powerPeaks1.RemoveAtColumn(powerPeaks1.Cols - 1);
                return (D3 - D4) % D4;
            }
        }
        public CMatrix M_PowerForecast_load
        {
            get { return m_loadpower; }
        }



        //public CMatrix M_Power_TehranProduced
        //{
        //    get { return m_Power_TehranProduced; }
        //    set { m_Power_TehranProduced = value; }
        //}


        //public CMatrix M_Power_RegionsPeak
        //{
        //    get { return m_Power_RegionsPeak; }
        //    set { m_Power_RegionsPeak = value; }
        //}

        public CMatrix LC_H_N
        {
            get { return lc_H_N; }
        }

        //public CMatrix M_TehranUsageNeed
        //{
        //    get { return m_TehranUsageNeed; }
        //}

        public List<CMatrix> L_ProducedRegionsHourly
        {
            get { return lst_ProducedRegionsHourly; }
        }
        public List<CMatrix> L_Rev_Produced
        {
            get { return lst_Rev_Produced; }
        }

        public List<CMatrix> L_Rev_Produced_Power
        {
            get { return lst_Rev_Produced_Power; }
        }


        public BiddingStrategyData(int TrainingDays, int TrainingDaysprice, int PriceTrainingDaysMinus, int PowerTrainingDaysMinus, DateTime BaseDate, bool IsNearest, int ppid, string ptype)
        {
            trainDaysMinusPrice = PriceTrainingDaysMinus;
            trainDays = TrainingDays ;
            trainingdaysprice = TrainingDaysprice;
            trainDaysMinusPower = PowerTrainingDaysMinus;
            baseDate = BaseDate;
            isnearest = IsNearest;
            PPID = ppid;
            PackageType = ptype;
        }
        
        public void GetFirstData()
        {
            DataTable dt = Utilities.GetTable("select * from BaseGencoInfo");
            if (dt.Rows.Count > 0)
            {                
               Gencode = dt.Rows[0]["GencoCode"].ToString().Trim();
               GenFarsi = dt.Rows[0]["GencoNamePersian"].ToString().Trim();
            }

            //Get_N_Day_Power_AllRegionsPeak(); // list<CMatrix(14, 24)>
           // Get_N_Days_Power_TehranProducedHourly(); // CMatrix(24 * Training Days)
            GetTehranProduced_Power(); // just for power forcast :: CMatrix(24 * Training Days)


         

            GetLC_H_NValue_Price();
            GetRegionsProduced_price(); // list<CMatrix(14, 24)>
            GetTehranUsageNeed_Price(); // CMatrix(24 * Training Days)
            
          //timing  GetRegionsPeaks_Common(); // common between two!!
            ///////////////for end power
            //timing  GetotherGencoPeaks_Common();
            //timing GetLoadpower();
            ////////////////////////////

            //timing  GetRegionsPeaksPrice_Common();
        
            GetDayTypes();
         
        }

        public void UpdateWithNextDay()
        {
            DataTable dt = Utilities.GetTable("select * from BaseGencoInfo");
            if (dt.Rows.Count > 0)
            {
                Gencode = dt.Rows[0]["GencoCode"].ToString().Trim();
                GenFarsi = dt.Rows[0]["GencoNamePersian"].ToString().Trim();
            }


            baseDate = baseDate.AddDays(1);
            UpdateRegionsPeak_Common();
            UpdateRegionsProduced_Price();
            UpdateTehranProduced_Power();
            UpdateTehranUsageNeed_Price();
            UpdateDayTypes();
            //////////
           // GetRival();
           // GetRivalTrainPower();
           //CalculateRIVAL();       
            GetRegionsPeaksPrice_Common();   

            /////////////for end
            GetotherGencoPeaks_Common();
            GetLoadpower();
        }

        private void GetDayTypes()
        {
            string strDayOfWeek = new PersianDate(baseDate.Subtract(new TimeSpan(1,0,0,0))).DayOfWeek.ToString();
            int dayIndex;
            for (dayIndex = 1; dayIndex <= 7; dayIndex++)
            {
                Days curDay = (Days)Enum.Parse(typeof(Days), dayIndex.ToString());
                if (curDay.ToString() == strDayOfWeek)
                    break;
            }

            m_DayTypes = new CMatrix(1, trainDays - trainDaysMinusPower);
            m_DayTypes[0, m_DayTypes.Cols - 1] = dayIndex;

            for (int f = trainDays - trainDaysMinusPower - 2; f >= 0; f--)
            {
                m_DayTypes[0, f] = (m_DayTypes[0, f + 1] == 7 ? 1 : m_DayTypes[0, f + 1] + 1);
            }
        }

       
        private void UpdateDayTypes()
        {

            m_DayTypes.RemoveAtColumn(0);
            m_DayTypes.AddColumns(new CMatrix(1, 1));
            int dayIndex = (m_DayTypes[0, m_DayTypes.Cols - 2]==7? 1: (int)(m_DayTypes[0, m_DayTypes.Cols - 2] + 1));
            m_DayTypes[0, m_DayTypes.Cols - 1] = dayIndex;
        }
       
        private void GetLC_H_NValue_Price()
        {
            //Application.DoEvents();
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();
            

            int maxCount = trainingdaysprice * 24;
            int lineTypesNo = Enum.GetNames(typeof(Line_codes)).Length;
            lc_H_N = new CMatrix(lineTypesNo, maxCount);

            DateTime tempDate = baseDate.Subtract(new TimeSpan(1, 0, 0, 0));
            string endDate = new PersianDate(tempDate).ToString("d");
            string startDate = new PersianDate(tempDate.Subtract(new TimeSpan(trainingdaysprice, 0, 0, 0))).ToString("d");

            if (isnearest)
            {
               
                endDate = FindNearInterchange(endDate);
                DateTime tend = PersianDateConverter.ToGregorianDateTime(endDate);
                startDate = new PersianDate(tend.Subtract(new TimeSpan(trainingdaysprice, 0, 0, 0))).ToString("d");
            }

            DataRow lastrow = null;

            for (int index = 1; index <= lineTypesNo; index++)
            {
                Line_codes line = (Line_codes)Enum.Parse(typeof(Line_codes), index.ToString());

                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = new SqlCommand("SELECT * FROM [InterchangedEnergy] " +
                "WHERE Date>=@startdate AND Date<=@enddate AND Code=@code ORDER BY Date", myConnection);
                Maxda.SelectCommand.Parameters.Add("@startdate", SqlDbType.Char, 10);
                Maxda.SelectCommand.Parameters["@startdate"].Value = startDate;
                Maxda.SelectCommand.Parameters.Add("@enddate", SqlDbType.Char, 10);
                Maxda.SelectCommand.Parameters["@enddate"].Value = endDate;   
                Maxda.SelectCommand.Parameters.Add("@code", SqlDbType.NChar, 20);
                Maxda.SelectCommand.Parameters["@code"].Value = line.ToString();
                Maxda.Fill(MaxDS);

                DataTable dt = MaxDS.Tables[0];


                /////////////////if null happen///////////////////////////

                DataTable nultable = Utilities.GetTable("SELECT * FROM [InterchangedEnergy] " +
                "WHERE Date in (SELECT max(date) FROM [InterchangedEnergy]) AND Code='" + line.ToString() + "' ORDER BY Date");
                
                ///////////////////////////////////////////////////////

                for (int daysbefore = trainingdaysprice; daysbefore >= 1; daysbefore--)
                {

                    DateTime selectedDate = PersianDateConverter.ToGregorianDateTime(endDate).Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                    string strDate = new PersianDate(selectedDate).ToString("d");
                    //DateTime selectedDate = baseDate.Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                    //string strDate = new PersianDate(selectedDate).ToString("d");
                    

                    DataRow selectedRow = null;
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["Date"].ToString().Trim() == strDate)
                            selectedRow = row;
                    }
                    if (selectedRow != null)
                    {
                        for (int j = 0; j < 24; j++)
                        {
                            string colName = "Hour" + (j + 1).ToString();
                            int col = maxCount - (24 * daysbefore) + j;
                            lc_H_N[index - 1, col] = MyDoubleParse(selectedRow[colName].ToString());
                        }
                        lastrow = selectedRow;
                    }
                    else if (lastrow != null)
                    {
                        for (int j = 0; j < 24; j++)
                        {
                            string colName = "Hour" + (j + 1).ToString();
                            int col = maxCount - (24 * daysbefore) + j;
                            lc_H_N[index - 1, col] = MyDoubleParse(lastrow[colName].ToString());
                        }

                        // it is by default equal to zero
                    }
                    else
                    {
                        for (int j = 0; j < 24; j++)
                        {
                            string colName = "Hour" + (j + 1).ToString();
                            int col = maxCount - (24 * daysbefore) + j;
                            lc_H_N[index - 1, col] = MyDoubleParse(nultable.Rows[0][colName].ToString());
                        }
                    }

                }

            }
            myConnection.Close();
        }

        //private void Get_N_Day_Power_AllRegionsPeak() // 14*trainingDays-trainingDaysMinus
        //{
        //    CMatrix matrix = new CMatrix(Regions, trainingDays - trainingDaysMinusPowerForecast);

        //    for (int f = 1; f <= trainingDays - trainingDaysMinusPowerForecast; f++)
        //    {
        //        date = date.Subtract(new TimeSpan(1, 0, 0));
        //        string strCmd = "select Peak from Manategh where" +
        //            " date ='" + new PersianDate(date).ToString("d") + "'" +
        //            " and Title= N'توليد بدون صنايع' order by Code";
        //        DataTable dt = utilities.GetTable(strCmd);
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            matrix[i, trainingDays - trainingDaysMinus - f] =
        //                double.Parse(dt.Rows[i][0].ToString());

        //        }
        //    }
        //    matrix = matrix.RemoveAtRow(11);
        //    matrix = matrix.RemoveAtRow(4);
        //    matrix = matrix.RemoveAtRow(3);
        //    matrix = matrix.RemoveAtRow(0);
        //    m_Power_RegionsPeak = matrix;
        //}

        private void GetRegionsPeaks_Common() // 14*trainingDays-trainingDaysMinus
        {
            int powerForecastDim = trainDays - trainDaysMinusPower;

            
            DateTime date = baseDate.Subtract(new TimeSpan(powerForecastDim+1, 0, 0,0));

            if (isnearest)
                date =PersianDateConverter.ToGregorianDateTime(FindNearManategh(new PersianDate(date).ToString("d")));


            // +1 is for D4 of priceforecast that is not used so far
            CMatrix temp = GetOneDayPeak(date); // recent day is today!!!!!!!!

            for (int f = 1; f <= powerForecastDim+1; f++) // f starts from 1 becase of needing today in D3 of power forecast
            {
               date=date.AddDays(1);
                temp = temp.AddColumns(GetOneDayPeak(date));
            }
            m_RegionsPeakPower = temp;
           
        }

        private void GetotherGencoPeaks_Common() // 14*trainingDays-trainingDaysMinus
        {
            int powerForecastDim = trainDays - trainDaysMinusPower;


            DateTime date = baseDate.Subtract(new TimeSpan(powerForecastDim + 1, 0, 0, 0));

            if (isnearest)
                date = PersianDateConverter.ToGregorianDateTime(FindNearProduceDate(new PersianDate(date).ToString("d")));


            // +1 is for D4 of priceforecast that is not used so far
            CMatrix temp = GetOneDayPeakotherGenco(date); // recent day is today!!!!!!!!
         
            for (int f = 1; f <= powerForecastDim + 1; f++) // f starts from 1 becase of needing today in D3 of power forecast
            {
                date = date.AddDays(1);
               temp = temp.AddColumns(GetOneDayPeakotherGenco(date));
               
            }
           m_otherGencoPeakPower = temp;

        }
        private void GetRegionsPeaksPrice_Common() // 14*trainingDays-trainingDaysMinus
        {
            int powerForecastDim = trainingdaysprice - trainDaysMinusPrice;

            DateTime date = baseDate.Subtract(new TimeSpan(powerForecastDim + 1, 0, 0, 0));
            if (isnearest)
                date = PersianDateConverter.ToGregorianDateTime(FindNearManategh(new PersianDate(date).ToString("d")));


            // +1 is for D4 of priceforecast that is not used so far
            CMatrix temp = GetOneDayPeak(date); // recent day is today!!!!!!!!

            for (int f = 1; f <= powerForecastDim + 1; f++) // f starts from 1 becase of needing today in D3 of power forecast
            {
                date=date.AddDays(1);
                temp = temp.AddColumns(GetOneDayPeak(date));
            }
            m_RegionsPeak = temp;
          
        }

        private CMatrix GetOneDayPeak(DateTime date)
        {
            CMatrix newCol = new CMatrix(Regions, 1);
            string pdate = new PersianDate(date).ToString("d");

            if (isnearest)
            {
                pdate = FindNearManategh(pdate);
            }
            string strCmd = "select Peak from Manategh where" +
                " date ='" + pdate + "'" +
                " and Title= N'توليد بدون صنايع' order by Code";
            DataTable dt = Utilities.GetTable(strCmd);
            if (dt.Rows.Count > 0)
            {
                
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (MyDoubleParse(dt.Rows[i][0].ToString()) != 0)
                        {
                            newCol[i, 0] =
                                MyDoubleParse(dt.Rows[i][0].ToString());
                        }
                        else
                        {
                            newCol[i, 0] = 1.0;

                        }


                    }
               
            }
            else
            {
                pdate = FindNearManategh(pdate);

                string strCmd1 = "select Peak from Manategh where" +
                " date ='" + pdate + "'" +
                " and Title= N'توليد بدون صنايع' order by Code";
                DataTable dt1 = Utilities.GetTable(strCmd1);
                if (dt1.Rows.Count >0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        if (MyDoubleParse(dt1.Rows[i][0].ToString()) != 0)
                        {
                            newCol[i, 0] =
                                MyDoubleParse(dt1.Rows[i][0].ToString());
                        }
                        else
                        {
                            newCol[i, 0] = 1.0;

                        }


                    }
                }
                
                 
            }

            for (int r = 0; r < Regions; r++)
            {
                if (newCol[r, 0] == 0.0)
                {
                    newCol[r, 0] = 1.0;
                }

            }

            return newCol;
        }

        //private CMatrix GetOneDayPeakotherGenco(DateTime date)
        //{
            
        //    string pdate = new PersianDate(date).ToString("d");
            
        //    if (isnearest)
        //    {
        //        pdate = FindNearProduceDate(pdate);
        //    }
            
        //    DataTable regexcept = utilities.GetTable("select distinct PPCode from dbo.RegionNetComp where Code='R01' and PPCode!='" + PPID + "'and Date='" + pdate + "'and Type!='H'");

          
        //   CMatrix newCol = new CMatrix(regexcept.Rows.Count, 1);
        //    int irowgenco=0;
        //    foreach(DataRow myrow in regexcept.Rows)
        //    {
               
        //        string strCmd = "select sum(Hour1),sum(Hour2),sum(Hour3),sum(Hour4),"+
        //        "sum(Hour5),sum(Hour6),sum(Hour7),sum(Hour8),sum(Hour9),sum(Hour10),"+
        //        "sum(Hour11),sum(Hour12),sum(Hour13),sum(Hour14),sum(Hour15),sum(Hour16),"+
        //        "sum(Hour17),sum(Hour18),sum(Hour19),sum(Hour20),sum(Hour21),"+
        //        "sum(Hour22),sum(Hour23),sum(Hour24) from dbo.ProducedEnergy " +
        //        "where Date='"+pdate+"' and PPCode='"+myrow[0].ToString().Trim() + "'";

        //        DataTable dt = utilities.GetTable(strCmd);
        //        double max = 0.0;

        //        if (dt.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < dt.Columns.Count; i++)
        //            {
        //                if (max < double.Parse(dt.Rows[0][i].ToString()))
        //                    max = double.Parse(dt.Rows[0][i].ToString());

        //            }
        //        }

        //        newCol[irowgenco, 0] = max;
                       
        //       irowgenco++;
        //    }
           
        //   return newCol;

        //}

        private CMatrix GetOneDayPeakotherGenco(DateTime date)
        {

            string pdate = new PersianDate(date).ToString("d");

            if (isnearest)
            {
                pdate = FindNearProduceDate(pdate);
            }

            //DataTable regexcept = utilities.GetTable("select distinct PPCode from dbo.RegionNetComp where Code='R03' and PPCode!='" + PPID + "'and Date='" + pdate + "'and Type!='H' and PPCode in(select distinct ppid from dbo.PowerPlant)");
            DataTable regexcept = Utilities.GetTable("select distinct PPCode from dbo.RegionNetComp where Code='"+Gencode+"' and PPCode!='" + PPID + "'and Date='" + pdate + "'and Type!='H'");
            if (regexcept.Rows.Count == 0)
            {
                pdate = FindNearRegionDate(pdate);
                regexcept = Utilities.GetTable("select distinct PPCode from dbo.RegionNetComp where Code='"+Gencode+"' and PPCode!='" + PPID + "'and Date='" + pdate + "'and Type!='H'");

            }
           
            if (regexcept.Rows.Count > 0 && otherGenco==null)
            {
                countothergenco = regexcept.Rows.Count;
                otherGenco = new string[countothergenco];
                int i=0;
                foreach (DataRow mm in regexcept.Rows)
                {
                    otherGenco[i]=mm[0].ToString().Trim();
                    i++;
                }

            }

            CMatrix newCol = new CMatrix(countothergenco, 1);
            int irowgenco = 0;
         
            foreach (DataRow myrow in regexcept.Rows)
            {
                if (isinothers( myrow[0].ToString().Trim()))
                {
                    string strCmd = "select sum(Hour1),sum(Hour2),sum(Hour3),sum(Hour4)," +
                    "sum(Hour5),sum(Hour6),sum(Hour7),sum(Hour8),sum(Hour9),sum(Hour10)," +
                    "sum(Hour11),sum(Hour12),sum(Hour13),sum(Hour14),sum(Hour15),sum(Hour16)," +
                    "sum(Hour17),sum(Hour18),sum(Hour19),sum(Hour20),sum(Hour21)," +
                    "sum(Hour22),sum(Hour23),sum(Hour24) from dbo.ProducedEnergy " +
                    "where Date='" + pdate + "' and PPCode='" + myrow[0].ToString().Trim() + "'";

                    DataTable dt = Utilities.GetTable(strCmd);
                    double max = 0.0;

                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            if (dt.Rows[0][i].ToString() != "")
                            {
                                if (max < MyDoubleParse(dt.Rows[0][i].ToString()))
                                    max = MyDoubleParse(dt.Rows[0][i].ToString());
                            }
                            else
                            {
                                string pdatenotzero = FindNearProduceDate(pdate);
                                string strCmdnotzero = "select sum(Hour1),sum(Hour2),sum(Hour3),sum(Hour4)," +
                               "sum(Hour5),sum(Hour6),sum(Hour7),sum(Hour8),sum(Hour9),sum(Hour10)," +
                               "sum(Hour11),sum(Hour12),sum(Hour13),sum(Hour14),sum(Hour15),sum(Hour16)," +
                               "sum(Hour17),sum(Hour18),sum(Hour19),sum(Hour20),sum(Hour21)," +
                               "sum(Hour22),sum(Hour23),sum(Hour24) from dbo.ProducedEnergy " +
                               "where Date='" + pdatenotzero + "' and PPCode='" + myrow[0].ToString().Trim() + "'";
                                DataTable dtnotzero = Utilities.GetTable(strCmdnotzero);
                                if (dtnotzero.Rows[0][i].ToString() != "")
                                {
                                    if (max < MyDoubleParse(dtnotzero.Rows[0][i].ToString()))
                                        max = MyDoubleParse(dtnotzero.Rows[0][i].ToString());
                                }

                            }

                        }
                    }
                    if (max != 0)
                    {
                        newCol[irowgenco, 0] = max;
                    }
                    else
                    {
                        newCol[irowgenco, 0] = 1.0;
                    }

                    irowgenco++;

                }
            }



            return newCol;

        }

        private bool isinothers(string name)
        {
            for (int i = 0; i < countothergenco; i++)
            {
                if (name == otherGenco[i])
                    return true;

            }
            return false;

        }

        private void GetLoadpower()
        {
         m_loadpower = new CMatrix(1,trainDays - trainDaysMinusPower);


            DateTime date = baseDate;
            DateTime Finalprice;

            string pricedate = new PersianDate(date).ToString("d");

            if (isnearest)
            {
                pricedate = FindNearManategh(pricedate);
                Finalprice = PersianDateConverter.ToGregorianDateTime(pricedate);
            }

            else
                Finalprice = baseDate;

            for (int f = 0; f <= trainDays - trainDaysMinusPower-1; f++)
            {
                Finalprice = Finalprice.Subtract(new TimeSpan(1, 0, 0, 0));
                string strCmd = "select peak from Manategh where" +
                    " Name=N'"+GenFarsi+"' and date ='" + new PersianDate(Finalprice).ToString("d") + "'" +
                    " and Title= N'نياز مصرف'";
                DataTable dt = Utilities.GetTable(strCmd);
                if (dt.Rows.Count > 0)
                {
                    m_loadpower[0, trainDays - trainDaysMinusPower - f - 1] =
                         MyDoubleParse(dt.Rows[0][0].ToString());

                }
                else
                {

                 
                    string strCmd1 = "select peak from Manategh where" +
                        " Name=N'"+GenFarsi+"' and date ='" + FindNearManategh(new PersianDate(Finalprice).ToString("d")) + "'" +
                        " and Title= N'نياز مصرف'";
                    DataTable dt1 = Utilities.GetTable(strCmd1);
                    if (dt1.Rows.Count > 0)
                    {
                        m_loadpower[0, trainDays - trainDaysMinusPower - f - 1] =
                             MyDoubleParse(dt1.Rows[0][0].ToString());

                    }

                }

                    
            }



        }


        //private void Get_N_Day_AllRegionsPeak() // 14*trainingDays-trainingDaysMinus
        //{
        //    CMatrix matrix = new CMatrix(Regions, trainingDays - trainingDaysMinus);

        //    for (int f = 1; f <= trainingDays - trainingDaysMinus; f++)
        //    {
        //        date = date.Subtract(new TimeSpan(1, 0, 0));
        //        string strCmd = "select Peak from Manategh where" +
        //            " date ='" + new PersianDate(date).ToString("d") + "'" +
        //            " and Title= N'توليد بدون صنايع' order by Code";
        //        DataTable dt = utilities.GetTable(strCmd);
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {

        //            matrix[i, trainingDays - trainingDaysMinus - f] =
        //                double.Parse(dt.Rows[i][0].ToString());

        //        }
        //    }
        //    matrix = matrix.RemoveAtRow(4);
        //    matrix = matrix.RemoveAtRow(3);
        //    m_RegionsPeak = matrix;
        //}

        private void GetTehranUsageNeed_Price() // 24*trainingDays-trainingDaysMinus+1
        {
            m_TehranUsageNeed = new CMatrix(24, trainingdaysprice - trainDaysMinusPrice + 2);

          
            DateTime date = baseDate;
            DateTime Finalprice;
 
            string pricedate = new PersianDate(date).ToString("d");

            if (isnearest)
            {
                pricedate = FindNearManategh(pricedate);
                Finalprice = PersianDateConverter.ToGregorianDateTime(pricedate);
            }

            else
                Finalprice = baseDate;
          
            for (int f = 0; f < trainingdaysprice - trainDaysMinusPrice + 1; f++)
            {
                Finalprice = Finalprice.Subtract(new TimeSpan(1, 0, 0, 0));
                string strCmd = "select * from Manategh where" +
                    " Name=N'"+GenFarsi+"' and date ='" + new PersianDate(Finalprice).ToString("d") + "'" +
                    " and Title= N'نياز مصرف'";
                DataTable dt = Utilities.GetTable(strCmd);
                if (dt.Rows.Count > 0)
                {

                    for (int j = 1; j <= 24; j++)
                    {
                        string colName = "Hour" + j.ToString();
                        m_TehranUsageNeed[j - 1, trainingdaysprice - trainDaysMinusPrice - f] =
                            MyDoubleParse(dt.Rows[0][colName].ToString());


                        if ((dt.Rows[0][colName].ToString() == "0") )
                        {
                            if (colName != "Hour24")
                            {
                                if ((MyDoubleParse(dt.Rows[0]["Hour" + (j + 1).ToString()].ToString()) != 0.0))
                                {
                                    m_TehranUsageNeed[j - 1, trainingdaysprice - trainDaysMinusPrice - f] =
                                    MyDoubleParse(dt.Rows[0]["Hour" + (j + 1).ToString()].ToString());
                                }
                                else
                                {
                                    m_TehranUsageNeed[j - 1, trainingdaysprice - trainDaysMinusPrice - f] = 1.0;

                                }
                            }
                            else
                            {
                                m_TehranUsageNeed[j - 1, trainingdaysprice - trainDaysMinusPrice - f] = 1.0;

                            }
                        }

                    }
                }

                else
                {
                   
                    string strCmd1 = "select * from Manategh where" +
                        " Name=N'"+GenFarsi+"' and date ='" + FindNearManategh(new PersianDate(Finalprice).ToString("d")) + "'" +
                        " and Title= N'نياز مصرف'";
                    DataTable dt1 = Utilities.GetTable(strCmd1);
                    if (dt1.Rows.Count > 0)
                    {

                        for (int j = 1; j <= 24; j++)
                        {
                            string colName = "Hour" + j.ToString();
                            m_TehranUsageNeed[j - 1, trainingdaysprice - trainDaysMinusPrice - f] =
                                MyDoubleParse(dt1.Rows[0][colName].ToString());


                            if ((dt1.Rows[0][colName].ToString() == "0"))
                            {
                                if (colName != "Hour24")
                                {
                                    if ((MyDoubleParse(dt1.Rows[0]["Hour" + (j + 1).ToString()].ToString()) != 0.0))
                                    {
                                        m_TehranUsageNeed[j - 1, trainingdaysprice - trainDaysMinusPrice - f] =
                                        MyDoubleParse(dt1.Rows[0]["Hour" + (j + 1).ToString()].ToString());
                                    }
                                    else
                                    {
                                        m_TehranUsageNeed[j - 1, trainingdaysprice - trainDaysMinusPrice - f] = 1.0;
                                        
                                    }

                                }
                                else
                                {
                                    m_TehranUsageNeed[j - 1, trainingdaysprice - trainDaysMinusPrice - f] = 1.0;


                                }
                            }

                        }
                    }
                    else
                    {
                        for (int k = 1; k <= 24; k++)
                        {
                            m_TehranUsageNeed[k - 1, trainingdaysprice - trainDaysMinusPrice - f] = 1.0;
                        }
                    }

                }

            }

        }



        
        //private void Get_N_Days_Power_TehranProducedHourly()
        //{
        //    for (int f = 1; f <= trainingDays - trainingDaysMinusPowerForecast; f++)
        //    {
        //        date = date.Subtract(new TimeSpan(1, 0, 0));
        //        string strCmd = "select * from Manategh where" +
        //            " date ='" + new PersianDate(date).ToString("d") + "'" +
        //            " and code='R0'" +
        //            " and Title= N'توليد بدون صنايع'";
        //        DataTable dt = utilities.GetTable(strCmd);

        //        if (dt.Rows.Count > 0)
        //        {
        //            for (int j = 1; j <= 24; j++)
        //            {
        //                string colName = "Hour" + j.ToString();
        //                m_Power_TehranProduced[j - 1, trainingDays - trainingDaysMinusPowerForecast - f]
        //                    = double.Parse(dt.Rows[0][colName].ToString());
        //            }
        //        }
        //    }
        //}
        private void GetRegionsProduced_price()
        {
         
            List<CMatrix> list = new List<CMatrix>();
            for (int i = 0; i < 24; i++)
                list.Add(new CMatrix(Regions, trainingdaysprice - trainDaysMinusPrice+1)); // for D2 that is not used so far

            DateTime date = baseDate;
            DateTime Finalreg;
            string regdate = new PersianDate(date).ToString("d");

            if (isnearest)
            {
                regdate = FindNearManategh(regdate);
                Finalreg = PersianDateConverter.ToGregorianDateTime(regdate);
            }
            else

                Finalreg = baseDate;

            for (int f = 1; f <= trainingdaysprice - trainDaysMinusPrice; f++)
            {
                Finalreg = Finalreg.Subtract(new TimeSpan(1, 0, 0, 0));

                string strCmd = "select * from Manategh where" +
                    " date ='" + new PersianDate(Finalreg).ToString("d") + "'" +
                    " and Title= N'توليد بدون صنايع' order by Code";
                DataTable dt = Utilities.GetTable(strCmd);

                if (dt.Rows.Count == 0)
                {
                    string strCmd1 = "select * from Manategh where" +
                   " date ='" +FindNearManategh(new PersianDate(Finalreg).ToString("d")) + "'" +
                   " and Title= N'توليد بدون صنايع' order by Code";
                   dt = Utilities.GetTable(strCmd1);

                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int j = 1; j <= 24; j++)
                    {
                        string colName = "Hour" + j.ToString();
                        (list[j - 1])[i, trainingdaysprice - trainDaysMinusPrice - f+1]
                            = MyDoubleParse(dt.Rows[i][colName].ToString());
                    }
                }

            }

            for (int i = 0; i < 24; i++)
            {
                list[i] = list[i].RemoveAtRow(4);
                list[i] = list[i].RemoveAtRow(3);
            }

            lst_ProducedRegionsHourly = list;
        }

        //private void GetTehranProduced_Power()
        //{
        //    m_Power_TehranProduced = new CMatrix(24, trainDays - trainDaysMinusPower + 1); // actually trainDays - trainDaysMinusPower+'2'

        //    // one from latest extreme end for subtracting two matrix

        //    DateTime date = baseDate;
        //    if (isnearest)
        //    {

        //        date = PersianDateConverter.ToGregorianDateTime(FindNearProduceDate(new PersianDate(date).ToString("d")));
        //    }
        //    else
        //        date = baseDate;

        //    for (int f = 1; f <= trainDays - trainDaysMinusPower + 1; f++)
        //    {
        //        date = date.Subtract(new TimeSpan(1, 0, 0, 0));
               

               
        //        ////////////////////////////////////////////
        //        int dualpid = PPID + 1;
        //        bool isdual = false;
        //        DataTable dualtypeid = utilities.GetTable("select ppid,PackageType from dbo.PPUnit where PPID='" + PPID + "'");
        //        if (dualtypeid.Rows.Count > 1)
        //        {
        //            foreach (DataRow myrow in dualtypeid.Rows)
        //            {

        //                if (myrow[1].ToString().Trim() == "Combined Cycle")
        //                {
        //                    isdual = true;
        //                    break;
        //                }

        //            }

        //        }


        //        string strCmd = "";

        //        if (!isdual)
        //        {
        //            strCmd = "select * from dbo.ProducedEnergy where Date='" + new PersianDate(date).ToString("d") + "' and PPCode='" + PPID + "'";
        //        }
        //        else
        //        {
        //            strCmd = "select sum(hour1)as Hour1,sum(hour2)as Hour2,sum(hour3)as Hour3,sum(hour4)as Hour4,sum(hour5)as Hour5,sum(hour6)as Hour6,sum(hour7)as Hour7,sum(hour8)as Hour8,sum(hour9)as Hour9,sum(hour10)as Hour10,sum(hour11)as Hour11,sum(hour12)as Hour12,sum(hour13)as Hour13,sum(hour14)as Hour14,sum(hour15)as Hour15,sum(hour16)as Hour16,sum(hour17)as Hour17,sum(hour18)as Hour18,sum(hour19)as Hour19,sum(hour20)as Hour20,sum(hour21)as Hour21,sum(hour22)as Hour22,sum(hour23)as Hour23,sum(hour24)as Hour24 from dbo.ProducedEnergy where Date='" + new PersianDate(date).ToString("d") + "' and (PPCode='"+PPID+"' or PPCode='"+ dualpid+ "')";
        //        }

        //            //string strCmd = "select * from Manategh where" +
        //            //    " date ='" +new PersianDate(date).ToString("d") + "'" +
        //            //    " and code ='R01'" +
        //            //    " and Title= N'توليد بدون صنايع' order by Code";
        //            DataTable dt = utilities.GetTable(strCmd);


        //            if (dt.Rows.Count > 0)
        //            {
        //                double notzero=0.0;

        //                for (int j = 1; j <= 24; j++)
        //                {
                           
        //                    string colName = "Hour" + j.ToString();
        //                    m_Power_TehranProduced[j - 1, f - 1]
        //                        = double.Parse(dt.Rows[0][colName].ToString());

        //                    if (double.Parse(dt.Rows[0][colName].ToString()) != 0) { notzero = double.Parse(dt.Rows[0][colName].ToString()); }
        //                    if (notzero == 0) { notzero = 1.0; }
        //                    if (dt.Rows[0][colName].ToString() == "0")
        //                    {
        //                        m_Power_TehranProduced[j - 1, f - 1]
        //                        = notzero;
        //                    }
        //                }
        //            }
                
               
                
        //    }

           
        //}
        //private void GetTehranProduced_Power_rival()
        //{
        //    //string pppowerdate = "";
        //    m_Power_TehranProduced_rival = new CMatrix(24, trainDays - trainDaysMinusPower + 1); // actually trainDays - trainDaysMinusPower+'2'

        //    // one from latest extreme end for subtracting two matrix

        //    DateTime date = baseDate;
        //    DateTime pppowerdate;
        //    if (isnearest)
        //    {
        //        pppowerdate = PersianDateConverter.ToGregorianDateTime(FindNearManategh(new PersianDate(date).ToString("d")));
        //    }
        //    else
        //        pppowerdate = baseDate;

        //    for (int f = 1; f <= trainDays - trainDaysMinusPower + 1; f++)
        //    {
        //        pppowerdate = pppowerdate.Subtract(new TimeSpan(1, 0, 0, 0));

        //        //pppowerdate = new PersianDate(date).ToString("d");
        //        //if (isnearest)
        //        //{
        //        //    pppowerdate = FindNearManategh(pppowerdate);
        //        //}
        //        string strCmd = "select * from Manategh where" +
        //            " date ='" +new PersianDate(pppowerdate).ToString("d")+ "'" +
        //            " and code ='R01'" +
        //            " and Title= N'توليد بدون صنايع' order by Code";
        //        DataTable dt = utilities.GetTable(strCmd);


        //        if (dt.Rows.Count > 0)
        //        {
        //            for (int j = 1; j <= 24; j++)
        //            {
        //                string colName = "Hour" + j.ToString();
        //                m_Power_TehranProduced_rival[j - 1, f - 1]
        //                    = double.Parse(dt.Rows[0][colName].ToString());
        //                if (dt.Rows[0][colName].ToString() == "0")
        //                {

        //                    m_Power_TehranProduced_rival[j - 1, f - 1]
        //                   = double.Parse(dt.Rows[0]["Hour" + (j+1).ToString()].ToString());

        //                }
        //            }
        //        }
        //    }

        //    // one from the recent extreme for calculating T
        //    //m_Power_TehranProduced = m_Power_TehranProduced.AddColumns(new CMatrix(24, 1)); // for today!!!!

        //}
        //private CMatrix PowerForecast_T()
        //{
        //    CMatrix TehranProducediMinus1DayBefor = m_Power_TehranProduced.RemoveAtColumn(0).RemoveAtColumn(0);

        //    CMatrix withoutToday = m_Power_TehranProduced.RemoveAtColumn(m_Power_TehranProduced.Cols - 1);
        //    CMatrix TehranProducediDaysBefor = withoutToday.RemoveAtColumn(0);

        //    CMatrix temp = (TehranProducediMinus1DayBefor - TehranProducediDaysBefor) % TehranProducediDaysBefor;
        //    return temp;
        //}

        private void GetTehranProduced_Power()
        {
            m_Power_TehranProduced = new CMatrix(24, trainDays - trainDaysMinusPower + 1); // actually trainDays - trainDaysMinusPower+'2'

            // one from latest extreme end for subtracting two matrix

            DateTime date = baseDate;
            if (isnearest)
            {

                date = PersianDateConverter.ToGregorianDateTime(FindNearProduceDate(new PersianDate(date).ToString("d")));
            }
            else
                date = baseDate;

            for (int f = 1; f <= trainDays - trainDaysMinusPower + 1; f++)
            {
                date = date.Subtract(new TimeSpan(1, 0, 0, 0));

                ////////////////////////////////////////////////

                int dualpid = PPID + 1;
                bool isdual = false;
                DataTable dualtypeid = Utilities.GetTable("select ppid,PackageType from dbo.PPUnit where PPID='" + PPID + "'");
                if (dualtypeid.Rows.Count > 1)
                {
                    foreach (DataRow myrow in dualtypeid.Rows)
                    {

                        if (myrow[1].ToString().Trim() == "Combined Cycle")
                        {
                            isdual = true;
                            break;
                        }

                    }

                }

                double[] vv = new double[24];
                DataTable dp = Utilities.GetTable("SELECT   DISTINCT UnitCode,PackageType   FROM dbo.UnitsDataMain WHERE PackageType='" + PackageType + "'");
                string blockM005 = "";
               
                foreach (DataRow m in dp.Rows)
               {
                   int ppidWithPriority = PPID;
                   if (isdual) ppidWithPriority++;
                   string unit = m["UnitCode"].ToString().Trim();
                   string package = m["PackageType"].ToString().Trim();


                   string ptypenum = "0";
                   if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";
                   if (PPID == 232) ptypenum = "0";
                   string temp = unit.ToLower();
                   if (package.Contains("CC"))
                   {
                       temp = temp.Replace("cc", "c");
                       string[] sp = temp.Split('c');
                       temp = sp[0].Trim() + sp[1].Trim();
                       if (temp.Contains("gas"))
                       {
                           temp = temp.Replace("gas", "G");
                       }
                       else
                       {
                           temp = temp.Replace("steam", "S");
                       }
                       temp = ppidWithPriority + "-" + temp;
                   }
                   else if (temp.Contains("gas"))
                   {
                       temp = ppidWithPriority + "-" + temp.Replace("gas", "G");
                   }
                   else
                   {
                       temp = ppidWithPriority + "-" + temp.Replace("steam", "S");
                   }
                   blockM005 = temp.Trim();

                   ////////////////////////////
                    
                   DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + new PersianDate(date).ToString("d") + "'order by BaseID desc");
                   string staterun = "";
                   string Marketrule = "";
                   string smaxdate = "";
                   if (dtsmaxdate.Rows.Count > 0)
                   {
                       string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                       int ib = 0;
                       foreach (DataRow d in dtsmaxdate.Rows)
                       {
                           arrbasedata[ib] = d["Date"].ToString();
                           ib++;
                       }
                       smaxdate = buildmaxdate(arrbasedata);
                   }
                   else
                   {
                       dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                       smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
                   }
                   DataTable basetable = Utilities.GetTable("select ProposalDay from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                   if (basetable.Rows.Count > 0)
                   {
                       Marketrule = basetable.Rows[0][0].ToString();
                   }

                   string tname = "dbo.DetailFRM005";
                   if (Marketrule == "Fuel Limited") tname = "BaDetailFRM005";

                    

                    //////////////////////
                    for (int h = 0; h < 24; h++)
                    {
                       if (!isdual)
                        {
                           {
                                DataTable dp1 = Utilities.GetTable("select sum (Dispatchable) from "+tname+" where TargetMarketDate='" + new PersianDate(date).ToString("d") + "' and  PPID='" + PPID + "'and hour='" + (h + 1) + "'and block='" + blockM005 + "'");
                                vv[h] += MyDoubleParse(dp1.Rows[0][0].ToString());
                            }
                        }
                        else
                        {
                            {
                                DataTable dp1 = Utilities.GetTable("select sum (Dispatchable) from "+tname+" where TargetMarketDate='" + new PersianDate(date).ToString("d") + "'and hour='" + (h + 1) + "'and block='" + blockM005 + "'and (PPCode='" + PPID + "' or PPCode='" + dualpid + "')");
                                vv[h] += MyDoubleParse(dp1.Rows[0][0].ToString());
                            }

                        }

                    }
                }


                ////////////////////////////////////////////////////////////////
                string strCmd = "";
                string part = "";

                if (PackageType == "CC") part = "C";
                if (PackageType == "Gas") part = "G";
                if (PackageType == "Steam") part = "S";

                if (!isdual)
                {

                    strCmd = "select  sum(hour1)as Hour1,sum(hour2)as Hour2,sum(hour3)as Hour3,sum(hour4)as Hour4,sum(hour5)as Hour5,sum(hour6)as Hour6,sum(hour7)as Hour7,sum(hour8)as Hour8,sum(hour9)as Hour9,sum(hour10)as Hour10,sum(hour11)as Hour11,sum(hour12)as Hour12,sum(hour13)as Hour13,sum(hour14)as Hour14,sum(hour15)as Hour15,sum(hour16)as Hour16,sum(hour17)as Hour17,sum(hour18)as Hour18,sum(hour19)as Hour19,sum(hour20)as Hour20,sum(hour21)as Hour21,sum(hour22)as Hour22,sum(hour23)as Hour23,sum(hour24)as Hour24 from dbo.ProducedEnergy where Date='" + new PersianDate(date).ToString("d") + "' and PPCode='" + PPID + "'AND Part like '" + part + '%' +  "'";
                }
                else
                {

                    strCmd = "select sum(hour1)as Hour1,sum(hour2)as Hour2,sum(hour3)as Hour3,sum(hour4)as Hour4,sum(hour5)as Hour5,sum(hour6)as Hour6,sum(hour7)as Hour7,sum(hour8)as Hour8,sum(hour9)as Hour9,sum(hour10)as Hour10,sum(hour11)as Hour11,sum(hour12)as Hour12,sum(hour13)as Hour13,sum(hour14)as Hour14,sum(hour15)as Hour15,sum(hour16)as Hour16,sum(hour17)as Hour17,sum(hour18)as Hour18,sum(hour19)as Hour19,sum(hour20)as Hour20,sum(hour21)as Hour21,sum(hour22)as Hour22,sum(hour23)as Hour23,sum(hour24)as Hour24 from dbo.ProducedEnergy where Date='" + new PersianDate(date).ToString("d") + "'AND Part like '" + part + '%' + "' and (PPCode='" + PPID + "' or PPCode='" + dualpid + "')";
                }


                DataTable dt = Utilities.GetTable(strCmd);


                if (dt.Rows.Count > 0)
                {
                    double notzero = 0.0;

                    for (int j = 1; j <= 24; j++)
                    {
                        
                        string colName = "Hour" + j.ToString();
                        if (dt.Rows[0][colName].ToString() != "")
                        {
                            if (vv[j - 1] != 0)
                            {
                                m_Power_TehranProduced[j - 1, f - 1]
                                    = MyDoubleParse(dt.Rows[0][colName].ToString()) / vv[j - 1];
                                if (m_Power_TehranProduced[j - 1, f - 1] < 0.1) m_Power_TehranProduced[j - 1, f - 1] = 0.1;
                            }
                            else
                            {
                                m_Power_TehranProduced[j - 1, f - 1] = 0.1;
                            }

                            if (MyDoubleParse(dt.Rows[0][colName].ToString()) != 0)
                            { 
                                notzero = MyDoubleParse(dt.Rows[0][colName].ToString());
                                if (vv[j - 1] != 0) notzero = MyDoubleParse(dt.Rows[0][colName].ToString()) / vv[j - 1];
                            }
                        }


                        if (notzero == 0) { notzero = 0.1; }
                        if (dt.Rows[0][colName].ToString() == "0" || dt.Rows[0][colName].ToString() == "")
                        {
                            if (vv[j - 1] != 0)
                            {

                                m_Power_TehranProduced[j - 1, f - 1]
                                = notzero / vv[j - 1];
                                if (m_Power_TehranProduced[j - 1, f - 1] < 0.1) m_Power_TehranProduced[j - 1, f - 1] = 0.1;
                            }
                            else
                            {
                                m_Power_TehranProduced[j - 1, f - 1] = 0.1;
                            }
                        }
                    }
                }
            }
        }
        private static string buildmaxdate(string[] arrmax)
        {
            string maxdate = null;
            for (int fd = 0; fd < arrmax.Length; fd++)
            {
                if (maxdate == null || string.Compare(maxdate, arrmax[fd]) < 0)
                    maxdate = arrmax[fd];
            }
            return maxdate;

        }

        private void UpdateRegionsProduced_Price() // 14 manategh * 24 hour
        {

            string pupdate = new PersianDate(baseDate).ToString("d");
            if (isnearest)
            {
                pupdate = FindNearManategh(pupdate);
            }
            CMatrix result = new CMatrix(14, 24);
            string strCmd = "select * from Manategh where" +
                " date ='" + pupdate+ "'" +
                " and Title= N'توليد بدون صنايع' order by Code";
            DataTable dt = Utilities.GetTable(strCmd);

            if (dt.Rows.Count == 0)
            {
                string strCmd1 = "select * from Manategh where" +
               " date ='" + FindNearManategh(pupdate) + "'" +
               " and Title= N'توليد بدون صنايع' order by Code";
                dt = Utilities.GetTable(strCmd1);

            }
            

            for (int j = 0; j < 24; j++)
            {
                lst_ProducedRegionsHourly[j] = lst_ProducedRegionsHourly[j].RemoveAtColumn(0);
                lst_ProducedRegionsHourly[j] = lst_ProducedRegionsHourly[j].AddColumns
                    (new CMatrix(lst_ProducedRegionsHourly[j].Rows, 1));
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i != 3 && i != 4)
                {
                    DataRow row = dt.Rows[i];
                    int rowM = (i < 3 ? i : i - 2);
                    for (int j = 1; j <= 24; j++)
                    {
                        string colName = "Hour" + j.ToString();
                        int colIndex = lst_ProducedRegionsHourly[j - 1].Cols - 1;
                        lst_ProducedRegionsHourly[j - 1][rowM, colIndex] =
                            MyDoubleParse(row[colName].ToString());
                    }

                }
            }
        }

        //private void UpdateTehranProduced_Power() // 14 manategh * 24 hour
        //{
        //    string ppowerupdate = new PersianDate(baseDate.Subtract(new TimeSpan(1, 0, 0, 0))).ToString("d");
        //    if (isnearest)
        //    {
        //        ppowerupdate = FindNearProduceDate(ppowerupdate);
        //    }
        //    CMatrix result = new CMatrix(Regions, 24);

        //    //////////////////////////////////////////////////////////

        //    int dualpid = PPID + 1;
        //    bool isdual = false;
        //    DataTable dualtypeid = utilities.GetTable("select ppid,PackageType from dbo.PPUnit where PPID='" + PPID + "'");
        //    if (dualtypeid.Rows.Count > 1)
        //    {
        //        foreach (DataRow myrow in dualtypeid.Rows)
        //        {

        //            if (myrow[1].ToString().Trim() == "Combined Cycle")
        //            {
        //                isdual = true;
        //                break;
        //            }

        //        }

        //    }


        //    string strCmd = "";

        //    if (!isdual)
        //    {
        //        strCmd = "select * from dbo.ProducedEnergy where Date='" + ppowerupdate + "' and PPCode='" + PPID + "'";
        //    }
        //    else
        //    {
        //        strCmd = "select sum(hour1)as Hour1,sum(hour2)as Hour2,sum(hour3)as Hour3,sum(hour4)as Hour4,sum(hour5)as Hour5,sum(hour6)as Hour6,sum(hour7)as Hour7,sum(hour8)as Hour8,sum(hour9)as Hour9,sum(hour10)as Hour10,sum(hour11)as Hour11,sum(hour12)as Hour12,sum(hour13)as Hour13,sum(hour14)as Hour14,sum(hour15)as Hour15,sum(hour16)as Hour16,sum(hour17)as Hour17,sum(hour18)as Hour18,sum(hour19)as Hour19,sum(hour20)as Hour20,sum(hour21)as Hour21,sum(hour22)as Hour22,sum(hour23)as Hour23,sum(hour24)as Hour24 from dbo.ProducedEnergy where Date='" + ppowerupdate + "' and (PPCode='" + PPID + "' or PPCode='" + dualpid + "')";
        //    }

           
        //    DataTable dt = utilities.GetTable(strCmd);

            
        //    /////////////////////////////////////////////////////////


        //    //string strCmd = "select * from Manategh where" +
        //    //    " date ='" +ppowerupdate + "'" +
        //    //    " and code='R01'" +
        //    //    " and Title= N'توليد بدون صنايع' ";
        //    //DataTable dt = utilities.GetTable(strCmd);


        //    if (dt.Rows.Count > 0)
        //    {
        //        double notzero = 0.0;
        //        CMatrix addMatrix = new CMatrix(24, 1);
        //        for (int j = 1; j <= 24; j++)
        //        {
        //            string colName = "Hour" + j.ToString();
        //            addMatrix[j - 1, 0] =
        //                double.Parse(dt.Rows[0][colName].ToString());

        //            if (double.Parse(dt.Rows[0][colName].ToString()) != 0.0) { notzero = double.Parse(dt.Rows[0][colName].ToString()); }
        //            else notzero = 1.0;
                    

        //            if (dt.Rows[0][colName].ToString() == "0")
        //            {
        //                addMatrix[j - 1, 0] = notzero;
                     

        //            }
        //        }

        //        m_Power_TehranProduced = m_Power_TehranProduced.RemoveAtColumn(0).AddColumns(addMatrix);

        //    }

        //}

        //private void UpdateRegionsPeak_withRecentDay()// 14 manategh * 1
        //{
        //    CMatrix addMatrix = new CMatrix(Regions, 1);
        //    string strCmd = "select Peak from Manategh where" +
        //        " date ='" + new PersianDate(date).ToString("d") + "'" +
        //        " and Title= N'توليد بدون صنايع' order by Code";
        //    DataTable dt = utilities.GetTable(strCmd);

        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //            addMatrix[i, 0] = double.Parse(dt.Rows[i][0].ToString());
        //    }

        //    addMatrix = addMatrix.RemoveAtRow(4);
        //    addMatrix = addMatrix.RemoveAtRow(3);
        //    m_RegionsPeak = m_RegionsPeak.RemoveAtColumn(0);
        //    m_RegionsPeak = m_RegionsPeak.AddColumns(addMatrix);
        //}

        private void UpdateTehranProduced_Power() // 14 manategh * 24 hour
        {
            string ppowerupdate = new PersianDate(baseDate.Subtract(new TimeSpan(1, 0, 0, 0))).ToString("d");
            if (isnearest)
            {
                ppowerupdate = FindNearProduceDate(ppowerupdate);
            }
            // CMatrix result = new CMatrix(Regions, 24);

            //////////////////////////////////////////////////////////

            int dualpid = PPID + 1;
            bool isdual = false;
            DataTable dualtypeid = Utilities.GetTable("select ppid,PackageType from dbo.PPUnit where PPID='" + PPID + "'");
            if (dualtypeid.Rows.Count > 1)
            {
                foreach (DataRow myrow in dualtypeid.Rows)
                {

                    if (myrow[1].ToString().Trim() == "Combined Cycle")
                    {
                        isdual = true;
                        break;
                    }

                }

            }

            ///////////////////////////////////////////////////////////////////////////////////////////
            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + new PersianDate(ppowerupdate).ToString("d") + "'order by BaseID desc");
            string staterun = "";
            string Marketrule = "";
            string smaxdate = "";
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }
            DataTable basetable = Utilities.GetTable("select ProposalDay from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetable.Rows.Count > 0)
            {
                Marketrule = basetable.Rows[0][0].ToString();
            }

            string tname = "dbo.DetailFRM005";
            if (Marketrule == "Fuel Limited") tname = "BaDetailFRM005";

            

            ///////////////////////////////////////////new//////////////////////////////////////////////////////
            double[] vv = new double[24];
            for (int h = 0; h < 24; h++)
            {
                DataTable dp1 = Utilities.GetTable("select sum (Dispatchable) from "+tname+" where TargetMarketDate='" + new PersianDate(ppowerupdate).ToString("d") + "' and  PPID='" + PPID + "'and hour='" + (h + 1) + "'");
                vv[h] += MyDoubleParse(dp1.Rows[0][0].ToString());

            }
            //////////////////////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////
            string strCmd = "";
            string part = "";

            if (PackageType == "CC") part = "C";
            if (PackageType == "Gas") part = "G";
            if (PackageType == "Steam") part = "S";

            if (!isdual)
            {

                strCmd = "select  sum(hour1)as Hour1,sum(hour2)as Hour2,sum(hour3)as Hour3,sum(hour4)as Hour4,sum(hour5)as Hour5,sum(hour6)as Hour6,sum(hour7)as Hour7,sum(hour8)as Hour8,sum(hour9)as Hour9,sum(hour10)as Hour10,sum(hour11)as Hour11,sum(hour12)as Hour12,sum(hour13)as Hour13,sum(hour14)as Hour14,sum(hour15)as Hour15,sum(hour16)as Hour16,sum(hour17)as Hour17,sum(hour18)as Hour18,sum(hour19)as Hour19,sum(hour20)as Hour20,sum(hour21)as Hour21,sum(hour22)as Hour22,sum(hour23)as Hour23,sum(hour24)as Hour24 from dbo.ProducedEnergy where Date='" + new PersianDate(ppowerupdate).ToString("d") + "' and PPCode='" + PPID + "'AND Part like '" + part + '%' + "'";
            }
            else
            {

                strCmd = "select sum(hour1)as Hour1,sum(hour2)as Hour2,sum(hour3)as Hour3,sum(hour4)as Hour4,sum(hour5)as Hour5,sum(hour6)as Hour6,sum(hour7)as Hour7,sum(hour8)as Hour8,sum(hour9)as Hour9,sum(hour10)as Hour10,sum(hour11)as Hour11,sum(hour12)as Hour12,sum(hour13)as Hour13,sum(hour14)as Hour14,sum(hour15)as Hour15,sum(hour16)as Hour16,sum(hour17)as Hour17,sum(hour18)as Hour18,sum(hour19)as Hour19,sum(hour20)as Hour20,sum(hour21)as Hour21,sum(hour22)as Hour22,sum(hour23)as Hour23,sum(hour24)as Hour24 from dbo.ProducedEnergy where Date='" + new PersianDate(ppowerupdate).ToString("d") + "'AND Part like '" + part + '%' + "' and (PPCode='" + PPID + "' or PPCode='" + dualpid + "')";
            }
                      

            DataTable dt = Utilities.GetTable(strCmd);


            /////////////////////////////////////////////////////////
          

            if (dt.Rows.Count > 0)
            {
                double notzero = 0.0;
                CMatrix addMatrix = new CMatrix(24, 1);
                for (int j = 1; j <= 24; j++)
                {
                    string colName = "Hour" + j.ToString();
                    if (dt.Rows[0][colName].ToString() != "")
                    {
                        if (vv[j - 1] != 0)
                        {
                            addMatrix[j - 1, 0] =
                                MyDoubleParse(dt.Rows[0][colName].ToString()) / vv[j - 1];
                            if (addMatrix[j - 1, 0] < 0.1) addMatrix[j - 1, 0] = 0.1;

                        }
                        else
                        {
                            addMatrix[j - 1, 0] = 0.1;
                        }
                        if (MyDoubleParse(dt.Rows[0][colName].ToString()) != 0.0)
                        {
                            notzero = MyDoubleParse(dt.Rows[0][colName].ToString());
                            if (vv[j - 1] != 0) notzero = MyDoubleParse(dt.Rows[0][colName].ToString()) / vv[j - 1];
                        }
                    }


                    if (notzero == 0) { notzero = 0.1; }

                    if (dt.Rows[0][colName].ToString() == "0" || dt.Rows[0][colName].ToString() == "")
                    {
                        if (vv[j - 1] != 0)
                        {
                            addMatrix[j - 1, 0] = notzero / vv[j - 1];
                            if (addMatrix[j - 1, 0] < 0.1) addMatrix[j - 1, 0] = 0.1;
                        }
                        else
                        {
                            addMatrix[j - 1, 0] = 0.1;
                        }

                    }

                }

                m_Power_TehranProduced = m_Power_TehranProduced.RemoveAtColumn(0).AddColumns(addMatrix);

            }


        }

        private void UpdateRegionsPeak_Common()// 14 manategh * 1
        {
            CMatrix addMatrix = GetOneDayPeak(baseDate);

            m_RegionsPeak = m_RegionsPeak.RemoveAtColumn(0);
           // m_RegionsPeak = m_RegionsPeak.AddColumns(addMatrix.RemoveAtRow(4).RemoveAtRow(3));
            
        }


        //private void Update_Power_RegionsPeak_withRecentDay()// 14 manategh * 1
        //{
        //    CMatrix addMatrix = new CMatrix(Regions, 1);
        //    string strCmd = "select Peak from Manategh where" +
        //        " date ='" + new PersianDate(date).ToString("d") + "'" +
        //        " and Title= N'توليد بدون صنايع' order by Code";
        //    DataTable dt = utilities.GetTable(strCmd);

        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        addMatrix[i, 0] = double.Parse(dt.Rows[i][0].ToString());
        //    }

        //    addMatrix = addMatrix.RemoveAtRow(11);
        //    addMatrix = addMatrix.RemoveAtRow(4);
        //    addMatrix = addMatrix.RemoveAtRow(3);
        //    addMatrix = addMatrix.RemoveAtRow(0);
        //    m_Power_RegionsPeak = m_Power_RegionsPeak.RemoveAtColumn(0);
        //    m_Power_RegionsPeak = m_Power_RegionsPeak.AddColumns(addMatrix);
        //}

        private void UpdateTehranUsageNeed_Price() // 24*1
        {
            CMatrix addMatrix = new CMatrix(24, 1);
            string strDate = new PersianDate(baseDate.Subtract(new TimeSpan(1,0,0,0))).ToString("d");
            if (isnearest)
            {
                strDate = FindNearManategh(strDate);
            }
            string strCmd = "select * from Manategh where" +
                " Name=N'"+GenFarsi+"' and date ='" + strDate + "'" +
                " and Title= N'نياز مصرف'";
            DataTable dt = Utilities.GetTable(strCmd);
            if (dt.Rows.Count > 0)
            {
                for (int j = 1; j <= 24; j++)
                {
                    string colName = "Hour" + j.ToString();
                    addMatrix[j - 1, 0] = MyDoubleParse(dt.Rows[0][colName].ToString());
                    if (dt.Rows[0][colName].ToString() == "0" )
                    {
                        if (colName != "Hour24")
                        {
                            if ((MyDoubleParse(dt.Rows[0]["Hour" + (j + 1).ToString()].ToString()) != 0.0))
                            {
                                addMatrix[j - 1, 0] = MyDoubleParse(dt.Rows[0]["Hour" + (j + 1).ToString()].ToString());

                            }
                            else
                            {

                                addMatrix[j - 1, 0] = 1.0;

                            }
                        }
                        else
                        {

                            addMatrix[j - 1, 0] = 1.0;

                        }
                    }
                }
            }
            else
            {
                string strCmd1 = "select * from Manategh where" +
               " Name=N'"+GenFarsi+"' and date ='" + FindNearManategh(strDate) + "'" +
               " and Title= N'نياز مصرف'";
                DataTable dt1 = Utilities.GetTable(strCmd1);
                if (dt1.Rows.Count > 0)
                {
                    for (int j = 1; j <= 24; j++)
                    {
                        string colName = "Hour" + j.ToString();
                        addMatrix[j - 1, 0] = MyDoubleParse(dt1.Rows[0][colName].ToString());
                        if (dt1.Rows[0][colName].ToString() == "0")
                        {
                            if (colName != "Hour24")
                            {
                                if ((MyDoubleParse(dt1.Rows[0]["Hour" + (j + 1).ToString()].ToString()) != 0.0))
                                {
                                    addMatrix[j - 1, 0] = MyDoubleParse(dt1.Rows[0]["Hour" + (j + 1).ToString()].ToString());

                                }
                                else
                                {
                                    addMatrix[j - 1, 0] = 1.0;
                                }
                            }
                            else
                            {

                                addMatrix[j - 1, 0] = 1.0;

                            }
                        }
                    }
                }
                else
                {
                    for (int k = 1; k <= 24; k++)
                    {
                        addMatrix[k - 1, 0] = 1.0;
                    }
                }

            }
            m_TehranUsageNeed = m_TehranUsageNeed.RemoveAtColumn(0);
            m_TehranUsageNeed = m_TehranUsageNeed.AddColumns(addMatrix);
        }

        //private  void GetRival()
        //{
           
        //    int trainPrice = trainingdaysprice;

        //   DateTime dateTime = baseDate;
           
        //   string rivaldate = new PersianDate(dateTime).ToString("d");
        //   string producerivaldate = new PersianDate(dateTime).ToString("d");
        //   if (isnearest)
        //   {
        //       rivaldate = FindNearRegionDate(rivaldate);
        //       producerivaldate = FindNearProduceDate(producerivaldate);
        //   }

        //    DataTable regtableNoH = null;
        //    DataTable regtableH = null;

        //    DataTable fdat = utilities.GetTable("SELECT PPID,PackageType FROM dbo.PPUnit ");
        //    string exp = "";
        //    int count = 0;
        //    for (int i = 0; i < fdat.Rows.Count; i++)
        //    {
        //        if (i != (fdat.Rows.Count - 1))
        //        {

        //            if ((fdat.Rows[i][0].ToString().Trim() == fdat.Rows[i + 1][0].ToString().Trim()) && (fdat.Rows[i][1].ToString().Trim() == "Combined Cycle" || fdat.Rows[i + 1][1].ToString().Trim() == "Combined Cycle"))
        //            {
        //                count++;
        //                exp += (int.Parse(fdat.Rows[i + 1][0].ToString()) + 1).ToString() + ":";

        //            }
        //        }
        //    }
        //    string[] exepcode = exp.Split(':');
        //    int lenght = exepcode.Length;

        //    int countrev = 0;
        //    int temp = 0;

           
        //        DataTable findrev = utilities.GetTable("Select distinct PPCode from dbo.RegionNetComp where  Date='" + rivaldate + "'AND Code='R01' AND PPCode NOT IN (SELECT PPID FROM PowerPlant)and Type!='H'");
        //       foreach(DataRow myrow in findrev.Rows)
        //       {
        //            for (int i = 0; i < lenght; i++)
        //            {
        //                if (myrow[0].ToString().Trim() == exepcode[i])
        //                {
        //                    temp++;
        //                }
        //            }
        //       }

        //           // countrev = (findrev.Rows.Count - count) + 1;
        //       countrev = (findrev.Rows.Count - temp) + 1;
         
             
            
 

        //    List<CMatrix> list = new List<CMatrix>();
        //    for (int i = 0; i < 24; i++)
        //        list.Add(new CMatrix(countrev , trainPrice - 8 + 1));



        //    for (int daysbefore = 0; daysbefore < (trainPrice - trainDaysMinusPrice)+1; daysbefore++)
        //    {
            
        //        DateTime selectedDate = PersianDateConverter.ToGregorianDateTime(rivaldate).Subtract(new TimeSpan(daysbefore, 0, 0, 0));
        //        string date14 = new PersianDate(selectedDate).ToString("d");
        //        //if (isnearest)
        //        //{
        //        //    date14 = FindNearRegionDate(date14);
        //        //}
        //        regtableH = utilities.GetTable("Select distinct PPCode from dbo.RegionNetComp where  Date='" + date14 + "'AND Code='R01' AND PPCode NOT IN (SELECT PPID FROM PowerPlant) and Type='H'");
        //        string[] revcodeh = new string[regtableH.Rows.Count];
              
        //        for (int i = 0; i < regtableH.Rows.Count; i++)
        //        {
        //            revcodeh[i] = regtableH.Rows[i][0].ToString().Trim();
        //        }



        //        regtableNoH = utilities.GetTable("Select distinct PPCode from dbo.RegionNetComp where  Date='"+date14+"'AND Code='R01' AND PPCode NOT IN (SELECT PPID FROM PowerPlant) and Type!='H'");

        //        string[] revcodeNoh = new string[regtableNoH.Rows.Count];

        //        for (int i = 0; i < regtableNoH.Rows.Count; i++)
        //        {
        //            revcodeNoh[i] = regtableNoH.Rows[i][0].ToString().Trim();
        //        }

        //        /////////////////////////////delete  132, 145////////////////////
        //        for (int i = 0; i < regtableH.Rows.Count; i++)
        //        {
        //            for (int j = 0; j < lenght; j++)
        //            {
        //                if (revcodeh[i].ToString().Trim() == exepcode[j])
        //                {
        //                    revcodeh[i] = "0";
                         
        //                }
        //            }

        //        }

        //        for (int i = 0; i < regtableNoH.Rows.Count; i++)
        //        {
        //            for (int j = 0; j < lenght; j++)
        //            {
        //                if (revcodeNoh[i].ToString().Trim() == exepcode[j])
        //                {
        //                    revcodeNoh[i] = "0";
        //                }
        //            }

        //        }

        //        ///////////////////////////////////////////////////////////////
                
        //        ArrayList revh=new ArrayList();
        //        for (int i = 0; i < regtableH.Rows.Count; i++)
        //        {
        //            if (revcodeh[i] != "0")
        //            {
        //               revh.Add(revcodeh[i]);

        //            }              
        //        }

        //        ArrayList revnoh = new ArrayList();
        //        for (int i = 0; i < regtableNoH.Rows.Count; i++)
        //        {
        //            if (revcodeNoh[i] != "0")
        //            {
        //                revnoh.Add(revcodeNoh[i]);
                     
        //            }
        //        }

        //        /////////////////////////////////sum h tye not H////////////

        //        DateTime forproduceselectedDate = PersianDateConverter.ToGregorianDateTime(producerivaldate).Subtract(new TimeSpan(daysbefore, 0, 0, 0));
        //        string forpeoducedate = new PersianDate(forproduceselectedDate).ToString("d");
        //        //if (isnearest)
        //        //{
        //        //    forpeoducedate = FindNearProduceDate(forpeoducedate);
        //        //}

        //        double[,] ARRAYSUMNOH = new double[revnoh.Count, 24];
        //        for (int I = 0; I < revnoh.Count; I++)
        //        {
        //            for (int h = 0; h < 24; h++)
        //            {
        //                DataTable SumNOH = utilities.GetTable("Select * from ProducedEnergy where  Date='"+forpeoducedate+"'AND  PPCode='" + revnoh[I] + "'");
        //                if (SumNOH.Rows.Count == 1)
        //                {
        //                    ARRAYSUMNOH[I, h] = double.Parse(SumNOH.Rows[0]["Hour" + (h + 1)].ToString());
        //                }
        //                else if(SumNOH.Rows.Count>1)
        //                {
        //                   for(int i=0;i<SumNOH.Rows.Count;i++)
        //                    {
        //                       if(i==SumNOH.Rows.Count-2)
        //                        ARRAYSUMNOH[I, h] = double.Parse(SumNOH.Rows[i]["Hour" + (h + 1)].ToString()) + double.Parse(SumNOH.Rows[i + 1]["Hour" + (h + 1)].ToString());
        //                    }
        //                }
        //                else if( SumNOH.Rows.Count== 0)
        //                {
        //                    DataTable odar = utilities.GetTable("Select * from ProducedEnergy where  Date<='" + forpeoducedate + "'AND  PPCode='" + revnoh[I] + "'");
        //                    ARRAYSUMNOH[I, h] = double.Parse(odar.Rows[0]["Hour" + (h + 1)].ToString());


        //                }
        //            }
        //        }

        //        //////////////////////////////sum h tye H//////////////////////////////

        //        double[,] ARRAYSUM = new double[revh.Count, 24];
        //        for (int I = 0; I <revh.Count; I++)
        //        {
        //            for (int h = 0; h < 24; h++)
        //            {
        //                DataTable SumH = utilities.GetTable("Select * from ProducedEnergy where  Date='"+forpeoducedate+"'AND  PPCode='" + revh[I] + "'");
        //                if (SumH.Rows.Count ==1) 
        //                {
        //                    ARRAYSUM[I, h] = double.Parse(SumH.Rows[0]["Hour" + (h + 1)].ToString());
        //                }
        //                else if(SumH.Rows.Count>1)
        //                {
        //                    for (int i = 0; i < SumH.Rows.Count; i++)
        //                    {
        //                        if (i == SumH.Rows.Count - 2)
        //                            ARRAYSUM[I, h] = double.Parse(SumH.Rows[i]["Hour" + (h + 1)].ToString()) + double.Parse(SumH.Rows[i + 1]["Hour" + (h + 1)].ToString());
        //                    }
        //                }
        //                else if (SumH.Rows.Count == 0)
        //                {
        //                    DataTable odar = utilities.GetTable("Select * from ProducedEnergy where  Date<='" + forpeoducedate + "'AND  PPCode='" + revh[I] + "'");
        //                    ARRAYSUM[I, h] = double.Parse(odar.Rows[0]["Hour" + (h + 1)].ToString());


        //                }

        //            }
        //        }
        //        double[] SumEachHour = new double[24];
        //        for (int h = 0; h < 24; h++)
        //        {
        //            for (int I = 0; I < revh.Count; I++)
        //            {
        //                SumEachHour[h] += ARRAYSUM[I, h];

        //            }
        //        }

        //        ////////////////////////////////////////////////////////////////

        //       // ArrayList OUTRES = new ArrayList();

        //        double[,] OUTRES = new double[(revnoh.Count + 1), 24];
        //        for (int i = 0; i <revnoh.Count+1; i++)
        //        {
        //            for (int h = 0; h < 24; h++)
        //            {
        //                if (i == revnoh.Count)
        //                {
        //                    OUTRES[i, h] = SumEachHour[h];
        //                }
        //                else if(i!=ARRAYSUMNOH.Length)
        //                    OUTRES[i, h] = ARRAYSUMNOH[i, h];
                    

        //            }
        //        }

        //        //////////////////////////////////

        //        double[] OUTRESone = new double[(revnoh.Count + 1) * 24];
        //        for (int i = 0; i < revnoh.Count + 1; i++)
        //        {
        //            for (int h = 0; h < 24; h++)
        //            {
        //                OUTRESone[(i * 24) + h] = OUTRES[i, h];

        //            }
        //        }

        //        for (int i = 0; i < (revnoh.Count + 1); i++)
        //        {
        //            for (int j = 1; j <= 24; j++)
        //            {

        //                (list[j - 1])[i,daysbefore]
        //                    = OUTRES[i, (j-1)];
        //            }


        //        }
                              
                
        //    }
        //    lst_Rev_Produced = list;

        //}
        private string FindNearProduceDate(string date)
        {
            DataTable odat = Utilities.GetTable("Select distinct Date from dbo.ProducedEnergy where  Date<='"+date+"'order by Date desc");
            return odat.Rows[0][0].ToString().Trim();

        }
        //private void GetRivalTrainPower()
        //{
        //    int trainPrice = trainDays;

        //    DateTime dateTime = baseDate;

        //    string rivaldate = new PersianDate(dateTime).ToString("d");
        //    string prorivaldate = new PersianDate(dateTime).ToString("d");
        //    if (isnearest)
        //    {
        //        rivaldate = FindNearRegionDate(rivaldate);
        //        prorivaldate = FindNearProduceDate(prorivaldate);
        //    }

        //    DataTable regtableNoH = null;
        //    DataTable regtableH = null;

        //    DataTable fdat = utilities.GetTable("SELECT PPID,PackageType FROM dbo.PPUnit ");
        //    string exp = "";
        //    int count = 0;
        //    for (int i = 0; i < fdat.Rows.Count; i++)
        //    {
        //        if (i != (fdat.Rows.Count - 1))
        //        {

        //            if ((fdat.Rows[i][0].ToString().Trim() == fdat.Rows[i + 1][0].ToString().Trim()) && (fdat.Rows[i][1].ToString().Trim() == "Combined Cycle" || fdat.Rows[i + 1][1].ToString().Trim() == "Combined Cycle"))
        //            {
        //                count++;
        //                exp += (int.Parse(fdat.Rows[i + 1][0].ToString()) + 1).ToString() + ":";

        //            }
        //        }
        //    }
        //    string[] exepcode = exp.Split(':');
        //    int lenght = exepcode.Length;

        //    int countrev = 0;

        //    //DataTable findrev = utilities.GetTable("Select distinct PPCode from dbo.RegionNetComp where  Date='" + rivaldate + "'AND Code='R01' AND PPCode NOT IN (SELECT PPID FROM PowerPlant)and Type!='H'");
        //    //if (findrev.Rows.Count > 0)
        //    //{
        //    //    countrev = (findrev.Rows.Count - count) + 1;
        //    //}
        //    int temp = 0;


        //    DataTable findrev = utilities.GetTable("Select distinct PPCode from dbo.RegionNetComp where  Date='" + rivaldate + "'AND Code='R01' AND PPCode NOT IN (SELECT PPID FROM PowerPlant)and Type!='H'");
        //    foreach (DataRow myrow in findrev.Rows)
        //    {
        //        for (int i = 0; i < lenght; i++)
        //        {
        //            if (myrow[0].ToString().Trim() == exepcode[i])
        //            {
        //                temp++;
        //            }
        //        }
        //    }

          
        //    countrev = (findrev.Rows.Count - temp) + 1;





        //    List<CMatrix> Powerlist = new List<CMatrix>();
        //    for (int i = 0; i < 24; i++)
        //        Powerlist.Add(new CMatrix(countrev, trainPrice - 1));



           
        //    for (int daysbefore = 0; daysbefore < (trainPrice- 1); daysbefore++)
        //    {
        //        DateTime selectedDate = PersianDateConverter.ToGregorianDateTime(rivaldate).Subtract(new TimeSpan(daysbefore, 0, 0, 0));
        //        string date14 = new PersianDate(selectedDate).ToString("d");
        //        //if (isnearest)
        //        //{
        //        //    date14 = FindNearRegionDate(date14);
        //        //}

        //        regtableH = utilities.GetTable("Select distinct PPCode from dbo.RegionNetComp where  Date='" + date14 + "'AND Code='R01' AND PPCode NOT IN (SELECT PPID FROM PowerPlant) and Type='H'");
        //        string[] revcodeh = new string[regtableH.Rows.Count];

        //        for (int i = 0; i < regtableH.Rows.Count; i++)
        //        {
        //            revcodeh[i] = regtableH.Rows[i][0].ToString().Trim();
        //        }



        //        regtableNoH = utilities.GetTable("Select distinct PPCode from dbo.RegionNetComp where  Date='" + date14 + "'AND Code='R01' AND PPCode NOT IN (SELECT PPID FROM PowerPlant) and Type!='H'");

        //        string[] revcodeNoh = new string[regtableNoH.Rows.Count];

        //        for (int i = 0; i < regtableNoH.Rows.Count; i++)
        //        {
        //            revcodeNoh[i] = regtableNoH.Rows[i][0].ToString().Trim();
        //        }

        //        ///////////////////delete  132, 145////////////////////
        //        for (int i = 0; i < regtableH.Rows.Count; i++)
        //        {
        //            for (int j = 0; j < lenght; j++)
        //            {
        //                if (revcodeh[i].ToString().Trim() == exepcode[j])
        //                {
        //                    revcodeh[i] = "0";

        //                }
        //            }

        //        }

        //        for (int i = 0; i < regtableNoH.Rows.Count; i++)
        //        {
        //            for (int j = 0; j < lenght; j++)
        //            {
        //                if (revcodeNoh[i].ToString().Trim() == exepcode[j])
        //                {
        //                    revcodeNoh[i] = "0";
        //                }
        //            }

        //        }

        //        /////////////////////////////////////////////////////

        //        ArrayList revh = new ArrayList();
        //        for (int i = 0; i < regtableH.Rows.Count; i++)
        //        {
        //            if (revcodeh[i] != "0")
        //            {
        //                revh.Add(revcodeh[i]);

        //            }
        //        }

        //        ArrayList revnoh = new ArrayList();
        //        for (int i = 0; i < regtableNoH.Rows.Count; i++)
        //        {
        //            if (revcodeNoh[i] != "0")
        //            {
        //                revnoh.Add(revcodeNoh[i]);

        //            }
        //        }

        //        ///////////////////////sum h tye not H////////////

        //        DateTime forproduceselectedDate = PersianDateConverter.ToGregorianDateTime(prorivaldate).Subtract(new TimeSpan(daysbefore, 0, 0, 0));
        //        string forpeoducedate = new PersianDate(forproduceselectedDate).ToString("d");
        //        //if (isnearest)
        //        //{
        //        //    forpeoducedate = FindNearProduceDate(forpeoducedate);
        //        //}

        //        double[,] ARRAYSUMNOH = new double[revnoh.Count, 24];
        //        for (int I = 0; I < revnoh.Count; I++)
        //        {
        //            for (int h = 0; h < 24; h++)
        //            {
        //                DataTable SumNOH = utilities.GetTable("Select * from ProducedEnergy where  Date='" + forpeoducedate + "'AND  PPCode='" + revnoh[I] + "'");
        //                if (SumNOH.Rows.Count == 1)
        //                {
        //                    ARRAYSUMNOH[I, h] = double.Parse(SumNOH.Rows[0]["Hour" + (h + 1)].ToString());
        //                }
        //                else if (SumNOH.Rows.Count > 1)
        //                {
        //                    for (int i = 0; i < SumNOH.Rows.Count; i++)
        //                    {
        //                        if (i == SumNOH.Rows.Count - 2)
        //                            ARRAYSUMNOH[I, h] = double.Parse(SumNOH.Rows[i]["Hour" + (h + 1)].ToString()) + double.Parse(SumNOH.Rows[i + 1]["Hour" + (h + 1)].ToString());
        //                    }
        //                }
        //                else if (SumNOH.Rows.Count == 0)
        //                {
        //                    DataTable odar = utilities.GetTable("Select * from ProducedEnergy where  Date<='" + forpeoducedate + "'AND  PPCode='" + revnoh[I] + "'");
        //                    ARRAYSUMNOH[I, h] = double.Parse(odar.Rows[0]["Hour" + (h + 1)].ToString());


        //                }
        //            }
        //        }

        //        ////////////////////sum h tye H//////////////////////////////

        //        double[,] ARRAYSUM = new double[revh.Count, 24];
        //        for (int I = 0; I < revh.Count; I++)
        //        {
        //            for (int h = 0; h < 24; h++)
        //            {
        //                DataTable SumH = utilities.GetTable("Select * from ProducedEnergy where  Date='" + forpeoducedate + "'AND  PPCode='" + revh[I] + "'");
        //                if (SumH.Rows.Count == 1)
        //                {
        //                    ARRAYSUM[I, h] = double.Parse(SumH.Rows[0]["Hour" + (h + 1)].ToString());
        //                }
        //                else if (SumH.Rows.Count > 1)
        //                {
        //                    for (int i = 0; i < SumH.Rows.Count; i++)
        //                    {
        //                        if (i == SumH.Rows.Count - 2)
        //                            ARRAYSUM[I, h] = double.Parse(SumH.Rows[i]["Hour" + (h + 1)].ToString()) + double.Parse(SumH.Rows[i + 1]["Hour" + (h + 1)].ToString());
        //                    }
        //                }
        //                else if (SumH.Rows.Count == 0)
        //                {
        //                    DataTable odar = utilities.GetTable("Select * from ProducedEnergy where  Date<='" + forpeoducedate + "'AND  PPCode='" + revh[I] + "'");
        //                    ARRAYSUM[I, h] = double.Parse(odar.Rows[0]["Hour" + (h + 1)].ToString());


        //                }

        //            }
        //        }
        //        double[] SumEachHour = new double[24];
        //        for (int h = 0; h < 24; h++)
        //        {
        //            for (int I = 0; I < revh.Count; I++)
        //            {
        //                SumEachHour[h] += ARRAYSUM[I, h];

        //            }
        //        }

        //        //////////////////////////////////////////////////////



        //        double[,] OUTRES = new double[(revnoh.Count + 1), 24];
        //        for (int i = 0; i < revnoh.Count + 1; i++)
        //        {
        //            for (int h = 0; h < 24; h++)
        //            {
        //                if (i == revnoh.Count)
        //                {
        //                    OUTRES[i, h] = SumEachHour[h];
        //                }
        //                else if (i != ARRAYSUMNOH.Length)
        //                    OUTRES[i, h] = ARRAYSUMNOH[i, h];


        //            }
        //        }

        //        ////////////////////////

        //        double[] OUTRESone = new double[(revnoh.Count + 1) * 24];
        //        for (int i = 0; i < revnoh.Count + 1; i++)
        //        {
        //            for (int h = 0; h < 24; h++)
        //            {
        //                OUTRESone[(i * 24) + h] = OUTRES[i, h];

        //            }
        //        }

        //        for (int i = 0; i < (revnoh.Count + 1); i++)
        //        {
        //            for (int j = 1; j <= 24; j++)
        //            {

        //                (Powerlist[j - 1])[i, daysbefore]
        //                    = OUTRES[i, (j - 1)];
        //            }


        //        }



        //    }
        //    lst_Rev_Produced_Power = Powerlist;

        //}
     
        private string FindNearRegionDate(string date)
        {
            DataTable odat = Utilities.GetTable("Select distinct Date from dbo.RegionNetComp where Date<='" + date + "'AND Code='"+Gencode+"' order by Date desc");
            return odat.Rows[0][0].ToString().Trim();

        }
        private string FindNearManategh(string date)
        {
            DataTable odat = Utilities.GetTable("Select distinct Date from Manategh where Date<='" + date + "'AND Code='"+Gencode+"' order by Date desc");
            return odat.Rows[0][0].ToString().Trim();
        }
        //private void CalculateRIVAL()
        //{
        //    int number = exeptcc();
        //    CMatrix forcal = new CMatrix(24, trainDays - 1);
        //    CMatrix sample = m_Power_TehranProduced_rival;
        //  for(int t=0;t<trainDays-1;t++)
        //  {
        //      for(int i=0;i<24;i++)
        //       {
        //            for(int r=0;r<number;r++)
        //            {
        //                forcal[i, t] += lst_Rev_Produced_Power[i][r, t];
                 
        //            }
        //       }
        //   }

        //    for (int t = 0; t < trainDays - 1; t++)
        //    {
        //        for (int i = 0; i < 24; i++)
        //        {
        //           sample[i,t] = sample[i,t] - forcal[i, t];
                  
        //        }
        //    }

        //    m_Power_TehranProduced_final_minuspower =sample;

        //}
        private string FindNearInterchange(string date)
        {
          //int lineTypesNo = Enum.GetNames(typeof(Line_codes)).Length;
          // DateTime tempDate = baseDate.Subtract(new TimeSpan(1, 0, 0, 0));
          //  string endDate = new PersianDate(tempDate).ToString("d");
          //  string startDate = new PersianDate(tempDate.Subtract(new TimeSpan(trainingdaysprice, 0, 0, 0))).ToString("d");

            //for (int index = 1; index <= lineTypesNo; index++)
            //{
            //    Line_codes line = (Line_codes)Enum.Parse(typeof(Line_codes), index.ToString());

            DataTable odat = Utilities.GetTable("Select distinct Date FROM [InterchangedEnergy] WHERE Date<='" + date + "'AND Code!='' ORDER BY Date desc ");
                if (odat.Rows.Count > 0)
                {
                    return odat.Rows[0][0].ToString().Trim();
                }
            //}
            return null;
        }
        //private int exeptcc()
        //{
        //    DateTime dateTime = baseDate;
        //    string rivaldate = new PersianDate(dateTime).ToString("d");
        //    if (isnearest)
        //    {
        //        rivaldate = FindNearRegionDate(rivaldate);
        //    }

        //    DataTable fdat = utilities.GetTable("SELECT PPID,PackageType FROM dbo.PPUnit ");
        //    string exp = "";
        //    int count = 0;
        //    for (int i = 0; i < fdat.Rows.Count; i++)
        //    {
        //        if (i != (fdat.Rows.Count - 1))
        //        {

        //            if ((fdat.Rows[i][0].ToString().Trim() == fdat.Rows[i + 1][0].ToString().Trim()) && (fdat.Rows[i][1].ToString().Trim() == "Combined Cycle" || fdat.Rows[i + 1][1].ToString().Trim() == "Combined Cycle"))
        //            {
        //                count++;
        //                exp += (int.Parse(fdat.Rows[i + 1][0].ToString()) + 1).ToString() + ":";

        //            }
        //        }
        //    }
        //    string[] exepcode = exp.Split(':');
        //    int lenght = exepcode.Length;

        //    int countrev = 0;

        //    //DataTable findrev = utilities.GetTable("Select distinct PPCode from dbo.RegionNetComp where  Date='" + rivaldate + "'AND Code='R01' AND PPCode NOT IN (SELECT PPID FROM PowerPlant)and Type!='H'");
        //    //if (findrev.Rows.Count > 0)
        //    //{
        //    //    countrev = (findrev.Rows.Count - count) + 1;
        //    //}
        //    int temp = 0;


        //    DataTable findrev = utilities.GetTable("Select distinct PPCode from dbo.RegionNetComp where  Date='" + rivaldate + "'AND Code='R01' AND PPCode NOT IN (SELECT PPID FROM PowerPlant)and Type!='H'");
        //    foreach (DataRow myrow in findrev.Rows)
        //    {
        //        for (int i = 0; i < lenght; i++)
        //        {
        //            if (myrow[0].ToString().Trim() == exepcode[i])
        //            {
        //                temp++;
        //            }
        //        }
        //    }


        //    countrev = (findrev.Rows.Count - temp) + 1;
        //    // countrev = (findrev.Rows.Count - count) + 1;

        //    return countrev;



        //}


        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
      
    }
}
