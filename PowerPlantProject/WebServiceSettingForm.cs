﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using NRI.SBS.Common;
using FarsiLibrary.Utils;
using System.Collections;
using System.IO;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using Auto_Update_Library;
using PowerPlantProject.New;


namespace PowerPlantProject
{
    public partial class WebServiceSettingForm : Form
    {




        public WebServiceSettingForm()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void WebServiceSettingForm_Load(object sender, EventArgs e)
        {
            try
            {
                WebServiceSetting webServiceSetting = WebServiceSetting.GetSetting();
                txtUsername.Text = webServiceSetting.Username;
                txtPassword.Text = webServiceSetting.Password;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                WebServiceSetting webServiceSetting = new WebServiceSetting() { Username = txtUsername.Text, Password = txtPassword.Text };
                WebServiceSetting.SaveSetting(webServiceSetting);
                MessageBox.Show("Setting Saved");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }














    }
}
