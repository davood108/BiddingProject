﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using Dundas.Charting.WinControl.ChartTypes;
using Dundas.Charting.WinControl;

namespace PowerPlantProject
{
    partial class MainForm
    {
        public MaintenanceOutData maintData;

        private void LoadMaintenance()
        {
            try
            {
                if (User.getUser().Role == DefinedRoles.NoMaintenance ||
                    User.getUser().Role == DefinedRoles.ExceptComputational ||
                    User.getUser().Role == DefinedRoles.Unassigned ||
                User.getUser().Role == DefinedRoles.PlantUser)
                {
                    MainTabs.TabPages.Remove(MainTabs.TabPages["maintenance"]);
                    addToolStripMenuItem.Enabled = false;
                    DeletetoolStripMenuItem.Enabled = false;
                    
                }
            }
            catch { }
            
                            try
            {
                if (User.getUser().Role == DefinedRoles.NoMaintenance ||
                    User.getUser().Role == DefinedRoles.ExceptComputational ||
                    User.getUser().Role == DefinedRoles.Unassigned ||
                User.getUser().Role == DefinedRoles.PlantUser)
                {
                    MainTabs.TabPages.Remove(MainTabs.TabPages["tbPageMaintenance"]);
                    addToolStripMenuItem.Enabled = false;
                    DeletetoolStripMenuItem.Enabled = false;
                }
            }
            catch { }
            lblMaintPlant.Text = "All";
            lblMainStartDate.Text = new PersianDate(DateTime.Now).ToString("d");

            lblMaintEndDate.Text = new PersianDate(DateTime.Now.AddYears(1)).ToString("d");
            EnableMaintenanceGraph(false);
           // cmbStarategyMaintenance.Items.AddRange(Enum.GetNames(typeof(MaintenanceStratedgy)));

        }

        private void lblMaintPlant_TextChanged(object sender, EventArgs e)
        {
            FillMaintenanceStatusBox();
        }

        //private void FillMaintenanceStatusBox()
        //{

        //    if (persianDate != null)
        //    {
        //        string strDate = PersianDateConverter.ToPersianDate(persianDate).ToString("d");

        //        lstStatus.Text = "";
        //        if (lblPlantValue.Text.Trim() != "All" && strDate != "")
        //        {
        //            CheckMaintInfoAvailability();
                    

        //        }
        //    }
        //}

        private void FillMaintenanceStatusBox()
        {
            int MonthlyThreshold=20;
            txtMaintStatus.Text = "";

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);

            string[] Tables = { "MainFRM002", "MainFRM005", "MainFRM009" };

            List<string> ppidList = new List<string>();
            List<string> ppNameList = new List<string>();
            if (lblMaintPlant.Text.Trim() != "All")
            {
                ppidList.Add(PPID);
                ppNameList.Add(lblMaintPlant.Text.Trim());
            }
            else
            {
                string strCmd = "SELECT ppid,ppname from powerplant";
                DataTable dt = Utilities.GetTable(strCmd);
                foreach (DataRow row in dt.Rows)
                {
                    ppidList.Add(row["ppid"].ToString().Trim());
                    ppNameList.Add(row["ppname"].ToString().Trim());
                }
            }


            for (int k=0; k<ppidList.Count;k++)
            {

                DateTime montlyDate = DateTime.Now.Subtract(new TimeSpan(365,0,0,0));

                for (int month = 0; month < 12; month++)
                {
                    bool[] resultAvailable = new bool[3];

                    montlyDate = montlyDate.AddMonths(1);
                    PersianDate p= new PersianDate(montlyDate);
                    string substringDate = p.ToString("d").Remove(7);

                    string strCmdFirst = "SELECT count(*) from ";
                    string strCmdEnd = " as table1 where ppid='" + ppidList[k] + "'" +
                        " and table1.TargetMarketDate like '" + substringDate + "%'";

                    for (int i = 0; i < 3 ; i++)
                    {
                        string cmd =strCmdFirst + Tables[i] + strCmdEnd;
                        int count = int.Parse(Utilities.GetTable(cmd).Rows[0][0].ToString());
                        if (count < MonthlyThreshold)
                            resultAvailable[i] = false;

                    }
                    
                    if (!(resultAvailable[0]&&resultAvailable[1]&&resultAvailable[2]))
                    {
                        txtMaintStatus.Text += ppNameList[k] + " data in " + substringDate + " :\r\n";
                        for (int i=0; i<3; i++)
                            if (!resultAvailable[i])
                                txtMaintStatus.Text += "    " + Tables[i].Remove(0,6) + " data not complete.\r\n";
                    }
                        

                }
            }
        }

        private void btnRunMaint_Click(object sender, EventArgs e)
        {

            deleteStackedBarChartPlant();
            //FillStackedBarChartTest();
            ///??????????????????????????
            if (lblMaintPlant.Text.Trim() == "")
            {
                MessageBox.Show("Choose a plant!");
            }
            //else if (cmbStarategyMaintenance.SelectedItem == null)
            //{
            //    MessageBox.Show("Choose maintenance strategy!");
            //}
            else
            {
               
                MaintenanceStratedgy cmb = (MaintenanceStratedgy)Enum.Parse(typeof(MaintenanceStratedgy), "Normal");
                MaintenanceProgressForm progressForm = new MaintenanceProgressForm(this, lblMaintPlant.Text.Trim(), cmb);
                progressForm.Show();
            }

            //maintenance m = new maintenance(lblMaintPlant.Text);
            //m.get();
            //FillStackedBarChartPlant(0, m);
            //FillStackedBarChart(m);
        }
        
        public void EnableMaintenanceGraph(bool enable)
        {
            chart2.Enabled = enable;
            cmbMaintShownPlant.Visible = enable;
            if (enable)
            {
                cmbMaintShownPlant.Items.Clear();
                cmbMaintShownPlant.Text = "";
                if (maintData!=null)
                    cmbMaintShownPlant.Items.AddRange(maintData.PlantsName);
            }
        }
        public void FillStackedBarChartPlant(int plantIndex)
        {
            if (maintData.AvailableData[plantIndex] == false)
            {
                MessageBox.Show("Incomplete Maintenance Data in General Tab!");
                chart2.Series.Clear();
                return;
            }
            int[] Plant_unitsNum = maintData.Plants_UnitsNum;
            string[,] Plant_UnitCode = maintData.Plants_UnitCodes;
            int Plants_Num = maintData.Plants.Length;
            string[] plants = maintData.Plants;

            CMatrix Result_UnitsNum_Week = maintData.Result_Plants_UnitsNum_Week[plantIndex];
            DataTable oDataTable;


            double value = 0;
            string SeriesName1 = "Series"; //item + "_" + SeriesName1;
            string SeriesName = "";
            chart2.Series.Clear();
            for (int i = 0; i < 53; i++)
            {
                SeriesName = SeriesName1 + i;
                chart2.Series.Add(SeriesName);
                chart2.Series[SeriesName].Type =
                    Dundas.Charting.WinControl.SeriesChartType.StackedBar;
                chart2.Series[SeriesName].Points.Clear();
            }

            chart2.Legends.Clear();
            //Dundas.Charting.WinControl.Legend lg =
            //    new Dundas.Charting.WinControl.Legend(ReportItem);
            //lg.Title = ReportItem;
            //chart2.Legends.Add(lg);

            // Enable X axis margin
            chart2.ChartAreas["Default"].AxisY.Margin = false;
            chart2.ChartAreas["Default"].AxisY.Minimum = 0;
            chart2.ChartAreas["Default"].AxisY.Interval = 1;
            chart2.ChartAreas["Default"].AxisY.Maximum = 53;
            chart2.ChartAreas["Default"].AxisX.IntervalType =
                Dundas.Charting.WinControl.DateTimeIntervalType.Number;
            chart2.ChartAreas["Default"].AxisX.Margin = true;
            chart2.ChartAreas["Default"].AxisX.Interval = 1;
            chart2.ChartAreas["Default"].AxisX.Minimum = 0;
            chart2.ChartAreas["Default"].AxisX.Maximum = maintData.Plants_UnitsNum[plantIndex] + 1;


            //if (cmbUnit_Line.Text.Trim() == "%")
            //    chart2.ChartAreas["Default"].AxisX.Maximum = cmbUnit_Line.Items.Count + 1;
            //else
            //    chart2.ChartAreas["Default"].AxisX.Maximum = 2;
            // Show as 3D

            chart2.ChartAreas["Default"].Area3DStyle.Enable3D = false;
            // Set Title Names
            chart2.ChartAreas["Default"].AxisX.Title = "Unit" /*+ ReportItem*/;
            chart2.ChartAreas["Default"].AxisY.Title = "Week";
            chart2.Titles.Clear();

            //chart2.Titles.Add(GetModuleDescription(_Module) +
            //    "  [ Project : " + Vars.ProjectCode + ",  Genco : " +
            //    cmbGenco_Area.Text.Trim() + ",  Unit : " + cmbUnit_Line.Text.Trim() + "]");

            for (int i = 0; i < 53; i++)
            {
                for (int k = 0; k < Plant_unitsNum[plantIndex]; k++)
                {
                    string item = Plant_UnitCode[plantIndex, k];

                    //if (item == "%")
                    //    continue;
                    //if (Unit_Line_Bus != "%" && Unit_Line_Bus != item.Trim())
                    //    continue;


                    chart2.Series[SeriesName1 + i]["PixelPointWidth"] = "10";

                    string Item1 = "";
                    value = Result_UnitsNum_Week[k, i];

                    SeriesName = SeriesName1 + i;
                    chart2.Series[SeriesName].Points.AddXY(item, 1);
                    if (value == 0)
                        chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Transparent;
                    else
                        chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Black;

                    //foreach (DataRow row in drs)
                    //{
                    //    int WeekPart = Convert.ToInt32(row["WeeksPart"]);
                    //    if (Is_UNIT_LINE_BUS == "UNIT")
                    //        Item1 = row["UnitCode"].ToString().Trim();
                    //    else if (Is_UNIT_LINE_BUS == "LINE")
                    //        Item1 = row["LineCode"].ToString().Trim();
                    //    if (Item1.Trim() != item.Trim())
                    //        continue;

                    //    for (int h = 1; h <= 13; h++)
                    //    {

                    //        value = Funcs.GetDouble(row["Week" + h].ToString());
                    //        int h1 = 0;
                    //        h1 = (WeekPart - 1) * 13 + h;
                    //        if (i != h1)
                    //            continue;

                    //        SeriesName = SeriesName1 + i;
                    //        chart2.Series[SeriesName].Points.AddXY(item, 1);
                    //        if (value == 0)
                    //            chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Transparent;
                    //        else
                    //            chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Black;
                    //    }
                    //}
                }
            }

        }


        public void deleteStackedBarChartPlant()
        {           
                chart2.Series.Clear();
                    
        }
        //public void FillStackedBarChartPlant(int plantIndex)
        //{
        //    if (maintData.AvailableData[plantIndex] == false)
        //    {
        //        MessageBox.Show("Incomplete Maintenance Data in General Tab!");
        //        chart2.Series.Clear();
        //        return;
        //    }
        //    int[] Plant_unitsNum = maintData.Plants_UnitsNum;
        //    string[,] Plant_UnitCode = maintData.Plants_UnitCodes;
        //    int Plants_Num = maintData.Plants.Length;
        //    string[] plants = maintData.Plants;

        //    CMatrix Result_UnitsNum_Week = maintData.Result_Plants_UnitsNum_Week[plantIndex];
        //    DataTable oDataTable;


        //    double value = 0;
        //    string SeriesName1 = "Series"; //item + "_" + SeriesName1;
        //    string SeriesName = "";
        //    chart2.Series.Clear();
        //    for (int i = 0; i < 53; i++)
        //    {
        //        SeriesName = SeriesName1 + i;
        //        chart2.Series.Add(SeriesName);
        //        chart2.Series[SeriesName].Type =
        //            Dundas.Charting.WinControl.SeriesChartType.StackedBar;
        //        chart2.Series[SeriesName].Points.Clear();
        //    }

        //    chart2.Legends.Clear();
        //    //Dundas.Charting.WinControl.Legend lg =
        //    //    new Dundas.Charting.WinControl.Legend(ReportItem);
        //    //lg.Title = ReportItem;
        //    //chart2.Legends.Add(lg);

        //    // Enable X axis margin
        //    chart2.ChartAreas["Default"].AxisY.Margin = false;
        //    chart2.ChartAreas["Default"].AxisY.Minimum = 0;
        //    chart2.ChartAreas["Default"].AxisY.Interval = 1;
        //    chart2.ChartAreas["Default"].AxisY.Maximum = 53;
        //    chart2.ChartAreas["Default"].AxisX.IntervalType =
        //        Dundas.Charting.WinControl.DateTimeIntervalType.Number;
        //    chart2.ChartAreas["Default"].AxisX.Margin = true;
        //    chart2.ChartAreas["Default"].AxisX.Interval = 1;
        //    chart2.ChartAreas["Default"].AxisX.Minimum = 0;
        //    chart2.ChartAreas["Default"].AxisX.Maximum = maintData.Plants_UnitsNum[0]+1;


        //    //if (cmbUnit_Line.Text.Trim() == "%")
        //    //    chart2.ChartAreas["Default"].AxisX.Maximum = cmbUnit_Line.Items.Count + 1;
        //    //else
        //    //    chart2.ChartAreas["Default"].AxisX.Maximum = 2;
        //    // Show as 3D

        //    chart2.ChartAreas["Default"].Area3DStyle.Enable3D = false;
        //    // Set Title Names
        //    chart2.ChartAreas["Default"].AxisX.Title = "Unit" /*+ ReportItem*/;
        //    chart2.ChartAreas["Default"].AxisY.Title = "Week";
        //    chart2.Titles.Clear();

        //    //chart2.Titles.Add(GetModuleDescription(_Module) +
        //    //    "  [ Project : " + Vars.ProjectCode + ",  Genco : " +
        //    //    cmbGenco_Area.Text.Trim() + ",  Unit : " + cmbUnit_Line.Text.Trim() + "]");

        //    for (int i = 0; i < 53; i++)
        //    {
        //        for (int k = 0; k < Plant_unitsNum[0]; k++)
        //        {
        //            string item = Plant_UnitCode[0, k];

        //            //if (item == "%")
        //            //    continue;
        //            //if (Unit_Line_Bus != "%" && Unit_Line_Bus != item.Trim())
        //            //    continue;


        //            chart2.Series[SeriesName1 + i]["PixelPointWidth"] = "10";

        //            string Item1 = "";
        //            value = Result_UnitsNum_Week[ k, i];

        //            SeriesName = SeriesName1 + i;
        //            chart2.Series[SeriesName].Points.AddXY(item, 1);
        //            if (value == 0)
        //                chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Transparent;
        //            else
        //                chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Black;

        //            //foreach (DataRow row in drs)
        //            //{
        //            //    int WeekPart = Convert.ToInt32(row["WeeksPart"]);
        //            //    if (Is_UNIT_LINE_BUS == "UNIT")
        //            //        Item1 = row["UnitCode"].ToString().Trim();
        //            //    else if (Is_UNIT_LINE_BUS == "LINE")
        //            //        Item1 = row["LineCode"].ToString().Trim();
        //            //    if (Item1.Trim() != item.Trim())
        //            //        continue;

        //            //    for (int h = 1; h <= 13; h++)
        //            //    {

        //            //        value = Funcs.GetDouble(row["Week" + h].ToString());
        //            //        int h1 = 0;
        //            //        h1 = (WeekPart - 1) * 13 + h;
        //            //        if (i != h1)
        //            //            continue;

        //            //        SeriesName = SeriesName1 + i;
        //            //        chart2.Series[SeriesName].Points.AddXY(item, 1);
        //            //        if (value == 0)
        //            //            chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Transparent;
        //            //        else
        //            //            chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Black;
        //            //    }
        //            //}
        //        }
        //    }

        //}

        private void FillChart()
        {
            /*
            DataTable oDataTable = utilities.GetTable("select power from pow");
            chart2.Series.RemoveAt(chart2.Series.Count - 1);
            //Random random = new Random();
            Series series = chart2.Series.Add("Series1");
            for (int pointIndex = 0; pointIndex < 4; pointIndex++)
            {
                string[] arr = new string[4];
                string[] arr2 = new string[4];
                arr2[1] = "0";
                arr2[2] = "2";
                arr2[3] = "5";
                arr2[0] = "6";
                arr[pointIndex] = oDataTable.Rows[pointIndex][0].ToString();
                chart2.Series["Series1"].Points.AddXY(arr[pointIndex], arr2[pointIndex]);
                //chart2.Series["Series2"].Points.AddY(random.Next(5, 75));
            }

            // Set series chart type
            chart2.Series["Series1"].Type = SeriesChartType.StepLine;
            //chart2.Series["Series2"].Type = SeriesChartType.StepLine;

            // Set point labels
            chart2.Series["Series1"].ShowLabelAsValue = true;
            //chart2.Series["Series2"].ShowLabelAsValue = true;

            // Enable X axis margin
            chart2.ChartAreas["Default"].AxisX.Margin = true;
            // chart2.Series["Series1"].Enabled = false;

            */
            

          
            //Random random = new Random();
            Series series = chart2.Series.Add("Series1");
            chart2.Series["Series1"].Points.AddXY(1, 2);
            chart2.Series["Series1"].Points.AddXY(2, 3);
            //chart2.Series["Series1"].Points.AddXY(arr[pointIndex], arr2[pointIndex]);
            //chart2.Series["Series1"].Points.AddXY(arr[pointIndex], arr2[pointIndex]);

            //for (int pointIndex = 0; pointIndex < 4; pointIndex++)
            //{
            //    string[] arr = new string[4];
            //    string[] arr2 = new string[4];
            //    arr2[1] = "0";
            //    arr2[2] = "2";
            //    arr2[3] = "5";
            //    arr2[0] = "6";
            //    arr[pointIndex] = oDataTable.Rows[pointIndex][0].ToString();
            //    chart2.Series["Series1"].Points.AddXY(arr[pointIndex], arr2[pointIndex]);
            //    //chart2.Series["Series2"].Points.AddY(random.Next(5, 75));
            //}

            // Set series chart type
            chart2.Series["Series1"].Type = SeriesChartType.StepLine;
            //chart2.Series["Series2"].Type = SeriesChartType.StepLine;

            // Set point labels
            chart2.Series["Series1"].ShowLabelAsValue = true;
            //chart2.Series["Series2"].ShowLabelAsValue = true;

            // Enable X axis margin
            chart2.ChartAreas["Default"].AxisX.Margin = true;
            // chart2.Series["Series1"].Enabled = false;
      
        }

        //private void FillStackedBarChart(maintenance oMaintenance)
        //{
        //    double value = 0;
        //    string SeriesName1 = "Series"; //item + "_" + SeriesName1;
        //    string SeriesName = "";
        //    chart2.Series.Clear();
        //    for (int i = 1; i <= 52; i++)
        //    {
        //        SeriesName = SeriesName1 + i;
        //        chart2.Series.Add(SeriesName);
        //        chart2.Series[SeriesName].Type =
        //            Dundas.Charting.WinControl.SeriesChartType.StackedBar;
        //        chart2.Series[SeriesName].Points.Clear();
        //    }

        //    //chart2.Legends.Clear();
        //    //Dundas.Charting.WinControl.Legend lg =
        //    //    new Dundas.Charting.WinControl.Legend(ReportItem);
        //    //lg.Title = ReportItem;
        //    //chart2.Legends.Add(lg);

        //    // Enable X axis margin
        //    chart2.ChartAreas["Default"].AxisY.Margin = false;
        //    chart2.ChartAreas["Default"].AxisY.Minimum = 0;
        //    chart2.ChartAreas["Default"].AxisY.Interval = 1;
        //    chart2.ChartAreas["Default"].AxisY.Maximum = 52;
        //    chart2.ChartAreas["Default"].AxisX.IntervalType =
        //        Dundas.Charting.WinControl.DateTimeIntervalType.Number;
        //    chart2.ChartAreas["Default"].AxisX.Margin = true;
        //    chart2.ChartAreas["Default"].AxisX.Interval = 1;
        //    chart2.ChartAreas["Default"].AxisX.Minimum = 0;


        //    //if (cmbUnit_Line.Text.Trim() == "%")
        //    //    chart2.ChartAreas["Default"].AxisX.Maximum = cmbUnit_Line.Items.Count + 1;
        //    //else
        //    //    chart2.ChartAreas["Default"].AxisX.Maximum = 2;
        //    // Show as 3D
            
        //    chart2.ChartAreas["Default"].Area3DStyle.Enable3D = false;
        //    // Set Title Names
        //    chart2.ChartAreas["Default"].AxisX.Title = "Unit - " /*+ ReportItem*/;
        //    chart2.ChartAreas["Default"].AxisY.Title = "Week";
        //    chart2.Titles.Clear();

        //    //chart2.Titles.Add(GetModuleDescription(_Module) +
        //    //    "  [ Project : " + Vars.ProjectCode + ",  Genco : " +
        //    //    cmbGenco_Area.Text.Trim() + ",  Unit : " + cmbUnit_Line.Text.Trim() + "]");

        //    for (int i = 0; i < 53; i++)
        //    {
        //        for (int k = 0; k < oMaintenance.Plants_UnitsNum[0]; k++ )
        //        {
        //            string item = oMaintenance.Plants_UnitCodes[0, k];

        //            //if (item == "%")
        //            //    continue;
        //            //if (Unit_Line_Bus != "%" && Unit_Line_Bus != item.Trim())
        //            //    continue;


        //            chart2.Series[SeriesName1 + i]["PixelPointWidth"] = "10";

        //            string Item1 = "";
        //            value = oMaintenance.Result_Plants_UnitsNum_Week[0, k, i];
       
        //            SeriesName = SeriesName1 + i;
        //            chart2.Series[SeriesName].Points.AddXY(item, 1);
        //            if (value == 0)
        //                chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Transparent;
        //            else
        //                chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Black;

        //            //foreach (DataRow row in drs)
        //            //{
        //            //    int WeekPart = Convert.ToInt32(row["WeeksPart"]);
        //            //    if (Is_UNIT_LINE_BUS == "UNIT")
        //            //        Item1 = row["UnitCode"].ToString().Trim();
        //            //    else if (Is_UNIT_LINE_BUS == "LINE")
        //            //        Item1 = row["LineCode"].ToString().Trim();
        //            //    if (Item1.Trim() != item.Trim())
        //            //        continue;

        //            //    for (int h = 1; h <= 13; h++)
        //            //    {

        //            //        value = Funcs.GetDouble(row["Week" + h].ToString());
        //            //        int h1 = 0;
        //            //        h1 = (WeekPart - 1) * 13 + h;
        //            //        if (i != h1)
        //            //            continue;

        //            //        SeriesName = SeriesName1 + i;
        //            //        chart2.Series[SeriesName].Points.AddXY(item, 1);
        //            //        if (value == 0)
        //            //            chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Transparent;
        //            //        else
        //            //            chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Black;
        //            //    }
        //            //}
        //        }
        //    }

        //}

        private void FillStackedBarChartTest()
        {
            string[] plant;
            int[] Plant_unitsNum;
            string[,] Plant_UnitCode;
            double[, ,]  Result_Plants_UnitsNum_Week;
            DataTable oDataTable;
            int Max_UnitsNum=0;
            int Plants_Num = 0;

            if (lblMaintPlant.Text.ToLower().Trim() == "all")
            {
                oDataTable = Utilities.GetTable("select distinct PPID from UnitsDataMain ");
                Plants_Num = oDataTable.Rows.Count;

                plant = new string[Plants_Num];
                for (int il = 0; il < Plants_Num; il++)
                {
                    plant[il] = oDataTable.Rows[il][0].ToString().Trim();

                }
            }
            else
            {
                oDataTable = Utilities.GetTable("SELECT PPID FROM PowerPlant WHERE PPName='" + lblMaintPlant.Text.ToLower().Trim() + "'");

                Plants_Num = 1;
                plant = new string[1];
                plant[0] = oDataTable.Rows[0][0].ToString().Trim();

            }
            //////////////////////////
            Plant_unitsNum = new int[Plants_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                oDataTable = Utilities.GetTable("select UnitCode from UnitsDataMain where PPID='" + plant[j] + "'");

                Plant_unitsNum[j] = oDataTable.Rows.Count;
                if (Max_UnitsNum <= oDataTable.Rows.Count)
                {
                    Max_UnitsNum = oDataTable.Rows.Count;
                }


            }

            ////////////////////////
            Plant_UnitCode = new string[Plants_Num, Max_UnitsNum];
            Result_Plants_UnitsNum_Week = new double[Plants_Num, Max_UnitsNum, 53];
            for (int j = 0; j < Plants_Num; j++)
            {
                oDataTable = Utilities.GetTable("select UnitCode from UnitsDataMain where  PPID='" + plant[j] + "'");
                for (int u = 0; u < Plant_unitsNum[j]; u++)
                {
                    Plant_UnitCode[j, u] = oDataTable.Rows[u][0].ToString().Trim();

                    for (int w = 0; w < 53; w++)
                    {
                        Result_Plants_UnitsNum_Week[j, u, w] = (w+j+u) % 3;
                    }

                }
            }
            /////////////////////////////////////

            double value = 0;
            string SeriesName1 = "Series"; //item + "_" + SeriesName1;
            string SeriesName = "";
            chart2.Series.Clear();
            for (int i = 0; i < 53; i++)
            {
                SeriesName = SeriesName1 + i;
                chart2.Series.Add(SeriesName);
                chart2.Series[SeriesName].Type =
                    Dundas.Charting.WinControl.SeriesChartType.StackedBar;
                chart2.Series[SeriesName].Points.Clear();
            }

            //chart2.Legends.Clear();
            //Dundas.Charting.WinControl.Legend lg =
            //    new Dundas.Charting.WinControl.Legend(ReportItem);
            //lg.Title = ReportItem;
            //chart2.Legends.Add(lg);

            // Enable X axis margin
            chart2.ChartAreas["Default"].AxisY.Margin = false;
            chart2.ChartAreas["Default"].AxisY.Minimum = 0;
            chart2.ChartAreas["Default"].AxisY.Interval = 1;
            chart2.ChartAreas["Default"].AxisY.Maximum = 53;
            chart2.ChartAreas["Default"].AxisX.IntervalType =
                Dundas.Charting.WinControl.DateTimeIntervalType.Number;
            chart2.ChartAreas["Default"].AxisX.Margin = true;
            chart2.ChartAreas["Default"].AxisX.Interval = 1;
            chart2.ChartAreas["Default"].AxisX.Minimum = 0;


            //if (cmbUnit_Line.Text.Trim() == "%")
            //    chart2.ChartAreas["Default"].AxisX.Maximum = cmbUnit_Line.Items.Count + 1;
            //else
            //    chart2.ChartAreas["Default"].AxisX.Maximum = 2;
            // Show as 3D

            chart2.ChartAreas["Default"].Area3DStyle.Enable3D = false;
            // Set Title Names
            chart2.ChartAreas["Default"].AxisX.Title = "Unit - " /*+ ReportItem*/;
            chart2.ChartAreas["Default"].AxisY.Title = "Week";
            chart2.Titles.Clear();

            //chart2.Titles.Add(GetModuleDescription(_Module) +
            //    "  [ Project : " + Vars.ProjectCode + ",  Genco : " +
            //    cmbGenco_Area.Text.Trim() + ",  Unit : " + cmbUnit_Line.Text.Trim() + "]");

            for (int i = 0; i < 53; i++)
            {
                for (int k = 0; k < Plant_unitsNum[0]; k++)
                {
                    string item = Plant_UnitCode[0, k];

                    //if (item == "%")
                    //    continue;
                    //if (Unit_Line_Bus != "%" && Unit_Line_Bus != item.Trim())
                    //    continue;


                    chart2.Series[SeriesName1 + i]["PixelPointWidth"] = "10";

                    string Item1 = "";
                    value = Result_Plants_UnitsNum_Week[0, k, i];

                    SeriesName = SeriesName1 + i;
                    chart2.Series[SeriesName].Points.AddXY(item, 1);
                    if (value == 0)
                        chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Transparent;
                    else
                        chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Black;

                    //foreach (DataRow row in drs)
                    //{
                    //    int WeekPart = Convert.ToInt32(row["WeeksPart"]);
                    //    if (Is_UNIT_LINE_BUS == "UNIT")
                    //        Item1 = row["UnitCode"].ToString().Trim();
                    //    else if (Is_UNIT_LINE_BUS == "LINE")
                    //        Item1 = row["LineCode"].ToString().Trim();
                    //    if (Item1.Trim() != item.Trim())
                    //        continue;

                    //    for (int h = 1; h <= 13; h++)
                    //    {

                    //        value = Funcs.GetDouble(row["Week" + h].ToString());
                    //        int h1 = 0;
                    //        h1 = (WeekPart - 1) * 13 + h;
                    //        if (i != h1)
                    //            continue;

                    //        SeriesName = SeriesName1 + i;
                    //        chart2.Series[SeriesName].Points.AddXY(item, 1);
                    //        if (value == 0)
                    //            chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Transparent;
                    //        else
                    //            chart2.Series[SeriesName].Points[chart2.Series[SeriesName].Points.Count - 1].Color = Color.Black;
                    //    }
                    //}
                }
            }

        }

        private void cmbMaintShownPlant_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillStackedBarChartPlant(cmbMaintShownPlant.SelectedIndex);
        }

    }
}