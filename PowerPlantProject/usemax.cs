﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerPlantProject
{
    public class usemax
    {

        private static usemax self = null;


        private usemax()
        {

        }

        public static usemax GetInstance()
        {

            if (self == null)
                self = new usemax();
            return self;
        }

        private bool ismax;

        public bool Ismax
        {
            get { return ismax; }
            set { ismax = value; }
        }

    }
    public class useInitial
    {

        private static useInitial self = null;


        private useInitial()
        {

        }

        public static useInitial GetInstance()
        {

            if (self == null)
                self = new useInitial();
            return self;
        }

        private bool isInitial;

        public bool IsInitial
        {
            get { return isInitial; }
            set { isInitial = value; }
        }

    }
}
