﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
//using Microsoft.Office.Core;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    partial class MainForm
    {
        //-------------------------ReasetVisibility--------------------------
        private void ResetVisibility()
        {
            odunitosd1Valid.Visible = false;
            odunitosd2Valid.Visible = false;
            odunitmd1Valid.Visible = false;
            odunitmd2Valid.Visible = false;
            odunitsfd1Valid.Visible = false;
            odunitsfd2Valid.Visible = false;
            odunitpd1Valid.Visible = false;
           // odunitpd2Valid.Visible = false;
        }
        //--------------------------ODSaveBtn_Click--------------------------
        private void ODSaveBtn_Click(object sender, EventArgs e)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            //A Unit is Selected
            if (ODUnitPanel.Visible)
            {
                bool check = true;
                if (ODUnitOutCheck.Checked)
                {
                    if (ODUnitServiceStartDate.IsNull)
                    {
                        odunitosd1Valid.Visible = true;
                        check = false;
                    }
                    if (ODUnitServiceEndDate.IsNull)
                    {
                        odunitosd2Valid.Visible = true;
                        check = false;
                    }
                }
                if (ODUnitMainCheck.Checked)
                {
                    if (ODUnitMainStartDate.IsNull)
                    {
                        odunitmd1Valid.Visible = true;
                        check = false;
                    }
                    if (ODUnitMainEndDate.IsNull)
                    {
                        odunitmd2Valid.Visible = true;
                        check = false;
                    }
                }
                if (ODUnitFuelCheck.Checked)
                {
                    if (ODUnitFuelStartDate.IsNull)
                    {
                        odunitsfd1Valid.Visible = true;
                        check = false;
                    }
                    if (ODUnitFuelEndDate.IsNull)
                    {
                        odunitsfd2Valid.Visible = true;
                        check = false;
                    }
                    if (errorProvider1.GetError(ODUnitFuelTB) != "")
                        check = false;
                }
                //if (ODUnitPowerCheck.Checked)
                //{
                //    if (ODUnitPowerStartDate.IsNull)
                //    {
                //        odunitpd1Valid.Visible = true;
                //        check = false;
                //    }
                //    //if (ODUnitPowerEndDate.IsNull)
                //    //{
                //    //    odunitpd2Valid.Visible = true;
                //    //    check = false;
                //    //}
                //    if (errorProvider1.GetError(ODUnitPowerGrid1) != "") check = false;
                //    if (errorProvider1.GetError(ODUnitPowerGrid2) != "") check = false;
                //}

                if (!check) MessageBox.Show("Please Fill Marked Fields With Valid Values");
                else
                {
                    string type = ODPackLb.Text;
                    if (type.Contains("Combined")) type = "CC";
                    //Detect Farsi Date
                    PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
                    string mydate = prDate.ToString("d");

                    ////Is There a Row in DB for this Unit in this date?
                    DataTable IsThere = Utilities.GetTable("SELECT COUNT(*) FROM ConditionUnit WHERE PPID='" + PPID + "' AND UnitCode='" + ODUnitLb.Text + "' AND PackageType='" + type + "' AND Date='" + mydate + "'");
                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Connection = myConnection;

                    if (IsThere.Rows[0][0].ToString().Trim() == "0")
                        MyCom.CommandText = "INSERT INTO [ConditionUnit] (PPID,UnitCode,PackageType,Date,OutService," +
                        "OutServiceStartDate,OutServiceEndDate,Maintenance,MaintenanceStartDate,MaintenanceEndDate,SecondFuel," +
                        "SecondFuelStartDate,SecondFuelEndDate,FuelForOneDay,OutServiceStartHour,OutServiceEndHour," +
                        "MaintenanceStartHour,MaintenanceEndHour,MaintenanceType,SecondFuelStartHour,SecondFuelEndHour,SeconfuelPer,MustRun,MustMax) " +
                        "VALUES (@num,@unit,@type,@date,@os,@osd1,@osd2,@m,@md1,@md2,@sf,@sfd1,@sfd2,@sfq,@osh1,@osh2,@mh1,@mh2,@mt,@sfh1,@sfh2,@sfpercent,@must,@maxmust) ";
                    else
                        MyCom.CommandText = "UPDATE [ConditionUnit] SET OutService=@os,OutServiceStartDate=@osd1," +
                       "OutServiceEndDate=@osd2,Maintenance=@m,MaintenanceStartDate=@md1," +
                       "MaintenanceEndDate=@md2,SecondFuel=@sf,SecondFuelStartDate=@sfd1,SecondFuelEndDate=@sfd2," +
                       "FuelForOneDay=@sfq,OutServiceStartHour=@osh1,OutServiceEndHour=@osh2,MaintenanceStartHour=@mh1," +
                       "MaintenanceEndHour=@mh2,MaintenanceType=@mt,SecondFuelStartHour=@sfh1,SecondFuelEndHour=@sfh2,SeconfuelPer=@sfpercent,MustRun=@must,MustMax=@maxmust" +
                       " WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type AND Date=@date";

                    MyCom.Parameters.Add("@date", SqlDbType.NChar, 10);
                    MyCom.Parameters["@date"].Value = mydate;
                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                    MyCom.Parameters["@num"].Value = PPID;
                    MyCom.Parameters.Add("@unit", SqlDbType.NChar, 10);
                    MyCom.Parameters["@unit"].Value = ODUnitLb.Text;
                    MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
                    MyCom.Parameters["@type"].Value = type;
                    MyCom.Parameters.Add("@os", SqlDbType.Bit);
                    MyCom.Parameters["@os"].Value = ODUnitOutCheck.Checked;

                    MyCom.Parameters.Add("@must", SqlDbType.Bit);
                    MyCom.Parameters["@must"].Value = MustrunCheck.Checked;

                    MyCom.Parameters.Add("@maxmust", SqlDbType.Bit);
                    MyCom.Parameters["@maxmust"].Value = MUSTMAXCKECK.Checked;
                   
                    MyCom.Parameters.Add("@osd1", SqlDbType.Char, 10);
                    if (ODUnitOutCheck.Checked)
                        MyCom.Parameters["@osd1"].Value = ODUnitServiceStartDate.Text;
                    else MyCom.Parameters["@osd1"].Value = "";
                    MyCom.Parameters.Add("@osd2", SqlDbType.Char, 10);
                    if (ODUnitOutCheck.Checked)
                        MyCom.Parameters["@osd2"].Value = ODUnitServiceEndDate.Text;
                    else MyCom.Parameters["@osd2"].Value = "";
                    MyCom.Parameters.Add("@osh1", SqlDbType.Int);
                    if (ODUnitOutCheck.Checked)
                        MyCom.Parameters["@osh1"].Value = ODOutServiceStartHour.Text;
                    else MyCom.Parameters["@osh1"].Value = "0";
                    MyCom.Parameters.Add("@osh2", SqlDbType.Int);
                    if (ODUnitOutCheck.Checked)
                        MyCom.Parameters["@osh2"].Value = ODOutServiceEndHour.Text;
                    else MyCom.Parameters["@osh2"].Value = "0";

                    MyCom.Parameters.Add("@m", SqlDbType.Bit);
                    MyCom.Parameters["@m"].Value = ODUnitMainCheck.Checked;
                    MyCom.Parameters.Add("@md1", SqlDbType.Char, 10);
                    if (ODUnitMainCheck.Checked)
                        MyCom.Parameters["@md1"].Value = ODUnitMainStartDate.Text;
                    else MyCom.Parameters["@md1"].Value = "";
                    MyCom.Parameters.Add("@md2", SqlDbType.Char, 10);
                    if (ODUnitMainCheck.Checked)
                        MyCom.Parameters["@md2"].Value = ODUnitMainEndDate.Text;
                    else MyCom.Parameters["@md2"].Value = "";
                    MyCom.Parameters.Add("@mh1", SqlDbType.Int);
                    if (ODUnitMainCheck.Checked)
                        MyCom.Parameters["@mh1"].Value = ODMaintenanceStartHour.Text;
                    else MyCom.Parameters["@mh1"].Value = "0";
                    MyCom.Parameters.Add("@mh2", SqlDbType.Int);
                    if (ODUnitMainCheck.Checked)
                        MyCom.Parameters["@mh2"].Value = ODMaintenanceEndHour.Text;
                    else MyCom.Parameters["@mh2"].Value = "0";
                    MyCom.Parameters.Add("@mt", SqlDbType.NChar, 12);
                    if (ODUnitMainCheck.Checked)
                        MyCom.Parameters["@mt"].Value = ODMaintenanceType.Text;
                    else MyCom.Parameters["@mt"].Value = "";

                    MyCom.Parameters.Add("@sf", SqlDbType.Bit);
                    MyCom.Parameters["@sf"].Value = ODUnitFuelCheck.Checked;
                    MyCom.Parameters.Add("@sfd1", SqlDbType.Char, 10);
                    if (ODUnitFuelCheck.Checked)
                        MyCom.Parameters["@sfd1"].Value = ODUnitFuelStartDate.Text;
                    else MyCom.Parameters["@sfd1"].Value = "";
                    MyCom.Parameters.Add("@sfd2", SqlDbType.Char, 10);
                    if (ODUnitFuelCheck.Checked)
                        MyCom.Parameters["@sfd2"].Value = ODUnitFuelEndDate.Text;
                    else MyCom.Parameters["@sfd2"].Value = "";
                    MyCom.Parameters.Add("@sfh1", SqlDbType.Int);
                    if (ODUnitFuelCheck.Checked)
                        MyCom.Parameters["@sfh1"].Value = ODSecondFuelStartHour.Text;
                    else MyCom.Parameters["@sfh1"].Value = "0";
                    MyCom.Parameters.Add("@sfh2", SqlDbType.Int);
                    if (ODUnitFuelCheck.Checked)
                        MyCom.Parameters["@sfh2"].Value = ODSecondFuelEndHour.Text;
                    else MyCom.Parameters["@sfh2"].Value = "0";
                    MyCom.Parameters.Add("@sfq", SqlDbType.Real);
                    if ((ODUnitFuelCheck.Checked) && (ODUnitFuelTB.Text != ""))
                        MyCom.Parameters["@sfq"].Value = ODUnitFuelTB.Text;
                    else MyCom.Parameters["@sfq"].Value = "0";
                    MyCom.Parameters.Add("@sfpercent", SqlDbType.Real);
                    if ((ODUnitFuelCheck.Checked) && (textSECONDFUELPERCENT.Text != ""))
                        MyCom.Parameters["@sfpercent"].Value = textSECONDFUELPERCENT.Text;
                    else MyCom.Parameters["@sfpercent"].Value = "0";

                    //try
                    //{
                    MyCom.ExecuteNonQuery();
                   // }
                    //catch (Exception exp)
                    //{
                    //    string str = exp.Message;
                    //}


                    //if (ODUnitPowerCheck.Checked)
                    //{
                    //    //Is There any rows for this Unit?
                    //    IsThere = utilities.GetTable("SELECT COUNT(*) FROM PowerLimitedUnit WHERE PPID='" + PPID + "' AND UnitCode='" + ODUnitLb.Text + "' AND PackageType='" + type + "'and StartDate='" + ODUnitPowerStartDate.Text + "'");
                    //    if (IsThere.Rows[0][0].ToString().Trim() == "0")
                    //        MyCom.CommandText = "INSERT INTO [PowerLimitedUnit] (PPID,UnitCode," +
                    //        "PackageType,StartDate,EndDate,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10," +
                    //        "Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24,Time,isRec) " +
                    //        "VALUES (@num,@unit,@type,@date1,@date2,@h1,@h2,@h3,@h4,@h5,@h6,@h7,@h8,@h9,@h10,@h11,@h12,@h13," +
                    //        "@h14,@h15,@h16,@h17,@h18,@h19,@h20,@h21,@h22,@h23,@h24,@time,1)";
                    //    else
                    //    {
                    //        string currentdate = new PersianDate(DateTime.Now).ToString("d");
                    //        string  date=ODUnitPowerStartDate.Text;
                    //        if(date==currentdate )
                    //        {
                    //        MyCom.CommandText = "UPDATE [PowerLimitedUnit] SET Hour1=@h1,Hour2=@h2,Hour3=@h3," +
                    //        "Hour4=@h4,Hour5=@h5,Hour6=@h6,Hour7=@h7,Hour8=@h8,Hour9=@h9,Hour10=@h10,Hour11=@h11," +
                    //        "Hour12=@h12,Hour13=@h13,Hour14=@h14,Hour15=@h15,Hour16=@h16,Hour17=@h17,Hour18=@h18," +
                    //        "Hour19=@h19,Hour20=@h20,Hour21=@h21,Hour22=@h22,Hour23=@h23,Hour24=@h24,StartDate=@date1," +
                    //        "EndDate=@date2,Time=@time,isRec=1 WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type AND StartDate=@date1 ";
                    //        }
                    //        else
                    //        {
                    //        MyCom.CommandText = "UPDATE [PowerLimitedUnit] SET Hour1=@h1,Hour2=@h2,Hour3=@h3," +
                    //        "Hour4=@h4,Hour5=@h5,Hour6=@h6,Hour7=@h7,Hour8=@h8,Hour9=@h9,Hour10=@h10,Hour11=@h11," +
                    //        "Hour12=@h12,Hour13=@h13,Hour14=@h14,Hour15=@h15,Hour16=@h16,Hour17=@h17,Hour18=@h18," +
                    //        "Hour19=@h19,Hour20=@h20,Hour21=@h21,Hour22=@h22,Hour23=@h23,Hour24=@h24,StartDate=@date1," +
                    //        "EndDate=@date2  WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type AND StartDate=@date1 ";
                    //        }
                    //    }
                    //    MyCom.Parameters.Add("@date1", SqlDbType.Char, 10);
                    //    MyCom.Parameters["@date1"].Value = ODUnitPowerStartDate.Text;
                    //    MyCom.Parameters.Add("@date2", SqlDbType.Char, 10);
                    //    MyCom.Parameters["@date2"].Value = ODUnitPowerStartDate.Text;
                    //    MyCom.Parameters.Add("@h1", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[0].Value != null))
                    //        MyCom.Parameters["@h1"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[0].Value.ToString());
                    //    else MyCom.Parameters["@h1"].Value = 0;
                    //    MyCom.Parameters.Add("@h2", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[1].Value != null))
                    //        MyCom.Parameters["@h2"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[1].Value.ToString());
                    //    else MyCom.Parameters["@h2"].Value = 0;
                    //    MyCom.Parameters.Add("@h3", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[2].Value != null))
                    //        MyCom.Parameters["@h3"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[2].Value.ToString());
                    //    else MyCom.Parameters["@h3"].Value = 0;
                    //    MyCom.Parameters.Add("@h4", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[3].Value != null))
                    //        MyCom.Parameters["@h4"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[3].Value.ToString());
                    //    else MyCom.Parameters["@h4"].Value = 0;
                    //    MyCom.Parameters.Add("@h5", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[4].Value != null))
                    //        MyCom.Parameters["@h5"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[4].Value.ToString());
                    //    else MyCom.Parameters["@h5"].Value = 0;
                    //    MyCom.Parameters.Add("@h6", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[5].Value != null))
                    //        MyCom.Parameters["@h6"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[5].Value.ToString());
                    //    else MyCom.Parameters["@h6"].Value = 0;
                    //    MyCom.Parameters.Add("@h7", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[6].Value != null))
                    //        MyCom.Parameters["@h7"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[6].Value.ToString());
                    //    else MyCom.Parameters["@h7"].Value = 0;
                    //    MyCom.Parameters.Add("@h8", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[7].Value != null))
                    //        MyCom.Parameters["@h8"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[7].Value.ToString());
                    //    else MyCom.Parameters["@h8"].Value = 0;
                    //    MyCom.Parameters.Add("@h9", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[8].Value != null))
                    //        MyCom.Parameters["@h9"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[8].Value.ToString());
                    //    else MyCom.Parameters["@h9"].Value = 0;
                    //    MyCom.Parameters.Add("@h10", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[9].Value != null))
                    //        MyCom.Parameters["@h10"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[9].Value.ToString());
                    //    else MyCom.Parameters["@h10"].Value = 0;
                    //    MyCom.Parameters.Add("@h11", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[10].Value != null))
                    //        MyCom.Parameters["@h11"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[10].Value.ToString());
                    //    else MyCom.Parameters["@h11"].Value = 0;
                    //    MyCom.Parameters.Add("@h12", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[11].Value != null))
                    //        MyCom.Parameters["@h12"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[11].Value.ToString());
                    //    else MyCom.Parameters["@h12"].Value = 0;
                    //    MyCom.Parameters.Add("@h13", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[0].Value != null))
                    //        MyCom.Parameters["@h13"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[0].Value.ToString());
                    //    else MyCom.Parameters["@h13"].Value = 0;
                    //    MyCom.Parameters.Add("@h14", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[1].Value != null))
                    //        MyCom.Parameters["@h14"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[1].Value.ToString());
                    //    else MyCom.Parameters["@h14"].Value = 0;
                    //    MyCom.Parameters.Add("@h15", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[2].Value != null))
                    //        MyCom.Parameters["@h15"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[2].Value.ToString());
                    //    else MyCom.Parameters["@h15"].Value = 0;
                    //    MyCom.Parameters.Add("@h16", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[3].Value != null))
                    //        MyCom.Parameters["@h16"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[3].Value.ToString());
                    //    else MyCom.Parameters["@h16"].Value = 0;
                    //    MyCom.Parameters.Add("@h17", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[4].Value != null))
                    //        MyCom.Parameters["@h17"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[4].Value.ToString());
                    //    else MyCom.Parameters["@h17"].Value = 0;
                    //    MyCom.Parameters.Add("@h18", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[5].Value != null))
                    //        MyCom.Parameters["@h18"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[5].Value.ToString());
                    //    else MyCom.Parameters["@h18"].Value = 0;
                    //    MyCom.Parameters.Add("@h19", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[6].Value != null))
                    //        MyCom.Parameters["@h19"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[6].Value.ToString());
                    //    else MyCom.Parameters["@h19"].Value = 0;
                    //    MyCom.Parameters.Add("@h20", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[7].Value != null))
                    //        MyCom.Parameters["@h20"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[7].Value.ToString());
                    //    else MyCom.Parameters["@h20"].Value = 0;
                    //    MyCom.Parameters.Add("@h21", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[8].Value != null))
                    //        MyCom.Parameters["@h21"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[8].Value.ToString());
                    //    else MyCom.Parameters["@h21"].Value = 0;
                    //    MyCom.Parameters.Add("@h22", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[9].Value != null))
                    //        MyCom.Parameters["@h22"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[9].Value.ToString());
                    //    else MyCom.Parameters["@h22"].Value = 0;
                    //    MyCom.Parameters.Add("@h23", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[10].Value != null))
                    //        MyCom.Parameters["@h23"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[10].Value.ToString());
                    //    else MyCom.Parameters["@h23"].Value = 0;
                    //    MyCom.Parameters.Add("@h24", SqlDbType.Real);
                    //    if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[11].Value != null))
                    //        MyCom.Parameters["@h24"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[11].Value.ToString());
                    //    else MyCom.Parameters["@h24"].Value = 0;
                    //   //////////////////////////////
                    //    MyCom.Parameters.Add("@time", SqlDbType.Char, 5);
                    //    if (ODUnitPowerCheck.Checked)
                    //    {
                    //         string CurrentTime = DateTime.Now.Hour + ":" + DateTime.Now.Minute;
                    //         MyCom.Parameters["@time"].Value = CurrentTime;
                    //    }

                    //    else MyCom.Parameters["@time"].Value = "0:0";
                    //    ///////////////////////////////////
                        

                      
                    //    //try
                    //    //{
                    //    MyCom.ExecuteNonQuery();
                    //    //}
                    //    //catch (Exception exp)
                    //    //{
                    //    //  string str = exp.Message;
                    //    //}
                    //}
                    ResetVisibility();
                    MessageBox.Show("Values have been Saved!");
                }
            }

            myConnection.Close();
        }
        private void btnsavedispach_Click(object sender, EventArgs e)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            //A Unit is Selected
            if (ODUnitPanel.Visible)
            {
                bool check = true;
                
               
               
                if (ODUnitPowerCheck.Checked)
                {
                    if (ODUnitPowerStartDate.IsNull)
                    {
                        odunitpd1Valid.Visible = true;
                        check = false;
                    }
                   
                    if (errorProvider1.GetError(ODUnitPowerGrid1) != "") check = false;
                    if (errorProvider1.GetError(ODUnitPowerGrid2) != "") check = false;
                }

                if (!check) MessageBox.Show("Please Fill Marked Fields With Valid Values");
                else
                {
                    string type = ODPackLb.Text;
                    if (type.Contains("Combined")) type = "CC";
                    //Detect Farsi Date
                    PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
                    string mydate = prDate.ToString("d");
                    ////Is There a Row in DB for this Unit in this date?
                    DataTable IsThere = Utilities.GetTable("SELECT COUNT(*) FROM ConditionUnit WHERE PPID='" + PPID + "' AND UnitCode='" + ODUnitLb.Text + "' AND PackageType='" + type + "' AND Date='" + mydate + "'");
                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Connection = myConnection;
                    if (ODUnitPowerCheck.Checked)
                    {
                        //Is There any rows for this Unit?
                        IsThere = Utilities.GetTable("SELECT COUNT(*) FROM PowerLimitedUnit WHERE PPID='" + PPID + "' AND UnitCode='" + ODUnitLb.Text + "' AND PackageType='" + type + "'and StartDate='" + ODUnitPowerStartDate.Text + "'and isEmpty=0");
                        if (IsThere.Rows[0][0].ToString().Trim() == "0")
                            MyCom.CommandText = "INSERT INTO [PowerLimitedUnit] (PPID,UnitCode," +
                            "PackageType,StartDate,EndDate,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10," +
                            "Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24,Time,isRec,isEmpty) " +
                            "VALUES (@num,@unit,@type,@date1,@date2,@h1,@h2,@h3,@h4,@h5,@h6,@h7,@h8,@h9,@h10,@h11,@h12,@h13," +
                            "@h14,@h15,@h16,@h17,@h18,@h19,@h20,@h21,@h22,@h23,@h24,@time,1,0)";
                        else
                        {
                            string currentdate = new PersianDate(DateTime.Now).ToString("d");
                          

                            string date = ODUnitPowerStartDate.Text;
                            if (date == currentdate)
                            {
                                MyCom.CommandText = "UPDATE [PowerLimitedUnit] SET Hour1=@h1,Hour2=@h2,Hour3=@h3," +
                                "Hour4=@h4,Hour5=@h5,Hour6=@h6,Hour7=@h7,Hour8=@h8,Hour9=@h9,Hour10=@h10,Hour11=@h11," +
                                "Hour12=@h12,Hour13=@h13,Hour14=@h14,Hour15=@h15,Hour16=@h16,Hour17=@h17,Hour18=@h18," +
                                "Hour19=@h19,Hour20=@h20,Hour21=@h21,Hour22=@h22,Hour23=@h23,Hour24=@h24,StartDate=@date1," +
                                "EndDate=@date2,Time=@time,isRec=1,isEmpty=0 WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type AND StartDate=@date1 ";
                            }
                            else
                            {
                                MyCom.CommandText = "UPDATE [PowerLimitedUnit] SET Hour1=@h1,Hour2=@h2,Hour3=@h3," +
                                "Hour4=@h4,Hour5=@h5,Hour6=@h6,Hour7=@h7,Hour8=@h8,Hour9=@h9,Hour10=@h10,Hour11=@h11," +
                                "Hour12=@h12,Hour13=@h13,Hour14=@h14,Hour15=@h15,Hour16=@h16,Hour17=@h17,Hour18=@h18," +
                                "Hour19=@h19,Hour20=@h20,Hour21=@h21,Hour22=@h22,Hour23=@h23,Hour24=@h24,StartDate=@date1," +
                                "EndDate=@date2,isEmpty=0 WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type AND StartDate=@date1 ";
                            }
                        }
                        MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                        MyCom.Parameters["@num"].Value = PPID;
                        MyCom.Parameters.Add("@unit", SqlDbType.NChar, 10);
                        MyCom.Parameters["@unit"].Value = ODUnitLb.Text;
                        MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
                        MyCom.Parameters["@type"].Value = type;
                        MyCom.Parameters.Add("@date1", SqlDbType.Char, 10);
                        MyCom.Parameters["@date1"].Value = ODUnitPowerStartDate.Text;
                        MyCom.Parameters.Add("@date2", SqlDbType.Char, 10);
                        MyCom.Parameters["@date2"].Value = ODUnitPowerStartDate.Text;
                        MyCom.Parameters.Add("@h1", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[0].Value != null))
                            MyCom.Parameters["@h1"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[0].Value.ToString());
                        else MyCom.Parameters["@h1"].Value = 0;
                        MyCom.Parameters.Add("@h2", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[1].Value != null))
                            MyCom.Parameters["@h2"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[1].Value.ToString());
                        else MyCom.Parameters["@h2"].Value = 0;
                        MyCom.Parameters.Add("@h3", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[2].Value != null))
                            MyCom.Parameters["@h3"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[2].Value.ToString());
                        else MyCom.Parameters["@h3"].Value = 0;
                        MyCom.Parameters.Add("@h4", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[3].Value != null))
                            MyCom.Parameters["@h4"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[3].Value.ToString());
                        else MyCom.Parameters["@h4"].Value = 0;
                        MyCom.Parameters.Add("@h5", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[4].Value != null))
                            MyCom.Parameters["@h5"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[4].Value.ToString());
                        else MyCom.Parameters["@h5"].Value = 0;
                        MyCom.Parameters.Add("@h6", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[5].Value != null))
                            MyCom.Parameters["@h6"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[5].Value.ToString());
                        else MyCom.Parameters["@h6"].Value = 0;
                        MyCom.Parameters.Add("@h7", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[6].Value != null))
                            MyCom.Parameters["@h7"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[6].Value.ToString());
                        else MyCom.Parameters["@h7"].Value = 0;
                        MyCom.Parameters.Add("@h8", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[7].Value != null))
                            MyCom.Parameters["@h8"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[7].Value.ToString());
                        else MyCom.Parameters["@h8"].Value = 0;
                        MyCom.Parameters.Add("@h9", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[8].Value != null))
                            MyCom.Parameters["@h9"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[8].Value.ToString());
                        else MyCom.Parameters["@h9"].Value = 0;
                        MyCom.Parameters.Add("@h10", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[9].Value != null))
                            MyCom.Parameters["@h10"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[9].Value.ToString());
                        else MyCom.Parameters["@h10"].Value = 0;
                        MyCom.Parameters.Add("@h11", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[10].Value != null))
                            MyCom.Parameters["@h11"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[10].Value.ToString());
                        else MyCom.Parameters["@h11"].Value = 0;
                        MyCom.Parameters.Add("@h12", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid1.Rows[0].Cells[11].Value != null))
                            MyCom.Parameters["@h12"].Value = double.Parse(ODUnitPowerGrid1.Rows[0].Cells[11].Value.ToString());
                        else MyCom.Parameters["@h12"].Value = 0;
                        MyCom.Parameters.Add("@h13", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[0].Value != null))
                            MyCom.Parameters["@h13"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[0].Value.ToString());
                        else MyCom.Parameters["@h13"].Value = 0;
                        MyCom.Parameters.Add("@h14", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[1].Value != null))
                            MyCom.Parameters["@h14"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[1].Value.ToString());
                        else MyCom.Parameters["@h14"].Value = 0;
                        MyCom.Parameters.Add("@h15", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[2].Value != null))
                            MyCom.Parameters["@h15"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[2].Value.ToString());
                        else MyCom.Parameters["@h15"].Value = 0;
                        MyCom.Parameters.Add("@h16", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[3].Value != null))
                            MyCom.Parameters["@h16"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[3].Value.ToString());
                        else MyCom.Parameters["@h16"].Value = 0;
                        MyCom.Parameters.Add("@h17", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[4].Value != null))
                            MyCom.Parameters["@h17"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[4].Value.ToString());
                        else MyCom.Parameters["@h17"].Value = 0;
                        MyCom.Parameters.Add("@h18", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[5].Value != null))
                            MyCom.Parameters["@h18"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[5].Value.ToString());
                        else MyCom.Parameters["@h18"].Value = 0;
                        MyCom.Parameters.Add("@h19", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[6].Value != null))
                            MyCom.Parameters["@h19"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[6].Value.ToString());
                        else MyCom.Parameters["@h19"].Value = 0;
                        MyCom.Parameters.Add("@h20", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[7].Value != null))
                            MyCom.Parameters["@h20"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[7].Value.ToString());
                        else MyCom.Parameters["@h20"].Value = 0;
                        MyCom.Parameters.Add("@h21", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[8].Value != null))
                            MyCom.Parameters["@h21"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[8].Value.ToString());
                        else MyCom.Parameters["@h21"].Value = 0;
                        MyCom.Parameters.Add("@h22", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[9].Value != null))
                            MyCom.Parameters["@h22"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[9].Value.ToString());
                        else MyCom.Parameters["@h22"].Value = 0;
                        MyCom.Parameters.Add("@h23", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[10].Value != null))
                            MyCom.Parameters["@h23"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[10].Value.ToString());
                        else MyCom.Parameters["@h23"].Value = 0;
                        MyCom.Parameters.Add("@h24", SqlDbType.Real);
                        if ((ODUnitPowerCheck.Checked) && (ODUnitPowerGrid2.Rows[0].Cells[11].Value != null))
                            MyCom.Parameters["@h24"].Value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[11].Value.ToString());
                        else MyCom.Parameters["@h24"].Value = 0;
                        //////////////////////////////
                        MyCom.Parameters.Add("@time", SqlDbType.Char, 5);
                        if (ODUnitPowerCheck.Checked)
                        {
                            string CurrentTime = DateTime.Now.Hour + ":" + DateTime.Now.Minute;

                            /////////////////0:0/////////////////////////
                          

                           // string[] h1 = new string[2];
                          
                            //for (int i = 0; i < 2; i++)
                            //{
                            //    h1[i] = CurrentTime.Split(':').GetValue(i).ToString();
                            //}

                            string[] h1 = CurrentTime.Split(':');

                            if (int.Parse(h1[0]) < 10)
                            {

                                h1[0] = "0" + h1[0];

                            }
                            if (int.Parse(h1[1]) < 10)
                            {

                                h1[1] = "0" + h1[1];

                            }


                            CurrentTime = h1[0] + ":" + h1[1];
       
                            /////////////////////0:0/////////////////

                           MyCom.Parameters["@time"].Value = CurrentTime;
                        }

                        else MyCom.Parameters["@time"].Value = "0:0";
                        ///////////////////////////////////

                        MyCom.ExecuteNonQuery();
                       
                    }
                    ResetVisibility();
                    MessageBox.Show("Values have been Saved!");
                }
            }

            myConnection.Close();
        }


        //------------------------------------ClearODUnitTab()---------------------------------
        private void ClearODUnitTab()
        {
            textupdatetime.Visible = false;
            labelupdatetime.Text = "";
            MustrunCheck.Checked = false;
            MUSTMAXCKECK.Checked = false;
            ODUnitOutCheck.Checked = false;
            ODUnitServiceStartDate.IsNull = true;
            ODUnitServiceEndDate.IsNull = true;
            ODUnitMainCheck.Checked = false;
            ODUnitMainStartDate.IsNull = true;
            ODUnitMainEndDate.IsNull = true;
            ODUnitFuelCheck.Checked = false;
            ODUnitFuelStartDate.IsNull = true;
            ODUnitFuelEndDate.IsNull = true;
            ODUnitFuelTB.Text = "";
            textSECONDFUELPERCENT.Text = "";
            ODUnitPowerCheck.Checked = false;
            ODUnitPowerStartDate.IsNull = true;
            // ODUnitPowerEndDate.IsNull = true;
            ///////////new//////////////////////
            ODOutServiceStartHour.Value = 1;
            ODOutServiceEndHour.Value = 1;
            ODMaintenanceStartHour.Value = 1;
            ODMaintenanceEndHour.Value = 1;
            ODSecondFuelStartHour.Value = 1;
            ODSecondFuelEndHour.Value = 1;
            /////////////////////////////////////////
            
            ODUnitPowerGrid1.DataSource = null;
            if (ODUnitPowerGrid1.Rows != null) ODUnitPowerGrid1.Rows.Clear();
            ODUnitPowerGrid2.DataSource = null;
            if (ODUnitPowerGrid2.Rows != null) ODUnitPowerGrid2.Rows.Clear();
        }
        //----------------------------------FillODUnit--------------------------------------------
        private void FillODUnit(string unit, string package, int index)
        {
            ClearODUnitTab();
            //detect current date and hour
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");
            int myhour = DateTime.Now.Hour;

            ////////////////////filled by operational data///////////////////////////////////////
            label114.Text = "";
            /////////////////////////////////////////////////////////////////////////////

            //Is There a Row in DB for this Unit?
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            SqlCommand MyCom = new SqlCommand();
            MyCom.CommandText = "SELECT  @result1 =count(PPID) FROM [ConditionUnit] WHERE PPID=@num AND UnitCode=@unit " +
            "AND PackageType=@type SELECT @result2=COUNT(PPID) FROM PowerLimitedUnit WHERE PPID=@num AND UnitCode=@unit AND PackageType=@type AND isEmpty=0";
            MyCom.Connection = myConnection;
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPID;
            MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
            MyCom.Parameters["@unit"].Value = unit;
            string type = package;
            if (type.Contains("Combined")) type = "CC";
            MyCom.Parameters.Add("@type", SqlDbType.NChar, 10);
            MyCom.Parameters["@type"].Value = type;
            MyCom.Parameters.Add("@result1", SqlDbType.Int);
            MyCom.Parameters["@result1"].Direction = ParameterDirection.Output;
            MyCom.Parameters.Add("@result2", SqlDbType.Int);
            MyCom.Parameters["@result2"].Direction = ParameterDirection.Output;
            //try
            // {
            MyCom.ExecuteNonQuery();
            // }
            //catch()
            //{
            //}
            int result1;
            result1 = (int)MyCom.Parameters["@result1"].Value;
            int result2;
            result2 = (int)MyCom.Parameters["@result2"].Value;
            if (result1 != 0)
            {
                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT OutService,OutServiceStartDate,OutServiceEndDate,NoOn,NoOff," +
                "SecondFuel,SecondFuelStartDate,SecondFuelEndDate,FuelForOneDay,Maintenance,MaintenanceStartDate," +
                "MaintenanceEndDate,OutServiceStartHour,OutServiceEndHour,MaintenanceStartHour,MaintenanceEndHour," +
                "SecondFuelStartHour,SecondFuelEndHour,MaintenanceType,SeconfuelPer,MustRun,MustMax FROM ConditionUnit WHERE UnitCode=@unit " +
                "AND PackageType=@type AND PPID=" + PPID + " AND Date=(SELECT MAX(ConditionUnit.Date) FROM " +
                "ConditionUnit WHERE UnitCode=@unit AND PackageType=@type AND PPID=" + PPID + ")", myConnection);
                Myda.SelectCommand.Parameters.Add("@unit", SqlDbType.NChar, 20);
                Myda.SelectCommand.Parameters["@unit"].Value = unit;
                Myda.SelectCommand.Parameters.Add("@type", SqlDbType.NChar, 10);
                Myda.SelectCommand.Parameters["@type"].Value = type;
                Myda.Fill(MyDS);
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {

                    if ((MyRow[20].ToString() != "") && bool.Parse(MyRow[20].ToString()))
                    {
                        MustrunCheck.Checked = true;
                    }
                    else MustrunCheck.Checked = false;

                    if ((MyRow[21].ToString() != "") && bool.Parse(MyRow[21].ToString()))
                    {
                        MUSTMAXCKECK.Checked = true;
                    }
                    else MUSTMAXCKECK.Checked = false;





                    if ((MyRow[0].ToString() != "") && (bool.Parse(MyRow[0].ToString())))
                    //if (CheckDate(mydate, MyRow[1].ToString(), MyRow[2].ToString()))
                    {
                        ODUnitOutCheck.Checked = true;
                        ODUnitServiceStartDate.IsNull = false;
                        ODUnitServiceEndDate.IsNull = false;
                        ODUnitServiceStartDate.Text = MyRow[1].ToString().Trim();
                        ODUnitServiceEndDate.Text = MyRow[2].ToString().Trim();
                        ODOutServiceStartHour.Value = int.Parse(MyRow[12].ToString().Trim());
                        ODOutServiceEndHour.Value = int.Parse(MyRow[13].ToString().Trim());
                    }
                    //if (bool.Parse(MyRow[3].ToString()))
                    //    ODUnitNoOnCheck.Checked = true;
                    //if (bool.Parse(MyRow[4].ToString()))
                    //    ODUnitNoOffCheck.Checked = true;
                    string myunit = unit.ToLower();
                    if (myunit.Contains("steam"))
                    {
                        ODMaintenanceType.Items.Clear();
                        ODMaintenanceType.Items.Add("CI");
                        ODMaintenanceType.Items.Add("MO");
                    }
                    else
                    {
                        ODMaintenanceType.Items.Clear();
                        ODMaintenanceType.Items.Add("CI");
                        ODMaintenanceType.Items.Add("MO");
                        ODMaintenanceType.Items.Add("HGP");
                    }
                    if ((MyRow[9].ToString() != "") && (bool.Parse(MyRow[9].ToString().Trim())))
                    //if (CheckDate(mydate, MyRow[10].ToString(), MyRow[11].ToString()))
                    {
                        ODUnitMainCheck.Checked = true;
                        ODUnitMainStartDate.IsNull = false;
                        ODUnitMainEndDate.IsNull = false;
                        ODUnitMainStartDate.Text = MyRow[10].ToString().Trim();
                        ODUnitMainEndDate.Text = MyRow[11].ToString().Trim();
                        ODMaintenanceStartHour.Value = int.Parse(MyRow[14].ToString().Trim());
                        ODMaintenanceEndHour.Value = int.Parse(MyRow[15].ToString().Trim());
                        ODMaintenanceType.Text = MyRow[18].ToString().Trim();
                    }
                    if ((MyRow[5].ToString() != "") && (bool.Parse(MyRow[5].ToString().Trim())))
                    //if (CheckDate(mydate, MyRow[6].ToString(), MyRow[7].ToString()))
                    {
                        ODUnitFuelCheck.Checked = true;
                        ODUnitFuelStartDate.IsNull = false;
                        ODUnitFuelEndDate.IsNull = false;
                        ODUnitFuelStartDate.Text = MyRow[6].ToString().Trim();
                        ODUnitFuelEndDate.Text = MyRow[7].ToString().Trim();
                        ODUnitFuelTB.Text = MyRow[8].ToString().Trim();
                        textSECONDFUELPERCENT.Text = MyRow[19].ToString().Trim();
                        ODSecondFuelStartHour.Value = int.Parse(MyRow[16].ToString().Trim());
                        ODSecondFuelEndHour.Value = int.Parse(MyRow[17].ToString().Trim());
                    }
                }
            }
            if (result2 != 0)
            {
               
              
                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                Myda.SelectCommand = new SqlCommand("SELECT StartDate,EndDate,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6," +
                "Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21," +
                "Hour22,Hour23,Hour24,Time,isRec FROM PowerLimitedUnit WHERE UnitCode=@unit AND PackageType=@type AND isEmpty=0 AND PPID=" + PPID + "and StartDate in (select max(StartDate) from dbo.PowerLimitedUnit where PPID=" + PPID + "and UnitCode =@unit and isEmpty=0)", myConnection);
                Myda.SelectCommand.Parameters.Add("@unit", SqlDbType.NChar, 20);
                Myda.SelectCommand.Parameters["@unit"].Value = unit;
                Myda.SelectCommand.Parameters.Add("@type", SqlDbType.NChar, 10);
                Myda.SelectCommand.Parameters["@type"].Value = type;
                Myda.Fill(MyDS);
                foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                {
                    //if (CheckDate(mydate, MyRow[0].ToString(), MyRow[1].ToString()))
                    {
                        ODUnitPowerCheck.Checked = true;
                        ODUnitPowerStartDate.IsNull = false;
                        // ODUnitPowerEndDate.IsNull = false;
                        ODUnitPowerStartDate.Text = MyRow[0].ToString().Trim();
                        // ODUnitPowerEndDate.Text = MyRow[1].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[0].Value = MyRow[2].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[1].Value = MyRow[3].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[2].Value = MyRow[4].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[3].Value = MyRow[5].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[4].Value = MyRow[6].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[5].Value = MyRow[7].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[6].Value = MyRow[8].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[7].Value = MyRow[9].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[8].Value = MyRow[10].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[9].Value = MyRow[11].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[10].Value = MyRow[12].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[11].Value = MyRow[13].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[0].Value = MyRow[14].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[1].Value = MyRow[15].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[2].Value = MyRow[16].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[3].Value = MyRow[17].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[4].Value = MyRow[18].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[5].Value = MyRow[19].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[6].Value = MyRow[20].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[7].Value = MyRow[21].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[8].Value = MyRow[22].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[9].Value = MyRow[23].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[10].Value = MyRow[24].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[11].Value = MyRow[25].ToString().Trim();
                        if(MyRow[27].ToString()=="True")
                        {
                            labelupdatetime.Text = "Rec User Has Recorded";
                        }
                       else
                        {
                            labelupdatetime.Text = ODPlantLb.Text + "  Plant User Has Recorded";
                        }
                        textupdatetime.Visible = true;
                        textupdatetime.Text = MyRow[26].ToString();
                    }
                }


            }
            myConnection.Close();
        }

        //-----------------------------------FillODUnitCombine------------------------------------
        private void FillODUnitCombine(string name)
        {
            //ClearODUnitTab();
            //detect current date and hour
            try
            {
                string packnum = name.Replace("Combined Cycle", "");
                ODUnitPowerGrid1.DataSource = null;
                if (ODUnitPowerGrid1.Rows != null) ODUnitPowerGrid1.Rows.Clear();
                ODUnitPowerGrid2.DataSource = null;
                if (ODUnitPowerGrid2.Rows != null) ODUnitPowerGrid2.Rows.Clear();

                DataTable dt = Utilities.GetTable("select  StartDate, sum(hour1)as hour1, sum(hour2)as hour2,sum(hour3)as hour3, sum(hour4)as hour4,sum(hour5)as hour5, sum(hour6)as hour6,sum(hour7)as hour7," +
                    "sum(hour8)as hour8,sum(hour9)as hour9, sum(hour10)as hour10,sum(hour11)as hour11," +
                    "sum(hour12)as hour12,sum(hour13)as hour13, sum(hour14)as hour14,sum(hour15)as hour15," +
                    "sum(hour16)as hour16,sum(hour17)as hour17, sum(hour18)as hour18,sum(hour19)as hour19," +
                    "sum(hour20)as hour20,sum(hour21)as hour21, sum(hour22)as hour22,sum(hour23)as hour23," +
                    "sum(hour24)as hour24 from dbo.PowerLimitedUnit where PPID='" + PPID +
                    "'and PackageType='cc' AND isEmpty=0  and StartDate in (select max(StartDate) from dbo.PowerLimitedUnit where PPID=" + PPID + " and unitcode in(select UnitCode from dbo.UnitsDataMain where PPID='" + PPID + "' and PackageType='cc' and PackageCode='" + packnum + "') and isEmpty=0)" +
                    "and unitcode in(select UnitCode from dbo.UnitsDataMain where PPID='" + PPID + "' and PackageType='cc' and PackageCode='" + packnum + "')group by StartDate");
                //Myda.SelectCommand = new SqlCommand("SELECT StartDate,EndDate,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6," +
                //"Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21," +
                //"Hour22,Hour23,Hour24,Time,isRec FROM PowerLimitedUnit WHERE UnitCode=@unit AND PackageType=@type AND isEmpty=0 AND PPID=" + PPID + "and StartDate in (select max(StartDate) from dbo.PowerLimitedUnit where PPID=" + PPID + "and UnitCode =@unit and isEmpty=0)", myConnection);
                //Myda.SelectCommand.Parameters.Add("@unit", SqlDbType.NChar, 20);
                //Myda.SelectCommand.Parameters["@unit"].Value = unit;
                //Myda.SelectCommand.Parameters.Add("@type", SqlDbType.NChar, 10);
                //Myda.SelectCommand.Parameters["@type"].Value = type;
                //Myda.Fill(MyDS);
                //  DataTable dtime = utilities.GetTable("select max(StartDate) from dbo.PowerLimitedUnit where PPID=" + PPID + " and unitcode in(select UnitCode from dbo.UnitsDataMain where PPID='" + PPID + "' and PackageType='cc' and PackageCode='" + packnum + "') and isEmpty=0");
                foreach (DataRow MyRow in dt.Rows)
                {

                    ODUnitPowerStartDate.Text = MyRow["StartDate"].ToString().Trim();
                    ODUnitPowerStartDate.Enabled = true;
                    ODUnitPowerCheck.Checked = true;

                    ODUnitPowerGrid1.Rows[0].Cells[0].Value = MyRow["hour1"].ToString().Trim();
                    ODUnitPowerGrid1.Rows[0].Cells[1].Value = MyRow["hour2"].ToString().Trim();
                    ODUnitPowerGrid1.Rows[0].Cells[2].Value = MyRow["hour3"].ToString().Trim();
                    ODUnitPowerGrid1.Rows[0].Cells[3].Value = MyRow["hour4"].ToString().Trim();
                    ODUnitPowerGrid1.Rows[0].Cells[4].Value = MyRow["hour5"].ToString().Trim();
                    ODUnitPowerGrid1.Rows[0].Cells[5].Value = MyRow["hour6"].ToString().Trim();
                    ODUnitPowerGrid1.Rows[0].Cells[6].Value = MyRow["hour7"].ToString().Trim();
                    ODUnitPowerGrid1.Rows[0].Cells[7].Value = MyRow["hour8"].ToString().Trim();
                    ODUnitPowerGrid1.Rows[0].Cells[8].Value = MyRow["hour9"].ToString().Trim();
                    ODUnitPowerGrid1.Rows[0].Cells[9].Value = MyRow["hour10"].ToString().Trim();
                    ODUnitPowerGrid1.Rows[0].Cells[10].Value = MyRow["hour11"].ToString().Trim();
                    ODUnitPowerGrid1.Rows[0].Cells[11].Value = MyRow["hour12"].ToString().Trim();
                    ODUnitPowerGrid2.Rows[0].Cells[0].Value = MyRow["hour13"].ToString().Trim();
                    ODUnitPowerGrid2.Rows[0].Cells[1].Value = MyRow["hour14"].ToString().Trim();
                    ODUnitPowerGrid2.Rows[0].Cells[2].Value = MyRow["hour15"].ToString().Trim();
                    ODUnitPowerGrid2.Rows[0].Cells[3].Value = MyRow["hour16"].ToString().Trim();
                    ODUnitPowerGrid2.Rows[0].Cells[4].Value = MyRow["hour17"].ToString().Trim();
                    ODUnitPowerGrid2.Rows[0].Cells[5].Value = MyRow["hour18"].ToString().Trim();
                    ODUnitPowerGrid2.Rows[0].Cells[6].Value = MyRow["hour19"].ToString().Trim();
                    ODUnitPowerGrid2.Rows[0].Cells[7].Value = MyRow["hour20"].ToString().Trim();
                    ODUnitPowerGrid2.Rows[0].Cells[8].Value = MyRow["hour21"].ToString().Trim();
                    ODUnitPowerGrid2.Rows[0].Cells[9].Value = MyRow["hour22"].ToString().Trim();
                    ODUnitPowerGrid2.Rows[0].Cells[10].Value = MyRow["hour23"].ToString().Trim();
                    ODUnitPowerGrid2.Rows[0].Cells[11].Value = MyRow["hour24"].ToString().Trim();


                }
            }
            catch
            {

            }


        }



        private void FillODPlant(string Plant, string date)
        {
            ////////////////////filled by operational data///////////////////////////////////////
            label114.Text = "";
            /////////////////////////////////////////////////////////////////////////////

            dataGridDispatch.DataSource = null;
            if (dataGridDispatch.Rows != null) dataGridDispatch.Rows.Clear();
            dataGridDispatch.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;


            if (date == "")
            {
                DataTable date1 = Utilities.GetTable("select  max(StartDate) from dbo.Dispathable where PPID='" + Plant + "'");
                date = date1.Rows[0][0].ToString().Trim();
                faDatePickDispatch.Text = date1.Rows[0][0].ToString().Trim();
            }
            DataTable filltable = Utilities.GetTable("select Hour,Block,DeclaredCapacity,DispachableCapacity,PackageType from dbo.Dispathable where PPID='" + int.Parse(Plant) + "'and StartDate='" + date + "'order by Block asc ");

            DataTable drfull = Utilities.GetTable("select FilledBy,TimeFilled from dbo.Dispathable where PPID='" + int.Parse(Plant) + "'and StartDate='" + date + "'");

            if (filltable.Rows.Count > 0)
            {
                dataGridDispatch.RowCount = filltable.Rows.Count;
                int i = 0;

                ///////////////////filled by/////////////////////////////////////////////////////

                try
                {
                    if (drfull.Rows.Count > 0)
                    {
                        if (drfull.Rows[0]["FilledBy"].ToString() != "" && drfull.Rows[0]["TimeFilled"].ToString()!="")
                        {
                            string[] time = drfull.Rows[0]["TimeFilled"].ToString().Split('|');

                            label114.Text = "Filled By : " + drfull.Rows[0]["FilledBy"].ToString() + "   At Date : " + time[0] + "  Hour : " + time[1];
                        }
                        else
                            label114.Text = "";
                    }
                }
                catch
                {

                }
                ////////////////////////////////////////////////////////////////////////////

                foreach (DataRow mrow in filltable.Rows)
                {

                    dataGridDispatch.Rows[i].Cells[0].Value = mrow[0].ToString();
                    dataGridDispatch.Rows[i].Cells[1].Value = mrow[1].ToString().Trim();
                    dataGridDispatch.Rows[i].Cells[3].Value = mrow[2].ToString();
                    dataGridDispatch.Rows[i].Cells[4].Value = mrow[3].ToString();
                    if (mrow[4].ToString().Trim() == "0")
                    {
                        dataGridDispatch.Rows[i].Cells[2].Value = mrow[1].ToString().Substring(0,1).ToUpper();
                        //if (PPID == "232") dataGridDispatch.Rows[i].Cells[2].Value = "CC";
                        if (Findcconetype(PPID)) dataGridDispatch.Rows[i].Cells[2].Value = "CC";

                    }
                    else
                    {
                        dataGridDispatch.Rows[i].Cells[2].Value = "CC";
                    }
                    i++;
                }

            }
            else
            {
                faDatePickDispatch.SelectedDateTime = System.DateTime.Now;

            }
        }

        private void faDatePickDispatch_ValueChanged(object sender, EventArgs e)
        {
            dataGridDispatch.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridDispatch.DataSource = null;

            //filled by
            label114.Text = "";

            if (faDatePickDispatch.Text != "")
            {
                DataTable filltable = Utilities.GetTable("select Hour,DeclaredCapacity,DispachableCapacity,Block from dbo.Dispathable where PPID='" + int.Parse(PPID) + "'and StartDate='" + faDatePickDispatch.Text.Trim() + "'order by Block,Hour asc ");
                if (filltable.Rows.Count > 0)
                {
                    //dataGridDispatch.DataSource = filltable;
                    FillODPlant(PPID, faDatePickDispatch.Text.Trim());
                    faDatePickDispatch.Text = faDatePickDispatch.Text.Trim();
                }
                else
                    dataGridDispatch.Rows.Clear();
            }
        }
            


        private void ODUnitPowerGrid1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                DataObject d = ODUnitPowerGrid1.GetClipboardContent();
                Clipboard.SetDataObject(d);
                e.Handled = true;
            }
            else if (e.Control && e.KeyCode == Keys.V)
            {
                string s = Clipboard.GetText();
                string[] lines = s.Split('\n');
                int row = ODUnitPowerGrid1.CurrentCell.RowIndex;
                int col = ODUnitPowerGrid1.CurrentCell.ColumnIndex;
                foreach (string line in lines)
                {
                    if (row < ODUnitPowerGrid1.RowCount && line.Length > 0)
                    {
                        string[] cells = line.Split('\t');
                        for (int i = 0; i < cells.GetLength(0); ++i)
                        {
                            if (col + i < this.ODUnitPowerGrid1.ColumnCount)
                            {
                                ODUnitPowerGrid1[col + i, row].Value = Convert.ChangeType(cells[i], ODUnitPowerGrid1[col + i, row].ValueType);
                            }
                            else
                            {
                                break;
                            }
                        }
                        row++;
                    }
                    else
                    {
                        break;
                    }
                }
            }

        }


        private void ODUnitPowerGrid2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                DataObject d = ODUnitPowerGrid2.GetClipboardContent();
                Clipboard.SetDataObject(d);
                e.Handled = true;
            }
            else if (e.Control && e.KeyCode == Keys.V)
            {
                string s = Clipboard.GetText();
                string[] lines = s.Split('\n');
                int row = ODUnitPowerGrid2.CurrentCell.RowIndex;
                int col = ODUnitPowerGrid2.CurrentCell.ColumnIndex;
                foreach (string line in lines)
                {
                    if (row < ODUnitPowerGrid2.RowCount && line.Length > 0)
                    {
                        string[] cells = line.Split('\t');
                        for (int i = 0; i < cells.GetLength(0); ++i)
                        {
                            if (col + i < this.ODUnitPowerGrid2.ColumnCount)
                            {
                                ODUnitPowerGrid2[col + i, row].Value = Convert.ChangeType(cells[i], ODUnitPowerGrid2[col + i, row].ValueType);
                            }
                            else
                            {
                                break;
                            }
                        }
                        row++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

     

        private void ODUnitPowerStartDate_ValueChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (ODUnitPowerCheck.Checked)
            {

                ODUnitPowerGrid1.Rows.Clear();
                ODUnitPowerGrid2.Rows.Clear();
                labelupdatetime.Text = "";
                textupdatetime.Visible = false;

               
                string type = ODPackLb.Text;
                string unitlable = ODUnitLb.Text;
                if (type.Contains("Combined")) type = "CC";
                DataTable morethanone = Utilities.GetTable(" SELECT COUNT(StartDate) FROM PowerLimitedUnit WHERE PPID='" + PPID + "' AND UnitCode='" + unitlable + "' AND PackageType='" + type + "' and isEmpty=0 ");
                int numdate = int.Parse(morethanone.Rows[0][0].ToString());
                if (numdate > 1)
                {
                    string opdate = ODUnitPowerStartDate.Text;

                    DataTable odatatable = Utilities.GetTable("SELECT StartDate,EndDate,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6," +
                     "Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21," +
                     "Hour22,Hour23,Hour24,Time,isRec FROM PowerLimitedUnit WHERE UnitCode='" + unitlable + "' AND PackageType='" + type + "' AND PPID=" + PPID + "and StartDate='" + opdate + "' and isEmpty=0 ");

                    foreach (DataRow MyRow in odatatable.Rows)
                    {

                        //ODUnitPowerCheck.Checked = true;
                        //ODUnitPowerStartDate.IsNull = false;
                        // ODUnitPowerEndDate.IsNull = false;
                        ODUnitPowerStartDate.Text = MyRow[0].ToString().Trim();
                        // ODUnitPowerEndDate.Text = MyRow[1].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[0].Value = MyRow[2].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[1].Value = MyRow[3].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[2].Value = MyRow[4].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[3].Value = MyRow[5].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[4].Value = MyRow[6].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[5].Value = MyRow[7].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[6].Value = MyRow[8].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[7].Value = MyRow[9].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[8].Value = MyRow[10].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[9].Value = MyRow[11].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[10].Value = MyRow[12].ToString().Trim();
                        ODUnitPowerGrid1.Rows[0].Cells[11].Value = MyRow[13].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[0].Value = MyRow[14].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[1].Value = MyRow[15].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[2].Value = MyRow[16].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[3].Value = MyRow[17].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[4].Value = MyRow[18].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[5].Value = MyRow[19].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[6].Value = MyRow[20].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[7].Value = MyRow[21].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[8].Value = MyRow[22].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[9].Value = MyRow[23].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[10].Value = MyRow[24].ToString().Trim();
                        ODUnitPowerGrid2.Rows[0].Cells[11].Value = MyRow[25].ToString().Trim();
                        if (MyRow[27].ToString() == "True")
                        {
                            labelupdatetime.Text = "Rec User Has Recorded";
                        }
                        else
                        {
                            labelupdatetime.Text = ODPlantLb.Text + "  Plant User Has Recorded";
                        }
                        textupdatetime.Visible = true;
                        textupdatetime.Text = MyRow[26].ToString();

                    }
                }
            }
        }

        private void linkoutservice_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string unit = ODUnitLb.Text.Trim();
            unit = unit.Trim();
            string package = ODPackLb.Text.Trim();
            package = package.Trim();
            string type = unit.ToLower();
            if (type.Contains("steam")) type = "Steam";
            else type = "Gas";

            //Get Index Of Selected Unit(Row) in GridView (In Generad data Tab)
            int index = 0;
            if (GDSteamGroup.Text.Contains(package))
                for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                {
                    if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                        index = i;
                }
            else
                for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                {
                    if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                        index = i;
                }


            EditOperationalForm frm = new EditOperationalForm(PPID, ODUnitLb.Text.Trim(), "outservice");
            frm.ShowDialog();
            FillODUnit(ODUnitLb.Text.Trim(), package, index);

        }

        private void linkmaintenance_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            string unit = ODUnitLb.Text.Trim();
            unit = unit.Trim();
            string package = ODPackLb.Text.Trim();
            package = package.Trim();
            string type = unit.ToLower();
            if (type.Contains("steam")) type = "Steam";
            else type = "Gas";

            //Get Index Of Selected Unit(Row) in GridView (In Generad data Tab)
            int index = 0;
            if (GDSteamGroup.Text.Contains(package))
                for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                {
                    if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                        index = i;
                }
            else
                for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                {
                    if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                        index = i;
                }


            EditOperationalForm frm = new EditOperationalForm(PPID, ODUnitLb.Text.Trim(), "maintenance");
            frm.ShowDialog();
            FillODUnit(ODUnitLb.Text.Trim(), package, index);
        }

        private void linkfuel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string unit = ODUnitLb.Text.Trim();
            unit = unit.Trim();
            string package = ODPackLb.Text.Trim();
            package = package.Trim();
            string type = unit.ToLower();
            if (type.Contains("steam")) type = "Steam";
            else type = "Gas";

            //Get Index Of Selected Unit(Row) in GridView (In Generad data Tab)
            int index = 0;
            if (GDSteamGroup.Text.Contains(package))
                for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                {
                    if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                        index = i;
                }
            else
                for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                {
                    if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                        index = i;
                }




            EditOperationalForm frm = new EditOperationalForm(PPID, ODUnitLb.Text.Trim(), "secondfuel");
            frm.ShowDialog();
            FillODUnit(ODUnitLb.Text.Trim(), package, index);
        }

       


    }
}