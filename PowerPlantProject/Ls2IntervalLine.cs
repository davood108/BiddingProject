﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using NRI.SBS.Common;
using FarsiLibrary.Utils;
using System.Collections;
using System.IO;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using Auto_Update_Library;

namespace PowerPlantProject
{
    public partial class Ls2IntervalLine : Form
    {
        List<PersianDate> missingDates;
        Thread thread;
        public Ls2IntervalLine()
        {
            InitializeComponent();

            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Visible = true;

            DataTable numline = Utilities.ls2returntbl("select distinct count(*) from dbo.TransLine");
           
            missingDates = GetDatesBetween();
            int max = missingDates.Count * int.Parse(numline.Rows[0][0].ToString());
            if (max == 0) max = 1;
            progressBar1.Maximum = max;


            button1.Enabled = false;
            label3.Visible = true;
            thread = new Thread(LongTask);
            thread.IsBackground = true;
            thread.Start();
           
        }
        public void UpdateProgressBar(int value, string description)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<int, string>(UpdateProgressBar), new object[] { value, description });
                return;
            }
            // lblTitle.Text = description;

            progressBar1.Value = value;
            Thread.Sleep(1);

        }
        private void LongTask()
        {
            string allmessage = "";
            missingDates = GetDatesBetween();
            int p = 1;
            DataTable numline = null;
            if (Auto_Update_Library.BaseData.GetInstance().CounterPath != "")
            {
                foreach (PersianDate date in missingDates)
                {
                    allmessage = "";
                     numline = Utilities.ls2returntbl("select distinct count(*) from dbo.TransLine");
                    for (int k = 0; k < int.Parse(numline.Rows[0][0].ToString()); k++)
                    {
                        string message = "";
                        if (numline.Rows[0][0].ToString() != "0")
                        {
                            UpdateProgressBar(p, "Getting Data For");
                            allmessage += ReadLS2Trans(k, date, message);
                            p++;
                        }
                    }

                }

                UpdateProgressBar(progressBar1.Maximum, "");

                if (allmessage != "" && allmessage.Length < 4400)
                {

                    MessageBox.Show(allmessage, "There is No Data for");
                }
                if (int.Parse(numline.Rows[0][0].ToString())==0)
                {
                     MessageBox.Show(" There Is No Serial Number!");

                }
            }
            else
            {
                MessageBox.Show(" Counter Path Is Not Set !");
            }
        }
        private string ReadLS2Trans(int k, PersianDate date, string msg)
        {

            if (!Auto_Update_Library.BaseData.GetInstance().Useftpcounter)
            {

                DataTable tableline = Utilities.ls2returntbl("select distinct  LineCode from dbo.TransLine ");
                string[] arrlinecode = new string[tableline.Rows.Count];
                for (int i = 0; i < tableline.Rows.Count; i++)
                {
                    arrlinecode[i] = tableline.Rows[i][0].ToString().Trim();
                }
                DataTable trans1 = Utilities.ls2returntbl("SELECT  PowerSerialtrans FROM dbo.Lines WHERE LineCode='" + arrlinecode[k] + "'");
                string formessserial = arrlinecode[k];
                /////////////////////////////detect path//////////////////////////////
                string temppath = BaseData.GetInstance().M009Path;
                temppath += @"\" + trans1.Rows[0][0].ToString().Trim() + ".xls";

                string Path = Auto_Update_Library.BaseData.GetInstance().CounterPath;
                DateTime Miladi = PersianDateConverter.ToGregorianDateTime(date).Date;
                string smiladi = Miladi.ToString("d");
                string[] splitmiladi = smiladi.Split('/');

                string yearpath = splitmiladi[0];
                string monthpath = splitmiladi[1];  
                string daypath = splitmiladi[2];

                string virtualpath = @"\" + yearpath + @"\" + yearpath + monthpath + daypath;
                string pathname = folderFormat(smiladi, "", "Counters");
                if (pathname != "") virtualpath = pathname;


                Path += virtualpath + @"\" + trans1.Rows[0][0].ToString().Trim();
                Path += ".xls";
                if (File.Exists(temppath)) File.Delete(temppath);
                if (File.Exists(Path.Replace("xls", "ls2")))
                {
                    //Save AS ls2file to xls file
                    Path = Path.Replace("xls", "ls2");
                    Excel.Application exobj = new Excel.Application();
                    exobj.Visible = false;
                    exobj.UserControl = true;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    Excel.Workbook book = (Excel.Workbook)exobj.Workbooks.Add(Path);
                    Path = Path.Replace("ls2", "xls");
                    book.SaveAs(temppath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                    ////////////////////////
                    book.Close(false, book, Type.Missing);
                    exobj.Workbooks.Close();
                    exobj.Quit();
                    ///////////////////////


                    //Read From Excel File
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + temppath + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = trans1.Rows[0][0].ToString().Trim();
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    DataTable TempTable = objDataset1.Tables[0];
                    objConn.Close();

                    int index = 4;
                    while (index < TempTable.Rows.Count)
                    {
                        if (TempTable.Rows[index][1].ToString().Trim() != "")
                        {
                            int finalindex = index;
                            //Seperate Time
                            string myDateTime = TempTable.Rows[index][1].ToString().Trim();
                            string TempTime = myDateTime.Substring(myDateTime.Length - 11, 11).Trim();
                            string myDate = myDateTime.Remove(myDateTime.Length - 11, 11).Trim();
                            char[] temp = new char[1];
                            temp[0] = ':';
                            int pos1 = TempTime.IndexOfAny(temp);
                            string myTime = TempTime.Remove(pos1);
                            if ((TempTime.Contains("PM")) && (myTime != "12"))
                                myTime = (int.Parse(myTime) + 12).ToString();
                            if ((TempTime.Contains("AM")) && (myTime == "12"))
                                myTime = "0";
                            //Seperate Date
                            System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
                            DateTime CurDate = DateTime.Parse(myDate);
                            int imonth = pr.GetMonth(CurDate);
                            int iyear = pr.GetYear(CurDate);
                            int iday = pr.GetDayOfMonth(CurDate);
                            string day = iday.ToString();
                            if (int.Parse(day) < 10) day = "0" + day;
                            string month = imonth.ToString();
                            if (int.Parse(month) < 10) month = "0" + month;
                            myDate = iyear.ToString() + "/" + month + "/" + day;
                            //-------------------------- is6secondarymonth --------------------------------//


                            if (int.Parse(month) >= 6)
                            {
                                if (TempTable.Rows[finalindex - 1][1].ToString().Trim() != "")
                                    finalindex = index - 1;
                            }
                            //---------------------------------------------------------------------------
                            //Is There any Data for this Plant(and type) at this Date in the MainFRM009 and DetailFRM009?
                            DataTable IsThere = null;
                            IsThere = Utilities.ls2returntbl("SELECT COUNT(*) FROM DetailFRM009LINE WHERE LineCode='" + arrlinecode[k] + "' AND TargetMarketDate='" + myDate + "' AND Hour=" + myTime);
                            int check = int.Parse(IsThere.Rows[0][0].ToString());

                            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                            myConnection.Open();
                            SqlCommand MyCom = new SqlCommand();
                            MyCom.Connection = myConnection;
                            MyCom.Parameters.Add("@ld", SqlDbType.NChar, 10);

                            MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                            MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);

                            MyCom.Parameters.Add("@P", SqlDbType.Real);
                            MyCom.Parameters.Add("@fl", SqlDbType.Int);

                            //Insert Into DATABASE
                            if (check == 0)
                                MyCom.CommandText = "INSERT INTO [DetailFRM009LINE] (TargetMarketDate,LineCode" +
                                ",Hour,P,FlagLine) VALUES (@tdate,@ld,@hour,@P,@fl)";
                            else
                                MyCom.CommandText = "UPDATE DetailFRM009LINE SET P=@P,FlagLine=@fl WHERE " +
                                "TargetMarketDate=@tdate AND LineCode=@ld " +
                                " AND Hour=@hour";

                            MyCom.Parameters["@ld"].Value = arrlinecode[k].ToString();
                            MyCom.Parameters["@tdate"].Value = myDate;
                            MyCom.Parameters["@hour"].Value = myTime;
                            if (TempTable.Rows[index][2].ToString().Trim() != "0")
                            {
                                MyCom.Parameters["@fl"].Value = -1;
                                if (TempTable.Rows[finalindex][2].ToString().Trim() != "")
                                {
                                    MyCom.Parameters["@P"].Value = MyDoubleParse(TempTable.Rows[finalindex][2].ToString().Trim()) / 1000000.0;
                                }
                            }
                            else
                            {
                                MyCom.Parameters["@fl"].Value = 1;
                                if (TempTable.Rows[finalindex][3].ToString().Trim() != "")
                                {
                                    MyCom.Parameters["@P"].Value = MyDoubleParse(TempTable.Rows[finalindex][3].ToString().Trim()) / 1000000.0;
                                }
                            }
                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                                //MessageBox.Show("Invalid Data . \r\n UnSuccessFull Process .");

                            }
                            myConnection.Close();
                        }
                        index++;
                    }

                    System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                   if (File.Exists(temppath)) File.Delete(temppath);
                }
                else
                {
                    msg += " < Date :" + date.ToString("d") + " and Line : " + formessserial + ">...";

                }


            }
            else
            {

                MessageBox.Show("Invalid Data");
                thread.Abort();

            }

            return msg;
        }

        private List<PersianDate> GetDatesBetween()
        {


            if (datePickerFrom != null)
            {
                List<PersianDate> missingDates = new List<PersianDate>();


                DateTime dateFrom = datePickerFrom.SelectedDateTime.Date;
                DateTime dateTo = datePickerTo.SelectedDateTime.Date;
                missingDates.Add(dateFrom);

                TimeSpan span = dateTo - dateFrom;
                while (span.Days > 0)
                {
                    dateFrom = dateFrom.AddDays(1);
                    missingDates.Add(new PersianDate(dateFrom));
                    span = dateTo - dateFrom;
                }
                return missingDates;
            }
            else
                return null;
        }

        private void Ls2IntervalLine_Load(object sender, EventArgs e)
        {
            datePickerFrom.Text = new PersianDate(DateTime.Now).ToString("d");
            datePickerTo.Text = new PersianDate(DateTime.Now.AddDays(1)).ToString("d");
            datePickerFrom.SelectedDateTime = DateTime.Now;
            datePickerTo.SelectedDateTime = DateTime.Now.AddDays(1);
        }

        private void datePickerTo_ValueChanged(object sender, EventArgs e)
        {
            lblDate.Visible = true;
            button1.Enabled = true;
            label3.Visible = false;

            if (datePickerFrom.Text != "")
            {

                DateTime showtime = PersianDateConverter.ToGregorianDateTime(datePickerFrom.Text).Date;
                string labletime = showtime.ToString("d");
                labletime = labletime.Replace("/", "-");
                lblDate.Text = " Date - DA :" + labletime;


            }
            if (datePickerTo.Text != "")
            {

                DateTime showtimeend = PersianDateConverter.ToGregorianDateTime(datePickerTo.Text).Date;
                string labletimeend = showtimeend.ToString("d");
                labletimeend = labletimeend.Replace("/", "-");
                labelenddate.Text = " Date - DA : " + labletimeend;
            }

        }

        private void datePickerFrom_ValueChanged(object sender, EventArgs e)
        {
            lblDate.Visible = true;
            button1.Enabled = true;
            label3.Visible = false;

            if (datePickerFrom.Text != "")
            {

                DateTime showtime = PersianDateConverter.ToGregorianDateTime(datePickerFrom.Text).Date;
                string labletime = showtime.ToString("d");
                labletime = labletime.Replace("/", "-");
                lblDate.Text = " Date - DA :" + labletime;


            }
        }

        private void Ls2IntervalLine_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (thread != null)
            {
                thread.Abort();
            }
        }

        private void Ls2IntervalLine_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (thread != null)
            {
                thread.Abort();
            }
        }
        private double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        public string folderFormat(string date, string ppid, string type)
        {

            //Year/2char
            //Year/4char
            //month/2char
            //month/1char
            //day/2char
            //day/1char
            //PlantName/English
            //PlantName/Farsi
            //MonthName/Finglish
            //Year-6MaheAval
            //Year-6MaheDovom
            string farsi = "";
            string english = "";
            DataTable vv = Utilities.GetTable("select * from powerplant where ppid='" + ppid + "'");
            if (vv.Rows.Count > 0)
            {
                farsi = vv.Rows[0]["PNameFarsi"].ToString().Trim();
                english = vv.Rows[0]["PPName"].ToString().Trim();

            }
            string xcmonth = "Year-6MaheAval";
            string year = date.Substring(0, 4).Trim();
            string month = date.Substring(5, 2).Trim();
            string day = date.Substring(8, 2).Trim();
            if (int.Parse(month) > 6) xcmonth = "Year-6MaheDovom";
            string pathname = "";
            DataTable dddd = Utilities.GetTable("select * from FilesNameFormat where filename='" + type + "'");
            if (dddd.Rows.Count > 0 && dddd.Rows[0]["FoldersFormat"].ToString().Trim() != "")
            {

                string[] x = dddd.Rows[0]["FoldersFormat"].ToString().Trim().Split('*');

                for (int i = 0; i < x.Length; i++)
                {

                    if (x[i].Trim() == "Plantname/english")
                    {
                        pathname += @"\" + english;
                    }
                    else if (x[i].Trim() == "Plantname/Farsi")
                    {
                        pathname += @"\" + farsi;
                    }
                    else if (x[i].Trim() == "PlantID")
                    {
                        pathname += @"\" + ppid;
                    }

                    else if (x[i].Trim() == "space")
                    {
                        pathname += @"\" + " ";
                    }
                    else if (x[i].Trim() == "Year/2char")
                    {
                        pathname += @"\" + year.Substring(2, 2).Trim();
                    }
                    else if (x[i].Trim() == "Year/4char")
                    {
                        pathname += @"\" + year.Trim();
                    }
                    else if (x[i].Trim() == "month/2char")
                    {
                        pathname += @"\" + month.Trim();

                    }
                    else if (x[i].Trim() == "month/1char")
                    {
                        //pathname += @"\" + month.Substring(1, 1).Trim();
                        pathname += @"\" + int.Parse(month);

                    }
                    else if (x[i].Trim() == "day/2char")
                    {
                        pathname += @"\" + day.Trim();

                    }
                    else if (x[i].Trim() == "day/1char")
                    {
                        pathname += @"\" + day.Substring(1, 1).Trim();

                    }
                    else if (x[i].Trim() == "MonthName/Finglish")
                    {
                        string Sendmonth = int.Parse(month).ToString().Trim();
                        switch (Sendmonth)
                        {
                            case "1":
                                Sendmonth = "Farvardin";
                                break;
                            case "2":
                                Sendmonth = "Ordibehesht";
                                break;
                            case "3":
                                Sendmonth = "Khordad";
                                break;
                            case "4":
                                Sendmonth = "Tir";
                                break;
                            case "5":
                                Sendmonth = "Mordad";
                                break;
                            case "6":
                                Sendmonth = "Shahrivar";
                                break;
                            case "7":
                                Sendmonth = "Mehr";
                                break;
                            case "8":
                                Sendmonth = "Aban";
                                break;
                            case "9":
                                Sendmonth = "Azar";
                                break;
                            case "10":
                                Sendmonth = "Day";
                                break;
                            case "11":
                                Sendmonth = "Bahman";
                                break;
                            case "12":
                                Sendmonth = "Esfand";
                                break;

                        }
                        pathname += @"\" + Sendmonth.ToString().Trim();
                    }
                    else if (x[i].Trim() == "Date Without Seperator/8char")
                    {
                        string strDate = date;
                        strDate = strDate.Remove(7, 1);
                        strDate = strDate.Remove(4, 1);
                        pathname += @"\" + strDate;
                    }
                    else
                    {
                        pathname += @"\" + x[i];
                    }

                }



                return pathname;
            }
            return pathname;
        }

    }
}
