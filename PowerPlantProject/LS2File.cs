﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using NRI.SBS.Common;
using FarsiLibrary.Utils;
using System.Collections;
using System.IO;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using Auto_Update_Library;

namespace PowerPlantProject
{
    public partial class LS2File : Form
    {
        ArrayList PPIDArray = new ArrayList();
        ArrayList PPIDType = new ArrayList();
        List<PersianDate> missingDates;
        Thread thread;

        public LS2File()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Visible = true;

            //--------------------max progress------------------------//
            PPIDArray.Clear();
            PPIDType.Clear();
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            Myda.SelectCommand = new SqlCommand("SELECT PPID FROM PowerPlant", myConnection);
            Myda.Fill(MyDS, "ppid");
            foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
            {
                PPIDArray.Add(MyRow["PPID"].ToString().Trim());
                PPIDType.Add("real");
            }

            for (int i = 0; i < PPIDArray.Count; i++)
            {
                SqlCommand mycom = new SqlCommand();
                mycom.Connection = myConnection;
                mycom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id SELECT @result2 =count(PPID) FROM [PPUnit] WHERE PPID=@id AND PackageType LIKE 'Combined Cycle%'";
                mycom.Parameters.Add("@id", SqlDbType.NChar, 10);
                mycom.Parameters["@id"].Value = PPIDArray[i].ToString();
                mycom.Parameters.Add("@result1", SqlDbType.Int);
                mycom.Parameters["@result1"].Direction = ParameterDirection.Output;
                mycom.Parameters.Add("@result2", SqlDbType.Int);
                mycom.Parameters["@result2"].Direction = ParameterDirection.Output;
                mycom.ExecuteNonQuery();
                int result1 = (int)mycom.Parameters["@result1"].Value;
                int result2 = (int)mycom.Parameters["@result2"].Value;
                if ((result1 > 1) && (result2 > 0))
                {
                    PPIDArray.Add((int.Parse(PPIDArray[i].ToString())) + 1);
                    PPIDType.Add("virtual");
                }
            }

            missingDates = GetDatesBetween();
            int max = (missingDates.Count) * (PPIDArray.Count);
            progressBar1.Maximum = max;
            //------------------------------------------------------------------------
            button1.Enabled = false;
            label3.Visible = true;
            thread = new Thread(LongTask);
            thread.IsBackground = true;
            thread.Start();    
         
           
            
            
        }
        public void UpdateProgressBar(int value, string description)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<int, string>(UpdateProgressBar), new object[] { value, description });
                return;
            }
           // lblTitle.Text = description;
         
            progressBar1.Value = value;
            Thread.Sleep(1);

        }
        private void LongTask()
        {

            PPIDArray.Clear();
            PPIDType.Clear();

            if (Auto_Update_Library.BaseData.GetInstance().CounterPath != "")
            {
                DataSet MyDS = new DataSet();
                SqlDataAdapter Myda = new SqlDataAdapter();
                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                Myda.SelectCommand = new SqlCommand("SELECT PPID FROM PowerPlant", myConnection);
                Myda.Fill(MyDS, "ppid");
                foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
                {
                    PPIDArray.Add(MyRow["PPID"].ToString().Trim());
                    PPIDType.Add("real");
                }

                for (int i = 0; i < PPIDArray.Count; i++)
                {
                    SqlCommand mycom = new SqlCommand();
                    mycom.Connection = myConnection;
                    mycom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id SELECT @result2 =count(PPID) FROM [PPUnit] WHERE PPID=@id AND PackageType LIKE 'Combined Cycle%'";
                    mycom.Parameters.Add("@id", SqlDbType.NChar, 10);
                    mycom.Parameters["@id"].Value = PPIDArray[i].ToString();
                    mycom.Parameters.Add("@result1", SqlDbType.Int);
                    mycom.Parameters["@result1"].Direction = ParameterDirection.Output;
                    mycom.Parameters.Add("@result2", SqlDbType.Int);
                    mycom.Parameters["@result2"].Direction = ParameterDirection.Output;
                    mycom.ExecuteNonQuery();
                    int result1 = (int)mycom.Parameters["@result1"].Value;
                    int result2 = (int)mycom.Parameters["@result2"].Value;
                    if ((result1 > 1) && (result2 > 0))
                    {
                        PPIDArray.Add((int.Parse(PPIDArray[i].ToString())) + 1);
                        PPIDType.Add("virtual");
                    }
                }

                string msg = "";
                missingDates = GetDatesBetween();
                int p = 1;
                foreach (PersianDate date in missingDates)
                {
                    msg = "";
                    for (int k = 0; k < PPIDArray.Count; k++)
                    {
                        if ((PPIDArray[k].ToString() != "0") && (PPIDType[k].ToString() == "real"))
                        {
                            UpdateProgressBar(p, "Getting Data For");
                            msg += ReadLS2Files(k, date) + "\r\n";
                            p++;
                        }
                    }
                   
                }
                UpdateProgressBar(progressBar1.Maximum, "");
                //////////////////errors

                if (msg.Contains("error") && msg.Length < 4400)
                {

                    MessageBox.Show(msg.Replace("error", ""));
                }
                else if (!msg.Contains("Invalid Path"))
                {
                    MessageBox.Show("Download Complete");

                }
                else
                {
                    MessageBox.Show("Invalid Path");

                }
            }
            else
            {
                MessageBox.Show(" Counter Path Is Not Set !");
            }
          

        }
        private string ReadLS2Files(int k,PersianDate date)
        {
            if (!Auto_Update_Library.BaseData.GetInstance().Useftpcounter)
            {
                
              
                DateTime Miladi = PersianDateConverter.ToGregorianDateTime(date).Date;
                string smiladi = Miladi.ToString("d");
                string[] splitmiladi = smiladi.Split('/');

                string yearpath = splitmiladi[0];
                string monthpath = splitmiladi[1];
                string daypath = splitmiladi[2];

                string virtualpath = @"\" + yearpath + @"\" + yearpath + monthpath + daypath;

                DataTable plantname = Utilities.ls2returntbl("select PPName from dbo.PowerPlant where PPID='" + PPIDArray[k] + "'");
                string ppnamek = plantname.Rows[0][0].ToString().Trim();

                string status = "Incomplete Data or Serial Number For Plant <<<<" + ppnamek + " In Date :" + date + ">>>>\r\n";
                int loopcount = 0;
                DataTable UnitsData = null;
                UnitsData = Utilities.ls2returntbl("SELECT UnitCode,PackageCode,PowerSerial,ConsumedSerial,StateConnection,BusNumber FROM [UnitsDataMain] WHERE PPID='" + PPIDArray[k] + "'");
                foreach (DataRow MyRow in UnitsData.Rows)
                {
                    for (int column = 0; column < UnitsData.Columns.Count; column++)
                    {
                        if (MyRow[column].ToString() == "") MyRow[column] = "0";
                        MyRow[4] = "0";
                        MyRow[5] = "0";
                    }
                    int PowerCount = 0, ConsumeCount = 0;
                    foreach (DataRow SecondRow in UnitsData.Rows)
                    {
                        if (SecondRow[2].ToString().Trim() == MyRow[2].ToString().Trim()) PowerCount++;
                        if (SecondRow[3].ToString().Trim() == MyRow[3].ToString().Trim()) ConsumeCount++;
                    }
                    MyRow[4] = PowerCount;
                    MyRow[5] = ConsumeCount;
                }
                //try
                //{
                foreach (DataRow MyRow in UnitsData.Rows)
                {

                    for (int Count = 2; Count < 4; Count++)
                    {
                        string temppath = BaseData.GetInstance().M009Path;
                        temppath += @"\" + MyRow[Count].ToString().Trim() + ".xls";

                        string Path = Auto_Update_Library.BaseData.GetInstance().CounterPath;
                        Path += virtualpath + @"\" + MyRow[Count].ToString().Trim();
                        Path += ".xls";
                        if (File.Exists(temppath)) File.Delete(temppath);
                        if (File.Exists(Path.Replace("xls", "ls2")))
                        {
                            //Save AS ls2file to xls file
                            Path = Path.Replace("xls", "ls2");
                            Excel.Application exobj = new Excel.Application();
                            exobj.Visible = false;
                            exobj.UserControl = true;
                            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                            Excel.Workbook book = (Excel.Workbook)exobj.Workbooks.Add(Path);
                            Path = Path.Replace("ls2", "xls");
                            book.SaveAs(temppath, Excel.XlFileFormat.xlWorkbookNormal, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                            ///////////////
                            book.Close(false, book, Type.Missing);
                            exobj.Workbooks.Close();
                            exobj.Quit();

                            //////////////////////

                            //Read From Excel File
                            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + temppath + ";Extended Properties=Excel 8.0";
                            OleDbConnection objConn = new OleDbConnection(sConnectionString);
                            objConn.Open();
                            string price = MyRow[Count].ToString().Trim();
                            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                            objAdapter1.SelectCommand = objCmdSelect;
                            DataSet objDataset1 = new DataSet();
                            objAdapter1.Fill(objDataset1);
                            DataTable TempTable = objDataset1.Tables[0];
                            objConn.Close();

                            if (Count == 2) //Power Serial Type
                            {
                                int index = 4;
                                while (index < TempTable.Rows.Count)
                                {
                                    if (TempTable.Rows[index][1].ToString().Trim() != "")
                                    {
                                        int finalindex = index;
                                        //Seperate Time
                                        string myDateTime = TempTable.Rows[index][1].ToString().Trim();
                                        string TempTime = myDateTime.Substring(myDateTime.Length - 11, 11).Trim();
                                        string myDate = myDateTime.Remove(myDateTime.Length - 11, 11).Trim();
                                        char[] temp = new char[1];
                                        temp[0] = ':';
                                        int pos1 = TempTime.IndexOfAny(temp);
                                        string myTime = TempTime.Remove(pos1);
                                        if ((TempTime.Contains("PM")) && (myTime != "12"))
                                            myTime = (int.Parse(myTime) + 12).ToString();
                                        if ((TempTime.Contains("AM")) && (myTime == "12"))
                                            myTime = "0";
                                        //Seperate Date
                                        System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
                                        DateTime CurDate = DateTime.Parse(myDate);
                                        int imonth = pr.GetMonth(CurDate);
                                        int iyear = pr.GetYear(CurDate);
                                        int iday = pr.GetDayOfMonth(CurDate);
                                        string day = iday.ToString();
                                        if (int.Parse(day) < 10) day = "0" + day;
                                        string month = imonth.ToString();
                                        if (int.Parse(month) < 10) month = "0" + month;
                                        myDate = iyear.ToString() + "/" + month + "/" + day;
                                        //-------------------------- is6secondarymonth --------------------------------//


                                        if (int.Parse(month) >= 6)
                                        {
                                            if (TempTable.Rows[finalindex - 1][1].ToString().Trim() != "")
                                                finalindex = index - 1;
                                        }
                                         //---------------------------------------------------------------------------
                                        //Is There any Data for this Plant(and type) at this Date in the MainFRM009 and DetailFRM009?
                                        DataTable IsThere = null;
                                        IsThere = Utilities.ls2returntbl("SELECT COUNT(*) FROM DetailFRM009 WHERE PPID='" + PPIDArray[k] + "' AND Block='" + MyRow[0].ToString().Trim() + "' AND TargetMarketDate='" + myDate + "' AND Hour=" + myTime);
                                        int check = int.Parse(IsThere.Rows[0][0].ToString());

                                        SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                                        myConnection.Open();
                                        SqlCommand MyCom = new SqlCommand();
                                        MyCom.Connection = myConnection;
                                        MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                                        MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                                        MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                                        MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                                        MyCom.Parameters.Add("@PCode", SqlDbType.Int);
                                        MyCom.Parameters.Add("@P", SqlDbType.Real);
                                        MyCom.Parameters.Add("@QC", SqlDbType.Real);
                                        MyCom.Parameters.Add("@QL", SqlDbType.Real);

                                        //Insert Into DATABASE
                                        if (check == 0)
                                            MyCom.CommandText = "INSERT INTO [DetailFRM009] (TargetMarketDate,PPID" +
                                            ",Block,PackageCode,Hour,P,QC,QL) VALUES (@tdate,@id,@block,@PCode,@hour,@P,@QC,@QL)";
                                        else
                                            MyCom.CommandText = "UPDATE DetailFRM009 SET P=@P,QC=@QC,QL=@QL WHERE " +
                                            "TargetMarketDate=@tdate AND PPID=@id AND Block=@block AND " +
                                            "PackageCode=@PCode AND Hour=@hour";

                                        MyCom.Parameters["@id"].Value = PPIDArray[k].ToString();
                                        MyCom.Parameters["@tdate"].Value = myDate;
                                        MyCom.Parameters["@block"].Value = MyRow[0].ToString().Trim();
                                        MyCom.Parameters["@PCode"].Value = MyRow[1].ToString().Trim();
                                        MyCom.Parameters["@hour"].Value = myTime;
                                        if (TempTable.Rows[finalindex][2].ToString().Trim() != "0")
                                            MyCom.Parameters["@P"].Value = (MyDoubleParse(TempTable.Rows[finalindex][2].ToString().Trim()) / (MyDoubleParse(MyRow[4].ToString().Trim()))) / 1000000.0;
                                        else if (TempTable.Rows[finalindex][3].ToString().Trim() != "0")
                                            MyCom.Parameters["@P"].Value = (MyDoubleParse(TempTable.Rows[finalindex][3].ToString().Trim()) / (MyDoubleParse(MyRow[4].ToString().Trim()))) / 1000000.0;
                                            
                                        else if (check != 0)
                                        {
                                            double consumed = 0.0;
                                            IsThere = Utilities.ls2returntbl("SELECT Consumed FROM DetailFRM009 WHERE PPID='" + PPIDArray[k] + "' AND Block='" + MyRow[0].ToString().Trim() + "' AND TargetMarketDate='" + myDate + "' AND Hour=" + myTime);
                                            if (IsThere.Rows[0][0].ToString() != "")
                                            {
                                                consumed = MyDoubleParse(IsThere.Rows[0][0].ToString().Trim());
                                            }

                                            MyCom.Parameters["@P"].Value = consumed;
                                        }

                                            ////////////if all is zero//////////////////////////////////
                                        else
                                            MyCom.Parameters["@P"].Value = 0.0;
                                        /////////////////////////////////////////////////////////////////

                                        if (TempTable.Rows[finalindex][4].ToString().Trim() != "")
                                            MyCom.Parameters["@QC"].Value = MyDoubleParse(TempTable.Rows[finalindex][4].ToString().Trim()) / 1000000.0;
                                        else MyCom.Parameters["@QC"].Value = 0;
                                        if (TempTable.Rows[finalindex][5].ToString().Trim() != "")
                                            MyCom.Parameters["@QL"].Value = MyDoubleParse(TempTable.Rows[finalindex][5].ToString().Trim()) / 1000000.0;
                                        else MyCom.Parameters["@QL"].Value = 0;
                                        try
                                        {
                                            MyCom.ExecuteNonQuery();
                                        }
                                        catch (Exception exp)
                                        {
                                            string str = exp.Message;
                                        }
                                        myConnection.Close();
                                    }
                                    index++;
                                }
                            }
                            else //if (Count==3):: Consumed Serial Type
                            {
                                int index = 4;
                                while (index < TempTable.Rows.Count)
                                {
                                    if (TempTable.Rows[index][1].ToString().Trim() != "")
                                    {
                                        int finalindex = index;
                                        //Seperate Time
                                        string myDateTime = TempTable.Rows[index][1].ToString().Trim();
                                        string TempTime = myDateTime.Substring(myDateTime.Length - 11, 11).Trim();
                                        string myDate = myDateTime.Remove(myDateTime.Length - 11, 11).Trim();
                                        char[] temp = new char[1];
                                        temp[0] = ':';
                                        int pos1 = TempTime.IndexOfAny(temp);
                                        string myTime = TempTime.Remove(pos1);
                                        if ((TempTime.Contains("PM")) && (myTime != "12"))
                                            myTime = (int.Parse(myTime) + 12).ToString();
                                        if ((TempTime.Contains("AM")) && (myTime == "12"))
                                            myTime = "0";
                                        //Seperate Date
                                        System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
                                        DateTime CurDate = DateTime.Parse(myDate);//DateTime.Now;
                                        int imonth = pr.GetMonth(CurDate);
                                        int iyear = pr.GetYear(CurDate);
                                        int iday = pr.GetDayOfMonth(CurDate);
                                        string day = iday.ToString();
                                        if (int.Parse(day) < 10) day = "0" + day;
                                        string month = imonth.ToString();
                                        if (int.Parse(month) < 10) month = "0" + month;
                                        myDate = iyear.ToString() + "/" + month + "/" + day;
                                        //-------------------------is6secondarymonth ---------------------------
                                        if (int.Parse(month) >= 6)
                                        {
                                            if (TempTable.Rows[finalindex - 1][1].ToString().Trim() != "")
                                                finalindex = index - 1;
                                        }
                                        //-------------------------------------------------------------
                                        //Is There any Data for this Plant(and type) at this Date in the MainFRM009 and DetailFRM009?
                                        DataTable IsThere = null;
                                        IsThere = Utilities.ls2returntbl("SELECT COUNT(*) FROM DetailFRM009 WHERE PPID='" + PPIDArray[k] + "' AND Block='" + MyRow[0].ToString().Trim() + "' AND TargetMarketDate='" + myDate + "' AND Hour=" + myTime);
                                        int check = int.Parse(IsThere.Rows[0][0].ToString());

                                        //SET P With Consumed
                                        //Insert Into DATABASE
                                        SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                                        myConnection.Open();
                                        SqlCommand MyCom = new SqlCommand();
                                        MyCom.Connection = myConnection;
                                        MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                                        MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                                        MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                                        MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                                        MyCom.Parameters.Add("@PCode", SqlDbType.Int);
                                        MyCom.Parameters.Add("@P", SqlDbType.Real);
                                        MyCom.Parameters.Add("@Consumed", SqlDbType.Real);

                                        if (check == 0)
                                            MyCom.CommandText = "INSERT INTO [DetailFRM009] (TargetMarketDate,PPID,Block,PackageCode,Hour" +
                                            ",Consumed) VALUES (@tdate,@id,@block,@PCode,@hour,@Consumed)";
                                        else //if (check != 0)
                                        {
                                            IsThere = Utilities.ls2returntbl("SELECT P FROM DetailFRM009 WHERE PPID='" + PPIDArray[k] + "' AND Block='" + MyRow[0].ToString().Trim() + "' AND TargetMarketDate='" + myDate + "' AND Hour=" + myTime);
                                            double MyP = MyDoubleParse(IsThere.Rows[0][0].ToString().Trim());

                                            if (MyP == 0)
                                                MyCom.CommandText = "UPDATE DetailFRM009 SET Consumed=@Consumed,P=@P WHERE Block=@block " +
                                                "AND PackageCode=@PCode AND Hour=@hour AND TargetMarketDate=@tdate AND PPID=@id";
                                            else //if (MyP!=0)
                                                MyCom.CommandText = "UPDATE DetailFRM009 SET Consumed=@Consumed WHERE Block=@block " +
                                                "AND PackageCode=@PCode AND Hour=@hour AND TargetMarketDate=@tdate AND PPID=@id";

                                        }

                                        MyCom.Parameters["@id"].Value = PPIDArray[k].ToString();
                                        MyCom.Parameters["@tdate"].Value = myDate;
                                        MyCom.Parameters["@block"].Value = MyRow[0].ToString().Trim();
                                        MyCom.Parameters["@PCode"].Value = MyRow[1].ToString().Trim();
                                        MyCom.Parameters["@hour"].Value = myTime;
                                        if (TempTable.Rows[finalindex][2].ToString().Trim() != "0")
                                            MyCom.Parameters["@Consumed"].Value = (MyDoubleParse(TempTable.Rows[finalindex][2].ToString().Trim()) / (MyDoubleParse(MyRow[5].ToString().Trim()))) / 1000000.0;
                                        else if (TempTable.Rows[finalindex][3].ToString().Trim() != "0")
                                            MyCom.Parameters["@Consumed"].Value = (MyDoubleParse(TempTable.Rows[finalindex][3].ToString().Trim()) / (MyDoubleParse(MyRow[5].ToString().Trim()))) / 1000000.0;
                                        else
                                            MyCom.Parameters["@Consumed"].Value = 0.0;

                                        MyCom.Parameters["@P"].Value = ((double)MyCom.Parameters["@Consumed"].Value / (MyDoubleParse(MyRow[4].ToString()))) / 1000000.0;

                                        try
                                        {
                                            MyCom.ExecuteNonQuery();
                                        }
                                        catch (Exception exp)
                                        {
                                            string str = exp.Message;
                                        }
                                        myConnection.Close();

                                    }
                                    index++;
                                }

                            }
                            //book.Close(false, book, Type.Missing);
                            //exobj.Workbooks.Close();
                            //exobj.Quit();
                            System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                            if (loopcount == 0)
                            {
                                status = status.Replace("Incomplete", "Complete");
                            }
                        }
                        else
                        {

                            if (Count == 2)
                            {
                                if (loopcount != 0)
                                {
                                    status = status.Replace("Complete", "Incomplete");
                                }
                                status += "error" + " Unit:  " + MyRow[0].ToString().Trim() + "   ";
                            }
                            loopcount++;
                        }

                        if (File.Exists(temppath)) File.Delete(temppath);
                    }

                }


                return status;
            }
            else
            {

                return "Invalid Path";
            }

           
        }

       

        private List<PersianDate> GetDatesBetween()
        {

            
            if (datePickerFrom != null)
            {
                List<PersianDate> missingDates = new List<PersianDate>();

               
                DateTime dateFrom = datePickerFrom.SelectedDateTime.Date;
                DateTime dateTo = datePickerTo.SelectedDateTime.Date; 
                missingDates.Add(dateFrom);

                TimeSpan span = dateTo - dateFrom;
                while (span.Days > 0)
                {
                    dateFrom = dateFrom.AddDays(1);
                    missingDates.Add(new PersianDate(dateFrom));
                    span = dateTo - dateFrom;
                }
                return missingDates;
            }
            else
                return null;
        }

        private void LS2Interval_Load(object sender, EventArgs e)
        {
            this.Text = "Ls2File";
            datePickerFrom.Text = new PersianDate(DateTime.Now).ToString("d");
            datePickerTo.Text = new PersianDate(DateTime.Now.AddDays(1)).ToString("d");
            datePickerFrom.SelectedDateTime = DateTime.Now;
            datePickerTo.SelectedDateTime = DateTime.Now.AddDays(1);
        }

        private void datePickerTo_ValueChanged(object sender, EventArgs e)
        {
            lblDate.Visible = true;
            button1.Enabled = true;
            label3.Visible = false;

            if (datePickerFrom.Text != "")
            {

                DateTime showtime = PersianDateConverter.ToGregorianDateTime(datePickerFrom.Text).Date;
                string labletime = showtime.ToString("d");
                labletime = labletime.Replace("/", "-");
                lblDate.Text = " Date - DA :" + labletime;


            }
            if (datePickerTo.Text != "")
            {

                DateTime showtimeend = PersianDateConverter.ToGregorianDateTime(datePickerTo.Text).Date;
                string labletimeend = showtimeend.ToString("d");
                labletimeend = labletimeend.Replace("/", "-");
                labelenddate.Text = " Date - DA : " + labletimeend;
            }


        }

        private void datePickerFrom_ValueChanged(object sender, EventArgs e)
        {
            lblDate.Visible = true;
            button1.Enabled = true;
            label3.Visible = false;

            if (datePickerFrom.Text != "")
            {

                DateTime showtime = PersianDateConverter.ToGregorianDateTime(datePickerFrom.Text).Date;
                string labletime = showtime.ToString("d");
                labletime = labletime.Replace("/", "-");
                lblDate.Text = " Date - DA :" + labletime;


            }
        }

        private void LS2Interval_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (thread != null)
            {
                thread.Abort();
            }
        }

        private void LS2Interval_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (thread != null)
            {
                thread.Abort();
            }
        }

        private double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }

      
     

     
        
    }
}
