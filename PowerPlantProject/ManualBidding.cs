﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using System.IO;
using Microsoft.Win32;


namespace PowerPlantProject
{
    public partial class ManualBidding : Form
    {
        bool isall;
        bool isavg;
        double CAPPRICE;
        public string Plant;
        public string CurrentDate;
        public string BiddingDate;
        Thread thread;
        string path;
        bool easyload;
        string maxbasedate;
        double Dist = 0;
        string sblock = "";
        string blockname002 = "";
        public ManualBidding(string plant,string curdate,string biddate)
        {
            Plant = plant;
            CurrentDate = curdate;
            BiddingDate = biddate;
            InitializeComponent();

            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////
                //////////////////////////////////textbox////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                        c.BackColor = FormColors.GetColor().Textbackcolor;

                }
                foreach (Control c in panel2.Controls)
                {
                    if (c is TextBox || c is ListBox)
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    else if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;

                }

                ////////////////////////////grid////////////////////////////////////
                dataGridView1.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                dataGridView1.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;
            }
            catch
            {

            }
        }

        private void ManualBidding_Load(object sender, EventArgs e)
        {
            DataTable dtname = Utilities.GetTable("select ppid from dbo.PowerPlant where PPName='" + Plant + "'");
            string[] bblock = Getblock002pack(int.Parse(dtname.Rows[0][0].ToString()));
          

            comboBox1.Text = bblock[0];

            for (int n=0 ;n<bblock.Length; n++)
            {
                comboBox1.Items.Add(bblock[n].Trim());

            }



            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" +BiddingDate + "'order by BaseID desc");
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                maxbasedate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                maxbasedate = dtsmaxdate.Rows[0]["Date"].ToString();
            }
            //
            double cap = 0.0;
            DataTable basetabledata = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
            if (basetabledata.Rows.Count > 0)
            {
                cap = MyDoubleParse(basetabledata.Rows[0][0].ToString());

            }
            if (cap == 0) cap = 330000;
            Dist = cap / 66.0;
            //
            System.Windows.Forms.Form.CheckForIllegalCrossThreadCalls = false;
            CAPPRICE = 0;
            fillgrid();
        }
        private void fillgrid()
        {
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            if (Plant != "All")
            {
                int i = 0;
                label1.Text = "Bidding for " + Plant + " Plant In Date : " + BiddingDate;
                DataTable dtname = Utilities.GetTable("select ppid from dbo.PowerPlant where PPName='" + Plant + "'");
                dataGridView1.DataSource = null;



                DataTable dt = Utilities.GetTable(" select * from ManualData where TargetMarketDate='" + BiddingDate + "'and PPID='" + dtname.Rows[0][0].ToString().Trim() + "'");

                if (dt.Rows.Count >= 23)
                {
                    dataGridView1.RowCount = dt.Rows.Count;
                    foreach (DataRow mrow in dt.Rows)
                    {

                        dataGridView1.Rows[i].Cells[0].Value = mrow["PPID"].ToString().Trim();
                        dataGridView1.Rows[i].Cells[1].Value = mrow["Block"].ToString().Trim();
                        dataGridView1.Rows[i].Cells[2].Value = mrow["Hour"].ToString().Trim();                   
                        dataGridView1.Rows[i].Cells[3].Value = mrow["PmaxPrice"].ToString().Trim();
                        dataGridView1.Rows[i].Cells[4].Value = mrow["Interval"].ToString().Trim();
                        i++;

                    }

                }
                else
                {

                    //dt = utilities.GetTable("select * from dbo.Dispathable where ppid='" + dtname.Rows[0][0].ToString().Trim() + "' and StartDate='1389/01/02'");
                    //dataGridView1.RowCount = dt.Rows.Count;
                    //foreach (DataRow mrow in dt.Rows)
                    //{

                    //    dataGridView1.Rows[i].Cells[0].Value = mrow["PPID"].ToString().Trim();
                    //    dataGridView1.Rows[i].Cells[1].Value = mrow["Block"].ToString().Trim();
                    //    dataGridView1.Rows[i].Cells[2].Value = mrow["Hour"].ToString().Trim();

                    //    i++;

                    //}



                    string[] bblock = Getblock002pack(int.Parse(dtname.Rows[0][0].ToString()));
                    dataGridView1.RowCount = bblock.Length * 24;
                    Array.Sort(bblock);
                    int b=0;
                    int c = 0;
                    foreach (string block in bblock)
                    {
                        for (int h = 0; h < 24; h++)
                        {

                              dataGridView1.Rows[c].Cells[0].Value = dtname.Rows[0][0].ToString().Trim();
                              dataGridView1.Rows[c].Cells[1].Value = block.ToString().Trim();
                              dataGridView1.Rows[c].Cells[2].Value = (h+1).ToString().Trim();
                              c++;
                        }

                        b++;

                    }
                }
            }
            else
            {
                isall = true;
                int i = 0;
                label1.Text = "Bidding for All Plant In Date : " + BiddingDate;
                DataTable dtname = Utilities.GetTable("select PPID from dbo.PowerPlant");

                dataGridView1.DataSource = null;


                string ppid = "";
                DataTable ddr = Utilities.GetTable("select PPName from dbo.BaseData where Date='" + maxbasedate + "'and BaseID=(select max(BaseID) from dbo.BaseData where Date='" + maxbasedate + "')");
                string[] sf = ddr.Rows[0][0].ToString().Trim().Split('-');

                DataTable du = Utilities.GetTable("select PPID from dbo.PowerPlant where PPName='" + sf[0].Trim() + "'");
                if (du.Rows.Count != 0)
                    ppid = du.Rows[0][0].ToString().Trim();


                
                DataTable dt = Utilities.GetTable(" select * from ManualData where TargetMarketDate='" + BiddingDate + "'and PPID='" +ppid + "'");

                if (dt.Rows.Count >= 23)
                {
                    dataGridView1.RowCount = dt.Rows.Count;
                    foreach (DataRow mrow in dt.Rows)
                    {

                        dataGridView1.Rows[i].Cells[0].Value = mrow["PPID"].ToString().Trim();
                        dataGridView1.Rows[i].Cells[1].Value = mrow["Block"].ToString().Trim();
                        dataGridView1.Rows[i].Cells[2].Value = mrow["Hour"].ToString().Trim();
                        dataGridView1.Rows[i].Cells[3].Value = mrow["PmaxPrice"].ToString().Trim();
                        dataGridView1.Rows[i].Cells[4].Value = mrow["Interval"].ToString().Trim();
                        i++;
                        if (i == 24) break;
                    }

                }
                else
                {
                    //string[] bblock = Getblock002pack(int.Parse(ppid));
                    ////dt = utilities.GetTable("select * from dbo.Dispathable where ppid='" + ppid + "' and StartDate='1389/01/02'");
                    //dataGridView1.RowCount = 24;
                    //for (int h = 0; h < 24;h++ )
                    //{

                    //    dataGridView1.Rows[h].Cells[0].Value = ppid.Trim();
                    //    dataGridView1.Rows[h].Cells[1].Value =bblock[0].ToString().Trim();
                    //    dataGridView1.Rows[h].Cells[2].Value = (h + 1).ToString().Trim();
                                               
                    //    if (h == 24) break;
                    //}
                    string[] bblock = Getblock002pack(int.Parse(ppid));
                    string bbblock1 = bblock[0];
                    for (int y = 0; y < bblock.Length; y++)
                    {
                        string temp = bblock[y].Trim();
                        if (bblock[y].Contains("-C")) bblock[y] = bblock[y].Replace("-C", "").Trim();
                        DataTable dtout = Utilities.GetTable(" select * from dbo.Dispathable where ppid='" + ppid + "'and Block='" + bblock[y].Trim() + "' and StartDate='" + BiddingDate + "'");
                        if (dtout.Rows.Count > 0)
                        {
                            if (dtout.Rows[0]["DeclaredCapacity"].ToString().Trim() != "0")
                            {
                                bbblock1 = temp.Trim();
                               //  bbblock1 = bblock[y].Trim();
                               // if (temp.Contains("-C")) bbblock1 = temp.Trim();

                                break;
                            }
                        }

                    }
                    //dt = utilities.GetTable("select * from dbo.Dispathable where ppid='" + ppid + "' and StartDate='1389/01/02'");
                    dataGridView1.RowCount = 24;
                    for (int h = 0; h < 24; h++)
                    {

                        dataGridView1.Rows[h].Cells[0].Value = ppid.Trim();
                        dataGridView1.Rows[h].Cells[1].Value = bbblock1.Trim();
                        dataGridView1.Rows[h].Cells[2].Value = (h + 1).ToString().Trim();

                        if (h == 24) break;
                    }
                }
                dataGridView1.RowCount = 24;
                //for (int i = (dataGridView1.RowCount - 2); i > 1; i--)
                //    dataGridView1.Rows.Remove(dataGridView1.Rows[i]);
            }
        }
        private void btnExport_Click(object sender, EventArgs e)
        {
           
          
            folderBrowserDialog1.ShowNewFolderButton = true;
            DialogResult answer = folderBrowserDialog1.ShowDialog();
            if (answer == DialogResult.OK)
            {
                path = folderBrowserDialog1.SelectedPath;
                progressBar1.Visible = true;
                progressBar1.Maximum = 3;
                UpdateProgressBar(1, "kk");
                //-------------------forall--------------------\\
                string strCmd = "select PPID from powerplant";
                DataTable drr = Utilities.GetTable(strCmd);

                if (Plant == "All")
                {
                    int n = 1;
                    progressBar1.Maximum = (drr.Rows.Count) * 2;
                    foreach (DataRow m in drr.Rows)
                    {

                        fillallplant(int.Parse(m[0].ToString()));
                        UpdateProgressBar(n, "kk");
                        n++;
                    }

                }
              
                ///////////////////////////////////////////////////
               
                thread = new Thread(long_task);
                thread.IsBackground = true;
                thread.Start();                 
                             
               
            }

            else
            {
                MessageBox.Show("Please Fill Path ...");

            }
            //UpdateProgressBar(progressBar1.Maximum, "");
            //progressBar1.Visible = false;
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            progressBar1.Visible = false;

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {

                string PPID = dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
                string Block = dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
                string Hour = dataGridView1.Rows[i].Cells[2].Value.ToString().Trim();
            
               

                double max = 0.0;
                if (dataGridView1.Rows[i].Cells[3].Value != null)
                {
                    max = MyDoubleParse(dataGridView1.Rows[i].Cells[3].Value.ToString().Trim());
                }

                double cap = 0.0;
                DataTable basetabledata = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
                if (basetabledata.Rows.Count > 0)
                {
                    cap = MyDoubleParse(basetabledata.Rows[0][0].ToString()) ;

                }

                if ( max<=cap )
                {
                    double interval = 0.0;
                    if (dataGridView1.Rows[i].Cells[4].Value != null)
                    {
                        interval = MyDoubleParse(dataGridView1.Rows[i].Cells[4].Value.ToString().Trim());
                    }



                    DataTable dt1 = Utilities.GetTable("delete from ManualData where TargetMarketDate='" + BiddingDate + "'and PPID='" + PPID +
                    "'and Block='" + Block + "'and Hour='" + Hour + "'");



                    dt1 = Utilities.GetTable("insert into ManualData (TargetMarketDate,PPID,Block,Hour,PmaxPrice,Interval) values ('"
                   + BiddingDate + "','" + PPID + "','" + Block + "','" + Hour + "','" + max + "','" + interval + "')");
                }
                else
                {
                   
                    MessageBox.Show("Please Fill All fild Correctly !");
                    break;
                }
            }
        }
    
        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "" ||str.Trim() ==null)
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }

        //old private void btnsavesimilar_Click(object sender, EventArgs e)
        //{
        //    dataGridView1.Visible = true;
        //    progressBar1.Visible = false;
        //    fillsimillar();

        //    for (int i = 0; i < dataGridView1.Rows.Count; i++)
        //    {

        //        string PPID = dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
        //        string Block = dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
        //        string Hour = dataGridView1.Rows[i].Cells[2].Value.ToString().Trim();
        //        double min = 0.0;
        //        if (dataGridView1.Rows[0].Cells[3].Value != null)
        //        {
        //            min = MyDoubleParse(dataGridView1.Rows[0].Cells[3].Value.ToString().Trim());

        //        }
                
        //        double cap = 0.0;
        //        DataTable basetabledata = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
        //        if (basetabledata.Rows.Count > 0)
        //        {
        //            cap = MyDoubleParse(basetabledata.Rows[0][0].ToString()) ;

        //        }


        //        double max = 0.0;
        //        if (dataGridView1.Rows[0].Cells[4].Value != null)
        //        {
        //            max = MyDoubleParse(dataGridView1.Rows[0].Cells[4].Value.ToString().Trim());
        //        }

        //        if (max > min &&  max<=cap)
        //        {
        //            double interval = 0.0;
        //            if (dataGridView1.Rows[0].Cells[5].Value != null)
        //            {
        //                interval = MyDoubleParse(dataGridView1.Rows[0].Cells[5].Value.ToString().Trim());
        //            }

        //            DataTable dt1 = utilities.GetTable("delete from ManualData where TargetMarketDate='" + BiddingDate + "'and PPID='" + PPID +
        //            "'and Block='" + Block + "'and Hour='" + Hour + "'");



        //            dt1 = utilities.GetTable("insert into ManualData (TargetMarketDate,PPID,Block,Hour,PmaxPrice,PminPrice,Interval) values ('"
        //           + BiddingDate + "','" + PPID + "','" + Block + "','" + Hour + "','" + max + "','" + min + "','" + interval + "')");
        //        }

        //        else
        //        {
        //            MessageBox.Show("Please Fill All fild Correctly !.....\r\n...Price Max Must Be Bigger Than PriceMin");
        //            break;
                   
        //        }
        //    }
        //}


        private void btnsavesimilar_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            progressBar1.Visible = false;
            fillsimillar();

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                double max = 0.0;
                string PPID = dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
                string Block = dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
                string Hour = dataGridView1.Rows[i].Cells[2].Value.ToString().Trim();
            
                if (dataGridView1.Rows[0].Cells[3].Value != null)
                {
                   max = MyDoubleParse(dataGridView1.Rows[0].Cells[3].Value.ToString().Trim());

                }

                double cap = 0.0;
                DataTable basetabledata = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
                if (basetabledata.Rows.Count > 0)
                {
                    cap = MyDoubleParse(basetabledata.Rows[0][0].ToString());

                }               
               

                if (max <= cap)
                {
                    double interval = 0.0;
                    if (dataGridView1.Rows[0].Cells[4].Value != null)
                    {
                        interval = MyDoubleParse(dataGridView1.Rows[0].Cells[4].Value.ToString().Trim());
                    }

                    DataTable dt1 = Utilities.GetTable("delete from ManualData where TargetMarketDate='" + BiddingDate + "'and PPID='" + PPID +
                    "'and Block='" + Block + "'and Hour='" + Hour + "'");



                    dt1 = Utilities.GetTable("insert into ManualData (TargetMarketDate,PPID,Block,Hour,PmaxPrice,Interval) values ('"
                   + BiddingDate + "','" + PPID + "','" + Block + "','" + Hour + "','" + max + "','" + interval + "')");
                }

                else
                {
                    MessageBox.Show("Please Fill All fild Correctly !.....\r\n");
                    break;

                }
            }
        }
        private void fillsimillar()
        {
          
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {

                string PPID = dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
                string Block = dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
                string Hour = dataGridView1.Rows[i].Cells[2].Value.ToString().Trim();

                //double min = 0.0;
                if (dataGridView1.Rows[0].Cells[3].Value != null)
                {
                    dataGridView1.Rows[i].Cells[3].Value = dataGridView1.Rows[0].Cells[3].Value.ToString().Trim();

                }
              
                //double max = 0.0;
                if (dataGridView1.Rows[0].Cells[4].Value != null)
                {
                    dataGridView1.Rows[i].Cells[4].Value= dataGridView1.Rows[0].Cells[4].Value.ToString().Trim();
                }

                ////double interval = 0.0;old
                //if (dataGridView1.Rows[0].Cells[5].Value != null)
                //{
                //    dataGridView1.Rows[i].Cells[5].Value = dataGridView1.Rows[0].Cells[5].Value.ToString().Trim();
                //}

            
            }
        }
        private void fillsimillarBlock(int index)
        {
            

                string PPID = dataGridView1.Rows[index].Cells[0].Value.ToString().Trim();
                string Block = dataGridView1.Rows[index].Cells[1].Value.ToString().Trim();
                string Hour = dataGridView1.Rows[index].Cells[2].Value.ToString().Trim();
                if (Hour == "1")
                {
                    for (int i = 0; i < 24; i++)
                    {
                        //double min = 0.0;
                        if (dataGridView1.Rows[index].Cells[3].Value != null)
                        {
                            dataGridView1.Rows[i+index].Cells[3].Value = dataGridView1.Rows[index].Cells[3].Value.ToString().Trim();

                        }

                        //double max = 0.0;
                        if (dataGridView1.Rows[index].Cells[4].Value != null)
                        {
                            dataGridView1.Rows[i + index].Cells[4].Value = dataGridView1.Rows[index].Cells[4].Value.ToString().Trim();
                        }

                        ////double interval = 0.0;
                        //if (dataGridView1.Rows[index].Cells[5].Value != null)
                        //{
                        //    dataGridView1.Rows[i + index].Cells[5].Value = dataGridView1.Rows[index].Cells[5].Value.ToString().Trim();
                        //}
                    }

            }
        }
     
        private void filllasteasyold()
        {
            easyload = true;
        
            for (int i = 0; i < 24; i++)
            {
                string PPID = dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
               // string Block = dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
              string Block = comboBox1.Text.Trim();
                string Hour = dataGridView1.Rows[i].Cells[2].Value.ToString().Trim();
                double cap1 = 0;
                string[] bblock = Getblock002pack(int.Parse(PPID));

                //for (int y = 0; y < bblock.Length; y++)
                //{
                //    if (bblock[y].Contains("-C")) bblock[y] = bblock[y].Replace("-C", "").Trim();
                //    DataTable dtout = utilities.GetTable(" select * from dbo.Dispathable where ppid='" + PPID + "'and Block='" + bblock[y].Trim() + "' and StartDate='" + BiddingDate + "'");
                //    if (dtout.Rows.Count > 0)
                //    {
                //        if (dtout.Rows[0]["DeclaredCapacity"].ToString().Trim() != "0")
                //        {
                //            Block = bblock[y].Trim();
                //            if (Block.Contains("-C")) Block = Block.Replace("-C", "").Trim();
                //            break;
                //        }
                //    }
                //}

                string ptypenum = "0";
                //if (Block.Contains("-C")) ptypenum = "1";
                ////if (PPID == "232") ptypenum = "0";
                if (Findcctwotype(PPID)) ptypenum = "1";
                if (Findcconetype(PPID)) ptypenum = "0";
                

                //if (Block.Contains("-C")) Block = Block.Replace("-C", "").Trim();
              


                DataTable bas = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
                if (bas.Rows.Count > 0)
                {
                    cap1 = MyDoubleParse(bas.Rows[0][0].ToString()) ;
                 
                }



                DataTable dt1 = Utilities.GetTable("select * from dbo.DetailFRM002 where TargetMarketDate='" + BiddingDate + "' and PPID='" + PPID + "' and  Block='" + blockname002 + "' and Hour='" + Hour + "'and PPType='"+ptypenum +"' and Estimated=1");

                DataTable dt2 = Utilities.GetTable("select * from dbo.DetailFRM002 where TargetMarketDate='" + CurrentDate + "' and PPID='" + PPID + "' and  Block='" + blockname002 + "' and Hour='" + Hour + "'and PPType='" + ptypenum + "'and Estimated=0");

                //////////////////////////////////nearest current real bid///////////////////////////////////////////////////////

                DataTable odat = Utilities.GetTable("Select max(TargetMarketDate) from dbo.DetailFRM002 where TargetMarketDate<='" + CurrentDate + "'AND PPID='" + PPID + "' and  Block='" + blockname002 + "' and Hour='" + Hour + "'and PPType='" + ptypenum + "' and Estimated=0");
                string nearest = odat.Rows[0][0].ToString().Trim();

                DataTable dt3 = Utilities.GetTable("select * from dbo.DetailFRM002 where TargetMarketDate='" + nearest + "'and PPID='" + PPID + "'and  Block='" + blockname002 + "' and Hour='" + Hour + "'and PPType='" + ptypenum + "'  and Estimated=0");

                ///////////////////////////////////////////////////////////////////////////////////////////////
                if (dt1.Rows.Count > 0)
                {
                    double cap = 0.0;
                    DataTable basetabledata = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date='"+maxbasedate+"'order by BaseID desc");
                    if (basetabledata.Rows.Count > 0)
                    {
                        cap = MyDoubleParse(basetabledata.Rows[0][0].ToString()) ;

                    }


                    for (int j = 1; j < 10; j++)
                    {
                        double powerx = MyDoubleParse(dt1.Rows[0]["Power" + j].ToString());
                        double pricex = MyDoubleParse(dt1.Rows[0]["Price" + j].ToString());
                        double powerxx = MyDoubleParse(dt1.Rows[0]["Power" + (j + 1)].ToString());
                        double pricexx = MyDoubleParse(dt1.Rows[0]["Price" + (j + 1)].ToString());
                        bool enter = true;
                        if (powerxx == 0 && pricexx == 0 && j == 1) enter = false;

                        if (powerxx - powerx > 1 && enter)
                        {
                            dataGridView1.Rows[i].Cells[3].Value = MyDoubleParse(dt1.Rows[0]["Price1"].ToString());
                            dataGridView1.Rows[i].Cells[4].Value = pricexx.ToString();

                        }
                        else if (powerxx - powerx < 1 && enter && j == 9)
                        {
                            dataGridView1.Rows[i].Cells[3].Value = MyDoubleParse(dt1.Rows[0]["Price1"].ToString());
                            dataGridView1.Rows[i].Cells[4].Value = MyDoubleParse(dt1.Rows[0]["Price2"].ToString());

                        }
                        else if (enter == false)
                        {
                            dataGridView1.Rows[i].Cells[3].Value = MyDoubleParse(dt1.Rows[0]["Price1"].ToString()) - 100;
                            dataGridView1.Rows[i].Cells[4].Value = MyDoubleParse(dt1.Rows[0]["Price1"].ToString());
                            break;
                        }


                    }
                    if (dataGridView1.Rows[i].Cells[3].Value == null)
                    {

                        dataGridView1.Rows[i].Cells[3].Value = cap -(Dist);
                        dataGridView1.Rows[i].Cells[4].Value = cap;
                    }



                    dataGridView1.Rows[i].Cells[5].Value = (Dist).ToString();

                }
                else if (dt2.Rows.Count > 0)
                {
                    double cap = 0.0;
                    DataTable basetabledata = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date='"+maxbasedate+"'order by BaseID desc");
                    if (basetabledata.Rows.Count > 0)
                    {
                        cap = MyDoubleParse(basetabledata.Rows[0][0].ToString()) ;

                    }



                    for (int j = 1; j < 10; j++)
                    {
                        double powerx = MyDoubleParse(dt2.Rows[0]["Power" + j].ToString());
                        double pricex = MyDoubleParse(dt2.Rows[0]["Price" + j].ToString());
                        double powerxx = MyDoubleParse(dt2.Rows[0]["Power" + (j + 1)].ToString());
                        double pricexx = MyDoubleParse(dt2.Rows[0]["Price" + (j + 1)].ToString());
                        bool enter = true;
                        if (powerxx == 0 && pricexx == 0 && j == 1) enter = false;

                        if (powerxx - powerx > 1 && enter)
                        {
                            dataGridView1.Rows[i].Cells[3].Value = MyDoubleParse(dt2.Rows[0]["Price1"].ToString());
                            dataGridView1.Rows[i].Cells[4].Value = pricexx.ToString();
                          
                        }
                        else if (powerxx - powerx < 1 && enter && j == 9)
                        {
                            dataGridView1.Rows[i].Cells[3].Value = MyDoubleParse(dt2.Rows[0]["Price1"].ToString());
                            dataGridView1.Rows[i].Cells[4].Value = MyDoubleParse(dt2.Rows[0]["Price2"].ToString());

                        }
                        else if (enter == false)
                        {
                            dataGridView1.Rows[i].Cells[3].Value = MyDoubleParse(dt2.Rows[0]["Price1"].ToString())-100;
                            dataGridView1.Rows[i].Cells[4].Value = MyDoubleParse(dt2.Rows[0]["Price1"].ToString());
                            break;
                        }


                    }
                    if (dataGridView1.Rows[i].Cells[3].Value == null)
                    {

                        dataGridView1.Rows[i].Cells[3].Value = cap - (Dist);
                        dataGridView1.Rows[i].Cells[4].Value = cap;
                    }



                    dataGridView1.Rows[i].Cells[5].Value = (Dist).ToString();


                }
                else if (dt3.Rows.Count > 0)
                {
                    double cap = 0.0;
                    DataTable basetabledata = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
                    if (basetabledata.Rows.Count > 0)
                    {
                        cap = MyDoubleParse(basetabledata.Rows[0][0].ToString()) ;

                    }



                    for (int j = 1; j < 10; j++)
                    {
                        double powerx = MyDoubleParse(dt3.Rows[0]["Power" + j].ToString());
                        double pricex = MyDoubleParse(dt3.Rows[0]["Price" + j].ToString());
                        double powerxx = MyDoubleParse(dt3.Rows[0]["Power" + (j + 1)].ToString());
                        double pricexx = MyDoubleParse(dt3.Rows[0]["Price" + (j + 1)].ToString());
                        bool enter = true;
                        if (powerxx == 0 && pricexx == 0 && j == 1) enter = false;

                        if (powerxx - powerx > 1 && enter)
                        {
                            dataGridView1.Rows[i].Cells[3].Value = MyDoubleParse(dt3.Rows[0]["Price1"].ToString());
                            dataGridView1.Rows[i].Cells[4].Value = pricexx.ToString();

                        }
                        else if (powerxx - powerx < 1 && enter && j == 9)
                        {
                            dataGridView1.Rows[i].Cells[3].Value = MyDoubleParse(dt3.Rows[0]["Price1"].ToString());
                            dataGridView1.Rows[i].Cells[4].Value = MyDoubleParse(dt3.Rows[0]["Price2"].ToString());

                        }
                        else if (enter == false)
                        {
                            dataGridView1.Rows[i].Cells[3].Value = MyDoubleParse(dt3.Rows[0]["Price1"].ToString()) - 100;
                            dataGridView1.Rows[i].Cells[4].Value = MyDoubleParse(dt3.Rows[0]["Price1"].ToString());
                            break;
                        }


                    }
                    if (dataGridView1.Rows[i].Cells[3].Value == null)
                    {

                        dataGridView1.Rows[i].Cells[3].Value = cap - (Dist);
                        dataGridView1.Rows[i].Cells[4].Value = cap;
                    }



                    dataGridView1.Rows[i].Cells[5].Value = (Dist).ToString();

                }
                   //////////////////////load numeric////////////////////////////////////
              

                    if (i == 0)
                    {
                        try
                        {
                            numeric1.Maximum = decimal.Parse(cap1.ToString());
                            numeric2.Maximum = decimal.Parse(cap1.ToString());
                            numeric3.Maximum = decimal.Parse("10000");

                            numeric1.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric2.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric3.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {

                        }
                  
                    }
                   
                    if (i == 1)
                    {
                        try
                        {

                            numeric4.Maximum = decimal.Parse(cap1.ToString());
                            numeric5.Maximum = decimal.Parse(cap1.ToString());
                            numeric6.Maximum = decimal.Parse("10000");

                            numeric4.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric5.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric6.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {

                        }
                       
                    }
                    if (i == 2)
                    {
                        try
                        {
                            numeric7.Maximum = decimal.Parse(cap1.ToString());
                            numeric8.Maximum = decimal.Parse(cap1.ToString());
                            numeric9.Maximum = decimal.Parse("10000");

                            numeric7.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric8.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric9.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {

                        }
                     
                    }
                    if (i == 3)
                    {
                        try
                        {
                            numeric10.Maximum = decimal.Parse(cap1.ToString());
                            numeric11.Maximum = decimal.Parse(cap1.ToString());
                            numeric12.Maximum = decimal.Parse("10000");

                            numeric10.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric11.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric12.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {

                        }
                       
                    }
                    if (i == 4)
                    {
                        try
                        {
                            numeric13.Maximum = decimal.Parse(cap1.ToString());
                            numeric14.Maximum = decimal.Parse(cap1.ToString());
                            numeric15.Maximum = decimal.Parse("10000");

                            numeric13.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric14.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric15.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {
                        }
                      

                    }
                    if (i == 5)
                    {
                        try
                        {
                            numeric16.Maximum = decimal.Parse(cap1.ToString());
                            numeric17.Maximum = decimal.Parse(cap1.ToString());
                            numeric18.Maximum = decimal.Parse("10000");

                            numeric16.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric17.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric18.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {
                        }
                       
                    }
                    if (i == 6)
                    {
                        try
                        {
                            numeric19.Maximum = decimal.Parse(cap1.ToString());
                            numeric20.Maximum = decimal.Parse(cap1.ToString());
                            numeric21.Maximum = decimal.Parse("10000");

                            numeric19.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric20.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric21.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());

                        }
                        catch
                        {

                        }
                    }
                    if (i == 7)
                    {
                        try
                        {
                            numeric22.Maximum = decimal.Parse(cap1.ToString());
                            numeric23.Maximum = decimal.Parse(cap1.ToString());
                            numeric24.Maximum = decimal.Parse("10000");

                            numeric22.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric23.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric24.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {
                        }
                     
                    }
                    if (i == 8)
                    {
                        try
                        {
                            numeric25.Maximum = decimal.Parse(cap1.ToString());
                            numeric26.Maximum = decimal.Parse(cap1.ToString());
                            numeric27.Maximum = decimal.Parse("10000");

                            numeric25.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric26.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric27.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {

                        }
                       
                    }
                    if (i == 9)
                    {
                        try
                        {
                            numeric28.Maximum = decimal.Parse(cap1.ToString());
                            numeric29.Maximum = decimal.Parse(cap1.ToString());
                            numeric30.Maximum = decimal.Parse("10000");

                            numeric28.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric29.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric30.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {

                        }
                      
                    }
                    if (i ==10)
                    {
                        try
                        {
                            numeric31.Maximum = decimal.Parse(cap1.ToString());
                            numeric32.Maximum = decimal.Parse(cap1.ToString());
                            numeric33.Maximum = decimal.Parse("10000");

                            numeric31.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric32.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric33.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {
                        }
                       
                    }
                    if (i == 11)
                    {
                        try
                        {
                            numeric34.Maximum = decimal.Parse(cap1.ToString());
                            numeric35.Maximum = decimal.Parse(cap1.ToString());
                            numeric36.Maximum = decimal.Parse("10000");

                            numeric34.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric35.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric36.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {
                        }

                       
                    }
                    if (i == 12)
                    {
                        try
                        {
                            numeric37.Maximum = decimal.Parse(cap1.ToString());
                            numeric38.Maximum = decimal.Parse(cap1.ToString());
                            numeric39.Maximum = decimal.Parse("10000");


                            numeric37.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric38.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric39.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {
                        }
                      
                    }
                    if (i ==13)
                    {
                        try
                        {
                            numeric40.Maximum = decimal.Parse(cap1.ToString());
                            numeric41.Maximum = decimal.Parse(cap1.ToString());
                            numeric42.Maximum = decimal.Parse("10000");

                            numeric40.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric41.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric42.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());

                        }
                        catch
                        {

                        }
                    }
                    if (i == 14)
                    {
                        try
                        {
                            numeric43.Maximum = decimal.Parse(cap1.ToString());
                            numeric44.Maximum = decimal.Parse(cap1.ToString());
                            numeric45.Maximum = decimal.Parse("10000");

                            numeric43.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric44.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric45.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {

                        }
                      
                    }
                    if (i == 15)
                    {
                       
                        numeric46.Maximum = decimal.Parse(cap1.ToString());
                        numeric47.Maximum = decimal.Parse(cap1.ToString());
                        numeric48.Maximum = decimal.Parse("10000");

                        try
                        {
                            numeric46.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric47.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric48.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {

                        }
                       
                    }
                    if (i == 16)
                    {
                        try
                        {

                            numeric49.Maximum = decimal.Parse(cap1.ToString());
                            numeric50.Maximum = decimal.Parse(cap1.ToString());
                            numeric51.Maximum = decimal.Parse("10000");

                            numeric49.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric50.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric51.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {

                        }
                       
                    }
                    if (i == 17)
                    {
                        try
                        {
                            numeric52.Maximum = decimal.Parse(cap1.ToString());
                            numeric53.Maximum = decimal.Parse(cap1.ToString());
                            numeric54.Maximum = decimal.Parse("10000");

                            numeric52.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric53.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric54.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {
                        }
                     
                    }
                    if (i == 18)
                    {
                        try
                        {
                            numeric55.Maximum = decimal.Parse(cap1.ToString());
                            numeric56.Maximum = decimal.Parse(cap1.ToString());
                            numeric57.Maximum = decimal.Parse("10000");

                            numeric55.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric56.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric57.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {
                        }
                       

                    }
                    if (i ==19)
                    {
                        try
                        {
                            numeric58.Maximum = decimal.Parse(cap1.ToString());
                            numeric59.Maximum = decimal.Parse(cap1.ToString());
                            numeric60.Maximum = decimal.Parse("10000");

                            numeric58.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric59.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric60.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {

                        }
                      
                    }
                    if (i == 20)
                    {
                        try
                        {
                            numeric61.Maximum = decimal.Parse(cap1.ToString());
                            numeric62.Maximum = decimal.Parse(cap1.ToString());
                            numeric63.Maximum = decimal.Parse("10000");

                            numeric61.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric62.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric63.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {
                        }
                      
                    }
                    if (i == 21)
                    {
                        try
                        {
                            numeric64.Maximum = decimal.Parse(cap1.ToString());
                            numeric65.Maximum = decimal.Parse(cap1.ToString());
                            numeric66.Maximum = decimal.Parse("10000");

                            numeric64.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric65.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric66.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {
                        }
                       
                    }
                    if (i == 22)
                    {
                        try
                        {
                            numeric67.Maximum = decimal.Parse(cap1.ToString());
                            numeric68.Maximum = decimal.Parse(cap1.ToString());
                            numeric69.Maximum = decimal.Parse("10000");

                            numeric67.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric68.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric69.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {
                        }
                       
                    }
                    if (i == 23)
                    {
                        try
                        {
                            numeric70.Maximum = decimal.Parse(cap1.ToString());
                            numeric71.Maximum = decimal.Parse(cap1.ToString());
                            numeric72.Maximum = decimal.Parse("10000");

                            numeric70.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                            numeric71.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                            numeric72.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                        }
                        catch
                        {

                        }
                    }

                                 
            }


            easyload = false;
        
        }
        private void filllasteasy()
        {
            easyload = true;

            for (int i = 0; i < 24; i++)
            {
                string PPID = dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
                // string Block = dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
                string Block = comboBox1.Text.Trim();
                string Hour = dataGridView1.Rows[i].Cells[2].Value.ToString().Trim();
                double cap1 = 0;
                string[] bblock = Getblock002pack(int.Parse(PPID));

                //for (int y = 0; y < bblock.Length; y++)
                //{
                //    if (bblock[y].Contains("-C")) bblock[y] = bblock[y].Replace("-C", "").Trim();
                //    DataTable dtout = utilities.GetTable(" select * from dbo.Dispathable where ppid='" + PPID + "'and Block='" + bblock[y].Trim() + "' and StartDate='" + BiddingDate + "'");
                //    if (dtout.Rows.Count > 0)
                //    {
                //        if (dtout.Rows[0]["DeclaredCapacity"].ToString().Trim() != "0")
                //        {
                //            Block = bblock[y].Trim();
                //            if (Block.Contains("-C")) Block = Block.Replace("-C", "").Trim();
                //            break;
                //        }
                //    }
                //}

                string ptypenum = "0";
                //if (Block.Contains("-C")) ptypenum = "1";
                ////if (PPID == "232") ptypenum = "0";
                if (Findcctwotype(PPID)) ptypenum = "1";
                if (Findcconetype(PPID)) ptypenum = "0";


                //if (Block.Contains("-C")) Block = Block.Replace("-C", "").Trim();



                DataTable bas = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
                if (bas.Rows.Count > 0)
                {
                    cap1 = MyDoubleParse(bas.Rows[0][0].ToString());

                }



                DataTable dt1 = Utilities.GetTable("select * from dbo.DetailFRM002 where TargetMarketDate='" + BiddingDate + "' and PPID='" + PPID + "' and  Block='" + blockname002 + "' and Hour='" + Hour + "'and PPType='" + ptypenum + "' and Estimated=1");

                DataTable dt2 = Utilities.GetTable("select * from dbo.DetailFRM002 where TargetMarketDate='" + CurrentDate + "' and PPID='" + PPID + "' and  Block='" + blockname002 + "' and Hour='" + Hour + "'and PPType='" + ptypenum + "'and Estimated=0");

                //////////////////////////////////nearest current real bid///////////////////////////////////////////////////////

                DataTable odat = Utilities.GetTable("Select max(TargetMarketDate) from dbo.DetailFRM002 where TargetMarketDate<='" + CurrentDate + "'AND PPID='" + PPID + "' and  Block='" + blockname002 + "' and Hour='" + Hour + "'and PPType='" + ptypenum + "' and Estimated=0");
                string nearest = odat.Rows[0][0].ToString().Trim();

                DataTable dt3 = Utilities.GetTable("select * from dbo.DetailFRM002 where TargetMarketDate='" + nearest + "'and PPID='" + PPID + "'and  Block='" + blockname002 + "' and Hour='" + Hour + "'and PPType='" + ptypenum + "'  and Estimated=0");

                ///////////////////////////////////////////////////////////////////////////////////////////////

                double powerx = 0; ;
                double pricex =0;
                double powerxx = 0;
                double pricexx = 0;
                if (dt1.Rows.Count > 0)
                {

                     powerx = MyDoubleParse(dt1.Rows[0]["Power1"].ToString());
                     pricex = MyDoubleParse(dt1.Rows[0]["Price1"].ToString());
                     powerxx = MyDoubleParse(dt1.Rows[0]["Power2"].ToString());
                    pricexx = MyDoubleParse(dt1.Rows[0]["Price2"].ToString());
                }
                if (dt2.Rows.Count > 0)
                {

                    powerx = MyDoubleParse(dt2.Rows[0]["Power1"].ToString());
                    pricex = MyDoubleParse(dt2.Rows[0]["Price1"].ToString());
                    powerxx = MyDoubleParse(dt2.Rows[0]["Power2"].ToString());
                    pricexx = MyDoubleParse(dt2.Rows[0]["Price2"].ToString());
                }

                else if (dt3.Rows.Count > 0)
                {

                    powerx = MyDoubleParse(dt3.Rows[0]["Power1"].ToString());
                   pricex = MyDoubleParse(dt3.Rows[0]["Price1"].ToString());
                     powerxx = MyDoubleParse(dt3.Rows[0]["Power2"].ToString());
                     pricexx = MyDoubleParse(dt3.Rows[0]["Price2"].ToString());

                }
                    double cap = 0.0;
                    DataTable basetabledata = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
                    if (basetabledata.Rows.Count > 0)
                    {
                        cap = MyDoubleParse(basetabledata.Rows[0][0].ToString());

                    }


                    


                        if (pricexx > 0)
                        {
                            dataGridView1.Rows[i].Cells[3].Value = pricexx.ToString();

                        }
                        else if(pricex>0)
                        {
                            dataGridView1.Rows[i].Cells[3].Value = pricex.ToString();
                        }
                 
                       


                  
                    if (dataGridView1.Rows[i].Cells[3].Value == null)
                    {

                        dataGridView1.Rows[i].Cells[3].Value = cap - (Dist);
                     
                    }



                    dataGridView1.Rows[i].Cells[4].Value = (Dist).ToString();

                
                //////////////////////load numeric////////////////////////////////////


                if (i == 0)
                {
                    try
                    {
                        numeric1.Maximum = decimal.Parse(cap1.ToString());
                        numeric2.Maximum = decimal.Parse(cap1.ToString());
                        numeric3.Maximum = decimal.Parse("10000");

                        numeric1.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric2.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric3.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {

                    }

                }

                if (i == 1)
                {
                    try
                    {

                        numeric4.Maximum = decimal.Parse(cap1.ToString());
                        numeric5.Maximum = decimal.Parse(cap1.ToString());
                        numeric6.Maximum = decimal.Parse("10000");

                        numeric4.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric5.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric6.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {

                    }

                }
                if (i == 2)
                {
                    try
                    {
                        numeric7.Maximum = decimal.Parse(cap1.ToString());
                        numeric8.Maximum = decimal.Parse(cap1.ToString());
                        numeric9.Maximum = decimal.Parse("10000");

                        numeric7.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric8.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric9.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {

                    }

                }
                if (i == 3)
                {
                    try
                    {
                        numeric10.Maximum = decimal.Parse(cap1.ToString());
                        numeric11.Maximum = decimal.Parse(cap1.ToString());
                        numeric12.Maximum = decimal.Parse("10000");

                        numeric10.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric11.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric12.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {

                    }

                }
                if (i == 4)
                {
                    try
                    {
                        numeric13.Maximum = decimal.Parse(cap1.ToString());
                        numeric14.Maximum = decimal.Parse(cap1.ToString());
                        numeric15.Maximum = decimal.Parse("10000");

                        numeric13.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric14.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric15.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {
                    }


                }
                if (i == 5)
                {
                    try
                    {
                        numeric16.Maximum = decimal.Parse(cap1.ToString());
                        numeric17.Maximum = decimal.Parse(cap1.ToString());
                        numeric18.Maximum = decimal.Parse("10000");

                        numeric16.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric17.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric18.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {
                    }

                }
                if (i == 6)
                {
                    try
                    {
                        numeric19.Maximum = decimal.Parse(cap1.ToString());
                        numeric20.Maximum = decimal.Parse(cap1.ToString());
                        numeric21.Maximum = decimal.Parse("10000");

                        numeric19.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric20.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric21.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());

                    }
                    catch
                    {

                    }
                }
                if (i == 7)
                {
                    try
                    {
                        numeric22.Maximum = decimal.Parse(cap1.ToString());
                        numeric23.Maximum = decimal.Parse(cap1.ToString());
                        numeric24.Maximum = decimal.Parse("10000");

                        numeric22.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric23.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric24.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {
                    }

                }
                if (i == 8)
                {
                    try
                    {
                        numeric25.Maximum = decimal.Parse(cap1.ToString());
                        numeric26.Maximum = decimal.Parse(cap1.ToString());
                        numeric27.Maximum = decimal.Parse("10000");

                        numeric25.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric26.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric27.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {

                    }

                }
                if (i == 9)
                {
                    try
                    {
                        numeric28.Maximum = decimal.Parse(cap1.ToString());
                        numeric29.Maximum = decimal.Parse(cap1.ToString());
                        numeric30.Maximum = decimal.Parse("10000");

                        numeric28.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric29.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric30.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {

                    }

                }
                if (i == 10)
                {
                    try
                    {
                        numeric31.Maximum = decimal.Parse(cap1.ToString());
                        numeric32.Maximum = decimal.Parse(cap1.ToString());
                        numeric33.Maximum = decimal.Parse("10000");

                        numeric31.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric32.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric33.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {
                    }

                }
                if (i == 11)
                {
                    try
                    {
                        numeric34.Maximum = decimal.Parse(cap1.ToString());
                        numeric35.Maximum = decimal.Parse(cap1.ToString());
                        numeric36.Maximum = decimal.Parse("10000");

                        numeric34.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric35.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric36.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {
                    }


                }
                if (i == 12)
                {
                    try
                    {
                        numeric37.Maximum = decimal.Parse(cap1.ToString());
                        numeric38.Maximum = decimal.Parse(cap1.ToString());
                        numeric39.Maximum = decimal.Parse("10000");


                        numeric37.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric38.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric39.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {
                    }

                }
                if (i == 13)
                {
                    try
                    {
                        numeric40.Maximum = decimal.Parse(cap1.ToString());
                        numeric41.Maximum = decimal.Parse(cap1.ToString());
                        numeric42.Maximum = decimal.Parse("10000");

                        numeric40.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric41.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric42.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());

                    }
                    catch
                    {

                    }
                }
                if (i == 14)
                {
                    try
                    {
                        numeric43.Maximum = decimal.Parse(cap1.ToString());
                        numeric44.Maximum = decimal.Parse(cap1.ToString());
                        numeric45.Maximum = decimal.Parse("10000");

                        numeric43.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric44.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric45.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {

                    }

                }
                if (i == 15)
                {

                    numeric46.Maximum = decimal.Parse(cap1.ToString());
                    numeric47.Maximum = decimal.Parse(cap1.ToString());
                    numeric48.Maximum = decimal.Parse("10000");

                    try
                    {
                        numeric46.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric47.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric48.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {

                    }

                }
                if (i == 16)
                {
                    try
                    {

                        numeric49.Maximum = decimal.Parse(cap1.ToString());
                        numeric50.Maximum = decimal.Parse(cap1.ToString());
                        numeric51.Maximum = decimal.Parse("10000");

                        numeric49.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric50.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric51.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {

                    }

                }
                if (i == 17)
                {
                    try
                    {
                        numeric52.Maximum = decimal.Parse(cap1.ToString());
                        numeric53.Maximum = decimal.Parse(cap1.ToString());
                        numeric54.Maximum = decimal.Parse("10000");

                        numeric52.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric53.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric54.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {
                    }

                }
                if (i == 18)
                {
                    try
                    {
                        numeric55.Maximum = decimal.Parse(cap1.ToString());
                        numeric56.Maximum = decimal.Parse(cap1.ToString());
                        numeric57.Maximum = decimal.Parse("10000");

                        numeric55.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric56.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric57.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {
                    }


                }
                if (i == 19)
                {
                    try
                    {
                        numeric58.Maximum = decimal.Parse(cap1.ToString());
                        numeric59.Maximum = decimal.Parse(cap1.ToString());
                        numeric60.Maximum = decimal.Parse("10000");

                        numeric58.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric59.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric60.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {

                    }

                }
                if (i == 20)
                {
                    try
                    {
                        numeric61.Maximum = decimal.Parse(cap1.ToString());
                        numeric62.Maximum = decimal.Parse(cap1.ToString());
                        numeric63.Maximum = decimal.Parse("10000");

                        numeric61.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric62.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric63.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {
                    }

                }
                if (i == 21)
                {
                    try
                    {
                        numeric64.Maximum = decimal.Parse(cap1.ToString());
                        numeric65.Maximum = decimal.Parse(cap1.ToString());
                        numeric66.Maximum = decimal.Parse("10000");

                        numeric64.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric65.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric66.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {
                    }

                }
                if (i == 22)
                {
                    try
                    {
                        numeric67.Maximum = decimal.Parse(cap1.ToString());
                        numeric68.Maximum = decimal.Parse(cap1.ToString());
                        numeric69.Maximum = decimal.Parse("10000");

                        numeric67.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric68.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric69.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {
                    }

                }
                if (i == 23)
                {
                    try
                    {
                        numeric70.Maximum = decimal.Parse(cap1.ToString());
                        numeric71.Maximum = decimal.Parse(cap1.ToString());
                        numeric72.Maximum = decimal.Parse("10000");

                        numeric70.Value = decimal.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                        numeric71.Value = decimal.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                        numeric72.Value = decimal.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                    }
                    catch
                    {

                    }
                }


            }


            easyload = false;

        }
        private void fillavgprice()
        {
            
            isavg = true;
            CAPPRICE = MyDoubleParse(txtzarib.Text);

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                string PPID = dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
                string Block = dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
                string Hour = dataGridView1.Rows[i].Cells[2].Value.ToString().Trim();
                double cap=0;
                  DataTable basetabledata = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
                    if (basetabledata.Rows.Count > 0)
                    {
                        cap = MyDoubleParse(basetabledata.Rows[0][0].ToString());

                    }
                  DataTable dt1 = Utilities.GetTable("select AcceptedAverage from dbo.AveragePrice where Date=(select distinct max(Date) from dbo.AveragePrice where Date<='" + BiddingDate + "') and hour ='"+Hour+"'");

                  if (dt1.Rows.Count > 0)
                  {
                      if ((MyDoubleParse(dt1.Rows[0][0].ToString().Trim()) * MyDoubleParse(txtzarib.Text)) < cap && ((MyDoubleParse(dt1.Rows[0][0].ToString().Trim()) * MyDoubleParse(txtzarib.Text)) + 1000)<=cap)
                      {
                          dataGridView1.Rows[i].Cells[3].Value = (MyDoubleParse(dt1.Rows[0][0].ToString().Trim()) * MyDoubleParse(txtzarib.Text)).ToString();

                      }
                      else
                      {
                          dataGridView1.Rows[i].Cells[3].Value = (cap - (Dist)).ToString();
                      }

                      dataGridView1.Rows[i].Cells[4].Value = MyDoubleParse(dataGridView1.Rows[i].Cells[3].Value.ToString()) + (Dist);

                      dataGridView1.Rows[i].Cells[5].Value = (Dist).ToString();
                  }
            
                
            }
        }
        private void btnsavelastbid_Click(object sender, EventArgs e)
        {
          
            /////////////////////////////////////////////////////////////////
        
            dataGridView1.Visible = false;
            paneleasy.Visible = true;
            panehidebutton.Visible = true;
            filllasteasy();        

            progressBar1.Visible = false;
        
            //filllast();
            //for (int i = 0; i < dataGridView1.Rows.Count; i++)
            //{

            //    string PPID = dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
            //    string Block = dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
            //    string Hour = dataGridView1.Rows[i].Cells[2].Value.ToString().Trim();
            //    double min = 0.0;
            //    if (dataGridView1.Rows[i].Cells[3].Value != null)
            //    {
            //        min = MyDoubleParse(dataGridView1.Rows[i].Cells[3].Value.ToString().Trim());

            //    }

            //    double max = 0.0;
            //    if (dataGridView1.Rows[i].Cells[4].Value != null)
            //    {
            //        max = MyDoubleParse(dataGridView1.Rows[i].Cells[4].Value.ToString().Trim());
            //    }

            //    double cap = 0.0;
            //    DataTable basetabledata = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date in (select  max(Date) from dbo.BaseData)order by BaseID desc");
            //    if (basetabledata.Rows.Count > 0)
            //    {
            //        cap = MyDoubleParse(basetabledata.Rows[0][0].ToString());

            //    }


            //    if (max > min && max<=cap)
            //    {
            //        double interval = 0.0;
            //        if (dataGridView1.Rows[i].Cells[5].Value != null)
            //        {
            //            interval = MyDoubleParse(dataGridView1.Rows[i].Cells[5].Value.ToString().Trim());
            //        }



            //        DataTable dt1 = utilities.GetTable("delete from ManualData where TargetMarketDate='" + BiddingDate + "'and PPID='" + PPID +
            //        "'and Block='" + Block + "'and Hour='" + Hour + "'");



            //        dt1 = utilities.GetTable("insert into ManualData (TargetMarketDate,PPID,Block,Hour,PmaxPrice,PminPrice,Interval) values ('"
            //       + BiddingDate + "','" + PPID + "','" + Block + "','" + Hour + "','" + max + "','" + min + "','" + interval + "')");
            //    }
            //    else
            //    {
                   
            //        MessageBox.Show("Please Fill All fild Correctly !.....\r\n...Price Max Must Be Bigger Than PriceMin");
            //        break;
            //    }
            //}
           
           
        }

        private void btncap_Click(object sender, EventArgs e)
        {
           
           
           
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            progressBar1.Visible = false;
            if (Plant != "All")
            {
                DataTable dtname = Utilities.GetTable("select ppid from dbo.PowerPlant where PPName='" + Plant + "'");
                DataTable dt = Utilities.GetTable(" delete from ManualData where TargetMarketDate='" + BiddingDate + "'and PPID='" + dtname.Rows[0][0].ToString().Trim() + "'");
                fillgrid();
            }
            else
            {
                DataTable dt = Utilities.GetTable(" delete from ManualData where TargetMarketDate='" + BiddingDate + "'");
                fillgrid();

            }
        }

        private void btnsimilarblock_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            progressBar1.Visible = false;
            fillsimillarBlock(dataGridView1.CurrentCell.RowIndex);
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {

                string PPID = dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
                string Block = dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
                string Hour = dataGridView1.Rows[i].Cells[2].Value.ToString().Trim();
              

                double max = 0.0;
                if (dataGridView1.Rows[i].Cells[3].Value != null)
                {
                    max = MyDoubleParse(dataGridView1.Rows[i].Cells[3].Value.ToString().Trim());
                }

                double cap = 0.0;
                DataTable basetabledata = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
                if (basetabledata.Rows.Count > 0)
                {
                    cap = MyDoubleParse(basetabledata.Rows[0][0].ToString());

                }

                if ( max <= cap)
                {
                    double interval = 0.0;
                    if (dataGridView1.Rows[i].Cells[4].Value != null)
                    {
                        interval = MyDoubleParse(dataGridView1.Rows[i].Cells[4].Value.ToString().Trim());
                    }



                    DataTable dt1 = Utilities.GetTable("delete from ManualData where TargetMarketDate='" + BiddingDate + "'and PPID='" + PPID +
                    "'and Block='" + Block + "'and Hour='" + Hour + "'");



                    dt1 = Utilities.GetTable("insert into ManualData (TargetMarketDate,PPID,Block,Hour,PmaxPrice,Interval) values ('"
                   + BiddingDate + "','" + PPID + "','" + Block + "','" + Hour + "','" + max + "','" + interval + "')");
                }
                else
                {

                    MessageBox.Show("Please Fill All fild Correctly !.");
                    break;
                }
            }
           
            
        }

        private void btncap_Click_1(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;

            progressBar1.Visible = false;
            fillavgprice();         

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {

                string PPID = dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
                string Block = dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
                string Hour = dataGridView1.Rows[i].Cells[2].Value.ToString().Trim();
                double min = 0.0;
                if (dataGridView1.Rows[i].Cells[3].Value != null)
                {
                    min = MyDoubleParse(dataGridView1.Rows[i].Cells[3].Value.ToString().Trim());

                }

                double max = 0.0;
                if (dataGridView1.Rows[i].Cells[4].Value != null)
                {
                    max = MyDoubleParse(dataGridView1.Rows[i].Cells[4].Value.ToString().Trim());
                }

                double cap = 0.0;
                DataTable basetabledata = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
                if (basetabledata.Rows.Count > 0)
                {
                    cap = MyDoubleParse(basetabledata.Rows[0][0].ToString()) ;

                }

                if (max > min && max <= cap)
                {
                    double interval = 0.0;
                    if (dataGridView1.Rows[i].Cells[5].Value != null)
                    {
                        interval = MyDoubleParse(dataGridView1.Rows[i].Cells[5].Value.ToString().Trim());
                    }



                    DataTable dt1 = Utilities.GetTable("delete from ManualData where TargetMarketDate='" + BiddingDate + "'and PPID='" + PPID +
                    "'and Block='" + Block + "'and Hour='" + Hour + "'");



                    dt1 = Utilities.GetTable("insert into ManualData (TargetMarketDate,PPID,Block,Hour,PmaxPrice,PminPrice,Interval) values ('"
                   + BiddingDate + "','" + PPID + "','" + Block + "','" + Hour + "','" + max + "','" + min + "','" + interval + "')");
                }
                else
                {

                    MessageBox.Show("Please Fill All fild Correctly !.....\r\n...Price Max Must Be Bigger Than PriceMin");
                    break;
                }
            }
        }
        private void fillallplant(int ppid)
        {
            dataGridView1.Visible = true;
            progressBar1.Visible = false;
            string PPID = dataGridView1.Rows[0].Cells[0].Value.ToString().Trim();
            string Block = dataGridView1.Rows[0].Cells[1].Value.ToString().Trim();
         
             string [] bblock = Getblock002pack(ppid);
             Array.Sort(bblock);
             foreach (string block in bblock)
             {

                 // DataTable dt = utilities.GetTable("select distinct Block from dbo.Dispathable where ppid='" + ppid + "' and StartDate='1389/01/02'");
                 
                 for (int i = 1; i <= 24; i++)
                 {
                     DataTable dddr = Utilities.GetTable("select * from dbo.ManualData where TargetMarketDate='" + BiddingDate + "' and  ppid='" + PPID + "' and  Block='" +Block + "' AND hour='" + i + "'");
                     DataTable dt1 = Utilities.GetTable("delete from ManualData where TargetMarketDate='" + BiddingDate + "'and PPID='" + ppid +
                         "'and Block='" + block + "'and Hour='" + i + "'");

                     dt1 = Utilities.GetTable("insert into ManualData (TargetMarketDate,PPID,Block,Hour,PmaxPrice,PminPrice,Interval) values ('"
                    + BiddingDate + "','" + ppid + "','" + block + "','" + i + "','" + dddr.Rows[0][5].ToString() + "','" + dddr.Rows[0][4].ToString() + "','" + dddr.Rows[0][6].ToString() + "')");
                 }

             }
        }
        private void fillblockhour()
        {
            string[] pmax = new string [25];
            string[] pmin = new string [25];
            string [] interval = new string [25];

            for (int h = 1; h < 25; h++)
            {
                try
                {
                    pmin[h] = dataGridView1.Rows[h - 1].Cells[3].Value.ToString().Trim();
                    pmax[h] = dataGridView1.Rows[h - 1].Cells[4].Value.ToString().Trim();
                    interval[h] = dataGridView1.Rows[h - 1].Cells[5].Value.ToString().Trim();
                }
                catch
                {
                }
            }
            for (int i = 24; i < dataGridView1.Rows.Count; i++)
            {
                try
                {
                    string PPID = dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
                    string Block = dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
                    int Hour = int.Parse(dataGridView1.Rows[i].Cells[2].Value.ToString());
                    dataGridView1.Rows[i].Cells[3].Value = pmin[Hour].ToString();
                    dataGridView1.Rows[i].Cells[4].Value = pmax[Hour].ToString();
                    dataGridView1.Rows[i].Cells[5].Value = interval[Hour].ToString();
                }
                catch
                {

                }
                
            }
                
            
        }

        private void btnsavehourblock_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            progressBar1.Visible = false;
            fillblockhour();
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {

                string PPID = dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
                string Block = dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
                string Hour = dataGridView1.Rows[i].Cells[2].Value.ToString().Trim();
              
                double max = 0.0;
                if (dataGridView1.Rows[i].Cells[3].Value != null)
                {
                    max = MyDoubleParse(dataGridView1.Rows[i].Cells[3].Value.ToString().Trim());
                }

                double cap = 0.0;
                DataTable basetabledata = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
                if (basetabledata.Rows.Count > 0)
                {
                    cap = MyDoubleParse(basetabledata.Rows[0][0].ToString()) ;

                }

                if (max <= cap)
                {
                    double interval = 0.0;
                    if (dataGridView1.Rows[i].Cells[4].Value != null)
                    {
                        interval = MyDoubleParse(dataGridView1.Rows[i].Cells[4].Value.ToString().Trim());
                    }



                    DataTable dt1 = Utilities.GetTable("delete from ManualData where TargetMarketDate='" + BiddingDate + "'and PPID='" + PPID +
                    "'and Block='" + Block + "'and Hour='" + Hour + "'");



                    dt1 = Utilities.GetTable("insert into ManualData (TargetMarketDate,PPID,Block,Hour,PmaxPrice,Interval) values ('"
                   + BiddingDate + "','" + PPID + "','" + Block + "','" + Hour + "','" + max + "'," + interval + "')");
                }
                else
                {

                    MessageBox.Show("Please Fill All fild Correctly !.");
                    break;
                }
            }
        }

        private void long_task()
        {
            progressBar1.Visible = true;
            ManualClassfinal c1 = new ManualClassfinal(Plant, CurrentDate, BiddingDate, path, isavg, CAPPRICE);
            c1.value();
            UpdateProgressBar(progressBar1.Maximum, "");
        
        }
   

        public void UpdateProgressBar(int value, string description)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<int, string>(UpdateProgressBar), new object[] { value, description });
                return;
            }
         
            progressBar1.Value = value;
           
            Thread.Sleep(1);
            //if (progressBar1.Value == progressBar1.Maximum) progressBar1.Visible = false;
        }
    
        private void ManualBidding_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (thread != null)
                thread.Abort();
        }


        private string[] Getblock002pack(int ppid)
        {
            DataTable oDataTable = null;        

            DataTable oData = Utilities.GetTable("select unitcode from dbo.UnitsDataMain where PPID='" +
                   ppid + "'");


            DataTable oda = Utilities.GetTable("select  Max(PackageCode) from dbo.UnitsDataMain where PPID='" +
                  ppid + "'");

            int plantpack = int.Parse(oda.Rows[0][0].ToString());


            string[] blocks1 = new string[oData.Rows.Count];
                  

            
                int packcode = 0;
                for (int k = 0; k <oData.Rows.Count; k++)
                {
                    if (packcode <plantpack) packcode++;
                    oDataTable = Utilities.GetTable("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                       ppid + "'" + " and packageCode='" + packcode + "'");


                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        ppid + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY UnitType ASC");
                        int loopcount = 0;
                        foreach (DataRow mn in oDataTable.Rows)
                        {

                            string ptype = mn[0].ToString().Trim();
                            ptype = ptype.ToLower();
                            ptype = ptype.Replace("cc", "c");
                            string[] sp = ptype.Split('c');
                            ptype = sp[0].Trim() + sp[1].Trim();
                            if (ptype.Contains("gas"))
                                ptype = ptype.Replace("gas", "G");
                            else if (ptype.Contains("steam"))
                                ptype = ptype.Replace("steam", "S");

                            blocks1[k] = ptype+"-C";

                            loopcount++;
                            if (loopcount != oDataTable.Rows.Count) k++;

                        }



                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                       ppid + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'ORDER BY UnitType ASC");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                        string blockName1 = Initial;

                        blocks1[k] = blockName1;

                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        ppid + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'ORDER BY UnitType ASC");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                        string blockName1 = Initial;

                        blocks1[k] = blockName1;

                    }


                }
            
            return blocks1;
            


        }
        private string[] Getblock002(int ppid)
        { 
            string[] blocks1;
            DataTable oDataTable = null;

            DataTable oData = Utilities.GetTable("select unitcode from dbo.UnitsDataMain where PPID='" +
                   ppid + "'");


            DataTable oda = Utilities.GetTable("select  Max(PackageCode) from dbo.UnitsDataMain where PPID='" +
                  ppid + "'");

            int plantpack = int.Parse(oda.Rows[0][0].ToString());


          blocks1 = new string[oData.Rows.Count];
                  
            
           
                int packcode = 0;
                 for (int k = 0; k <oData.Rows.Count; k++)
               
                {
                    if (packcode <plantpack) packcode++;
                    oDataTable = Utilities.GetTable("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                      ppid + "'" + " and packageCode='" + packcode + "'");


                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        ppid + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY unittype asc");
                        int loopcount = 0;
                        foreach (DataRow mn in oDataTable.Rows)
                        {

                            string ptype = mn[0].ToString().Trim();
                            ptype = ptype.ToLower();
                            ptype = ptype.Replace("cc", "c");
                            string[] sp = ptype.Split('c');
                            ptype = sp[0].Trim() + sp[1].Trim();
                            if (ptype.Contains("gas"))
                                ptype = ptype.Replace("gas", "G");
                            else if (ptype.Contains("steam"))
                                ptype = ptype.Replace("steam", "S");

                            blocks1[k] = ptype;

                            loopcount++;
                            if (loopcount != oDataTable.Rows.Count) k++;

                        }



                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        ppid + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'ORDER BY unittype asc");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                        string blockName1 = Initial;

                        blocks1[k] = blockName1;

                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        ppid + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'ORDER BY unittype asc");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                        string blockName1 = Initial;

                        blocks1[k] = blockName1;

                    }


                }

                 return blocks1;
            }

        
        private void txtzarib_TextChanged(object sender, EventArgs e)
        {
            if(txtzarib.Text=="" || txtzarib.Text.Trim()=="0")
                MessageBox.Show("Please Insert Valid Number");


        }

        private void oldbtnsaveeasy_Click(object sender, EventArgs e)
        {
              double cap1 = 330000;
               DataTable bas = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
                if (bas.Rows.Count > 0)
                {
                    cap1 = MyDoubleParse(bas.Rows[0][0].ToString());
                 
                }
             


            string[] pricemin = new string[24];
            string[] pricemax = new string[24];
            string[] distance = new string[24];
            /////////////////////////changed value//////////////////////////////////////
            for (int i = 0; i < 24; i++)
            {
                if (i == 0)
                {
                    pricemin[i] = numeric1.Value.ToString();
                    pricemax[i] = numeric2.Value.ToString();
                    distance[i] = numeric3.Value.ToString();
                }

                if (i == 1)
                {

                    pricemin[i] = numeric4.Value.ToString();
                    pricemax[i] = numeric5.Value.ToString();
                    distance[i] = numeric6.Value.ToString();
                }
                if (i == 2)
                {

                    pricemin[i] = numeric7.Value.ToString();
                    pricemax[i] = numeric8.Value.ToString();
                    distance[i] = numeric9.Value.ToString();
                }
                if (i == 3)
                {

                    pricemin[i] = numeric10.Value.ToString();
                    pricemax[i] = numeric11.Value.ToString();
                    distance[i] = numeric12.Value.ToString();
                }
                if (i == 4)
                {

                    pricemin[i] = numeric13.Value.ToString();
                    pricemax[i] = numeric14.Value.ToString();
                    distance[i] = numeric15.Value.ToString();

                }
                if (i == 5)
                {

                    pricemin[i] = numeric16.Value.ToString();
                    pricemax[i] = numeric17.Value.ToString();
                    distance[i] = numeric18.Value.ToString();
                }
                if (i == 6)
                {

                    pricemin[i] = numeric19.Value.ToString();
                    pricemax[i] = numeric20.Value.ToString();
                    distance[i] = numeric21.Value.ToString();
                }
                if (i == 7)
                {

                    pricemin[i] = numeric22.Value.ToString();
                    pricemax[i] = numeric23.Value.ToString();
                    distance[i] = numeric24.Value.ToString();
                }
                if (i == 8)
                {

                    pricemin[i] = numeric25.Value.ToString();
                    pricemax[i] = numeric26.Value.ToString();
                    distance[i] = numeric27.Value.ToString();
                }
                if (i == 9)
                {

                    pricemin[i] = numeric28.Value.ToString();
                    pricemax[i] = numeric29.Value.ToString();
                    distance[i] = numeric30.Value.ToString();
                }
                if (i == 10)
                {

                    pricemin[i] = numeric31.Value.ToString();
                    pricemax[i] = numeric32.Value.ToString();
                    distance[i] = numeric33.Value.ToString();
                }
                if (i == 11)
                {

                    pricemin[i] = numeric34.Value.ToString();
                    pricemax[i] = numeric35.Value.ToString();
                    distance[i] = numeric36.Value.ToString();
                }
                if (i == 12)
                {

                    pricemin[i] = numeric37.Value.ToString();
                    pricemax[i] = numeric38.Value.ToString();
                    distance[i] = numeric39.Value.ToString();
                }
                if (i == 13)
                {

                    pricemin[i] = numeric40.Value.ToString();
                    pricemax[i] = numeric41.Value.ToString();
                    distance[i] = numeric42.Value.ToString();
                }
                if (i == 14)
                {

                    pricemin[i] = numeric43.Value.ToString();
                    pricemax[i] = numeric44.Value.ToString();
                    distance[i] = numeric45.Value.ToString();
                }
                if (i == 15)
                {

                    pricemin[i] = numeric46.Value.ToString();
                    pricemax[i] = numeric47.Value.ToString();
                    distance[i] = numeric48.Value.ToString();
                }
                if (i == 16)
                {

                    pricemin[i] = numeric49.Value.ToString();
                    pricemax[i] = numeric50.Value.ToString();
                    distance[i] = numeric51.Value.ToString();
                }
                if (i == 17)
                {

                    pricemin[i] = numeric52.Value.ToString();
                    pricemax[i] = numeric53.Value.ToString();
                    distance[i] = numeric54.Value.ToString();
                }
                if (i == 18)
                {

                    pricemin[i] = numeric55.Value.ToString();
                    pricemax[i] = numeric56.Value.ToString();
                    distance[i] = numeric57.Value.ToString();

                }
                if (i == 19)
                {

                    pricemin[i] = numeric58.Value.ToString();
                    pricemax[i] = numeric59.Value.ToString();
                    distance[i] = numeric60.Value.ToString();
                }
                if (i == 20)
                {

                    pricemin[i] = numeric61.Value.ToString();
                    pricemax[i] = numeric62.Value.ToString();
                    distance[i] = numeric63.Value.ToString();
                }
                if (i == 21)
                {

                    pricemin[i] = numeric64.Value.ToString();
                    pricemax[i] = numeric65.Value.ToString();
                    distance[i] = numeric66.Value.ToString();
                }
                if (i == 22)
                {

                    pricemin[i] = numeric67.Value.ToString();
                    pricemax[i] = numeric68.Value.ToString();
                    distance[i] = numeric69.Value.ToString();
                }
                if (i == 23)
                {


                    pricemin[i] = numeric70.Value.ToString();
                    pricemax[i] = numeric71.Value.ToString();
                    distance[i] = numeric72.Value.ToString();
                }
            }
            /////////////////////////save for all block///////////////////////////////////

            if (errorProvider1.GetError(labelh1) == "" && errorProvider1.GetError(labelh2) == "" &&
                errorProvider1.GetError(labelh3) == "" && errorProvider1.GetError(labelh4) == "" &&
                errorProvider1.GetError(labelh5) == "" && errorProvider1.GetError(labelh6) == "" &&
                errorProvider1.GetError(labelh7) == "" && errorProvider1.GetError(labelh8) == "" &&
                errorProvider1.GetError(labelh9) == "" && errorProvider1.GetError(labelh10) == "" &&
                errorProvider1.GetError(labelh11) == "" && errorProvider1.GetError(labelh12) == "" &&
                errorProvider1.GetError(labelh13) == "" && errorProvider1.GetError(labelh14) == "" &&
                errorProvider1.GetError(labelh15) == "" && errorProvider1.GetError(labelh16) == "" &&
                errorProvider1.GetError(labelh17) == "" && errorProvider1.GetError(labelh18) == "" &&
                errorProvider1.GetError(labelh19) == "" && errorProvider1.GetError(labelh20) == "" &&
                errorProvider1.GetError(labelh21) == "" && errorProvider1.GetError(labelh22) == "" &&
                errorProvider1.GetError(labelh23) == "" && errorProvider1.GetError(labelh24) == "")
            {



                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {

                    string PPID = dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
                    string Block = dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
                    string Hour = dataGridView1.Rows[i].Cells[2].Value.ToString().Trim();
                    int hHour = int.Parse(Hour);
                    double min = 0.0;

                    min = MyDoubleParse(pricemin[hHour - 1]);



                    double max = 0.0;

                    max = MyDoubleParse(pricemax[hHour - 1]);


                    double cap = 0.0;
                    DataTable basetabledata = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
                    if (basetabledata.Rows.Count > 0)
                    {
                        cap = MyDoubleParse(basetabledata.Rows[0][0].ToString()) ;

                    }


                    if (max > min && max <= cap)
                    {
                        double interval = 0.0;

                        interval = MyDoubleParse(distance[hHour - 1]);




                        DataTable dt1 = Utilities.GetTable("delete from ManualData where TargetMarketDate='" + BiddingDate + "'and PPID='" + PPID +
                        "'and Block='" + Block + "'and Hour='" + Hour + "'");



                        dt1 = Utilities.GetTable("insert into ManualData (TargetMarketDate,PPID,Block,Hour,PmaxPrice,PminPrice,Interval) values ('"
                       + BiddingDate + "','" + PPID + "','" + Block + "','" + Hour + "','" + max + "','" + min + "','" + interval + "')");
                    }
                    else
                    {

                        MessageBox.Show("Please Fill All fild Correctly !.....\r\n...Price Max Must Be Bigger Than PriceMin");

                        break;
                    }
                }
            }
            else
            {

                MessageBox.Show("Please Fill All fild Correctly !.....\r\n...Price Max Must Be Bigger Than PriceMin");

               
            }
            paneleasy.Visible = false;
            panehidebutton.Visible = false;
            dataGridView1.Visible = true;
            fillgrid();
        }
        private void btnsaveeasy_Click(object sender, EventArgs e)
        {
            double cap1 = 330000;
            DataTable bas = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
            if (bas.Rows.Count > 0)
            {
                cap1 = MyDoubleParse(bas.Rows[0][0].ToString());

            }



            string[] pricemin = new string[24];
            string[] pricemax = new string[24];
            string[] distance = new string[24];
            /////////////////////////changed value//////////////////////////////////////
            for (int i = 0; i < 24; i++)
            {
                if (i == 0)
                {
                    //pricemin[i] = numeric1.Value.ToString();
                    pricemax[i] = numeric1.Value.ToString();
                    distance[i] = numeric3.Value.ToString();
                }

                if (i == 1)
                {

                    //pricemin[i] = numeric4.Value.ToString();
                    pricemax[i] = numeric4.Value.ToString();
                    distance[i] = numeric6.Value.ToString();
                }
                if (i == 2)
                {

                   // pricemin[i] = numeric7.Value.ToString();
                    pricemax[i] = numeric7.Value.ToString();
                    distance[i] = numeric9.Value.ToString();
                }
                if (i == 3)
                {

                    //pricemin[i] = numeric10.Value.ToString();
                    pricemax[i] = numeric10.Value.ToString();
                    distance[i] = numeric12.Value.ToString();
                }
                if (i == 4)
                {

                    //pricemin[i] = numeric13.Value.ToString();
                    pricemax[i] = numeric13.Value.ToString();
                    distance[i] = numeric15.Value.ToString();

                }
                if (i == 5)
                {

                   // pricemin[i] = numeric16.Value.ToString();
                    pricemax[i] = numeric16.Value.ToString();
                    distance[i] = numeric18.Value.ToString();
                }
                if (i == 6)
                {

                   // pricemin[i] = numeric19.Value.ToString();
                    pricemax[i] = numeric19.Value.ToString();
                    distance[i] = numeric21.Value.ToString();
                }
                if (i == 7)
                {

                    //pricemin[i] = numeric22.Value.ToString();
                    pricemax[i] = numeric22.Value.ToString();
                    distance[i] = numeric24.Value.ToString();
                }
                if (i == 8)
                {

                   // pricemin[i] = numeric25.Value.ToString();
                    pricemax[i] = numeric25.Value.ToString();
                    distance[i] = numeric27.Value.ToString();
                }
                if (i == 9)
                {

                   // pricemin[i] = numeric28.Value.ToString();
                    pricemax[i] = numeric28.Value.ToString();
                    distance[i] = numeric30.Value.ToString();
                }
                if (i == 10)
                {

                    //pricemin[i] = numeric31.Value.ToString();
                    pricemax[i] = numeric31.Value.ToString();
                    distance[i] = numeric33.Value.ToString();
                }
                if (i == 11)
                {

                   // pricemin[i] = numeric34.Value.ToString();
                    pricemax[i] = numeric34.Value.ToString();
                    distance[i] = numeric36.Value.ToString();
                }
                if (i == 12)
                {

                    //pricemin[i] = numeric37.Value.ToString();
                    pricemax[i] = numeric37.Value.ToString();
                    distance[i] = numeric39.Value.ToString();
                }
                if (i == 13)
                {

                    //pricemin[i] = numeric40.Value.ToString();
                    pricemax[i] = numeric40.Value.ToString();
                    distance[i] = numeric42.Value.ToString();
                }
                if (i == 14)
                {

                   // pricemin[i] = numeric43.Value.ToString();
                    pricemax[i] = numeric43.Value.ToString();
                    distance[i] = numeric45.Value.ToString();
                }
                if (i == 15)
                {

                   // pricemin[i] = numeric46.Value.ToString();
                    pricemax[i] = numeric46.Value.ToString();
                    distance[i] = numeric48.Value.ToString();
                }
                if (i == 16)
                {

                   // pricemin[i] = numeric49.Value.ToString();
                    pricemax[i] = numeric49.Value.ToString();
                    distance[i] = numeric51.Value.ToString();
                }
                if (i == 17)
                {

                  // pricemin[i] = numeric52.Value.ToString();
                    pricemax[i] = numeric52.Value.ToString();
                    distance[i] = numeric54.Value.ToString();
                }
                if (i == 18)
                {

                    //pricemin[i] = numeric55.Value.ToString();
                    pricemax[i] = numeric55.Value.ToString();
                    distance[i] = numeric57.Value.ToString();

                }
                if (i == 19)
                {

                    //pricemin[i] = numeric58.Value.ToString();
                    pricemax[i] = numeric58.Value.ToString();
                    distance[i] = numeric60.Value.ToString();
                }
                if (i == 20)
                {

                    //pricemin[i] = numeric61.Value.ToString();
                    pricemax[i] = numeric61.Value.ToString();
                    distance[i] = numeric63.Value.ToString();
                }
                if (i == 21)
                {

                   // pricemin[i] = numeric64.Value.ToString();
                    pricemax[i] = numeric64.Value.ToString();
                    distance[i] = numeric66.Value.ToString();
                }
                if (i == 22)
                {

                   // pricemin[i] = numeric67.Value.ToString();
                    pricemax[i] = numeric67.Value.ToString();
                    distance[i] = numeric69.Value.ToString();
                }
                if (i == 23)
                {


                    //pricemin[i] = numeric70.Value.ToString();
                    pricemax[i] = numeric70.Value.ToString();
                    distance[i] = numeric72.Value.ToString();
                }
            }
            /////////////////////////save for all block///////////////////////////////////

            if (errorProvider1.GetError(labelh1) == "" && errorProvider1.GetError(labelh2) == "" &&
                errorProvider1.GetError(labelh3) == "" && errorProvider1.GetError(labelh4) == "" &&
                errorProvider1.GetError(labelh5) == "" && errorProvider1.GetError(labelh6) == "" &&
                errorProvider1.GetError(labelh7) == "" && errorProvider1.GetError(labelh8) == "" &&
                errorProvider1.GetError(labelh9) == "" && errorProvider1.GetError(labelh10) == "" &&
                errorProvider1.GetError(labelh11) == "" && errorProvider1.GetError(labelh12) == "" &&
                errorProvider1.GetError(labelh13) == "" && errorProvider1.GetError(labelh14) == "" &&
                errorProvider1.GetError(labelh15) == "" && errorProvider1.GetError(labelh16) == "" &&
                errorProvider1.GetError(labelh17) == "" && errorProvider1.GetError(labelh18) == "" &&
                errorProvider1.GetError(labelh19) == "" && errorProvider1.GetError(labelh20) == "" &&
                errorProvider1.GetError(labelh21) == "" && errorProvider1.GetError(labelh22) == "" &&
                errorProvider1.GetError(labelh23) == "" && errorProvider1.GetError(labelh24) == "")
            {



                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {

                    string PPID = dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
                    string Block = dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
                    string Hour = dataGridView1.Rows[i].Cells[2].Value.ToString().Trim();
                    int hHour = int.Parse(Hour);
                    //double min = 0.0;

                    // min = MyDoubleParse(pricemin[hHour - 1]);



                    double max = 0.0;

                    max = MyDoubleParse(pricemax[hHour - 1]);


                    double cap = 0.0;
                    DataTable basetabledata = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
                    if (basetabledata.Rows.Count > 0)
                    {
                        cap = MyDoubleParse(basetabledata.Rows[0][0].ToString());

                    }


                    if (max <= cap)
                    {
                        double interval = 0.0;

                        interval = MyDoubleParse(distance[hHour - 1]);




                        DataTable dt1 = Utilities.GetTable("delete from ManualData where TargetMarketDate='" + BiddingDate + "'and PPID='" + PPID +
                        "'and Block='" + Block + "'and Hour='" + Hour + "'");



                        dt1 = Utilities.GetTable("insert into ManualData (TargetMarketDate,PPID,Block,Hour,PmaxPrice,Interval) values ('"
                       + BiddingDate + "','" + PPID + "','" + Block + "','" + Hour + "','" + max + "','" + interval + "')");
                    }
                    else
                    {

                        MessageBox.Show("Please Fill All fild Correctly !.....\r\n..");

                        break;
                    }
                }
            }
            else
            {

                MessageBox.Show("Please Fill All fild Correctly !.....\r\n...");


            }
            paneleasy.Visible = false;
            panehidebutton.Visible = false;
            dataGridView1.Visible = true;
            fillgrid();
        }





        //private void numeric1_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString());

        //        }
        //        string pmin = numeric1.Value.ToString();
        //        string pmax = numeric2.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[0].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[0].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric2.Maximum) numeric2.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric2.Value >decimal.Parse(cap1.ToString())) numeric2.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric1.Value >= numeric2.Value) numeric1.Value = numeric2.Value - 500;


        //        ////////////////1////////////////////////////////

        //        if (numeric1.Value >= numeric2.Value)
        //            errorProvider1.SetError(labelh1, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh1, "");
        //    }
        //}

        //private void numeric4_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString());

        //        }
        //        string pmin = numeric4.Value.ToString();
        //        string pmax = numeric5.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[1].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[1].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x) ;
        //            if (decimal.Parse((y + sub).ToString()) <= numeric5.Maximum) numeric5.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric5.Value >decimal.Parse(cap1.ToString())) numeric5.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric4.Value >= numeric5.Value) numeric4.Value = numeric5.Value - 500;

        //        ////////////////2////////////////////////////////

        //        if (numeric4.Value >= numeric5.Value)
        //            errorProvider1.SetError(labelh2, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh2, "");
        //    }
        //}

        //private void numeric7_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString()) ;

        //        }
        //        string pmin = numeric7.Value.ToString();
        //        string pmax = numeric8.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[2].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[2].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric8.Maximum) numeric8.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric8.Value >decimal.Parse(cap1.ToString())) numeric8.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric7.Value >= numeric8.Value) numeric7.Value = numeric8.Value - 500;

        //        ////////////////3////////////////////////////////

        //        if (numeric7.Value >= numeric8.Value)
        //            errorProvider1.SetError(labelh3, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh3, "");
        //    }
        //}

        //private void numeric10_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString());

        //        }
        //        string pmin = numeric10.Value.ToString();
        //        string pmax = numeric11.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[3].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[3].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric11.Maximum) numeric11.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric11.Value > decimal.Parse(cap1.ToString())) numeric11.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric10.Value >= numeric11.Value) numeric10.Value = numeric11.Value - 500;

        //        ////////////////4////////////////////////////////

        //        if (numeric10.Value >= numeric11.Value)
        //            errorProvider1.SetError(labelh4, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh4, "");
        //    }
        //}

        //private void numeric13_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString());

        //        }
        //        string pmin = numeric13.Value.ToString();
        //        string pmax = numeric14.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[4].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[4].Cells[4].Value.ToString());

        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric14.Maximum) numeric14.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric14.Value > decimal.Parse(cap1.ToString())) numeric14.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric13.Value >= numeric14.Value) numeric13.Value = numeric14.Value - 500;

        //        ////////////////5////////////////////////////////

        //        if (numeric13.Value >= numeric14.Value)
        //            errorProvider1.SetError(labelh5, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh5, "");
        //    }
        //}

        //private void numeric16_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date='"+maxbasedate+"'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString());

        //        }
        //        string pmin = numeric16.Value.ToString();
        //        string pmax = numeric17.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[5].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[5].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric17.Maximum) numeric17.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric17.Value > decimal.Parse(cap1.ToString())) numeric17.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric16.Value >= numeric17.Value) numeric16.Value = numeric17.Value - 500;

        //        ////////////////6////////////////////////////////

        //        if (numeric16.Value >= numeric17.Value)
        //            errorProvider1.SetError(labelh6, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh6, "");
        //    }
        //}

        //private void numeric19_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date='"+maxbasedate+"' order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString()) ;

        //        }
        //        string pmin = numeric19.Value.ToString();
        //        string pmax = numeric20.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[6].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[6].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric20.Maximum) numeric20.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric20.Value > decimal.Parse(cap1.ToString())) numeric20.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric19.Value >= numeric20.Value) numeric19.Value = numeric20.Value - 500;

        //        ////////////////7////////////////////////////////

        //        if (numeric19.Value >= numeric20.Value)
        //            errorProvider1.SetError(labelh7, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh7, "");
        //    }
        //}

        //private void numeric22_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+maxbasedate+"'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString()) ;

        //        }
        //        string pmin = numeric22.Value.ToString();
        //        string pmax = numeric23.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[7].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[7].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric23.Maximum) numeric23.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric23.Value > decimal.Parse(cap1.ToString())) numeric23.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric22.Value >= numeric23.Value) numeric22.Value = numeric23.Value - 500;

        //        ////////////////8////////////////////////////////

        //        if (numeric22.Value >= numeric23.Value)
        //            errorProvider1.SetError(labelh8, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh8, "");
        //    }
        //}

        //private void numeric25_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date='"+maxbasedate+"'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString()) ;

        //        }
        //        string pmin = numeric25.Value.ToString();
        //        string pmax = numeric26.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[8].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[8].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric26.Maximum) numeric26.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric26.Value > decimal.Parse(cap1.ToString())) numeric26.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric25.Value >= numeric26.Value) numeric25.Value = numeric26.Value - 500;

        //        ////////////////9////////////////////////////////

        //        if (numeric25.Value >= numeric26.Value)
        //            errorProvider1.SetError(labelh9, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh9, "");
        //    }
        //}

        //private void numeric28_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date='" + maxbasedate + "'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString()) ;

        //        }
        //        string pmin = numeric28.Value.ToString();
        //        string pmax = numeric29.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[9].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[9].Cells[4].Value.ToString());

        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric29.Maximum) numeric29.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric29.Value > decimal.Parse(cap1.ToString())) numeric29.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric28.Value >= numeric29.Value) numeric28.Value = numeric29.Value - 500;

        //        ////////////////10////////////////////////////////

        //        if (numeric28.Value >= numeric29.Value)
        //            errorProvider1.SetError(labelh10, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh10, "");
        //    }
        //}

        //private void numeric31_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString());

        //        }
        //        string pmin = numeric31.Value.ToString();
        //        string pmax = numeric32.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[10].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[10].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric32.Maximum) numeric32.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric32.Value > decimal.Parse(cap1.ToString())) numeric32.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric31.Value >= numeric32.Value) numeric31.Value = numeric32.Value - 5000;

        //        ////////////////11////////////////////////////////

        //        if (numeric31.Value >= numeric32.Value)
        //            errorProvider1.SetError(labelh11, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh11, "");
        //    }
        //}

        //private void numeric34_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString()) ;

        //        }
        //        string pmin = numeric34.Value.ToString();
        //        string pmax = numeric35.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[11].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[11].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric35.Maximum) numeric35.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric35.Value > decimal.Parse(cap1.ToString())) numeric35.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric34.Value >= numeric35.Value) numeric34.Value = numeric35.Value - 5000;

        //        ////////////////12////////////////////////////////

        //        if (numeric34.Value >= numeric35.Value)
        //            errorProvider1.SetError(labelh12, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh12, "");
        //    }
        //}

        //private void numeric37_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date='" + maxbasedate + "'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString());

        //        }
        //        string pmin = numeric37.Value.ToString();
        //        string pmax = numeric38.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[12].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[12].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric38.Maximum) numeric38.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric38.Value > decimal.Parse(cap1.ToString())) numeric38.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric37.Value >= numeric38.Value) numeric37.Value = numeric38.Value - 5000;

        //        ////////////////13////////////////////////////////

        //        if (numeric37.Value >= numeric38.Value)
        //            errorProvider1.SetError(labelh13, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh13, "");
        //    }
        //}

        //private void numeric40_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString()) ;

        //        }
        //        string pmin = numeric40.Value.ToString();
        //        string pmax = numeric41.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[13].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[13].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric41.Maximum) numeric41.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric41.Value > decimal.Parse(cap1.ToString())) numeric41.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric40.Value >= numeric41.Value) numeric40.Value = numeric41.Value - 500;

        //        ////////////////14////////////////////////////////

        //        if (numeric40.Value >= numeric41.Value)
        //            errorProvider1.SetError(labelh14, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh14, "");
        //    }
        //}

        //private void numeric43_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date='" + maxbasedate + "'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString()) ;

        //        }
        //        string pmin = numeric43.Value.ToString();
        //        string pmax = numeric44.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[14].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[14].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric44.Maximum) numeric44.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric44.Value > decimal.Parse(cap1.ToString())) numeric44.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric43.Value >= numeric44.Value) numeric43.Value = numeric44.Value - 500;

        //        ////////////////15////////////////////////////////

        //        if (numeric43.Value >= numeric44.Value)
        //            errorProvider1.SetError(labelh15, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh15, "");
        //    }
        //}

        //private void numeric46_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString());

        //        }
        //        string pmin = numeric46.Value.ToString();
        //        string pmax = numeric47.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[15].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[15].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric47.Maximum) numeric47.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric47.Value > decimal.Parse(cap1.ToString())) numeric47.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric46.Value >= numeric47.Value) numeric46.Value = numeric47.Value - 500;

        //        ////////////////16////////////////////////////////

        //        if (numeric46.Value >= numeric47.Value)
        //            errorProvider1.SetError(labelh16, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh16, "");
        //    }
        //}

        //private void numeric49_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date='" + maxbasedate + "'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString());

        //        }
        //        string pmin = numeric49.Value.ToString();
        //        string pmax = numeric50.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[16].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[16].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric50.Maximum) numeric50.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric50.Value > decimal.Parse(cap1.ToString())) numeric50.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric49.Value >= numeric50.Value) numeric49.Value = numeric50.Value - 500;

        //        ////////////////17////////////////////////////////

        //        if (numeric49.Value >= numeric50.Value)
        //            errorProvider1.SetError(labelh17, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh17, "");
        //    }
        //}

        //private void numeric52_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString());

        //        }
        //        string pmin = numeric52.Value.ToString();
        //        string pmax = numeric53.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[17].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[17].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric53.Maximum) numeric53.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric53.Value > decimal.Parse(cap1.ToString())) numeric53.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric52.Value >= numeric53.Value) numeric52.Value = numeric53.Value - 500;

        //        ////////////////18////////////////////////////////

        //        if (numeric52.Value >= numeric53.Value)
        //            errorProvider1.SetError(labelh18, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh18, "");
        //    }
        //}

        //private void numeric55_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString()) ;

        //        }
        //        string pmin = numeric55.Value.ToString();
        //        string pmax = numeric56.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[18].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[18].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric56.Maximum) numeric56.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric56.Value > decimal.Parse(cap1.ToString())) numeric56.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric55.Value >= numeric56.Value) numeric55.Value = numeric56.Value - 500;

        //        ////////////////19////////////////////////////////

        //        if (numeric55.Value >= numeric56.Value)
        //            errorProvider1.SetError(labelh19, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh19, "");
        //    }
        //}

        //private void numeric58_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString()) ;

        //        }
        //        string pmin = numeric58.Value.ToString();
        //        string pmax = numeric59.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[19].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[19].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric59.Maximum) numeric59.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric59.Value > decimal.Parse(cap1.ToString())) numeric59.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric58.Value >= numeric59.Value) numeric58.Value = numeric59.Value - 500;

        //        ////////////////20////////////////////////////////

        //        if (numeric58.Value >= numeric59.Value)
        //            errorProvider1.SetError(labelh20, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh20, "");
        //    }
        //}

        //private void numeric61_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString()) ;

        //        }
        //        string pmin = numeric61.Value.ToString();
        //        string pmax = numeric62.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[20].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[20].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric62.Maximum) numeric62.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric62.Value > decimal.Parse(cap1.ToString())) numeric62.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric61.Value >= numeric62.Value) numeric61.Value = numeric62.Value - 500;

        //        ////////////////21////////////////////////////////

        //        if (numeric61.Value >= numeric62.Value)
        //            errorProvider1.SetError(labelh21, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh21, "");
        //    }
        //}

        //private void numeric64_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString());

        //        }
        //        string pmin = numeric64.Value.ToString();
        //        string pmax = numeric65.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[21].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[21].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric65.Maximum) numeric65.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric65.Value > decimal.Parse(cap1.ToString())) numeric65.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric64.Value >= numeric65.Value) numeric64.Value = numeric65.Value - 500;

        //        ////////////////22////////////////////////////////

        //        if (numeric64.Value >= numeric65.Value)
        //            errorProvider1.SetError(labelh22, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh22, "");
        //    }
        //}

        //private void numeric67_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString()) ;

        //        }
        //        string pmin = numeric67.Value.ToString();
        //        string pmax = numeric68.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[22].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[22].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric68.Maximum) numeric68.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric68.Value > decimal.Parse(cap1.ToString())) numeric68.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric67.Value >= numeric68.Value) numeric67.Value = numeric68.Value - 500;

        //        ////////////////23////////////////////////////////

        //        if (numeric67.Value >= numeric68.Value)
        //            errorProvider1.SetError(labelh23, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh23, "");
        //    }
        //}

        //private void numeric70_ValueChanged(object sender, EventArgs e)
        //{
        //    if (easyload == false)
        //    {
        //        double cap1 = 330000;
        //        DataTable bas = utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='" + maxbasedate + "'order by BaseID desc");
        //        if (bas.Rows.Count > 0)
        //        {
        //            cap1 = MyDoubleParse(bas.Rows[0][0].ToString());

        //        }
        //        string pmin = numeric70.Value.ToString();
        //        string pmax = numeric71.Value.ToString();
        //        //////////////calculate sub pmin for add to pmax/////////////////////////
        //        double x = MyDoubleParse(dataGridView1.Rows[23].Cells[3].Value.ToString());
        //        double y = MyDoubleParse(dataGridView1.Rows[23].Cells[4].Value.ToString());
        //        double sub = 0.0;
        //        if (double.Parse(pmin) > x)
        //        {
        //            sub = (double.Parse(pmin) - x);
        //            if (decimal.Parse((y + sub).ToString()) <= numeric71.Maximum) numeric71.Value = decimal.Parse((y + sub).ToString());
        //        }
        //        if (numeric71.Value > decimal.Parse(cap1.ToString())) numeric71.Value = decimal.Parse(cap1.ToString());
        //        //////////////////////////////////////////////////////////////////////////////
        //        if (numeric70.Value >= numeric71.Value) numeric70.Value = numeric71.Value - 500;

        //        ////////////////24////////////////////////////////

        //        if (numeric70.Value >= numeric71.Value)
        //            errorProvider1.SetError(labelh24, "Price Max Must Be Bigger Than PriceMin");
        //        else
        //            errorProvider1.SetError(labelh24, "");
        //    }
        //}

       

        //private void numeric2_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////1////////////////////////////////

        //    if (numeric1.Value >= numeric2.Value)
        //        errorProvider1.SetError(labelh1, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh1, "");
        //}

        //private void numeric5_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////2////////////////////////////////

        //    if (numeric4.Value >= numeric5.Value)
        //        errorProvider1.SetError(labelh2, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh2, "");
        //}

        //private void numeric8_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////3////////////////////////////////

        //    if (numeric7.Value >= numeric8.Value)
        //        errorProvider1.SetError(labelh3, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh3, "");
        //}

        //private void numeric11_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////4////////////////////////////////

        //    if (numeric10.Value >= numeric11.Value)
        //        errorProvider1.SetError(labelh4, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh4, "");
        //}

        //private void numeric14_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////5////////////////////////////////

        //    if (numeric13.Value >= numeric14.Value)
        //        errorProvider1.SetError(labelh5, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh5, "");
        //}

        //private void numeric17_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////6////////////////////////////////

        //    if (numeric16.Value >= numeric17.Value)
        //        errorProvider1.SetError(labelh6, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh6, "");
        //}

        //private void numeric20_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////7////////////////////////////////

        //    if (numeric19.Value >= numeric20.Value)
        //        errorProvider1.SetError(labelh7, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh7, "");
        //}

        //private void numeric23_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////8////////////////////////////////

        //    if (numeric22.Value >= numeric23.Value)
        //        errorProvider1.SetError(labelh8, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh8, "");
        //}

        //private void numeric26_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////9////////////////////////////////

        //    if (numeric25.Value >= numeric26.Value)
        //        errorProvider1.SetError(labelh9, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh9, "");
        //}

        //private void numeric29_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////10////////////////////////////////

        //    if (numeric28.Value >= numeric29.Value)
        //        errorProvider1.SetError(labelh10, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh10, "");
        //}

        //private void numeric32_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////11////////////////////////////////

        //    if (numeric31.Value >= numeric32.Value)
        //        errorProvider1.SetError(labelh11, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh11, "");
        //}

        //private void numeric35_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////12////////////////////////////////

        //    if (numeric34.Value >= numeric35.Value)
        //        errorProvider1.SetError(labelh12, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh12, "");
        //}

        //private void numeric38_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////13////////////////////////////////

        //    if (numeric37.Value >= numeric38.Value)
        //        errorProvider1.SetError(labelh13, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh13, "");
        //}

        //private void numeric41_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////14////////////////////////////////

        //    if (numeric40.Value >= numeric41.Value)
        //        errorProvider1.SetError(labelh14, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh14, "");
        //}

        //private void numeric44_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////15////////////////////////////////

        //    if (numeric43.Value >= numeric44.Value)
        //        errorProvider1.SetError(labelh15, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh15, "");
        //}

        //private void numeric47_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////16////////////////////////////////

        //    if (numeric46.Value >= numeric47.Value)
        //        errorProvider1.SetError(labelh16, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh16, "");
        //}

        //private void numeric50_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////17////////////////////////////////

        //    if (numeric49.Value >= numeric50.Value)
        //        errorProvider1.SetError(labelh17, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh17, "");
        //}

        //private void numeric53_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////18////////////////////////////////

        //    if (numeric52.Value >= numeric53.Value)
        //        errorProvider1.SetError(labelh18, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh18, "");
        //}

        //private void numeric56_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////19////////////////////////////////

        //    if (numeric55.Value >= numeric56.Value)
        //        errorProvider1.SetError(labelh19, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh19, "");
        //}

        //private void numeric59_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////20////////////////////////////////

        //    if (numeric58.Value >= numeric59.Value)
        //        errorProvider1.SetError(labelh20, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh20, "");
        //}

        //private void numeric62_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////21////////////////////////////////

        //    if (numeric61.Value >= numeric62.Value)
        //        errorProvider1.SetError(labelh21, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh21, "");
        //}

        //private void numeric65_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////22////////////////////////////////

        //    if (numeric64.Value >= numeric65.Value)
        //        errorProvider1.SetError(labelh22, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh22, "");
        //}

        //private void numeric68_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////23////////////////////////////////

        //    if (numeric67.Value >= numeric68.Value)
        //        errorProvider1.SetError(labelh23, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh23, "");
        //}

        //private void numeric71_ValueChanged(object sender, EventArgs e)
        //{
        //    ////////////////24////////////////////////////////

        //    if (numeric70.Value >= numeric71.Value)
        //        errorProvider1.SetError(labelh24, "Price Max Must Be Bigger Than PriceMin");
        //    else
        //        errorProvider1.SetError(labelh24, "");
        //}



        private string buildmaxdate(string[] arrmax)
        {
            string maxdate = null;
            for (int fd = 0; fd < arrmax.Length; fd++)
            {
                if (maxdate == null || string.Compare(maxdate, arrmax[fd]) < 0)
                    maxdate = arrmax[fd];
            }
            return maxdate;

        }

        public bool Findcconetype(string ppid)
        {
            int tr = 0;

          DataTable   oDataTable = Utilities.GetTable("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count == 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }

        public bool Findcctwotype(string ppid)
        {
            int tr = 0;

            DataTable oDataTable = Utilities.GetTable("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count >1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 24; i++)
            {
                if (i == 0)
                {
                    numeric3.Value = decimal.Parse(textBox1.Text.ToString());
                }

                if (i == 1)
                {
                    numeric6.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 2)
                {

                    numeric9.Value =decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 3)
                {
                numeric12.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 4)
                {
                numeric15.Value= decimal.Parse(textBox1.Text.ToString());

                }
                if (i == 5)
                {

                    numeric18.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 6)
                {

                     numeric21.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 7)
                {

                    numeric24.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 8)
                {

                    numeric27.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 9)
                {

                    numeric30.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 10)
                {

                     numeric33.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 11)
                {

                     numeric36.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 12)
                {

                  numeric39.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 13)
                {

                    numeric42.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 14)
                {

                    numeric45.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 15)
                {

                    numeric48.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 16)
                {

                    numeric51.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 17)
                {

                    numeric54.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 18)
                {

                     numeric57.Value= decimal.Parse(textBox1.Text.ToString());

                }
                if (i == 19)
                {

                   numeric60.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 20)
                {

                    numeric63.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 21)
                {

                    numeric66.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 22)
                {

                     numeric69.Value= decimal.Parse(textBox1.Text.ToString());
                }
                if (i == 23)
                {


                    numeric72.Value = decimal.Parse(textBox1.Text.ToString());
                }
            }
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            DataTable dtname = Utilities.GetTable("select ppid from dbo.PowerPlant where PPName='" + Plant + "'");
            string[] bblock = Getblock002pack(int.Parse(dtname.Rows[0][0].ToString()));
           
            sblock = comboBox1.Text.Trim();
            string[] bblockname = Getblock002(int.Parse(dtname.Rows[0][0].ToString()));
         
            for (int yi = 0; yi < bblock.Length; yi++)
            {

                if (bblock[yi] == sblock)
                {
                    blockname002 = bblockname[yi];
                    break;
                }
            
               
            }
        }

       
     

       

       

       
    }
}
