﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    public partial class EfficiencyMarketForm : Form
    {
        string LastDate = "";
        public EfficiencyMarketForm()
        {
            InitializeComponent();

            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }

        }
//----------------------------------EfficiencyMarketForm_Load----------------------------
        private void EfficiencyMarketForm_Load(object sender, EventArgs e)
        {
            FromDateCal.SelectedDateTime = System.DateTime.Now;
            ToDateCal.SelectedDateTime = System.DateTime.Now;
            SqlConnection MyConnection = new SqlConnection(ConnectionManager.ConnectionString);
            MyConnection.Open();
            DataSet MyDs = new DataSet();
            SqlDataAdapter MyDa = new SqlDataAdapter();
            MyDa.SelectCommand = new SqlCommand("SELECT Date,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8" +
            ",Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21," +
            "Hour22,Hour23,Hour24,FromDate,ToDate FROM EfficiencyMarket WHERE Date=(SELECT MAX(Date) FROM " +
            "EfficiencyMarket)", MyConnection);
            MyDa.Fill(MyDs);
            foreach (DataRow MyRow in MyDs.Tables[0].Rows)
            {
                LastDate = MyRow[0].ToString().Trim();
                FromDateCal.Text = MyRow[25].ToString().Trim();
                ToDateCal.Text = MyRow[26].ToString().Trim();
                H1Tb.Text = MyRow[1].ToString().Trim();
                H2Tb.Text = MyRow[2].ToString().Trim();
                H3Tb.Text = MyRow[3].ToString().Trim();
                H4Tb.Text = MyRow[4].ToString().Trim();
                H5Tb.Text = MyRow[5].ToString().Trim();
                H6Tb.Text = MyRow[6].ToString().Trim();
                H7Tb.Text = MyRow[7].ToString().Trim();
                H8Tb.Text = MyRow[8].ToString().Trim();
                H9Tb.Text = MyRow[9].ToString().Trim();
                H10Tb.Text = MyRow[10].ToString().Trim();
                H11Tb.Text = MyRow[11].ToString().Trim();
                H12Tb.Text = MyRow[12].ToString().Trim();
                H13Tb.Text = MyRow[13].ToString().Trim();
                H14Tb.Text = MyRow[14].ToString().Trim();
                H15Tb.Text = MyRow[15].ToString().Trim();
                H16Tb.Text = MyRow[16].ToString().Trim();
                H17Tb.Text = MyRow[17].ToString().Trim();
                H18Tb.Text = MyRow[18].ToString().Trim();
                H19Tb.Text = MyRow[19].ToString().Trim();
                H20Tb.Text = MyRow[20].ToString().Trim();
                H21Tb.Text = MyRow[21].ToString().Trim();
                H22Tb.Text = MyRow[22].ToString().Trim();
                H23Tb.Text = MyRow[23].ToString().Trim();
                H24Tb.Text = MyRow[24].ToString().Trim();
            }
            MyConnection.Close();
        }
//---------------------------------H1Tb_Validated-----------------------------
        private void H1Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H1Tb.Text);
                errorProvider1.SetError(H1Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H1Tb, "Invalid Value!");
            }
        }
//----------------------------H2Tb_Validated--------------------------------
        private void H2Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H2Tb.Text);
                errorProvider1.SetError(H2Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H2Tb, "Invalid Value!");
            }
        }
//--------------------------------H3Tb_Validated-------------------------------
        private void H3Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H3Tb.Text);
                errorProvider1.SetError(H3Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H3Tb, "Invalid Value!");
            }
        }
//----------------------------------H4Tb_Validated-------------------------------
        private void H4Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H4Tb.Text);
                errorProvider1.SetError(H4Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H4Tb, "Invalid Value!");
            }
        }
//------------------------------------H5Tb_Validated------------------------
        private void H5Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H5Tb.Text);
                errorProvider1.SetError(H5Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H5Tb, "Invalid Value!");
            }
        }
//--------------------------------H6Tb_Validated------------------------------
        private void H6Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H6Tb.Text);
                errorProvider1.SetError(H6Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H6Tb, "Invalid Value!");
            }
        }
//----------------------------------H7Tb_Validated------------------------------
        private void H7Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H7Tb.Text);
                errorProvider1.SetError(H7Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H7Tb, "Invalid Value!");
            }
        }
//------------------------------------H8Tb_Validated------------------------------
        private void H8Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H8Tb.Text);
                errorProvider1.SetError(H8Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H8Tb, "Invalid Value!");
            }
        }
//------------------------------------H9Tb_Validated----------------------------
        private void H9Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H9Tb.Text);
                errorProvider1.SetError(H9Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H9Tb, "Invalid Value!");
            }
        }
//--------------------------------H10Tb_Validated-----------------------
        private void H10Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H10Tb.Text);
                errorProvider1.SetError(H10Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H10Tb, "Invalid Value!");
            }
        }
//----------------------------------H11Tb_Validated------------------------
        private void H11Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H11Tb.Text);
                errorProvider1.SetError(H11Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H11Tb, "Invalid Value!");
            }
        }
//----------------------------------H12Tb_Validated---------------------
        private void H12Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H12Tb.Text);
                errorProvider1.SetError(H12Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H12Tb, "Invalid Value!");
            }
        }
//------------------------------------H13Tb_Validated--------------------------
        private void H13Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H13Tb.Text);
                errorProvider1.SetError(H13Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H13Tb, "Invalid Value!");
            }
        }
//------------------------------H14Tb_Validated------------------------
        private void H14Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H14Tb.Text);
                errorProvider1.SetError(H14Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H14Tb, "Invalid Value!");
            }
        }
//----------------------------------H15Tb_Validated----------------------
        private void H15Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H15Tb.Text);
                errorProvider1.SetError(H15Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H15Tb, "Invalid Value!");
            }
        }
//------------------------------------H16Tb_Validated------------------------
        private void H16Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H16Tb.Text);
                errorProvider1.SetError(H16Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H16Tb, "Invalid Value!");
            }
        }
//---------------------------------------H17Tb_Validated----------------------------------
        private void H17Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H17Tb.Text);
                errorProvider1.SetError(H17Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H17Tb, "Invalid Value!");
            }
        }
//---------------------------------------H18Tb_Validated----------------------
        private void H18Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H18Tb.Text);
                errorProvider1.SetError(H18Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H18Tb, "Invalid Value!");
            }
        }
//-------------------------------------------H19Tb_Validated----------------------------
        private void H19Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H19Tb.Text);
                errorProvider1.SetError(H19Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H19Tb, "Invalid Value!");
            }
        }
//-------------------------------------------H20Tb_Validated-----------------------------
        private void H20Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H20Tb.Text);
                errorProvider1.SetError(H20Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H20Tb, "Invalid Value!");
            }
        }
//------------------------------------------H21Tb_Validated-----------------------------
        private void H21Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H21Tb.Text);
                errorProvider1.SetError(H21Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H21Tb, "Invalid Value!");
            }
        }
//-----------------------------------------H22Tb_Validated----------------------------
        private void H22Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H22Tb.Text);
                errorProvider1.SetError(H22Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H22Tb, "Invalid Value!");
            }
        }
//----------------------------------------H23Tb_Validated----------------------
        private void H23Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H23Tb.Text);
                errorProvider1.SetError(H23Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H23Tb, "Invalid Value!");
            }
        }
//--------------------------------------H24Tb_Validated--------------------
        private void H24Tb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(H24Tb.Text);
                errorProvider1.SetError(H24Tb, "");
            }
            catch
            {
                errorProvider1.SetError(H24Tb, "Invalid Value!");
            }
        }
//------------------------------------SaveBtn_Click----------------------------
        private void SaveBtn_Click(object sender, EventArgs e)
        {
            if (FromDateCal.IsNull) FromValidate.Visible = true;
            if (ToDateCal.IsNull) ToValidate.Visible = true;
            if ((!FromDateCal.IsNull) && (!ToDateCal.IsNull) && (errorProvider1.GetError(H1Tb) == "") &&
                (errorProvider1.GetError(H2Tb) == "") && (errorProvider1.GetError(H3Tb) == "") && (errorProvider1.GetError(H4Tb) == "")
                && (errorProvider1.GetError(H5Tb) == "") && (errorProvider1.GetError(H6Tb) == "") && (errorProvider1.GetError(H7Tb) == "")
                && (errorProvider1.GetError(H8Tb) == "") && (errorProvider1.GetError(H9Tb) == "") && (errorProvider1.GetError(H10Tb) == "")
                && (errorProvider1.GetError(H11Tb) == "") && (errorProvider1.GetError(H12Tb) == "") && (errorProvider1.GetError(H13Tb) == "")
                && (errorProvider1.GetError(H14Tb) == "") && (errorProvider1.GetError(H15Tb) == "") && (errorProvider1.GetError(H16Tb) == "")
                && (errorProvider1.GetError(H17Tb) == "") && (errorProvider1.GetError(H18Tb) == "") && (errorProvider1.GetError(H19Tb) == "")
                && (errorProvider1.GetError(H20Tb) == "") && (errorProvider1.GetError(H21Tb) == "") && (errorProvider1.GetError(H22Tb) == "")
                && (errorProvider1.GetError(H23Tb) == "") && (errorProvider1.GetError(H24Tb) == ""))
            {
                FromValidate.Visible = false;
                ToValidate.Visible = false;
                //detect Today
                System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
                DateTime CurDate = DateTime.Now;
                int year = pr.GetYear(CurDate);
                string month = pr.GetMonth(CurDate).ToString();
                if ((int.Parse(month)) < 10) month = "0" + month;
                string day = pr.GetDayOfMonth(CurDate).ToString();
                if ((int.Parse(day)) < 10) day = "0" + day;
                string today = year.ToString() + "/" + month + "/" + day;
                
                SqlCommand MyCom = new SqlCommand();
                SqlConnection MyConnection = new SqlConnection(ConnectionManager.ConnectionString); 
                MyConnection.Open();
                MyCom.Connection = MyConnection;

                if (today != LastDate)
                    MyCom.CommandText = "INSERT INTO EfficiencyMarket (Date,Hour1,Hour2,Hour3,Hour4,Hour5," +
                    "Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18" +
                    ",Hour19,Hour20,Hour21,Hour22,Hour23,Hour24,FromDate,ToDate) VALUES (@date,@h1,@h2,@h3," +
                    "@h4,@h5,@h6,@h7,@h8,@h9,@h10,@h11,@h12,@h13,@h14,@h15,@h16,@h17,@h18,@h19,@h20,@h21,@h22" +
                    ",@h23,@h24,@from,@to)";
                else
                    MyCom.CommandText = "UPDATE EfficiencyMarket SET Hour1=@h1,Hour2=@h2,Hour3=@h3,Hour4=@h4" +
                    ",Hour5=@h5,Hour6=@h6,Hour7=@h7,Hour8=@h8,Hour9=@h9,Hour10=@h10,Hour11=@h11,Hour12=@h12,"+
                    "Hour13=@h13,Hour14=@h14,Hour15=@h15,Hour16=@h16,Hour17=@h17,Hour18=@h18,Hour19=@h19,"+
                    "Hour20=@h20,Hour21=@h21,Hour22=@h22,Hour23=@h23,Hour24=@h24,FromDate=@from,ToDate=@to"+
                    " WHERE Date=@date";

                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = today;
                MyCom.Parameters.Add("@h1", SqlDbType.Real);
                MyCom.Parameters["@h1"].Value = double.Parse(H1Tb.Text);
                MyCom.Parameters.Add("@h2", SqlDbType.Real);
                MyCom.Parameters["@h2"].Value = double.Parse(H2Tb.Text);
                MyCom.Parameters.Add("@h3", SqlDbType.Real);
                MyCom.Parameters["@h3"].Value = double.Parse(H3Tb.Text);
                MyCom.Parameters.Add("@h4", SqlDbType.Real);
                MyCom.Parameters["@h4"].Value = double.Parse(H4Tb.Text);
                MyCom.Parameters.Add("@h5", SqlDbType.Real);
                MyCom.Parameters["@h5"].Value = double.Parse(H5Tb.Text);
                MyCom.Parameters.Add("@h6", SqlDbType.Real);
                MyCom.Parameters["@h6"].Value = double.Parse(H6Tb.Text);
                MyCom.Parameters.Add("@h7", SqlDbType.Real);
                MyCom.Parameters["@h7"].Value = double.Parse(H7Tb.Text);
                MyCom.Parameters.Add("@h8", SqlDbType.Real);
                MyCom.Parameters["@h8"].Value = double.Parse(H8Tb.Text);
                MyCom.Parameters.Add("@h9", SqlDbType.Real);
                MyCom.Parameters["@h9"].Value = double.Parse(H9Tb.Text);
                MyCom.Parameters.Add("@h10", SqlDbType.Real);
                MyCom.Parameters["@h10"].Value = double.Parse(H10Tb.Text);
                MyCom.Parameters.Add("@h11", SqlDbType.Real);
                MyCom.Parameters["@h11"].Value = double.Parse(H11Tb.Text);
                MyCom.Parameters.Add("@h12", SqlDbType.Real);
                MyCom.Parameters["@h12"].Value = double.Parse(H12Tb.Text);
                MyCom.Parameters.Add("@h13", SqlDbType.Real);
                MyCom.Parameters["@h13"].Value = double.Parse(H13Tb.Text);
                MyCom.Parameters.Add("@h14", SqlDbType.Real);
                MyCom.Parameters["@h14"].Value = double.Parse(H14Tb.Text);
                MyCom.Parameters.Add("@h15", SqlDbType.Real);
                MyCom.Parameters["@h15"].Value = double.Parse(H15Tb.Text);
                MyCom.Parameters.Add("@h16", SqlDbType.Real);
                MyCom.Parameters["@h16"].Value = double.Parse(H16Tb.Text);
                MyCom.Parameters.Add("@h17", SqlDbType.Real);
                MyCom.Parameters["@h17"].Value = double.Parse(H17Tb.Text);
                MyCom.Parameters.Add("@h18", SqlDbType.Real);
                MyCom.Parameters["@h18"].Value = double.Parse(H18Tb.Text);
                MyCom.Parameters.Add("@h19", SqlDbType.Real);
                MyCom.Parameters["@h19"].Value = double.Parse(H19Tb.Text);
                MyCom.Parameters.Add("@h20", SqlDbType.Real);
                MyCom.Parameters["@h20"].Value = double.Parse(H20Tb.Text);
                MyCom.Parameters.Add("@h21", SqlDbType.Real);
                MyCom.Parameters["@h21"].Value = double.Parse(H21Tb.Text);
                MyCom.Parameters.Add("@h22", SqlDbType.Real);
                MyCom.Parameters["@h22"].Value = double.Parse(H22Tb.Text);
                MyCom.Parameters.Add("@h23", SqlDbType.Real);
                MyCom.Parameters["@h23"].Value = double.Parse(H23Tb.Text);
                MyCom.Parameters.Add("@h24", SqlDbType.Real);
                MyCom.Parameters["@h24"].Value = double.Parse(H24Tb.Text);
                MyCom.Parameters.Add("@from", SqlDbType.Char, 10);
                MyCom.Parameters["@from"].Value = FromDateCal.Text;
                MyCom.Parameters.Add("@to", SqlDbType.Char, 10);
                MyCom.Parameters["@to"].Value = ToDateCal.Text;

                try
                {
                    MyCom.ExecuteNonQuery();
                    this.Close();
                }
                catch 
                {
                }
                finally
                {
                    MyConnection.Close();
                }
            }
        }



    }
}
