﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    public partial class MarketBillPrint : Form
    {
        DataTable dt =null;
        string hed;
        string ppid;
        public MarketBillPrint(DataTable D,string header,string Pid)
        {
            InitializeComponent(); try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
            dt = D;
            hed = header;
            ppid = Pid;
        }

        private void MarketBillPrint_Load(object sender, EventArgs e)
        {

            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.DataSource = dt;

            //int d = x.Length;
           
            //dataGridView1.RowCount = x.Length;
            //for (int k = 0; k < d-1; k++)
            //{ 
            //    string[] sp = x[k].Split(';');
            //    dataGridView1.Rows[k].Cells[0].Value = sp[0];
            //    dataGridView1.Rows[k].Cells[1].Value = sp[1];
            //    dataGridView1.Rows[k].Cells[2].Value = sp[2];
            //    dataGridView1.Rows[k].Cells[3].Value = sp[3];
            //    double ddd = MyDoubleParse(sp[3].ToString()) - MyDoubleParse(sp[2].ToString());
            //    if (ddd <= 0) dataGridView1.Rows[k].Cells[4].Value = ddd;
            //    if (ddd > 0) dataGridView1.Rows[k].Cells[5].Value = ddd;


            //}

            
            ///////////////////////////////////////////////
            //dt = new DataTable();
            //DataColumn[] dsc = new DataColumn[] { };
            //foreach (DataGridViewColumn c in dataGridView1.Columns)
            //{
            //    DataColumn dc = new DataColumn();
            //    dc.ColumnName = c.Name.Trim();

            //    dt.Columns.Add(dc);
            //}
            //foreach (DataGridViewRow r in dataGridView1.Rows)
            //{
            //    DataRow drow = dt.NewRow();
            //    foreach (DataGridViewCell cell in r.Cells)
            //    {
            //        try
            //        {
            //            drow[cell.OwningColumn.Name] = cell.Value.ToString().Trim();
            //        }
            //        catch
            //        {
            //        }
            //    }
            //    dt.Rows.Add(drow);

            //}
        }
        private double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = dt;
            if (chkland.Checked)
            {
                PrintDGV.landscape = true;
            }
            PrintDGV.info("Monthly bill report for plant " + ppid + " Item " + hed);
            PrintDGV.Print_DataGridView(dataGridView1);
        }
    }
}
