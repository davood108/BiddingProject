﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dundas.Charting.WinControl;
using Dundas.Charting.WinControl.ChartTypes;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    public partial class FRPlot : Form
    {
        string PID, myDate;
        //public FRPlot(string PPID, string Date)
        //{
        //    PID = PPID;
        //    myDate = Date;
        //}

        public FRPlot(string PPID, string Date)
        {
            InitializeComponent();

            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
            PID = PPID;
            myDate = Date;

        }

 
        private void FRPlot_Load(object sender, EventArgs e)
        {

            label1.Text = " Plant :  " + PID + "  In  Date : "+ myDate;
            DataTable EconomicTable = null;
            string[] d = myDate.Split('-');

          
                EconomicTable = Utilities.GetTable("SELECT date, income ,Benefit   FROM [EconomicPlant] WHERE Date>='" + d[0].Trim() + "' AND date<='" + d[1].Trim() + "'and  PPID='" + PID + "'order by date asc");

                string[] Economic = new string[EconomicTable.Rows.Count * 2];
                string[] Economic2 = new string[EconomicTable.Rows.Count * 2];

             FRChart1.Titles.RemoveAt(0);
            FRChart1.Titles.Add("Daily Plot for Plant : " +PID );
            //FRChart1.Palette = ChartColorPalette.SeaGreen;
            //FRChart1.BackColor = Color.Khaki;

            FRChart1.Series["Series1"].Type = SeriesChartType.Column;
            FRChart1.Series["Series2"].Type = SeriesChartType.Column;
            FRChart1.Series["Series3"].Type = SeriesChartType.Column;

            FRChart1.ChartAreas["Default"].AxisY.Margin = true;
            FRChart1.ChartAreas["Default"].AxisY.Minimum = 0;

            int nn = 0;

           foreach(DataRow n in EconomicTable.Rows)
            {
              
                string ddate = n["date"].ToString().Trim();
                Economic[nn] = "" + ddate.Substring(2, 8).Replace("/","").Trim();
                nn++;
                Economic2[nn] = "benefit - " + ddate.Substring(2, 8).Replace("/","").Trim();

                FRChart1.Series["Series1"].Points.AddXY(Economic[nn - 1], n["income"].ToString());
                FRChart1.Series["Series2"].Points.AddXY(Economic2[nn], n["benefit"].ToString());
               // FRChart1.Series["Series3"].Points.AddXY("--", "0");
                           
                nn++;         
               
            }
         
          

            /////////////old version//////////////////////////////////////////////////////////////////
            //label1.Text = myDate;
            //DataTable EconomicTable = utilities.GetTable("SELECT  Income,Cost,CapacityPayment,EnergyPayment,BidPayment,IncrementPayment,DecreasePayment,ULPayment,AvailableCapacity,TotalPower,BidPower,ULPower,IncrementPower,DecreasePower FROM EconomicPlant WHERE PPID='" + PID + "' AND Date='" + myDate + "'");
            //if (EconomicTable.Rows.Count > 0)
            //{
            //    FRChart1.Titles.RemoveAt(0);
            //    FRChart1.Titles.Add("Daily Plot");
            //    FRChart2.Titles.RemoveAt(0);
            //    FRChart2.Titles.Add("Daily Plot");
            //    FRChart1.Palette = ChartColorPalette.SeaGreen;
            //    FRChart1.BackColor = Color.Khaki;

            //    FRChart2.Palette = ChartColorPalette.SeaGreen;
            //    FRChart2.BackColor = Color.Khaki;

            //    FRChart1.Series["Series1"].Type = SeriesChartType.Column;

            //    FRChart1.ChartAreas["Default"].AxisY.Margin = true;
            //    FRChart1.ChartAreas["Default"].AxisY.Minimum = 0;
            //    double max1 = 0.0;
            //    for (int i = 0; i < 8; i++)
            //        if (double.Parse(EconomicTable.Rows[0][i].ToString()) > max1) max1 = double.Parse(EconomicTable.Rows[0][i].ToString());
            //    FRChart1.ChartAreas["Default"].AxisY.Maximum = max1 + 50.0;


            //    FRChart2.Series["Series1"].Type = SeriesChartType.Column;

            //    FRChart2.ChartAreas["Default"].AxisY.Margin = true;
            //    FRChart2.ChartAreas["Default"].AxisY.Minimum = 0;
            //    double max2 = 0.0;
            //    for (int i = 8; i < 14; i++)
            //        if (double.Parse(EconomicTable.Rows[0][i].ToString()) > max2) max2 = double.Parse(EconomicTable.Rows[0][i].ToString());
            //    FRChart2.ChartAreas["Default"].AxisY.Maximum = max2 + 50.0;

            //    string[] Economic = { "Income", "Cost", "CapacityPayment", "EnergyPayment", "BidPayment", "IncrementPayment", "DecreasePayment", "ULPayment","AvailableCapacity", "TotalPower", "BidPower", "ULPower", "IncrementPower", "DecreasePower" };

            //    for (int i = 0; i < 8; i++)
            //        FRChart1.Series["Series1"].Points.AddXY(Economic[i], EconomicTable.Rows[0][i].ToString());
            //    for (int i = 8; i < 14; i++)
            //        FRChart2.Series["Series1"].Points.AddXY(Economic[i], EconomicTable.Rows[0][i].ToString());
            //}
            /////////////////////////////////////////////////////////////////////////////////////////
        }

        private void DailyRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (DailyRadio.Checked)
            {
                DataTable EconomicTable = Utilities.GetTable("SELECT  Income,Cost,CapacityPayment,EnergyPayment,BidPayment,IncrementPayment,DecreasePayment,ULPayment,AvailableCapacity,TotalPower,BidPower,ULPower,IncrementPower,DecreasePower FROM EconomicPlant WHERE PPID='" + PID + "' AND Date='" + myDate + "'");
                if (EconomicTable.Rows.Count > 0)
                {

                    FRChart1.Titles.RemoveAt(0);
                    FRChart1.Titles.Add("Daily Plot");
                    FRChart2.Titles.RemoveAt(0);
                    FRChart2.Titles.Add("Daily Plot");

                    //FRChart1.Series.RemoveAt(FRChart1.Series.Count - 1);
                    FRChart1.Series.Clear();

                    FRChart1.Series.Add("Series1");
                    //begin
                    FRChart1.Legends.Clear();
                    FRChart1.Series["Series1"].Points.Clear();
                    FRChart1.ChartAreas["Default"].AxisY.Margin = true;
                    FRChart1.ChartAreas["Default"].AxisY.Minimum = 0;
                    double max1 = 0.0;
                    for (int i = 0; i < 8; i++)
                        if (double.Parse(EconomicTable.Rows[0][i].ToString()) > max1) max1 = double.Parse(EconomicTable.Rows[0][i].ToString());
                    FRChart1.ChartAreas["Default"].AxisY.Maximum = max1 + 50.0;

                    //end

                    FRChart1.Palette = ChartColorPalette.SeaGreen;


                    //FRChart2.Series.RemoveAt(FRChart2.Series.Count - 1);
                    FRChart2.Series.Clear();

                    FRChart2.Series.Add("Series1");
                    //begin
                    FRChart2.Legends.Clear();
                    FRChart2.Series["Series1"].Points.Clear();
                    FRChart2.ChartAreas["Default"].AxisY.Margin = true;
                    FRChart2.ChartAreas["Default"].AxisY.Minimum = 0;
                    double max2 = 0.0;
                    for (int i = 8; i < 14; i++)
                        if (double.Parse(EconomicTable.Rows[0][i].ToString()) > max2) max2 = double.Parse(EconomicTable.Rows[0][i].ToString());
                    FRChart2.ChartAreas["Default"].AxisY.Maximum = max2 + 50.0;
                    //end

                    FRChart2.Palette = ChartColorPalette.SeaGreen;


                    FRChart1.Series["Series1"].BackGradientEndColor = Color.LightSteelBlue;
                    FRChart1.Series["Series1"].BackGradientType = GradientType.DiagonalLeft;
                    // FRChart1.Series["Series1"].BorderLineColor = Color.LightSlateGray;
                    FRChart1.Series["Series1"].BorderColor = Color.SteelBlue;
                    // FRChart1.Series["Series1"].BackColor = Color.White;

                    FRChart2.Series["Series1"].BackGradientEndColor = Color.LightSteelBlue;
                    FRChart2.Series["Series1"].BackGradientType = GradientType.DiagonalLeft;
                    // FRChart2.Series["Series1"].BorderLineColor = Color.LightSlateGray;
                    FRChart2.Series["Series1"].BorderColor = Color.SteelBlue;
                    // FRChart2.Series["Series1"].BackColor = Color.White;

                    FRChart1.Series["Series1"].Type = SeriesChartType.Column;
                    FRChart2.Series["Series1"].Type = SeriesChartType.Column;

                    FRChart1.ChartAreas["Default"].AxisX.Margin = true;
                    FRChart2.ChartAreas["Default"].AxisX.Margin = true;
                    FRChart1.ChartAreas["Default"].AxisY.Margin = true;
                    FRChart2.ChartAreas["Default"].AxisY.Margin = true;

                    string[] Economic = { "Income", "Cost", "CapacityPayment", "EnergyPayment", "BidPayment", "IncrementPayment", "DecreasePayment", "ULPayment", "AvailableCapacity", "TotalPower", "BidPower", "ULPower", "IncrementPower", "DecreasePower" };

                    for (int i = 0; i < 8; i++)
                        FRChart1.Series["Series1"].Points.AddXY(Economic[i], EconomicTable.Rows[0][i].ToString());
                    for (int i = 8; i < 14; i++)
                        FRChart2.Series["Series1"].Points.AddXY(Economic[i], EconomicTable.Rows[0][i].ToString());
                }
                else
                {
                    FRChart1.Series.Clear();
                    FRChart2.Series.Clear();
                }
                   
            }
            else if (MonthlyRadio.Checked)
            {
                string newdate = myDate.Remove(7);
                DataTable EconomicTable = Utilities.GetTable("SELECT  SUM(Income),SUM(Cost),SUM(CapacityPayment),SUM(EnergyPayment),SUM(BidPayment),SUM(IncrementPayment),SUM(DecreasePayment),SUM(ULPayment), SUM(AvailableCapacity),SUM(TotalPower),SUM(BidPower),SUM(ULPower),SUM(IncrementPower),SUM(DecreasePower) FROM EconomicPlant WHERE PPID='" + PID + "' AND Date like '" + newdate + "%'");
                if (EconomicTable.Rows.Count > 0 && EconomicTable.Rows[0][0].ToString() != "" && EconomicTable.Rows[0][1].ToString() != "" && EconomicTable.Rows[0][2].ToString() != "" && EconomicTable.Rows[0][3].ToString() != "" && EconomicTable.Rows[0][4].ToString() != ""
                    && EconomicTable.Rows[0][5].ToString() != "" && EconomicTable.Rows[0][6].ToString() != "" && EconomicTable.Rows[0][7].ToString() != "" && EconomicTable.Rows[0][8].ToString() != "" && EconomicTable.Rows[0][9].ToString() != ""
                    && EconomicTable.Rows[0][10].ToString() != "" && EconomicTable.Rows[0][11].ToString() != "" && EconomicTable.Rows[0][12].ToString() != "" && EconomicTable.Rows[0][13].ToString() != "")
                {
                    FRChart1.Titles.RemoveAt(0);
                    FRChart1.Titles.Add("Monthly Plot");
                    FRChart2.Titles.RemoveAt(0);
                    FRChart2.Titles.Add("Monthly Plot");

                    //FRChart1.Series.RemoveAt(FRChart1.Series.Count - 1);
                    //begin
                    FRChart1.Series.Clear();
                    //end
                    FRChart1.Series.Add("Series1");
                    //begin
                    FRChart1.Legends.Clear();
                    FRChart1.Series["Series1"].Points.Clear();
                    FRChart1.ChartAreas["Default"].AxisY.Margin = true;
                    FRChart1.ChartAreas["Default"].AxisY.Minimum = 0;
                    double max1 = 0.0;
                    for (int i = 0; i < 8; i++)
                        if (double.Parse(EconomicTable.Rows[0][i].ToString()) > max1) max1 = double.Parse(EconomicTable.Rows[0][i].ToString());
                    FRChart1.ChartAreas["Default"].AxisY.Maximum = max1 + 50.0;

                    //end
                    //FRChart2.Series.RemoveAt(FRChart2.Series.Count - 1);
                    //begin
                    FRChart2.Series.Clear();
                    //end

                    FRChart2.Series.Add("Series1");

                    //begin
                    FRChart2.Legends.Clear();
                    FRChart2.Series["Series1"].Points.Clear();
                    FRChart2.ChartAreas["Default"].AxisY.Margin = true;
                    FRChart2.ChartAreas["Default"].AxisY.Minimum = 0;
                    double max2 = 0.0;
                    for (int i = 8; i < 14; i++)
                        if (double.Parse(EconomicTable.Rows[0][i].ToString()) > max2) max2 = double.Parse(EconomicTable.Rows[0][i].ToString());
                    FRChart2.ChartAreas["Default"].AxisY.Maximum = max2 + 50.0;

                    //end

                    FRChart2.Palette = ChartColorPalette.SeaGreen;
                    FRChart2.BackGradientEndColor = Color.LightSteelBlue;

                    FRChart1.Series["Series1"].BackGradientEndColor = Color.LightSteelBlue;
                    FRChart1.Series["Series1"].BackGradientType = GradientType.DiagonalLeft;
                    // FRChart1.Series["Series1"].BorderLineColor = Color.LightSlateGray;
                    FRChart1.Series["Series1"].BorderColor = Color.SteelBlue;
                    // FRChart1.Series["Series1"].BackColor = Color.White;

                    FRChart2.Series["Series1"].BackGradientEndColor = Color.LightSteelBlue;
                    FRChart2.Series["Series1"].BackGradientType = GradientType.DiagonalLeft;
                    // FRChart2.Series["Series1"].BorderLineColor = Color.LightSlateGray;
                    FRChart2.Series["Series1"].BorderColor = Color.SteelBlue;
                    // FRChart2.Series["Series1"].BackColor = Color.White;


                    FRChart1.Series["Series1"].Type = SeriesChartType.Column;
                    FRChart2.Series["Series1"].Type = SeriesChartType.Column;

                    //FRChart1.ChartAreas["Default"].AxisX.Margin = true;
                    //FRChart2.ChartAreas["Default"].AxisX.Margin = true;
                    //FRChart1.ChartAreas["Default"].AxisY.Margin = true;
                    //FRChart2.ChartAreas["Default"].AxisY.Margin = true;


                    string[] Economic = { "Income", "Cost", "CapacityPayment", "EnergyPayment", "BidPayment", "IncrementPayment", "DecreasePayment", "ULPayment", "AvailableCapacity", "TotalPower", "BidPower", "ULPower", "IncrementPower", "DecreasePower" };

                    for (int i = 0; i < 8; i++)
                        FRChart1.Series["Series1"].Points.AddXY(Economic[i], EconomicTable.Rows[0][i].ToString());
                    for (int i = 8; i < 14; i++)
                        FRChart2.Series["Series1"].Points.AddXY(Economic[i], EconomicTable.Rows[0][i].ToString());
                }
                else
                {
                    FRChart1.Series.Clear();
                    FRChart2.Series.Clear();
                }
            }
        }
    }
}
