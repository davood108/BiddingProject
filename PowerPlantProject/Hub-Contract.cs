﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using System.IO;
using Microsoft.Win32;

namespace PowerPlantProject
{
    public partial class Hub_Contract : Form
    {
        string status = "";
        public Hub_Contract()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dataGridView2.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void Hub_Contract_Load(object sender, EventArgs e)
        {
            textBox1.Text = "";
            this.Text = "PowerSetting";
            //dataGridView2.Columns[0].Width = 17;
            //dataGridView2.Columns[0].HeaderText = "";
            datePickerFrom.Text = new PersianDate(DateTime.Now).ToString("d");
            datePickerto.Text = new PersianDate(DateTime.Now.AddDays(1)).ToString("d");
            DataTable c = Utilities.GetTable("select ppid from powerplant");
            cmbplant.Text = c.Rows[0][0].ToString();
            foreach (DataRow n in c.Rows)
            {
                cmbplant.Items.Add(n[0].ToString().Trim());

            }
            DataTable v = Utilities.GetTable("select unitcode from UnitsDataMain where ppid='" + cmbplant.Text.Trim() + "'order by unitcode asc");
            foreach (DataRow x in v.Rows)
            {


                string unit = x[0].ToString().Trim();
                string temp = unit;
                string package = "";
     

                //Build the Bolck for FRM002

                string ptypenum = "0";
                if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";
                //if (PPID == "232") ptypenum = "0";
                if (Findcconetype(cmbplant.Text.Trim())) ptypenum = "0";

                temp = temp.ToLower();
                if (temp.Contains("cc"))
                {
                    string packagecode = "";
              
                    // temp = "C" + packagecode;

                    //ccunitbase///////////////////////////////////////////
                    //temp = x + "-" + "C" + packagecode;
                    temp = temp.Replace("cc", "c");
                    string[] sp = temp.Split('c');
                    temp = sp[0].Trim() + sp[1].Trim();
                    if (temp.Contains("gas"))
                    {
                        temp = temp.Replace("gas", "G");

                    }
                    else
                    {
                        temp = temp.Replace("steam", "S");

                    }

                }
                else
                {
                    if (temp.Contains("gas"))
                        temp = temp.Replace("gas", "G");
                    else if (temp.Contains("steam"))
                        temp = temp.Replace("steam", "S");
                }
                temp = temp.Trim();

                cmbunit.Items.Add(temp);

            }
            cmbunit.Text = "All Units";
            cmbunit.Items.Add("All Units");

            cmbfhour.Text = "1";
            cmbthour.Text = "24";
            try
            {
                fillallunit();
            }
            catch
            {
            }
        }


        public void fillallunit()
        {
            try
            {
                dataGridView2.DataSource = null;
                DataTable dd = Utilities.GetTable("select type,PPID,unit,fromdate,todate,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15,h16,h17,h18,h19,h20,h21,h22,h23,h24 from powersetting where type='" + status + "' order by PPID,unit asc");
                dataGridView2.DataSource = dd;
            }
            catch
            {
            }
        }
        public void fill()
        {
            
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                double c = MyDoubleParse(textBox1.Text);
            }
            catch
            {
                textBox1.Text = "";
            }
            int fromh = int.Parse(cmbfhour.Text.Trim());
            int thour = int.Parse(cmbthour.Text.Trim());
           
            if (rdMustNet.Checked) status = "Contract";
            else if (rdmustrun.Checked) status = "Exchange";
            else if (rdMustOff.Checked) status = "Hub";

            if (status != "")
            {
                for (int h = fromh; h <= thour; h++)
                {
                    dataGridView1.Rows[0].Cells[h - 1].Value = textBox1.Text ;

                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            //try
            //{
            //    DataTable d = utilities.GetTable("delete from powersetting where fromdate='" + datePickerFrom.Text.Trim() + "'and todate='" + datePickerto.Text.Trim() + "' and unit='" + cmbunit.Text.Trim() + "'and ppid='" + cmbplant.Text.Trim() + "'and type='" + status + "'");
            //    fill();
            //}
            //catch
            //{

            //}
        }
        public static bool Findcconetype(string ppid)
        {
            int tr = 0;

            DataTable oDataTable = Utilities.GetTable("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count == 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }
        private void button3_Click(object sender, EventArgs e)
        {

            TimeSpan span = PersianDateConverter.ToGregorianDateTime(datePickerto.Text) - PersianDateConverter.ToGregorianDateTime(datePickerFrom.Text);
            int dday = span.Days;
            if (dday < 0)
            {
                MessageBox.Show("Please Select Correct Interval Date!");
                return;
            }

            try
            {

                if (cmbunit.Text == "All Units" && textBox1.Text != "" && cmbunit.Text != "")
                {
                    ///////////////////////////////////////////////////////
                    for (int yn = 0; yn < cmbunit.Items.Count - 1; yn++)
                    {
                        string uname = cmbunit.Items[yn].ToString();
                        string st1 = "insert into powersetting(ppid,unit,type,fromdate,todate,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15,h16,h17,h18,h19,h20,h21,h22,h23,h24)values('" + cmbplant.Text.Trim() + "','" + uname + "','" + status + "','" + datePickerFrom.Text.Trim() + "','" + datePickerto.Text.Trim();
                        DataTable d = Utilities.GetTable("delete from powersetting where unit='" + uname + "'and ppid='" + cmbplant.Text.Trim() + "'and type='" + status + "'and fromdate='" + datePickerFrom.Text + "'and todate='" + datePickerto.Text + "'");
                        string st2 = "";
                        string v = "";
                        for (int i = 0; i < 24; i++)
                        {
                            try
                            {
                                v = dataGridView1.Rows[0].Cells[i].Value.ToString().Trim();
                            }
                            catch
                            {
                                v = "";
                            }


                            st2 += ("','" + v);
                            if (i == 23) st2 += "')";
                        }
                        DataTable ind = Utilities.GetTable(st1 + st2);

                    }
                    MessageBox.Show("Saved.");
                    fillallunit();
                    fill();
                    /////////////////////////////////////////////////////////////
                }


                else  if (textBox1.Text != "" && cmbunit.Text != "")
                {
                    string st1 = "insert into powersetting(ppid,unit,type,fromdate,todate,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15,h16,h17,h18,h19,h20,h21,h22,h23,h24)values('" + cmbplant.Text.Trim() + "','" + cmbunit.Text.Trim() + "','" + status + "','" + datePickerFrom.Text.Trim() + "','" + datePickerto.Text.Trim();
                    DataTable d = Utilities.GetTable("delete from powersetting where unit='" + cmbunit.Text.Trim() + "'and ppid='" + cmbplant.Text.Trim() + "'and type='" + status + "'and fromdate='"+datePickerFrom.Text+"'and todate='"+datePickerto.Text+"'");
                    string st2 = "";
                    string v = "";
                    for (int i = 0; i < 24; i++)
                    {
                        try
                        {
                            v = dataGridView1.Rows[0].Cells[i].Value.ToString().Trim();
                        }
                        catch
                        {
                            v = "";
                        }


                        st2 += ("','" + v);
                        if (i == 23) st2 += "')";
                    }
                    DataTable ind = Utilities.GetTable(st1 + st2);
                    MessageBox.Show("Saved.");
                    fillallunit();
                    fill();
                }
                else
                {
                    MessageBox.Show("Fill Completely.");
                }
            }
            catch
            {

            }

        }
        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "" || str.Trim() == null)
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }

        private void rdmustrun_CheckedChanged(object sender, EventArgs e)
        {
            if (rdMustNet.Checked) status = "Contract";
            else if (rdmustrun.Checked) status = "Exchange";
            else if (rdMustOff.Checked) status = "Hub";
            fillallunit();
        }

        private void rdMustOff_CheckedChanged(object sender, EventArgs e)
        {
            if (rdMustNet.Checked) status = "Contract";
            else if (rdmustrun.Checked) status = "Exchange";
            else if (rdMustOff.Checked) status = "Hub";
            fillallunit();
        }

        private void rdMustNet_CheckedChanged(object sender, EventArgs e)
        {
            if (rdMustNet.Checked) status = "Contract";
            else if (rdmustrun.Checked) status = "Exchange";
            else if (rdMustOff.Checked) status = "Hub";
            fillallunit();
        }

       
        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;

            string x = dataGridView2.Rows[rowIndex].Cells[e.ColumnIndex].Value.ToString();
            if (x == "System.Drawing.Bitmap")
            {
                DataTable bb = Utilities.GetTable("delete from powersetting where ppid='" + dataGridView2.Rows[rowIndex].Cells[2].Value.ToString() + "' and  unit='" + dataGridView2.Rows[rowIndex].Cells[3].Value.ToString() + "'and fromdate='" + dataGridView2.Rows[rowIndex].Cells[4].Value.ToString() + "'and todate='" + dataGridView2.Rows[rowIndex].Cells[5].Value.ToString() + "'");
            }
            fillallunit();
        }

        private void cmbplant_SelectedValueChanged(object sender, EventArgs e)
        {
            cmbunit.Items.Clear();
            DataTable v = Utilities.GetTable("select unitcode from UnitsDataMain where ppid='" + cmbplant.Text.Trim() + "'order by unitcode asc");
            foreach (DataRow x in v.Rows)
            {


                string unit = x[0].ToString().Trim();
                string temp = unit;
                string package = "";


                //Build the Bolck for FRM002

                string ptypenum = "0";
                if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";
                //if (PPID == "232") ptypenum = "0";
                if (Findcconetype(cmbplant.Text.Trim())) ptypenum = "0";

                temp = temp.ToLower();
                if (temp.Contains("cc"))
                {
                    string packagecode = "";

                    // temp = "C" + packagecode;

                    //ccunitbase///////////////////////////////////////////
                    //temp = x + "-" + "C" + packagecode;
                    temp = temp.Replace("cc", "c");
                    string[] sp = temp.Split('c');
                    temp = sp[0].Trim() + sp[1].Trim();
                    if (temp.Contains("gas"))
                    {
                        temp = temp.Replace("gas", "G");

                    }
                    else
                    {
                        temp = temp.Replace("steam", "S");

                    }

                }
                else
                {
                    if (temp.Contains("gas"))
                        temp = temp.Replace("gas", "G");
                    else if (temp.Contains("steam"))
                        temp = temp.Replace("steam", "S");
                }
                temp = temp.Trim();

                cmbunit.Items.Add(temp);

            }
            cmbunit.Text = "All Units";
            cmbunit.Items.Add("All Units");

            fillallunit();

        }

       
       
    }
}
