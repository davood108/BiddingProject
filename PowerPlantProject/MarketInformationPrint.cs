﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PowerPlantProject
{
    public partial class MarketInformationPrint : Form
    {
        DataTable Dg1 = null;
        string pname;
        string Date;

        public MarketInformationPrint(DataTable dg1,string name, string DATE)
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dgv.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
            Dg1 = dg1;
            pname = name;
            Date = DATE;
        }

       

        private void btnPrint_Click(object sender, EventArgs e)
        {
           
            dgv.DataSource = Dg1;
            if (chkland.Checked)
            {
                PrintDGV.landscape = true;
            }
            PrintDGV.info(label1.Text);
            PrintDGV.Print_DataGridView(dgv);
        }

        private void MarketInformationPrint_Load(object sender, EventArgs e)
        {
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgv.AllowUserToDeleteRows = false;
            label1.Text = pname + "    In Date :  " + Date;

            dgv.DataSource = Dg1;
        }
    }
}
