﻿namespace PowerPlantProject
{
    partial class EconomicPlantDailyReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.EconomicPlantDailycrystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            //this.EconomicPlantDailyCrystalReportDoc1 = new PowerPlantProject.EconomicPlantDailyCrystalReport();
            this.SuspendLayout();
            // 
            // EconomicPlantDailycrystalReportViewer1
            // 
            //this.EconomicPlantDailycrystalReportViewer1.ActiveViewIndex = -1;
            //this.EconomicPlantDailycrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            //this.EconomicPlantDailycrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            //this.EconomicPlantDailycrystalReportViewer1.Location = new System.Drawing.Point(0, 0);
            //this.EconomicPlantDailycrystalReportViewer1.Name = "EconomicPlantDailycrystalReportViewer1";
            //this.EconomicPlantDailycrystalReportViewer1.SelectionFormula = "";
            //this.EconomicPlantDailycrystalReportViewer1.Size = new System.Drawing.Size(292, 266);
            //this.EconomicPlantDailycrystalReportViewer1.TabIndex = 0;
            //this.EconomicPlantDailycrystalReportViewer1.ViewTimeSelectionFormula = "";
            //this.EconomicPlantDailycrystalReportViewer1.Load += new System.EventHandler(this.EconomicPlantDailycrystalReportViewer1_Load);
            //// 
            // EconomicPlantDailyReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
           // this.Controls.Add(this.EconomicPlantDailycrystalReportViewer1);
            this.Name = "EconomicPlantDailyReportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EconomicPlantDailyReportForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

       // private CrystalDecisions.Windows.Forms.CrystalReportViewer EconomicPlantDailycrystalReportViewer1;
      //  private EconomicPlantDailyCrystalReport EconomicPlantDailyCrystalReportDoc1;
    }
}