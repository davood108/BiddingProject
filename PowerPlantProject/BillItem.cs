﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using NRI.SBS.Common;
using FarsiLibrary.Utils;
using System.Collections;
using System.IO;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using Auto_Update_Library;

namespace PowerPlantProject
{
    public partial class BillItem : Form
    {
        public BillItem()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void BillItem_Load(object sender, EventArgs e)
        {
        

            datePickerFrom.Text = new PersianDate(DateTime.Now).ToString("d");
            fillcheck();

        }
        public void fillcheck()
        {
            this.checkedListBox1.Items.Clear();
            DataTable bb = Utilities.GetTable("select distinct code,item from dbo.MonthlyBillTotal where month like'%"+datePickerFrom.Text.Substring(0,7).Trim()+"%' order by code asc");
            //if (bb.Rows.Count == 0)
            //{
            //    bb = utilities.GetTable("select distinct code,item from dbo.MonthlyBillTotal where month in (select max(month) from MonthlyBillTotal) order by code asc");

            //}
            if (bb.Rows.Count == 0) return;
            for (int y = 0; y < bb.Rows.Count; y++)
            {
                string nn = bb.Rows[y][0].ToString().Trim();
                if (nn.Contains("01") || nn.Contains("02") || nn.Contains("03") || nn.Contains("04") || nn.Contains("05") || nn.Contains("06") || nn.Contains("07") || nn.Contains("08") || nn.Contains("09"))
                    this.checkedListBox1.Items.Add(bb.Rows[y][0].ToString().Replace("0", "").Trim() + "=" + bb.Rows[y][1].ToString().Trim());
                else this.checkedListBox1.Items.Add(bb.Rows[y][0].ToString().Trim() + "=" + bb.Rows[y][1].ToString().Trim());
            }

            for (int b = 0; b < checkedListBox1.Items.Count; b++)
            {
                this.checkedListBox1.SetItemChecked(b, true);
            }


            DataTable d = Utilities.GetTable("select * from billitem where  date like'%"+datePickerFrom.Text.Substring(0,7).Trim()+"%'");
            //if (d.Rows.Count == 0)
            //{
            //    d = utilities.GetTable("select * from billitem where  date in ( select max(date) from billitem ) ");

            //}
            if (d.Rows.Count == 0)
            {
                for (int b = 0; b < checkedListBox1.Items.Count; b++)
                {
                    this.checkedListBox1.SetItemChecked(b,false);
                }
                return;

            }

            try
            {
                for (int indexChecked = 0; indexChecked < checkedListBox1.Items.Count; indexChecked++)
                {

                    string c = checkedListBox1.Items[indexChecked].ToString();
                    string[] v = c.Split('=');

                    if (v[0].Trim().ToLower() == d.Columns[indexChecked + 1].ToString().ToLower())
                    {
                        if (d.Rows[0][indexChecked+1].ToString().Trim() == "1" || d.Rows[0][indexChecked+1].ToString().Trim() == "True")
                            checkedListBox1.SetItemChecked(indexChecked, true);
                        else checkedListBox1.SetItemChecked(indexChecked, false);

                        if(v[0].Trim().ToLower() =="s15")
                            checkedListBox1.SetItemChecked(indexChecked, true);
                    }
                }
            }
            catch
            {

            }

        }
        private void button1_Click(object sender, EventArgs e)
        {
            DataTable s = Utilities.GetTable("delete from billitem where  date like'%"+datePickerFrom.Text.Substring(0,7).Trim()+"%'");
            int i = 0;
            for (int indexChecked = 0;  indexChecked < checkedListBox1.CheckedItems.Count; indexChecked++)
            {

                string c = checkedListBox1.CheckedItems[indexChecked].ToString();

                string[] v = c.Split('=');
                if (i == 0)
                {
                    s = Utilities.GetTable("insert into billitem (" + v[0].Trim() + ",date)values('1','"+datePickerFrom.Text.Trim()+"')");
                }
                else
                {
                    s = Utilities.GetTable("update billitem  set " + v[0].Trim() + " ='1'where date='" + datePickerFrom.Text.Trim() + "'");
                }
                i++;


            }


            MessageBox.Show("Saved.");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BillName n = new BillName(datePickerFrom.Text.Trim());
            n.Show();
        }

        private void datePickerFrom_ValueChanged(object sender, EventArgs e)
        {
            fillcheck();
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            string xx = datePickerFrom.Text.Trim();
            this.Close();
            BillName m = new BillName(xx);           
            m.panel1.Visible = true;
            m.Show();
        }

       

       
    }
}
