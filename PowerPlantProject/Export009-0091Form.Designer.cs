﻿namespace PowerPlantProject
{
    partial class Export009_0091Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Export009_0091Form));
            this.comboplant = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.exportdate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnexport = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboplant
            // 
            this.comboplant.FormattingEnabled = true;
            this.comboplant.Location = new System.Drawing.Point(237, 27);
            this.comboplant.Name = "comboplant";
            this.comboplant.Size = new System.Drawing.Size(121, 21);
            this.comboplant.TabIndex = 0;
            this.comboplant.SelectedIndexChanged += new System.EventHandler(this.comboplant_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(9, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Date :";
            // 
            // exportdate
            // 
            this.exportdate.HasButtons = true;
            this.exportdate.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.exportdate.Location = new System.Drawing.Point(54, 28);
            this.exportdate.Name = "exportdate";
            this.exportdate.Size = new System.Drawing.Size(116, 20);
            this.exportdate.TabIndex = 17;
            this.exportdate.ValueChanged += new System.EventHandler(this.exportdate_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(193, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "plant :";
            // 
            // btnexport
            // 
            this.btnexport.BackColor = System.Drawing.Color.CadetBlue;
            this.btnexport.BackgroundImage = global::PowerPlantProject.Properties.Resources.officeexcel2;
            this.btnexport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnexport.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnexport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnexport.Location = new System.Drawing.Point(137, 73);
            this.btnexport.Name = "btnexport";
            this.btnexport.Size = new System.Drawing.Size(92, 28);
            this.btnexport.TabIndex = 19;
            this.btnexport.Text = "Export";
            this.btnexport.UseVisualStyleBackColor = false;
            this.btnexport.Click += new System.EventHandler(this.btnexport_Click);
            // 
            // Export009_0091Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(370, 113);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnexport);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.exportdate);
            this.Controls.Add(this.comboplant);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Export009_0091Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Export 009_91Form";
            this.Load += new System.EventHandler(this.Export009_0091Form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboplant;
        private System.Windows.Forms.Label label1;
        private FarsiLibrary.Win.Controls.FADatePicker exportdate;
        private System.Windows.Forms.Button btnexport;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}