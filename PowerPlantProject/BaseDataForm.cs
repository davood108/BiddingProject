﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using NRI.SBS.Common;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;

namespace PowerPlantProject
{
    public partial class BaseDataForm : Form
    {
        string ConStr;
        public BaseDataForm()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dataGridView2.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox ||  c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////

            }
            catch
            {

            }
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            
            //DataTable PlantTable = utilities.GetTable("select distinct PPName from dbo.PowerPlant inner join dbo.PPUnit on dbo.PowerPlant.PPID=dbo.PPUnit.PPID where dbo.PPUnit.PackageType!='Gas'");
             DataTable PlantTable = Utilities.GetTable("select  PowerPlant.PPName, PPUnit.PackageType from dbo.PowerPlant inner join dbo.PPUnit on dbo.PowerPlant.PPID=dbo.PPUnit.PPID");
           
            for (int i = 0; i < PlantTable.Rows.Count; i++)
                comboSelectPlant.Items.Add(PlantTable.Rows[i][0].ToString().Trim() + "-" + PlantTable.Rows[i][1].ToString().Trim());
            UpdateCal.SelectedDateTime = System.DateTime.Now;
            loaddata();
            //UpdateCal.IsNull = true;

        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            if ((!UpdateCal.IsNull) && (errorProvider1.GetError(PmaxTb) == "") && (errorProvider1.GetError(PminTb) == "") &&
            (errorProvider1.GetError(GasTb) == "") && (errorProvider1.GetError(SteamTb) == "") &&
            (errorProvider1.GetError(CCTb) == "") && (errorProvider1.GetError(GasPriceTb) == "") &&
            (errorProvider1.GetError(MazutPriceTb) == "") && (errorProvider1.GetError(GasOilPriceTb) == "")&&
            (errorProvider1.GetError(LineEnergyPaymentTB) == "")&& (errorProvider1.GetError(LineCapacityPaymentTB) == "")&&
            (errorProvider1.GetError(GasSubTB) == "") && (errorProvider1.GetError(MazutSubTB) == "")&&
            (errorProvider1.GetError(GasoilSubTB) == "") && 
            (errorProvider1.GetError(txtdownerror)=="") && (errorProvider1.GetError(txtuperror)==""))
            {
                SqlCommand MyCom = new SqlCommand();
                MyCom.CommandText = "INSERT INTO [BaseData] (Date,MarketPriceMax,MarketPriceMin,CapacityPayment,"+
                "ProposalHour,ProposalDay,GasProduction,SteamProduction,CCProduction,GasPrice,MazutPrice,"+
                "GasOilPrice,LineEnergyPayment,LineCapacityPayment,GasSubsidiesPrice,MazutSubsidiesPrice,"+
                "GasOilSubsidiesPrice,GasHeatValueAverage,GasOilHeatValueAverage,PPName,reference,transmission) VALUES (@date,@Pmax,@Pmin,@cap,@status,@day,@gas,@steam,@cc,@gp,@mp,@gop" +
                ",@lep,@lcp,@gsub,@msub,@gosub,@gasheatavg,@oilheatavg,@plant,@ref,@trans)";
                MyCom.Connection = new SqlConnection(ConnectionManager.ConnectionString);
                MyCom.Connection.Open();

                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = UpdateCal.Text;
                MyCom.Parameters.Add("@Pmax", SqlDbType.Real);
                MyCom.Parameters["@Pmax"].Value = double.Parse(PmaxTb.Text);
                MyCom.Parameters.Add("@Pmin", SqlDbType.Real);
                MyCom.Parameters["@Pmin"].Value = double.Parse(PminTb.Text);
                MyCom.Parameters.Add("@gas", SqlDbType.Real);
                MyCom.Parameters["@gas"].Value = double.Parse(GasTb.Text);
                MyCom.Parameters.Add("@steam", SqlDbType.Real);
                MyCom.Parameters["@steam"].Value = double.Parse(SteamTb.Text);
                MyCom.Parameters.Add("@cc", SqlDbType.Real);
                MyCom.Parameters["@cc"].Value = double.Parse(CCTb.Text);
                MyCom.Parameters.Add("@gp", SqlDbType.Real);
                MyCom.Parameters["@gp"].Value = double.Parse(GasPriceTb.Text);
                MyCom.Parameters.Add("@mp", SqlDbType.Real);
                MyCom.Parameters["@mp"].Value = double.Parse(MazutPriceTb.Text);
                MyCom.Parameters.Add("@gop", SqlDbType.Real);
                MyCom.Parameters["@gop"].Value = double.Parse(GasOilPriceTb.Text);
                MyCom.Parameters.Add("@lep", SqlDbType.Real);
                MyCom.Parameters["@lep"].Value = double.Parse(LineEnergyPaymentTB.Text);
                MyCom.Parameters.Add("@lcp", SqlDbType.Real);
                MyCom.Parameters["@lcp"].Value = double.Parse(LineCapacityPaymentTB.Text);
                MyCom.Parameters.Add("@gsub", SqlDbType.Real);
                MyCom.Parameters["@gsub"].Value = double.Parse(GasSubTB.Text);
                MyCom.Parameters.Add("@msub", SqlDbType.Real);
                MyCom.Parameters["@msub"].Value = double.Parse(MazutSubTB.Text);
                MyCom.Parameters.Add("@gosub", SqlDbType.Real);
                MyCom.Parameters["@gosub"].Value = double.Parse(GasoilSubTB.Text);

                            
                MyCom.Parameters.Add("@plant", SqlDbType.NVarChar,50);
                MyCom.Parameters["@plant"].Value = comboSelectPlant.Text.Trim();
                MyCom.Parameters.Add("@status", SqlDbType.NVarChar, 50);
                MyCom.Parameters["@status"].Value = comboBoxstatus.Text.Trim();
                MyCom.Parameters.Add("@day", SqlDbType.NVarChar, 50);
                MyCom.Parameters["@day"].Value = comMarketRule.Text.Trim();


            
              ////////////////////
                MyCom.Parameters.Add("@gasheatavg", SqlDbType.Real);
                MyCom.Parameters["@gasheatavg"].Value = double.Parse(txtuperror.Text);

                MyCom.Parameters.Add("@oilheatavg", SqlDbType.Real);
                MyCom.Parameters["@oilheatavg"].Value = double.Parse(txtdownerror.Text);
                ////////////////////////////

                MyCom.Parameters.Add("@ref", SqlDbType.NVarChar, 50);
                MyCom.Parameters["@ref"].Value = cmbrefer.Text.Trim();
                MyCom.Parameters.Add("@trans", SqlDbType.NVarChar, 50);
                MyCom.Parameters["@trans"].Value = cmbtrans.Text.Trim();
                MyCom.Parameters.Add("@cap", SqlDbType.NVarChar, 50);
                MyCom.Parameters["@cap"].Value = comFuel.Text.Trim();

                try
                {
                    MyCom.ExecuteNonQuery();
                    MessageBox.Show("Save Successfully");
                    this.Close();
                }
                catch (Exception exp)
                {
                    string str = exp.Message;
                    if (str.Contains("PRIMARY KEY"))
                        MessageBox.Show("Data for selected date has saved before!");
                }
                finally
                {
                    MyCom.Connection.Close();
                }
            }
        }

        private void PmaxTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(PmaxTb.Text);
                errorProvider1.SetError(PmaxTb, "");
            }
            catch
            {
                errorProvider1.SetError(PmaxTb, "Invalid Value!");
            }
        }

        private void PminTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(PminTb.Text);
                errorProvider1.SetError(PminTb, "");
            }
            catch
            {
                errorProvider1.SetError(PminTb, "Invalid Value!");
            }
        }

        //private void CapacityTb_Validated(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        double x = double.Parse(CapacityTb.Text);
        //        errorProvider1.SetError(CapacityTb, "");
        //    }
        //    catch
        //    {
        //        errorProvider1.SetError(CapacityTb, "Invalid Value!");
        //    }
        //}

        //private void HourTb_Validated(object sender, EventArgs e)
        //{
        //    //9108
        //    //try
        //    //{
        //    //    double x = double.Parse(HourTb.Text);
        //    //    errorProvider1.SetError(HourTb, "");
        //    //}
        //    //catch
        //    //{
        //    //    errorProvider1.SetError(HourTb, "Invalid Value!");
        //    //}
        //}

        //private void DayTb_Validated(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        double x = double.Parse(DayTb.Text);
        //        errorProvider1.SetError(DayTb, "");
        //    }
        //    catch
        //    {
        //        errorProvider1.SetError(DayTb, "Invalid Value!");
        //    }
        //}

        private void GasTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(GasTb.Text);
                errorProvider1.SetError(GasTb, "");
            }
            catch
            {
                errorProvider1.SetError(GasTb, "Invalid Value!");
            }
        }

        private void SteamTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(SteamTb.Text);
                errorProvider1.SetError(SteamTb, "");
            }
            catch
            {
                errorProvider1.SetError(SteamTb, "Invalid Value!");
            }
        }

        private void CCTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(CCTb.Text);
                errorProvider1.SetError(CCTb, "");
            }
            catch
            {
                errorProvider1.SetError(CCTb, "Invalid Value!");
            }
        }

        private void GasPriceTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(GasPriceTb.Text);
                errorProvider1.SetError(GasPriceTb, "");
            }
            catch
            {
                errorProvider1.SetError(GasPriceTb, "Invalid Value!");
            }

        }

        private void MazutPriceTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(MazutPriceTb.Text);
                errorProvider1.SetError(MazutPriceTb, "");
            }
            catch
            {
                errorProvider1.SetError(MazutPriceTb, "Invalid Value!");
            }

        }

        private void GasOilPriceTb_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(GasOilPriceTb.Text);
                errorProvider1.SetError(GasOilPriceTb, "");
            }
            catch
            {
                errorProvider1.SetError(GasOilPriceTb, "Invalid Value!");
            }
        }

        private void LineEnergyPaymentTB_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(LineEnergyPaymentTB.Text);
                errorProvider1.SetError(LineEnergyPaymentTB, "");
            }
            catch
            {
                errorProvider1.SetError(LineEnergyPaymentTB, "Invalid Value!");
            }
        }

        private void LineCapacityPaymentTB_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(LineCapacityPaymentTB.Text);
                errorProvider1.SetError(LineCapacityPaymentTB, "");
            }
            catch
            {
                errorProvider1.SetError(LineCapacityPaymentTB, "Invalid Value!");
            }
        }
//--------------------------------GasSubTB_Validated----------------------------------------------
        private void GasSubTB_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(GasSubTB.Text);
                errorProvider1.SetError(GasSubTB, "");
            }
            catch
            {
                errorProvider1.SetError(GasSubTB, "Invalid Value!");
            }
        }

        /////////////////////txtGasHeatValueAverage_Validated////////////////////

        private void txtGasHeatValueAverage_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(txtuperror.Text);
                errorProvider1.SetError(txtuperror, "");
            }
            catch
            {
                errorProvider1.SetError(txtuperror, "Invalid Value!");
            }
            
        }
        /////////////////////////////txtGasOilHeatValueAverage_Validated//////////////////////////////
        
        private void txtGasOilHeatValueAverage_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(txtdownerror.Text);
                errorProvider1.SetError(txtdownerror, "");
            }
            catch
            {
                errorProvider1.SetError(txtdownerror, "Invalid Value!");
            }
        }
        
//--------------------------------MazutSubTB_Validated----------------------------------------------
        private void MazutSubTB_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(MazutSubTB.Text);
                errorProvider1.SetError(MazutSubTB, "");
            }
            catch
            {
                errorProvider1.SetError(MazutSubTB, "Invalid Value!");
            }
        }
//--------------------------------GasoilSubTB_Validated----------------------------------------------
        private void GasoilSubTB_Validated(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(GasoilSubTB.Text);
                errorProvider1.SetError(GasoilSubTB, "");
            }
            catch
            {
                errorProvider1.SetError(GasoilSubTB, "Invalid Value!");
            }
        }

        private void loaddata()
        {
            DataTable dtshow = Utilities.GetTable("select * from dbo.BaseData where Date in (select  max(Date) from dbo.BaseData)order by BaseID desc");
            UpdateCal.Text = dtshow.Rows[0]["Date"].ToString();
            PmaxTb.Text = dtshow.Rows[0]["MarketPriceMax"].ToString();
            PminTb.Text = dtshow.Rows[0]["MarketPriceMin"].ToString();
            GasTb.Text = dtshow.Rows[0]["GasProduction"].ToString();
            SteamTb.Text = dtshow.Rows[0]["SteamProduction"].ToString();
            CCTb.Text = dtshow.Rows[0]["CCProduction"].ToString();
            GasPriceTb.Text = dtshow.Rows[0]["GasPrice"].ToString();
            MazutPriceTb.Text = dtshow.Rows[0]["MazutPrice"].ToString();
            GasOilPriceTb.Text = dtshow.Rows[0]["GasOilPrice"].ToString();
            LineEnergyPaymentTB.Text = dtshow.Rows[0]["LineEnergyPayment"].ToString();
            LineCapacityPaymentTB.Text = dtshow.Rows[0]["LineCapacityPayment"].ToString();
            GasSubTB.Text = dtshow.Rows[0]["GasSubsidiesPrice"].ToString();
            MazutSubTB.Text = dtshow.Rows[0]["MazutSubsidiesPrice"].ToString();
            GasoilSubTB.Text = dtshow.Rows[0]["GasOilSubsidiesPrice"].ToString();
            txtuperror.Text = dtshow.Rows[0]["GasHeatValueAverage"].ToString();
            txtdownerror.Text = dtshow.Rows[0]["GasOilHeatValueAverage"].ToString();
            comboSelectPlant.Text = dtshow.Rows[0]["PPName"].ToString();
            comboBoxstatus.Text = dtshow.Rows[0]["ProposalHour"].ToString();
            comMarketRule.Text = dtshow.Rows[0]["ProposalDay"].ToString();
            cmbrefer.Text = dtshow.Rows[0]["reference"].ToString();
            cmbtrans.Text = dtshow.Rows[0]["transmission"].ToString();
            comFuel.Text = dtshow.Rows[0]["CapacityPayment"].ToString(); 

        }

        private void UpdateCal_ValueChanged(object sender, EventArgs e)
        {
            string smaxdate = "";
            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + UpdateCal.Text.Trim() + "'order by BaseID desc");
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }

            DataTable dtshow = Utilities.GetTable("select * from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (dtshow.Rows.Count > 0)
            {
                //UpdateCal.Text = dtshow.Rows[0]["Date"].ToString();
                PmaxTb.Text = dtshow.Rows[0]["MarketPriceMax"].ToString();
                PminTb.Text = dtshow.Rows[0]["MarketPriceMin"].ToString();
                GasTb.Text = dtshow.Rows[0]["GasProduction"].ToString();
                SteamTb.Text = dtshow.Rows[0]["SteamProduction"].ToString();
                CCTb.Text = dtshow.Rows[0]["CCProduction"].ToString();
                GasPriceTb.Text = dtshow.Rows[0]["GasPrice"].ToString();
                MazutPriceTb.Text = dtshow.Rows[0]["MazutPrice"].ToString();
                GasOilPriceTb.Text = dtshow.Rows[0]["GasOilPrice"].ToString();
                LineEnergyPaymentTB.Text = dtshow.Rows[0]["LineEnergyPayment"].ToString();
                LineCapacityPaymentTB.Text = dtshow.Rows[0]["LineCapacityPayment"].ToString();
                GasSubTB.Text = dtshow.Rows[0]["GasSubsidiesPrice"].ToString();
                MazutSubTB.Text = dtshow.Rows[0]["MazutSubsidiesPrice"].ToString();
                GasoilSubTB.Text = dtshow.Rows[0]["GasOilSubsidiesPrice"].ToString();
                txtuperror.Text = dtshow.Rows[0]["GasHeatValueAverage"].ToString();
                txtdownerror.Text = dtshow.Rows[0]["GasOilHeatValueAverage"].ToString();
                comboSelectPlant.Text = dtshow.Rows[0]["PPName"].ToString();
                comboBoxstatus.Text = dtshow.Rows[0]["ProposalHour"].ToString();
                comMarketRule.Text = dtshow.Rows[0]["ProposalDay"].ToString();
                cmbrefer.Text = dtshow.Rows[0]["reference"].ToString();
                cmbtrans.Text = dtshow.Rows[0]["transmission"].ToString();
                comFuel.Text = dtshow.Rows[0]["CapacityPayment"].ToString();
            }
        }

        private string buildmaxdate(string[] arrmax)
        {
            string maxdate = null;
            for (int fd = 0; fd < arrmax.Length; fd++)
            {
                if (maxdate == null || string.Compare(maxdate, arrmax[fd]) < 0)
                    maxdate = arrmax[fd];
            }
            return maxdate;

        }

        private void PmaxTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(PmaxTb, "");
        }

        private void PmaxTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(PmaxTb, "Rial");
        }

        private void PminTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(PminTb, "Rial");
        }

        private void PminTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(PminTb, "");
        }

        private void CapacityTb_Leave(object sender, EventArgs e)
        {
            //toolTip1.SetToolTip(CapacityTb, "");
        }

        private void CapacityTb_MouseClick(object sender, MouseEventArgs e)
        {
            //toolTip1.SetToolTip(CapacityTb, "Rial");
        }

        //private void DayTb_Leave(object sender, EventArgs e)
        //{
        //    toolTip1.SetToolTip(DayTb, "");
        //}

        //private void DayTb_MouseClick(object sender, MouseEventArgs e)
        //{
        //    toolTip1.SetToolTip(DayTb, "Day");
        //}

        private void GasTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(GasTb, "Rial");
        }

        private void GasTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GasTb, "");
        }

        private void SteamTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(SteamTb, "");
        }

        private void SteamTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(SteamTb, "Rial");
        }

        private void CCTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(CCTb, "Rial");
        }

        private void CCTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(CCTb, "");
        }

        private void txtGasHeatValueAverage_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtuperror, "");
        }

        private void txtGasHeatValueAverage_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(txtuperror, "BTU");
        }

        private void txtGasOilHeatValueAverage_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(txtdownerror, "BTU");
        }

        private void txtGasOilHeatValueAverage_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtdownerror, "");
        }

        private void GasPriceTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GasPriceTb, "");
        }

        private void GasPriceTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(GasPriceTb, "Rial");
        }

        private void MazutPriceTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(MazutPriceTb, "");
        }

        private void MazutPriceTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(MazutPriceTb, "Rial");
        }

        private void GasOilPriceTb_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(GasOilPriceTb, "Rial");
        }

        private void GasOilPriceTb_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GasOilPriceTb, "");
        }

        private void GasSubTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GasSubTB, "");
        }

        private void GasSubTB_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(GasSubTB, "Rial");
        }

        private void MazutSubTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(MazutSubTB, "");
        }

        private void MazutSubTB_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(MazutSubTB, "Rial");
        }

        private void GasoilSubTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(GasoilSubTB, "");
        }

        private void GasoilSubTB_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(GasoilSubTB, "Rial");
        }

        private void LineEnergyPaymentTB_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(LineEnergyPaymentTB, "Rial");
        }

        private void LineEnergyPaymentTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(LineEnergyPaymentTB, "");
        }

        private void LineCapacityPaymentTB_Leave(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(LineCapacityPaymentTB, "");
        }

        private void LineCapacityPaymentTB_MouseClick(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(LineCapacityPaymentTB, "Rial");
        }


        public void fillgrid()
        {
            dataGridView2.Visible = true;
            dataGridView2.DataSource = null;
            DataTable dtshow = Utilities.GetTable("select top(3)* from dbo.BaseData order by date desc, BaseID desc");
            dataGridView2.DataSource = dtshow;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            fillgrid();
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowIndex = e.RowIndex;

                string x = dataGridView2.Rows[rowIndex].Cells[e.ColumnIndex].Value.ToString();
                if (x == "System.Drawing.Bitmap")
                {
                    DataTable bb = Utilities.GetTable("delete from dbo.BaseData where  date='" + dataGridView2.Rows[rowIndex].Cells[2].Value.ToString() + "'and baseid='" + dataGridView2.Rows[rowIndex].Cells[1].Value.ToString() + "'");
                }
               
            }
            catch
            {
            }
            fillgrid();
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            this.Close();
            FuelSetting n = new FuelSetting();          
            n.panel1.Visible = true;
            n.Show();
        }

    

     


        
       

      
     

       

       

     

     


    }
}
