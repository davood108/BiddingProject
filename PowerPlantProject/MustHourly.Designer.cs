﻿namespace PowerPlantProject
{
    partial class MustHourly
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MustHourly));
            this.datePickerFrom = new FarsiLibrary.Win.Controls.FADatePicker();
            this.rdmustrun = new System.Windows.Forms.RadioButton();
            this.rdMustNet = new System.Windows.Forms.RadioButton();
            this.rdMustOff = new System.Windows.Forms.RadioButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.H1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbfhour = new System.Windows.Forms.ComboBox();
            this.cmbthour = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnadd = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmbplant = new System.Windows.Forms.ComboBox();
            this.cmbunit = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.datePickerto = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.delete = new System.Windows.Forms.DataGridViewImageColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // datePickerFrom
            // 
            this.datePickerFrom.HasButtons = true;
            this.datePickerFrom.Location = new System.Drawing.Point(57, 31);
            this.datePickerFrom.Name = "datePickerFrom";
            this.datePickerFrom.Readonly = true;
            this.datePickerFrom.Size = new System.Drawing.Size(120, 20);
            this.datePickerFrom.TabIndex = 44;
            this.datePickerFrom.SelectedDateTimeChanged += new System.EventHandler(this.datePickerFrom_SelectedDateTimeChanged);
            this.datePickerFrom.ValueChanged += new System.EventHandler(this.datePickerFrom_ValueChanged);
            // 
            // rdmustrun
            // 
            this.rdmustrun.AutoSize = true;
            this.rdmustrun.Location = new System.Drawing.Point(16, 19);
            this.rdmustrun.Name = "rdmustrun";
            this.rdmustrun.Size = new System.Drawing.Size(68, 17);
            this.rdmustrun.TabIndex = 45;
            this.rdmustrun.Text = "MustRun";
            this.rdmustrun.UseVisualStyleBackColor = true;
            // 
            // rdMustNet
            // 
            this.rdMustNet.AutoSize = true;
            this.rdMustNet.Location = new System.Drawing.Point(69, 48);
            this.rdMustNet.Name = "rdMustNet";
            this.rdMustNet.Size = new System.Drawing.Size(65, 17);
            this.rdMustNet.TabIndex = 45;
            this.rdMustNet.Text = "MustNet";
            this.rdMustNet.UseVisualStyleBackColor = true;
            // 
            // rdMustOff
            // 
            this.rdMustOff.AutoSize = true;
            this.rdMustOff.Location = new System.Drawing.Point(125, 23);
            this.rdMustOff.Name = "rdMustOff";
            this.rdMustOff.Size = new System.Drawing.Size(62, 17);
            this.rdMustOff.TabIndex = 45;
            this.rdMustOff.Text = "MustOff";
            this.rdMustOff.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(262, 28);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(120, 20);
            this.textBox1.TabIndex = 46;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox1.Leave += new System.EventHandler(this.textBox1_Leave);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.H1,
            this.H2,
            this.H3,
            this.H4,
            this.H5,
            this.H6,
            this.H7,
            this.H8,
            this.H9,
            this.H10,
            this.H11,
            this.H12,
            this.H13,
            this.H14,
            this.H15,
            this.H16,
            this.H17,
            this.H18,
            this.H19,
            this.H20,
            this.H21,
            this.H22,
            this.H23,
            this.H24});
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(1, 225);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(936, 60);
            this.dataGridView1.TabIndex = 47;
            // 
            // H1
            // 
            this.H1.HeaderText = "H1";
            this.H1.Name = "H1";
            // 
            // H2
            // 
            this.H2.HeaderText = "H2";
            this.H2.Name = "H2";
            // 
            // H3
            // 
            this.H3.HeaderText = "H3";
            this.H3.Name = "H3";
            // 
            // H4
            // 
            this.H4.HeaderText = "H4";
            this.H4.Name = "H4";
            // 
            // H5
            // 
            this.H5.HeaderText = "H5";
            this.H5.Name = "H5";
            // 
            // H6
            // 
            this.H6.HeaderText = "H6";
            this.H6.Name = "H6";
            // 
            // H7
            // 
            this.H7.HeaderText = "H7";
            this.H7.Name = "H7";
            // 
            // H8
            // 
            this.H8.HeaderText = "H8";
            this.H8.Name = "H8";
            // 
            // H9
            // 
            this.H9.HeaderText = "H9";
            this.H9.Name = "H9";
            // 
            // H10
            // 
            this.H10.HeaderText = "H10";
            this.H10.Name = "H10";
            // 
            // H11
            // 
            this.H11.HeaderText = "H11";
            this.H11.Name = "H11";
            // 
            // H12
            // 
            this.H12.HeaderText = "H12";
            this.H12.Name = "H12";
            // 
            // H13
            // 
            this.H13.HeaderText = "H13";
            this.H13.Name = "H13";
            // 
            // H14
            // 
            this.H14.HeaderText = "H14";
            this.H14.Name = "H14";
            // 
            // H15
            // 
            this.H15.HeaderText = "H15";
            this.H15.Name = "H15";
            // 
            // H16
            // 
            this.H16.HeaderText = "H16";
            this.H16.Name = "H16";
            // 
            // H17
            // 
            this.H17.HeaderText = "H17";
            this.H17.Name = "H17";
            // 
            // H18
            // 
            this.H18.HeaderText = "H18";
            this.H18.Name = "H18";
            // 
            // H19
            // 
            this.H19.HeaderText = "H19";
            this.H19.Name = "H19";
            // 
            // H20
            // 
            this.H20.HeaderText = "H20";
            this.H20.Name = "H20";
            // 
            // H21
            // 
            this.H21.HeaderText = "H21";
            this.H21.Name = "H21";
            // 
            // H22
            // 
            this.H22.HeaderText = "H22";
            this.H22.Name = "H22";
            // 
            // H23
            // 
            this.H23.HeaderText = "H23";
            this.H23.Name = "H23";
            // 
            // H24
            // 
            this.H24.HeaderText = "H24";
            this.H24.Name = "H24";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 48;
            this.label1.Text = "From :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(218, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 48;
            this.label4.Text = "Value";
            // 
            // cmbfhour
            // 
            this.cmbfhour.FormattingEnabled = true;
            this.cmbfhour.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"});
            this.cmbfhour.Location = new System.Drawing.Point(70, 23);
            this.cmbfhour.Name = "cmbfhour";
            this.cmbfhour.Size = new System.Drawing.Size(62, 21);
            this.cmbfhour.TabIndex = 49;
            // 
            // cmbthour
            // 
            this.cmbthour.FormattingEnabled = true;
            this.cmbthour.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"});
            this.cmbthour.Location = new System.Drawing.Point(228, 23);
            this.cmbthour.Name = "cmbthour";
            this.cmbthour.Size = new System.Drawing.Size(63, 21);
            this.cmbthour.TabIndex = 49;
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::PowerPlantProject.Properties.Resources.savemaeium;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(436, 303);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(35, 28);
            this.button1.TabIndex = 50;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 48;
            this.label5.Text = "From";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(202, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 48;
            this.label6.Text = "To";
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.Color.Transparent;
            this.btnadd.BackgroundImage = global::PowerPlantProject.Properties.Resources.add89;
            this.btnadd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnadd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnadd.Location = new System.Drawing.Point(418, 187);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(33, 23);
            this.btnadd.TabIndex = 51;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::PowerPlantProject.Properties.Resources.deletered81;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Location = new System.Drawing.Point(458, 187);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(33, 23);
            this.button2.TabIndex = 52;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdmustrun);
            this.groupBox1.Controls.Add(this.rdMustNet);
            this.groupBox1.Controls.Add(this.rdMustOff);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(471, 88);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(404, 71);
            this.groupBox1.TabIndex = 53;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbthour);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cmbfhour);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(64, 88);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(368, 71);
            this.groupBox2.TabIndex = 54;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Hour";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmbplant);
            this.groupBox3.Controls.Add(this.cmbunit);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(471, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(404, 68);
            this.groupBox3.TabIndex = 55;
            this.groupBox3.TabStop = false;
            // 
            // cmbplant
            // 
            this.cmbplant.FormattingEnabled = true;
            this.cmbplant.Location = new System.Drawing.Point(59, 23);
            this.cmbplant.Name = "cmbplant";
            this.cmbplant.Size = new System.Drawing.Size(121, 21);
            this.cmbplant.TabIndex = 0;
            this.cmbplant.SelectedValueChanged += new System.EventHandler(this.cmbplant_SelectedValueChanged);
            // 
            // cmbunit
            // 
            this.cmbunit.FormattingEnabled = true;
            this.cmbunit.Location = new System.Drawing.Point(261, 24);
            this.cmbunit.Name = "cmbunit";
            this.cmbunit.Size = new System.Drawing.Size(121, 21);
            this.cmbunit.TabIndex = 0;
            this.cmbunit.SelectedValueChanged += new System.EventHandler(this.cmbunit_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 48;
            this.label2.Text = "Plant";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(229, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 48;
            this.label3.Text = "Unit";
            // 
            // datePickerto
            // 
            this.datePickerto.HasButtons = true;
            this.datePickerto.Location = new System.Drawing.Point(228, 31);
            this.datePickerto.Name = "datePickerto";
            this.datePickerto.Readonly = true;
            this.datePickerto.Size = new System.Drawing.Size(120, 20);
            this.datePickerto.TabIndex = 44;
            this.datePickerto.SelectedDateTimeChanged += new System.EventHandler(this.datePickerFrom_SelectedDateTimeChanged);
            this.datePickerto.ValueChanged += new System.EventHandler(this.datePickerFrom_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(199, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 48;
            this.label7.Text = "To:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.datePickerFrom);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.datePickerto);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Location = new System.Drawing.Point(64, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(368, 68);
            this.groupBox4.TabIndex = 56;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Date";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.delete});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.Location = new System.Drawing.Point(1, 345);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.Size = new System.Drawing.Size(936, 151);
            this.dataGridView2.TabIndex = 58;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // delete
            // 
            this.delete.HeaderText = "Delete";
            this.delete.Image = global::PowerPlantProject.Properties.Resources.deletered81;
            this.delete.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.delete.Name = "delete";
            // 
            // MustHourly
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(938, 507);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnadd);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MustHourly";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MustHourly";
            this.Load += new System.EventHandler(this.MustHourly_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private FarsiLibrary.Win.Controls.FADatePicker datePickerFrom;
        private System.Windows.Forms.RadioButton rdmustrun;
        private System.Windows.Forms.RadioButton rdMustNet;
        private System.Windows.Forms.RadioButton rdMustOff;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbfhour;
        private System.Windows.Forms.ComboBox cmbthour;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.DataGridViewTextBoxColumn H1;
        private System.Windows.Forms.DataGridViewTextBoxColumn H2;
        private System.Windows.Forms.DataGridViewTextBoxColumn H3;
        private System.Windows.Forms.DataGridViewTextBoxColumn H4;
        private System.Windows.Forms.DataGridViewTextBoxColumn H5;
        private System.Windows.Forms.DataGridViewTextBoxColumn H6;
        private System.Windows.Forms.DataGridViewTextBoxColumn H7;
        private System.Windows.Forms.DataGridViewTextBoxColumn H8;
        private System.Windows.Forms.DataGridViewTextBoxColumn H9;
        private System.Windows.Forms.DataGridViewTextBoxColumn H10;
        private System.Windows.Forms.DataGridViewTextBoxColumn H11;
        private System.Windows.Forms.DataGridViewTextBoxColumn H12;
        private System.Windows.Forms.DataGridViewTextBoxColumn H13;
        private System.Windows.Forms.DataGridViewTextBoxColumn H14;
        private System.Windows.Forms.DataGridViewTextBoxColumn H15;
        private System.Windows.Forms.DataGridViewTextBoxColumn H16;
        private System.Windows.Forms.DataGridViewTextBoxColumn H17;
        private System.Windows.Forms.DataGridViewTextBoxColumn H18;
        private System.Windows.Forms.DataGridViewTextBoxColumn H19;
        private System.Windows.Forms.DataGridViewTextBoxColumn H20;
        private System.Windows.Forms.DataGridViewTextBoxColumn H21;
        private System.Windows.Forms.DataGridViewTextBoxColumn H22;
        private System.Windows.Forms.DataGridViewTextBoxColumn H23;
        private System.Windows.Forms.DataGridViewTextBoxColumn H24;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cmbplant;
        private System.Windows.Forms.ComboBox cmbunit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewImageColumn delete;
    }
}