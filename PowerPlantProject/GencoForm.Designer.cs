﻿namespace PowerPlantProject
{
    partial class GencoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GencoForm));
            this.button1 = new System.Windows.Forms.Button();
            this.txtgenenglish = new System.Windows.Forms.TextBox();
            this.lbgenenglish = new System.Windows.Forms.Label();
            this.lbgenpersian = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbgencode = new System.Windows.Forms.ComboBox();
            this.cmbgenpersian = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.CadetBlue;
            this.button1.BackgroundImage = global::PowerPlantProject.Properties.Resources.Savedata;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(103, 214);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 27);
            this.button1.TabIndex = 0;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtgenenglish
            // 
            this.txtgenenglish.Location = new System.Drawing.Point(134, 42);
            this.txtgenenglish.Name = "txtgenenglish";
            this.txtgenenglish.Size = new System.Drawing.Size(121, 20);
            this.txtgenenglish.TabIndex = 1;
            // 
            // lbgenenglish
            // 
            this.lbgenenglish.AutoSize = true;
            this.lbgenenglish.Location = new System.Drawing.Point(13, 45);
            this.lbgenenglish.Name = "lbgenenglish";
            this.lbgenenglish.Size = new System.Drawing.Size(113, 13);
            this.lbgenenglish.TabIndex = 2;
            this.lbgenenglish.Text = "English Genco Name :";
            // 
            // lbgenpersian
            // 
            this.lbgenpersian.AutoSize = true;
            this.lbgenpersian.Location = new System.Drawing.Point(12, 102);
            this.lbgenpersian.Name = "lbgenpersian";
            this.lbgenpersian.Size = new System.Drawing.Size(114, 13);
            this.lbgenpersian.TabIndex = 2;
            this.lbgenpersian.Text = "Persian Genco Name :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 159);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Genco Code :";
            // 
            // cmbgencode
            // 
            this.cmbgencode.FormattingEnabled = true;
            this.cmbgencode.Items.AddRange(new object[] {
            "R01",
            "R02",
            "R03",
            "R04",
            "R05",
            "R06",
            "R07",
            "R08",
            "R09",
            "R10",
            "R11",
            "R12",
            "R13",
            "R14",
            "R15",
            "R16",
            "R17",
            "R18",
            "R19",
            "R20"});
            this.cmbgencode.Location = new System.Drawing.Point(134, 156);
            this.cmbgencode.Name = "cmbgencode";
            this.cmbgencode.Size = new System.Drawing.Size(121, 21);
            this.cmbgencode.TabIndex = 3;
            // 
            // cmbgenpersian
            // 
            this.cmbgenpersian.FormattingEnabled = true;
            this.cmbgenpersian.Items.AddRange(new object[] {
            "تهران ",
            "مازندران",
            "گيلان ",
            "سمنان",
            "زنجان",
            "آذربايجان ",
            "باختر ",
            "اصفهان",
            "فارس",
            "خوزستان",
            "غرب",
            "يزد",
            "كرمان",
            "هرمزگان ",
            "خراسان ",
            "سيستان و بلوچستان   "});
            this.cmbgenpersian.Location = new System.Drawing.Point(134, 99);
            this.cmbgenpersian.Name = "cmbgenpersian";
            this.cmbgenpersian.Size = new System.Drawing.Size(121, 21);
            this.cmbgenpersian.TabIndex = 3;
            // 
            // GencoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.cmbgenpersian);
            this.Controls.Add(this.cmbgencode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbgenpersian);
            this.Controls.Add(this.lbgenenglish);
            this.Controls.Add(this.txtgenenglish);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "GencoForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GencoForm ";
            this.Load += new System.EventHandler(this.GencoForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtgenenglish;
        private System.Windows.Forms.Label lbgenenglish;
        private System.Windows.Forms.Label lbgenpersian;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbgencode;
        private System.Windows.Forms.ComboBox cmbgenpersian;
    }
}