﻿namespace PowerPlantProject
{
    partial class BillItemPrintEx
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BillItemPrintEx));
            this.panel81 = new System.Windows.Forms.Panel();
            this.datePickerFrom = new FarsiLibrary.Win.Controls.FADatePicker();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel81.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel81
            // 
            this.panel81.BackColor = System.Drawing.Color.CadetBlue;
            this.panel81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel81.Controls.Add(this.datePickerFrom);
            this.panel81.Location = new System.Drawing.Point(185, 8);
            this.panel81.Name = "panel81";
            this.panel81.Size = new System.Drawing.Size(137, 33);
            this.panel81.TabIndex = 43;
            this.panel81.Visible = false;
            // 
            // datePickerFrom
            // 
            this.datePickerFrom.HasButtons = true;
            this.datePickerFrom.Location = new System.Drawing.Point(6, 6);
            this.datePickerFrom.Name = "datePickerFrom";
            this.datePickerFrom.Readonly = true;
            this.datePickerFrom.Size = new System.Drawing.Size(124, 20);
            this.datePickerFrom.TabIndex = 33;
            this.datePickerFrom.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.datePickerFrom.Visible = false;
            this.datePickerFrom.ValueChanged += new System.EventHandler(this.datePickerFrom_ValueChanged);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.checkedListBox1.Font = new System.Drawing.Font("B Nazanin", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(14, 47);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkedListBox1.Size = new System.Drawing.Size(472, 288);
            this.checkedListBox1.TabIndex = 42;
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(211, 341);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 44;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BillItemPrintEx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 375);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel81);
            this.Controls.Add(this.checkedListBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BillItemPrintEx";
            this.Text = "BillItemEx";
            this.Load += new System.EventHandler(this.BillItemPrintEx_Load);
            this.panel81.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel81;
        public FarsiLibrary.Win.Controls.FADatePicker datePickerFrom;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Button button1;

    }
}