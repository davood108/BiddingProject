﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using NRI.SBS.Common;
using Auto_Update_Library;
using System.Collections;
using System.IO;


namespace PowerPlantProject
{
    public partial class RunSetting : Form
    {
        string Plant = "";
        bool EnablePre = false;
        public RunSetting(string plantname,bool enable)
        {
            Plant = plantname;
            EnablePre=enable;
           InitializeComponent();
           try
           {
               this.BackColor = FormColors.GetColor().Formbackcolor;

               //////////////////////////buttons/////////////////////////////

               foreach (Control c in this.Controls)
               {
                   if (c is Button)
                       c.BackColor = FormColors.GetColor().Buttonbackcolor;
               }
               btnSave.BackColor = FormColors.GetColor().Buttonbackcolor;
               //////////////////////////////panel,groupbox/////////////////////////////;
               foreach (Control c in this.Controls)
               {
                   if (c is Panel || c is GroupBox)
                       c.BackColor = FormColors.GetColor().Panelbackcolor;

               }

               //////////////////////////////////////////////////////////////////////////
               ////////////////////////////////textbox /////////////////////////////////////

               foreach (Control c in this.Controls)
               {
                   if (c is TextBox || c is ListBox)
                   {
                       c.BackColor = FormColors.GetColor().Textbackcolor;
                   }
                  else  if (c is Button)
                   {
                       c.BackColor = FormColors.GetColor().Buttonbackcolor;
                   }


               }

               //////////////////////////////////////////////////////////////////////////////
           }
           catch
           {

           }
        }

        private void RunSetting_Load(object sender, EventArgs e)
        {
            //build tree//------------------------------------------------------------------
            string GenCo = "";
            DataTable dt = Utilities.GetTable("select * from BaseGencoInfo");
            if (dt.Rows.Count > 0)
            {
                GenCo = dt.Rows[0]["GencoNameEnglish"].ToString().Substring(0, 1).Trim().ToUpper() + "rec";

            }
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            treeView1.Nodes.Clear();
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();

            //Insert Trec
            TreeNode MyNode = new TreeNode();
            MyNode.Text = GenCo;
            treeView1.Nodes.Add(MyNode);

            //Insert Plants
            Myda.SelectCommand = new SqlCommand("SELECT PPName FROM PowerPlant order by ppid", myConnection);
            Myda.Fill(MyDS, "plantname");
            TreeNode PlantNode = new TreeNode();
            PlantNode.Text = "Plant";
            MyNode.Nodes.Add(PlantNode);
            foreach (DataRow MyRow in MyDS.Tables["plantname"].Rows)
            {
                TreeNode ChildNode = new TreeNode();
                ChildNode.Text = MyRow["PPName"].ToString().Trim();
                PlantNode.Nodes.Add(ChildNode);
            }
            treeView1.ExpandAll();

            //------------------------------------------------------------
            loaddata();
            lblPlantValue.Text = Plant;
            if (EnablePre == false)
            {

                checkPreSolve.Enabled = false;
            }
            else
                checkPreSolve.Enabled = true;
        }
      

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            string GenCo = "";
            DataTable dt = Utilities.GetTable("select * from BaseGencoInfo");
            if (dt.Rows.Count > 0)
            {
                GenCo = dt.Rows[0]["GencoNameEnglish"].ToString().Substring(0, 1).Trim().ToUpper() + "rec";

            }

            if (e.Node.Text == GenCo || e.Node.Text == "Plant")
            {
                lblPlantValue.Text = "All";
                Plant = "All";
            }
            else
            {
                lblPlantValue.Text = e.Node.Text;
                Plant = e.Node.Text;
            }
            loaddata();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Plant != "All")
            {
                int BiddingStrategySettingId = 0;

                DataTable dt = Utilities.GetTable("select max(id) from [BidRunSetting] " +
                " where Plant='" + lblPlantValue.Text.Trim() + "'");
                if (dt.Rows[0][0].ToString() != "")
                {
                    BiddingStrategySettingId = int.Parse(dt.Rows[0][0].ToString());

                    if (BiddingStrategySettingId != 0)
                    {
                        DataTable dt1 = Utilities.GetTable("Delete from BidRunSetting where id='" + BiddingStrategySettingId.ToString() + "'");

                    }
                }
                DataTable dt2 = Utilities.GetTable("insert INTO [BidRunSetting] " +
                   "(Plant, FlagOpf, FlagSensetivity, FlagMarketPower,FlagLossPayment, FlagPresolve, FlagCostFunction,FairPlay,LMP)"
               + " VALUES ('" + Plant + "','" + rdopf.Checked.ToString() + "','" + rdsensetivity.Checked.ToString() + "','" + chkmarketpower.Checked.ToString() + "','" + chkloospay.Checked.ToString() + "','" + checkPreSolve.Checked.ToString() + "','" + chkCost.Checked.ToString() + "','" + chkfairplay.Checked.ToString() +"','"+ chKlmp.Checked.ToString()+ "')");
               
            }
            else
            {
                DataTable dd = Utilities.GetTable(" select distinct PPName from dbo.PowerPlant");
                int num = dd.Rows.Count;
                for (int i = 0; i < num; i++)
                {
                    int BiddingStrategySettingId = 0;

                    DataTable dt = Utilities.GetTable("select max(id) from [BidRunSetting] " +
                    " where Plant='" + dd.Rows[i][0].ToString() + "'");
                    if (dt.Rows[0][0].ToString() != "")
                    {
                        BiddingStrategySettingId = int.Parse(dt.Rows[0][0].ToString());

                        if (BiddingStrategySettingId != 0)
                        {
                            DataTable dt1 = Utilities.GetTable("Delete from BidRunSetting where id='" + BiddingStrategySettingId.ToString() + "'");

                        }
                    }
                    DataTable dt2 = Utilities.GetTable("insert INTO [BidRunSetting] " +
                       "(Plant, FlagOpf, FlagSensetivity, FlagMarketPower,FlagLossPayment, FlagPresolve, FlagCostFunction,FairPlay,LMP)"
                   + " VALUES ('" + dd.Rows[i][0].ToString().Trim() + "','" + rdopf.Checked.ToString() + "','" + rdsensetivity.Checked.ToString() + "','" + chkmarketpower.Checked.ToString() + "','" + chkloospay.Checked.ToString() + "','" + checkPreSolve.Checked.ToString() + "','" + chkCost.Checked.ToString() + "','" + chkfairplay.Checked.ToString() +"','"+ chKlmp.Checked.ToString()+ "')");

                }
            }
               
             
                      
           
        }
        private void loaddata()
        {
            DataTable dd = Utilities.GetTable(" select PPName from dbo.PowerPlant");
            DataTable dt = null;
            if (Plant != "All")
            {
                dt = Utilities.GetTable("select * from BidRunSetting where Plant='" + Plant + "'");
            }
            else
            {
                dt = Utilities.GetTable("select * from BidRunSetting where Plant='" + dd.Rows[0][0].ToString().Trim() + "'");

            }
            if (dt.Rows.Count > 0)
            {
                if (EnablePre)
                {
                   checkPreSolve.Checked = MyboolParse(dt.Rows[0]["FlagPresolve"].ToString());
                }
                else
                {
                    checkPreSolve.Checked = MyboolParse(dt.Rows[0]["FlagPresolve"].ToString());
                    //checkPreSolve.Checked = false;
                }
                chkmarketpower.Checked = MyboolParse(dt.Rows[0]["FlagMarketPower"].ToString());
                rdopf.Checked = MyboolParse(dt.Rows[0]["FlagOpf"].ToString());
                rdsensetivity.Checked = MyboolParse(dt.Rows[0]["FlagSensetivity"].ToString());
                chkloospay.Checked = MyboolParse(dt.Rows[0]["FlagLossPayment"].ToString());
                chkCost.Checked = MyboolParse(dt.Rows[0]["FlagCostFunction"].ToString());
                chkfairplay.Checked = MyboolParse(dt.Rows[0]["FairPlay"].ToString());
                chKlmp.Checked = MyboolParse(dt.Rows[0]["LMP"].ToString());
            }
            else
            {

              
                 checkPreSolve.Checked = false;
                 chkmarketpower.Checked = false;
                 rdopf.Checked = false;
                 rdsensetivity.Checked = false;
                 chkloospay.Checked = false;
                 chkCost.Checked = false;
                 chkfairplay.Checked = false;
                 chKlmp.Checked = false;
            }

        }
        private static bool  MyboolParse(string str)
        {
            if (str.Trim() == "")
                return false;
            else
            {
                try { return bool.Parse(str.Trim()); }
                catch { return false; }
            }
        }

      

       

        

    }
}
