﻿namespace PowerPlantProject
{
    partial class EditOperationalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditOperationalForm));
            this.editoperationalgrid = new System.Windows.Forms.DataGridView();
            this.Button = new System.Windows.Forms.DataGridViewButtonColumn();
            this.button2 = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.editoperationalgrid)).BeginInit();
            this.SuspendLayout();
            // 
            // editoperationalgrid
            // 
            this.editoperationalgrid.AllowUserToAddRows = false;
            this.editoperationalgrid.AllowUserToOrderColumns = true;
            this.editoperationalgrid.BackgroundColor = System.Drawing.Color.White;
            this.editoperationalgrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.editoperationalgrid.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.editoperationalgrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.editoperationalgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.editoperationalgrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Button,
            this.button2});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.editoperationalgrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.editoperationalgrid.EnableHeadersVisualStyles = false;
            this.editoperationalgrid.Location = new System.Drawing.Point(8, 12);
            this.editoperationalgrid.Name = "editoperationalgrid";
            this.editoperationalgrid.RowHeadersVisible = false;
            this.editoperationalgrid.RowTemplate.Height = 30;
            this.editoperationalgrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.editoperationalgrid.Size = new System.Drawing.Size(921, 111);
            this.editoperationalgrid.TabIndex = 15;
            this.editoperationalgrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.editoperationalgrid_CellClick);
            // 
            // Button
            // 
            this.Button.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Button.DataPropertyName = "Delete";
            this.Button.HeaderText = "Delete";
            this.Button.Name = "Button";
            this.Button.Text = "Delete";
            this.Button.UseColumnTextForButtonValue = true;
            this.Button.Width = 50;
            // 
            // button2
            // 
            this.button2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.button2.DataPropertyName = "Edit";
            this.button2.HeaderText = "Edit";
            this.button2.Name = "button2";
            this.button2.Text = "Edit";
            this.button2.UseColumnTextForButtonValue = true;
            this.button2.Width = 35;
            // 
            // EditOperationalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(937, 132);
            this.Controls.Add(this.editoperationalgrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "EditOperationalForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EditOperationalForm";
            this.Load += new System.EventHandler(this.EditOperationalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.editoperationalgrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView editoperationalgrid;
        private System.Windows.Forms.DataGridViewButtonColumn Button;
        private System.Windows.Forms.DataGridViewButtonColumn button2;

    }
}