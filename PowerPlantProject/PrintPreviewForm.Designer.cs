﻿namespace PowerPlantProject
{
    partial class PrintPreviewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintPreviewForm));
            this.pdfviewer = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // pdfviewer
            // 
            this.pdfviewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pdfviewer.Location = new System.Drawing.Point(0, 0);
            this.pdfviewer.MinimumSize = new System.Drawing.Size(20, 20);
            this.pdfviewer.Name = "pdfviewer";
            this.pdfviewer.Size = new System.Drawing.Size(837, 561);
            this.pdfviewer.TabIndex = 0;
            // 
            // PrintPreviewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 561);
            this.Controls.Add(this.pdfviewer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PrintPreviewForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrintPreviewForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser pdfviewer;
    }
}