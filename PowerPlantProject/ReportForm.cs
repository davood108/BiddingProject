﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    public partial class ReportForm : Form
    {
        string myType;
        DataTable PlantTable = null;
        public ReportForm(string type)
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;
                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
            myType = type;
        }

        private void ReportForm_Load(object sender, EventArgs e)
        {
            if (myType == "allplant")
            {

                PlantName.Enabled = false;
                PlantCombo.Enabled = false;
                ToDateLb.Enabled = false;
                ToDateCal.Enabled = false;
                FromDateLb.Text = "     Date :";
                DailyRadio.Visible = true;
                DailyRadio.Enabled = true;
                MonthlyRadio.Visible = true;
                MonthlyRadio.Enabled = true;
                MaintenanceRadio.Visible = false;
                MaintenanceRadio.Enabled = false;
                FinancialRadio.Checked = true;
            }
            else
            {
                PlantTable = Utilities.GetTable("SELECT DISTINCT PPID,PPName FROM PowerPlant");
                for (int i = 0; i < PlantTable.Rows.Count; i++)
                    PlantCombo.Items.Add(PlantTable.Rows[i][1].ToString());
                if (myType == "selectedplant")
                {
                    DailyRadio.Visible = true;
                    DailyRadio.Enabled = true;
                    MonthlyRadio.Visible = true;
                    MonthlyRadio.Enabled = true;
                    MaintenanceRadio.Visible = true;
                    MaintenanceRadio.Enabled = true;
                }
                if (myType == "selectedunit")
                {
                    DailyRadio.Visible = false;
                    DailyRadio.Enabled = false;
                    MonthlyRadio.Visible = false;
                    MonthlyRadio.Enabled = false;
                    MaintenanceRadio.Visible = false;
                    MaintenanceRadio.Enabled = false;
                    FinancialRadio.Checked = true;
                }
            }
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            if (myType == "allplant")//All Plant
            {
                string Date = FromDateCal.Text.Trim();
                if (FinancialRadio.Checked)
                {
                    if (DailyRadio.Checked)
                    {
                        EconomicAllPlantDailyReportForm myform = new EconomicAllPlantDailyReportForm(Date);
                        myform.ShowDialog();
                    }
                    else if (MonthlyRadio.Checked)
                    {
                        string myMonth = Date.Remove(7);
                        EconomicAllPlantMonthlyReportForm myform = new EconomicAllPlantMonthlyReportForm(myMonth);
                        myform.ShowDialog();
                    }
                }

            }
            else if ((myType == "selectedplant")||(myType== "selectedunit"))//selected Plant
            {
                string PPID = PlantTable.Rows[PlantCombo.SelectedIndex][0].ToString().Trim();
                string FromDate = FromDateCal.Text.Trim();
                string ToDate = ToDateCal.Text.Trim();

                if (FinancialRadio.Checked)
                {
                    if (myType == "selectedplant")
                    {
                        if (DailyRadio.Checked)
                        {
                            EconomicPlantDailyReportForm myform = new EconomicPlantDailyReportForm(PPID, FromDate, ToDate);
                            myform.ShowDialog();
                        }
                        else if (MonthlyRadio.Checked)
                        {
                            string FromMonth = FromDate.Remove(7);
                            string ToMonth = ToDate.Remove(7);
                            EconomicPlantMonthlyReportForm myform = new EconomicPlantMonthlyReportForm(PPID, FromMonth, ToMonth);
                            myform.ShowDialog();
                        }
                    }
                    else if (myType == "selectedunit")
                    {
                        EconomicUnitReportForm myform = new EconomicUnitReportForm(PPID, FromDate, ToDate);
                        myform.ShowDialog();
                    }

                }
                else if (MaintenanceRadio.Checked)
                {
                    MaintenancePlantReportForm myform = new MaintenancePlantReportForm(PPID);
                    myform.ShowDialog();
                }

            }
        }

        private void FinancialRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (FinancialRadio.Checked)
            {
                DailyRadio.Enabled = true;
                MonthlyRadio.Enabled = true;
                ToDateLb.Enabled = true;
                ToDateCal.Enabled = true;
                FromDateCal.Enabled = true;
                FromDateLb.Visible = true;
                ToDateCal.Visible = true;
                ToDateLb.Visible = true;
                FromDateCal.Visible = true;
            }
            else if (MaintenanceRadio.Checked)
            {
                DailyRadio.Enabled = false;
                MonthlyRadio.Enabled = false;
                ToDateLb.Enabled = false;
                ToDateCal.Enabled = false;
                FromDateCal.Enabled = false;
                FromDateCal.Visible = false;
                FromDateLb.Visible = false;
                ToDateCal.Visible = false;
                ToDateLb.Visible = false;
            }
        }
    }
}
