﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using System.IO;
using Microsoft.Win32;

namespace PowerPlantProject
{
    public partial class Bourse : Form
    {
        public Bourse()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dataGridView2.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void Bourse_Load(object sender, EventArgs e)
        {
            dataGridView2.Columns[0].Width = 17;
            dataGridView2.Columns[0].HeaderText = "";
            datePickerFrom.Text = new PersianDate(DateTime.Now).ToString("d");
            datePickerto.Text = new PersianDate(DateTime.Now.AddDays(1)).ToString("d");
            DataTable c = Utilities.GetTable("select ppid from powerplant");      


            cmbfhour.Text = "1";
            cmbthour.Text = "24";
            fillallunit();
        }
        public void fillallunit()
        {
            try
            {
                dataGridView2.DataSource = null;
                DataTable dd = Utilities.GetTable("select fromdate,todate,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15,h16,h17,h18,h19,h20,h21,h22,h23,h24 from dbo.Bourse order by id asc");
                dataGridView2.DataSource = dd;
            }
            catch
            {

            }
        }

        private void cmbplant_SelectedValueChanged(object sender, EventArgs e)
        {
          
        }

        private void cmbunit_SelectedValueChanged(object sender, EventArgs e)
        {
           
        }
        public void fill()
        {

            try
            {
                DataTable x = Utilities.GetTable("select * from dbo.Bourse where fromdate='" + datePickerFrom.Text.Trim() + "'and todate='" + datePickerto.Text.Trim() + "'ORDER BY id DESC");
                dataGridView1.DataSource = null;
                dataGridView1.Rows.Clear();
                if (x.Rows.Count > 0)
                {
                    for (int i = 0; i < 24; i++)
                    {
                        string r = x.Rows[0][i + 3].ToString().Trim();
                        string val = r;
                        dataGridView1.Rows[0].Cells[i].Value = r.ToString();
                        if (r == "") dataGridView1.Rows[0].Cells[i].Style.BackColor = Color.Gray;
                        else if (val == "Base") dataGridView1.Rows[0].Cells[i].Style.BackColor = Color.Orange;
                        else if (val == "Medium") dataGridView1.Rows[0].Cells[i].Style.BackColor = Color.Green;
                        else if (val == "Peak") dataGridView1.Rows[0].Cells[i].Style.BackColor = Color.Red;

                    }
                }
            }
            catch
            {

            }


        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                bool enter = true;
                string st1 = "insert into Bourse (fromdate,todate,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15,h16,h17,h18,h19,h20,h21,h22,h23,h24)values('" + datePickerFrom.Text.Trim() + "','" + datePickerto.Text.Trim();
                DataTable d = Utilities.GetTable("delete from bourse where  fromdate='" + datePickerFrom.Text + "'and todate='" + datePickerto.Text + "'");
                string st2 = "";
                string v = "";
                for (int i = 0; i < 24; i++)
                {
                    try
                    {
                        v = dataGridView1.Rows[0].Cells[i].Value.ToString().Trim();

                    }
                    catch
                    {
                        enter = false;
                        MessageBox.Show("Please Fill All Hours");
                        break;

                    }


                    st2 += ("','" + v);
                    if (i == 23) st2 += "')";
                }
                if (enter)
                {
                    DataTable ind = Utilities.GetTable(st1 + st2);
                    MessageBox.Show("Saved.");
                    fillallunit();
                    fill();
                }
            }
            catch
            {
            }
          

        }
        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "" || str.Trim() == null)
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private void btnadd_Click(object sender, EventArgs e)
        {
           
            int fromh = int.Parse(cmbfhour.Text.Trim());
            int thour = int.Parse(cmbthour.Text.Trim());
            string status = "";
            if (rdMedium.Checked) status = "Medium";
            else if (rdpeak.Checked) status = "peak";
            else if (rdBase.Checked) status = "Base";

            if (status != "")
            {
                for (int h = fromh; h <= thour; h++)
                {
                    dataGridView1.Rows[0].Cells[h - 1].Value =  status;

                }
            }
        }

      
        private void datePickerFrom_ValueChanged(object sender, EventArgs e)
        {
            fill();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
           
        }

        //private void textBox1_Leave(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DataTable v = utilities.GetTable("select pmax from UnitsDataMain where ppid='" + cmbplant.Text.Trim() + "'and unitcode='" + cmbunit.Text.Trim() + "'order by unitcode asc");
        //        double pmax = MyDoubleParse(v.Rows[0][0].ToString());
        //        if (MyDoubleParse(textBox1.Text) > pmax || MyDoubleParse(textBox1.Text) < 0)
        //        {
        //            textBox1.Text = "";
        //            MessageBox.Show("Please insert  0< value<=pmax");

        //        }
        //    }
        //    catch
        //    {

        //    }
        //}

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowIndex = e.RowIndex;

                string x = dataGridView2.Rows[rowIndex].Cells[e.ColumnIndex].Value.ToString();
                if (x == "System.Drawing.Bitmap")
                {
                    DataTable bb = Utilities.GetTable("delete from dbo.Bourse where  fromdate='" + dataGridView2.Rows[rowIndex].Cells[1].Value.ToString() + "'and todate='" + dataGridView2.Rows[rowIndex].Cells[2].Value.ToString() + "'");
                }
                fillallunit();
            }
            catch
            {
            }
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Billing Wizard Completed.");
            this.Close();

        }

      

       

       
      

     







    }
}
