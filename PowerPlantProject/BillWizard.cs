﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    public partial class BillWizard : Form
    {
        string monthlydate = "";
        public BillWizard()
        {
            InitializeComponent();
        }

        private void BillWizard_Load(object sender, EventArgs e)
        {
            faDatePicker1.Text = new PersianDate(DateTime.Now).ToString("d");
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogResult re1 = openFileDialog1.ShowDialog();
            if (re1 != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName; ;
                fillmonthlybilltotal(path);
                fillmonthlybilldate(path);


                ////////////////////////////////////////////////////////////////
                int num = 31;
                string year = monthlydate.Substring(0, 4);
                string month = monthlydate.Substring(5, 2);
                if (int.Parse(year) % 4 == 3 && month == "12")
                {
                    num = 30;
                }
                else if (month == "12")
                {
                    num = 29;
                }


                for (int i = 1; i <= num; i++)
                {
                    fillmonthlybillplant(path, i);

                }
                MessageBox.Show("Completed");
            }



        }
        public void fillmonthlybilldate(string path)
        {
            string price = "Month";


            Excel.Application exobj = new Excel.Application();
            exobj.Visible = false;
            exobj.UserControl = true;
            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            book.Save();
            book.Close(true, book, Type.Missing);
            exobj.Workbooks.Close();
            exobj.Quit();

            //////////////////////

            //Read From Excel File
            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            objConn.Open();

            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
            objAdapter1.SelectCommand = objCmdSelect;
            DataSet objDataset1 = new DataSet();
            objAdapter1.Fill(objDataset1);
            DataTable TempTable = objDataset1.Tables[0];
            objConn.Close();
            monthlydate = TempTable.Rows[0][3].ToString().Substring(0, 7).Trim();
            DataTable dd = Utilities.GetTable("delete from MonthlyBillDate where substring(date,1,7)='" + monthlydate + "'");

            foreach (DataRow m in TempTable.Rows)
            {
                if (m[1].ToString().Trim() != "")
                {
                    string ppid = m[0].ToString().Trim();
                    string plantname = m[1].ToString().Trim();

                    string n = "";
                    string v = "";

                    DataTable bb = Utilities.GetTable("select distinct code from dbo.MonthlyBillTotal where month='" + monthlydate + "'order by code asc");
                    string[] name = new string[bb.Rows.Count];
                    for (int y = 0; y < name.Length; y++)
                    {
                        name[y] = bb.Rows[y][0].ToString().Trim();


                        if (name[y].Contains("01") || name[y].Contains("02") || name[y].Contains("03") || name[y].Contains("04") || name[y].Contains("05") || name[y].Contains("06") || name[y].Contains("07") || name[y].Contains("08") || name[y].Contains("09"))
                            name[y] = name[y].Replace("0", "").Trim();
                        n += name[y].Trim() + ",";
                        if (y == name.Length - 1)
                        {
                            v += m[y + 4].ToString().Trim();
                        }
                        else
                        {
                            v += m[y + 4].ToString().Trim() + "','";
                        }
                    }
                    n = n.TrimEnd(',');
                    v = v.TrimEnd('"', '"');
                    //string s1 = m[4].ToString().Trim();
                    //string s2 = m[5].ToString().Trim();
                    //string s3 = m[6].ToString().Trim();
                    //string s4 = m[7].ToString().Trim();
                    //string s5 = m[8].ToString().Trim();
                    //string s6 = m[9].ToString().Trim();
                    //string s7 = m[10].ToString().Trim();
                    //string s8 = m[11].ToString().Trim();
                    //string s9 = m[12].ToString().Trim();
                    //string s10 = m[13].ToString().Trim();
                    //string s11 = m[14].ToString().Trim();
                    //string s12 = m[15].ToString().Trim();
                    //string s13 = m[16].ToString().Trim();
                    //string s14 = m[17].ToString().Trim();
                    //string s15 = m[18].ToString().Trim();
                    //string s16 = m[19].ToString().Trim();
                    //string s17 = m[20].ToString().Trim();
                    //string s18 = m[21].ToString().Trim();
                    //string s19 = m[22].ToString().Trim();
                    //string s20 = m[23].ToString().Trim();
                    //string s21 = m[24].ToString().Trim();
                    //string s22 = m[25].ToString().Trim();
                    //string s23 = m[26].ToString().Trim();
                    //string s24 = m[27].ToString().Trim();
                    //string s26 = m[28].ToString().Trim();
                    //string s27 = m[29].ToString().Trim();
                    //string s28 = m[30].ToString().Trim();
                    //string s29 = m[31].ToString().Trim();
                    //string s31 = m[32].ToString().Trim();
                    //string s36 = m[33].ToString().Trim();
                    //string s37 = m[34].ToString().Trim();
                    //string s38 = m[35].ToString().Trim();
                    //string s39 = m[36].ToString().Trim();
                    //string s40 = m[37].ToString().Trim();
                    //string s41 = m[38].ToString().Trim();
                    //string s42 = m[39].ToString().Trim();
                    //string s43 = m[40].ToString().Trim();
                    string ddate = m[3].ToString().Trim();
                    //DataTable d = utilities.GetTable("insert into MonthlyBillDate (ppid,plantname,Date,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15,s16,s17,s18,s19,s20,s21,s22,s23,s24,s26,s27,s28,s29,s31,s36,s37,s38,s39,s40,s41,s42,s43)" +
                    //    "values ('" + ppid + "',N'" + plantname + "','" + ddate + "','" + s1 + "','" + s2 + "','" + s3 + "','" + s4 + "','" + s5 + "','" + s6 + "','" + s7 + "','" + s8 + "','" + s9 + "','" + s10 + "','" + s11 + "','" + s12 + "','" + s13 + "','" + s14 + "','" + s15 + "','" + s16 + "','" + s17 + "','" + s18 + "','" + s19 + "','" + s20 + "','" + s21 + "','" + s22 + "','" + s23 + "','" + s24 + "','" + s26 + "','" + s27 + "','" + s28 + "','" + s29 + "','" + s31 + "','" + s36 + "','" + s37 + "','" + s38 + "','" + s39 + "','" + s40 + "','" + s41 + "','" + s42 + "','" + s43 + "')");

                    DataTable d = Utilities.GetTable("insert into MonthlyBillDate (ppid,plantname,Date," + n + ")" +
                     "values ('" + ppid + "',N'" + plantname + "','" + ddate + "','" + v + "')");


                }



            }


        }
        public void fillmonthlybilltotal(string path)
        {
            string price = "total";

            Excel.Application exobj = new Excel.Application();
            exobj.Visible = false;
            exobj.UserControl = true;
            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            book.Save();
            book.Close(true, book, Type.Missing);
            exobj.Workbooks.Close();
            exobj.Quit();

            //////////////////////

            //Read From Excel File
            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            objConn.Open();

            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
            objAdapter1.SelectCommand = objCmdSelect;
            DataSet objDataset1 = new DataSet();
            objAdapter1.Fill(objDataset1);
            DataTable TempTable = objDataset1.Tables[0];
            objConn.Close();
           // string cxx = openFileDialog1.SafeFileName;

            monthlydate = faDatePicker1.Text.Substring(0, 4) +  faDatePicker1.Text.Substring(4, 3);
            // string date = billdate;
            DataTable dd = Utilities.GetTable("delete from MonthlyBillTotal where month='" + monthlydate + "'");

            foreach (DataRow m in TempTable.Rows)
            {
                if (m[1].ToString().Trim() != "")
                {
                    string code = m[0].ToString().Trim();
                    string item = m[1].ToString().Trim();
                    double value = MyDoubleParse(m[2].ToString());


                    DataTable d = Utilities.GetTable("insert into MonthlyBillTotal (Code,Item,Value,month) values ('" + code + "',N'" + item + "','" + value + "','" + monthlydate + "')");

                }
            }





        }
        public void fillmonthlybillplant(string path, int day)
        {
            string price = "Day_" + day.ToString().Trim();

            Excel.Application exobj = new Excel.Application();
            exobj.Visible = false;
            exobj.UserControl = true;
            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            book.Save();
            book.Close(true, book, Type.Missing);
            exobj.Workbooks.Close();
            exobj.Quit();

            //////////////////////

            //Read From Excel File
            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            objConn.Open();

            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
            objAdapter1.SelectCommand = objCmdSelect;
            DataSet objDataset1 = new DataSet();
            objAdapter1.Fill(objDataset1);
            DataTable TempTable = objDataset1.Tables[0];
            objConn.Close();

            string date = TempTable.Rows[0][3].ToString();
            DataTable dd = Utilities.GetTable("delete from MonthlyBillPlant where date='" + date + "'");

            foreach (DataRow m in TempTable.Rows)
            {
                if (m[1].ToString().Trim() != "")
                {
                    string ppid = m[0].ToString().Trim();
                    string plantname = m[1].ToString().Trim();

                    string n = "";
                    string v = "";

                    DataTable bb = Utilities.GetTable("select distinct code from dbo.MonthlyBillTotal where month='" + date.Substring(0, 7).Trim() + "'  order by code asc");
                    string[] name = new string[bb.Rows.Count];
                    for (int y = 0; y < name.Length; y++)
                    {
                        name[y] = bb.Rows[y][0].ToString().Trim();

                        if (name[y].Contains("01") || name[y].Contains("02") || name[y].Contains("03") || name[y].Contains("04") || name[y].Contains("05") || name[y].Contains("06") || name[y].Contains("07") || name[y].Contains("08") || name[y].Contains("09"))
                            name[y] = name[y].Replace("0", "").Trim();
                        n += name[y].Trim() + ",";
                        if (y == name.Length - 1) v += m[y + 5].ToString().Trim();
                        else v += m[y + 5].ToString().Trim() + "','";
                    }
                    n = n.TrimEnd(',');
                    v = v.TrimEnd('"', '"');
                    double hour = MyDoubleParse(m[4].ToString());
                    //string s1 = m[5].ToString().Trim();
                    //string s2 = m[6].ToString().Trim();
                    //string s3 = m[7].ToString().Trim();
                    //string s4 = m[8].ToString().Trim();
                    //string s5 = m[9].ToString().Trim();
                    //string s6 = m[10].ToString().Trim();
                    //string s7 = m[11].ToString().Trim();
                    //string s8 = m[12].ToString().Trim();
                    //string s9 = m[13].ToString().Trim();
                    //string s10 = m[14].ToString().Trim();
                    //string s11 = m[15].ToString().Trim();
                    //string s12 = m[16].ToString().Trim();
                    //string s13 = m[17].ToString().Trim();
                    //string s14 = m[18].ToString().Trim();
                    //string s15 = m[19].ToString().Trim();
                    //string s16 = m[20].ToString().Trim();
                    //string s17 = m[21].ToString().Trim();
                    //string s18 = m[22].ToString().Trim();
                    //string s19 = m[23].ToString().Trim();
                    //string s20 = m[24].ToString().Trim();
                    //string s21 = m[25].ToString().Trim();
                    //string s22 = m[26].ToString().Trim();
                    //string s23 = m[27].ToString().Trim();
                    //string s24 = m[28].ToString().Trim();
                    //string s26 = m[29].ToString().Trim();
                    //string s27 = m[30].ToString().Trim();
                    //string s28 = m[31].ToString().Trim();
                    //string s29 = m[32].ToString().Trim();
                    //string s31 = m[33].ToString().Trim();
                    //string s36 = m[34].ToString().Trim();
                    //string s37 = m[35].ToString().Trim();
                    //string s38 = m[36].ToString().Trim();
                    //string s39 = m[37].ToString().Trim();
                    //string s40 = m[38].ToString().Trim();
                    //string s41 = m[39].ToString().Trim();
                    //string s42 = m[40].ToString().Trim();
                    //string s43 = m[41].ToString().Trim();
                    //DataTable d = utilities.GetTable("insert into MonthlyBillPlant (ppid,plantname,Date,hour,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15,s16,s17,s18,s19,s20,s21,s22,s23,s24,s26,s27,s28,s29,s31,s36,s37,s38,s39,s40,s41,s42,s43) values ('" + ppid + "',N'" + plantname + "','" + date + "','" + hour + "','" + s1 + "','" + s2 + "','" + s3 + "','" + s4 + "','" + s5 + "','" + s6 + "','" + s7 + "','" + s8 + "','" + s9 + "','" + s10 + "','" + s11 + "','" + s12 + "','" + s13 + "','" + s14 + "','" + s15 + "','" + s16 +
                    //"','" + s17 + "','" + s18 + "','" + s19 + "','" + s20 + "','" + s21 + "','" + s22 + "','" + s23 + "','" + s24 + "','" + s26 + "','" + s27 + "','" + s28 + "','" + s29 + "','" + s31 + "','" + s36 + "','" + s37 + "','" + s38 + "','" + s39 + "','" + s40 + "','" + s41 + "','" + s42 + "','" + s43 + "')");
                    DataTable d = Utilities.GetTable("insert into MonthlyBillPlant (ppid,plantname,Date,hour," + n + ")" +
                    "values ('" + ppid + "',N'" + plantname + "','" + date + "','" + hour + "','" + v + "')");


                }
            }





        }


        private double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }

     
        private void panel1_Click(object sender, EventArgs e)
        {

            DataTable d3 = Utilities.GetTable("select p from dbo.DetailFRM009 where TargetMarketDate='" + faDatePicker1.Text.Trim() + "'");
            DataTable d2 = Utilities.GetTable("select * from DetailFRM002 where TargetMarketDate='" + faDatePicker1.Text.Trim() + "'and estimated='0'");
            DataTable d5 = Utilities.GetTable("select * from DetailFRM005 where TargetMarketDate='" + faDatePicker1.Text.Trim() + "'");
            if (d3.Rows.Count > 24 && d2.Rows.Count > 24 && d5.Rows.Count > 24)
            {
                this.Close();
                BillItem m = new BillItem();
                m.panel1.Visible = true;
                m.Show();
            }
            else
            {
                this.Close();
                FormDownloadInterval m = new FormDownloadInterval();
                m.panel4.Visible = true;
                m.Show();
            }

           

        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogResult re1 = openFileDialog1.ShowDialog();
            if (re1 != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName; ;
                fillyearhub(path);
                MessageBox.Show("Save.");
            }
        }

        public void fillyearhub(string path)
        {
            string price = "Gen";


            Excel.Application exobj = new Excel.Application();
            exobj.Visible = false;
            exobj.UserControl = true;
            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            book.Save();
            book.Close(true, book, Type.Missing);
            exobj.Workbooks.Close();
            exobj.Quit();

            //////////////////////

            //Read From Excel File
            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            objConn.Open();

            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
            objAdapter1.SelectCommand = objCmdSelect;
            DataSet objDataset1 = new DataSet();
            objAdapter1.Fill(objDataset1);
            DataTable TempTable = objDataset1.Tables[0];
            objConn.Close();
            string year = openFileDialog1.SafeFileName.Replace("Usages-final", "").Substring(0, 5).Trim();


            foreach (DataRow m in TempTable.Rows)
            {
                if (m[1].ToString().Trim() != "")
                {
                    string ppid = m[0].ToString().Trim();
                    string plantname = m[1].ToString().Trim();
                    DataTable xx = Utilities.GetTable("select distinct PPID from dbo.PowerPlant");
                    foreach (DataRow b in xx.Rows)
                    {
                        if (ppid == b[0].ToString().Trim())
                        {
                            string vv = "";
                            int i = 2;
                            while (i <= 38)
                            {
                                if (i == 38)
                                {
                                    vv += m[i].ToString().Trim();
                                }
                                else
                                {
                                    vv += m[i].ToString().Trim() + "','" + m[i + 1].ToString().Trim() + "','" + m[i + 2].ToString().Trim() + "','";
                                }
                                i = i + 3;
                            }



                            DataTable dd = Utilities.GetTable("delete from yearhub where year='" + year + "'and ppid='" + ppid + "'");

                            DataTable d = Utilities.GetTable("insert into yearhub (year,ppid,M1Low,M1Mid,M1Peak,M2Low,M2Mid,M2Peak,M3Low,M3Mid,M3Peak,M4Low,M4Mid,M4Peak,M5Low,M5Mid,M5Peak,M6Low,M6Mid,M6Peak,M7Low,M7Mid,M7Peak,M8Low,M8Mid,M8Peak,M9Low,M9Mid,M9Peak,M10Low,M10Mid,M10Peak,M11Low,M11Mid,M11Peak,M12Low,M12Mid,M12Peak,avg)" +
                                "values ('" + year + "',N'" + ppid + "','" + vv + "')");

                        }
                    }


                }



            }
        }


        public void fillyeartrans(string path)
        {
            string price = "Gen";


            Excel.Application exobj = new Excel.Application();
            exobj.Visible = false;
            exobj.UserControl = true;
            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            book.Save();
            book.Close(true, book, Type.Missing);
            exobj.Workbooks.Close();
            exobj.Quit();

            //////////////////////

            //Read From Excel File
            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            objConn.Open();

            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
            objAdapter1.SelectCommand = objCmdSelect;
            DataSet objDataset1 = new DataSet();
            objAdapter1.Fill(objDataset1);
            DataTable TempTable = objDataset1.Tables[0];
            objConn.Close();
            string year = openFileDialog1.SafeFileName.Replace("Loss-final", "").Substring(0, 5).Trim();


            foreach (DataRow m in TempTable.Rows)
            {
                if (m[1].ToString().Trim() != "")
                {
                    string ppid = m[0].ToString().Trim();
                    string plantname = m[1].ToString().Trim();
                    DataTable xx = Utilities.GetTable("select distinct PPID from dbo.PowerPlant");
                    foreach (DataRow b in xx.Rows)
                    {
                        if (ppid == b[0].ToString().Trim())
                        {
                            string vv = "";
                            int i = 2;
                            while (i <= 38)
                            {
                                if (i == 38)
                                {
                                    vv += m[i].ToString().Trim();
                                }
                                else
                                {
                                    vv += m[i].ToString().Trim() + "','" + m[i + 1].ToString().Trim() + "','" + m[i + 2].ToString().Trim() + "','";
                                }
                                i = i + 3;
                            }



                            DataTable dd = Utilities.GetTable("delete from yeartrans where year='" + year + "'and ppid='" + ppid + "'");

                            DataTable d = Utilities.GetTable("insert into yeartrans (year,ppid,M1Low,M1Mid,M1Peak,M2Low,M2Mid,M2Peak,M3Low,M3Mid,M3Peak,M4Low,M4Mid,M4Peak,M5Low,M5Mid,M5Peak,M6Low,M6Mid,M6Peak,M7Low,M7Mid,M7Peak,M8Low,M8Mid,M8Peak,M9Low,M9Mid,M9Peak,M10Low,M10Mid,M10Peak,M11Low,M11Mid,M11Peak,M12Low,M12Mid,M12Peak,avg)" +
                                "values ('" + year + "',N'" + ppid + "','" + vv + "')");

                        }
                    }


                }



            }
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogResult re1 = openFileDialog1.ShowDialog();
            if (re1 != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName; ;
                fillyeartrans(path);
                MessageBox.Show("Save.");
            }
        }

       

       
    }
}
