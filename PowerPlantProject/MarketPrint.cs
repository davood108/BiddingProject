﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Common;
using PowerPlantProject;
using FarsiLibrary.Utils;
using NRI.SBS.Common;
using System.Collections;

namespace PowerPlantProject
{
    public partial class MarketPrint : Form
    {
        DataTable  Dg1 = null;
        string pname;
        string unit;
        string package;
        string Date;

         public MarketPrint(DataTable dg1,string Pname,string Unit,string Package,string date)
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;
                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dgv.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }

            Dg1 = dg1;
            pname = Pname;
            unit = Unit;
            package = Package;
            Date = date;

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
          
            dgv.DataSource = Dg1;
            //PrintDGV printclass = new PrintDGV(label1.Text);

            //printclass.Print_DataGridView(dgv);
            if (chkland.Checked)
            {
                PrintDGV.landscape = true;
            }

            PrintDGV.info(label1.Text);
            PrintDGV.Print_DataGridView(dgv);
           
       
                       
        }

        private void MarketPrint_Load(object sender, EventArgs e)
        {
            label1.Text = "         Plant Name :  " + pname + "        Package :  " + package + " \r\n          Block :  " + unit + "            Date :  " + Date;

            dgv.DataSource = Dg1;
           
        }
    }
}
