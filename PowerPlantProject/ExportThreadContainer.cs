﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    public class ExportThreadContainer
    {
        string path = "";
        string biddingDate;
        string lblPlantValue;
        
        public ExportThreadContainer(string Path, string BiddingDate, string lablePlant)
        {
            path = Path;
            biddingDate = BiddingDate;
            lblPlantValue = lablePlant;
        }

        public void LongTask_Export()
        {
            int success = 0;
            if (lblPlantValue.ToLower() != "all")
            {
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = new SqlConnection(ConnectionManager.ConnectionString);
                MyCom.Connection.Open();

                MyCom.CommandText = "SELECT @num=PPID, @nameFarsi=PNameFarsi FROM PowerPlant WHERE PPName=@name";
                MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@name", SqlDbType.NChar, 20);
                MyCom.Parameters["@num"].Direction = ParameterDirection.Output;
                MyCom.Parameters["@name"].Value = lblPlantValue;
                MyCom.Parameters.Add("@nameFarsi", SqlDbType.NVarChar, 50);
                MyCom.Parameters["@nameFarsi"].Direction = ParameterDirection.Output;
                MyCom.ExecuteNonQuery();
                MyCom.Connection.Close();
                try
                {
                    ExportM002Excel(path,
                        int.Parse(MyCom.Parameters["@num"].Value.ToString().Trim()),
                        MyCom.Parameters["@nameFarsi"].Value.ToString().Trim());
                    success++;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                DataTable dt = Utilities.GetTable("select * from powerplant");
                foreach (DataRow row in dt.Rows)
                {
                    try
                    {
                        ExportM002Excel(path,
                            int.Parse(row["PPID"].ToString().Trim()),
                            row["PNameFarsi"].ToString().Trim());
                        success++;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }
            }
            //if (success>0)
            //    MessageBox.Show("Export of " + success.ToString() + " file(s) to " + path + " completed successfully!");
            /////////////////
            if (success > 0)
            {

                MessageBox.Show("Export of " + success.ToString() + " file(s) to " + path + " completed successfully!");

                //----------------------------------copy to real---------------------------------------------------------

                //int coping = 0;
                //DialogResult Result;
                //Result = MessageBox.Show("Do you Want To Copy Estimated Bid In Real Bid in Date : " + biddingDate + " And Plant :" + lblPlantValue + " ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //if (Result == DialogResult.Yes)
                //{
                //    DataTable dd = null;
                //    DataTable blockdd = null;
                //    if (lblPlantValue.Trim() != "All")
                //    {
                //        dd = utilities.GetTable("select PPID from dbo.PowerPlant where PPName='" + lblPlantValue + "'");

                //    }
                //    else
                //    {
                //        dd = utilities.GetTable("select * from powerplant");

                //    }
                //    foreach (DataRow midrow in dd.Rows)
                //    {

                //        blockdd = utilities.GetTable("select distinct Block from dbo.DetailFRM002 where PPID='" + midrow[0].ToString().Trim() + "' and TargetMarketDate='" + biddingDate + "'and Estimated=1");
                //        foreach (DataRow mrow in blockdd.Rows)
                //        {

                //            DataTable dt = utilities.GetTable("select * from dbo.DetailFRM002 where PPID='" + midrow[0].ToString().Trim() + "' and TargetMarketDate='" + biddingDate + "' and Block='" + mrow[0].ToString().Trim() + "'and Estimated=1 ORDER BY Hour ASC");

                //            DataTable dcheck = utilities.GetTable("select * from dbo.DetailFRM002 where PPID='" + midrow[0].ToString().Trim() + "' and TargetMarketDate='" + biddingDate + "' and Block='" + mrow[0].ToString().Trim() + "'and Estimated=0");

                //            if (dcheck.Rows.Count == 0 && dt.Rows.Count == 24)
                //            {
                //                DataTable dVALDEL = utilities.GetTable("delete from dbo.DetailFRM002 where PPID='" + midrow[0].ToString().Trim() + "' and TargetMarketDate='" + biddingDate + "'and Estimated=0  and Block='" + mrow[0].ToString().Trim() + "'");


                //                foreach (DataRow mrow1 in dt.Rows)
                //                {
                //                    string Date = mrow1["TargetMarketDate"].ToString().Trim();
                //                    string Plant = mrow1["PPID"].ToString().Trim();
                //                    string pptype = mrow1["PPType"].ToString().Trim();
                //                    string block = mrow1["Block"].ToString().Trim();
                //                    string hour = mrow1["Hour"].ToString().Trim();
                //                    string estimated = "0";
                //                    string declaredcapacity = mrow1["DeclaredCapacity"].ToString().Trim();
                //                    string dispatchablecapacity = mrow1["DispachableCapacity"].ToString().Trim();
                //                    string Power1 = mrow1["Power1"].ToString().Trim();
                //                    string Power2 = mrow1["Power2"].ToString().Trim();
                //                    string Power3 = mrow1["Power3"].ToString().Trim();
                //                    string Power4 = mrow1["Power4"].ToString().Trim();
                //                    string Power5 = mrow1["Power5"].ToString().Trim();
                //                    string Power6 = mrow1["Power6"].ToString().Trim();
                //                    string Power7 = mrow1["Power7"].ToString().Trim();
                //                    string Power8 = mrow1["Power8"].ToString().Trim();
                //                    string Power9 = mrow1["Power9"].ToString().Trim();
                //                    string Power10 = mrow1["Power10"].ToString().Trim();
                //                    string price1 = mrow1["Price1"].ToString().Trim();
                //                    string price2 = mrow1["Price2"].ToString().Trim();
                //                    string price3 = mrow1["Price3"].ToString().Trim();
                //                    string price4 = mrow1["Price4"].ToString().Trim();
                //                    string price5 = mrow1["Price5"].ToString().Trim();
                //                    string price6 = mrow1["Price6"].ToString().Trim();
                //                    string price7 = mrow1["Price7"].ToString().Trim();
                //                    string price8 = mrow1["Price8"].ToString().Trim();
                //                    string price9 = mrow1["Price9"].ToString().Trim();
                //                    string price10 = mrow1["Price10"].ToString().Trim();

                //                    try
                //                    {
                //                        DataTable dinsert = utilities.GetTable("insert INTO DetailFRM002 (TargetMarketDate,PPID,PPType,Block,Hour,Estimated,DeclaredCapacity,DispachableCapacity"
                //                            + ",Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10) VALUES ('" + Date + "','" + Plant + "','" + pptype + "','" + block +
                //                            "','" + hour + "','" + estimated + "','" + declaredcapacity + "','" + dispatchablecapacity + "','" + Power1 + "','" + price1 + "','" + Power2 + "','" + price2 + "','" + Power3 + "','" + price3 + "','" + Power4 + "','" + price4 + "','" + Power5 + "','" + price5 +
                //                            "','" + Power6 + "','" + price6 + "','" + Power7 + "','" + price7 + "','" + Power8 + "','" + price8 + "','" + Power9 + "','" + price9 + "','" + Power10 + "','" + price10 + "')");

                //                        coping++;
                //                    }
                //                    catch
                //                    {


                //                    }


                //                }
                //            }

                //        }

                //    }

                //    if (coping == blockdd.Rows.Count * dd.Rows.Count * 24 && lblPlantValue.Trim() != "All")
                //    {
                //        MessageBox.Show("Finished Successfully ");
                //    }
                //    if (lblPlantValue.Trim() == "All")
                //    {

                //        MessageBox.Show("Finished Successfully");
                //    }


                //}


            }






            //Excel.Application exobj1 = new Excel.Application();
            //exobj1.Visible = true;
            //exobj1.UserControl = true;
            //System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            //Excel.Workbook book1 = null;
            //book1 = exobj1.Workbooks.Open("C://test.xls", 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            //book1.Save();
            //book1.Close(true, book1, Type.Missing);
            //exobj1.Workbooks.Close();
            //exobj1.Quit();


            //////////////////
        }
        private void ExportM002Excel(string path, int ppid, string ppnameFarsi)
        {
            List<string> Packages = new List<string>();

            string cmd = "select distinct PackageType from dbo.UnitsDataMain where PPID='" + ppid.ToString() + "'"+
" AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%') AND (UnitCode not like 'Gas cc%')))";
            DataTable oDataTable = Utilities.GetTable(cmd);
            foreach (DataRow row in oDataTable.Rows )
            {
                Packages.Add(row["PackageType"].ToString().Trim());
            }

           // string[] headers = { "Block/Unit No", "Peak", "Max. Daily Generation", "Hour","Bourse","Bilat",
                                  // "Declared Capacity", "Dispatchable Capacity"};old


            string[] headers = { "Block/Unit No", "Peak", "Max. Daily Generation", "Hour",
                                   "Declared Capacity","Limitedto", "Dispatchable Capacity"};

            string[] firstColValues = { "Form Name :", "Date Of Issue :", "Time Of Issue :", "Target Market Date :",
                                    "Power Plant Name :", "Code :", "Revision :", 
                                    "Filled By :", "Approved By :" };

            for (int packageIndex =0; packageIndex<Packages.Count; packageIndex++)
            {
                int rowIndex = 0;
                int colIndex = 50;

                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                Excel.Application oExcel = new Excel.Application();

                oExcel.SheetsInNewWorkbook = 1;
                Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
                Excel.Worksheet sheet = null;
                Excel.Range range = null;

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers




                rowIndex = 0;
                colIndex = 0;
                int sheetIndex = 1;
                if (sheet != null)
                    sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
                else
                    sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

                sheet.Name = "Sheet" + (sheetIndex);

                int ppidToPrint = ppid;
                string ppnameFarsiModified = ppnameFarsi;

                if (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count>1)
                {
                    ppnameFarsiModified = "سيكل تركيبي" + " " + ppnameFarsiModified;
                    ppidToPrint++;
                }

                string[] secondColValues = { "M0022", 
                                          DateTime.Now.ToString("d"), DateTime.Now.ToString("T"),
                                          biddingDate,
                                          ppnameFarsiModified, ppidToPrint.ToString(), 
                                          "0", "", "" };


                for (int i = 0; i < firstColValues.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[i + 1, 1];
                    range.Value2 = firstColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    range = (Excel.Range)sheet.Cells[i + 1, 2];
                    range.Value2 = secondColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


                rowIndex = firstColValues.Length + 2;
                colIndex = 0;
                for (int i = 0; i < headers.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = headers[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
                for (int i = 1; i <= 10; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = "Power_" + i.ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = "Price_" + i.ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                ////////////////////////////////////////b tartib horofe alefba////////////////////////////////////////////////////

                //string strCmd = "select distinct block from DetailFRM002 where ppid='" + ppid.ToString() +
                //"' AND TargetMarketDate='" + biddingDate + "'and Estimated=1 " +
                //" and pptype=" + (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count > 1 ? 1 : 0).ToString();
                ////if (mixUnitStatus == MixUnits.JustCombined)
                ////    strCmd += " AND block like 'C%'";

                ////else if (mixUnitStatus == MixUnits.ExeptCombined)
                ////    strCmd += " AND block like 'S%'";
                //strCmd += " order by block";

                //oDataTable = utilities.GetTable(strCmd);

                //string[] blocks = new string[oDataTable.Rows.Count];

                //for (int i = 0; i < oDataTable.Rows.Count; i++)
                //    blocks[i] = oDataTable.Rows[i][0].ToString().Trim();

                //Array.Sort(blocks);

                //----------------------------------------------------------------------------
                //-------------------------g11-g12-s1---------------------------------------------
                string strCmd = "";
                string packname1 = Packages[packageIndex];
                bool ismix = true;
                for (int u = 0; u < Packages.Count; u++)
                {
                    if (Packages[u] == "CC")
                    {
                        ismix = false;
                        break;
                    }

                }                
                string[] blocks1;

                if (ismix) blocks1 = block002(ppid.ToString());

                else
                    blocks1 = Getblock002pack(ppid, packname1);




                string[] blocks = new string[blocks1.Length];

             
                int y = 0;

                int yy = 0;

                while (yy < blocks1.Length)
                {


                  
                    blocks[y] = blocks1[yy].Trim();
                    if (blocks[y].Contains("-C")) blocks[y] = blocks[y].Replace("-C", "").Trim();
                    if (y < blocks1.Length)
                    {
                        y++;
                    }

                    yy++;
                }
              


                ///////////////////////////////////////////////////////////////////////
                //int y = 0;
                //for (int ii = 0; ii < Enum.GetNames(typeof(PackageTypePriority)).Length; ii++)
                //{
                //    int yy = 0;

                //    while (yy < oDataTable.Rows.Count)
                //    {

                //        PackageTypePriority MMpppo = (PackageTypePriority)Enum.Parse(typeof(PackageTypePriority), ii.ToString());
                //        if (MMpppo.ToString().Substring(0, 1) == oDataTable.Rows[yy][0].ToString().Trim().Substring(0, 1))
                //        {

                //            blocks[y] = oDataTable.Rows[yy][0].ToString().Trim();
                //            if (y < oDataTable.Rows.Count)
                //            {
                //                y++;
                //            }
                //        }
                //        yy++;
                //    }
                //}
             
             ///////////////////////////////// JUST FOR LOWSHAN///////////////////////////////////////////
                if (ppid == 206)
                {
                    blocks = LOWSHANblock002(ppid.ToString());
                }
              ///////////////////////////////////////////////////////////////////////////////////////////////

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                rowIndex = firstColValues.Length + 3;
                foreach (string blockName in blocks)
                {
                   

                    int firstLine = rowIndex;
                    range = (Excel.Range)sheet.Cells[rowIndex, 1];
                    range.Value2 = blockName;
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    ////////!!!!!!! Peak!!!>???????????????
                    string pacblock = "0";
                    if (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count > 1) pacblock = "1";



                    strCmd = "select * from DetailFRM002 where ppid='" + ppid.ToString() + "'" +
                        " AND Block='" + blockName + "'" +
                        " AND TargetMarketDate='" + biddingDate + "'" +
                        " AND Estimated=1" +
                        " and pptype='"+pacblock+"'order by block, hour";
                    oDataTable = Utilities.GetTable(strCmd);

                    colIndex = 4;

                    double max = 0;
                    double sum = 0;
                    foreach (DataRow row in oDataTable.Rows)
                    {
                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = (int.Parse(row["Hour"].ToString().Trim())).ToString();
                        //range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;



                        ////-------------------------BOURSE-------------------------------//
                        //range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        //range.Value2 = "0";
                        /////////////////////////////////////////////////////////////////////////
                        ////-------------------------BILAT-------------------------------//
                        //range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        //range.Value2 = "0";
                        /////////////////////////////////////////////////////////////////////////

                        //////////////////bourse////////////////////////////////////
                        //DataTable zx = utilities.GetTable("select H" + ((int.Parse(row["Hour"].ToString().Trim())).ToString()) + " from dbo.powersetting where Unit='" + blockName + "' and PPID='" + ppid.ToString() + "'and fromdate<='" + biddingDate + "' and todate>='" + biddingDate + "' and type='Exchange'order by fromdate,id desc");

                        //range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        //if (zx.Rows.Count > 0) range.Value2 =Math.Round(MyDoubleParse( zx.Rows[0][0].ToString()),2).ToString();
                        //else range.Value2 = 0;
                        
                        ///////////////////////////////////////////////////////////////
                        ////////////////////bilat////////////////////////////////////
                        //DataTable zx2 = utilities.GetTable("select H" + ((int.Parse(row["Hour"].ToString().Trim())).ToString()) + " from dbo.powersetting where Unit='" + blockName + "' and PPID='" + ppid.ToString() + "'and fromdate<='" + biddingDate + "' and todate>='" + biddingDate + "' and type='Contract'order by fromdate,id desc");

                        //range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        //if (zx2.Rows.Count > 0) range.Value2 = Math.Round(MyDoubleParse(zx2.Rows[0][0].ToString()),2).ToString();
                        //else range.Value2 = 0;
                        /////////////////////////////////////////////////////////////
                        


                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 =Math.Round(MyDoubleParse( row["DeclaredCapacity"].ToString()),2).ToString().Trim();

                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = "";

                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 =Math.Round( MyDoubleParse(row["DispachableCapacity"].ToString()),2).ToString().Trim();
                        double dC = double.Parse(Math.Round(MyDoubleParse(row["DispachableCapacity"].ToString()),2).ToString().Trim());

                        sum += dC;
                        if (dC > max)
                            max = dC;

                        for (int i = 1; i <= 10; i++)
                        {
                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 =Math.Round(MyDoubleParse(row["Power" + i.ToString()].ToString()),2).ToString().Trim();

                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 =Math.Round(MyDoubleParse(row["Price" + i.ToString()].ToString()),0).ToString().Trim();
                        }

                        rowIndex++;
                        colIndex = 4;
                    }

                    range = (Excel.Range)sheet.Cells[firstLine, 2];
                    range.Value2 = max.ToString();

                    range = (Excel.Range)sheet.Cells[firstLine, 3];
                    range.Value2 = sum.ToString();
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                sheet = ((Excel.Worksheet)WB.Worksheets["Sheet1"]);

                range = (Excel.Range)sheet.Cells[1, 1];
                range = range.EntireColumn;
                range.AutoFit();

                range = (Excel.Range)sheet.Cells[1, 2];
                range = range.EntireColumn;
                range.AutoFit();

                sheet.Activate();
                //oExcel.Visible = true;

                string strDate = biddingDate;
                strDate = strDate.Remove(7, 1);
                strDate = strDate.Remove(4, 1);
                string filePath = path + "\\" + ppidToPrint.ToString() + "-" + strDate + ".xls";
                string makename = NameFormat(biddingDate, ppidToPrint.ToString(), ppnameFarsi);                
                if (makename != "") filePath = path + "\\" + makename + ".xls";
             


                //oExcel.Visible = true;
                WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                WB.Close(Missing.Value, Missing.Value, Missing.Value);
                oExcel.Workbooks.Close();
                oExcel.Quit();

            } 

        }
        public double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private void floatExportM002Excel(string path, int ppid, string ppnameFarsi)
        {
            List<string> Packages = new List<string>();

            string cmd = "select distinct PackageType from dbo.UnitsDataMain where PPID='" + ppid.ToString() + "'" +
" AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%') AND (UnitCode not like 'Gas cc%')))";
            DataTable oDataTable = Utilities.GetTable(cmd);
            foreach (DataRow row in oDataTable.Rows)
            {
                Packages.Add(row["PackageType"].ToString().Trim());
            }

            string[] headers = { "Block/Unit No", "Peak", "Max. Daily Generation", "Hour","Bourse","Bilat",
                                   "Declared Capacity", "Dispatchable Capacity"};

            string[] firstColValues = { "Form Name :", "Date Of Issue :", "Time Of Issue :", "Target Market Date :",
                                    "Power Plant Name :", "Code :", "Revision :", 
                                    "Filled By :", "Approved By :" };

            for (int packageIndex = 0; packageIndex < Packages.Count; packageIndex++)
            {
                int rowIndex = 0;
                int colIndex = 50;

                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                Excel.Application oExcel = new Excel.Application();

                oExcel.SheetsInNewWorkbook = 1;
                Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
                Excel.Worksheet sheet = null;
                Excel.Range range = null;

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers




                rowIndex = 0;
                colIndex = 0;
                int sheetIndex = 1;
                if (sheet != null)
                    sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
                else
                    sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

                sheet.Name = "Sheet" + (sheetIndex);

                int ppidToPrint = ppid;
                string ppnameFarsiModified = ppnameFarsi;

                if (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count > 1)
                {
                    ppnameFarsiModified = "سيكل تركيبي" + " " + ppnameFarsiModified;
                    ppidToPrint++;
                }

                string[] secondColValues = { "M0022", 
                                          DateTime.Now.ToString("d"), DateTime.Now.ToString("T"),
                                          biddingDate,
                                          ppnameFarsiModified, ppidToPrint.ToString(), 
                                          "0", "", "" };


                for (int i = 0; i < firstColValues.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[i + 1, 1];
                    range.Value2 = firstColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    range = (Excel.Range)sheet.Cells[i + 1, 2];
                    range.Value2 = secondColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


                rowIndex = firstColValues.Length + 2;
                colIndex = 0;
                for (int i = 0; i < headers.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = headers[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
                for (int i = 1; i <= 10; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = "Power_" + i.ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = "Price_" + i.ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                ////////////////////////////////////////b tartib horofe alefba////////////////////////////////////////////////////

                //string strCmd = "select distinct block from DetailFRM002 where ppid='" + ppid.ToString() +
                //"' AND TargetMarketDate='" + biddingDate + "'and Estimated=1 " +
                //" and pptype=" + (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count > 1 ? 1 : 0).ToString();
                ////if (mixUnitStatus == MixUnits.JustCombined)
                ////    strCmd += " AND block like 'C%'";

                ////else if (mixUnitStatus == MixUnits.ExeptCombined)
                ////    strCmd += " AND block like 'S%'";
                //strCmd += " order by block";

                //oDataTable = utilities.GetTable(strCmd);

                //string[] blocks = new string[oDataTable.Rows.Count];

                //for (int i = 0; i < oDataTable.Rows.Count; i++)
                //    blocks[i] = oDataTable.Rows[i][0].ToString().Trim();

                //Array.Sort(blocks);

                //----------------------------------------------------------------------------
                //-------------------------g11-g12-s1---------------------------------------------
                string strCmd = "";
                string packname1 = Packages[packageIndex];
                bool ismix = true;
                for (int u = 0; u < Packages.Count; u++)
                {
                    if (Packages[u] == "CC")
                    {
                        ismix = false;
                        break;
                    }

                }
                string[] blocks1;

                if (ismix) blocks1 = block002(ppid.ToString());

                else
                    blocks1 = Getblock002pack(ppid, packname1);




                string[] blocks = new string[blocks1.Length];


                int y = 0;

                int yy = 0;

                while (yy < blocks1.Length)
                {



                    blocks[y] = blocks1[yy].Trim();
                    if (blocks[y].Contains("-C")) blocks[y] = blocks[y].Replace("-C", "").Trim();
                    if (y < blocks1.Length)
                    {
                        y++;
                    }

                    yy++;
                }



                ///////////////////////////////////////////////////////////////////////
                //int y = 0;
                //for (int ii = 0; ii < Enum.GetNames(typeof(PackageTypePriority)).Length; ii++)
                //{
                //    int yy = 0;

                //    while (yy < oDataTable.Rows.Count)
                //    {

                //        PackageTypePriority MMpppo = (PackageTypePriority)Enum.Parse(typeof(PackageTypePriority), ii.ToString());
                //        if (MMpppo.ToString().Substring(0, 1) == oDataTable.Rows[yy][0].ToString().Trim().Substring(0, 1))
                //        {

                //            blocks[y] = oDataTable.Rows[yy][0].ToString().Trim();
                //            if (y < oDataTable.Rows.Count)
                //            {
                //                y++;
                //            }
                //        }
                //        yy++;
                //    }
                //}

                ///////////////////////////////// JUST FOR LOWSHAN///////////////////////////////////////////
                if (ppid == 206)
                {
                    blocks = LOWSHANblock002(ppid.ToString());
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                rowIndex = firstColValues.Length + 3;
                foreach (string blockName in blocks)
                {


                    int firstLine = rowIndex;
                    range = (Excel.Range)sheet.Cells[rowIndex, 1];
                    range.Value2 = blockName;
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    ////////!!!!!!! Peak!!!>???????????????
                    string pacblock = "0";
                    if (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count > 1) pacblock = "1";



                    strCmd = "select * from DetailFRM002 where ppid='" + ppid.ToString() + "'" +
                        " AND Block='" + blockName + "'" +
                        " AND TargetMarketDate='" + biddingDate + "'" +
                        " AND Estimated=1" +
                        " and pptype='" + pacblock + "'order by block, hour";
                    oDataTable = Utilities.GetTable(strCmd);

                    colIndex = 4;

                    double max = 0;
                    double sum = 0;
                    foreach (DataRow row in oDataTable.Rows)
                    {
                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = (int.Parse(row["Hour"].ToString().Trim())).ToString();
                        //range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;



                        ////-------------------------BOURSE-------------------------------//
                        //range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        //range.Value2 = "0";
                        /////////////////////////////////////////////////////////////////////////
                        ////-------------------------BILAT-------------------------------//
                        //range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        //range.Value2 = "0";
                        /////////////////////////////////////////////////////////////////////////

                        //////////////////bourse////////////////////////////////////
                        DataTable zx = Utilities.GetTable("select H" + ((int.Parse(row["Hour"].ToString().Trim())).ToString()) + " from dbo.powersetting where Unit='" + blockName + "' and PPID='" + ppid.ToString() + "'and fromdate<='" + biddingDate + "' and todate>='" + biddingDate + "' and type='Exchange'order by fromdate,id desc");

                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        if (zx.Rows.Count > 0) range.Value2 = zx.Rows[0][0].ToString();
                        else range.Value2 = 0;

                        /////////////////////////////////////////////////////////////
                        //////////////////bilat////////////////////////////////////
                        DataTable zx2 = Utilities.GetTable("select H" + ((int.Parse(row["Hour"].ToString().Trim())).ToString()) + " from dbo.powersetting where Unit='" + blockName + "' and PPID='" + ppid.ToString() + "'and fromdate<='" + biddingDate + "' and todate>='" + biddingDate + "' and type='Contract'order by fromdate,id desc");

                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        if (zx2.Rows.Count > 0) range.Value2 = zx2.Rows[0][0].ToString();
                        else range.Value2 = 0;
                        /////////////////////////////////////////////////////////////



                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["DeclaredCapacity"].ToString().Trim();


                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["DispachableCapacity"].ToString().Trim();
                        double dC = double.Parse(row["DispachableCapacity"].ToString().Trim());

                        sum += dC;
                        if (dC > max)
                            max = dC;

                        for (int i = 1; i <= 10; i++)
                        {
                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 = row["Power" + i.ToString()].ToString().Trim();

                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 = row["Price" + i.ToString()].ToString().Trim();
                        }

                        rowIndex++;
                        colIndex = 4;
                    }

                    range = (Excel.Range)sheet.Cells[firstLine, 2];
                    range.Value2 = max.ToString();

                    range = (Excel.Range)sheet.Cells[firstLine, 3];
                    range.Value2 = sum.ToString();
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                sheet = ((Excel.Worksheet)WB.Worksheets["Sheet1"]);

                range = (Excel.Range)sheet.Cells[1, 1];
                range = range.EntireColumn;
                range.AutoFit();

                range = (Excel.Range)sheet.Cells[1, 2];
                range = range.EntireColumn;
                range.AutoFit();

                sheet.Activate();
                //oExcel.Visible = true;

                string strDate = biddingDate;
                strDate = strDate.Remove(7, 1);
                strDate = strDate.Remove(4, 1);
                string filePath = path + "\\" + ppidToPrint.ToString() + "-" + strDate + ".xls";
                string makename = NameFormat(biddingDate, ppidToPrint.ToString(), ppnameFarsi);
                if (makename != "") filePath = path + "\\" + makename + ".xls";



                //oExcel.Visible = true;
                WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                WB.Close(Missing.Value, Missing.Value, Missing.Value);
                oExcel.Workbooks.Close();
                oExcel.Quit();

            }

        }

        private string[] block002(string ppidpre)
        {
            DataTable oDataTable = null;
            DataTable oDatapre = Utilities.GetTable("select unitcode from UnitsDataMain where  PPID='" + ppidpre + "'");
            int packprenumber = oDatapre.Rows.Count;

            DataTable oDatapre1 = Utilities.GetTable("select max(packagecode) from UnitsDataMain where  PPID='" + ppidpre + "'");
            int pack = (int)oDatapre1.Rows[0][0];

            string[] blocks = new string[packprenumber];
            string temp = "";



            int packcode = 0;
            for (int k = 0; k < oDatapre.Rows.Count; k++)
            {
                if (packcode < pack) packcode++;

                oDataTable = Utilities.GetTable("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                    ppidpre + "'" + " and packageCode='" + packcode + "'");

                int ppid = int.Parse(ppidpre);

                DataRow myRow = oDataTable.Rows[0];

                string packageType = myRow["PackageType"].ToString().Trim();

                string Initial = "";
                if (packageType == "CC")
                {


                    oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                    ppidpre + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY unittype asc");
                    int loopcount = 0;
                    foreach (DataRow mn in oDataTable.Rows)
                    {

                        string ptype = mn[0].ToString().Trim();
                        ptype = ptype.ToLower();
                        ptype = ptype.Replace("cc", "c");
                        string[] sp = ptype.Split('c');
                        ptype = sp[0].Trim() + sp[1].Trim();
                        if (ptype.Contains("gas"))
                        {
                            ptype = ptype.Replace("gas", "G");

                        }
                        else
                        {
                            ptype = ptype.Replace("steam", "S");

                        }


                        blocks[k] = ptype;

                        loopcount++;
                        if (loopcount != oDataTable.Rows.Count) k++;
                    }
                }
                else if (packageType == "Gas")
                {
                    oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                   ppidpre + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'ORDER BY unittype asc");

                    Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                    string blockName = Initial;

                    blocks[k] = blockName;
                }
                else if (packageType == "Steam")
                {
                    oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                    ppidpre + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'ORDER BY unittype asc");

                    Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                    string blockName = Initial;

                    blocks[k] = blockName;
                }



            }
            return blocks;


        }

        private string[] LOWSHANblock002(string ppidpre)
        {
            string[] blocks = new string[4];
            blocks[0] = "S1";
            blocks[1] = "S2";
            blocks[2] = "G11";
            blocks[3] = "G12";
            return blocks;

        }


        private string[] Getblock002pack(int ppid, string packtype)
        {
            DataTable oDataTable = null;
            DataTable oData = null;

            if (packtype == "All")
            {

                oData = Utilities.GetTable("select unitcode,packageCode,PackageType from dbo.UnitsDataMain where PPID='" +
                   ppid + "'");

            }
            else
            {
                oData = Utilities.GetTable("select unitcode,packageCode,PackageType from dbo.UnitsDataMain where PPID='" +
                   ppid + "'" + " and PackageType='" + packtype + "'");

            }


            string[] blocks1 = new string[oData.Rows.Count];


            DataTable oda = Utilities.GetTable("select  distinct(packagecode) from dbo.UnitsDataMain where PPID='" +
              ppid + "'and PackageType='" + packtype + "'");

            int plantpack = oda.Rows.Count;



            int packcode = int.Parse(oda.Rows[0][0].ToString()) - 1;
            int packc = 0;
            for (int k = 0; k < oData.Rows.Count; k++)
            {
                if (packc < plantpack)
                {
                    packc++;
                    packcode++;
                }
                string packageType = packtype;

                string Initial = "";
                if (packageType == "CC")
                {
                    oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                    ppid + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY unittype asc");
                    int loopcount = 0;
                    foreach (DataRow mn in oDataTable.Rows)
                    {

                        string ptype = mn[0].ToString().Trim();
                        ptype = ptype.ToLower();
                        ptype = ptype.Replace("cc", "c");
                        string[] sp = ptype.Split('c');
                        ptype = sp[0].Trim() + sp[1].Trim();
                        if (ptype.Contains("gas"))
                            ptype = ptype.Replace("gas", "G");
                        else if (ptype.Contains("steam"))
                            ptype = ptype.Replace("steam", "S");

                        blocks1[k] = ptype + "-C";

                        loopcount++;
                        if (loopcount != oDataTable.Rows.Count) k++;

                    }



                }
                else if (packageType == "Gas")
                {
                    oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                   ppid + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'ORDER BY unittype asc");

                    Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                    string blockName1 = Initial;

                    blocks1[k] = blockName1;

                }
                else if (packageType == "Steam")
                {
                    oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                    ppid + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'ORDER BY unittype asc");

                    Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                    string blockName1 = Initial;

                    blocks1[k] = blockName1;

                }


            }

            return blocks1;




        }

        public string NameFormat(string date, string ppid,string pnamefarsi)
        {


            string english = "";
            DataTable dv = Utilities.GetTable("select * from powerplant where ppid='" + ppid + "'");
            if (dv.Rows.Count > 0)
            {
                pnamefarsi = dv.Rows[0]["PNameFarsi"].ToString().Trim();
                english = dv.Rows[0]["PPName"].ToString().Trim();

            }

            string year = date.Substring(0, 4).Trim();
            string month = date.Substring(5, 2).Trim();
            string day = date.Substring(8, 2).Trim();

            string excelname = "";
            DataTable dddd = Utilities.GetTable("select * from FilesNameFormat where filename='" + "M002" + "'");
            if (dddd.Rows.Count > 0 && dddd.Rows[0]["ExcelName"].ToString().Trim()!="")
            {
               
                string [] x = dddd.Rows[0]["ExcelName"].ToString().Trim().Split('*');

                for (int i = 0; i < x.Length; i++)
                {
                   
                    if (x[i].Trim() == "Plantname/english")
                    {
                        excelname += english;
                    }
                    else if (x[i].Trim() == "Plantname/Farsi")
                    {
                        excelname += pnamefarsi;
                    }
                    else if (x[i].Trim() == "PlantID")
                    {
                        excelname += ppid;
                    }
                    else if (x[i].Trim() == "Date Without Seperator/8char")
                    {
                        string strDate = date;
                        strDate = strDate.Remove(7, 1);
                        strDate = strDate.Remove(4, 1);
                        excelname += strDate;
                    }
                   
                    else if (x[i].Trim() == "Date With Seperator/10char")
                    {
                        excelname += date;
                    }
                    else if (x[i].Trim() == "Year/2char")
                    {
                        excelname += year.Substring(2, 2).Trim();
                    }
                    else if (x[i].Trim() == "Year/4char")
                    {
                        excelname += year.Trim();
                    }
                    else if (x[i].Trim() == "month/2char")
                    {
                        excelname += month.Trim();

                    }
                    else if (x[i].Trim() == "month/1char")
                    {
                       // excelname += month.Substring(1, 1).Trim();
                        excelname += int.Parse(month);

                    }
                    else if (x[i].Trim() == "day/2char")
                    {
                        excelname += day.Trim();

                    }
                    else if (x[i].Trim() == "day/1char")
                    {
                        excelname += day.Substring(1, 1).Trim();

                    }
                    else if (x[i].Trim() == "space")
                    {
                        excelname += " "; 
                    }
                    else if (x[i].Trim() == "VersionNumber/max10")
                    {
                        excelname += "0";
                    }
                    else
                    {
                        excelname += x[i];
                    }
                   
                }


               
                return excelname;
            }
            return excelname;
        }
//        private void ExportM002Excel(string path, int ppid, string ppnameFarsi)
//        {
//            List<string> Package = new List<string>();

//            string cmd = "select distict PackageType from dbo.UnitsDataMain where PPID=" + plant[j] +
//" AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%')AND(UnitCode not like 'Gas cc%')))";
//            oDataTable = utilities.GetTable(cmd);
//            for (int k = 0; k < Plant_Packages_Num[j]; k++)
//            {
//                Package.Add(oDataTable.Rows[k][0].ToString().Trim());
//            }



//            MixUnits mixUnitStatus = MixUnits.All;
//            if (MainForm.CombinedCyclePowerPlant(ppid.ToString()))
//                mixUnitStatus = MixUnits.ExeptCombined;

//            string[] headers = { "Block/Unit No", "Peak", "Max. Daily Generation", "Hour", 
//                                   "Declared Capacity", "Dispatchable Capacity"};

//            string[] firstColValues = { "Form Name :", "Date Of Issue :", "Time Of Issue :", "Target Market Date :",
//                                    "Power Plant Name :", "Code :", "Revision :", 
//                                    "Filled By :", "Approved By :" };

//            do
//            {
//                int rowIndex = 0;
//                int colIndex = 50;

//                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
//                Excel.Application oExcel = new Excel.Application();

//                oExcel.SheetsInNewWorkbook = 1;
//                Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
//                Excel.Worksheet sheet = null;
//                Excel.Range range = null;

//                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers
//                string ppnameFarsiModified = ppnameFarsi;
//                if (mixUnitStatus == MixUnits.ExeptCombined)
//                    ppnameFarsiModified = "سيكل تركيبي" + " " + ppnameFarsiModified;



//                rowIndex = 0;
//                colIndex = 0;
//                int sheetIndex = 1;
//                if (sheet != null)
//                    sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
//                else
//                    sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

//                sheet.Name = "Page " + (sheetIndex);

//                int ppidToPrint = ppid;
//                if (mixUnitStatus == MixUnits.JustCombined)// the second one
//                    ppidToPrint++;
//                string[] secondColValues = { "M0022", 
//                                          DateTime.Now.ToString("d"), DateTime.Now.ToString("T"),
//                                          biddingDate,
//                                          ppnameFarsiModified, ppidToPrint.ToString(), 
//                                          "0", "", "" };


//                for (int i = 0; i < firstColValues.Length; i++)
//                {
//                    range = (Excel.Range)sheet.Cells[i + 1, 1];
//                    range.Value2 = firstColValues[i];
//                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

//                    range = (Excel.Range)sheet.Cells[i + 1, 2];
//                    range.Value2 = secondColValues[i];
//                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
//                }

//                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


//                rowIndex = firstColValues.Length + 2;
//                colIndex = 0;
//                for (int i = 0; i < headers.Length; i++)
//                {
//                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
//                    range.Value2 = headers[i];
//                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
//                }
//                for (int i = 1; i <= 10; i++)
//                {
//                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
//                    range.Value2 = "Power_" + i.ToString();
//                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

//                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
//                    range.Value2 = "Price_" + i.ToString();
//                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
//                }
//                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//                string strCmd = "select distinct block from DetailFRM002 where ppid='" + ppid.ToString() +
//                "' AND TargetMarketDate='" + biddingDate + "'";
//                if (mixUnitStatus == MixUnits.JustCombined)
//                    strCmd += " AND block like 'C%'";

//                //else if (mixUnitStatus == MixUnits.ExeptCombined)
//                //    strCmd += " AND block like 'S%'";
//                strCmd += " order by block";

//                DataTable oDataTable = utilities.GetTable(strCmd);

//                string[] blocks = new string[oDataTable.Rows.Count];
//                for (int i = 0; i < oDataTable.Rows.Count; i++)
//                    blocks[i] = oDataTable.Rows[i][0].ToString().Trim();

//                //strCmd = "select * from DetailFRM002 where ppid='" + ppid.ToString() +
//                //    "' AND TargetMarketDate='" + datePickerBidding.Text + "' order by block, hour";
//                //oDataTable = utilities.GetTable(strCmd);

//                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//                rowIndex = firstColValues.Length + 3;
//                foreach (string blockName in blocks)
//                {
//                    int firstLine = rowIndex;
//                    range = (Excel.Range)sheet.Cells[rowIndex, 1];
//                    range.Value2 = blockName;
//                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

//                    ////////!!!!!!! Peak!!!>???????????????

//                    strCmd = "select * from DetailFRM002 where ppid='" + ppid.ToString() + "'" +
//                        " AND Block='" + blockName + "'" +
//                        " AND TargetMarketDate='" + biddingDate + "'" +
//                        " AND Estimated=1" +
//                        " order by block, hour";
//                    oDataTable = utilities.GetTable(strCmd);

//                    colIndex = 4;

//                    double max = 0;
//                    foreach (DataRow row in oDataTable.Rows)
//                    {
//                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
//                        range.Value2 = (int.Parse(row["Hour"].ToString().Trim())).ToString();
//                        //range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

//                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
//                        range.Value2 = row["DeclaredCapacity"].ToString().Trim();


//                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
//                        range.Value2 = row["DispachableCapacity"].ToString().Trim();
//                        double dC = double.Parse(row["DispachableCapacity"].ToString().Trim());

//                        if (dC > max)
//                            max = dC;

//                        for (int i = 1; i <= 10; i++)
//                        {
//                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
//                            range.Value2 = row["Power" + i.ToString()].ToString().Trim();

//                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
//                            range.Value2 = row["Price" + i.ToString()].ToString().Trim();
//                        }

//                        rowIndex++;
//                        colIndex = 4;
//                    }

//                    range = (Excel.Range)sheet.Cells[firstLine, 2];
//                    range.Value2 = max.ToString();
//                }

//                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//                sheet = ((Excel.Worksheet)WB.Worksheets["Page 1"]);

//                range = (Excel.Range)sheet.Cells[1, 1];
//                range = range.EntireColumn;
//                range.AutoFit();

//                range = (Excel.Range)sheet.Cells[1, 2];
//                range = range.EntireColumn;
//                range.AutoFit();

//                sheet.Activate();
//                //oExcel.Visible = true;

//                string strDate = biddingDate;
//                strDate = strDate.Remove(7, 1);
//                strDate = strDate.Remove(4, 1);
//                string filePath = path + "\\" + ppidToPrint.ToString() + "-" + strDate + ".xls";
//                //oExcel.Visible = true;
//                WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
//                WB.Close(Missing.Value, Missing.Value, Missing.Value);
//                oExcel.Workbooks.Close();
//                oExcel.Quit();

//                if (mixUnitStatus == MixUnits.ExeptCombined)
//                    mixUnitStatus = MixUnits.JustCombined;
//                else
//                    mixUnitStatus = MixUnits.All;

//            } while (mixUnitStatus != MixUnits.All);

//        }

    }
}
