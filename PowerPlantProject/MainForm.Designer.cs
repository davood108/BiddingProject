﻿namespace PowerPlantProject
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Dundas.Charting.WinControl.ChartArea chartArea1 = new Dundas.Charting.WinControl.ChartArea();
            Dundas.Charting.WinControl.Legend legend1 = new Dundas.Charting.WinControl.Legend();
            Dundas.Charting.WinControl.Series series1 = new Dundas.Charting.WinControl.Series();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.linesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.powerPalntDBDataSet4BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.unitsDataMainBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.linesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.detailFRM002BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.treeView2 = new System.Windows.Forms.TreeView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeletetoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripuseraccount = new System.Windows.Forms.ToolStripMenuItem();
            this.addUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backToLogInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemData = new System.Windows.Forms.ToolStripMenuItem();
            this.baseDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoPathToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.weekendCalenderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripspecialtime = new System.Windows.Forms.ToolStripMenuItem();
            this.dispatchConstrantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gencoNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.biddingSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trainingPriceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.powerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem44 = new System.Windows.Forms.ToolStripMenuItem();
            this.consumeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billimgSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billItemToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fuelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bourseSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.marketToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m002ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m005ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dispatchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.averagePriceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadForecastingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaledEnergytoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.betToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.efficiencyMarketToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.capacityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annualFactorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.weekFactorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.meteringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ls2IntervalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ls2LineIntervalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ls2PostIntervalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allTypeIntervalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.internetplantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ls2FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadIntervalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyBillToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlyBillToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hubToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transmissionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pactualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.powerLimmitedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.priorityOnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.riskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.strategyToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.importBillFromWebServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.avEnergyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemExport = new System.Windows.Forms.ToolStripMenuItem();
            this.ReporttoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allPlantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectedPlantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.plantReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unitsReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.daramadReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.protestReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billCheckToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billCheckAvEnergyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oPFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sensetivityAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wizardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.biddingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.marketInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem45 = new System.Windows.Forms.ToolStripMenuItem();
            this.formsDesignToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.contactUsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.chart2 = new Dundas.Charting.WinControl.Chart();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.folderBrowserDialogmarket = new System.Windows.Forms.FolderBrowserDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lbllogname = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel96 = new System.Windows.Forms.Panel();
            this.tbPageMaintenance = new System.Windows.Forms.TabPage();
            this.cmbMaintShownPlant = new System.Windows.Forms.ComboBox();
            this.groupBox59 = new System.Windows.Forms.GroupBox();
            this.lblMainStartDate = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.lblMaintStartDate = new System.Windows.Forms.Label();
            this.lblMaintEndDate = new System.Windows.Forms.Label();
            this.label229 = new System.Windows.Forms.Label();
            this.label227 = new System.Windows.Forms.Label();
            this.btnRunMaint = new System.Windows.Forms.Button();
            this.groupBox61 = new System.Windows.Forms.GroupBox();
            this.txtMaintStatus = new System.Windows.Forms.TextBox();
            this.groupBox62 = new System.Windows.Forms.GroupBox();
            this.lblMaintPlant = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this.label233 = new System.Windows.Forms.Label();
            this.BiddingStrategy = new System.Windows.Forms.TabPage();
            this.groupBoxBiddingStrategy3 = new System.Windows.Forms.GroupBox();
            this.Initialprice = new System.Windows.Forms.CheckBox();
            this.panel93 = new System.Windows.Forms.Panel();
            this.btnrunseting = new System.Windows.Forms.Button();
            this.chkmaxall = new System.Windows.Forms.CheckBox();
            this.btnRunManual = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.rdStrategyBidding = new System.Windows.Forms.RadioButton();
            this.rdBidAllocation = new System.Windows.Forms.RadioButton();
            this.rdForecastingPrice = new System.Windows.Forms.RadioButton();
            this.btnRunBidding = new System.Windows.Forms.Button();
            this.groupBoxBiddingStrategy2 = new System.Windows.Forms.GroupBox();
            this.lstStatus = new System.Windows.Forms.TextBox();
            this.groupBoxBiddingStrategy1 = new System.Windows.Forms.GroupBox();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.panel80 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.datePickerBidding = new FarsiLibrary.Win.Controls.FADatePicker();
            this.panel81 = new System.Windows.Forms.Panel();
            this.datePickerCurrent = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.panel90 = new System.Windows.Forms.Panel();
            this.label98 = new System.Windows.Forms.Label();
            this.txtpowertraining = new System.Windows.Forms.TextBox();
            this.panel24 = new System.Windows.Forms.Panel();
            this.cmbPeakBid = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel75 = new System.Windows.Forms.Panel();
            this.cmbCostLevel = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel77 = new System.Windows.Forms.Panel();
            this.cmbStrategyBidding = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel78 = new System.Windows.Forms.Panel();
            this.txtTraining = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel79 = new System.Windows.Forms.Panel();
            this.cmbRunSetting = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblPlantValue = new System.Windows.Forms.Label();
            this.lblGencoValue = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.grBoxCustomize = new System.Windows.Forms.GroupBox();
            this.menuStrip9 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox2 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem29 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox3 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox4 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem30 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox5 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox6 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem31 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox7 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox8 = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip10 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem32 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox9 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox10 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem33 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox11 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox12 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem34 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox13 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox14 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem35 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox15 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox16 = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip11 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem36 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox17 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox18 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem37 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox19 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox20 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem38 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox21 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox22 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem39 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox23 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox24 = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip12 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem40 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox25 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox26 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem41 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox27 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox28 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem42 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox29 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox30 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem43 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox31 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBox32 = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip7 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy6 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak6 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy12 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak12 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy18 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak18 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy24 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak24 = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip6 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy5 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak5 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy11 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak11 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy17 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak17 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy23 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak23 = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip8 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem23 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem24 = new System.Windows.Forms.ToolStripMenuItem();
            this.strategyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.peakToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.strategyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.strategyToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.peakToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.hourToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.strategyToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.peakToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip5 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy4 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak4 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy10 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak10 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy16 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak16 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy22 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak22 = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip3 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy2 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak2 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy8 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak8 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy14 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak14 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy20 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak20 = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip4 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem25 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy3 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak3 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem26 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy9 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak9 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy15 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak15 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy21 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak21 = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.hour1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy1 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak1 = new System.Windows.Forms.ToolStripComboBox();
            this.hour2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy7 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak7 = new System.Windows.Forms.ToolStripComboBox();
            this.hour3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy13 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak13 = new System.Windows.Forms.ToolStripComboBox();
            this.hour4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbStrategy19 = new System.Windows.Forms.ToolStripComboBox();
            this.cmbPeak19 = new System.Windows.Forms.ToolStripComboBox();
            this.miniToolStrip = new System.Windows.Forms.MenuStrip();
            this.FinancialReport = new System.Windows.Forms.TabPage();
            this.FRMainPanel = new System.Windows.Forms.Panel();
            this.btnfinanceexcel = new System.Windows.Forms.Button();
            this.btnfrprint = new System.Windows.Forms.Button();
            this.FRRunAuto = new System.Windows.Forms.Button();
            this.FRRun = new System.Windows.Forms.Button();
            this.FRPlot = new System.Windows.Forms.Button();
            this.FRUpdateBtn = new System.Windows.Forms.Button();
            this.FROKBtn = new System.Windows.Forms.Button();
            this.FRUnitPanel = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel19 = new System.Windows.Forms.Panel();
            this.FRUnitSecondaryFuelRb = new System.Windows.Forms.RadioButton();
            this.FRUnitPrimaryFuelRb = new System.Windows.Forms.RadioButton();
            this.panel18 = new System.Windows.Forms.Panel();
            this.FRUnitPActiveTb = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.FRUnitQReactiveTb = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.FRUnitFuelCostTb = new System.Windows.Forms.TextBox();
            this.FRUnitFuelNoCostTb = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.FRUnitCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.panel76 = new System.Windows.Forms.Panel();
            this.FRUnitIncomeTb = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.panel69 = new System.Windows.Forms.Panel();
            this.FRUnitEneryPayTb = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.panel70 = new System.Windows.Forms.Panel();
            this.FRUnitCapPayTb = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.panel71 = new System.Windows.Forms.Panel();
            this.FRUnitULPowerTb = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.panel73 = new System.Windows.Forms.Panel();
            this.FRUnitTotalPowerTb = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.panel74 = new System.Windows.Forms.Panel();
            this.FRUnitCapacityTb = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.panel65 = new System.Windows.Forms.Panel();
            this.FRUnitHotTb = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.panel68 = new System.Windows.Forms.Panel();
            this.FRUnitColdTb = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.panel66 = new System.Windows.Forms.Panel();
            this.FRUnitBmainTb = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.panel67 = new System.Windows.Forms.Panel();
            this.FRUnitAmainTb = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.panel62 = new System.Windows.Forms.Panel();
            this.FRUnitCmargTb2 = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.panel63 = new System.Windows.Forms.Panel();
            this.FRUnitBmargTb2 = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.panel64 = new System.Windows.Forms.Panel();
            this.FRUnitAmargTb2 = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.panel59 = new System.Windows.Forms.Panel();
            this.FRUnitCmargTb1 = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.panel60 = new System.Windows.Forms.Panel();
            this.FRUnitBmargTb1 = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.panel61 = new System.Windows.Forms.Panel();
            this.FRUnitAmargTb1 = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.panel53 = new System.Windows.Forms.Panel();
            this.FRUnitVariableTb = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.panel57 = new System.Windows.Forms.Panel();
            this.FRUnitFixedTb = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.panel58 = new System.Windows.Forms.Panel();
            this.FRUnitCapitalTb = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.FRPlantPanel = new System.Windows.Forms.Panel();
            this.lblinsufficient = new System.Windows.Forms.LinkLabel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.panel108 = new System.Windows.Forms.Panel();
            this.dataGridViewECO = new System.Windows.Forms.DataGridView();
            this.FRPlantCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.panel54 = new System.Windows.Forms.Panel();
            this.FRPlantCostTb = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.panel55 = new System.Windows.Forms.Panel();
            this.FRPlantIncomeTb = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.panel56 = new System.Windows.Forms.Panel();
            this.FRPlantBenefitTB = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.panel47 = new System.Windows.Forms.Panel();
            this.FRPlantDecPayTb = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.panel48 = new System.Windows.Forms.Panel();
            this.FRPlantIncPayTb = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.panel49 = new System.Windows.Forms.Panel();
            this.FRPlantULPayTb = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.panel50 = new System.Windows.Forms.Panel();
            this.FRPlantBidPayTb = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.panel51 = new System.Windows.Forms.Panel();
            this.FRPlantEnergyPayTb = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.panel52 = new System.Windows.Forms.Panel();
            this.FRPlantCapPayTb = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.panel41 = new System.Windows.Forms.Panel();
            this.FRPlantDecPowerTb = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.panel42 = new System.Windows.Forms.Panel();
            this.FRPlantIncPowerTb = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.panel43 = new System.Windows.Forms.Panel();
            this.FRPlantULPowerTb = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.panel44 = new System.Windows.Forms.Panel();
            this.FRPlantBidPowerTb = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.panel45 = new System.Windows.Forms.Panel();
            this.FRPlantTotalPowerTb = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.panel46 = new System.Windows.Forms.Panel();
            this.FRPlantAvaCapTb = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.FRHeaderGb = new System.Windows.Forms.GroupBox();
            this.FRPlantLb = new System.Windows.Forms.Label();
            this.FRHeaderPanel = new System.Windows.Forms.Panel();
            this.FRTypeLb = new System.Windows.Forms.Label();
            this.FRUnitLb = new System.Windows.Forms.Label();
            this.FRPackLb = new System.Windows.Forms.Label();
            this.L20 = new System.Windows.Forms.Label();
            this.L19 = new System.Windows.Forms.Label();
            this.L18 = new System.Windows.Forms.Label();
            this.L17 = new System.Windows.Forms.Label();
            this.BidData = new System.Windows.Forms.TabPage();
            this.BDMainPanel = new System.Windows.Forms.Panel();
            this.btnToExcel = new System.Windows.Forms.Button();
            this.btnbidprint = new System.Windows.Forms.Button();
            this.label113 = new System.Windows.Forms.Label();
            this.BDPlotBtn = new System.Windows.Forms.Button();
            this.BDCur2 = new System.Windows.Forms.GroupBox();
            this.EstimatedBidCheck = new System.Windows.Forms.RadioButton();
            this.RealBidCheck = new System.Windows.Forms.RadioButton();
            this.BDCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.BDCurGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power6DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price6DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power7DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price7DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power8DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price8DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power9DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price9DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power10DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price10DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label43 = new System.Windows.Forms.Label();
            this.BDCur1 = new System.Windows.Forms.GroupBox();
            this.panel38 = new System.Windows.Forms.Panel();
            this.BDMaxBidTb = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.panel39 = new System.Windows.Forms.Panel();
            this.BDPowerTb = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.panel40 = new System.Windows.Forms.Panel();
            this.BDStateTb = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.BDHeaderGb = new System.Windows.Forms.GroupBox();
            this.BDPlantLb = new System.Windows.Forms.Label();
            this.BDHeaderPanel = new System.Windows.Forms.Panel();
            this.BDTypeLb = new System.Windows.Forms.Label();
            this.BDUnitLb = new System.Windows.Forms.Label();
            this.BDPackLb = new System.Windows.Forms.Label();
            this.L16 = new System.Windows.Forms.Label();
            this.L15 = new System.Windows.Forms.Label();
            this.L14 = new System.Windows.Forms.Label();
            this.L13 = new System.Windows.Forms.Label();
            this.MarketResults = new System.Windows.Forms.TabPage();
            this.MRHeaderGB = new System.Windows.Forms.GroupBox();
            this.MRPlantLb = new System.Windows.Forms.Label();
            this.MRHeaderPanel = new System.Windows.Forms.Panel();
            this.MRTypeLb = new System.Windows.Forms.Label();
            this.MRUnitLb = new System.Windows.Forms.Label();
            this.MRPackLb = new System.Windows.Forms.Label();
            this.L12 = new System.Windows.Forms.Label();
            this.L11 = new System.Windows.Forms.Label();
            this.L10 = new System.Windows.Forms.Label();
            this.L9 = new System.Windows.Forms.Label();
            this.MRMainPanel = new System.Windows.Forms.Panel();
            this.btnToExcelMR = new System.Windows.Forms.Button();
            this.rdbam005ba = new System.Windows.Forms.RadioButton();
            this.rdm005 = new System.Windows.Forms.RadioButton();
            this.mrbtnpprint = new System.Windows.Forms.Button();
            this.btnex009 = new System.Windows.Forms.Button();
            this.MRPlotBtn = new System.Windows.Forms.Button();
            this.MRGB = new System.Windows.Forms.GroupBox();
            this.btnmarketforward = new System.Windows.Forms.Button();
            this.rbrealplant = new System.Windows.Forms.RadioButton();
            this.btnmarketback = new System.Windows.Forms.Button();
            this.rbestimateplant = new System.Windows.Forms.RadioButton();
            this.MRCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label39 = new System.Windows.Forms.Label();
            this.MRCurGrid1 = new System.Windows.Forms.DataGridView();
            this.ITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MRCurGrid2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MRPlantCurGb = new System.Windows.Forms.GroupBox();
            this.panel34 = new System.Windows.Forms.Panel();
            this.MRPlantPowerTb = new System.Windows.Forms.TextBox();
            this.MRLabel2 = new System.Windows.Forms.Label();
            this.panel35 = new System.Windows.Forms.Panel();
            this.MRPlantOnUnitTb = new System.Windows.Forms.TextBox();
            this.MRLabel1 = new System.Windows.Forms.Label();
            this.MRUnitCurGb = new System.Windows.Forms.GroupBox();
            this.panel37 = new System.Windows.Forms.Panel();
            this.MRUnitMaxBidTb = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.panel36 = new System.Windows.Forms.Panel();
            this.MRUnitPowerTb = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.panel72 = new System.Windows.Forms.Panel();
            this.MRUnitStateTb = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.OperationalData = new System.Windows.Forms.TabPage();
            this.ODHeaderGB = new System.Windows.Forms.GroupBox();
            this.ODPlantLb = new System.Windows.Forms.Label();
            this.ODHeaderPanel = new System.Windows.Forms.Panel();
            this.ODTypeLb = new System.Windows.Forms.Label();
            this.ODUnitLb = new System.Windows.Forms.Label();
            this.ODPackLb = new System.Windows.Forms.Label();
            this.L8 = new System.Windows.Forms.Label();
            this.L7 = new System.Windows.Forms.Label();
            this.L6 = new System.Windows.Forms.Label();
            this.L5 = new System.Windows.Forms.Label();
            this.ODMainPanel = new System.Windows.Forms.Panel();
            this.panel94 = new System.Windows.Forms.Panel();
            this.label116 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.faDatePickDispatch = new FarsiLibrary.Win.Controls.FADatePicker();
            this.dataGridDispatch = new System.Windows.Forms.DataGridView();
            this.Hour = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Block = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Packagetype = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeclaredCapacity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DispachableCapacity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ODUnitPanel = new System.Windows.Forms.Panel();
            this.ODUnitMainGB = new System.Windows.Forms.GroupBox();
            this.linkmaintenance = new System.Windows.Forms.LinkLabel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.ODMaintenanceType = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.odunitmd2Valid = new System.Windows.Forms.Label();
            this.odunitmd1Valid = new System.Windows.Forms.Label();
            this.panel32 = new System.Windows.Forms.Panel();
            this.ODMaintenanceEndHour = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.ODUnitMainEndDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label37 = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.ODMaintenanceStartHour = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.ODUnitMainStartDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label38 = new System.Windows.Forms.Label();
            this.ODUnitMainCheck = new System.Windows.Forms.CheckBox();
            this.ODUnitPowerGB = new System.Windows.Forms.GroupBox();
            this.btnsavedispach = new System.Windows.Forms.Button();
            this.textupdatetime = new System.Windows.Forms.TextBox();
            this.labelupdatetime = new System.Windows.Forms.Label();
            this.odunitpd1Valid = new System.Windows.Forms.Label();
            this.ODUnitPowerGrid2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn62 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn65 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn66 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ODUnitPowerGrid1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn67 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn68 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn69 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn70 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn71 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn72 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn73 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn74 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn75 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn76 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn77 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn78 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel26 = new System.Windows.Forms.Panel();
            this.ODUnitPowerStartDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label30 = new System.Windows.Forms.Label();
            this.ODUnitPowerCheck = new System.Windows.Forms.CheckBox();
            this.ODUnitServiceGB = new System.Windows.Forms.GroupBox();
            this.linkoutservice = new System.Windows.Forms.LinkLabel();
            this.odunitosd2Valid = new System.Windows.Forms.Label();
            this.odunitosd1Valid = new System.Windows.Forms.Label();
            this.panel30 = new System.Windows.Forms.Panel();
            this.ODOutServiceEndHour = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.ODUnitServiceEndDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label34 = new System.Windows.Forms.Label();
            this.panel31 = new System.Windows.Forms.Panel();
            this.ODOutServiceStartHour = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.ODUnitServiceStartDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label35 = new System.Windows.Forms.Label();
            this.ODUnitOutCheck = new System.Windows.Forms.CheckBox();
            this.ODUnitFuelGB = new System.Windows.Forms.GroupBox();
            this.linkfuel = new System.Windows.Forms.LinkLabel();
            this.odunitsfd2Valid = new System.Windows.Forms.Label();
            this.odunitsfd1Valid = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.textSECONDFUELPERCENT = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.ODUnitFuelTB = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.ODSecondFuelEndHour = new System.Windows.Forms.NumericUpDown();
            this.label28 = new System.Windows.Forms.Label();
            this.ODUnitFuelEndDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label32 = new System.Windows.Forms.Label();
            this.panel29 = new System.Windows.Forms.Panel();
            this.ODSecondFuelStartHour = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.ODUnitFuelStartDate = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label33 = new System.Windows.Forms.Label();
            this.ODUnitFuelCheck = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel92 = new System.Windows.Forms.Panel();
            this.MUSTMAXCKECK = new System.Windows.Forms.CheckBox();
            this.MustrunCheck = new System.Windows.Forms.CheckBox();
            this.ODSaveBtn = new System.Windows.Forms.Button();
            this.panel97 = new System.Windows.Forms.Panel();
            this.GeneralData = new System.Windows.Forms.TabPage();
            this.GDPostpanel = new System.Windows.Forms.Panel();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.FromListBox = new System.Windows.Forms.ListBox();
            this.label101 = new System.Windows.Forms.Label();
            this.ToListBox = new System.Windows.Forms.ListBox();
            this.label102 = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.radioEditPost = new System.Windows.Forms.RadioButton();
            this.radioAddpost = new System.Windows.Forms.RadioButton();
            this.BtnOKpost = new System.Windows.Forms.Button();
            this.label112 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.txtaddvoltagenum = new System.Windows.Forms.TextBox();
            this.txtpostname = new System.Windows.Forms.TextBox();
            this.txtaddpostnum = new System.Windows.Forms.TextBox();
            this.panel95 = new System.Windows.Forms.Panel();
            this.label108 = new System.Windows.Forms.Label();
            this.DeletePostBtn = new System.Windows.Forms.Button();
            this.VolNumLb = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.voltagelevellb = new System.Windows.Forms.Label();
            this.postnumlb = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.GDPowerGb = new System.Windows.Forms.GroupBox();
            this.AddPowerBtn = new System.Windows.Forms.Button();
            this.label105 = new System.Windows.Forms.Label();
            this.AddPowerList = new System.Windows.Forms.CheckedListBox();
            this.DeletePowerBtn = new System.Windows.Forms.Button();
            this.label106 = new System.Windows.Forms.Label();
            this.DeletePowerList = new System.Windows.Forms.CheckedListBox();
            this.ConsumedGb = new System.Windows.Forms.GroupBox();
            this.txtselectconsum = new System.Windows.Forms.TextBox();
            this.AddConsumBtn = new System.Windows.Forms.Button();
            this.label104 = new System.Windows.Forms.Label();
            this.DeleteConsumBtn = new System.Windows.Forms.Button();
            this.label103 = new System.Windows.Forms.Label();
            this.DeleteConsumeList = new System.Windows.Forms.CheckedListBox();
            this.GDMainPanel = new System.Windows.Forms.Panel();
            this.TempGV = new System.Windows.Forms.DataGridView();
            this.GDNewBtn = new System.Windows.Forms.Button();
            this.GDDeleteBtn = new System.Windows.Forms.Button();
            this.GDSteamGroup = new System.Windows.Forms.GroupBox();
            this.Grid230 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CheckBox2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PlantGV2 = new System.Windows.Forms.DataGridView();
            this.unitCodeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packageCodeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unitTypeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.capacityDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pMinDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pMaxDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.GDGasGroup = new System.Windows.Forms.GroupBox();
            this.Grid400 = new System.Windows.Forms.DataGridView();
            this.lineCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fromBusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toBusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lineLengthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ownerGencoFromDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ownerGencoToDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CheckBox1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PlantGV1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PlantGV11 = new System.Windows.Forms.DataGridView();
            this.Fuel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Maintenance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Outservice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GDUnitPanel = new System.Windows.Forms.Panel();
            this.GDUnitMaintenanceGB = new System.Windows.Forms.GroupBox();
            this.panel23 = new System.Windows.Forms.Panel();
            this.txtMaintStartUpFactor = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.txtMaintFuelFactor = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.cmbMaintLastMaintType = new System.Windows.Forms.ComboBox();
            this.label77 = new System.Windows.Forms.Label();
            this.panel84 = new System.Windows.Forms.Panel();
            this.txtMaintMOHours = new System.Windows.Forms.TextBox();
            this.label240 = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.txtMaintLastHour = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.panel86 = new System.Windows.Forms.Panel();
            this.txtMaintHGPHours = new System.Windows.Forms.TextBox();
            this.label242 = new System.Windows.Forms.Label();
            this.panel178 = new System.Windows.Forms.Panel();
            this.txtMaintCIHours = new System.Windows.Forms.TextBox();
            this.label243 = new System.Windows.Forms.Label();
            this.panel88 = new System.Windows.Forms.Panel();
            this.txtDurabilityHours = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.panel87 = new System.Windows.Forms.Panel();
            this.datePickerDurability = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label82 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label236 = new System.Windows.Forms.Label();
            this.panel82 = new System.Windows.Forms.Panel();
            this.txtMaintMODur = new System.Windows.Forms.TextBox();
            this.label238 = new System.Windows.Forms.Label();
            this.panel83 = new System.Windows.Forms.Panel();
            this.txtMaintHGPDur = new System.Windows.Forms.TextBox();
            this.label239 = new System.Windows.Forms.Label();
            this.panel85 = new System.Windows.Forms.Panel();
            this.txtMaintCIDur = new System.Windows.Forms.TextBox();
            this.label241 = new System.Windows.Forms.Label();
            this.Countergb = new System.Windows.Forms.GroupBox();
            this.panel89 = new System.Windows.Forms.Panel();
            this.GDConsumedSerialTb = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.panel91 = new System.Windows.Forms.Panel();
            this.GDPowerSerialTb = new System.Windows.Forms.TextBox();
            this.label99 = new System.Windows.Forms.Label();
            this.Anonygb = new System.Windows.Forms.GroupBox();
            this.panelAvc = new System.Windows.Forms.Panel();
            this.GDtxtAvc = new System.Windows.Forms.TextBox();
            this.lblAvc = new System.Windows.Forms.Label();
            this.GDSecondaryFuelPanel = new System.Windows.Forms.Panel();
            this.GDSecondaryFuelTB = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.GDPrimaryFuelPanel = new System.Windows.Forms.Panel();
            this.GDPrimaryFuelTB = new System.Windows.Forms.TextBox();
            this.GDConsLb = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.GDRampRateTB = new System.Windows.Forms.TextBox();
            this.GDRampLb = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.GDTimeHotStartTB = new System.Windows.Forms.TextBox();
            this.GDHotLb = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.GDTimeColdStartTB = new System.Windows.Forms.TextBox();
            this.GDColdLb = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.GDTimeDownTB = new System.Windows.Forms.TextBox();
            this.GDTimeDownLb = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.GDTimeUpTB = new System.Windows.Forms.TextBox();
            this.GDTUpLb = new System.Windows.Forms.Label();
            this.Powergb = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.GDPminTB = new System.Windows.Forms.TextBox();
            this.GDPminLb = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.GDPmaxTB = new System.Windows.Forms.TextBox();
            this.GDPmaxLb = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.GDcapacityTB = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel25 = new System.Windows.Forms.Panel();
            this.label100 = new System.Windows.Forms.Label();
            this.GDpowertransserialtb = new System.Windows.Forms.TextBox();
            this.Maintenancegb = new System.Windows.Forms.GroupBox();
            this.GDOutServiceCheck = new System.Windows.Forms.CheckBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.GDEndDateCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.GDStartDateCal = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.GDMainTypeTB = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.Currentgb = new System.Windows.Forms.GroupBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.GDFuelTB = new System.Windows.Forms.TextBox();
            this.GDFuelLb = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.GDStateTB = new System.Windows.Forms.TextBox();
            this.GDStateLb = new System.Windows.Forms.Label();
            this.GDHeaderGB = new System.Windows.Forms.GroupBox();
            this.GDPlantLb = new System.Windows.Forms.Label();
            this.GDHeaderPanel = new System.Windows.Forms.Panel();
            this.GDTypeLb = new System.Windows.Forms.Label();
            this.GDUnitLb = new System.Windows.Forms.Label();
            this.GDPackLb = new System.Windows.Forms.Label();
            this.L4 = new System.Windows.Forms.Label();
            this.L3 = new System.Windows.Forms.Label();
            this.L2 = new System.Windows.Forms.Label();
            this.L1 = new System.Windows.Forms.Label();
            this.MainTabs = new System.Windows.Forms.TabControl();
            ((System.ComponentModel.ISupportInitialize)(this.linesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.powerPalntDBDataSet4BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unitsDataMainBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linesBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailFRM002BindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.tbPageMaintenance.SuspendLayout();
            this.groupBox59.SuspendLayout();
            this.groupBox61.SuspendLayout();
            this.groupBox62.SuspendLayout();
            this.BiddingStrategy.SuspendLayout();
            this.groupBoxBiddingStrategy3.SuspendLayout();
            this.groupBoxBiddingStrategy2.SuspendLayout();
            this.groupBoxBiddingStrategy1.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.panel80.SuspendLayout();
            this.panel81.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.panel90.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel75.SuspendLayout();
            this.panel77.SuspendLayout();
            this.panel78.SuspendLayout();
            this.panel79.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grBoxCustomize.SuspendLayout();
            this.menuStrip9.SuspendLayout();
            this.menuStrip10.SuspendLayout();
            this.menuStrip11.SuspendLayout();
            this.menuStrip12.SuspendLayout();
            this.menuStrip7.SuspendLayout();
            this.menuStrip6.SuspendLayout();
            this.menuStrip8.SuspendLayout();
            this.menuStrip5.SuspendLayout();
            this.menuStrip3.SuspendLayout();
            this.menuStrip4.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.FinancialReport.SuspendLayout();
            this.FRMainPanel.SuspendLayout();
            this.FRUnitPanel.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.panel76.SuspendLayout();
            this.panel69.SuspendLayout();
            this.panel70.SuspendLayout();
            this.panel71.SuspendLayout();
            this.panel73.SuspendLayout();
            this.panel74.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.panel65.SuspendLayout();
            this.panel68.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.panel66.SuspendLayout();
            this.panel67.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.panel62.SuspendLayout();
            this.panel63.SuspendLayout();
            this.panel64.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.panel59.SuspendLayout();
            this.panel60.SuspendLayout();
            this.panel61.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.panel53.SuspendLayout();
            this.panel57.SuspendLayout();
            this.panel58.SuspendLayout();
            this.FRPlantPanel.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel108.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewECO)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.panel54.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel56.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel48.SuspendLayout();
            this.panel49.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panel52.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel46.SuspendLayout();
            this.FRHeaderGb.SuspendLayout();
            this.FRHeaderPanel.SuspendLayout();
            this.BidData.SuspendLayout();
            this.BDMainPanel.SuspendLayout();
            this.BDCur2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BDCurGrid)).BeginInit();
            this.BDCur1.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel40.SuspendLayout();
            this.BDHeaderGb.SuspendLayout();
            this.BDHeaderPanel.SuspendLayout();
            this.MarketResults.SuspendLayout();
            this.MRHeaderGB.SuspendLayout();
            this.MRHeaderPanel.SuspendLayout();
            this.MRMainPanel.SuspendLayout();
            this.MRGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MRCurGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MRCurGrid2)).BeginInit();
            this.MRPlantCurGb.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel35.SuspendLayout();
            this.MRUnitCurGb.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel72.SuspendLayout();
            this.OperationalData.SuspendLayout();
            this.ODHeaderGB.SuspendLayout();
            this.ODHeaderPanel.SuspendLayout();
            this.ODMainPanel.SuspendLayout();
            this.panel94.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDispatch)).BeginInit();
            this.ODUnitPanel.SuspendLayout();
            this.ODUnitMainGB.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODMaintenanceEndHour)).BeginInit();
            this.panel33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODMaintenanceStartHour)).BeginInit();
            this.ODUnitPowerGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODUnitPowerGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ODUnitPowerGrid1)).BeginInit();
            this.panel26.SuspendLayout();
            this.ODUnitServiceGB.SuspendLayout();
            this.panel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODOutServiceEndHour)).BeginInit();
            this.panel31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODOutServiceStartHour)).BeginInit();
            this.ODUnitFuelGB.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODSecondFuelEndHour)).BeginInit();
            this.panel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODSecondFuelStartHour)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.panel92.SuspendLayout();
            this.GeneralData.SuspendLayout();
            this.GDPostpanel.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.panel95.SuspendLayout();
            this.GDPowerGb.SuspendLayout();
            this.ConsumedGb.SuspendLayout();
            this.GDMainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TempGV)).BeginInit();
            this.GDSteamGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid230)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlantGV2)).BeginInit();
            this.GDGasGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid400)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlantGV1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlantGV11)).BeginInit();
            this.GDUnitPanel.SuspendLayout();
            this.GDUnitMaintenanceGB.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel84.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel86.SuspendLayout();
            this.panel178.SuspendLayout();
            this.panel88.SuspendLayout();
            this.panel87.SuspendLayout();
            this.panel82.SuspendLayout();
            this.panel83.SuspendLayout();
            this.panel85.SuspendLayout();
            this.Countergb.SuspendLayout();
            this.panel89.SuspendLayout();
            this.panel91.SuspendLayout();
            this.Anonygb.SuspendLayout();
            this.panelAvc.SuspendLayout();
            this.GDSecondaryFuelPanel.SuspendLayout();
            this.GDPrimaryFuelPanel.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.Powergb.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel25.SuspendLayout();
            this.Maintenancegb.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel14.SuspendLayout();
            this.Currentgb.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.GDHeaderGB.SuspendLayout();
            this.GDHeaderPanel.SuspendLayout();
            this.MainTabs.SuspendLayout();
            this.SuspendLayout();
            // 
            // linesBindingSource
            // 
            this.linesBindingSource.DataMember = "Lines";
            this.linesBindingSource.DataSource = this.powerPalntDBDataSet4BindingSource;
            // 
            // unitsDataMainBindingSource
            // 
            this.unitsDataMainBindingSource.DataMember = "UnitsDataMain";
            this.unitsDataMainBindingSource.DataSource = this.powerPalntDBDataSet4BindingSource;
            // 
            // linesBindingSource1
            // 
            this.linesBindingSource1.DataMember = "Lines";
            this.linesBindingSource1.DataSource = this.powerPalntDBDataSet4BindingSource;
            // 
            // detailFRM002BindingSource
            // 
            this.detailFRM002BindingSource.DataMember = "DetailFRM002";
            this.detailFRM002BindingSource.DataSource = this.powerPalntDBDataSet4BindingSource;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.treeView1);
            this.panel1.Location = new System.Drawing.Point(12, 59);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(210, 259);
            this.panel1.TabIndex = 11;
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(17, 14);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(179, 227);
            this.treeView1.TabIndex = 0;
            this.treeView1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
            this.treeView1.Leave += new System.EventHandler(this.treeView1_Leave);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.treeView2);
            this.panel2.Location = new System.Drawing.Point(12, 336);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(210, 349);
            this.panel2.TabIndex = 12;
            // 
            // treeView2
            // 
            this.treeView2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeView2.Location = new System.Drawing.Point(17, 16);
            this.treeView2.Name = "treeView2";
            this.treeView2.Size = new System.Drawing.Size(179, 318);
            this.treeView2.TabIndex = 0;
            this.treeView2.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView2_NodeMouseClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.toolStripMenuItemData,
            this.importToolStripMenuItem,
            this.toolStripMenuItemStatus,
            this.toolStripMenuItemExport,
            this.ReporttoolStripMenuItem,
            this.oPFToolStripMenuItem,
            this.sensetivityAnalysisToolStripMenuItem,
            this.wizardToolStripMenuItem,
            this.marketInformationToolStripMenuItem,
            this.toolStripMenuItem45,
            this.formsDesignToolStripMenuItem,
            this.backUpToolStripMenuItem,
            this.profileToolStripMenuItem,
            this.emailToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1028, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.DeletetoolStripMenuItem,
            this.toolStripuseraccount,
            this.backToLogInToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "&Menu";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Image = global::PowerPlantProject.Properties.Resources.add89;
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.ShowShortcutKeys = false;
            this.addToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.addToolStripMenuItem.Text = "&Update";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // DeletetoolStripMenuItem
            // 
            this.DeletetoolStripMenuItem.Image = global::PowerPlantProject.Properties.Resources.deletered8;
            this.DeletetoolStripMenuItem.Name = "DeletetoolStripMenuItem";
            this.DeletetoolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.DeletetoolStripMenuItem.Text = "&Delete Selected Plant";
            this.DeletetoolStripMenuItem.Click += new System.EventHandler(this.DeletetoolStripMenuItem_Click);
            // 
            // toolStripuseraccount
            // 
            this.toolStripuseraccount.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addUserToolStripMenuItem,
            this.EditUserToolStripMenuItem});
            this.toolStripuseraccount.Image = global::PowerPlantProject.Properties.Resources.Edit_user;
            this.toolStripuseraccount.Name = "toolStripuseraccount";
            this.toolStripuseraccount.Size = new System.Drawing.Size(184, 22);
            this.toolStripuseraccount.Text = "UserAccount";
            // 
            // addUserToolStripMenuItem
            // 
            this.addUserToolStripMenuItem.Name = "addUserToolStripMenuItem";
            this.addUserToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.addUserToolStripMenuItem.Text = "AddUser";
            this.addUserToolStripMenuItem.Click += new System.EventHandler(this.addUserToolStripMenuItem_Click);
            // 
            // EditUserToolStripMenuItem
            // 
            this.EditUserToolStripMenuItem.Name = "EditUserToolStripMenuItem";
            this.EditUserToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.EditUserToolStripMenuItem.Text = "EditUser";
            this.EditUserToolStripMenuItem.Click += new System.EventHandler(this.EditUserToolStripMenuItem_Click);
            // 
            // backToLogInToolStripMenuItem
            // 
            this.backToLogInToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.backToLogInToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.backToLogInToolStripMenuItem.Image = global::PowerPlantProject.Properties.Resources._1263016697_3;
            this.backToLogInToolStripMenuItem.Name = "backToLogInToolStripMenuItem";
            this.backToLogInToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.backToLogInToolStripMenuItem.Text = "LogIn";
            this.backToLogInToolStripMenuItem.Click += new System.EventHandler(this.backToLogInToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.closeToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.closeToolStripMenuItem.Image = global::PowerPlantProject.Properties.Resources.exit5;
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.closeToolStripMenuItem.Text = "&Exit";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // toolStripMenuItemData
            // 
            this.toolStripMenuItemData.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.baseDataToolStripMenuItem,
            this.autoPathToolStripMenuItem1,
            this.weekendCalenderToolStripMenuItem,
            this.ToolStripspecialtime,
            this.dispatchConstrantsToolStripMenuItem,
            this.deleteDataToolStripMenuItem,
            this.gencoNameToolStripMenuItem,
            this.biddingSettingToolStripMenuItem,
            this.billimgSettingToolStripMenuItem,
            this.bourseSettingToolStripMenuItem});
            this.toolStripMenuItemData.Name = "toolStripMenuItemData";
            this.toolStripMenuItemData.Size = new System.Drawing.Size(43, 20);
            this.toolStripMenuItemData.Text = "&Data";
            // 
            // baseDataToolStripMenuItem
            // 
            this.baseDataToolStripMenuItem.Name = "baseDataToolStripMenuItem";
            this.baseDataToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.baseDataToolStripMenuItem.Text = "Base Data";
            this.baseDataToolStripMenuItem.Click += new System.EventHandler(this.baseDataToolStripMenuItem_Click);
            // 
            // autoPathToolStripMenuItem1
            // 
            this.autoPathToolStripMenuItem1.Name = "autoPathToolStripMenuItem1";
            this.autoPathToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.autoPathToolStripMenuItem1.Text = "Auto Path";
            this.autoPathToolStripMenuItem1.Click += new System.EventHandler(this.autoPathToolStripMenuItem1_Click);
            // 
            // weekendCalenderToolStripMenuItem
            // 
            this.weekendCalenderToolStripMenuItem.Name = "weekendCalenderToolStripMenuItem";
            this.weekendCalenderToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.weekendCalenderToolStripMenuItem.Text = "Weekend Calender";
            this.weekendCalenderToolStripMenuItem.Click += new System.EventHandler(this.weekendCalenderToolStripMenuItem_Click);
            // 
            // ToolStripspecialtime
            // 
            this.ToolStripspecialtime.Name = "ToolStripspecialtime";
            this.ToolStripspecialtime.Size = new System.Drawing.Size(180, 22);
            this.ToolStripspecialtime.Text = "SpecialTimes";
            this.ToolStripspecialtime.Click += new System.EventHandler(this.ToolStripspecialtime_Click);
            // 
            // dispatchConstrantsToolStripMenuItem
            // 
            this.dispatchConstrantsToolStripMenuItem.Name = "dispatchConstrantsToolStripMenuItem";
            this.dispatchConstrantsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.dispatchConstrantsToolStripMenuItem.Text = "DispatchConstraints";
            this.dispatchConstrantsToolStripMenuItem.Click += new System.EventHandler(this.dispatchConstrantsToolStripMenuItem_Click);
            // 
            // deleteDataToolStripMenuItem
            // 
            this.deleteDataToolStripMenuItem.Name = "deleteDataToolStripMenuItem";
            this.deleteDataToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.deleteDataToolStripMenuItem.Text = "DeleteData";
            this.deleteDataToolStripMenuItem.Click += new System.EventHandler(this.deleteDataToolStripMenuItem_Click);
            // 
            // gencoNameToolStripMenuItem
            // 
            this.gencoNameToolStripMenuItem.Name = "gencoNameToolStripMenuItem";
            this.gencoNameToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.gencoNameToolStripMenuItem.Text = "GencoName";
            this.gencoNameToolStripMenuItem.Click += new System.EventHandler(this.gencoNameToolStripMenuItem_Click);
            // 
            // biddingSettingToolStripMenuItem
            // 
            this.biddingSettingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trainingPriceToolStripMenuItem1,
            this.powerToolStripMenuItem,
            this.toolStripMenuItem44,
            this.consumeToolStripMenuItem});
            this.biddingSettingToolStripMenuItem.Name = "biddingSettingToolStripMenuItem";
            this.biddingSettingToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.biddingSettingToolStripMenuItem.Text = "BiddingSetting";
            // 
            // trainingPriceToolStripMenuItem1
            // 
            this.trainingPriceToolStripMenuItem1.Name = "trainingPriceToolStripMenuItem1";
            this.trainingPriceToolStripMenuItem1.Size = new System.Drawing.Size(144, 22);
            this.trainingPriceToolStripMenuItem1.Text = "TrainingPrice";
            this.trainingPriceToolStripMenuItem1.Click += new System.EventHandler(this.trainingPriceToolStripMenuItem1_Click);
            // 
            // powerToolStripMenuItem
            // 
            this.powerToolStripMenuItem.Name = "powerToolStripMenuItem";
            this.powerToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.powerToolStripMenuItem.Text = "Power";
            this.powerToolStripMenuItem.Click += new System.EventHandler(this.powerToolStripMenuItem_Click);
            // 
            // toolStripMenuItem44
            // 
            this.toolStripMenuItem44.Name = "toolStripMenuItem44";
            this.toolStripMenuItem44.Size = new System.Drawing.Size(144, 22);
            this.toolStripMenuItem44.Text = "MustRun";
            this.toolStripMenuItem44.Click += new System.EventHandler(this.toolStripMenuItem44_Click);
            // 
            // consumeToolStripMenuItem
            // 
            this.consumeToolStripMenuItem.Name = "consumeToolStripMenuItem";
            this.consumeToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.consumeToolStripMenuItem.Text = "Consume";
            this.consumeToolStripMenuItem.Click += new System.EventHandler(this.consumeToolStripMenuItem_Click);
            // 
            // billimgSettingToolStripMenuItem
            // 
            this.billimgSettingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.billItemToolStripMenuItem1,
            this.fuelToolStripMenuItem});
            this.billimgSettingToolStripMenuItem.Name = "billimgSettingToolStripMenuItem";
            this.billimgSettingToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.billimgSettingToolStripMenuItem.Text = "BillingSetting";
            // 
            // billItemToolStripMenuItem1
            // 
            this.billItemToolStripMenuItem1.Name = "billItemToolStripMenuItem1";
            this.billItemToolStripMenuItem1.Size = new System.Drawing.Size(114, 22);
            this.billItemToolStripMenuItem1.Text = "BillItem";
            this.billItemToolStripMenuItem1.Click += new System.EventHandler(this.billItemToolStripMenuItem1_Click);
            // 
            // fuelToolStripMenuItem
            // 
            this.fuelToolStripMenuItem.Name = "fuelToolStripMenuItem";
            this.fuelToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.fuelToolStripMenuItem.Text = "Fuel";
            this.fuelToolStripMenuItem.Click += new System.EventHandler(this.fuelToolStripMenuItem_Click);
            // 
            // bourseSettingToolStripMenuItem
            // 
            this.bourseSettingToolStripMenuItem.Name = "bourseSettingToolStripMenuItem";
            this.bourseSettingToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.bourseSettingToolStripMenuItem.Text = "BourseSetting";
            this.bourseSettingToolStripMenuItem.Click += new System.EventHandler(this.bourseSettingToolStripMenuItem_Click);
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.marketToolStripMenuItem,
            this.capacityToolStripMenuItem,
            this.toolStripMenuItem1,
            this.meteringToolStripMenuItem,
            this.loadIntervalToolStripMenuItem,
            this.dailyBillToolStripMenuItem,
            this.monthlyBillToolStripMenuItem,
            this.hubToolStripMenuItem,
            this.transmissionToolStripMenuItem,
            this.pactualToolStripMenuItem,
            this.powerLimmitedToolStripMenuItem,
            this.priorityOnToolStripMenuItem,
            this.riskToolStripMenuItem,
            this.strategyToolStripMenuItem4,
            this.importBillFromWebServiceToolStripMenuItem,
            this.avEnergyToolStripMenuItem});
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.importToolStripMenuItem.Text = "&Import";
            // 
            // marketToolStripMenuItem
            // 
            this.marketToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m002ToolStripMenuItem,
            this.m005ToolStripMenuItem,
            this.dispatchToolStripMenuItem,
            this.toolStripMenuItem3,
            this.averagePriceToolStripMenuItem,
            this.loadForecastingToolStripMenuItem,
            this.SaledEnergytoolStripMenu,
            this.toolStripMenuItem4,
            this.betToolStripMenuItem,
            this.efficiencyMarketToolStripMenuItem});
            this.marketToolStripMenuItem.Name = "marketToolStripMenuItem";
            this.marketToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.marketToolStripMenuItem.Text = "Market";
            // 
            // m002ToolStripMenuItem
            // 
            this.m002ToolStripMenuItem.Name = "m002ToolStripMenuItem";
            this.m002ToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.m002ToolStripMenuItem.Text = "M002";
            this.m002ToolStripMenuItem.Click += new System.EventHandler(this.m002ToolStripMenuItem_Click);
            // 
            // m005ToolStripMenuItem
            // 
            this.m005ToolStripMenuItem.Name = "m005ToolStripMenuItem";
            this.m005ToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.m005ToolStripMenuItem.Text = "M005";
            this.m005ToolStripMenuItem.Click += new System.EventHandler(this.m005ToolStripMenuItem_Click);
            // 
            // dispatchToolStripMenuItem
            // 
            this.dispatchToolStripMenuItem.Name = "dispatchToolStripMenuItem";
            this.dispatchToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.dispatchToolStripMenuItem.Text = "Dispatch";
            this.dispatchToolStripMenuItem.Click += new System.EventHandler(this.dispatchToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(209, 22);
            this.toolStripMenuItem3.Text = "---------------------------";
            // 
            // averagePriceToolStripMenuItem
            // 
            this.averagePriceToolStripMenuItem.Name = "averagePriceToolStripMenuItem";
            this.averagePriceToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.averagePriceToolStripMenuItem.Text = "Average Price";
            this.averagePriceToolStripMenuItem.Click += new System.EventHandler(this.averagePriceToolStripMenuItem_Click);
            // 
            // loadForecastingToolStripMenuItem
            // 
            this.loadForecastingToolStripMenuItem.Name = "loadForecastingToolStripMenuItem";
            this.loadForecastingToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.loadForecastingToolStripMenuItem.Text = "Load Forecasting";
            this.loadForecastingToolStripMenuItem.Click += new System.EventHandler(this.loadForecastingToolStripMenuItem_Click);
            // 
            // SaledEnergytoolStripMenu
            // 
            this.SaledEnergytoolStripMenu.Name = "SaledEnergytoolStripMenu";
            this.SaledEnergytoolStripMenu.Size = new System.Drawing.Size(209, 22);
            this.SaledEnergytoolStripMenu.Text = "SaledEnergy";
            this.SaledEnergytoolStripMenu.Click += new System.EventHandler(this.SaledEnergytoolStripMenu_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(209, 22);
            this.toolStripMenuItem4.Text = "---------------------------";
            // 
            // betToolStripMenuItem
            // 
            this.betToolStripMenuItem.Name = "betToolStripMenuItem";
            this.betToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.betToolStripMenuItem.Text = "Interval";
            this.betToolStripMenuItem.Click += new System.EventHandler(this.betToolStripMenuItem_Click);
            // 
            // efficiencyMarketToolStripMenuItem
            // 
            this.efficiencyMarketToolStripMenuItem.Name = "efficiencyMarketToolStripMenuItem";
            this.efficiencyMarketToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.efficiencyMarketToolStripMenuItem.Text = "EfficiencyMarket";
            this.efficiencyMarketToolStripMenuItem.Click += new System.EventHandler(this.efficiencyMarketToolStripMenuItem_Click);
            // 
            // capacityToolStripMenuItem
            // 
            this.capacityToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.annualFactorToolStripMenuItem,
            this.weekFactorToolStripMenuItem});
            this.capacityToolStripMenuItem.Name = "capacityToolStripMenuItem";
            this.capacityToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.capacityToolStripMenuItem.Text = "Capacity";
            // 
            // annualFactorToolStripMenuItem
            // 
            this.annualFactorToolStripMenuItem.Name = "annualFactorToolStripMenuItem";
            this.annualFactorToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.annualFactorToolStripMenuItem.Text = "Annual Factor";
            this.annualFactorToolStripMenuItem.Click += new System.EventHandler(this.annualFactorToolStripMenuItem_Click);
            // 
            // weekFactorToolStripMenuItem
            // 
            this.weekFactorToolStripMenuItem.Name = "weekFactorToolStripMenuItem";
            this.weekFactorToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.weekFactorToolStripMenuItem.Text = "Week Factor";
            this.weekFactorToolStripMenuItem.Click += new System.EventHandler(this.weekFactorToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.toolStripMenuItem1.Text = "---------------------------";
            // 
            // meteringToolStripMenuItem
            // 
            this.meteringToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ls2IntervalToolStripMenuItem,
            this.ls2LineIntervalToolStripMenuItem,
            this.ls2PostIntervalToolStripMenuItem,
            this.allTypeIntervalToolStripMenuItem,
            this.internetplantToolStripMenuItem,
            this.ls2FileToolStripMenuItem});
            this.meteringToolStripMenuItem.Name = "meteringToolStripMenuItem";
            this.meteringToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.meteringToolStripMenuItem.Text = "Metering";
            // 
            // ls2IntervalToolStripMenuItem
            // 
            this.ls2IntervalToolStripMenuItem.Name = "ls2IntervalToolStripMenuItem";
            this.ls2IntervalToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.ls2IntervalToolStripMenuItem.Text = "Ls2Interval";
            this.ls2IntervalToolStripMenuItem.Click += new System.EventHandler(this.ls2IntervalToolStripMenuItem_Click);
            // 
            // ls2LineIntervalToolStripMenuItem
            // 
            this.ls2LineIntervalToolStripMenuItem.Enabled = false;
            this.ls2LineIntervalToolStripMenuItem.Name = "ls2LineIntervalToolStripMenuItem";
            this.ls2LineIntervalToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.ls2LineIntervalToolStripMenuItem.Text = "Ls2LineInterval";
            this.ls2LineIntervalToolStripMenuItem.Visible = false;
            this.ls2LineIntervalToolStripMenuItem.Click += new System.EventHandler(this.ls2LineIntervalToolStripMenuItem_Click);
            // 
            // ls2PostIntervalToolStripMenuItem
            // 
            this.ls2PostIntervalToolStripMenuItem.Enabled = false;
            this.ls2PostIntervalToolStripMenuItem.Name = "ls2PostIntervalToolStripMenuItem";
            this.ls2PostIntervalToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.ls2PostIntervalToolStripMenuItem.Text = "Ls2PostInterval";
            this.ls2PostIntervalToolStripMenuItem.Visible = false;
            this.ls2PostIntervalToolStripMenuItem.Click += new System.EventHandler(this.ls2PostIntervalToolStripMenuItem_Click);
            // 
            // allTypeIntervalToolStripMenuItem
            // 
            this.allTypeIntervalToolStripMenuItem.Enabled = false;
            this.allTypeIntervalToolStripMenuItem.Name = "allTypeIntervalToolStripMenuItem";
            this.allTypeIntervalToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.allTypeIntervalToolStripMenuItem.Text = "AllTypeInterval";
            this.allTypeIntervalToolStripMenuItem.Visible = false;
            this.allTypeIntervalToolStripMenuItem.Click += new System.EventHandler(this.allTypeIntervalToolStripMenuItem_Click);
            // 
            // internetplantToolStripMenuItem
            // 
            this.internetplantToolStripMenuItem.Enabled = false;
            this.internetplantToolStripMenuItem.Name = "internetplantToolStripMenuItem";
            this.internetplantToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.internetplantToolStripMenuItem.Text = "Internet-plant";
            this.internetplantToolStripMenuItem.Visible = false;
            this.internetplantToolStripMenuItem.Click += new System.EventHandler(this.internetplantToolStripMenuItem_Click);
            // 
            // ls2FileToolStripMenuItem
            // 
            this.ls2FileToolStripMenuItem.Name = "ls2FileToolStripMenuItem";
            this.ls2FileToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.ls2FileToolStripMenuItem.Text = "Ls2File";
            this.ls2FileToolStripMenuItem.Click += new System.EventHandler(this.ls2FileToolStripMenuItem_Click);
            // 
            // loadIntervalToolStripMenuItem
            // 
            this.loadIntervalToolStripMenuItem.Name = "loadIntervalToolStripMenuItem";
            this.loadIntervalToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.loadIntervalToolStripMenuItem.Text = "LoadInterval";
            this.loadIntervalToolStripMenuItem.Click += new System.EventHandler(this.loadIntervalToolStripMenuItem_Click);
            // 
            // dailyBillToolStripMenuItem
            // 
            this.dailyBillToolStripMenuItem.Name = "dailyBillToolStripMenuItem";
            this.dailyBillToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.dailyBillToolStripMenuItem.Text = "DailyBill";
            this.dailyBillToolStripMenuItem.Click += new System.EventHandler(this.dailyBillToolStripMenuItem_Click);
            // 
            // monthlyBillToolStripMenuItem
            // 
            this.monthlyBillToolStripMenuItem.Name = "monthlyBillToolStripMenuItem";
            this.monthlyBillToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.monthlyBillToolStripMenuItem.Text = "MonthlyBill";
            this.monthlyBillToolStripMenuItem.Click += new System.EventHandler(this.monthlyBillToolStripMenuItem_Click);
            // 
            // hubToolStripMenuItem
            // 
            this.hubToolStripMenuItem.Name = "hubToolStripMenuItem";
            this.hubToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.hubToolStripMenuItem.Text = "Hub";
            this.hubToolStripMenuItem.Click += new System.EventHandler(this.hubToolStripMenuItem_Click);
            // 
            // transmissionToolStripMenuItem
            // 
            this.transmissionToolStripMenuItem.Name = "transmissionToolStripMenuItem";
            this.transmissionToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.transmissionToolStripMenuItem.Text = "Transmission";
            this.transmissionToolStripMenuItem.Click += new System.EventHandler(this.transmissionToolStripMenuItem_Click);
            // 
            // pactualToolStripMenuItem
            // 
            this.pactualToolStripMenuItem.Name = "pactualToolStripMenuItem";
            this.pactualToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.pactualToolStripMenuItem.Text = "Pactual";
            this.pactualToolStripMenuItem.Click += new System.EventHandler(this.pactualToolStripMenuItem_Click);
            // 
            // powerLimmitedToolStripMenuItem
            // 
            this.powerLimmitedToolStripMenuItem.Name = "powerLimmitedToolStripMenuItem";
            this.powerLimmitedToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.powerLimmitedToolStripMenuItem.Text = "AvEnergy - PowerLimmited";
            this.powerLimmitedToolStripMenuItem.Click += new System.EventHandler(this.powerLimmitedToolStripMenuItem_Click);
            // 
            // priorityOnToolStripMenuItem
            // 
            this.priorityOnToolStripMenuItem.Name = "priorityOnToolStripMenuItem";
            this.priorityOnToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.priorityOnToolStripMenuItem.Text = "PriorityOn";
            this.priorityOnToolStripMenuItem.Click += new System.EventHandler(this.priorityOnToolStripMenuItem_Click);
            // 
            // riskToolStripMenuItem
            // 
            this.riskToolStripMenuItem.Name = "riskToolStripMenuItem";
            this.riskToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.riskToolStripMenuItem.Text = "Risk";
            this.riskToolStripMenuItem.Click += new System.EventHandler(this.riskToolStripMenuItem_Click);
            // 
            // strategyToolStripMenuItem4
            // 
            this.strategyToolStripMenuItem4.Name = "strategyToolStripMenuItem4";
            this.strategyToolStripMenuItem4.Size = new System.Drawing.Size(224, 22);
            this.strategyToolStripMenuItem4.Text = "Strategy";
            this.strategyToolStripMenuItem4.Click += new System.EventHandler(this.strategyToolStripMenuItem4_Click);
            // 
            // importBillFromWebServiceToolStripMenuItem
            // 
            this.importBillFromWebServiceToolStripMenuItem.Name = "importBillFromWebServiceToolStripMenuItem";
            this.importBillFromWebServiceToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.importBillFromWebServiceToolStripMenuItem.Text = "Import Bill From WebService";
            this.importBillFromWebServiceToolStripMenuItem.Click += new System.EventHandler(this.importBillFromWebServiceToolStripMenuItem_Click);
            // 
            // avEnergyToolStripMenuItem
            // 
            this.avEnergyToolStripMenuItem.Name = "avEnergyToolStripMenuItem";
            this.avEnergyToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.avEnergyToolStripMenuItem.Text = "AvEnergy";
            this.avEnergyToolStripMenuItem.Click += new System.EventHandler(this.avEnergyToolStripMenuItem_Click);
            // 
            // toolStripMenuItemStatus
            // 
            this.toolStripMenuItemStatus.Name = "toolStripMenuItemStatus";
            this.toolStripMenuItemStatus.Size = new System.Drawing.Size(51, 20);
            this.toolStripMenuItemStatus.Text = "&Status";
            this.toolStripMenuItemStatus.Click += new System.EventHandler(this.toolStripMenuItemStatus_Click);
            // 
            // toolStripMenuItemExport
            // 
            this.toolStripMenuItemExport.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItemExport.Name = "toolStripMenuItemExport";
            this.toolStripMenuItemExport.Size = new System.Drawing.Size(52, 20);
            this.toolStripMenuItemExport.Text = "&Export";
            this.toolStripMenuItemExport.Click += new System.EventHandler(this.toolStripMenuItemExport_Click);
            // 
            // ReporttoolStripMenuItem
            // 
            this.ReporttoolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.allPlantToolStripMenuItem,
            this.selectedPlantToolStripMenuItem,
            this.billReportToolStripMenuItem,
            this.salesToolStripMenuItem,
            this.daramadReportToolStripMenuItem,
            this.protestReportToolStripMenuItem,
            this.billToolStripMenuItem,
            this.billCheckToolStripMenuItem,
            this.billCheckAvEnergyToolStripMenuItem});
            this.ReporttoolStripMenuItem.Name = "ReporttoolStripMenuItem";
            this.ReporttoolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.ReporttoolStripMenuItem.Text = "&Report";
            this.ReporttoolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // allPlantToolStripMenuItem
            // 
            this.allPlantToolStripMenuItem.Name = "allPlantToolStripMenuItem";
            this.allPlantToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.allPlantToolStripMenuItem.Text = "&All Plant";
            this.allPlantToolStripMenuItem.Click += new System.EventHandler(this.allPlantToolStripMenuItem_Click);
            // 
            // selectedPlantToolStripMenuItem
            // 
            this.selectedPlantToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plantReportToolStripMenuItem,
            this.unitsReportToolStripMenuItem});
            this.selectedPlantToolStripMenuItem.Name = "selectedPlantToolStripMenuItem";
            this.selectedPlantToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.selectedPlantToolStripMenuItem.Text = "&Selected Plant";
            // 
            // plantReportToolStripMenuItem
            // 
            this.plantReportToolStripMenuItem.Name = "plantReportToolStripMenuItem";
            this.plantReportToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.plantReportToolStripMenuItem.Text = "Plant Report";
            this.plantReportToolStripMenuItem.Click += new System.EventHandler(this.plantReportToolStripMenuItem_Click);
            // 
            // unitsReportToolStripMenuItem
            // 
            this.unitsReportToolStripMenuItem.Name = "unitsReportToolStripMenuItem";
            this.unitsReportToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.unitsReportToolStripMenuItem.Text = "Units Report";
            this.unitsReportToolStripMenuItem.Click += new System.EventHandler(this.unitsReportToolStripMenuItem_Click);
            // 
            // billReportToolStripMenuItem
            // 
            this.billReportToolStripMenuItem.Name = "billReportToolStripMenuItem";
            this.billReportToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.billReportToolStripMenuItem.Text = "BillReport";
            this.billReportToolStripMenuItem.Click += new System.EventHandler(this.billReportToolStripMenuItem_Click);
            // 
            // salesToolStripMenuItem
            // 
            this.salesToolStripMenuItem.Name = "salesToolStripMenuItem";
            this.salesToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.salesToolStripMenuItem.Text = "SaleReport";
            this.salesToolStripMenuItem.Click += new System.EventHandler(this.salesToolStripMenuItem_Click);
            // 
            // daramadReportToolStripMenuItem
            // 
            this.daramadReportToolStripMenuItem.Name = "daramadReportToolStripMenuItem";
            this.daramadReportToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.daramadReportToolStripMenuItem.Text = "DaramadReport";
            this.daramadReportToolStripMenuItem.Click += new System.EventHandler(this.daramadReportToolStripMenuItem_Click);
            // 
            // protestReportToolStripMenuItem
            // 
            this.protestReportToolStripMenuItem.Name = "protestReportToolStripMenuItem";
            this.protestReportToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.protestReportToolStripMenuItem.Text = "ProtestReport";
            this.protestReportToolStripMenuItem.Click += new System.EventHandler(this.protestReportToolStripMenuItem_Click);
            // 
            // billToolStripMenuItem
            // 
            this.billToolStripMenuItem.Name = "billToolStripMenuItem";
            this.billToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.billToolStripMenuItem.Text = "Bill";
            this.billToolStripMenuItem.Click += new System.EventHandler(this.billToolStripMenuItem_Click);
            // 
            // billCheckToolStripMenuItem
            // 
            this.billCheckToolStripMenuItem.Name = "billCheckToolStripMenuItem";
            this.billCheckToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.billCheckToolStripMenuItem.Text = "Bill Check";
            this.billCheckToolStripMenuItem.Click += new System.EventHandler(this.billCheckToolStripMenuItem_Click);
            // 
            // billCheckAvEnergyToolStripMenuItem
            // 
            this.billCheckAvEnergyToolStripMenuItem.Name = "billCheckAvEnergyToolStripMenuItem";
            this.billCheckAvEnergyToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.billCheckAvEnergyToolStripMenuItem.Text = "Bill Check (AvEnergy)";
            this.billCheckAvEnergyToolStripMenuItem.Click += new System.EventHandler(this.billCheckAvEnergyToolStripMenuItem_Click);
            // 
            // oPFToolStripMenuItem
            // 
            this.oPFToolStripMenuItem.Name = "oPFToolStripMenuItem";
            this.oPFToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.oPFToolStripMenuItem.Text = "OPF";
            this.oPFToolStripMenuItem.Click += new System.EventHandler(this.oPFToolStripMenuItem_Click);
            // 
            // sensetivityAnalysisToolStripMenuItem
            // 
            this.sensetivityAnalysisToolStripMenuItem.Name = "sensetivityAnalysisToolStripMenuItem";
            this.sensetivityAnalysisToolStripMenuItem.Size = new System.Drawing.Size(121, 20);
            this.sensetivityAnalysisToolStripMenuItem.Text = "Sensetivity Analysis";
            this.sensetivityAnalysisToolStripMenuItem.Click += new System.EventHandler(this.sensetivityAnalysisToolStripMenuItem_Click);
            // 
            // wizardToolStripMenuItem
            // 
            this.wizardToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.billingToolStripMenuItem,
            this.biddingToolStripMenuItem});
            this.wizardToolStripMenuItem.Name = "wizardToolStripMenuItem";
            this.wizardToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.wizardToolStripMenuItem.Text = "Wizard";
            // 
            // billingToolStripMenuItem
            // 
            this.billingToolStripMenuItem.Name = "billingToolStripMenuItem";
            this.billingToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.billingToolStripMenuItem.Text = "Billing";
            this.billingToolStripMenuItem.Click += new System.EventHandler(this.billingToolStripMenuItem_Click);
            // 
            // biddingToolStripMenuItem
            // 
            this.biddingToolStripMenuItem.Name = "biddingToolStripMenuItem";
            this.biddingToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.biddingToolStripMenuItem.Text = "Bidding";
            // 
            // marketInformationToolStripMenuItem
            // 
            this.marketInformationToolStripMenuItem.Name = "marketInformationToolStripMenuItem";
            this.marketInformationToolStripMenuItem.Size = new System.Drawing.Size(122, 20);
            this.marketInformationToolStripMenuItem.Text = "Market Information";
            this.marketInformationToolStripMenuItem.Click += new System.EventHandler(this.marketInformationToolStripMenuItem_Click);
            // 
            // toolStripMenuItem45
            // 
            this.toolStripMenuItem45.Name = "toolStripMenuItem45";
            this.toolStripMenuItem45.Size = new System.Drawing.Size(105, 20);
            this.toolStripMenuItem45.Text = "UpdateDatabase";
            this.toolStripMenuItem45.Click += new System.EventHandler(this.toolStripMenuItem45_Click);
            // 
            // formsDesignToolStripMenuItem
            // 
            this.formsDesignToolStripMenuItem.Name = "formsDesignToolStripMenuItem";
            this.formsDesignToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.formsDesignToolStripMenuItem.Text = "FormsDesign";
            this.formsDesignToolStripMenuItem.Click += new System.EventHandler(this.formsDesignToolStripMenuItem_Click);
            // 
            // backUpToolStripMenuItem
            // 
            this.backUpToolStripMenuItem.Name = "backUpToolStripMenuItem";
            this.backUpToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.backUpToolStripMenuItem.Text = "BackUp";
            this.backUpToolStripMenuItem.Visible = false;
            this.backUpToolStripMenuItem.Click += new System.EventHandler(this.backUpToolStripMenuItem_Click);
            // 
            // profileToolStripMenuItem
            // 
            this.profileToolStripMenuItem.Name = "profileToolStripMenuItem";
            this.profileToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.profileToolStripMenuItem.Text = "Profile";
            this.profileToolStripMenuItem.Visible = false;
            this.profileToolStripMenuItem.Click += new System.EventHandler(this.profileToolStripMenuItem_Click);
            // 
            // emailToolStripMenuItem
            // 
            this.emailToolStripMenuItem.Image = global::PowerPlantProject.Properties.Resources.email;
            this.emailToolStripMenuItem.Name = "emailToolStripMenuItem";
            this.emailToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.emailToolStripMenuItem.Text = "Email";
            this.emailToolStripMenuItem.Click += new System.EventHandler(this.emailToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pDFToolStripMenuItem,
            this.wordToolStripMenuItem,
            this.toolStripSeparator1,
            this.contactUsToolStripMenuItem});
            this.helpToolStripMenuItem.Image = global::PowerPlantProject.Properties.Resources.help1;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // pDFToolStripMenuItem
            // 
            this.pDFToolStripMenuItem.Image = global::PowerPlantProject.Properties.Resources.pdf;
            this.pDFToolStripMenuItem.Name = "pDFToolStripMenuItem";
            this.pDFToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.pDFToolStripMenuItem.Text = "PDF";
            this.pDFToolStripMenuItem.Click += new System.EventHandler(this.pDFToolStripMenuItem_Click_1);
            // 
            // wordToolStripMenuItem
            // 
            this.wordToolStripMenuItem.Image = global::PowerPlantProject.Properties.Resources._1263108087_Microsoft_Office_2007_Word;
            this.wordToolStripMenuItem.Name = "wordToolStripMenuItem";
            this.wordToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.wordToolStripMenuItem.Text = "Word";
            this.wordToolStripMenuItem.Click += new System.EventHandler(this.wordToolStripMenuItem_Click_1);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(126, 6);
            // 
            // contactUsToolStripMenuItem
            // 
            this.contactUsToolStripMenuItem.Image = global::PowerPlantProject.Properties.Resources.internet7_explorer;
            this.contactUsToolStripMenuItem.Name = "contactUsToolStripMenuItem";
            this.contactUsToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.contactUsToolStripMenuItem.Text = "ContactUs";
            this.contactUsToolStripMenuItem.Click += new System.EventHandler(this.contactUsToolStripMenuItem_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // chart2
            // 
            this.chart2.AlwaysRecreateHotregions = true;
            this.chart2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart2.BackGradientEndColor = System.Drawing.Color.Azure;
            this.chart2.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft;
            this.chart2.BorderLineColor = System.Drawing.Color.SkyBlue;
            this.chart2.BorderLineStyle = Dundas.Charting.WinControl.ChartDashStyle.Solid;
            this.chart2.BorderSkin.FrameBackColor = System.Drawing.Color.Azure;
            this.chart2.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.LightBlue;
            this.chart2.BorderSkin.FrameBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.chart2.BorderSkin.FrameBorderWidth = 2;
            this.chart2.BorderSkin.PageColor = System.Drawing.Color.Transparent;
            chartArea1.Area3DStyle.WallWidth = 0;
            chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            chartArea1.AxisX.MinorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            chartArea1.AxisX.MinorTickMark.Size = 2F;
            chartArea1.AxisX.Title = "units";
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            chartArea1.AxisY.MinorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            chartArea1.AxisY.MinorTickMark.Size = 2F;
            chartArea1.AxisY.Title = "week";
            chartArea1.BackColor = System.Drawing.Color.White;
            chartArea1.BackGradientEndColor = System.Drawing.Color.White;
            chartArea1.BorderColor = System.Drawing.Color.LightSlateGray;
            chartArea1.BorderStyle = Dundas.Charting.WinControl.ChartDashStyle.Solid;
            chartArea1.Name = "Default";
            this.chart2.ChartAreas.Add(chartArea1);
            legend1.Name = "Default";
            this.chart2.Legends.Add(legend1);
            this.chart2.Location = new System.Drawing.Point(15, 227);
            this.chart2.Name = "chart2";
            this.chart2.Palette = Dundas.Charting.WinControl.ChartColorPalette.AcidWash;
            series1.BackGradientEndColor = System.Drawing.Color.White;
            series1.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalRight;
            series1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            series1.ChartType = "StackedBar";
            series1.Name = "Series1";
            series1.PaletteCustomColors = new System.Drawing.Color[0];
            this.chart2.Series.Add(series1);
            this.chart2.Size = new System.Drawing.Size(720, 391);
            this.chart2.TabIndex = 61;
            this.chart2.Text = "chart2";
            this.chart2.UI.Toolbar.BackColor = System.Drawing.Color.Transparent;
            this.chart2.UI.Toolbar.BorderSkin.PageColor = System.Drawing.Color.Transparent;
            this.chart2.UI.Toolbar.BorderStyle = Dundas.Charting.WinControl.ChartDashStyle.NotSet;
            this.chart2.UI.Toolbar.Enabled = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lbllogname
            // 
            this.lbllogname.AutoSize = true;
            this.lbllogname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)), true);
            this.lbllogname.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lbllogname.Location = new System.Drawing.Point(10, 34);
            this.lbllogname.Name = "lbllogname";
            this.lbllogname.Size = new System.Drawing.Size(89, 15);
            this.lbllogname.TabIndex = 14;
            this.lbllogname.Text = "Log In User :";
            this.toolTip1.SetToolTip(this.lbllogname, "Welcome");
            // 
            // label115
            // 
            this.label115.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label115.BackColor = System.Drawing.Color.White;
            this.label115.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label115.Location = new System.Drawing.Point(918, 5);
            this.label115.Name = "label115";
            this.label115.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label115.Size = new System.Drawing.Size(72, 19);
            this.label115.TabIndex = 34;
            this.label115.Tag = "";
            this.label115.Text = "label115";
            this.label115.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.label115, "Time");
            // 
            // colorDialog1
            // 
            this.colorDialog1.Color = System.Drawing.Color.Gray;
            this.colorDialog1.FullOpen = true;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel96
            // 
            this.panel96.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel96.BackColor = System.Drawing.Color.Transparent;
            this.panel96.BackgroundImage = global::PowerPlantProject.Properties.Resources.history;
            this.panel96.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel96.Location = new System.Drawing.Point(996, 1);
            this.panel96.Name = "panel96";
            this.panel96.Size = new System.Drawing.Size(32, 24);
            this.panel96.TabIndex = 35;
            // 
            // tbPageMaintenance
            // 
            this.tbPageMaintenance.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.tbPageMaintenance.Controls.Add(this.cmbMaintShownPlant);
            this.tbPageMaintenance.Controls.Add(this.groupBox59);
            this.tbPageMaintenance.Controls.Add(this.btnRunMaint);
            this.tbPageMaintenance.Controls.Add(this.groupBox61);
            this.tbPageMaintenance.Controls.Add(this.groupBox62);
            this.tbPageMaintenance.Location = new System.Drawing.Point(4, 29);
            this.tbPageMaintenance.Name = "tbPageMaintenance";
            this.tbPageMaintenance.Size = new System.Drawing.Size(757, 625);
            this.tbPageMaintenance.TabIndex = 7;
            this.tbPageMaintenance.Text = "Maintenance";
            this.tbPageMaintenance.UseVisualStyleBackColor = true;
            // 
            // cmbMaintShownPlant
            // 
            this.cmbMaintShownPlant.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbMaintShownPlant.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cmbMaintShownPlant.Items.AddRange(new object[] {
            "Automatic",
            "Customize"});
            this.cmbMaintShownPlant.Location = new System.Drawing.Point(29, 186);
            this.cmbMaintShownPlant.Name = "cmbMaintShownPlant";
            this.cmbMaintShownPlant.Size = new System.Drawing.Size(131, 21);
            this.cmbMaintShownPlant.TabIndex = 62;
            this.cmbMaintShownPlant.SelectedIndexChanged += new System.EventHandler(this.cmbMaintShownPlant_SelectedIndexChanged);
            // 
            // groupBox59
            // 
            this.groupBox59.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox59.BackColor = System.Drawing.Color.Beige;
            this.groupBox59.Controls.Add(this.lblMainStartDate);
            this.groupBox59.Controls.Add(this.label90);
            this.groupBox59.Controls.Add(this.lblMaintStartDate);
            this.groupBox59.Controls.Add(this.lblMaintEndDate);
            this.groupBox59.Controls.Add(this.label229);
            this.groupBox59.Controls.Add(this.label227);
            this.groupBox59.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox59.ForeColor = System.Drawing.Color.Black;
            this.groupBox59.Location = new System.Drawing.Point(411, 68);
            this.groupBox59.Name = "groupBox59";
            this.groupBox59.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox59.Size = new System.Drawing.Size(324, 100);
            this.groupBox59.TabIndex = 60;
            this.groupBox59.TabStop = false;
            this.groupBox59.Text = "setting";
            // 
            // lblMainStartDate
            // 
            this.lblMainStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMainStartDate.AutoSize = true;
            this.lblMainStartDate.Location = new System.Drawing.Point(78, 43);
            this.lblMainStartDate.Name = "lblMainStartDate";
            this.lblMainStartDate.Size = new System.Drawing.Size(75, 13);
            this.lblMainStartDate.TabIndex = 58;
            this.lblMainStartDate.Text = "1391/07/09";
            // 
            // label90
            // 
            this.label90.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(16, 43);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(73, 13);
            this.label90.TabIndex = 59;
            this.label90.Text = "Start Date :";
            // 
            // lblMaintStartDate
            // 
            this.lblMaintStartDate.AutoSize = true;
            this.lblMaintStartDate.Location = new System.Drawing.Point(-71, -3);
            this.lblMaintStartDate.Name = "lblMaintStartDate";
            this.lblMaintStartDate.Size = new System.Drawing.Size(75, 13);
            this.lblMaintStartDate.TabIndex = 40;
            this.lblMaintStartDate.Text = "1391/07/02";
            // 
            // lblMaintEndDate
            // 
            this.lblMaintEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMaintEndDate.AutoSize = true;
            this.lblMaintEndDate.Location = new System.Drawing.Point(247, 43);
            this.lblMaintEndDate.Name = "lblMaintEndDate";
            this.lblMaintEndDate.Size = new System.Drawing.Size(75, 13);
            this.lblMaintEndDate.TabIndex = 42;
            this.lblMaintEndDate.Text = "1391/07/09";
            // 
            // label229
            // 
            this.label229.AutoSize = true;
            this.label229.Location = new System.Drawing.Point(-138, -3);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(73, 13);
            this.label229.TabIndex = 41;
            this.label229.Text = "Start Date :";
            // 
            // label227
            // 
            this.label227.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label227.AutoSize = true;
            this.label227.Location = new System.Drawing.Point(186, 43);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(68, 13);
            this.label227.TabIndex = 43;
            this.label227.Text = "End Date :";
            // 
            // btnRunMaint
            // 
            this.btnRunMaint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRunMaint.BackColor = System.Drawing.Color.CadetBlue;
            this.btnRunMaint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRunMaint.Location = new System.Drawing.Point(638, 184);
            this.btnRunMaint.Name = "btnRunMaint";
            this.btnRunMaint.Size = new System.Drawing.Size(75, 23);
            this.btnRunMaint.TabIndex = 63;
            this.btnRunMaint.Text = "Run";
            this.btnRunMaint.UseVisualStyleBackColor = false;
            this.btnRunMaint.Click += new System.EventHandler(this.btnRunMaint_Click);
            // 
            // groupBox61
            // 
            this.groupBox61.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox61.BackColor = System.Drawing.Color.Beige;
            this.groupBox61.Controls.Add(this.txtMaintStatus);
            this.groupBox61.Location = new System.Drawing.Point(15, 70);
            this.groupBox61.Name = "groupBox61";
            this.groupBox61.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox61.Size = new System.Drawing.Size(385, 98);
            this.groupBox61.TabIndex = 59;
            this.groupBox61.TabStop = false;
            this.groupBox61.Text = "Status";
            // 
            // txtMaintStatus
            // 
            this.txtMaintStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaintStatus.BackColor = System.Drawing.SystemColors.Window;
            this.txtMaintStatus.Location = new System.Drawing.Point(14, 14);
            this.txtMaintStatus.Multiline = true;
            this.txtMaintStatus.Name = "txtMaintStatus";
            this.txtMaintStatus.ReadOnly = true;
            this.txtMaintStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMaintStatus.Size = new System.Drawing.Size(357, 79);
            this.txtMaintStatus.TabIndex = 0;
            // 
            // groupBox62
            // 
            this.groupBox62.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox62.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox62.BackColor = System.Drawing.Color.Beige;
            this.groupBox62.Controls.Add(this.lblMaintPlant);
            this.groupBox62.Controls.Add(this.label231);
            this.groupBox62.Controls.Add(this.label233);
            this.groupBox62.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox62.ForeColor = System.Drawing.Color.Black;
            this.groupBox62.Location = new System.Drawing.Point(15, 11);
            this.groupBox62.Name = "groupBox62";
            this.groupBox62.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox62.Size = new System.Drawing.Size(720, 53);
            this.groupBox62.TabIndex = 51;
            this.groupBox62.TabStop = false;
            // 
            // lblMaintPlant
            // 
            this.lblMaintPlant.AutoSize = true;
            this.lblMaintPlant.BackColor = System.Drawing.Color.Transparent;
            this.lblMaintPlant.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblMaintPlant.Location = new System.Drawing.Point(62, 23);
            this.lblMaintPlant.Name = "lblMaintPlant";
            this.lblMaintPlant.Size = new System.Drawing.Size(0, 13);
            this.lblMaintPlant.TabIndex = 11;
            this.lblMaintPlant.TextChanged += new System.EventHandler(this.lblMaintPlant_TextChanged);
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.BackColor = System.Drawing.Color.Transparent;
            this.label231.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label231.Location = new System.Drawing.Point(62, 23);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(0, 13);
            this.label231.TabIndex = 10;
            // 
            // label233
            // 
            this.label233.AutoSize = true;
            this.label233.BackColor = System.Drawing.Color.Transparent;
            this.label233.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label233.Location = new System.Drawing.Point(10, 23);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(52, 13);
            this.label233.TabIndex = 8;
            this.label233.Text = "Plant  : ";
            // 
            // BiddingStrategy
            // 
            this.BiddingStrategy.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.BiddingStrategy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BiddingStrategy.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BiddingStrategy.Controls.Add(this.groupBoxBiddingStrategy3);
            this.BiddingStrategy.Controls.Add(this.groupBoxBiddingStrategy2);
            this.BiddingStrategy.Controls.Add(this.groupBoxBiddingStrategy1);
            this.BiddingStrategy.Controls.Add(this.groupBox1);
            this.BiddingStrategy.Controls.Add(this.grBoxCustomize);
            this.BiddingStrategy.Location = new System.Drawing.Point(4, 29);
            this.BiddingStrategy.Name = "BiddingStrategy";
            this.BiddingStrategy.Size = new System.Drawing.Size(757, 625);
            this.BiddingStrategy.TabIndex = 4;
            this.BiddingStrategy.Text = "Bidding Strategy";
            this.BiddingStrategy.UseVisualStyleBackColor = true;
            // 
            // groupBoxBiddingStrategy3
            // 
            this.groupBoxBiddingStrategy3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.groupBoxBiddingStrategy3.Controls.Add(this.Initialprice);
            this.groupBoxBiddingStrategy3.Controls.Add(this.panel93);
            this.groupBoxBiddingStrategy3.Controls.Add(this.btnrunseting);
            this.groupBoxBiddingStrategy3.Controls.Add(this.chkmaxall);
            this.groupBoxBiddingStrategy3.Controls.Add(this.btnRunManual);
            this.groupBoxBiddingStrategy3.Controls.Add(this.btnExport);
            this.groupBoxBiddingStrategy3.Controls.Add(this.rdStrategyBidding);
            this.groupBoxBiddingStrategy3.Controls.Add(this.rdBidAllocation);
            this.groupBoxBiddingStrategy3.Controls.Add(this.rdForecastingPrice);
            this.groupBoxBiddingStrategy3.Controls.Add(this.btnRunBidding);
            this.groupBoxBiddingStrategy3.Location = new System.Drawing.Point(8, 532);
            this.groupBoxBiddingStrategy3.Name = "groupBoxBiddingStrategy3";
            this.groupBoxBiddingStrategy3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBoxBiddingStrategy3.Size = new System.Drawing.Size(737, 86);
            this.groupBoxBiddingStrategy3.TabIndex = 14;
            this.groupBoxBiddingStrategy3.TabStop = false;
            this.groupBoxBiddingStrategy3.Text = "Run";
            // 
            // Initialprice
            // 
            this.Initialprice.AutoSize = true;
            this.Initialprice.Location = new System.Drawing.Point(438, 32);
            this.Initialprice.Name = "Initialprice";
            this.Initialprice.Size = new System.Drawing.Size(68, 17);
            this.Initialprice.TabIndex = 48;
            this.Initialprice.Text = "Initial Bid";
            this.Initialprice.UseVisualStyleBackColor = true;
            // 
            // panel93
            // 
            this.panel93.BackColor = System.Drawing.Color.Transparent;
            this.panel93.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel93.Location = new System.Drawing.Point(708, 19);
            this.panel93.Name = "panel93";
            this.panel93.Size = new System.Drawing.Size(26, 23);
            this.panel93.TabIndex = 47;
            // 
            // btnrunseting
            // 
            this.btnrunseting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnrunseting.BackColor = System.Drawing.Color.CadetBlue;
            this.btnrunseting.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnrunseting.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnrunseting.Location = new System.Drawing.Point(328, 42);
            this.btnrunseting.Name = "btnrunseting";
            this.btnrunseting.Size = new System.Drawing.Size(84, 23);
            this.btnrunseting.TabIndex = 46;
            this.btnrunseting.Text = "RunSetting";
            this.btnrunseting.UseVisualStyleBackColor = false;
            this.btnrunseting.Click += new System.EventHandler(this.btnrunseting_Click);
            // 
            // chkmaxall
            // 
            this.chkmaxall.AutoSize = true;
            this.chkmaxall.Location = new System.Drawing.Point(438, 55);
            this.chkmaxall.Name = "chkmaxall";
            this.chkmaxall.Size = new System.Drawing.Size(74, 17);
            this.chkmaxall.TabIndex = 43;
            this.chkmaxall.Text = "Similar Bid";
            this.chkmaxall.UseVisualStyleBackColor = true;
            this.chkmaxall.Visible = false;
            // 
            // btnRunManual
            // 
            this.btnRunManual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRunManual.BackColor = System.Drawing.Color.CadetBlue;
            this.btnRunManual.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRunManual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnRunManual.Location = new System.Drawing.Point(542, 51);
            this.btnRunManual.Name = "btnRunManual";
            this.btnRunManual.Size = new System.Drawing.Size(166, 23);
            this.btnRunManual.TabIndex = 45;
            this.btnRunManual.Text = "Run Manually";
            this.btnRunManual.UseVisualStyleBackColor = false;
            this.btnRunManual.Click += new System.EventHandler(this.btnRunManual_Click);
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.BackColor = System.Drawing.Color.CadetBlue;
            this.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnExport.Location = new System.Drawing.Point(632, 19);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 23);
            this.btnExport.TabIndex = 39;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // rdStrategyBidding
            // 
            this.rdStrategyBidding.AutoSize = true;
            this.rdStrategyBidding.Location = new System.Drawing.Point(163, 40);
            this.rdStrategyBidding.Name = "rdStrategyBidding";
            this.rdStrategyBidding.Size = new System.Drawing.Size(78, 17);
            this.rdStrategyBidding.TabIndex = 38;
            this.rdStrategyBidding.TabStop = true;
            this.rdStrategyBidding.Text = "Old version";
            this.rdStrategyBidding.UseVisualStyleBackColor = true;
            // 
            // rdBidAllocation
            // 
            this.rdBidAllocation.AutoSize = true;
            this.rdBidAllocation.Location = new System.Drawing.Point(20, 41);
            this.rdBidAllocation.Name = "rdBidAllocation";
            this.rdBidAllocation.Size = new System.Drawing.Size(102, 17);
            this.rdBidAllocation.TabIndex = 37;
            this.rdBidAllocation.TabStop = true;
            this.rdBidAllocation.Text = "Bidding Strategy";
            this.rdBidAllocation.UseVisualStyleBackColor = true;
            this.rdBidAllocation.CheckedChanged += new System.EventHandler(this.rdBidAllocation_CheckedChanged);
            // 
            // rdForecastingPrice
            // 
            this.rdForecastingPrice.AutoSize = true;
            this.rdForecastingPrice.Enabled = false;
            this.rdForecastingPrice.Location = new System.Drawing.Point(163, 17);
            this.rdForecastingPrice.Name = "rdForecastingPrice";
            this.rdForecastingPrice.Size = new System.Drawing.Size(107, 17);
            this.rdForecastingPrice.TabIndex = 36;
            this.rdForecastingPrice.TabStop = true;
            this.rdForecastingPrice.Text = "Price Forecasting";
            this.rdForecastingPrice.UseVisualStyleBackColor = true;
            this.rdForecastingPrice.Visible = false;
            // 
            // btnRunBidding
            // 
            this.btnRunBidding.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRunBidding.BackColor = System.Drawing.Color.CadetBlue;
            this.btnRunBidding.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRunBidding.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnRunBidding.Location = new System.Drawing.Point(542, 19);
            this.btnRunBidding.Name = "btnRunBidding";
            this.btnRunBidding.Size = new System.Drawing.Size(75, 23);
            this.btnRunBidding.TabIndex = 32;
            this.btnRunBidding.Text = "Run";
            this.btnRunBidding.UseVisualStyleBackColor = false;
            this.btnRunBidding.Click += new System.EventHandler(this.btnRunBidding_Click);
            // 
            // groupBoxBiddingStrategy2
            // 
            this.groupBoxBiddingStrategy2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.groupBoxBiddingStrategy2.Controls.Add(this.lstStatus);
            this.groupBoxBiddingStrategy2.Location = new System.Drawing.Point(8, 430);
            this.groupBoxBiddingStrategy2.Name = "groupBoxBiddingStrategy2";
            this.groupBoxBiddingStrategy2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBoxBiddingStrategy2.Size = new System.Drawing.Size(737, 96);
            this.groupBoxBiddingStrategy2.TabIndex = 39;
            this.groupBoxBiddingStrategy2.TabStop = false;
            this.groupBoxBiddingStrategy2.Text = "Status";
            // 
            // lstStatus
            // 
            this.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lstStatus.BackColor = System.Drawing.SystemColors.Window;
            this.lstStatus.Location = new System.Drawing.Point(14, 13);
            this.lstStatus.Multiline = true;
            this.lstStatus.Name = "lstStatus";
            this.lstStatus.ReadOnly = true;
            this.lstStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lstStatus.Size = new System.Drawing.Size(709, 77);
            this.lstStatus.TabIndex = 0;
            // 
            // groupBoxBiddingStrategy1
            // 
            this.groupBoxBiddingStrategy1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBoxBiddingStrategy1.Controls.Add(this.groupBox18);
            this.groupBoxBiddingStrategy1.Controls.Add(this.groupBox19);
            this.groupBoxBiddingStrategy1.Location = new System.Drawing.Point(7, 65);
            this.groupBoxBiddingStrategy1.Name = "groupBoxBiddingStrategy1";
            this.groupBoxBiddingStrategy1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBoxBiddingStrategy1.Size = new System.Drawing.Size(738, 194);
            this.groupBoxBiddingStrategy1.TabIndex = 42;
            this.groupBoxBiddingStrategy1.TabStop = false;
            this.groupBoxBiddingStrategy1.Text = "Run";
            // 
            // groupBox18
            // 
            this.groupBox18.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox18.BackColor = System.Drawing.Color.Beige;
            this.groupBox18.Controls.Add(this.panel80);
            this.groupBox18.Controls.Add(this.panel81);
            this.groupBox18.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox18.ForeColor = System.Drawing.Color.Black;
            this.groupBox18.Location = new System.Drawing.Point(179, 14);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox18.Size = new System.Drawing.Size(381, 75);
            this.groupBox18.TabIndex = 20;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = " DATE";
            // 
            // panel80
            // 
            this.panel80.BackColor = System.Drawing.Color.CadetBlue;
            this.panel80.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel80.Controls.Add(this.label15);
            this.panel80.Controls.Add(this.label16);
            this.panel80.Controls.Add(this.datePickerBidding);
            this.panel80.Location = new System.Drawing.Point(120, 17);
            this.panel80.Name = "panel80";
            this.panel80.Size = new System.Drawing.Size(140, 49);
            this.panel80.TabIndex = 6;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label15.Location = new System.Drawing.Point(33, 8);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Bidding Date";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label16.Location = new System.Drawing.Point(23, 8);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(0, 13);
            this.label16.TabIndex = 1;
            // 
            // datePickerBidding
            // 
            this.datePickerBidding.HasButtons = true;
            this.datePickerBidding.Location = new System.Drawing.Point(14, 24);
            this.datePickerBidding.Name = "datePickerBidding";
            this.datePickerBidding.Readonly = true;
            this.datePickerBidding.Size = new System.Drawing.Size(120, 20);
            this.datePickerBidding.TabIndex = 32;
            this.datePickerBidding.SelectedDateTimeChanged += new System.EventHandler(this.datePickerBidding_SelectedDateTimeChanged);
            // 
            // panel81
            // 
            this.panel81.BackColor = System.Drawing.Color.CadetBlue;
            this.panel81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel81.Controls.Add(this.datePickerCurrent);
            this.panel81.Controls.Add(this.label17);
            this.panel81.Location = new System.Drawing.Point(4, 17);
            this.panel81.Name = "panel81";
            this.panel81.Size = new System.Drawing.Size(78, 49);
            this.panel81.TabIndex = 6;
            this.panel81.Visible = false;
            // 
            // datePickerCurrent
            // 
            this.datePickerCurrent.HasButtons = true;
            this.datePickerCurrent.Location = new System.Drawing.Point(3, 24);
            this.datePickerCurrent.Name = "datePickerCurrent";
            this.datePickerCurrent.Readonly = true;
            this.datePickerCurrent.Size = new System.Drawing.Size(68, 20);
            this.datePickerCurrent.TabIndex = 33;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label17.Location = new System.Drawing.Point(-2, 7);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Current Date";
            // 
            // groupBox19
            // 
            this.groupBox19.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.groupBox19.BackColor = System.Drawing.Color.Beige;
            this.groupBox19.Controls.Add(this.panel90);
            this.groupBox19.Controls.Add(this.panel24);
            this.groupBox19.Controls.Add(this.panel75);
            this.groupBox19.Controls.Add(this.panel77);
            this.groupBox19.Controls.Add(this.panel78);
            this.groupBox19.Controls.Add(this.panel79);
            this.groupBox19.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox19.ForeColor = System.Drawing.Color.Black;
            this.groupBox19.Location = new System.Drawing.Point(15, 97);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox19.Size = new System.Drawing.Size(709, 88);
            this.groupBox19.TabIndex = 21;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "setting";
            // 
            // panel90
            // 
            this.panel90.BackColor = System.Drawing.Color.CadetBlue;
            this.panel90.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel90.Controls.Add(this.label98);
            this.panel90.Controls.Add(this.txtpowertraining);
            this.panel90.Location = new System.Drawing.Point(355, 21);
            this.panel90.Name = "panel90";
            this.panel90.Size = new System.Drawing.Size(117, 49);
            this.panel90.TabIndex = 10;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.BackColor = System.Drawing.Color.Transparent;
            this.label98.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label98.Location = new System.Drawing.Point(-2, 6);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(120, 13);
            this.label98.TabIndex = 1;
            this.label98.Text = "PowerTraining Days";
            // 
            // txtpowertraining
            // 
            this.txtpowertraining.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtpowertraining.Location = new System.Drawing.Point(5, 22);
            this.txtpowertraining.Name = "txtpowertraining";
            this.txtpowertraining.Size = new System.Drawing.Size(104, 20);
            this.txtpowertraining.TabIndex = 0;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.CadetBlue;
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel24.Controls.Add(this.cmbPeakBid);
            this.panel24.Controls.Add(this.label5);
            this.panel24.Controls.Add(this.label6);
            this.panel24.Controls.Add(this.label7);
            this.panel24.Location = new System.Drawing.Point(590, 21);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(113, 49);
            this.panel24.TabIndex = 9;
            // 
            // cmbPeakBid
            // 
            this.cmbPeakBid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cmbPeakBid.FormattingEnabled = true;
            this.cmbPeakBid.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeakBid.Location = new System.Drawing.Point(4, 21);
            this.cmbPeakBid.Name = "cmbPeakBid";
            this.cmbPeakBid.Size = new System.Drawing.Size(104, 21);
            this.cmbPeakBid.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(24, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = " Peak Bid";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label6.Location = new System.Drawing.Point(25, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(23, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 1;
            // 
            // panel75
            // 
            this.panel75.BackColor = System.Drawing.Color.CadetBlue;
            this.panel75.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel75.Controls.Add(this.cmbCostLevel);
            this.panel75.Controls.Add(this.label9);
            this.panel75.Controls.Add(this.label10);
            this.panel75.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel75.Location = new System.Drawing.Point(474, 21);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(113, 49);
            this.panel75.TabIndex = 8;
            // 
            // cmbCostLevel
            // 
            this.cmbCostLevel.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbCostLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cmbCostLevel.Items.AddRange(new object[] {
            "Demand Fixed",
            "Manual",
            "Software"});
            this.cmbCostLevel.Location = new System.Drawing.Point(3, 22);
            this.cmbCostLevel.Name = "cmbCostLevel";
            this.cmbCostLevel.Size = new System.Drawing.Size(104, 21);
            this.cmbCostLevel.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label9.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label9.Location = new System.Drawing.Point(17, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Bid History";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label10.Location = new System.Drawing.Point(23, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 13);
            this.label10.TabIndex = 1;
            // 
            // panel77
            // 
            this.panel77.BackColor = System.Drawing.Color.CadetBlue;
            this.panel77.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel77.Controls.Add(this.cmbStrategyBidding);
            this.panel77.Controls.Add(this.label11);
            this.panel77.Controls.Add(this.label12);
            this.panel77.Location = new System.Drawing.Point(122, 21);
            this.panel77.Name = "panel77";
            this.panel77.Size = new System.Drawing.Size(113, 49);
            this.panel77.TabIndex = 6;
            // 
            // cmbStrategyBidding
            // 
            this.cmbStrategyBidding.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbStrategyBidding.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cmbStrategyBidding.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategyBidding.Location = new System.Drawing.Point(4, 22);
            this.cmbStrategyBidding.Name = "cmbStrategyBidding";
            this.cmbStrategyBidding.Size = new System.Drawing.Size(104, 21);
            this.cmbStrategyBidding.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.label11.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label11.Location = new System.Drawing.Point(2, 6);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(104, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = " Bidding Strategy";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label12.Location = new System.Drawing.Point(23, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 13);
            this.label12.TabIndex = 1;
            // 
            // panel78
            // 
            this.panel78.BackColor = System.Drawing.Color.CadetBlue;
            this.panel78.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel78.Controls.Add(this.txtTraining);
            this.panel78.Controls.Add(this.label13);
            this.panel78.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel78.Location = new System.Drawing.Point(239, 21);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(113, 49);
            this.panel78.TabIndex = 7;
            // 
            // txtTraining
            // 
            this.txtTraining.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTraining.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtTraining.Location = new System.Drawing.Point(3, 22);
            this.txtTraining.Name = "txtTraining";
            this.txtTraining.Size = new System.Drawing.Size(104, 20);
            this.txtTraining.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label13.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label13.Location = new System.Drawing.Point(-2, 6);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(114, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "PriceTraining Days";
            // 
            // panel79
            // 
            this.panel79.BackColor = System.Drawing.Color.CadetBlue;
            this.panel79.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel79.Controls.Add(this.cmbRunSetting);
            this.panel79.Controls.Add(this.label14);
            this.panel79.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel79.Location = new System.Drawing.Point(6, 21);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(113, 49);
            this.panel79.TabIndex = 6;
            // 
            // cmbRunSetting
            // 
            this.cmbRunSetting.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbRunSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cmbRunSetting.Items.AddRange(new object[] {
            "Automatic",
            "Customize"});
            this.cmbRunSetting.Location = new System.Drawing.Point(3, 22);
            this.cmbRunSetting.Name = "cmbRunSetting";
            this.cmbRunSetting.Size = new System.Drawing.Size(104, 21);
            this.cmbRunSetting.TabIndex = 1;
            this.cmbRunSetting.SelectedIndexChanged += new System.EventHandler(this.cmbRunSetting_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.label14.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label14.Location = new System.Drawing.Point(23, 6);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = " Setting";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox1.BackColor = System.Drawing.Color.Beige;
            this.groupBox1.Controls.Add(this.lblPlantValue);
            this.groupBox1.Controls.Add(this.lblGencoValue);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(15, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox1.Size = new System.Drawing.Size(720, 53);
            this.groupBox1.TabIndex = 41;
            this.groupBox1.TabStop = false;
            // 
            // lblPlantValue
            // 
            this.lblPlantValue.AutoSize = true;
            this.lblPlantValue.BackColor = System.Drawing.Color.Transparent;
            this.lblPlantValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblPlantValue.Location = new System.Drawing.Point(493, 23);
            this.lblPlantValue.Name = "lblPlantValue";
            this.lblPlantValue.Size = new System.Drawing.Size(0, 13);
            this.lblPlantValue.TabIndex = 10;
            this.lblPlantValue.TextChanged += new System.EventHandler(this.lblPlantValue_TextChanged);
            // 
            // lblGencoValue
            // 
            this.lblGencoValue.AutoSize = true;
            this.lblGencoValue.BackColor = System.Drawing.Color.Transparent;
            this.lblGencoValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblGencoValue.Location = new System.Drawing.Point(71, 23);
            this.lblGencoValue.Name = "lblGencoValue";
            this.lblGencoValue.Size = new System.Drawing.Size(0, 13);
            this.lblGencoValue.TabIndex = 9;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label18.Location = new System.Drawing.Point(443, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 13);
            this.label18.TabIndex = 8;
            this.label18.Text = "Plant  : ";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label23.Location = new System.Drawing.Point(10, 23);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 13);
            this.label23.TabIndex = 7;
            this.label23.Text = "Genco  : ";
            // 
            // grBoxCustomize
            // 
            this.grBoxCustomize.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.grBoxCustomize.Controls.Add(this.menuStrip9);
            this.grBoxCustomize.Controls.Add(this.menuStrip10);
            this.grBoxCustomize.Controls.Add(this.menuStrip11);
            this.grBoxCustomize.Controls.Add(this.menuStrip12);
            this.grBoxCustomize.Controls.Add(this.menuStrip7);
            this.grBoxCustomize.Controls.Add(this.menuStrip6);
            this.grBoxCustomize.Controls.Add(this.menuStrip8);
            this.grBoxCustomize.Controls.Add(this.menuStrip5);
            this.grBoxCustomize.Controls.Add(this.menuStrip3);
            this.grBoxCustomize.Controls.Add(this.menuStrip4);
            this.grBoxCustomize.Controls.Add(this.menuStrip2);
            this.grBoxCustomize.Location = new System.Drawing.Point(7, 261);
            this.grBoxCustomize.Name = "grBoxCustomize";
            this.grBoxCustomize.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grBoxCustomize.Size = new System.Drawing.Size(740, 163);
            this.grBoxCustomize.TabIndex = 38;
            this.grBoxCustomize.TabStop = false;
            this.grBoxCustomize.Text = "Customize";
            // 
            // menuStrip9
            // 
            this.menuStrip9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip9.AutoSize = false;
            this.menuStrip9.BackColor = System.Drawing.Color.Beige;
            this.menuStrip9.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip9.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripComboBox1,
            this.toolStripComboBox2,
            this.toolStripMenuItem29,
            this.toolStripComboBox3,
            this.toolStripComboBox4,
            this.toolStripMenuItem30,
            this.toolStripComboBox5,
            this.toolStripComboBox6,
            this.toolStripMenuItem31,
            this.toolStripComboBox7,
            this.toolStripComboBox8});
            this.menuStrip9.Location = new System.Drawing.Point(6, 85);
            this.menuStrip9.Name = "menuStrip9";
            this.menuStrip9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip9.Size = new System.Drawing.Size(730, 24);
            this.menuStrip9.TabIndex = 9;
            this.menuStrip9.Text = "menuStrip9";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem2.Text = "hour - 4";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.AutoSize = false;
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox2
            // 
            this.toolStripComboBox2.AutoSize = false;
            this.toolStripComboBox2.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox2.Name = "toolStripComboBox2";
            this.toolStripComboBox2.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem29
            // 
            this.toolStripMenuItem29.Name = "toolStripMenuItem29";
            this.toolStripMenuItem29.Size = new System.Drawing.Size(79, 20);
            this.toolStripMenuItem29.Text = "    hour - 10";
            // 
            // toolStripComboBox3
            // 
            this.toolStripComboBox3.AutoSize = false;
            this.toolStripComboBox3.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox3.Name = "toolStripComboBox3";
            this.toolStripComboBox3.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox4
            // 
            this.toolStripComboBox4.AutoSize = false;
            this.toolStripComboBox4.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox4.Name = "toolStripComboBox4";
            this.toolStripComboBox4.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem30
            // 
            this.toolStripMenuItem30.Name = "toolStripMenuItem30";
            this.toolStripMenuItem30.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem30.Text = "     hour - 16";
            // 
            // toolStripComboBox5
            // 
            this.toolStripComboBox5.AutoSize = false;
            this.toolStripComboBox5.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox5.Name = "toolStripComboBox5";
            this.toolStripComboBox5.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox6
            // 
            this.toolStripComboBox6.AutoSize = false;
            this.toolStripComboBox6.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox6.Name = "toolStripComboBox6";
            this.toolStripComboBox6.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem31
            // 
            this.toolStripMenuItem31.Name = "toolStripMenuItem31";
            this.toolStripMenuItem31.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem31.Text = "     hour - 22";
            // 
            // toolStripComboBox7
            // 
            this.toolStripComboBox7.AutoSize = false;
            this.toolStripComboBox7.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox7.Name = "toolStripComboBox7";
            this.toolStripComboBox7.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox8
            // 
            this.toolStripComboBox8.AutoSize = false;
            this.toolStripComboBox8.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox8.Name = "toolStripComboBox8";
            this.toolStripComboBox8.Size = new System.Drawing.Size(38, 23);
            // 
            // menuStrip10
            // 
            this.menuStrip10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip10.AutoSize = false;
            this.menuStrip10.BackColor = System.Drawing.Color.Beige;
            this.menuStrip10.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip10.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem32,
            this.toolStripComboBox9,
            this.toolStripComboBox10,
            this.toolStripMenuItem33,
            this.toolStripComboBox11,
            this.toolStripComboBox12,
            this.toolStripMenuItem34,
            this.toolStripComboBox13,
            this.toolStripComboBox14,
            this.toolStripMenuItem35,
            this.toolStripComboBox15,
            this.toolStripComboBox16});
            this.menuStrip10.Location = new System.Drawing.Point(6, 43);
            this.menuStrip10.Name = "menuStrip10";
            this.menuStrip10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip10.Size = new System.Drawing.Size(730, 24);
            this.menuStrip10.TabIndex = 7;
            this.menuStrip10.Text = "menuStrip10";
            // 
            // toolStripMenuItem32
            // 
            this.toolStripMenuItem32.Name = "toolStripMenuItem32";
            this.toolStripMenuItem32.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem32.Text = "hour - 2";
            // 
            // toolStripComboBox9
            // 
            this.toolStripComboBox9.AutoSize = false;
            this.toolStripComboBox9.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox9.Name = "toolStripComboBox9";
            this.toolStripComboBox9.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox10
            // 
            this.toolStripComboBox10.AutoSize = false;
            this.toolStripComboBox10.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox10.Name = "toolStripComboBox10";
            this.toolStripComboBox10.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem33
            // 
            this.toolStripMenuItem33.Name = "toolStripMenuItem33";
            this.toolStripMenuItem33.Size = new System.Drawing.Size(79, 20);
            this.toolStripMenuItem33.Text = "    hour - 8  ";
            // 
            // toolStripComboBox11
            // 
            this.toolStripComboBox11.AutoSize = false;
            this.toolStripComboBox11.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox11.Name = "toolStripComboBox11";
            this.toolStripComboBox11.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox12
            // 
            this.toolStripComboBox12.AutoSize = false;
            this.toolStripComboBox12.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox12.Name = "toolStripComboBox12";
            this.toolStripComboBox12.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem34
            // 
            this.toolStripMenuItem34.Name = "toolStripMenuItem34";
            this.toolStripMenuItem34.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem34.Text = "     hour - 14";
            // 
            // toolStripComboBox13
            // 
            this.toolStripComboBox13.AutoSize = false;
            this.toolStripComboBox13.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox13.Name = "toolStripComboBox13";
            this.toolStripComboBox13.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox14
            // 
            this.toolStripComboBox14.AutoSize = false;
            this.toolStripComboBox14.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox14.Name = "toolStripComboBox14";
            this.toolStripComboBox14.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem35
            // 
            this.toolStripMenuItem35.Name = "toolStripMenuItem35";
            this.toolStripMenuItem35.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem35.Text = "     hour - 20";
            // 
            // toolStripComboBox15
            // 
            this.toolStripComboBox15.AutoSize = false;
            this.toolStripComboBox15.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox15.Name = "toolStripComboBox15";
            this.toolStripComboBox15.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox16
            // 
            this.toolStripComboBox16.AutoSize = false;
            this.toolStripComboBox16.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox16.Name = "toolStripComboBox16";
            this.toolStripComboBox16.Size = new System.Drawing.Size(38, 23);
            // 
            // menuStrip11
            // 
            this.menuStrip11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip11.AutoSize = false;
            this.menuStrip11.BackColor = System.Drawing.Color.Beige;
            this.menuStrip11.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip11.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem36,
            this.toolStripComboBox17,
            this.toolStripComboBox18,
            this.toolStripMenuItem37,
            this.toolStripComboBox19,
            this.toolStripComboBox20,
            this.toolStripMenuItem38,
            this.toolStripComboBox21,
            this.toolStripComboBox22,
            this.toolStripMenuItem39,
            this.toolStripComboBox23,
            this.toolStripComboBox24});
            this.menuStrip11.Location = new System.Drawing.Point(6, 65);
            this.menuStrip11.Name = "menuStrip11";
            this.menuStrip11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip11.Size = new System.Drawing.Size(730, 24);
            this.menuStrip11.TabIndex = 8;
            this.menuStrip11.Text = "menuStrip11";
            // 
            // toolStripMenuItem36
            // 
            this.toolStripMenuItem36.Name = "toolStripMenuItem36";
            this.toolStripMenuItem36.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem36.Text = "hour - 3";
            // 
            // toolStripComboBox17
            // 
            this.toolStripComboBox17.AutoSize = false;
            this.toolStripComboBox17.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox17.Name = "toolStripComboBox17";
            this.toolStripComboBox17.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox18
            // 
            this.toolStripComboBox18.AutoSize = false;
            this.toolStripComboBox18.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox18.Name = "toolStripComboBox18";
            this.toolStripComboBox18.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem37
            // 
            this.toolStripMenuItem37.Name = "toolStripMenuItem37";
            this.toolStripMenuItem37.Size = new System.Drawing.Size(79, 20);
            this.toolStripMenuItem37.Text = "    hour - 9  ";
            // 
            // toolStripComboBox19
            // 
            this.toolStripComboBox19.AutoSize = false;
            this.toolStripComboBox19.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox19.Name = "toolStripComboBox19";
            this.toolStripComboBox19.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox20
            // 
            this.toolStripComboBox20.AutoSize = false;
            this.toolStripComboBox20.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox20.Name = "toolStripComboBox20";
            this.toolStripComboBox20.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem38
            // 
            this.toolStripMenuItem38.Name = "toolStripMenuItem38";
            this.toolStripMenuItem38.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem38.Text = "     hour - 15";
            // 
            // toolStripComboBox21
            // 
            this.toolStripComboBox21.AutoSize = false;
            this.toolStripComboBox21.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox21.Name = "toolStripComboBox21";
            this.toolStripComboBox21.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox22
            // 
            this.toolStripComboBox22.AutoSize = false;
            this.toolStripComboBox22.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox22.Name = "toolStripComboBox22";
            this.toolStripComboBox22.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem39
            // 
            this.toolStripMenuItem39.Name = "toolStripMenuItem39";
            this.toolStripMenuItem39.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem39.Text = "     hour - 21";
            // 
            // toolStripComboBox23
            // 
            this.toolStripComboBox23.AutoSize = false;
            this.toolStripComboBox23.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox23.Name = "toolStripComboBox23";
            this.toolStripComboBox23.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox24
            // 
            this.toolStripComboBox24.AutoSize = false;
            this.toolStripComboBox24.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox24.Name = "toolStripComboBox24";
            this.toolStripComboBox24.Size = new System.Drawing.Size(38, 23);
            // 
            // menuStrip12
            // 
            this.menuStrip12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip12.AutoSize = false;
            this.menuStrip12.BackColor = System.Drawing.Color.Beige;
            this.menuStrip12.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip12.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem40,
            this.toolStripComboBox25,
            this.toolStripComboBox26,
            this.toolStripMenuItem41,
            this.toolStripComboBox27,
            this.toolStripComboBox28,
            this.toolStripMenuItem42,
            this.toolStripComboBox29,
            this.toolStripComboBox30,
            this.toolStripMenuItem43,
            this.toolStripComboBox31,
            this.toolStripComboBox32});
            this.menuStrip12.Location = new System.Drawing.Point(6, 21);
            this.menuStrip12.Name = "menuStrip12";
            this.menuStrip12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip12.Size = new System.Drawing.Size(730, 24);
            this.menuStrip12.TabIndex = 6;
            this.menuStrip12.Text = "menuStrip12";
            // 
            // toolStripMenuItem40
            // 
            this.toolStripMenuItem40.Name = "toolStripMenuItem40";
            this.toolStripMenuItem40.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem40.Text = "hour - 1";
            // 
            // toolStripComboBox25
            // 
            this.toolStripComboBox25.AutoSize = false;
            this.toolStripComboBox25.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox25.Name = "toolStripComboBox25";
            this.toolStripComboBox25.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox26
            // 
            this.toolStripComboBox26.AutoSize = false;
            this.toolStripComboBox26.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox26.Name = "toolStripComboBox26";
            this.toolStripComboBox26.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem41
            // 
            this.toolStripMenuItem41.Name = "toolStripMenuItem41";
            this.toolStripMenuItem41.Size = new System.Drawing.Size(79, 20);
            this.toolStripMenuItem41.Text = "    hour - 7  ";
            // 
            // toolStripComboBox27
            // 
            this.toolStripComboBox27.AutoSize = false;
            this.toolStripComboBox27.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox27.Name = "toolStripComboBox27";
            this.toolStripComboBox27.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox28
            // 
            this.toolStripComboBox28.AutoSize = false;
            this.toolStripComboBox28.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox28.Name = "toolStripComboBox28";
            this.toolStripComboBox28.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem42
            // 
            this.toolStripMenuItem42.Name = "toolStripMenuItem42";
            this.toolStripMenuItem42.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem42.Text = "     hour - 13";
            // 
            // toolStripComboBox29
            // 
            this.toolStripComboBox29.AutoSize = false;
            this.toolStripComboBox29.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox29.Name = "toolStripComboBox29";
            this.toolStripComboBox29.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox30
            // 
            this.toolStripComboBox30.AutoSize = false;
            this.toolStripComboBox30.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox30.Name = "toolStripComboBox30";
            this.toolStripComboBox30.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem43
            // 
            this.toolStripMenuItem43.Name = "toolStripMenuItem43";
            this.toolStripMenuItem43.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem43.Text = "     hour - 19";
            // 
            // toolStripComboBox31
            // 
            this.toolStripComboBox31.AutoSize = false;
            this.toolStripComboBox31.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.toolStripComboBox31.Name = "toolStripComboBox31";
            this.toolStripComboBox31.Size = new System.Drawing.Size(65, 23);
            // 
            // toolStripComboBox32
            // 
            this.toolStripComboBox32.AutoSize = false;
            this.toolStripComboBox32.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.toolStripComboBox32.Name = "toolStripComboBox32";
            this.toolStripComboBox32.Size = new System.Drawing.Size(38, 23);
            // 
            // menuStrip7
            // 
            this.menuStrip7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip7.AutoSize = false;
            this.menuStrip7.BackColor = System.Drawing.Color.Beige;
            this.menuStrip7.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem17,
            this.cmbStrategy6,
            this.cmbPeak6,
            this.toolStripMenuItem18,
            this.cmbStrategy12,
            this.cmbPeak12,
            this.toolStripMenuItem19,
            this.cmbStrategy18,
            this.cmbPeak18,
            this.toolStripMenuItem20,
            this.cmbStrategy24,
            this.cmbPeak24});
            this.menuStrip7.Location = new System.Drawing.Point(6, 130);
            this.menuStrip7.Name = "menuStrip7";
            this.menuStrip7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip7.Size = new System.Drawing.Size(730, 24);
            this.menuStrip7.TabIndex = 5;
            this.menuStrip7.Text = "menuStrip7";
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem17.Text = "hour - 6";
            // 
            // cmbStrategy6
            // 
            this.cmbStrategy6.AutoSize = false;
            this.cmbStrategy6.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy6.Name = "cmbStrategy6";
            this.cmbStrategy6.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak6
            // 
            this.cmbPeak6.AutoSize = false;
            this.cmbPeak6.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak6.Name = "cmbPeak6";
            this.cmbPeak6.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(79, 20);
            this.toolStripMenuItem18.Text = "    hour - 12";
            // 
            // cmbStrategy12
            // 
            this.cmbStrategy12.AutoSize = false;
            this.cmbStrategy12.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy12.Name = "cmbStrategy12";
            this.cmbStrategy12.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak12
            // 
            this.cmbPeak12.AutoSize = false;
            this.cmbPeak12.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak12.Name = "cmbPeak12";
            this.cmbPeak12.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem19
            // 
            this.toolStripMenuItem19.Name = "toolStripMenuItem19";
            this.toolStripMenuItem19.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem19.Text = "     hour - 18";
            // 
            // cmbStrategy18
            // 
            this.cmbStrategy18.AutoSize = false;
            this.cmbStrategy18.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy18.Name = "cmbStrategy18";
            this.cmbStrategy18.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak18
            // 
            this.cmbPeak18.AutoSize = false;
            this.cmbPeak18.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak18.Name = "cmbPeak18";
            this.cmbPeak18.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem20
            // 
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            this.toolStripMenuItem20.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem20.Text = "     hour - 24";
            // 
            // cmbStrategy24
            // 
            this.cmbStrategy24.AutoSize = false;
            this.cmbStrategy24.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy24.Name = "cmbStrategy24";
            this.cmbStrategy24.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak24
            // 
            this.cmbPeak24.AutoSize = false;
            this.cmbPeak24.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak24.Name = "cmbPeak24";
            this.cmbPeak24.Size = new System.Drawing.Size(38, 23);
            // 
            // menuStrip6
            // 
            this.menuStrip6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip6.AutoSize = false;
            this.menuStrip6.BackColor = System.Drawing.Color.Beige;
            this.menuStrip6.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem13,
            this.cmbStrategy5,
            this.cmbPeak5,
            this.toolStripMenuItem14,
            this.cmbStrategy11,
            this.cmbPeak11,
            this.toolStripMenuItem15,
            this.cmbStrategy17,
            this.cmbPeak17,
            this.toolStripMenuItem16,
            this.cmbStrategy23,
            this.cmbPeak23});
            this.menuStrip6.Location = new System.Drawing.Point(6, 107);
            this.menuStrip6.Name = "menuStrip6";
            this.menuStrip6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip6.Size = new System.Drawing.Size(730, 24);
            this.menuStrip6.TabIndex = 4;
            this.menuStrip6.Text = "menuStrip6";
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem13.Text = "hour - 5";
            // 
            // cmbStrategy5
            // 
            this.cmbStrategy5.AutoSize = false;
            this.cmbStrategy5.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy5.Name = "cmbStrategy5";
            this.cmbStrategy5.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak5
            // 
            this.cmbPeak5.AutoSize = false;
            this.cmbPeak5.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak5.Name = "cmbPeak5";
            this.cmbPeak5.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(79, 20);
            this.toolStripMenuItem14.Text = "    hour - 11";
            // 
            // cmbStrategy11
            // 
            this.cmbStrategy11.AutoSize = false;
            this.cmbStrategy11.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy11.Name = "cmbStrategy11";
            this.cmbStrategy11.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak11
            // 
            this.cmbPeak11.AutoSize = false;
            this.cmbPeak11.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak11.Name = "cmbPeak11";
            this.cmbPeak11.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem15.Text = "     hour - 17";
            // 
            // cmbStrategy17
            // 
            this.cmbStrategy17.AutoSize = false;
            this.cmbStrategy17.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy17.Name = "cmbStrategy17";
            this.cmbStrategy17.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak17
            // 
            this.cmbPeak17.AutoSize = false;
            this.cmbPeak17.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak17.Name = "cmbPeak17";
            this.cmbPeak17.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem16.Text = "     hour - 23";
            // 
            // cmbStrategy23
            // 
            this.cmbStrategy23.AutoSize = false;
            this.cmbStrategy23.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy23.Name = "cmbStrategy23";
            this.cmbStrategy23.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak23
            // 
            this.cmbPeak23.AutoSize = false;
            this.cmbPeak23.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak23.Name = "cmbPeak23";
            this.cmbPeak23.Size = new System.Drawing.Size(38, 23);
            // 
            // menuStrip8
            // 
            this.menuStrip8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip8.AutoSize = false;
            this.menuStrip8.BackColor = System.Drawing.Color.SkyBlue;
            this.menuStrip8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.menuStrip8.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip8.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.menuStrip8.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem21,
            this.toolStripMenuItem22,
            this.toolStripMenuItem23,
            this.toolStripMenuItem24,
            this.strategyToolStripMenuItem,
            this.peakToolStripMenuItem,
            this.strategyToolStripMenuItem1,
            this.strategyToolStripMenuItem2,
            this.peakToolStripMenuItem1,
            this.hourToolStripMenuItem,
            this.strategyToolStripMenuItem3,
            this.peakToolStripMenuItem2});
            this.menuStrip8.Location = new System.Drawing.Point(6, 10);
            this.menuStrip8.Margin = new System.Windows.Forms.Padding(1);
            this.menuStrip8.Name = "menuStrip8";
            this.menuStrip8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip8.Size = new System.Drawing.Size(730, 24);
            this.menuStrip8.TabIndex = 5;
            this.menuStrip8.Text = "menuStrip8";
            this.menuStrip8.Visible = false;
            // 
            // toolStripMenuItem21
            // 
            this.toolStripMenuItem21.Name = "toolStripMenuItem21";
            this.toolStripMenuItem21.Size = new System.Drawing.Size(56, 20);
            this.toolStripMenuItem21.Text = "hour     ";
            // 
            // toolStripMenuItem22
            // 
            this.toolStripMenuItem22.Name = "toolStripMenuItem22";
            this.toolStripMenuItem22.Size = new System.Drawing.Size(70, 20);
            this.toolStripMenuItem22.Text = " Strategy  ";
            // 
            // toolStripMenuItem23
            // 
            this.toolStripMenuItem23.Name = "toolStripMenuItem23";
            this.toolStripMenuItem23.Size = new System.Drawing.Size(45, 20);
            this.toolStripMenuItem23.Text = "Peak ";
            // 
            // toolStripMenuItem24
            // 
            this.toolStripMenuItem24.Name = "toolStripMenuItem24";
            this.toolStripMenuItem24.Size = new System.Drawing.Size(56, 20);
            this.toolStripMenuItem24.Text = "  hour   ";
            // 
            // strategyToolStripMenuItem
            // 
            this.strategyToolStripMenuItem.Name = "strategyToolStripMenuItem";
            this.strategyToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.strategyToolStripMenuItem.Text = "     Strategy ";
            // 
            // peakToolStripMenuItem
            // 
            this.peakToolStripMenuItem.Name = "peakToolStripMenuItem";
            this.peakToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.peakToolStripMenuItem.Text = " Peak   ";
            // 
            // strategyToolStripMenuItem1
            // 
            this.strategyToolStripMenuItem1.Name = "strategyToolStripMenuItem1";
            this.strategyToolStripMenuItem1.Size = new System.Drawing.Size(62, 20);
            this.strategyToolStripMenuItem1.Text = " hour      ";
            // 
            // strategyToolStripMenuItem2
            // 
            this.strategyToolStripMenuItem2.Name = "strategyToolStripMenuItem2";
            this.strategyToolStripMenuItem2.Size = new System.Drawing.Size(67, 20);
            this.strategyToolStripMenuItem2.Text = "  Strategy";
            // 
            // peakToolStripMenuItem1
            // 
            this.peakToolStripMenuItem1.Name = "peakToolStripMenuItem1";
            this.peakToolStripMenuItem1.Size = new System.Drawing.Size(51, 20);
            this.peakToolStripMenuItem1.Text = " Peak  ";
            // 
            // hourToolStripMenuItem
            // 
            this.hourToolStripMenuItem.Name = "hourToolStripMenuItem";
            this.hourToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.hourToolStripMenuItem.Text = "  hour     ";
            // 
            // strategyToolStripMenuItem3
            // 
            this.strategyToolStripMenuItem3.Name = "strategyToolStripMenuItem3";
            this.strategyToolStripMenuItem3.Size = new System.Drawing.Size(70, 20);
            this.strategyToolStripMenuItem3.Text = "  Strategy ";
            // 
            // peakToolStripMenuItem2
            // 
            this.peakToolStripMenuItem2.Name = "peakToolStripMenuItem2";
            this.peakToolStripMenuItem2.Size = new System.Drawing.Size(48, 20);
            this.peakToolStripMenuItem2.Text = " Peak ";
            // 
            // menuStrip5
            // 
            this.menuStrip5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip5.AutoSize = false;
            this.menuStrip5.BackColor = System.Drawing.Color.Beige;
            this.menuStrip5.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem9,
            this.cmbStrategy4,
            this.cmbPeak4,
            this.toolStripMenuItem10,
            this.cmbStrategy10,
            this.cmbPeak10,
            this.toolStripMenuItem11,
            this.cmbStrategy16,
            this.cmbPeak16,
            this.toolStripMenuItem12,
            this.cmbStrategy22,
            this.cmbPeak22});
            this.menuStrip5.Location = new System.Drawing.Point(6, 87);
            this.menuStrip5.Name = "menuStrip5";
            this.menuStrip5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip5.Size = new System.Drawing.Size(730, 24);
            this.menuStrip5.TabIndex = 3;
            this.menuStrip5.Text = "menuStrip5";
            this.menuStrip5.Visible = false;
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem9.Text = "hour - 4";
            // 
            // cmbStrategy4
            // 
            this.cmbStrategy4.AutoSize = false;
            this.cmbStrategy4.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy4.Name = "cmbStrategy4";
            this.cmbStrategy4.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak4
            // 
            this.cmbPeak4.AutoSize = false;
            this.cmbPeak4.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak4.Name = "cmbPeak4";
            this.cmbPeak4.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(79, 20);
            this.toolStripMenuItem10.Text = "    hour - 10";
            // 
            // cmbStrategy10
            // 
            this.cmbStrategy10.AutoSize = false;
            this.cmbStrategy10.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy10.Name = "cmbStrategy10";
            this.cmbStrategy10.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak10
            // 
            this.cmbPeak10.AutoSize = false;
            this.cmbPeak10.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak10.Name = "cmbPeak10";
            this.cmbPeak10.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem11.Text = "     hour - 16";
            // 
            // cmbStrategy16
            // 
            this.cmbStrategy16.AutoSize = false;
            this.cmbStrategy16.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy16.Name = "cmbStrategy16";
            this.cmbStrategy16.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak16
            // 
            this.cmbPeak16.AutoSize = false;
            this.cmbPeak16.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak16.Name = "cmbPeak16";
            this.cmbPeak16.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem12.Text = "     hour - 22";
            // 
            // cmbStrategy22
            // 
            this.cmbStrategy22.AutoSize = false;
            this.cmbStrategy22.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy22.Name = "cmbStrategy22";
            this.cmbStrategy22.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak22
            // 
            this.cmbPeak22.AutoSize = false;
            this.cmbPeak22.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak22.Name = "cmbPeak22";
            this.cmbPeak22.Size = new System.Drawing.Size(38, 23);
            // 
            // menuStrip3
            // 
            this.menuStrip3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip3.AutoSize = false;
            this.menuStrip3.BackColor = System.Drawing.Color.Beige;
            this.menuStrip3.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.cmbStrategy2,
            this.cmbPeak2,
            this.toolStripMenuItem6,
            this.cmbStrategy8,
            this.cmbPeak8,
            this.toolStripMenuItem7,
            this.cmbStrategy14,
            this.cmbPeak14,
            this.toolStripMenuItem8,
            this.cmbStrategy20,
            this.cmbPeak20});
            this.menuStrip3.Location = new System.Drawing.Point(6, 45);
            this.menuStrip3.Name = "menuStrip3";
            this.menuStrip3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip3.Size = new System.Drawing.Size(730, 24);
            this.menuStrip3.TabIndex = 1;
            this.menuStrip3.Text = "menuStrip3";
            this.menuStrip3.Visible = false;
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem5.Text = "hour - 2";
            // 
            // cmbStrategy2
            // 
            this.cmbStrategy2.AutoSize = false;
            this.cmbStrategy2.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy2.Name = "cmbStrategy2";
            this.cmbStrategy2.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak2
            // 
            this.cmbPeak2.AutoSize = false;
            this.cmbPeak2.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak2.Name = "cmbPeak2";
            this.cmbPeak2.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(79, 20);
            this.toolStripMenuItem6.Text = "    hour - 8  ";
            // 
            // cmbStrategy8
            // 
            this.cmbStrategy8.AutoSize = false;
            this.cmbStrategy8.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy8.Name = "cmbStrategy8";
            this.cmbStrategy8.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak8
            // 
            this.cmbPeak8.AutoSize = false;
            this.cmbPeak8.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak8.Name = "cmbPeak8";
            this.cmbPeak8.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem7.Text = "     hour - 14";
            // 
            // cmbStrategy14
            // 
            this.cmbStrategy14.AutoSize = false;
            this.cmbStrategy14.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy14.Name = "cmbStrategy14";
            this.cmbStrategy14.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak14
            // 
            this.cmbPeak14.AutoSize = false;
            this.cmbPeak14.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak14.Name = "cmbPeak14";
            this.cmbPeak14.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem8.Text = "     hour - 20";
            // 
            // cmbStrategy20
            // 
            this.cmbStrategy20.AutoSize = false;
            this.cmbStrategy20.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy20.Name = "cmbStrategy20";
            this.cmbStrategy20.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak20
            // 
            this.cmbPeak20.AutoSize = false;
            this.cmbPeak20.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak20.Name = "cmbPeak20";
            this.cmbPeak20.Size = new System.Drawing.Size(38, 23);
            // 
            // menuStrip4
            // 
            this.menuStrip4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip4.AutoSize = false;
            this.menuStrip4.BackColor = System.Drawing.Color.Beige;
            this.menuStrip4.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem25,
            this.cmbStrategy3,
            this.cmbPeak3,
            this.toolStripMenuItem26,
            this.cmbStrategy9,
            this.cmbPeak9,
            this.toolStripMenuItem27,
            this.cmbStrategy15,
            this.cmbPeak15,
            this.toolStripMenuItem28,
            this.cmbStrategy21,
            this.cmbPeak21});
            this.menuStrip4.Location = new System.Drawing.Point(6, 67);
            this.menuStrip4.Name = "menuStrip4";
            this.menuStrip4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip4.Size = new System.Drawing.Size(730, 24);
            this.menuStrip4.TabIndex = 2;
            this.menuStrip4.Text = "menuStrip4";
            this.menuStrip4.Visible = false;
            // 
            // toolStripMenuItem25
            // 
            this.toolStripMenuItem25.Name = "toolStripMenuItem25";
            this.toolStripMenuItem25.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem25.Text = "hour - 3";
            // 
            // cmbStrategy3
            // 
            this.cmbStrategy3.AutoSize = false;
            this.cmbStrategy3.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy3.Name = "cmbStrategy3";
            this.cmbStrategy3.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak3
            // 
            this.cmbPeak3.AutoSize = false;
            this.cmbPeak3.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak3.Name = "cmbPeak3";
            this.cmbPeak3.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem26
            // 
            this.toolStripMenuItem26.Name = "toolStripMenuItem26";
            this.toolStripMenuItem26.Size = new System.Drawing.Size(79, 20);
            this.toolStripMenuItem26.Text = "    hour - 9  ";
            // 
            // cmbStrategy9
            // 
            this.cmbStrategy9.AutoSize = false;
            this.cmbStrategy9.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy9.Name = "cmbStrategy9";
            this.cmbStrategy9.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak9
            // 
            this.cmbPeak9.AutoSize = false;
            this.cmbPeak9.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak9.Name = "cmbPeak9";
            this.cmbPeak9.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem27
            // 
            this.toolStripMenuItem27.Name = "toolStripMenuItem27";
            this.toolStripMenuItem27.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem27.Text = "     hour - 15";
            // 
            // cmbStrategy15
            // 
            this.cmbStrategy15.AutoSize = false;
            this.cmbStrategy15.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy15.Name = "cmbStrategy15";
            this.cmbStrategy15.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak15
            // 
            this.cmbPeak15.AutoSize = false;
            this.cmbPeak15.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak15.Name = "cmbPeak15";
            this.cmbPeak15.Size = new System.Drawing.Size(38, 23);
            // 
            // toolStripMenuItem28
            // 
            this.toolStripMenuItem28.Name = "toolStripMenuItem28";
            this.toolStripMenuItem28.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem28.Text = "     hour - 21";
            // 
            // cmbStrategy21
            // 
            this.cmbStrategy21.AutoSize = false;
            this.cmbStrategy21.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy21.Name = "cmbStrategy21";
            this.cmbStrategy21.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak21
            // 
            this.cmbPeak21.AutoSize = false;
            this.cmbPeak21.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak21.Name = "cmbPeak21";
            this.cmbPeak21.Size = new System.Drawing.Size(38, 23);
            // 
            // menuStrip2
            // 
            this.menuStrip2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.menuStrip2.AutoSize = false;
            this.menuStrip2.BackColor = System.Drawing.Color.Beige;
            this.menuStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hour1ToolStripMenuItem,
            this.cmbStrategy1,
            this.cmbPeak1,
            this.hour2ToolStripMenuItem,
            this.cmbStrategy7,
            this.cmbPeak7,
            this.hour3ToolStripMenuItem,
            this.cmbStrategy13,
            this.cmbPeak13,
            this.hour4ToolStripMenuItem,
            this.cmbStrategy19,
            this.cmbPeak19});
            this.menuStrip2.Location = new System.Drawing.Point(6, 23);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip2.Size = new System.Drawing.Size(730, 24);
            this.menuStrip2.TabIndex = 0;
            this.menuStrip2.Text = "menuStrip2";
            this.menuStrip2.Visible = false;
            // 
            // hour1ToolStripMenuItem
            // 
            this.hour1ToolStripMenuItem.Name = "hour1ToolStripMenuItem";
            this.hour1ToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.hour1ToolStripMenuItem.Text = "hour - 1";
            // 
            // cmbStrategy1
            // 
            this.cmbStrategy1.AutoSize = false;
            this.cmbStrategy1.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy1.Name = "cmbStrategy1";
            this.cmbStrategy1.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak1
            // 
            this.cmbPeak1.AutoSize = false;
            this.cmbPeak1.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak1.Name = "cmbPeak1";
            this.cmbPeak1.Size = new System.Drawing.Size(38, 23);
            // 
            // hour2ToolStripMenuItem
            // 
            this.hour2ToolStripMenuItem.Name = "hour2ToolStripMenuItem";
            this.hour2ToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.hour2ToolStripMenuItem.Text = "    hour - 7  ";
            // 
            // cmbStrategy7
            // 
            this.cmbStrategy7.AutoSize = false;
            this.cmbStrategy7.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy7.Name = "cmbStrategy7";
            this.cmbStrategy7.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak7
            // 
            this.cmbPeak7.AutoSize = false;
            this.cmbPeak7.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak7.Name = "cmbPeak7";
            this.cmbPeak7.Size = new System.Drawing.Size(38, 23);
            // 
            // hour3ToolStripMenuItem
            // 
            this.hour3ToolStripMenuItem.Name = "hour3ToolStripMenuItem";
            this.hour3ToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.hour3ToolStripMenuItem.Text = "     hour - 13";
            // 
            // cmbStrategy13
            // 
            this.cmbStrategy13.AutoSize = false;
            this.cmbStrategy13.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy13.Name = "cmbStrategy13";
            this.cmbStrategy13.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak13
            // 
            this.cmbPeak13.AutoSize = false;
            this.cmbPeak13.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak13.Name = "cmbPeak13";
            this.cmbPeak13.Size = new System.Drawing.Size(38, 23);
            // 
            // hour4ToolStripMenuItem
            // 
            this.hour4ToolStripMenuItem.Name = "hour4ToolStripMenuItem";
            this.hour4ToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.hour4ToolStripMenuItem.Text = "     hour - 19";
            // 
            // cmbStrategy19
            // 
            this.cmbStrategy19.AutoSize = false;
            this.cmbStrategy19.Items.AddRange(new object[] {
            "Low",
            "Medium",
            "High"});
            this.cmbStrategy19.Name = "cmbStrategy19";
            this.cmbStrategy19.Size = new System.Drawing.Size(65, 23);
            // 
            // cmbPeak19
            // 
            this.cmbPeak19.AutoSize = false;
            this.cmbPeak19.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbPeak19.Name = "cmbPeak19";
            this.cmbPeak19.Size = new System.Drawing.Size(38, 23);
            // 
            // miniToolStrip
            // 
            this.miniToolStrip.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.miniToolStrip.AutoSize = false;
            this.miniToolStrip.BackColor = System.Drawing.Color.Beige;
            this.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.miniToolStrip.Location = new System.Drawing.Point(0, 0);
            this.miniToolStrip.Name = "miniToolStrip";
            this.miniToolStrip.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.miniToolStrip.Size = new System.Drawing.Size(730, 24);
            this.miniToolStrip.TabIndex = 0;
            this.miniToolStrip.Visible = false;
            // 
            // FinancialReport
            // 
            this.FinancialReport.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.FinancialReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FinancialReport.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FinancialReport.Controls.Add(this.FRMainPanel);
            this.FinancialReport.Controls.Add(this.FRHeaderGb);
            this.FinancialReport.Location = new System.Drawing.Point(4, 29);
            this.FinancialReport.Name = "FinancialReport";
            this.FinancialReport.Size = new System.Drawing.Size(757, 625);
            this.FinancialReport.TabIndex = 3;
            this.FinancialReport.Text = "Financial Report";
            this.FinancialReport.UseVisualStyleBackColor = true;
            // 
            // FRMainPanel
            // 
            this.FRMainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRMainPanel.Controls.Add(this.btnfinanceexcel);
            this.FRMainPanel.Controls.Add(this.btnfrprint);
            this.FRMainPanel.Controls.Add(this.FRRunAuto);
            this.FRMainPanel.Controls.Add(this.FRRun);
            this.FRMainPanel.Controls.Add(this.FRPlot);
            this.FRMainPanel.Controls.Add(this.FRUpdateBtn);
            this.FRMainPanel.Controls.Add(this.FROKBtn);
            this.FRMainPanel.Controls.Add(this.FRUnitPanel);
            this.FRMainPanel.Controls.Add(this.FRPlantPanel);
            this.FRMainPanel.Location = new System.Drawing.Point(3, 65);
            this.FRMainPanel.Name = "FRMainPanel";
            this.FRMainPanel.Size = new System.Drawing.Size(741, 555);
            this.FRMainPanel.TabIndex = 23;
            // 
            // btnfinanceexcel
            // 
            this.btnfinanceexcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnfinanceexcel.BackColor = System.Drawing.Color.CadetBlue;
            this.btnfinanceexcel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnfinanceexcel.Location = new System.Drawing.Point(22, 528);
            this.btnfinanceexcel.Name = "btnfinanceexcel";
            this.btnfinanceexcel.Size = new System.Drawing.Size(95, 23);
            this.btnfinanceexcel.TabIndex = 44;
            this.btnfinanceexcel.Text = "Excel";
            this.btnfinanceexcel.UseVisualStyleBackColor = false;
            this.btnfinanceexcel.Visible = false;
            this.btnfinanceexcel.Click += new System.EventHandler(this.btnfinanceexcel_Click);
            // 
            // btnfrprint
            // 
            this.btnfrprint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnfrprint.BackColor = System.Drawing.Color.CadetBlue;
            this.btnfrprint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnfrprint.Location = new System.Drawing.Point(345, 528);
            this.btnfrprint.Name = "btnfrprint";
            this.btnfrprint.Size = new System.Drawing.Size(95, 23);
            this.btnfrprint.TabIndex = 43;
            this.btnfrprint.Text = "Print";
            this.btnfrprint.UseVisualStyleBackColor = false;
            this.btnfrprint.Visible = false;
            this.btnfrprint.Click += new System.EventHandler(this.btnfrprint_Click);
            // 
            // FRRunAuto
            // 
            this.FRRunAuto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FRRunAuto.BackColor = System.Drawing.Color.CadetBlue;
            this.FRRunAuto.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.FRRunAuto.Location = new System.Drawing.Point(130, 528);
            this.FRRunAuto.Name = "FRRunAuto";
            this.FRRunAuto.Size = new System.Drawing.Size(95, 23);
            this.FRRunAuto.TabIndex = 42;
            this.FRRunAuto.Text = "Automatic Run";
            this.FRRunAuto.UseVisualStyleBackColor = false;
            this.FRRunAuto.Visible = false;
            this.FRRunAuto.Click += new System.EventHandler(this.FRRunAuto_Click);
            // 
            // FRRun
            // 
            this.FRRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FRRun.BackColor = System.Drawing.Color.CadetBlue;
            this.FRRun.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.FRRun.Location = new System.Drawing.Point(233, 529);
            this.FRRun.Name = "FRRun";
            this.FRRun.Size = new System.Drawing.Size(95, 23);
            this.FRRun.TabIndex = 40;
            this.FRRun.Text = " Manual Run";
            this.FRRun.UseVisualStyleBackColor = false;
            this.FRRun.Visible = false;
            this.FRRun.Click += new System.EventHandler(this.FRRun_Click);
            // 
            // FRPlot
            // 
            this.FRPlot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FRPlot.BackColor = System.Drawing.Color.CadetBlue;
            this.FRPlot.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.FRPlot.Location = new System.Drawing.Point(454, 528);
            this.FRPlot.Name = "FRPlot";
            this.FRPlot.Size = new System.Drawing.Size(95, 23);
            this.FRPlot.TabIndex = 39;
            this.FRPlot.Text = "Plot";
            this.FRPlot.UseVisualStyleBackColor = false;
            this.FRPlot.Visible = false;
            this.FRPlot.Click += new System.EventHandler(this.FRPlot_Click);
            // 
            // FRUpdateBtn
            // 
            this.FRUpdateBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FRUpdateBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.FRUpdateBtn.Enabled = false;
            this.FRUpdateBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.FRUpdateBtn.Location = new System.Drawing.Point(555, 528);
            this.FRUpdateBtn.Name = "FRUpdateBtn";
            this.FRUpdateBtn.Size = new System.Drawing.Size(75, 23);
            this.FRUpdateBtn.TabIndex = 36;
            this.FRUpdateBtn.Text = "Save";
            this.FRUpdateBtn.UseVisualStyleBackColor = false;
            this.FRUpdateBtn.Visible = false;
            this.FRUpdateBtn.Click += new System.EventHandler(this.FRUpdateBtn_Click);
            // 
            // FROKBtn
            // 
            this.FROKBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FROKBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.FROKBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.FROKBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.FROKBtn.Location = new System.Drawing.Point(645, 528);
            this.FROKBtn.Name = "FROKBtn";
            this.FROKBtn.Size = new System.Drawing.Size(75, 23);
            this.FROKBtn.TabIndex = 35;
            this.FROKBtn.Text = "Edit Mode";
            this.FROKBtn.UseVisualStyleBackColor = false;
            this.FROKBtn.Visible = false;
            this.FROKBtn.Click += new System.EventHandler(this.FROKBtn_Click);
            // 
            // FRUnitPanel
            // 
            this.FRUnitPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRUnitPanel.Controls.Add(this.groupBox2);
            this.FRUnitPanel.Controls.Add(this.groupBox10);
            this.FRUnitPanel.Controls.Add(this.groupBox9);
            this.FRUnitPanel.Location = new System.Drawing.Point(19, 3);
            this.FRUnitPanel.Name = "FRUnitPanel";
            this.FRUnitPanel.Size = new System.Drawing.Size(703, 517);
            this.FRUnitPanel.TabIndex = 37;
            this.FRUnitPanel.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.panel19);
            this.groupBox2.Controls.Add(this.label88);
            this.groupBox2.Controls.Add(this.FRUnitFuelCostTb);
            this.groupBox2.Controls.Add(this.FRUnitFuelNoCostTb);
            this.groupBox2.Controls.Add(this.label87);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox2.Location = new System.Drawing.Point(6, 260);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(694, 134);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "FUEL COST";
            // 
            // panel19
            // 
            this.panel19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel19.BackColor = System.Drawing.Color.Beige;
            this.panel19.Controls.Add(this.FRUnitSecondaryFuelRb);
            this.panel19.Controls.Add(this.FRUnitPrimaryFuelRb);
            this.panel19.Controls.Add(this.panel18);
            this.panel19.Controls.Add(this.panel11);
            this.panel19.Location = new System.Drawing.Point(31, 15);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(250, 115);
            this.panel19.TabIndex = 15;
            // 
            // FRUnitSecondaryFuelRb
            // 
            this.FRUnitSecondaryFuelRb.AutoSize = true;
            this.FRUnitSecondaryFuelRb.Location = new System.Drawing.Point(138, 52);
            this.FRUnitSecondaryFuelRb.Name = "FRUnitSecondaryFuelRb";
            this.FRUnitSecondaryFuelRb.Size = new System.Drawing.Size(109, 17);
            this.FRUnitSecondaryFuelRb.TabIndex = 10;
            this.FRUnitSecondaryFuelRb.Text = "SecondaryFuel";
            this.FRUnitSecondaryFuelRb.UseVisualStyleBackColor = true;
            // 
            // FRUnitPrimaryFuelRb
            // 
            this.FRUnitPrimaryFuelRb.AutoSize = true;
            this.FRUnitPrimaryFuelRb.Checked = true;
            this.FRUnitPrimaryFuelRb.Location = new System.Drawing.Point(138, 29);
            this.FRUnitPrimaryFuelRb.Name = "FRUnitPrimaryFuelRb";
            this.FRUnitPrimaryFuelRb.Size = new System.Drawing.Size(90, 17);
            this.FRUnitPrimaryFuelRb.TabIndex = 9;
            this.FRUnitPrimaryFuelRb.TabStop = true;
            this.FRUnitPrimaryFuelRb.Text = "PrimaryFuel";
            this.FRUnitPrimaryFuelRb.UseVisualStyleBackColor = true;
            this.FRUnitPrimaryFuelRb.CheckedChanged += new System.EventHandler(this.FRUnitPrimaryFuelRb_CheckedChanged);
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.CadetBlue;
            this.panel18.Controls.Add(this.FRUnitPActiveTb);
            this.panel18.Controls.Add(this.label86);
            this.panel18.Location = new System.Drawing.Point(21, 61);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(95, 49);
            this.panel18.TabIndex = 8;
            // 
            // FRUnitPActiveTb
            // 
            this.FRUnitPActiveTb.BackColor = System.Drawing.Color.White;
            this.FRUnitPActiveTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitPActiveTb.Name = "FRUnitPActiveTb";
            this.FRUnitPActiveTb.ReadOnly = true;
            this.FRUnitPActiveTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitPActiveTb.TabIndex = 1;
            this.FRUnitPActiveTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitPActiveTb.Validated += new System.EventHandler(this.FRUnitPActiveTb_Validated);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.ForeColor = System.Drawing.SystemColors.Window;
            this.label86.Location = new System.Drawing.Point(17, 4);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(55, 13);
            this.label86.TabIndex = 0;
            this.label86.Text = "P Active";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.CadetBlue;
            this.panel11.Controls.Add(this.FRUnitQReactiveTb);
            this.panel11.Controls.Add(this.label85);
            this.panel11.Location = new System.Drawing.Point(21, 4);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(95, 49);
            this.panel11.TabIndex = 7;
            // 
            // FRUnitQReactiveTb
            // 
            this.FRUnitQReactiveTb.BackColor = System.Drawing.Color.White;
            this.FRUnitQReactiveTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitQReactiveTb.Name = "FRUnitQReactiveTb";
            this.FRUnitQReactiveTb.ReadOnly = true;
            this.FRUnitQReactiveTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitQReactiveTb.TabIndex = 1;
            this.FRUnitQReactiveTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitQReactiveTb.Validated += new System.EventHandler(this.FRUnitQReactiveTb_Validated);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.ForeColor = System.Drawing.SystemColors.Window;
            this.label85.Location = new System.Drawing.Point(10, 4);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(71, 13);
            this.label85.TabIndex = 0;
            this.label85.Text = "Q Reactive";
            // 
            // label88
            // 
            this.label88.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(377, 92);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(153, 13);
            this.label88.TabIndex = 14;
            this.label88.Text = "Fuel Cost with Subsidies :";
            // 
            // FRUnitFuelCostTb
            // 
            this.FRUnitFuelCostTb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRUnitFuelCostTb.BackColor = System.Drawing.Color.White;
            this.FRUnitFuelCostTb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FRUnitFuelCostTb.Location = new System.Drawing.Point(532, 89);
            this.FRUnitFuelCostTb.Name = "FRUnitFuelCostTb";
            this.FRUnitFuelCostTb.ReadOnly = true;
            this.FRUnitFuelCostTb.Size = new System.Drawing.Size(124, 20);
            this.FRUnitFuelCostTb.TabIndex = 1;
            this.FRUnitFuelCostTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitFuelCostTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRUnitFuelCostTb_MouseClick);
            this.FRUnitFuelCostTb.Leave += new System.EventHandler(this.FRUnitFuelCostTb_Leave);
            // 
            // FRUnitFuelNoCostTb
            // 
            this.FRUnitFuelNoCostTb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FRUnitFuelNoCostTb.BackColor = System.Drawing.Color.White;
            this.FRUnitFuelNoCostTb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FRUnitFuelNoCostTb.Location = new System.Drawing.Point(532, 39);
            this.FRUnitFuelNoCostTb.Name = "FRUnitFuelNoCostTb";
            this.FRUnitFuelNoCostTb.ReadOnly = true;
            this.FRUnitFuelNoCostTb.Size = new System.Drawing.Size(124, 20);
            this.FRUnitFuelNoCostTb.TabIndex = 1;
            this.FRUnitFuelNoCostTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitFuelNoCostTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRUnitFuelNoCostTb_MouseClick);
            this.FRUnitFuelNoCostTb.Leave += new System.EventHandler(this.FRUnitFuelNoCostTb_Leave);
            // 
            // label87
            // 
            this.label87.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(361, 41);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(173, 13);
            this.label87.TabIndex = 13;
            this.label87.Text = "Fuel Cost with No Subsidies :";
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox10.Controls.Add(this.FRUnitCal);
            this.groupBox10.Controls.Add(this.groupBox16);
            this.groupBox10.Controls.Add(this.label73);
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox10.Location = new System.Drawing.Point(6, 399);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(694, 52);
            this.groupBox10.TabIndex = 1;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "REVENUE";
            this.groupBox10.Visible = false;
            // 
            // FRUnitCal
            // 
            this.FRUnitCal.HasButtons = true;
            this.FRUnitCal.Location = new System.Drawing.Point(53, 15);
            this.FRUnitCal.Name = "FRUnitCal";
            this.FRUnitCal.Readonly = true;
            this.FRUnitCal.Size = new System.Drawing.Size(120, 20);
            this.FRUnitCal.TabIndex = 33;
            this.FRUnitCal.ValueChanged += new System.EventHandler(this.FRUnitCal_ValueChanged);
            // 
            // groupBox16
            // 
            this.groupBox16.BackColor = System.Drawing.Color.Beige;
            this.groupBox16.Controls.Add(this.panel76);
            this.groupBox16.Controls.Add(this.panel69);
            this.groupBox16.Controls.Add(this.panel70);
            this.groupBox16.Controls.Add(this.panel71);
            this.groupBox16.Controls.Add(this.panel73);
            this.groupBox16.Controls.Add(this.panel74);
            this.groupBox16.Location = new System.Drawing.Point(-10, 38);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(695, 79);
            this.groupBox16.TabIndex = 5;
            this.groupBox16.TabStop = false;
            // 
            // panel76
            // 
            this.panel76.BackColor = System.Drawing.Color.CadetBlue;
            this.panel76.Controls.Add(this.FRUnitIncomeTb);
            this.panel76.Controls.Add(this.label81);
            this.panel76.Location = new System.Drawing.Point(360, 19);
            this.panel76.Name = "panel76";
            this.panel76.Size = new System.Drawing.Size(95, 49);
            this.panel76.TabIndex = 1;
            // 
            // FRUnitIncomeTb
            // 
            this.FRUnitIncomeTb.BackColor = System.Drawing.Color.White;
            this.FRUnitIncomeTb.Location = new System.Drawing.Point(4, 22);
            this.FRUnitIncomeTb.Name = "FRUnitIncomeTb";
            this.FRUnitIncomeTb.ReadOnly = true;
            this.FRUnitIncomeTb.Size = new System.Drawing.Size(88, 20);
            this.FRUnitIncomeTb.TabIndex = 1;
            this.FRUnitIncomeTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitIncomeTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRUnitIncomeTb_MouseClick);
            this.FRUnitIncomeTb.Leave += new System.EventHandler(this.FRUnitIncomeTb_Leave);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.ForeColor = System.Drawing.SystemColors.Window;
            this.label81.Location = new System.Drawing.Point(19, 4);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(55, 13);
            this.label81.TabIndex = 0;
            this.label81.Text = "INCOME";
            // 
            // panel69
            // 
            this.panel69.BackColor = System.Drawing.Color.CadetBlue;
            this.panel69.Controls.Add(this.FRUnitEneryPayTb);
            this.panel69.Controls.Add(this.label74);
            this.panel69.Location = new System.Drawing.Point(580, 19);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(105, 49);
            this.panel69.TabIndex = 4;
            // 
            // FRUnitEneryPayTb
            // 
            this.FRUnitEneryPayTb.BackColor = System.Drawing.Color.White;
            this.FRUnitEneryPayTb.Location = new System.Drawing.Point(6, 21);
            this.FRUnitEneryPayTb.Name = "FRUnitEneryPayTb";
            this.FRUnitEneryPayTb.ReadOnly = true;
            this.FRUnitEneryPayTb.Size = new System.Drawing.Size(96, 20);
            this.FRUnitEneryPayTb.TabIndex = 1;
            this.FRUnitEneryPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitEneryPayTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRUnitEneryPayTb_MouseClick);
            this.FRUnitEneryPayTb.Leave += new System.EventHandler(this.FRUnitEneryPayTb_Leave);
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.SystemColors.Window;
            this.label74.Location = new System.Drawing.Point(3, 4);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(98, 13);
            this.label74.TabIndex = 0;
            this.label74.Text = "Energy Payment";
            // 
            // panel70
            // 
            this.panel70.BackColor = System.Drawing.Color.CadetBlue;
            this.panel70.Controls.Add(this.FRUnitCapPayTb);
            this.panel70.Controls.Add(this.label75);
            this.panel70.Location = new System.Drawing.Point(464, 19);
            this.panel70.Name = "panel70";
            this.panel70.Size = new System.Drawing.Size(106, 49);
            this.panel70.TabIndex = 5;
            // 
            // FRUnitCapPayTb
            // 
            this.FRUnitCapPayTb.BackColor = System.Drawing.Color.White;
            this.FRUnitCapPayTb.Location = new System.Drawing.Point(5, 22);
            this.FRUnitCapPayTb.Name = "FRUnitCapPayTb";
            this.FRUnitCapPayTb.ReadOnly = true;
            this.FRUnitCapPayTb.Size = new System.Drawing.Size(98, 20);
            this.FRUnitCapPayTb.TabIndex = 1;
            this.FRUnitCapPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitCapPayTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRUnitCapPayTb_MouseClick);
            this.FRUnitCapPayTb.Leave += new System.EventHandler(this.FRUnitCapPayTb_Leave);
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.SystemColors.Window;
            this.label75.Location = new System.Drawing.Point(0, 4);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(108, 13);
            this.label75.TabIndex = 0;
            this.label75.Text = "Capacity Payment";
            // 
            // panel71
            // 
            this.panel71.BackColor = System.Drawing.Color.CadetBlue;
            this.panel71.Controls.Add(this.FRUnitULPowerTb);
            this.panel71.Controls.Add(this.label76);
            this.panel71.Location = new System.Drawing.Point(254, 19);
            this.panel71.Name = "panel71";
            this.panel71.Size = new System.Drawing.Size(98, 49);
            this.panel71.TabIndex = 4;
            // 
            // FRUnitULPowerTb
            // 
            this.FRUnitULPowerTb.BackColor = System.Drawing.Color.White;
            this.FRUnitULPowerTb.Location = new System.Drawing.Point(4, 22);
            this.FRUnitULPowerTb.Name = "FRUnitULPowerTb";
            this.FRUnitULPowerTb.ReadOnly = true;
            this.FRUnitULPowerTb.Size = new System.Drawing.Size(89, 20);
            this.FRUnitULPowerTb.TabIndex = 1;
            this.FRUnitULPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitULPowerTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRUnitULPowerTb_MouseClick);
            this.FRUnitULPowerTb.Leave += new System.EventHandler(this.FRUnitULPowerTb_Leave);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.SystemColors.Window;
            this.label76.Location = new System.Drawing.Point(16, 4);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(62, 13);
            this.label76.TabIndex = 0;
            this.label76.Text = "UL Power";
            // 
            // panel73
            // 
            this.panel73.BackColor = System.Drawing.Color.CadetBlue;
            this.panel73.Controls.Add(this.FRUnitTotalPowerTb);
            this.panel73.Controls.Add(this.label78);
            this.panel73.Location = new System.Drawing.Point(139, 19);
            this.panel73.Name = "panel73";
            this.panel73.Size = new System.Drawing.Size(105, 49);
            this.panel73.TabIndex = 2;
            // 
            // FRUnitTotalPowerTb
            // 
            this.FRUnitTotalPowerTb.BackColor = System.Drawing.Color.White;
            this.FRUnitTotalPowerTb.Location = new System.Drawing.Point(7, 22);
            this.FRUnitTotalPowerTb.Name = "FRUnitTotalPowerTb";
            this.FRUnitTotalPowerTb.ReadOnly = true;
            this.FRUnitTotalPowerTb.Size = new System.Drawing.Size(93, 20);
            this.FRUnitTotalPowerTb.TabIndex = 1;
            this.FRUnitTotalPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitTotalPowerTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRUnitTotalPowerTb_MouseClick);
            this.FRUnitTotalPowerTb.Leave += new System.EventHandler(this.FRUnitTotalPowerTb_Leave);
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.SystemColors.Window;
            this.label78.Location = new System.Drawing.Point(15, 4);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(75, 13);
            this.label78.TabIndex = 0;
            this.label78.Text = "Total Power";
            // 
            // panel74
            // 
            this.panel74.BackColor = System.Drawing.Color.CadetBlue;
            this.panel74.Controls.Add(this.FRUnitCapacityTb);
            this.panel74.Controls.Add(this.label79);
            this.panel74.Location = new System.Drawing.Point(16, 19);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(114, 49);
            this.panel74.TabIndex = 1;
            // 
            // FRUnitCapacityTb
            // 
            this.FRUnitCapacityTb.BackColor = System.Drawing.Color.White;
            this.FRUnitCapacityTb.Location = new System.Drawing.Point(10, 22);
            this.FRUnitCapacityTb.Name = "FRUnitCapacityTb";
            this.FRUnitCapacityTb.ReadOnly = true;
            this.FRUnitCapacityTb.Size = new System.Drawing.Size(90, 20);
            this.FRUnitCapacityTb.TabIndex = 1;
            this.FRUnitCapacityTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitCapacityTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRUnitCapacityTb_MouseClick);
            this.FRUnitCapacityTb.Leave += new System.EventHandler(this.FRUnitCapacityTb_Leave);
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.SystemColors.Window;
            this.label79.Location = new System.Drawing.Point(-1, 4);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(112, 13);
            this.label79.TabIndex = 0;
            this.label79.Text = "Available Capacity";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(8, 18);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(42, 13);
            this.label73.TabIndex = 2;
            this.label73.Text = "Date :";
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.groupBox15);
            this.groupBox9.Controls.Add(this.groupBox14);
            this.groupBox9.Controls.Add(this.groupBox13);
            this.groupBox9.Controls.Add(this.groupBox12);
            this.groupBox9.Controls.Add(this.groupBox11);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox9.Location = new System.Drawing.Point(3, 14);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(697, 242);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "COST FUNCTION";
            // 
            // groupBox15
            // 
            this.groupBox15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox15.BackColor = System.Drawing.Color.Beige;
            this.groupBox15.Controls.Add(this.panel65);
            this.groupBox15.Controls.Add(this.panel68);
            this.groupBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox15.Location = new System.Drawing.Point(401, 164);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(252, 67);
            this.groupBox15.TabIndex = 28;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "START-SHUTDOWN";
            // 
            // panel65
            // 
            this.panel65.BackColor = System.Drawing.Color.CadetBlue;
            this.panel65.Controls.Add(this.FRUnitHotTb);
            this.panel65.Controls.Add(this.label69);
            this.panel65.Location = new System.Drawing.Point(138, 12);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(95, 49);
            this.panel65.TabIndex = 1;
            // 
            // FRUnitHotTb
            // 
            this.FRUnitHotTb.BackColor = System.Drawing.Color.White;
            this.FRUnitHotTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitHotTb.Name = "FRUnitHotTb";
            this.FRUnitHotTb.ReadOnly = true;
            this.FRUnitHotTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitHotTb.TabIndex = 0;
            this.FRUnitHotTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitHotTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRUnitHotTb_MouseClick);
            this.FRUnitHotTb.Leave += new System.EventHandler(this.FRUnitHotTb_Leave);
            this.FRUnitHotTb.Validated += new System.EventHandler(this.FRUnitHotTb_Validated);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.ForeColor = System.Drawing.SystemColors.Window;
            this.label69.Location = new System.Drawing.Point(26, 4);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(33, 13);
            this.label69.TabIndex = 0;
            this.label69.Text = "HOT";
            // 
            // panel68
            // 
            this.panel68.BackColor = System.Drawing.Color.CadetBlue;
            this.panel68.Controls.Add(this.FRUnitColdTb);
            this.panel68.Controls.Add(this.label72);
            this.panel68.Location = new System.Drawing.Point(18, 12);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(95, 49);
            this.panel68.TabIndex = 0;
            // 
            // FRUnitColdTb
            // 
            this.FRUnitColdTb.BackColor = System.Drawing.Color.White;
            this.FRUnitColdTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitColdTb.Name = "FRUnitColdTb";
            this.FRUnitColdTb.ReadOnly = true;
            this.FRUnitColdTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitColdTb.TabIndex = 0;
            this.FRUnitColdTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitColdTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRUnitColdTb_MouseClick);
            this.FRUnitColdTb.Leave += new System.EventHandler(this.FRUnitColdTb_Leave);
            this.FRUnitColdTb.Validated += new System.EventHandler(this.FRUnitColdTb_Validated);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.ForeColor = System.Drawing.SystemColors.Window;
            this.label72.Location = new System.Drawing.Point(25, 6);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(40, 13);
            this.label72.TabIndex = 0;
            this.label72.Text = "COLD";
            // 
            // groupBox14
            // 
            this.groupBox14.BackColor = System.Drawing.Color.Beige;
            this.groupBox14.Controls.Add(this.panel66);
            this.groupBox14.Controls.Add(this.panel67);
            this.groupBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox14.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox14.Location = new System.Drawing.Point(32, 164);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(252, 67);
            this.groupBox14.TabIndex = 27;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "MAINTENANCE";
            // 
            // panel66
            // 
            this.panel66.BackColor = System.Drawing.Color.CadetBlue;
            this.panel66.Controls.Add(this.FRUnitBmainTb);
            this.panel66.Controls.Add(this.label70);
            this.panel66.Location = new System.Drawing.Point(137, 13);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(95, 49);
            this.panel66.TabIndex = 1;
            // 
            // FRUnitBmainTb
            // 
            this.FRUnitBmainTb.BackColor = System.Drawing.Color.White;
            this.FRUnitBmainTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitBmainTb.Name = "FRUnitBmainTb";
            this.FRUnitBmainTb.ReadOnly = true;
            this.FRUnitBmainTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitBmainTb.TabIndex = 0;
            this.FRUnitBmainTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitBmainTb.Validated += new System.EventHandler(this.FRUnitBmainTb_Validated);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.ForeColor = System.Drawing.SystemColors.Window;
            this.label70.Location = new System.Drawing.Point(3, 4);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(87, 13);
            this.label70.TabIndex = 0;
            this.label70.Text = "Bmaintenance";
            // 
            // panel67
            // 
            this.panel67.BackColor = System.Drawing.Color.CadetBlue;
            this.panel67.Controls.Add(this.FRUnitAmainTb);
            this.panel67.Controls.Add(this.label71);
            this.panel67.Location = new System.Drawing.Point(17, 13);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(95, 49);
            this.panel67.TabIndex = 0;
            // 
            // FRUnitAmainTb
            // 
            this.FRUnitAmainTb.BackColor = System.Drawing.Color.White;
            this.FRUnitAmainTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitAmainTb.Name = "FRUnitAmainTb";
            this.FRUnitAmainTb.ReadOnly = true;
            this.FRUnitAmainTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitAmainTb.TabIndex = 0;
            this.FRUnitAmainTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitAmainTb.Validated += new System.EventHandler(this.FRUnitAmainTb_Validated);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.ForeColor = System.Drawing.SystemColors.Window;
            this.label71.Location = new System.Drawing.Point(6, 4);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(87, 13);
            this.label71.TabIndex = 0;
            this.label71.Text = "Amaintenance";
            // 
            // groupBox13
            // 
            this.groupBox13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox13.BackColor = System.Drawing.Color.Beige;
            this.groupBox13.Controls.Add(this.panel62);
            this.groupBox13.Controls.Add(this.panel63);
            this.groupBox13.Controls.Add(this.panel64);
            this.groupBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox13.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox13.Location = new System.Drawing.Point(355, 90);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(335, 70);
            this.groupBox13.TabIndex = 26;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "SECONDRY FUEL";
            // 
            // panel62
            // 
            this.panel62.BackColor = System.Drawing.Color.CadetBlue;
            this.panel62.Controls.Add(this.FRUnitCmargTb2);
            this.panel62.Controls.Add(this.label66);
            this.panel62.Location = new System.Drawing.Point(232, 14);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(95, 49);
            this.panel62.TabIndex = 2;
            // 
            // FRUnitCmargTb2
            // 
            this.FRUnitCmargTb2.BackColor = System.Drawing.Color.White;
            this.FRUnitCmargTb2.Location = new System.Drawing.Point(9, 22);
            this.FRUnitCmargTb2.Name = "FRUnitCmargTb2";
            this.FRUnitCmargTb2.ReadOnly = true;
            this.FRUnitCmargTb2.Size = new System.Drawing.Size(75, 20);
            this.FRUnitCmargTb2.TabIndex = 0;
            this.FRUnitCmargTb2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitCmargTb2.Validated += new System.EventHandler(this.FRUnitCmargTb2_Validated);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.ForeColor = System.Drawing.SystemColors.Window;
            this.label66.Location = new System.Drawing.Point(16, 4);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(62, 13);
            this.label66.TabIndex = 0;
            this.label66.Text = "Cmarginal";
            // 
            // panel63
            // 
            this.panel63.BackColor = System.Drawing.Color.CadetBlue;
            this.panel63.Controls.Add(this.FRUnitBmargTb2);
            this.panel63.Controls.Add(this.label67);
            this.panel63.Location = new System.Drawing.Point(118, 14);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(95, 49);
            this.panel63.TabIndex = 1;
            // 
            // FRUnitBmargTb2
            // 
            this.FRUnitBmargTb2.BackColor = System.Drawing.Color.White;
            this.FRUnitBmargTb2.Location = new System.Drawing.Point(9, 22);
            this.FRUnitBmargTb2.Name = "FRUnitBmargTb2";
            this.FRUnitBmargTb2.ReadOnly = true;
            this.FRUnitBmargTb2.Size = new System.Drawing.Size(75, 20);
            this.FRUnitBmargTb2.TabIndex = 0;
            this.FRUnitBmargTb2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitBmargTb2.Validated += new System.EventHandler(this.FRUnitBmargTb2_Validated);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.ForeColor = System.Drawing.SystemColors.Window;
            this.label67.Location = new System.Drawing.Point(15, 4);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(62, 13);
            this.label67.TabIndex = 0;
            this.label67.Text = "Bmarginal";
            // 
            // panel64
            // 
            this.panel64.BackColor = System.Drawing.Color.CadetBlue;
            this.panel64.Controls.Add(this.FRUnitAmargTb2);
            this.panel64.Controls.Add(this.label68);
            this.panel64.Location = new System.Drawing.Point(6, 14);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(95, 49);
            this.panel64.TabIndex = 0;
            // 
            // FRUnitAmargTb2
            // 
            this.FRUnitAmargTb2.BackColor = System.Drawing.Color.White;
            this.FRUnitAmargTb2.Location = new System.Drawing.Point(9, 22);
            this.FRUnitAmargTb2.Name = "FRUnitAmargTb2";
            this.FRUnitAmargTb2.ReadOnly = true;
            this.FRUnitAmargTb2.Size = new System.Drawing.Size(75, 20);
            this.FRUnitAmargTb2.TabIndex = 0;
            this.FRUnitAmargTb2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitAmargTb2.Validated += new System.EventHandler(this.FRUnitAmargTb2_Validated);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.ForeColor = System.Drawing.SystemColors.Window;
            this.label68.Location = new System.Drawing.Point(14, 4);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(62, 13);
            this.label68.TabIndex = 0;
            this.label68.Text = "Amarginal";
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.Beige;
            this.groupBox12.Controls.Add(this.panel59);
            this.groupBox12.Controls.Add(this.panel60);
            this.groupBox12.Controls.Add(this.panel61);
            this.groupBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox12.Location = new System.Drawing.Point(8, 90);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(335, 69);
            this.groupBox12.TabIndex = 25;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "PRIMARY FUEL";
            // 
            // panel59
            // 
            this.panel59.BackColor = System.Drawing.Color.CadetBlue;
            this.panel59.Controls.Add(this.FRUnitCmargTb1);
            this.panel59.Controls.Add(this.label63);
            this.panel59.Location = new System.Drawing.Point(232, 15);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(95, 49);
            this.panel59.TabIndex = 2;
            // 
            // FRUnitCmargTb1
            // 
            this.FRUnitCmargTb1.BackColor = System.Drawing.Color.White;
            this.FRUnitCmargTb1.Location = new System.Drawing.Point(9, 22);
            this.FRUnitCmargTb1.Name = "FRUnitCmargTb1";
            this.FRUnitCmargTb1.ReadOnly = true;
            this.FRUnitCmargTb1.Size = new System.Drawing.Size(75, 20);
            this.FRUnitCmargTb1.TabIndex = 0;
            this.FRUnitCmargTb1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitCmargTb1.Validated += new System.EventHandler(this.FRUnitCmargTb1_Validated);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.ForeColor = System.Drawing.SystemColors.Window;
            this.label63.Location = new System.Drawing.Point(15, 4);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(62, 13);
            this.label63.TabIndex = 0;
            this.label63.Text = "Cmarginal";
            // 
            // panel60
            // 
            this.panel60.BackColor = System.Drawing.Color.CadetBlue;
            this.panel60.Controls.Add(this.FRUnitBmargTb1);
            this.panel60.Controls.Add(this.label64);
            this.panel60.Location = new System.Drawing.Point(118, 15);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(95, 49);
            this.panel60.TabIndex = 1;
            // 
            // FRUnitBmargTb1
            // 
            this.FRUnitBmargTb1.BackColor = System.Drawing.Color.White;
            this.FRUnitBmargTb1.Location = new System.Drawing.Point(9, 22);
            this.FRUnitBmargTb1.Name = "FRUnitBmargTb1";
            this.FRUnitBmargTb1.ReadOnly = true;
            this.FRUnitBmargTb1.Size = new System.Drawing.Size(75, 20);
            this.FRUnitBmargTb1.TabIndex = 0;
            this.FRUnitBmargTb1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitBmargTb1.Validated += new System.EventHandler(this.FRUnitBmargTb1_Validated);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.ForeColor = System.Drawing.SystemColors.Window;
            this.label64.Location = new System.Drawing.Point(15, 4);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(62, 13);
            this.label64.TabIndex = 0;
            this.label64.Text = "Bmarginal";
            // 
            // panel61
            // 
            this.panel61.BackColor = System.Drawing.Color.CadetBlue;
            this.panel61.Controls.Add(this.FRUnitAmargTb1);
            this.panel61.Controls.Add(this.label65);
            this.panel61.Location = new System.Drawing.Point(6, 15);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(95, 49);
            this.panel61.TabIndex = 0;
            // 
            // FRUnitAmargTb1
            // 
            this.FRUnitAmargTb1.BackColor = System.Drawing.Color.White;
            this.FRUnitAmargTb1.Location = new System.Drawing.Point(9, 22);
            this.FRUnitAmargTb1.Name = "FRUnitAmargTb1";
            this.FRUnitAmargTb1.ReadOnly = true;
            this.FRUnitAmargTb1.Size = new System.Drawing.Size(75, 20);
            this.FRUnitAmargTb1.TabIndex = 0;
            this.FRUnitAmargTb1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitAmargTb1.Validated += new System.EventHandler(this.FRUnitAmargTb1_Validated);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.ForeColor = System.Drawing.SystemColors.Window;
            this.label65.Location = new System.Drawing.Point(15, 4);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(62, 13);
            this.label65.TabIndex = 0;
            this.label65.Text = "Amarginal";
            // 
            // groupBox11
            // 
            this.groupBox11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox11.BackColor = System.Drawing.Color.Beige;
            this.groupBox11.Controls.Add(this.panel53);
            this.groupBox11.Controls.Add(this.panel57);
            this.groupBox11.Controls.Add(this.panel58);
            this.groupBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox11.Location = new System.Drawing.Point(181, 13);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(335, 70);
            this.groupBox11.TabIndex = 24;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "COST";
            // 
            // panel53
            // 
            this.panel53.BackColor = System.Drawing.Color.CadetBlue;
            this.panel53.Controls.Add(this.FRUnitVariableTb);
            this.panel53.Controls.Add(this.label57);
            this.panel53.Location = new System.Drawing.Point(232, 14);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(95, 49);
            this.panel53.TabIndex = 2;
            // 
            // FRUnitVariableTb
            // 
            this.FRUnitVariableTb.BackColor = System.Drawing.Color.White;
            this.FRUnitVariableTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitVariableTb.Name = "FRUnitVariableTb";
            this.FRUnitVariableTb.ReadOnly = true;
            this.FRUnitVariableTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitVariableTb.TabIndex = 0;
            this.FRUnitVariableTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitVariableTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRUnitVariableTb_MouseClick);
            this.FRUnitVariableTb.Leave += new System.EventHandler(this.FRUnitVariableTb_Leave);
            this.FRUnitVariableTb.Validated += new System.EventHandler(this.FRUnitVariableTb_Validated);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.ForeColor = System.Drawing.SystemColors.Window;
            this.label57.Location = new System.Drawing.Point(14, 4);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(67, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "VARIABLE";
            // 
            // panel57
            // 
            this.panel57.BackColor = System.Drawing.Color.CadetBlue;
            this.panel57.Controls.Add(this.FRUnitFixedTb);
            this.panel57.Controls.Add(this.label61);
            this.panel57.Location = new System.Drawing.Point(118, 14);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(95, 49);
            this.panel57.TabIndex = 1;
            // 
            // FRUnitFixedTb
            // 
            this.FRUnitFixedTb.BackColor = System.Drawing.Color.White;
            this.FRUnitFixedTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitFixedTb.Name = "FRUnitFixedTb";
            this.FRUnitFixedTb.ReadOnly = true;
            this.FRUnitFixedTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitFixedTb.TabIndex = 0;
            this.FRUnitFixedTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitFixedTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRUnitFixedTb_MouseClick);
            this.FRUnitFixedTb.Leave += new System.EventHandler(this.FRUnitFixedTb_Leave);
            this.FRUnitFixedTb.Validated += new System.EventHandler(this.FRUnitFixedTb_Validated);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.ForeColor = System.Drawing.SystemColors.Window;
            this.label61.Location = new System.Drawing.Point(21, 4);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(43, 13);
            this.label61.TabIndex = 0;
            this.label61.Text = "FIXED";
            // 
            // panel58
            // 
            this.panel58.BackColor = System.Drawing.Color.CadetBlue;
            this.panel58.Controls.Add(this.FRUnitCapitalTb);
            this.panel58.Controls.Add(this.label62);
            this.panel58.Location = new System.Drawing.Point(6, 14);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(95, 49);
            this.panel58.TabIndex = 0;
            // 
            // FRUnitCapitalTb
            // 
            this.FRUnitCapitalTb.BackColor = System.Drawing.Color.White;
            this.FRUnitCapitalTb.Location = new System.Drawing.Point(9, 22);
            this.FRUnitCapitalTb.Name = "FRUnitCapitalTb";
            this.FRUnitCapitalTb.ReadOnly = true;
            this.FRUnitCapitalTb.Size = new System.Drawing.Size(75, 20);
            this.FRUnitCapitalTb.TabIndex = 0;
            this.FRUnitCapitalTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRUnitCapitalTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRUnitCapitalTb_MouseClick);
            this.FRUnitCapitalTb.Leave += new System.EventHandler(this.FRUnitCapitalTb_Leave);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.ForeColor = System.Drawing.SystemColors.Window;
            this.label62.Location = new System.Drawing.Point(21, 4);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(58, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "CAPITAL";
            // 
            // FRPlantPanel
            // 
            this.FRPlantPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRPlantPanel.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.FRPlantPanel.Controls.Add(this.lblinsufficient);
            this.FRPlantPanel.Controls.Add(this.groupBox5);
            this.FRPlantPanel.Location = new System.Drawing.Point(33, 46);
            this.FRPlantPanel.Name = "FRPlantPanel";
            this.FRPlantPanel.Size = new System.Drawing.Size(680, 446);
            this.FRPlantPanel.TabIndex = 34;
            this.FRPlantPanel.Visible = false;
            // 
            // lblinsufficient
            // 
            this.lblinsufficient.AutoSize = true;
            this.lblinsufficient.LinkColor = System.Drawing.Color.Gold;
            this.lblinsufficient.Location = new System.Drawing.Point(51, 422);
            this.lblinsufficient.Name = "lblinsufficient";
            this.lblinsufficient.Size = new System.Drawing.Size(0, 13);
            this.lblinsufficient.TabIndex = 2;
            this.lblinsufficient.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblinsufficient_LinkClicked);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.BackColor = System.Drawing.Color.Beige;
            this.groupBox5.Controls.Add(this.panel108);
            this.groupBox5.Controls.Add(this.FRPlantCal);
            this.groupBox5.Controls.Add(this.groupBox8);
            this.groupBox5.Controls.Add(this.groupBox7);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Controls.Add(this.label44);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox5.Location = new System.Drawing.Point(9, 13);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(663, 422);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "FINANCIAL";
            // 
            // panel108
            // 
            this.panel108.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel108.Controls.Add(this.dataGridViewECO);
            this.panel108.Location = new System.Drawing.Point(6, 82);
            this.panel108.Name = "panel108";
            this.panel108.Size = new System.Drawing.Size(647, 304);
            this.panel108.TabIndex = 34;
            // 
            // dataGridViewECO
            // 
            dataGridViewCellStyle45.BackColor = System.Drawing.Color.White;
            this.dataGridViewECO.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle45;
            this.dataGridViewECO.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewECO.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewECO.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle46.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle46.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle46.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle46.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle46.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewECO.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle46;
            this.dataGridViewECO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle47.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle47.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle47.Format = "C2";
            dataGridViewCellStyle47.NullValue = null;
            dataGridViewCellStyle47.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle47.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle47.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewECO.DefaultCellStyle = dataGridViewCellStyle47;
            this.dataGridViewECO.EnableHeadersVisualStyles = false;
            this.dataGridViewECO.Location = new System.Drawing.Point(6, 1);
            this.dataGridViewECO.Name = "dataGridViewECO";
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle48.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle48.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle48.Format = "N2";
            dataGridViewCellStyle48.NullValue = null;
            dataGridViewCellStyle48.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle48.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle48.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewECO.RowHeadersDefaultCellStyle = dataGridViewCellStyle48;
            this.dataGridViewECO.RowHeadersVisible = false;
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle49.BackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle49.Format = "N2";
            dataGridViewCellStyle49.NullValue = null;
            dataGridViewCellStyle49.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewECO.RowsDefaultCellStyle = dataGridViewCellStyle49;
            this.dataGridViewECO.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewECO.Size = new System.Drawing.Size(639, 299);
            this.dataGridViewECO.TabIndex = 38;
            // 
            // FRPlantCal
            // 
            this.FRPlantCal.HasButtons = true;
            this.FRPlantCal.Location = new System.Drawing.Point(64, 29);
            this.FRPlantCal.Name = "FRPlantCal";
            this.FRPlantCal.Readonly = true;
            this.FRPlantCal.Size = new System.Drawing.Size(120, 20);
            this.FRPlantCal.TabIndex = 33;
            this.FRPlantCal.ValueChanged += new System.EventHandler(this.FRPlantCal_ValueChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox8.BackColor = System.Drawing.Color.Beige;
            this.groupBox8.Controls.Add(this.panel54);
            this.groupBox8.Controls.Add(this.panel55);
            this.groupBox8.Controls.Add(this.panel56);
            this.groupBox8.Location = new System.Drawing.Point(311, 38);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(25, 22);
            this.groupBox8.TabIndex = 6;
            this.groupBox8.TabStop = false;
            this.groupBox8.Visible = false;
            // 
            // panel54
            // 
            this.panel54.BackColor = System.Drawing.Color.CadetBlue;
            this.panel54.Controls.Add(this.FRPlantCostTb);
            this.panel54.Controls.Add(this.label58);
            this.panel54.Location = new System.Drawing.Point(342, 14);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(95, 49);
            this.panel54.TabIndex = 5;
            // 
            // FRPlantCostTb
            // 
            this.FRPlantCostTb.Location = new System.Drawing.Point(5, 22);
            this.FRPlantCostTb.Name = "FRPlantCostTb";
            this.FRPlantCostTb.Size = new System.Drawing.Size(83, 20);
            this.FRPlantCostTb.TabIndex = 1;
            this.FRPlantCostTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRPlantCostTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRPlantCostTb_MouseClick);
            this.FRPlantCostTb.Leave += new System.EventHandler(this.FRPlantCostTb_Leave);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.SystemColors.Window;
            this.label58.Location = new System.Drawing.Point(26, 6);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(40, 13);
            this.label58.TabIndex = 0;
            this.label58.Text = "COST";
            // 
            // panel55
            // 
            this.panel55.BackColor = System.Drawing.Color.CadetBlue;
            this.panel55.Controls.Add(this.FRPlantIncomeTb);
            this.panel55.Controls.Add(this.label59);
            this.panel55.Location = new System.Drawing.Point(230, 14);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(98, 49);
            this.panel55.TabIndex = 4;
            // 
            // FRPlantIncomeTb
            // 
            this.FRPlantIncomeTb.Location = new System.Drawing.Point(6, 22);
            this.FRPlantIncomeTb.Name = "FRPlantIncomeTb";
            this.FRPlantIncomeTb.Size = new System.Drawing.Size(85, 20);
            this.FRPlantIncomeTb.TabIndex = 1;
            this.FRPlantIncomeTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRPlantIncomeTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRPlantIncomeTb_MouseClick);
            this.FRPlantIncomeTb.Leave += new System.EventHandler(this.FRPlantIncomeTb_Leave);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.SystemColors.Window;
            this.label59.Location = new System.Drawing.Point(21, 4);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(55, 13);
            this.label59.TabIndex = 0;
            this.label59.Text = "INCOME";
            // 
            // panel56
            // 
            this.panel56.BackColor = System.Drawing.Color.CadetBlue;
            this.panel56.Controls.Add(this.FRPlantBenefitTB);
            this.panel56.Controls.Add(this.label60);
            this.panel56.Location = new System.Drawing.Point(116, 14);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(101, 49);
            this.panel56.TabIndex = 3;
            // 
            // FRPlantBenefitTB
            // 
            this.FRPlantBenefitTB.Location = new System.Drawing.Point(6, 21);
            this.FRPlantBenefitTB.Name = "FRPlantBenefitTB";
            this.FRPlantBenefitTB.Size = new System.Drawing.Size(90, 20);
            this.FRPlantBenefitTB.TabIndex = 1;
            this.FRPlantBenefitTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRPlantBenefitTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRPlantBenefitTB_MouseClick);
            this.FRPlantBenefitTB.Leave += new System.EventHandler(this.FRPlantBenefitTB_Leave);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.SystemColors.Window;
            this.label60.Location = new System.Drawing.Point(21, 5);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(59, 13);
            this.label60.TabIndex = 0;
            this.label60.Text = "BENEFIT";
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.BackColor = System.Drawing.Color.Beige;
            this.groupBox7.Controls.Add(this.panel47);
            this.groupBox7.Controls.Add(this.panel48);
            this.groupBox7.Controls.Add(this.panel49);
            this.groupBox7.Controls.Add(this.panel50);
            this.groupBox7.Controls.Add(this.panel51);
            this.groupBox7.Controls.Add(this.panel52);
            this.groupBox7.Location = new System.Drawing.Point(307, 9);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(0, 22);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Visible = false;
            // 
            // panel47
            // 
            this.panel47.BackColor = System.Drawing.Color.CadetBlue;
            this.panel47.Controls.Add(this.FRPlantDecPayTb);
            this.panel47.Controls.Add(this.label51);
            this.panel47.Location = new System.Drawing.Point(575, 19);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(110, 49);
            this.panel47.TabIndex = 4;
            // 
            // FRPlantDecPayTb
            // 
            this.FRPlantDecPayTb.Location = new System.Drawing.Point(9, 22);
            this.FRPlantDecPayTb.Name = "FRPlantDecPayTb";
            this.FRPlantDecPayTb.Size = new System.Drawing.Size(92, 20);
            this.FRPlantDecPayTb.TabIndex = 1;
            this.FRPlantDecPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRPlantDecPayTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRPlantDecPayTb_MouseClick);
            this.FRPlantDecPayTb.Leave += new System.EventHandler(this.FRPlantDecPayTb_Leave);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.SystemColors.Window;
            this.label51.Location = new System.Drawing.Point(-1, 4);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(113, 13);
            this.label51.TabIndex = 0;
            this.label51.Text = "Decrease Payment";
            // 
            // panel48
            // 
            this.panel48.BackColor = System.Drawing.Color.CadetBlue;
            this.panel48.Controls.Add(this.FRPlantIncPayTb);
            this.panel48.Controls.Add(this.label52);
            this.panel48.Location = new System.Drawing.Point(453, 19);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(111, 49);
            this.panel48.TabIndex = 5;
            // 
            // FRPlantIncPayTb
            // 
            this.FRPlantIncPayTb.Location = new System.Drawing.Point(9, 22);
            this.FRPlantIncPayTb.Name = "FRPlantIncPayTb";
            this.FRPlantIncPayTb.Size = new System.Drawing.Size(92, 20);
            this.FRPlantIncPayTb.TabIndex = 1;
            this.FRPlantIncPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRPlantIncPayTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRPlantIncPayTb_MouseClick);
            this.FRPlantIncPayTb.Leave += new System.EventHandler(this.FRPlantIncPayTb_Validated);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.SystemColors.Window;
            this.label52.Location = new System.Drawing.Point(-3, 4);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(115, 13);
            this.label52.TabIndex = 0;
            this.label52.Text = "Increment Payment";
            // 
            // panel49
            // 
            this.panel49.BackColor = System.Drawing.Color.CadetBlue;
            this.panel49.Controls.Add(this.FRPlantULPayTb);
            this.panel49.Controls.Add(this.label53);
            this.panel49.Location = new System.Drawing.Point(347, 19);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(95, 49);
            this.panel49.TabIndex = 4;
            // 
            // FRPlantULPayTb
            // 
            this.FRPlantULPayTb.Location = new System.Drawing.Point(4, 22);
            this.FRPlantULPayTb.Name = "FRPlantULPayTb";
            this.FRPlantULPayTb.Size = new System.Drawing.Size(86, 20);
            this.FRPlantULPayTb.TabIndex = 1;
            this.FRPlantULPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRPlantULPayTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRPlantULPayTb_MouseClick);
            this.FRPlantULPayTb.Leave += new System.EventHandler(this.FRPlantULPayTb_Leave);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.SystemColors.Window;
            this.label53.Location = new System.Drawing.Point(8, 4);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(75, 13);
            this.label53.TabIndex = 0;
            this.label53.Text = "UL Payment";
            // 
            // panel50
            // 
            this.panel50.BackColor = System.Drawing.Color.CadetBlue;
            this.panel50.Controls.Add(this.FRPlantBidPayTb);
            this.panel50.Controls.Add(this.label54);
            this.panel50.Location = new System.Drawing.Point(240, 19);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(101, 49);
            this.panel50.TabIndex = 3;
            // 
            // FRPlantBidPayTb
            // 
            this.FRPlantBidPayTb.Location = new System.Drawing.Point(6, 21);
            this.FRPlantBidPayTb.Name = "FRPlantBidPayTb";
            this.FRPlantBidPayTb.Size = new System.Drawing.Size(87, 20);
            this.FRPlantBidPayTb.TabIndex = 1;
            this.FRPlantBidPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRPlantBidPayTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRPlantBidPayTb_MouseClick);
            this.FRPlantBidPayTb.Leave += new System.EventHandler(this.FRPlantBidPayTb_Leave);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.SystemColors.Window;
            this.label54.Location = new System.Drawing.Point(10, 3);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(77, 13);
            this.label54.TabIndex = 0;
            this.label54.Text = "Bid Payment";
            // 
            // panel51
            // 
            this.panel51.BackColor = System.Drawing.Color.CadetBlue;
            this.panel51.Controls.Add(this.FRPlantEnergyPayTb);
            this.panel51.Controls.Add(this.label55);
            this.panel51.Location = new System.Drawing.Point(126, 19);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(105, 49);
            this.panel51.TabIndex = 2;
            // 
            // FRPlantEnergyPayTb
            // 
            this.FRPlantEnergyPayTb.Location = new System.Drawing.Point(8, 22);
            this.FRPlantEnergyPayTb.Name = "FRPlantEnergyPayTb";
            this.FRPlantEnergyPayTb.Size = new System.Drawing.Size(87, 20);
            this.FRPlantEnergyPayTb.TabIndex = 1;
            this.FRPlantEnergyPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRPlantEnergyPayTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRPlantEnergyPayTb_MouseClick);
            this.FRPlantEnergyPayTb.Leave += new System.EventHandler(this.FRPlantEnergyPayTb_Leave);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.SystemColors.Window;
            this.label55.Location = new System.Drawing.Point(3, 4);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(98, 13);
            this.label55.TabIndex = 0;
            this.label55.Text = "Energy Payment";
            // 
            // panel52
            // 
            this.panel52.BackColor = System.Drawing.Color.CadetBlue;
            this.panel52.Controls.Add(this.FRPlantCapPayTb);
            this.panel52.Controls.Add(this.label56);
            this.panel52.Location = new System.Drawing.Point(6, 19);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(111, 49);
            this.panel52.TabIndex = 1;
            // 
            // FRPlantCapPayTb
            // 
            this.FRPlantCapPayTb.Location = new System.Drawing.Point(10, 22);
            this.FRPlantCapPayTb.Name = "FRPlantCapPayTb";
            this.FRPlantCapPayTb.Size = new System.Drawing.Size(91, 20);
            this.FRPlantCapPayTb.TabIndex = 1;
            this.FRPlantCapPayTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRPlantCapPayTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRPlantCapPayTb_MouseClick);
            this.FRPlantCapPayTb.Leave += new System.EventHandler(this.FRPlantCapPayTb_Leave);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.SystemColors.Window;
            this.label56.Location = new System.Drawing.Point(0, 4);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(108, 13);
            this.label56.TabIndex = 0;
            this.label56.Text = "Capacity Payment";
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.BackColor = System.Drawing.Color.Beige;
            this.groupBox6.Controls.Add(this.panel41);
            this.groupBox6.Controls.Add(this.panel42);
            this.groupBox6.Controls.Add(this.panel43);
            this.groupBox6.Controls.Add(this.panel44);
            this.groupBox6.Controls.Add(this.panel45);
            this.groupBox6.Controls.Add(this.panel46);
            this.groupBox6.Location = new System.Drawing.Point(367, 39);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(0, 21);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            this.groupBox6.Visible = false;
            // 
            // panel41
            // 
            this.panel41.BackColor = System.Drawing.Color.CadetBlue;
            this.panel41.Controls.Add(this.FRPlantDecPowerTb);
            this.panel41.Controls.Add(this.label45);
            this.panel41.Location = new System.Drawing.Point(578, 19);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(105, 49);
            this.panel41.TabIndex = 4;
            // 
            // FRPlantDecPowerTb
            // 
            this.FRPlantDecPowerTb.Location = new System.Drawing.Point(10, 21);
            this.FRPlantDecPowerTb.Name = "FRPlantDecPowerTb";
            this.FRPlantDecPowerTb.Size = new System.Drawing.Size(87, 20);
            this.FRPlantDecPowerTb.TabIndex = 1;
            this.FRPlantDecPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRPlantDecPowerTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRPlantDecPowerTb_MouseClick);
            this.FRPlantDecPowerTb.Leave += new System.EventHandler(this.FRPlantDecPowerTb_Leave);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.SystemColors.Window;
            this.label45.Location = new System.Drawing.Point(0, 4);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(100, 13);
            this.label45.TabIndex = 0;
            this.label45.Text = "Decrease Power";
            // 
            // panel42
            // 
            this.panel42.BackColor = System.Drawing.Color.CadetBlue;
            this.panel42.Controls.Add(this.FRPlantIncPowerTb);
            this.panel42.Controls.Add(this.label46);
            this.panel42.Location = new System.Drawing.Point(458, 19);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(106, 49);
            this.panel42.TabIndex = 5;
            // 
            // FRPlantIncPowerTb
            // 
            this.FRPlantIncPowerTb.Location = new System.Drawing.Point(10, 22);
            this.FRPlantIncPowerTb.Name = "FRPlantIncPowerTb";
            this.FRPlantIncPowerTb.Size = new System.Drawing.Size(88, 20);
            this.FRPlantIncPowerTb.TabIndex = 1;
            this.FRPlantIncPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRPlantIncPowerTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRPlantIncPowerTb_MouseClick);
            this.FRPlantIncPowerTb.Leave += new System.EventHandler(this.FRPlantIncPowerTb_Leave);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.SystemColors.Window;
            this.label46.Location = new System.Drawing.Point(3, 4);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(102, 13);
            this.label46.TabIndex = 0;
            this.label46.Text = "Increment Power";
            // 
            // panel43
            // 
            this.panel43.BackColor = System.Drawing.Color.CadetBlue;
            this.panel43.Controls.Add(this.FRPlantULPowerTb);
            this.panel43.Controls.Add(this.label47);
            this.panel43.Location = new System.Drawing.Point(348, 19);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(99, 49);
            this.panel43.TabIndex = 4;
            // 
            // FRPlantULPowerTb
            // 
            this.FRPlantULPowerTb.Location = new System.Drawing.Point(6, 22);
            this.FRPlantULPowerTb.Name = "FRPlantULPowerTb";
            this.FRPlantULPowerTb.Size = new System.Drawing.Size(88, 20);
            this.FRPlantULPowerTb.TabIndex = 1;
            this.FRPlantULPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRPlantULPowerTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRPlantULPowerTb_MouseClick);
            this.FRPlantULPowerTb.Leave += new System.EventHandler(this.FRPlantULPowerTb_Leave);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.SystemColors.Window;
            this.label47.Location = new System.Drawing.Point(16, 4);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(62, 13);
            this.label47.TabIndex = 0;
            this.label47.Text = "UL Power";
            // 
            // panel44
            // 
            this.panel44.BackColor = System.Drawing.Color.CadetBlue;
            this.panel44.Controls.Add(this.FRPlantBidPowerTb);
            this.panel44.Controls.Add(this.label48);
            this.panel44.Location = new System.Drawing.Point(239, 19);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(101, 49);
            this.panel44.TabIndex = 3;
            // 
            // FRPlantBidPowerTb
            // 
            this.FRPlantBidPowerTb.Location = new System.Drawing.Point(5, 21);
            this.FRPlantBidPowerTb.Name = "FRPlantBidPowerTb";
            this.FRPlantBidPowerTb.Size = new System.Drawing.Size(90, 20);
            this.FRPlantBidPowerTb.TabIndex = 1;
            this.FRPlantBidPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRPlantBidPowerTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRPlantBidPowerTb_MouseClick);
            this.FRPlantBidPowerTb.Leave += new System.EventHandler(this.FRPlantBidPowerTb_Leave);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.SystemColors.Window;
            this.label48.Location = new System.Drawing.Point(14, 4);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(64, 13);
            this.label48.TabIndex = 0;
            this.label48.Text = "Bid Power";
            // 
            // panel45
            // 
            this.panel45.BackColor = System.Drawing.Color.CadetBlue;
            this.panel45.Controls.Add(this.FRPlantTotalPowerTb);
            this.panel45.Controls.Add(this.label49);
            this.panel45.Location = new System.Drawing.Point(132, 19);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(95, 49);
            this.panel45.TabIndex = 2;
            // 
            // FRPlantTotalPowerTb
            // 
            this.FRPlantTotalPowerTb.Location = new System.Drawing.Point(5, 22);
            this.FRPlantTotalPowerTb.Name = "FRPlantTotalPowerTb";
            this.FRPlantTotalPowerTb.Size = new System.Drawing.Size(86, 20);
            this.FRPlantTotalPowerTb.TabIndex = 1;
            this.FRPlantTotalPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRPlantTotalPowerTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRPlantTotalPowerTb_MouseClick);
            this.FRPlantTotalPowerTb.Leave += new System.EventHandler(this.FRPlantTotalPowerTb_Leave);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.SystemColors.Window;
            this.label49.Location = new System.Drawing.Point(6, 4);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(75, 13);
            this.label49.TabIndex = 0;
            this.label49.Text = "Total Power";
            // 
            // panel46
            // 
            this.panel46.BackColor = System.Drawing.Color.CadetBlue;
            this.panel46.Controls.Add(this.FRPlantAvaCapTb);
            this.panel46.Controls.Add(this.label50);
            this.panel46.Location = new System.Drawing.Point(6, 19);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(114, 49);
            this.panel46.TabIndex = 1;
            // 
            // FRPlantAvaCapTb
            // 
            this.FRPlantAvaCapTb.Location = new System.Drawing.Point(13, 22);
            this.FRPlantAvaCapTb.Name = "FRPlantAvaCapTb";
            this.FRPlantAvaCapTb.Size = new System.Drawing.Size(88, 20);
            this.FRPlantAvaCapTb.TabIndex = 1;
            this.FRPlantAvaCapTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FRPlantAvaCapTb.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FRPlantAvaCapTb_MouseClick);
            this.FRPlantAvaCapTb.Leave += new System.EventHandler(this.FRPlantAvaCapTb_Leave);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.SystemColors.Window;
            this.label50.Location = new System.Drawing.Point(-1, 4);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(112, 13);
            this.label50.TabIndex = 0;
            this.label50.Text = "Available Capacity";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(19, 30);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(42, 13);
            this.label44.TabIndex = 2;
            this.label44.Text = "Date :";
            // 
            // FRHeaderGb
            // 
            this.FRHeaderGb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FRHeaderGb.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.FRHeaderGb.BackColor = System.Drawing.Color.Beige;
            this.FRHeaderGb.Controls.Add(this.FRPlantLb);
            this.FRHeaderGb.Controls.Add(this.FRHeaderPanel);
            this.FRHeaderGb.Controls.Add(this.L17);
            this.FRHeaderGb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FRHeaderGb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FRHeaderGb.ForeColor = System.Drawing.Color.Black;
            this.FRHeaderGb.Location = new System.Drawing.Point(15, 11);
            this.FRHeaderGb.Name = "FRHeaderGb";
            this.FRHeaderGb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FRHeaderGb.Size = new System.Drawing.Size(720, 53);
            this.FRHeaderGb.TabIndex = 22;
            this.FRHeaderGb.TabStop = false;
            this.FRHeaderGb.Visible = false;
            // 
            // FRPlantLb
            // 
            this.FRPlantLb.AutoSize = true;
            this.FRPlantLb.Location = new System.Drawing.Point(62, 23);
            this.FRPlantLb.Name = "FRPlantLb";
            this.FRPlantLb.Size = new System.Drawing.Size(0, 13);
            this.FRPlantLb.TabIndex = 9;
            // 
            // FRHeaderPanel
            // 
            this.FRHeaderPanel.Controls.Add(this.FRTypeLb);
            this.FRHeaderPanel.Controls.Add(this.FRUnitLb);
            this.FRHeaderPanel.Controls.Add(this.FRPackLb);
            this.FRHeaderPanel.Controls.Add(this.L20);
            this.FRHeaderPanel.Controls.Add(this.L19);
            this.FRHeaderPanel.Controls.Add(this.L18);
            this.FRHeaderPanel.Location = new System.Drawing.Point(144, 12);
            this.FRHeaderPanel.Name = "FRHeaderPanel";
            this.FRHeaderPanel.Size = new System.Drawing.Size(536, 35);
            this.FRHeaderPanel.TabIndex = 8;
            this.FRHeaderPanel.Visible = false;
            // 
            // FRTypeLb
            // 
            this.FRTypeLb.AutoSize = true;
            this.FRTypeLb.Location = new System.Drawing.Point(441, 11);
            this.FRTypeLb.Name = "FRTypeLb";
            this.FRTypeLb.Size = new System.Drawing.Size(0, 13);
            this.FRTypeLb.TabIndex = 13;
            // 
            // FRUnitLb
            // 
            this.FRUnitLb.AutoSize = true;
            this.FRUnitLb.Location = new System.Drawing.Point(278, 11);
            this.FRUnitLb.Name = "FRUnitLb";
            this.FRUnitLb.Size = new System.Drawing.Size(0, 13);
            this.FRUnitLb.TabIndex = 12;
            // 
            // FRPackLb
            // 
            this.FRPackLb.AutoSize = true;
            this.FRPackLb.Location = new System.Drawing.Point(112, 11);
            this.FRPackLb.Name = "FRPackLb";
            this.FRPackLb.Size = new System.Drawing.Size(0, 13);
            this.FRPackLb.TabIndex = 11;
            // 
            // L20
            // 
            this.L20.AutoSize = true;
            this.L20.BackColor = System.Drawing.Color.Transparent;
            this.L20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L20.Location = new System.Drawing.Point(402, 11);
            this.L20.Name = "L20";
            this.L20.Size = new System.Drawing.Size(47, 13);
            this.L20.TabIndex = 10;
            this.L20.Text = "Type : ";
            // 
            // L19
            // 
            this.L19.AutoSize = true;
            this.L19.BackColor = System.Drawing.Color.Transparent;
            this.L19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L19.Location = new System.Drawing.Point(234, 11);
            this.L19.Name = "L19";
            this.L19.Size = new System.Drawing.Size(42, 13);
            this.L19.TabIndex = 9;
            this.L19.Text = "Unit : ";
            // 
            // L18
            // 
            this.L18.AutoSize = true;
            this.L18.BackColor = System.Drawing.Color.Transparent;
            this.L18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L18.Location = new System.Drawing.Point(45, 11);
            this.L18.Name = "L18";
            this.L18.Size = new System.Drawing.Size(69, 13);
            this.L18.TabIndex = 8;
            this.L18.Text = "Package : ";
            // 
            // L17
            // 
            this.L17.AutoSize = true;
            this.L17.BackColor = System.Drawing.Color.Transparent;
            this.L17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L17.Location = new System.Drawing.Point(10, 23);
            this.L17.Name = "L17";
            this.L17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.L17.Size = new System.Drawing.Size(48, 13);
            this.L17.TabIndex = 7;
            this.L17.Text = "Plant : ";
            this.L17.Visible = false;
            // 
            // BidData
            // 
            this.BidData.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.BidData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BidData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BidData.Controls.Add(this.BDMainPanel);
            this.BidData.Controls.Add(this.BDHeaderGb);
            this.BidData.Location = new System.Drawing.Point(4, 29);
            this.BidData.Name = "BidData";
            this.BidData.Size = new System.Drawing.Size(757, 625);
            this.BidData.TabIndex = 2;
            this.BidData.Text = "Bid Data";
            this.BidData.UseVisualStyleBackColor = true;
            // 
            // BDMainPanel
            // 
            this.BDMainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BDMainPanel.Controls.Add(this.btnToExcel);
            this.BDMainPanel.Controls.Add(this.btnbidprint);
            this.BDMainPanel.Controls.Add(this.label113);
            this.BDMainPanel.Controls.Add(this.BDPlotBtn);
            this.BDMainPanel.Controls.Add(this.BDCur2);
            this.BDMainPanel.Controls.Add(this.BDCur1);
            this.BDMainPanel.Location = new System.Drawing.Point(1, 66);
            this.BDMainPanel.Name = "BDMainPanel";
            this.BDMainPanel.Size = new System.Drawing.Size(743, 554);
            this.BDMainPanel.TabIndex = 22;
            // 
            // btnToExcel
            // 
            this.btnToExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnToExcel.BackColor = System.Drawing.Color.CadetBlue;
            this.btnToExcel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnToExcel.Location = new System.Drawing.Point(427, 523);
            this.btnToExcel.Name = "btnToExcel";
            this.btnToExcel.Size = new System.Drawing.Size(75, 23);
            this.btnToExcel.TabIndex = 37;
            this.btnToExcel.Text = "To Excel";
            this.btnToExcel.UseVisualStyleBackColor = false;
            this.btnToExcel.Visible = false;
            this.btnToExcel.Click += new System.EventHandler(this.btnToExcel_Click);
            // 
            // btnbidprint
            // 
            this.btnbidprint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnbidprint.BackColor = System.Drawing.Color.CadetBlue;
            this.btnbidprint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnbidprint.Location = new System.Drawing.Point(532, 523);
            this.btnbidprint.Name = "btnbidprint";
            this.btnbidprint.Size = new System.Drawing.Size(75, 23);
            this.btnbidprint.TabIndex = 37;
            this.btnbidprint.Text = "Print";
            this.btnbidprint.UseVisualStyleBackColor = false;
            this.btnbidprint.Visible = false;
            this.btnbidprint.Click += new System.EventHandler(this.btnbidprint_Click);
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label113.Location = new System.Drawing.Point(16, 533);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(0, 13);
            this.label113.TabIndex = 35;
            // 
            // BDPlotBtn
            // 
            this.BDPlotBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BDPlotBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.BDPlotBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BDPlotBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BDPlotBtn.Location = new System.Drawing.Point(638, 523);
            this.BDPlotBtn.Name = "BDPlotBtn";
            this.BDPlotBtn.Size = new System.Drawing.Size(75, 23);
            this.BDPlotBtn.TabIndex = 36;
            this.BDPlotBtn.Text = "Plot";
            this.BDPlotBtn.UseVisualStyleBackColor = false;
            this.BDPlotBtn.Visible = false;
            this.BDPlotBtn.Click += new System.EventHandler(this.BDPlotBtn_Click);
            // 
            // BDCur2
            // 
            this.BDCur2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BDCur2.BackColor = System.Drawing.Color.Beige;
            this.BDCur2.Controls.Add(this.EstimatedBidCheck);
            this.BDCur2.Controls.Add(this.RealBidCheck);
            this.BDCur2.Controls.Add(this.BDCal);
            this.BDCur2.Controls.Add(this.BDCurGrid);
            this.BDCur2.Controls.Add(this.label43);
            this.BDCur2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BDCur2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.BDCur2.Location = new System.Drawing.Point(9, 74);
            this.BDCur2.Name = "BDCur2";
            this.BDCur2.Size = new System.Drawing.Size(724, 443);
            this.BDCur2.TabIndex = 35;
            this.BDCur2.TabStop = false;
            this.BDCur2.Text = "CURRENT STATE";
            this.BDCur2.Visible = false;
            // 
            // EstimatedBidCheck
            // 
            this.EstimatedBidCheck.AutoSize = true;
            this.EstimatedBidCheck.Location = new System.Drawing.Point(386, 18);
            this.EstimatedBidCheck.Name = "EstimatedBidCheck";
            this.EstimatedBidCheck.Size = new System.Drawing.Size(102, 17);
            this.EstimatedBidCheck.TabIndex = 34;
            this.EstimatedBidCheck.TabStop = true;
            this.EstimatedBidCheck.Text = "Estimated Bid";
            this.EstimatedBidCheck.UseVisualStyleBackColor = true;
            this.EstimatedBidCheck.CheckedChanged += new System.EventHandler(this.EstimatedBidCheck_CheckedChanged);
            // 
            // RealBidCheck
            // 
            this.RealBidCheck.AutoSize = true;
            this.RealBidCheck.Checked = true;
            this.RealBidCheck.Location = new System.Drawing.Point(283, 18);
            this.RealBidCheck.Name = "RealBidCheck";
            this.RealBidCheck.Size = new System.Drawing.Size(73, 17);
            this.RealBidCheck.TabIndex = 33;
            this.RealBidCheck.TabStop = true;
            this.RealBidCheck.Text = "Real Bid";
            this.RealBidCheck.UseVisualStyleBackColor = true;
            this.RealBidCheck.CheckedChanged += new System.EventHandler(this.RealBidCheck_CheckedChanged);
            // 
            // BDCal
            // 
            this.BDCal.HasButtons = true;
            this.BDCal.Location = new System.Drawing.Point(67, 20);
            this.BDCal.Name = "BDCal";
            this.BDCal.Readonly = true;
            this.BDCal.Size = new System.Drawing.Size(120, 20);
            this.BDCal.TabIndex = 32;
            this.BDCal.ValueChanged += new System.EventHandler(this.BDCal_ValueChanged);
            // 
            // BDCurGrid
            // 
            dataGridViewCellStyle41.BackColor = System.Drawing.Color.White;
            this.BDCurGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle41;
            this.BDCurGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BDCurGrid.AutoGenerateColumns = false;
            this.BDCurGrid.BackgroundColor = System.Drawing.Color.White;
            this.BDCurGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle42.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle42.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BDCurGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle42;
            this.BDCurGrid.ColumnHeadersHeight = 22;
            this.BDCurGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40,
            this.dataGridViewTextBoxColumn41,
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewTextBoxColumn45,
            this.dataGridViewTextBoxColumn46,
            this.Column30,
            this.power6DataGridViewTextBoxColumn,
            this.price6DataGridViewTextBoxColumn,
            this.power7DataGridViewTextBoxColumn,
            this.price7DataGridViewTextBoxColumn,
            this.power8DataGridViewTextBoxColumn,
            this.price8DataGridViewTextBoxColumn,
            this.power9DataGridViewTextBoxColumn,
            this.price9DataGridViewTextBoxColumn,
            this.power10DataGridViewTextBoxColumn,
            this.price10DataGridViewTextBoxColumn});
            this.BDCurGrid.DataSource = this.detailFRM002BindingSource;
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle43.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle43.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.BDCurGrid.DefaultCellStyle = dataGridViewCellStyle43;
            this.BDCurGrid.EnableHeadersVisualStyles = false;
            this.BDCurGrid.Location = new System.Drawing.Point(19, 48);
            this.BDCurGrid.Name = "BDCurGrid";
            this.BDCurGrid.ReadOnly = true;
            this.BDCurGrid.RowHeadersVisible = false;
            dataGridViewCellStyle44.BackColor = System.Drawing.Color.LemonChiffon;
            this.BDCurGrid.RowsDefaultCellStyle = dataGridViewCellStyle44;
            this.BDCurGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.BDCurGrid.Size = new System.Drawing.Size(685, 393);
            this.BDCurGrid.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.DataPropertyName = "Hour";
            this.dataGridViewTextBoxColumn37.HeaderText = "Hour";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.ReadOnly = true;
            this.dataGridViewTextBoxColumn37.Width = 62;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.DataPropertyName = "Power1";
            this.dataGridViewTextBoxColumn38.HeaderText = "Power1";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            this.dataGridViewTextBoxColumn38.Width = 62;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.DataPropertyName = "Price1";
            this.dataGridViewTextBoxColumn39.HeaderText = "Price1";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.ReadOnly = true;
            this.dataGridViewTextBoxColumn39.Width = 62;
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.DataPropertyName = "Power2";
            this.dataGridViewTextBoxColumn40.HeaderText = "Power2";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.ReadOnly = true;
            this.dataGridViewTextBoxColumn40.Width = 62;
            // 
            // dataGridViewTextBoxColumn41
            // 
            this.dataGridViewTextBoxColumn41.DataPropertyName = "Price2";
            this.dataGridViewTextBoxColumn41.HeaderText = "Price2";
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.ReadOnly = true;
            this.dataGridViewTextBoxColumn41.Width = 62;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.DataPropertyName = "Power3";
            this.dataGridViewTextBoxColumn42.HeaderText = "Power3";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.ReadOnly = true;
            this.dataGridViewTextBoxColumn42.Width = 62;
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.DataPropertyName = "Price3";
            this.dataGridViewTextBoxColumn43.HeaderText = "Price3";
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.ReadOnly = true;
            this.dataGridViewTextBoxColumn43.Width = 62;
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.DataPropertyName = "Power4";
            this.dataGridViewTextBoxColumn44.HeaderText = "Power4";
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.ReadOnly = true;
            this.dataGridViewTextBoxColumn44.Width = 62;
            // 
            // dataGridViewTextBoxColumn45
            // 
            this.dataGridViewTextBoxColumn45.DataPropertyName = "Price4";
            this.dataGridViewTextBoxColumn45.HeaderText = "Price4";
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            this.dataGridViewTextBoxColumn45.ReadOnly = true;
            this.dataGridViewTextBoxColumn45.Width = 62;
            // 
            // dataGridViewTextBoxColumn46
            // 
            this.dataGridViewTextBoxColumn46.DataPropertyName = "Power5";
            this.dataGridViewTextBoxColumn46.HeaderText = "Power5";
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.ReadOnly = true;
            this.dataGridViewTextBoxColumn46.Width = 62;
            // 
            // Column30
            // 
            this.Column30.DataPropertyName = "Price5";
            this.Column30.HeaderText = "Price5";
            this.Column30.Name = "Column30";
            this.Column30.ReadOnly = true;
            this.Column30.Width = 62;
            // 
            // power6DataGridViewTextBoxColumn
            // 
            this.power6DataGridViewTextBoxColumn.DataPropertyName = "Power6";
            this.power6DataGridViewTextBoxColumn.HeaderText = "Power6";
            this.power6DataGridViewTextBoxColumn.Name = "power6DataGridViewTextBoxColumn";
            this.power6DataGridViewTextBoxColumn.ReadOnly = true;
            this.power6DataGridViewTextBoxColumn.Width = 62;
            // 
            // price6DataGridViewTextBoxColumn
            // 
            this.price6DataGridViewTextBoxColumn.DataPropertyName = "Price6";
            this.price6DataGridViewTextBoxColumn.HeaderText = "Price6";
            this.price6DataGridViewTextBoxColumn.Name = "price6DataGridViewTextBoxColumn";
            this.price6DataGridViewTextBoxColumn.ReadOnly = true;
            this.price6DataGridViewTextBoxColumn.Width = 62;
            // 
            // power7DataGridViewTextBoxColumn
            // 
            this.power7DataGridViewTextBoxColumn.DataPropertyName = "Power7";
            this.power7DataGridViewTextBoxColumn.HeaderText = "Power7";
            this.power7DataGridViewTextBoxColumn.Name = "power7DataGridViewTextBoxColumn";
            this.power7DataGridViewTextBoxColumn.ReadOnly = true;
            this.power7DataGridViewTextBoxColumn.Width = 62;
            // 
            // price7DataGridViewTextBoxColumn
            // 
            this.price7DataGridViewTextBoxColumn.DataPropertyName = "Price7";
            this.price7DataGridViewTextBoxColumn.HeaderText = "Price7";
            this.price7DataGridViewTextBoxColumn.Name = "price7DataGridViewTextBoxColumn";
            this.price7DataGridViewTextBoxColumn.ReadOnly = true;
            this.price7DataGridViewTextBoxColumn.Width = 62;
            // 
            // power8DataGridViewTextBoxColumn
            // 
            this.power8DataGridViewTextBoxColumn.DataPropertyName = "Power8";
            this.power8DataGridViewTextBoxColumn.HeaderText = "Power8";
            this.power8DataGridViewTextBoxColumn.Name = "power8DataGridViewTextBoxColumn";
            this.power8DataGridViewTextBoxColumn.ReadOnly = true;
            this.power8DataGridViewTextBoxColumn.Width = 62;
            // 
            // price8DataGridViewTextBoxColumn
            // 
            this.price8DataGridViewTextBoxColumn.DataPropertyName = "Price8";
            this.price8DataGridViewTextBoxColumn.HeaderText = "Price8";
            this.price8DataGridViewTextBoxColumn.Name = "price8DataGridViewTextBoxColumn";
            this.price8DataGridViewTextBoxColumn.ReadOnly = true;
            this.price8DataGridViewTextBoxColumn.Width = 62;
            // 
            // power9DataGridViewTextBoxColumn
            // 
            this.power9DataGridViewTextBoxColumn.DataPropertyName = "Power9";
            this.power9DataGridViewTextBoxColumn.HeaderText = "Power9";
            this.power9DataGridViewTextBoxColumn.Name = "power9DataGridViewTextBoxColumn";
            this.power9DataGridViewTextBoxColumn.ReadOnly = true;
            this.power9DataGridViewTextBoxColumn.Width = 62;
            // 
            // price9DataGridViewTextBoxColumn
            // 
            this.price9DataGridViewTextBoxColumn.DataPropertyName = "Price9";
            this.price9DataGridViewTextBoxColumn.HeaderText = "Price9";
            this.price9DataGridViewTextBoxColumn.Name = "price9DataGridViewTextBoxColumn";
            this.price9DataGridViewTextBoxColumn.ReadOnly = true;
            this.price9DataGridViewTextBoxColumn.Width = 62;
            // 
            // power10DataGridViewTextBoxColumn
            // 
            this.power10DataGridViewTextBoxColumn.DataPropertyName = "Power10";
            this.power10DataGridViewTextBoxColumn.HeaderText = "Power10";
            this.power10DataGridViewTextBoxColumn.Name = "power10DataGridViewTextBoxColumn";
            this.power10DataGridViewTextBoxColumn.ReadOnly = true;
            this.power10DataGridViewTextBoxColumn.Width = 62;
            // 
            // price10DataGridViewTextBoxColumn
            // 
            this.price10DataGridViewTextBoxColumn.DataPropertyName = "Price10";
            this.price10DataGridViewTextBoxColumn.HeaderText = "Price10";
            this.price10DataGridViewTextBoxColumn.Name = "price10DataGridViewTextBoxColumn";
            this.price10DataGridViewTextBoxColumn.ReadOnly = true;
            this.price10DataGridViewTextBoxColumn.Width = 62;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(23, 22);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(42, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "Date :";
            // 
            // BDCur1
            // 
            this.BDCur1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BDCur1.BackColor = System.Drawing.Color.Beige;
            this.BDCur1.Controls.Add(this.panel38);
            this.BDCur1.Controls.Add(this.panel39);
            this.BDCur1.Controls.Add(this.panel40);
            this.BDCur1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BDCur1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.BDCur1.Location = new System.Drawing.Point(204, 9);
            this.BDCur1.Name = "BDCur1";
            this.BDCur1.Size = new System.Drawing.Size(327, 59);
            this.BDCur1.TabIndex = 34;
            this.BDCur1.TabStop = false;
            this.BDCur1.Text = "CURRENT STATE";
            this.BDCur1.Visible = false;
            // 
            // panel38
            // 
            this.panel38.BackColor = System.Drawing.Color.CadetBlue;
            this.panel38.Controls.Add(this.BDMaxBidTb);
            this.panel38.Controls.Add(this.label40);
            this.panel38.Location = new System.Drawing.Point(232, 15);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(91, 40);
            this.panel38.TabIndex = 3;
            // 
            // BDMaxBidTb
            // 
            this.BDMaxBidTb.BackColor = System.Drawing.Color.White;
            this.BDMaxBidTb.Location = new System.Drawing.Point(5, 18);
            this.BDMaxBidTb.Name = "BDMaxBidTb";
            this.BDMaxBidTb.ReadOnly = true;
            this.BDMaxBidTb.Size = new System.Drawing.Size(82, 20);
            this.BDMaxBidTb.TabIndex = 1;
            this.BDMaxBidTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.ForeColor = System.Drawing.SystemColors.Window;
            this.label40.Location = new System.Drawing.Point(21, 2);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(51, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "MAX BID";
            // 
            // panel39
            // 
            this.panel39.BackColor = System.Drawing.Color.CadetBlue;
            this.panel39.Controls.Add(this.BDPowerTb);
            this.panel39.Controls.Add(this.label41);
            this.panel39.Location = new System.Drawing.Point(117, 15);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(91, 40);
            this.panel39.TabIndex = 2;
            // 
            // BDPowerTb
            // 
            this.BDPowerTb.BackColor = System.Drawing.Color.White;
            this.BDPowerTb.Location = new System.Drawing.Point(4, 18);
            this.BDPowerTb.Name = "BDPowerTb";
            this.BDPowerTb.ReadOnly = true;
            this.BDPowerTb.Size = new System.Drawing.Size(82, 20);
            this.BDPowerTb.TabIndex = 1;
            this.BDPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.ForeColor = System.Drawing.SystemColors.Window;
            this.label41.Location = new System.Drawing.Point(21, 4);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(48, 13);
            this.label41.TabIndex = 0;
            this.label41.Text = "POWER";
            // 
            // panel40
            // 
            this.panel40.BackColor = System.Drawing.Color.CadetBlue;
            this.panel40.Controls.Add(this.BDStateTb);
            this.panel40.Controls.Add(this.label42);
            this.panel40.Location = new System.Drawing.Point(6, 16);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(91, 40);
            this.panel40.TabIndex = 1;
            // 
            // BDStateTb
            // 
            this.BDStateTb.BackColor = System.Drawing.Color.White;
            this.BDStateTb.Location = new System.Drawing.Point(4, 17);
            this.BDStateTb.Name = "BDStateTb";
            this.BDStateTb.ReadOnly = true;
            this.BDStateTb.Size = new System.Drawing.Size(82, 20);
            this.BDStateTb.TabIndex = 1;
            this.BDStateTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.ForeColor = System.Drawing.SystemColors.Window;
            this.label42.Location = new System.Drawing.Point(22, 1);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(42, 13);
            this.label42.TabIndex = 0;
            this.label42.Text = "STATE";
            // 
            // BDHeaderGb
            // 
            this.BDHeaderGb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BDHeaderGb.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BDHeaderGb.BackColor = System.Drawing.Color.Beige;
            this.BDHeaderGb.Controls.Add(this.BDPlantLb);
            this.BDHeaderGb.Controls.Add(this.BDHeaderPanel);
            this.BDHeaderGb.Controls.Add(this.L13);
            this.BDHeaderGb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BDHeaderGb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.BDHeaderGb.ForeColor = System.Drawing.Color.Black;
            this.BDHeaderGb.Location = new System.Drawing.Point(15, 11);
            this.BDHeaderGb.Name = "BDHeaderGb";
            this.BDHeaderGb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.BDHeaderGb.Size = new System.Drawing.Size(720, 53);
            this.BDHeaderGb.TabIndex = 21;
            this.BDHeaderGb.TabStop = false;
            this.BDHeaderGb.Visible = false;
            // 
            // BDPlantLb
            // 
            this.BDPlantLb.AutoSize = true;
            this.BDPlantLb.Location = new System.Drawing.Point(62, 23);
            this.BDPlantLb.Name = "BDPlantLb";
            this.BDPlantLb.Size = new System.Drawing.Size(0, 13);
            this.BDPlantLb.TabIndex = 9;
            // 
            // BDHeaderPanel
            // 
            this.BDHeaderPanel.Controls.Add(this.BDTypeLb);
            this.BDHeaderPanel.Controls.Add(this.BDUnitLb);
            this.BDHeaderPanel.Controls.Add(this.BDPackLb);
            this.BDHeaderPanel.Controls.Add(this.L16);
            this.BDHeaderPanel.Controls.Add(this.L15);
            this.BDHeaderPanel.Controls.Add(this.L14);
            this.BDHeaderPanel.Location = new System.Drawing.Point(144, 12);
            this.BDHeaderPanel.Name = "BDHeaderPanel";
            this.BDHeaderPanel.Size = new System.Drawing.Size(537, 35);
            this.BDHeaderPanel.TabIndex = 8;
            this.BDHeaderPanel.Visible = false;
            // 
            // BDTypeLb
            // 
            this.BDTypeLb.AutoSize = true;
            this.BDTypeLb.Location = new System.Drawing.Point(445, 11);
            this.BDTypeLb.Name = "BDTypeLb";
            this.BDTypeLb.Size = new System.Drawing.Size(0, 13);
            this.BDTypeLb.TabIndex = 13;
            // 
            // BDUnitLb
            // 
            this.BDUnitLb.AutoSize = true;
            this.BDUnitLb.Location = new System.Drawing.Point(266, 11);
            this.BDUnitLb.Name = "BDUnitLb";
            this.BDUnitLb.Size = new System.Drawing.Size(0, 13);
            this.BDUnitLb.TabIndex = 12;
            // 
            // BDPackLb
            // 
            this.BDPackLb.AutoSize = true;
            this.BDPackLb.Location = new System.Drawing.Point(110, 11);
            this.BDPackLb.Name = "BDPackLb";
            this.BDPackLb.Size = new System.Drawing.Size(0, 13);
            this.BDPackLb.TabIndex = 11;
            // 
            // L16
            // 
            this.L16.AutoSize = true;
            this.L16.BackColor = System.Drawing.Color.Transparent;
            this.L16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L16.Location = new System.Drawing.Point(402, 11);
            this.L16.Name = "L16";
            this.L16.Size = new System.Drawing.Size(47, 13);
            this.L16.TabIndex = 10;
            this.L16.Text = "Type : ";
            // 
            // L15
            // 
            this.L15.AutoSize = true;
            this.L15.BackColor = System.Drawing.Color.Transparent;
            this.L15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L15.Location = new System.Drawing.Point(234, 11);
            this.L15.Name = "L15";
            this.L15.Size = new System.Drawing.Size(42, 13);
            this.L15.TabIndex = 9;
            this.L15.Text = "Unit : ";
            // 
            // L14
            // 
            this.L14.AutoSize = true;
            this.L14.BackColor = System.Drawing.Color.Transparent;
            this.L14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L14.Location = new System.Drawing.Point(45, 11);
            this.L14.Name = "L14";
            this.L14.Size = new System.Drawing.Size(69, 13);
            this.L14.TabIndex = 8;
            this.L14.Text = "Package : ";
            // 
            // L13
            // 
            this.L13.AutoSize = true;
            this.L13.BackColor = System.Drawing.Color.Transparent;
            this.L13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L13.Location = new System.Drawing.Point(10, 23);
            this.L13.Name = "L13";
            this.L13.Size = new System.Drawing.Size(48, 13);
            this.L13.TabIndex = 7;
            this.L13.Text = "Plant : ";
            this.L13.Visible = false;
            // 
            // MarketResults
            // 
            this.MarketResults.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.MarketResults.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MarketResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.MarketResults.Controls.Add(this.MRHeaderGB);
            this.MarketResults.Controls.Add(this.MRMainPanel);
            this.MarketResults.Location = new System.Drawing.Point(4, 29);
            this.MarketResults.Name = "MarketResults";
            this.MarketResults.Padding = new System.Windows.Forms.Padding(3);
            this.MarketResults.Size = new System.Drawing.Size(757, 625);
            this.MarketResults.TabIndex = 1;
            this.MarketResults.Text = "Market Results";
            this.MarketResults.UseVisualStyleBackColor = true;
            // 
            // MRHeaderGB
            // 
            this.MRHeaderGB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MRHeaderGB.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.MRHeaderGB.BackColor = System.Drawing.Color.Beige;
            this.MRHeaderGB.Controls.Add(this.MRPlantLb);
            this.MRHeaderGB.Controls.Add(this.MRHeaderPanel);
            this.MRHeaderGB.Controls.Add(this.L9);
            this.MRHeaderGB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MRHeaderGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MRHeaderGB.ForeColor = System.Drawing.Color.Black;
            this.MRHeaderGB.Location = new System.Drawing.Point(15, 11);
            this.MRHeaderGB.Name = "MRHeaderGB";
            this.MRHeaderGB.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.MRHeaderGB.Size = new System.Drawing.Size(720, 53);
            this.MRHeaderGB.TabIndex = 20;
            this.MRHeaderGB.TabStop = false;
            this.MRHeaderGB.Visible = false;
            // 
            // MRPlantLb
            // 
            this.MRPlantLb.AutoSize = true;
            this.MRPlantLb.Location = new System.Drawing.Point(62, 23);
            this.MRPlantLb.Name = "MRPlantLb";
            this.MRPlantLb.Size = new System.Drawing.Size(0, 13);
            this.MRPlantLb.TabIndex = 9;
            // 
            // MRHeaderPanel
            // 
            this.MRHeaderPanel.Controls.Add(this.MRTypeLb);
            this.MRHeaderPanel.Controls.Add(this.MRUnitLb);
            this.MRHeaderPanel.Controls.Add(this.MRPackLb);
            this.MRHeaderPanel.Controls.Add(this.L12);
            this.MRHeaderPanel.Controls.Add(this.L11);
            this.MRHeaderPanel.Controls.Add(this.L10);
            this.MRHeaderPanel.Location = new System.Drawing.Point(144, 12);
            this.MRHeaderPanel.Name = "MRHeaderPanel";
            this.MRHeaderPanel.Size = new System.Drawing.Size(537, 35);
            this.MRHeaderPanel.TabIndex = 8;
            this.MRHeaderPanel.Visible = false;
            // 
            // MRTypeLb
            // 
            this.MRTypeLb.AutoSize = true;
            this.MRTypeLb.Location = new System.Drawing.Point(445, 11);
            this.MRTypeLb.Name = "MRTypeLb";
            this.MRTypeLb.Size = new System.Drawing.Size(0, 13);
            this.MRTypeLb.TabIndex = 13;
            // 
            // MRUnitLb
            // 
            this.MRUnitLb.AutoSize = true;
            this.MRUnitLb.Location = new System.Drawing.Point(279, 11);
            this.MRUnitLb.Name = "MRUnitLb";
            this.MRUnitLb.Size = new System.Drawing.Size(0, 13);
            this.MRUnitLb.TabIndex = 12;
            // 
            // MRPackLb
            // 
            this.MRPackLb.AutoSize = true;
            this.MRPackLb.Location = new System.Drawing.Point(114, 11);
            this.MRPackLb.Name = "MRPackLb";
            this.MRPackLb.Size = new System.Drawing.Size(0, 13);
            this.MRPackLb.TabIndex = 11;
            // 
            // L12
            // 
            this.L12.AutoSize = true;
            this.L12.BackColor = System.Drawing.Color.Transparent;
            this.L12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L12.Location = new System.Drawing.Point(402, 11);
            this.L12.Name = "L12";
            this.L12.Size = new System.Drawing.Size(47, 13);
            this.L12.TabIndex = 10;
            this.L12.Text = "Type : ";
            // 
            // L11
            // 
            this.L11.AutoSize = true;
            this.L11.BackColor = System.Drawing.Color.Transparent;
            this.L11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L11.Location = new System.Drawing.Point(234, 11);
            this.L11.Name = "L11";
            this.L11.Size = new System.Drawing.Size(42, 13);
            this.L11.TabIndex = 9;
            this.L11.Text = "Unit : ";
            // 
            // L10
            // 
            this.L10.AutoSize = true;
            this.L10.BackColor = System.Drawing.Color.Transparent;
            this.L10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L10.Location = new System.Drawing.Point(45, 11);
            this.L10.Name = "L10";
            this.L10.Size = new System.Drawing.Size(69, 13);
            this.L10.TabIndex = 8;
            this.L10.Text = "Package : ";
            // 
            // L9
            // 
            this.L9.AutoSize = true;
            this.L9.BackColor = System.Drawing.Color.Transparent;
            this.L9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L9.Location = new System.Drawing.Point(10, 23);
            this.L9.Name = "L9";
            this.L9.Size = new System.Drawing.Size(48, 13);
            this.L9.TabIndex = 7;
            this.L9.Text = "Plant : ";
            this.L9.Visible = false;
            // 
            // MRMainPanel
            // 
            this.MRMainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MRMainPanel.Controls.Add(this.btnToExcelMR);
            this.MRMainPanel.Controls.Add(this.rdbam005ba);
            this.MRMainPanel.Controls.Add(this.rdm005);
            this.MRMainPanel.Controls.Add(this.mrbtnpprint);
            this.MRMainPanel.Controls.Add(this.btnex009);
            this.MRMainPanel.Controls.Add(this.MRPlotBtn);
            this.MRMainPanel.Controls.Add(this.MRGB);
            this.MRMainPanel.Controls.Add(this.MRPlantCurGb);
            this.MRMainPanel.Controls.Add(this.MRUnitCurGb);
            this.MRMainPanel.Location = new System.Drawing.Point(6, 65);
            this.MRMainPanel.Name = "MRMainPanel";
            this.MRMainPanel.Size = new System.Drawing.Size(738, 550);
            this.MRMainPanel.TabIndex = 21;
            // 
            // btnToExcelMR
            // 
            this.btnToExcelMR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnToExcelMR.BackColor = System.Drawing.Color.CadetBlue;
            this.btnToExcelMR.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnToExcelMR.Location = new System.Drawing.Point(90, 518);
            this.btnToExcelMR.Name = "btnToExcelMR";
            this.btnToExcelMR.Size = new System.Drawing.Size(75, 23);
            this.btnToExcelMR.TabIndex = 38;
            this.btnToExcelMR.Text = "To Excel";
            this.btnToExcelMR.UseVisualStyleBackColor = false;
            this.btnToExcelMR.Visible = false;
            this.btnToExcelMR.Click += new System.EventHandler(this.btnToExcelMR_Click);
            // 
            // rdbam005ba
            // 
            this.rdbam005ba.AutoSize = true;
            this.rdbam005ba.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rdbam005ba.Location = new System.Drawing.Point(22, 51);
            this.rdbam005ba.Name = "rdbam005ba";
            this.rdbam005ba.Size = new System.Drawing.Size(130, 17);
            this.rdbam005ba.TabIndex = 34;
            this.rdbam005ba.Text = "M005 With Limited";
            this.rdbam005ba.UseVisualStyleBackColor = true;
            this.rdbam005ba.Visible = false;
            this.rdbam005ba.CheckedChanged += new System.EventHandler(this.MRCal_ValueChanged);
            // 
            // rdm005
            // 
            this.rdm005.AutoSize = true;
            this.rdm005.Checked = true;
            this.rdm005.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rdm005.Location = new System.Drawing.Point(22, 18);
            this.rdm005.Name = "rdm005";
            this.rdm005.Size = new System.Drawing.Size(56, 17);
            this.rdm005.TabIndex = 34;
            this.rdm005.TabStop = true;
            this.rdm005.Text = "M005";
            this.rdm005.UseVisualStyleBackColor = true;
            this.rdm005.Visible = false;
            this.rdm005.CheckedChanged += new System.EventHandler(this.MRCal_ValueChanged);
            // 
            // mrbtnpprint
            // 
            this.mrbtnpprint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mrbtnpprint.BackColor = System.Drawing.Color.CadetBlue;
            this.mrbtnpprint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.mrbtnpprint.Location = new System.Drawing.Point(9, 519);
            this.mrbtnpprint.Name = "mrbtnpprint";
            this.mrbtnpprint.Size = new System.Drawing.Size(75, 23);
            this.mrbtnpprint.TabIndex = 33;
            this.mrbtnpprint.Text = "Print";
            this.mrbtnpprint.UseVisualStyleBackColor = false;
            this.mrbtnpprint.Visible = false;
            this.mrbtnpprint.Click += new System.EventHandler(this.mrbtnpprint_Click);
            // 
            // btnex009
            // 
            this.btnex009.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnex009.BackColor = System.Drawing.Color.CadetBlue;
            this.btnex009.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnex009.Location = new System.Drawing.Point(435, 519);
            this.btnex009.Name = "btnex009";
            this.btnex009.Size = new System.Drawing.Size(179, 23);
            this.btnex009.TabIndex = 31;
            this.btnex009.Text = "Export M009/M0091";
            this.btnex009.UseVisualStyleBackColor = false;
            this.btnex009.Visible = false;
            this.btnex009.Click += new System.EventHandler(this.btnex009_Click);
            // 
            // MRPlotBtn
            // 
            this.MRPlotBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.MRPlotBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.MRPlotBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.MRPlotBtn.Location = new System.Drawing.Point(636, 519);
            this.MRPlotBtn.Name = "MRPlotBtn";
            this.MRPlotBtn.Size = new System.Drawing.Size(75, 23);
            this.MRPlotBtn.TabIndex = 30;
            this.MRPlotBtn.Text = "Plot";
            this.MRPlotBtn.UseVisualStyleBackColor = false;
            this.MRPlotBtn.Visible = false;
            this.MRPlotBtn.Click += new System.EventHandler(this.MRPlotBtn_Click_1);
            // 
            // MRGB
            // 
            this.MRGB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MRGB.BackColor = System.Drawing.Color.Beige;
            this.MRGB.Controls.Add(this.btnmarketforward);
            this.MRGB.Controls.Add(this.rbrealplant);
            this.MRGB.Controls.Add(this.btnmarketback);
            this.MRGB.Controls.Add(this.rbestimateplant);
            this.MRGB.Controls.Add(this.MRCal);
            this.MRGB.Controls.Add(this.label39);
            this.MRGB.Controls.Add(this.MRCurGrid1);
            this.MRGB.Controls.Add(this.MRCurGrid2);
            this.MRGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MRGB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.MRGB.Location = new System.Drawing.Point(4, 89);
            this.MRGB.Name = "MRGB";
            this.MRGB.Size = new System.Drawing.Size(724, 423);
            this.MRGB.TabIndex = 29;
            this.MRGB.TabStop = false;
            this.MRGB.Text = "CURRENT STATE";
            this.MRGB.Visible = false;
            // 
            // btnmarketforward
            // 
            this.btnmarketforward.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnmarketforward.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnmarketforward.Location = new System.Drawing.Point(654, 12);
            this.btnmarketforward.Name = "btnmarketforward";
            this.btnmarketforward.Size = new System.Drawing.Size(51, 23);
            this.btnmarketforward.TabIndex = 35;
            this.btnmarketforward.Text = ">>>";
            this.btnmarketforward.UseVisualStyleBackColor = true;
            this.btnmarketforward.Click += new System.EventHandler(this.btnmarketforward_Click);
            // 
            // rbrealplant
            // 
            this.rbrealplant.AutoSize = true;
            this.rbrealplant.Checked = true;
            this.rbrealplant.Location = new System.Drawing.Point(331, 18);
            this.rbrealplant.Name = "rbrealplant";
            this.rbrealplant.Size = new System.Drawing.Size(51, 17);
            this.rbrealplant.TabIndex = 34;
            this.rbrealplant.TabStop = true;
            this.rbrealplant.Text = "Real";
            this.rbrealplant.UseVisualStyleBackColor = true;
            this.rbrealplant.CheckedChanged += new System.EventHandler(this.rbrealplant_CheckedChanged);
            // 
            // btnmarketback
            // 
            this.btnmarketback.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnmarketback.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnmarketback.Location = new System.Drawing.Point(654, 12);
            this.btnmarketback.Name = "btnmarketback";
            this.btnmarketback.Size = new System.Drawing.Size(51, 23);
            this.btnmarketback.TabIndex = 35;
            this.btnmarketback.Text = "<<<";
            this.btnmarketback.UseVisualStyleBackColor = true;
            this.btnmarketback.Visible = false;
            this.btnmarketback.Click += new System.EventHandler(this.btnmarketback_Click);
            // 
            // rbestimateplant
            // 
            this.rbestimateplant.AutoSize = true;
            this.rbestimateplant.Location = new System.Drawing.Point(431, 18);
            this.rbestimateplant.Name = "rbestimateplant";
            this.rbestimateplant.Size = new System.Drawing.Size(80, 17);
            this.rbestimateplant.TabIndex = 34;
            this.rbestimateplant.Text = "Estimated";
            this.rbestimateplant.UseVisualStyleBackColor = true;
            // 
            // MRCal
            // 
            this.MRCal.HasButtons = true;
            this.MRCal.Location = new System.Drawing.Point(68, 18);
            this.MRCal.Name = "MRCal";
            this.MRCal.Readonly = true;
            this.MRCal.Size = new System.Drawing.Size(120, 20);
            this.MRCal.TabIndex = 33;
            this.MRCal.ValueChanged += new System.EventHandler(this.MRCal_ValueChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(23, 22);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(42, 13);
            this.label39.TabIndex = 0;
            this.label39.Text = "Date :";
            // 
            // MRCurGrid1
            // 
            dataGridViewCellStyle33.BackColor = System.Drawing.Color.White;
            this.MRCurGrid1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle33;
            this.MRCurGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MRCurGrid1.AutoGenerateColumns = false;
            this.MRCurGrid1.BackgroundColor = System.Drawing.Color.White;
            this.MRCurGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MRCurGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.MRCurGrid1.ColumnHeadersHeight = 22;
            this.MRCurGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ITEM,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12});
            this.MRCurGrid1.DataSource = this.powerPalntDBDataSet4BindingSource;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle35.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.MRCurGrid1.DefaultCellStyle = dataGridViewCellStyle35;
            this.MRCurGrid1.EnableHeadersVisualStyles = false;
            this.MRCurGrid1.Location = new System.Drawing.Point(16, 46);
            this.MRCurGrid1.Name = "MRCurGrid1";
            this.MRCurGrid1.ReadOnly = true;
            this.MRCurGrid1.RowHeadersVisible = false;
            dataGridViewCellStyle36.BackColor = System.Drawing.Color.LemonChiffon;
            this.MRCurGrid1.RowsDefaultCellStyle = dataGridViewCellStyle36;
            this.MRCurGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MRCurGrid1.Size = new System.Drawing.Size(692, 369);
            this.MRCurGrid1.TabIndex = 2;
            // 
            // ITEM
            // 
            this.ITEM.HeaderText = "ITEM";
            this.ITEM.Name = "ITEM";
            this.ITEM.ReadOnly = true;
            this.ITEM.Width = 53;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 53;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 53;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "3";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 53;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "4";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 53;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "5";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 53;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "6";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 53;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "7";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 53;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "8";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 53;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "9";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 53;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "10";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 53;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "11";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 53;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "12";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Width = 53;
            // 
            // MRCurGrid2
            // 
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.White;
            this.MRCurGrid2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle37;
            this.MRCurGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MRCurGrid2.AutoGenerateColumns = false;
            this.MRCurGrid2.BackgroundColor = System.Drawing.Color.White;
            this.MRCurGrid2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MRCurGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this.MRCurGrid2.ColumnHeadersHeight = 22;
            this.MRCurGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13});
            this.MRCurGrid2.DataSource = this.powerPalntDBDataSet4BindingSource;
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle39.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.MRCurGrid2.DefaultCellStyle = dataGridViewCellStyle39;
            this.MRCurGrid2.EnableHeadersVisualStyles = false;
            this.MRCurGrid2.Location = new System.Drawing.Point(16, 46);
            this.MRCurGrid2.Name = "MRCurGrid2";
            this.MRCurGrid2.ReadOnly = true;
            this.MRCurGrid2.RowHeadersVisible = false;
            dataGridViewCellStyle40.BackColor = System.Drawing.Color.LemonChiffon;
            this.MRCurGrid2.RowsDefaultCellStyle = dataGridViewCellStyle40;
            this.MRCurGrid2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MRCurGrid2.Size = new System.Drawing.Size(692, 369);
            this.MRCurGrid2.TabIndex = 3;
            this.MRCurGrid2.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ITEM";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 53;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "13";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 53;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "14";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 53;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "15";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 53;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "16";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 53;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "17";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 53;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "18";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 53;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "19";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 53;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "20";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 53;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "21";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 53;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "22";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 53;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "23";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 53;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "24";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 53;
            // 
            // MRPlantCurGb
            // 
            this.MRPlantCurGb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.MRPlantCurGb.BackColor = System.Drawing.Color.Beige;
            this.MRPlantCurGb.Controls.Add(this.panel34);
            this.MRPlantCurGb.Controls.Add(this.panel35);
            this.MRPlantCurGb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MRPlantCurGb.ForeColor = System.Drawing.SystemColors.WindowText;
            this.MRPlantCurGb.Location = new System.Drawing.Point(245, 15);
            this.MRPlantCurGb.Name = "MRPlantCurGb";
            this.MRPlantCurGb.Size = new System.Drawing.Size(242, 68);
            this.MRPlantCurGb.TabIndex = 27;
            this.MRPlantCurGb.TabStop = false;
            this.MRPlantCurGb.Text = "CURRENT STATE";
            this.MRPlantCurGb.Visible = false;
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.Color.CadetBlue;
            this.panel34.Controls.Add(this.MRPlantPowerTb);
            this.panel34.Controls.Add(this.MRLabel2);
            this.panel34.Location = new System.Drawing.Point(126, 19);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(108, 43);
            this.panel34.TabIndex = 2;
            // 
            // MRPlantPowerTb
            // 
            this.MRPlantPowerTb.BackColor = System.Drawing.Color.White;
            this.MRPlantPowerTb.Location = new System.Drawing.Point(16, 17);
            this.MRPlantPowerTb.Name = "MRPlantPowerTb";
            this.MRPlantPowerTb.ReadOnly = true;
            this.MRPlantPowerTb.Size = new System.Drawing.Size(75, 20);
            this.MRPlantPowerTb.TabIndex = 1;
            this.MRPlantPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MRLabel2
            // 
            this.MRLabel2.AutoSize = true;
            this.MRLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MRLabel2.ForeColor = System.Drawing.SystemColors.Window;
            this.MRLabel2.Location = new System.Drawing.Point(30, 3);
            this.MRLabel2.Name = "MRLabel2";
            this.MRLabel2.Size = new System.Drawing.Size(45, 13);
            this.MRLabel2.TabIndex = 0;
            this.MRLabel2.Text = "POWER";
            // 
            // panel35
            // 
            this.panel35.BackColor = System.Drawing.Color.CadetBlue;
            this.panel35.Controls.Add(this.MRPlantOnUnitTb);
            this.panel35.Controls.Add(this.MRLabel1);
            this.panel35.Location = new System.Drawing.Point(9, 19);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(111, 43);
            this.panel35.TabIndex = 1;
            // 
            // MRPlantOnUnitTb
            // 
            this.MRPlantOnUnitTb.BackColor = System.Drawing.Color.White;
            this.MRPlantOnUnitTb.Location = new System.Drawing.Point(18, 18);
            this.MRPlantOnUnitTb.Name = "MRPlantOnUnitTb";
            this.MRPlantOnUnitTb.ReadOnly = true;
            this.MRPlantOnUnitTb.Size = new System.Drawing.Size(75, 20);
            this.MRPlantOnUnitTb.TabIndex = 1;
            this.MRPlantOnUnitTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MRLabel1
            // 
            this.MRLabel1.AutoSize = true;
            this.MRLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MRLabel1.ForeColor = System.Drawing.SystemColors.Window;
            this.MRLabel1.Location = new System.Drawing.Point(27, 4);
            this.MRLabel1.Name = "MRLabel1";
            this.MRLabel1.Size = new System.Drawing.Size(54, 13);
            this.MRLabel1.TabIndex = 0;
            this.MRLabel1.Text = "ON UNITS";
            // 
            // MRUnitCurGb
            // 
            this.MRUnitCurGb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.MRUnitCurGb.BackColor = System.Drawing.Color.Beige;
            this.MRUnitCurGb.Controls.Add(this.panel37);
            this.MRUnitCurGb.Controls.Add(this.panel36);
            this.MRUnitCurGb.Controls.Add(this.panel72);
            this.MRUnitCurGb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MRUnitCurGb.ForeColor = System.Drawing.SystemColors.WindowText;
            this.MRUnitCurGb.Location = new System.Drawing.Point(202, 15);
            this.MRUnitCurGb.Name = "MRUnitCurGb";
            this.MRUnitCurGb.Size = new System.Drawing.Size(335, 68);
            this.MRUnitCurGb.TabIndex = 28;
            this.MRUnitCurGb.TabStop = false;
            this.MRUnitCurGb.Text = "CURRENT STATE";
            this.MRUnitCurGb.Visible = false;
            // 
            // panel37
            // 
            this.panel37.BackColor = System.Drawing.Color.CadetBlue;
            this.panel37.Controls.Add(this.MRUnitMaxBidTb);
            this.panel37.Controls.Add(this.label36);
            this.panel37.Location = new System.Drawing.Point(232, 19);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(95, 43);
            this.panel37.TabIndex = 3;
            // 
            // MRUnitMaxBidTb
            // 
            this.MRUnitMaxBidTb.BackColor = System.Drawing.Color.White;
            this.MRUnitMaxBidTb.Location = new System.Drawing.Point(9, 18);
            this.MRUnitMaxBidTb.Name = "MRUnitMaxBidTb";
            this.MRUnitMaxBidTb.ReadOnly = true;
            this.MRUnitMaxBidTb.Size = new System.Drawing.Size(75, 20);
            this.MRUnitMaxBidTb.TabIndex = 1;
            this.MRUnitMaxBidTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.SystemColors.Window;
            this.label36.Location = new System.Drawing.Point(21, 4);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(51, 13);
            this.label36.TabIndex = 0;
            this.label36.Text = "MAX BID";
            // 
            // panel36
            // 
            this.panel36.BackColor = System.Drawing.Color.CadetBlue;
            this.panel36.Controls.Add(this.MRUnitPowerTb);
            this.panel36.Controls.Add(this.label24);
            this.panel36.Location = new System.Drawing.Point(118, 19);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(95, 43);
            this.panel36.TabIndex = 2;
            // 
            // MRUnitPowerTb
            // 
            this.MRUnitPowerTb.BackColor = System.Drawing.Color.White;
            this.MRUnitPowerTb.Location = new System.Drawing.Point(9, 18);
            this.MRUnitPowerTb.Name = "MRUnitPowerTb";
            this.MRUnitPowerTb.ReadOnly = true;
            this.MRUnitPowerTb.Size = new System.Drawing.Size(75, 20);
            this.MRUnitPowerTb.TabIndex = 1;
            this.MRUnitPowerTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.SystemColors.Window;
            this.label24.Location = new System.Drawing.Point(21, 4);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "POWER";
            // 
            // panel72
            // 
            this.panel72.BackColor = System.Drawing.Color.CadetBlue;
            this.panel72.Controls.Add(this.MRUnitStateTb);
            this.panel72.Controls.Add(this.label25);
            this.panel72.Location = new System.Drawing.Point(6, 19);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(95, 43);
            this.panel72.TabIndex = 1;
            // 
            // MRUnitStateTb
            // 
            this.MRUnitStateTb.BackColor = System.Drawing.Color.White;
            this.MRUnitStateTb.Location = new System.Drawing.Point(9, 18);
            this.MRUnitStateTb.Name = "MRUnitStateTb";
            this.MRUnitStateTb.ReadOnly = true;
            this.MRUnitStateTb.Size = new System.Drawing.Size(75, 20);
            this.MRUnitStateTb.TabIndex = 1;
            this.MRUnitStateTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.Window;
            this.label25.Location = new System.Drawing.Point(29, 4);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(38, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "STATE";
            // 
            // OperationalData
            // 
            this.OperationalData.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.OperationalData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.OperationalData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.OperationalData.Controls.Add(this.ODHeaderGB);
            this.OperationalData.Controls.Add(this.ODMainPanel);
            this.OperationalData.Location = new System.Drawing.Point(4, 29);
            this.OperationalData.Name = "OperationalData";
            this.OperationalData.Padding = new System.Windows.Forms.Padding(3);
            this.OperationalData.Size = new System.Drawing.Size(757, 625);
            this.OperationalData.TabIndex = 0;
            this.OperationalData.Text = "Operational Data";
            this.OperationalData.UseVisualStyleBackColor = true;
            // 
            // ODHeaderGB
            // 
            this.ODHeaderGB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ODHeaderGB.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ODHeaderGB.BackColor = System.Drawing.Color.Beige;
            this.ODHeaderGB.Controls.Add(this.ODPlantLb);
            this.ODHeaderGB.Controls.Add(this.ODHeaderPanel);
            this.ODHeaderGB.Controls.Add(this.L5);
            this.ODHeaderGB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ODHeaderGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.ODHeaderGB.ForeColor = System.Drawing.Color.Black;
            this.ODHeaderGB.Location = new System.Drawing.Point(15, 11);
            this.ODHeaderGB.Name = "ODHeaderGB";
            this.ODHeaderGB.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ODHeaderGB.Size = new System.Drawing.Size(720, 53);
            this.ODHeaderGB.TabIndex = 20;
            this.ODHeaderGB.TabStop = false;
            this.ODHeaderGB.Visible = false;
            // 
            // ODPlantLb
            // 
            this.ODPlantLb.AutoSize = true;
            this.ODPlantLb.Location = new System.Drawing.Point(62, 23);
            this.ODPlantLb.Name = "ODPlantLb";
            this.ODPlantLb.Size = new System.Drawing.Size(0, 13);
            this.ODPlantLb.TabIndex = 9;
            // 
            // ODHeaderPanel
            // 
            this.ODHeaderPanel.Controls.Add(this.ODTypeLb);
            this.ODHeaderPanel.Controls.Add(this.ODUnitLb);
            this.ODHeaderPanel.Controls.Add(this.ODPackLb);
            this.ODHeaderPanel.Controls.Add(this.L8);
            this.ODHeaderPanel.Controls.Add(this.L7);
            this.ODHeaderPanel.Controls.Add(this.L6);
            this.ODHeaderPanel.Location = new System.Drawing.Point(144, 12);
            this.ODHeaderPanel.Name = "ODHeaderPanel";
            this.ODHeaderPanel.Size = new System.Drawing.Size(550, 35);
            this.ODHeaderPanel.TabIndex = 8;
            this.ODHeaderPanel.Visible = false;
            // 
            // ODTypeLb
            // 
            this.ODTypeLb.AutoSize = true;
            this.ODTypeLb.Location = new System.Drawing.Point(445, 11);
            this.ODTypeLb.Name = "ODTypeLb";
            this.ODTypeLb.Size = new System.Drawing.Size(0, 13);
            this.ODTypeLb.TabIndex = 13;
            // 
            // ODUnitLb
            // 
            this.ODUnitLb.AutoSize = true;
            this.ODUnitLb.Location = new System.Drawing.Point(283, 11);
            this.ODUnitLb.Name = "ODUnitLb";
            this.ODUnitLb.Size = new System.Drawing.Size(0, 13);
            this.ODUnitLb.TabIndex = 12;
            // 
            // ODPackLb
            // 
            this.ODPackLb.AutoSize = true;
            this.ODPackLb.Location = new System.Drawing.Point(129, 11);
            this.ODPackLb.Name = "ODPackLb";
            this.ODPackLb.Size = new System.Drawing.Size(0, 13);
            this.ODPackLb.TabIndex = 11;
            // 
            // L8
            // 
            this.L8.AutoSize = true;
            this.L8.BackColor = System.Drawing.Color.Transparent;
            this.L8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L8.Location = new System.Drawing.Point(402, 11);
            this.L8.Name = "L8";
            this.L8.Size = new System.Drawing.Size(47, 13);
            this.L8.TabIndex = 10;
            this.L8.Text = "Type : ";
            // 
            // L7
            // 
            this.L7.AutoSize = true;
            this.L7.BackColor = System.Drawing.Color.Transparent;
            this.L7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L7.Location = new System.Drawing.Point(234, 11);
            this.L7.Name = "L7";
            this.L7.Size = new System.Drawing.Size(42, 13);
            this.L7.TabIndex = 9;
            this.L7.Text = "Unit : ";
            // 
            // L6
            // 
            this.L6.AutoSize = true;
            this.L6.BackColor = System.Drawing.Color.Transparent;
            this.L6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L6.Location = new System.Drawing.Point(45, 11);
            this.L6.Name = "L6";
            this.L6.Size = new System.Drawing.Size(69, 13);
            this.L6.TabIndex = 8;
            this.L6.Text = "Package : ";
            // 
            // L5
            // 
            this.L5.AutoSize = true;
            this.L5.BackColor = System.Drawing.Color.Transparent;
            this.L5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L5.Location = new System.Drawing.Point(10, 23);
            this.L5.Name = "L5";
            this.L5.Size = new System.Drawing.Size(48, 13);
            this.L5.TabIndex = 7;
            this.L5.Text = "Plant : ";
            this.L5.Visible = false;
            // 
            // ODMainPanel
            // 
            this.ODMainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ODMainPanel.Controls.Add(this.panel94);
            this.ODMainPanel.Controls.Add(this.ODUnitPanel);
            this.ODMainPanel.Controls.Add(this.panel97);
            this.ODMainPanel.Location = new System.Drawing.Point(3, 66);
            this.ODMainPanel.Name = "ODMainPanel";
            this.ODMainPanel.Size = new System.Drawing.Size(746, 554);
            this.ODMainPanel.TabIndex = 21;
            // 
            // panel94
            // 
            this.panel94.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel94.BackColor = System.Drawing.Color.Beige;
            this.panel94.Controls.Add(this.label116);
            this.panel94.Controls.Add(this.label114);
            this.panel94.Controls.Add(this.faDatePickDispatch);
            this.panel94.Controls.Add(this.dataGridDispatch);
            this.panel94.Location = new System.Drawing.Point(42, 26);
            this.panel94.Name = "panel94";
            this.panel94.Size = new System.Drawing.Size(663, 483);
            this.panel94.TabIndex = 30;
            this.panel94.Visible = false;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label116.Location = new System.Drawing.Point(11, 15);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(42, 13);
            this.label116.TabIndex = 36;
            this.label116.Text = "Date :";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label114.Location = new System.Drawing.Point(62, 459);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(0, 13);
            this.label114.TabIndex = 35;
            // 
            // faDatePickDispatch
            // 
            this.faDatePickDispatch.HasButtons = true;
            this.faDatePickDispatch.Location = new System.Drawing.Point(59, 12);
            this.faDatePickDispatch.Name = "faDatePickDispatch";
            this.faDatePickDispatch.Readonly = true;
            this.faDatePickDispatch.Size = new System.Drawing.Size(120, 20);
            this.faDatePickDispatch.TabIndex = 34;
            this.faDatePickDispatch.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.faDatePickDispatch.ValueChanged += new System.EventHandler(this.faDatePickDispatch_ValueChanged);
            // 
            // dataGridDispatch
            // 
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.White;
            this.dataGridDispatch.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridDispatch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridDispatch.BackgroundColor = System.Drawing.Color.White;
            this.dataGridDispatch.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridDispatch.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridDispatch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridDispatch.ColumnHeadersHeight = 22;
            this.dataGridDispatch.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Hour,
            this.Block,
            this.Packagetype,
            this.DeclaredCapacity,
            this.DispachableCapacity});
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridDispatch.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridDispatch.EnableHeadersVisualStyles = false;
            this.dataGridDispatch.Location = new System.Drawing.Point(11, 45);
            this.dataGridDispatch.Name = "dataGridDispatch";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridDispatch.RowHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridDispatch.RowHeadersVisible = false;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.LemonChiffon;
            this.dataGridDispatch.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridDispatch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridDispatch.Size = new System.Drawing.Size(640, 401);
            this.dataGridDispatch.TabIndex = 33;
            // 
            // Hour
            // 
            this.Hour.HeaderText = "Hour";
            this.Hour.Name = "Hour";
            // 
            // Block
            // 
            this.Block.HeaderText = "Block";
            this.Block.Name = "Block";
            // 
            // Packagetype
            // 
            this.Packagetype.HeaderText = "Packagetype";
            this.Packagetype.Name = "Packagetype";
            // 
            // DeclaredCapacity
            // 
            this.DeclaredCapacity.HeaderText = "DeclaredCapacity";
            this.DeclaredCapacity.Name = "DeclaredCapacity";
            // 
            // DispachableCapacity
            // 
            this.DispachableCapacity.HeaderText = "DispachableCapacity";
            this.DispachableCapacity.Name = "DispachableCapacity";
            // 
            // ODUnitPanel
            // 
            this.ODUnitPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ODUnitPanel.Controls.Add(this.ODUnitMainGB);
            this.ODUnitPanel.Controls.Add(this.ODUnitPowerGB);
            this.ODUnitPanel.Controls.Add(this.ODUnitServiceGB);
            this.ODUnitPanel.Controls.Add(this.ODUnitFuelGB);
            this.ODUnitPanel.Controls.Add(this.groupBox4);
            this.ODUnitPanel.Location = new System.Drawing.Point(101, 363);
            this.ODUnitPanel.Name = "ODUnitPanel";
            this.ODUnitPanel.Size = new System.Drawing.Size(169, 50);
            this.ODUnitPanel.TabIndex = 29;
            this.ODUnitPanel.Visible = false;
            // 
            // ODUnitMainGB
            // 
            this.ODUnitMainGB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ODUnitMainGB.BackColor = System.Drawing.Color.Beige;
            this.ODUnitMainGB.Controls.Add(this.linkmaintenance);
            this.ODUnitMainGB.Controls.Add(this.panel17);
            this.ODUnitMainGB.Controls.Add(this.odunitmd2Valid);
            this.ODUnitMainGB.Controls.Add(this.odunitmd1Valid);
            this.ODUnitMainGB.Controls.Add(this.panel32);
            this.ODUnitMainGB.Controls.Add(this.panel33);
            this.ODUnitMainGB.Controls.Add(this.ODUnitMainCheck);
            this.ODUnitMainGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ODUnitMainGB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ODUnitMainGB.Location = new System.Drawing.Point(-274, 103);
            this.ODUnitMainGB.Name = "ODUnitMainGB";
            this.ODUnitMainGB.Size = new System.Drawing.Size(716, 81);
            this.ODUnitMainGB.TabIndex = 3;
            this.ODUnitMainGB.TabStop = false;
            this.ODUnitMainGB.Text = "MAINTENANCE";
            // 
            // linkmaintenance
            // 
            this.linkmaintenance.AutoSize = true;
            this.linkmaintenance.LinkColor = System.Drawing.Color.SteelBlue;
            this.linkmaintenance.Location = new System.Drawing.Point(55, 57);
            this.linkmaintenance.Name = "linkmaintenance";
            this.linkmaintenance.Size = new System.Drawing.Size(29, 13);
            this.linkmaintenance.TabIndex = 13;
            this.linkmaintenance.TabStop = true;
            this.linkmaintenance.Text = "Edit";
            this.linkmaintenance.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkmaintenance_LinkClicked);
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.CadetBlue;
            this.panel17.Controls.Add(this.ODMaintenanceType);
            this.panel17.Controls.Add(this.label26);
            this.panel17.Location = new System.Drawing.Point(580, 22);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(113, 49);
            this.panel17.TabIndex = 8;
            // 
            // ODMaintenanceType
            // 
            this.ODMaintenanceType.Enabled = false;
            this.ODMaintenanceType.FormattingEnabled = true;
            this.ODMaintenanceType.Location = new System.Drawing.Point(6, 22);
            this.ODMaintenanceType.Name = "ODMaintenanceType";
            this.ODMaintenanceType.Size = new System.Drawing.Size(101, 21);
            this.ODMaintenanceType.TabIndex = 1;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.Window;
            this.label26.Location = new System.Drawing.Point(34, 4);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(39, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "TYPE";
            // 
            // odunitmd2Valid
            // 
            this.odunitmd2Valid.AutoSize = true;
            this.odunitmd2Valid.BackColor = System.Drawing.Color.Transparent;
            this.odunitmd2Valid.ForeColor = System.Drawing.Color.Red;
            this.odunitmd2Valid.Location = new System.Drawing.Point(338, 48);
            this.odunitmd2Valid.Name = "odunitmd2Valid";
            this.odunitmd2Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.odunitmd2Valid.Size = new System.Drawing.Size(12, 13);
            this.odunitmd2Valid.TabIndex = 7;
            this.odunitmd2Valid.Text = "*";
            this.odunitmd2Valid.Visible = false;
            // 
            // odunitmd1Valid
            // 
            this.odunitmd1Valid.AutoSize = true;
            this.odunitmd1Valid.BackColor = System.Drawing.Color.Transparent;
            this.odunitmd1Valid.ForeColor = System.Drawing.Color.Red;
            this.odunitmd1Valid.Location = new System.Drawing.Point(133, 48);
            this.odunitmd1Valid.Name = "odunitmd1Valid";
            this.odunitmd1Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.odunitmd1Valid.Size = new System.Drawing.Size(12, 13);
            this.odunitmd1Valid.TabIndex = 6;
            this.odunitmd1Valid.Text = "*";
            this.odunitmd1Valid.Visible = false;
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.CadetBlue;
            this.panel32.Controls.Add(this.ODMaintenanceEndHour);
            this.panel32.Controls.Add(this.label4);
            this.panel32.Controls.Add(this.ODUnitMainEndDate);
            this.panel32.Controls.Add(this.label37);
            this.panel32.Location = new System.Drawing.Point(367, 22);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(179, 49);
            this.panel32.TabIndex = 4;
            // 
            // ODMaintenanceEndHour
            // 
            this.ODMaintenanceEndHour.Enabled = false;
            this.ODMaintenanceEndHour.Location = new System.Drawing.Point(133, 21);
            this.ODMaintenanceEndHour.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.ODMaintenanceEndHour.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ODMaintenanceEndHour.Name = "ODMaintenanceEndHour";
            this.ODMaintenanceEndHour.Size = new System.Drawing.Size(39, 20);
            this.ODMaintenanceEndHour.TabIndex = 10;
            this.ODMaintenanceEndHour.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.Window;
            this.label4.Location = new System.Drawing.Point(130, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "HOUR";
            // 
            // ODUnitMainEndDate
            // 
            this.ODUnitMainEndDate.Enabled = false;
            this.ODUnitMainEndDate.HasButtons = true;
            this.ODUnitMainEndDate.Location = new System.Drawing.Point(3, 20);
            this.ODUnitMainEndDate.Name = "ODUnitMainEndDate";
            this.ODUnitMainEndDate.Readonly = true;
            this.ODUnitMainEndDate.Size = new System.Drawing.Size(120, 20);
            this.ODUnitMainEndDate.TabIndex = 8;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.ForeColor = System.Drawing.SystemColors.Window;
            this.label37.Location = new System.Drawing.Point(30, 4);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(70, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "END DATE";
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.CadetBlue;
            this.panel33.Controls.Add(this.ODMaintenanceStartHour);
            this.panel33.Controls.Add(this.label3);
            this.panel33.Controls.Add(this.ODUnitMainStartDate);
            this.panel33.Controls.Add(this.label38);
            this.panel33.Location = new System.Drawing.Point(151, 22);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(179, 49);
            this.panel33.TabIndex = 3;
            // 
            // ODMaintenanceStartHour
            // 
            this.ODMaintenanceStartHour.Enabled = false;
            this.ODMaintenanceStartHour.Location = new System.Drawing.Point(133, 23);
            this.ODMaintenanceStartHour.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.ODMaintenanceStartHour.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ODMaintenanceStartHour.Name = "ODMaintenanceStartHour";
            this.ODMaintenanceStartHour.Size = new System.Drawing.Size(39, 20);
            this.ODMaintenanceStartHour.TabIndex = 9;
            this.ODMaintenanceStartHour.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.Window;
            this.label3.Location = new System.Drawing.Point(130, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "HOUR";
            // 
            // ODUnitMainStartDate
            // 
            this.ODUnitMainStartDate.Enabled = false;
            this.ODUnitMainStartDate.HasButtons = true;
            this.ODUnitMainStartDate.Location = new System.Drawing.Point(3, 21);
            this.ODUnitMainStartDate.Name = "ODUnitMainStartDate";
            this.ODUnitMainStartDate.Readonly = true;
            this.ODUnitMainStartDate.Size = new System.Drawing.Size(120, 20);
            this.ODUnitMainStartDate.TabIndex = 7;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.ForeColor = System.Drawing.SystemColors.Window;
            this.label38.Location = new System.Drawing.Point(24, 4);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(85, 13);
            this.label38.TabIndex = 0;
            this.label38.Text = "START DATE";
            // 
            // ODUnitMainCheck
            // 
            this.ODUnitMainCheck.AutoSize = true;
            this.ODUnitMainCheck.Location = new System.Drawing.Point(17, 33);
            this.ODUnitMainCheck.Name = "ODUnitMainCheck";
            this.ODUnitMainCheck.Size = new System.Drawing.Size(115, 17);
            this.ODUnitMainCheck.TabIndex = 6;
            this.ODUnitMainCheck.Text = "MAINTENANCE";
            this.ODUnitMainCheck.UseVisualStyleBackColor = true;
            this.ODUnitMainCheck.CheckedChanged += new System.EventHandler(this.ODUnitMainCheck_CheckedChanged);
            // 
            // ODUnitPowerGB
            // 
            this.ODUnitPowerGB.BackColor = System.Drawing.Color.Beige;
            this.ODUnitPowerGB.Controls.Add(this.btnsavedispach);
            this.ODUnitPowerGB.Controls.Add(this.textupdatetime);
            this.ODUnitPowerGB.Controls.Add(this.labelupdatetime);
            this.ODUnitPowerGB.Controls.Add(this.odunitpd1Valid);
            this.ODUnitPowerGB.Controls.Add(this.ODUnitPowerGrid2);
            this.ODUnitPowerGB.Controls.Add(this.ODUnitPowerGrid1);
            this.ODUnitPowerGB.Controls.Add(this.panel26);
            this.ODUnitPowerGB.Controls.Add(this.ODUnitPowerCheck);
            this.ODUnitPowerGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ODUnitPowerGB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ODUnitPowerGB.Location = new System.Drawing.Point(12, 277);
            this.ODUnitPowerGB.Name = "ODUnitPowerGB";
            this.ODUnitPowerGB.Size = new System.Drawing.Size(713, 236);
            this.ODUnitPowerGB.TabIndex = 2;
            this.ODUnitPowerGB.TabStop = false;
            this.ODUnitPowerGB.Text = "DISPATCHABLE";
            // 
            // btnsavedispach
            // 
            this.btnsavedispach.BackColor = System.Drawing.Color.CadetBlue;
            this.btnsavedispach.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsavedispach.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsavedispach.Location = new System.Drawing.Point(578, 197);
            this.btnsavedispach.Name = "btnsavedispach";
            this.btnsavedispach.Size = new System.Drawing.Size(75, 23);
            this.btnsavedispach.TabIndex = 18;
            this.btnsavedispach.Text = "Save";
            this.btnsavedispach.UseVisualStyleBackColor = false;
            this.btnsavedispach.Click += new System.EventHandler(this.btnsavedispach_Click);
            // 
            // textupdatetime
            // 
            this.textupdatetime.Location = new System.Drawing.Point(201, 197);
            this.textupdatetime.Name = "textupdatetime";
            this.textupdatetime.ReadOnly = true;
            this.textupdatetime.Size = new System.Drawing.Size(55, 20);
            this.textupdatetime.TabIndex = 17;
            this.textupdatetime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelupdatetime
            // 
            this.labelupdatetime.AutoSize = true;
            this.labelupdatetime.Location = new System.Drawing.Point(275, 201);
            this.labelupdatetime.Name = "labelupdatetime";
            this.labelupdatetime.Size = new System.Drawing.Size(0, 13);
            this.labelupdatetime.TabIndex = 16;
            // 
            // odunitpd1Valid
            // 
            this.odunitpd1Valid.AutoSize = true;
            this.odunitpd1Valid.BackColor = System.Drawing.Color.Azure;
            this.odunitpd1Valid.ForeColor = System.Drawing.Color.Red;
            this.odunitpd1Valid.Location = new System.Drawing.Point(25, 210);
            this.odunitpd1Valid.Name = "odunitpd1Valid";
            this.odunitpd1Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.odunitpd1Valid.Size = new System.Drawing.Size(12, 13);
            this.odunitpd1Valid.TabIndex = 9;
            this.odunitpd1Valid.Text = "*";
            this.odunitpd1Valid.Visible = false;
            // 
            // ODUnitPowerGrid2
            // 
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            this.ODUnitPowerGrid2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle25;
            this.ODUnitPowerGrid2.BackgroundColor = System.Drawing.Color.White;
            this.ODUnitPowerGrid2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.ODUnitPowerGrid2.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ODUnitPowerGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.ODUnitPowerGrid2.ColumnHeadersHeight = 22;
            this.ODUnitPowerGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn55,
            this.dataGridViewTextBoxColumn56,
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn58,
            this.dataGridViewTextBoxColumn59,
            this.dataGridViewTextBoxColumn60,
            this.dataGridViewTextBoxColumn61,
            this.dataGridViewTextBoxColumn62,
            this.dataGridViewTextBoxColumn63,
            this.dataGridViewTextBoxColumn64,
            this.dataGridViewTextBoxColumn65,
            this.dataGridViewTextBoxColumn66});
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ODUnitPowerGrid2.DefaultCellStyle = dataGridViewCellStyle27;
            this.ODUnitPowerGrid2.Enabled = false;
            this.ODUnitPowerGrid2.EnableHeadersVisualStyles = false;
            this.ODUnitPowerGrid2.Location = new System.Drawing.Point(18, 113);
            this.ODUnitPowerGrid2.Name = "ODUnitPowerGrid2";
            this.ODUnitPowerGrid2.RowHeadersVisible = false;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.LemonChiffon;
            this.ODUnitPowerGrid2.RowsDefaultCellStyle = dataGridViewCellStyle28;
            this.ODUnitPowerGrid2.RowTemplate.Height = 30;
            this.ODUnitPowerGrid2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ODUnitPowerGrid2.Size = new System.Drawing.Size(663, 63);
            this.ODUnitPowerGrid2.TabIndex = 15;
            this.ODUnitPowerGrid2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ODUnitPowerGrid2_KeyDown);
            this.ODUnitPowerGrid2.Validated += new System.EventHandler(this.ODUnitPowerGrid2_Validated);
            // 
            // dataGridViewTextBoxColumn55
            // 
            this.dataGridViewTextBoxColumn55.HeaderText = "13";
            this.dataGridViewTextBoxColumn55.Name = "dataGridViewTextBoxColumn55";
            this.dataGridViewTextBoxColumn55.Width = 55;
            // 
            // dataGridViewTextBoxColumn56
            // 
            this.dataGridViewTextBoxColumn56.HeaderText = "14";
            this.dataGridViewTextBoxColumn56.Name = "dataGridViewTextBoxColumn56";
            this.dataGridViewTextBoxColumn56.Width = 55;
            // 
            // dataGridViewTextBoxColumn57
            // 
            this.dataGridViewTextBoxColumn57.HeaderText = "15";
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            this.dataGridViewTextBoxColumn57.Width = 55;
            // 
            // dataGridViewTextBoxColumn58
            // 
            this.dataGridViewTextBoxColumn58.HeaderText = "16";
            this.dataGridViewTextBoxColumn58.Name = "dataGridViewTextBoxColumn58";
            this.dataGridViewTextBoxColumn58.Width = 55;
            // 
            // dataGridViewTextBoxColumn59
            // 
            this.dataGridViewTextBoxColumn59.HeaderText = "17";
            this.dataGridViewTextBoxColumn59.Name = "dataGridViewTextBoxColumn59";
            this.dataGridViewTextBoxColumn59.Width = 55;
            // 
            // dataGridViewTextBoxColumn60
            // 
            this.dataGridViewTextBoxColumn60.HeaderText = "18";
            this.dataGridViewTextBoxColumn60.Name = "dataGridViewTextBoxColumn60";
            this.dataGridViewTextBoxColumn60.Width = 55;
            // 
            // dataGridViewTextBoxColumn61
            // 
            this.dataGridViewTextBoxColumn61.HeaderText = "19";
            this.dataGridViewTextBoxColumn61.Name = "dataGridViewTextBoxColumn61";
            this.dataGridViewTextBoxColumn61.Width = 55;
            // 
            // dataGridViewTextBoxColumn62
            // 
            this.dataGridViewTextBoxColumn62.HeaderText = "20";
            this.dataGridViewTextBoxColumn62.Name = "dataGridViewTextBoxColumn62";
            this.dataGridViewTextBoxColumn62.Width = 55;
            // 
            // dataGridViewTextBoxColumn63
            // 
            this.dataGridViewTextBoxColumn63.HeaderText = "21";
            this.dataGridViewTextBoxColumn63.Name = "dataGridViewTextBoxColumn63";
            this.dataGridViewTextBoxColumn63.Width = 55;
            // 
            // dataGridViewTextBoxColumn64
            // 
            this.dataGridViewTextBoxColumn64.HeaderText = "22";
            this.dataGridViewTextBoxColumn64.Name = "dataGridViewTextBoxColumn64";
            this.dataGridViewTextBoxColumn64.Width = 55;
            // 
            // dataGridViewTextBoxColumn65
            // 
            this.dataGridViewTextBoxColumn65.HeaderText = "23";
            this.dataGridViewTextBoxColumn65.Name = "dataGridViewTextBoxColumn65";
            this.dataGridViewTextBoxColumn65.Width = 55;
            // 
            // dataGridViewTextBoxColumn66
            // 
            this.dataGridViewTextBoxColumn66.HeaderText = "24";
            this.dataGridViewTextBoxColumn66.Name = "dataGridViewTextBoxColumn66";
            this.dataGridViewTextBoxColumn66.Width = 55;
            // 
            // ODUnitPowerGrid1
            // 
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.White;
            this.ODUnitPowerGrid1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle29;
            this.ODUnitPowerGrid1.BackgroundColor = System.Drawing.Color.White;
            this.ODUnitPowerGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.ODUnitPowerGrid1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ODUnitPowerGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.ODUnitPowerGrid1.ColumnHeadersHeight = 22;
            this.ODUnitPowerGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn67,
            this.dataGridViewTextBoxColumn68,
            this.dataGridViewTextBoxColumn69,
            this.dataGridViewTextBoxColumn70,
            this.dataGridViewTextBoxColumn71,
            this.dataGridViewTextBoxColumn72,
            this.dataGridViewTextBoxColumn73,
            this.dataGridViewTextBoxColumn74,
            this.dataGridViewTextBoxColumn75,
            this.dataGridViewTextBoxColumn76,
            this.dataGridViewTextBoxColumn77,
            this.dataGridViewTextBoxColumn78});
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ODUnitPowerGrid1.DefaultCellStyle = dataGridViewCellStyle31;
            this.ODUnitPowerGrid1.Enabled = false;
            this.ODUnitPowerGrid1.EnableHeadersVisualStyles = false;
            this.ODUnitPowerGrid1.Location = new System.Drawing.Point(19, 46);
            this.ODUnitPowerGrid1.Name = "ODUnitPowerGrid1";
            this.ODUnitPowerGrid1.RowHeadersVisible = false;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.LemonChiffon;
            this.ODUnitPowerGrid1.RowsDefaultCellStyle = dataGridViewCellStyle32;
            this.ODUnitPowerGrid1.RowTemplate.Height = 30;
            this.ODUnitPowerGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ODUnitPowerGrid1.Size = new System.Drawing.Size(663, 63);
            this.ODUnitPowerGrid1.TabIndex = 14;
            this.ODUnitPowerGrid1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ODUnitPowerGrid1_KeyDown);
            this.ODUnitPowerGrid1.Validated += new System.EventHandler(this.ODUnitPowerGrid1_Validated);
            // 
            // dataGridViewTextBoxColumn67
            // 
            this.dataGridViewTextBoxColumn67.HeaderText = "1";
            this.dataGridViewTextBoxColumn67.Name = "dataGridViewTextBoxColumn67";
            this.dataGridViewTextBoxColumn67.Width = 55;
            // 
            // dataGridViewTextBoxColumn68
            // 
            this.dataGridViewTextBoxColumn68.HeaderText = "2";
            this.dataGridViewTextBoxColumn68.Name = "dataGridViewTextBoxColumn68";
            this.dataGridViewTextBoxColumn68.Width = 55;
            // 
            // dataGridViewTextBoxColumn69
            // 
            this.dataGridViewTextBoxColumn69.HeaderText = "3";
            this.dataGridViewTextBoxColumn69.Name = "dataGridViewTextBoxColumn69";
            this.dataGridViewTextBoxColumn69.Width = 55;
            // 
            // dataGridViewTextBoxColumn70
            // 
            this.dataGridViewTextBoxColumn70.HeaderText = "4";
            this.dataGridViewTextBoxColumn70.Name = "dataGridViewTextBoxColumn70";
            this.dataGridViewTextBoxColumn70.Width = 55;
            // 
            // dataGridViewTextBoxColumn71
            // 
            this.dataGridViewTextBoxColumn71.HeaderText = "5";
            this.dataGridViewTextBoxColumn71.Name = "dataGridViewTextBoxColumn71";
            this.dataGridViewTextBoxColumn71.Width = 55;
            // 
            // dataGridViewTextBoxColumn72
            // 
            this.dataGridViewTextBoxColumn72.HeaderText = "6";
            this.dataGridViewTextBoxColumn72.Name = "dataGridViewTextBoxColumn72";
            this.dataGridViewTextBoxColumn72.Width = 55;
            // 
            // dataGridViewTextBoxColumn73
            // 
            this.dataGridViewTextBoxColumn73.HeaderText = "7";
            this.dataGridViewTextBoxColumn73.Name = "dataGridViewTextBoxColumn73";
            this.dataGridViewTextBoxColumn73.Width = 55;
            // 
            // dataGridViewTextBoxColumn74
            // 
            this.dataGridViewTextBoxColumn74.HeaderText = "8";
            this.dataGridViewTextBoxColumn74.Name = "dataGridViewTextBoxColumn74";
            this.dataGridViewTextBoxColumn74.Width = 55;
            // 
            // dataGridViewTextBoxColumn75
            // 
            this.dataGridViewTextBoxColumn75.HeaderText = "9";
            this.dataGridViewTextBoxColumn75.Name = "dataGridViewTextBoxColumn75";
            this.dataGridViewTextBoxColumn75.Width = 55;
            // 
            // dataGridViewTextBoxColumn76
            // 
            this.dataGridViewTextBoxColumn76.HeaderText = "10";
            this.dataGridViewTextBoxColumn76.Name = "dataGridViewTextBoxColumn76";
            this.dataGridViewTextBoxColumn76.Width = 55;
            // 
            // dataGridViewTextBoxColumn77
            // 
            this.dataGridViewTextBoxColumn77.HeaderText = "11";
            this.dataGridViewTextBoxColumn77.Name = "dataGridViewTextBoxColumn77";
            this.dataGridViewTextBoxColumn77.Width = 55;
            // 
            // dataGridViewTextBoxColumn78
            // 
            this.dataGridViewTextBoxColumn78.HeaderText = "12";
            this.dataGridViewTextBoxColumn78.Name = "dataGridViewTextBoxColumn78";
            this.dataGridViewTextBoxColumn78.Width = 55;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.CadetBlue;
            this.panel26.Controls.Add(this.ODUnitPowerStartDate);
            this.panel26.Controls.Add(this.label30);
            this.panel26.Location = new System.Drawing.Point(48, 182);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(131, 49);
            this.panel26.TabIndex = 3;
            // 
            // ODUnitPowerStartDate
            // 
            this.ODUnitPowerStartDate.Enabled = false;
            this.ODUnitPowerStartDate.HasButtons = true;
            this.ODUnitPowerStartDate.Location = new System.Drawing.Point(3, 21);
            this.ODUnitPowerStartDate.Name = "ODUnitPowerStartDate";
            this.ODUnitPowerStartDate.Readonly = true;
            this.ODUnitPowerStartDate.Size = new System.Drawing.Size(120, 20);
            this.ODUnitPowerStartDate.TabIndex = 16;
            this.ODUnitPowerStartDate.ValueChanged += new System.EventHandler(this.ODUnitPowerStartDate_ValueChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.SystemColors.Window;
            this.label30.Location = new System.Drawing.Point(37, 4);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(44, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = " DATE";
            // 
            // ODUnitPowerCheck
            // 
            this.ODUnitPowerCheck.AutoSize = true;
            this.ODUnitPowerCheck.Location = new System.Drawing.Point(17, 22);
            this.ODUnitPowerCheck.Name = "ODUnitPowerCheck";
            this.ODUnitPowerCheck.Size = new System.Drawing.Size(119, 17);
            this.ODUnitPowerCheck.TabIndex = 13;
            this.ODUnitPowerCheck.Text = "DISPATCHABLE";
            this.ODUnitPowerCheck.UseVisualStyleBackColor = true;
            this.ODUnitPowerCheck.CheckedChanged += new System.EventHandler(this.ODUnitPowerCheck_CheckedChanged);
            // 
            // ODUnitServiceGB
            // 
            this.ODUnitServiceGB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ODUnitServiceGB.BackColor = System.Drawing.Color.Beige;
            this.ODUnitServiceGB.Controls.Add(this.linkoutservice);
            this.ODUnitServiceGB.Controls.Add(this.odunitosd2Valid);
            this.ODUnitServiceGB.Controls.Add(this.odunitosd1Valid);
            this.ODUnitServiceGB.Controls.Add(this.panel30);
            this.ODUnitServiceGB.Controls.Add(this.panel31);
            this.ODUnitServiceGB.Controls.Add(this.ODUnitOutCheck);
            this.ODUnitServiceGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ODUnitServiceGB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ODUnitServiceGB.Location = new System.Drawing.Point(-273, 6);
            this.ODUnitServiceGB.Name = "ODUnitServiceGB";
            this.ODUnitServiceGB.Size = new System.Drawing.Size(543, 92);
            this.ODUnitServiceGB.TabIndex = 0;
            this.ODUnitServiceGB.TabStop = false;
            this.ODUnitServiceGB.Text = "SERVICE";
            // 
            // linkoutservice
            // 
            this.linkoutservice.AutoSize = true;
            this.linkoutservice.LinkColor = System.Drawing.Color.SteelBlue;
            this.linkoutservice.Location = new System.Drawing.Point(54, 69);
            this.linkoutservice.Name = "linkoutservice";
            this.linkoutservice.Size = new System.Drawing.Size(33, 13);
            this.linkoutservice.TabIndex = 12;
            this.linkoutservice.TabStop = true;
            this.linkoutservice.Text = "Edit ";
            this.linkoutservice.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkoutservice_LinkClicked);
            // 
            // odunitosd2Valid
            // 
            this.odunitosd2Valid.AutoSize = true;
            this.odunitosd2Valid.BackColor = System.Drawing.Color.Transparent;
            this.odunitosd2Valid.ForeColor = System.Drawing.Color.Red;
            this.odunitosd2Valid.Location = new System.Drawing.Point(334, 61);
            this.odunitosd2Valid.Name = "odunitosd2Valid";
            this.odunitosd2Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.odunitosd2Valid.Size = new System.Drawing.Size(12, 13);
            this.odunitosd2Valid.TabIndex = 6;
            this.odunitosd2Valid.Text = "*";
            this.odunitosd2Valid.Visible = false;
            // 
            // odunitosd1Valid
            // 
            this.odunitosd1Valid.AutoSize = true;
            this.odunitosd1Valid.BackColor = System.Drawing.Color.Transparent;
            this.odunitosd1Valid.ForeColor = System.Drawing.Color.Red;
            this.odunitosd1Valid.Location = new System.Drawing.Point(131, 61);
            this.odunitosd1Valid.Name = "odunitosd1Valid";
            this.odunitosd1Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.odunitosd1Valid.Size = new System.Drawing.Size(12, 13);
            this.odunitosd1Valid.TabIndex = 5;
            this.odunitosd1Valid.Text = "*";
            this.odunitosd1Valid.Visible = false;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.CadetBlue;
            this.panel30.Controls.Add(this.ODOutServiceEndHour);
            this.panel30.Controls.Add(this.label2);
            this.panel30.Controls.Add(this.ODUnitServiceEndDate);
            this.panel30.Controls.Add(this.label34);
            this.panel30.Location = new System.Drawing.Point(352, 33);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(179, 49);
            this.panel30.TabIndex = 4;
            // 
            // ODOutServiceEndHour
            // 
            this.ODOutServiceEndHour.Enabled = false;
            this.ODOutServiceEndHour.Location = new System.Drawing.Point(135, 23);
            this.ODOutServiceEndHour.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.ODOutServiceEndHour.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ODOutServiceEndHour.Name = "ODOutServiceEndHour";
            this.ODOutServiceEndHour.Size = new System.Drawing.Size(39, 20);
            this.ODOutServiceEndHour.TabIndex = 8;
            this.ODOutServiceEndHour.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.Window;
            this.label2.Location = new System.Drawing.Point(132, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "HOUR";
            // 
            // ODUnitServiceEndDate
            // 
            this.ODUnitServiceEndDate.Enabled = false;
            this.ODUnitServiceEndDate.HasButtons = true;
            this.ODUnitServiceEndDate.Location = new System.Drawing.Point(3, 21);
            this.ODUnitServiceEndDate.Name = "ODUnitServiceEndDate";
            this.ODUnitServiceEndDate.Readonly = true;
            this.ODUnitServiceEndDate.Size = new System.Drawing.Size(120, 20);
            this.ODUnitServiceEndDate.TabIndex = 5;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.ForeColor = System.Drawing.SystemColors.Window;
            this.label34.Location = new System.Drawing.Point(27, 4);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(70, 13);
            this.label34.TabIndex = 0;
            this.label34.Text = "END DATE";
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.CadetBlue;
            this.panel31.Controls.Add(this.ODOutServiceStartHour);
            this.panel31.Controls.Add(this.label1);
            this.panel31.Controls.Add(this.ODUnitServiceStartDate);
            this.panel31.Controls.Add(this.label35);
            this.panel31.Location = new System.Drawing.Point(149, 33);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(179, 49);
            this.panel31.TabIndex = 3;
            // 
            // ODOutServiceStartHour
            // 
            this.ODOutServiceStartHour.Enabled = false;
            this.ODOutServiceStartHour.Location = new System.Drawing.Point(136, 22);
            this.ODOutServiceStartHour.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.ODOutServiceStartHour.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ODOutServiceStartHour.Name = "ODOutServiceStartHour";
            this.ODOutServiceStartHour.Size = new System.Drawing.Size(39, 20);
            this.ODOutServiceStartHour.TabIndex = 6;
            this.ODOutServiceStartHour.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.Window;
            this.label1.Location = new System.Drawing.Point(133, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "HOUR";
            // 
            // ODUnitServiceStartDate
            // 
            this.ODUnitServiceStartDate.Enabled = false;
            this.ODUnitServiceStartDate.HasButtons = true;
            this.ODUnitServiceStartDate.Location = new System.Drawing.Point(3, 21);
            this.ODUnitServiceStartDate.Name = "ODUnitServiceStartDate";
            this.ODUnitServiceStartDate.Readonly = true;
            this.ODUnitServiceStartDate.Size = new System.Drawing.Size(120, 20);
            this.ODUnitServiceStartDate.TabIndex = 4;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.ForeColor = System.Drawing.SystemColors.Window;
            this.label35.Location = new System.Drawing.Point(24, 4);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(85, 13);
            this.label35.TabIndex = 0;
            this.label35.Text = "START DATE";
            // 
            // ODUnitOutCheck
            // 
            this.ODUnitOutCheck.AutoSize = true;
            this.ODUnitOutCheck.Location = new System.Drawing.Point(17, 44);
            this.ODUnitOutCheck.Name = "ODUnitOutCheck";
            this.ODUnitOutCheck.Size = new System.Drawing.Size(109, 17);
            this.ODUnitOutCheck.TabIndex = 1;
            this.ODUnitOutCheck.Text = "OUT SERVICE";
            this.ODUnitOutCheck.UseVisualStyleBackColor = true;
            this.ODUnitOutCheck.CheckedChanged += new System.EventHandler(this.ODUnitOutCheck_CheckedChanged);
            // 
            // ODUnitFuelGB
            // 
            this.ODUnitFuelGB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ODUnitFuelGB.BackColor = System.Drawing.Color.Beige;
            this.ODUnitFuelGB.Controls.Add(this.linkfuel);
            this.ODUnitFuelGB.Controls.Add(this.odunitsfd2Valid);
            this.ODUnitFuelGB.Controls.Add(this.odunitsfd1Valid);
            this.ODUnitFuelGB.Controls.Add(this.panel27);
            this.ODUnitFuelGB.Controls.Add(this.panel28);
            this.ODUnitFuelGB.Controls.Add(this.panel29);
            this.ODUnitFuelGB.Controls.Add(this.ODUnitFuelCheck);
            this.ODUnitFuelGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ODUnitFuelGB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ODUnitFuelGB.Location = new System.Drawing.Point(-273, 190);
            this.ODUnitFuelGB.Name = "ODUnitFuelGB";
            this.ODUnitFuelGB.Size = new System.Drawing.Size(715, 81);
            this.ODUnitFuelGB.TabIndex = 1;
            this.ODUnitFuelGB.TabStop = false;
            this.ODUnitFuelGB.Text = "FUEL";
            // 
            // linkfuel
            // 
            this.linkfuel.AutoSize = true;
            this.linkfuel.LinkColor = System.Drawing.Color.SteelBlue;
            this.linkfuel.Location = new System.Drawing.Point(54, 59);
            this.linkfuel.Name = "linkfuel";
            this.linkfuel.Size = new System.Drawing.Size(29, 13);
            this.linkfuel.TabIndex = 14;
            this.linkfuel.TabStop = true;
            this.linkfuel.Text = "Edit";
            this.linkfuel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkfuel_LinkClicked);
            // 
            // odunitsfd2Valid
            // 
            this.odunitsfd2Valid.AutoSize = true;
            this.odunitsfd2Valid.BackColor = System.Drawing.Color.Transparent;
            this.odunitsfd2Valid.ForeColor = System.Drawing.Color.Red;
            this.odunitsfd2Valid.Location = new System.Drawing.Point(328, 50);
            this.odunitsfd2Valid.Name = "odunitsfd2Valid";
            this.odunitsfd2Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.odunitsfd2Valid.Size = new System.Drawing.Size(12, 13);
            this.odunitsfd2Valid.TabIndex = 10;
            this.odunitsfd2Valid.Text = "*";
            this.odunitsfd2Valid.Visible = false;
            // 
            // odunitsfd1Valid
            // 
            this.odunitsfd1Valid.AutoSize = true;
            this.odunitsfd1Valid.BackColor = System.Drawing.Color.Transparent;
            this.odunitsfd1Valid.ForeColor = System.Drawing.Color.Red;
            this.odunitsfd1Valid.Location = new System.Drawing.Point(132, 49);
            this.odunitsfd1Valid.Name = "odunitsfd1Valid";
            this.odunitsfd1Valid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.odunitsfd1Valid.Size = new System.Drawing.Size(12, 13);
            this.odunitsfd1Valid.TabIndex = 9;
            this.odunitsfd1Valid.Text = "*";
            this.odunitsfd1Valid.Visible = false;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.CadetBlue;
            this.panel27.Controls.Add(this.textSECONDFUELPERCENT);
            this.panel27.Controls.Add(this.label29);
            this.panel27.Controls.Add(this.ODUnitFuelTB);
            this.panel27.Controls.Add(this.label31);
            this.panel27.Location = new System.Drawing.Point(556, 23);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(138, 49);
            this.panel27.TabIndex = 5;
            // 
            // textSECONDFUELPERCENT
            // 
            this.textSECONDFUELPERCENT.Location = new System.Drawing.Point(108, 22);
            this.textSECONDFUELPERCENT.Name = "textSECONDFUELPERCENT";
            this.textSECONDFUELPERCENT.Size = new System.Drawing.Size(26, 20);
            this.textSECONDFUELPERCENT.TabIndex = 16;
            this.textSECONDFUELPERCENT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(112, 3);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(16, 13);
            this.label29.TabIndex = 15;
            this.label29.Text = "%";
            // 
            // ODUnitFuelTB
            // 
            this.ODUnitFuelTB.BackColor = System.Drawing.Color.White;
            this.ODUnitFuelTB.Enabled = false;
            this.ODUnitFuelTB.Location = new System.Drawing.Point(4, 22);
            this.ODUnitFuelTB.Name = "ODUnitFuelTB";
            this.ODUnitFuelTB.Size = new System.Drawing.Size(99, 20);
            this.ODUnitFuelTB.TabIndex = 12;
            this.ODUnitFuelTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ODUnitFuelTB.Validated += new System.EventHandler(this.ODUnitFuelTB_Validated);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.SystemColors.Window;
            this.label31.Location = new System.Drawing.Point(3, 4);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(105, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "FUEL QUANTITY";
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.CadetBlue;
            this.panel28.Controls.Add(this.ODSecondFuelEndHour);
            this.panel28.Controls.Add(this.label28);
            this.panel28.Controls.Add(this.ODUnitFuelEndDate);
            this.panel28.Controls.Add(this.label32);
            this.panel28.Location = new System.Drawing.Point(354, 23);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(171, 49);
            this.panel28.TabIndex = 4;
            // 
            // ODSecondFuelEndHour
            // 
            this.ODSecondFuelEndHour.Enabled = false;
            this.ODSecondFuelEndHour.Location = new System.Drawing.Point(128, 20);
            this.ODSecondFuelEndHour.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.ODSecondFuelEndHour.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ODSecondFuelEndHour.Name = "ODSecondFuelEndHour";
            this.ODSecondFuelEndHour.Size = new System.Drawing.Size(39, 20);
            this.ODSecondFuelEndHour.TabIndex = 13;
            this.ODSecondFuelEndHour.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.SystemColors.Window;
            this.label28.Location = new System.Drawing.Point(125, 5);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(43, 13);
            this.label28.TabIndex = 12;
            this.label28.Text = "HOUR";
            // 
            // ODUnitFuelEndDate
            // 
            this.ODUnitFuelEndDate.Enabled = false;
            this.ODUnitFuelEndDate.HasButtons = true;
            this.ODUnitFuelEndDate.Location = new System.Drawing.Point(3, 20);
            this.ODUnitFuelEndDate.Name = "ODUnitFuelEndDate";
            this.ODUnitFuelEndDate.Readonly = true;
            this.ODUnitFuelEndDate.Size = new System.Drawing.Size(120, 20);
            this.ODUnitFuelEndDate.TabIndex = 11;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.ForeColor = System.Drawing.SystemColors.Window;
            this.label32.Location = new System.Drawing.Point(30, 4);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(70, 13);
            this.label32.TabIndex = 0;
            this.label32.Text = "END DATE";
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.CadetBlue;
            this.panel29.Controls.Add(this.ODSecondFuelStartHour);
            this.panel29.Controls.Add(this.label27);
            this.panel29.Controls.Add(this.ODUnitFuelStartDate);
            this.panel29.Controls.Add(this.label33);
            this.panel29.Location = new System.Drawing.Point(150, 23);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(174, 49);
            this.panel29.TabIndex = 3;
            // 
            // ODSecondFuelStartHour
            // 
            this.ODSecondFuelStartHour.Enabled = false;
            this.ODSecondFuelStartHour.Location = new System.Drawing.Point(128, 21);
            this.ODSecondFuelStartHour.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.ODSecondFuelStartHour.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ODSecondFuelStartHour.Name = "ODSecondFuelStartHour";
            this.ODSecondFuelStartHour.Size = new System.Drawing.Size(39, 20);
            this.ODSecondFuelStartHour.TabIndex = 12;
            this.ODSecondFuelStartHour.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.SystemColors.Window;
            this.label27.Location = new System.Drawing.Point(126, 5);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 13);
            this.label27.TabIndex = 11;
            this.label27.Text = "HOUR";
            // 
            // ODUnitFuelStartDate
            // 
            this.ODUnitFuelStartDate.Enabled = false;
            this.ODUnitFuelStartDate.HasButtons = true;
            this.ODUnitFuelStartDate.Location = new System.Drawing.Point(3, 20);
            this.ODUnitFuelStartDate.Name = "ODUnitFuelStartDate";
            this.ODUnitFuelStartDate.Readonly = true;
            this.ODUnitFuelStartDate.Size = new System.Drawing.Size(120, 20);
            this.ODUnitFuelStartDate.TabIndex = 10;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.ForeColor = System.Drawing.SystemColors.Window;
            this.label33.Location = new System.Drawing.Point(24, 4);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(85, 13);
            this.label33.TabIndex = 0;
            this.label33.Text = "START DATE";
            // 
            // ODUnitFuelCheck
            // 
            this.ODUnitFuelCheck.AutoSize = true;
            this.ODUnitFuelCheck.Location = new System.Drawing.Point(17, 33);
            this.ODUnitFuelCheck.Name = "ODUnitFuelCheck";
            this.ODUnitFuelCheck.Size = new System.Drawing.Size(112, 17);
            this.ODUnitFuelCheck.TabIndex = 9;
            this.ODUnitFuelCheck.Text = "SECOND FUEL";
            this.ODUnitFuelCheck.UseVisualStyleBackColor = true;
            this.ODUnitFuelCheck.CheckedChanged += new System.EventHandler(this.ODUnitFuelCheck_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.BackColor = System.Drawing.Color.Beige;
            this.groupBox4.Controls.Add(this.panel92);
            this.groupBox4.Controls.Add(this.ODSaveBtn);
            this.groupBox4.Location = new System.Drawing.Point(-7, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(165, 91);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            // 
            // panel92
            // 
            this.panel92.BackColor = System.Drawing.Color.CadetBlue;
            this.panel92.Controls.Add(this.MUSTMAXCKECK);
            this.panel92.Controls.Add(this.MustrunCheck);
            this.panel92.Location = new System.Drawing.Point(15, 33);
            this.panel92.Name = "panel92";
            this.panel92.Size = new System.Drawing.Size(127, 49);
            this.panel92.TabIndex = 8;
            // 
            // MUSTMAXCKECK
            // 
            this.MUSTMAXCKECK.AutoSize = true;
            this.MUSTMAXCKECK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MUSTMAXCKECK.ForeColor = System.Drawing.Color.White;
            this.MUSTMAXCKECK.Location = new System.Drawing.Point(17, 24);
            this.MUSTMAXCKECK.Name = "MUSTMAXCKECK";
            this.MUSTMAXCKECK.Size = new System.Drawing.Size(91, 17);
            this.MUSTMAXCKECK.TabIndex = 1;
            this.MUSTMAXCKECK.Text = "MUST MAX";
            this.MUSTMAXCKECK.UseVisualStyleBackColor = true;
            // 
            // MustrunCheck
            // 
            this.MustrunCheck.AutoSize = true;
            this.MustrunCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MustrunCheck.ForeColor = System.Drawing.Color.White;
            this.MustrunCheck.Location = new System.Drawing.Point(17, 5);
            this.MustrunCheck.Name = "MustrunCheck";
            this.MustrunCheck.Size = new System.Drawing.Size(92, 17);
            this.MustrunCheck.TabIndex = 0;
            this.MustrunCheck.Text = "MUST RUN";
            this.MustrunCheck.UseVisualStyleBackColor = true;
            // 
            // ODSaveBtn
            // 
            this.ODSaveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ODSaveBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.ODSaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ODSaveBtn.Location = new System.Drawing.Point(-17, 19);
            this.ODSaveBtn.Name = "ODSaveBtn";
            this.ODSaveBtn.Size = new System.Drawing.Size(75, 23);
            this.ODSaveBtn.TabIndex = 27;
            this.ODSaveBtn.Text = "Save";
            this.ODSaveBtn.UseVisualStyleBackColor = false;
            this.ODSaveBtn.Visible = false;
            this.ODSaveBtn.Click += new System.EventHandler(this.ODSaveBtn_Click);
            // 
            // panel97
            // 
            this.panel97.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel97.BackgroundImage = global::PowerPlantProject.Properties.Resources._100848480725;
            this.panel97.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel97.Location = new System.Drawing.Point(3, 4);
            this.panel97.Name = "panel97";
            this.panel97.Size = new System.Drawing.Size(740, 547);
            this.panel97.TabIndex = 31;
            // 
            // GeneralData
            // 
            this.GeneralData.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.GeneralData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.GeneralData.Controls.Add(this.GDPostpanel);
            this.GeneralData.Controls.Add(this.GDMainPanel);
            this.GeneralData.Controls.Add(this.GDHeaderGB);
            this.GeneralData.Location = new System.Drawing.Point(4, 29);
            this.GeneralData.Name = "GeneralData";
            this.GeneralData.Size = new System.Drawing.Size(757, 625);
            this.GeneralData.TabIndex = 6;
            this.GeneralData.Text = "General Data";
            this.GeneralData.UseVisualStyleBackColor = true;
            // 
            // GDPostpanel
            // 
            this.GDPostpanel.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.GDPostpanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.GDPostpanel.Controls.Add(this.groupBox20);
            this.GDPostpanel.Controls.Add(this.groupBox17);
            this.GDPostpanel.Controls.Add(this.panel95);
            this.GDPostpanel.Controls.Add(this.GDPowerGb);
            this.GDPostpanel.Controls.Add(this.ConsumedGb);
            this.GDPostpanel.Location = new System.Drawing.Point(17, 68);
            this.GDPostpanel.Name = "GDPostpanel";
            this.GDPostpanel.Size = new System.Drawing.Size(704, 514);
            this.GDPostpanel.TabIndex = 16;
            this.GDPostpanel.Visible = false;
            // 
            // groupBox20
            // 
            this.groupBox20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox20.Controls.Add(this.FromListBox);
            this.groupBox20.Controls.Add(this.label101);
            this.groupBox20.Controls.Add(this.ToListBox);
            this.groupBox20.Controls.Add(this.label102);
            this.groupBox20.Location = new System.Drawing.Point(377, 54);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(297, 150);
            this.groupBox20.TabIndex = 39;
            this.groupBox20.TabStop = false;
            // 
            // FromListBox
            // 
            this.FromListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FromListBox.FormattingEnabled = true;
            this.FromListBox.Location = new System.Drawing.Point(17, 56);
            this.FromListBox.Name = "FromListBox";
            this.FromListBox.Size = new System.Drawing.Size(120, 54);
            this.FromListBox.TabIndex = 1;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(18, 40);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(33, 13);
            this.label101.TabIndex = 2;
            this.label101.Text = "From:";
            // 
            // ToListBox
            // 
            this.ToListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ToListBox.FormattingEnabled = true;
            this.ToListBox.Location = new System.Drawing.Point(159, 57);
            this.ToListBox.Name = "ToListBox";
            this.ToListBox.Size = new System.Drawing.Size(120, 54);
            this.ToListBox.TabIndex = 3;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(158, 41);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(23, 13);
            this.label102.TabIndex = 4;
            this.label102.Text = "To:";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.radioEditPost);
            this.groupBox17.Controls.Add(this.radioAddpost);
            this.groupBox17.Controls.Add(this.BtnOKpost);
            this.groupBox17.Controls.Add(this.label112);
            this.groupBox17.Controls.Add(this.label111);
            this.groupBox17.Controls.Add(this.label110);
            this.groupBox17.Controls.Add(this.txtaddvoltagenum);
            this.groupBox17.Controls.Add(this.txtpostname);
            this.groupBox17.Controls.Add(this.txtaddpostnum);
            this.groupBox17.Location = new System.Drawing.Point(31, 54);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(297, 152);
            this.groupBox17.TabIndex = 36;
            this.groupBox17.TabStop = false;
            // 
            // radioEditPost
            // 
            this.radioEditPost.AutoSize = true;
            this.radioEditPost.Location = new System.Drawing.Point(65, 15);
            this.radioEditPost.Name = "radioEditPost";
            this.radioEditPost.Size = new System.Drawing.Size(67, 17);
            this.radioEditPost.TabIndex = 39;
            this.radioEditPost.TabStop = true;
            this.radioEditPost.Text = "Edit Post";
            this.radioEditPost.UseVisualStyleBackColor = true;
            this.radioEditPost.CheckedChanged += new System.EventHandler(this.radioEditPost_CheckedChanged);
            // 
            // radioAddpost
            // 
            this.radioAddpost.AutoSize = true;
            this.radioAddpost.Location = new System.Drawing.Point(150, 15);
            this.radioAddpost.Name = "radioAddpost";
            this.radioAddpost.Size = new System.Drawing.Size(93, 17);
            this.radioAddpost.TabIndex = 39;
            this.radioAddpost.TabStop = true;
            this.radioAddpost.Text = "Add New Post";
            this.radioAddpost.UseVisualStyleBackColor = true;
            this.radioAddpost.CheckedChanged += new System.EventHandler(this.radioAddpost_CheckedChanged);
            // 
            // BtnOKpost
            // 
            this.BtnOKpost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnOKpost.BackColor = System.Drawing.Color.CadetBlue;
            this.BtnOKpost.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnOKpost.Location = new System.Drawing.Point(172, 118);
            this.BtnOKpost.Name = "BtnOKpost";
            this.BtnOKpost.Size = new System.Drawing.Size(36, 20);
            this.BtnOKpost.TabIndex = 39;
            this.BtnOKpost.Text = "OK";
            this.BtnOKpost.UseVisualStyleBackColor = false;
            this.BtnOKpost.Click += new System.EventHandler(this.BtnOKpost_Click);
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(48, 70);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(65, 13);
            this.label112.TabIndex = 36;
            this.label112.Text = "Post Name :";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(47, 93);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(78, 13);
            this.label111.TabIndex = 36;
            this.label111.Text = "Voltage Level :";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(47, 46);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(74, 13);
            this.label110.TabIndex = 36;
            this.label110.Text = "Post Number :";
            // 
            // txtaddvoltagenum
            // 
            this.txtaddvoltagenum.Location = new System.Drawing.Point(131, 46);
            this.txtaddvoltagenum.Name = "txtaddvoltagenum";
            this.txtaddvoltagenum.Size = new System.Drawing.Size(110, 20);
            this.txtaddvoltagenum.TabIndex = 10;
            this.txtaddvoltagenum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtpostname
            // 
            this.txtpostname.Location = new System.Drawing.Point(131, 69);
            this.txtpostname.Name = "txtpostname";
            this.txtpostname.Size = new System.Drawing.Size(110, 20);
            this.txtpostname.TabIndex = 35;
            this.txtpostname.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtaddpostnum
            // 
            this.txtaddpostnum.Location = new System.Drawing.Point(131, 92);
            this.txtaddpostnum.Name = "txtaddpostnum";
            this.txtaddpostnum.Size = new System.Drawing.Size(110, 20);
            this.txtaddpostnum.TabIndex = 34;
            this.txtaddpostnum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel95
            // 
            this.panel95.BackColor = System.Drawing.Color.Beige;
            this.panel95.Controls.Add(this.label108);
            this.panel95.Controls.Add(this.DeletePostBtn);
            this.panel95.Controls.Add(this.VolNumLb);
            this.panel95.Controls.Add(this.label107);
            this.panel95.Controls.Add(this.voltagelevellb);
            this.panel95.Controls.Add(this.postnumlb);
            this.panel95.Controls.Add(this.label109);
            this.panel95.Location = new System.Drawing.Point(10, 9);
            this.panel95.Name = "panel95";
            this.panel95.Size = new System.Drawing.Size(684, 37);
            this.panel95.TabIndex = 38;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label108.Location = new System.Drawing.Point(45, 13);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(87, 13);
            this.label108.TabIndex = 8;
            this.label108.Text = "Post Number :";
            // 
            // DeletePostBtn
            // 
            this.DeletePostBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DeletePostBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.DeletePostBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.DeletePostBtn.Location = new System.Drawing.Point(600, 8);
            this.DeletePostBtn.Name = "DeletePostBtn";
            this.DeletePostBtn.Size = new System.Drawing.Size(72, 22);
            this.DeletePostBtn.TabIndex = 37;
            this.DeletePostBtn.Text = "Delete Post";
            this.DeletePostBtn.UseVisualStyleBackColor = false;
            this.DeletePostBtn.Click += new System.EventHandler(this.DeletePostBtn_Click);
            // 
            // VolNumLb
            // 
            this.VolNumLb.AutoSize = true;
            this.VolNumLb.Location = new System.Drawing.Point(134, 14);
            this.VolNumLb.Name = "VolNumLb";
            this.VolNumLb.Size = new System.Drawing.Size(47, 13);
            this.VolNumLb.TabIndex = 0;
            this.VolNumLb.Text = "label101";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label107.Location = new System.Drawing.Point(406, 14);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(93, 13);
            this.label107.TabIndex = 7;
            this.label107.Text = "Voltage Level :";
            // 
            // voltagelevellb
            // 
            this.voltagelevellb.AutoSize = true;
            this.voltagelevellb.Location = new System.Drawing.Point(306, 13);
            this.voltagelevellb.Name = "voltagelevellb";
            this.voltagelevellb.Size = new System.Drawing.Size(47, 13);
            this.voltagelevellb.TabIndex = 9;
            this.voltagelevellb.Text = "label109";
            // 
            // postnumlb
            // 
            this.postnumlb.AutoSize = true;
            this.postnumlb.Location = new System.Drawing.Point(511, 14);
            this.postnumlb.Name = "postnumlb";
            this.postnumlb.Size = new System.Drawing.Size(47, 13);
            this.postnumlb.TabIndex = 8;
            this.postnumlb.Text = "label108";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label109.Location = new System.Drawing.Point(214, 13);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(76, 13);
            this.label109.TabIndex = 9;
            this.label109.Text = "Post Name :";
            // 
            // GDPowerGb
            // 
            this.GDPowerGb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GDPowerGb.Controls.Add(this.AddPowerBtn);
            this.GDPowerGb.Controls.Add(this.label105);
            this.GDPowerGb.Controls.Add(this.AddPowerList);
            this.GDPowerGb.Controls.Add(this.DeletePowerBtn);
            this.GDPowerGb.Controls.Add(this.label106);
            this.GDPowerGb.Controls.Add(this.DeletePowerList);
            this.GDPowerGb.Location = new System.Drawing.Point(377, 210);
            this.GDPowerGb.Name = "GDPowerGb";
            this.GDPowerGb.Size = new System.Drawing.Size(297, 296);
            this.GDPowerGb.TabIndex = 6;
            this.GDPowerGb.TabStop = false;
            this.GDPowerGb.Text = "Power";
            // 
            // AddPowerBtn
            // 
            this.AddPowerBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AddPowerBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.AddPowerBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AddPowerBtn.Location = new System.Drawing.Point(180, 262);
            this.AddPowerBtn.Name = "AddPowerBtn";
            this.AddPowerBtn.Size = new System.Drawing.Size(75, 23);
            this.AddPowerBtn.TabIndex = 32;
            this.AddPowerBtn.Text = "Add";
            this.AddPowerBtn.UseVisualStyleBackColor = false;
            this.AddPowerBtn.Click += new System.EventHandler(this.AddPowerBtn_Click);
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(170, 28);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(95, 13);
            this.label105.TabIndex = 31;
            this.label105.Text = "Selectable Powers";
            // 
            // AddPowerList
            // 
            this.AddPowerList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AddPowerList.FormattingEnabled = true;
            this.AddPowerList.Location = new System.Drawing.Point(155, 53);
            this.AddPowerList.Name = "AddPowerList";
            this.AddPowerList.Size = new System.Drawing.Size(127, 197);
            this.AddPowerList.TabIndex = 30;
            // 
            // DeletePowerBtn
            // 
            this.DeletePowerBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DeletePowerBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.DeletePowerBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.DeletePowerBtn.Location = new System.Drawing.Point(43, 260);
            this.DeletePowerBtn.Name = "DeletePowerBtn";
            this.DeletePowerBtn.Size = new System.Drawing.Size(75, 23);
            this.DeletePowerBtn.TabIndex = 29;
            this.DeletePowerBtn.Text = "Delete";
            this.DeletePowerBtn.UseVisualStyleBackColor = false;
            this.DeletePowerBtn.Click += new System.EventHandler(this.DeletePowerBtn_Click);
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(30, 29);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(88, 13);
            this.label106.TabIndex = 1;
            this.label106.Text = "Available Powers";
            // 
            // DeletePowerList
            // 
            this.DeletePowerList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DeletePowerList.FormattingEnabled = true;
            this.DeletePowerList.Location = new System.Drawing.Point(13, 54);
            this.DeletePowerList.Name = "DeletePowerList";
            this.DeletePowerList.Size = new System.Drawing.Size(127, 197);
            this.DeletePowerList.TabIndex = 0;
            // 
            // ConsumedGb
            // 
            this.ConsumedGb.Controls.Add(this.txtselectconsum);
            this.ConsumedGb.Controls.Add(this.AddConsumBtn);
            this.ConsumedGb.Controls.Add(this.label104);
            this.ConsumedGb.Controls.Add(this.DeleteConsumBtn);
            this.ConsumedGb.Controls.Add(this.label103);
            this.ConsumedGb.Controls.Add(this.DeleteConsumeList);
            this.ConsumedGb.Location = new System.Drawing.Point(31, 212);
            this.ConsumedGb.Name = "ConsumedGb";
            this.ConsumedGb.Size = new System.Drawing.Size(297, 294);
            this.ConsumedGb.TabIndex = 5;
            this.ConsumedGb.TabStop = false;
            this.ConsumedGb.Text = "Consumed";
            // 
            // txtselectconsum
            // 
            this.txtselectconsum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtselectconsum.Location = new System.Drawing.Point(181, 145);
            this.txtselectconsum.Name = "txtselectconsum";
            this.txtselectconsum.Size = new System.Drawing.Size(100, 20);
            this.txtselectconsum.TabIndex = 7;
            this.txtselectconsum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AddConsumBtn
            // 
            this.AddConsumBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AddConsumBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.AddConsumBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AddConsumBtn.Location = new System.Drawing.Point(193, 181);
            this.AddConsumBtn.Name = "AddConsumBtn";
            this.AddConsumBtn.Size = new System.Drawing.Size(75, 23);
            this.AddConsumBtn.TabIndex = 32;
            this.AddConsumBtn.Text = "Add";
            this.AddConsumBtn.UseVisualStyleBackColor = false;
            this.AddConsumBtn.Click += new System.EventHandler(this.AddConsumBtn_Click);
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(188, 106);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(80, 13);
            this.label104.TabIndex = 31;
            this.label104.Text = "Insert Consume";
            // 
            // DeleteConsumBtn
            // 
            this.DeleteConsumBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DeleteConsumBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.DeleteConsumBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.DeleteConsumBtn.Location = new System.Drawing.Point(41, 257);
            this.DeleteConsumBtn.Name = "DeleteConsumBtn";
            this.DeleteConsumBtn.Size = new System.Drawing.Size(75, 23);
            this.DeleteConsumBtn.TabIndex = 29;
            this.DeleteConsumBtn.Text = "Delete";
            this.DeleteConsumBtn.UseVisualStyleBackColor = false;
            this.DeleteConsumBtn.Click += new System.EventHandler(this.DeleteConsumBtn_Click);
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(29, 31);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(102, 13);
            this.label103.TabIndex = 1;
            this.label103.Text = "Available Consumes";
            // 
            // DeleteConsumeList
            // 
            this.DeleteConsumeList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DeleteConsumeList.FormattingEnabled = true;
            this.DeleteConsumeList.Location = new System.Drawing.Point(16, 54);
            this.DeleteConsumeList.Name = "DeleteConsumeList";
            this.DeleteConsumeList.Size = new System.Drawing.Size(127, 197);
            this.DeleteConsumeList.TabIndex = 0;
            // 
            // GDMainPanel
            // 
            this.GDMainPanel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.GDMainPanel.Controls.Add(this.TempGV);
            this.GDMainPanel.Controls.Add(this.GDNewBtn);
            this.GDMainPanel.Controls.Add(this.GDDeleteBtn);
            this.GDMainPanel.Controls.Add(this.GDSteamGroup);
            this.GDMainPanel.Controls.Add(this.GDGasGroup);
            this.GDMainPanel.Controls.Add(this.GDUnitPanel);
            this.GDMainPanel.Location = new System.Drawing.Point(3, 71);
            this.GDMainPanel.Name = "GDMainPanel";
            this.GDMainPanel.Size = new System.Drawing.Size(743, 552);
            this.GDMainPanel.TabIndex = 20;
            // 
            // TempGV
            // 
            this.TempGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TempGV.Location = new System.Drawing.Point(10, 492);
            this.TempGV.Name = "TempGV";
            this.TempGV.Size = new System.Drawing.Size(92, 46);
            this.TempGV.TabIndex = 33;
            this.TempGV.Visible = false;
            // 
            // GDNewBtn
            // 
            this.GDNewBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.GDNewBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.GDNewBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.GDNewBtn.Location = new System.Drawing.Point(526, 520);
            this.GDNewBtn.Name = "GDNewBtn";
            this.GDNewBtn.Size = new System.Drawing.Size(75, 23);
            this.GDNewBtn.TabIndex = 28;
            this.GDNewBtn.Text = "Add Unit";
            this.GDNewBtn.UseVisualStyleBackColor = false;
            this.GDNewBtn.Visible = false;
            this.GDNewBtn.Click += new System.EventHandler(this.GDNewBtn_Click);
            // 
            // GDDeleteBtn
            // 
            this.GDDeleteBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GDDeleteBtn.BackColor = System.Drawing.Color.CadetBlue;
            this.GDDeleteBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.GDDeleteBtn.Location = new System.Drawing.Point(643, 520);
            this.GDDeleteBtn.Name = "GDDeleteBtn";
            this.GDDeleteBtn.Size = new System.Drawing.Size(75, 23);
            this.GDDeleteBtn.TabIndex = 31;
            this.GDDeleteBtn.Text = "Delete Unit";
            this.GDDeleteBtn.UseVisualStyleBackColor = false;
            this.GDDeleteBtn.Visible = false;
            this.GDDeleteBtn.Click += new System.EventHandler(this.GDDeleteBtn_Click);
            // 
            // GDSteamGroup
            // 
            this.GDSteamGroup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.GDSteamGroup.BackColor = System.Drawing.Color.Beige;
            this.GDSteamGroup.Controls.Add(this.Grid230);
            this.GDSteamGroup.Controls.Add(this.PlantGV2);
            this.GDSteamGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GDSteamGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.GDSteamGroup.ForeColor = System.Drawing.Color.Black;
            this.GDSteamGroup.Location = new System.Drawing.Point(19, 263);
            this.GDSteamGroup.Name = "GDSteamGroup";
            this.GDSteamGroup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.GDSteamGroup.Size = new System.Drawing.Size(713, 237);
            this.GDSteamGroup.TabIndex = 30;
            this.GDSteamGroup.TabStop = false;
            this.GDSteamGroup.Text = "STEAM";
            this.GDSteamGroup.Visible = false;
            // 
            // Grid230
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.Grid230.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.Grid230.AutoGenerateColumns = false;
            this.Grid230.BackgroundColor = System.Drawing.Color.White;
            this.Grid230.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grid230.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Grid230.ColumnHeadersHeight = 22;
            this.Grid230.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn32,
            this.CheckBox2});
            this.Grid230.DataSource = this.linesBindingSource;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grid230.DefaultCellStyle = dataGridViewCellStyle3;
            this.Grid230.EnableHeadersVisualStyles = false;
            this.Grid230.Location = new System.Drawing.Point(17, 55);
            this.Grid230.Name = "Grid230";
            this.Grid230.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LemonChiffon;
            this.Grid230.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.Grid230.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grid230.Size = new System.Drawing.Size(678, 150);
            this.Grid230.TabIndex = 2;
            this.Grid230.Visible = false;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "LineCode";
            this.dataGridViewTextBoxColumn14.HeaderText = "Line";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Width = 60;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "FromBus";
            this.dataGridViewTextBoxColumn15.HeaderText = "From Bus";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Width = 75;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "ToBus";
            this.dataGridViewTextBoxColumn16.HeaderText = "To Bus";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Width = 75;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "LineLength";
            this.dataGridViewTextBoxColumn27.HeaderText = "Line Length";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.Width = 80;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "Owner_GencoFrom";
            this.dataGridViewTextBoxColumn28.HeaderText = "Genco Start Owner";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.Width = 125;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "Owner_GencoTo";
            this.dataGridViewTextBoxColumn29.HeaderText = "Genco End Owner";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.Width = 125;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "RateA";
            this.dataGridViewTextBoxColumn30.HeaderText = "Capacity";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.Width = 60;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.HeaderText = "Status";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            this.dataGridViewTextBoxColumn32.Width = 50;
            // 
            // CheckBox2
            // 
            this.CheckBox2.FalseValue = "0";
            this.CheckBox2.HeaderText = "";
            this.CheckBox2.Name = "CheckBox2";
            this.CheckBox2.ReadOnly = true;
            this.CheckBox2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CheckBox2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.CheckBox2.TrueValue = "1";
            this.CheckBox2.Width = 25;
            // 
            // PlantGV2
            // 
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.PlantGV2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.PlantGV2.AutoGenerateColumns = false;
            this.PlantGV2.BackgroundColor = System.Drawing.Color.White;
            this.PlantGV2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PlantGV2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.PlantGV2.ColumnHeadersHeight = 22;
            this.PlantGV2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.unitCodeDataGridViewTextBoxColumn1,
            this.packageCodeDataGridViewTextBoxColumn1,
            this.unitTypeDataGridViewTextBoxColumn1,
            this.capacityDataGridViewTextBoxColumn1,
            this.pMinDataGridViewTextBoxColumn1,
            this.pMaxDataGridViewTextBoxColumn1,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column31});
            this.PlantGV2.DataSource = this.unitsDataMainBindingSource;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PlantGV2.DefaultCellStyle = dataGridViewCellStyle7;
            this.PlantGV2.EnableHeadersVisualStyles = false;
            this.PlantGV2.Location = new System.Drawing.Point(17, 38);
            this.PlantGV2.Name = "PlantGV2";
            this.PlantGV2.RowHeadersVisible = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.LemonChiffon;
            this.PlantGV2.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.PlantGV2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PlantGV2.Size = new System.Drawing.Size(678, 150);
            this.PlantGV2.TabIndex = 3;
            this.PlantGV2.Visible = false;
            // 
            // unitCodeDataGridViewTextBoxColumn1
            // 
            this.unitCodeDataGridViewTextBoxColumn1.DataPropertyName = "UnitCode";
            this.unitCodeDataGridViewTextBoxColumn1.HeaderText = "Unit";
            this.unitCodeDataGridViewTextBoxColumn1.Name = "unitCodeDataGridViewTextBoxColumn1";
            this.unitCodeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.unitCodeDataGridViewTextBoxColumn1.Width = 70;
            // 
            // packageCodeDataGridViewTextBoxColumn1
            // 
            this.packageCodeDataGridViewTextBoxColumn1.DataPropertyName = "PackageCode";
            this.packageCodeDataGridViewTextBoxColumn1.HeaderText = "Package";
            this.packageCodeDataGridViewTextBoxColumn1.Name = "packageCodeDataGridViewTextBoxColumn1";
            this.packageCodeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.packageCodeDataGridViewTextBoxColumn1.Width = 65;
            // 
            // unitTypeDataGridViewTextBoxColumn1
            // 
            this.unitTypeDataGridViewTextBoxColumn1.DataPropertyName = "UnitType";
            this.unitTypeDataGridViewTextBoxColumn1.HeaderText = "Type";
            this.unitTypeDataGridViewTextBoxColumn1.Name = "unitTypeDataGridViewTextBoxColumn1";
            this.unitTypeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.unitTypeDataGridViewTextBoxColumn1.Width = 60;
            // 
            // capacityDataGridViewTextBoxColumn1
            // 
            this.capacityDataGridViewTextBoxColumn1.DataPropertyName = "Capacity";
            this.capacityDataGridViewTextBoxColumn1.HeaderText = "Capacity";
            this.capacityDataGridViewTextBoxColumn1.Name = "capacityDataGridViewTextBoxColumn1";
            this.capacityDataGridViewTextBoxColumn1.ReadOnly = true;
            this.capacityDataGridViewTextBoxColumn1.Width = 75;
            // 
            // pMinDataGridViewTextBoxColumn1
            // 
            this.pMinDataGridViewTextBoxColumn1.DataPropertyName = "PMin";
            this.pMinDataGridViewTextBoxColumn1.HeaderText = "PMin";
            this.pMinDataGridViewTextBoxColumn1.Name = "pMinDataGridViewTextBoxColumn1";
            this.pMinDataGridViewTextBoxColumn1.ReadOnly = true;
            this.pMinDataGridViewTextBoxColumn1.Width = 55;
            // 
            // pMaxDataGridViewTextBoxColumn1
            // 
            this.pMaxDataGridViewTextBoxColumn1.DataPropertyName = "PMax";
            this.pMaxDataGridViewTextBoxColumn1.HeaderText = "PMax";
            this.pMaxDataGridViewTextBoxColumn1.Name = "pMaxDataGridViewTextBoxColumn1";
            this.pMaxDataGridViewTextBoxColumn1.ReadOnly = true;
            this.pMaxDataGridViewTextBoxColumn1.Width = 55;
            // 
            // Column14
            // 
            this.Column14.HeaderText = "Fuel";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 55;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "Status";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 65;
            // 
            // Column16
            // 
            this.Column16.HeaderText = "Maintenance";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Width = 75;
            // 
            // Column17
            // 
            this.Column17.HeaderText = "Outservice";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 75;
            // 
            // Column31
            // 
            this.Column31.FalseValue = "0";
            this.Column31.HeaderText = "";
            this.Column31.Name = "Column31";
            this.Column31.ReadOnly = true;
            this.Column31.TrueValue = "1";
            this.Column31.Width = 25;
            // 
            // GDGasGroup
            // 
            this.GDGasGroup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.GDGasGroup.BackColor = System.Drawing.Color.Beige;
            this.GDGasGroup.Controls.Add(this.Grid400);
            this.GDGasGroup.Controls.Add(this.PlantGV1);
            this.GDGasGroup.Controls.Add(this.PlantGV11);
            this.GDGasGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GDGasGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.GDGasGroup.ForeColor = System.Drawing.Color.Black;
            this.GDGasGroup.Location = new System.Drawing.Point(19, 30);
            this.GDGasGroup.Name = "GDGasGroup";
            this.GDGasGroup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.GDGasGroup.Size = new System.Drawing.Size(713, 204);
            this.GDGasGroup.TabIndex = 29;
            this.GDGasGroup.TabStop = false;
            this.GDGasGroup.Text = "GAS";
            this.GDGasGroup.Visible = false;
            // 
            // Grid400
            // 
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            this.Grid400.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.Grid400.AutoGenerateColumns = false;
            this.Grid400.BackgroundColor = System.Drawing.Color.White;
            this.Grid400.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Grid400.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.Grid400.ColumnHeadersHeight = 22;
            this.Grid400.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.lineCodeDataGridViewTextBoxColumn,
            this.fromBusDataGridViewTextBoxColumn,
            this.toBusDataGridViewTextBoxColumn,
            this.lineLengthDataGridViewTextBoxColumn,
            this.ownerGencoFromDataGridViewTextBoxColumn,
            this.ownerGencoToDataGridViewTextBoxColumn,
            this.Column13,
            this.dataGridViewTextBoxColumn31,
            this.CheckBox1});
            this.Grid400.DataSource = this.linesBindingSource1;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Grid400.DefaultCellStyle = dataGridViewCellStyle11;
            this.Grid400.EnableHeadersVisualStyles = false;
            this.Grid400.Location = new System.Drawing.Point(17, 47);
            this.Grid400.Name = "Grid400";
            this.Grid400.RowHeadersVisible = false;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.LemonChiffon;
            this.Grid400.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.Grid400.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grid400.Size = new System.Drawing.Size(678, 150);
            this.Grid400.TabIndex = 1;
            this.Grid400.Visible = false;
            // 
            // lineCodeDataGridViewTextBoxColumn
            // 
            this.lineCodeDataGridViewTextBoxColumn.DataPropertyName = "LineCode";
            this.lineCodeDataGridViewTextBoxColumn.HeaderText = "Line";
            this.lineCodeDataGridViewTextBoxColumn.Name = "lineCodeDataGridViewTextBoxColumn";
            this.lineCodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.lineCodeDataGridViewTextBoxColumn.Width = 60;
            // 
            // fromBusDataGridViewTextBoxColumn
            // 
            this.fromBusDataGridViewTextBoxColumn.DataPropertyName = "FromBus";
            this.fromBusDataGridViewTextBoxColumn.HeaderText = "From Bus";
            this.fromBusDataGridViewTextBoxColumn.Name = "fromBusDataGridViewTextBoxColumn";
            this.fromBusDataGridViewTextBoxColumn.ReadOnly = true;
            this.fromBusDataGridViewTextBoxColumn.Width = 75;
            // 
            // toBusDataGridViewTextBoxColumn
            // 
            this.toBusDataGridViewTextBoxColumn.DataPropertyName = "ToBus";
            this.toBusDataGridViewTextBoxColumn.HeaderText = "To Bus";
            this.toBusDataGridViewTextBoxColumn.Name = "toBusDataGridViewTextBoxColumn";
            this.toBusDataGridViewTextBoxColumn.ReadOnly = true;
            this.toBusDataGridViewTextBoxColumn.Width = 75;
            // 
            // lineLengthDataGridViewTextBoxColumn
            // 
            this.lineLengthDataGridViewTextBoxColumn.DataPropertyName = "LineLength";
            this.lineLengthDataGridViewTextBoxColumn.HeaderText = "Line Length";
            this.lineLengthDataGridViewTextBoxColumn.Name = "lineLengthDataGridViewTextBoxColumn";
            this.lineLengthDataGridViewTextBoxColumn.ReadOnly = true;
            this.lineLengthDataGridViewTextBoxColumn.Width = 80;
            // 
            // ownerGencoFromDataGridViewTextBoxColumn
            // 
            this.ownerGencoFromDataGridViewTextBoxColumn.DataPropertyName = "Owner_GencoFrom";
            this.ownerGencoFromDataGridViewTextBoxColumn.HeaderText = "Genco Start Owner";
            this.ownerGencoFromDataGridViewTextBoxColumn.Name = "ownerGencoFromDataGridViewTextBoxColumn";
            this.ownerGencoFromDataGridViewTextBoxColumn.ReadOnly = true;
            this.ownerGencoFromDataGridViewTextBoxColumn.Width = 125;
            // 
            // ownerGencoToDataGridViewTextBoxColumn
            // 
            this.ownerGencoToDataGridViewTextBoxColumn.DataPropertyName = "Owner_GencoTo";
            this.ownerGencoToDataGridViewTextBoxColumn.HeaderText = "Genco End Owner";
            this.ownerGencoToDataGridViewTextBoxColumn.Name = "ownerGencoToDataGridViewTextBoxColumn";
            this.ownerGencoToDataGridViewTextBoxColumn.ReadOnly = true;
            this.ownerGencoToDataGridViewTextBoxColumn.Width = 125;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "RateA";
            this.Column13.HeaderText = "Capacity";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 65;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.HeaderText = "Status";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.Width = 50;
            // 
            // CheckBox1
            // 
            this.CheckBox1.DataPropertyName = "LineNumber";
            this.CheckBox1.FalseValue = "0";
            this.CheckBox1.HeaderText = "";
            this.CheckBox1.Name = "CheckBox1";
            this.CheckBox1.ReadOnly = true;
            this.CheckBox1.TrueValue = "1";
            this.CheckBox1.Width = 20;
            // 
            // PlantGV1
            // 
            this.PlantGV1.AllowUserToOrderColumns = true;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            this.PlantGV1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.PlantGV1.AutoGenerateColumns = false;
            this.PlantGV1.BackgroundColor = System.Drawing.Color.White;
            this.PlantGV1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PlantGV1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.PlantGV1.ColumnHeadersHeight = 22;
            this.PlantGV1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.CheckBox});
            this.PlantGV1.DataSource = this.unitsDataMainBindingSource;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PlantGV1.DefaultCellStyle = dataGridViewCellStyle16;
            this.PlantGV1.EnableHeadersVisualStyles = false;
            this.PlantGV1.Location = new System.Drawing.Point(17, 27);
            this.PlantGV1.Name = "PlantGV1";
            this.PlantGV1.RowHeadersVisible = false;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.LemonChiffon;
            this.PlantGV1.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this.PlantGV1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PlantGV1.Size = new System.Drawing.Size(678, 150);
            this.PlantGV1.TabIndex = 4;
            this.PlantGV1.Visible = false;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "UnitCode";
            this.dataGridViewTextBoxColumn17.HeaderText = "Unit";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Width = 70;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "PackageCode";
            this.dataGridViewTextBoxColumn18.HeaderText = "Package";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Width = 65;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "UnitType";
            this.dataGridViewTextBoxColumn19.HeaderText = "Type";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Width = 60;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "Capacity";
            this.dataGridViewTextBoxColumn20.HeaderText = "Capacity";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Width = 75;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "PMin";
            this.dataGridViewTextBoxColumn22.HeaderText = "PMin";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.Width = 55;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "PMax";
            this.dataGridViewTextBoxColumn21.HeaderText = "PMax";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Width = 55;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.HeaderText = "Fuel";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Width = 55;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.HeaderText = "Status";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Width = 65;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.HeaderText = "Maintenance";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.Width = 75;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.HeaderText = "Outservice";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Width = 75;
            // 
            // CheckBox
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Miriam Fixed", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle15.NullValue = false;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.CheckBox.DefaultCellStyle = dataGridViewCellStyle15;
            this.CheckBox.FalseValue = "0";
            this.CheckBox.HeaderText = "";
            this.CheckBox.Name = "CheckBox";
            this.CheckBox.ReadOnly = true;
            this.CheckBox.TrueValue = "1";
            this.CheckBox.Width = 25;
            // 
            // PlantGV11
            // 
            this.PlantGV11.AutoGenerateColumns = false;
            this.PlantGV11.BackgroundColor = System.Drawing.Color.White;
            this.PlantGV11.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PlantGV11.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.PlantGV11.ColumnHeadersHeight = 22;
            this.PlantGV11.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Fuel,
            this.Status,
            this.Maintenance,
            this.Outservice});
            this.PlantGV11.DataSource = this.powerPalntDBDataSet4BindingSource;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PlantGV11.DefaultCellStyle = dataGridViewCellStyle19;
            this.PlantGV11.EnableHeadersVisualStyles = false;
            this.PlantGV11.Location = new System.Drawing.Point(15, 152);
            this.PlantGV11.Name = "PlantGV11";
            this.PlantGV11.RowHeadersVisible = false;
            this.PlantGV11.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PlantGV11.Size = new System.Drawing.Size(678, 150);
            this.PlantGV11.TabIndex = 0;
            this.PlantGV11.Visible = false;
            // 
            // Fuel
            // 
            this.Fuel.DataPropertyName = "GencoCode";
            this.Fuel.HeaderText = "Fuel";
            this.Fuel.Name = "Fuel";
            this.Fuel.Width = 60;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "GencoCode";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.Width = 75;
            // 
            // Maintenance
            // 
            this.Maintenance.DataPropertyName = "GencoCode";
            this.Maintenance.HeaderText = "Maintenance";
            this.Maintenance.Name = "Maintenance";
            this.Maintenance.Width = 75;
            // 
            // Outservice
            // 
            this.Outservice.DataPropertyName = "GencoCode";
            this.Outservice.HeaderText = "Outservice";
            this.Outservice.Name = "Outservice";
            this.Outservice.Width = 75;
            // 
            // GDUnitPanel
            // 
            this.GDUnitPanel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.GDUnitPanel.Controls.Add(this.GDUnitMaintenanceGB);
            this.GDUnitPanel.Controls.Add(this.Countergb);
            this.GDUnitPanel.Controls.Add(this.Anonygb);
            this.GDUnitPanel.Controls.Add(this.Powergb);
            this.GDUnitPanel.Controls.Add(this.groupBox3);
            this.GDUnitPanel.Controls.Add(this.Maintenancegb);
            this.GDUnitPanel.Controls.Add(this.Currentgb);
            this.GDUnitPanel.Location = new System.Drawing.Point(19, 7);
            this.GDUnitPanel.Name = "GDUnitPanel";
            this.GDUnitPanel.Size = new System.Drawing.Size(713, 507);
            this.GDUnitPanel.TabIndex = 32;
            this.GDUnitPanel.Visible = false;
            // 
            // GDUnitMaintenanceGB
            // 
            this.GDUnitMaintenanceGB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GDUnitMaintenanceGB.BackColor = System.Drawing.Color.Beige;
            this.GDUnitMaintenanceGB.Controls.Add(this.panel23);
            this.GDUnitMaintenanceGB.Controls.Add(this.label96);
            this.GDUnitMaintenanceGB.Controls.Add(this.panel22);
            this.GDUnitMaintenanceGB.Controls.Add(this.label92);
            this.GDUnitMaintenanceGB.Controls.Add(this.label94);
            this.GDUnitMaintenanceGB.Controls.Add(this.label91);
            this.GDUnitMaintenanceGB.Controls.Add(this.panel20);
            this.GDUnitMaintenanceGB.Controls.Add(this.panel84);
            this.GDUnitMaintenanceGB.Controls.Add(this.panel21);
            this.GDUnitMaintenanceGB.Controls.Add(this.panel86);
            this.GDUnitMaintenanceGB.Controls.Add(this.panel178);
            this.GDUnitMaintenanceGB.Controls.Add(this.panel88);
            this.GDUnitMaintenanceGB.Controls.Add(this.panel87);
            this.GDUnitMaintenanceGB.Controls.Add(this.label22);
            this.GDUnitMaintenanceGB.Controls.Add(this.label80);
            this.GDUnitMaintenanceGB.Controls.Add(this.label236);
            this.GDUnitMaintenanceGB.Controls.Add(this.panel82);
            this.GDUnitMaintenanceGB.Controls.Add(this.panel83);
            this.GDUnitMaintenanceGB.Controls.Add(this.panel85);
            this.GDUnitMaintenanceGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GDUnitMaintenanceGB.ForeColor = System.Drawing.SystemColors.WindowText;
            this.GDUnitMaintenanceGB.Location = new System.Drawing.Point(33, 234);
            this.GDUnitMaintenanceGB.Name = "GDUnitMaintenanceGB";
            this.GDUnitMaintenanceGB.Size = new System.Drawing.Size(632, 245);
            this.GDUnitMaintenanceGB.TabIndex = 6;
            this.GDUnitMaintenanceGB.TabStop = false;
            this.GDUnitMaintenanceGB.Text = "MAINTENANCE";
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.CadetBlue;
            this.panel23.Controls.Add(this.txtMaintStartUpFactor);
            this.panel23.Controls.Add(this.label95);
            this.panel23.Location = new System.Drawing.Point(127, 185);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(80, 49);
            this.panel23.TabIndex = 32;
            // 
            // txtMaintStartUpFactor
            // 
            this.txtMaintStartUpFactor.BackColor = System.Drawing.Color.White;
            this.txtMaintStartUpFactor.Location = new System.Drawing.Point(7, 22);
            this.txtMaintStartUpFactor.Name = "txtMaintStartUpFactor";
            this.txtMaintStartUpFactor.ReadOnly = true;
            this.txtMaintStartUpFactor.Size = new System.Drawing.Size(62, 20);
            this.txtMaintStartUpFactor.TabIndex = 13;
            this.txtMaintStartUpFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMaintStartUpFactor.Validated += new System.EventHandler(this.txtMaintStartUpFactor_Validated);
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.ForeColor = System.Drawing.SystemColors.Window;
            this.label95.Location = new System.Drawing.Point(16, 6);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(37, 13);
            this.label95.TabIndex = 0;
            this.label95.Text = "Ratio";
            // 
            // label96
            // 
            this.label96.BackColor = System.Drawing.Color.Transparent;
            this.label96.Location = new System.Drawing.Point(11, 209);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(106, 20);
            this.label96.TabIndex = 33;
            this.label96.Text = "Startup Factor";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.CadetBlue;
            this.panel22.Controls.Add(this.txtMaintFuelFactor);
            this.panel22.Controls.Add(this.label93);
            this.panel22.Location = new System.Drawing.Point(128, 130);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(80, 49);
            this.panel22.TabIndex = 30;
            // 
            // txtMaintFuelFactor
            // 
            this.txtMaintFuelFactor.BackColor = System.Drawing.Color.White;
            this.txtMaintFuelFactor.Location = new System.Drawing.Point(7, 22);
            this.txtMaintFuelFactor.Name = "txtMaintFuelFactor";
            this.txtMaintFuelFactor.ReadOnly = true;
            this.txtMaintFuelFactor.Size = new System.Drawing.Size(62, 20);
            this.txtMaintFuelFactor.TabIndex = 13;
            this.txtMaintFuelFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMaintFuelFactor.Validated += new System.EventHandler(this.txtMaintFuelFactor_Validated);
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.ForeColor = System.Drawing.SystemColors.Window;
            this.label93.Location = new System.Drawing.Point(16, 6);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(37, 13);
            this.label93.TabIndex = 0;
            this.label93.Text = "Ratio";
            // 
            // label92
            // 
            this.label92.BackColor = System.Drawing.Color.Transparent;
            this.label92.Location = new System.Drawing.Point(22, 96);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(81, 20);
            this.label92.TabIndex = 29;
            this.label92.Text = "Last Done";
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label94
            // 
            this.label94.BackColor = System.Drawing.Color.Transparent;
            this.label94.Location = new System.Drawing.Point(22, 154);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(81, 20);
            this.label94.TabIndex = 31;
            this.label94.Text = "Fuel Factor";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label91
            // 
            this.label91.BackColor = System.Drawing.Color.Transparent;
            this.label91.Location = new System.Drawing.Point(22, 43);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(81, 20);
            this.label91.TabIndex = 28;
            this.label91.Text = "Durability";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.CadetBlue;
            this.panel20.Controls.Add(this.cmbMaintLastMaintType);
            this.panel20.Controls.Add(this.label77);
            this.panel20.Location = new System.Drawing.Point(218, 76);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(123, 49);
            this.panel20.TabIndex = 27;
            // 
            // cmbMaintLastMaintType
            // 
            this.cmbMaintLastMaintType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMaintLastMaintType.FormattingEnabled = true;
            this.cmbMaintLastMaintType.Location = new System.Drawing.Point(7, 20);
            this.cmbMaintLastMaintType.Name = "cmbMaintLastMaintType";
            this.cmbMaintLastMaintType.Size = new System.Drawing.Size(108, 21);
            this.cmbMaintLastMaintType.TabIndex = 1;
            this.cmbMaintLastMaintType.Validated += new System.EventHandler(this.cmbMaintLastMaintType_Validated);
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.ForeColor = System.Drawing.SystemColors.Window;
            this.label77.Location = new System.Drawing.Point(44, 4);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(35, 13);
            this.label77.TabIndex = 0;
            this.label77.Text = "Type";
            // 
            // panel84
            // 
            this.panel84.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel84.BackColor = System.Drawing.Color.CadetBlue;
            this.panel84.Controls.Add(this.txtMaintMOHours);
            this.panel84.Controls.Add(this.label240);
            this.panel84.Location = new System.Drawing.Point(405, 130);
            this.panel84.Name = "panel84";
            this.panel84.Size = new System.Drawing.Size(79, 49);
            this.panel84.TabIndex = 19;
            // 
            // txtMaintMOHours
            // 
            this.txtMaintMOHours.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaintMOHours.BackColor = System.Drawing.Color.White;
            this.txtMaintMOHours.Location = new System.Drawing.Point(7, 22);
            this.txtMaintMOHours.Name = "txtMaintMOHours";
            this.txtMaintMOHours.ReadOnly = true;
            this.txtMaintMOHours.Size = new System.Drawing.Size(61, 20);
            this.txtMaintMOHours.TabIndex = 13;
            this.txtMaintMOHours.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMaintMOHours.Validated += new System.EventHandler(this.txtMaintMOHours_Validated);
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.ForeColor = System.Drawing.SystemColors.Window;
            this.label240.Location = new System.Drawing.Point(19, 6);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(40, 13);
            this.label240.TabIndex = 0;
            this.label240.Text = "Hours";
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.CadetBlue;
            this.panel21.Controls.Add(this.txtMaintLastHour);
            this.panel21.Controls.Add(this.label89);
            this.panel21.Location = new System.Drawing.Point(129, 75);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(80, 49);
            this.panel21.TabIndex = 26;
            // 
            // txtMaintLastHour
            // 
            this.txtMaintLastHour.BackColor = System.Drawing.Color.White;
            this.txtMaintLastHour.Location = new System.Drawing.Point(7, 22);
            this.txtMaintLastHour.Name = "txtMaintLastHour";
            this.txtMaintLastHour.ReadOnly = true;
            this.txtMaintLastHour.Size = new System.Drawing.Size(62, 20);
            this.txtMaintLastHour.TabIndex = 13;
            this.txtMaintLastHour.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMaintLastHour.Validated += new System.EventHandler(this.txtMaintLastHour_Validated);
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.ForeColor = System.Drawing.SystemColors.Window;
            this.label89.Location = new System.Drawing.Point(16, 6);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(40, 13);
            this.label89.TabIndex = 0;
            this.label89.Text = "Hours";
            // 
            // panel86
            // 
            this.panel86.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel86.BackColor = System.Drawing.Color.CadetBlue;
            this.panel86.Controls.Add(this.txtMaintHGPHours);
            this.panel86.Controls.Add(this.label242);
            this.panel86.Location = new System.Drawing.Point(405, 75);
            this.panel86.Name = "panel86";
            this.panel86.Size = new System.Drawing.Size(79, 49);
            this.panel86.TabIndex = 16;
            // 
            // txtMaintHGPHours
            // 
            this.txtMaintHGPHours.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaintHGPHours.BackColor = System.Drawing.Color.White;
            this.txtMaintHGPHours.Location = new System.Drawing.Point(7, 22);
            this.txtMaintHGPHours.Name = "txtMaintHGPHours";
            this.txtMaintHGPHours.ReadOnly = true;
            this.txtMaintHGPHours.Size = new System.Drawing.Size(61, 20);
            this.txtMaintHGPHours.TabIndex = 13;
            this.txtMaintHGPHours.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMaintHGPHours.Validated += new System.EventHandler(this.txtMaintHGPHours_Validated);
            // 
            // label242
            // 
            this.label242.AutoSize = true;
            this.label242.ForeColor = System.Drawing.SystemColors.Window;
            this.label242.Location = new System.Drawing.Point(19, 6);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(40, 13);
            this.label242.TabIndex = 0;
            this.label242.Text = "Hours";
            // 
            // panel178
            // 
            this.panel178.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel178.BackColor = System.Drawing.Color.CadetBlue;
            this.panel178.Controls.Add(this.txtMaintCIHours);
            this.panel178.Controls.Add(this.label243);
            this.panel178.Location = new System.Drawing.Point(405, 21);
            this.panel178.Name = "panel178";
            this.panel178.Size = new System.Drawing.Size(80, 49);
            this.panel178.TabIndex = 2;
            // 
            // txtMaintCIHours
            // 
            this.txtMaintCIHours.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaintCIHours.BackColor = System.Drawing.Color.White;
            this.txtMaintCIHours.Location = new System.Drawing.Point(7, 22);
            this.txtMaintCIHours.Name = "txtMaintCIHours";
            this.txtMaintCIHours.ReadOnly = true;
            this.txtMaintCIHours.Size = new System.Drawing.Size(62, 20);
            this.txtMaintCIHours.TabIndex = 13;
            this.txtMaintCIHours.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMaintCIHours.Validated += new System.EventHandler(this.txtMaintCIHours_Validated);
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.ForeColor = System.Drawing.SystemColors.Window;
            this.label243.Location = new System.Drawing.Point(20, 6);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(40, 13);
            this.label243.TabIndex = 0;
            this.label243.Text = "Hours";
            // 
            // panel88
            // 
            this.panel88.BackColor = System.Drawing.Color.CadetBlue;
            this.panel88.Controls.Add(this.txtDurabilityHours);
            this.panel88.Controls.Add(this.label83);
            this.panel88.Location = new System.Drawing.Point(129, 20);
            this.panel88.Name = "panel88";
            this.panel88.Size = new System.Drawing.Size(80, 49);
            this.panel88.TabIndex = 20;
            // 
            // txtDurabilityHours
            // 
            this.txtDurabilityHours.BackColor = System.Drawing.Color.White;
            this.txtDurabilityHours.Location = new System.Drawing.Point(7, 22);
            this.txtDurabilityHours.Name = "txtDurabilityHours";
            this.txtDurabilityHours.ReadOnly = true;
            this.txtDurabilityHours.Size = new System.Drawing.Size(62, 20);
            this.txtDurabilityHours.TabIndex = 13;
            this.txtDurabilityHours.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDurabilityHours.Validated += new System.EventHandler(this.txtDurabilityHours_Validated);
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.ForeColor = System.Drawing.SystemColors.Window;
            this.label83.Location = new System.Drawing.Point(16, 6);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(40, 13);
            this.label83.TabIndex = 0;
            this.label83.Text = "Hours";
            // 
            // panel87
            // 
            this.panel87.BackColor = System.Drawing.Color.CadetBlue;
            this.panel87.Controls.Add(this.datePickerDurability);
            this.panel87.Controls.Add(this.label82);
            this.panel87.Location = new System.Drawing.Point(218, 21);
            this.panel87.Name = "panel87";
            this.panel87.Size = new System.Drawing.Size(123, 49);
            this.panel87.TabIndex = 25;
            // 
            // datePickerDurability
            // 
            this.datePickerDurability.HasButtons = true;
            this.datePickerDurability.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.datePickerDurability.Location = new System.Drawing.Point(3, 21);
            this.datePickerDurability.Name = "datePickerDurability";
            this.datePickerDurability.Size = new System.Drawing.Size(116, 20);
            this.datePickerDurability.TabIndex = 14;
            this.datePickerDurability.Validated += new System.EventHandler(this.datePickerDurability_Validated);
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.ForeColor = System.Drawing.SystemColors.Window;
            this.label82.Location = new System.Drawing.Point(26, 4);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(65, 13);
            this.label82.TabIndex = 0;
            this.label82.Text = "Date Time";
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Location = new System.Drawing.Point(350, 150);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(51, 20);
            this.label22.TabIndex = 24;
            this.label22.Text = " MO";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label80
            // 
            this.label80.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label80.BackColor = System.Drawing.Color.Transparent;
            this.label80.Location = new System.Drawing.Point(351, 99);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(51, 20);
            this.label80.TabIndex = 22;
            this.label80.Text = " HGP";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label236
            // 
            this.label236.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label236.BackColor = System.Drawing.Color.Transparent;
            this.label236.Location = new System.Drawing.Point(351, 44);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(51, 20);
            this.label236.TabIndex = 1;
            this.label236.Text = "CI";
            this.label236.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel82
            // 
            this.panel82.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel82.BackColor = System.Drawing.Color.CadetBlue;
            this.panel82.Controls.Add(this.txtMaintMODur);
            this.panel82.Controls.Add(this.label238);
            this.panel82.Location = new System.Drawing.Point(501, 130);
            this.panel82.Name = "panel82";
            this.panel82.Size = new System.Drawing.Size(95, 49);
            this.panel82.TabIndex = 20;
            // 
            // txtMaintMODur
            // 
            this.txtMaintMODur.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaintMODur.BackColor = System.Drawing.Color.White;
            this.txtMaintMODur.Location = new System.Drawing.Point(9, 22);
            this.txtMaintMODur.Name = "txtMaintMODur";
            this.txtMaintMODur.ReadOnly = true;
            this.txtMaintMODur.Size = new System.Drawing.Size(71, 20);
            this.txtMaintMODur.TabIndex = 14;
            this.txtMaintMODur.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMaintMODur.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtMaintMODur_MouseClick);
            this.txtMaintMODur.Leave += new System.EventHandler(this.txtMaintMODur_Leave);
            this.txtMaintMODur.Validated += new System.EventHandler(this.txtMaintMODur_Validated);
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.ForeColor = System.Drawing.SystemColors.Window;
            this.label238.Location = new System.Drawing.Point(20, 6);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(55, 13);
            this.label238.TabIndex = 0;
            this.label238.Text = "Duration";
            // 
            // panel83
            // 
            this.panel83.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel83.BackColor = System.Drawing.Color.CadetBlue;
            this.panel83.Controls.Add(this.txtMaintHGPDur);
            this.panel83.Controls.Add(this.label239);
            this.panel83.Location = new System.Drawing.Point(501, 76);
            this.panel83.Name = "panel83";
            this.panel83.Size = new System.Drawing.Size(95, 49);
            this.panel83.TabIndex = 17;
            // 
            // txtMaintHGPDur
            // 
            this.txtMaintHGPDur.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaintHGPDur.BackColor = System.Drawing.Color.White;
            this.txtMaintHGPDur.Location = new System.Drawing.Point(12, 22);
            this.txtMaintHGPDur.Name = "txtMaintHGPDur";
            this.txtMaintHGPDur.ReadOnly = true;
            this.txtMaintHGPDur.Size = new System.Drawing.Size(71, 20);
            this.txtMaintHGPDur.TabIndex = 14;
            this.txtMaintHGPDur.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMaintHGPDur.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtMaintHGPDur_MouseClick);
            this.txtMaintHGPDur.Leave += new System.EventHandler(this.txtMaintHGPDur_Leave);
            this.txtMaintHGPDur.Validated += new System.EventHandler(this.txtMaintHGPDur_Validated);
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.ForeColor = System.Drawing.SystemColors.Window;
            this.label239.Location = new System.Drawing.Point(16, 6);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(55, 13);
            this.label239.TabIndex = 0;
            this.label239.Text = "Duration";
            // 
            // panel85
            // 
            this.panel85.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel85.BackColor = System.Drawing.Color.CadetBlue;
            this.panel85.Controls.Add(this.txtMaintCIDur);
            this.panel85.Controls.Add(this.label241);
            this.panel85.Location = new System.Drawing.Point(502, 21);
            this.panel85.Name = "panel85";
            this.panel85.Size = new System.Drawing.Size(95, 49);
            this.panel85.TabIndex = 3;
            // 
            // txtMaintCIDur
            // 
            this.txtMaintCIDur.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaintCIDur.BackColor = System.Drawing.Color.White;
            this.txtMaintCIDur.Location = new System.Drawing.Point(12, 22);
            this.txtMaintCIDur.Name = "txtMaintCIDur";
            this.txtMaintCIDur.ReadOnly = true;
            this.txtMaintCIDur.Size = new System.Drawing.Size(70, 20);
            this.txtMaintCIDur.TabIndex = 14;
            this.txtMaintCIDur.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMaintCIDur.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtMaintCIDur_MouseClick);
            this.txtMaintCIDur.Leave += new System.EventHandler(this.txtMaintCIDur_Leave);
            this.txtMaintCIDur.Validated += new System.EventHandler(this.txtMaintCIDur_Validated);
            // 
            // label241
            // 
            this.label241.AutoSize = true;
            this.label241.ForeColor = System.Drawing.SystemColors.Window;
            this.label241.Location = new System.Drawing.Point(20, 6);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(55, 13);
            this.label241.TabIndex = 0;
            this.label241.Text = "Duration";
            // 
            // Countergb
            // 
            this.Countergb.BackColor = System.Drawing.Color.Beige;
            this.Countergb.Controls.Add(this.panel89);
            this.Countergb.Controls.Add(this.panel91);
            this.Countergb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Countergb.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Countergb.Location = new System.Drawing.Point(410, 10);
            this.Countergb.Name = "Countergb";
            this.Countergb.Size = new System.Drawing.Size(255, 76);
            this.Countergb.TabIndex = 7;
            this.Countergb.TabStop = false;
            this.Countergb.Text = "COUNTER SERIALS";
            // 
            // panel89
            // 
            this.panel89.BackColor = System.Drawing.Color.CadetBlue;
            this.panel89.Controls.Add(this.GDConsumedSerialTb);
            this.panel89.Controls.Add(this.label97);
            this.panel89.Location = new System.Drawing.Point(137, 18);
            this.panel89.Name = "panel89";
            this.panel89.Size = new System.Drawing.Size(105, 49);
            this.panel89.TabIndex = 1;
            // 
            // GDConsumedSerialTb
            // 
            this.GDConsumedSerialTb.BackColor = System.Drawing.Color.White;
            this.GDConsumedSerialTb.Location = new System.Drawing.Point(15, 22);
            this.GDConsumedSerialTb.Name = "GDConsumedSerialTb";
            this.GDConsumedSerialTb.ReadOnly = true;
            this.GDConsumedSerialTb.Size = new System.Drawing.Size(75, 20);
            this.GDConsumedSerialTb.TabIndex = 0;
            this.GDConsumedSerialTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.ForeColor = System.Drawing.SystemColors.Window;
            this.label97.Location = new System.Drawing.Point(1, 4);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(101, 13);
            this.label97.TabIndex = 0;
            this.label97.Text = "Consumed Serial";
            // 
            // panel91
            // 
            this.panel91.BackColor = System.Drawing.Color.CadetBlue;
            this.panel91.Controls.Add(this.GDPowerSerialTb);
            this.panel91.Controls.Add(this.label99);
            this.panel91.Location = new System.Drawing.Point(16, 19);
            this.panel91.Name = "panel91";
            this.panel91.Size = new System.Drawing.Size(104, 49);
            this.panel91.TabIndex = 0;
            // 
            // GDPowerSerialTb
            // 
            this.GDPowerSerialTb.BackColor = System.Drawing.Color.White;
            this.GDPowerSerialTb.Location = new System.Drawing.Point(15, 22);
            this.GDPowerSerialTb.Name = "GDPowerSerialTb";
            this.GDPowerSerialTb.ReadOnly = true;
            this.GDPowerSerialTb.Size = new System.Drawing.Size(75, 20);
            this.GDPowerSerialTb.TabIndex = 0;
            this.GDPowerSerialTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.ForeColor = System.Drawing.SystemColors.Window;
            this.label99.Location = new System.Drawing.Point(13, 6);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(78, 13);
            this.label99.TabIndex = 0;
            this.label99.Text = "Power Serial";
            // 
            // Anonygb
            // 
            this.Anonygb.BackColor = System.Drawing.Color.Beige;
            this.Anonygb.Controls.Add(this.panelAvc);
            this.Anonygb.Controls.Add(this.GDSecondaryFuelPanel);
            this.Anonygb.Controls.Add(this.GDPrimaryFuelPanel);
            this.Anonygb.Controls.Add(this.panel10);
            this.Anonygb.Controls.Add(this.panel9);
            this.Anonygb.Controls.Add(this.panel8);
            this.Anonygb.Controls.Add(this.panel7);
            this.Anonygb.Controls.Add(this.panel6);
            this.Anonygb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Anonygb.Location = new System.Drawing.Point(33, 92);
            this.Anonygb.Name = "Anonygb";
            this.Anonygb.Size = new System.Drawing.Size(632, 129);
            this.Anonygb.TabIndex = 1;
            this.Anonygb.TabStop = false;
            // 
            // panelAvc
            // 
            this.panelAvc.BackColor = System.Drawing.Color.CadetBlue;
            this.panelAvc.Controls.Add(this.GDtxtAvc);
            this.panelAvc.Controls.Add(this.lblAvc);
            this.panelAvc.Location = new System.Drawing.Point(448, 74);
            this.panelAvc.Name = "panelAvc";
            this.panelAvc.Size = new System.Drawing.Size(107, 49);
            this.panelAvc.TabIndex = 7;
            // 
            // GDtxtAvc
            // 
            this.GDtxtAvc.BackColor = System.Drawing.Color.White;
            this.GDtxtAvc.Location = new System.Drawing.Point(7, 22);
            this.GDtxtAvc.Name = "GDtxtAvc";
            this.GDtxtAvc.ReadOnly = true;
            this.GDtxtAvc.Size = new System.Drawing.Size(93, 20);
            this.GDtxtAvc.TabIndex = 0;
            this.GDtxtAvc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDtxtAvc.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GDtxtAvc_MouseClick);
            this.GDtxtAvc.Leave += new System.EventHandler(this.GDtxtAvc_Leave);
            this.GDtxtAvc.Validated += new System.EventHandler(this.GDtxtAvc_Validated);
            // 
            // lblAvc
            // 
            this.lblAvc.AutoSize = true;
            this.lblAvc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvc.ForeColor = System.Drawing.SystemColors.Window;
            this.lblAvc.Location = new System.Drawing.Point(38, 4);
            this.lblAvc.Name = "lblAvc";
            this.lblAvc.Size = new System.Drawing.Size(31, 13);
            this.lblAvc.TabIndex = 0;
            this.lblAvc.Text = "AVC";
            // 
            // GDSecondaryFuelPanel
            // 
            this.GDSecondaryFuelPanel.BackColor = System.Drawing.Color.CadetBlue;
            this.GDSecondaryFuelPanel.Controls.Add(this.GDSecondaryFuelTB);
            this.GDSecondaryFuelPanel.Controls.Add(this.label84);
            this.GDSecondaryFuelPanel.Location = new System.Drawing.Point(263, 74);
            this.GDSecondaryFuelPanel.Name = "GDSecondaryFuelPanel";
            this.GDSecondaryFuelPanel.Size = new System.Drawing.Size(171, 49);
            this.GDSecondaryFuelPanel.TabIndex = 6;
            // 
            // GDSecondaryFuelTB
            // 
            this.GDSecondaryFuelTB.BackColor = System.Drawing.Color.White;
            this.GDSecondaryFuelTB.Location = new System.Drawing.Point(39, 22);
            this.GDSecondaryFuelTB.Name = "GDSecondaryFuelTB";
            this.GDSecondaryFuelTB.ReadOnly = true;
            this.GDSecondaryFuelTB.Size = new System.Drawing.Size(93, 20);
            this.GDSecondaryFuelTB.TabIndex = 0;
            this.GDSecondaryFuelTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDSecondaryFuelTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GDSecondaryFuelTB_MouseClick);
            this.GDSecondaryFuelTB.Leave += new System.EventHandler(this.GDSecondaryFuelTB_Leave);
            this.GDSecondaryFuelTB.Validated += new System.EventHandler(this.GDSecondaryFuelTB_Validated);
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.SystemColors.Window;
            this.label84.Location = new System.Drawing.Point(3, 4);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(162, 13);
            this.label84.TabIndex = 0;
            this.label84.Text = "Heat Value Secondary Fuel";
            // 
            // GDPrimaryFuelPanel
            // 
            this.GDPrimaryFuelPanel.BackColor = System.Drawing.Color.CadetBlue;
            this.GDPrimaryFuelPanel.Controls.Add(this.GDPrimaryFuelTB);
            this.GDPrimaryFuelPanel.Controls.Add(this.GDConsLb);
            this.GDPrimaryFuelPanel.Location = new System.Drawing.Point(77, 74);
            this.GDPrimaryFuelPanel.Name = "GDPrimaryFuelPanel";
            this.GDPrimaryFuelPanel.Size = new System.Drawing.Size(171, 49);
            this.GDPrimaryFuelPanel.TabIndex = 5;
            // 
            // GDPrimaryFuelTB
            // 
            this.GDPrimaryFuelTB.BackColor = System.Drawing.Color.White;
            this.GDPrimaryFuelTB.Location = new System.Drawing.Point(34, 22);
            this.GDPrimaryFuelTB.Name = "GDPrimaryFuelTB";
            this.GDPrimaryFuelTB.ReadOnly = true;
            this.GDPrimaryFuelTB.Size = new System.Drawing.Size(100, 20);
            this.GDPrimaryFuelTB.TabIndex = 0;
            this.GDPrimaryFuelTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDPrimaryFuelTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GDPrimaryFuelTB_MouseClick);
            this.GDPrimaryFuelTB.Leave += new System.EventHandler(this.GDPrimaryFuelTB_Leave);
            this.GDPrimaryFuelTB.Validated += new System.EventHandler(this.GDPrimaryFuelTB_Validated);
            // 
            // GDConsLb
            // 
            this.GDConsLb.AutoSize = true;
            this.GDConsLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GDConsLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDConsLb.Location = new System.Drawing.Point(10, 4);
            this.GDConsLb.Name = "GDConsLb";
            this.GDConsLb.Size = new System.Drawing.Size(143, 13);
            this.GDConsLb.TabIndex = 0;
            this.GDConsLb.Text = "Heat Value Primary Fuel";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.CadetBlue;
            this.panel10.Controls.Add(this.GDRampRateTB);
            this.panel10.Controls.Add(this.GDRampLb);
            this.panel10.Location = new System.Drawing.Point(504, 19);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(95, 49);
            this.panel10.TabIndex = 4;
            // 
            // GDRampRateTB
            // 
            this.GDRampRateTB.BackColor = System.Drawing.Color.White;
            this.GDRampRateTB.Location = new System.Drawing.Point(9, 22);
            this.GDRampRateTB.Name = "GDRampRateTB";
            this.GDRampRateTB.ReadOnly = true;
            this.GDRampRateTB.Size = new System.Drawing.Size(75, 20);
            this.GDRampRateTB.TabIndex = 0;
            this.GDRampRateTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDRampRateTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GDRampRateTB_MouseClick);
            this.GDRampRateTB.Leave += new System.EventHandler(this.GDRampRateTB_Leave);
            this.GDRampRateTB.Validated += new System.EventHandler(this.GDRampRateTB_Validated);
            // 
            // GDRampLb
            // 
            this.GDRampLb.AutoSize = true;
            this.GDRampLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GDRampLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDRampLb.Location = new System.Drawing.Point(10, 4);
            this.GDRampLb.Name = "GDRampLb";
            this.GDRampLb.Size = new System.Drawing.Size(66, 13);
            this.GDRampLb.TabIndex = 0;
            this.GDRampLb.Text = "RampRate";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.CadetBlue;
            this.panel9.Controls.Add(this.GDTimeHotStartTB);
            this.panel9.Controls.Add(this.GDHotLb);
            this.panel9.Location = new System.Drawing.Point(387, 19);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(98, 49);
            this.panel9.TabIndex = 3;
            // 
            // GDTimeHotStartTB
            // 
            this.GDTimeHotStartTB.BackColor = System.Drawing.Color.White;
            this.GDTimeHotStartTB.Location = new System.Drawing.Point(6, 22);
            this.GDTimeHotStartTB.Name = "GDTimeHotStartTB";
            this.GDTimeHotStartTB.ReadOnly = true;
            this.GDTimeHotStartTB.Size = new System.Drawing.Size(79, 20);
            this.GDTimeHotStartTB.TabIndex = 0;
            this.GDTimeHotStartTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDTimeHotStartTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GDTimeHotStartTB_MouseClick);
            this.GDTimeHotStartTB.Leave += new System.EventHandler(this.GDTimeHotStartTB_Leave);
            this.GDTimeHotStartTB.Validated += new System.EventHandler(this.GDTimeHotStartTB_Validated);
            // 
            // GDHotLb
            // 
            this.GDHotLb.AutoSize = true;
            this.GDHotLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GDHotLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDHotLb.Location = new System.Drawing.Point(2, 4);
            this.GDHotLb.Name = "GDHotLb";
            this.GDHotLb.Size = new System.Drawing.Size(89, 13);
            this.GDHotLb.TabIndex = 0;
            this.GDHotLb.Text = "Time Hot Start";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.CadetBlue;
            this.panel8.Controls.Add(this.GDTimeColdStartTB);
            this.panel8.Controls.Add(this.GDColdLb);
            this.panel8.Location = new System.Drawing.Point(266, 19);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(101, 49);
            this.panel8.TabIndex = 2;
            // 
            // GDTimeColdStartTB
            // 
            this.GDTimeColdStartTB.BackColor = System.Drawing.Color.White;
            this.GDTimeColdStartTB.Location = new System.Drawing.Point(8, 21);
            this.GDTimeColdStartTB.Name = "GDTimeColdStartTB";
            this.GDTimeColdStartTB.ReadOnly = true;
            this.GDTimeColdStartTB.Size = new System.Drawing.Size(82, 20);
            this.GDTimeColdStartTB.TabIndex = 0;
            this.GDTimeColdStartTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDTimeColdStartTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GDTimeColdStartTB_MouseClick);
            this.GDTimeColdStartTB.Leave += new System.EventHandler(this.GDTimeColdStartTB_Leave);
            this.GDTimeColdStartTB.Validated += new System.EventHandler(this.GDTimeColdStartTB_Validated);
            // 
            // GDColdLb
            // 
            this.GDColdLb.AutoSize = true;
            this.GDColdLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GDColdLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDColdLb.Location = new System.Drawing.Point(3, 3);
            this.GDColdLb.Name = "GDColdLb";
            this.GDColdLb.Size = new System.Drawing.Size(94, 13);
            this.GDColdLb.TabIndex = 0;
            this.GDColdLb.Text = "Time Cold Start";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.CadetBlue;
            this.panel7.Controls.Add(this.GDTimeDownTB);
            this.panel7.Controls.Add(this.GDTimeDownLb);
            this.panel7.Location = new System.Drawing.Point(150, 19);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(95, 49);
            this.panel7.TabIndex = 1;
            // 
            // GDTimeDownTB
            // 
            this.GDTimeDownTB.BackColor = System.Drawing.Color.White;
            this.GDTimeDownTB.Location = new System.Drawing.Point(9, 22);
            this.GDTimeDownTB.Name = "GDTimeDownTB";
            this.GDTimeDownTB.ReadOnly = true;
            this.GDTimeDownTB.Size = new System.Drawing.Size(75, 20);
            this.GDTimeDownTB.TabIndex = 0;
            this.GDTimeDownTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDTimeDownTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GDTimeDownTB_MouseClick);
            this.GDTimeDownTB.Leave += new System.EventHandler(this.GDTimeDownTB_Leave);
            this.GDTimeDownTB.Validated += new System.EventHandler(this.GDTimeDownTB_Validated);
            // 
            // GDTimeDownLb
            // 
            this.GDTimeDownLb.AutoSize = true;
            this.GDTimeDownLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GDTimeDownLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDTimeDownLb.Location = new System.Drawing.Point(6, 4);
            this.GDTimeDownLb.Name = "GDTimeDownLb";
            this.GDTimeDownLb.Size = new System.Drawing.Size(70, 13);
            this.GDTimeDownLb.TabIndex = 0;
            this.GDTimeDownLb.Text = "Time Down";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.CadetBlue;
            this.panel6.Controls.Add(this.GDTimeUpTB);
            this.panel6.Controls.Add(this.GDTUpLb);
            this.panel6.Location = new System.Drawing.Point(32, 19);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(95, 49);
            this.panel6.TabIndex = 0;
            // 
            // GDTimeUpTB
            // 
            this.GDTimeUpTB.BackColor = System.Drawing.Color.White;
            this.GDTimeUpTB.Location = new System.Drawing.Point(9, 22);
            this.GDTimeUpTB.Name = "GDTimeUpTB";
            this.GDTimeUpTB.ReadOnly = true;
            this.GDTimeUpTB.Size = new System.Drawing.Size(75, 20);
            this.GDTimeUpTB.TabIndex = 0;
            this.GDTimeUpTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDTimeUpTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GDTimeUpTB_MouseClick);
            this.GDTimeUpTB.Leave += new System.EventHandler(this.GDTimeUpTB_Leave);
            this.GDTimeUpTB.Validated += new System.EventHandler(this.GDTimeUpTB_Validated);
            // 
            // GDTUpLb
            // 
            this.GDTUpLb.AutoSize = true;
            this.GDTUpLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GDTUpLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDTUpLb.Location = new System.Drawing.Point(17, 4);
            this.GDTUpLb.Name = "GDTUpLb";
            this.GDTUpLb.Size = new System.Drawing.Size(54, 13);
            this.GDTUpLb.TabIndex = 0;
            this.GDTUpLb.Text = "Time Up";
            // 
            // Powergb
            // 
            this.Powergb.BackColor = System.Drawing.Color.Beige;
            this.Powergb.Controls.Add(this.panel5);
            this.Powergb.Controls.Add(this.panel4);
            this.Powergb.Controls.Add(this.panel3);
            this.Powergb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Powergb.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Powergb.Location = new System.Drawing.Point(33, 10);
            this.Powergb.Name = "Powergb";
            this.Powergb.Size = new System.Drawing.Size(356, 76);
            this.Powergb.TabIndex = 0;
            this.Powergb.TabStop = false;
            this.Powergb.Text = "POWER";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.CadetBlue;
            this.panel5.Controls.Add(this.GDPminTB);
            this.panel5.Controls.Add(this.GDPminLb);
            this.panel5.Location = new System.Drawing.Point(238, 19);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(95, 49);
            this.panel5.TabIndex = 1;
            // 
            // GDPminTB
            // 
            this.GDPminTB.BackColor = System.Drawing.Color.White;
            this.GDPminTB.Location = new System.Drawing.Point(9, 22);
            this.GDPminTB.Name = "GDPminTB";
            this.GDPminTB.ReadOnly = true;
            this.GDPminTB.Size = new System.Drawing.Size(75, 20);
            this.GDPminTB.TabIndex = 0;
            this.GDPminTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDPminTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GDPminTB_MouseClick);
            this.GDPminTB.Leave += new System.EventHandler(this.GDPminTB_Leave);
            this.GDPminTB.Validated += new System.EventHandler(this.GDPminTB_Validated);
            // 
            // GDPminLb
            // 
            this.GDPminLb.AutoSize = true;
            this.GDPminLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDPminLb.Location = new System.Drawing.Point(26, 4);
            this.GDPminLb.Name = "GDPminLb";
            this.GDPminLb.Size = new System.Drawing.Size(34, 13);
            this.GDPminLb.TabIndex = 0;
            this.GDPminLb.Text = "Pmin";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.CadetBlue;
            this.panel4.Controls.Add(this.GDPmaxTB);
            this.panel4.Controls.Add(this.GDPmaxLb);
            this.panel4.Location = new System.Drawing.Point(124, 19);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(95, 49);
            this.panel4.TabIndex = 2;
            // 
            // GDPmaxTB
            // 
            this.GDPmaxTB.BackColor = System.Drawing.Color.White;
            this.GDPmaxTB.Location = new System.Drawing.Point(9, 22);
            this.GDPmaxTB.Name = "GDPmaxTB";
            this.GDPmaxTB.ReadOnly = true;
            this.GDPmaxTB.Size = new System.Drawing.Size(75, 20);
            this.GDPmaxTB.TabIndex = 0;
            this.GDPmaxTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDPmaxTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GDPmaxTB_MouseClick);
            this.GDPmaxTB.Leave += new System.EventHandler(this.GDPmaxTB_Leave);
            this.GDPmaxTB.Validated += new System.EventHandler(this.GDPmaxTB_Validated);
            // 
            // GDPmaxLb
            // 
            this.GDPmaxLb.AutoSize = true;
            this.GDPmaxLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDPmaxLb.Location = new System.Drawing.Point(26, 4);
            this.GDPmaxLb.Name = "GDPmaxLb";
            this.GDPmaxLb.Size = new System.Drawing.Size(37, 13);
            this.GDPmaxLb.TabIndex = 0;
            this.GDPmaxLb.Text = "Pmax";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.CadetBlue;
            this.panel3.Controls.Add(this.GDcapacityTB);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Location = new System.Drawing.Point(11, 19);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(95, 49);
            this.panel3.TabIndex = 0;
            // 
            // GDcapacityTB
            // 
            this.GDcapacityTB.BackColor = System.Drawing.Color.White;
            this.GDcapacityTB.Location = new System.Drawing.Point(9, 22);
            this.GDcapacityTB.Name = "GDcapacityTB";
            this.GDcapacityTB.ReadOnly = true;
            this.GDcapacityTB.Size = new System.Drawing.Size(75, 20);
            this.GDcapacityTB.TabIndex = 0;
            this.GDcapacityTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDcapacityTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GDcapacityTB_MouseClick);
            this.GDcapacityTB.Leave += new System.EventHandler(this.GDcapacityTB_Leave);
            this.GDcapacityTB.Validated += new System.EventHandler(this.GDcapacityTB_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.Window;
            this.label8.Location = new System.Drawing.Point(11, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "CAPACITY";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Beige;
            this.groupBox3.Controls.Add(this.panel25);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(546, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(119, 76);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "CounterSerial";
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.CadetBlue;
            this.panel25.Controls.Add(this.label100);
            this.panel25.Controls.Add(this.GDpowertransserialtb);
            this.panel25.Location = new System.Drawing.Point(8, 19);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(105, 49);
            this.panel25.TabIndex = 8;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label100.ForeColor = System.Drawing.Color.White;
            this.label100.Location = new System.Drawing.Point(-2, 4);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(109, 13);
            this.label100.TabIndex = 1;
            this.label100.Text = "power/trans serial";
            // 
            // GDpowertransserialtb
            // 
            this.GDpowertransserialtb.BackColor = System.Drawing.Color.White;
            this.GDpowertransserialtb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.GDpowertransserialtb.Location = new System.Drawing.Point(12, 21);
            this.GDpowertransserialtb.Name = "GDpowertransserialtb";
            this.GDpowertransserialtb.Size = new System.Drawing.Size(82, 20);
            this.GDpowertransserialtb.TabIndex = 0;
            // 
            // Maintenancegb
            // 
            this.Maintenancegb.BackColor = System.Drawing.Color.Beige;
            this.Maintenancegb.Controls.Add(this.GDOutServiceCheck);
            this.Maintenancegb.Controls.Add(this.panel16);
            this.Maintenancegb.Controls.Add(this.panel15);
            this.Maintenancegb.Controls.Add(this.panel14);
            this.Maintenancegb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Maintenancegb.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Maintenancegb.Location = new System.Drawing.Point(127, 367);
            this.Maintenancegb.Name = "Maintenancegb";
            this.Maintenancegb.Size = new System.Drawing.Size(471, 106);
            this.Maintenancegb.TabIndex = 3;
            this.Maintenancegb.TabStop = false;
            this.Maintenancegb.Text = "OutService";
            this.Maintenancegb.Visible = false;
            // 
            // GDOutServiceCheck
            // 
            this.GDOutServiceCheck.AutoSize = true;
            this.GDOutServiceCheck.Enabled = false;
            this.GDOutServiceCheck.Location = new System.Drawing.Point(34, 19);
            this.GDOutServiceCheck.Name = "GDOutServiceCheck";
            this.GDOutServiceCheck.Size = new System.Drawing.Size(89, 17);
            this.GDOutServiceCheck.TabIndex = 5;
            this.GDOutServiceCheck.Text = "OutService";
            this.GDOutServiceCheck.UseVisualStyleBackColor = true;
            this.GDOutServiceCheck.CheckedChanged += new System.EventHandler(this.GDOutServiceCheck_CheckedChanged);
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.CadetBlue;
            this.panel16.Controls.Add(this.GDEndDateCal);
            this.panel16.Controls.Add(this.label21);
            this.panel16.Location = new System.Drawing.Point(317, 44);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(123, 49);
            this.panel16.TabIndex = 3;
            // 
            // GDEndDateCal
            // 
            this.GDEndDateCal.Enabled = false;
            this.GDEndDateCal.HasButtons = true;
            this.GDEndDateCal.Location = new System.Drawing.Point(2, 25);
            this.GDEndDateCal.Name = "GDEndDateCal";
            this.GDEndDateCal.Readonly = true;
            this.GDEndDateCal.Size = new System.Drawing.Size(117, 20);
            this.GDEndDateCal.TabIndex = 14;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.SystemColors.Window;
            this.label21.Location = new System.Drawing.Point(26, 4);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "End Date";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.CadetBlue;
            this.panel15.Controls.Add(this.GDStartDateCal);
            this.panel15.Controls.Add(this.label20);
            this.panel15.Location = new System.Drawing.Point(172, 44);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(122, 49);
            this.panel15.TabIndex = 2;
            // 
            // GDStartDateCal
            // 
            this.GDStartDateCal.Enabled = false;
            this.GDStartDateCal.HasButtons = true;
            this.GDStartDateCal.Location = new System.Drawing.Point(4, 25);
            this.GDStartDateCal.Name = "GDStartDateCal";
            this.GDStartDateCal.Readonly = true;
            this.GDStartDateCal.Size = new System.Drawing.Size(114, 20);
            this.GDStartDateCal.TabIndex = 13;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.SystemColors.Window;
            this.label20.Location = new System.Drawing.Point(26, 4);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Start Date";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.CadetBlue;
            this.panel14.Controls.Add(this.GDMainTypeTB);
            this.panel14.Controls.Add(this.label19);
            this.panel14.Location = new System.Drawing.Point(31, 44);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(115, 49);
            this.panel14.TabIndex = 1;
            // 
            // GDMainTypeTB
            // 
            this.GDMainTypeTB.BackColor = System.Drawing.Color.White;
            this.GDMainTypeTB.Location = new System.Drawing.Point(6, 22);
            this.GDMainTypeTB.Name = "GDMainTypeTB";
            this.GDMainTypeTB.ReadOnly = true;
            this.GDMainTypeTB.Size = new System.Drawing.Size(103, 20);
            this.GDMainTypeTB.TabIndex = 12;
            this.GDMainTypeTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.SystemColors.Window;
            this.label19.Location = new System.Drawing.Point(42, 6);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(39, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "TYPE";
            // 
            // Currentgb
            // 
            this.Currentgb.BackColor = System.Drawing.Color.Beige;
            this.Currentgb.Controls.Add(this.panel13);
            this.Currentgb.Controls.Add(this.panel12);
            this.Currentgb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Currentgb.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Currentgb.Location = new System.Drawing.Point(242, 262);
            this.Currentgb.Name = "Currentgb";
            this.Currentgb.Size = new System.Drawing.Size(219, 86);
            this.Currentgb.TabIndex = 2;
            this.Currentgb.TabStop = false;
            this.Currentgb.Text = "CURRENT STATE";
            this.Currentgb.Visible = false;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.CadetBlue;
            this.panel13.Controls.Add(this.GDFuelTB);
            this.panel13.Controls.Add(this.GDFuelLb);
            this.panel13.Location = new System.Drawing.Point(118, 19);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(95, 49);
            this.panel13.TabIndex = 2;
            // 
            // GDFuelTB
            // 
            this.GDFuelTB.BackColor = System.Drawing.Color.White;
            this.GDFuelTB.Location = new System.Drawing.Point(9, 22);
            this.GDFuelTB.Name = "GDFuelTB";
            this.GDFuelTB.ReadOnly = true;
            this.GDFuelTB.Size = new System.Drawing.Size(75, 20);
            this.GDFuelTB.TabIndex = 11;
            this.GDFuelTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDFuelTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GDFuelTB_MouseClick);
            this.GDFuelTB.Leave += new System.EventHandler(this.GDFuelTB_Leave);
            // 
            // GDFuelLb
            // 
            this.GDFuelLb.AutoSize = true;
            this.GDFuelLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDFuelLb.Location = new System.Drawing.Point(26, 4);
            this.GDFuelLb.Name = "GDFuelLb";
            this.GDFuelLb.Size = new System.Drawing.Size(38, 13);
            this.GDFuelLb.TabIndex = 0;
            this.GDFuelLb.Text = "FUEL";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.CadetBlue;
            this.panel12.Controls.Add(this.GDStateTB);
            this.panel12.Controls.Add(this.GDStateLb);
            this.panel12.Location = new System.Drawing.Point(6, 19);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(95, 49);
            this.panel12.TabIndex = 1;
            // 
            // GDStateTB
            // 
            this.GDStateTB.BackColor = System.Drawing.Color.White;
            this.GDStateTB.Location = new System.Drawing.Point(9, 22);
            this.GDStateTB.Name = "GDStateTB";
            this.GDStateTB.ReadOnly = true;
            this.GDStateTB.Size = new System.Drawing.Size(75, 20);
            this.GDStateTB.TabIndex = 10;
            this.GDStateTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GDStateTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GDStateTB_MouseClick);
            this.GDStateTB.Leave += new System.EventHandler(this.GDStateTB_Leave);
            // 
            // GDStateLb
            // 
            this.GDStateLb.AutoSize = true;
            this.GDStateLb.ForeColor = System.Drawing.SystemColors.Window;
            this.GDStateLb.Location = new System.Drawing.Point(26, 4);
            this.GDStateLb.Name = "GDStateLb";
            this.GDStateLb.Size = new System.Drawing.Size(47, 13);
            this.GDStateLb.TabIndex = 0;
            this.GDStateLb.Text = "STATE";
            // 
            // GDHeaderGB
            // 
            this.GDHeaderGB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GDHeaderGB.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.GDHeaderGB.BackColor = System.Drawing.Color.Beige;
            this.GDHeaderGB.Controls.Add(this.GDPlantLb);
            this.GDHeaderGB.Controls.Add(this.GDHeaderPanel);
            this.GDHeaderGB.Controls.Add(this.L1);
            this.GDHeaderGB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GDHeaderGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.GDHeaderGB.ForeColor = System.Drawing.Color.Black;
            this.GDHeaderGB.Location = new System.Drawing.Point(15, 11);
            this.GDHeaderGB.Name = "GDHeaderGB";
            this.GDHeaderGB.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.GDHeaderGB.Size = new System.Drawing.Size(720, 53);
            this.GDHeaderGB.TabIndex = 19;
            this.GDHeaderGB.TabStop = false;
            this.GDHeaderGB.Visible = false;
            // 
            // GDPlantLb
            // 
            this.GDPlantLb.AutoSize = true;
            this.GDPlantLb.Location = new System.Drawing.Point(62, 23);
            this.GDPlantLb.Name = "GDPlantLb";
            this.GDPlantLb.Size = new System.Drawing.Size(0, 13);
            this.GDPlantLb.TabIndex = 9;
            // 
            // GDHeaderPanel
            // 
            this.GDHeaderPanel.Controls.Add(this.GDTypeLb);
            this.GDHeaderPanel.Controls.Add(this.GDUnitLb);
            this.GDHeaderPanel.Controls.Add(this.GDPackLb);
            this.GDHeaderPanel.Controls.Add(this.L4);
            this.GDHeaderPanel.Controls.Add(this.L3);
            this.GDHeaderPanel.Controls.Add(this.L2);
            this.GDHeaderPanel.Location = new System.Drawing.Point(144, 12);
            this.GDHeaderPanel.Name = "GDHeaderPanel";
            this.GDHeaderPanel.Size = new System.Drawing.Size(551, 35);
            this.GDHeaderPanel.TabIndex = 8;
            this.GDHeaderPanel.Visible = false;
            // 
            // GDTypeLb
            // 
            this.GDTypeLb.AutoSize = true;
            this.GDTypeLb.Location = new System.Drawing.Point(445, 11);
            this.GDTypeLb.Name = "GDTypeLb";
            this.GDTypeLb.Size = new System.Drawing.Size(0, 13);
            this.GDTypeLb.TabIndex = 13;
            // 
            // GDUnitLb
            // 
            this.GDUnitLb.AutoSize = true;
            this.GDUnitLb.Location = new System.Drawing.Point(281, 11);
            this.GDUnitLb.Name = "GDUnitLb";
            this.GDUnitLb.Size = new System.Drawing.Size(0, 13);
            this.GDUnitLb.TabIndex = 12;
            // 
            // GDPackLb
            // 
            this.GDPackLb.AutoSize = true;
            this.GDPackLb.Location = new System.Drawing.Point(118, 11);
            this.GDPackLb.Name = "GDPackLb";
            this.GDPackLb.Size = new System.Drawing.Size(0, 13);
            this.GDPackLb.TabIndex = 11;
            // 
            // L4
            // 
            this.L4.AutoSize = true;
            this.L4.BackColor = System.Drawing.Color.Transparent;
            this.L4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L4.Location = new System.Drawing.Point(402, 11);
            this.L4.Name = "L4";
            this.L4.Size = new System.Drawing.Size(47, 13);
            this.L4.TabIndex = 10;
            this.L4.Text = "Type : ";
            // 
            // L3
            // 
            this.L3.AutoSize = true;
            this.L3.BackColor = System.Drawing.Color.Transparent;
            this.L3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L3.Location = new System.Drawing.Point(234, 11);
            this.L3.Name = "L3";
            this.L3.Size = new System.Drawing.Size(42, 13);
            this.L3.TabIndex = 9;
            this.L3.Text = "Unit : ";
            // 
            // L2
            // 
            this.L2.AutoSize = true;
            this.L2.BackColor = System.Drawing.Color.Transparent;
            this.L2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L2.Location = new System.Drawing.Point(45, 11);
            this.L2.Name = "L2";
            this.L2.Size = new System.Drawing.Size(69, 13);
            this.L2.TabIndex = 8;
            this.L2.Text = "Package : ";
            // 
            // L1
            // 
            this.L1.AutoSize = true;
            this.L1.BackColor = System.Drawing.Color.Transparent;
            this.L1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.L1.Location = new System.Drawing.Point(10, 23);
            this.L1.Name = "L1";
            this.L1.Size = new System.Drawing.Size(48, 13);
            this.L1.TabIndex = 7;
            this.L1.Text = "Plant : ";
            this.L1.Visible = false;
            // 
            // MainTabs
            // 
            this.MainTabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainTabs.Controls.Add(this.GeneralData);
            this.MainTabs.Controls.Add(this.OperationalData);
            this.MainTabs.Controls.Add(this.MarketResults);
            this.MainTabs.Controls.Add(this.BidData);
            this.MainTabs.Controls.Add(this.FinancialReport);
            this.MainTabs.Controls.Add(this.BiddingStrategy);
            this.MainTabs.Controls.Add(this.tbPageMaintenance);
            this.MainTabs.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.MainTabs.ItemSize = new System.Drawing.Size(125, 25);
            this.MainTabs.Location = new System.Drawing.Point(251, 27);
            this.MainTabs.Name = "MainTabs";
            this.MainTabs.Padding = new System.Drawing.Point(6, 6);
            this.MainTabs.SelectedIndex = 0;
            this.MainTabs.Size = new System.Drawing.Size(765, 658);
            this.MainTabs.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.MainTabs.TabIndex = 10;
            this.MainTabs.SelectedIndexChanged += new System.EventHandler(this.MainTabs_SelectedIndexChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1028, 711);
            this.Controls.Add(this.panel96);
            this.Controls.Add(this.label115);
            this.Controls.Add(this.lbllogname);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.MainTabs);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1022, 726);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
           
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.tbPageMaintenance.ResumeLayout(false);
            this.groupBox59.ResumeLayout(false);
            this.groupBox59.PerformLayout();
            this.groupBox61.ResumeLayout(false);
            this.groupBox61.PerformLayout();
            this.groupBox62.ResumeLayout(false);
            this.groupBox62.PerformLayout();
            this.BiddingStrategy.ResumeLayout(false);
            this.groupBoxBiddingStrategy3.ResumeLayout(false);
            this.groupBoxBiddingStrategy3.PerformLayout();
            this.groupBoxBiddingStrategy2.ResumeLayout(false);
            this.groupBoxBiddingStrategy2.PerformLayout();
            this.groupBoxBiddingStrategy1.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.panel80.ResumeLayout(false);
            this.panel80.PerformLayout();
            this.panel81.ResumeLayout(false);
            this.panel81.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.panel90.ResumeLayout(false);
            this.panel90.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel75.ResumeLayout(false);
            this.panel75.PerformLayout();
            this.panel77.ResumeLayout(false);
            this.panel77.PerformLayout();
            this.panel78.ResumeLayout(false);
            this.panel78.PerformLayout();
            this.panel79.ResumeLayout(false);
            this.panel79.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grBoxCustomize.ResumeLayout(false);
            this.menuStrip9.ResumeLayout(false);
            this.menuStrip9.PerformLayout();
            this.menuStrip10.ResumeLayout(false);
            this.menuStrip10.PerformLayout();
            this.menuStrip11.ResumeLayout(false);
            this.menuStrip11.PerformLayout();
            this.menuStrip12.ResumeLayout(false);
            this.menuStrip12.PerformLayout();
            this.menuStrip7.ResumeLayout(false);
            this.menuStrip7.PerformLayout();
            this.menuStrip6.ResumeLayout(false);
            this.menuStrip6.PerformLayout();
            this.menuStrip8.ResumeLayout(false);
            this.menuStrip8.PerformLayout();
            this.menuStrip5.ResumeLayout(false);
            this.menuStrip5.PerformLayout();
            this.menuStrip3.ResumeLayout(false);
            this.menuStrip3.PerformLayout();
            this.menuStrip4.ResumeLayout(false);
            this.menuStrip4.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.FinancialReport.ResumeLayout(false);
            this.FRMainPanel.ResumeLayout(false);
            this.FRUnitPanel.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.panel76.ResumeLayout(false);
            this.panel76.PerformLayout();
            this.panel69.ResumeLayout(false);
            this.panel69.PerformLayout();
            this.panel70.ResumeLayout(false);
            this.panel70.PerformLayout();
            this.panel71.ResumeLayout(false);
            this.panel71.PerformLayout();
            this.panel73.ResumeLayout(false);
            this.panel73.PerformLayout();
            this.panel74.ResumeLayout(false);
            this.panel74.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.panel65.ResumeLayout(false);
            this.panel65.PerformLayout();
            this.panel68.ResumeLayout(false);
            this.panel68.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.panel66.ResumeLayout(false);
            this.panel66.PerformLayout();
            this.panel67.ResumeLayout(false);
            this.panel67.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.panel62.ResumeLayout(false);
            this.panel62.PerformLayout();
            this.panel63.ResumeLayout(false);
            this.panel63.PerformLayout();
            this.panel64.ResumeLayout(false);
            this.panel64.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.panel59.ResumeLayout(false);
            this.panel59.PerformLayout();
            this.panel60.ResumeLayout(false);
            this.panel60.PerformLayout();
            this.panel61.ResumeLayout(false);
            this.panel61.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.panel53.ResumeLayout(false);
            this.panel53.PerformLayout();
            this.panel57.ResumeLayout(false);
            this.panel57.PerformLayout();
            this.panel58.ResumeLayout(false);
            this.panel58.PerformLayout();
            this.FRPlantPanel.ResumeLayout(false);
            this.FRPlantPanel.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.panel108.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewECO)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.panel54.ResumeLayout(false);
            this.panel54.PerformLayout();
            this.panel55.ResumeLayout(false);
            this.panel55.PerformLayout();
            this.panel56.ResumeLayout(false);
            this.panel56.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.panel47.ResumeLayout(false);
            this.panel47.PerformLayout();
            this.panel48.ResumeLayout(false);
            this.panel48.PerformLayout();
            this.panel49.ResumeLayout(false);
            this.panel49.PerformLayout();
            this.panel50.ResumeLayout(false);
            this.panel50.PerformLayout();
            this.panel51.ResumeLayout(false);
            this.panel51.PerformLayout();
            this.panel52.ResumeLayout(false);
            this.panel52.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.panel41.ResumeLayout(false);
            this.panel41.PerformLayout();
            this.panel42.ResumeLayout(false);
            this.panel42.PerformLayout();
            this.panel43.ResumeLayout(false);
            this.panel43.PerformLayout();
            this.panel44.ResumeLayout(false);
            this.panel44.PerformLayout();
            this.panel45.ResumeLayout(false);
            this.panel45.PerformLayout();
            this.panel46.ResumeLayout(false);
            this.panel46.PerformLayout();
            this.FRHeaderGb.ResumeLayout(false);
            this.FRHeaderGb.PerformLayout();
            this.FRHeaderPanel.ResumeLayout(false);
            this.FRHeaderPanel.PerformLayout();
            this.BidData.ResumeLayout(false);
            this.BDMainPanel.ResumeLayout(false);
            this.BDMainPanel.PerformLayout();
            this.BDCur2.ResumeLayout(false);
            this.BDCur2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BDCurGrid)).EndInit();
            this.BDCur1.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panel38.PerformLayout();
            this.panel39.ResumeLayout(false);
            this.panel39.PerformLayout();
            this.panel40.ResumeLayout(false);
            this.panel40.PerformLayout();
            this.BDHeaderGb.ResumeLayout(false);
            this.BDHeaderGb.PerformLayout();
            this.BDHeaderPanel.ResumeLayout(false);
            this.BDHeaderPanel.PerformLayout();
            this.MarketResults.ResumeLayout(false);
            this.MRHeaderGB.ResumeLayout(false);
            this.MRHeaderGB.PerformLayout();
            this.MRHeaderPanel.ResumeLayout(false);
            this.MRHeaderPanel.PerformLayout();
            this.MRMainPanel.ResumeLayout(false);
            this.MRMainPanel.PerformLayout();
            this.MRGB.ResumeLayout(false);
            this.MRGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MRCurGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MRCurGrid2)).EndInit();
            this.MRPlantCurGb.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            this.MRUnitCurGb.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            this.panel37.PerformLayout();
            this.panel36.ResumeLayout(false);
            this.panel36.PerformLayout();
            this.panel72.ResumeLayout(false);
            this.panel72.PerformLayout();
            this.OperationalData.ResumeLayout(false);
            this.ODHeaderGB.ResumeLayout(false);
            this.ODHeaderGB.PerformLayout();
            this.ODHeaderPanel.ResumeLayout(false);
            this.ODHeaderPanel.PerformLayout();
            this.ODMainPanel.ResumeLayout(false);
            this.panel94.ResumeLayout(false);
            this.panel94.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDispatch)).EndInit();
            this.ODUnitPanel.ResumeLayout(false);
            this.ODUnitMainGB.ResumeLayout(false);
            this.ODUnitMainGB.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODMaintenanceEndHour)).EndInit();
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODMaintenanceStartHour)).EndInit();
            this.ODUnitPowerGB.ResumeLayout(false);
            this.ODUnitPowerGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODUnitPowerGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ODUnitPowerGrid1)).EndInit();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.ODUnitServiceGB.ResumeLayout(false);
            this.ODUnitServiceGB.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODOutServiceEndHour)).EndInit();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODOutServiceStartHour)).EndInit();
            this.ODUnitFuelGB.ResumeLayout(false);
            this.ODUnitFuelGB.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODSecondFuelEndHour)).EndInit();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ODSecondFuelStartHour)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.panel92.ResumeLayout(false);
            this.panel92.PerformLayout();
            this.GeneralData.ResumeLayout(false);
            this.GDPostpanel.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.panel95.ResumeLayout(false);
            this.panel95.PerformLayout();
            this.GDPowerGb.ResumeLayout(false);
            this.GDPowerGb.PerformLayout();
            this.ConsumedGb.ResumeLayout(false);
            this.ConsumedGb.PerformLayout();
            this.GDMainPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TempGV)).EndInit();
            this.GDSteamGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grid230)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlantGV2)).EndInit();
            this.GDGasGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grid400)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlantGV1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlantGV11)).EndInit();
            this.GDUnitPanel.ResumeLayout(false);
            this.GDUnitMaintenanceGB.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel84.ResumeLayout(false);
            this.panel84.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel86.ResumeLayout(false);
            this.panel86.PerformLayout();
            this.panel178.ResumeLayout(false);
            this.panel178.PerformLayout();
            this.panel88.ResumeLayout(false);
            this.panel88.PerformLayout();
            this.panel87.ResumeLayout(false);
            this.panel87.PerformLayout();
            this.panel82.ResumeLayout(false);
            this.panel82.PerformLayout();
            this.panel83.ResumeLayout(false);
            this.panel83.PerformLayout();
            this.panel85.ResumeLayout(false);
            this.panel85.PerformLayout();
            this.Countergb.ResumeLayout(false);
            this.panel89.ResumeLayout(false);
            this.panel89.PerformLayout();
            this.panel91.ResumeLayout(false);
            this.panel91.PerformLayout();
            this.Anonygb.ResumeLayout(false);
            this.panelAvc.ResumeLayout(false);
            this.panelAvc.PerformLayout();
            this.GDSecondaryFuelPanel.ResumeLayout(false);
            this.GDSecondaryFuelPanel.PerformLayout();
            this.GDPrimaryFuelPanel.ResumeLayout(false);
            this.GDPrimaryFuelPanel.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.Powergb.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.Maintenancegb.ResumeLayout(false);
            this.Maintenancegb.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.Currentgb.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.GDHeaderGB.ResumeLayout(false);
            this.GDHeaderGB.PerformLayout();
            this.GDHeaderPanel.ResumeLayout(false);
            this.GDHeaderPanel.PerformLayout();
            this.MainTabs.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TreeView treeView2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.BindingSource linesBindingSource;
        private System.Windows.Forms.BindingSource linesBindingSource1;
        private System.Windows.Forms.BindingSource unitsDataMainBindingSource;
        private System.Windows.Forms.BindingSource detailFRM002BindingSource;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem marketToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m002ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m005ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem averagePriceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadForecastingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem capacityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annualFactorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem weekFactorToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem betToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ToolStripMenuItem efficiencyMarketToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DeletetoolStripMenuItem;
        private Dundas.Charting.WinControl.Chart chart2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogmarket;
        private System.Windows.Forms.ToolStripMenuItem ReporttoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allPlantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectedPlantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem plantReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unitsReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemData;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExport;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemStatus;
        private System.Windows.Forms.ToolStripMenuItem baseDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoPathToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem meteringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ls2IntervalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ls2LineIntervalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadIntervalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dispatchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem weekendCalenderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dispatchConstrantsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oPFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sensetivityAnalysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ls2PostIntervalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allTypeIntervalToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem backUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaledEnergytoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem ToolStripspecialtime;
        private System.Windows.Forms.ToolStripMenuItem toolStripuseraccount;
        private System.Windows.Forms.ToolStripMenuItem addUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EditUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem marketInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gencoNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backToLogInToolStripMenuItem;
        private System.Windows.Forms.Label lbllogname;
        private System.Windows.Forms.ToolStripMenuItem formsDesignToolStripMenuItem;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem emailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.ToolStripMenuItem pDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wordToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem contactUsToolStripMenuItem;
        private System.Windows.Forms.Panel panel96;
        private System.Windows.Forms.ToolStripMenuItem profileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem internetplantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dailyBillToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monthlyBillToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem biddingSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billimgSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trainingPriceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem billItemToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fuelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem powerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bourseSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hubToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transmissionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wizardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem biddingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pactualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem powerLimmitedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem priorityOnToolStripMenuItem;
        private System.Windows.Forms.BindingSource powerPalntDBDataSet4BindingSource;
        private System.Windows.Forms.ToolStripMenuItem daramadReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ls2FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem44;
        private System.Windows.Forms.ToolStripMenuItem consumeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem protestReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem45;
        private System.Windows.Forms.ToolStripMenuItem riskToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem strategyToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem importBillFromWebServiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billCheckToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem avEnergyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billCheckAvEnergyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billToolStripMenuItem;
        public System.Windows.Forms.TabControl MainTabs;
        private System.Windows.Forms.TabPage GeneralData;
        private System.Windows.Forms.Panel GDPostpanel;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.ListBox FromListBox;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.ListBox ToListBox;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.RadioButton radioEditPost;
        private System.Windows.Forms.RadioButton radioAddpost;
        private System.Windows.Forms.Button BtnOKpost;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.TextBox txtaddvoltagenum;
        private System.Windows.Forms.TextBox txtpostname;
        private System.Windows.Forms.TextBox txtaddpostnum;
        private System.Windows.Forms.Panel panel95;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Button DeletePostBtn;
        private System.Windows.Forms.Label VolNumLb;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label voltagelevellb;
        private System.Windows.Forms.Label postnumlb;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.GroupBox GDPowerGb;
        private System.Windows.Forms.Button AddPowerBtn;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.CheckedListBox AddPowerList;
        private System.Windows.Forms.Button DeletePowerBtn;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.CheckedListBox DeletePowerList;
        private System.Windows.Forms.GroupBox ConsumedGb;
        private System.Windows.Forms.TextBox txtselectconsum;
        private System.Windows.Forms.Button AddConsumBtn;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Button DeleteConsumBtn;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.CheckedListBox DeleteConsumeList;
        private System.Windows.Forms.Panel GDMainPanel;
        private System.Windows.Forms.DataGridView TempGV;
        private System.Windows.Forms.Button GDNewBtn;
        private System.Windows.Forms.Button GDDeleteBtn;
        private System.Windows.Forms.GroupBox GDSteamGroup;
        private System.Windows.Forms.DataGridView Grid230;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckBox2;
        private System.Windows.Forms.DataGridView PlantGV2;
        private System.Windows.Forms.DataGridViewTextBoxColumn unitCodeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn packageCodeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn unitTypeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn capacityDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pMinDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pMaxDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column31;
        private System.Windows.Forms.GroupBox GDGasGroup;
        private System.Windows.Forms.DataGridView Grid400;
        private System.Windows.Forms.DataGridViewTextBoxColumn lineCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fromBusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn toBusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lineLengthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ownerGencoFromDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ownerGencoToDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckBox1;
        private System.Windows.Forms.DataGridView PlantGV1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckBox;
        private System.Windows.Forms.DataGridView PlantGV11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fuel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn Maintenance;
        private System.Windows.Forms.DataGridViewTextBoxColumn Outservice;
        private System.Windows.Forms.Panel GDUnitPanel;
        private System.Windows.Forms.GroupBox GDUnitMaintenanceGB;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.TextBox txtMaintStartUpFactor;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.TextBox txtMaintFuelFactor;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.ComboBox cmbMaintLastMaintType;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Panel panel84;
        private System.Windows.Forms.TextBox txtMaintMOHours;
        private System.Windows.Forms.Label label240;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.TextBox txtMaintLastHour;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Panel panel86;
        private System.Windows.Forms.TextBox txtMaintHGPHours;
        private System.Windows.Forms.Label label242;
        private System.Windows.Forms.Panel panel178;
        private System.Windows.Forms.TextBox txtMaintCIHours;
        private System.Windows.Forms.Label label243;
        private System.Windows.Forms.Panel panel88;
        private System.Windows.Forms.TextBox txtDurabilityHours;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Panel panel87;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerDurability;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label236;
        private System.Windows.Forms.Panel panel82;
        private System.Windows.Forms.TextBox txtMaintMODur;
        private System.Windows.Forms.Label label238;
        private System.Windows.Forms.Panel panel83;
        private System.Windows.Forms.TextBox txtMaintHGPDur;
        private System.Windows.Forms.Label label239;
        private System.Windows.Forms.Panel panel85;
        private System.Windows.Forms.TextBox txtMaintCIDur;
        private System.Windows.Forms.Label label241;
        private System.Windows.Forms.GroupBox Countergb;
        private System.Windows.Forms.Panel panel89;
        private System.Windows.Forms.TextBox GDConsumedSerialTb;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Panel panel91;
        private System.Windows.Forms.TextBox GDPowerSerialTb;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.GroupBox Anonygb;
        private System.Windows.Forms.Panel panelAvc;
        private System.Windows.Forms.TextBox GDtxtAvc;
        private System.Windows.Forms.Label lblAvc;
        private System.Windows.Forms.Panel GDSecondaryFuelPanel;
        private System.Windows.Forms.TextBox GDSecondaryFuelTB;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Panel GDPrimaryFuelPanel;
        private System.Windows.Forms.TextBox GDPrimaryFuelTB;
        private System.Windows.Forms.Label GDConsLb;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.TextBox GDRampRateTB;
        private System.Windows.Forms.Label GDRampLb;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox GDTimeHotStartTB;
        private System.Windows.Forms.Label GDHotLb;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox GDTimeColdStartTB;
        private System.Windows.Forms.Label GDColdLb;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox GDTimeDownTB;
        private System.Windows.Forms.Label GDTimeDownLb;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox GDTimeUpTB;
        private System.Windows.Forms.Label GDTUpLb;
        private System.Windows.Forms.GroupBox Powergb;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox GDPminTB;
        private System.Windows.Forms.Label GDPminLb;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox GDPmaxTB;
        private System.Windows.Forms.Label GDPmaxLb;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox GDcapacityTB;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.TextBox GDpowertransserialtb;
        private System.Windows.Forms.GroupBox Maintenancegb;
        private System.Windows.Forms.CheckBox GDOutServiceCheck;
        private System.Windows.Forms.Panel panel16;
        private FarsiLibrary.Win.Controls.FADatePicker GDEndDateCal;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel15;
        private FarsiLibrary.Win.Controls.FADatePicker GDStartDateCal;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.TextBox GDMainTypeTB;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox Currentgb;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.TextBox GDFuelTB;
        private System.Windows.Forms.Label GDFuelLb;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox GDStateTB;
        private System.Windows.Forms.Label GDStateLb;
        private System.Windows.Forms.GroupBox GDHeaderGB;
        private System.Windows.Forms.Label GDPlantLb;
        private System.Windows.Forms.Panel GDHeaderPanel;
        private System.Windows.Forms.Label GDTypeLb;
        private System.Windows.Forms.Label GDUnitLb;
        private System.Windows.Forms.Label GDPackLb;
        private System.Windows.Forms.Label L4;
        private System.Windows.Forms.Label L3;
        private System.Windows.Forms.Label L2;
        private System.Windows.Forms.Label L1;
        private System.Windows.Forms.TabPage OperationalData;
        private System.Windows.Forms.GroupBox ODHeaderGB;
        private System.Windows.Forms.Label ODPlantLb;
        private System.Windows.Forms.Panel ODHeaderPanel;
        private System.Windows.Forms.Label ODTypeLb;
        private System.Windows.Forms.Label ODUnitLb;
        private System.Windows.Forms.Label ODPackLb;
        private System.Windows.Forms.Label L8;
        private System.Windows.Forms.Label L7;
        private System.Windows.Forms.Label L6;
        private System.Windows.Forms.Label L5;
        private System.Windows.Forms.Panel ODMainPanel;
        private System.Windows.Forms.Panel panel94;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label114;
        private FarsiLibrary.Win.Controls.FADatePicker faDatePickDispatch;
        private System.Windows.Forms.DataGridView dataGridDispatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour;
        private System.Windows.Forms.DataGridViewTextBoxColumn Block;
        private System.Windows.Forms.DataGridViewTextBoxColumn Packagetype;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeclaredCapacity;
        private System.Windows.Forms.DataGridViewTextBoxColumn DispachableCapacity;
        private System.Windows.Forms.Panel ODUnitPanel;
        private System.Windows.Forms.GroupBox ODUnitMainGB;
        private System.Windows.Forms.LinkLabel linkmaintenance;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.ComboBox ODMaintenanceType;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label odunitmd2Valid;
        private System.Windows.Forms.Label odunitmd1Valid;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.NumericUpDown ODMaintenanceEndHour;
        private System.Windows.Forms.Label label4;
        private FarsiLibrary.Win.Controls.FADatePicker ODUnitMainEndDate;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.NumericUpDown ODMaintenanceStartHour;
        private System.Windows.Forms.Label label3;
        private FarsiLibrary.Win.Controls.FADatePicker ODUnitMainStartDate;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.CheckBox ODUnitMainCheck;
        private System.Windows.Forms.GroupBox ODUnitPowerGB;
        private System.Windows.Forms.Button btnsavedispach;
        private System.Windows.Forms.TextBox textupdatetime;
        private System.Windows.Forms.Label labelupdatetime;
        private System.Windows.Forms.Label odunitpd1Valid;
        private System.Windows.Forms.DataGridView ODUnitPowerGrid2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn55;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn56;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn58;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn59;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn61;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn62;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn63;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn64;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn65;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn66;
        private System.Windows.Forms.DataGridView ODUnitPowerGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn67;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn68;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn69;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn70;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn71;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn72;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn73;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn74;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn75;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn76;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn77;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn78;
        private System.Windows.Forms.Panel panel26;
        private FarsiLibrary.Win.Controls.FADatePicker ODUnitPowerStartDate;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.CheckBox ODUnitPowerCheck;
        private System.Windows.Forms.GroupBox ODUnitServiceGB;
        private System.Windows.Forms.LinkLabel linkoutservice;
        private System.Windows.Forms.Label odunitosd2Valid;
        private System.Windows.Forms.Label odunitosd1Valid;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.NumericUpDown ODOutServiceEndHour;
        private System.Windows.Forms.Label label2;
        private FarsiLibrary.Win.Controls.FADatePicker ODUnitServiceEndDate;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.NumericUpDown ODOutServiceStartHour;
        private System.Windows.Forms.Label label1;
        private FarsiLibrary.Win.Controls.FADatePicker ODUnitServiceStartDate;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.CheckBox ODUnitOutCheck;
        private System.Windows.Forms.GroupBox ODUnitFuelGB;
        private System.Windows.Forms.LinkLabel linkfuel;
        private System.Windows.Forms.Label odunitsfd2Valid;
        private System.Windows.Forms.Label odunitsfd1Valid;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.TextBox textSECONDFUELPERCENT;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox ODUnitFuelTB;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.NumericUpDown ODSecondFuelEndHour;
        private System.Windows.Forms.Label label28;
        private FarsiLibrary.Win.Controls.FADatePicker ODUnitFuelEndDate;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.NumericUpDown ODSecondFuelStartHour;
        private System.Windows.Forms.Label label27;
        private FarsiLibrary.Win.Controls.FADatePicker ODUnitFuelStartDate;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.CheckBox ODUnitFuelCheck;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel panel92;
        private System.Windows.Forms.CheckBox MUSTMAXCKECK;
        private System.Windows.Forms.CheckBox MustrunCheck;
        private System.Windows.Forms.Button ODSaveBtn;
        private System.Windows.Forms.Panel panel97;
        private System.Windows.Forms.TabPage MarketResults;
        private System.Windows.Forms.GroupBox MRHeaderGB;
        private System.Windows.Forms.Label MRPlantLb;
        private System.Windows.Forms.Panel MRHeaderPanel;
        private System.Windows.Forms.Label MRTypeLb;
        private System.Windows.Forms.Label MRUnitLb;
        private System.Windows.Forms.Label MRPackLb;
        private System.Windows.Forms.Label L12;
        private System.Windows.Forms.Label L11;
        private System.Windows.Forms.Label L10;
        private System.Windows.Forms.Label L9;
        private System.Windows.Forms.Panel MRMainPanel;
        private System.Windows.Forms.Button btnToExcelMR;
        private System.Windows.Forms.RadioButton rdbam005ba;
        private System.Windows.Forms.RadioButton rdm005;
        private System.Windows.Forms.Button mrbtnpprint;
        private System.Windows.Forms.Button btnex009;
        private System.Windows.Forms.Button MRPlotBtn;
        private System.Windows.Forms.GroupBox MRGB;
        private System.Windows.Forms.Button btnmarketforward;
        private System.Windows.Forms.RadioButton rbrealplant;
        private System.Windows.Forms.Button btnmarketback;
        private System.Windows.Forms.RadioButton rbestimateplant;
        private FarsiLibrary.Win.Controls.FADatePicker MRCal;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.DataGridView MRCurGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridView MRCurGrid2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.GroupBox MRPlantCurGb;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.TextBox MRPlantPowerTb;
        private System.Windows.Forms.Label MRLabel2;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.TextBox MRPlantOnUnitTb;
        private System.Windows.Forms.Label MRLabel1;
        private System.Windows.Forms.GroupBox MRUnitCurGb;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.TextBox MRUnitMaxBidTb;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.TextBox MRUnitPowerTb;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.TextBox MRUnitStateTb;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TabPage BidData;
        private System.Windows.Forms.Panel BDMainPanel;
        private System.Windows.Forms.Button btnToExcel;
        private System.Windows.Forms.Button btnbidprint;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Button BDPlotBtn;
        private System.Windows.Forms.GroupBox BDCur2;
        private System.Windows.Forms.RadioButton EstimatedBidCheck;
        private System.Windows.Forms.RadioButton RealBidCheck;
        private FarsiLibrary.Win.Controls.FADatePicker BDCal;
        private System.Windows.Forms.DataGridView BDCurGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn power6DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn price6DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn power7DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn price7DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn power8DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn price8DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn power9DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn price9DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn power10DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn price10DataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.GroupBox BDCur1;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.TextBox BDMaxBidTb;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.TextBox BDPowerTb;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.TextBox BDStateTb;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.GroupBox BDHeaderGb;
        private System.Windows.Forms.Label BDPlantLb;
        private System.Windows.Forms.Panel BDHeaderPanel;
        private System.Windows.Forms.Label BDTypeLb;
        private System.Windows.Forms.Label BDUnitLb;
        private System.Windows.Forms.Label BDPackLb;
        private System.Windows.Forms.Label L16;
        private System.Windows.Forms.Label L15;
        private System.Windows.Forms.Label L14;
        private System.Windows.Forms.Label L13;
        private System.Windows.Forms.TabPage FinancialReport;
        private System.Windows.Forms.Panel FRMainPanel;
        private System.Windows.Forms.Button btnfinanceexcel;
        private System.Windows.Forms.Button btnfrprint;
        private System.Windows.Forms.Button FRRunAuto;
        private System.Windows.Forms.Button FRRun;
        private System.Windows.Forms.Button FRPlot;
        private System.Windows.Forms.Button FRUpdateBtn;
        private System.Windows.Forms.Button FROKBtn;
        private System.Windows.Forms.Panel FRUnitPanel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.RadioButton FRUnitSecondaryFuelRb;
        private System.Windows.Forms.RadioButton FRUnitPrimaryFuelRb;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.TextBox FRUnitPActiveTb;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.TextBox FRUnitQReactiveTb;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox FRUnitFuelCostTb;
        private System.Windows.Forms.TextBox FRUnitFuelNoCostTb;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.GroupBox groupBox10;
        private FarsiLibrary.Win.Controls.FADatePicker FRUnitCal;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Panel panel76;
        private System.Windows.Forms.TextBox FRUnitIncomeTb;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Panel panel69;
        private System.Windows.Forms.TextBox FRUnitEneryPayTb;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Panel panel70;
        private System.Windows.Forms.TextBox FRUnitCapPayTb;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Panel panel71;
        private System.Windows.Forms.TextBox FRUnitULPowerTb;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Panel panel73;
        private System.Windows.Forms.TextBox FRUnitTotalPowerTb;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Panel panel74;
        private System.Windows.Forms.TextBox FRUnitCapacityTb;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Panel panel65;
        private System.Windows.Forms.TextBox FRUnitHotTb;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Panel panel68;
        private System.Windows.Forms.TextBox FRUnitColdTb;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Panel panel66;
        private System.Windows.Forms.TextBox FRUnitBmainTb;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.TextBox FRUnitAmainTb;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.TextBox FRUnitCmargTb2;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.TextBox FRUnitBmargTb2;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.TextBox FRUnitAmargTb2;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.TextBox FRUnitCmargTb1;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.TextBox FRUnitBmargTb1;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.TextBox FRUnitAmargTb1;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.TextBox FRUnitVariableTb;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.TextBox FRUnitFixedTb;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.TextBox FRUnitCapitalTb;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Panel FRPlantPanel;
        private System.Windows.Forms.LinkLabel lblinsufficient;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Panel panel108;
        private System.Windows.Forms.DataGridView dataGridViewECO;
        private FarsiLibrary.Win.Controls.FADatePicker FRPlantCal;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Panel panel54;
        public System.Windows.Forms.TextBox FRPlantCostTb;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Panel panel55;
        public System.Windows.Forms.TextBox FRPlantIncomeTb;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Panel panel56;
        public System.Windows.Forms.TextBox FRPlantBenefitTB;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Panel panel47;
        public System.Windows.Forms.TextBox FRPlantDecPayTb;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Panel panel48;
        public System.Windows.Forms.TextBox FRPlantIncPayTb;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Panel panel49;
        public System.Windows.Forms.TextBox FRPlantULPayTb;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Panel panel50;
        public System.Windows.Forms.TextBox FRPlantBidPayTb;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Panel panel51;
        public System.Windows.Forms.TextBox FRPlantEnergyPayTb;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Panel panel52;
        public System.Windows.Forms.TextBox FRPlantCapPayTb;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Panel panel41;
        public System.Windows.Forms.TextBox FRPlantDecPowerTb;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Panel panel42;
        public System.Windows.Forms.TextBox FRPlantIncPowerTb;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Panel panel43;
        public System.Windows.Forms.TextBox FRPlantULPowerTb;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Panel panel44;
        public System.Windows.Forms.TextBox FRPlantBidPowerTb;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Panel panel45;
        public System.Windows.Forms.TextBox FRPlantTotalPowerTb;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Panel panel46;
        public System.Windows.Forms.TextBox FRPlantAvaCapTb;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.GroupBox FRHeaderGb;
        private System.Windows.Forms.Label FRPlantLb;
        private System.Windows.Forms.Panel FRHeaderPanel;
        private System.Windows.Forms.Label FRTypeLb;
        private System.Windows.Forms.Label FRUnitLb;
        private System.Windows.Forms.Label FRPackLb;
        private System.Windows.Forms.Label L20;
        private System.Windows.Forms.Label L19;
        private System.Windows.Forms.Label L18;
        private System.Windows.Forms.Label L17;
        private System.Windows.Forms.TabPage BiddingStrategy;
        private System.Windows.Forms.GroupBox groupBoxBiddingStrategy3;
        private System.Windows.Forms.CheckBox Initialprice;
        private System.Windows.Forms.Panel panel93;
        private System.Windows.Forms.Button btnrunseting;
        private System.Windows.Forms.CheckBox chkmaxall;
        private System.Windows.Forms.Button btnRunManual;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.RadioButton rdStrategyBidding;
        private System.Windows.Forms.RadioButton rdBidAllocation;
        private System.Windows.Forms.RadioButton rdForecastingPrice;
        private System.Windows.Forms.Button btnRunBidding;
        private System.Windows.Forms.GroupBox groupBoxBiddingStrategy2;
        private System.Windows.Forms.TextBox lstStatus;
        private System.Windows.Forms.GroupBox groupBoxBiddingStrategy1;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Panel panel80;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerBidding;
        private System.Windows.Forms.Panel panel81;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerCurrent;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Panel panel90;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox txtpowertraining;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.ComboBox cmbPeakBid;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel75;
        private System.Windows.Forms.ComboBox cmbCostLevel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel77;
        private System.Windows.Forms.ComboBox cmbStrategyBidding;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel78;
        private System.Windows.Forms.TextBox txtTraining;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel79;
        private System.Windows.Forms.ComboBox cmbRunSetting;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblPlantValue;
        private System.Windows.Forms.Label lblGencoValue;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox grBoxCustomize;
        private System.Windows.Forms.MenuStrip menuStrip9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem29;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox3;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem30;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox5;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem31;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox7;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox8;
        private System.Windows.Forms.MenuStrip menuStrip10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem32;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox9;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem33;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox11;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem34;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox13;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem35;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox15;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox16;
        private System.Windows.Forms.MenuStrip menuStrip11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem36;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox17;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox18;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem37;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox19;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox20;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem38;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox21;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox22;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem39;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox23;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox24;
        private System.Windows.Forms.MenuStrip menuStrip12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem40;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox25;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox26;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem41;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox27;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox28;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem42;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox29;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox30;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem43;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox31;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox32;
        private System.Windows.Forms.MenuStrip menuStrip7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy6;
        private System.Windows.Forms.ToolStripComboBox cmbPeak6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy12;
        private System.Windows.Forms.ToolStripComboBox cmbPeak12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem19;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy18;
        private System.Windows.Forms.ToolStripComboBox cmbPeak18;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem20;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy24;
        private System.Windows.Forms.ToolStripComboBox cmbPeak24;
        private System.Windows.Forms.MenuStrip menuStrip6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy5;
        private System.Windows.Forms.ToolStripComboBox cmbPeak5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy11;
        private System.Windows.Forms.ToolStripComboBox cmbPeak11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy17;
        private System.Windows.Forms.ToolStripComboBox cmbPeak17;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy23;
        private System.Windows.Forms.ToolStripComboBox cmbPeak23;
        private System.Windows.Forms.MenuStrip menuStrip8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem24;
        private System.Windows.Forms.ToolStripMenuItem strategyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem peakToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem strategyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem strategyToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem peakToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem hourToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem strategyToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem peakToolStripMenuItem2;
        private System.Windows.Forms.MenuStrip menuStrip5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy4;
        private System.Windows.Forms.ToolStripComboBox cmbPeak4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy10;
        private System.Windows.Forms.ToolStripComboBox cmbPeak10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy16;
        private System.Windows.Forms.ToolStripComboBox cmbPeak16;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy22;
        private System.Windows.Forms.ToolStripComboBox cmbPeak22;
        private System.Windows.Forms.MenuStrip menuStrip3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy2;
        private System.Windows.Forms.ToolStripComboBox cmbPeak2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy8;
        private System.Windows.Forms.ToolStripComboBox cmbPeak8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy14;
        private System.Windows.Forms.ToolStripComboBox cmbPeak14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy20;
        private System.Windows.Forms.ToolStripComboBox cmbPeak20;
        private System.Windows.Forms.MenuStrip menuStrip4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem25;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy3;
        private System.Windows.Forms.ToolStripComboBox cmbPeak3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem26;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy9;
        private System.Windows.Forms.ToolStripComboBox cmbPeak9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem27;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy15;
        private System.Windows.Forms.ToolStripComboBox cmbPeak15;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem28;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy21;
        private System.Windows.Forms.ToolStripComboBox cmbPeak21;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem hour1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy1;
        private System.Windows.Forms.ToolStripComboBox cmbPeak1;
        private System.Windows.Forms.ToolStripMenuItem hour2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy7;
        private System.Windows.Forms.ToolStripComboBox cmbPeak7;
        private System.Windows.Forms.ToolStripMenuItem hour3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy13;
        private System.Windows.Forms.ToolStripComboBox cmbPeak13;
        private System.Windows.Forms.ToolStripMenuItem hour4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cmbStrategy19;
        private System.Windows.Forms.ToolStripComboBox cmbPeak19;
        private System.Windows.Forms.TabPage tbPageMaintenance;
        private System.Windows.Forms.ComboBox cmbMaintShownPlant;
        private System.Windows.Forms.GroupBox groupBox59;
        private System.Windows.Forms.Label lblMainStartDate;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label lblMaintStartDate;
        private System.Windows.Forms.Label lblMaintEndDate;
        private System.Windows.Forms.Label label229;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.Button btnRunMaint;
        private System.Windows.Forms.GroupBox groupBox61;
        private System.Windows.Forms.TextBox txtMaintStatus;
        private System.Windows.Forms.GroupBox groupBox62;
        private System.Windows.Forms.Label lblMaintPlant;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.Label label233;
        private System.Windows.Forms.MenuStrip miniToolStrip;
    }
}