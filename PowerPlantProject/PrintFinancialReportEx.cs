﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Reflection;
using System.Configuration;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Windows.Forms;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Threading;
using NRI.SBS.Common;
using Microsoft.Win32;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Utilities = NRI.SBS.Common.Utilities;

namespace PowerPlantProject
{
    public partial class PrintFinancialReportEx : Form
    {
        int StrHour;
        int EndHour;
        int addChartToDoc = 0;
        int printPlotType = 0;
        int  valPrint = 0;
        int keyActiv = 0;
        int keyActivNRI = 0;


        //{
        //    "Bid Power",
        //    "Total Power",
        //    "Pure Power", 
        //    "Bid Power",
        //    "UL Power",
        //    "Increase Power",
        //    "Bid Payment",
        //    "UL Payment",
        //    "Increase Payment",
        //    "Pure Energy Payment",
        //    "Decrease Power",
        //    "Decrease Payment",
        //    "Disobedience Power",
        //    "Disobedience Payment",
        //    "Energy Payment",
        //    "Available Capacity",
        //    "Capacity Payment",
        //    "Capacity test",
        //    "Capacity test Payment",
        //    "Start up Capacity",
        //    "Start up Payment",
        //    "Reserve Payment",
        //    "Reactive",
        //    "Reactive Payment",
         
        //    "S-26",
        //    "S-27",
        //    "S-28",
        //    "S-29",
        //    "S-31",
        //    "S-36",
        //    "S-37",
        //    "S-38",
        //    "S-39",
        //    "S-40",
        //    "S-41",
        //    "S-42",
        //    "S-43"
        //};
        string[] KeyStrItms;
        public PrintFinancialReportEx()
        {


              
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in groupBox1.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                foreach (Control c in groupBox2.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                foreach (Control c in groupBox3.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }
                foreach (Control c in groupBox7.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }
                foreach (Control c in tabPage2.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }


                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in groupBox1.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                foreach (Control c in groupBox2.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                foreach (Control c in groupBox3.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                foreach (Control c in groupBox7.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }

                foreach (Control c in tabPage1.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                foreach (Control c in tabPage2.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in groupBox1.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }
                foreach (Control c in groupBox2.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }
                foreach (Control c in groupBox3.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////

                //foreach (Control c in groupBox12.Controls)
                //{
                //    if (c is TextBox || c is ListBox)
                //    {
                //        c.BackColor = FormColors.GetColor().Textbackcolor;
                //       c.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                //        c.ForeColor = Color.Red;
                //    }
                //}
            }
            catch
            {

            }
        }


        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            BillItemPrintEx nBill = new BillItemPrintEx(1,faDatePicker2.Text);
            nBill.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            BillItemPrintEx nBill = new BillItemPrintEx(0,faDatePicker1.Text);
            nBill.Show();
        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }
        public void prepareNameArray(string datePick)
        {

            DataTable bb = Utilities.GetTable("select distinct code,name from dbo.MonthlyBillTotal where month like'%" + datePick.ToString().Substring(0, 7).Trim() + "%' order by code asc");
            if (bb.Rows.Count == 0)
            {
                bb = Utilities.GetTable("select distinct code,name from dbo.MonthlyBillTotal where month in (select max(month) from MonthlyBillTotal) order by code asc");

            }
            KeyStrItms = new string[60];
            if (bb.Rows.Count == 0) return;
            for (int y = 0; y < bb.Rows.Count; y++)
            {
                //string nn = bb.Rows[y][0].ToString().Trim();
                KeyStrItms[y] = bb.Rows[y][1].ToString().Trim();
            }
        }
        private void button8_Click(object sender, EventArgs e)
        {


            itmBasedDaily();
                
            ////===============================================================

            ////actual width of table in points
            //table.TotalWidth = 216f;
            ////fix the absolute width of the table
            //table.LockedWidth = true;

            ////relative col widths in proportions - 1/3 and 2/3
            //float[] widths = new float[] { 1f, 2f };
            //table.SetWidths(widths);
            //table.HorizontalAlignment = 0;
            ////leave a gap before and after the table
            //table.SpacingBefore = 20f;
            //table.SpacingAfter = 30f;

            //PdfPCell cell = new PdfPCell(new Phrase("Products"));
            //cell.Colspan = 2;
            //cell.Border = 0;
            //cell.HorizontalAlignment = 1;
            //table.AddCell(cell);
            //string connect = "Server=.\\SQLEXPRESS;Database=Northwind;Trusted_Connection=True;";
            //using (SqlConnection conn = new SqlConnection(connect))
            //{
            //    string query = "SELECT ProductID, ProductName FROM MonthlyBillPlant";
            //    SqlCommand cmd = new SqlCommand(query, conn);
            //    try
            //    {
            //        conn.Open();
            //        using (SqlDataReader rdr = cmd.ExecuteReader())
            //        {
            //            while (rdr.Read())
            //            {
            //                table.AddCell(rdr[0].ToString());
            //                table.AddCell(rdr[1].ToString());
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Response.Write(ex.Message);
            //    }
            //    doc.Add(table);
            //}
        }
        public int CorrectItemInxData(int itmInxCorr)
        {
            if (itmInxCorr > 24&&itmInxCorr<30)
            {
               itmInxCorr=(itmInxCorr-1);
            }
            else 
            {
                if (itmInxCorr == 31)
                { itmInxCorr = 29; }
                else
                {
                    if (itmInxCorr > 35 && itmInxCorr < 44)
                    {
                        itmInxCorr = (itmInxCorr - 6);
                    }
                }
            }

            return itmInxCorr;
        }
        public void itmBasedDaily()
        {
            //=====================Time range========

            StrHour = Convert.ToInt16(textBox7.Text);

            if ((StrHour < 1) || (StrHour > 24))
            {
                MessageBox.Show("Selected range is illegal");
                return;
            }
            EndHour = Convert.ToInt16(textBox2.Text);
            if ((EndHour < 1) || (EndHour > 24))
            {
                MessageBox.Show("Selected range is illegal");
                return;
            }
            
            //=======================================
            ///////////////////////////////////////////////////////////////////

            //=====================Date range========
            string strDateitm = PersianDate.Now.ToString();
            string endDateitm = PersianDate.Now.ToString();
            //DateTime startMonth = Convert.ToDateTime(strDate);
            DateTime endMonth = Convert.ToDateTime(faDatePicker1.Text);
            DateTime curK = endMonth; 
          curK= endMonth.AddDays(1);
            //cur.ToString();
            if (faDatePickerbill.Text != "[Empty Value]")
            {
                 strDateitm = faDatePickerbill.Text;
            }
            else { MessageBox.Show("Please Select Date"); }
            //faDatePicker1.Text=faDatePicker1.SelectedDateTime.AddDays(1).ToString();
            if (faDatePicker1.Text != "[Empty Value]")
            {
                endDateitm = faDatePicker1.Text;
               //khorsand endDateitm = curK.ToString();
            }
            else { MessageBox.Show("Please Select Date"); }

            if (strDateitm == endDateitm)
            {
                MessageBox.Show("Please select a valid range of date");
                valPrint = 0;
                return;
            }
            else
            {
                valPrint = 1;
            }
            //=======================================
            //=============Item Selection============
            DataTable ditem = Utilities.GetTable("select * from ItemSelection");
            BillItemPrintExOld billbill = new BillItemPrintExOld(0);
            int itemsNum=ditem.Columns.Count;
            int itemsNums = billbill.checkedListBoxitmslk.Items.Count;
            //string cK = billbill.checkedListBoxitmslk.Items[0].ToString();
            //string[] v = cK.Split('=');
            int inxItmSelk=0;
            for (int ii = 0; ii < itemsNums; ii++)
            {
                string cKk = billbill.checkedListBoxitmslk.Items[ii].ToString();
                string[] vh = cKk.Split('=');
           // if(ditem.Columns[ii].Equals==true)
                if (vh[0].Trim() == ditem.Columns[ii].ToString())
               {
                    if (ditem.Rows[0][ii].ToString().Trim() == "1" || ditem.Rows[0][ii].ToString().Trim() == "True")
                    {
                        billbill.checkedListBoxitmslk.SetItemChecked(ii, true);
                    inxItmSelk = ii;
                     }
                else
                    { billbill.checkedListBoxitmslk.SetItemChecked(ii, false); }

              }   
            }
          
            int cIk = billbill.checkedListBoxitmslk.CheckedItems.Count;
            //string cK = billbill.checkedListBoxitmslk.CheckedItems[0].ToString();
            string cK = billbill.checkedListBoxitmslk.CheckedItems[0].ToString();
            string[] v = cK.Split('=');

            string mm=     v[0].Trim();
            //inxItmSelk = CorrectItemInxData(inxItmSelk);
            int iiItem=inxItmSelk+4;
            //=======================================
            //////////////////////////////////////////////////////////////////
            var doc1 = new Document();
            Boolean xrotate = false;
            if (radioButton2.Checked == true)
            {
                doc1.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                xrotate = true;
            }
            else
            {
                if (xrotate == true)
                {
                    doc1.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                }
            }

            //
           //  string path = @"C:\Program Files\SBS";
            //
            //khorsand
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                "\\SBS";
            ////////////////////
            PdfWriter.GetInstance(doc1, new FileStream(path + "/PrePrintReport.pdf", FileMode.Create));
            doc1.Open();

           // iTextSharp.text.BaseColor.BLUE();
            ///===========================================Print Header================================
            ////PdfPTable table = new PdfPTable(5);DundasImg
            iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(path + "/arm.gif");
            doc1.Add(gif);
           // doc1.Add(new Paragraph(" "));
            iTextSharp.text.Font link = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.UNDERLINE, iTextSharp.text.BaseColor.BLUE);
            Anchor anchor = new Anchor("www.nri.ac.ir", link);
            anchor.Reference = "http://www.nri.ac.ir";
            doc1.Add(anchor);
            //doc1.Add(new Paragraph(" "));
            doc1.Add(new Paragraph("Power Plant Report"));
            if (xrotate == true)
            {
                doc1.Add(new Paragraph("__________________________________________________________________________________________________________________")); 
            }
            else
            {
                doc1.Add(new Paragraph("_____________________________________________________________________________"));
            }
                //PdfPCell cell = new PdfPCell(new Phrase("Header spanning 3 columns"));
            //cell.Colspan = 4;
            //cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //cell.VerticalAlignment = 1;
            //table.AddCell(cell);
            //============================================================================================



            ///////////////////////////////////////////////////////////////////
            iTextSharp.text.Font font8 = iTextSharp.text.FontFactory.GetFont("ARIAL", 6);
            iTextSharp.text.Font fontHeader = iTextSharp.text.FontFactory.GetFont("ARIAL", 10);
            iTextSharp.text.Font font9 = iTextSharp.text.FontFactory.GetFont("ARIAL", 8);
            ///////////////////////////////////////////////////////////////////
            string databaseSlk;
            if (radioButton9.Checked==true)
            { databaseSlk = "MonthlyBillPlant"; }
            else { databaseSlk = "DailyBillPlant"; }
            string queryStr = "select * from '" + databaseSlk;
            string querystr2 = "select * from '" + databaseSlk + "'";
            string querystr3 = "select * from " + databaseSlk ;
            DataTable dt = Utilities.GetTable("select * from MonthlyBillPlant where ppid='"+CMBPLANT.Text.Trim()+"'");
            DataTable dt5 = Utilities.GetTable(querystr3);
            DataTable dt2 = Utilities.GetTable("select * from " + databaseSlk + " where Date between '" + strDateitm + "' and '" + endDateitm + "'and ppid='" + CMBPLANT.Text.Trim() + "'");
 //select * from MonthlyBillPlant where Date between '"+TABLE1.MIN_RANGE+"' and "'+TABLE1.MAX_RANGE+"';
            int rowcountm=dt2.Rows.Count;
            int rowcontdt = dt.Rows.Count;
           //string test1= dt2.Rows[0][2].ToString();
           //string test2 = dt2.Rows[1][2].ToString();
           //string test3 = dt2.Rows[2][2].ToString();
           //string test4 = dt2.Rows[24][2].ToString();
           //string test5 = dt2.Rows[190][2].ToString();
           //string test6 = dt2.Rows[rowcountm-1][2].ToString();
            //Craete instance of the pdf table and set the number of column in that table
            iTextSharp.text.Font font10 = iTextSharp.text.FontFactory.GetFont("ARIAL", 9);

            //Add Header of the pdf table
            // PdfPCell = new PdfPCell(new Phrase(new Chunk("ID", font8)));
            // PdfTable.AddCell(PdfPCell);





            //------------------------------------------------------------------------
            // PdfPCell = new PdfPCell(new Phrase(new Chunk("Name", font8)));
            // PdfTable.AddCell(PdfPCell);

            ////===============================================================
            //How add the data from datatable to pdf table
            //for (int column = 0; column < 10; column++)
            //{
            //   // PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[0][column].ToString(), font8)));
            //   // PdfTable.AddCell(PdfPCell);
            //    PdfPCell c = new PdfPCell();

            //    c.AddElement(new Chunk(o.ToString()));
            //    PdfTable.AddCell(c);
            int colmsCount = EndHour - StrHour + 3;
            int numDays = (rowcountm/24)-1;
            if (numDays == 0) numDays = 1;
           // int[] sumHours = new int[jj];
            int[] sumOneHour = new int[(EndHour - StrHour + 1)];
            double sumHour;
            double sumAll;
            //==================================================


            PdfPTable PdfTable = new PdfPTable(colmsCount+3);
            PdfPCell PdfPCell = null;
            Paragraph paragraph11 = new Paragraph(" ");
            Paragraph paragraph12 = new Paragraph(" ");
            doc1.Add(paragraph11);
            doc1.Add(paragraph12);

            PdfPCell cell = new PdfPCell(new Phrase("Hours"));
            cell.Colspan = 4;
           // cell.Border = 0;
           
            cell.HorizontalAlignment = 1;
            doc1.Add(cell); 
            int itmINX = 0;
            string[] numSTINX=  v[0].Trim().Split('s');
            itmINX = Convert.ToInt16(numSTINX[1].Trim());
            //Write some content
            string strHeader = "Daily report for item " + KeyStrItms[inxItmSelk] + " in date: " + PersianDate.Now.ToString() + "Scientific : " + comboBox2.Text.Trim() + "  Decimel : " + comboBox1.Text.Trim(); 
            Paragraph paragraph = new Paragraph(new Chunk (strHeader,fontHeader));
           /////===================
            /////    //       
            ////// 
            PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = 3;
            //PdfPCell.BorderWidth = 1;
            PdfTable.AddCell(PdfPCell);

            PdfPCell = new PdfPCell(new Phrase(new Chunk("Hours", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            //PdfPCell.BorderWidth = 1;
            PdfPCell.Colspan = (EndHour - StrHour+1);
            PdfTable.AddCell(PdfPCell);

            PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            //PdfPCell.BorderWidth = 1;
            PdfPCell.Colspan = 2;
            PdfTable.AddCell(PdfPCell);
            ////////
            //====PPID
            PdfPCell = new PdfPCell(new Phrase(new Chunk("PPID", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            //PdfPCell.BorderWidth = 1;
            PdfPCell.BorderWidthBottom = 1;
            PdfPCell.BorderWidthLeft = 1;
            PdfPCell.BorderWidthTop = 1;
            PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            PdfTable.AddCell(PdfPCell);
            //====Day
            PdfPCell = new PdfPCell(new Phrase(new Chunk(" Date ", font9)));
            PdfPCell.Colspan = 2;
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.BorderWidthBottom = 1;
            PdfPCell.BorderWidthTop = 1;
            PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;

            //PdfPCell.BorderWidth = 1;
            //PdfPCell.BorderWidthLeft = 8;
          //  PdfPCell.BorderWidthRight = 8;
            PdfTable.AddCell(PdfPCell);

            for (int colms = 3; colms < (colmsCount +1); colms++)
            {
                PdfPCell = new PdfPCell(new Phrase(new Chunk((colms - 3 + StrHour).ToString(), font9)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                //PdfPCell.BorderWidth = 1;
                PdfPCell.BorderWidthBottom = 1;
                PdfPCell.BorderWidthTop=1;
                PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
                PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                PdfPCell.VerticalAlignment = 1;
                PdfTable.AddCell(PdfPCell);
            }

            //PdfPCell = new PdfPCell(new Phrase(new Chunk((colmsCount - 3 + StrHour).ToString(), font9)));
            //    PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //    //PdfPCell.BorderWidth = 1;
            //    PdfPCell.BorderWidthBottom = 1;
            //    PdfPCell.BorderWidthTop = 1;
            //    PdfPCell.BorderWidthRight = 1;
            //    PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            //    PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            //    PdfPCell.VerticalAlignment = 1;
            //    PdfTable.AddCell(PdfPCell);

                PdfPCell = new PdfPCell(new Phrase(new Chunk(" Sum ", font9)));
                PdfPCell.Colspan = 2;
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                PdfPCell.VerticalAlignment = 1;
                PdfPCell.BorderWidthBottom = 1;
                PdfPCell.BorderWidthTop = 1;
                PdfPCell.BorderWidthRight = 1;
                PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
                PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                PdfTable.AddCell(PdfPCell);
            //___________________________________________________________________________________________
            //___________________________________________________________________________________________

            //}
                int rowC;
                sumAll = 0;
            for (int rowDay = 0; rowDay <= numDays; rowDay++)
            {
                PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[(rowDay * 24 + 5)][0].ToString(), font8)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                PdfPCell.VerticalAlignment = 1;

                rowC = (rowDay + 1) % 2;
                if (rowC == 0)
                {
                    PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                }
                PdfTable.AddCell(PdfPCell);
                //=====
                PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[(rowDay * 24 + 5)][2].ToString(), font8)));
                PdfPCell.Colspan = 2;
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                PdfPCell.VerticalAlignment = 1;
                //PdfPCell.Rotation = 45;
              
                //Math.DivRem(rowDay, 2, remC);
                if (rowC == 0)
                {
                    PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                }
                PdfTable.AddCell(PdfPCell);
                sumHour=0;
                for (int rows = 0; rows < (EndHour - StrHour + 1); rows++)
                {
                    
                    //for (int column = 0; column < 10; column++)
                    //  {
                    //PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[rows][8].ToString(), font8)));
                    //PdfTable.AddCell(PdfPCell);
                    //  }
                    // for (int column = 0; column < 10; column++)
                    //   {

                    //=====
                    string val = "";
                    val = dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem].ToString();
                    try
                    {
                        val = decimasci(MyDoubleParse(val));
                    }
                    catch
                    {

                    }



                   //khorsand PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[(rowDay * 24 + StrHour + rows-1)][iiItem].ToString(), font8)));
                    PdfPCell = new PdfPCell(new Phrase(new Chunk(val, font8)));
                    PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    PdfPCell.VerticalAlignment = 1;
                   // sumHour = sumHour +Convert.ToInt32( dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem]);
                    sumOneHour[rows] = sumOneHour[rows] + Convert.ToInt32(dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem]);
                    sumHour = sumHour + Convert.ToDouble(dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem]);
                    // Math.DivRem(rowDay, 2, remC);
                    if (rowC == 0)
                    {
                        PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                    }
                    PdfTable.AddCell(PdfPCell);
                    //  }
                }

                string val2 = "";
                val2 = sumHour.ToString();
                try
                {
                    val2 = decimasci(MyDoubleParse(val2));
                }
                catch
                {

                }
                //khorsand PdfPCell = new PdfPCell(new Phrase(new Chunk(sumHour.ToString(), font8)));
                PdfPCell = new PdfPCell(new Phrase(new Chunk(val2, font8)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                PdfPCell.VerticalAlignment = 1;
                PdfPCell.Colspan = 2;
               // sumHour = sumHour + dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem];
                // Math.DivRem(rowDay, 2, remC);
                if (rowC == 0)
                {
                    PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                }
                PdfTable.AddCell(PdfPCell);
                sumAll = sumAll + sumHour;
            }
            PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font8)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = 1;
           // PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.GREEN;
            PdfTable.AddCell(PdfPCell);

            PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font8)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = 2;
           // PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.GREEN;
            PdfTable.AddCell(PdfPCell);

            for (int rows = 0; rows < (EndHour - StrHour + 1); rows++)
            {

                string val = "";
                val = sumOneHour[rows].ToString();
                try
                {
                    val = decimasci(MyDoubleParse(val));
                }
                catch
                {

                }

               //khorsand PdfPCell = new PdfPCell(new Phrase(new Chunk( sumOneHour[rows].ToString(), font8)));
                PdfPCell = new PdfPCell(new Phrase(new Chunk(val, font8)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                PdfPCell.VerticalAlignment = 1;
                PdfPCell.Colspan = 1;
                PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                PdfTable.AddCell(PdfPCell);
         
            }

            string val1 = "";
            val1 = sumAll.ToString();
            try
            {
                val1 = decimasci(MyDoubleParse(val1));
            }
            catch
            {

            }
           //khorsand PdfPCell = new PdfPCell(new Phrase(new Chunk(sumAll.ToString(), font8)));
            PdfPCell = new PdfPCell(new Phrase(new Chunk(val1, font8)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = 2;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.GREEN;
            PdfTable.AddCell(PdfPCell);

            PdfTable.SpacingBefore = 15f; // Give some space after the text or it may overlap the table
            PdfTable.SpacingAfter = 15f;

            doc1.Add(paragraph);// add paragraph to the document
            doc1.Add(PdfTable); // add pdf table to the document
            ////PdfPTable table = new PdfPTable(5);DundasImg
            //@"E:\Source\Update-CCorumieh-unitbase\PowerPlantProject\PowerPlantProject\Resources";
            if (addChartToDoc == 1)
            {
               // string pathImg = @"C:\Program Files\SBS";
                //khorsand
                string pathImg = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                   "\\SBS";
                ////////////


                iTextSharp.text.Image gif2 = iTextSharp.text.Image.GetInstance(pathImg + "/DundasImg.jpeg");
                gif2.ScalePercent(65f);
                gif2.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                doc1.Add(gif2);
            }
            addChartToDoc = 0;
            doc1.Close();
        }
        public void itmBasedDailyNRI()
        {
            //=====================Time range========

            StrHour = 1;
            EndHour = 1;

            //=======================================
            ///////////////////////////////////////////////////////////////////

            //=====================Date range========
            string strDateitm = PersianDate.Now.ToString();
            string endDateitm = PersianDate.Now.ToString();
            //DateTime startMonth = Convert.ToDateTime(strDate);
            DateTime endMonth = Convert.ToDateTime(faDatePicker1.Text);
            DateTime curK = endMonth;
            curK = endMonth.AddDays(1);
            //cur.ToString();
            if (faDatePickerbill.Text != "[Empty Value]")
            {
                strDateitm = faDatePickerbill.Text;
            }
            else { MessageBox.Show("Please Select Date"); }
            //faDatePicker1.Text=faDatePicker1.SelectedDateTime.AddDays(1).ToString();
            if (faDatePicker1.Text != "[Empty Value]")
            {
                 endDateitm = faDatePicker1.Text;
               //khorsand endDateitm = curK.ToString();
            }
            else { MessageBox.Show("Please Select Date"); }

            if (strDateitm == endDateitm)
            {
                MessageBox.Show("Please select a valid range of date");
                valPrint = 0;
                return;
            }
            else
            {
                valPrint = 1;
            }
            //=======================================
            //=============Item Selection============
          // DataTable ditem = utilities.GetTable("select * from ItemSelection");
          //  BillItemPrintEx billbill = new BillItemPrintEx(0);
          // int itemsNum = ditem.Columns.Count;
           // int itemsNums = billbill.checkedListBoxitmslk.Items.Count;
            //string cK = billbill.checkedListBoxitmslk.Items[0].ToString();
            //string[] v = cK.Split('=');
            //int inxItmSelk = 0;
            //for (int ii = 0; ii < itemsNums; ii++)
            //{
            //    string cKk = billbill.checkedListBoxitmslk.Items[ii].ToString();
            //    string[] vh = cKk.Split('=');
            //    // if(ditem.Columns[ii].Equals==true)
            //    if (vh[0].Trim() == ditem.Columns[ii].ToString())
            //    {
            //        if (ditem.Rows[0][ii].ToString().Trim() == "1" || ditem.Rows[0][ii].ToString().Trim() == "True")
            //        {
            //            billbill.checkedListBoxitmslk.SetItemChecked(ii, true);
            //            inxItmSelk = ii;
            //        }
            //        else
            //        { billbill.checkedListBoxitmslk.SetItemChecked(ii, false); }

            //    }
            //}

            //int cIk = billbill.checkedListBoxitmslk.CheckedItems.Count;
            ////string cK = billbill.checkedListBoxitmslk.CheckedItems[0].ToString();
            //string cK = billbill.checkedListBoxitmslk.CheckedItems[0].ToString();
            //string[] v = cK.Split('=');
            //string mm = v[0].Trim();
            //int iiItem = inxItmSelk + 4;
            //=======================================
            //////////////////////////////////////////////////////////////////
            var doc1 = new Document();
            Boolean xrotate = false;
            if (radioButton2.Checked == true)
            {
                doc1.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                xrotate = true;
            }
            else
            {
                if (xrotate == true)
                {
                    doc1.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                }
            }

          //  string path = @"C:\Program Files\SBS";
            //khorsand
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
               "\\SBS";
            ////////////
            PdfWriter.GetInstance(doc1, new FileStream(path + "/PrePrintReport.pdf", FileMode.Create));
            doc1.Open();

            // iTextSharp.text.BaseColor.BLUE();
            ///===========================================Print Header================================
            ////PdfPTable table = new PdfPTable(5);DundasImg
            iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(path + "/arm.gif");
            doc1.Add(gif);
            // doc1.Add(new Paragraph(" "));
            iTextSharp.text.Font link = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.UNDERLINE, iTextSharp.text.BaseColor.BLUE);
            Anchor anchor = new Anchor("www.nri.ac.ir", link);
            anchor.Reference = "http://www.nri.ac.ir";
            doc1.Add(anchor);
            //doc1.Add(new Paragraph(" "));
            doc1.Add(new Paragraph("Power Plant Report"));
            if (xrotate == true)
            {
                doc1.Add(new Paragraph("__________________________________________________________________________________________________________________"));
            }
            else
            {
                doc1.Add(new Paragraph("_____________________________________________________________________________"));
            }
            //PdfPCell cell = new PdfPCell(new Phrase("Header spanning 3 columns"));
            //cell.Colspan = 4;
            //cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //cell.VerticalAlignment = 1;
            //table.AddCell(cell);
            //============================================================================================



            ///////////////////////////////////////////////////////////////////
            iTextSharp.text.Font font8 = iTextSharp.text.FontFactory.GetFont("ARIAL", 6);
            iTextSharp.text.Font fontHeader = iTextSharp.text.FontFactory.GetFont("ARIAL", 10);
            iTextSharp.text.Font font9 = iTextSharp.text.FontFactory.GetFont("ARIAL", 8);
            ///////////////////////////////////////////////////////////////////
            string databaseSlk;
            //if (radioButton9.Checked == true)
            //{
            databaseSlk = "EconomicPlant";
        //}
            //else { databaseSlk = "DailyBillPlant"; }
           // string queryStr = "select * from '" + databaseSlk;
           // string querystr2 = "select * from '" + databaseSlk + "'";
          //  string querystr3 = "select * from " + databaseSlk;
         //   DataTable dt = utilities.GetTable("select * from MonthlyBillPlant");
           // DataTable dt5 = utilities.GetTable(querystr3);
            DataTable dt2 = Utilities.GetTable("select * from " + databaseSlk + " where Date between '" + strDateitm + "' and '" + endDateitm + "'and ppid='" + CMBPLANT.Text.Trim() + "'");
            //select * from MonthlyBillPlant where Date between '"+TABLE1.MIN_RANGE+"' and "'+TABLE1.MAX_RANGE+"';
            int rowcountm = dt2.Rows.Count;
            int colcountm = dt2.Columns.Count;
            
           // int rowcontdt = dt.Rows.Count;
            //string test1= dt2.Rows[0][2].ToString();
            //string test2 = dt2.Rows[1][2].ToString();
            //string test3 = dt2.Rows[2][2].ToString();
            //string test4 = dt2.Rows[24][2].ToString();
            //string test5 = dt2.Rows[190][2].ToString();
            //string test6 = dt2.Rows[rowcountm-1][2].ToString();
            //Craete instance of the pdf table and set the number of column in that table
            iTextSharp.text.Font font10 = iTextSharp.text.FontFactory.GetFont("ARIAL", 9);

            //Add Header of the pdf table
            // PdfPCell = new PdfPCell(new Phrase(new Chunk("ID", font8)));
            // PdfTable.AddCell(PdfPCell);





            //------------------------------------------------------------------------
            // PdfPCell = new PdfPCell(new Phrase(new Chunk("Name", font8)));
            // PdfTable.AddCell(PdfPCell);

            ////===============================================================
            //How add the data from datatable to pdf table
            //for (int column = 0; column < 10; column++)
            //{
            //   // PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[0][column].ToString(), font8)));
            //   // PdfTable.AddCell(PdfPCell);
            //    PdfPCell c = new PdfPCell();

            //    c.AddElement(new Chunk(o.ToString()));
            //    PdfTable.AddCell(c);
           // int colmsCount = EndHour - StrHour + 3;
            int numDays = (rowcountm / 24) - 1;
            // int[] sumHours = new int[jj];
            int[] sumOneHour = new int[colcountm];
           // double sumHour;
           // double sumAll;
            //==================================================

            //1 bod ghablan
            PdfPTable PdfTable = new PdfPTable(colcountm + 1);
            PdfPCell PdfPCell = null;
            Paragraph paragraph11 = new Paragraph(" ");
            Paragraph paragraph12 = new Paragraph(" ");
            doc1.Add(paragraph11);
            doc1.Add(paragraph12);

            PdfPCell cell = new PdfPCell(new Phrase("Items"));
            cell.Colspan = 4;
            // cell.Border = 0;

            cell.HorizontalAlignment = 1;
            doc1.Add(cell);
            //int itmINX = 0;
            //string[] numSTINX = v[0].Trim().Split('s');
            //itmINX = Convert.ToInt16(numSTINX[1].Trim());
            //Write some content
            string strHeader = " Daily Report For NriBill in date: " + PersianDate.Now.ToString()  + "Scientific : "+comboBox2.Text.Trim() +"  Decimel : "+ comboBox1.Text.Trim();
            Paragraph paragraph = new Paragraph(new Chunk(strHeader, fontHeader));
            /////===================
            /////    //       
            ////// 
            PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = 3;
            //PdfPCell.BorderWidth = 1;
            PdfTable.AddCell(PdfPCell);

            PdfPCell = new PdfPCell(new Phrase(new Chunk("Items", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = colcountm -2;
            //PdfPCell.BorderWidth = 1;
            
            PdfTable.AddCell(PdfPCell);


            //khorsand//////////////////////////////////////////
            //PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font9)));
            //PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //PdfPCell.VerticalAlignment = 1;
            //PdfPCell.Colspan = colcountm - 2;
            ////PdfPCell.BorderWidth = 1;
            //PdfPCell.Colspan = 2;
            //PdfTable.AddCell(PdfPCell);
            ////////////////////////////////////////////



            ////////
            //====PPID
            PdfPCell = new PdfPCell(new Phrase(new Chunk("PPID", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            //PdfPCell.BorderWidth = 1;
            PdfPCell.BorderWidthBottom = 1;
            PdfPCell.BorderWidthLeft = 1;
            PdfPCell.BorderWidthTop = 1;
            PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            PdfTable.AddCell(PdfPCell);
            //====Day
            PdfPCell = new PdfPCell(new Phrase(new Chunk(" Date ", font9)));
            PdfPCell.Colspan = 2;
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.BorderWidthBottom = 1;
            PdfPCell.BorderWidthTop = 1;
            PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;

            //PdfPCell.BorderWidth = 1;
            //PdfPCell.BorderWidthLeft = 8;
            //  PdfPCell.BorderWidthRight = 8;
            PdfTable.AddCell(PdfPCell);


           



            //for (int colms = 3; colms < (colmsCount + 1); colms++)
            //{
            for (int ijk = 2; ijk < colcountm; ijk++)
            {

             
                PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Columns[ijk].ColumnName.ToString(), font8)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                //    //PdfPCell.BorderWidth = 1;
                PdfPCell.BorderWidthBottom = 1;
                PdfPCell.Colspan = 1;
                PdfPCell.BorderWidthTop = 1;
                PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
                PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                PdfPCell.VerticalAlignment = 1;
                PdfTable.AddCell(PdfPCell);
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////

            ///////khorsand/////////////////////////
            //PdfPCell = new PdfPCell(new Phrase(new Chunk(" Sum ", font9)));
            //PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right         
            //PdfPCell.BorderWidthBottom = 1;
            //PdfPCell.Colspan = 2;
            //PdfPCell.BorderWidthTop = 1;
            //PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            //PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            //PdfPCell.VerticalAlignment = 1;
            //PdfTable.AddCell(PdfPCell);
            /////////////////////////////////////////////

             double[] cx=new double[15];

            
            //}
            int rowC;
            //sumAll = 0;
            for (int rowDay = 0; rowDay < rowcountm; rowDay++)
            {
               
                PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[rowDay][0].ToString(), font8)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                PdfPCell.VerticalAlignment = 1;

                rowC = (rowDay + 1) % 2;
                if (rowC == 0)
                {
                    PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                }
                PdfTable.AddCell(PdfPCell);
                //    //=====
                PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[rowDay][1].ToString(), font8)));
                PdfPCell.Colspan = 2;
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                PdfPCell.VerticalAlignment = 1;
                //    //PdfPCell.Rotation = 45;

                //    //Math.DivRem(rowDay, 2, remC);
                if (rowC == 0)
                {
                    PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                }
                PdfTable.AddCell(PdfPCell);
               //  sumHour = 0;
               
                for (int cols = 2; cols < colcountm; cols++)
                {
                    string val = "";
                    val = dt2.Rows[rowDay][cols].ToString();
                    try
                    {
                        val = decimasci(MyDoubleParse(val));
                    }
                    catch
                    {

                    }
                    //        //=====
                   cx[cols - 2] += MyDoubleParse(dt2.Rows[rowDay][cols].ToString());
                    // khorsand  PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[rowDay][cols].ToString(), font8)));
                    PdfPCell = new PdfPCell(new Phrase(new Chunk(val, font8)));

                    PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    PdfPCell.VerticalAlignment = 1;
                    //        // sumHour = sumHour +Convert.ToInt32( dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem]);
                    //       // sumOneHour[rows] = sumOneHour[rows] + Convert.ToInt32(dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem]);
                    //        //sumHour = sumHour + Convert.ToDouble(dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem]);
                    //        // Math.DivRem(rowDay, 2, remC);
                    if (rowC == 0)
                    {
                        PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                    }
                    PdfTable.AddCell(PdfPCell);
                    //        //  }
                }
                //    //PdfPCell = new PdfPCell(new Phrase(new Chunk(sumHour.ToString(), font8)));

                ////khorsand////////////////////////////////////////////////////////////////////
                //PdfPCell = new PdfPCell(new Phrase(new Chunk("0", font8)));
                //PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                //PdfPCell.VerticalAlignment = 1;
                //PdfPCell.Colspan = 2;
                //// sumHour = sumHour + dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem];
                //// Math.DivRem(rowDay, 2, remC);              
                //PdfTable.AddCell(PdfPCell);
                //////////////////////////////////////////////////////////////////////////////////
            }

            /////khorsand////////////////////////////////////

            PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font8)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = 1;
            // PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.GREEN;
            PdfTable.AddCell(PdfPCell);

            PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font8)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = 2;
            // PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.GREEN;
            PdfTable.AddCell(PdfPCell);


            for (int rows = 0; rows < 15; rows++)
            {
                string val = "";
                val = cx[rows].ToString();
                try
                {
                    val = decimasci(MyDoubleParse(val));
                }
                catch
                {

                }
                PdfPCell = new PdfPCell(new Phrase(new Chunk(val, font8)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                PdfPCell.VerticalAlignment = 1;
                PdfPCell.Colspan = 1;
                PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                PdfTable.AddCell(PdfPCell);

            }
        

            ///////////////////////khorsand//////////////////////////////


            PdfTable.SpacingBefore = 15f; // Give some space after the text or it may overlap the table
            PdfTable.SpacingAfter = 15f;

            doc1.Add(paragraph);// add paragraph to the document
            doc1.Add(PdfTable); // add pdf table to the document
            ////PdfPTable table = new PdfPTable(5);DundasImg
            //@"E:\Source\Update-CCorumieh-unitbase\PowerPlantProject\PowerPlantProject\Resources";
            if (addChartToDoc == 1)
            {
               // string pathImg = @"C:\Program Files\SBS";
                //khorsand
                string pathImg = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                   "\\SBS";
                ////////////

                iTextSharp.text.Image gif2 = iTextSharp.text.Image.GetInstance(pathImg + "/DundasImg.jpeg");
                gif2.ScalePercent(65f);
                gif2.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                doc1.Add(gif2);
            }
            addChartToDoc = 0;
            doc1.Close();
            //PdfPCell = new PdfPCell(new Phrase(new Chunk((colmsCount - 3 + StrHour).ToString(), font9)));
            //    PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //    //PdfPCell.BorderWidth = 1;
            //    PdfPCell.BorderWidthBottom = 1;
            //    PdfPCell.BorderWidthTop = 1;
            //    PdfPCell.BorderWidthRight = 1;
            //    PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            //    PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            //    PdfPCell.VerticalAlignment = 1;
            //    PdfTable.AddCell(PdfPCell);

            //PdfPCell = new PdfPCell(new Phrase(new Chunk(" Sum ", font9)));
            //PdfPCell.Colspan = 2;
            //PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //PdfPCell.VerticalAlignment = 1;
            //PdfPCell.BorderWidthBottom = 1;
            //PdfPCell.BorderWidthTop = 1;
            //PdfPCell.BorderWidthRight = 1;
            //PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            //PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            //PdfTable.AddCell(PdfPCell);
            //___________________________________________________________________________________________
            //___________________________________________________________________________________________

            //}
            //int rowC;
            //sumAll = 0;
            //for (int rowDay = 0; rowDay < numDays; rowDay++)
            //{
            //    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[(rowDay * 24 + 5)][0].ToString(), font8)));
            //    PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //    PdfPCell.VerticalAlignment = 1;

            //    rowC = (rowDay + 1) % 2;
            //    if (rowC == 0)
            //    {
            //        PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
            //    }
            //    PdfTable.AddCell(PdfPCell);
            //    //=====
            //    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[(rowDay * 24 + 5)][2].ToString(), font8)));
            //    PdfPCell.Colspan = 2;
            //    PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //    PdfPCell.VerticalAlignment = 1;
            //    //PdfPCell.Rotation = 45;

            //    //Math.DivRem(rowDay, 2, remC);
            //    if (rowC == 0)
            //    {
            //        PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
            //    }
            //    PdfTable.AddCell(PdfPCell);
            //    sumHour = 0;
            //    for (int rows = 0; rows < (colcountm); rows++)
            //    {

            //        //for (int column = 0; column < 10; column++)
            //        //  {
            //        //PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[rows][8].ToString(), font8)));
            //        //PdfTable.AddCell(PdfPCell);
            //        //  }
            //        // for (int column = 0; column < 10; column++)
            //        //   {

            //        //=====
            //        PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem].ToString(), font8)));
            //        PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //        PdfPCell.VerticalAlignment = 1;
            //        // sumHour = sumHour +Convert.ToInt32( dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem]);
            //       // sumOneHour[rows] = sumOneHour[rows] + Convert.ToInt32(dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem]);
            //        //sumHour = sumHour + Convert.ToDouble(dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem]);
            //        // Math.DivRem(rowDay, 2, remC);
            //        if (rowC == 0)
            //        {
            //            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
            //        }
            //        PdfTable.AddCell(PdfPCell);
            //        //  }
            //    }
            //    //PdfPCell = new PdfPCell(new Phrase(new Chunk(sumHour.ToString(), font8)));
            //    //PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //    //PdfPCell.VerticalAlignment = 1;
            //    //PdfPCell.Colspan = 2;
            //    //// sumHour = sumHour + dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem];
            //    //// Math.DivRem(rowDay, 2, remC);
            //    //if (rowC == 0)
            //    //{
            //    //    PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
            //    //}
            //    //PdfTable.AddCell(PdfPCell);
            //    //sumAll = sumAll + sumHour;
            //}
            ////////////////////////////////////////////////////////////////////////////////////////////////
            //PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font8)));
            //PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //PdfPCell.VerticalAlignment = 1;
            //PdfPCell.Colspan = 1;
            //// PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.GREEN;
            //PdfTable.AddCell(PdfPCell);

            //PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font8)));
            //PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //PdfPCell.VerticalAlignment = 1;
            //PdfPCell.Colspan = 2;
            //// PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.GREEN;
            //PdfTable.AddCell(PdfPCell);

           // for (int rows = 0; rows < (colcountm); rows++)
           // {
           //     PdfPCell = new PdfPCell(new Phrase(new Chunk(sumOneHour[rows].ToString(), font8)));
           //    PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
           //     PdfPCell.VerticalAlignment = 1;
           //   PdfPCell.Colspan = 1;
           //     PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
           //     PdfTable.AddCell(PdfPCell);

           //}
            //PdfPCell = new PdfPCell(new Phrase(new Chunk(sumAll.ToString(), font8)));
            //PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //PdfPCell.VerticalAlignment = 1;
            //PdfPCell.Colspan = 2;
            //PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.GREEN;
            //PdfTable.AddCell(PdfPCell);

          // PdfTable.SpacingBefore = 15f; // Give some space after the text or it may overlap the table
          //PdfTable.SpacingAfter = 15f;

          //  doc1.Add(paragraph);// add paragraph to the document
          //  doc1.Add(PdfTable); // add pdf table to the document
          //  ////PdfPTable table = new PdfPTable(5);DundasImg
          //  //@"E:\Source\Update-CCorumieh-unitbase\PowerPlantProject\PowerPlantProject\Resources";
          //  if (addChartToDoc == 1)
          //  {
          //      string pathImg = @"C:\Program Files\SBS";
          //      iTextSharp.text.Image gif2 = iTextSharp.text.Image.GetInstance(pathImg + "/DundasImg.jpeg");
          //      gif2.ScalePercent(65f);
          //      gif2.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
          //      doc1.Add(gif2);
          //  }
          //  addChartToDoc = 0;
          //  doc1.Close();
        }

        public void itmBasedDailyamadegitolid()
        {
            //=====================Time range========

            StrHour = 1;
            EndHour = 1;

            //=======================================
            ///////////////////////////////////////////////////////////////////

            //=====================Date range========
            string strDateitm = PersianDate.Now.ToString();
            string endDateitm = PersianDate.Now.ToString();
            //DateTime startMonth = Convert.ToDateTime(strDate);
            DateTime endMonth = Convert.ToDateTime(faDatePicker1.Text);
            DateTime curK = endMonth;
            curK = endMonth.AddDays(1);
            //cur.ToString();
            if (faDatePickerbill.Text != "[Empty Value]")
            {
                strDateitm = faDatePickerbill.Text;
            }
            else { MessageBox.Show("Please Select Date"); }
            //faDatePicker1.Text=faDatePicker1.SelectedDateTime.AddDays(1).ToString();
            if (faDatePicker1.Text != "[Empty Value]")
            {
                endDateitm = faDatePicker1.Text;
                //khorsand endDateitm = curK.ToString();
            }
            else { MessageBox.Show("Please Select Date"); }

            if (strDateitm == endDateitm)
            {
                MessageBox.Show("Please select a valid range of date");
                valPrint = 0;
                return;
            }
            else
            {
                valPrint = 1;
            }

            //////////////////////////////////////////////////////////////////
            var doc1 = new Document();
            Boolean xrotate = false;
            if (radioButton2.Checked == true)
            {
                doc1.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                xrotate = true;
            }
            else
            {
                if (xrotate == true)
                {
                    doc1.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                }
            }

            //  string path = @"C:\Program Files\SBS";
            //khorsand
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
               "\\SBS";
            ////////////
            PdfWriter.GetInstance(doc1, new FileStream(path + "/PrePrintReport.pdf", FileMode.Create));
            doc1.Open();

            // iTextSharp.text.BaseColor.BLUE();
            ///===========================================Print Header================================
            ////PdfPTable table = new PdfPTable(5);DundasImg
            iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(path + "/arm.gif");
            doc1.Add(gif);
            // doc1.Add(new Paragraph(" "));
            iTextSharp.text.Font link = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.UNDERLINE, iTextSharp.text.BaseColor.BLUE);
            Anchor anchor = new Anchor("www.nri.ac.ir", link);
            anchor.Reference = "http://www.nri.ac.ir";
            doc1.Add(anchor);
            //doc1.Add(new Paragraph(" "));
            doc1.Add(new Paragraph("Power Plant Report"));
            if (xrotate == true)
            {
                doc1.Add(new Paragraph("__________________________________________________________________________________________________________________"));
            }
            else
            {
                doc1.Add(new Paragraph("_____________________________________________________________________________"));
            }
            //PdfPCell cell = new PdfPCell(new Phrase("Header spanning 3 columns"));
            //cell.Colspan = 4;
            //cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //cell.VerticalAlignment = 1;
            //table.AddCell(cell);
            //============================================================================================



            ///////////////////////////////////////////////////////////////////
            iTextSharp.text.Font font8 = iTextSharp.text.FontFactory.GetFont("ARIAL", 6);
            iTextSharp.text.Font fontHeader = iTextSharp.text.FontFactory.GetFont("ARIAL", 10);
            iTextSharp.text.Font font9 = iTextSharp.text.FontFactory.GetFont("ARIAL", 8);
            ///////////////////////////////////////////////////////////////////
            string databaseSlk;
            //if (radioButton9.Checked == true)
            //{
            databaseSlk = "EconomicPlant";
            //}
            //else { databaseSlk = "DailyBillPlant"; }
            // string queryStr = "select * from '" + databaseSlk;
            // string querystr2 = "select * from '" + databaseSlk + "'";
            //  string querystr3 = "select * from " + databaseSlk;
            //   DataTable dt = utilities.GetTable("select * from MonthlyBillPlant");
            // DataTable dt5 = utilities.GetTable(querystr3);
            DataTable dt2 = Utilities.GetTable("select * from " + databaseSlk + " where Date between '" + strDateitm + "' and '" + endDateitm + "'and ppid='" + CMBPLANT.Text.Trim() + "'");
            //select * from MonthlyBillPlant where Date between '"+TABLE1.MIN_RANGE+"' and "'+TABLE1.MAX_RANGE+"';




          



            DateTime startMonth1 = PersianDateConverter.ToGregorianDateTime(strDateitm);
            DateTime endMonth1 = PersianDateConverter.ToGregorianDateTime(endDateitm);
            
            DateTime cur = startMonth1;
            TimeSpan span = endMonth1 - startMonth1;
            int diff = span.Days + 1;



            int rowcountm =diff;
            int colcountm = 5;
            //////////////////////////////////////////////////////////////////////////////////////////


           
            iTextSharp.text.Font font10 = iTextSharp.text.FontFactory.GetFont("ARIAL", 9);

         
            int[] sumOneHour = new int[colcountm];
            // double sumHour;
            // double sumAll;
            //==================================================

            //1 bod ghablan
            PdfPTable PdfTable = new PdfPTable(colcountm + 1);
            PdfPCell PdfPCell = null;
            Paragraph paragraph11 = new Paragraph(" ");
            Paragraph paragraph12 = new Paragraph(" ");
            doc1.Add(paragraph11);
            doc1.Add(paragraph12);

            PdfPCell cell = new PdfPCell(new Phrase("Items"));
            cell.Colspan = 4;
            // cell.Border = 0;

            cell.HorizontalAlignment = 1;
            doc1.Add(cell);
            //int itmINX = 0;
            //string[] numSTINX = v[0].Trim().Split('s');
            //itmINX = Convert.ToInt16(numSTINX[1].Trim());
            //Write some content
           
            string strHeader = "Daily Report For PurePower in date: " + PersianDate.Now.ToString() + "Scientific : " + comboBox2.Text.Trim() + "  Decimel : " + comboBox1.Text.Trim();
            if (rddeclare.Checked) strHeader = "Daily Report For Capacity in date: " + PersianDate.Now.ToString() + "Scientific : " + comboBox2.Text.Trim() + "  Decimel : " + comboBox1.Text.Trim();

            Paragraph paragraph = new Paragraph(new Chunk(strHeader, fontHeader));
            /////===================
            /////    //       
            ////// 
            PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = 3;
            //PdfPCell.BorderWidth = 1;
            PdfTable.AddCell(PdfPCell);

            PdfPCell = new PdfPCell(new Phrase(new Chunk("Items", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = colcountm - 2;
            //PdfPCell.BorderWidth = 1;

            PdfTable.AddCell(PdfPCell);


            //khorsand//////////////////////////////////////////
            //PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font9)));
            //PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //PdfPCell.VerticalAlignment = 1;
            //PdfPCell.Colspan = colcountm - 2;
            ////PdfPCell.BorderWidth = 1;
            //PdfPCell.Colspan = 2;
            //PdfTable.AddCell(PdfPCell);
            ////////////////////////////////////////////



            ////////
            //====PPID
            PdfPCell = new PdfPCell(new Phrase(new Chunk("PPID", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            //PdfPCell.BorderWidth = 1;
            PdfPCell.BorderWidthBottom = 1;
            PdfPCell.BorderWidthLeft = 1;
            PdfPCell.BorderWidthTop = 1;
            PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            PdfTable.AddCell(PdfPCell);
            //====Day
            PdfPCell = new PdfPCell(new Phrase(new Chunk(" Date ", font9)));
            PdfPCell.Colspan = 2;
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.BorderWidthBottom = 1;
            PdfPCell.BorderWidthTop = 1;
            PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;

            //PdfPCell.BorderWidth = 1;
            //PdfPCell.BorderWidthLeft = 8;
            //  PdfPCell.BorderWidthRight = 8;
            PdfTable.AddCell(PdfPCell);






            //for (int colms = 3; colms < (colmsCount + 1); colms++)
            //{
            string [] name=new string[colcountm];
            name[0] = "Base";
            name[1] = "Medium";
            name[2] = "peak";
            for (int ijk = 2; ijk < colcountm; ijk++)
            {


                PdfPCell = new PdfPCell(new Phrase(new Chunk(name[ijk - 2], font8)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                //    //PdfPCell.BorderWidth = 1;
                PdfPCell.BorderWidthBottom = 1;
                PdfPCell.Colspan = 1;
                PdfPCell.BorderWidthTop = 1;
                PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
                PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                PdfPCell.VerticalAlignment = 1;
                PdfTable.AddCell(PdfPCell);
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////

            ///////khorsand/////////////////////////
            //PdfPCell = new PdfPCell(new Phrase(new Chunk(" Sum ", font9)));
            //PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right         
            //PdfPCell.BorderWidthBottom = 1;
            //PdfPCell.Colspan = 2;
            //PdfPCell.BorderWidthTop = 1;
            //PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            //PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            //PdfPCell.VerticalAlignment = 1;
            //PdfTable.AddCell(PdfPCell);
            /////////////////////////////////////////////

            double[] cx = new double[3];
            DataTable xxx = null;

            //}
            int rowC;
            //sumAll = 0;
            for (int rowDay = 0; rowDay < rowcountm; rowDay++)
            {

                PdfPCell = new PdfPCell(new Phrase(new Chunk(CMBPLANT.Text.Trim(), font8)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                PdfPCell.VerticalAlignment = 1;

                rowC = (rowDay + 1) % 2;
                if (rowC == 0)
                {
                    PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                }
                PdfTable.AddCell(PdfPCell);
                //    //=====


                string ssd = new PersianDate(PersianDateConverter.ToGregorianDateTime(strDateitm).AddDays(rowDay)).ToString("d");
                PdfPCell = new PdfPCell(new Phrase(new Chunk(ssd, font8)));
                PdfPCell.Colspan = 2;
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                PdfPCell.VerticalAlignment = 1;
                //    //PdfPCell.Rotation = 45;

                //    //Math.DivRem(rowDay, 2, remC);
                if (rowC == 0)
                {
                    PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                }
                PdfTable.AddCell(PdfPCell);
                //  sumHour = 0;

                for (int cols = 2; cols < colcountm; cols++)
                {

                    ////////////////////////////////////////////////////////////////////////////
                    DataTable zx = Utilities.GetTable("select * from dbo.bourse where fromdate<='" + ssd + "' and todate>='" + ssd + "' order by fromdate,id desc");
                    DataTable dt31 = Utilities.GetTable("select AvailableCapacity from  economicplant where Date='" + ssd + "'and ppid='" + CMBPLANT.Text.Trim() + "'");
                    double basse = 0;
                    double peak = 0;
                    double medium = 0;
                    double sum = 0;
                    for (int i = 0; i < 24; i++)
                    {

                        if(rddeclare.Checked)
                            xxx = Utilities.GetTable("select sum(dispachablecapacity) from dbo.DetailFRM002 where TargetMarketDate='" + ssd + "'and  Hour='" + (i + 1) + "'and Estimated=0 and ppid='" + CMBPLANT.Text.Trim() + "'");
                        else
                            xxx = Utilities.GetTable("select sum(p) from dbo.DetailFRM009 where TargetMarketDate='" + ssd + "'and  Hour='" + (i) + "'and ppid='" + CMBPLANT.Text.Trim() + "'");
                        if (zx.Rows[0][i + 3].ToString() == "Base") basse += MyDoubleParse(xxx.Rows[0][0].ToString());
                        if (zx.Rows[0][i + 3].ToString() == "Medium") medium += MyDoubleParse(xxx.Rows[0][0].ToString());
                        if (zx.Rows[0][i + 3].ToString() == "peak") peak += MyDoubleParse(xxx.Rows[0][0].ToString());


                    }






                    string val = "";
                    if (cols == 2) val = basse.ToString();
                    if (cols == 3) val = medium.ToString();
                    if (cols == 4) val = peak.ToString();
                  //  val = dt2.Rows[rowDay][cols].ToString();
                    try
                    {
                        val = decimasci(MyDoubleParse(val));
                    }
                    catch
                    {

                    }
                    //        //=====
                    cx[cols - 2] += MyDoubleParse(val);
                    // khorsand  PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[rowDay][cols].ToString(), font8)));
                    PdfPCell = new PdfPCell(new Phrase(new Chunk(val, font8)));

                    PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    PdfPCell.VerticalAlignment = 1;
                    //        // sumHour = sumHour +Convert.ToInt32( dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem]);
                    //       // sumOneHour[rows] = sumOneHour[rows] + Convert.ToInt32(dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem]);
                    //        //sumHour = sumHour + Convert.ToDouble(dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem]);
                    //        // Math.DivRem(rowDay, 2, remC);
                    if (rowC == 0)
                    {
                        PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                    }
                    PdfTable.AddCell(PdfPCell);
                    //        //  }
                }
                //    //PdfPCell = new PdfPCell(new Phrase(new Chunk(sumHour.ToString(), font8)));

                ////khorsand////////////////////////////////////////////////////////////////////
                //PdfPCell = new PdfPCell(new Phrase(new Chunk("0", font8)));
                //PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                //PdfPCell.VerticalAlignment = 1;
                //PdfPCell.Colspan = 2;
                //// sumHour = sumHour + dt2.Rows[(rowDay * 24 + StrHour + rows - 1)][iiItem];
                //// Math.DivRem(rowDay, 2, remC);              
                //PdfTable.AddCell(PdfPCell);
                //////////////////////////////////////////////////////////////////////////////////
            }

            /////khorsand////////////////////////////////////

            PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font8)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = 1;
            // PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.GREEN;
            PdfTable.AddCell(PdfPCell);

            PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font8)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = 2;
            // PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.GREEN;
            PdfTable.AddCell(PdfPCell);


            for (int rows = 0; rows < cx.Length; rows++)
            {
                string val = "";
                val = cx[rows].ToString();
                try
                {
                    val = decimasci(MyDoubleParse(val));
                }
                catch
                {

                }
                PdfPCell = new PdfPCell(new Phrase(new Chunk(val, font8)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                PdfPCell.VerticalAlignment = 1;
                PdfPCell.Colspan = 1;
                PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                PdfTable.AddCell(PdfPCell);

            }


            ///////////////////////khorsand//////////////////////////////


            PdfTable.SpacingBefore = 15f; // Give some space after the text or it may overlap the table
            PdfTable.SpacingAfter = 15f;

            doc1.Add(paragraph);// add paragraph to the document
            doc1.Add(PdfTable); // add pdf table to the document
            ////PdfPTable table = new PdfPTable(5);DundasImg
            //@"E:\Source\Update-CCorumieh-unitbase\PowerPlantProject\PowerPlantProject\Resources";
            if (addChartToDoc == 1)
            {
                // string pathImg = @"C:\Program Files\SBS";
                //khorsand
                string pathImg = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                   "\\SBS";
                ////////////

                iTextSharp.text.Image gif2 = iTextSharp.text.Image.GetInstance(pathImg + "/DundasImg.jpeg");
                gif2.ScalePercent(65f);
                gif2.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                doc1.Add(gif2);
            }
            addChartToDoc = 0;
            doc1.Close();
           

            

        }







        /// <summary>
        /// ///////////////////////////////////////////////////////
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button7_Click(object sender, EventArgs e)
        {

            try
            {
                prepareNameArray(faDatePicker1.Text);
                if (keyActivNRI == 0)
                {
                    itmBasedDaily();
                }
                else if (rdnri.Checked)
                {
                    itmBasedDailyNRI();
                }
                else
                {
                    itmBasedDailyamadegitolid();
                }
                if (valPrint == 1)
                {
                    //string path = @"C:\Program Files\SBS";

                    //khorsand
                    string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                       "\\SBS";
                    ////////////


                    string pdfPathdoc = path + "/PrePrintReport.pdf";
                    if (System.IO.File.Exists(pdfPathdoc))
                    {
                        PrintPreviewForm prPform = new PrintPreviewForm(pdfPathdoc);
                        prPform.Show();
                        // System.Diagnostics.Process.Start(pdfPathdoc);

                        //Pdfviewer.Navigate(pdfPathdoc);
                        valPrint = 0;
                    }
                    //string destinationFile = @"C:\Program Files\SBS\DundasImg.jpeg";

                    //khorsand
                    string destinationFile = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                       "\\SBS\\DundasImg.jpeg";
                    ////////////
                    try
                    {
                        File.Delete(destinationFile);
                    }
                    catch (IOException iox)
                    {
                        Console.WriteLine(iox.Message);
                    }

                }
            }
            catch
            {
                MessageBox.Show("Incomplete Data !!");
            }


        }
        ////////+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        /// <summary>
        /// This function creates a date-based print from powerplant database.
        /// In this function we select a spesific date and then after item selection 
        /// we would have one print of these informations
        /// for creating pdf from data we choose iTextSharp library that provides helpful functions
        /// for our purposes.
        /// </summary>
        /////////++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        public void datBsdDaily()
        {
            //=====================Time range========

            StrHour = Convert.ToInt16(textBox4.Text);

            if ((StrHour < 1) || (StrHour > 24))
            {
                MessageBox.Show("Selected range is illegal");
                return;
            }
            EndHour = Convert.ToInt16(textBox3.Text);
            if ((EndHour < 1) || (EndHour > 24))
            {
                MessageBox.Show("Selected range is illegal");
                return;
            }
            //=======================================
            ///////////////////////////////////////////////////////////////////

            //=====================Date range========
            //string singlDate = faDatePicker2.Text;
            string singlDate = PersianDate.Now.ToString();
            if (faDatePicker2.Text != "[Empty Value]")
            {
                 singlDate = faDatePicker2.Text;
            }
            else { MessageBox.Show("Please Select Date"); }

            //=======================================
            //=============Item Selection============
            DataTable ditem = Utilities.GetTable("select * from ItemSelection");
            BillItemPrintExOld billbill2 = new BillItemPrintExOld(1);
            int itemsNum = ditem.Columns.Count;
            int itemsNums = billbill2.checkedListBoxitmslk.Items.Count;
            int chitm=billbill2.checkedListBoxitmslk.CheckedItems.Count;
         
            //string cK = billbill.checkedListBoxitmslk.Items[0].ToString();
            //string[] v = cK.Split('=');
            int inxItmSelk = 0;
            int jj=0;
            for (int ii = 0; ii < itemsNums; ii++)
            {
                string cKk = billbill2.checkedListBoxitmslk.Items[ii].ToString();
                string[] vh = cKk.Split('=');
                // if(ditem.Columns[ii].Equals==true)
                if (vh[0].Trim() == ditem.Columns[ii].ToString())
                {
                    if (ditem.Rows[0][ii].ToString().Trim() == "1" || ditem.Rows[0][ii].ToString().Trim() == "True")
                    {
                        billbill2.checkedListBoxitmslk.SetItemChecked(ii, true);
                        //inxItmSelk = ii;
                        
                        jj = jj + 1;
                    }
                    else
                    { billbill2.checkedListBoxitmslk.SetItemChecked(ii, false); }

                }
            }
            int[] selkItmsInx = new int[jj];
            jj = 0;
            /////================================================
            for (int ii = 0; ii < itemsNums; ii++)
            {
                string cKk = billbill2.checkedListBoxitmslk.Items[ii].ToString();
                string[] vh = cKk.Split('=');
                // if(ditem.Columns[ii].Equals==true)
                if (vh[0].Trim() == ditem.Columns[ii].ToString())
                {
                    if (ditem.Rows[0][ii].ToString().Trim() == "1" || ditem.Rows[0][ii].ToString().Trim() == "True")
                    {
                        billbill2.checkedListBoxitmslk.SetItemChecked(ii, true);
                        //inxItmSelk = ii;
                        selkItmsInx[jj] = ii;
                        jj = jj + 1;
                    }
                    else
                    { billbill2.checkedListBoxitmslk.SetItemChecked(ii, false); }

                }
            }

            int cIk = billbill2.checkedListBoxitmslk.CheckedItems.Count;
            //string cK = billbill.checkedListBoxitmslk.CheckedItems[0].ToString();
            string cK = billbill2.checkedListBoxitmslk.CheckedItems[0].ToString();
            string[] v = cK.Split('=');
            string mm = v[0].Trim();
            //int iiItem = inxItmSelk + 4;
            //=======================================
            //////////////////////////////////////////////////////////////////
            var doc1 = new Document();
            Boolean xrotate = false;
            if (radioButton2.Checked == true)
            {
                doc1.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                xrotate = true;
            }
            else
            {
                if (xrotate == true)
                {
                    doc1.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                }
            }

            //string path = @"C:\Program Files\SBS";


            //khorsand
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
               "\\SBS";
            ////////////


            PdfWriter.GetInstance(doc1, new FileStream(path + "/PrePrintReport.pdf", FileMode.Create));
            doc1.Open();

            // iTextSharp.text.BaseColor.BLUE();
            ///===========================================Print Header================================
            ////PdfPTable table = new PdfPTable(5);
            iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(path + "/arm.gif");
            doc1.Add(gif);
            // doc1.Add(new Paragraph(" "));
            iTextSharp.text.Font link = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.UNDERLINE, iTextSharp.text.BaseColor.BLUE);
            Anchor anchor = new Anchor("www.nri.ac.ir", link);
            anchor.Reference = "http://www.nri.ac.ir";
            doc1.Add(anchor);
            //doc1.Add(new Paragraph(" "));
            doc1.Add(new Paragraph("Power Plant Report"));
            if (xrotate == true)
            {
                doc1.Add(new Paragraph("__________________________________________________________________________________________________________________"));
            }
            else
            {
                doc1.Add(new Paragraph("_____________________________________________________________________________"));
            }

            //============================================================================================



            ///////////////////////////////////////////////////////////////////
            iTextSharp.text.Font font8 = iTextSharp.text.FontFactory.GetFont("ARIAL", 6);
            iTextSharp.text.Font fontHeader = iTextSharp.text.FontFactory.GetFont("ARIAL", 10);
            iTextSharp.text.Font font9 = iTextSharp.text.FontFactory.GetFont("ARIAL", 8);
            ///////////////////////////////////////////////////////////////////
            string databaseSlk;
            if (radioButton12.Checked == true)
            { databaseSlk = "MonthlyBillPlant"; }
            else { databaseSlk = "DailyBillPlant"; }
            DataTable dt = Utilities.GetTable("select * from MonthlyBillPlant where ppid='"+CMBPLANT.Text.Trim()+"'");
            DataTable dt2 = Utilities.GetTable("select * from " + databaseSlk + " where Date ='" + singlDate + "'and ppid='" + CMBPLANT.Text.Trim() + "'");
            //select * from MonthlyBillPlant where Date between '"+TABLE1.MIN_RANGE+"' and "'+TABLE1.MAX_RANGE+"';
            int rowcountm = dt2.Rows.Count;
            int rowcontdt = dt.Rows.Count;

            iTextSharp.text.Font font10 = iTextSharp.text.FontFactory.GetFont("ARIAL", 9);


            int colmsCount = EndHour - StrHour + 3;
            int numDays = (rowcountm / 24) - 1;

            //==================================================


            PdfPTable PdfTable = new PdfPTable(colmsCount + 3);
            PdfPCell PdfPCell = null;
            Paragraph paragraph11 = new Paragraph(" ");
            Paragraph paragraph12 = new Paragraph(" ");
            doc1.Add(paragraph11);
            doc1.Add(paragraph12);

            PdfPCell cell = new PdfPCell(new Phrase("Hours"));
            cell.Colspan = 4;
            // cell.Border = 0;
            cell.HorizontalAlignment = 1;
            doc1.Add(cell);
            //Write some content
            string strHeader = "Daily report in date: " + PersianDate.Now.ToString() + "Scientific : " + comboBox2.Text.Trim() + "  Decimel : " + comboBox1.Text.Trim(); ;
            Paragraph paragraph = new Paragraph(new Chunk(strHeader, fontHeader));
            /////===================
            /////    //       
            ////// 
            PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[1][2].ToString(), font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = 3;
            //PdfPCell.BorderWidth = 1;
            PdfTable.AddCell(PdfPCell);

            PdfPCell = new PdfPCell(new Phrase(new Chunk("Hours", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            //PdfPCell.BorderWidth = 1;
            PdfPCell.Colspan = (EndHour - StrHour + 1);
            PdfTable.AddCell(PdfPCell);

            PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            //PdfPCell.BorderWidth = 1;
            PdfPCell.Colspan = 2;
            PdfTable.AddCell(PdfPCell);
            ////////
            //====PPID
            PdfPCell = new PdfPCell(new Phrase(new Chunk("PPID", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            //PdfPCell.BorderWidth = 1;
            PdfPCell.BorderWidthBottom = 1;
            PdfPCell.BorderWidthLeft = 1;
            PdfPCell.BorderWidthTop = 1;
            PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            PdfTable.AddCell(PdfPCell);
            //====Day
            PdfPCell = new PdfPCell(new Phrase(new Chunk(" Items ", font9)));
            PdfPCell.Colspan = 2;
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.BorderWidthBottom = 1;
            PdfPCell.BorderWidthTop = 1;
            PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;

            //PdfPCell.BorderWidth = 1;
            //PdfPCell.BorderWidthLeft = 8;
            //  PdfPCell.BorderWidthRight = 8;
            PdfTable.AddCell(PdfPCell);

            for (int colms = 3; colms < (colmsCount+1); colms++)
            {
                PdfPCell = new PdfPCell(new Phrase(new Chunk((colms - 3 + StrHour).ToString(), font9)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                //PdfPCell.BorderWidth = 1;
                PdfPCell.BorderWidthBottom = 1;
                PdfPCell.BorderWidthTop = 1;
                PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
                PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                PdfPCell.VerticalAlignment = 1;
                PdfTable.AddCell(PdfPCell);
            }
            PdfPCell = new PdfPCell(new Phrase(new Chunk("Sum", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //PdfPCell.BorderWidth = 1;
            PdfPCell.Colspan = 2;
            PdfPCell.BorderWidthBottom = 1;
            PdfPCell.BorderWidthTop = 1;
            PdfPCell.BorderWidthRight = 1;
            PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            PdfPCell.VerticalAlignment = 1;
            PdfTable.AddCell(PdfPCell);
            //___________________________________________________________________________________________
            //___________________________________________________________________________________________

            //}
            int rowC;
            int sumAll = 0;
            for (int rowItm = 0; rowItm < cIk; rowItm++)
            {
                PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[1][0].ToString(), font8)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                PdfPCell.VerticalAlignment = 1;

                rowC = (rowItm + 1) % 2;
                if (rowC == 0)
                {
                    PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                }
                PdfTable.AddCell(PdfPCell);
                //=====selkItmsInx[jj]
                string cKn = billbill2.checkedListBoxitmslk.CheckedItems[rowItm].ToString();
                string[] vn = cKn.Split('=');
               // string mmb = vn[0].Trim();
                int itmINXn = 0;
                string[] numSTINXn = vn[0].Trim().Split('s');
                itmINXn = Convert.ToInt16(numSTINXn[1].Trim());
                PdfPCell = new PdfPCell(new Phrase(new Chunk(KeyStrItms[selkItmsInx[rowItm]], font8)));
                PdfPCell.Colspan = 2;
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                PdfPCell.VerticalAlignment = 1;
                //PdfPCell.Rotation = 45;

                //Math.DivRem(rowDay, 2, remC);
                if (rowC == 0)
                {
                    PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                }
                PdfTable.AddCell(PdfPCell);
                double sumHours = 0;
                for (int rows = 0; rows < (EndHour - StrHour + 1); rows++)
                {
                    //for (int column = 0; column < 10; column++)
                    //  {
                    //PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[rows][8].ToString(), font8)));
                    //PdfTable.AddCell(PdfPCell);
                    //  }
                    // for (int column = 0; column < 10; column++)
                    //   {

                    //=====

                    string val = "";
                    
                        val = dt2.Rows[(StrHour + rows - 1)][selkItmsInx[rowItm] + 4].ToString();
                        try
                        {
                            val = decimasci(MyDoubleParse(val));
                        }
                        catch
                        {

                        }
                   


                  // khorsand  PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[(StrHour + rows - 1)][selkItmsInx[rowItm] + 3].ToString(), font8)));
                    PdfPCell = new PdfPCell(new Phrase(new Chunk(val, font8)));
                    PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    PdfPCell.VerticalAlignment = 1;
                    sumHours = sumHours + Convert.ToDouble(dt2.Rows[(StrHour + rows - 1)][selkItmsInx[rowItm] + 4]);
                    // Math.DivRem(rowDay, 2, remC);
                    if (rowC == 0)
                    {
                        PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                    }
                    PdfTable.AddCell(PdfPCell);
                    //  }
                }



                string val1 = "";
                val1 = sumHours.ToString();
                try
                {
                    val1 = decimasci(MyDoubleParse(val1));
                }
                catch
                {

                }


                PdfPCell = new PdfPCell(new Phrase(new Chunk(val1, font8)));
                // khorsand PdfPCell = new PdfPCell(new Phrase(new Chunk(sumHours.ToString(), font8)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                //PdfPCell.BorderWidth = 1;
                PdfPCell.Colspan = 2;
                PdfPCell.VerticalAlignment = 1;
                PdfTable.AddCell(PdfPCell);

            }

            PdfTable.SpacingBefore = 15f; // Give some space after the text or it may overlap the table
            PdfTable.SpacingAfter = 15f;

            doc1.Add(paragraph);// add paragraph to the document
            doc1.Add(PdfTable); // add pdf table to the document
            //==============================================================
            if (addChartToDoc == 1)
            {
                //string pathImg = @"C:\Program Files\SBS";

                //khorsand
                string pathImg = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                   "\\SBS";
                ////////////


                iTextSharp.text.Image gif2 = iTextSharp.text.Image.GetInstance(pathImg + "/DundasImg.jpeg");
                gif2.ScalePercent(65f);
                gif2.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                doc1.Add(gif2);
            }
            addChartToDoc = 0;
            doc1.Close();
        }
        public void datBsdDailyNRI()
        {
            //=====================Time range========

            StrHour =1;
            EndHour = 1;
            //=======================================
            ///////////////////////////////////////////////////////////////////

            //=====================Date range========
            //string singlDate = faDatePicker2.Text;
            string singlDate = PersianDate.Now.ToString();
            if (faDatePicker2.Text != "[Empty Value]")
            {
                singlDate = faDatePicker2.Text;
            }
            else { MessageBox.Show("Please Select Date"); }

            //=======================================
            //=============Item Selection============
            DataTable ditem = Utilities.GetTable("select * from ItemSelection");
            BillItemPrintExOld billbill2 = new BillItemPrintExOld(1);
            int itemsNum = ditem.Columns.Count;
            int itemsNums = billbill2.checkedListBoxitmslk.Items.Count;
            int chitm = billbill2.checkedListBoxitmslk.CheckedItems.Count;

            //string cK = billbill.checkedListBoxitmslk.Items[0].ToString();
            //string[] v = cK.Split('=');
            int inxItmSelk = 0;
            int jj = 0;
            for (int ii = 0; ii < itemsNums; ii++)
            {
                string cKk = billbill2.checkedListBoxitmslk.Items[ii].ToString();
                string[] vh = cKk.Split('=');
                // if(ditem.Columns[ii].Equals==true)
                if (vh[0].Trim() == ditem.Columns[ii].ToString())
                {
                    if (ditem.Rows[0][ii].ToString().Trim() == "1" || ditem.Rows[0][ii].ToString().Trim() == "True")
                    {
                        billbill2.checkedListBoxitmslk.SetItemChecked(ii, true);
                        //inxItmSelk = ii;

                        jj = jj + 1;
                    }
                    else
                    { billbill2.checkedListBoxitmslk.SetItemChecked(ii, false); }

                }
            }
            int[] selkItmsInx = new int[jj];
            jj = 0;
            /////================================================
            for (int ii = 0; ii < itemsNums; ii++)
            {
                string cKk = billbill2.checkedListBoxitmslk.Items[ii].ToString();
                string[] vh = cKk.Split('=');
                // if(ditem.Columns[ii].Equals==true)
                if (vh[0].Trim() == ditem.Columns[ii].ToString())
                {
                    if (ditem.Rows[0][ii].ToString().Trim() == "1" || ditem.Rows[0][ii].ToString().Trim() == "True")
                    {
                        billbill2.checkedListBoxitmslk.SetItemChecked(ii, true);
                        //inxItmSelk = ii;
                        selkItmsInx[jj] = ii + 1;
                        jj = jj + 1;
                    }
                    else
                    { billbill2.checkedListBoxitmslk.SetItemChecked(ii, false); }

                }
            }

            int cIk = billbill2.checkedListBoxitmslk.CheckedItems.Count;
            //string cK = billbill.checkedListBoxitmslk.CheckedItems[0].ToString();
            string cK = billbill2.checkedListBoxitmslk.CheckedItems[0].ToString();
            string[] v = cK.Split('=');
            string mm = v[0].Trim();
            //int iiItem = inxItmSelk + 4;
            //=======================================
            //////////////////////////////////////////////////////////////////
            var doc1 = new Document();
            Boolean xrotate = false;
            if (radioButton2.Checked == true)
            {
                doc1.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                xrotate = true;
            }
            else
            {
                if (xrotate == true)
                {
                    doc1.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                }
            }

            //string path = @"C:\Program Files\SBS";


            //khorsand
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
               "\\SBS";
            ////////////

            PdfWriter.GetInstance(doc1, new FileStream(path + "/PrePrintReport.pdf", FileMode.Create));
            doc1.Open();

            // iTextSharp.text.BaseColor.BLUE();
            ///===========================================Print Header================================
            ////PdfPTable table = new PdfPTable(5);
            iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(path + "/arm.gif");
            doc1.Add(gif);
            // doc1.Add(new Paragraph(" "));
            iTextSharp.text.Font link = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.UNDERLINE, iTextSharp.text.BaseColor.BLUE);
            Anchor anchor = new Anchor("www.nri.ac.ir", link);
            anchor.Reference = "http://www.nri.ac.ir";
            doc1.Add(anchor);
            //doc1.Add(new Paragraph(" "));
            doc1.Add(new Paragraph("Power Plant Report"));
            if (xrotate == true)
            {
                doc1.Add(new Paragraph("__________________________________________________________________________________________________________________"));
            }
            else
            {
                doc1.Add(new Paragraph("_____________________________________________________________________________"));
            }

            //============================================================================================



            ///////////////////////////////////////////////////////////////////
            iTextSharp.text.Font font8 = iTextSharp.text.FontFactory.GetFont("ARIAL", 6);
            iTextSharp.text.Font fontHeader = iTextSharp.text.FontFactory.GetFont("ARIAL", 10);
            iTextSharp.text.Font font9 = iTextSharp.text.FontFactory.GetFont("ARIAL", 8);
            ///////////////////////////////////////////////////////////////////
            string databaseSlk;
            if (radioButton12.Checked == true)
            { databaseSlk = "MonthlyBillPlant"; }
            else { databaseSlk = "DailyBillPlant"; }
            DataTable dt = Utilities.GetTable("select * from MonthlyBillPlant where ppid='" + CMBPLANT.Text.Trim() + "'");
            DataTable dt2 = Utilities.GetTable("select * from " + databaseSlk + " where Date ='" + singlDate + "'and ppid='" + CMBPLANT.Text.Trim() + "'");
            //select * from MonthlyBillPlant where Date between '"+TABLE1.MIN_RANGE+"' and "'+TABLE1.MAX_RANGE+"';
            int rowcountm = dt2.Rows.Count;
            int rowcontdt = dt.Rows.Count;

            iTextSharp.text.Font font10 = iTextSharp.text.FontFactory.GetFont("ARIAL", 9);


            int colmsCount = EndHour - StrHour + 3;
            int numDays = (rowcountm / 24) - 1;

            //==================================================


            PdfPTable PdfTable = new PdfPTable(colmsCount + 3);
            PdfPCell PdfPCell = null;
            Paragraph paragraph11 = new Paragraph(" ");
            Paragraph paragraph12 = new Paragraph(" ");
            doc1.Add(paragraph11);
            doc1.Add(paragraph12);

            PdfPCell cell = new PdfPCell(new Phrase("Hours"));
            cell.Colspan = 4;
            // cell.Border = 0;
            cell.HorizontalAlignment = 1;
            doc1.Add(cell);
            //Write some content
            string strHeader = "Daily report in date: " + PersianDate.Now.ToString();
            Paragraph paragraph = new Paragraph(new Chunk(strHeader, fontHeader));
            /////===================
            /////    //       
            ////// 
            PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[1][2].ToString(), font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = 3;
            //PdfPCell.BorderWidth = 1;
            PdfTable.AddCell(PdfPCell);

            PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            //PdfPCell.BorderWidth = 1;
            PdfPCell.Colspan = (EndHour - StrHour + 1);
            PdfTable.AddCell(PdfPCell);

            PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            //PdfPCell.BorderWidth = 1;
            PdfPCell.Colspan = 2;
            PdfTable.AddCell(PdfPCell);
            ////////
            //====PPID
            PdfPCell = new PdfPCell(new Phrase(new Chunk("PPID", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            //PdfPCell.BorderWidth = 1;
            PdfPCell.BorderWidthBottom = 1;
            PdfPCell.BorderWidthLeft = 1;
            PdfPCell.BorderWidthTop = 1;
            PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            PdfTable.AddCell(PdfPCell);
            //====Day
            PdfPCell = new PdfPCell(new Phrase(new Chunk(" Items ", font9)));
            PdfPCell.Colspan = 2;
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.BorderWidthBottom = 1;
            PdfPCell.BorderWidthTop = 1;
            PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;

            //PdfPCell.BorderWidth = 1;
            //PdfPCell.BorderWidthLeft = 8;
            //  PdfPCell.BorderWidthRight = 8;
            PdfTable.AddCell(PdfPCell);

            //for (int colms = 3; colms < (colmsCount + 1); colms++)
           // {
                PdfPCell = new PdfPCell(new Phrase(new Chunk("Value", font9)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                //PdfPCell.BorderWidth = 1;
                PdfPCell.BorderWidthBottom = 1;
                PdfPCell.BorderWidthTop = 1;
                PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
                PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                PdfPCell.VerticalAlignment = 1;
                PdfTable.AddCell(PdfPCell);
           // }
            PdfPCell = new PdfPCell(new Phrase(new Chunk("Sum", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //PdfPCell.BorderWidth = 1;
            PdfPCell.Colspan = 2;
            PdfPCell.BorderWidthBottom = 1;
            PdfPCell.BorderWidthTop = 1;
            PdfPCell.BorderWidthRight = 1;
            PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            PdfPCell.VerticalAlignment = 1;
            PdfTable.AddCell(PdfPCell);
            //___________________________________________________________________________________________
            //___________________________________________________________________________________________

            //}
            int rowC;
            int sumAll = 0;
            for (int rowItm = 0; rowItm < cIk; rowItm++)
            {
                PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[1][0].ToString(), font8)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                PdfPCell.VerticalAlignment = 1;

                rowC = (rowItm + 1) % 2;
                if (rowC == 0)
                {
                    PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                }
                PdfTable.AddCell(PdfPCell);
                //=====selkItmsInx[jj]
                string cKn = billbill2.checkedListBoxitmslk.CheckedItems[rowItm].ToString();
                string[] vn = cKn.Split('=');
                // string mmb = vn[0].Trim();
                int itmINXn = 0;
               
                string[] numSTINXn = vn[0].Trim().Split('s');
                itmINXn = Convert.ToInt16(numSTINXn[1].Trim());
               // PdfPCell = new PdfPCell(new Phrase(new Chunk(KeyStrItms[itmINXn - 1], font8)));
                PdfPCell = new PdfPCell(new Phrase(new Chunk(KeyStrItms[selkItmsInx[rowItm]], font8)));
                PdfPCell.Colspan = 2;
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                PdfPCell.VerticalAlignment = 1;
                //PdfPCell.Rotation = 45;

                //Math.DivRem(rowDay, 2, remC);
                if (rowC == 0)
                {
                    PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                }
                PdfTable.AddCell(PdfPCell);
                double sumHours = 0;
                for (int rows = 0; rows < (EndHour - StrHour + 1); rows++)
                {
                    //for (int column = 0; column < 10; column++)
                    //  {
                    //PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[rows][8].ToString(), font8)));
                    //PdfTable.AddCell(PdfPCell);
                    //  }
                    // for (int column = 0; column < 10; column++)
                    //   {

                    //=====
                    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[(StrHour + rows - 1)][selkItmsInx[rowItm] + 3].ToString(), font8)));
                    PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    PdfPCell.VerticalAlignment = 1;
                    sumHours = sumHours + Convert.ToDouble(dt2.Rows[(StrHour + rows - 1)][selkItmsInx[rowItm] + 3]);
                    // Math.DivRem(rowDay, 2, remC);
                    if (rowC == 0)
                    {
                        PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                    }
                    PdfTable.AddCell(PdfPCell);
                    //  }
                }
                PdfPCell = new PdfPCell(new Phrase(new Chunk(sumHours.ToString(), font8)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                //PdfPCell.BorderWidth = 1;
                PdfPCell.Colspan = 2;
                PdfPCell.VerticalAlignment = 1;
                PdfTable.AddCell(PdfPCell);

            }

            PdfTable.SpacingBefore = 15f; // Give some space after the text or it may overlap the table
            PdfTable.SpacingAfter = 15f;

            doc1.Add(paragraph);// add paragraph to the document
            doc1.Add(PdfTable); // add pdf table to the document
            //==============================================================
            if (addChartToDoc == 1)
            {
                //string pathImg = @"C:\Program Files\SBS";


                //khorsand
                string pathImg = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                   "\\SBS";
                ////////////



                iTextSharp.text.Image gif2 = iTextSharp.text.Image.GetInstance(pathImg + "/DundasImg.jpeg");
                gif2.ScalePercent(65f);
                gif2.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                doc1.Add(gif2);
            }
            addChartToDoc = 0;
            doc1.Close();
        }
        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {


        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void prntdatedaily_Click(object sender, EventArgs e)
        {
            try
            {
                prepareNameArray(faDatePicker2.Text);
                if (keyActivNRI == 0)
                {
                    datBsdDaily();
                }
                else
                {
                    datBsdDailyNRI();
                }

                // string path = @"C:\Program Files\SBS";
                //khorsand
                string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                   "\\SBS";
                ////////////


                string pdfPathdoc = path + "/PrePrintReport.pdf";
                if (System.IO.File.Exists(pdfPathdoc))
                {
                    PrintPreviewForm prPform = new PrintPreviewForm(pdfPathdoc);
                    prPform.Show();
                    // System.Diagnostics.Process.Start(pdfPathdoc);

                    //Pdfviewer.Navigate(pdfPathdoc);
                }
                //string destinationFile = @"C:\Program Files\SBS\DundasImg.jpeg";



                //khorsand
                string destinationFile = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                   "\\SBS\\DundasImg.jpeg";
                ////////////
                try
                {
                    File.Delete(destinationFile);
                }
                catch (IOException iox)
                {
                    Console.WriteLine(iox.Message);
                }

            }
            catch
            {
                MessageBox.Show("Incomplete Data !!");
            }



        }


        ////////+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        /// <summary>
        /// This function creates a item-based print from powerplant database.
        /// In this function we select a spesific month range and then after item selection 
        /// we would have one print of these informations
        /// for creating pdf from data we choose iTextSharp library that provides helpful functions
        /// for our purposes.
        /// </summary>
        /////////++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        public void exportitmBsdMonthlySelective(string path)
        {
            

            DataTable ditem = Utilities.GetTable("select * from ItemSelection");
            BillItemPrintExOld billbill = new BillItemPrintExOld(0);
            int itemsNum = ditem.Columns.Count;
            int itemsNums = billbill.checkedListBoxitmslk.Items.Count;
         
            int inxItmSelk = 0;
            for (int ii = 0; ii < itemsNums; ii++)
            {
                string cKk = billbill.checkedListBoxitmslk.Items[ii].ToString();
                string[] vh = cKk.Split('=');
              
                if (vh[0].Trim() == ditem.Columns[ii].ToString())
                {
                    if (ditem.Rows[0][ii].ToString().Trim() == "1" || ditem.Rows[0][ii].ToString().Trim() == "True")
                    {
                        billbill.checkedListBoxitmslk.SetItemChecked(ii, true);
                        inxItmSelk = ii;
                    }
                    else
                    { billbill.checkedListBoxitmslk.SetItemChecked(ii, false); }

                }
            }

            int cIk = billbill.checkedListBoxitmslk.CheckedItems.Count;
           

            int numListboxItmsMonths = listBox1.Items.Count;
            int numCheckedboxItms = billbill.checkedListBoxitmslk.CheckedItems.Count;


            string[,] val = new string[numListboxItmsMonths, numCheckedboxItms];
            for (int ijj = 0; ijj < numListboxItmsMonths; ijj++)
            {
                for (int i = 0; i < cIk; i++)
                {

                    string selectiveMonth = listBox1.Items[ijj].ToString();
                    string cK = billbill.checkedListBoxitmslk.CheckedItems[i].ToString();
                    string[] v = cK.Split('=');
                    string mm = v[0].Trim();
                    string [] numSTINXn = mm.Split('s');
                    string nb=mm;
                   if(int.Parse( numSTINXn[1])<10)
                    {
                        nb = "S0" + numSTINXn[1];
                    }
                   DataTable dt2 = Utilities.GetTable("select value from MonthlyBillTotal where Month ='" + selectiveMonth + "'and code='" + nb + "'and ppid='" + CMBPLANT.Text.Trim() + "'");
                   if (dt2.Rows.Count > 0)
                   {
                       val[ijj, i] = dt2.Rows[0][0].ToString();
                   }
                    int rowcountm = dt2.Rows.Count;
                    if (rowcountm == 0)
                    {
                       // MessageBox.Show("Selected month " + selectiveMonth + "Not exists");
                        //break;
                    }

                }
            }

            ////////////////////////////export//////////////////////////


            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            Excel.Application oExcel = new Excel.Application();

            oExcel.SheetsInNewWorkbook = 1;
            Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
            Excel.Worksheet sheet = null;
            sheet = ((Excel.Worksheet)WB.Worksheets["Sheet1"]);
            Excel.Range range = null;

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers
            oExcel.DefaultSheetDirection = (int)Excel.Constants.xlLeftToRight;

            sheet.DisplayRightToLeft = false;

            int sheetIndex = 1;

            sheet.Name = "Sheet" + (sheetIndex);

            /////////////////////////////////////////////
            Excel.Range range8 = sheet.get_Range(sheet.Cells[1, 1], sheet.Cells[1, 2]);
            range8.Merge(true);
            range8.Value2 = " شماره نيروگاه : " + CMBPLANT.Text.Trim();
            range8.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range8.Columns.Font.Size = 10;
            range8.Columns.AutoFit();
            range8.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(255, 255, 153));
            ////
   




            range = (Excel.Range)sheet.Cells[2, 1];
            range.Value2 = " تاريخ ";
            range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range.Columns.AutoFit();
            range.ColumnWidth = 25;
            range.Borders.Weight = 3;
            range.Borders.LineStyle = Excel.Constants.xlSolid;
            range.Cells.RowHeight = 18;
            range.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));

            for (int y = 0; y < cIk; y++)
            {
                string cK = billbill.checkedListBoxitmslk.CheckedItems[y].ToString();
                string[] v = cK.Split('=');

                string nma = v[1].Trim();
                range = (Excel.Range)sheet.Cells[2, 2 + y];
                range.Value2 = nma;
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Columns.AutoFit();
                range.ColumnWidth = 25;
                range.Borders.Weight = 3;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Cells.RowHeight = 30;
                range.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(255, 255, 0));
                range.WrapText = true;
            }

            for (int ijj = 0; ijj < numListboxItmsMonths; ijj++)
            {
                range = (Excel.Range)sheet.Cells[ijj+3, 1];
                range.Value2 = listBox1.Items[ijj].ToString();
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Columns.AutoFit();
                range.ColumnWidth = 25;
                range.Borders.Weight = 3;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Cells.RowHeight = 30;               
       
                for (int i = 0; i < cIk; i++)
                {
                    string cK = billbill.checkedListBoxitmslk.CheckedItems[i].ToString();
                    string[] v = cK.Split('=');

                  
                    range = (Excel.Range)sheet.Cells[ijj+3, 2 + i];
                    range.Value2 = val[ijj,i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.Columns.AutoFit();
                    range.ColumnWidth = 25;
                    range.Borders.Weight = 3;
                    range.Borders.LineStyle = Excel.Constants.xlSolid;
                    range.Cells.RowHeight = 30;
                
                }

            }
           


            sheet.Activate();



            string filePath = path + "\\" + "IgmcMonthlyReport" + ".xls";

            WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            WB.Close(Missing.Value, Missing.Value, Missing.Value);
            oExcel.Workbooks.Close();
            oExcel.Quit();

            ///////////////////////////////
            MessageBox.Show("Successfully Export In" + filePath , "Export");
          
        }
        public void itmBsdMonthlyRange()
        {

           //  string startMonth = faDatePicker5.Text
        
            //string dtadd = faDatePicker3.Text;
            //string[] HU1 = startMonth.Split('/');
            //string HG1 = HU1[0].Trim();
            //for (DateTime date = startMonth; date.Date <= endMonth; date = date.AddDays(1))
            //{

            //}
            //string startMonth = HG1 + "/" + HU1[1].Trim();
           // listBox1.Items.Add(HG2);
            //=======================================
            //=============Item Selection============
            DataTable ditem = Utilities.GetTable("select * from ItemSelection");
            BillItemPrintExOld billbill = new BillItemPrintExOld(0);
            int itemsNum = ditem.Columns.Count;
            int itemsNums = billbill.checkedListBoxitmslk.Items.Count;
            //string cK = billbill.checkedListBoxitmslk.Items[0].ToString();
            //string[] v = cK.Split('=');
            int inxItmSelk = 0;
            for (int ii = 0; ii < itemsNums; ii++)
            {
                string cKk = billbill.checkedListBoxitmslk.Items[ii].ToString();
                string[] vh = cKk.Split('=');
                // if(ditem.Columns[ii].Equals==true)
                if (vh[0].Trim() == ditem.Columns[ii].ToString())
                {
                    if (ditem.Rows[0][ii].ToString().Trim() == "1" || ditem.Rows[0][ii].ToString().Trim() == "True")
                    {
                        billbill.checkedListBoxitmslk.SetItemChecked(ii, true);
                        inxItmSelk = ii;
                    }
                    else
                    { billbill.checkedListBoxitmslk.SetItemChecked(ii, false); }

                }
            }

            int cIk = billbill.checkedListBoxitmslk.CheckedItems.Count;
            //string cK = billbill.checkedListBoxitmslk.CheckedItems[0].ToString();
            string cK = billbill.checkedListBoxitmslk.CheckedItems[0].ToString();
            string[] v = cK.Split('=');
            string mm = v[0].Trim();
            int iiItem = inxItmSelk + 4;
            //=======================================
            //////////////////////////////////////////////////////////////////
            var doc1 = new Document();
            Boolean xrotate = false;
            if (radioButton2.Checked == true)
            {
                doc1.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                xrotate = true;
            }
            else
            {
                if (xrotate == true)
                {
                    doc1.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                }
            }
            //string path = @"C:\Program Files\SBS";
            //khorsand
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
               "\\SBS";
            ////////////




            PdfWriter.GetInstance(doc1, new FileStream(path + "/PrePrintReport.pdf", FileMode.Create));
            doc1.Open();

            // iTextSharp.text.BaseColor.BLUE();
            ///===========================================Print Header================================
            ////PdfPTable table = new PdfPTable(5);
            iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(path + "/arm.gif");
            doc1.Add(gif);
            // doc1.Add(new Paragraph(" "));
            iTextSharp.text.Font link = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.UNDERLINE, iTextSharp.text.BaseColor.BLUE);
            Anchor anchor = new Anchor("www.nri.ac.ir", link);
            anchor.Reference = "http://www.nri.ac.ir";
            doc1.Add(anchor);
            //doc1.Add(new Paragraph(" "));
            doc1.Add(new Paragraph("Power Plant Report"));
            if (xrotate == true)
            {
                doc1.Add(new Paragraph("__________________________________________________________________________________________________________________"));
            }
            else
            {
                doc1.Add(new Paragraph("_____________________________________________________________________________"));
            }
            //PdfPCell cell = new PdfPCell(new Phrase("Header spanning 3 columns"));
            //cell.Colspan = 4;
            //cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //cell.VerticalAlignment = 1;
            //table.AddCell(cell);
            //============================================================================================



            ///////////////////////////////////////////////////////////////////
            iTextSharp.text.Font font8 = iTextSharp.text.FontFactory.GetFont("ARIAL", 6);
            iTextSharp.text.Font fontHeader = iTextSharp.text.FontFactory.GetFont("ARIAL", 10);
            iTextSharp.text.Font font9 = iTextSharp.text.FontFactory.GetFont("ARIAL", 8);
            ///////////////////////////////////////////////////////////////////
            DataTable dt = Utilities.GetTable("select * from MonthlyBillTotal where ppid='" + CMBPLANT.Text.Trim() + "'");

            //Craete instance of the pdf table and set the number of column in that table
            iTextSharp.text.Font font10 = iTextSharp.text.FontFactory.GetFont("ARIAL", 9);


            ////===============================================================
            //How add the data from datatable to pdf table


            //==================================================
            // BillItemPrintEx billbill3 = new BillItemPrintEx(1);
            // itemsNum = ditem.Columns.Count;
            //  itemsNums = billbill3.checkedListBoxitmslk.Items.Count;
            int numCheckedboxItms = billbill.checkedListBoxitmslk.CheckedItems.Count;

            int numListboxItmsMonths = listBox1.Items.Count;
            PdfPTable PdfTable = new PdfPTable(numCheckedboxItms + 2);
            PdfPCell PdfPCell = null;
            Paragraph paragraph11 = new Paragraph(" ");
            Paragraph paragraph12 = new Paragraph(" ");
            doc1.Add(paragraph11);
            doc1.Add(paragraph12);


            string strHeader = "Monthly report in date: " + PersianDate.Now.ToString() + "Scientific : " + comboBox4.Text.Trim() + "  Decimel : " + comboBox3.Text.Trim();
            Paragraph paragraph = new Paragraph(new Chunk(strHeader, fontHeader));
            /////===================
            /////    //       
            ////// 
            PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = 2;
            //PdfPCell.BorderWidth = 1;
            PdfTable.AddCell(PdfPCell);

            PdfPCell = new PdfPCell(new Phrase(new Chunk("Items", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            //PdfPCell.BorderWidth = 1;
            PdfPCell.Colspan = numCheckedboxItms;
            PdfTable.AddCell(PdfPCell);

            //====Day
            PdfPCell = new PdfPCell(new Phrase(new Chunk(" Month ", font9)));
            PdfPCell.Colspan = 2;
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.BorderWidthBottom = 1;
            PdfPCell.BorderWidthTop = 1;
            PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            PdfTable.AddCell(PdfPCell);


            int itmINXn;
            string cKn;
            string[] vn;
            string[] numSTINXn;
            for (int colms = 0; colms < (numCheckedboxItms - 1); colms++)
            {

                ////////////////
                cKn = billbill.checkedListBoxitmslk.CheckedItems[colms].ToString();
                vn = cKn.Split('=');
                // string mmb = vn[0].Trim();
                itmINXn = 0;
                numSTINXn = vn[0].Trim().Split('s');
                itmINXn = Convert.ToInt16(numSTINXn[1].Trim());
                PdfPCell = new PdfPCell(new Phrase(new Chunk(KeyStrItms[itmINXn], font8)));
                //////////////////
                // PdfPCell = new PdfPCell(new Phrase(, font9)));
                PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                //PdfPCell.BorderWidth = 1;
                PdfPCell.BorderWidthBottom = 1;
                PdfPCell.BorderWidthTop = 1;
                PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
                PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                PdfPCell.VerticalAlignment = 1;
                PdfTable.AddCell(PdfPCell);
            }
            cKn = billbill.checkedListBoxitmslk.CheckedItems[numCheckedboxItms - 1].ToString();
            vn = cKn.Split('=');
            // string mmb = vn[0].Trim();
            itmINXn = 0;
            numSTINXn = vn[0].Trim().Split('s');
            itmINXn = Convert.ToInt16(numSTINXn[1].Trim());
            PdfPCell = new PdfPCell(new Phrase(new Chunk(KeyStrItms[itmINXn], font8)));

            // PdfPCell = new PdfPCell(new Phrase(new Chunk((colmsCount - 3 + StrHour).ToString(), font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //PdfPCell.BorderWidth = 1;
            PdfPCell.BorderWidthBottom = 1;
            PdfPCell.BorderWidthTop = 1;
            PdfPCell.BorderWidthRight = 1;
            PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            PdfPCell.VerticalAlignment = 1;
            PdfTable.AddCell(PdfPCell);
            //___________________________________________________________________________________________
            //___________________________________________________________________________________________

            //}
            int rowC;
            for (int ijj = 0; ijj < numListboxItmsMonths; ijj++)
            {
                
                string selectiveMonth = listBox1.Items[ijj].ToString();

                DataTable dt2 = Utilities.GetTable("select * from MonthlyBillTotal where Month ='" + selectiveMonth + "'and ppid='" + CMBPLANT.Text.Trim() + "'");
                //select * from MonthlyBillPlant where Date between '"+TABLE1.MIN_RANGE+"' and "'+TABLE1.MAX_RANGE+"';
                int rowcountm = dt2.Rows.Count;
                if (rowcountm == 0)
                {
                    MessageBox.Show("Selected month " + selectiveMonth + "Not exists");
                    PdfPCell = new PdfPCell(new Phrase(new Chunk(selectiveMonth, font8)));
                    PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    PdfPCell.VerticalAlignment = 1;
                    PdfPCell.Colspan = 2;

                    rowC = (ijj + 1) % 2;
                    if (rowC == 0)
                    {
                        PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                    }
                    PdfTable.AddCell(PdfPCell);


                    for (int rows = 0; rows < (numCheckedboxItms); rows++)
                    {

                        //=====
                        PdfPCell = new PdfPCell(new Phrase(new Chunk("not exists", font8)));
                        PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                        PdfPCell.VerticalAlignment = 1;

                        // Math.DivRem(rowDay, 2, remC);
                        if (rowC == 0)
                        {
                            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                        }
                        PdfTable.AddCell(PdfPCell);
                        //  }
                    }
                }
                else
                {
                    //for (int rowMonth = 0; rowMonth < numDays; rowMonth++)
                    // {
                    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[0][1].ToString(), font8)));
                    PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    PdfPCell.VerticalAlignment = 1;
                    PdfPCell.Colspan = 2;

                    rowC = (ijj + 1) % 2;
                    if (rowC == 0)
                    {
                        PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                    }
                    PdfTable.AddCell(PdfPCell);


                    for (int rows = 0; rows < (numCheckedboxItms); rows++)
                    {

                        cKn = billbill.checkedListBoxitmslk.CheckedItems[rows].ToString();
                        vn = cKn.Split('=');
                        // string mmb = vn[0].Trim();
                        itmINXn = 0;
                        numSTINXn = vn[0].Trim().Split('s');
                        itmINXn = Convert.ToInt16(numSTINXn[1].Trim());
                        //=====


                        string val1 = "";
                        val1 = dt2.Rows[itmINXn - 1][3].ToString();
                        try
                        {
                            val1 = decimasciMONTH(MyDoubleParse(val1));
                        }
                        catch
                        {

                        }
                        // KHORSANDPdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[itmINXn - 1][3].ToString(), font8)));
                        PdfPCell = new PdfPCell(new Phrase(new Chunk(val1, font8)));
                        PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                        PdfPCell.VerticalAlignment = 1;

                        // Math.DivRem(rowDay, 2, remC);
                        if (rowC == 0)
                        {
                            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                        }
                        PdfTable.AddCell(PdfPCell);
                        //  }
                    }
                    // }
                }
            }

            PdfTable.SpacingBefore = 15f; // Give some space after the text or it may overlap the table
            PdfTable.SpacingAfter = 15f;

            doc1.Add(paragraph);// add paragraph to the document
            doc1.Add(PdfTable); // add pdf table to the document

            doc1.Close();
        }
        public void itmBsdMonthlySelective()
        {


 
            //=======================================
            //=============Item Selection============
            DataTable ditem = Utilities.GetTable("select * from ItemSelection");
            BillItemPrintExOld billbill = new BillItemPrintExOld(0);
            int itemsNum = ditem.Columns.Count;
            int itemsNums = billbill.checkedListBoxitmslk.Items.Count;
            //string cK = billbill.checkedListBoxitmslk.Items[0].ToString();
            //string[] v = cK.Split('=');
            int inxItmSelk = 0;
            for (int ii = 0; ii < itemsNums; ii++)
            {
                string cKk = billbill.checkedListBoxitmslk.Items[ii].ToString();
                string[] vh = cKk.Split('=');
                // if(ditem.Columns[ii].Equals==true)
                if (vh[0].Trim() == ditem.Columns[ii].ToString())
                {
                    if (ditem.Rows[0][ii].ToString().Trim() == "1" || ditem.Rows[0][ii].ToString().Trim() == "True")
                    {
                        billbill.checkedListBoxitmslk.SetItemChecked(ii, true);
                        inxItmSelk = ii;
                    }
                    else
                    { billbill.checkedListBoxitmslk.SetItemChecked(ii, false); }

                }
            }

            int cIk = billbill.checkedListBoxitmslk.CheckedItems.Count;
            //string cK = billbill.checkedListBoxitmslk.CheckedItems[0].ToString();
            string cK = billbill.checkedListBoxitmslk.CheckedItems[0].ToString();
            string[] v = cK.Split('=');
            string mm = v[0].Trim();
            int iiItem = inxItmSelk + 4;
            //=======================================
            //////////////////////////////////////////////////////////////////
            var doc1 = new Document();
            Boolean xrotate = false;
            if (radioButton2.Checked == true)
            {
                doc1.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                xrotate = true;
            }
            else
            {
                if (xrotate == true)
                {
                    doc1.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                }
            }
            //string path = @"C:\Program Files\SBS";
            //khorsand
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
               "\\SBS";
            ////////////


            PdfWriter.GetInstance(doc1, new FileStream(path + "/PrePrintReport.pdf", FileMode.Create));
            doc1.Open();

            // iTextSharp.text.BaseColor.BLUE();
            ///===========================================Print Header================================
            ////PdfPTable table = new PdfPTable(5);
            iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(path + "/arm.gif");
            doc1.Add(gif);
            // doc1.Add(new Paragraph(" "));
            iTextSharp.text.Font link = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.UNDERLINE, iTextSharp.text.BaseColor.BLUE);
            Anchor anchor = new Anchor("www.nri.ac.ir", link);
            anchor.Reference = "http://www.nri.ac.ir";
            doc1.Add(anchor);
            //doc1.Add(new Paragraph(" "));
            doc1.Add(new Paragraph("Power Plant Report"));
            if (xrotate == true)
            {
                doc1.Add(new Paragraph("__________________________________________________________________________________________________________________"));
            }
            else
            {
                doc1.Add(new Paragraph("_____________________________________________________________________________"));
            }
            //PdfPCell cell = new PdfPCell(new Phrase("Header spanning 3 columns"));
            //cell.Colspan = 4;
            //cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            //cell.VerticalAlignment = 1;
            //table.AddCell(cell);
            //============================================================================================



            ///////////////////////////////////////////////////////////////////
            iTextSharp.text.Font font8 = iTextSharp.text.FontFactory.GetFont("ARIAL", 6);
            iTextSharp.text.Font fontHeader = iTextSharp.text.FontFactory.GetFont("ARIAL", 10);
            iTextSharp.text.Font font9 = iTextSharp.text.FontFactory.GetFont("ARIAL", 8);
            ///////////////////////////////////////////////////////////////////
            DataTable dt = Utilities.GetTable("select * from MonthlyBillTotal where ppid='" + CMBPLANT.Text.Trim() + "'");

            //Craete instance of the pdf table and set the number of column in that table
            iTextSharp.text.Font font10 = iTextSharp.text.FontFactory.GetFont("ARIAL", 9);


            ////===============================================================
            //How add the data from datatable to pdf table


            //==================================================
           // BillItemPrintEx billbill3 = new BillItemPrintEx(1);
           // itemsNum = ditem.Columns.Count;
           //  itemsNums = billbill3.checkedListBoxitmslk.Items.Count;
            int numCheckedboxItms = billbill.checkedListBoxitmslk.CheckedItems.Count;
       
            int numListboxItmsMonths = listBox1.Items.Count;
            PdfPTable PdfTable = new PdfPTable(numCheckedboxItms + 2);
            PdfPCell PdfPCell = null;
            Paragraph paragraph11 = new Paragraph(" ");
            Paragraph paragraph12 = new Paragraph(" ");
            doc1.Add(paragraph11);
            doc1.Add(paragraph12);


            string strHeader = "Monthly report in date: " + PersianDate.Now.ToString() + "Scientific : " + comboBox4.Text.Trim() + "  Decimel : " + comboBox3.Text.Trim(); 
            Paragraph paragraph = new Paragraph(new Chunk(strHeader, fontHeader));
            /////===================
            /////    //       
            ////// 
            PdfPCell = new PdfPCell(new Phrase(new Chunk("--", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.Colspan = 2;
            //PdfPCell.BorderWidth = 1;
            PdfTable.AddCell(PdfPCell);

            PdfPCell = new PdfPCell(new Phrase(new Chunk("Items", font9)));
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            //PdfPCell.BorderWidth = 1;
            PdfPCell.Colspan = numCheckedboxItms;
            PdfTable.AddCell(PdfPCell);

            //====Day
            PdfPCell = new PdfPCell(new Phrase(new Chunk(" Month ", font9)));
            PdfPCell.Colspan = 2;
            PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            PdfPCell.VerticalAlignment = 1;
            PdfPCell.BorderWidthBottom = 1;
            PdfPCell.BorderWidthTop = 1;
            PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
            PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            PdfTable.AddCell(PdfPCell);


            int itmINXn;
            string cKn;
            string[] vn;
            string[] numSTINXn;
                for (int colms = 0; colms < (numCheckedboxItms-1); colms++)
                 {

                    ////////////////
                 cKn = billbill.checkedListBoxitmslk.CheckedItems[colms].ToString();
             vn = cKn.Split('=');
               // string mmb = vn[0].Trim();
                 itmINXn = 0;
               numSTINXn = vn[0].Trim().Split('s');
                itmINXn = Convert.ToInt16(numSTINXn[1].Trim());
                PdfPCell = new PdfPCell(new Phrase(new Chunk(KeyStrItms[itmINXn], font8)));
                    //////////////////
                    // PdfPCell = new PdfPCell(new Phrase(, font9)));
                     PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                     //PdfPCell.BorderWidth = 1;
                     PdfPCell.BorderWidthBottom = 1;
                     PdfPCell.BorderWidthTop = 1;
                     PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
                     PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                     PdfPCell.VerticalAlignment = 1;
                     PdfTable.AddCell(PdfPCell);
                 }
                cKn = billbill.checkedListBoxitmslk.CheckedItems[numCheckedboxItms-1].ToString();
                vn = cKn.Split('=');
                // string mmb = vn[0].Trim();
                 itmINXn = 0;
                numSTINXn = vn[0].Trim().Split('s');
                itmINXn = Convert.ToInt16(numSTINXn[1].Trim());
                PdfPCell = new PdfPCell(new Phrase(new Chunk(KeyStrItms[itmINXn], font8)));

                // PdfPCell = new PdfPCell(new Phrase(new Chunk((colmsCount - 3 + StrHour).ToString(), font9)));
                 PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                 //PdfPCell.BorderWidth = 1;
                 PdfPCell.BorderWidthBottom = 1;
                 PdfPCell.BorderWidthTop = 1;
                 PdfPCell.BorderWidthRight = 1;
                 PdfPCell.BorderColor = iTextSharp.text.BaseColor.DARK_GRAY;
                 PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                 PdfPCell.VerticalAlignment = 1;
                 PdfTable.AddCell(PdfPCell);
                 //___________________________________________________________________________________________
                 //___________________________________________________________________________________________

                 //}
                 int rowC;
             for (int ijj = 0; ijj < numListboxItmsMonths; ijj++)
               {
                 string selectiveMonth = listBox1.Items[ijj].ToString();

                 DataTable dt2 = Utilities.GetTable("select * from MonthlyBillTotal where Month ='" + selectiveMonth + "'and ppid='" + CMBPLANT.Text.Trim() + "'");
                 //select * from MonthlyBillPlant where Date between '"+TABLE1.MIN_RANGE+"' and "'+TABLE1.MAX_RANGE+"';
                int rowcountm = dt2.Rows.Count;
                if (rowcountm == 0)
                {
                    MessageBox.Show("Selected month "+selectiveMonth+"Not exists");
                    return;
                }
                 //for (int rowMonth = 0; rowMonth < numDays; rowMonth++)
                // {
                     PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[0][1].ToString(), font8)));
                     PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                     PdfPCell.VerticalAlignment = 1;
                     PdfPCell.Colspan = 2;

                     rowC = (ijj + 1) % 2;
                     if (rowC == 0)
                     {
                         PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                     }
                     PdfTable.AddCell(PdfPCell);
                     ////=====
                     //PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[0][2].ToString(), font8)));
                     //PdfPCell.Colspan = 2;
                     //PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                     //PdfPCell.VerticalAlignment = 1;
                     //PdfPCell.Rotation = 45;

                     //Math.DivRem(rowDay, 2, remC);
                     //if (rowC == 0)
                     //{
                     //    PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                     //}
                     //PdfTable.AddCell(PdfPCell);

                     for (int rows = 0; rows < (numCheckedboxItms); rows++)
                     {
                         //for (int column = 0; column < 10; column++)
                         //  {
                         //PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[rows][8].ToString(), font8)));
                         //PdfTable.AddCell(PdfPCell);
                         //  }
                         // for (int column = 0; column < 10; column++)
                         //   {
                         cKn = billbill.checkedListBoxitmslk.CheckedItems[rows].ToString();
                         vn = cKn.Split('=');
                         // string mmb = vn[0].Trim();
                         itmINXn = 0;
                         numSTINXn = vn[0].Trim().Split('s');
                       itmINXn = Convert.ToInt16(numSTINXn[1].Trim());
                         //=====


                       string val1 = "";
                       val1 = dt2.Rows[itmINXn - 1][3].ToString();
                       try
                       {
                           val1 = decimasciMONTH(MyDoubleParse(val1));
                       }
                       catch
                       {

                       }




                       // KHORSAND PdfPCell = new PdfPCell(new Phrase(new Chunk(dt2.Rows[itmINXn - 1][3].ToString(), font8)));
                         PdfPCell = new PdfPCell(new Phrase(new Chunk(val1, font8)));
                         PdfPCell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                         PdfPCell.VerticalAlignment = 1;

                         // Math.DivRem(rowDay, 2, remC);
                         if (rowC == 0)
                         {
                             PdfPCell.BackgroundColor = iTextSharp.text.BaseColor.YELLOW;
                         }
                         PdfTable.AddCell(PdfPCell);
                         //  }
                     }
                // }
             }

            PdfTable.SpacingBefore = 15f; // Give some space after the text or it may overlap the table
            PdfTable.SpacingAfter = 15f;

            doc1.Add(paragraph);// add paragraph to the document
            doc1.Add(PdfTable); // add pdf table to the document

            doc1.Close();
        }
        public void PlotChartItmBased(string[] itmName,int[] itemInx)
        {
            //string itmName;
            bool Estimated = true;
            PlotChart1 frm = new PlotChart1(printPlotType, Estimated, keyActivNRI, faDatePickerbill.Text, faDatePicker1.Text, itmName, itemInx, CMBPLANT.Text.Trim());
                //frm.Date = MRCal.Text;
                //frm.PlantId = PPID;
           
                frm.ShowDialog();
        
                frm.ChartRepEx.Titles.RemoveAt(0);
               frm.ChartRepEx.Titles.Add("Daily Plot");
                //frm.ChartRepEx.Titles.RemoveAt(0);
                //frm.ChartRepEx.Titles.Add("Monthly Plot");

                //    //FRChart1.Series.RemoveAt(FRChart1.Series.Count - 1);
                //    //begin
                //frm.ChartRepEx.Series.Clear();
                //    //end
                //frm.ChartRepEx.Series.Add("Power");
                //    //begin
                //frm.ChartRepEx.Legends.Clear();
                //frm.ChartRepEx.Series["Power"].Points.Clear();
                //frm.ChartRepEx.ChartAreas["Default"].AxisY.Margin = true;
                //frm.ChartRepEx.ChartAreas["Default"].AxisY.Minimum = 0;
                //    double max1 = 0.0;
                //    for (int i = 0; i < 8; i++)
                //        //if (double.Parse(EconomicTable.Rows[0][i].ToString()) > max1) max1 = double.Parse(EconomicTable.Rows[0][i].ToString());
                //        frm.ChartRepEx.ChartAreas["Default"].AxisY.Maximum = max1 + 50.0;

                  
        }
        private void prntitmmonthly_Click(object sender, EventArgs e)
        {
            try
            {
                //khorsand
                prepareNameArray(faDatePicker3.Text);
                //
                if (radioButton7.Checked == true)
                {
                    //  addDateToListbox.Enabled = false;
                    keyActiv = 0;
                    itmBsdMonthlyRange();
                }
                else
                {
                    // addDateToListbox.Enabled = true;
                    keyActiv = 1;
                    itmBsdMonthlySelective();

                }

                //string path = @"C:\Program Files\SBS";


                //khorsand//
                string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                   "\\SBS";



                string pdfPathdoc = path + "/PrePrintReport.pdf";
                if (System.IO.File.Exists(pdfPathdoc))
                {
                    PrintPreviewForm prPform = new PrintPreviewForm(pdfPathdoc);
                    prPform.Show();
                    // System.Diagnostics.Process.Start(pdfPathdoc);

                    //Pdfviewer.Navigate(pdfPathdoc);
                }
            }
            catch
            {
                MessageBox.Show("Incomplete Data !!");
            }



        }

        private void selectItmMonthly_Click(object sender, EventArgs e)
        {
            BillItemPrintExOld nBill = new BillItemPrintExOld(1);
            nBill.Show();
        }

        private void addDateToListbox_Click(object sender, EventArgs e)
        {
            if (faDatePicker3.Text != "[Empty Value]")
            {
                string dtadd = faDatePicker3.Text;
                string[] HU1 = dtadd.Split('/');
                string HG1 = HU1[0].Trim();
                string HG2 = HG1 + "/" + HU1[1].Trim();
                listBox1.Items.Add(HG2);
            }
            else
            {
                MessageBox.Show("Please select month");
            }
        }

        private void addDateToListbox_Click_1(object sender, EventArgs e)
        {
            if (keyActiv == 1)
            {
                if (faDatePicker3.Text != "[Empty Value]")
                {
                    string dtadd = faDatePicker3.Text;
                    string[] HU1 = dtadd.Split('/');
                    string HG1 = HU1[0].Trim();
                    string HG2 = HG1 + "/" + HU1[1].Trim();
                    listBox1.Items.Add(HG2);
                }
                else
                {
                    MessageBox.Show("Please select month");
                }
            }
            
        }

        private void selectItmMonthly_Click_1(object sender, EventArgs e)
        {
            BillItemPrintExOld nBill = new BillItemPrintExOld(1);
            nBill.Show();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] HU11;
            string HG11;
            string HG21;
            string dtadd;
            string startMonthS = PersianDate.Now.ToString();
            string endMonthS = PersianDate.Now.ToString();
            if (faDatePicker5.Text != "[Empty Value]")
            {
                startMonthS = faDatePicker5.Text;
                dtadd = startMonthS.ToString();
                HU11 = dtadd.Split('/');
                HG11 = HU11[0].Trim();
                startMonthS = HG11 + "/" + HU11[1].Trim() + "/01";
            }
            else
            {
                MessageBox.Show("Please select Start Month");
            }
            if (faDatePicker4.Text != "[Empty Value]")
            {
                endMonthS = faDatePicker4.Text;
                dtadd = endMonthS.ToString();
                HU11 = dtadd.Split('/');
                HG11 = HU11[0].Trim();
                endMonthS = HG11 + "/" + HU11[1].Trim() + "/20";
            }
            else
            {
                MessageBox.Show("Please select end month");
            }
            DateTime startMonth = Convert.ToDateTime(startMonthS);
            DateTime endMonth = Convert.ToDateTime(endMonthS);
            DateTime cur = startMonth;

            for (int i = 0; cur <= endMonth; cur = startMonth.AddMonths(++i))
            {

                dtadd = cur.ToString();
                HU11 = dtadd.Split('/');
                HG11 = HU11[0].Trim();
                HG21 = HG11 + "/" + HU11[1].Trim();
                // listBox1.Items.Add(HG2);
                listBox1.Items.Add(HG21);
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            try
            {

                prepareNameArray(faDatePicker1.Text);
                addChartToDoc = 1;
                DataTable ditem = Utilities.GetTable("select * from ItemSelection");
                BillItemPrintExOld billbill = new BillItemPrintExOld(0);
                int itemsNum = ditem.Columns.Count;
                int itemsNums = billbill.checkedListBoxitmslk.Items.Count;
                //string cK = billbill.checkedListBoxitmslk.Items[0].ToString();
                //string[] v = cK.Split('=');
                int inxItmSelk = 0;
                for (int ii = 0; ii < itemsNums; ii++)
                {
                    string cKk = billbill.checkedListBoxitmslk.Items[ii].ToString();
                    string[] vh = cKk.Split('=');
                    // if(ditem.Columns[ii].Equals==true)
                    if (vh[0].Trim() == ditem.Columns[ii].ToString())
                    {
                        if (ditem.Rows[0][ii].ToString().Trim() == "1" || ditem.Rows[0][ii].ToString().Trim() == "True")
                        {
                            billbill.checkedListBoxitmslk.SetItemChecked(ii, true);
                            inxItmSelk = ii;
                        }
                        else
                        { billbill.checkedListBoxitmslk.SetItemChecked(ii, false); }

                    }
                }

                int cIk = billbill.checkedListBoxitmslk.CheckedItems.Count;
                //string cK = billbill.checkedListBoxitmslk.CheckedItems[0].ToString();
                string cK = billbill.checkedListBoxitmslk.CheckedItems[0].ToString();
                string[] v = cK.Split('=');
                //  string mm = v[0].Trim();
                int[] itmINX = new int[1];
                string[] numSTINX = v[0].Trim().Split('s');
                itmINX[0] = Convert.ToInt16(numSTINX[1].Trim());
                //Write some content

                PlotChartItmBased(KeyStrItms, itmINX);
            }
            catch
            {
                MessageBox.Show("Incomplete Data !!");

            }



        }

        public void PlotChartDateBased(string[] itmName, int[] itemInx)
        {
            //string itmName;
            bool Estimated = true;
            PlotChart1 frm = new PlotChart1(1, Estimated, keyActivNRI, faDatePicker2.Text, "Null", itmName, itemInx, CMBPLANT.Text.Trim());
            //frm.Date = MRCal.Text;
            //frm.PlantId = PPID;
            frm.ppid = CMBPLANT.Text.Trim();
            frm.ShowDialog();

            frm.ChartRepEx.Titles.RemoveAt(0);
            frm.ChartRepEx.Titles.Add("Daily Plot");


        }
        private void button3_Click(object sender, EventArgs e)
        {

            try
            {
                prepareNameArray(faDatePicker2.Text);
                addChartToDoc = 1;
                DataTable ditem = Utilities.GetTable("select * from ItemSelection");
                BillItemPrintExOld billbill = new BillItemPrintExOld(1);
                int itemsNum = ditem.Columns.Count;
                int itemsNums = billbill.checkedListBoxitmslk.Items.Count;
                //string cK = billbill.checkedListBoxitmslk.Items[0].ToString();
                //string[] v = cK.Split('=');
                int inxItmSelk = 0;
                for (int ii = 0; ii < itemsNums; ii++)
                {
                    string cKk = billbill.checkedListBoxitmslk.Items[ii].ToString();
                    string[] vh = cKk.Split('=');
                    // if(ditem.Columns[ii].Equals==true)
                    if (vh[0].Trim() == ditem.Columns[ii].ToString())
                    {
                        if (ditem.Rows[0][ii].ToString().Trim() == "1" || ditem.Rows[0][ii].ToString().Trim() == "True")
                        {
                            billbill.checkedListBoxitmslk.SetItemChecked(ii, true);
                            inxItmSelk = ii;
                        }
                        else
                        { billbill.checkedListBoxitmslk.SetItemChecked(ii, false); }

                    }
                }

                int cIk = billbill.checkedListBoxitmslk.CheckedItems.Count;
                //string cK = billbill.checkedListBoxitmslk.CheckedItems[0].ToString();
                string cK = billbill.checkedListBoxitmslk.CheckedItems[0].ToString();
                string[] v = cK.Split('=');
                //  string mm = v[0].Trim();
                // int itmINX = 0;
                int[] itmINX = new int[cIk];
                string[] numSTINX = v[0].Trim().Split('s');
                itmINX[0] = Convert.ToInt16(numSTINX[1].Trim());
                //Write some content
                for (int jjk = 1; jjk < cIk; jjk++)
                {
                    cK = billbill.checkedListBoxitmslk.CheckedItems[jjk].ToString();
                    v = cK.Split('=');
                    //  string mm = v[0].Trim();
                    // int itmINX = 0;

                    numSTINX = v[0].Trim().Split('s');
                    itmINX[jjk] = Convert.ToInt16(numSTINX[1].Trim());
                }
                PlotChartDateBased(KeyStrItms, itmINX);
            }
            catch
            {
                MessageBox.Show("Incomplete Data !!");
            }

        }

        private void PrintFinancialReportEx_Load(object sender, EventArgs e)
        {
            this.Text = "BillReport";
            comboBox1.Text = "0";
            comboBox3.Text = "0";
            comboBox4.Text = "1";
            comboBox2.Text = "1";
            faDatePicker1.Text = new PersianDate(DateTime.Now).ToString("d");
            faDatePicker2.Text = new PersianDate(DateTime.Now).ToString("d");
            faDatePicker3.Text = new PersianDate(DateTime.Now).ToString("d");
            faDatePicker4.Text = new PersianDate(DateTime.Now).ToString("d");
            faDatePicker5.Text = new PersianDate(DateTime.Now.AddMonths(-1)).ToString("d");
            faDatePickerbill.Text = new PersianDate(DateTime.Now.AddMonths(-1)).ToString("d");
            DataTable cc = Utilities.GetTable("select distinct ppid from powerplant");
            CMBPLANT.Text = cc.Rows[0][0].ToString().Trim();
            foreach (DataRow n in cc.Rows)
            {
                CMBPLANT.Items.Add(n[0].ToString().Trim());
            }
           
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            addDateToListbox.Enabled = true;
      
           button1.Enabled = false;
            keyActiv = 1;
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            //khorsand///
            addDateToListbox.Enabled = true;
            ////
            //addDateToListbox.Enabled = false;
            button1.Enabled = true;
           if(radioButton7.Checked)keyActiv = 0;
           else if(radioButton8.Checked) keyActiv = 1;
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            listBox1.Items.Remove(listBox1.SelectedItem);
            //int iik = listBox1.Items.Count;
            //for (int iim = 0; iim < iik; iim++)
            //{
            //    if (listBox1.Items[iim])
            //    {
            //        ChooseItems.Items.Remove(ChooseItems.Items[i]);
            //    }
            //}
        }

        private void radioButton14_CheckedChanged(object sender, EventArgs e)
        {
            keyActivNRI = 1;
           
            groupBox9.Visible = false;
            groupBox10.Visible = false;
            groupBox11.Visible = false;
            groupBox12.Visible = false;
            groupBox2.Visible = false;
            selectItmDaily.Visible = false;
            groupBox13.Visible = true;
            button5.Visible = false;
        }

        private void radioButton13_CheckedChanged(object sender, EventArgs e)
        {
            keyActivNRI = 0;
         
            groupBox9.Visible = true;
            groupBox10.Visible = true;
            groupBox11.Visible = true;
            groupBox12.Visible = true;
            groupBox2.Visible = true;
            selectItmDaily.Visible = true;
            groupBox13.Visible = false;
            button2.Visible = true;
            button5.Visible = true;
        }

        private void faDatePickerbill_ValueChanged(object sender, EventArgs e)
        {
            faDatePicker1.Text = new PersianDate(PersianDateConverter.ToGregorianDateTime(faDatePickerbill.Text.Trim()).AddDays(2)).ToString("d");
        }

        private void faDatePicker5_ValueChanged(object sender, EventArgs e)
        {
            faDatePicker4.Text = new PersianDate(PersianDateConverter.ToGregorianDateTime(faDatePicker5.Text.Trim()).AddDays(2)).ToString("d");
        }

        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }

        public string decimasci(double value)
        {

            string x1 = comboBox1.Text;
            string x2 = comboBox2.Text;
            double cc = 0;
            string ex="0";


            if (x2 == "1")
            {
                cc = value ;                
            }
           else  if (x2 == "10^1")
            {
                cc = value / 10;                
            }
            else if(x2=="10^2")
            {
                cc = value / 100;
            }
            else if (x2 == "10^3")
            {
                cc = value / 1000;
            }
            else if (x2 == "10^4")
            {
                cc = value / 10000;
            }
            else if (x2 == "10^5")
            {
                cc = value / 100000;
            }
            else if (x2 == "10^6")
            {
                cc = value / 1000000;
            }
            else if (x2 == "10^7")
            {
                cc = value / 10000000;
            }
            else if (x2 == "10^8")
            {
                cc = value / 100000000;
            }
            else if (x2 == "10^9")
            {
                cc = value / 1000000000;
            }

            ////////////////////////////////////////////////////
            if (x1 == "0")
            {
                cc = Math.Round(cc, 0);
            }
            else if (x1 == "1")
            {
                cc = Math.Round(cc, 1);
            }
            else if (x1 == "2")
            {
                cc = Math.Round(cc, 2);
            }
            else if (x1 == "3")
            {
                cc = Math.Round(cc, 3);
            }
            else if (x1 == "4")
            {
                cc = Math.Round(cc, 4);
            }
            else if (x1 == "5")
            {
                cc = Math.Round(cc, 5);
            }
            else if (x1 == "6")
            {
                cc = Math.Round(cc, 6);
            }
            else if (x1 == "7")
            {
                cc = Math.Round(cc, 7);
            }
            else if (x1 == "8")
            {
                cc = Math.Round(cc, 8);
            }
            else if (x1 == "9")
            {
                cc = Math.Round(cc, 9);
            }

            ex = cc.ToString();
            return ex;
        }
        public string decimasciMONTH(double value)
        {

            string x1 = comboBox3.Text;
            string x2 = comboBox4.Text;
            double cc = 0;
            string ex = "0";


            if (x2 == "1")
            {
                cc = value;
            }
            else if (x2 == "10^1")
            {
                cc = value / 10;
            }
            else if (x2 == "10^2")
            {
                cc = value / 100;
            }
            else if (x2 == "10^3")
            {
                cc = value / 1000;
            }
            else if (x2 == "10^4")
            {
                cc = value / 10000;
            }
            else if (x2 == "10^5")
            {
                cc = value / 100000;
            }
            else if (x2 == "10^6")
            {
                cc = value / 1000000;
            }
            else if (x2 == "10^7")
            {
                cc = value / 10000000;
            }
            else if (x2 == "10^8")
            {
                cc = value / 100000000;
            }
            else if (x2 == "10^9")
            {
                cc = value / 1000000000;
            }

            ////////////////////////////////////////////////////
            if (x1 == "0")
            {
                cc = Math.Round(cc, 0);
            }
            else if (x1 == "1")
            {
                cc = Math.Round(cc, 1);
            }
            else if (x1 == "2")
            {
                cc = Math.Round(cc, 2);
            }
            else if (x1 == "3")
            {
                cc = Math.Round(cc, 3);
            }
            else if (x1 == "4")
            {
                cc = Math.Round(cc, 4);
            }
            else if (x1 == "5")
            {
                cc = Math.Round(cc, 5);
            }
            else if (x1 == "6")
            {
                cc = Math.Round(cc, 6);
            }
            else if (x1 == "7")
            {
                cc = Math.Round(cc, 7);
            }
            else if (x1 == "8")
            {
                cc = Math.Round(cc, 8);
            }
            else if (x1 == "9")
            {
                cc = Math.Round(cc, 9);
            }

            ex = cc.ToString();
            return ex;
        }

        private void rdtolid_Click(object sender, EventArgs e)
        {
            if (rdtolid.Checked && radioButton14.Checked )
            {
                button2.Visible = false;
            }
            else
            {
                button2.Visible = false;
            }

        }

        private void rddeclare_Click(object sender, EventArgs e)
        {
            if (rddeclare.Checked && radioButton14.Checked)
            {
                button2.Visible = false;
            }
            else
            {
                button2.Visible = false;
            }
        }

        private void rdnri_Click(object sender, EventArgs e)
        {
            button2.Visible = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DialogResult answer = folderBrowserDialog1.ShowDialog();

            if (answer == DialogResult.OK)
            {
               string  path = folderBrowserDialog1.SelectedPath;
                MessageBox.Show("Please Wait.");
                if (keyActivNRI == 0)
                {
                    exportitmBasedDaily(path);
                }
                else if (rdnri.Checked)
                {
                   
                }
                else
                {
                   
                }
            }
            
        }



        public void exportitmBasedDaily(string path)
        {
            //=====================Time range========

            StrHour = Convert.ToInt16(textBox7.Text);

            if ((StrHour < 1) || (StrHour > 24))
            {
                MessageBox.Show("Selected range is illegal");
                return;
            }
            EndHour = Convert.ToInt16(textBox2.Text);
            if ((EndHour < 1) || (EndHour > 24))
            {
                MessageBox.Show("Selected range is illegal");
                return;
            }

            //=======================================
            ///////////////////////////////////////////////////////////////////

            //=====================Date range========
            string strDateitm = PersianDate.Now.ToString();
            string endDateitm = PersianDate.Now.ToString();
         
            //DateTime endMonth = Convert.ToDateTime(faDatePicker1.Text);
            //DateTime curK = endMonth;
            //curK = endMonth.AddDays(1);
           
            if (faDatePickerbill.Text != "[Empty Value]")
            {
                strDateitm = faDatePickerbill.Text;
            }
            else { MessageBox.Show("Please Select Date"); }
            //faDatePicker1.Text=faDatePicker1.SelectedDateTime.AddDays(1).ToString();
            if (faDatePicker1.Text != "[Empty Value]")
            {
                endDateitm = faDatePicker1.Text;
                //khorsand endDateitm = curK.ToString();
            }
            else { MessageBox.Show("Please Select Date"); }

            if (strDateitm == endDateitm)
            {
                MessageBox.Show("Please select a valid range of date");
                valPrint = 0;
                return;
            }
            else
            {
                valPrint = 1;
            }
            //=======================================
            //=============Item Selection============
            DataTable ditem = Utilities.GetTable("select * from ItemSelection");
            BillItemPrintExOld billbill = new BillItemPrintExOld(0);
            int itemsNum = ditem.Columns.Count;
            int itemsNums = billbill.checkedListBoxitmslk.Items.Count;
            //string cK = billbill.checkedListBoxitmslk.Items[0].ToString();
            //string[] v = cK.Split('=');
            int inxItmSelk = 0;
            for (int ii = 0; ii < itemsNums; ii++)
            {
                string cKk = billbill.checkedListBoxitmslk.Items[ii].ToString();
                string[] vh = cKk.Split('=');
                // if(ditem.Columns[ii].Equals==true)
                if (vh[0].Trim() == ditem.Columns[ii].ToString())
                {
                    if (ditem.Rows[0][ii].ToString().Trim() == "1" || ditem.Rows[0][ii].ToString().Trim() == "True")
                    {
                        billbill.checkedListBoxitmslk.SetItemChecked(ii, true);
                        inxItmSelk = ii;
                    }
                    else
                    { billbill.checkedListBoxitmslk.SetItemChecked(ii, false); }

                }
            }

            int cIk = billbill.checkedListBoxitmslk.CheckedItems.Count;
         
            string cK = billbill.checkedListBoxitmslk.CheckedItems[0].ToString();
            string[] v = cK.Split('=');

            string mm = v[0].Trim();
            string nma = v[1].Trim();
            int iiItem = inxItmSelk + 4;
            //=======================================
            //////////////////////////////////////////////////////////////////
           
               


            string databaseSlk;
            if (radioButton9.Checked == true)
            { databaseSlk = "MonthlyBillPlant"; }
            else { databaseSlk = "DailyBillPlant"; }

            DataTable dt2 = Utilities.GetTable("select sum("+mm+"),date,hour from " + databaseSlk + " where Date between '" + strDateitm + "' and '" + endDateitm + "'and ppid='" + CMBPLANT.Text.Trim() + "'and hour between '"+StrHour+"'and '"+EndHour+"' group by hour,date order by date,hour asc");
      
           // double [] val=new double[EndHour];
          
            //foreach (DataRow m in dt2.Rows)
            //{
            //    val[int.Parse(m[1].ToString()) - 1] =MyDoubleParse(m[0].ToString());

            //}
            ////////////////////////////export//////////////////////////
            if (dt2.Rows.Count == 0)
            {
                MessageBox.Show("Incpmplete date.");
                return;

            }

            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            Excel.Application oExcel = new Excel.Application();

            oExcel.SheetsInNewWorkbook = 1;
            Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
            Excel.Worksheet sheet = null;
            sheet = ((Excel.Worksheet)WB.Worksheets["Sheet1"]);
            Excel.Range range = null;

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers
            oExcel.DefaultSheetDirection = (int)Excel.Constants.xlLeftToRight;

            sheet.DisplayRightToLeft = false;

            int sheetIndex = 1;
           
            sheet.Name = "Sheet" + (sheetIndex);

            /////////////////////////////////////////////
            Excel.Range range8= sheet.get_Range(sheet.Cells[1, 1], sheet.Cells[1, 2]);
            range8.Merge(true);
            range8.Value2 =" شماره نيروگاه : " + CMBPLANT.Text.Trim();        
            range8.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range8.Columns.Font.Size = 10;
            range8.Columns.AutoFit();
            range8.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(255, 255, 153));
            ////
            Excel.Range range9 = sheet.get_Range(sheet.Cells[2, 1], sheet.Cells[2, 3]);
            range9.Merge(true);
            range9.Value2 = "از:" + strDateitm + " تا " + endDateitm;
    
            range9.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range9.Columns.Font.Size = 10;
            range9.Columns.AutoFit();

            /////


            range = (Excel.Range)sheet.Cells[3, 1];
            range.Value2 = " تاريخ ";
            range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range.Columns.AutoFit();
            range.ColumnWidth = 25;
            range.Borders.Weight = 3;
            range.Borders.LineStyle = Excel.Constants.xlSolid;
            range.Cells.RowHeight = 18;


            range = (Excel.Range)sheet.Cells[3, 2];
            range.Value2 = " ساعت ";
            range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range.Columns.AutoFit();
            range.ColumnWidth = 25;
            range.Borders.Weight = 3;
            range.Borders.LineStyle = Excel.Constants.xlSolid;
            range.Cells.RowHeight = 18;
            range.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));
            
            range = (Excel.Range)sheet.Cells[3, 3];
            range.Value2 = nma;
            range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range.Columns.AutoFit();
            range.ColumnWidth = 25;
            range.Borders.Weight = 3;
            range.Borders.LineStyle = Excel.Constants.xlSolid;
            range.Cells.RowHeight = 30;
            range.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(255, 255, 0));
            range.WrapText = true;

                for (int d = 0; d < dt2.Rows.Count;d++ )
                {
                    range = (Excel.Range)sheet.Cells[4 + d, 1];
                    range.Value2 = dt2.Rows[d][1].ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.Columns.AutoFit();
                    range.ColumnWidth = 25;
                    range.Borders.Weight = 3;
                    range.Borders.LineStyle = Excel.Constants.xlSolid;
                    range.Cells.RowHeight = 18;


                    range = (Excel.Range)sheet.Cells[4+d,2];
                    range.Value2 = dt2.Rows[d][2].ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.Columns.AutoFit();
                    range.ColumnWidth = 25;
                    range.Borders.Weight = 3;
                    range.Borders.LineStyle = Excel.Constants.xlSolid;
                    range.Cells.RowHeight = 18;



                    range = (Excel.Range)sheet.Cells[4 + d, 3];
                    range.Value2 = dt2.Rows[d][0].ToString(); ;
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    range.Columns.AutoFit();
                    range.ColumnWidth = 25;
                    range.Borders.Weight = 3;
                    range.Borders.LineStyle = Excel.Constants.xlSolid;
                    range.Cells.RowHeight = 18;
                }


                sheet.Activate();


              
               string  filePath = path + "\\" + "IgmcDailyReport" +strDateitm.Replace("/","").Trim()+"-"+endDateitm.Replace("/","").Trim()+".xls";

                WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                WB.Close(Missing.Value, Missing.Value, Missing.Value);
                oExcel.Workbooks.Close();
                oExcel.Quit();

                ///////////////////////////////
                MessageBox.Show("Successfully Eaxport in " + filePath , "Export");
        }
        public void exportitmsBasedDaily(string path)
        {
            //=====================Time range========

            StrHour = Convert.ToInt16(textBox4.Text);

            if ((StrHour < 1) || (StrHour > 24))
            {
                MessageBox.Show("Selected range is illegal");
                return;
            }
            EndHour = Convert.ToInt16(textBox3.Text);
            if ((EndHour < 1) || (EndHour > 24))
            {
                MessageBox.Show("Selected range is illegal");
                return;
            }

            //=======================================
            ///////////////////////////////////////////////////////////////////                       
           

        
            //=======================================
            //=============Item Selection============
            DataTable ditem = Utilities.GetTable("select * from ItemSelection");
            BillItemPrintExOld billbill = new BillItemPrintExOld(0);
            int itemsNum = ditem.Columns.Count;
            int itemsNums = billbill.checkedListBoxitmslk.Items.Count;
        
            int inxItmSelk = 0;
            for (int ii = 0; ii < itemsNums; ii++)
            {
                string cKk = billbill.checkedListBoxitmslk.Items[ii].ToString();
                string[] vh = cKk.Split('=');
                // if(ditem.Columns[ii].Equals==true)
                if (vh[0].Trim() == ditem.Columns[ii].ToString())
                {
                    if (ditem.Rows[0][ii].ToString().Trim() == "1" || ditem.Rows[0][ii].ToString().Trim() == "True")
                    {
                        billbill.checkedListBoxitmslk.SetItemChecked(ii, true);
                        inxItmSelk = ii;
                    }
                    else
                    { billbill.checkedListBoxitmslk.SetItemChecked(ii, false); }

                }
            }

            int cIk = billbill.checkedListBoxitmslk.CheckedItems.Count;

          

            string [,] val = new string [EndHour,cIk];

            
            for (int y = 0; y < cIk;y++ )
            {
                string cK = billbill.checkedListBoxitmslk.CheckedItems[y].ToString();
                string[] v = cK.Split('=');
                string mm = v[0].Trim();
                string nma = v[1].Trim();
                string databaseSlk;
                if (radioButton9.Checked == true)
                { databaseSlk = "MonthlyBillPlant"; }
                else { databaseSlk = "DailyBillPlant"; }

                DataTable dt2 = Utilities.GetTable("select sum(" + mm + "),hour from " + databaseSlk + " where Date='"+faDatePicker2.Text.Trim()+"'and ppid='" + CMBPLANT.Text.Trim() + "'and hour between '" + StrHour + "'and '" + EndHour + "' group by hour order by hour asc");
                foreach (DataRow m in dt2.Rows)
                {
                    val[int.Parse(m[1].ToString()) - 1,y] = (m[0].ToString())+";"+nma;

                }
                if (dt2.Rows.Count == 0)
                {
                    MessageBox.Show("Incpmplete date.");
                    return;

                }
            }
           
            //=======================================
            //////////////////////////////////////////////////////////////////

                   
         

            
            ////////////////////////////export//////////////////////////


            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            Excel.Application oExcel = new Excel.Application();

            oExcel.SheetsInNewWorkbook = 1;
            Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
            Excel.Worksheet sheet = null;
            sheet = ((Excel.Worksheet)WB.Worksheets["Sheet1"]);
            Excel.Range range = null;

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers
            oExcel.DefaultSheetDirection = (int)Excel.Constants.xlLeftToRight;

            sheet.DisplayRightToLeft = false;

            int sheetIndex = 1;

            sheet.Name = "Sheet" + (sheetIndex);

            /////////////////////////////////////////////
            Excel.Range range8 = sheet.get_Range(sheet.Cells[1, 1], sheet.Cells[1, 2]);
            range8.Merge(true);
            range8.Value2 = " شماره نيروگاه : " + CMBPLANT.Text.Trim();
            range8.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range8.Columns.Font.Size = 10;
            range8.Columns.AutoFit();
            range8.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(255, 255, 153));
            ////
            Excel.Range range9 = sheet.get_Range(sheet.Cells[2, 1], sheet.Cells[2, 3]);
            range9.Merge(true);
            range9.Value2 = "تاريخ :" + faDatePicker2.Text.Trim() ;

            range9.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range9.Columns.Font.Size = 10;
            range9.Columns.AutoFit();

            /////


         

            range = (Excel.Range)sheet.Cells[3, 1];
            range.Value2 = " ساعت ";
            range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            range.Columns.AutoFit();
            range.ColumnWidth = 25;
            range.Borders.Weight = 3;
            range.Borders.LineStyle = Excel.Constants.xlSolid;
            range.Cells.RowHeight = 18;
            range.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(178, 178, 178));

            for (int y = 0; y < cIk; y++)
            {
                string cK = billbill.checkedListBoxitmslk.CheckedItems[y].ToString();
                string[] v = cK.Split('=');
          
                string nma = v[1].Trim();
                range = (Excel.Range)sheet.Cells[3, 2+y];
                range.Value2 = nma;
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Columns.AutoFit();
                range.ColumnWidth = 25;
                range.Borders.Weight = 3;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Cells.RowHeight = 30;
                range.Columns.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.FromArgb(255, 255, 0));
                range.WrapText = true;
            }


            for (int d = 0; d < EndHour; d++)
            {
                range = (Excel.Range)sheet.Cells[4 + d, 1];
                range.Value2 = d + 1;
                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                range.Columns.AutoFit();
                range.ColumnWidth = 25;
                range.Borders.Weight = 3;
                range.Borders.LineStyle = Excel.Constants.xlSolid;
                range.Cells.RowHeight = 18;
                for (int y = 0; y < cIk; y++)
                {
                    if (val[d, y] != null)
                    {
                        range = (Excel.Range)sheet.Cells[4 + d, y + 2];
                        string[] sp = val[d, y].Split(';');
                        string vv = sp[0];
                        range.Value2 = vv;
                        range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                        range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                        range.Columns.AutoFit();
                        range.ColumnWidth = 25;
                        range.Borders.Weight = 3;
                        range.Borders.LineStyle = Excel.Constants.xlSolid;
                        range.Cells.RowHeight = 18;
                    }
                   
                }
            }


            sheet.Activate();



            string filePath = path + "\\" + "IgmcItemsDailyReport" + faDatePicker2.Text.Replace("/", "").Trim() + ".xls";

            WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            WB.Close(Missing.Value, Missing.Value, Missing.Value);
            oExcel.Workbooks.Close();
            oExcel.Quit();

            ///////////////////////////////
            MessageBox.Show("Successfully Export In" + filePath , "Export");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DialogResult answer = folderBrowserDialog1.ShowDialog();

            if (answer == DialogResult.OK)
            {
                string path = folderBrowserDialog1.SelectedPath;
                MessageBox.Show(".لطفا تا نمایش پیغام موفقیت منتظر بمانید");
                if (keyActivNRI == 0)
                {
                    exportitmsBasedDaily(path);
                }
                else
                {
                    // datBsdDailyNRI();
                }

            }
        }

        private void btnmontexp_Click(object sender, EventArgs e)
        {
            DialogResult answer = folderBrowserDialog1.ShowDialog();

            if (answer == DialogResult.OK)
            {
                string path = folderBrowserDialog1.SelectedPath;
                MessageBox.Show(".لطفا تا نمایش پیغام موفقیت منتظر بمانید");
                exportitmBsdMonthlySelective(path);
            }
        }

      

       

        





        //----------------------------------------------------------------------
        /////////////////////////////////////////////////////////////////////////
        //----------------------------------------------------------------------
        /////////////////////////////////////////////////////////////////////////
        //----------------------------------------------------------------------
        /////////////////////////////////////////////////////////////////////////

    }
}
