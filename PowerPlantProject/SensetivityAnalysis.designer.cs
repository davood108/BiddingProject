﻿namespace PowerPlantProject
{
    partial class SensetivityAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SensetivityAnalysis));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtnday = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.datePickerCurrent = new FarsiLibrary.Win.Controls.FADatePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbPlant = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hour24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnprint = new System.Windows.Forms.Button();
            this.BtnRun = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ITEM,
            this.Hour1,
            this.Hour2,
            this.Hour3,
            this.Hour4,
            this.Hour5,
            this.Hour6,
            this.Hour7,
            this.Hour8,
            this.Hour9,
            this.Hour10,
            this.Hour11,
            this.Hour12});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.DarkSeaGreen;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(3, 95);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LemonChiffon;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(997, 255);
            this.dataGridView1.TabIndex = 1;
            // 
            // ITEM
            // 
            this.ITEM.HeaderText = "ITEM";
            this.ITEM.Name = "ITEM";
            // 
            // Hour1
            // 
            this.Hour1.HeaderText = "Hour1";
            this.Hour1.Name = "Hour1";
            // 
            // Hour2
            // 
            this.Hour2.HeaderText = "Hour2";
            this.Hour2.Name = "Hour2";
            // 
            // Hour3
            // 
            this.Hour3.HeaderText = "Hour3";
            this.Hour3.Name = "Hour3";
            // 
            // Hour4
            // 
            this.Hour4.HeaderText = "Hour4";
            this.Hour4.Name = "Hour4";
            // 
            // Hour5
            // 
            this.Hour5.HeaderText = "Hour5";
            this.Hour5.Name = "Hour5";
            // 
            // Hour6
            // 
            this.Hour6.HeaderText = "Hour6";
            this.Hour6.Name = "Hour6";
            // 
            // Hour7
            // 
            this.Hour7.HeaderText = "Hour7";
            this.Hour7.Name = "Hour7";
            // 
            // Hour8
            // 
            this.Hour8.HeaderText = "Hour8";
            this.Hour8.Name = "Hour8";
            // 
            // Hour9
            // 
            this.Hour9.HeaderText = "Hour9";
            this.Hour9.Name = "Hour9";
            // 
            // Hour10
            // 
            this.Hour10.HeaderText = "Hour10";
            this.Hour10.Name = "Hour10";
            // 
            // Hour11
            // 
            this.Hour11.HeaderText = "Hour11";
            this.Hour11.Name = "Hour11";
            // 
            // Hour12
            // 
            this.Hour12.HeaderText = "Hour12";
            this.Hour12.Name = "Hour12";
            // 
            // txtnday
            // 
            this.txtnday.Location = new System.Drawing.Point(714, 23);
            this.txtnday.Name = "txtnday";
            this.txtnday.Size = new System.Drawing.Size(100, 20);
            this.txtnday.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(626, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Training Days  :";
            // 
            // datePickerCurrent
            // 
            this.datePickerCurrent.HasButtons = false;
            this.datePickerCurrent.Location = new System.Drawing.Point(126, 24);
            this.datePickerCurrent.Name = "datePickerCurrent";
            this.datePickerCurrent.Readonly = true;
            this.datePickerCurrent.Size = new System.Drawing.Size(120, 20);
            this.datePickerCurrent.TabIndex = 34;
            this.datePickerCurrent.ValueChanged += new System.EventHandler(this.datePickerCurrent_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(44, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Current Date  :";
            // 
            // cmbPlant
            // 
            this.cmbPlant.FormattingEnabled = true;
            this.cmbPlant.Location = new System.Drawing.Point(403, 24);
            this.cmbPlant.Name = "cmbPlant";
            this.cmbPlant.Size = new System.Drawing.Size(121, 21);
            this.cmbPlant.TabIndex = 35;
            this.cmbPlant.TextChanged += new System.EventHandler(this.cmbPlant_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(357, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Plant  :";
            // 
            // dataGridView2
            // 
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.dataGridView2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.Hour13,
            this.Hour14,
            this.Hour15,
            this.Hour16,
            this.Hour17,
            this.Hour18,
            this.Hour19,
            this.Hour20,
            this.Hour21,
            this.Hour22,
            this.Hour23,
            this.Hour24});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.Location = new System.Drawing.Point(3, 375);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.LemonChiffon;
            this.dataGridView2.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(997, 255);
            this.dataGridView2.TabIndex = 36;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ITEM";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // Hour13
            // 
            this.Hour13.HeaderText = "Hour13";
            this.Hour13.Name = "Hour13";
            // 
            // Hour14
            // 
            this.Hour14.HeaderText = "Hour14";
            this.Hour14.Name = "Hour14";
            // 
            // Hour15
            // 
            this.Hour15.HeaderText = "Hour15";
            this.Hour15.Name = "Hour15";
            // 
            // Hour16
            // 
            this.Hour16.HeaderText = "Hour16";
            this.Hour16.Name = "Hour16";
            // 
            // Hour17
            // 
            this.Hour17.HeaderText = "Hour17";
            this.Hour17.Name = "Hour17";
            // 
            // Hour18
            // 
            this.Hour18.HeaderText = "Hour18";
            this.Hour18.Name = "Hour18";
            // 
            // Hour19
            // 
            this.Hour19.HeaderText = "Hour19";
            this.Hour19.Name = "Hour19";
            // 
            // Hour20
            // 
            this.Hour20.HeaderText = "Hour20";
            this.Hour20.Name = "Hour20";
            // 
            // Hour21
            // 
            this.Hour21.HeaderText = "Hour21";
            this.Hour21.Name = "Hour21";
            // 
            // Hour22
            // 
            this.Hour22.HeaderText = "Hour22";
            this.Hour22.Name = "Hour22";
            // 
            // Hour23
            // 
            this.Hour23.HeaderText = "Hour23";
            this.Hour23.Name = "Hour23";
            // 
            // Hour24
            // 
            this.Hour24.HeaderText = "Hour24";
            this.Hour24.Name = "Hour24";
            // 
            // btnprint
            // 
            this.btnprint.BackColor = System.Drawing.Color.Linen;
            this.btnprint.BackgroundImage = global::PowerPlantProject.Properties.Resources._1263108050_agt_print;
            this.btnprint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnprint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnprint.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnprint.Location = new System.Drawing.Point(5, 59);
            this.btnprint.Name = "btnprint";
            this.btnprint.Size = new System.Drawing.Size(76, 30);
            this.btnprint.TabIndex = 38;
            this.btnprint.Text = "Print";
            this.btnprint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnprint.UseVisualStyleBackColor = false;
            this.btnprint.Click += new System.EventHandler(this.btnprint_Click);
            // 
            // BtnRun
            // 
            this.BtnRun.BackColor = System.Drawing.Color.CadetBlue;
            this.BtnRun.BackgroundImage = global::PowerPlantProject.Properties.Resources.run88;
            this.BtnRun.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BtnRun.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnRun.Location = new System.Drawing.Point(900, 16);
            this.BtnRun.Name = "BtnRun";
            this.BtnRun.Size = new System.Drawing.Size(91, 31);
            this.BtnRun.TabIndex = 0;
            this.BtnRun.Text = "Run";
            this.BtnRun.UseVisualStyleBackColor = false;
            this.BtnRun.Click += new System.EventHandler(this.button1_Click);
            // 
            // SensetivityAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1003, 648);
            this.Controls.Add(this.btnprint);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.cmbPlant);
            this.Controls.Add(this.datePickerCurrent);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtnday);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.BtnRun);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SensetivityAnalysis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SensetivityAnalysis";
            this.Load += new System.EventHandler(this.SensetivityAnalysis_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnRun;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtnday;
        private System.Windows.Forms.Label label1;
        private FarsiLibrary.Win.Controls.FADatePicker datePickerCurrent;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbPlant;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hour24;
        private System.Windows.Forms.Button btnprint;
    }
}