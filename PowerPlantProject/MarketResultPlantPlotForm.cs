﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dundas.Charting.WinControl.ChartTypes;
using Dundas.Charting.WinControl;
using System.Data.SqlClient;
using NRI.SBS.Common;


namespace PowerPlantProject
{
    public partial class MarketResultPlantPlotForm : Form
    {
        public string Date { get; set; }
        public string PlantId { get; set; }
        string[] arr ;
        bool IsEstimated;
        bool Limmit;

        public MarketResultPlantPlotForm(bool estimate,bool limmit)
        {
            IsEstimated = estimate;
            Limmit = limmit;

            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }
        //--------------------------------------
        private void MarketResultPlantPlotForm_Load(object sender, EventArgs e)
        {
            FillMRPlantDataSet();


            FillChart();
        }
        //-------------------------------------
       
        private void FillChart()
        {
            chart1.ChartAreas["Default"].AxisX.Margin = false;
            chart1.ChartAreas["Default"].AxisX.Minimum = 0;
            chart1.ChartAreas["Default"].AxisX.Interval = 1;
            chart1.ChartAreas["Default"].AxisX.Maximum = 24;
            //chart1.ChartAreas["Default"].AxisY.Minimum = 0;
            // chart1.ChartAreas["Default"].AxisY.Interval = 20;
            double min = 0, max = 0;
            findMinMaxOfArray(arr, ref min, ref max);

            if (min > 50)
                chart1.ChartAreas["Default"].AxisY.Minimum = min - 50;
            else
                chart1.ChartAreas["Default"].AxisY.Minimum = min;
            chart1.ChartAreas["Default"].AxisY.Maximum = max + 50;

            for (int pointIndex = 1; pointIndex <= 24; pointIndex++)
            {
                chart1.Series["Power"].Points.AddY(arr[pointIndex]);

            }

            // Set series chart type
            chart1.Series["Power"].Type = SeriesChartType.Spline;

            // Set point labels
            chart1.Series["Power"].ShowLabelAsValue = true;
            chart1.Series["Power"].Color = Color.Red;


            // Enable X axis margin
            chart1.ChartAreas["Default"].AxisX.Margin = true;
            chart1.UI.Toolbar.Enabled = true;
        }

        //-------------------------------------

        private void FillMRPlantDataSet()
        {

            string tname = "DetailFRM005";
            if (Limmit) tname = "baDetailFRM005";

            if (!IsEstimated)
            {
                if ((Date != null) && (Date != ""))
                {

                    DataSet MyDS = new DataSet();
                    SqlDataAdapter Myda = new SqlDataAdapter();
                    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                    myConnection.Open();

                    Myda.SelectCommand = new SqlCommand("SELECT Hour,SUM(Required) AS SR FROM "+tname+" WHERE PPID= " + PlantId + " AND TargetMarketDate=@date GROUP BY Hour", myConnection);
                    Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                    Myda.SelectCommand.Parameters["@date"].Value = Date;
                    Myda.Fill(MyDS);
                    myConnection.Close();

                    arr = new string[25];
                    foreach (DataRow MyRow in MyDS.Tables[0].Rows)
                    {
                        int index = int.Parse(MyRow["Hour"].ToString().Trim());
                        arr[index] = string.Format("{0:n1}", double.Parse(MyRow["SR"].ToString()));
                    }
                }
            }
            else if (IsEstimated)
            {
                if ((Date != null) && (Date != ""))
                {

                    DataSet MyDS = new DataSet();
                    SqlDataAdapter Myda = new SqlDataAdapter();
                    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                    myConnection.Open();

                    Myda.SelectCommand = new SqlCommand("select * from dbo.PowerForecast where date=@date and gencode='" + PlantId + "' order by Id desc", myConnection);
                    Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                    Myda.SelectCommand.Parameters["@date"].Value = Date;
                    Myda.Fill(MyDS);
                    myConnection.Close();

                    arr = new string[25];
                   
                    for (int i = 1; i <= 24; i++)
                    {

                        arr[i] = string.Format("{0:n1}", double.Parse(MyDS.Tables[0].Rows[0][i + 2].ToString()));

                    }
                }



            }
        }

        //--------------------------------------
        private void findMinMaxOfArray(string[] arr, ref double min, ref double max)
        {
            if (arr[1] != null)
            {
                min = double.Parse(arr[1]);
                max = double.Parse(arr[1]);
            }
            try
            {
                for (int i = 2; i < 25; i++)
                {
                    if (arr[i] != null)
                    {
                        if (double.Parse(arr[i]) < min)
                            min = double.Parse(arr[i]);
                        if (double.Parse(arr[i]) > max)
                            max = double.Parse(arr[i]);
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }
            min = 0;

        }
        //--------------------------------------
    }
}
