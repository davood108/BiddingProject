﻿namespace PowerPlantProject
{
    partial class MarketInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MarketInformation));
            this.datePickerweekend = new FarsiLibrary.Win.Controls.FADatePicker();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdload = new System.Windows.Forms.RadioButton();
            this.rdchart = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdinter = new System.Windows.Forms.RadioButton();
            this.rdpro = new System.Windows.Forms.RadioButton();
            this.btnprint = new System.Windows.Forms.Button();
            this.btnprint2 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdhub = new System.Windows.Forms.RadioButton();
            this.rdtrans = new System.Windows.Forms.RadioButton();
            this.rdpact = new System.Windows.Forms.RadioButton();
            this.rdplimm = new System.Windows.Forms.RadioButton();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // datePickerweekend
            // 
            this.datePickerweekend.HasButtons = true;
            this.datePickerweekend.Location = new System.Drawing.Point(12, 40);
            this.datePickerweekend.Name = "datePickerweekend";
            this.datePickerweekend.Readonly = true;
            this.datePickerweekend.Size = new System.Drawing.Size(149, 20);
            this.datePickerweekend.TabIndex = 38;
            this.datePickerweekend.Theme = FarsiLibrary.Win.Enums.ThemeTypes.Office2007;
            this.datePickerweekend.ValueChanged += new System.EventHandler(this.datePickerweekend_ValueChanged);
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.GridColor = System.Drawing.Color.CadetBlue;
            this.dataGridView1.Location = new System.Drawing.Point(2, 80);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LemonChiffon;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1014, 133);
            this.dataGridView1.TabIndex = 39;
            // 
            // dataGridView2
            // 
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.dataGridView2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.GridColor = System.Drawing.Color.CadetBlue;
            this.dataGridView2.Location = new System.Drawing.Point(2, 278);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.LemonChiffon;
            this.dataGridView2.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(1014, 175);
            this.dataGridView2.TabIndex = 39;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.groupBox1.Controls.Add(this.rdload);
            this.groupBox1.Controls.Add(this.rdchart);
            this.groupBox1.Location = new System.Drawing.Point(386, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(247, 34);
            this.groupBox1.TabIndex = 40;
            this.groupBox1.TabStop = false;
            // 
            // rdload
            // 
            this.rdload.AutoSize = true;
            this.rdload.Checked = true;
            this.rdload.Location = new System.Drawing.Point(7, 11);
            this.rdload.Name = "rdload";
            this.rdload.Size = new System.Drawing.Size(104, 17);
            this.rdload.TabIndex = 1;
            this.rdload.TabStop = true;
            this.rdload.Text = "LoadForecasting";
            this.rdload.UseVisualStyleBackColor = true;
            this.rdload.CheckedChanged += new System.EventHandler(this.rdload_CheckedChanged);
            // 
            // rdchart
            // 
            this.rdchart.AutoSize = true;
            this.rdchart.Location = new System.Drawing.Point(158, 10);
            this.rdchart.Name = "rdchart";
            this.rdchart.Size = new System.Drawing.Size(74, 17);
            this.rdchart.TabIndex = 0;
            this.rdchart.Text = "ChartPrice";
            this.rdchart.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.groupBox2.Controls.Add(this.rdinter);
            this.groupBox2.Controls.Add(this.rdpro);
            this.groupBox2.Location = new System.Drawing.Point(386, 226);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(247, 39);
            this.groupBox2.TabIndex = 41;
            this.groupBox2.TabStop = false;
            // 
            // rdinter
            // 
            this.rdinter.AutoSize = true;
            this.rdinter.Checked = true;
            this.rdinter.Location = new System.Drawing.Point(6, 16);
            this.rdinter.Name = "rdinter";
            this.rdinter.Size = new System.Drawing.Size(121, 17);
            this.rdinter.TabIndex = 1;
            this.rdinter.TabStop = true;
            this.rdinter.Text = "IntercahangeEnergy";
            this.rdinter.UseVisualStyleBackColor = true;
            this.rdinter.CheckedChanged += new System.EventHandler(this.rdinter_CheckedChanged);
            // 
            // rdpro
            // 
            this.rdpro.AutoSize = true;
            this.rdpro.Location = new System.Drawing.Point(133, 16);
            this.rdpro.Name = "rdpro";
            this.rdpro.Size = new System.Drawing.Size(98, 17);
            this.rdpro.TabIndex = 0;
            this.rdpro.Text = "ProduceEnergy";
            this.rdpro.UseVisualStyleBackColor = true;
            // 
            // btnprint
            // 
            this.btnprint.BackColor = System.Drawing.Color.Linen;
            this.btnprint.BackgroundImage = global::PowerPlantProject.Properties.Resources._1263108050_agt_print;
            this.btnprint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnprint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnprint.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnprint.Location = new System.Drawing.Point(974, 44);
            this.btnprint.Name = "btnprint";
            this.btnprint.Size = new System.Drawing.Size(33, 30);
            this.btnprint.TabIndex = 42;
            this.btnprint.Tag = "Print";
            this.btnprint.UseVisualStyleBackColor = false;
            this.btnprint.Click += new System.EventHandler(this.btnprint_Click);
            // 
            // btnprint2
            // 
            this.btnprint2.BackColor = System.Drawing.Color.Linen;
            this.btnprint2.BackgroundImage = global::PowerPlantProject.Properties.Resources._1263108050_agt_print;
            this.btnprint2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnprint2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnprint2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnprint2.Location = new System.Drawing.Point(974, 235);
            this.btnprint2.Name = "btnprint2";
            this.btnprint2.Size = new System.Drawing.Size(33, 30);
            this.btnprint2.TabIndex = 42;
            this.btnprint2.Tag = "Print";
            this.btnprint2.UseVisualStyleBackColor = false;
            this.btnprint2.Click += new System.EventHandler(this.btnprint2_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.groupBox3.Controls.Add(this.rdhub);
            this.groupBox3.Controls.Add(this.rdplimm);
            this.groupBox3.Controls.Add(this.rdpact);
            this.groupBox3.Controls.Add(this.rdtrans);
            this.groupBox3.Location = new System.Drawing.Point(277, 474);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(464, 50);
            this.groupBox3.TabIndex = 43;
            this.groupBox3.TabStop = false;
            // 
            // rdhub
            // 
            this.rdhub.AutoSize = true;
            this.rdhub.Checked = true;
            this.rdhub.Location = new System.Drawing.Point(18, 19);
            this.rdhub.Name = "rdhub";
            this.rdhub.Size = new System.Drawing.Size(45, 17);
            this.rdhub.TabIndex = 1;
            this.rdhub.TabStop = true;
            this.rdhub.Text = "Hub";
            this.rdhub.UseVisualStyleBackColor = true;
            this.rdhub.CheckedChanged += new System.EventHandler(this.rdhub_CheckedChanged);
            // 
            // rdtrans
            // 
            this.rdtrans.AutoSize = true;
            this.rdtrans.Location = new System.Drawing.Point(114, 19);
            this.rdtrans.Name = "rdtrans";
            this.rdtrans.Size = new System.Drawing.Size(86, 17);
            this.rdtrans.TabIndex = 0;
            this.rdtrans.Text = "Transmission";
            this.rdtrans.UseVisualStyleBackColor = true;
            this.rdtrans.CheckedChanged += new System.EventHandler(this.rdtrans_CheckedChanged);
            // 
            // rdpact
            // 
            this.rdpact.AutoSize = true;
            this.rdpact.Location = new System.Drawing.Point(241, 19);
            this.rdpact.Name = "rdpact";
            this.rdpact.Size = new System.Drawing.Size(61, 17);
            this.rdpact.TabIndex = 0;
            this.rdpact.Text = "Pactual";
            this.rdpact.UseVisualStyleBackColor = true;
            this.rdpact.CheckedChanged += new System.EventHandler(this.rdpact_CheckedChanged);
            // 
            // rdplimm
            // 
            this.rdplimm.AutoSize = true;
            this.rdplimm.Location = new System.Drawing.Point(352, 19);
            this.rdplimm.Name = "rdplimm";
            this.rdplimm.Size = new System.Drawing.Size(96, 17);
            this.rdplimm.TabIndex = 0;
            this.rdplimm.Text = "PowerLimmited";
            this.rdplimm.UseVisualStyleBackColor = true;
            this.rdplimm.CheckedChanged += new System.EventHandler(this.rdplimm_CheckedChanged);
            // 
            // dataGridView3
            // 
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            this.dataGridView3.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView3.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView3.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView3.EnableHeadersVisualStyles = false;
            this.dataGridView3.GridColor = System.Drawing.Color.CadetBlue;
            this.dataGridView3.Location = new System.Drawing.Point(2, 539);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RowHeadersVisible = false;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.LemonChiffon;
            this.dataGridView3.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(1014, 139);
            this.dataGridView3.TabIndex = 44;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Linen;
            this.button1.BackgroundImage = global::PowerPlantProject.Properties.Resources._1263108050_agt_print;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.button1.Location = new System.Drawing.Point(974, 490);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 30);
            this.button1.TabIndex = 45;
            this.button1.Tag = "Print";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MarketInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.ClientSize = new System.Drawing.Size(1019, 682);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnprint2);
            this.Controls.Add(this.btnprint);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.datePickerweekend);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MarketInformation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MarketInformation";
            this.Load += new System.EventHandler(this.MarketInformation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private FarsiLibrary.Win.Controls.FADatePicker datePickerweekend;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdload;
        private System.Windows.Forms.RadioButton rdchart;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdinter;
        private System.Windows.Forms.RadioButton rdpro;
        private System.Windows.Forms.Button btnprint;
        private System.Windows.Forms.Button btnprint2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdhub;
        private System.Windows.Forms.RadioButton rdplimm;
        private System.Windows.Forms.RadioButton rdpact;
        private System.Windows.Forms.RadioButton rdtrans;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Button button1;
    }
}