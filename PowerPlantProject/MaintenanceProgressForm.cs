﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using NRI.SBS.Common;
using FarsiLibrary.Utils;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using ILOG.Concert;
using ILOG.CPLEX;

namespace PowerPlantProject
{
    public partial class MaintenanceProgressForm : Form
    {

        MainForm parentForm;
        bool finish = false;
        string titleForm = "Maintenance";
        Thread thread;
        ///////////////new////////////////


        public MaintenanceProgressForm(MainForm form2, string maintPlant, MaintenanceStratedgy mmaintenanceStratedgy)
        {
            parentForm = form2;
            selectedPlant = maintPlant;
            maintenanceStratedgy = mmaintenanceStratedgy;
            InitializeComponent();

            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        public MaintenanceProgressForm(string maintPlant, MaintenanceStratedgy mmaintenanceStratedgy)
        {
            parentForm = null;
            selectedPlant = maintPlant;
            maintenanceStratedgy = mmaintenanceStratedgy;
            InitializeComponent();
        }

        private void MaintenanceProgressForm_Load(object sender, EventArgs e)
        {
            this.FormClosing += new FormClosingEventHandler(ProgressForm_OnClosing);
            LongTaskBegin();
        }

        private void LongTaskBegin()
        {
            progressBar1.Maximum = 10;
            if (parentForm!=null)
                parentForm.EnableMaintenanceGraph(false);
            thread = new Thread(RunMaintenance);
            thread.IsBackground = true;
            thread.Start();
        }

        public void UpdateProgressBar(int value, string description)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action< int, string>(UpdateProgressBar), new object[] { value, description });
                return;
            }
            lblTitle.Text = description;
            this.Text = titleForm;
            progressBar1.Value = value;
            Thread.Sleep(1);
            //if (cancel)
            //    Thread.CurrentThread.Abort();
        }


        private double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private double MyDoubleParse(object obj)
        {
            return MyDoubleParse(obj.ToString());
        }
        //private void RunMaintenance()
        //{
        //    UpdateProgressBar(2, "");
        //    oMaintenance.get();
        //    UpdateProgressBar(progressBar1.Maximum, "Finish");
        //    this.parentForm.maintData = oMaintenance;
        //    //btnExportEnable = true;
        //}
        private void btnClose_Click(object sender, EventArgs e)
        {
            thread.Abort();
            this.Close();
            
            
        }

        private void ProgressForm_OnClosing(object sender, EventArgs e)
        {
            if (parentForm!=null)
                parentForm.EnableMaintenanceGraph(true);
            //parentForm.FillStackedBarChartPlant(0);
        }

        private int[,] daysdiff()
        {
            int[,] daystype_maint = new int[Plants_Num, Max_UnitsNum];

            DateTime gToday = DateTime.Now;

            PersianDate pToday = new PersianDate(gToday);

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {

                    string strCmd = "SELECT * FROM ConditionUnit" +
                        " WHERE UnitCode='" + Plant_UnitCode[j, i] + "'" +
                    " AND PackageType='" + Plant_UnitDec[j, i, 1] + "'" +
                    " AND PPID='" + plant[j] + "'" +
                     " AND MaintenanceEndDate>'" + pToday.ToString("d") + "'" +
                     " Order by Date DESC";

                    DataTable oDataTable = Utilities.GetTable(strCmd);

                    if (oDataTable != null && oDataTable.Rows.Count > 0)
                    {
                        DataRow row = oDataTable.Rows[0];
                        if (row["Maintenance"] != null && bool.Parse(row["Maintenance"].ToString()))
                        {
                            DateTime date = PersianDateConverter.ToGregorianDateTime(row["MaintenanceStartDate"].ToString());
                            daystype_maint[j, i] = (new DateTime(gToday.Year, gToday.Month, gToday.Day, 0, 0, 0) - new DateTime(date.Year, date.Month, date.Day, 0, 0, 0)).Days;

                        }
                    }


                }
            }
            return daystype_maint;

        }
        /////////////////////////////////////

        MaintenanceOutData maintData = null;

        public MaintenanceOutData MaintData
        {
            get { return maintData; }
            set { maintData = value; }
        }
        //Fixed Set :
        int Const_Hour = 24;
        int Const_DecNum = 4;
        int Const_DataNum = 20;
        int Const_Mmax = 3;
        int Const_StepNum = 10;
        int Const_Week = 52;
        //int Const_Period = 5;
        int Max_Periods;
        int Const_Day = 7;
        int Const_daysInYear = 365;
        int CplexSolve = 0;


        List<CMatrix> result;
        /// ///////////////////
        /// 
        MaintenanceStratedgy maintenanceStratedgy;
        string selectedPlant;
        private DataTable oDataTable = null;
        private string[] plant;
        private string[] plantName;
        int Max_UnitsNum;
        int Plants_Num;
        int Max_Package_Num;
        int[] Plant_unitsNum;
        int[] Plant_Packages_Num;
        string[, ,] Plant_UnitDec;
        string[,] Plant_UnitCode;
        double[, ,] Plant_UnitData;
        int[,] Plant_units_maint;
        int[,] Plant_units_CI_Hour;
        int[,] Plant_units_CI_Dur ;
        int[,] Plant_units_MO_Hour;
        int[,] Plant_units_MO_Dur;
        int[,] Plant_units_HGP_Hour;
        int[,] Plant_units_HGP_Dur ;
        int[,] Plant_units_DurabilityHour;
        double[,] Plant_units_FuelFactor ;
        double[,] Plant_units_StartUpFactor ;

        private string GetNearestDate(string strPPID, string strUnitCode/*, string strPackageType*/)
        {
            // HAMIDI SHOULD CHANGE
           string date = (new PersianDate(DateTime.Now)).ToString("d");
           // string date = "1389/04/25";

            string strComd = "select max(DurabilityDateTime) from MaintenanceGeneralData where" +
                                " PPID='" + strPPID + "'" +
                                " AND UnitCode='" + strUnitCode + "'" ;
                                //" AND PackageType='" + strPackageType + "'";

            string strComd1 = strComd + " and DurabilityDateTime<'" + date + "'";
            DataTable oDatatable = Utilities.GetTable(strComd1);
            if (oDatatable != null && oDatatable.Rows.Count > 0 && oDatatable.Rows[0][0].ToString() != "")
                //return new PersianDate(oDatatable.Rows[0][0].ToString());
                return oDatatable.Rows[0][0].ToString().Trim();

            oDatatable = Utilities.GetTable(strComd);
            if (oDatatable != null && oDatatable.Rows.Count > 0 && oDatatable.Rows[0][0].ToString() != "")
                //return new PersianDate(oDatatable.Rows[0][0].ToString());
                return oDatatable.Rows[0][0].ToString().Trim();
            return null;

        }
        private bool[] CheckSettingAvailability()
        {
            string[,] checktype = new string[Plants_Num, Max_UnitsNum];

            for (int j = 0; j < Plants_Num; j++)
            {

                DataTable tableunittype = Utilities.GetTable("select *  from UnitsDataMain where  PPID='" + plant[j] + "'");

                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {

                    checktype[j, i] = tableunittype.Rows[i]["UnitType"].ToString().Trim();
                }
            }



            bool[] available = new bool[Plants_Num];
            for (int i = 0; i < Plants_Num; i++)
                available[i] = true;


            Plant_units_maint = new int[Plants_Num, Max_UnitsNum];
            Plant_units_CI_Hour = new int[Plants_Num, Max_UnitsNum];
            Plant_units_CI_Dur = new int[Plants_Num, Max_UnitsNum];
            Plant_units_MO_Hour = new int[Plants_Num, Max_UnitsNum];
            Plant_units_MO_Dur = new int[Plants_Num, Max_UnitsNum];
            Plant_units_HGP_Hour = new int[Plants_Num, Max_UnitsNum];
            Plant_units_HGP_Dur = new int[Plants_Num, Max_UnitsNum];
            Plant_units_DurabilityHour = new int[Plants_Num, Max_UnitsNum];
            Plant_units_FuelFactor = new double[Plants_Num, Max_UnitsNum];
            Plant_units_StartUpFactor = new double[Plants_Num, Max_UnitsNum];

            Max_Periods = 0;
            for (int j = 0; j < Plants_Num; j++)
            {

                for (int i = 0; i < Plant_unitsNum[j] && available[j]; i++)
                {
                    //Plant_units_maint[j, i] = Const_Period;
                    string strPersianDate = GetNearestDate(plant[j], Plant_UnitCode[j, i]/*, Plant_UnitDec[j, i, 1]*/);
                    if (strPersianDate == "" || strPersianDate == null)
                    {
                        available[j] = false;
                        break;
                    }


                    PersianDate nearestDate = new PersianDate(strPersianDate);
                    string strCmd = "select * from maintenanceGeneralData where" +
                                    " PPID='" + plant[j] + "'" +
                                    " AND UnitCode='" + Plant_UnitCode[j, i] + "'" +
                        //" AND PackageType='" + Plant_UnitDec[j, i, 1] + "'" +
                                    " AND DurabilityDateTime='" + nearestDate.ToString("d") + "'";

                    DataTable odataTable = Utilities.GetTable(strCmd);
                    if (odataTable != null && odataTable.Rows.Count > 0)
                    {
                        DataRow row = odataTable.Rows[0];

                        string temp = row["CIHour"].ToString();
                        if (temp.Trim() == "")
                            available[j] = false;
                        else
                            Plant_units_CI_Hour[j, i] = Convert.ToInt32(temp);

                        temp = row["HGPHour"].ToString();
                        if (temp.Trim() == "" && checktype[j, i] != "Steam")
                            available[j] = false;
                        else if (checktype[j, i] != "Steam")
                            Plant_units_HGP_Hour[j, i] = Convert.ToInt32(temp);

                        temp = row["MOHour"].ToString();
                        if (temp.Trim() == "")
                            available[j] = false;
                        else
                            Plant_units_MO_Hour[j, i] = Convert.ToInt32(temp);

                        temp = row["CIDuration"].ToString();
                        if (temp.Trim() == "")
                            available[j] = false;
                        else
                            Plant_units_CI_Dur[j, i] = Convert.ToInt32(temp);

                        temp = row["HGPDuration"].ToString();
                        if (temp.Trim() == "" && checktype[j, i] != "Steam")
                            available[j] = false;
                        else if (checktype[j, i] != "Steam")
                            Plant_units_HGP_Dur[j, i] = Convert.ToInt32(temp);


                        temp = row["MODuration"].ToString();
                        if (temp.Trim() == "")
                            available[j] = false;
                        else
                            Plant_units_MO_Dur[j, i] = Convert.ToInt32(temp);

                        temp = row["DurabilityHours"].ToString();
                        if (temp.Trim() == "")
                            available[j] = false;
                        else
                            Plant_units_DurabilityHour[j, i] = Convert.ToInt32(temp);

                        temp = row["FuelFactor"].ToString();
                        if (temp.Trim() == "")
                            available[j] = false;
                        else
                            Plant_units_FuelFactor[j, i] = Convert.ToInt32(temp);

                        temp = row["StartUpFactor"].ToString();
                        if (temp.Trim() == "")
                            available[j] = false;
                        else
                            Plant_units_StartUpFactor[j, i] = Convert.ToInt32(temp);

                        int max = (Plant_units_HGP_Hour[j, i] > Plant_units_MO_Hour[j, i] ? Plant_units_HGP_Hour[j, i] : Plant_units_MO_Hour[j, i]);
                        int period = (int)Math.Round((double)max / (double)Plant_units_CI_Hour[j, i]);
                        Max_Periods = (Max_Periods < period ? period : Max_Periods);

                        Plant_units_maint[j, i] = period;

                    }
                }
            }
            return available;
        }
       
        private double[, , , ,] FillPlantsPrices(double[,] oneYearPrice, double[, , , ,] Price_Plant_unit, int plantIndex, int unitIndex)
        {
            int count = 1;
            int temp = 0;

            count = 1;
            for (int w = 0; w < 53; w++)
            {
                if (count == 358) { break; }

                for (int d = 0; d < 7; d++)
                {

                    if (w == 0)
                    {
                        if (temp != 0)
                        {
                            d = temp;
                        }
                        for (int h = 0; h < 24; h++)
                        {

                            Price_Plant_unit[plantIndex, unitIndex, w, d, h] = oneYearPrice[d, h];

                        }

                    }
                    else if (w != 0)
                    {


                        for (int h = 0; h < 24; h++)
                        {

                            Price_Plant_unit[plantIndex, unitIndex, w, d, h] = oneYearPrice[7 + count, h];

                        }
                        count++;
                        if (count == 358) break;

                    }

                }

            }


            ////////////////////////last week////////////////////////////////////

            for (int d = 0; d < Const_Day; d++)
            {
                for (int h = 0; h < Const_Hour; h++)
                {
                    Price_Plant_unit[plantIndex, unitIndex, 52, d, h] = oneYearPrice[358 + d, h];

                }
            }

            return Price_Plant_unit;
        }
        private double[,] hcpfandwcpf()
        {
           
            int countweek = 0;
            double[] hcpf = new double[24];
            double[,] multhcpfwcpf = new double[365, 24];
            //////////////////////////////////////

            DateTime Now = DateTime.Now;
            for (int daysbefore1 = 0; daysbefore1 < 365; daysbefore1++)
            {
                DateTime selectedDateoneyear = Now.Subtract(new TimeSpan(daysbefore1, 0, 0, 0));
                PersianDate pDateofyear = new PersianDate(selectedDateoneyear);
                string strDateofyear = pDateofyear.ToString("d");

                //PersianDate date = new PersianDate(GetNearestDatetohcpf(strDateofyear));

                int year = pDateofyear.Year;
                int day = pDateofyear.Day;

                int week = day / 7;
                if (day < 7) week = 1;
                PersianDate date = new PersianDate(GetNearestDatetohcpf(strDateofyear));
                string entityyear = GetNearestDatetowcpf(year, week);
                DataTable capdata = Utilities.GetTable("select CapacityPayment from dbo.BaseData where Date in (select max(Date) from dbo.BaseData)");
                DataTable odatawcpf = Utilities.GetTable(" select * from dbo.WCPF where Year='" + entityyear + "' and Week='" + week + "'");

                DataTable odatahcpf = Utilities.GetTable(" select * from dbo.HCPF where Date='" + date.ToString("d") + "'");

                double wcpfresult1 = 0.0;
                if (odatawcpf.Rows.Count == 0)
                {
                    wcpfresult1 = 1.0;
                }
                else
                {
                    wcpfresult1 = double.Parse(odatawcpf.Rows[0][2].ToString());
                }


                for (int i = 0; i < 24; i++)
                {
                    if (odatahcpf.Rows.Count == 0) { hcpf[i] = 1.1; }
                    else
                    {
                        hcpf[i] = double.Parse(odatahcpf.Rows[0][i + 1].ToString());
                    }
                    multhcpfwcpf[daysbefore1, i] = hcpf[i] * wcpfresult1 * double.Parse(capdata.Rows[0][0].ToString());
                }

            }
            return multhcpfwcpf;

        }
        private double[, , , ,] Fillmultweek(double[,] multipleresult1)
        {
            double[, , , ,] hwpf_Plant_unit = new double[Plants_Num, Max_UnitsNum, 53, 7, 24];

            int count = 1;
            int temp = 0;

            count = 1;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < 53; w++)
                    {
                        if (count == 358) { break; }

                        for (int d = 0; d < 7; d++)
                        {

                            if (w == 0)
                            {
                                if (temp != 0)
                                {
                                    d = temp;
                                }
                                for (int h = 0; h < 24; h++)
                                {

                                    hwpf_Plant_unit[j, i, w, d, h] = multipleresult1[d, h];

                                }

                            }
                            else if (w != 0)
                            {


                                for (int h = 0; h < 24; h++)
                                {

                                    hwpf_Plant_unit[j, i, w, d, h] = multipleresult1[7 + count, h];

                                }
                                count++;
                                if (count == 358) break;

                            }

                        }

                    }

                }
            }


            ////////////////////////last week////////////////////////////////////
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int d = 0; d < Const_Day; d++)
                    {
                        for (int h = 0; h < Const_Hour; h++)
                        {
                            hwpf_Plant_unit[j, i, 52, d, h] = multipleresult1[358 + d, h];

                        }
                    }
                }
            }

            return hwpf_Plant_unit;

        }
        private string GetNearestDatetohcpf(string nowdate)
        {


            string datelike = nowdate.Substring(4);
            string date = nowdate;

            //string strComd = "select max(Date) from dbo.HCPF " ;

            string strComd1 = "select max(Date) from dbo.HCPF where Date <='" + date + "'and Date like '" + "%" + datelike + "'";

            DataTable oDatatable = Utilities.GetTable(strComd1);
            if (oDatatable != null && oDatatable.Rows.Count > 0 && oDatatable.Rows[0][0].ToString() != "")

                return oDatatable.Rows[0][0].ToString().Trim();

            //oDatatable = utilities.GetTable(strComd);
            //if (oDatatable != null && oDatatable.Rows.Count > 0 && oDatatable.Rows[0][0].ToString() != "")

            //    return oDatatable.Rows[0][0].ToString().Trim();

            else
            {
                DataTable ifnulltale = Utilities.GetTable("select max(Date) from dbo.HCPF");

                return ifnulltale.Rows[0][0].ToString().Trim();
            }

        }
        private string GetNearestDatetowcpf(int year, int week)
        {


            string strComd = "select max(Year) from dbo.WCPF";

            string strComd1 = "select max(Year) from dbo.WCPF where Year<='" + year + "'and Week='" + week + "'";

            DataTable oDatatable = Utilities.GetTable(strComd1);
            if (oDatatable != null && oDatatable.Rows.Count > 0 && oDatatable.Rows[0][0].ToString() != "")

                return oDatatable.Rows[0][0].ToString().Trim();

            //oDatatable = utilities.GetTable(strComd);
            //if (oDatatable != null && oDatatable.Rows.Count > 0 && oDatatable.Rows[0][0].ToString() != "")

            //    return oDatatable.Rows[0][0].ToString().Trim();
            else
            {

                DataTable ifnulltale23 = Utilities.GetTable("select max(Year) from dbo.WCPF");

                return ifnulltale23.Rows[0][0].ToString().Trim();
            }
        }
        private void RunMaintenance()
        {

            int local_Plants_Num=0;
                string[] local_plant;
                string[] local_plantName;

            if (selectedPlant.ToLower().Trim() == "all")
            {
                oDataTable = Utilities.GetTable("select distinct PPID from UnitsDataMain ");
                local_Plants_Num = oDataTable.Rows.Count;
                local_plant = new string[local_Plants_Num];
                local_plantName = new string[local_Plants_Num];

                for (int il = 0; il < local_Plants_Num; il++)
                {
                    local_plant[il] = oDataTable.Rows[il][0].ToString().Trim();

                    /////////////
                    DataTable oDataTable2 = Utilities.GetTable("SELECT PPName FROM PowerPlant WHERE PPID='" + local_plant[il] + "'");
                    local_plantName[il] = oDataTable2.Rows[0][0].ToString().Trim();
                }
            }
            else
            {
                oDataTable = Utilities.GetTable("SELECT PPID FROM PowerPlant WHERE PPName='" + selectedPlant + "'");

                local_Plants_Num = 1;
                local_plant = new string[1];
                local_plantName = new string[1];
                local_plant[0] = oDataTable.Rows[0][0].ToString().Trim();
                local_plantName[0] = selectedPlant;
            }
            
            //////////////////////////////////////
            bool[] local_GeneralDataAvailable = new bool[local_Plants_Num];
            ///////////////////////////////


            int local_Max_UnitsNum = 0;
            
            int[] local_Plant_unitsNum = new int[local_Plants_Num];

            for (int re = 0; re < local_Plants_Num; re++)
            {
                oDataTable = Utilities.GetTable("select UnitCode from UnitsDataMain where PPID='" + local_plant[re] + "'");

                local_Plant_unitsNum[re] = oDataTable.Rows.Count;
                
                if (local_Max_UnitsNum <= oDataTable.Rows.Count)
                {
                    local_Max_UnitsNum = oDataTable.Rows.Count;
                }
            }

            string[,] local_Plant_UnitCode = new string[local_Plants_Num, local_Max_UnitsNum];

            for (int j = 0; j < local_Plants_Num; j++)
            {
                oDataTable = Utilities.GetTable("select *  from UnitsDataMain where  PPID='" + local_plant[j] + "'");

                for (int i = 0; i < local_Plant_unitsNum[j]; i++)
                {
                    local_Plant_UnitCode[j, i] = oDataTable.Rows[i]["UnitCode"].ToString().Trim();
                }
            }
            /////////////////////////


            //////////////////////////
            maintData = new MaintenanceOutData();
            maintData.Plants = local_plant;
            maintData.PlantsName = local_plantName;
            maintData.Plants_UnitCodes = local_Plant_UnitCode;
            maintData.Plants_UnitsNum = local_Plant_unitsNum;
            maintData.Result_Plants_UnitsNum_Week = new List<CMatrix>();

            for (int ppI = 0; ppI < local_Plants_Num; ppI++)
            {
                result = new List<CMatrix>();
                result.Add(new CMatrix(local_Plant_unitsNum[ppI], 53));
                Plants_Num = 1;
                plant = new string[1];
                plantName = new string[1];
                plant[0] = local_plant[ppI];
                plantName[0] = local_plantName[ppI];
                ///////////////////////          FOR CheckSettingAvailability!!!!!!!!!!!!
                Plant_unitsNum = new int[1];
                Plant_unitsNum[0] = local_Plant_unitsNum[ppI];
                Max_UnitsNum = local_Plant_unitsNum[ppI];

                Plant_UnitCode= new string[1,Plant_unitsNum[0]];
                for (int g = 0; g < Plant_unitsNum[0]; g++)
                    Plant_UnitCode[0,g] = local_Plant_UnitCode[ppI, g];
                /////////////////////////
                if (CheckSettingAvailability()[0])
                {

                    local_GeneralDataAvailable[ppI] = true;
                    string strCmd = "select Max (PackageCode) from UnitsDataMain ";
                    if (Plants_Num == 1)
                        strCmd += " where ppid=" + plant[0];
                    oDataTable = Utilities.GetTable(strCmd);
                    Max_Package_Num = (int)oDataTable.Rows[0][0];

                    Plant_Packages_Num = new int[Plants_Num];
                    Plant_Packages_Num[0] = Max_Package_Num;

                    RunMaintenanceForOne();
                }
                else
                    local_GeneralDataAvailable[ppI] = false;

                maintData.Result_Plants_UnitsNum_Week.Add(result[0]);
            }

            this.titleForm = " Maintenance for " + selectedPlant;
            UpdateProgressBar(progressBar1.Maximum, "Maintenance Finished for " + selectedPlant);
            maintData.AvailableData = local_GeneralDataAvailable;
            if (parentForm!=null)
                parentForm.maintData = maintData;
        }


        public List<CMatrix> RunMaintenanceForOne()
        {
            //string strCmd = "select Max (PackageCode) from UnitsDataMain ";
            //if (Plants_Num == 1)
            //    strCmd += " where ppid=" + plant[0];
            //oDataTable = utilities.GetTable(strCmd);
            //Max_Package_Num = (int)oDataTable.Rows[0][0];

            //Plant_Packages_Num = new int[Plants_Num];
            //for (int re = 0; re < Plants_Num; re++)
            //{
            //    oDataTable = utilities.GetTable("select Max(PackageCode) from UnitsDataMain where  PPID='" + plant[re] + "'");
            //    Plant_Packages_Num[re] = (int)oDataTable.Rows[0][0];
            //}

            //strCmd = "select Max (PackageCode) from UnitsDataMain ";
            //if (Plants_Num == 1)
            //    strCmd += " where ppid=" + plant[0];
            //oDataTable = utilities.GetTable(strCmd);
            //Max_Package_Num = (int)oDataTable.Rows[0][0];

            this.titleForm = " " + plantName[0]+ " Maintenance";
            UpdateProgressBar(1, "Initialize Data");
            double[] daymax7 = new double[Const_Hour * Const_Day];

            if (daymax7 != null)
                daymax7[0] = 1;


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //Unit Number
            int Max_UnitsNum = 0;

            int[] plant_unitsNum = new int[Plants_Num];

            for (int re = 0; re < Plants_Num; re++)
            {
                oDataTable = Utilities.GetTable("select UnitCode from UnitsDataMain where PPID='" + plant[re] + "'");

                Plant_unitsNum[re] = oDataTable.Rows.Count;

                if (Max_UnitsNum <= oDataTable.Rows.Count)
                {
                    Max_UnitsNum = oDataTable.Rows.Count;
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            Plant_UnitCode = new string[Plants_Num, Max_UnitsNum];

            for (int j = 0; j < Plants_Num; j++)
            {
                oDataTable = Utilities.GetTable("select UnitCode from UnitsDataMain where  PPID='" + plant[j] + "'");
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    Plant_UnitCode[j, i] = oDataTable.Rows[i][0].ToString().Trim();

                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            Plant_UnitDec = new string[Plants_Num, Max_UnitsNum, Const_DecNum];
            // 0 : UnitType
            // 1 : PackageType
            // 2 : = "Gas"
            // 3 : SecondFuel
            for (int j = 0; j < Plants_Num; j++)
            {
                oDataTable = Utilities.GetTable("select *  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype");

                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    Plant_UnitCode[j, i] = oDataTable.Rows[i]["UnitCode"].ToString().Trim();

                    for (int k = 0; k < Const_DecNum; k++)
                    {
                        switch (k)
                        {
                            case 0:
                                Plant_UnitDec[j, i, k] = oDataTable.Rows[i]["UnitType"].ToString().Trim();
                                break;
                            case 1:
                                Plant_UnitDec[j, i, k] = oDataTable.Rows[i]["PackageType"].ToString().Trim();
                                break;
                            case 2:
                                Plant_UnitDec[j, i, k] = "Gas";
                                break;
                            case 3:
                                Plant_UnitDec[j, i, k] = oDataTable.Rows[i]["SecondFuel"].ToString().Trim();
                                break;
                        }
                    }
                }
            }

            ///////////////////////////////////////////////////


            //CheckSettingAvailability();

            ///////////////////////////////////////////////////
            string pSufNow=new PersianDate(DateTime.Now).ToString("d");
            bool[,] SufficientData = new bool[Plants_Num, Max_UnitsNum];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    string strCmd = "select * from conditionUnit where" +
                            " maintenanceStartDate<='" + pSufNow+"'" +
                            " and maintenanceEndDate>='" + pSufNow + "'" +
                            " and date in " +
                            "( select max(date) from ConditionUnit where" +
                            " ppid='" + plant[j].ToString() + "'" +
                                    " and unitcode='" + Plant_UnitCode[j,i] + "'" +
                            ")";
                    if (Utilities.GetTable(strCmd).Rows.Count > 0)
                        SufficientData[j,i] = true;
                    else
                        SufficientData[j,i] = false;
                }
            }
            ///////////////////////////////////////////////////
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Plant_UnitData = new double[Plants_Num, Max_UnitsNum, Const_DataNum];
            // 0 : PackageCode
            // 1 : RampUp
            // 2 : RampDown
            // 3 : Pmin
            // 4 : pmax
            // 5 : RampStart&shut
            // 6 : MinOn
            // 7 : MinOff
            // 8 :  Am
            // 9 :  Bm
            // 10 : Cm
            // 11 : Bmain
            // 12 : Cmain
            // 13 : Dmain
            // 14 : Variable cost
            // 15 : Fixed Cost
            // 16 : Tcold
            // 17 : Thot
            // 18 : Cold start cost
            // 19 : Hot start cost
            
            for (int j = 0; j < Plants_Num; j++)
            {
                oDataTable = Utilities.GetTable("select *  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype");
                if (oDataTable.Rows.Count > 0)
                    for (int i = 0; i < Plant_unitsNum[j]; i++)
                    {
                        for (int k = 0; k < Const_DataNum; k++)
                        {


                            switch (k)
                            {
                                case 0:

                                    Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PackageCode"].ToString());
                                    break;
                                case 1:
                                    if (oDataTable.Rows[i]["RampUpRate"].ToString() != "")
                                    {
                                        Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["RampUpRate"].ToString());
                                        Plant_UnitData[j, i, k] = 60 * Plant_UnitData[j, i, k];
                                    }
                                    break;
                                case 2:
                                    Plant_UnitData[j, i, k] = Plant_UnitData[j, i, 1];
                                    break;

                                case 3:

                                    Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PMin"].ToString());
                                    break;

                                case 4:

                                    Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PMax"].ToString());
                                    break;
                                case 5:
                                    double pow = Plant_UnitData[j, i, 1];
                                    if (Plant_UnitData[j, i, 3] > pow)
                                    {
                                        Plant_UnitData[j, i, k] = Plant_UnitData[j, i, 3];
                                    }
                                    else
                                    {
                                        Plant_UnitData[j, i, k] = pow;
                                    }
                                    break;
                                case 6:
                                    if (oDataTable.Rows[i]["TUp"].ToString() != "")
                                    {
                                        Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TUp"].ToString());
                                    }
                                    else Plant_UnitData[j, i, k] = 3;

                                    break;
                                case 7:
                                    if (oDataTable.Rows[i]["TDown"].ToString() != "")
                                    {
                                        Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TDown"].ToString());

                                    }
                                    else Plant_UnitData[j, i, k] = 10;

                                    break;

                                case 8:
                                    if (oDataTable.Rows[i]["PrimaryFuelAmargin"].ToString() != "")
                                    {
                                        Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PrimaryFuelAmargin"].ToString());
                                    }
                                    break;
                                case 9:
                                    if (oDataTable.Rows[i]["PrimaryFuelBmargin"].ToString() != "")
                                    {
                                        Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PrimaryFuelBmargin"].ToString());
                                    }
                                    break;
                                case 10:
                                    if (oDataTable.Rows[i]["PrimaryFuelCmargin"].ToString() != "")
                                    {
                                        Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PrimaryFuelCmargin"].ToString());
                                    }
                                    break;
                                case 11:
                                    if (oDataTable.Rows[i]["BMaintenance"].ToString() != "")
                                    {
                                        Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["BMaintenance"].ToString());
                                    }
                                    break;

                                case 12:
                                    if (oDataTable.Rows[i]["CMaintenance"].ToString() != "")
                                    {
                                        Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["CMaintenance"].ToString());
                                    }
                                    break;
                                case 13:
                                    Plant_UnitData[j, i, k] = Plant_UnitData[j, i, 12];

                                    break;
                                case 14:

                                    if (oDataTable.Rows[i]["VariableCost"].ToString() != "")
                                    {
                                        Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["VariableCost"].ToString());
                                    }
                                    break;
                                case 15:
                                    if (oDataTable.Rows[i]["FixedCost"].ToString() != "")
                                    {
                                        Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["FixedCost"].ToString());
                                    }
                                    break;

                                case 16:
                                    if (oDataTable.Rows[i]["TStartCold"].ToString() != "")
                                    {
                                        Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TStartCold"].ToString());
                                    }
                                    break;
                                case 17:

                                    if (oDataTable.Rows[i]["TStartHot"].ToString() != "")
                                    {
                                        Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TStartHot"].ToString());
                                    }

                                    break;
                                case 18:
                                    if (oDataTable.Rows[i]["StartUpCostA"].ToString() != "")
                                    {
                                        Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["StartUpCostA"].ToString());
                                    }


                                    break;

                                case 19:
                                    if (oDataTable.Rows[i]["StartUpCostB"].ToString() != "")
                                    {
                                        Plant_UnitData[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["StartUpCostB"].ToString());
                                    }

                                    break;
                            }
                        }

                    }

            }

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int k = 0; k < Const_DataNum; k++)
                    {
                        if (Plant_UnitData[j, i, 1] > (Plant_UnitData[j, i, 4] - Plant_UnitData[j, i, 3]))
                        {
                            Plant_UnitData[j, i, 1] = (Plant_UnitData[j, i, 4] - Plant_UnitData[j, i, 3]);
                            Plant_UnitData[j, i, 2] = (Plant_UnitData[j, i, 4] - Plant_UnitData[j, i, 3]);
                        }
                    }
                }
            }

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int k = 0; k < Const_DataNum; k++)
                    {
                        if (Plant_UnitData[j, i, 5] > (Plant_UnitData[j, i, 4]))
                        {
                            Plant_UnitData[j, i, 5] = (Plant_UnitData[j, i, 4]);
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    Plant_UnitData[j, i, 0] = Plant_UnitData[j, i, 0] - 1;
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,] Plant_PackageType = new string[Plants_Num, Max_Package_Num];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    string cmd = "select PackageType from dbo.UnitsDataMain where PPID=" + plant[j] +
                    " AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%')AND(UnitCode not like 'Gas cc%')))";
                    oDataTable = Utilities.GetTable(cmd);
                    Plant_PackageType[j, k] = oDataTable.Rows[k][0].ToString().Trim();
                }
            }
            double[,] multipleresult = hcpfandwcpf();
            double[, , , ,] multweekdayresult = Fillmultweek(multipleresult);


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            ///**************************************************************************************
            ///*************************************************************************************
            // >>>>>>>>>>>>>>>>>>> HAMIDI:::  Period has to be calculated for each unit based on date??????????????!!!!!!!!!!
                        double[,] Operation_hours_Unit = new double[Plants_Num, Max_UnitsNum];
            Random rd = new Random();  ///// HAMIDI: Operatin_hours_Unit has to be calculated for each unit

            for (int j = 0; j < Plants_Num; j++)
            {
                /////// for finding Indez Priority
                List<string> tempPackageTypes = new List<string>();
                for (int h = 0; h < Plant_Packages_Num[j]; h++)
                    if (!tempPackageTypes.Contains(Plant_PackageType[j, h]))
                        tempPackageTypes.Add(Plant_PackageType[j, h]);

                List<string> orderedPackages = new List<string>();
                foreach (string typeName in Enum.GetNames(typeof(PackageTypePriority)))
                {
                    //////////// have to make sure later whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                    if (tempPackageTypes.Contains(typeName))
                        orderedPackages.Add(typeName);
                }
                /////// for finding Indez Priority

                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {

                    // HAMIDI: has to make blockname
                    string packageType = Plant_UnitDec[j, i, 1];
                    string unitCode = Plant_UnitCode[j, i];
                    string packagecode = Plant_UnitData[j, i, 0].ToString();
                    /////////////////////////
                    double fuelfactor = 1;
                    string strCmd = "select secondFuelStartDate,secondFuelEndDate" +
                    " from conditionUnit where" +
                            " secondFuel=1" +
                            " and date in " +
                            "( select max(date) from ConditionUnit where" +
                            " ppid='" + plant[j].ToString() + "'" +
                            " and unitcode='" + unitCode + "'" +
                            " And PackageType='" + packageType + "'" +
                            ")";
                    DataTable dtConditionUnit = Utilities.GetTable(strCmd);
                    if (dtConditionUnit.Rows.Count > 0)
                    {
                        fuelfactor = Plant_units_FuelFactor[j, i];
                    }
                    //////////////////////////
                    string temp = unitCode.ToLower();
                    int packagePriorityIndex = orderedPackages.IndexOf(packageType);
                    string ppidWithPriority = (Convert.ToInt32(plant[j]) + packagePriorityIndex).ToString();

                    if (packageType.Contains("CC"))
                    {
                       // temp = ppidWithPriority + "-" + "C" + packagecode;
                        temp = temp.Replace("cc", "c");
                        string[] sp = temp.Split('c');
                        temp = sp[0].Trim() + sp[1].Trim();
                        if (temp.Contains("gas"))
                        {
                            temp = temp.Replace("gas", "G");

                        }
                        else
                        {
                            temp = temp.Replace("steam", "S");

                        }
                        temp = ppidWithPriority + "-" + temp;
                    }
                    else if (temp.Contains("gas"))
                    {
                        temp = ppidWithPriority + "-" + temp.Replace("gas", "G");
                    }
                    else
                    {
                        temp = ppidWithPriority + "-" + temp.Replace("steam", "S");
                    }
                    string blockM005 = temp.Trim();

                    //!!!!!!!!!!!!! HAMIDI SHould be UNcommented
                    DateTime today = DateTime.Now;

                    //PersianDate ptoday = new PersianDate(1389, 4, 25);
                    //DateTime today = PersianDateConverter.ToGregorianDateTime(ptoday);
                    PersianDate pNearestDate = new PersianDate(GetNearestDate(plant[j], Plant_UnitCode[j, i]/*, Plant_UnitDec[j, i, 1]*/));
                    DateTime dDate = PersianDateConverter.ToGregorianDateTime(pNearestDate);
                    //new double Operation_hours_UnitDouble[, ] = new double[,];

                    ///?????????????????????????????????????????????????
                    do
                    {
                        PersianDate pdate = PersianDateConverter.ToPersianDate(dDate);
                        strCmd = "select * from DetailFRM005 where " +
                            " ppid='" + plant[j].ToString() + "'" +
                            " and Block='" + blockM005 + "'" +
                            " And TargetMarketDate='" +pdate.ToString("d") /*pNearestDate.ToString("d")*/ + "'" +
                            " and Required!=0" +
                            " order by hour";
                        DataTable dtR = Utilities.GetTable(strCmd);
                        Operation_hours_Unit[j, i] += dtR.Rows.Count * fuelfactor;
                        ////////////////////
                        int requiredChange = 0;
                        if (dtR.Rows.Count > 1)
                            for (int m = 1; m < dtR.Rows.Count; m++)
                            {
                                if (dtR.Rows[m]["Required"].ToString() != dtR.Rows[m - 1]["Required"].ToString())
                                {
                                    requiredChange++;
                                }
                            }
                        Operation_hours_Unit[j, i] += Plant_units_StartUpFactor[j, i] * requiredChange;
                        dDate = dDate.AddDays(1);
                    } while (dDate.ToString("d") != today.ToString("d"));

                    Operation_hours_Unit[j, i] = Operation_hours_Unit[j, i] % Plant_units_MO_Hour[j, i] + Plant_units_DurabilityHour[j, i];
                    Operation_hours_Unit[j, i] = Math.Round(Operation_hours_Unit[j, i]);
                    ///?????????????????????????????????????????????????
                }
            }
            ////////////////////////
            string[, ,] lastmaintenance = new string[Plants_Num, Max_UnitsNum, 2];

          
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    if (SufficientData[j, i])
                    {
                        lastmaintenance[j, i, 0] = Plant_units_DurabilityHour[j, i].ToString();
                    }
                    else
                    {
                        oDataTable = Utilities.GetTable("select LastMaintHour,LastMaintType from dbo.MaintenanceGeneralData where PPID='" + plant[j] + "' and unitCode='" + Plant_UnitCode[j, i] + "' AND LastMaintType!='MO'");
                        if (oDataTable.Rows.Count > 0)
                        {
                            lastmaintenance[j, i, 0] = oDataTable.Rows[0][0].ToString();
                            lastmaintenance[j, i, 1] = oDataTable.Rows[0][1].ToString();
                        }
                        else
                            lastmaintenance[j, i, 0] = "0";
                    }
                }
            }
            int[,] typemaintdays = daysdiff();

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    if (typemaintdays[j, i] >= 1)
                    {
                        lastmaintenance[j, i, 0] = Operation_hours_Unit[j, i].ToString().Trim();
                    }
                }
            }


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[, ,] Maintenance_End_Unit = new int[Plants_Num, Max_UnitsNum, Max_Periods];
            int[, ,] Maintenance_Start_Unit = new int[Plants_Num, Max_UnitsNum, Max_Periods];
            double[, ,] Maintenance_Start_invers = new double[Plants_Num, Max_UnitsNum, Max_Periods];
            double[, ,] Maintenance_End_invers = new double[Plants_Num, Max_UnitsNum, Max_Periods];
            int count = 0;
            int temphour1 = 0;
            int temphour2 = 0;
            int difftemp1 = 0;
            int difftemp2 = 0;
            int globaldiff = 0;
            bool enterance = false;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    difftemp1 = 0;
                    difftemp2 = 0;
                    globaldiff = 0;
                    count = 0;
                    enterance = false;

                    for (int n = 0; n < Plant_units_maint[j, i]; n++)
                    {


                        if (int.Parse(lastmaintenance[j, i, 0]) <= Convert.ToInt32(0.9 * Plant_units_CI_Hour[j, i] * (n + 1)) && int.Parse(lastmaintenance[j, i, 0]) >= (Plant_units_CI_Hour[j, i] * (n + 1)))
                        {

                            Maintenance_End_Unit[j, i, n] = Plant_units_CI_Hour[j, i] * (n + 1);
                            Maintenance_Start_Unit[j, i, n] = Convert.ToInt32(0.9 * Maintenance_End_Unit[j, i, n]);
                            enterance = true;
                        }


                        else if (int.Parse(lastmaintenance[j, i, 0]) == Plant_units_CI_Hour[j, i] * (n + 1) || lastmaintenance[j, i, 0] == "0")
                        {
                            Maintenance_End_Unit[j, i, n] = Plant_units_CI_Hour[j, i] * (n + 1);
                            Maintenance_Start_Unit[j, i, n] = Convert.ToInt32(0.9 * Maintenance_End_Unit[j, i, n]);
                            enterance = true;

                        }

                        else if (Plant_units_CI_Hour[j, i] * (n + 1) < int.Parse(lastmaintenance[j, i, 0]))
                        {

                            Maintenance_End_Unit[j, i, n] = Plant_units_CI_Hour[j, i] * (n + 1);
                            Maintenance_Start_Unit[j, i, n] = Convert.ToInt32(0.9 * Maintenance_End_Unit[j, i, n]);

                        }
                        else if (int.Parse(lastmaintenance[j, i, 0]) < Plant_units_CI_Hour[j, i] * (n + 1) && int.Parse(lastmaintenance[j, i, 0]) != 0 && enterance == false)
                        {
                            if (count == 0)
                            {
                                temphour1 = Plant_units_CI_Hour[j, i] * ((n - 1) + 1);
                                temphour2 = Plant_units_CI_Hour[j, i] * (n + 1);
                                difftemp1 = (int.Parse(lastmaintenance[j, i, 0]) - temphour1);
                                difftemp2 = (int.Parse(lastmaintenance[j, i, 0]) - temphour2);
                                if (Math.Abs(difftemp1) < Math.Abs(difftemp2))
                                {
                                    Maintenance_End_Unit[j, i, (n - 1)] = temphour1 + difftemp1;
                                    Maintenance_Start_Unit[j, i, (n - 1)] = Convert.ToInt32(0.9 * Maintenance_End_Unit[j, i, (n - 1)]);
                                    globaldiff = difftemp1;
                                    Maintenance_End_Unit[j, i, n] = Plant_units_CI_Hour[j, i] * (n + 1)  + globaldiff;
                                }
                                else if (Math.Abs(difftemp1) > Math.Abs(difftemp2))
                                {
                                    Maintenance_End_Unit[j, i, n] = temphour2 + difftemp2;
                                    globaldiff = difftemp2;
                                }
                                else if (Math.Abs(difftemp2) == Math.Abs(difftemp1))
                                {

                                    Maintenance_End_Unit[j, i, n] = temphour2 + difftemp2;
                                    globaldiff = difftemp2;
                                }

                                // Maintenance_End_Unit[j, i, (n - 1)] = temphour1 + difftemp;

                                // Maintenance_End_Unit[j, i, n] = (Plant_units_CI_Hour[j, i] * (n + 1)) + difftemp;
                                Maintenance_Start_Unit[j, i, n] = Convert.ToInt32(0.9 * Maintenance_End_Unit[j, i, n]);

                            }


                            if (count == 1)
                            {
                                Maintenance_End_Unit[j, i, n] = Plant_units_CI_Hour[j, i] * (n + 1) + globaldiff;
                                Maintenance_Start_Unit[j, i, n] = Convert.ToInt32(0.9 * Maintenance_End_Unit[j, i, n]);
                            }



                            count = 1;
                        }
                        else
                        {
                            Maintenance_End_Unit[j, i, n] = Plant_units_CI_Hour[j, i] * (n + 1);
                            Maintenance_Start_Unit[j, i, n] = Convert.ToInt32(0.9 * Maintenance_End_Unit[j, i, n]);

                        }

                        Maintenance_Start_invers[j, i, n] = Math.Pow(Maintenance_Start_Unit[j, i, n], -1);
                        Maintenance_End_invers[j, i, n] = Math.Pow(Maintenance_End_Unit[j, i, n], -1);

                    }
                }
            }


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Type_Maint_Unit = new int[Plants_Num, Max_UnitsNum];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    Type_Maint_Unit[j, i] = 0;
                    for (int n = 0; n < Plant_units_maint[j, i]; n++)
                    {
                        if ((Operation_hours_Unit[j, i] >= Maintenance_End_Unit[j, i, n]) && (typemaintdays[j, i] < 1))
                        {
                            Type_Maint_Unit[j, i] = n + 1;
                        }
                        else if ((Operation_hours_Unit[j, i] >= Maintenance_End_Unit[j, i, n]) && (typemaintdays[j, i] >= 1))
                        {
                            Type_Maint_Unit[j, i] = n;
                        }

                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[, ,] Maintenance_End_Unit_match = new int[Plants_Num, Max_UnitsNum, Max_Periods];
         
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int n = 0; n < Plant_units_maint[j, i]; n++)
                    {
                      Maintenance_End_Unit_match[j, i, n] = Plant_units_CI_Hour[j, i] * (n + 1);
                    }
                }
            }

            //////////////////////////////////////////////////////////
            int[, ,] Duration_main_Unit = new int[Plants_Num, Max_UnitsNum, Max_Periods];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int n = 0; n < Plant_units_maint[j, i]; n++)
                    {
                        //Duration_main_Unit[j, i, n] = 1 * (n + 1); // HAMIDI!!!!!!!!!
                        //  ??????????????????????????????????????
                        Duration_main_Unit[j, i, n] = Plant_units_CI_Dur[j, i];

                        if (Maintenance_End_Unit_match[j, i, n] == Plant_units_HGP_Hour[j, i])
                            Duration_main_Unit[j, i, n] = Plant_units_HGP_Dur[j, i];
                        else if (Maintenance_End_Unit_match[j, i, n] == Plant_units_MO_Hour[j, i])
                            Duration_main_Unit[j, i, n] = Plant_units_MO_Dur[j, i];

                        //Duration_main_Unit[j, i, n] = (int)Math.Round((double)Duration_main_Unit[j, i, n] / (double)7);
                    }
                }
            }

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int n = 0; n < Plant_units_maint[j, i]; n++)
                    {
                        Duration_main_Unit[j, i, n] = (int)Math.Round((double)Duration_main_Unit[j, i, n] / (double)7);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            int[,] Duration_main_Sum = new int[Plants_Num, Max_UnitsNum];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    Duration_main_Sum[j, i] = 0;
                    for (int n = 0; n < (Type_Maint_Unit[j, i]); n++)
                    {
                        Duration_main_Sum[j, i] = Duration_main_Sum[j, i] + Duration_main_Unit[j, i, n];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[, ,] Operatin_Factor_Unit_Week = new int[Plants_Num, Max_UnitsNum, 53];
            double[,] maxturnon = maxoneyear();
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < 53; w++)
                    {
                        if (Plant_UnitDec[j, i, 0] == "Steam")
                        {
                            if (maxturnon[j, w] > 0.7 * 168)
                                Operatin_Factor_Unit_Week[j, i, w] = Convert.ToInt32(maxturnon[j, w]);
                            else Operatin_Factor_Unit_Week[j, i, w] = 117;
                        }
                        else if (Plant_UnitDec[j, i, 0] == "Gas")
                        {

                            if (maxturnon[j, w] > 0.5 * 168)
                                Operatin_Factor_Unit_Week[j, i, w] = Convert.ToInt32(maxturnon[j, w]);
                            else Operatin_Factor_Unit_Week[j, i, w] = 84;


                        }


                    }
                }
            }
          
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            int[,] History_Maint_Unit = new int[Plants_Num, Max_UnitsNum];

            //!!!!!!!!!!!!! HAMIDI SHould be UNcommented
            DateTime gToday = DateTime.Now;
            //PersianDate test2 = new PersianDate(1389, 4, 25);
            //DateTime gToday = PersianDateConverter.ToGregorianDateTime(test2);

            PersianDate pToday = new PersianDate(gToday);

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {

                    string strCmd = "SELECT * FROM ConditionUnit" +
                        " WHERE UnitCode='" + Plant_UnitCode[j, i] + "'" +
                    " AND PackageType='" + Plant_UnitDec[j, i, 1] + "'" +
                    " AND PPID='" + plant[j] + "'" +
                     " AND MaintenanceEndDate>'" + pToday.ToString("d") + "'" +
                     " Order by Date DESC";

                    DataTable oDataTable = Utilities.GetTable(strCmd);

                    if (oDataTable != null && oDataTable.Rows.Count > 0)
                    {
                        DataRow row = oDataTable.Rows[0];
                        if (row["Maintenance"] != null && bool.Parse(row["Maintenance"].ToString()))
                        {
                            DateTime date = PersianDateConverter.ToGregorianDateTime(row["MaintenanceStartDate"].ToString());
                            int daysDifference = (new DateTime(gToday.Year, gToday.Month, gToday.Day, 0, 0, 0) - new DateTime(date.Year, date.Month, date.Day, 0, 0, 0)).Days;
                            History_Maint_Unit[j, i] = (int)Math.Round((double)daysDifference / 7);
                        }
                    }
                    History_Maint_Unit[j, i] = History_Maint_Unit[j, i] + Duration_main_Sum[j, i];
                }
            }
            ////////////////////
            //for (int j = 0; j < Plants_Num; j++)
            //{
            //    for (int i = 0; i < Plant_unitsNum[j]; i++)
            //    {
            //        if (History_Maint_Unit[j, i] > (Duration_main_Unit[j, i, Type_Maint_Unit[j, i]] + Duration_main_Sum[j, i]))
            //        {
            //            History_Maint_Unit[j, i] = (Duration_main_Unit[j, i, Type_Maint_Unit[j, i]] + Duration_main_Sum[j, i]);
            //        }
            //    }
            //}
            ////////////////////////////////
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    if (History_Maint_Unit[j, i] > Duration_main_Sum[j, i] + Duration_main_Unit[j, i, Type_Maint_Unit[j,i]])
                    {
                        History_Maint_Unit[j, i] = Duration_main_Sum[j, i] + Duration_main_Unit[j, i, Type_Maint_Unit[j, i]];
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] X_history_Plant_unit = new int[Plants_Num, Max_UnitsNum];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    if (History_Maint_Unit[j, i] > Duration_main_Sum[j, i])
                        X_history_Plant_unit[j, i] = 1;
                    else
                        X_history_Plant_unit[j, i] = 0;
                }
            }
            UpdateProgressBar(2, "Getting Price Data");
            //double[, , ] Export = new double[Plants_Num, Max_UnitsNum, Const_Week];
            //Export = new double[Plants_Num, Max_UnitsNum, Const_Week];

            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------
            //           Price        //
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------


            /////////mainform////////////
            ////////// calculate win price of one year before today ( CalculatePowerPlantMaxBid)
            //CMatrix price = s.Get365Day_M002(ppId, dsUnits, mixUnitStatus);
            double[, , , ,] Price_Plant_unit = new double[Plants_Num, Max_UnitsNum, 53, 7, 24];

            double[,] Price_Plant = new double[Plants_Num, 24 * Const_daysInYear];

            //!!!!!!!!!!!!! HAMIDI SHould be UNcommented
           DateTime Now = DateTime.Now;
            //PersianDate test1 = new PersianDate(1389, 4, 25);
            //DateTime Now = PersianDateConverter.ToGregorianDateTime(test1);

            //double interval = intervalprice(new PersianDate(DateTime.Now).ToString("d"), new PersianDate(DateTime.Now.AddYears(-1)).ToString("d"));
            for (int j = 0; j < Plants_Num; j++)
            {
                DataSet myDs = UtilityPowerPlantFunctions.GetUnits(int.Parse(plant[j]));

                DataTable[] orderedPackages = UtilityPowerPlantFunctions.OrderPackages(myDs);


                foreach (DataTable dtPackageType in orderedPackages)
                {

                    double[,] oneYearPrice = new double[Const_daysInYear, 24];

                    for (int daysbefore = 0; daysbefore < Const_daysInYear; daysbefore++)
                    {
                        DateTime selectedDate = Now.Subtract(new TimeSpan(daysbefore, 0, 0, 0));

                        int packageOrder = 0;
                        if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                        orderedPackages.Length > 1)
                            packageOrder = 1;

                        CMatrix oneDayPrice = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid
                               (Convert.ToInt32(plant[j]), dtPackageType, selectedDate, packageOrder, new PersianDate(DateTime.Now.AddYears(1)).ToString("d"));

                        for (int k = 0; k < 24; k++)
                            oneYearPrice[daysbefore, k] = oneDayPrice[0, k];
                    }

                    string packageTypeName = dtPackageType.TableName;
                    for (int i = 0; i < Plant_unitsNum[j]; i++)
                    {
                        if (Plant_UnitDec[j, i, 1] == packageTypeName)
                        {
                            Price_Plant_unit = FillPlantsPrices(oneYearPrice, Price_Plant_unit, j, i);
                        }
                    }

                }
            }
             for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {

                    for (int w = 0; w < 53; w++)
                    {
                        for (int d = 0; d < 7; d++)
                        {
                            for (int h = 0; h < 24; h++)
                            {

                                Price_Plant_unit[j, i, w, d, h] = Price_Plant_unit[j, i, w, d, h] + multweekdayresult[j, i, w, d, h];

                            }
                        }
                    }
                }
            }

            ///////////////////////////////////////////////////////////////////////////

            //double[, , ,] O_Week = new double[Plants_Num, Max_UnitsNum, Const_Week, Max_Periods+1];
            double[, ,] O_Week = new double[Plants_Num, Max_UnitsNum, Const_Week];

            //*******************************************************************************************************
            UpdateProgressBar(3, "Initializing Cplex");
            //try
            //{
            Cplex cplex = new Cplex();

            UpdateProgressBar(4, "Processing");

            IIntVar[, , , ,] X_Plant_unit = new IIntVar[Plants_Num, Max_UnitsNum, Const_Week, Const_Day, Const_Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        for (int d = 0; d < Const_Day; d++)
                        {
                            for (int h = 0; h < Const_Hour; h++)
                            {
                                X_Plant_unit[j, i, w, d, h] = cplex.IntVar(0, 1);
                            }
                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            IIntVar[, , , ,] V_Plant_unit = new IIntVar[Plants_Num, Max_UnitsNum, Const_Week, Const_Day, Const_Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        for (int d = 0; d < Const_Day; d++)
                        {
                            for (int h = 0; h < Const_Hour; h++)
                            {
                                V_Plant_unit[j, i, w, d, h] = cplex.IntVar(0, 1);
                            }
                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            IIntVar[, ,] X_week_Plant_unit = new IIntVar[Plants_Num, Max_UnitsNum, Const_Week];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        X_week_Plant_unit[j, i, w] = cplex.IntVar(0, 1);
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            IIntVar[, ,] Vx_week_Plant_unit = new IIntVar[Plants_Num, Max_UnitsNum, Const_Week];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        Vx_week_Plant_unit[j, i, w] = cplex.IntVar(0, 1);
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            IIntVar[, ,] X_Sum_Plant_unit = new IIntVar[Plants_Num, Max_UnitsNum, Const_Week];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        X_Sum_Plant_unit[j, i, w] = cplex.IntVar(0, 90000);
                    }
                }
            }
            int _Sum_X = 1;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        ILinearIntExpr Sum_xx = cplex.LinearIntExpr();
                        for (int ww = 0; ww < (w + 1); ww++)
                        {
                            Sum_xx.AddTerm(_Sum_X, X_week_Plant_unit[j, i, ww]);
                        }
                        cplex.AddEq(X_Sum_Plant_unit[j, i, w], Sum_xx);
                    }
                }
            };
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            UpdateProgressBar(5, "Processing");

            IIntVar[, ,] V_week_Plant_unit = new IIntVar[Plants_Num, Max_UnitsNum, Const_Week];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        V_week_Plant_unit[j, i, w] = cplex.IntVar(0, 168);
                    }
                }
            }
            int _Sum_V = 1;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        ILinearIntExpr Sum_V = cplex.LinearIntExpr();
                        for (int d = 0; d < Const_Day; d++)
                        {
                            for (int h = 0; h < Const_Hour; h++)
                            {
                                Sum_V.AddTerm(_Sum_V, V_Plant_unit[j, i, w, d, h]);
                            }
                        }
                        cplex.AddEq(V_week_Plant_unit[j, i, w], Sum_V);
                    }
                }
            };
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            IIntVar[, ,] Xw_Plant_unit = new IIntVar[Plants_Num, Max_UnitsNum, Const_Week];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        Xw_Plant_unit[j, i, w] = cplex.IntVar(0, 168);
                    }
                }
            }
            int _Sum_xw = 1;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        ILinearIntExpr Sum_xw = cplex.LinearIntExpr();
                        for (int ww = 0; ww < (w + 1); ww++)
                        {
                            Sum_xw.AddTerm(_Sum_xw, X_week_Plant_unit[j, i, ww]);
                        }
                        cplex.AddEq(Xw_Plant_unit[j, i, w], Sum_xw);
                    }
                }
            };

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            IIntVar[, ,] V_Sum_Plant_unit = new IIntVar[Plants_Num, Max_UnitsNum, Const_Week];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        V_Sum_Plant_unit[j, i, w] = cplex.IntVar(0, 90000);
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        ILinearIntExpr Sum_vv = cplex.LinearIntExpr();
                        for (int ww = 0; ww < (w + 1); ww++)
                        {
                            Sum_vv.AddTerm(_Sum_V, V_week_Plant_unit[j, i, ww]);
                        }
                        cplex.AddEq(V_Sum_Plant_unit[j, i, w], cplex.Sum(Sum_vv, Operation_hours_Unit[j, i]));
                    }
                }
            };
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            INumVar[, , , ,] Income_hour_unit = new INumVar[Plants_Num, Max_UnitsNum, Const_Week, Const_Day, Const_Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        for (int d = 0; d < Const_Day; d++)
                        {
                            for (int h = 0; h < Const_Hour; h++)
                            {
                                Income_hour_unit[j, i, w, d, h] = cplex.NumVar(0.0, double.MaxValue);
                                cplex.AddEq(Income_hour_unit[j, i, w, d, h], cplex.Prod(Price_Plant_unit[j, i, w, d, h], V_Plant_unit[j, i, w, d, h]));
                            }
                        }
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            UpdateProgressBar(6, "Processing");

            ILinearNumExpr Income_unit = cplex.LinearNumExpr();
            double _Sum_income = 1;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        for (int d = 0; d < Const_Day; d++)
                        {
                            for (int h = 0; h < Const_Hour; h++)
                            {
                                Income_unit.AddTerm(_Sum_income, Income_hour_unit[j, i, w, d, h]);
                            }
                        }
                    }
                }
            };

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        for (int d = 0; d < Const_Day; d++)
                        {
                            for (int h = 0; h < Const_Hour; h++)
                            {
                                cplex.AddLe(cplex.Sum(V_Plant_unit[j, i, w, d, h], X_Plant_unit[j, i, w, d, h]), 1);
                            }
                        }
                    }
                }
            };

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        for (int d = 0; d < Const_Day; d++)
                        {
                            for (int h = 0; h < Const_Hour; h++)
                            {
                                cplex.AddGe(cplex.Diff(Vx_week_Plant_unit[j, i, w], V_Plant_unit[j, i, w, d, h]), 0);
                            }
                        }
                    }
                }
            };

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {

                        cplex.AddLe(cplex.Sum(Vx_week_Plant_unit[j, i, w], X_week_Plant_unit[j, i, w]), 1);

                    }
                }
            };
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        cplex.AddLe(V_week_Plant_unit[j, i, w], cplex.Prod(Operatin_Factor_Unit_Week[j, i, w], cplex.Diff(1, X_week_Plant_unit[j, i, w])));
                    }
                }
            };

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            IIntVar[, , , ,] U_Plant_unit = new IIntVar[Plants_Num, Max_UnitsNum, Const_Week, Max_Periods, 2];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        for (int n = 0; n < Plant_units_maint[j, i]; n++)
                        {
                            for (int bn = 0; bn < 2; bn++)
                            {
                                U_Plant_unit[j, i, w, n, bn] = cplex.IntVar(0, 1);
                            }
                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            IIntVar[, , ,] Z_Plant_unit = new IIntVar[Plants_Num, Max_UnitsNum, Const_Week, Max_Periods];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        for (int n = 0; n < Plant_units_maint[j, i]; n++)
                        {
                            Z_Plant_unit[j, i, w, n] = cplex.IntVar(0, 1);
                        }
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        for (int n = 0; n < Plant_units_maint[j, i]; n++)
                        {
                            cplex.AddLe(U_Plant_unit[j, i, w, n, 0], cplex.Prod(V_Sum_Plant_unit[j, i, w], Maintenance_Start_invers[j, i, n]));
                            cplex.AddGe(U_Plant_unit[j, i, w, n, 0], cplex.Prod(cplex.Diff(V_Sum_Plant_unit[j, i, w], Maintenance_Start_Unit[j, i, n]), (Math.Pow(1000000, -1))));
                            cplex.AddLe(U_Plant_unit[j, i, w, n, 1], cplex.Prod(V_Sum_Plant_unit[j, i, w], Maintenance_End_invers[j, i, n]));
                            cplex.AddGe(U_Plant_unit[j, i, w, n, 1], cplex.Prod(cplex.Diff(V_Sum_Plant_unit[j, i, w], Maintenance_End_Unit[j, i, n]), (Math.Pow(1000000, -1))));
                            cplex.AddEq(Z_Plant_unit[j, i, w, n], cplex.Diff(U_Plant_unit[j, i, w, n, 0], U_Plant_unit[j, i, w, n, 1]));
                        }
                    }
                }
            };
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            IIntVar[, ,] Z_Maint_unit = new IIntVar[Plants_Num, Max_UnitsNum, Const_Week];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        Z_Maint_unit[j, i, w] = cplex.IntVar(0, 1); ;
                    }
                }
            }
            int _Z = 1;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        ILinearIntExpr Z_unit = cplex.LinearIntExpr();
                        for (int n = 0; n < Plant_units_maint[j, i]; n++)
                        {
                            Z_unit.AddTerm(_Z, Z_Plant_unit[j, i, w, n]);
                        }
                        cplex.AddEq(Z_Maint_unit[j, i, w], Z_unit);
                        cplex.AddLe(X_week_Plant_unit[j, i, w], Z_unit);
                    }
                }
            };
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            UpdateProgressBar(7, "Processing");
            IIntVar[, ,] U1_Sum_Plant_unit = new IIntVar[Plants_Num, Max_UnitsNum, Const_Week];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        U1_Sum_Plant_unit[j, i, w] = cplex.IntVar(0, 168);
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        ILinearIntExpr Sum_u1 = cplex.LinearIntExpr();
                        for (int n = 0; n < Plant_units_maint[j, i]; n++)
                        {
                            Sum_u1.AddTerm(Duration_main_Unit[j, i, n], U_Plant_unit[j, i, w, n, 0]);
                        }
                        cplex.AddEq(U1_Sum_Plant_unit[j, i, w], Sum_u1);
                    }
                }
            };
            ////^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            IIntVar[, ,] U2_Sum_Plant_unit = new IIntVar[Plants_Num, Max_UnitsNum, Const_Week];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        U2_Sum_Plant_unit[j, i, w] = cplex.IntVar(0, 168);
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        ILinearIntExpr Sum_u2 = cplex.LinearIntExpr();
                        for (int n = 0; n < Plant_units_maint[j, i]; n++)
                        {
                            Sum_u2.AddTerm(Duration_main_Unit[j, i, n], U_Plant_unit[j, i, w, n, 1]);
                        }
                        cplex.AddEq(U2_Sum_Plant_unit[j, i, w], Sum_u2);
                    }
                }
            };
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            //define Equation_(h):Minupdown_1
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    if (typemaintdays[j, i] >= 1)
                    {
                        for (int w = 0; w < Math.Abs((Duration_main_Sum[j, i] + Duration_main_Unit[j, i, Type_Maint_Unit[j, i]] - History_Maint_Unit[j, i])); w++)
                        {
                            cplex.AddEq(X_week_Plant_unit[j, i, w], 1);
                        }
                        if (typemaintdays[j, i] < 1)
                        {
                            for (int w = 0; w < Math.Abs((Duration_main_Sum[j, i] - History_Maint_Unit[j, i])); w++)
                            {
                                cplex.AddEq(X_week_Plant_unit[j, i, w], 1);
                            }
                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            //define Equation_(h):Minupdown_2
            double _Minup_2 = 1;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int n = 0; n < Plant_units_maint[j, i]; n++)
                    {
                        for (int w = 0; w <= (Const_Week - Duration_main_Unit[j, i, n]); w++)
                        {
                            ILinearNumExpr X_up = cplex.LinearNumExpr();
                            for (int ww = w; ww <= (w + Duration_main_Unit[j, i, n] - 1); ww++)
                            {
                                X_up.AddTerm(_Minup_2, X_week_Plant_unit[j, i, ww]);
                            }
                            if (w > 0)
                            {
                                cplex.AddGe(X_up, cplex.Sum(cplex.Prod(Duration_main_Unit[j, i, n], cplex.Diff(X_week_Plant_unit[j, i, w], X_week_Plant_unit[j, i, w - 1])), cplex.Prod(-366, cplex.Diff(1, Z_Plant_unit[j, i, w, n]))));
                            }
                            else
                            {
                                cplex.AddGe(X_up, cplex.Sum(cplex.Prod(Duration_main_Unit[j, i, n], cplex.Diff(X_week_Plant_unit[j, i, w], X_history_Plant_unit[j, i])), cplex.Prod(-366, cplex.Diff(1, Z_Plant_unit[j, i, w, n]))));
                            }
                        }
                    }
                }
            }


            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            // define Equation_(h):Minupdown_3
            double _Minup_h = 1;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int n = 0; n < Plant_units_maint[j, i]; n++)
                    {
                        for (int w = (Const_Week - (int)Duration_main_Unit[j, i, n] + 1); w < Const_Week; w++)
                        {
                            ILinearNumExpr Xh_up = cplex.LinearNumExpr();
                            for (int ww = w; ww < Const_Week; ww++)
                            {
                                Xh_up.AddTerm(_Minup_h, X_week_Plant_unit[j, i, ww]);
                            }
                            if (w > 0)
                            {
                                cplex.AddGe(cplex.Diff(Xh_up, cplex.Diff(cplex.Prod((Const_Week - w), X_week_Plant_unit[j, i, w]), cplex.Prod((Const_Week - w), X_week_Plant_unit[j, i, w - 1]))), cplex.Prod(-366, cplex.Diff(1, Z_Plant_unit[j, i, w, n])));
                            }
                            else
                            {
                                cplex.AddGe(cplex.Diff(Xh_up, cplex.Diff(cplex.Prod((Const_Week - w), X_week_Plant_unit[j, i, w]), ((Const_Week - w) * X_history_Plant_unit[j, i]))), cplex.Prod(-366, cplex.Diff(1, Z_Plant_unit[j, i, w, n])));
                            }
                        }
                    }
                }
            }


            ////////////////////////////////////////////////////////////////////////////
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_unitsNum[j]; i++)
                {
                    for (int w = 0; w < Const_Week; w++)
                    {
                        cplex.AddGe(cplex.Sum(Xw_Plant_unit[j, i, w], History_Maint_Unit[j, i]), U2_Sum_Plant_unit[j, i, w]);
                        cplex.AddLe(cplex.Sum(Xw_Plant_unit[j, i, w], History_Maint_Unit[j, i]), cplex.Diff(U1_Sum_Plant_unit[j, i, w], 0));

                    }
                }
            };


            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            cplex.AddMaximize(Income_unit);
            //*******************************************************************************************************
            //Export
            //*******************************************************************************************************
            UpdateProgressBar(8, "Solving Cplex");
            if (cplex.Solve())
            {
                // System.Windows.Forms.MessageBox.Show(" Value =   Price:   " + cplex.ObjValue + "   " + cplex.GetStatus());
                UpdateProgressBar(9, "Saving Results");
                CplexSolve = 1;
                for (int j = 0; j < Plants_Num; j++)
                {

                    for (int i = 0; i < Plant_unitsNum[j]; i++)
                    {


                        ////////////// delete from DB
                        string strDate = new PersianDate(DateTime.Now).ToString("d");
                        string cmd = "delete from MaintenanceResult where" +
                            " ppid='" + plant[j] + "'" +
                            " and packagetype='" + Plant_UnitDec[j, i, 1] + "'" +
                            " and unitCode='" + Plant_UnitCode[j, i] + "'" +
                            " and date='" + strDate + "'";

                        Utilities.ExecQuery(cmd);


                        /// save in db

                        cmd = "insert into MaintenanceResult (ppid, packagetype, unitCode, date) values " +
                            " ('" + plant[j] + "'" +
                            " ,'" + Plant_UnitDec[j, i, 1] + "'" +
                            " , '" + Plant_UnitCode[j, i] + "'" +
                            " ,'" + strDate + "'" +
                            ")";
                        Utilities.ExecQuery(cmd);
                        cmd = "select max(id) from MaintenanceResult where" +
                                " ppid='" + plant[j] + "'" +
                                " and packagetype='" + Plant_UnitDec[j, i, 1] + "'" +
                                " and unitCode='" + Plant_UnitCode[j, i] + "'" +
                                " and date='" + strDate + "'";

                        DataTable oDataTable = Utilities.GetTable(cmd);
                        string MaintenanceResultId = "";
                        if (oDataTable.Rows.Count > 0)
                        {
                            MaintenanceResultId = oDataTable.Rows[0][0].ToString();

                            for (int w = 0; w < Const_Week; w++)
                            {
                                // for (int n = 0; n < Plant_units_maint[j, i]; n++)
                                // {
                                //    O_Week[j, i, w, n ] = cplex.GetValue(U_Plant_unit[j, i, w, n, 0]);
                                //}
                                O_Week[j, i, w] = cplex.GetValue(U2_Sum_Plant_unit[j, i, w]);
                                result[j][i, w] = cplex.GetValue(X_week_Plant_unit[j, i, w]);
                                result[j][i, w] = Math.Round(result[j][i, w]);
                                ///////////Export is output of maintenence class that must be 0 or 1 and its import data for chart ////////

                                //if (Export[j, i, w] == 1)
                                //{
                                //    MessageBox.Show("Test");
                                //}

                                cmd = "insert into MaintResultWeekly (MaintenanceResultId, weekIndex, value) values (" +
                                    MaintenanceResultId + "," + w.ToString() + "," + result[j][i, w].ToString() + ")";
                                Utilities.ExecQuery(cmd);

                            }
                        }
                    }
                }
            }

            cplex.End();
            if (CplexSolve == 0)
            {
                UpdateProgressBar(progressBar1.Maximum, plantName[0] + " Program is Failed...Input Information is Confliction");
            }
            if (CplexSolve == 1)
            {
                UpdateProgressBar(progressBar1.Maximum, plantName[0] + " Maintenance Finished...");
            }
            return result;
            //maintData.Result_Plants_UnitsNum_Week = result;

            //this.parentForm.maintData = maintData;
            //}
            //catch (ILOG.CPLEX.Cplex.UnknownObjectException e1)
            //{
            //    MessageBox.Show("concert exception" + e1.ToString() + "caught");
            //}

        }

        public bool Findccplant(string ppid)
        {
            oDataTable = Utilities.GetTable("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count > 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }

        public bool Findccplant(int ppid)
        {
            return Findccplant(ppid.ToString());
        }
        
        public double[,] maxoneyear()
        {
            double[,] maxon = new double[Plants_Num, 53];
            double[, ,] onweek = new double[Plants_Num, Max_UnitsNum, 53];
            int nday = Const_daysInYear;

            //!!!!!!!!!!!!! HAMIDI SHould be UNcommented
            DateTime Now = DateTime.Now;
            //PersianDate test = new PersianDate(1389, 1, 26);
            //DateTime Now = PersianDateConverter.ToGregorianDateTime(test);

            //string[,] blocks = Getblockmaintenance(Max_Package_Num, Plant_Packages_Num);
            string[,] blocks = Getblockmaintenance_UnitBased();
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_unitsNum[j]; k++)
                {
                    int countweek = 0;
                    for (int daysbefore1 = 0; daysbefore1 < nday; daysbefore1 = daysbefore1 + 7)
                    {
                        DateTime selectedDateoneyear = Now.Subtract(new TimeSpan(daysbefore1, 0, 0, 0));
                        PersianDate pDateofyear = new PersianDate(selectedDateoneyear);
                        string strDateofyear = pDateofyear.ToString("d");
                        if (Plant_UnitDec[j, k, 1] != "CC")
                        {
                            string strCmd = "select count(*) from DetailFRM005 where " +
                                   " ppid='" + plant[j].ToString() + "'" +
                                   " and Block='" + blocks[j, k] + "'" +
                                   " And TargetMarketDate>'" + PersianDateConverter.ToPersianDate(selectedDateoneyear.Subtract(new TimeSpan(6, 0, 0, 0))).ToString("d") + "'" +
                                   " And TargetMarketDate<='" + strDateofyear + "'" +
                                   " and Required!=0";

                            int countResults = Convert.ToInt32(Utilities.GetTable(strCmd).Rows[0][0]);

                            onweek[j, k, countweek] = countResults;

                            countweek++;
                        }
                        else
                        {

                            string strCmd = "select count(*) from DetailFRM009 where " +
                                 " PPID='" + plant[j].ToString() + "'" +
                                 " and Block='" + Plant_UnitCode[j, k] + "'" +
                                 " And TargetMarketDate>'" + PersianDateConverter.ToPersianDate(selectedDateoneyear.Subtract(new TimeSpan(6, 0, 0, 0))).ToString("d") + "'" +
                                 " And TargetMarketDate<='" + strDateofyear + "'" +
                                 " and P!=0";

                            int countResults = Convert.ToInt32(Utilities.GetTable(strCmd).Rows[0][0]);

                            onweek[j, k, countweek] = countResults;

                            countweek++;
                        }
                    }
                }
            }

            double[,] sum = new double[Plants_Num, 53];
            double[,] avg = new double[Plants_Num, 53];
            double avgdiff = 0;
            double[,] sumdif = new double[Plants_Num, 53];
            double[, ,] diff = new double[Plants_Num, Max_UnitsNum, 53];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int c = 0; c < 53; c++)
                {
                    for (int k = 0; k < Plant_unitsNum[j]; k++)
                    {
                        sum[j, c] += onweek[j, k, c];
                        avg[j, c] = sum[j, c] / Max_UnitsNum;
                    }
                }
            }

            ///////////////////////////mneeeeeeeee
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int c = 0; c < 53; c++)
                {
                    for (int k = 0; k < Plant_unitsNum[j]; k++)
                    {
                        diff[j, k, c] = avg[j, c] - onweek[j, k, c];
                        diff[j, k, c] = diff[j, k, c] * diff[j, k, c];
                        sumdif[j, c] += diff[j, k, c];

                    }
                    avgdiff = sumdif[j, c] / Max_UnitsNum;
                    maxon[j, c] = Math.Sqrt(MyDoubleParse(avgdiff.ToString()));
                }
            }


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int c = 0; c < 53; c++)
                {
                    switch (maintenanceStratedgy)
                    {
                        case MaintenanceStratedgy.Low:
                            maxon[j, c] = avg[j, c] - maxon[j, c];
                            if (maxon[j, c] > 168.0) maxon[j, c] =
                                168.0;
                            break;
                        case MaintenanceStratedgy.Normal:
                            maxon[j, c] = avg[j, c];
                            break;
                        case MaintenanceStratedgy.High:
                            maxon[j, c] = avg[j, c] + maxon[j, c];
                            if (maxon[j, c] > 168.0) maxon[j, c] =
                                168.0;
                            break;
                    }
                }
            }
            return maxon;


        }
//        private string[,] Getblockmaintenance(int Package_Num, int[] Plant_Packages_Num)
//        {

//            string[,] blocks = new string[Plants_Num, Package_Num];

//            for (int j = 0; j < Plants_Num; j++)
//            {
//                //  int x=int.Parse(plant[j]);
//                // oDataTable = utilities.GetTable("select  PackageType,UnitCode,PackageCode from dbo.UnitsDataMain where PPID='" + plant[j] + "'order by UnitCode");

//                for (int k = 0; k < Plant_Packages_Num[j]; k++)
//                {



//                    oDataTable = utilities.GetTable("select distinct UnitCode from dbo.UnitsDataMain where PPID='" +
//                        plant[j] + "'");

//                    int ppid = int.Parse(plant[j]);

//                    DataRow myRow = oDataTable.Rows[0];

//                    string packageType = myRow["UnitCode"].ToString().Trim();

//                    string Initial = "";
//                    if (packageType .Contains("CC"))
//                    {
//                        Initial = "C" + (k + 1).ToString();

                       
//                        if (Findccplant(ppid) == true)
//                        {
//                            ppid++;
//                        }

//                    }

//                    else if (packageType.Contains("Gas"))
//                    {
//                        oDataTable = utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
//    plant[j] + "' and packageType ='Gas'");

//                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
//                    }
//                    else if (packageType.Contains("Steam"))
//                    {
//                        oDataTable = utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
//plant[j] + "'and packageType ='Steam'");

//                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
//                    }


//                    string blockName = ppid.ToString() + "-" + Initial;

//                    blocks[j, k] = blockName;



//                }
//            }
//            return blocks;

//        }
        private string[,] Getblockmaintenance_UnitBased(/*int Package_Num, int[] Plant_Packages_Num*/)
        {

            string[,] blocks = new string[Plants_Num, Max_UnitsNum];

            for (int j = 0; j < Plants_Num; j++)
            {
                int packcode = 0;
                //  int x=int.Parse(plant[j]);
                // oDataTable = utilities.GetTable("select  PackageType,UnitCode,PackageCode from dbo.UnitsDataMain where PPID='" + plant[j] + "'order by UnitCode");

                for (int k = 0; k < Plant_unitsNum[j]; k++)
                {                
                


                    int packageNUM = (int)(Plant_UnitData[j, k, 0] + 1);
                   
                    if (packcode < Plant_Packages_Num[j]) packcode++;

                    oDataTable = Utilities.GetTable("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "'");

                    int ppid = int.Parse(plant[j]);

                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {
                        Initial = "C" + (k + 1).ToString();

                        if (Findccplant(plant[j]))
                        {
                            ppid++;
                        }


                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY unittype");
                        int loopcount = 0;
                        foreach (DataRow mn in oDataTable.Rows)
                        {

                            string ptype = mn[0].ToString().Trim();
                            ptype = ptype.ToLower();
                            ptype = ptype.Replace("cc", "c");
                            string[] sp = ptype.Split('c');
                            ptype = sp[0].Trim() + sp[1].Trim();
                            if (ptype.Contains("gas"))
                            {
                                ptype = ptype.Replace("gas", "G");

                            }
                            else
                            {
                                ptype = ptype.Replace("steam", "S");

                            }
                            ptype = ppid + "-" + ptype;

                            blocks[j, k] = ptype;

                            loopcount++;
                            if (loopcount != oDataTable.Rows.Count) k++;
                        }           

                    //{
                    //    Initial = "C" + packageNUM.ToString();

                       
                    //    if (Findccplant(ppid) == true)
                    //    {
                    //        ppid++;
                    //    }

                    }

                    else if (packageType == "Gas")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                          plant[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'ORDER BY unittype");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;

                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = Utilities.GetTable("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'ORDER BY unittype");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();

                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;

                    }


                  


                }
            }
            return blocks;

        }
    
    }
}
