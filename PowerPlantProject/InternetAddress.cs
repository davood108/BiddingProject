﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using NRI.SBS.Common;
using System.Xml.Serialization;
using System.IO;

namespace PowerPlantProject
{
    public partial class InternetAddress : Form
    {
        public InternetAddress()
        {
            InitializeComponent();

            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtInterchangedEnergy.Text != "" && txtLinenetcomp.Text != "" && txtManategh.Text != "" && txtProduceEnergy.Text != "" && txtReginnetcomp.Text != "" && txtUnitnetcomp.Text != "" && cmbinterchange.Text!="" && cmblinenet.Text!="" && cmbmanategh.Text!="" && cmbproduce.Text!="" && cmbreginnet.Text!="" && cmbrep.Text!="" && cmbunit.Text!="" )
                {
                    DataTable dt = Utilities.GetTable("delete from internetaddress");
                    DataTable din = Utilities.GetTable("insert into  internetaddress (Unitnetcomp,Linenetcomp,Reginnetcomp,InterchangedEnergy,Manategh,ProduceEnergy,Rep12Page,sheetUnitnetcomp,sheetLinenetcomp,sheetReginnetcomp,sheetInterchangedEnergy,sheetManategh,sheetProduceEnergy,sheetRep12Page) values ('" + (txtUnitnetcomp.Text.Trim() + ".xls") +
                        "','" + (txtLinenetcomp.Text.Trim() + ".xls") + "','" + (txtReginnetcomp.Text.Trim() + ".xls") + "','" + (txtInterchangedEnergy.Text.Trim() + ".xls") +
                        "','" + (txtManategh.Text.Trim() + ".xls") + "','" + (txtProduceEnergy.Text.Trim() + ".xls") + "','" + (txtrep12.Text.Trim() + ".xls") + "',N'"+cmbunit.Text+
                        "',N'"+cmblinenet.Text+"',N'"+cmbreginnet.Text+"',N'"+cmbinterchange.Text+"',N'"+cmbmanategh.Text+"',N'"+cmbproduce.Text+"',N'"+cmbrep.Text+"')");
                    if (din != null)
                    {
                        MessageBox.Show("Save Successfully");
                    }
                }
                else
                {
                    MessageBox.Show("Please Fill ALL Address");
                }
            }
            catch
            {
                MessageBox.Show("Save UnSuccessfully");
            }
        }

        private void InternetAddress_Load(object sender, EventArgs e)
        {
            DataTable dd = Utilities.GetTable("select * from internetaddress");
            if (dd.Rows.Count > 0)
            {
                txtUnitnetcomp.Text = dd.Rows[0]["Unitnetcomp"].ToString().Replace(".xls","").Trim();
                txtReginnetcomp.Text = dd.Rows[0]["Reginnetcomp"].ToString().Replace(".xls", "").Trim();
                txtProduceEnergy.Text = dd.Rows[0]["ProduceEnergy"].ToString().Replace(".xls","").Trim();
                txtManategh.Text = dd.Rows[0]["Manategh"].ToString().Replace(".xls","").Trim();
                txtLinenetcomp.Text = dd.Rows[0]["Linenetcomp"].ToString().Replace(".xls","").Trim();
                txtInterchangedEnergy.Text = dd.Rows[0]["InterchangedEnergy"].ToString().Replace(".xls","").Trim();
                txtrep12.Text = dd.Rows[0]["Rep12Page"].ToString().Replace(".xls", "").Trim();

                cmbunit.Text = dd.Rows[0]["sheetUnitnetcomp"].ToString().Replace(".xls", "").Trim();
                cmbreginnet.Text = dd.Rows[0]["sheetReginnetcomp"].ToString().Replace(".xls", "").Trim();
                cmbproduce.Text = dd.Rows[0]["sheetProduceEnergy"].ToString().Replace(".xls", "").Trim();
                cmbmanategh.Text = dd.Rows[0]["sheetManategh"].ToString().Replace(".xls", "").Trim();
                cmblinenet.Text = dd.Rows[0]["sheetLinenetcomp"].ToString().Replace(".xls", "").Trim();
                cmbinterchange.Text = dd.Rows[0]["sheetInterchangedEnergy"].ToString().Replace(".xls", "").Trim();
                cmbrep.Text = dd.Rows[0]["sheetRep12Page"].ToString().Replace(".xls", "").Trim();
            }
        }

        private void txtUnitnetcomp_MouseClick(object sender, MouseEventArgs e)
        {
          
            toolTip1.SetToolTip(txtUnitnetcomp, "Example----->http://sccisrep.igmc.ir/Html/13900705/NetworkComponents_R.xls");
           
        }

        

       

       
    }
}
