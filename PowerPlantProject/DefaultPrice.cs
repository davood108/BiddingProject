﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using System.IO;
using Microsoft.Win32;

namespace PowerPlantProject
{
    public partial class DefaultPrice : Form
    {
        public DefaultPrice()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void DefaultPrice_Load(object sender, EventArgs e)
        {
            DataTable dt = Utilities.GetTable("select ppid from powerplant");
            foreach (DataRow c in dt.Rows)
            {

                comboBox1.Items.Add(c[0].ToString().Trim());

            }
            comboBox1.Text = comboBox1.Items[0].ToString();

            double cap1=0;
               DataTable bas = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date <='"+new PersianDate(DateTime.Now).ToString("d")+"'order by BaseID desc");
                if (bas.Rows.Count > 0)
                {
                    cap1 = MyDoubleParse(bas.Rows[0][0].ToString()) ;
                 
                }
                numericUpDown1.Maximum= decimal.Parse(cap1.ToString());
                numericUpDown2.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown3.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown4.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown5.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown6.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown7.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown8.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown9.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown10.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown11.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown12.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown13.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown14.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown15.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown16.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown17.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown18.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown19.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown20.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown21.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown22.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown23.Maximum = decimal.Parse(cap1.ToString());
                numericUpDown24.Maximum = decimal.Parse(cap1.ToString());
                fill();

               


        }
        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "" || str.Trim() == null)
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        public void fill()
        {
            DataTable v = Utilities.GetTable("select * from defaultprice where ppid='"+comboBox1.Text.Trim()+"'");

            if (v.Rows.Count > 0)
            {
                numericUpDown1.Value = decimal.Parse(v.Rows[0]["H1"].ToString());
                numericUpDown2.Value = decimal.Parse(v.Rows[0]["H2"].ToString());
                numericUpDown3.Value = decimal.Parse(v.Rows[0]["H3"].ToString());
                numericUpDown4.Value = decimal.Parse(v.Rows[0]["H4"].ToString());
                numericUpDown5.Value = decimal.Parse(v.Rows[0]["H5"].ToString());
                numericUpDown6.Value = decimal.Parse(v.Rows[0]["H6"].ToString());
                numericUpDown7.Value = decimal.Parse(v.Rows[0]["H7"].ToString());
                numericUpDown8.Value = decimal.Parse(v.Rows[0]["H8"].ToString());
                numericUpDown9.Value = decimal.Parse(v.Rows[0]["H9"].ToString());
                numericUpDown10.Value = decimal.Parse(v.Rows[0]["H10"].ToString());
                numericUpDown11.Value = decimal.Parse(v.Rows[0]["H11"].ToString());
                numericUpDown12.Value = decimal.Parse(v.Rows[0]["H12"].ToString());
                numericUpDown13.Value = decimal.Parse(v.Rows[0]["H13"].ToString());
                numericUpDown14.Value = decimal.Parse(v.Rows[0]["H14"].ToString());
                numericUpDown15.Value = decimal.Parse(v.Rows[0]["H15"].ToString());
                numericUpDown16.Value = decimal.Parse(v.Rows[0]["H16"].ToString());
                numericUpDown17.Value = decimal.Parse(v.Rows[0]["H17"].ToString());
                numericUpDown18.Value = decimal.Parse(v.Rows[0]["H18"].ToString());
                numericUpDown19.Value = decimal.Parse(v.Rows[0]["H19"].ToString());
                numericUpDown20.Value = decimal.Parse(v.Rows[0]["H20"].ToString());
                numericUpDown21.Value = decimal.Parse(v.Rows[0]["H21"].ToString());
                numericUpDown22.Value = decimal.Parse(v.Rows[0]["H22"].ToString());
                numericUpDown23.Value = decimal.Parse(v.Rows[0]["H23"].ToString());
                numericUpDown24.Value = decimal.Parse(v.Rows[0]["H24"].ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable v = Utilities.GetTable("delete from defaultprice WHERE PPID='"+comboBox1.Text.Trim()+"'");
                string vi = "";
                string vj = "";
                for (int o = 1; o <= 24; o++)
                {
                    if (o < 24)
                        vi += "H" + o + ",";
                    else vi += "H" + o;

                }


                DataTable insert = Utilities.GetTable("insert into defaultprice ("+"ppid,"+ vi +") values('"+comboBox1.Text.Trim()+"','"+ numericUpDown1.Value.ToString() + "','"
                    + numericUpDown2.Value.ToString() + "','" + numericUpDown3.Value.ToString() + "','" + numericUpDown4.Value.ToString() + "','" + numericUpDown5.Value.ToString() + "','"
                    + numericUpDown6.Value.ToString() + "','" + numericUpDown7.Value.ToString() + "','" + numericUpDown8.Value.ToString() + "','" + numericUpDown9.Value.ToString() + "','"
                    + numericUpDown10.Value.ToString() + "','" + numericUpDown11.Value.ToString() + "','" + numericUpDown12.Value.ToString() + "','" + numericUpDown13.Value.ToString() + "','"
                    + numericUpDown14.Value.ToString() + "','" + numericUpDown15.Value.ToString() + "','" + numericUpDown16.Value.ToString() + "','" + numericUpDown17.Value.ToString() + "','"
                    + numericUpDown18.Value.ToString() + "','" + numericUpDown19.Value.ToString() + "','" + numericUpDown20.Value.ToString() + "','" + numericUpDown21.Value.ToString() + "','" + numericUpDown22.Value.ToString() + "','" + numericUpDown23.Value.ToString() + "','" + numericUpDown24.Value.ToString() + "')");


                //if (radioButton1.Checked)
                //{
                //    DataTable d = utilities.GetTable("select ppid from powerplant");
                //    foreach (DataRow n in d.Rows)
                //    {

                //        string strCmd = "delete from DetailFRM002 where TargetMarketDate ='" + datePickerFrom.Text.Trim() + "'" +
                //      " and ppid='" + n[0].ToString().Trim() + "'and Estimated=0";
                //        DataTable oDataTable = utilities.GetTable(strCmd);


                //        DataTable p = utilities.GetTable("select unitcode,packagetype,pmax from unitsdatamain where ppid='" + n[0].ToString().Trim() + "'");
                //        foreach (DataRow b in p.Rows)
                //        {
                //            string unit = b[0].ToString().Trim();
                //            string pptype = "0";
                //            if (unit.Contains("cc") || unit.Contains("CC")) pptype = "1";
                //            if (Findcconetype(n[0].ToString().Trim())) pptype = "0";

                //            string m002 = detectblockm002(b[0].ToString().Trim(), b[1].ToString().Trim(), n[0].ToString().Trim());


                //            for (int h = 0; h < 24; h++)
                //            {
                //                DataTable v2 = utilities.GetTable("select * from defaultprice");
                //                double[] price = new double[24];
                //                if (v2.Rows.Count > 0)
                //                {
                //                    price[h] = MyDoubleParse(v2.Rows[0]["H" + (h + 1)].ToString());
                //                }

                //                string firstCommand = "insert into DetailFRM002 (Estimated,TargetMarketDate, PPID,PPType,Block, Hour,DeclaredCapacity,DispachableCapacity ";

                //                string secondCommand = "values (0,'" + datePickerFrom.Text.Trim() + "','" + n[0].ToString().Trim() + "','" +
                //                    pptype.ToString() + "','" + m002 + "'," +
                //                    (h + 1).ToString() + "," +
                //                     b[2].ToString() + ","
                //                    + b[2].ToString();


                //                for (int s = 0; s < 10; s++)
                //                {


                //                    firstCommand += ",Power" + (s + 1).ToString().Trim();
                //                    if (s == 0)
                //                        secondCommand += "," + b[2].ToString();
                //                    else
                //                        secondCommand += "," + "0";


                //                    firstCommand += ",Price" + (s + 1).ToString().Trim();
                //                    if (s == 0)
                //                        secondCommand += "," + price[h].ToString().Trim();
                //                    else
                //                        secondCommand += "," + "0";



                //                }
                //                firstCommand += " ) ";
                //                secondCommand += " ) ";

                //                oDataTable = utilities.GetTable(firstCommand + secondCommand);

                //            }

                //        }
                //    }


                //}

               
                ////////////////////////////////////////////////////////////////////////////////////////////////////

                fill();
                MessageBox.Show("Save Successfully");
            }
            catch
            {   

            }
        }

       
        public bool Findcconetype(string ppid)
        {
            int tr = 0;

            DataTable oDataTable = Utilities.GetTable("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count == 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }
        public string detectblockm002(string unit, string package, string ppid)
        {

            
            string ptypenum = "0";
            if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";

            if (Findcconetype(ppid.ToString())) ptypenum = "0";

             string temp = unit.ToLower();
                  

            /////////////////////////////////////

            string blockM002 = unit.ToLower();
            if (package.Contains("CC"))
            {
                blockM002 = blockM002.Replace("cc", "c");
                string[] sp = blockM002.Split('c');
                blockM002 = sp[0].Trim() + sp[1].Trim();
                if (blockM002.Contains("gas"))
                    blockM002 = blockM002.Replace("gas", "G");
                else if (blockM002.Contains("steam"))
                    blockM002 = blockM002.Replace("steam", "S");

            }
            else
            {
                if (blockM002.Contains("gas"))
                    blockM002 = blockM002.Replace("gas", "G");
                else if (blockM002.Contains("steam"))
                    blockM002 = blockM002.Replace("steam", "S");
            }
            blockM002 = blockM002.Trim();

            /////////////////////////////////////
            return blockM002;
        }
    }
}
