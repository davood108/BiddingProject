﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using NRI.SBS.Common;
using System.Collections;
using Dundas.Charting.WinControl;

namespace PowerPlantProject
{
    public partial class BidDataUnitPlotForm : Form
    {
        public string Date { get; set; }
        public string package { get; set; }
        public string unit { get; set; }
        public string PPId { get; set; }
        //public bool isRealBid { get; set; }
        public string unitType { get; set; }

        public int Hour;
        string[] arrPrice;
        string[] arrPower;
        string[] arrPricees;
        string[] arrPoweres;
       
        SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);


        public BidDataUnitPlotForm()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void BidDataUnitPlotForm_Load(object sender, EventArgs e)
        {

            label1.Text = "Plant  :   " + PPId + "   Unit :   " + unit + "   Date  :   " + Date;

            if (Hour == 0)
                Hour = 1;
            FillBDUnitGrid();
            
                FillBDUnitGridestim();
                if (arrPricees[1] != null)
                {
                    FillChartest();
                }
            FillChart();
            lblHour.Text = Hour.ToString();
            if ((Hour == 0) || (Hour == 1))
            {
                btnPrevious.Enabled = false;
            }
            else
            {
                btnPrevious.Enabled = true;
            }

            if (Hour == 24)
            {
                btnNext.Enabled = false;
            }
            else
            {
                btnNext.Enabled = true;
            }
        }
        //------------------------------------
        private void FillBDUnitGrid()
        {

            if ((Date != null) && (Date != ""))
            {
                string temp = unit;


                string ptypenum = "0";
                if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";
                //if (PPId == "232") ptypenum = "0";
                if (Findcconetype(PPId)) ptypenum = "0";

                //Build the Bolck for FRM002
                temp = DetectBlockM002();
                myConnection.Open();
                DataSet PowerDS = new DataSet();
                SqlDataAdapter Powerda = new SqlDataAdapter();
                //if (isRealBid)
                    Powerda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4," +
                    "Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10 " +
                    "FROM [DetailFRM002] WHERE Estimated<>1 AND TargetMarketDate=@date AND PPType='"+ ptypenum+"'and Block=@temp AND PPID=" + PPId + " AND Hour=" + Hour, myConnection);
                //else
                //    Powerda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4," +
                //    "Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10 " +
                //    "FROM [DetailFRM002] WHERE Estimated=1 AND TargetMarketDate=@date AND PPType='"+ ptypenum +"'and Block=@temp AND PPID=" + PPId + " AND Hour=" + Hour, myConnection);
                Powerda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Powerda.SelectCommand.Parameters["@date"].Value = Date;
                Powerda.SelectCommand.Parameters.Add("@temp", SqlDbType.NChar, 20);
                Powerda.SelectCommand.Parameters["@temp"].Value = temp;
                Powerda.Fill(PowerDS);
                myConnection.Close();
                arrPower = new string[11];
                arrPrice = new string[11];

                
                foreach (DataRow MyRow in PowerDS.Tables[0].Rows)
                {
                    arrPower[1] = MyRow[0].ToString();
                    arrPower[2] = MyRow[2].ToString();
                    arrPower[3] = MyRow[4].ToString();
                    arrPower[4] = MyRow[6].ToString();
                    arrPower[5] = MyRow[8].ToString();
                    arrPower[6] = MyRow[10].ToString();
                    arrPower[7] = MyRow[12].ToString();
                    arrPower[8] = MyRow[14].ToString();
                    arrPower[9] = MyRow[16].ToString();
                    arrPower[10] = MyRow[18].ToString();
                    arrPrice[1] = MyRow[1].ToString();
                    arrPrice[2] = MyRow[3].ToString();
                    arrPrice[3] = MyRow[5].ToString();
                    arrPrice[4] = MyRow[7].ToString();
                    arrPrice[5] = MyRow[9].ToString();
                    arrPrice[6] = MyRow[11].ToString();
                    arrPrice[7] = MyRow[13].ToString();
                    arrPrice[8] = MyRow[15].ToString();
                    arrPrice[9] = MyRow[17].ToString();
                    arrPrice[10] = MyRow[19].ToString();

                }
            }
        }

        private void FillBDUnitGridestim()
        {

            if ((Date != null) && (Date != ""))
            {
                string temp = unit;


                string ptypenum = "0";
                if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";
                //if (PPId == "232") ptypenum = "0";
                if (Findcconetype(PPId)) ptypenum = "0";

                //Build the Bolck for FRM002
                temp = DetectBlockM002();
                myConnection.Open();
                DataSet PowerDS = new DataSet();
                SqlDataAdapter Powerda = new SqlDataAdapter();

                Powerda.SelectCommand = new SqlCommand("SELECT Power1,Price1,Power2,Price2,Power3,Price3,Power4," +
                "Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10 " +
                "FROM [DetailFRM002] WHERE Estimated=1 AND TargetMarketDate=@date AND PPType='" + ptypenum + "'and Block=@temp AND PPID=" + PPId + " AND Hour=" + Hour, myConnection);
                Powerda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Powerda.SelectCommand.Parameters["@date"].Value = Date;
                Powerda.SelectCommand.Parameters.Add("@temp", SqlDbType.NChar, 20);
                Powerda.SelectCommand.Parameters["@temp"].Value = temp;
                Powerda.Fill(PowerDS);
                myConnection.Close();
                arrPoweres = new string[11];
                arrPricees = new string[11];


                foreach (DataRow MyRow in PowerDS.Tables[0].Rows)
                {
                    arrPoweres[1] = MyRow[0].ToString();
                    arrPoweres[2] = MyRow[2].ToString();
                    arrPoweres[3] = MyRow[4].ToString();
                    arrPoweres[4] = MyRow[6].ToString();
                    arrPoweres[5] = MyRow[8].ToString();
                    arrPoweres[6] = MyRow[10].ToString();
                    arrPoweres[7] = MyRow[12].ToString();
                    arrPoweres[8] = MyRow[14].ToString();
                    arrPoweres[9] = MyRow[16].ToString();
                    arrPoweres[10] = MyRow[18].ToString();
                    arrPricees[1] = MyRow[1].ToString();
                    arrPricees[2] = MyRow[3].ToString();
                    arrPricees[3] = MyRow[5].ToString();
                    arrPricees[4] = MyRow[7].ToString();
                    arrPricees[5] = MyRow[9].ToString();
                    arrPricees[6] = MyRow[11].ToString();
                    arrPricees[7] = MyRow[13].ToString();
                    arrPricees[8] = MyRow[15].ToString();
                    arrPricees[9] = MyRow[17].ToString();
                    arrPricees[10] = MyRow[19].ToString();

                }
            }
        }


        //------------------------------------
        private void FillChart()
        {
            //string PMin = GetPMin();
            string PMin = "0";
            chart1.Series["Real"].Points.Clear();
            chart1.Series["Real"].Name = "Real";
            chart1.ChartAreas["Default"].AxisX.CustomLabels.Clear();
            chart1.ChartAreas["Default"].AxisX.Margin = false;
            chart1.ChartAreas["Default"].AxisX.Minimum = 0;
            chart1.ChartAreas["Default"].AxisX.Interval = 1;
            chart1.ChartAreas["Default"].AxisX.Maximum = 10;
            double min = 0, max = 0;
            findMinMaxOfArray(arrPrice, ref min, ref max);
            if (min > 1500)
                chart1.ChartAreas["Default"].AxisY.Minimum = min - 1000;
            else
                chart1.ChartAreas["Default"].AxisY.Minimum = min;
            chart1.ChartAreas["Default"].AxisY.Maximum = max + 1000;

            // arrPrice[1] = chart1.ChartAreas["Default"].AxisY.Minimum.ToString();

            chart1.Series["Real"].Points.AddXY(0, chart1.ChartAreas["Default"].AxisY.Minimum);
            chart1.Series["Real"].Points.AddXY(0, arrPrice[1]);


            if (PMin == arrPower[1])
            {
                bool nocheck = false;
                for (int pointIndex = 0; pointIndex < 10; pointIndex++)
                {
                    if ((arrPrice[pointIndex + 1] == "0") && (arrPrice[pointIndex] != "0"))
                    {
                        nocheck = true;
                    }
                    else
                    {
                        if (!nocheck)
                        {
                            chart1.Series["Real"].Points.AddXY(pointIndex, arrPrice[pointIndex + 1]);


                        }
                    }

                }

            }
            else
            {
                bool nocheck = false;

                for (int pointIndex = 1; pointIndex < 11; pointIndex++)
                {
                    if ((pointIndex > 1) && (arrPrice[pointIndex] == "0") && (arrPrice[pointIndex - 1] != "0"))
                    {
                        nocheck = true;
                    }
                    else
                    {
                        if (!nocheck)
                        {
                            chart1.Series["Real"].Points.AddXY(pointIndex, arrPrice[pointIndex]);
                            if ((pointIndex + 1 < 11) && (arrPrice[pointIndex + 1] != "0"))

                                chart1.Series["Real"].Points.AddXY(pointIndex, arrPrice[pointIndex + 1]);

                        }
                    }
                }

            }

            // Set series chart type
            chart1.Series["Real"].Type = SeriesChartType.StepLine;

            // Set point labels
            //chart1.Series["Real"].ShowLabelAsValue = true;
            chart1.Series["Real"].ShowLabelAsValue = false;
            chart1.Series["Real"].Color = Color.Red;


            // Enable X axis margin
            chart1.ChartAreas["Default"].AxisX.Margin = true;
            chart1.UI.Toolbar.Enabled = true;
            LabelMark LabelMarkStyle = new LabelMark();
            if (PMin == arrPower[1])
            {
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(-1, 1, PMin, 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(0, 2, arrPower[2], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(1, 3, arrPower[3], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(2, 4, arrPower[4], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(3, 5, arrPower[5], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(4, 6, arrPower[6], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(5, 7, arrPower[7], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(6, 8, arrPower[8], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(7, 9, arrPower[9], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(8, 10, arrPower[10], 0, LabelMarkStyle);
            }
            else
            {
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(-1, 1, PMin, 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(0, 2, arrPower[1], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(1, 3, arrPower[2], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(2, 4, arrPower[3], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(3, 5, arrPower[4], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(4, 6, arrPower[5], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(5, 7, arrPower[6], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(6, 8, arrPower[7], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(7, 9, arrPower[8], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(8, 10, arrPower[9], 0, LabelMarkStyle);
                chart1.ChartAreas["Default"].AxisX.CustomLabels.Add(9, 11, arrPower[10], 0, LabelMarkStyle);
            }




        }

        private void FillChartest()
        {
           
           string PMin = "0";
            chart2.Series["Estimated"].Points.Clear();
            chart2.Series["Estimated"].Name = "Estimated";
            chart2.ChartAreas["Default"].AxisX.CustomLabels.Clear();
            chart2.ChartAreas["Default"].AxisX.Margin = false;
            chart2.ChartAreas["Default"].AxisX.Minimum = 0;
            chart2.ChartAreas["Default"].AxisX.Interval = 1;
            chart2.ChartAreas["Default"].AxisX.Maximum = 10;
            double min = 0, max = 0;
            findMinMaxOfArray(arrPricees, ref min, ref max);
            if (min > 1500)
                chart2.ChartAreas["Default"].AxisY.Minimum = min - 1000;
            else
                chart2.ChartAreas["Default"].AxisY.Minimum = min;
            chart2.ChartAreas["Default"].AxisY.Maximum = max + 1000;

            arrPricees[1] = chart2.ChartAreas["Default"].AxisY.Minimum.ToString();

            chart2.Series["Estimated"].Points.AddXY(0, chart2.ChartAreas["Default"].AxisY.Minimum);
            chart2.Series["Estimated"].Points.AddXY(0, arrPricees[1]);


            if (PMin == arrPoweres[1])
            {
                bool nocheck = false;
                for (int pointIndex = 0; pointIndex < 10; pointIndex++)
                {
                    if ((arrPricees[pointIndex + 1] == "0") && (arrPricees[pointIndex] != "0"))
                    {
                        nocheck = true;
                    }
                    else
                    {
                        if (!nocheck)
                        {
                            chart2.Series["Estimated"].Points.AddXY(pointIndex, arrPricees[pointIndex + 1]);


                        }
                    }

                }

            }
            else
            {
                bool nocheck = false;

                for (int pointIndex = 1; pointIndex < 11; pointIndex++)
                {
                    if ((pointIndex > 1) && (arrPricees[pointIndex] == "0") && (arrPricees[pointIndex - 1] != "0"))
                    {
                        nocheck = true;
                    }
                    else
                    {
                        if (!nocheck)
                        {
                            chart2.Series["Estimated"].Points.AddXY(pointIndex, arrPricees[pointIndex]);
                            if ((pointIndex + 1 < 11) && (arrPricees[pointIndex + 1] != "0"))

                                chart2.Series["Estimated"].Points.AddXY(pointIndex, arrPricees[pointIndex + 1]);

                        }
                    }
                }

            }

            // Set series chart type
            chart2.Series["Estimated"].Type = SeriesChartType.StepLine;

            // Set point labels
            //chart1.Series["Real"].ShowLabelAsValue = true;
            chart2.Series["Estimated"].ShowLabelAsValue = false;
            chart2.Series["Estimated"].Color = Color.Green;


            // Enable X axis margin
            chart2.ChartAreas["Default"].AxisX.Margin = true;
            chart2.UI.Toolbar.Enabled = true;
            LabelMark LabelMarkStyle = new LabelMark();
            if (PMin == arrPoweres[1])
            {
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(-1, 1, PMin, 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(0, 2, arrPoweres[2], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(1, 3, arrPoweres[3], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(2, 4, arrPoweres[4], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(3, 5, arrPoweres[5], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(4, 6, arrPoweres[6], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(5, 7, arrPoweres[7], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(6, 8, arrPoweres[8], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(7, 9, arrPoweres[9], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(8, 10, arrPoweres[10], 0, LabelMarkStyle);
            }
            else
            {
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(-1, 1, PMin, 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(0, 2, arrPoweres[1], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(1, 3, arrPoweres[2], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(2, 4, arrPoweres[3], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(3, 5, arrPoweres[4], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(4, 6, arrPoweres[5], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(5, 7, arrPoweres[6], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(6, 8, arrPoweres[7], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(7, 9, arrPoweres[8], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(8, 10, arrPoweres[9], 0, LabelMarkStyle);
                chart2.ChartAreas["Default"].AxisX.CustomLabels.Add(9, 11, arrPoweres[10], 0, LabelMarkStyle);
            }




        }

        //------------------------------------------------------
        private string DetectBlockM002()
        {
            string ptype = unit.Trim(); ;
            ptype = ptype.ToLower();
            if (package.Contains("Combined"))
            {
                //int x;
                //x = CCExcelCode();
                //string packagecode = GetPackageCode();
                //ptype = "C" + packagecode;

                ptype = ptype.Replace("cc", "c");
                string[] sp = ptype.Split('c');
                ptype = sp[0].Trim() + sp[1].Trim();
                if (ptype.Contains("gas"))
                    ptype = ptype.Replace("gas", "G");
                else if (ptype.Contains("steam"))
                    ptype = ptype.Replace("steam", "S");
            }
            else if (ptype.Contains("gas"))
            {
                ptype = ptype.Replace("gas", "G");
            }
            else
            {
                ptype = ptype.Replace("steam", "S");
            }
            return ptype.Trim();
        }
        //--------------------------------------------------------
        private int CCExcelCode()
        {
            ArrayList PPIDArray = new ArrayList();
            ArrayList PPIDType = new ArrayList();

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            myConnection.Open();

            Myda.SelectCommand = new SqlCommand("SELECT PPID FROM PowerPlant", myConnection);
            Myda.Fill(MyDS, "ppid");
            foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
            {
                PPIDArray.Add(MyRow["PPID"].ToString().Trim());
                PPIDType.Add("real");
            }

            for (int i = 0; i < PPIDArray.Count; i++)
            {
                SqlCommand mycom = new SqlCommand();
                mycom.Connection = myConnection;
                mycom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id SELECT @result2 =count(PPID) FROM [PPUnit] WHERE PPID=@id AND PackageType LIKE 'Combined Cycle%'";
                mycom.Parameters.Add("@id", SqlDbType.NChar, 10);
                mycom.Parameters["@id"].Value = PPIDArray[i].ToString();
                mycom.Parameters.Add("@result1", SqlDbType.Int);
                mycom.Parameters["@result1"].Direction = ParameterDirection.Output;
                mycom.Parameters.Add("@result2", SqlDbType.Int);
                mycom.Parameters["@result2"].Direction = ParameterDirection.Output;
                mycom.ExecuteNonQuery();
                int result1 = (int)mycom.Parameters["@result1"].Value;
                int result2 = (int)mycom.Parameters["@result2"].Value;
                if ((result1 > 1) && (result2 > 0))
                {
                    PPIDArray.Add((int.Parse(PPIDArray[i].ToString())) + 1);
                    PPIDType.Add("virtual");
                }
            }
            myConnection.Close();

            int x;
            x = int.Parse(PPId);
            if (PPIDArray.Contains(x + 1))
                if (PPIDType[PPIDArray.IndexOf(x + 1)].ToString() == "virtual") x++;
            return x;

        }
        //--------------------------------------------------------
        private string GetPackageCode()
        {
            DataSet dataDS = new DataSet();
            SqlCommand MyCom = new SqlCommand();
            MyCom.CommandText = "select @pc=PackageCode from dbo.UnitsDataMain where PPID=" + PPId + " and UnitCode='" + unit + "' and UnitType='" + unitType + "'";
            MyCom.Parameters.Add("@pc", SqlDbType.Int);
            MyCom.Parameters["@pc"].Direction = ParameterDirection.Output;
            myConnection.Open();
            MyCom.Connection = myConnection;
            MyCom.ExecuteNonQuery();
            myConnection.Close();
            return MyCom.Parameters["@pc"].Value.ToString();



        }
        //--------------------------------------
        private void findMinMaxOfArray(string[] arr, ref double min, ref double max)
        {
            bool noChek = false;
            if (arr[1] != null)
            {
                min = double.Parse(arr[1]);
                max = double.Parse(arr[1]);
            }
            try
            {
                for (int i = 2; i < 11; i++)
                {
                    if (arr[i] != null)
                    {
                        if((double.Parse(arr[i]) ==0) &&(double.Parse(arr[i-1]) !=0))//dade vared nashode
                        {
                            noChek = true;
                        }
                        else
                        {
                            if (!noChek)
                            {
                                if (double.Parse(arr[i]) < min)
                                    min = double.Parse(arr[i]);

                            }
                        }
                        if (double.Parse(arr[i]) > max)
                            max = double.Parse(arr[i]);
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }


        }
        //--------------------------------------
        private string GetPMin()
        {
            SqlCommand MyCom = new SqlCommand();
            MyCom.CommandText = "SELECT @pmin=PMin FROM UnitsDataMain WHERE PPID=@num AND UnitCode=@unit AND PackageType=@package ";
            MyCom.Connection = myConnection;
            MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
            MyCom.Parameters["@num"].Value = PPId;
            MyCom.Parameters.Add("@unit", SqlDbType.NChar, 20);
            MyCom.Parameters["@unit"].Value = unit;
            MyCom.Parameters.Add("@package", SqlDbType.NChar, 10);
            string Pc = package;
            if (Pc.Contains("Combined")) Pc = "CC";
            MyCom.Parameters["@package"].Value = Pc;
            MyCom.Parameters.Add("@pmin", SqlDbType.Real, 10);
            MyCom.Parameters["@pmin"].Direction = ParameterDirection.Output;
            string PMin="0";            
            try
            {
                myConnection.Open();
                MyCom.ExecuteNonQuery();
                myConnection.Close();
                PMin = MyCom.Parameters["@pmin"].Value.ToString();


            }
            catch (Exception)
            {
                
                throw;
            }
            return PMin;


        }
        //----------------------------------------------------
        private void btnFirst_Click(object sender, EventArgs e)
        {
            Hour = 1;
            FillBDUnitGrid();
            FillChart();
            
                FillBDUnitGridestim();
                if (arrPricees[1] != null)
                {
                    FillChartest();
                }
            lblHour.Text = Hour.ToString();
            btnPrevious.Enabled = false;
            btnNext.Enabled = true;

        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            Hour = Hour - 1;
            FillBDUnitGrid();
            FillChart();

            FillBDUnitGridestim();
            if (arrPricees[1] != null)
            {
                FillChartest();
            }
            lblHour.Text = Hour.ToString();
            if ((Hour == 0) || (Hour == 1))
            {
                btnPrevious.Enabled = false;
            }
            else
            {
                btnPrevious.Enabled = true;
            }

            if (Hour == 24)
            {
                btnNext.Enabled = false;
            }
            else
            {
                btnNext.Enabled = true;
            }
            

        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Hour = Hour + 1;
            FillBDUnitGrid();
            FillChart();
            FillBDUnitGridestim();
            if (arrPricees[1] != null)
            {
                FillChartest();
            }
            lblHour.Text = Hour.ToString();
            if ((Hour == 0) || (Hour == 1))
            {
                btnPrevious.Enabled = false;
            }
            else
            {
                btnPrevious.Enabled = true;
            }

            if (Hour == 24)
            {
                btnNext.Enabled = false;
            }
            else
            {
                btnNext.Enabled = true;
            }
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            Hour = 24;
            FillBDUnitGrid();
            FillChart();
            FillBDUnitGridestim();
            if (arrPricees[1] != null)
            {
                FillChartest();
            }
            lblHour.Text = Hour.ToString();
            btnNext.Enabled = false;
            btnPrevious.Enabled = true;
            

        }

        public bool Findcconetype(string ppid)
        {
            int tr = 0;

           DataTable  oDataTable = Utilities.GetTable("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count == 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }
        //--------------------------------------
    }
}
