﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Common;
using PowerPlantProject;
using FarsiLibrary.Utils;
using NRI.SBS.Common;
using System.Collections;

namespace PowerPlantProject
{
    public partial class LackOfInternetDataForm : Form
    {
        string gencocode = "";
        private string Date;
        private int Train;
        public LackOfInternetDataForm(string date,int train)
        {
            Date = date;
            Train = train;
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }

            DataTable dt = Utilities.GetTable("select * from BaseGencoInfo");
            if (dt.Rows.Count > 0)
            {
                gencocode = dt.Rows[0]["GencoCode"].ToString().Trim();

            }
        }
      

        private void LackOfInternetDataForm_Load(object sender, EventArgs e)
        {
            textBox1.Text = " Please Download Below Dates \r\n";
           string perdate = "";
            DataTable findrev = Utilities.GetTable("Select distinct PPCode from dbo.RegionNetComp where  Date='" + Date + "'AND Code='"+gencocode+"' AND PPCode NOT IN (SELECT PPID FROM PowerPlant)");
            DataTable findproduce = Utilities.GetTable("Select * from ProducedEnergy where Date='" + Date + "'");
            DataTable findinterchange = Utilities.GetTable("SELECT * FROM [InterchangedEnergy] WHERE Date='" + Date + "'or Date='" + Date + "' ORDER BY Date");
            DataTable findmanategh = Utilities.GetTable("select Peak from Manategh where Date='" + Date + "'and Title= N'توليد بدون صنايع' order by Code");
            if (findrev.Rows.Count == 0 || findinterchange.Rows.Count == 0 || findmanategh.Rows.Count == 0 || findproduce.Rows.Count == 0)
            {
               
                PersianDate test1 = new PersianDate(Date);
                DateTime Now = PersianDateConverter.ToGregorianDateTime(test1);
                
                for (int i = 0; i < Train; i++)
                {
                 DateTime selectedDate = Now.Subtract(new TimeSpan(i, 0, 0, 0));
                 perdate = new PersianDate(selectedDate).ToString("d");
                 findrev = Utilities.GetTable("Select distinct PPCode from dbo.RegionNetComp where  Date='" + perdate + "'AND Code='"+gencocode+"' AND PPCode NOT IN (SELECT PPID FROM PowerPlant)");
                 findproduce = Utilities.GetTable("Select * from ProducedEnergy where Date='" + perdate + "'");
                 findinterchange = Utilities.GetTable("SELECT * FROM [InterchangedEnergy] WHERE Date='" + perdate + "'or Date='" + perdate + "' ORDER BY Date");
                 findmanategh = Utilities.GetTable("select Peak from Manategh where Date='" + perdate + "'and Title= N'توليد بدون صنايع' order by Code");
                 if (findrev.Rows.Count == 0 || findinterchange.Rows.Count == 0 || findmanategh.Rows.Count == 0 || findproduce.Rows.Count == 0)
                 {
                     
                     textBox1.Text += "\r\n" + "<<<<<<<<<<<<<<   " + perdate + "   >>>>>>>>>>>>>>>";
                 }
                }





            }
        }
    }
}
