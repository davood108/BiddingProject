﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    partial class MainForm
    {
        string beforecmbname = "";
        private void lblPlantValue_TextChanged(object sender, EventArgs e)
        {
            UpdateBiddingTab();
        }



        //private void datePickerCurrent_SelectedDateTimeChanged(object sender, EventArgs e)
        //{


        //  datePickerBidding.SelectedDateTime = datePickerCurrent.SelectedDateTime.AddDays(1);

        //    FillBiddingStatusBox();
        //}
        
        private bool ValidateBiddingDates()
        {
            string msg = "";
            if (lblPlantValue.Text.Trim() == "")
            {
                msg = "Please select the plant(s)";
            }
            else if (datePickerCurrent.IsNull || datePickerBidding.IsNull)
            {
                msg = "Please select current and bidding dates";
            }
            else if (datePickerCurrent.SelectedDateTime > datePickerBidding.SelectedDateTime)
            {
                msg = "Bidding date should be ahead of current date";
            }
            if (msg == "")
                return true;
            else
            {
                MessageBox.Show(msg);
                return false;
            }
        }

        private void btnRunBidding_Click(object sender, EventArgs e)
        {

            bool presolve = false;
            bool Mcp = false;
            bool OpfRun = false;
            bool MaxALL = false;
            DataTable dt = null;

            if (!ValidateBiddingDates())
                return;

            if (lblPlantValue.Text.Trim() != "All")
            {
                dt = Utilities.GetTable("select * from BidRunSetting where Plant='" + lblPlantValue.Text.Trim() + "'");
                if (dt.Rows.Count > 0)
                {
                    if (MyboolParse(dt.Rows[0]["FlagPresolve"].ToString())) presolve = true;
                    if (MyboolParse(dt.Rows[0]["FlagMarketPower"].ToString())) Mcp = true;

                    if (MyboolParse(dt.Rows[0]["FlagOpf"].ToString())) OpfRun = true;
                    //if (lblPlantValue.Text.Trim() == "All") OpfRun = true;
                }
            }
            else
            {
                DataTable drun = Utilities.GetTable("select * from dbo.BidRunSetting");
                foreach (DataRow n in drun.Rows)
                {
                    if (n["FlagPresolve"].ToString() == "True")
                    {
                        presolve = true;
                        break;
                    }

                }
                foreach (DataRow n1 in drun.Rows)
                {
                    if (n1["FlagMarketPower"].ToString() == "True")
                    {
                        Mcp = true;
                        break;
                    }
                }
                foreach (DataRow n2 in drun.Rows)
                {
                    if (n2["FlagOpf"].ToString() == "True")
                    {
                        OpfRun = true;
                        break;
                    }
                }

                //DataTable dd = utilities.GetTable(" select PPName from dbo.PowerPlant");
                //dt = utilities.GetTable("select * from BidRunSetting where Plant='" + dd.Rows[0][0].ToString().Trim() + "'");
            }
            

            //if (checkPreSolve.Checked)
            //{
            //    presolve = true;

            //}
            //if (chkmarketpower.Checked)
            //{
            //    Mcp = true;

            //}

            //if (chkopfrun.Checked)
            //{
            //    OpfRun = true;
            //}


            if (chkmaxall.Checked)
            {
                //usemax.GetInstance().Ismax = true;
            }
            else if (chkmaxall.Checked == false)
            {
               // usemax.GetInstance().Ismax = false;
            }

            if (Initialprice.Checked)
            {
                useInitial.GetInstance().IsInitial = true;
            }
            else if (Initialprice.Checked == false)
            {
                useInitial.GetInstance().IsInitial = false;
            }

            BiddingStrategyProgressForm frm = new BiddingStrategyProgressForm(this, presolve, Mcp, OpfRun);
            frm.BiddingStrategySettingId = SaveBiddingStrategy();

            frm.ShowDialog();

        }

        


        private void cmbRunSetting_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbRunSetting.SelectedItem.ToString() == "Customize")
                grBoxCustomize.Enabled = true;
            else
                grBoxCustomize.Enabled = false;

          

            if(cmbRunSetting.SelectedItem.ToString()=="Customize" && beforecmbname=="Automatic")EnableBiddingStrategycustom();
            beforecmbname=cmbRunSetting.SelectedItem.ToString();
        }
        private void btnRunManual_Click(object sender, EventArgs e)
        {
            ManualBidding fr = new ManualBidding(lblPlantValue.Text.Trim(), datePickerCurrent.Text.Trim(), datePickerBidding.Text.Trim());
            fr.ShowDialog();
        }

        private void btnSaveBidding_Click(object sender, EventArgs e)
        {
            SaveBiddingStrategy();
        }

        private int SaveBiddingStrategy()
        {
            SqlCommand MyCom = new SqlCommand();
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            MyCom.Connection = myConnection;

            //////// Delete previous results
            int BiddingStrategySettingId = 0;

            MyCom.CommandText = "select @id=max(id) from [BiddingStrategySetting] " +
            " where Plant=@Plant AND CurrentDate=@CurrentDate AND BiddingDate=@BiddingDate";

            MyCom.Parameters.Add("@Plant", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@CurrentDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@BiddingDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@id", SqlDbType.Int);
            MyCom.Parameters["@Plant"].Value = lblPlantValue.Text.Trim();
            MyCom.Parameters["@CurrentDate"].Value = new PersianDate(datePickerCurrent.SelectedDateTime).ToString("d"); ;
            MyCom.Parameters["@BiddingDate"].Value = new PersianDate(datePickerBidding.SelectedDateTime).ToString("d"); ; ;
            MyCom.Parameters["@id"].Direction = ParameterDirection.Output;

            MyCom.ExecuteNonQuery();

            try
            {
                BiddingStrategySettingId = int.Parse(MyCom.Parameters["@id"].Value.ToString());
            }
            catch { }

            if (BiddingStrategySettingId != 0)
            {
                MyCom = new SqlCommand();
                MyCom.Connection = myConnection;
                MyCom.CommandText = "Delete from BiddingStrategySetting where id=" + BiddingStrategySettingId.ToString();
                MyCom.ExecuteNonQuery();

                MyCom = new SqlCommand();
                MyCom.Connection = myConnection;
                MyCom.CommandText = "Delete from BiddingStrategyCustomized where FkBiddingStrategySettingId=" + BiddingStrategySettingId.ToString();
                MyCom.ExecuteNonQuery();
            }

            // Insert into BiddingStrategySetting table
            MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            MyCom.CommandText = "insert INTO [BiddingStrategySetting] " +
                "(Plant, CurrentDate, BiddingDate, RunSetting, StrategyBidding, TrainingDays,TrainingDaysPower, CostLevel, PeakBid, Status, FlagForecastingPrice, FlagBidAllocation, FlagStrategyBidding)"
                        + " VALUES (@Plant, @CurrentDate, @BiddingDate, @RunSetting, @StrategyBidding, @TrainingDays,@TrainingDaysPower, @CostLevel, @PeakBid, @Status, @FlagForecastingPrice, @FlagBidAllocation, @FlagStrategyBidding)";

            MyCom.Parameters.Add("@Plant", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@CurrentDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@BiddingDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@RunSetting", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@StrategyBidding", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@PeakBid", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@TrainingDays", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@TrainingDaysPower", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@CostLevel", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@Status", SqlDbType.VarChar);
            MyCom.Parameters.Add("@FlagForecastingPrice", SqlDbType.Bit);
            MyCom.Parameters.Add("@FlagBidAllocation", SqlDbType.Bit);
            MyCom.Parameters.Add("@FlagStrategyBidding", SqlDbType.Bit);

            MyCom.Parameters["@Plant"].Value = lblPlantValue.Text;
            MyCom.Parameters["@CurrentDate"].Value = new PersianDate(datePickerCurrent.SelectedDateTime).ToString("d"); ;
            MyCom.Parameters["@BiddingDate"].Value = new PersianDate(datePickerBidding.SelectedDateTime).ToString("d"); ; ;
            MyCom.Parameters["@RunSetting"].Value = cmbRunSetting.Text;
            MyCom.Parameters["@StrategyBidding"].Value = cmbStrategyBidding.Text;
            MyCom.Parameters["@TrainingDays"].Value = txtTraining.Text;
            MyCom.Parameters["@TrainingDaysPower"].Value = txtpowertraining.Text;//???????????????
            MyCom.Parameters["@CostLevel"].Value = cmbCostLevel.Text;
            MyCom.Parameters["@Status"].Value = lstStatus.Text;
            MyCom.Parameters["@PeakBid"].Value = cmbPeakBid.Text;
            MyCom.Parameters["@FlagForecastingPrice"].Value = rdForecastingPrice.Checked;
            MyCom.Parameters["@FlagBidAllocation"].Value = rdBidAllocation.Checked;
            MyCom.Parameters["@FlagStrategyBidding"].Value = rdStrategyBidding.Checked;
            MyCom.ExecuteNonQuery();

            // retrieve the newly inserted id in FinalForcast

            int maxId = 0;

            MyCom.CommandText = "select max(id) from [BiddingStrategySetting]";
            maxId = (int)MyCom.ExecuteScalar();
            if (cmbRunSetting.Text == "Customize")
            {
                for (int j = 1; j <= 24; j++)
                {
                    // Insert into FinalForcastHourly table
                    MyCom = new SqlCommand();
                    MyCom.Connection = myConnection;
                    MyCom.CommandText = "insert INTO [BiddingStrategyCustomized] (FkBiddingStrategySettingId,hour,Strategy, Peak)"
                                + " VALUES (@FkBiddingStrategySettingId,@hour,@Strategy, @Peak)";

                    MyCom.Parameters.Add("@FkBiddingStrategySettingId", SqlDbType.Int);
                    MyCom.Parameters.Add("@hour", SqlDbType.Int);
                    MyCom.Parameters.Add("@Strategy", SqlDbType.NChar, 10);
                    MyCom.Parameters.Add("@Peak", SqlDbType.NChar, 10);

                    ToolStripComboBox cmbStrategy = FindBiddingComboBox("cmbStrategy", j);
                    ToolStripComboBox cmbPeak = FindBiddingComboBox("cmbPeak", j);

                    MyCom.Parameters["@FkBiddingStrategySettingId"].Value = maxId;
                    MyCom.Parameters["@hour"].Value = j;
                    MyCom.Parameters["@Strategy"].Value = cmbStrategy.Text;
                    MyCom.Parameters["@Peak"].Value = cmbPeak.Text;
                    MyCom.ExecuteNonQuery();

                }
            }
            myConnection.Close();
            return maxId;

        }

        private void LoadBiddingStrategy()
        {
            if (User.getUser().Role == DefinedRoles.NoBidding ||
    User.getUser().Role == DefinedRoles.ExceptComputational ||
    User.getUser().Role == DefinedRoles.Unassigned ||
                User.getUser().Role == DefinedRoles.PlantUser)
            {
                MainTabs.TabPages.Remove(MainTabs.TabPages["BiddingStrategy"]);
                addToolStripMenuItem.Enabled = false;
                DeletetoolStripMenuItem.Enabled = false;
            }

            datePickerCurrent.SelectedDateTime = DateTime.Now;
            datePickerBidding.SelectedDateTime = DateTime.Now.AddDays(1);

            
            lblGencoValue.Text = GenName;
            lblPlantValue.Text = "All";
            ////////////////////////////////////
           
          
            


            try
            {
                SqlCommand MyCom = new SqlCommand();
                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                MyCom.Connection = myConnection;
                // Insert into FinalForcast table
                MyCom.CommandText = "select max(id) from [BiddingStrategySetting]";

                int maxId = (int)MyCom.ExecuteScalar();


                MyCom.CommandText = "select * from [BiddingStrategySetting] " +
                    "where id=" + maxId.ToString();

                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;
                Maxda.Fill(MaxDS);

                DataTable dt = MaxDS.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    lblPlantValue.Text = row["Plant"].ToString();
                    //datePickerCurrent.SelectedDateTime = PersianDateConverter.ToGregorianDateTime(new PersianDate(row["CurrentDate"].ToString()));
                    datePickerCurrent.SelectedDateTime = PersianDateConverter.ToGregorianDateTime(new PersianDate(row["BiddingDate"].ToString())).AddDays(-1);
                    datePickerBidding.SelectedDateTime = PersianDateConverter.ToGregorianDateTime(new PersianDate(row["BiddingDate"].ToString()));
                    cmbRunSetting.Text = row["RunSetting"].ToString().Trim();
                    cmbStrategyBidding.Text = row["StrategyBidding"].ToString().Trim();
                    txtTraining.Text = row["TrainingDays"].ToString().Trim();
                    txtpowertraining.Text = row["TrainingDaysPower"].ToString().Trim();
                    //????????????????????????????
                    cmbCostLevel.Text = row["CostLevel"].ToString().Trim();
                    cmbPeakBid.Text = row["PeakBid"].ToString().Trim();
                    //lstStatus.Text = row["Status"].ToString().Trim();
                    string strBool = row["FlagForecastingPrice"].ToString().Trim().ToLower();
                    rdForecastingPrice.Checked = (strBool == "true" ? true : false);

                    strBool = row["FlagBidAllocation"].ToString().Trim().ToLower();
                    rdBidAllocation.Checked = (strBool == "true" ? true : false);
                   
                    strBool = row["FlagStrategyBidding"].ToString().Trim().ToLower();
                    rdStrategyBidding.Checked = (strBool == "true" ? true : false);
                                      
                    MyCom.CommandText = "select * from [BiddingStrategyCustomized] " +
                        "where FkBiddingStrategySettingId = " + maxId.ToString();
                    MaxDS = new DataSet();
                    Maxda = new SqlDataAdapter();
                    Maxda.SelectCommand = MyCom;
                    Maxda.Fill(MaxDS);

                    dt = MaxDS.Tables[0];
                    foreach (DataRow row2 in dt.Rows)
                    {
                        int hour = Int16.Parse(row2["hour"].ToString().Trim());

                        ToolStripComboBox cmbStrategy = FindBiddingComboBox("cmbStrategy", hour);
                        ToolStripComboBox cmbPeak = FindBiddingComboBox("cmbPeak", hour);

                        cmbStrategy.Text = row2["Strategy"].ToString().Trim();
                        cmbPeak.Text = row2["Peak"].ToString().Trim();
                    }
                }
                myConnection.Close();

            }
            catch
            {
            }

        }

       
        private ToolStripComboBox FindBiddingComboBox(string type, int index)
        {
            ToolStripComboBox cmb = null;
            cmb = FindBiddingComboBox_2(this.menuStrip1, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip2, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip3, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip4, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip5, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip6, type, index);
            if (cmb != null)
                return cmb;
            cmb = FindBiddingComboBox_2(this.menuStrip7, type, index);
            if (cmb != null)
                return cmb;
            return FindBiddingComboBox_2(this.menuStrip8, type, index);
        }

        private ToolStripComboBox FindBiddingComboBox_2(MenuStrip menuStrip, string type, int index)
        {
            foreach (ToolStripItem item in menuStrip.Items)
            {
                if (item.GetType() == typeof(ToolStripComboBox))
                {
                    ToolStripComboBox cmb = (ToolStripComboBox)item;
                    if (item.Name.ToLower() == type.ToLower() + index.ToString())
                    {
                        return cmb;
                    }

                }
            }
            return null;
        }

        private void betToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormDownloadInterval frm = new FormDownloadInterval();
            frm.Show();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowNewFolderButton = true;
            DialogResult answer = folderBrowserDialog1.ShowDialog();
            if (answer == DialogResult.OK)
            {
                string path = folderBrowserDialog1.SelectedPath;
                ExportThreadContainer oExportThreadContainer = new ExportThreadContainer(path, datePickerBidding.Text.Trim(), lblPlantValue.Text.Trim());
                Thread thread = new Thread(oExportThreadContainer.LongTask_Export);
                thread.IsBackground = true;
                thread.Start();
            }

        }
      

        private void rdBidAllocation_CheckedChanged(object sender, EventArgs e)
        {
            //if (rdBidAllocation.Checked)
            //{
            //    checkPreSolve.Enabled = false;
            //    checkPreSolve.Checked = false;
               
              
            //}
            //else 
            //{
            //    checkPreSolve.Enabled = true;
             
            //}
        }
       

     

        private void UpdateBiddingTab()
        {
            m002Generated = false;
            btnExport.Enabled = m002Generated;
            FillBiddingStatusBox();

            //if (lblPlantValue.Text == "All")
            //{
            //    btnRunManual.Enabled = false;
            //}
            //else
            //{
            //    btnRunManual.Enabled = true;
            //}

        }

        private void FillBiddingStatusBox()
        {
            PersianDate persianDate = datePickerBidding.SelectedDateTime;
            if (persianDate != null)
            {
                string strDate = PersianDateConverter.ToPersianDate(persianDate).ToString("d");

                lstStatus.Text = "";
                checkpricerun();
                if (lblPlantValue.Text.Trim() != "All" && strDate != "")
                {
                    CheckBiddingInfoAvailability();
                    CheckFuelAvailabiliyty();

                }
            }
        }
        private void checkpricerun()
        {

            try
            {
                string endDate = PersianDateConverter.ToPersianDate(datePickerBidding.SelectedDateTime).ToString("d");
                string strCmd = "select * from powerplant";

                if (lblPlantValue.Text.Trim() != "All")
                {
                    strCmd += " WHERE PPName= '" + lblPlantValue.Text.Trim() + "'";
                }
                DataTable dtPlants = Utilities.GetTable(strCmd);
                if (dtPlants.Rows.Count > 0)
                {
                    foreach (DataRow m in dtPlants.Rows)
                    {
                        DataTable dt = Utilities.GetTable("select * from dbo.FinalForecast where PPId='" + m["PPID"].ToString().Trim() + "' and date='" + endDate + "'order by ID desc");
                        DataTable dts1 = Utilities.GetTable("select * from BidRunSetting where Plant='" + m["PPName"].ToString().Trim() + "'");

                        if (dt.Rows.Count > 0)
                        {
                            lstStatus.Text += "Price Forecasting Run Before For Plant :" + m["PPName"].ToString().Trim() + "\r\n";
                        }
                        else if (MyboolParse(dts1.Rows[0]["FlagPresolve"].ToString()))
                        {
                            //////////////////////////////////////////////////////////////////////////
                            string smaxdate = "";
                            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + endDate + "'order by BaseID desc");
                            if (dtsmaxdate.Rows.Count > 0)
                            {
                                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                                int ib = 0;
                                foreach (DataRow m1 in dtsmaxdate.Rows)
                                {
                                    arrbasedata[ib] = m1["Date"].ToString();
                                    ib++;
                                }
                                smaxdate = buildmaxdate(arrbasedata);
                            }
                            else
                            {
                                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
                            }

                            ////////////////////////////////////////////////////////////////////////

                            DataTable baseplant1 = Utilities.GetTable("select PPName from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                            string[] s = baseplant1.Rows[0][0].ToString().Trim().Split('-');
                            DataTable baseplant = Utilities.GetTable("select PPID FROM powerplant WHERE PPName='" + s[0].Trim() + "'");
                            try
                            {
                                string preplant = baseplant.Rows[0][0].ToString().Trim();


                                dt = Utilities.GetTable("select * from dbo.FinalForecast where PPId='" + preplant + "' and date='" + endDate + "'order by ID desc");

                                if (dt.Rows.Count > 0)
                                {
                                    lstStatus.Text += "Price Forecasting Run Before For Plant :" + m["PPName"].ToString().Trim() + "\r\n";
                                }
                                else
                                {

                                    lstStatus.Text += "You Must Run PriceForecasting For Plant : " + m["PPName"].ToString().Trim() + ".\r\n";
                                }
                            }
                            catch
                            {
                                MessageBox.Show("you must change presolve plant in basedata in this date.");
                            }
                        }
                        else
                        {

                            lstStatus.Text += "You Must Run PriceForecasting For Plant : " + m["PPName"].ToString().Trim() + ".\r\n";
                        }
                    }
                }
            }
            catch
            {

            }
        }
        private string buildmaxdate(string[] arrmax)
        {
            string maxdate = null;
            for (int fd = 0; fd < arrmax.Length; fd++)
            {
                if (maxdate == null || string.Compare(maxdate, arrmax[fd]) < 0)
                    maxdate = arrmax[fd];
            }
            return maxdate;

        } 
        private void CheckBiddingInfoAvailability()
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);

            myConnection.Open();

            string strDate = PersianDateConverter.ToPersianDate(datePickerCurrent.SelectedDateTime).ToString("d");

            string[] M002Tables = { "MainFRM002", "DetailFRM002", "BlockFRM002" };
            string[] M005Tables = { "MainFRM005", "DetailFRM005", "BlockFRM005" };

            string strCmdFirst = "SELECT @count=count(*) from ";
            string strCmdEnd = " as table1 inner join dbo.PowerPlant on PowerPlant.PPID=table1.PPID" +
                " where table1.TargetMarketDate ='" + strDate + "' AND PowerPlant.PPName='" + lblPlantValue.Text.Trim() + "'";

            bool M002Available = true, M005Available = true;
            for (int i = 0; i < 3 && (M002Available || M005Available); i++)
            {
                SqlCommand command = new SqlCommand(strCmdFirst + M002Tables[i] + strCmdEnd, myConnection);
                command.Parameters.Add("@count", SqlDbType.Int);
                command.Parameters["@count"].Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                int count = int.Parse(command.Parameters["@count"].Value.ToString());
                if (count == 0)
                    M002Available = false;

                command = new SqlCommand(strCmdFirst + M005Tables[i] + strCmdEnd, myConnection);
                command.Parameters.Add("@count", SqlDbType.Int);
                command.Parameters["@count"].Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                count = int.Parse(command.Parameters["@count"].Value.ToString());
                if (count == 0)
                    M005Available = false;

            }
            myConnection.Close();


            //////////////////////////////

            int number = 0;
            int dispatchnumber=0;
            int discount = 0;

            string endDate = PersianDateConverter.ToPersianDate(datePickerBidding.SelectedDateTime).ToString("d");
            DataTable pidtable = Utilities.GetTable("select PPID from dbo.PowerPlant where PPName ='" + lblPlantValue.Text.Trim() + "'");
            if (pidtable.Rows.Count > 0)
            {
                DataTable checkdispatch = Utilities.GetTable("select count(*) from  dbo.PowerLimitedUnit where ppid='" + pidtable.Rows[0][0].ToString().Trim() + "' and StartDate='" + endDate + "'and isEmpty=0 ");

                discount = int.Parse(checkdispatch.Rows[0][0].ToString());


                DataTable unitcount = Utilities.GetTable("select distinct count(*) from  dbo.UnitsDataMain where PPID='" + pidtable.Rows[0][0].ToString().Trim() + "'");
                number = int.Parse(unitcount.Rows[0][0].ToString());


                DataTable dispatchform = Utilities.GetTable("select count(*) from dbo.Dispathable where StartDate='"+ endDate +"'and PPID='" + pidtable.Rows[0][0].ToString().Trim() + "'");
                dispatchnumber=int.Parse(dispatchform.Rows[0][0].ToString());
            }


        

            if (M002Available)
                lstStatus.Text += "M002 Data Available...\r\n";
            else
                lstStatus.Text += "M002 Data Not Available...\r\n";

            if (M005Available)
                lstStatus.Text += "M005 Data Available...\r\n";
            else
                lstStatus.Text += "M005 Data Not Available...\r\n";
            
            /////////////////////////////////////////////////////////////
            lstStatus.Text += "...................................\r\n";
            ////////////////////////////////////////////////////////////
            
            if(discount==number)
                lstStatus.Text += "Dispatchable Data Available...\r\n";
            else
                lstStatus.Text += "Dispatchable Data Not Available...\r\n";


            if(dispatchnumber>0)

                lstStatus.Text += "Excel Dispatchable Data Available...\r\n";
            else
                lstStatus.Text += "Excel Dispatchable Data Not Available...\r\n";


            ///////////////////////////////////////////////////////////////////////////

            ///////////////////////////////////////////////////////////////////////////
            lstStatus.Text += "\r\n"; ;
            try
            {
                if (pidtable.Rows.Count > 0)
                {
                    string strCmd = "select * from YearHub where ppid='" + pidtable.Rows[0][0].ToString().Trim() + "'and year='" + datePickerBidding.Text.Trim().Substring(0, 4) + "'";
                    DataTable oDataTable = Utilities.GetTable(strCmd);
                    if (oDataTable.Rows.Count == 0) lstStatus.Text += "YearHub Not Available" + "\r\n";
                    else lstStatus.Text += "YearHub Available" + "\r\n"; ;


                    strCmd = "select * from Yeartrans where ppid='" + pidtable.Rows[0][0].ToString().Trim() + "'and year='" + datePickerBidding.Text.Trim().Substring(0, 4) + "'";
                    oDataTable = Utilities.GetTable(strCmd);
                    if (oDataTable.Rows.Count == 0) lstStatus.Text += "YearTrans Not Available" + "\r\n";
                    else lstStatus.Text += "YearTrans Available" + "\r\n";

                    strCmd = "select * from Consumed where ppid='" + pidtable.Rows[0][0].ToString().Trim() + "'and fromdate<='" + datePickerBidding.Text.Trim() + "'and todate>='" + datePickerBidding.Text.Trim() + "'";
                    oDataTable = Utilities.GetTable(strCmd);
                    if (oDataTable.Rows.Count == 0) lstStatus.Text += "Consume Not Available" + "\r\n";
                    else lstStatus.Text += "Consume Available" + "\r\n";

                    strCmd = "select * from bourse where  fromdate<='" + datePickerBidding.Text.Trim() + "'and todate>='" + datePickerBidding.Text.Trim() + "'";
                    oDataTable = Utilities.GetTable(strCmd);
                    if (oDataTable.Rows.Count == 0) lstStatus.Text += "Bourse Setting Not Available" + "\r\n";
                    else lstStatus.Text += "Bourse Setting Available" + "\r\n";


                }

            }
            catch
            {

            }




        }

        private void CheckFuelAvailabiliyty()
        {
            string strDate = PersianDateConverter.ToPersianDate(datePickerBidding.SelectedDateTime).ToString("d");

            string strCmd = "select UnitCode,SecondFuel,SecondFuelStartDate,SecondFuelEndDate " +
                " from ConditionUnit inner join PowerPlant on PowerPlant.PPID=ConditionUnit.PPID" +
                " where PowerPlant.ppname=N'" + lblPlantValue.Text.Trim() + "'" +
                " and SecondFuelStartDate<'" + strDate + "' and '" + strDate + "'<SecondFuelEndDate";

            DataTable oDataTable = Utilities.GetTable(strCmd);
            foreach (DataRow myrow in oDataTable.Rows)
            {
                if (myrow["SecondFuel"].ToString() != "" && bool.Parse(myrow["SecondFuel"].ToString()) == true)
                {
                    if (oDataTable.Rows.Count != 0)
                    {
                        //   MessageBox.Show("Test");
                        lstStatus.Text += "*********************units of plant that have second fuel*****************\r\n";
                        lstStatus.Text += myrow["UnitCode"].ToString() + "\r\n";

                        lstStatus.Text += "***************dates of second fuel <<< " + myrow["UnitCode"].ToString().Trim() + " >>> ********************\r\n";
                        lstStatus.Text += myrow["SecondFuelStartDate"].ToString() + "\r\n";

                        lstStatus.Text += myrow["SecondFuelEndDate"].ToString() + "\r\n";
                    }
                }
            }
            ////////////////////
            //lstStatus.Text += "\r\n";
            //lstStatus.Text += "\r\n";

            strCmd = "select UnitCode,Maintenance,MaintenanceStartDate,MaintenanceEndDate " +
                " from ConditionUnit inner join PowerPlant on PowerPlant.PPID=ConditionUnit.PPID" +
                " where PowerPlant.ppname='" + lblPlantValue.Text.Trim() + "'" +
                " and MaintenanceStartDate<'" + strDate + "' and '" + strDate + "'<MaintenanceEndDate";

            oDataTable = Utilities.GetTable(strCmd);

            string[] mm = new string[oDataTable.Rows.Count];
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {
                mm[i] = oDataTable.Rows[i][0].ToString();
            }

            foreach (DataRow myrow1 in oDataTable.Rows)
            {

                if (myrow1["Maintenance"].ToString() != "" && bool.Parse(myrow1["Maintenance"].ToString()) == true)
                {
                    lstStatus.Text += "*********************units of plant that have maintenance***************\r\n";
                    lstStatus.Text += myrow1["UnitCode"].ToString() + "\r\n"; ;

                    lstStatus.Text += "***************dates of maintenance <<<" + myrow1["UnitCode"].ToString().Trim() + " >>>*****************\r\n";
                    lstStatus.Text += myrow1["MaintenanceStartDate"].ToString() + "\r\n"; ;

                    lstStatus.Text += myrow1["MaintenanceEndDate"].ToString() + "\r\n"; ;


                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            lstStatus.Text += "\r\n";
            lstStatus.Text += "\r\n";

            strCmd = "select UnitCode,OutService,OutServiceStartDate,OutServiceEndDate" +
                " from ConditionUnit inner join PowerPlant on PowerPlant.PPID=ConditionUnit.PPID" +
                " where PowerPlant.ppname='" + lblPlantValue.Text.Trim() + "'" +
                " and OutServiceStartDate<'" + strDate + "' and '" + strDate + "'<OutServiceEndDate";

            oDataTable = Utilities.GetTable(strCmd);

            foreach (DataRow myrow1 in oDataTable.Rows)
            {

                if (bool.Parse(myrow1["OutService"].ToString()) == true)
                {
                    lstStatus.Text += "*********************units of plant that have outservice***************\r\n";
                    lstStatus.Text += myrow1["UnitCode"].ToString() + "\r\n";

                    lstStatus.Text += "***************dates of outservice <<<" + myrow1["UnitCode"].ToString().Trim() + " >>>*****************\r\n";
                    lstStatus.Text += myrow1["OutServiceStartDate"].ToString() + "\r\n";

                    lstStatus.Text += myrow1["OutServiceEndDate"].ToString() + "\r\n";


                }
            }
        }

        private void datePickerBidding_SelectedDateTimeChanged(object sender, EventArgs e)
        {
            datePickerCurrent.SelectedDateTime = datePickerBidding.SelectedDateTime.AddDays(-1);
            datePickerCurrent.Text = new PersianDate(datePickerBidding.SelectedDateTime.AddDays(-1)).ToString("d");

            UpdateBiddingTab();
        }

        public void EnableBiddingStrategyTab(bool enable)
        {
            grBoxCustomize.Enabled = enable;
            groupBoxBiddingStrategy1.Enabled = enable;
            groupBoxBiddingStrategy2.Enabled = enable;
            groupBoxBiddingStrategy3.Enabled = enable;
        }
        public void erasecmbcustom()
        {
            if(cmbRunSetting.SelectedItem.ToString().Trim()=="Automatic")
            EnableBiddingStrategycustom();
        }

        public void EnableBiddingStrategycustom()
        {
            cmbStrategy1.Text = "";
            cmbPeak1.Text = "";
            cmbStrategy2.Text = "";
            cmbPeak2.Text = "";
            cmbStrategy3.Text = "";
            cmbPeak3.Text = "";
            cmbStrategy4.Text = "";
            cmbPeak4.Text = "";            
            cmbStrategy5.Text = "";
            cmbPeak5.Text = "";
            cmbStrategy6.Text = "";
            cmbPeak6.Text = "";
            cmbStrategy7.Text = "";
            cmbPeak7.Text = "";
            cmbStrategy8.Text = "";
            cmbPeak8.Text = "";
            cmbStrategy9.Text = "";
            cmbPeak9.Text = "";
            cmbStrategy10.Text = "";
            cmbPeak10.Text = "";
            cmbStrategy11.Text = "";
            cmbPeak11.Text = "";
            cmbStrategy12.Text = "";
            cmbPeak12.Text = "";
            cmbStrategy13.Text = "";
            cmbPeak13.Text = "";
            cmbStrategy14.Text = "";
            cmbPeak14.Text = "";
            cmbStrategy15.Text = "";
            cmbPeak15.Text = "";
            cmbStrategy16.Text = "";
            cmbPeak16.Text = "";
            cmbStrategy17.Text = "";
            cmbPeak17.Text = "";
            cmbStrategy18.Text = "";
            cmbPeak18.Text = "";
            cmbStrategy19.Text = "";
            cmbPeak19.Text = "";
            cmbStrategy20.Text = "";
            cmbPeak20.Text = "";
            cmbStrategy21.Text = "";
            cmbPeak21.Text = "";
            cmbStrategy22.Text = "";
            cmbPeak22.Text = "";
            cmbStrategy23.Text = "";
            cmbPeak23.Text = "";
            cmbStrategy24.Text = "";
            cmbPeak24.Text = "";



           
        }



        public void EnableBiddingStrategyExportButton(bool enable)
        {
            btnExport.Enabled = enable;
          
        }

        private void btnrunseting_Click(object sender, EventArgs e)
        {
            bool enpre = false;
            if (rdBidAllocation.Checked == false)
                enpre = true;

            RunSetting frm = new RunSetting(lblPlantValue.Text.Trim(), enpre);
            frm.Show();
        }
        private static bool MyboolParse(string str)
        {
            if (str.Trim() == "")
                return false;
            else
            {
                try { return bool.Parse(str.Trim()); }
                catch { return false; }
            }
        }

    }
}