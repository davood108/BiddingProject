﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using NRI.SBS.Common;
using FarsiLibrary.Utils;
using System.Windows.Forms;

namespace PowerPlantProject
{
    public static class UtilityPowerPlantFunctions
    {
        public static bool CombinedCyclePowerPlant(string strPPID)
        {
            SqlCommand mycom = new SqlCommand();
            mycom.Connection = new SqlConnection(ConnectionManager.ConnectionString);
            mycom.Connection.Open();
            mycom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id SELECT @result2 =count(PPID) FROM [PPUnit] WHERE PPID=@id AND PackageType LIKE 'Combined Cycle%'";
            mycom.Parameters.Add("@id", SqlDbType.NChar, 10);
            mycom.Parameters["@id"].Value = strPPID;
            mycom.Parameters.Add("@result1", SqlDbType.Int);
            mycom.Parameters["@result1"].Direction = ParameterDirection.Output;
            mycom.Parameters.Add("@result2", SqlDbType.Int);
            mycom.Parameters["@result2"].Direction = ParameterDirection.Output;
            mycom.ExecuteNonQuery();
            int result1 = (int)mycom.Parameters["@result1"].Value;
            int result2 = (int)mycom.Parameters["@result2"].Value;
            if ((result1 > 1) && (result2 > 0))
                return true;
            else
                return false;
        }

        public static double[] Get_1_Day_PowerPlantPrice(int ppId, DataTable dtPackageType, DateTime baseDate, DateTime selectedDate, int packagePriorityIndex, int Count_view, string BidDate)
        {
            int maxAttemp = 3;
            
            double[] prices = new double[24];

            //for (int daysbefore = 0; daysbefore < N_Days; daysbefore++)
            //{

            //DateTime selectedDate = PersianDateConverter.ToGregorianDateTime(dateTime).Subtract(new TimeSpan(daysbefore, 0, 0, 0));
            string date = new PersianDate(selectedDate).ToString("d");
            double[] Factor_Hours = new double[24];
            for (int hour = 0; hour < 8; hour++)
            {
                if (Count_view == 1)
                {
                    Factor_Hours[hour] = 1;
                }
                if (Count_view == 2)
                {
                    Factor_Hours[hour] = 1;
                }
                if (Count_view == 3)
                {
                    Factor_Hours[hour] = 1;
                }
            }
             for (int hour = 8; hour < 24; hour++)
             {
                 if (Count_view == 1)
                 {
                     Factor_Hours[hour] = 1;
                 }
                 if (Count_view == 2)
                 {
                     Factor_Hours[hour] = 1;
                 }
                 if (Count_view == 3)
                 {
                     Factor_Hours[hour] = 1;
                 }
             }
             CMatrix oneDayPrice = CalculatePowerPlantMaxBid(ppId, dtPackageType, selectedDate, packagePriorityIndex, BidDate);

             /////////////////////////////STATUS/////////////////////////////////////////////////////////////////////////
             try
             {
                 string status = valtrain(ppId.ToString(), date);
                 string[] split = status.Split('-');
                 if (status.Contains("Presolve-Unit"))
                 {
                     DataSet myDs1 = UtilityPowerPlantFunctions.GetdefaultUnits(ppId, split[2].Trim());
                     DataTable[] orderedPackages1 = UtilityPowerPlantFunctions.OrderPackages(myDs1);
                     foreach (DataTable dtPackageType1 in orderedPackages1)
                     {
                         int packageOrder = 0;
                         if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                         orderedPackages1.Length > 1)
                             packageOrder = 1;

                         oneDayPrice = CalculatePowerPlantMaxBid(ppId, dtPackageType1, selectedDate, packagePriorityIndex, BidDate);
                     }
                 }
                 else if (status.Contains("Presolve - plant"))
                 {
                     int ppii = int.Parse(split[2].Trim());
                     DataSet myDs1 = UtilityPowerPlantFunctions.GetUnits(ppii);
                     DataTable[] orderedPackages1 = UtilityPowerPlantFunctions.OrderPackages(myDs1);
                     foreach (DataTable dtPackageType1 in orderedPackages1)
                     {
                         int packageOrder = 0;
                         if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                         orderedPackages1.Length > 1)
                             packageOrder = 1;

                         oneDayPrice = CalculatePowerPlantMaxBid(ppii, dtPackageType1, selectedDate, packagePriorityIndex, BidDate);
                     }
                 }
                 else if (status.Contains("date"))
                 {
                     string[] s = status.Split(':');
                     DateTime selectedDate1 = PersianDateConverter.ToGregorianDateTime(s[1].Trim());
                     oneDayPrice = CalculatePowerPlantMaxBid(ppId, dtPackageType, selectedDate1, packagePriorityIndex, BidDate);
                 }
                 else if (status == "defaultprice")
                 {

                     Random rand = new Random();
                     for (int h = 0; h < 24; h++)
                     {
                         double vc = defaultpicefind(ppId.ToString(), h);
                         if (vc > 0)
                         {
                             oneDayPrice[0, h] = vc + (10 * rand.NextDouble());
                         }

                     }
                 }

                 if (oneDayPrice.Zero)
                 {
                     Random rand = new Random();
                     for (int h = 0; h < 24; h++)
                     {
                         double vc = defaultpicefind(ppId.ToString(), h);
                         if (vc > 0)
                         {
                             oneDayPrice[0, h] = vc + (10 * rand.NextDouble());
                         }

                     }

                 }
             }
             catch
             {

             }
             /////////////////////////////////////ENDSTATUS////////////////////////////////////////////////////////////////



            // in case price is null we iterate three times;
            int attemp = 0;
            while ((oneDayPrice == null || oneDayPrice.Zero)
                    && selectedDate <= baseDate && attemp < maxAttemp)
            {
                attemp++;
                selectedDate = selectedDate.Subtract(new TimeSpan(attemp, 0, 0, 0));
                date = new PersianDate(selectedDate).ToString("d");
                oneDayPrice = CalculatePowerPlantMaxBid(ppId, dtPackageType, selectedDate, packagePriorityIndex, BidDate);
            }
            if ((oneDayPrice == null || oneDayPrice.Zero) && selectedDate > baseDate)
            {
                // can read from forcast
                try
                {
                    DataTable forcast = GetForcast(ppId, dtPackageType.TableName, date);
                    if (forcast.Rows.Count > 0)
                    {
                        oneDayPrice = new CMatrix(1, 24);
                        foreach (DataRow row in forcast.Rows)
                        {
                            int hour = Int32.Parse(row["hour"].ToString());
                            double value = MyDoubleParse(row["forecast"].ToString());
                            oneDayPrice[0, hour - 1] = value;
                        }
                    }
                }
                catch
                {

                }
            }
            if (oneDayPrice != null)
            {
                for (int hour = 0; hour < 24; hour++)
                    prices[hour] = oneDayPrice[0, hour] * Factor_Hours[hour];
            }
            else
            {
                MessageBox.Show("No Data for selected Date!");

            }


            return prices;

        }

        public static double[] Get_N_Days_PowerPlantPrices(int N_Days, int ppId, DataTable dtPackageType, DateTime baseDate, DateTime dateTime, int packagePriorityIndex, string BidDate)
        {
            int maxAttemp = 3;

            int maxCount = N_Days * 24;
            double[] prices = new double[maxCount];

           
            for (int daysbefore = 0; daysbefore < N_Days; daysbefore++)
            {
               

                //Application.DoEvents();
                DateTime selectedDate = PersianDateConverter.ToGregorianDateTime(dateTime).Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                string date = new PersianDate(selectedDate).ToString("d");

            
                //CMatrix oneDayPrice = GetOneDayPrices(date);

                CMatrix oneDayPrice = CalculatePowerPlantMaxBid(ppId, dtPackageType, selectedDate, packagePriorityIndex, BidDate);

                /////////////////////////////STATUS/////////////////////////////////////////////////////////////////////////
                try
                {
                    string status = valtrain(ppId.ToString(), date);
                    string[] split = status.Split('-');
                    if (status.Contains("Presolve-Unit"))
                    {
                        DataSet myDs1 = UtilityPowerPlantFunctions.GetdefaultUnits(ppId, split[2].Trim());
                        DataTable[] orderedPackages1 = UtilityPowerPlantFunctions.OrderPackages(myDs1);
                        foreach (DataTable dtPackageType1 in orderedPackages1)
                        {
                            int packageOrder = 0;
                            if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                            orderedPackages1.Length > 1)
                                packageOrder = 1;

                            oneDayPrice = CalculatePowerPlantMaxBid(ppId, dtPackageType1, selectedDate, packagePriorityIndex, BidDate);
                        }
                    }
                    else if (status.Contains("Presolve - plant"))
                    {
                        int ppii = int.Parse(split[2].Trim());
                        DataSet myDs1 = UtilityPowerPlantFunctions.GetUnits(ppii);
                        DataTable[] orderedPackages1 = UtilityPowerPlantFunctions.OrderPackages(myDs1);
                        foreach (DataTable dtPackageType1 in orderedPackages1)
                        {
                            int packageOrder = 0;
                            if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                            orderedPackages1.Length > 1)
                                packageOrder = 1;

                            oneDayPrice = CalculatePowerPlantMaxBid(ppii, dtPackageType1, selectedDate, packagePriorityIndex, BidDate);
                        }
                    }
                    else if (status.Contains("date"))
                    {
                        string[] s = status.Split(':');
                        DateTime selectedDate1 = PersianDateConverter.ToGregorianDateTime(s[1].Trim());
                        oneDayPrice = CalculatePowerPlantMaxBid(ppId, dtPackageType, selectedDate1, packagePriorityIndex, BidDate);
                    }
                    else if (status == "defaultprice")
                    {

                        Random rand = new Random();
                        for (int h = 0; h < 24; h++)
                        {
                            double vc = defaultpicefind(ppId.ToString(), h);
                            if (vc > 0)
                            {
                                oneDayPrice[0, h] = vc + (10 * rand.NextDouble());
                            }

                        }
                    }

                    if (oneDayPrice.Zero)
                    {
                        Random rand = new Random();
                        for (int h = 0; h < 24; h++)
                        {
                            double vc = defaultpicefind(ppId.ToString(), h);
                            if (vc > 0)
                            {
                                oneDayPrice[0, h] = vc + (10 * rand.NextDouble());
                            }

                        }

                    }
                }
                catch
                {
                }
                /////////////////////////////////////ENDSTATUS////////////////////////////////////////////////////////////////
                // in case price is null we iterate three times;
                int attemp = 0;
                while ((oneDayPrice == null || oneDayPrice.Zero)
                        && selectedDate <= baseDate && attemp < maxAttemp)
                {
                    attemp++;
                    selectedDate = PersianDateConverter.ToGregorianDateTime(dateTime).Subtract(new TimeSpan(daysbefore + attemp, 0, 0, 0));
                    date = new PersianDate(selectedDate).ToString("d");
                    oneDayPrice = CalculatePowerPlantMaxBid(ppId, dtPackageType, selectedDate, packagePriorityIndex, BidDate);
                }
                if ((oneDayPrice == null || oneDayPrice.Zero) && selectedDate > baseDate)
                {
                    // can read from forcast

                    try
                    {
                        DataTable forcast = GetForcast(ppId, dtPackageType.TableName, date);
                        if (forcast.Rows.Count > 0)
                        {
                            oneDayPrice = new CMatrix(1, 24);
                            foreach (DataRow row in forcast.Rows)
                            {
                                int hour = Int32.Parse(row["hour"].ToString());
                                double value = MyDoubleParse(row["forecast"].ToString());
                                oneDayPrice[0, hour - 1] = value;
                            }
                        }
                    }
                    catch
                    {

                    }
                }
                if (oneDayPrice != null)
                {
                    for (int hour = 0; hour < 24; hour++)
                        //prices[maxCount - 1 - (24 * daysbefore) - hour] = oneDayPrice[0, hour];
                        prices[maxCount - (24 * daysbefore) - 24 + hour] = oneDayPrice[0, hour];
                }
                else
                {
                    MessageBox.Show("No Data for selected Date!");

                }



            }// end loop for days

            
            

            return prices;

        }


        private static CMatrix GetOneDayPricesForEachUnit(int ppid, string date, string blockM005, string blockM002, string BidDate, string ptype)
        {
            //if (date == "1389/04/18")
            //    date = date;


            double interval = intervalprice(date, BidDate);
            // if (PersianDateConverter.ToGregorianDateTime(date)<= PersianDateConverter.ToGregorianDateTime("1390/02/20")) interval = 3;
            blockM005 = blockM005.Trim();
            blockM002 = blockM002.Trim();

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();



            CMatrix price = new CMatrix(1, 24);
            //////////////////////////////////////////////////////////////////////////
            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + BidDate + "'order by BaseID desc");
            string staterun = "";
            string Marketrule = "";
            string smaxdate = "";
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }
            DataTable basetable = Utilities.GetTable("select ProposalDay from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetable.Rows.Count > 0)
            {
                Marketrule = basetable.Rows[0][0].ToString();
            }
            //////////////////////////////////////////////////////////////////////////
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();

            Myda.SelectCommand = new SqlCommand("SELECT Hour,Required,Economic,Dispatchable FROM [DetailFRM005] WHERE PPID= '" + ppid.ToString() + "'and PPType='" + ptype + "' AND TargetMarketDate=@date AND Block=@block",
                myConnection);
            Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
            Myda.SelectCommand.Parameters["@date"].Value = date;
            Myda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
            Myda.SelectCommand.Parameters["@block"].Value = blockM005;
            Myda.Fill(MyDS);
            DataTable dt = MyDS.Tables[0];
            Myda.SelectCommand.Connection.Close();

            double[] requiredTableba = new double[24];
            double[] EconomicTableba = new double[24];
            double[] DispatchTableba = new double[24];

            double[] requiredTablebi = new double[24];
            double[] EconomicTablebi = new double[24];
            double[] DispatchTablebi = new double[24];

            double[] requiredTable = new double[24];
            double[] EconomicTable = new double[24];
            double[] DispatchTable = new double[24];

            ///////////////////////////////////////////////////////////////
            DataSet MyDSba = new DataSet();
            SqlDataAdapter Mydaba = new SqlDataAdapter();

            Mydaba.SelectCommand = new SqlCommand("SELECT Hour,Required,Economic,Dispatchable FROM [baDetailFRM005] WHERE PPID= '" + ppid.ToString() + "'and PPType='" + ptype + "' AND TargetMarketDate=@date AND Block=@block",
                myConnection);
            Mydaba.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
            Mydaba.SelectCommand.Parameters["@date"].Value = date;
            Mydaba.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
            Mydaba.SelectCommand.Parameters["@block"].Value = blockM005;
            Mydaba.Fill(MyDSba);
            DataTable dtba = MyDSba.Tables[0];
            Mydaba.SelectCommand.Connection.Close();

            ////////////////////////////////////////////////////////////////


            // if price exist
            if ((dt.Rows.Count > 0) && (Marketrule == "Normal"))
            {
                foreach (DataRow MyRow in dt.Rows)
                {
                    int x = int.Parse(MyRow["Hour"].ToString());
                    requiredTable[x - 1] = MyDoubleParse(MyRow["Required"].ToString());
                    EconomicTable[x - 1] = MyDoubleParse(MyRow["Economic"].ToString());

                    //////////////////////////////////change require////////////////////////////////////////////
                    try
                    {

                        DataTable dsd = Utilities.GetTable("select h" + x + " from consumed where PPID='" + ppid + "' and unit='" + blockM005 + "'and '" + date + "'between fromdate and todate");
                        double hub = 0;
                        double consume = 0;
                        if (dsd.Rows.Count > 0)
                        {
                            consume = MyDoubleParse(dsd.Rows[0][0].ToString());
                        }
                        //////////////////////////
                        int mn = int.Parse(date.Substring(5, 2).ToString());
                        DataTable zx = Utilities.GetTable("select H" + ((x).ToString()) + " from dbo.bourse where fromdate<='" + date + "' and todate>='" + date + "' order by fromdate,id desc");
                        if (zx.Rows.Count == 0)
                        {
                             zx = Utilities.GetTable("select H" + ((x).ToString()) + " from dbo.bourse where fromdate<='" + date + "'");
                        }
                        DataTable dsd1 = null;
                        if (zx.Rows.Count > 0)
                        {
                            string c = zx.Rows[0][0].ToString().Trim();
                            if (c == "Medium")
                            {
                                dsd1 = Utilities.GetTable("select M" + mn + "Mid from yeartrans where year='" + date.Substring(0, 4).Trim() + "'and ppid='" + ppid + "'");
                            }
                            else if (c == "Base")
                            {
                                dsd1 = Utilities.GetTable("select M" + mn + "Low from yeartrans where year='" + date.Substring(0, 4).Trim() + "'and ppid='" + ppid + "'");
                            }
                            else if (c == "peak")
                            {
                                dsd1 = Utilities.GetTable("select M" + mn + "Peak from yeartrans where year='" + date.Substring(0, 4).Trim() + "'and ppid='" + ppid + "'");
                            }
                            hub = MyDoubleParse(dsd1.Rows[0][0].ToString()) ;

                        }
                        if (((1 - (consume / 100)) * (1 - (hub / 100))) > 0)
                        {
                            requiredTable[x - 1] = (requiredTable[x - 1]) * ((1 - (consume / 100)) * (1 - (hub / 100)));
                            EconomicTable[x - 1] = (EconomicTable[x - 1]) * ((1 - (consume / 100)) * (1 - (hub / 100)));
                        }
                    }
                    catch
                    {

                    }

                    ////////////////////////////////////////////////////////////////////////////////////////

                    

                    DispatchTable[x - 1] = MyDoubleParse(MyRow["Dispatchable"].ToString());
                }

                /////////// calculate price
                double[] maxBid = new double[24];
                //Detect MaxBid field in MRCurGrid1

                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                string strCmd = "SELECT " +
                " Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                    "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10,DispachableCapacity FROM [DetailFRM002] " +
                    " WHERE TargetMarketDate=@date AND PPType='" + ptype + "'and Block=@block AND PPID='" + ppid.ToString() + "'" +
                    " and ( Estimated is null or Estimated = 0) order by Hour";
                Maxda.SelectCommand = new SqlCommand(strCmd, myConnection);
                Maxda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Maxda.SelectCommand.Parameters["@date"].Value = date;
                Maxda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
                Maxda.SelectCommand.Parameters["@block"].Value = blockM002;

                Maxda.Fill(MaxDS);

                if (MaxDS.Tables[0].Rows.Count == 0 || MaxDS.Tables[0].Rows.Count < 24)
                    return null;
                ///////////////////////////////

                string str = "select price1,price2,price3,price4,price5,price6,price7,price8,price9,price10" +
                " from dbo.DetailFRM002 where ppid='" + ppid.ToString() + "'" +
                " and TargetMarketDate='" + date + "'and PPType='" + ptype + "'" +
                " AND block='" + blockM002 + "' and ( Estimated is null or Estimated = 0) order By Hour";

                DataTable dtCondition = Utilities.GetTable(str);
                //////////////////////

                //////////////////////////////////////////////////////////////////////////
                DataTable basetabledata = Utilities.GetTable("select ProposalHour from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                if (basetabledata.Rows.Count > 0)
                {
                    staterun = basetabledata.Rows[0][0].ToString();
                }
                ////////////////////////////////////////////////////////////////////////// 

                for (int index = 0; index < 24; index++)
                {
                    double sub = 0;
                    double required = requiredTable[index];
                    double statusquantity = required;
                    if (staterun == "Economic")
                    {
                        statusquantity = EconomicTable[index];
                    }
                    if (staterun == "Maximum")
                    {
                        if (EconomicTable[index] > requiredTable[index])
                            statusquantity = EconomicTable[index];
                        else
                            statusquantity = requiredTable[index];
                    }
                    double DispatchM005 = DispatchTable[index];

                    /////////// find the last non zero step of power
                    double power = 0;
                    int s = 10;
                    for (s = 10; s > 0; s--)
                    {
                        string temp = MaxDS.Tables[0].Rows[index]["power" + s.ToString()].ToString().Trim();
                        if (temp != "" && temp != "0")
                        {
                            power = MyDoubleParse(temp);
                            break;
                        }

                    }
                    ////////////////////////////////////////////////

                    double DispachableCapacity = 0;
                    try
                    {
                        DispachableCapacity = MyDoubleParse(MaxDS.Tables[0].Rows[index]["DispachableCapacity"].ToString());
                    }
                    catch { }

                    if ((statusquantity > power) && (s > 0) && (statusquantity != 0))
                    {
                        //maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price" + s.ToString()]);
                        sub = 0;
                    }




                    DataRow MaxRow = MaxDS.Tables[0].Rows[index];
                    if (statusquantity > 0)
                    {
                        int Dsindex = 1;
                        while ((Dsindex <= 10) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) < Math.Ceiling(statusquantity - 0.2) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) != 0.0)))
                            Dsindex++;
                        if (Dsindex == 11)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                        else if (Dsindex == 1)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + Dsindex.ToString()].ToString());
                        else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) == 0)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                        else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) != 0)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex - 0).ToString()].ToString());
                        else
                            maxBid[index] = 0;



                        //////////////////////////next price//////////////////////////
                        //if ((Dsindex <= 10) && (Dsindex != 1))
                        //{
                        //    if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex - 1).ToString()]) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()]) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex).ToString()]) != 0.0))

                        //        maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex).ToString()]);
                        //}
                        //else if ((Dsindex == 1))
                        //{
                        //    if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()]) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()]) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()]) != 0.0))
                        //        maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex + 1).ToString()]);
                        //}

                        ////////////////////////////////////////////////////////////////////


                        //if (statusquantity < MyDoubleParse(MaxDS.Tables[0].Rows[index]["Power1"].ToString()) && statusquantity > 3.0)
                        //{
                        //    maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price1"].ToString());

                        //}

                        //if (maxBid[index] == 0 && s > 0)
                        //{
                        //    //maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price" + (s.ToString()]);
                        //    maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price1"]);
                        //}

                    }
                    else
                    {
                        maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price1"]);
                    }
                    ///////////////////////test
                    /////////////////////test

                    price[0, index] = maxBid[index];
                }
            }
            else if ((dtba.Rows.Count > 0) && (Marketrule == "Fuel Limited"))
            {
                foreach (DataRow MyRowba in dtba.Rows)
                {
                    int x = int.Parse(MyRowba["Hour"].ToString());
                    requiredTable[x - 1] = MyDoubleParse(MyRowba["Required"].ToString());
                    EconomicTable[x - 1] = MyDoubleParse(MyRowba["Economic"].ToString());
                    //////////////////////////////////change require////////////////////////////////////////////
                    try
                    {

                        DataTable dsd = Utilities.GetTable("select h" + x + " from consumed where PPID='" + ppid + "' and unit='" + blockM005 + "'and '" + date + "'between fromdate and todate");
                        double hub = 0;
                        double consume = 0;
                        if (dsd.Rows.Count > 0)
                        {
                            consume = MyDoubleParse(dsd.Rows[0][0].ToString());
                        }
                        //////////////////////////
                        int mn = int.Parse(date.Substring(5, 2).ToString());
                        DataTable zx = Utilities.GetTable("select H" + ((x).ToString()) + " from dbo.bourse where fromdate<='" + date + "' and todate>='" + date + "' order by fromdate,id desc");
                        DataTable dsd1 = null;
                        if (zx.Rows.Count > 0)
                        {
                            string c = zx.Rows[0][0].ToString().Trim();
                            if (c == "Medium")
                            {
                                dsd1 = Utilities.GetTable("select M" + mn + "Mid from yeartrans where year='" + date.Substring(0, 4).Trim() + "'and ppid='" + ppid + "'");
                            }
                            else if (c == "Base")
                            {
                                dsd1 = Utilities.GetTable("select M" + mn + "Low from yeartrans where year='" + date.Substring(0, 4).Trim() + "'and ppid='" + ppid + "'");
                            }
                            else if (c == "peak")
                            {
                                dsd1 = Utilities.GetTable("select M" + mn + "Peak from yeartrans where year='" + date.Substring(0, 4).Trim() + "'and ppid='" + ppid + "'");
                            }
                            hub = MyDoubleParse(dsd1.Rows[0][0].ToString());

                        }
                        if (((1 - (consume / 100)) * (1 - (hub / 100))) > 0)
                        {
                            requiredTable[x - 1] = (requiredTable[x - 1]) * ((1 - (consume / 100)) * (1 - (hub / 100)));
                            EconomicTable[x - 1] = (EconomicTable[x - 1]) * ((1 - (consume / 100)) * (1 - (hub / 100)));
                        }
                    }
                    catch
                    {

                    }

                    ////////////////////////////////////////////////////////////////////////////////////////
                    

                    DispatchTable[x - 1] = MyDoubleParse(MyRowba["Dispatchable"].ToString());
                }

                /////////// calculate price
                double[] maxBid = new double[24];
                //Detect MaxBid field in MRCurGrid1

                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                string strCmd = "SELECT " +
                " Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                    "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10,DispachableCapacity FROM [DetailFRM002] " +
                    " WHERE TargetMarketDate=@date AND PPType='" + ptype + "'and Block=@block AND PPID='" + ppid.ToString() + "'" +
                    " and ( Estimated is null or Estimated = 0) order by Hour";
                Maxda.SelectCommand = new SqlCommand(strCmd, myConnection);
                Maxda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Maxda.SelectCommand.Parameters["@date"].Value = date;
                Maxda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
                Maxda.SelectCommand.Parameters["@block"].Value = blockM002;

                Maxda.Fill(MaxDS);

                if (MaxDS.Tables[0].Rows.Count == 0 || MaxDS.Tables[0].Rows.Count < 24)
                    return null;
                ///////////////////////////////

                string str = "select price1,price2,price3,price4,price5,price6,price7,price8,price9,price10" +
                " from dbo.DetailFRM002 where ppid='" + ppid.ToString() + "'" +
                " and TargetMarketDate='" + date + "'and PPType='" + ptype + "'" +
                " AND block='" + blockM002 + "' and ( Estimated is null or Estimated = 0) order By Hour";

                DataTable dtCondition = Utilities.GetTable(str);
                //////////////////////

                //////////////////////////////////////////////////////////////////////////
                DataTable basetabledata = Utilities.GetTable("select ProposalHour from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                if (basetabledata.Rows.Count > 0)
                {
                    staterun = basetabledata.Rows[0][0].ToString();
                }
                ////////////////////////////////////////////////////////////////////////// 

                for (int index = 0; index < 24; index++)
                {
                    double sub = 0;
                    double required = requiredTable[index];
                    double statusquantity = required;
                    if (staterun == "Economic")
                    {
                        statusquantity = EconomicTable[index];
                    }
                    if (staterun == "Maximum")
                    {
                        if (EconomicTable[index] > requiredTable[index])
                            statusquantity = EconomicTable[index];
                        else
                            statusquantity = requiredTable[index];
                    }
                    double DispatchM005 = DispatchTable[index];

                    /////////// find the last non zero step of power
                    double power = 0;
                    int s = 10;
                    for (s = 10; s > 0; s--)
                    {
                        string temp = MaxDS.Tables[0].Rows[index]["power" + s.ToString()].ToString().Trim();
                        if (temp != "" && temp != "0")
                        {
                            power = MyDoubleParse(temp);
                            break;
                        }

                    }
                    ////////////////////////////////////////////////

                    double DispachableCapacity = 0;
                    try
                    {
                        DispachableCapacity = MyDoubleParse(MaxDS.Tables[0].Rows[index]["DispachableCapacity"].ToString());
                    }
                    catch { }

                    if ((statusquantity > power) && (s > 0) && (statusquantity != 0))
                    {
                        //maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price" + s.ToString()]);
                        sub = 0;
                    }




                    DataRow MaxRow = MaxDS.Tables[0].Rows[index];
                    if (statusquantity > 0)
                    {
                        int Dsindex = 1;
                        while ((Dsindex <= 10) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) < Math.Ceiling(statusquantity-0.2) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) != 0.0)))
                            Dsindex++;
                        if (Dsindex == 11)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                        else if (Dsindex == 1)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + Dsindex.ToString()].ToString());
                        else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) == 0)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                        else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) != 0)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex - 0).ToString()].ToString());
                        else
                            maxBid[index] = 0;



                        //////////////////////////next price//////////////////////////
                        //if ((Dsindex <= 10) && (Dsindex != 1))
                        //{
                        //    if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex - 1).ToString()]) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()]) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex).ToString()]) != 0.0))

                        //        maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex).ToString()]);
                        //}
                        //else if ((Dsindex == 1))
                        //{
                        //    if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()]) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()]) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()]) != 0.0))
                        //        maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex + 1).ToString()]);
                        //}

                        ////////////////////////////////////////////////////////////////////


                        //if (statusquantity < MyDoubleParse(MaxDS.Tables[0].Rows[index]["Power1"].ToString()) && statusquantity > 3.0)
                        //{
                        //    maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price1"].ToString());

                        //}

                        //if (maxBid[index] == 0 && s > 0)
                        //{
                        //    //maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price" + (s.ToString()]);
                        //    maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price1"]);
                        //}

                    }
                    else
                    {
                        maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price1"]);
                    }
                    ///////////////////////test
                    /////////////////////test

                    price[0, index] = maxBid[index] ;
                }
            }
            else if ((dt.Rows.Count > 0) && (dtba.Rows.Count > 0) && (Marketrule == "Elghaee"))
            {
                foreach (DataRow MyRow in dt.Rows)
                {
                    int x = int.Parse(MyRow["Hour"].ToString());
                    requiredTableba[x - 1] = MyDoubleParse(MyRow["Required"].ToString());
                    EconomicTableba[x - 1] = MyDoubleParse(MyRow["Economic"].ToString());
                    DispatchTableba[x - 1] = MyDoubleParse(MyRow["Dispatchable"].ToString());
                }
                foreach (DataRow MyRowba in dtba.Rows)
                {
                    int x = int.Parse(MyRowba["Hour"].ToString());
                    requiredTablebi[x - 1] = MyDoubleParse(MyRowba["Required"].ToString());
                    EconomicTablebi[x - 1] = MyDoubleParse(MyRowba["Economic"].ToString());
                    DispatchTablebi[x - 1] = MyDoubleParse(MyRowba["Dispatchable"].ToString());
                }
                for (int x = 0; x < 24; x++)
                {
                    requiredTable[x] = Math.Min(requiredTableba[x], requiredTablebi[x]);
                    EconomicTable[x] = Math.Min(EconomicTableba[x], EconomicTablebi[x]);
                    DispatchTable[x] = Math.Min(DispatchTableba[x], DispatchTablebi[x]);
                }
                /////////// calculate price
                double[] maxBid = new double[24];
                //Detect MaxBid field in MRCurGrid1

                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                string strCmd = "SELECT " +
                " Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                    "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10,DispachableCapacity FROM [DetailFRM002] " +
                    " WHERE TargetMarketDate=@date AND PPType='" + ptype + "'and Block=@block AND PPID='" + ppid.ToString() + "'" +
                    " and ( Estimated is null or Estimated = 0) order by Hour";
                Maxda.SelectCommand = new SqlCommand(strCmd, myConnection);
                Maxda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Maxda.SelectCommand.Parameters["@date"].Value = date;
                Maxda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
                Maxda.SelectCommand.Parameters["@block"].Value = blockM002;

                Maxda.Fill(MaxDS);

                if (MaxDS.Tables[0].Rows.Count == 0 || MaxDS.Tables[0].Rows.Count < 24)
                    return null;
                ///////////////////////////////

                string str = "select price1,price2,price3,price4,price5,price6,price7,price8,price9,price10" +
                " from dbo.DetailFRM002 where ppid='" + ppid.ToString() + "'" +
                " and TargetMarketDate='" + date + "'and PPType='" + ptype + "'" +
                " AND block='" + blockM002 + "' and ( Estimated is null or Estimated = 0) order By Hour";

                DataTable dtCondition = Utilities.GetTable(str);
                //////////////////////

                //////////////////////////////////////////////////////////////////////////
                DataTable basetabledata = Utilities.GetTable("select ProposalHour from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                if (basetabledata.Rows.Count > 0)
                {
                    staterun = basetabledata.Rows[0][0].ToString();
                }
                ////////////////////////////////////////////////////////////////////////// 

                for (int index = 0; index < 24; index++)
                {

                    double sub = 0;
                    double required = requiredTable[index];
                    double statusquantity = required;
                    if (staterun == "Economic")
                    {
                        statusquantity = EconomicTable[index];
                    }
                    if (staterun == "Maximum")
                    {
                        if (EconomicTable[index] > requiredTable[index])
                            statusquantity = EconomicTable[index];
                        else
                            statusquantity = requiredTable[index];
                    }
                    double DispatchM005 = DispatchTable[index];

                    /////////// find the last non zero step of power
                    double power = 0;
                    int s = 10;
                    for (s = 10; s > 0; s--)
                    {
                        string temp = MaxDS.Tables[0].Rows[index]["power" + s.ToString()].ToString().Trim();
                        if (temp != "" && temp != "0")
                        {
                            power = MyDoubleParse(temp);
                            break;
                        }

                    }
                    ////////////////////////////////////////////////

                    double DispachableCapacity = 0;
                    try
                    {
                        DispachableCapacity = MyDoubleParse(MaxDS.Tables[0].Rows[index]["DispachableCapacity"].ToString());
                    }
                    catch { }

                    if ((statusquantity > power) && (s > 0) && (statusquantity != 0))
                    {
                        //maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price" + s.ToString()]);
                        sub = DispatchM005 - power;
                    }




                    DataRow MaxRow = MaxDS.Tables[0].Rows[index];
                    if (statusquantity > 0)
                    {
                        int Dsindex = 1;
                        while ((Dsindex <= 10) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) < Math.Ceiling(statusquantity - 0.2) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()]) != 0.0)))
                            Dsindex++;
                        if (Dsindex == 11)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                        else if (Dsindex == 1)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + Dsindex.ToString()].ToString());
                        else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) == 0)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                        else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) != 0)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex - 0).ToString()].ToString());
                        else
                            maxBid[index] = 0;



                        //////////////////////////next price//////////////////////////
                        if ((Dsindex <= 10) && (Dsindex != 1))
                        {
                            if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex - 1).ToString()]) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()]) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex).ToString()]) != 0.0))

                                maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex).ToString()]);
                        }
                        else if ((Dsindex == 1))
                        {
                            if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()]) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()]) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()]) != 0.0))
                                maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex + 1).ToString()]);
                        }

                        ////////////////////////////////////////////////////////////////////


                        if (statusquantity < MyDoubleParse(MaxDS.Tables[0].Rows[index]["Power1"].ToString()) && statusquantity > 3.0)
                        {
                            maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price1"].ToString());

                        }

                        if (maxBid[index] == 0 && s > 0)
                        {
                            //maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price" + (s.ToString()]);
                            maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price1"]);
                        }

                    }
                    else
                    {
                        maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price1"]);
                    }
                    ///////////////////////test
                    /////////////////////test

                    price[0, index] = maxBid[index] ;
                }
            }
            else if ((Marketrule == "Elghaee"))
            {
                foreach (DataRow MyRowba in dtba.Rows)
                {
                    int x = int.Parse(MyRowba["Hour"].ToString());
                    requiredTable[x - 1] = MyDoubleParse(MyRowba["Required"].ToString());
                    EconomicTable[x - 1] = MyDoubleParse(MyRowba["Economic"].ToString());
                    DispatchTable[x - 1] = MyDoubleParse(MyRowba["Dispatchable"].ToString());
                }

                /////////// calculate price
                double[] maxBid = new double[24];
                //Detect MaxBid field in MRCurGrid1

                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                string strCmd = "SELECT " +
                " Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                    "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10,DispachableCapacity FROM [DetailFRM002] " +
                    " WHERE TargetMarketDate=@date AND PPType='" + ptype + "'and Block=@block AND PPID='" + ppid.ToString() + "'" +
                    " and ( Estimated is null or Estimated = 0) order by Hour";
                Maxda.SelectCommand = new SqlCommand(strCmd, myConnection);
                Maxda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Maxda.SelectCommand.Parameters["@date"].Value = date;
                Maxda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
                Maxda.SelectCommand.Parameters["@block"].Value = blockM002;

                Maxda.Fill(MaxDS);

                if (MaxDS.Tables[0].Rows.Count == 0 || MaxDS.Tables[0].Rows.Count < 24)
                    return null;
                ///////////////////////////////

                string str = "select price1,price2,price3,price4,price5,price6,price7,price8,price9,price10" +
                " from dbo.DetailFRM002 where ppid='" + ppid.ToString() + "'" +
                " and TargetMarketDate='" + date + "'and PPType='" + ptype + "'" +
                " AND block='" + blockM002 + "' and ( Estimated is null or Estimated = 0) order By Hour";

                DataTable dtCondition = Utilities.GetTable(str);
                //////////////////////

                //////////////////////////////////////////////////////////////////////////
                DataTable basetabledata = Utilities.GetTable("select ProposalHour from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                if (basetabledata.Rows.Count > 0)
                {
                    staterun = basetabledata.Rows[0][0].ToString();
                }
                ////////////////////////////////////////////////////////////////////////// 

                for (int index = 0; index < 24; index++)
                {
                    double sub = 0;
                    double required = requiredTable[index];
                    double statusquantity = required;
                    if (staterun == "Economic")
                    {
                        statusquantity = EconomicTable[index];
                    }
                    if (staterun == "Maximum")
                    {
                        if (EconomicTable[index] > requiredTable[index])
                            statusquantity = EconomicTable[index];
                        else
                            statusquantity = requiredTable[index];
                    }
                    double DispatchM005 = DispatchTable[index];

                    /////////// find the last non zero step of power
                    double power = 0;
                    int s = 10;
                    for (s = 10; s > 0; s--)
                    {
                        string temp = MaxDS.Tables[0].Rows[index]["power" + s.ToString()].ToString().Trim();
                        if (temp != "" && temp != "0")
                        {
                            power = MyDoubleParse(temp);
                            break;
                        }

                    }
                    ////////////////////////////////////////////////

                    double DispachableCapacity = 0;
                    try
                    {
                        DispachableCapacity = MyDoubleParse(MaxDS.Tables[0].Rows[index]["DispachableCapacity"].ToString());
                    }
                    catch { }

                    if ((statusquantity > power) && (s > 0) && (statusquantity != 0))
                    {
                        //maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price" + s.ToString()]);
                        sub = DispatchM005 - power;
                    }




                    DataRow MaxRow = MaxDS.Tables[0].Rows[index];
                    if (statusquantity > 0)
                    {
                        int Dsindex = 1;
                        while ((Dsindex <= 10) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) < Math.Ceiling(statusquantity - 0.2) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()]) != 0.0)))
                            Dsindex++;
                        if (Dsindex == 11)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                        else if (Dsindex == 1)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + Dsindex.ToString()].ToString());
                        else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) == 0)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                        else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) != 0)
                            maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex - 0).ToString()].ToString());
                        else
                            maxBid[index] = 0;



                        //////////////////////////next price//////////////////////////
                        if ((Dsindex <= 10) && (Dsindex != 1))
                        {
                            if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex - 1).ToString()]) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()]) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex).ToString()]) != 0.0))

                                maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex).ToString()]);
                        }
                        else if ((Dsindex == 1))
                        {
                            if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()]) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()]) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()]) != 0.0))
                                maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex + 1).ToString()]);
                        }

                        ////////////////////////////////////////////////////////////////////


                        if (statusquantity < MyDoubleParse(MaxDS.Tables[0].Rows[index]["Power1"].ToString()) && statusquantity > 3.0)
                        {
                            maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price1"].ToString());

                        }

                        if (maxBid[index] == 0 && s > 0)
                        {
                            //maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price" + (s.ToString()]);
                            maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price1"]);
                        }

                    }
                    else
                    {
                        maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price1"]);
                    }
                    ///////////////////////test
                    /////////////////////test

                    price[0, index] = maxBid[index];
                }

            }
            else
                price = null;

            myConnection.Close();
            return price;

        }

        private static DataTable GetForcast(int ppId, string packageType, string date)
        {
            
                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                myConnection.Open();

                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = myConnection;
                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;

                // Insert into FinalForcast table
                MyCom.CommandText = "select FinalForecastHourly.hour, FinalForecastHourly.forecast" +
                        " from FinalForecastHourly inner join FinalForecast" +
                        " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id" +
                        " where FinalForecast.PPId=@ppID AND FinalForecast.packageType=@packageType AND FinalForecast.date=@date" +
                        " order by FinalForecastHourly.hour";

                MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                MyCom.Parameters["@ppID"].Value = ppId.ToString();
                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = date;
                MyCom.Parameters.Add("@packageType", SqlDbType.Char, 20);
                MyCom.Parameters["@packageType"].Value = packageType.ToString();
                Maxda.Fill(MaxDS);

                myConnection.Close();


                return MaxDS.Tables[0];
            
        }

        public static DataTable[] OrderPackages(DataSet myDs)
        {
            DataTable[] orderedPackages = new DataTable[myDs.Tables[0].Rows.Count];
            int count = 0;
            foreach (string typeName in Enum.GetNames(typeof(PackageTypePriority)))
            {
                //////////// have to make sure later whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                if (myDs.Tables.Contains(typeName))
                    orderedPackages[count++] = myDs.Tables[myDs.Tables.IndexOf(typeName)];
            }
            return orderedPackages;
        }

     

        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private static double MyDoubleParse(object obj)
        {
            return MyDoubleParse(obj.ToString());
        }

        public static double[] GetAveragePriceInputMatlab(int N_Days, DateTime dateTime)
        {
            
            int maxCount = N_Days * 24;
            double[] result = new double[maxCount];
            for (int daysbefore = 0; daysbefore < N_Days; daysbefore++)
            {

                DateTime selectedDate = PersianDateConverter.ToGregorianDateTime(dateTime).Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                string date14 = new PersianDate(selectedDate).ToString("d");

                string strComd = "select AcceptedAverage, hour from dbo .AveragePrice" +
               " where Date in " +
               " (select max (date) from dbo .AveragePrice where date<='" + date14.ToString() + "')" +
               " order by hour";

                DataTable dt = Utilities.GetTable(strComd);
                double[] oneday = new double[24];
                foreach (DataRow row in dt.Rows)
                {
                    int hour = Int32.Parse(row["hour"].ToString());
                    double value = MyDoubleParse(row["AcceptedAverage"]);
                    oneday[hour - 1] = value;
                }

                for (int hour = 0; hour < 24; hour++)
                    result[maxCount - 1 - (24 * daysbefore) - hour] = oneday[hour];

            }
            return result;
        }
       


        public static DataSet GetUnits(int ppId)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();


            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT DISTINCT PackageType FROM UnitsDataMain WHERE PPID=@Num", myConnection);
            Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
            Myda.SelectCommand.Parameters["@Num"].Value = ppId;
            Myda.Fill(MyDS, "UnitType");

            foreach (DataRow MyRow in MyDS.Tables["UnitType"].Rows)
            {
                string Utype = MyRow["PackageType"].ToString();
                Utype = Utype.Trim();
                switch (Utype)
                {
                    case "Steam":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Steam%'",
                            myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, PackageTypePriority.Steam.ToString());
                        //DataView sdv = new DataView(MyDS.Tables[PackageTypePriority.Steam.ToString()]);
                        //sdv.Sort = "UnitCode ASC";
                        break;
                    case "Gas":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Gas%'",
                            myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, PackageTypePriority.Gas.ToString());
                        //DataView gdv = new DataView(MyDS.Tables[PackageTypePriority.Gas.ToString()]);
                        //gdv.Sort = "UnitCode";

                        break;
                    default:

                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM [UnitsDataMain] WHERE PPID=@Num AND PackageType LIKE 'CC%'",
                            myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, PackageTypePriority.CC.ToString());
                        //DataView cdv = new DataView(MyDS.Tables[PackageTypePriority.Gas.ToString()]);
                        //cdv.Sort = "UnitCode ASC";
                        break;
                }
            }

            MyDS.Dispose();
            Myda.Dispose();

            myConnection.Close();
            return MyDS;

        }


        public static CMatrix CalculatePowerPlantMaxBid(int ppid, DataTable dtPackageType, DateTime date, int packagePriorityIndex,string BidDate)
        {

            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + BidDate + "'order by BaseID desc");
            string staterun = "";
            string Marketrule = "";
            string smaxdate = "";
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }
            DataTable basetable = Utilities.GetTable("select ProposalDay from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetable.Rows.Count > 0)
            {
                Marketrule = basetable.Rows[0][0].ToString();
            }

            string tname = "dbo.DetailFRM005";
            if (Marketrule == "Fuel Limited") tname = "BaDetailFRM005";



            



            double interval = intervalprice(new PersianDate(date).ToString("d"),BidDate);
           // string smaxdate = "";

            //////////////////////////////////////////////////////////////////////////

            //DataTable dtsmaxdate = utilities.GetTable("select * from dbo.BaseData where Date<='" + BidDate + "'order by BaseID desc");
            //if (dtsmaxdate.Rows.Count > 0)
            //{
            //    string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
            //    int ib = 0;
            //    foreach (DataRow m in dtsmaxdate.Rows)
            //    {
            //        arrbasedata[ib] = m["Date"].ToString();
            //        ib++;
            //    }
            //    smaxdate = buildmaxdate(arrbasedata);
            //}
            //else
            //{
            //    dtsmaxdate = utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
            //    smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            //}

            //------------------capprice---------------------------------------------//
            double cap = 0;
            DataTable basetabledata = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+smaxdate+"'order by BaseID desc");
            if (basetabledata.Rows.Count > 0)
            {
                cap = MyDoubleParse(basetabledata.Rows[0][0].ToString());

            }

            //-----------------------------------------------------------------------//

            CMatrix maxBids = new CMatrix(1, 24);
            double[] MaxAllPlant = CALLPlantPowerPlantMaxBid(date,BidDate);

            foreach (DataRow unitRow in dtPackageType.Rows)
            {
                string package = unitRow["PackageType"].ToString().Trim();
                string unit = unitRow["UnitCode"].ToString().Trim();
                string packagecode = unitRow["PackageCode"].ToString().Trim();

                string ptypenum = "0";
                if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";
               // if (ppid== 232) ptypenum = "0";
                if (Findcconetype(ppid.ToString())) ptypenum = "0";

                string temp = unit.ToLower();
                string ppidWithPriority = (ppid + packagePriorityIndex).ToString();

                if (package.Contains("CC"))
                {
                    temp = temp.Replace("cc", "c");
                    string[] sp = temp.Split('c');
                    temp = sp[0].Trim() + sp[1].Trim();
                    if (temp.Contains("gas"))
                    {
                        temp = temp.Replace("gas", "G");

                    }
                    else
                    {
                        temp = temp.Replace("steam", "S");

                    }                   
                    temp = ppidWithPriority + "-" + temp;
                }
                else if (temp.Contains("gas"))
                {
                    temp = ppidWithPriority + "-" + temp.Replace("gas", "G");
                }
                else
                {
                    temp = ppidWithPriority + "-" + temp.Replace("steam", "S");
                }
                string blockM005 = temp.Trim();

                /////////////////////////////////////

                string blockM002 = unit.ToLower();
                if (package.Contains("CC"))
                {
                    blockM002 = blockM002.Replace("cc", "c");
                    string[] sp = blockM002.Split('c');
                    blockM002 = sp[0].Trim() + sp[1].Trim();
                    if (blockM002.Contains("gas"))
                        blockM002 = blockM002.Replace("gas", "G");
                    else if (blockM002.Contains("steam"))
                        blockM002 = blockM002.Replace("steam", "S");
          
                }
                else
                {
                    if (blockM002.Contains("gas"))
                        blockM002 = blockM002.Replace("gas", "G");
                    else if (blockM002.Contains("steam"))
                        blockM002 = blockM002.Replace("steam", "S");
                }
                blockM002 = blockM002.Trim();

                string strDate = new PersianDate(date).ToString("d");


                CMatrix currentMaxBid = UtilityPowerPlantFunctions.GetOneDayPricesForEachUnit(ppid, strDate, blockM005, blockM002,BidDate,ptypenum);

                for (int h1 = 0; h1 < 24; h1++)
                {
                    DataTable con002 = Utilities.GetTable("select DispachableCapacity from DetailFRM002 where PPID='" + ppid + "'and Block='" + blockM002 + "'and TargetMarketDate='" + strDate + "' and PPType='"+ ptypenum +"'and Hour='" + (h1 + 1) + "'and ( Estimated is null or Estimated = 0)");
                    DataTable con005 = Utilities.GetTable("select Contribution,Dispatchable,Required,Economic from "+tname+" where PPID='" + ppid + "'and Block='" + blockM005 + "'and TargetMarketDate='" + strDate + "'and PPType='" + ptypenum + "' and Hour='" + (h1 + 1) + "'");
                    DataTable conLMP = Utilities.GetTable("select LMP from dbo.BidRunSetting where Plant=(select PPName from dbo.PowerPlant where PPID='" + ppid +"')");
                    if (con005.Rows.Count > 0 && con002.Rows.Count > 0)
                    {
                        double req005 = MyDoubleParse(con005.Rows[0][2].ToString());
                        double dis002 = MyDoubleParse(con002.Rows[0][0].ToString());
                        double dis005 = MyDoubleParse(con005.Rows[0][1].ToString());
                        double economic005 = MyDoubleParse(con005.Rows[0][3].ToString());
                        //string cont005 = con005.Rows[0][0].ToString();
                        string cont005 = "N";


                        if ((MaxAllPlant != null) && ((cont005.Contains("UL"))) && (currentMaxBid != null))
                        {
                            if ((MaxAllPlant[h1] != 0) && (currentMaxBid[0, h1] > MaxAllPlant[h1]))
                            {
                                currentMaxBid[0, h1] = MaxAllPlant[h1];
                            }
                        }

                        if ((MaxAllPlant != null) && (currentMaxBid != null))
                        {
                            bool max = usemax.GetInstance().Ismax;
                            if (max)
                            {
                                currentMaxBid[0, h1] = MaxAllPlant[h1];
                            }
                        }

                        //if (currentMaxBid != null)
                        //{
                        //    if ((MaxAllPlant != null) && (currentMaxBid[0, h1] == cap))
                        //    {
                        //        if (MaxAllPlant[h1] != 0)
                        //        {
                        //            currentMaxBid[0, h1] = MaxAllPlant[h1];
                        //        }
                        //    }
                        //}
                        //if (currentMaxBid != null)
                        //{
                        //    if ((MaxAllPlant != null) && (MyboolParse(conLMP.Rows[0][0].ToString())))
                        //    {
                        //        if (MaxAllPlant[h1] != 0)
                        //        {
                        //            currentMaxBid[0, h1] = MaxAllPlant[h1];
                        //        }
                        //    }
                        //}
                    }
                }

                if (currentMaxBid != null)
                    for (int p = 0; p < 24; p++)
                    {
                        if (maxBids[0, p] < currentMaxBid[0, p])
                            maxBids[0, p] = currentMaxBid[0, p];
                    }
            }



            for (int p = 0; p < 24; p++)
            {
                if (MaxAllPlant != null)
                {
                    if (maxBids[0, p] == 0 && MaxAllPlant[p] != 0)
                    {
                         maxBids[0, p] = MaxAllPlant[p];
                    }
                }
            }

            return maxBids;

        }

        public static double[] CALLPlantPowerPlantMaxBid(DateTime date, string BidDate)
        {
            double interval = intervalprice(new PersianDate(date).ToString("d"), BidDate);
            string smaxdate = "";

            //////////////////////////////////////////////////////////////////////////

            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + BidDate + "'order by BaseID desc");
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }

            //------------------capprice---------------------------------------------//
            double cap = 0;
            DataTable basetabledata = Utilities.GetTable("select MarketPriceMax from dbo.BaseData where Date ='"+smaxdate+"'order by BaseID desc");
            if (basetabledata.Rows.Count > 0)
            {
                cap = MyDoubleParse(basetabledata.Rows[0][0].ToString());

            }

            //-----------------------------------------------------------------------//  
            double[] prices_Max = new double[24];
            double[] prices_Min = new double[24];
            double[] prices_Sum = new double[24];
            double[] prices_Low = new double[24];
            double[] prices_Hi = new double[24];
            double[] prices = new double[24];
            double[] prices_ave = new double[24];
            double[] Count_Price = new double[24];
            double[] Count_Price_Max = new double[24];
            for (int k = 0; k < 24; k++)
            {
                prices_Min[k] = cap;
            }

            DataTable oDataTable = Utilities.GetTable("select distinct PPID from UnitsDataMain ");
            int Plants_Num = oDataTable.Rows.Count;
            string[] plant = new string[Plants_Num];
            for (int il = 0; il < Plants_Num; il++)
            {
                plant[il] = oDataTable.Rows[il][0].ToString().Trim();

            }


            DataTable checkmax = Utilities.GetTable("select * from MaxBidALL where Date='" + new PersianDate(date).ToString("d") + "'");
            if (checkmax.Rows.Count == 0 || checkmax.Rows.Count < 24)
            {
                for (int j = 0; j < Plants_Num; j++)
                {

                    DataSet myDs = UtilityPowerPlantFunctions.GetUnits(int.Parse(plant[j]));
                    PersianDate test1 = new PersianDate(date);
                    DateTime Now = PersianDateConverter.ToGregorianDateTime(test1);

                    DataTable[] orderedPackages = UtilityPowerPlantFunctions.OrderPackages(myDs);
                    foreach (DataTable dtPackageType in orderedPackages)
                    {

                        int packageOrder = 0;
                        if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                        orderedPackages.Length > 1)
                            packageOrder = 1;

                        CMatrix NSelectPrice = UtilityPowerPlantFunctions.ForALLCalculatePowerPlantMaxBid
                               (Convert.ToInt32(plant[j]), dtPackageType, date, packageOrder, BidDate);


                        for (int k = 0; k < 24; k++)
                        {
                            if (NSelectPrice[0, k] != 0)
                            {
                                if (prices_Max[k] < NSelectPrice[0, k])
                                {
                                    prices_Max[k] = NSelectPrice[0, k];
                                }
                                if (prices_Min[k] > NSelectPrice[0, k])
                                {
                                    prices_Min[k] = NSelectPrice[0, k];
                                }
                            }
                        }
                    }
                }
                for (int k = 0; k < 24; k++)
                {
                    if (prices_Min[k] == cap)
                    {
                        prices_Min[k] = 0;
                    }
                }
                for (int k = 0; k < 24; k++)
                {
                    prices_Low[k] = prices_Min[k] * 1;
                    prices_Hi[k] = prices_Max[k] * 0.997;
                    if (Math.Abs(prices_Hi[k] - prices_Low[k]) < 0.2 * cap)
                    {
                        prices_Low[k] = prices_Min[k];
                        prices_Hi[k] = prices_Max[k];
                    }
                }

                for (int j = 0; j < Plants_Num; j++)
                {

                    DataSet myDs = UtilityPowerPlantFunctions.GetUnits(int.Parse(plant[j]));
                    PersianDate test1 = new PersianDate(date);
                    DateTime Now = PersianDateConverter.ToGregorianDateTime(test1);

                    DataTable[] orderedPackages = UtilityPowerPlantFunctions.OrderPackages(myDs);
                    foreach (DataTable dtPackageType in orderedPackages)
                    {

                        int packageOrder = 0;
                        if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                        orderedPackages.Length > 1)
                            packageOrder = 1;


                        CMatrix NSelectPrice = UtilityPowerPlantFunctions.ForALLCalculatePowerPlantMaxBid
                               (Convert.ToInt32(plant[j]), dtPackageType, date, packageOrder, BidDate);

                        for (int k = 0; k < 24; k++)
                        {
                            if ((NSelectPrice[0, k] != 0) && (prices_Hi[k] >= NSelectPrice[0, k]) && (prices_Low[k] <= NSelectPrice[0, k]))
                            {
                                prices_ave[k] = prices_ave[k] + NSelectPrice[0, k];
                                Count_Price[k] = Count_Price[k] + 1;
                            }
                        }
                        for (int k = 0; k < 24; k++)
                        {
                            if (prices_ave[k] == 0)
                            {
                                if ((NSelectPrice[0, k] != 0) && (prices_Max[k] >= NSelectPrice[0, k]) && (prices_Min[k] <= NSelectPrice[0, k]))
                                {
                                    if (prices[k] <= NSelectPrice[0, k])
                                    {
                                        prices[k] = NSelectPrice[0, k];
                                    }
                                }
                            }
                        }
                    }
                }
                for (int k = 0; k < 24; k++)
                {
                    if (prices_ave[k] == 0)
                    {
                        prices_ave[k] = prices[k];
                    }
                    else
                    {
                        prices_ave[k] = prices_ave[k] * Math.Pow(Count_Price[k], -1);
                    }
                }

                for (int i = 0; i < 24; i++)
                {
                    DataTable ddelete = Utilities.GetTable("delete from dbo.MaxBidALL where Date='" + new PersianDate(date).ToString("d") + "'and Hour='" + (i + 1) + "'");
                    DataTable insert = Utilities.GetTable("insert into dbo.MaxBidALL (Date,Hour,MaxAll) values ('" + new PersianDate(date).ToString("d") + "','" + (i + 1) + "','" + prices_ave[i] + "')");
                }
            }
            else if (checkmax.Rows.Count == 24)
            {
                for (int k = 0; k < 24; k++)
                {
                    prices_ave[k] = MyDoubleParse(checkmax.Rows[k]["MaxAll"].ToString());
                }
            }
            return prices_ave;
        }

        public static CMatrix ForALLCalculatePowerPlantMaxBid(int ppid, DataTable dtPackageType, DateTime date, int packagePriorityIndex, string BidDate)
        {
            CMatrix maxBids = new CMatrix(1, 24);

            foreach (DataRow unitRow in dtPackageType.Rows)
            {
                string package = unitRow["PackageType"].ToString().Trim();
                string unit = unitRow["UnitCode"].ToString().Trim();
                string packagecode = unitRow["PackageCode"].ToString().Trim();

                string ptypenum = "0";
                if (unit.Contains("cc") || unit.Contains("CC")) ptypenum = "1";
                //if (ppid == 232) ptypenum = "0";
                if (Findcconetype(ppid.ToString())) ptypenum = "0";

                string temp = unit.ToLower();
                string ppidWithPriority = (ppid + packagePriorityIndex).ToString();

                if (package.Contains("CC"))
                {
                    temp = temp.Replace("cc", "c");
                    string[] sp = temp.Split('c');
                    temp = sp[0].Trim() + sp[1].Trim();
                    if (temp.Contains("gas"))
                    {
                        temp = temp.Replace("gas", "G");

                    }
                    else
                    {
                        temp = temp.Replace("steam", "S");

                    }
                    temp = ppidWithPriority + "-" + temp;
                }
                else if (temp.Contains("gas"))
                {
                    temp = ppidWithPriority + "-" + temp.Replace("gas", "G");
                }
                else
                {
                    temp = ppidWithPriority + "-" + temp.Replace("steam", "S");
                }
                string blockM005 = temp.Trim();

                /////////////////////////////////////

                string blockM002 = unit.ToLower();
                if (package.Contains("CC"))
                {

                    blockM002 = blockM002.Replace("cc", "c");
                    string[] sp = blockM002.Split('c');
                    blockM002 = sp[0].Trim() + sp[1].Trim();
                    if (blockM002.Contains("gas"))
                        blockM002 = blockM002.Replace("gas", "G");
                    else if (blockM002.Contains("steam"))
                        blockM002 = blockM002.Replace("steam", "S");
                }
                else
                {
                    if (blockM002.Contains("gas"))
                        blockM002 = blockM002.Replace("gas", "G");
                    else if (blockM002.Contains("steam"))
                        blockM002 = blockM002.Replace("steam", "S");
                }
                blockM002 = blockM002.Trim();

                string strDate = new PersianDate(date).ToString("d");

                //????????????????????????
                CMatrix currentMaxBid = UtilityPowerPlantFunctions.forallGetOneDayPricesForEachUnit(ppid, strDate, blockM005, blockM002, BidDate,ptypenum);
              
                if (currentMaxBid != null)
                    for (int p = 0; p < 24; p++)
                        if (maxBids[0, p] < currentMaxBid[0, p])
                            maxBids[0, p] = currentMaxBid[0, p];
            }
            return maxBids;
        }

        private static CMatrix forallGetOneDayPricesForEachUnit(int ppid, string date, string blockM005, string blockM002,string BidDate,string ptype)
        {
            //////////////////////////////////////////////////////////////////////////
            DataTable dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where Date<='" + BidDate + "'order by BaseID desc");
            string staterun = "";
            string Marketrule = "";
            string smaxdate = "";
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = Utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }
            DataTable basetable = Utilities.GetTable("select ProposalDay from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            if (basetable.Rows.Count > 0)
            {
                Marketrule = basetable.Rows[0][0].ToString();
            }
            //////////////////////////////////////////////////////////////////////////
            string tname = "dbo.DetailFRM005";
            if (Marketrule == "Fuel Limited") tname = "BaDetailFRM005";



            double interval = intervalprice(date,BidDate);
            //if (PersianDateConverter.ToGregorianDateTime(date) <= PersianDateConverter.ToGregorianDateTime("1390/02/20")) interval = 3;
            blockM005 = blockM005.Trim();
            blockM002 = blockM002.Trim();

            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            
            CMatrix price = new CMatrix(1, 24);

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();

            Myda.SelectCommand = new SqlCommand("SELECT Hour,Required,Contribution,Economic,Dispatchable FROM "+tname+" WHERE PPID= '" + ppid.ToString() + "'and PPType='"+ptype+"' AND TargetMarketDate=@date AND Block=@block",
                myConnection);
            Myda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
            Myda.SelectCommand.Parameters["@date"].Value = date;
            Myda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
            Myda.SelectCommand.Parameters["@block"].Value = blockM005;
            Myda.Fill(MyDS);
            DataTable dt = MyDS.Tables[0];
            Myda.SelectCommand.Connection.Close();

            double[] requiredTable = new double[24];
            string[] ContributionTable = new string[24];
            double[] EconomicTable = new double[24];
            double[] DispatchTable = new double[24];


            DataTable con002 = Utilities.GetTable("select Hour,DispachableCapacity from DetailFRM002 where PPID='" + ppid + "'and Block='" + blockM002 + "'and TargetMarketDate='" + date + "'and PPType='"+ptype+"' and ( Estimated is null or Estimated = 0)");
            double[] DispatchCapacityTable = new double[24];
            if (con002.Rows.Count > 0)
            {
                foreach(DataRow mrow in con002.Rows)
                {
                    int x1 = int.Parse(mrow[0].ToString());
                    DispatchCapacityTable[x1 - 1] = MyDoubleParse(mrow[1].ToString());

                }
            }
           
            
            // if price exist
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow MyRow in dt.Rows)
                {
                    int x = int.Parse(MyRow["Hour"].ToString());
                    requiredTable[x - 1] = MyDoubleParse(MyRow["Required"].ToString());
                    ContributionTable[x - 1] = MyRow["Contribution"].ToString();
                    EconomicTable[x - 1] = MyDoubleParse(MyRow["Economic"].ToString());
                    DispatchTable[x - 1] = MyDoubleParse(MyRow["Dispatchable"].ToString());
                }

                /////////// calculate price
                double[] maxBid = new double[24];
                //Detect MaxBid field in MRCurGrid1

                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                string strCmd = "SELECT " +
                " Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4," +
                    "Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10,DispachableCapacity FROM [DetailFRM002] " +
                    " WHERE TargetMarketDate=@date AND Block=@block and PPType='"+ptype+"' AND PPID='" + ppid.ToString() + "'" +
                    " and ( Estimated is null or Estimated = 0) order by Hour";
                Maxda.SelectCommand = new SqlCommand(strCmd, myConnection);
                Maxda.SelectCommand.Parameters.Add("@date", SqlDbType.Char, 10);
                Maxda.SelectCommand.Parameters["@date"].Value = date;
                Maxda.SelectCommand.Parameters.Add("@block", SqlDbType.NChar, 20);
                Maxda.SelectCommand.Parameters["@block"].Value = blockM002;

                Maxda.Fill(MaxDS);

                if (MaxDS.Tables[0].Rows.Count == 0 || MaxDS.Tables[0].Rows.Count < 24)
                    return null;
                ///////////////////////////////

                string str = "select price1,price2,price3,price4,price5,price6,price7,price8,price9,price10" +
                " from dbo.DetailFRM002 where ppid='" + ppid.ToString() + "'" +
                " and TargetMarketDate='" + date + "'" +
                " AND block='" + blockM002 + "'and PPType='" + ptype + "' and ( Estimated is null or Estimated = 0) order By Hour";

                DataTable dtCondition = Utilities.GetTable(str);
                //////////////////////
               // string smaxdate = "";

                //////////////////////////////////////////////////////////////////////////
                // new 910820
                //DataTable dtsmaxdate = utilities.GetTable("select * from dbo.BaseData where Date<='" + BidDate + "'order by BaseID desc");
                //string staterun = "";
                //if (dtsmaxdate.Rows.Count > 0)
                //{
                //    string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                //    int ib = 0;
                //    foreach (DataRow m in dtsmaxdate.Rows)
                //    {
                //        arrbasedata[ib] = m["Date"].ToString();
                //        ib++;
                //    }
                //    smaxdate = buildmaxdate(arrbasedata);
                //}
                //else
                //{
                //    dtsmaxdate = utilities.GetTable("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                //    smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
                //}
                DataTable basetabledata = Utilities.GetTable("select ProposalHour from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                if (basetabledata.Rows.Count > 0)
                {
                    staterun = basetabledata.Rows[0][0].ToString();
                }

                for (int index = 0; index < 24; index++)
                {

                    double sub = 0;

                    
                    double required = requiredTable[index];
                    double statusquantity = requiredTable[index];
                    if (staterun == "Economic")
                    {
                        statusquantity = EconomicTable[index];
                    }
                    if (staterun == "Maximum")
                    {
                        if (EconomicTable[index] > requiredTable[index])
                            statusquantity = EconomicTable[index];
                        else
                            statusquantity = requiredTable[index];
                    }
                    double economic = EconomicTable[index];
                    double dispatch = DispatchTable[index];
                    string Contribution = ContributionTable[index];
                    double dispatch002 = DispatchCapacityTable[index];
                    double DispatchM005 = DispatchTable[index];


                    if (Contribution == null)
                    {
                        Contribution = "N";
                    }

                    if ((Contribution.Contains("N")))
                        //&& (!blockM005.Contains("G")))
                    {

                        /////////// find the last non zero step of power
                        double power = 0;
                        int s = 10;
                        for (s = 10; s > 0; s--)
                        {
                            string temp = MaxDS.Tables[0].Rows[index]["power" + s.ToString()].ToString().Trim();
                            if (temp != "" && temp != "0")
                            {
                                power = MyDoubleParse(temp);
                                break;
                            }
                        }
                        ////////////

                        double DispachableCapacity = 0;
                        try
                        {
                            DispachableCapacity = MyDoubleParse(MaxDS.Tables[0].Rows[index]["DispachableCapacity"].ToString());
                        }
                        catch { }

                        if ((statusquantity > power) && (s > 0) && (statusquantity != 0))
                        {
                            // maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price" + s.ToString()]);
                            sub = DispatchM005 - power; ;
                        }



                        /////~~~~~~~~~~~~~~~~ BEGIN  FOR PARAND!!!!!!!!!!!!!    ~~~~~~~~~~~~~~~~~~~~~~~~~~
                        //if (DispachableCapacity < required && DispachableCapacity > 0)
                        //{
                        //    maxBid[index] = 0;

                        //    double max = 0;
                        //    if (dtCondition.Rows.Count > 0)
                        //        for (int i = 0; i < 10; i++)
                        //        {
                        //            if (double.Parse(dtCondition.Rows[index][i].ToString()) > max)
                        //                max = double.Parse(dtCondition.Rows[index][i].ToString());
                        //        }

                        //    maxBid[index] = max;
                        //}
                        /////~~~~~~~~~~~~~~~~ END  FOR PARAND!!!!!!!!!!!!!    ~~~~~~~~~~~~~~~~~~~~~~~~~~

                        //else
                        
                            DataRow MaxRow = MaxDS.Tables[0].Rows[index];//pmousavi-97/04/02
                            if (statusquantity > 0)
                            {
                                int Dsindex = 1;
                                while ((Dsindex <= 10) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) < Math.Ceiling(statusquantity - 0.2) && (MyDoubleParse(MaxRow["power" + Dsindex.ToString()]) != 0.0)))
                                    Dsindex++;
                                if (Dsindex == 11)
                                    maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                                else if (Dsindex == 1)
                                    maxBid[index] = MyDoubleParse(MaxRow["price" + Dsindex.ToString()].ToString());
                                else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) == 0)
                                    maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex - 1).ToString()].ToString());
                                else if (MyDoubleParse(MaxRow["power" + Dsindex.ToString()].ToString()) != 0)
                                    maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex - 0).ToString()].ToString());
                                else
                                    maxBid[index] = 0;

                                ///////////////////////////next price/////////////////////////////
                                if ((Dsindex <= 10) && (Dsindex != 1))
                                {
                                    if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex - 1).ToString()]) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()]) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex).ToString()]) != 0.0))

                                        maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex).ToString()]);
                                }
                                else if ((Dsindex == 1))
                                {
                                    if (((Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex).ToString()]) + sub), 1, MidpointRounding.AwayFromZero) + Math.Round((MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()]) + sub), 1, MidpointRounding.AwayFromZero)) / 2) < statusquantity && (MyDoubleParse(MaxRow["power" + (Dsindex + 1).ToString()]) != 0.0))
                                        maxBid[index] = MyDoubleParse(MaxRow["price" + (Dsindex + 1).ToString()]);
                                }

                                ////////////////////////////////////////////////////////////////////

                                if (statusquantity < MyDoubleParse(MaxDS.Tables[0].Rows[index]["Power1"].ToString()) && statusquantity > 3.0)
                                {
                                    maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price1"].ToString());

                                }

                                if (maxBid[index] == 0 && s > 0)
                                {
                                    //maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price" + s.ToString()]);
                                    maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price1"]);
                                }

                                /////////////////////test
                            }
                            else
                            {
                                maxBid[index] = MyDoubleParse(MaxDS.Tables[0].Rows[index]["Price1"]);
                            }
                        price[0, index] = maxBid[index] ;
                        //}
                    }
                }
            }
            else
                price = null;

            myConnection.Close();
            return price;

        }
        private static bool MyboolParse(string str)
        {
            if (str.Trim() == "")
                return false;
            else
            {
                try { return bool.Parse(str.Trim()); }
                catch { return false; }
            }
        }

        private static double intervalprice(string date, string BidDate)
        {

            double interval = 1.0;
            double price1 = 0.0;
            double price2 = 0.0;
            DataTable dt = Utilities.GetTable("select * from dbo.BaseData where Date<='" + date + "' order by BaseID desc");
            DataTable dt2 = Utilities.GetTable("select * from dbo.BaseData where Date<='" + BidDate + "' order by BaseID desc");
            if (dt.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dt.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dt.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                string smaxdate = buildmaxdate(arrbasedata);
                dt = Utilities.GetTable("select * from dbo.BaseData where Date='" + smaxdate + "' order by BaseID desc");
                price1 = MyDoubleParse(dt.Rows[0]["MarketPriceMax"].ToString());
            }
            else
            {
                price1 = 110000;

            }

            ///////////////////////////////////////////////////////////////////////////////

            if (dt2.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dt2.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dt2.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                string smaxdate = buildmaxdate(arrbasedata);
                dt = Utilities.GetTable("select * from dbo.BaseData where Date='" + smaxdate + "' order by BaseID desc");
                price2 = MyDoubleParse(dt2.Rows[0]["MarketPriceMax"].ToString());
            }
            else
            {
                price2 = 383000;
            }

            if (price2 > price1)
            {
                interval = price2 / price1;
            }
            return interval;
        }
        private static string buildmaxdate(string[] arrmax)
        {
            string maxdate = null;
            for (int fd = 0; fd < arrmax.Length; fd++)
            {
                if (maxdate == null || string.Compare(maxdate, arrmax[fd]) < 0)
                    maxdate = arrmax[fd];
            }
            return maxdate;

        }
        public static  bool Findcconetype(string ppid)
        {
            int tr = 0;

           DataTable  oDataTable = Utilities.GetTable("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count == 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }
        public static double defaultpicefind(string ppid, int h)
        {
            string vv = "H" + (h + 1);
            DataTable v = Utilities.GetTable("select " + vv + " from defaultprice where ppid='" + ppid + "'");
            double defaultprice = 0;
            try
            {
                defaultprice = MyDoubleParse(v.Rows[0][0].ToString());
            }
            catch
            {
                defaultprice = 0;
            }

            return defaultprice;
        }

        public static string  valtrain(string ppid,string date)
        {
             string val = "";
             try
             {
                 DataTable dd = Utilities.GetTable("select * from trainingprice where date='" + date + "'and ppid='" + ppid + "'");
                 string plantpre = "";
                 string unitpre = "";
                 string defaultprice = "";
                 string defaultdate = "";

                 if (dd.Rows[0]["plantpresolve"].ToString().Trim() != "")
                 {
                     val = dd.Rows[0]["plantpresolve"].ToString().Trim();
                 }
                 else if (dd.Rows[0]["defaultprice"].ToString().Trim() == "True")
                 {
                     val = "defaultprice";
                 }
                 else if (dd.Rows[0]["unitpresolve"].ToString().Trim() != "")
                 {
                     val = dd.Rows[0]["unitpresolve"].ToString().Trim();
                 }
                 else if (dd.Rows[0]["defaultdate"].ToString().Trim() != "")
                 {
                     val ="date:"+ dd.Rows[0]["defaultdate"].ToString().Trim();
                 }
             }
             catch
             {
             }

                return val;
        }


        public static DataSet GetdefaultUnits(int ppId,string unit)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();


            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT DISTINCT PackageType FROM UnitsDataMain WHERE PPID=@Num and unitcode='"+unit+"'", myConnection);
            Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
            Myda.SelectCommand.Parameters["@Num"].Value = ppId;
            Myda.Fill(MyDS, "UnitType");

            foreach (DataRow MyRow in MyDS.Tables["UnitType"].Rows)
            {
                string Utype = MyRow["PackageType"].ToString();
                Utype = Utype.Trim();
                switch (Utype)
                {
                    case "Steam":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM UnitsDataMain WHERE PPID=@Num AND unitcode='" + unit + "'and PackageType LIKE 'Steam%'",
                            myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, PackageTypePriority.Steam.ToString());
                        //DataView sdv = new DataView(MyDS.Tables[PackageTypePriority.Steam.ToString()]);
                        //sdv.Sort = "UnitCode ASC";
                        break;
                    case "Gas":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM UnitsDataMain WHERE PPID=@Num and unitcode='" + unit + "' AND PackageType LIKE 'Gas%'",
                            myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, PackageTypePriority.Gas.ToString());
                        //DataView gdv = new DataView(MyDS.Tables[PackageTypePriority.Gas.ToString()]);
                        //gdv.Sort = "UnitCode";

                        break;
                    default:

                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM [UnitsDataMain] WHERE PPID=@Num and unitcode='" + unit + "' AND PackageType LIKE 'CC%'",
                            myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, PackageTypePriority.CC.ToString());
                        //DataView cdv = new DataView(MyDS.Tables[PackageTypePriority.Gas.ToString()]);
                        //cdv.Sort = "UnitCode ASC";
                        break;
                }
            }

            MyDS.Dispose();
            Myda.Dispose();

            myConnection.Close();
            return MyDS;

        }

        
    }
}
