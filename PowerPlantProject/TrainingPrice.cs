﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using System.IO;
using Microsoft.Win32;

namespace PowerPlantProject
{
     
    public partial class TrainingPrice : Form
    {
        string [] datee;
        public TrainingPrice()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DefaultPrice m = new DefaultPrice();
            m.Show();
        }
        public void fiill()
        {
            try
            {
                dataGridView1.DataSource = null;
                DataTable cc = Utilities.GetTable("select * from trainingprice where ppid='"+cmbplant.Text.Trim()+"'");
                dataGridView1.DataSource = cc;
            }
            catch
            {
            }
        }
        private void TrainingPrice_Load(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dataGridView1.Columns[0].Width = 17;

                dataGridView1.Columns[0].HeaderText = "";

            

                DataTable dtshow = Utilities.GetTable("select * from dbo.BaseData where Date in (select  max(Date) from dbo.BaseData)order by BaseID desc");
                string ppname = dtshow.Rows[0]["PPName"].ToString();
                string[] c11 = ppname.Split('-');
                DataTable cc = Utilities.GetTable("select ppid from powerplant where ppname='" + c11[0] + "'");


                foreach (Control nc in groupBox2.Controls)
                {
                    if (nc is ComboBox)
                    {
                        nc.Text = "Presolve - plant" + " - " + cc.Rows[0][0].ToString().Trim() + " - " + c11[1];
                    }
                }
                foreach (Control nc in groupBox2.Controls)
                {
                    if (nc is FarsiLibrary.Win.Controls.FADatePicker)
                    {
                        nc.Text = new PersianDate(DateTime.Now).ToString("d");
                    }
                }
                DataTable PlantTable = Utilities.GetTable("select  PowerPlant.PPID, PPUnit.PackageType from dbo.PowerPlant inner join dbo.PPUnit on dbo.PowerPlant.PPID=dbo.PPUnit.PPID");

                for (int i = 0; i < PlantTable.Rows.Count; i++)
                {
                    comboBox2.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox3.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox4.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox5.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox6.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox7.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox8.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox9.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox10.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox11.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox12.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                }
                datee = new string[11];
                label3.Text = new PersianDate(DateTime.Now.AddDays(-7)).ToString("d");
                datee[0] = label3.Text;
                label4.Text = new PersianDate(DateTime.Now.AddDays(-6)).ToString("d");
                datee[1] = label4.Text;
                label5.Text = new PersianDate(DateTime.Now.AddDays(-5)).ToString("d");
                datee[2] = label5.Text;
                label6.Text = new PersianDate(DateTime.Now.AddDays(-4)).ToString("d");
                datee[3] = label6.Text;
                label7.Text = new PersianDate(DateTime.Now.AddDays(-3)).ToString("d");
                datee[4] = label7.Text;
                label8.Text = new PersianDate(DateTime.Now.AddDays(-2)).ToString("d");
                datee[5] = label8.Text;
                label9.Text = new PersianDate(DateTime.Now.AddDays(-1)).ToString("d");
                datee[6] = label9.Text;
                label10.Text = new PersianDate(DateTime.Now.AddDays(0)).ToString("d");
                datee[7] = label10.Text;
                label11.Text = new PersianDate(DateTime.Now.AddDays(1)).ToString("d");
                datee[8] = label11.Text;
                label13.Text = new PersianDate(DateTime.Now.AddDays(2)).ToString("d");
                datee[9] = label13.Text;
                label14.Text = new PersianDate(DateTime.Now.AddDays(3)).ToString("d");
                datee[10] = label14.Text;
                /////////////////////////////////
                DataTable c = Utilities.GetTable("select ppid from powerplant");
                cmbplant.Text = c.Rows[0][0].ToString();
                foreach (DataRow n in c.Rows)
                {
                    cmbplant.Items.Add(n[0].ToString().Trim());

                }
                DataTable v = Utilities.GetTable("select unitcode from UnitsDataMain where ppid='" + cmbplant.Text.Trim() + "'order by unitcode asc");
                foreach (DataRow x in v.Rows)
                {

                    comboBox2.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox3.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox4.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox5.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox6.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox7.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox8.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox9.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox10.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox11.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox12.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());

                }
               
                
                    DataTable cr = Utilities.GetTable("delete from trainingprice where date<'"+datee[0]+"'");
               

                fiill();
                fillcombos();
            }
            catch
            {

            }

          
        }
        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "" || str.Trim() == null)
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }

        private void cmbplant_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtshow = Utilities.GetTable("select * from dbo.BaseData where Date in (select  max(Date) from dbo.BaseData)order by BaseID desc");
                string ppname = dtshow.Rows[0]["PPName"].ToString();
                string[] c11 = ppname.Split('-');
                DataTable cc = Utilities.GetTable("select ppid from powerplant where ppname='" + c11[0] + "'");


                foreach (Control nc in groupBox2.Controls)
                {
                    if (nc is ComboBox)
                    {
                        nc.Text = "Presolve - plant" + " - " + cc.Rows[0][0].ToString().Trim() + " - " + c11[1];
                    }
                }
                foreach (Control nc in groupBox2.Controls)
                {
                    if (nc is FarsiLibrary.Win.Controls.FADatePicker)
                    {
                        nc.Text = new PersianDate(DateTime.Now).ToString("d");
                    }
                }
                comboBox2.Items.Clear();
                comboBox2.Items.Add("DefaultPrice");
                comboBox2.Items.Add("DefaultDate");
                //////
                comboBox3.Items.Clear();
                comboBox3.Items.Add("DefaultPrice");
                comboBox3.Items.Add("DefaultDate");
                /////
                comboBox4.Items.Clear();
                comboBox4.Items.Add("DefaultPrice");
                comboBox4.Items.Add("DefaultDate");
                /////
                comboBox5.Items.Clear();

                comboBox5.Items.Add("DefaultPrice");
                comboBox5.Items.Add("DefaultDate");
                ////
                comboBox6.Items.Clear();

                comboBox6.Items.Add("DefaultPrice");
                comboBox6.Items.Add("DefaultDate");
                ////
                comboBox11.Items.Clear();

                comboBox11.Items.Add("DefaultPrice");
                comboBox11.Items.Add("DefaultDate");
                ////
                comboBox7.Items.Clear();

                comboBox7.Items.Add("DefaultPrice");
                comboBox7.Items.Add("DefaultDate");
                /////
                comboBox8.Items.Clear();

                comboBox8.Items.Add("DefaultPrice");
                comboBox8.Items.Add("DefaultDate");
                /////
                comboBox9.Items.Clear();

                comboBox9.Items.Add("DefaultPrice");
                comboBox9.Items.Add("DefaultDate");
                /////
                comboBox10.Items.Clear();

                comboBox10.Items.Add("DefaultPrice");
                comboBox10.Items.Add("DefaultDate");
                //////
                comboBox12.Items.Clear();

                comboBox12.Items.Add("DefaultPrice");
                comboBox12.Items.Add("DefaultDate");
                /////////
                DataTable PlantTable = Utilities.GetTable("select  PowerPlant.PPID, PPUnit.PackageType from dbo.PowerPlant inner join dbo.PPUnit on dbo.PowerPlant.PPID=dbo.PPUnit.PPID");

                for (int i = 0; i < PlantTable.Rows.Count; i++)
                {
                    comboBox2.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox3.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox4.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox5.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox6.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox7.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox8.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox9.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox10.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox11.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                    comboBox12.Items.Add("Presolve - plant" + " - " + PlantTable.Rows[i][0].ToString().Trim() + " - " + PlantTable.Rows[i][1].ToString().Trim());
                }
                DataTable v = Utilities.GetTable("select unitcode from UnitsDataMain where ppid='" + cmbplant.Text.Trim() + "'order by unitcode asc");
                foreach (DataRow x in v.Rows)
                {

                    comboBox2.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox3.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox4.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox5.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox6.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox7.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox8.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox9.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox10.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox11.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());
                    comboBox12.Items.Add("Presolve-Unit- " + x[0].ToString().Trim());

                }

                fiill();
                fillcombos();
            }
            catch
            {

            }
        }
        public void fillcombos()
        {
            for (int j = 0; j < dataGridView1.Rows.Count;j++ )
            {
                if (label3.Text ==dataGridView1.Rows[j].Cells[2].Value.ToString().Trim())
                {
                    if (dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim()!= "")
                        comboBox2.Text = dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["Defaultprice"].Value.ToString().Trim() == "True")
                        comboBox2.Text = "DefaultPrice";
                    else if (dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim() != "")
                        comboBox2.Text = dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim() != "")
                    {
                        comboBox2.Text = "DefaultDate";
                        faDatePicker2.Text = dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim();
                    }
                }
                /////////////////////////////////////////////
                else if (label4.Text == dataGridView1.Rows[j].Cells[2].Value.ToString().Trim())
                {
                    if (dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim() != "")
                        comboBox3.Text = dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["Defaultprice"].Value.ToString().Trim() == "True")
                        comboBox3.Text = "DefaultPrice";
                    else if (dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim() != "")
                        comboBox3.Text = dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim() != "")
                    {
                        comboBox3.Text = "DefaultDate";
                        faDatePicker3.Text = dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim();
                    }
                }

                //////////////////////////////////////////////
                else if (label5.Text == dataGridView1.Rows[j].Cells[2].Value.ToString().Trim())
                {
                    if (dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim() != "")
                        comboBox4.Text = dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["Defaultprice"].Value.ToString().Trim() == "True")
                        comboBox4.Text = "DefaultPrice";
                    else if (dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim() != "")
                        comboBox4.Text = dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim() != "")
                    {
                        comboBox4.Text = "DefaultDate";
                        faDatePicker4.Text = dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim();
                    }
                }
                /////////////////////////////////////////////////
                else if (label6.Text == dataGridView1.Rows[j].Cells[2].Value.ToString().Trim())
                {
                    if (dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim() != "")
                        comboBox5.Text = dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["Defaultprice"].Value.ToString().Trim() == "True")
                        comboBox5.Text = "DefaultPrice";
                    else if (dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim() != "")
                        comboBox5.Text = dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim() != "")
                    {
                        comboBox5.Text = "DefaultDate";
                        faDatePicker5.Text = dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim();
                    }
                } 
                /////////////////////////////////////////////////
                else if (label7.Text == dataGridView1.Rows[j].Cells[2].Value.ToString().Trim())
                {
                    if (dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim() != "")
                        comboBox6.Text = dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["Defaultprice"].Value.ToString().Trim() == "True")
                        comboBox6.Text = "DefaultPrice";
                    else if (dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim() != "")
                        comboBox6.Text = dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim() != "")
                    {
                        comboBox6.Text = "DefaultDate";
                        faDatePicker6.Text = dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim();
                    }
                }
                ////////////////////////////////////////////////////
                else if (label8.Text == dataGridView1.Rows[j].Cells[2].Value.ToString().Trim())
                {
                    if (dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim() != "")
                        comboBox7.Text = dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["Defaultprice"].Value.ToString().Trim() == "True")
                        comboBox7.Text = "DefaultPrice";
                    else if (dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim() != "")
                        comboBox7.Text = dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim() != "")
                    {
                        comboBox7.Text = "DefaultDate";
                        faDatePicker7.Text = dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim();
                    }
                }
                ///////////////////////////////////////////////////////
                else if (label9.Text == dataGridView1.Rows[j].Cells[2].Value.ToString().Trim())
                {
                    if (dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim() != "")
                        comboBox8.Text = dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["Defaultprice"].Value.ToString().Trim() == "True")
                        comboBox8.Text = "DefaultPrice";
                    else if (dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim() != "")
                        comboBox8.Text = dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim() != "")
                    {
                        comboBox8.Text = "DefaultDate";
                        faDatePicker8.Text = dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim();
                    }
                }
                //////////////////////////////////////////////////////
                else if (label10.Text == dataGridView1.Rows[j].Cells[2].Value.ToString().Trim())
                {
                    if (dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim() != "")
                        comboBox9.Text = dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["Defaultprice"].Value.ToString().Trim() == "True")
                        comboBox9.Text = "DefaultPrice";
                    else if (dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim() != "")
                        comboBox9.Text = dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim() != "")
                    {
                        comboBox9.Text = "DefaultDate";
                        faDatePicker9.Text = dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim();
                    }
                }
                /////////////////////////////////////////////////////////////
                else if (label11.Text == dataGridView1.Rows[j].Cells[2].Value.ToString().Trim())
                {
                    if (dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim() != "")
                        comboBox10.Text = dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["Defaultprice"].Value.ToString().Trim() == "True")
                        comboBox10.Text = "DefaultPrice";
                    else if (dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim() != "")
                        comboBox10.Text = dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim() != "")
                    {
                        comboBox10.Text = "DefaultDate";
                        faDatePicker10.Text = dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim();
                    }
                }
                ////////////////////////////////////////////////////////////
                else if (label13.Text == dataGridView1.Rows[j].Cells[2].Value.ToString().Trim())
                {
                    if (dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim() != "")
                        comboBox11.Text = dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["Defaultprice"].Value.ToString().Trim() == "True")
                        comboBox11.Text = "DefaultPrice";
                    else if (dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim() != "")
                        comboBox11.Text = dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim() != "")
                    {
                        comboBox11.Text = "DefaultDate";
                        faDatePicker11.Text = dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim();
                    }
                }
                ////////////////////////////////////////////////////////////
                else if (label14.Text == dataGridView1.Rows[j].Cells[2].Value.ToString().Trim())
                {
                    if (dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim() != "")
                        comboBox12.Text = dataGridView1.Rows[j].Cells["plantpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["Defaultprice"].Value.ToString().Trim() == "True")
                        comboBox12.Text = "DefaultPrice";
                    else if (dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim() != "")
                        comboBox12.Text = dataGridView1.Rows[j].Cells["Unitpresolve"].Value.ToString().Trim();
                    else if (dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim() != "")
                    {
                        comboBox12.Text = "DefaultDate";
                        faDatePicker12.Text = dataGridView1.Rows[j].Cells["DefaultDate"].Value.ToString().Trim();
                    }
                }

            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = Utilities.GetTable("delete from trainingprice where ppid='" + cmbplant.Text.Trim() + "'");
                for (int b = 0; b <= 10; b++)
                {
                    string date = datee[b];
                    string ppid = cmbplant.Text.Trim();

                  
                    string plantpre = "";
                    string unitpre = "";
                    string defaultprice = "";
                    string defaultdate = "";
                    if (b == 0)
                    {
                        if (comboBox2.Text.Contains("Presolve - plant")) plantpre = comboBox2.Text;
                        else if (comboBox2.Text.Contains("Presolve-Unit-")) unitpre = comboBox2.Text;
                        else if (comboBox2.Text == "DefaultPrice") defaultprice = "True";
                        else if (comboBox2.Text == "DefaultDate") defaultdate = faDatePicker2.Text.Trim();
                    }
                    if (b == 1)
                    {
                        if (comboBox3.Text.Contains("Presolve - plant")) plantpre = comboBox3.Text;
                        else if (comboBox3.Text.Contains("Presolve-Unit-")) unitpre = comboBox3.Text;
                        else if (comboBox3.Text == "DefaultPrice") defaultprice = "True";
                        else if (comboBox3.Text == "DefaultDate") defaultdate = faDatePicker3.Text.Trim();
                    }
                    if (b == 2)
                    {
                        if (comboBox4.Text.Contains("Presolve - plant")) plantpre = comboBox4.Text;
                        else if (comboBox4.Text.Contains("Presolve-Unit-")) unitpre = comboBox4.Text;
                        else if (comboBox4.Text == "DefaultPrice") defaultprice = "True";
                        else if (comboBox4.Text == "DefaultDate") defaultdate = faDatePicker4.Text.Trim();
                    }
                    if (b == 3)
                    {
                        if (comboBox5.Text.Contains("Presolve - plant")) plantpre = comboBox5.Text;
                        else if (comboBox5.Text.Contains("Presolve-Unit-")) unitpre = comboBox5.Text;
                        else if (comboBox5.Text == "DefaultPrice") defaultprice = "True";
                        else if (comboBox5.Text == "DefaultDate") defaultdate = faDatePicker5.Text.Trim();
                    }
                    if (b == 4)
                    {
                        if (comboBox6.Text.Contains("Presolve - plant")) plantpre = comboBox6.Text;
                        else if (comboBox6.Text.Contains("Presolve-Unit-")) unitpre = comboBox6.Text;
                        else if (comboBox6.Text == "DefaultPrice") defaultprice = "True";
                        else if (comboBox6.Text == "DefaultDate") defaultdate = faDatePicker6.Text.Trim();
                    }
                    if (b == 5)
                    {
                        if (comboBox7.Text.Contains("Presolve - plant")) plantpre = comboBox7.Text;
                        else if (comboBox7.Text.Contains("Presolve-Unit-")) unitpre = comboBox7.Text;
                        else if (comboBox7.Text == "DefaultPrice") defaultprice = "True";
                        else if (comboBox7.Text == "DefaultDate") defaultdate = faDatePicker7.Text.Trim();
                    }
                    if (b == 6)
                    {
                        if (comboBox8.Text.Contains("Presolve - plant")) plantpre = comboBox8.Text;
                        else if (comboBox8.Text.Contains("Presolve-Unit-")) unitpre = comboBox8.Text;
                        else if (comboBox8.Text == "DefaultPrice") defaultprice = "True";
                        else if (comboBox8.Text == "DefaultDate") defaultdate = faDatePicker8.Text.Trim();
                    }
                    if (b == 7)
                    {
                        if (comboBox9.Text.Contains("Presolve - plant")) plantpre = comboBox9.Text;
                        else if (comboBox9.Text.Contains("Presolve-Unit-")) unitpre = comboBox9.Text;
                        else if (comboBox9.Text == "DefaultPrice") defaultprice = "True";
                        else if (comboBox9.Text == "DefaultDate") defaultdate = faDatePicker9.Text.Trim();
                    }
                    if (b == 8)
                    {
                        if (comboBox10.Text.Contains("Presolve - plant")) plantpre = comboBox10.Text;
                        else if (comboBox10.Text.Contains("Presolve-Unit-")) unitpre = comboBox10.Text;
                        else if (comboBox10.Text == "DefaultPrice") defaultprice = "True";
                        else if (comboBox10.Text == "DefaultDate") defaultdate = faDatePicker10.Text.Trim();
                    }
                    if (b == 9)
                    {
                        if (comboBox11.Text.Contains("Presolve - plant")) plantpre = comboBox11.Text;
                        else if (comboBox11.Text.Contains("Presolve-Unit-")) unitpre = comboBox11.Text;
                        else if (comboBox11.Text == "DefaultPrice") defaultprice = "True";
                        else if (comboBox11.Text == "DefaultDate") defaultdate = faDatePicker11.Text.Trim();
                    }
                    if (b == 10)
                    {
                        if (comboBox12.Text.Contains("Presolve - plant")) plantpre = comboBox12.Text;
                        else if (comboBox12.Text.Contains("Presolve-Unit-")) unitpre = comboBox12.Text;
                        else if (comboBox12.Text == "DefaultPrice") defaultprice = "True";
                        else if (comboBox12.Text == "DefaultDate") defaultdate = faDatePicker12.Text.Trim();
                    }
                    dt = Utilities.GetTable("insert into trainingprice (ppid,date,plantpresolve,defaultprice,unitpresolve,defaultdate)values('" + ppid + "','" + date + "','" + plantpre + "','" + defaultprice + "','" + unitpre + "','" + defaultdate + "')");
                }

                fiill();
            }
            catch
            {
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowIndex = e.RowIndex;

                string x = dataGridView1.Rows[rowIndex].Cells[e.ColumnIndex].Value.ToString();
                if (x == "System.Drawing.Bitmap")
                {
                    DataTable bb = Utilities.GetTable("delete from trainingprice where ppid='" + cmbplant.Text.Trim() + "' and  date='" + dataGridView1.Rows[rowIndex].Cells[2].Value.ToString() + "'");
                }
                fiill();
            }
            catch
            {

            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.Text == "DefaultDate")
            {
                faDatePicker2.Enabled = true;
            }
        }
        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox3.Text == "DefaultDate")
            {
                faDatePicker3.Enabled = true;
            }
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox4.Text == "DefaultDate")
            {
                faDatePicker4.Enabled = true;
            }
        }
        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox5.Text == "DefaultDate")
            {
                faDatePicker5.Enabled = true;
            }
        }
        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox6.Text == "DefaultDate")
            {
                faDatePicker6.Enabled = true;
            }
        }
        private void comboBox7_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox7.Text == "DefaultDate")
            {
                faDatePicker7.Enabled = true;
            }
        }
        private void comboBox8_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox8.Text == "DefaultDate")
            {
                faDatePicker8.Enabled = true;
            }
        }
        private void comboBox9_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox9.Text == "DefaultDate")
            {
                faDatePicker9.Enabled = true;
            }
        }
        private void comboBox10_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox10.Text == "DefaultDate")
            {
                faDatePicker10.Enabled = true;
            }

        }
        private void comboBox11_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox11.Text == "DefaultDate")
            {
                faDatePicker11.Enabled = true;
            }

        }
        private void comboBox12_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox12.Text == "DefaultDate")
            {
                faDatePicker12.Enabled = true;
            }

        }







        

       
    }
}
