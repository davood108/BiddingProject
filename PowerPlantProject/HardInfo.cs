﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using NRI.SBS.Common;
using System.Security.Cryptography;
using System.Collections;
using System.Management;
using FarsiLibrary.Utils;

namespace PowerPlantProject
{
    public partial class HardInfo : Form
    {
        public HardInfo()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {


                string path = getPath();
                textBox1.Text = path;
                dataGridView1.RowCount = 12;
                dataGridView1.ColumnCount = 2;
                string[] x1 = InsertInfo1("Win32_BaseBoard");
                string[] x2 = InsertInfo1("Win32_Processor");

                for (int i = 0; i < x1.Length; i++)
                {
                    if (x1[i] == " ") x1[i] = "1";
                }
                for (int i = 0; i < x2.Length; i++)
                {
                    if (x2[i] == " ") x2[i] = "1";
                }

                for (int i = 0; i < 12; i++)
                {
                    if (i == 0) dataGridView1.Rows[0].Cells[0].Value = "<<Processor";
                    if (i == 1) dataGridView1.Rows[1].Cells[0].Value = "ProcessorId";
                    if (i == 2) dataGridView1.Rows[2].Cells[0].Value = "SystemName";
                    if (i == 3) dataGridView1.Rows[3].Cells[0].Value = "Version";
                    if (i == 4) dataGridView1.Rows[4].Cells[0].Value = "Revision";
                    if (i == 5) dataGridView1.Rows[5].Cells[0].Value = "Name";
                    if (i == 6) dataGridView1.Rows[6].Cells[0].Value = "<<BaseBoard";
                    if (i == 7) dataGridView1.Rows[7].Cells[0].Value = "SerialNumber";
                    if (i == 8) dataGridView1.Rows[8].Cells[0].Value = "Product";
                    if (i == 9) dataGridView1.Rows[9].Cells[0].Value = "Manufacture";
                    if (i == 10) dataGridView1.Rows[10].Cells[0].Value = "Version";
                    if (i == 11) dataGridView1.Rows[11].Cells[0].Value = "Name";

                    if (i <= 5)
                    {

                        if (i == 0) dataGridView1.Rows[0].Cells[1].Value = "Processor>>";
                        else if (i == 3) dataGridView1.Rows[i].Cells[1].Value = "1";
                        else if (i == 4) dataGridView1.Rows[i].Cells[1].Value = "1";
                        else if (x2[i - 1].Trim() != null) dataGridView1.Rows[i].Cells[1].Value = x2[i - 1].Trim();
                    }
                    else
                    {
                        if (i == 6) dataGridView1.Rows[6].Cells[1].Value = "BaseBoard>>";
                        else if (i == 10) dataGridView1.Rows[i].Cells[1].Value = "1";
                        else if (i >= 7 && x1[i - 7].Trim() != null)
                        {
                            dataGridView1.Rows[i].Cells[1].Value = x1[i - 7].Trim();
                        }

                    }

                }
                if (path == "") path = "C:\\SystemInfo.txt";
                else path += @"\" + "SystemInfo.txt";

                if (File.Exists(path)) File.Delete(path);
                //FileStream fileStream = new FileStream(@path, FileMode.Create);
                TextWriter sw = new StreamWriter(@path);
                int rowcount = 11;
                for (int i = 0; i < rowcount; i++)
                {
                    sw.WriteLine(dataGridView1.Rows[i].Cells[0].Value.ToString() + " : (" + dataGridView1.Rows[i].Cells[1].Value.ToString() + ").\r\n");
                }
                sw.Close();     //Don't Forget Close the TextWriter Object(sw)
                MessageBox.Show("Data Successfully Exported In Path --->" + path);
            }
            catch
            {

            }
        }

        private string getPath()
        {
            DialogResult result = this.folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                return folderBrowserDialog1.SelectedPath;
            }
            return "";
        }
        private string[] InsertInfo1(string Key)
        {
            string[] y = new string[5];
            string[] x = new string[5];
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from " + Key);

            try
            {
                foreach (ManagementObject share in searcher.Get())
                {

                    string grp = "";
                    try
                    {
                        grp = share["Name"].ToString();
                    }
                    catch
                    {
                        grp = share.ToString();
                    }

                    if (share.Properties.Count <= 0)
                    {
                        //MessageBox.Show("No Information Available", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //return;
                    }

                    foreach (PropertyData PC in share.Properties)
                    {

                        ListViewItem item = new ListViewItem(grp);

                        item.Text = PC.Name;

                        if (PC.Value != null && PC.Value.ToString() != "")
                        {
                            switch (PC.Value.GetType().ToString())
                            {
                                case "System.String[]":
                                    string[] str = (string[])PC.Value;

                                    string str2 = "";
                                    foreach (string st in str)
                                        str2 += st + " ";
                                    if (Key == "Win32_BaseBoard")
                                    {
                                        if (item.Text == "SerialNumber") x[0] = str2;
                                        if (item.Text == "Product") x[1] = str2;
                                        if (item.Text == "Manufacturer") x[2] = str2;
                                        if (item.Text == "Version") x[3] = str2;
                                        if (item.Text == "Name") x[4] = str2;

                                    }
                                    if (Key == "Win32_Processor")
                                    {
                                        if (item.Text == "Processorid") y[0] = str2;
                                        if (item.Text == "SystemName") y[1] = str2;
                                        if (item.Text == "Version") y[2] = str2;
                                        if (item.Text == "Revision") y[3] = str2;
                                        if (item.Text == "Name") y[4] = str2;

                                    }
                                    item.SubItems.Add(str2);

                                    break;
                                case "System.UInt16[]":
                                    ushort[] shortData = (ushort[])PC.Value;


                                    string tstr2 = "";
                                    foreach (ushort st in shortData)
                                        tstr2 += st.ToString() + " ";


                                    if (Key == "Win32_BaseBoard")
                                    {
                                        if (item.Text == "SerialNumber") x[0] = tstr2;
                                        if (item.Text == "Product") x[1] = tstr2;
                                        if (item.Text == "Manufacturer") x[2] = tstr2;
                                        if (item.Text == "Version") x[3] = tstr2;
                                        if (item.Text == "Name") x[4] = tstr2;

                                    }
                                    if (Key == "Win32_Processor")
                                    {
                                        if (item.Text == "Processorid") y[0] = tstr2;
                                        if (item.Text == "SystemName") y[1] = tstr2;
                                        if (item.Text == "Version") y[2] = tstr2;
                                        if (item.Text == "Revision") y[3] = tstr2;
                                        if (item.Text == "Name") y[4] = tstr2;

                                    }

                                    item.SubItems.Add(tstr2);

                                    break;

                                default:
                                    item.SubItems.Add(PC.Value.ToString());
                                    if (Key == "Win32_BaseBoard")
                                    {
                                        if (item.Text == "SerialNumber") x[0] = PC.Value.ToString();
                                        if (item.Text == "Product") x[1] = PC.Value.ToString();
                                        if (item.Text == "Manufacturer") x[2] = PC.Value.ToString();
                                        if (item.Text == "Version") x[3] = PC.Value.ToString();
                                        if (item.Text == "Name") x[4] = PC.Value.ToString();

                                    }
                                    if (Key == "Win32_Processor")
                                    {
                                        if (item.Text == "ProcessorId") y[0] = PC.Value.ToString();
                                        if (item.Text == "SystemName") y[1] = PC.Value.ToString();
                                        if (item.Text == "Version") y[2] = PC.Value.ToString();
                                        if (item.Text == "Revision") y[3] = PC.Value.ToString();
                                        if (item.Text == "Name") y[4] = PC.Value.ToString();

                                    }
                                    break;
                            }
                        }
                        else
                        {
                            //if (!DontInsertNull)
                            //    item.SubItems.Add("No Information available");
                            //else
                            //    continue;
                        }


                    }
                }
            }


            catch (Exception exp)
            {
                // MessageBox.Show("can't get data because of the followeing error \n" + exp.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            if (Key == "Win32_BaseBoard") return x;
            else if (Key == "Win32_Processor") return y;
            else return null;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabel1.LinkVisited = true;
            System.Diagnostics.Process.Start("http://www.yahoomail.com");
        }
    }
}
