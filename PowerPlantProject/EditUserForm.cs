﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using FarsiLibrary.Utils;
using System.Security.Cryptography;
using NRI.SBS.Common;
namespace PowerPlantProject
{
    public partial class EditUserForm : Form
    {
        int[] index;
        public EditUserForm()
        {
            InitializeComponent();
            try
            {
                this.BackColor = FormColors.GetColor().Formbackcolor;

                //////////////////////////buttons/////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = FormColors.GetColor().Buttonbackcolor;
                }

                //////////////////////////////panel,groupbox/////////////////////////////;
                foreach (Control c in this.Controls)
                {
                    if (c is Panel || c is GroupBox)
                        c.BackColor = FormColors.GetColor().Panelbackcolor;

                }
                dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = FormColors.GetColor().Panelbackcolor;
                //////////////////////////////////////////////////////////////////////////
                ////////////////////////////////textbox /////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ListBox)
                    {
                        c.BackColor = FormColors.GetColor().Textbackcolor;
                    }
                }

                //////////////////////////////////////////////////////////////////////////////
                ////////////////////////////grid//////////////////////////////////////////////////

                dataGridView1.RowsDefaultCellStyle.BackColor = FormColors.GetColor().Gridbackcolor;
                dataGridView1.RowsDefaultCellStyle.SelectionBackColor = FormColors.GetColor().Gridbackcolor;
                ////////////////////////////////////////////////////////////////////////////////
            }
            catch
            {

            }
        }

        private void EditUserForm_Load(object sender, EventArgs e)
        {

            label1.Text = "Dear User  (  " + User.getUser().Username + "  ) : If You Want To Edit Users Account Please Change Items Below And Press Button.";
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            fillgrid();
        

        }

        private void fillgrid()
        {

            dataGridView1.DataSource = null;
           DataTable dt=Utilities.GetTable("SELECT ID,FirstName,LastName,Username,Role " +
                "FROM login where(Role !='nriAdministrator')");
             index=new int[dt.Rows.Count];


           dataGridView1.RowCount = dt.Rows.Count+1;
           int i=0;
           foreach (DataRow m in dt.Rows)
           {
              
               dataGridView1.Rows[i].Cells[0].Value=m["FirstName"].ToString().Trim();
               dataGridView1.Rows[i].Cells[1].Value = m["LastName"].ToString().Trim();
               dataGridView1.Rows[i].Cells[2].Value = m["Username"].ToString().Trim();
               dataGridView1.Rows[i].Cells[3].Value = m["Role"].ToString().Trim();
               index[i] =int.Parse(m["ID"].ToString().Trim());
               i++;
           }

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
         
            try
            {
                dataGridView1.CurrentRow.DefaultCellStyle.ForeColor = Color.Black;
                dataGridView1.AllowUserToDeleteRows = false;
               

                string first = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                string last = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                string user = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                string role = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                int i = dataGridView1.CurrentCell.RowIndex;
                dataGridView1.CurrentRow.Cells[3].ReadOnly = true;
                

                if (dataGridView1.CurrentCell.Value.ToString().Trim() == "Edit")
                {
                    dataGridView1.ReadOnly = false;
                    dataGridView1.CurrentRow.DefaultCellStyle.ForeColor = Color.Red;
                  
                    DataTable otable = Utilities.GetTable("update  dbo.login set FirstName='" + first + "' , LastName='" + last + "',Username='" + user + "',Role='" + role + "' where ID='" + index[i] + "'");
                   
                    fillgrid();
                   
                    
                }
                if (dataGridView1.CurrentCell.Value.ToString().Trim() == "Delete")
                {
                    dataGridView1.ReadOnly = true;   
                    DataTable otable = Utilities.GetTable("delete from dbo.login where FirstName='"+first+"'and LastName='"+last+"' and Username='"+user+"' and Role='"+role+"'and ID='"+index[i]+"'");
                    fillgrid();
                }

                
            }
            catch
            {
                dataGridView1.ReadOnly = true;
            }

            //dataGridView1.CurrentRow.DefaultCellStyle.ForeColor = Color.Black; ;

        }
    }
}
