﻿namespace PowerPlantProject
{
    partial class InternetAddress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InternetAddress));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUnitnetcomp = new System.Windows.Forms.TextBox();
            this.txtLinenetcomp = new System.Windows.Forms.TextBox();
            this.txtReginnetcomp = new System.Windows.Forms.TextBox();
            this.txtInterchangedEnergy = new System.Windows.Forms.TextBox();
            this.txtManategh = new System.Windows.Forms.TextBox();
            this.txtProduceEnergy = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtrep12 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbunit = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.cmblinenet = new System.Windows.Forms.ComboBox();
            this.cmbreginnet = new System.Windows.Forms.ComboBox();
            this.cmbinterchange = new System.Windows.Forms.ComboBox();
            this.cmbmanategh = new System.Windows.Forms.ComboBox();
            this.cmbproduce = new System.Windows.Forms.ComboBox();
            this.cmbrep = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Unitnetcomp :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Linenetcomp :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Region netcomp :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 225);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "InterchangedEnergy :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(52, 271);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Manategh :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 319);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "ProduceEnergy :";
            // 
            // txtUnitnetcomp
            // 
            this.txtUnitnetcomp.Location = new System.Drawing.Point(132, 78);
            this.txtUnitnetcomp.Name = "txtUnitnetcomp";
            this.txtUnitnetcomp.Size = new System.Drawing.Size(540, 20);
            this.txtUnitnetcomp.TabIndex = 1;
            this.txtUnitnetcomp.Text = "http://sccisrep.igmc.ir/";
            this.txtUnitnetcomp.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtUnitnetcomp_MouseClick);
            // 
            // txtLinenetcomp
            // 
            this.txtLinenetcomp.Location = new System.Drawing.Point(132, 122);
            this.txtLinenetcomp.Name = "txtLinenetcomp";
            this.txtLinenetcomp.Size = new System.Drawing.Size(540, 20);
            this.txtLinenetcomp.TabIndex = 3;
            this.txtLinenetcomp.Text = "http://sccisrep.igmc.ir/";
            // 
            // txtReginnetcomp
            // 
            this.txtReginnetcomp.Location = new System.Drawing.Point(132, 176);
            this.txtReginnetcomp.Name = "txtReginnetcomp";
            this.txtReginnetcomp.Size = new System.Drawing.Size(540, 20);
            this.txtReginnetcomp.TabIndex = 5;
            this.txtReginnetcomp.Text = "http://sccisrep.igmc.ir/";
            // 
            // txtInterchangedEnergy
            // 
            this.txtInterchangedEnergy.Location = new System.Drawing.Point(132, 222);
            this.txtInterchangedEnergy.Name = "txtInterchangedEnergy";
            this.txtInterchangedEnergy.Size = new System.Drawing.Size(540, 20);
            this.txtInterchangedEnergy.TabIndex = 7;
            this.txtInterchangedEnergy.Text = "http://sccisrep.igmc.ir/";
            // 
            // txtManategh
            // 
            this.txtManategh.Location = new System.Drawing.Point(132, 271);
            this.txtManategh.Name = "txtManategh";
            this.txtManategh.Size = new System.Drawing.Size(540, 20);
            this.txtManategh.TabIndex = 9;
            this.txtManategh.Text = "http://sccisrep.igmc.ir/";
            // 
            // txtProduceEnergy
            // 
            this.txtProduceEnergy.Location = new System.Drawing.Point(132, 316);
            this.txtProduceEnergy.Name = "txtProduceEnergy";
            this.txtProduceEnergy.Size = new System.Drawing.Size(540, 20);
            this.txtProduceEnergy.TabIndex = 11;
            this.txtProduceEnergy.Text = "http://sccisrep.igmc.ir/";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(268, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "if date is without seperator  write Date without seperator";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(333, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Date :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label10.Location = new System.Drawing.Point(678, 81);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 15);
            this.label10.TabIndex = 7;
            this.label10.Text = ".xls";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label11.Location = new System.Drawing.Point(678, 125);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 15);
            this.label11.TabIndex = 7;
            this.label11.Text = ".xls";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label12.Location = new System.Drawing.Point(678, 176);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 15);
            this.label12.TabIndex = 7;
            this.label12.Text = ".xls";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label13.Location = new System.Drawing.Point(678, 225);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 15);
            this.label13.TabIndex = 7;
            this.label13.Text = ".xls";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label14.Location = new System.Drawing.Point(678, 271);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 15);
            this.label14.TabIndex = 7;
            this.label14.Text = ".xls";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label15.Location = new System.Drawing.Point(678, 316);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 15);
            this.label15.TabIndex = 7;
            this.label15.Text = ".xls";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(385, 26);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(156, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Date Without Seperator(8char)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label8.Location = new System.Drawing.Point(678, 359);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 15);
            this.label8.TabIndex = 11;
            this.label8.Text = ".xls";
            // 
            // txtrep12
            // 
            this.txtrep12.Location = new System.Drawing.Point(132, 359);
            this.txtrep12.Name = "txtrep12";
            this.txtrep12.Size = new System.Drawing.Size(540, 20);
            this.txtrep12.TabIndex = 13;
            this.txtrep12.Text = "http://sccisrep.igmc.ir/";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(27, 362);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 13);
            this.label16.TabIndex = 9;
            this.label16.Text = "Rep12Pages :";
            // 
            // cmbunit
            // 
            this.cmbunit.FormattingEnabled = true;
            this.cmbunit.Items.AddRange(new object[] {
            "مناطق",
            "توليد",
            "تبادل",
            "Regions_PowerPlants",
            "Lines",
            "PowerPlants_Units_A",
            "گزارش 12 برگي"});
            this.cmbunit.Location = new System.Drawing.Point(825, 75);
            this.cmbunit.Name = "cmbunit";
            this.cmbunit.Size = new System.Drawing.Size(121, 21);
            this.cmbunit.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(739, 78);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 13);
            this.label17.TabIndex = 13;
            this.label17.Text = "SheetName :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(739, 127);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(69, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "SheetName :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(739, 179);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 13);
            this.label19.TabIndex = 13;
            this.label19.Text = "SheetName :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(739, 225);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 13);
            this.label20.TabIndex = 13;
            this.label20.Text = "SheetName :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(739, 274);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(69, 13);
            this.label21.TabIndex = 13;
            this.label21.Text = "SheetName :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(739, 323);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(69, 13);
            this.label22.TabIndex = 13;
            this.label22.Text = "SheetName :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(739, 366);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(69, 13);
            this.label23.TabIndex = 13;
            this.label23.Text = "SheetName :";
            // 
            // cmblinenet
            // 
            this.cmblinenet.FormattingEnabled = true;
            this.cmblinenet.Items.AddRange(new object[] {
            "مناطق",
            "توليد",
            "تبادل",
            "Regions_PowerPlants",
            "Lines",
            "PowerPlants_Units_A",
            "گزارش 12 برگي"});
            this.cmblinenet.Location = new System.Drawing.Point(825, 122);
            this.cmblinenet.Name = "cmblinenet";
            this.cmblinenet.Size = new System.Drawing.Size(121, 21);
            this.cmblinenet.TabIndex = 4;
            // 
            // cmbreginnet
            // 
            this.cmbreginnet.FormattingEnabled = true;
            this.cmbreginnet.Items.AddRange(new object[] {
            "مناطق",
            "توليد",
            "تبادل",
            "Regions_PowerPlants",
            "Lines",
            "PowerPlants_Units_A",
            "گزارش 12 برگي"});
            this.cmbreginnet.Location = new System.Drawing.Point(825, 170);
            this.cmbreginnet.Name = "cmbreginnet";
            this.cmbreginnet.Size = new System.Drawing.Size(121, 21);
            this.cmbreginnet.TabIndex = 6;
            // 
            // cmbinterchange
            // 
            this.cmbinterchange.FormattingEnabled = true;
            this.cmbinterchange.Items.AddRange(new object[] {
            "مناطق",
            "توليد",
            "تبادل",
            "Regions_PowerPlants",
            "Lines",
            "PowerPlants_Units_A",
            "گزارش 12 برگي"});
            this.cmbinterchange.Location = new System.Drawing.Point(825, 222);
            this.cmbinterchange.Name = "cmbinterchange";
            this.cmbinterchange.Size = new System.Drawing.Size(121, 21);
            this.cmbinterchange.TabIndex = 8;
            // 
            // cmbmanategh
            // 
            this.cmbmanategh.FormattingEnabled = true;
            this.cmbmanategh.Items.AddRange(new object[] {
            "مناطق",
            "توليد",
            "تبادل",
            "Regions_PowerPlants",
            "Lines",
            "PowerPlants_Units_A",
            "گزارش 12 برگي"});
            this.cmbmanategh.Location = new System.Drawing.Point(825, 268);
            this.cmbmanategh.Name = "cmbmanategh";
            this.cmbmanategh.Size = new System.Drawing.Size(121, 21);
            this.cmbmanategh.TabIndex = 10;
            // 
            // cmbproduce
            // 
            this.cmbproduce.FormattingEnabled = true;
            this.cmbproduce.Items.AddRange(new object[] {
            "مناطق",
            "توليد",
            "تبادل",
            "Regions_PowerPlants",
            "Lines",
            "PowerPlants_Units_A",
            "گزارش 12 برگي"});
            this.cmbproduce.Location = new System.Drawing.Point(825, 316);
            this.cmbproduce.Name = "cmbproduce";
            this.cmbproduce.Size = new System.Drawing.Size(121, 21);
            this.cmbproduce.TabIndex = 12;
            // 
            // cmbrep
            // 
            this.cmbrep.FormattingEnabled = true;
            this.cmbrep.Items.AddRange(new object[] {
            "مناطق",
            "توليد",
            "تبادل",
            "Regions_PowerPlants",
            "Lines",
            "PowerPlants_Units_A",
            "گزارش 12 برگي"});
            this.cmbrep.Location = new System.Drawing.Point(825, 359);
            this.cmbrep.Name = "cmbrep";
            this.cmbrep.Size = new System.Drawing.Size(121, 21);
            this.cmbrep.TabIndex = 14;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.CadetBlue;
            this.button1.BackgroundImage = global::PowerPlantProject.Properties.Resources.Savedata;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(434, 423);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 27);
            this.button1.TabIndex = 2;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // InternetAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.ClientSize = new System.Drawing.Size(958, 477);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.cmbrep);
            this.Controls.Add(this.cmbproduce);
            this.Controls.Add(this.cmbmanategh);
            this.Controls.Add(this.cmbinterchange);
            this.Controls.Add(this.cmbreginnet);
            this.Controls.Add(this.cmblinenet);
            this.Controls.Add(this.cmbunit);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtrep12);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtProduceEnergy);
            this.Controls.Add(this.txtManategh);
            this.Controls.Add(this.txtInterchangedEnergy);
            this.Controls.Add(this.txtReginnetcomp);
            this.Controls.Add(this.txtLinenetcomp);
            this.Controls.Add(this.txtUnitnetcomp);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "InternetAddress";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InternetAddress";
            this.Load += new System.EventHandler(this.InternetAddress_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtUnitnetcomp;
        private System.Windows.Forms.TextBox txtLinenetcomp;
        private System.Windows.Forms.TextBox txtReginnetcomp;
        private System.Windows.Forms.TextBox txtInterchangedEnergy;
        private System.Windows.Forms.TextBox txtManategh;
        private System.Windows.Forms.TextBox txtProduceEnergy;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtrep12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbunit;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cmblinenet;
        private System.Windows.Forms.ComboBox cmbreginnet;
        private System.Windows.Forms.ComboBox cmbinterchange;
        private System.Windows.Forms.ComboBox cmbmanategh;
        private System.Windows.Forms.ComboBox cmbproduce;
        private System.Windows.Forms.ComboBox cmbrep;
    }
}