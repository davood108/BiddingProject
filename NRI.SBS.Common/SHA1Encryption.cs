﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace NRI.SBS.Common
{
    public class SHA1Encryption
    {
        public static string Encrypt(string text)
        {
            SHA1Managed sh = new SHA1Managed();
            byte[] hash = sh.ComputeHash(System.Text.UnicodeEncoding.Unicode.GetBytes(text));

            string hashedPassword = "";
            foreach (byte b in hash)
                hashedPassword += b.ToString();
            return hashedPassword;
        }
    }
}
