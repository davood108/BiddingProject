﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NRI.SBS.Common
{
    public class User
    {
        private string firstName;

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        private string lastName;

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        private string username;

        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        private string hashedPassword;

        public string HashedPassword
        {
            get { return hashedPassword; }
            set { hashedPassword = value; }
        }
        private DefinedRoles role;

        public DefinedRoles Role
        {
            get { return role; }
            set { role = value; }
        }
        private bool deleted;

        public bool Deleted
        {
            get { return deleted; }
            set { deleted = value; }
        }
        
        ///////////////////////////////////////

        static User self = null;

        private User()
        {

        }

        public static User getUser()
        {
            if (self == null)
                self = new User();
            return self;
        }
        
        public static void removeUser()
        {
            self = null;
        }

    }
}
