﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace NRI.SBS.Common
{

    public class ConnectionManager
    {
        public static string GetConnectionString()
        {
            try
            {
                
                CConfigDBXMLController xmlController = new CConfigDBXMLController();

                CDBConfigurationParams ConfigDBObj = xmlController.ReadDBConfig();
                string ServerConnectionString = "Server=" + ConfigDBObj.ServerName +
                    "; initial catalog=" + ConfigDBObj.ServerDBName +
                    "; timeout=" + ConfigDBObj.ServerDBTimeout.ToString() +
                    "; User ID=" + NRI.SBS.Common.RijndaelEncoding.Decrypt(ConfigDBObj.ServerUserName) +
                    "; Password=" + NRI.SBS.Common.RijndaelEncoding.Decrypt(ConfigDBObj.ServerUserPassword);
                return ServerConnectionString;
            }
            catch (System.IO.FileNotFoundException fex)
            {
                throw fex;
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

       
        public static string ConnectionString = GetConnectionString();

        //private SqlConnection connection = null;

        //public SqlConnection Connection
        //{
        //    get { return connection; }
        //    set { connection = value; }
        //}
        //private ConnectionManager()
        //{
        //    connection = new SqlConnection();
        //    connection.ConnectionString = "Data Source=.;Initial Catalog=PowerPalntDB;Integrated Security=True";
        //}

        //private static ConnectionManager self;

        //public static ConnectionManager GetInstance()
        //{
        //    if (self==null)
        //        self = new ConnectionManager();
        //    if (self.Connection == null)
        //        self.connection = new SqlConnection("Data Source=.;Initial Catalog=PowerPalntDB;Integrated Security=True");
        //    if (self.connection.State == System.Data.ConnectionState.Closed)
        //        self.connection.Open();
        //    return self;
        //}
    }

}
