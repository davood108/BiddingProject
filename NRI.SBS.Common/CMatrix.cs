using System;
using System.Collections.Generic;
using System.Text;

namespace NRI.SBS.Common
{
    public class CMatrix
    {
        private int rows;
        private int cols;
        private double[,] matrix;

        public int Rows
        {
            get { return rows; }
        }
        public int Cols
        {
            get { return cols; }
        }

        public bool Zero
        {
            get { return AllMembersEqualToZero(); }
        }

        private bool AllMembersEqualToZero()
        {
            for (int i=0; i<this.rows; i++)
                for (int j=0; j<this.cols; j++)
                    if (matrix[i,j]!=0)
                        return false;
            return true;
        }
        public CMatrix(int rowSize, int colSize)
        {
            this.rows = rowSize;
            this.cols = colSize;

            matrix = new double[this.rows, this.cols];
        }

        //public Matrix2Dimensional(double[,] array)
        //{
        //    this.rows = array.GetLength(0);
        //    this.cols = array.GetLength(1);

        //    matrix = array;
        //}

        public CMatrix(double[,] elements)
		{
			rows=elements.GetLength(0);
			cols=elements.GetLength(1);;
            matrix = new double[rows, cols];
			for(int i=0;i<elements.GetLength(0);i++)
			{
				for(int j=0;j<elements.GetLength(1);j++)
				{
					this[i,j]= elements[i,j];
				}
			}
		}
        /// <summary>
        /// Indexer
        /// </summary>
        public double this[int iRow, int iCol]		// matrix's index starts at 0,0
        {
            get { return GetElement(iRow, iCol); }
            set { SetElement(iRow, iCol, value); }
        }

        /// <summary>
        /// Internal functions for getting/setting values
        /// </summary>
        private double GetElement(int iRow, int iCol)
        {
            if (iRow < 0 || iRow > rows - 1 || iCol < 0 || iCol > cols - 1)
                throw new MatrixException("Invalid index specified");
            return matrix[iRow, iCol];
        }

        private void SetElement(int iRow, int iCol, double value)
        {
            if (iRow < 0 || iRow > rows - 1 || iCol < 0 || iCol > cols - 1)
                throw new MatrixException("Invalid index specified");
            matrix[iRow, iCol] = value;
        }
        /*
        public static Matrix2Dimensional operator +(Matrix2Dimensional m1, Matrix2Dimensional m2)
        {
            Matrix2Dimensional result = null;
            if (CheckSameDimension(m1, m2))
            {
                result = new Matrix2Dimensional(m1.Rows, m1.Cols);

                for (int i=0; i<m1.Rows; i++)
                    for (int j=0; j<m1.Cols; j++)
                        result[i,j] = m1[i,j] + m2[i,j];

            }
            return result;
        }

        public static Matrix2Dimensional operator -(Matrix2Dimensional m1, Matrix2Dimensional m2)
        {
            Matrix2Dimensional result = null;
            if (CheckSameDimension(m1, m2))
            {
                result = new Matrix2Dimensional(m1.Rows, m1.Cols);

                for (int i = 0; i < m1.Rows; i++)
                    for (int j = 0; j < m1.Cols; j++)
                        result[i,j] = m1[i,j] - m2[i,j];

            }
            return result;
        }

        public static Matrix2Dimensional operator *(Matrix2Dimensional m1, Matrix2Dimensional m2)
        {
            Matrix2Dimensional result = null;
            
            if (CheckForMultiply(m1, m2))
            {
                result = new Matrix2Dimensional(m1.Rows, m2.Cols);

                for (int i = 0; i < result.Rows; i++)
                {
                    for (int j = 0; j < result.Cols; j++)
                    {
                        double temp = 0;
                        for (int k = 0; k < m1.Cols; k++)
                        {
                            temp += m1[i,k] * m2[k,j];
                        }
                        result[i,j] = temp;
                    }
                }

            }
            return result;
        }
        */
        private static bool CheckSameDimension(CMatrix m1, CMatrix m2)
        {
            if (m1.Rows == m2.Rows && m1.Cols == m2.Cols)
                return true;
            else
                return false;
        }

        private static bool CheckForMultiply(CMatrix m1, CMatrix m2)
        {
            if (m1.Cols == m2.Rows)
                return true;
            else
                return false;
        }

        public override string ToString()
        {
            string str = "";
            for (int i = 0; i < this.rows; i++)
            {
                for (int j = 0; j < this.cols; j++)
                    str += this[i, j] + "\t";
                str += "\n";
            }
            return str;
        }

        /// <summary>
        /// The function return the Minor of element[Row,Col] of a Matrix object 
        /// </summary>
        public static CMatrix Minor(CMatrix matrix, int iRow, int iCol)
        {
            CMatrix minor = new CMatrix(matrix.Rows - 1, matrix.Cols - 1);
            int m = 0, n = 0;
            for (int i = 0; i < matrix.Rows; i++)
            {
                if (i == iRow)
                    continue;
                n = 0;
                for (int j = 0; j < matrix.Cols; j++)
                {
                    if (j == iCol)
                        continue;
                    minor[m, n] = matrix[i, j];
                    n++;
                }
                m++;
            }
            return minor;
        }
		

        public double Determinent()
        {
            return Determinent(this);
        }

        /// <summary>
        /// The helper function for the above Determinent() method
        /// it calls itself recursively and computes determinent using minors
        /// </summary>
        private double Determinent(CMatrix matrix)
        {
            double det = 0;
            if (matrix.Rows != matrix.Cols)
                throw new MatrixException("Determinent of a non-square matrix doesn't exist");
            if (matrix.Rows == 1)
                return matrix[0, 0];
            for (int j = 0; j < matrix.Cols; j++)
                det += (matrix[0, j] * Determinent(CMatrix.Minor(matrix, 0, j)) * (int)System.Math.Pow(-1, 0 + j));
            return det;
        }

        /// <summary>
        /// The function returns the inverse of the current matrix in the traditional way(by adjoint method)
        /// It can be much slower if the given matrix has order greater than 6
        /// Try using InverseFast() function if the order of matrix is greater than 6
        /// </summary>
        public CMatrix Inverse()
        {
            double det = this.Determinent();
            if (det == 0)
                throw new MatrixException("Inverse of a singular matrix is not possible");
            return (this.Adjoint() / det);
        }

        /*
        public double DeterminentFast()
        {
            if (this.Rows != this.Cols)
                throw new MatrixException("Determinent of a non-square matrix doesn't exist");
            double det = 1;
            try
            {
                Matrix2Dimensional ReducedEchelonMatrix = this.Duplicate();
                for (int i = 0; i < this.Rows; i++)
                {
                    if (ReducedEchelonMatrix[i, i-1] == 0)	// if diagonal entry is zero, 
                        for (int j = i + 1; j < ReducedEchelonMatrix.Rows; j++)
                            if (ReducedEchelonMatrix[j, i] != 0)	 //check if some below entry is non-zero
                            {
                                ReducedEchelonMatrix.InterchangeRow(i, j);	// then interchange the two rows
                                det *= -1;	//interchanging two rows negates the determinent
                            }

                    det *= ReducedEchelonMatrix[i, i];
                    ReducedEchelonMatrix.MultiplyRow(i, Fraction.Inverse(ReducedEchelonMatrix[i, i]));

                    for (int j = i + 1; j < ReducedEchelonMatrix.Rows; j++)
                    {
                        ReducedEchelonMatrix.AddRow(j, i, -ReducedEchelonMatrix[j, i]);
                    }
                    for (int j = i - 1; j >= 0; j--)
                    {
                        ReducedEchelonMatrix.AddRow(j, i, -ReducedEchelonMatrix[j, i]);
                    }
                }
                return det;
            }
            catch (Exception)
            {
                throw new MatrixException("Determinent of the given matrix could not be calculated");
            }
        }

        public Matrix2Dimensional InverseFast()
		{
			if ( this.DeterminentFast()==0 )
				throw new MatrixException("Inverse of a singular matrix is not possible");
			try
			{
                Matrix2Dimensional IdentityMatrix = Matrix.IdentityMatrix(this.Rows, this.Cols);
                Matrix2Dimensional ReducedEchelonMatrix = this.Duplicate();
				for (int i=0;i<this.Rows;i++)
				{	
					if (ReducedEchelonMatrix[i,i]==0)	// if diagonal entry is zero, 
						for (int j=i+1;j<ReducedEchelonMatrix.Rows;j++)
							if (ReducedEchelonMatrix[j,i]!=0)	 //check if some below entry is non-zero
							{
								ReducedEchelonMatrix.InterchangeRow(i,j);	// then interchange the two rows
								IdentityMatrix.InterchangeRow(i,j);	// then interchange the two rows
							}
					IdentityMatrix.MultiplyRow(i, 1/ReducedEchelonMatrix[i,i]);
					ReducedEchelonMatrix.MultiplyRow(i, Fraction.Inverse(ReducedEchelonMatrix[i,i]));
				
					for (int j=i+1;j<ReducedEchelonMatrix.Rows;j++)
					{
						IdentityMatrix.AddRow( j, i, -ReducedEchelonMatrix[j,i]);
						ReducedEchelonMatrix.AddRow( j, i, -ReducedEchelonMatrix[j,i]);
					}
					for (int j=i-1;j>=0;j--)
					{
						IdentityMatrix.AddRow( j, i, -ReducedEchelonMatrix[j,i]);
						ReducedEchelonMatrix.AddRow( j, i, -ReducedEchelonMatrix[j,i]);
					}
				}
				return IdentityMatrix;
			}
			catch(Exception)
			{
				throw new MatrixException("Inverse of the given matrix could not be calculated");
			}
		}

        public Matrix2Dimensional ReducedEchelonForm()
        {
            try
            {
                Matrix2Dimensional ReducedEchelonMatrix = this.Duplicate();
                for (int i = 0; i < this.Rows; i++)
                {
                    if (ReducedEchelonMatrix[i, i-1] == 0)	// if diagonal entry is zero, 
                        for (int j = i + 1; j < ReducedEchelonMatrix.Rows; j++)
                            if (ReducedEchelonMatrix[j, i] != 0)	 //check if some below entry is non-zero
                                ReducedEchelonMatrix.InterchangeRow(i, j);	// then interchange the two rows
                    if (ReducedEchelonMatrix[i, i-1] == 0)	// if not found any non-zero diagonal entry
                        continue;	// increment i;
                    if (ReducedEchelonMatrix[i, i] != 1)	// if diagonal entry is not 1 , 	
                        for (int j = i + 1; j < ReducedEchelonMatrix.Rows; j++)
                            if (ReducedEchelonMatrix[j, i-1] == 1)	 //check if some below entry is 1
                                ReducedEchelonMatrix.InterchangeRow(i, j);	// then interchange the two rows
                    ReducedEchelonMatrix.MultiplyRow(i, Fraction.Inverse(ReducedEchelonMatrix[i, i]));
                    for (int j = i + 1; j < ReducedEchelonMatrix.Rows; j++)
                        ReducedEchelonMatrix.AddRow(j, i, -ReducedEchelonMatrix[j, i]);
                    for (int j = i - 1; j >= 0; j--)
                        ReducedEchelonMatrix.AddRow(j, i, -ReducedEchelonMatrix[j, i]);
                }
                return ReducedEchelonMatrix;
            }
            catch (Exception)
            {
                throw new MatrixException("Matrix can not be reduced to Echelon form");
            }
        }

        public static Matrix2Dimensional IdentityMatrix(int iRows, int iCols)
        {
            return ScalarMatrix(iRows, iCols, 1);
        }

        public static Matrix2Dimensional ScalarMatrix(int iRows, int iCols, int K)
        {
            double zero = 0;
            double scalar = K;
            Matrix2Dimensional matrix = new Matrix2Dimensional(iRows, iCols);
            for (int i = 0; i < iRows; i++)
                for (int j = 0; j < iCols; j++)
                {
                    if (i == j)
                        matrix[i, j] = scalar;
                    else
                        matrix[i, j] = zero;
                }
            return matrix;
        }

        public void InterchangeRow(int iRow1, int iRow2)
        {
            for (int j = 0; j < this.Cols; j++)
            {
                double temp = this[iRow1, j];
                this[iRow1, j] = this[iRow2, j];
                this[iRow2, j] = temp;
            }
        }

        public void AddRow(int iTargetRow, int iSecondRow, double iMultiple)
        {
            for (int j = 0; j < this.Cols; j++)
                this[iTargetRow, j] += (this[iSecondRow, j] * iMultiple);
        }

        public void MultiplyRow(int iRow, double frac)
        {
            for (int j = 0; j < this.Cols; j++)
            {
                this[iRow, j] *= frac;
                Fraction.ReduceFraction(this[iRow, j]);
            }
        }
        */

        /// <summary>
        /// The function returns the adjoint of the current matrix
        /// </summary>
        /// 

        public CMatrix Adjoint()
        {
            if (this.rows != this.cols)
                throw new MatrixException("Adjoint of a non-square matrix does not exists");
            CMatrix AdjointMatrix = new CMatrix(this.rows, this.cols);
            for (int i = 0; i < this.rows; i++)
                for (int j = 0; j < this.cols; j++)
                    AdjointMatrix[i, j] = Math.Pow(-1, i + j) * (Minor(this, i, j).Determinent());
            AdjointMatrix = AdjointMatrix.Transpose();
            return AdjointMatrix;
        }

        /// <summary>
        /// The function returns the transpose of the current matrix
        /// </summary>
        public CMatrix Transpose()
        {
            CMatrix TransposeMatrix = new CMatrix(this.cols, this.rows);
            for (int i = 0; i < TransposeMatrix.Rows; i++)
                for (int j = 0; j < TransposeMatrix.Cols; j++)
                    TransposeMatrix[i, j] = this[j, i];
            return TransposeMatrix;
        }

        public CMatrix GetColumn(int index)
        {
            if (index >= this.cols)
                return null;

            CMatrix result = new CMatrix(this.rows,  1);
            for (int i = 0; i < this.rows; i++)
                result[i, 0] = this[i, 0];

            return result;
        }

        public CMatrix GetRow(int index)
        {
            if (index >= this.rows)
                return null;

            CMatrix result = new CMatrix(1, this.cols);
            for (int i = 0; i < this.cols; i++)
                result[0,i] = this[0,i];

            return result;
        }

        public CMatrix GetRows(int fromIndex, int toIndex)
        {
            if (fromIndex >= this.rows || toIndex >= this.rows || fromIndex >toIndex)
                return null;

            CMatrix result = new CMatrix(toIndex - fromIndex + 1, this.cols);
            for (int i = fromIndex; i <= toIndex; i++)
                for (int j=0; j<this.cols; j++)
                    result[i-fromIndex, j] = this[i, j];

            return result;
        }

        public CMatrix GetColumns(int fromIndex, int toIndex)
        {
            if (fromIndex >= this.cols || toIndex >= this.cols || fromIndex > toIndex)
                return null;

            CMatrix result = new CMatrix(this.rows, toIndex - fromIndex + 1);
            for (int i =0 ; i <=this.rows ; i++)
                for (int j = fromIndex; j < toIndex; j++)
                    result[i, j - fromIndex] = this[i, j];

            return result;
        }

        public CMatrix RemoveAtColumn(int index)
        {
            if (index >= this.cols)
                return null;

            CMatrix result = new CMatrix(this.rows, this.cols-1);
            for (int j = 0; j < index; j++)
                for (int i = 0; i < result.Rows; i++)
                    result[i, j] = this[i, j];

            for (int j = index + 1; j < this.Cols; j++)
                for (int i = 0; i < result.Rows; i++)
                    result[i , j-1] = this[i, j];
            return result;
        }

        public CMatrix RemoveAtRow(int index)
        {
            if (index >= this.rows)
                return null;

            CMatrix result = new CMatrix(this.rows-1, this.cols);
            for (int i = 0; i < index; i++)
                for (int j = 0; j < result.Cols; j++)
                    result[i, j] = this[i, j];

            for (int i = index + 1; i < this.rows; i++)
                for (int j = 0; j < result.Cols; j++)
                    result[i-1, j] = this[i, j];
            return result;
        }
        public CMatrix GetColumnMean()
        {
            CMatrix result = new CMatrix(1, this.cols);
            for (int j = 0; j < cols; j++)
            {
                double sum = 0;
                for (int i = 0; i < rows; i++)
                    sum += matrix[i, j];
                result[0, j] = sum / cols;
            }
            return result;
        }

        public CMatrix AddRow(CMatrix addMatrix)
        {
            if (this.Cols != addMatrix.Cols)
                return null;

            CMatrix result = new CMatrix(this.rows + addMatrix.Rows, this.cols);

            for (int i = 0; i < this.rows; i++)
                for (int j = 0; j < result.Cols; j++)
                    result[i, j] = this[i, j];

            for (int i = 0; i < addMatrix.Rows; i++)
                for (int j = 0; j < result.Cols; j++)
                    result[i + rows, j] = addMatrix[i, j];

            return result;
        }

        public CMatrix AddColumns(CMatrix addMatrix)
        {
            if (this.rows != addMatrix.rows)
                return null;

            CMatrix result = new CMatrix(this.rows,this.cols + addMatrix.Cols);

            for (int i = 0; i < this.rows; i++)
                for (int j = 0; j < this.cols; j++)
                    result[i, j] = this[i, j];

            for (int i = 0; i < this.rows; i++)
                for (int j = 0; j < addMatrix.Cols; j++)
                    result[i, j+this.cols] = addMatrix[i, j];

            return result;
        }

        /// <summary>
        /// The function duplicates the current Matrix object
        /// </summary>
        public CMatrix Duplicate()
        {
            CMatrix matrix = new CMatrix(rows, cols);
            for (int i = 0; i < rows; i++)
                for (int j = 0; j < cols; j++)
                    matrix[i, j] = this[i, j];
            return matrix;
        }


        /// <summary>
        /// Operators for the Matrix object
        /// includes -(unary), and binary opertors such as +,-,*,/
        /// </summary>
        public static CMatrix operator -(CMatrix matrix)
        { return CMatrix.Negate(matrix); }

        public static CMatrix operator +(CMatrix matrix1, CMatrix matrix2)
        { return CMatrix.Add(matrix1, matrix2); }

        public static CMatrix operator -(CMatrix matrix1, CMatrix matrix2)
        { return CMatrix.Add(matrix1, -matrix2); }

        public static CMatrix operator *(CMatrix matrix1, CMatrix matrix2)
        { return CMatrix.Multiply(matrix1, matrix2); }

        public static CMatrix operator &(CMatrix matrix1, CMatrix matrix2)
        { return CMatrix.MultiplyNormal(matrix1, matrix2); }

        public static CMatrix operator %(CMatrix matrix1, CMatrix matrix2)
        { return CMatrix.DivideNormal(matrix1, matrix2); }

        public static CMatrix operator *(CMatrix matrix1, int iNo)
        { return CMatrix.Multiply(matrix1, iNo); }

        public static CMatrix operator *(CMatrix matrix1, double dbl)
        { return CMatrix.Multiply(matrix1, dbl);}

        public static CMatrix operator *(int iNo, CMatrix matrix1)
        { return CMatrix.Multiply(matrix1, iNo); }

        public static CMatrix operator *(double dbl, CMatrix matrix1)
        { return CMatrix.Multiply(matrix1, dbl); }

        public static CMatrix operator /(CMatrix matrix1, int iNo)
        { return CMatrix.Multiply(matrix1, 1/iNo); }

        public static CMatrix operator /(CMatrix matrix1, double dbl)
        { return CMatrix.Multiply(matrix1, 1 / dbl); }


        /// <summary>
        /// Internal Fucntions for the above operators
        /// </summary>
        private static CMatrix Negate(CMatrix matrix)
        {
            return CMatrix.Multiply(matrix, -1);
        }

        private static CMatrix Add(CMatrix matrix1, CMatrix matrix2)
        {
            if (matrix1.Rows != matrix2.Rows || matrix1.Cols != matrix2.Cols)
                throw new MatrixException("Operation not possible");
            CMatrix result = new CMatrix(matrix1.Rows, matrix1.Cols);
            for (int i = 0; i < result.Rows; i++)
                for (int j = 0; j < result.Cols; j++)
                    result[i, j] = matrix1[i, j] + matrix2[i, j];
            return result;
        }

        private static CMatrix Multiply(CMatrix matrix1, CMatrix matrix2)
        {
            if (matrix1.Cols != matrix2.Rows)
                throw new MatrixException("Operation not possible");
            CMatrix result = new CMatrix(matrix1.Rows, matrix2.Cols);
            for (int i = 0; i < result.Rows; i++)
                for (int j = 0; j < result.Cols; j++)
                    for (int k = 0; k < matrix1.Cols; k++)
                        result[i, j] += matrix1[i, k] * matrix2[k, j];
            return result;
        }

        private static CMatrix MultiplyNormal(CMatrix matrix1, CMatrix matrix2)
        {
            if (matrix1.Rows != matrix2.Rows || matrix1.Cols != matrix2.Cols)
                throw new MatrixException("Operation not possible");
            CMatrix result = new CMatrix(matrix1.Rows, matrix1.Cols);
            for (int i = 0; i < result.Rows; i++)
                for (int j = 0; j < result.Cols; j++)
                    result[i, j] = matrix1[i, j] * matrix2[i, j];
            return result;
        }
        private static CMatrix DivideNormal(CMatrix matrix1, CMatrix matrix2)
        {
            if (matrix1.Rows != matrix2.Rows || matrix1.Cols != matrix2.Cols)
                throw new MatrixException("Operation not possible");
            CMatrix result = new CMatrix(matrix1.Rows, matrix1.Cols);
            for (int i = 0; i < result.Rows; i++)
                for (int j = 0; j < result.Cols; j++)
                    result[i, j] = matrix1[i, j] / matrix2[i, j];
            return result;
        }
        private static CMatrix Multiply(CMatrix matrix, int iNo)
        {
            CMatrix result = new CMatrix(matrix.Rows, matrix.Cols);
            for (int i = 0; i < matrix.Rows; i++)
                for (int j = 0; j < matrix.Cols; j++)
                    result[i, j] = matrix[i, j] * iNo;
            return result;
        }

        private static CMatrix Multiply(CMatrix matrix, double value)
        {
            CMatrix result = new CMatrix(matrix.Rows, matrix.Cols);
            for (int i = 0; i < matrix.Rows; i++)
                for (int j = 0; j < matrix.Cols; j++)
                    result[i, j] = matrix[i, j] * value;
            return result;
        }

        public double[,] GetDoubleMatrix()
        {
            return this.matrix;
        }

        public void RandomAssignation()
        {
            Random random = new Random(100);
            for (int i=0; i<rows; i++)
                for (int j=0; j<cols; j++)
                    matrix[i,j] = random.NextDouble();
            
        }
        

    }



    ///*******************************************************///
    ///

    public class MatrixException : Exception
    {
        public MatrixException()
            : base()
        { }

        public MatrixException(string Message)
            : base(Message)
        { }

        public MatrixException(string Message, Exception InnerException)
            : base(Message, InnerException)
        { }
    }	// end class MatrixException

}
