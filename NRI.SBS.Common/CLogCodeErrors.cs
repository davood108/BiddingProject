﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace NRI.SBS.Common
{
    public class CLogCodeErrors
    {
        public static void LogError(Exception ex,string UrlAdr,string hardPath,string proxy)
        {
            StreamWriter sw = File.AppendText("DownloadError.txt");
            sw.WriteLine("Date: " + DateTime.Now.ToString());
            sw.WriteLine("Url Address:  "+UrlAdr);
            sw.WriteLine("Hard Path:  "+hardPath);
            sw.WriteLine("Proxy:  "+proxy);
            sw.WriteLine("MainError:  " + ex.Message);
            if (ex.InnerException != null)
                sw.WriteLine("InnerExeption:  " + ex.InnerException.Message);
            sw.WriteLine("");
            sw.Close();
        }
    }
}
