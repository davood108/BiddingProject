﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NRI.SBS.Common
{
   public class SettingParameters
    {
       public string ServerProxy { get; set; }
       public string  Username { get; set; }
       public string Password { get; set; }
       public string Domain { get; set; }
       public string M002Path { get; set; }
       public string M005Path { get; set; }
       public string M005Pathba { get; set; }
       public string M009Path { get; set; }
       public string M0091Path { get; set; }
       public string CounterPath { get; set; }
       public string AvgPricePath{ get; set; }
       public string LoadForcastingPath { get; set; }
       public string CapacityFactorsPath { get; set; }
       public string DispatchFilePath { get; set; }

       public bool IsFTP002 { get; set; }
       public bool IsFTP005 { get; set; }
       public bool IsFTP005ba { get; set; }
       public bool IsFTPDispatch { get; set; }
       public bool IsFTP009 { get; set; }
       public bool IsFTP0091 { get; set; }
       public bool IsFTPcounter { get; set; } 
       public bool IsFTPavg { get; set; }
       public bool IsFTPload { get; set; }
       public bool IsFTPcapacity { get; set; }

       public string FTPServer { get; set; }
       public string FTPUsername { get; set; }
       public string FTPPassword { get; set; }

    }
}
