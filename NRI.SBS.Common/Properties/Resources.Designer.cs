﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NRI.SBS.Common.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("NRI.SBS.Common.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to به علت استفاده در جداول ديگر قابل حذف نمي باشد.
        /// </summary>
        internal static string CanNotDeleteForeignKey {
            get {
                return ResourceManager.GetString("CanNotDeleteForeignKey", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ارتباط با پايگاه داده قطع مي باشد.
        /// </summary>
        internal static string CanNotOpenConnection {
            get {
                return ResourceManager.GetString("CanNotOpenConnection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to امكان نمايش منحني وجود ندارد.
        /// </summary>
        internal static string ChartComponentNotAvailable {
            get {
                return ResourceManager.GetString("ChartComponentNotAvailable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to كد اين المان تكراري مي باشد.
        /// </summary>
        internal static string DuplicateRowName {
            get {
                return ResourceManager.GetString("DuplicateRowName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to عدم بارگذاري شبكه يا يافتن المان.
        /// </summary>
        internal static string ElementNotFound {
            get {
                return ResourceManager.GetString("ElementNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to به دليل عدم دسترسي به برنامه MATLAB امكان انجام محاسبات وجود ندارد.
        /// </summary>
        internal static string ErrorInMatlabComponent {
            get {
                return ResourceManager.GetString("ErrorInMatlabComponent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to فايل يا Sheet1 صفحه گسترده مورد نظر يافت نشد.
        /// </summary>
        internal static string ExcelFileOrSheetNotFound {
            get {
                return ResourceManager.GetString("ExcelFileOrSheetNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to فايل مورد نظر وجود ندارد.
        /// </summary>
        internal static string FileNotFound {
            get {
                return ResourceManager.GetString("FileNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to اجراي درخواست شما در پايگاه داده ممكن نيست.
        /// </summary>
        internal static string GeneralHibernateException {
            get {
                return ResourceManager.GetString("GeneralHibernateException", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to اجراي درخواست شما در پايگاه داده ممكن نيست.
        /// </summary>
        internal static string GeneralSQLException {
            get {
                return ResourceManager.GetString("GeneralSQLException", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to نوع يكي از تجهيزات در پايگاه داده تعريف نشده است.
        /// </summary>
        internal static string LinkDoesNotExist {
            get {
                return ResourceManager.GetString("LinkDoesNotExist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to درخت پخش بار اشكال دارد.
        /// </summary>
        internal static string LoadFlowNetworkCorrupt {
            get {
                return ResourceManager.GetString("LoadFlowNetworkCorrupt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to نگاشت پايگاه داده مشگل دارد.
        /// </summary>
        internal static string MappingException {
            get {
                return ResourceManager.GetString("MappingException", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to توپولوژي شبكه اشكال دارد.
        /// </summary>
        internal static string NetworkCorrupt {
            get {
                return ResourceManager.GetString("NetworkCorrupt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to تخمين اوليه براي بارهاي موردنظر موجود نيست.
        /// </summary>
        internal static string NoInitialEstimationResult {
            get {
                return ResourceManager.GetString("NoInitialEstimationResult", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to چنين موردي در پايگاه داده وجود ندارد.
        /// </summary>
        internal static string ObjectNotFoundException {
            get {
                return ResourceManager.GetString("ObjectNotFoundException", resourceCulture);
            }
        }
    }
}
