using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace NRI.SBS.Common
{
    public class CConfigDBXMLController
    {
        public static string GetDBFilePath()
        {
            return System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                "\\SBS\\ConfigDB.xml";
        }

        public static string internetGetDBFilePath()
        {
            return System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                "\\SBS\\internetConfigDB.xml";
        }

        //static string DefaultPath(Path criteria)
        //{
        //    string defaultpath = "";

        //    if (criteria == Path.Config)
        //        defaultpath = "\\Nri\\DMSFiles\\ConfigPath\\";
        //    else if (criteria == Path.Temp)
        //        defaultpath = "\\Nri\\DMSFiles\\TempPath\\";

        //    return defaultpath;
        //}


        // public static CDBConfigurationParams ConfigInfoObj; //2
        public CDBConfigurationParams ReadDBConfig()
        {
            // ConfigInfoObj = new CDBConfigurationParams();//2
            CDBConfigurationParams ConfigInfoObj = new CDBConfigurationParams();//1
            XmlSerializer mySerializer = new XmlSerializer(typeof(CDBConfigurationParams));
            try
            {
                //FileStream myFileStream = new FileStream("ConfigDB.xml", FileMode.Open);
                string path = GetDBFilePath();
                FileStream myFileStream = new FileStream(path, FileMode.Open);

                ConfigInfoObj = (CDBConfigurationParams)mySerializer.Deserialize(myFileStream);
                myFileStream.Close();
            }
            catch (System.IO.FileNotFoundException fex)
            {
                throw fex;
            }
            catch (Exception ex)
            {
                throw ex;
                //ConfigInfoObj = null;
                //throw new DMSGeneralException //impossible because can not call NRI.SBS.Common from CIM_DA
                //and if transfer this file to NRI.SBS.Common NHibernateHelper_CIM could not use this
            }
            return ConfigInfoObj;
        }


        public CDBConfigurationParams internetReadDBConfig()
        {
            // ConfigInfoObj = new CDBConfigurationParams();//2
            CDBConfigurationParams ConfigInfoObj = new CDBConfigurationParams();//1
            XmlSerializer mySerializer = new XmlSerializer(typeof(CDBConfigurationParams));
            try
            {
                //FileStream myFileStream = new FileStream("ConfigDB.xml", FileMode.Open);
                string path = internetGetDBFilePath();
                FileStream myFileStream = new FileStream(path, FileMode.Open);

                ConfigInfoObj = (CDBConfigurationParams)mySerializer.Deserialize(myFileStream);
                myFileStream.Close();
            }
            catch (System.IO.FileNotFoundException fex)
            {
                throw fex;
            }
            catch (Exception ex)
            {
                throw ex;
                //ConfigInfoObj = null;
                //throw new DMSGeneralException //impossible because can not call NRI.SBS.Common from CIM_DA
                //and if transfer this file to NRI.SBS.Common NHibernateHelper_CIM could not use this
            }
            return ConfigInfoObj;
        }

        public bool WriteDBConfig(CDBConfigurationParams ConfigInfo)//1
        //public bool WriteDBConfig()//2
        {
            bool result = true;

            try
            {
                XmlSerializer mySerializer = new XmlSerializer(typeof(CDBConfigurationParams));
                //StreamWriter myWriter = new StreamWriter("ConfigDB.xml"); ;
                StreamWriter myWriter = new StreamWriter(GetDBFilePath());
                mySerializer.Serialize(myWriter, ConfigInfo);//1
                // mySerializer.Serialize(myWriter, CConfigDBXMLController.ConfigInfoObj);//2
                myWriter.Close();
            }
            catch
            {
                result = false;
            }
            return result;
        }
    }
}
