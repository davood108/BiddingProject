using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration ;
//using ILOG.Concert;
//using ILOG.CPLEX;
using NRI.SBS.Common;

namespace NRI.SBS.Common
{
    public class Utilities
    {
        private static SqlConnection oSqlConnection = null;
        private static SqlCommand oSqlCommand = null;
        private static SqlDataReader oSqlDataReader = null;
        private static DataTable oDataTable = null;


        private static SqlConnection ls2oSqlConnection = null;
        private static SqlCommand ls2oSqlCommand = null;
        private static SqlDataReader ls2oSqlDataReader = null;
        private static DataTable ls2oDataTable = null;


        public Utilities()
        {
        }

        public static DataTable GetTable(string command)
        {


            oSqlConnection = new SqlConnection(ConnectionManager.ConnectionString);
            oSqlConnection.Open();

            try
            {
                oSqlCommand = new SqlCommand();
                oSqlCommand.CommandText = command;
                oSqlCommand.CommandTimeout = 200;
                oSqlCommand.Connection = oSqlConnection;
                oSqlDataReader = oSqlCommand.ExecuteReader();
                oDataTable = new DataTable();
                oDataTable.Load(oSqlDataReader);
                return oDataTable;

            }
            catch (SqlException ex)
            {
                throw ex;//1396/12/15
                //return null;
            }
            finally
            {
                oSqlDataReader.Close();
                oSqlConnection.Close();
            }


        }

        public static DataTable ls2returntbl(string command)
        {


            ls2oSqlConnection = new SqlConnection(ConnectionManager.ConnectionString);
            ls2oSqlConnection.Open();

            try
            {
                ls2oSqlCommand = new SqlCommand();
                ls2oSqlCommand.CommandText = command;
                ls2oSqlCommand.Connection = ls2oSqlConnection;
                ls2oSqlDataReader = ls2oSqlCommand.ExecuteReader();
                ls2oDataTable = new DataTable();
                ls2oDataTable.Load(ls2oSqlDataReader);
                return ls2oDataTable;

            }
            catch (SqlException ex)
            {
                return null;
            }
            finally
            {
                ls2oSqlDataReader.Close();
                ls2oSqlConnection.Close();
            }


        }


        public static bool returnbool(string command)
        {
            try
            {

                //oSqlConnection = new SqlConnection();
                //oSqlConnection.ConnectionString = "Data Source=.;Initial Catalog=PowerPalntDB;Integrated Security=True";
                //if (oSqlConnection.State != ConnectionState.Open)
                //    oSqlConnection.Open();

                oSqlCommand = new SqlCommand();
                oSqlCommand.CommandText = command;
                oSqlCommand.Connection = new SqlConnection(ConnectionManager.ConnectionString);
                int temp = oSqlCommand.ExecuteNonQuery();
                if (temp != 0)
                    return true;
                else
                    return false;


            }
            catch (System.Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Test\n" + e.Message);
                return false;

            }
            finally
            {
                oSqlConnection.Close();
            }
        }

        public static DataTable GetTable(SqlCommand command)
        {
            try
            {

                oSqlConnection = new SqlConnection();
                oSqlConnection.ConnectionString = ConnectionManager.ConnectionString;
                if (oSqlConnection.State != ConnectionState.Open)
                    oSqlConnection.Open();
                oSqlCommand = new SqlCommand();
                oSqlCommand = command;
                oSqlCommand.Connection = oSqlConnection;
                oSqlDataReader = oSqlCommand.ExecuteReader();
                oDataTable = new DataTable();
                oDataTable.Load(oSqlDataReader);
                return oDataTable;
                oSqlConnection.Close();
            }
            catch (System.Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Test\n" + e.Message);
                return null;
            }
        }

        public static bool returnbool(SqlCommand command)
        {
            try
            {

                oSqlConnection = new SqlConnection();
                oSqlConnection.ConnectionString = ConnectionManager.ConnectionString;
                if (oSqlConnection.State != ConnectionState.Open)
                    oSqlConnection.Open();

                oSqlCommand = new SqlCommand();
                oSqlCommand = command;
                oSqlCommand.Connection = oSqlConnection;
                int temp = oSqlCommand.ExecuteNonQuery();
                if (temp != 0)
                    return true;
                else
                    return false;
                oSqlConnection.Close();
            }
            catch (System.Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Test\n" + e.Message);
                return false;

            }
        }

        public static void ExecQuery(string command)
        {
            SqlCommand MyCom = new SqlCommand();
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            MyCom.Connection = myConnection;
            MyCom.CommandText = command;
            MyCom.ExecuteNonQuery();
            myConnection.Close();
        }
    }

}
