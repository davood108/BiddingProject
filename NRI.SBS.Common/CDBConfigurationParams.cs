using System;
using System.Collections.Generic;
using System.Text;

namespace NRI.SBS.Common
    {
    public class CDBConfigurationParams
    {
        //  static CDBConfigurationParams self = null;

        // private CDBConfigurationParams()
        //{

        //}

        //public static CDBConfigurationParams getInstance()
        //{
        //if (self == null)
        //    self = new CDBConfigurationParams();
        //    return self;
        //}
        private string _ServerName;

        public string ServerName
        {
            get { return _ServerName; }
            set { _ServerName = value; }
        }
        private string _ServerDBName;

        public string ServerDBName
        {
            get { return _ServerDBName; }
            set { _ServerDBName = value; }
        }
        private string _ServerUserName;

        public string ServerUserName
        {
            get { return _ServerUserName; }
            set { _ServerUserName = value; }
        }
        private string _ServerUserPassword;

        public string ServerUserPassword
        {
            get { return _ServerUserPassword; }
            set { _ServerUserPassword = value; }
        }

        private string _ServerDBTimeout;

        public string ServerDBTimeout
        {
            get { return _ServerDBTimeout; }
            set { _ServerDBTimeout = value; }
        }


    }
    }

