﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NRI.SBS.Common
{
    public partial class ListErrorMessageBox : Form
    {
        public ListErrorMessageBox()
        {
            InitializeComponent();
        }

        public void Set(string title, string description, string Message)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.MaximumSize = new Size(595, 398);
            lblDescription.Text= description;
            lstMessage.Text = Message;
            this.Text = title;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
