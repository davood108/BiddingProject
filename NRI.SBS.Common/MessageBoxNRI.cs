﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NRI.SBS.Common
{
    public static class MessageBoxNRI
    {
        public static void ShowMessageList(string description, string Message)
        {
            ListErrorMessageBox frm = new ListErrorMessageBox();
            frm.Set("Download", description, Message);
            frm.ShowDialog();

        }
    }
}
