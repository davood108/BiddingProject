﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NRI.SBS.Common
{
    public enum ItemsToBeDownloaded
    {
        M002,
        M005,
        M005ba,
        Lfc,
        ChartPrice,
        AveragePrice, /* DailyPrice */
        LoadForcasting, /* PriceEstimate */
        Rep12Pages, Outage, ProducedEenergy, InterchangedEnergy,
        RegionNetComp, lineNetComp,
        Manategh,
        UnitNetComp
    };

    public enum DefinedRoles
    {
        Administrator,
        All,
        NoMaintenance,
        NoBidding,
        ExceptComputational,
        Unassigned,
        Noaccessplant,
        nriAdministrator,
        PlantUser,
        NoMarketBill


    };
    public enum Days
    {
        Saturday = 1, Sunday = 2, Monday = 3, Tuesday = 4,
        Wednesday = 5, Tursday = 6, Friday = 7
    };

    public enum Line_codes
    {
        BL907 = 1, BP806 = 2, AQ902 = 3, AQ903 = 4
    };
    //public enum MixUnits
    //{
    //    All, JustCombined, ExeptCombined
    //};
    public enum Line_Opf
    {
        BL907 = 1, BP806 = 2, AQ902 = 3, AQ903 = 4 
    };

    public enum PackageTypePriority
    {
        Steam=0, Gas=1, CC=2
    };

    public enum MaintenanceStratedgy
    {
        Low, Normal, High
    };
    public enum MaintenanceInterval
    {
        Weekly, Monthly
    };
}
