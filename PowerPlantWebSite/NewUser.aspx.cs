﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using NRI.SBS.Common;

public partial class NewUser : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        foreach (string role in Enum.GetNames(typeof(DefinedRoles)))
        {
            ddRole.Items.Add(role);
        }
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        string command = "";

        command = "insert into [login] (Firstname, lastname,username, password, role)" +
               " values ('" + txtFirstName.Text + "' " +
               ",'" + txtLastName.Text + "' " +
               ",'" + txtUserName.Text + "' " +
               ",'" + SHA1Encryption.Encrypt(txtPassword.Text) + "' " +
               ",'" + ddRole.Text + "') ";

        utilities.ExecQuery(command);
        Response.Redirect("users.aspx");
    }
}
