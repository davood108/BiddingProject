﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using NRI.SBS.Common;

public partial class users : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["Role"].ToString().Trim() != DefinedRoles.Administrator.ToString())
        //    Response.Redirect("AccessDenied.aspx");
        PopulateGrid();
    }

    private void PopulateGrid()
    {
        string command = "select * from [login]";
        DataTable dt = utilities.returntbl(command);
        gdvUsers.DataSource = dt;
        //gdvUsers.DataMember = dt.TableName;
        gdvUsers.DataBind();
        
        
    }
    protected void gdvUsers_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gdvUsers_PageIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gdvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
}
