﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using NRI.SBS.Common;

public partial class EditUser : System.Web.UI.Page
{
    string userID;
    protected void Page_Load(object sender, EventArgs e)
    {
        foreach (string role in Enum.GetNames(typeof(DefinedRoles)))
        {
            ddRole.Items.Add(role);
        }

        if (Request.Params["ID"] != null)
        {
            userID = Request.Params["ID"].ToString().Trim();
            HyperLink1.NavigateUrl = "~/ChangePassword.aspx?Id=" + userID;
                //"&admin=true";
        }

        if (!IsPostBack)
        {
            DisalyUserInfo(userID);

        }
    }
    private void DisalyUserInfo(string userId)
    {

        DataTable dt = utilities.returntbl("select * from [login] where id=" + userId);
        if (dt.Rows.Count == 1)
        {
            DataRow row = dt.Rows[0];
            txtFirstName.Text = row["FirstName"].ToString().Trim();
            txtLastName.Text = row["LastName"].ToString().Trim();
            txtUserName.Text = row["UserName"].ToString().Trim();
           ddRole.Text = row["Role"].ToString().Trim();
        }
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        string command = "";
        if ((userID != null) && (userID != ""))
        {
            command = "update [login] set Firstname= '" + txtFirstName.Text + "' " +
               ", lastname='" + txtLastName.Text + "' " +
                //", username='" + txtUserName.Text + "' " +
                //", password='" + SHA1Encryption.Encrypt(txtPassword.Text) + "' " +
               ", role='" + ddRole.Text + "' " +
               " where id=" + userID;
        }
        utilities.ExecQuery(command);
        Response.Redirect("users.aspx");
    }
    protected void ddRole_TextChanged(object sender, EventArgs e)
    {
        int a = 3;
        a += 4;
    }
}
