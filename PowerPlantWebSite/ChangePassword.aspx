﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">


    <div>
            <asp:TextBox ID="txtOldPassword" runat="server" 
                ToolTip="Current Password" TextMode="Password"></asp:TextBox>
        <br />

                <asp:TextBox ID="txtPassword" runat="server" ToolTip="New Password" 
                TextMode="Password"></asp:TextBox>
        <br />
        <asp:TextBox ID="txtPassRPT" runat="server" ToolTip="Repeat New Password" 
                TextMode="Password"></asp:TextBox>
            <br />
            <asp:Button ID="btnOK" runat="server" onclick="btnOK_Click" Text="Save" />
        <br />
    </div>
    </form>
</body>
</html>
