﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using NRI.SBS.Common;

public partial class ChangePassword : System.Web.UI.Page
{
    bool needPassword = true;
    string userID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["ID"] != null)
        {
            userID = Request.Params["ID"].ToString().Trim();
            needPassword = false; // it is admin
            txtOldPassword.Visible = false;
        }
        else if (needPassword && userID == "")
        {
            userID = Session["UserId"].ToString().Trim();
        }
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        string command = "";
        if ((userID != null) && (userID != ""))
        {
            command = "update [login] set password='" +
                SHA1Encryption.Encrypt(txtPassword.Text) + "' " +
               " where id=" + userID;
            if (needPassword)
               command += " and password='" + SHA1Encryption.Encrypt(txtPassword.Text) + "' ";

        }
        utilities.ExecQuery(command);
        Response.Redirect("users.aspx");
    }
}
