﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditUser.aspx.cs" Inherits="EditUser" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">

    <div>
    
        <asp:TextBox ID="txtFirstName" runat="server" ToolTip="First Name"></asp:TextBox>
        <br />
        <asp:TextBox ID="txtLastName" runat="server" ToolTip="Last Name"></asp:TextBox>
        <br />
        <asp:DropDownList ID="ddRole" runat="server" Height="26px" Width="126px" 
            ToolTip="Role" ontextchanged="ddRole_TextChanged">
        </asp:DropDownList>
        <br />
        <asp:TextBox ID="txtUserName" runat="server" ReadOnly="True" Enabled="False" 
            ToolTip="Username"></asp:TextBox>
        <br />
        <asp:HyperLink ID="HyperLink1" runat="server" 
            >Reset Password</asp:HyperLink>
        <br />

        <asp:Button ID="btnOk" runat="server" onclick="btnOk_Click" Text="Save" 
            CausesValidation="False" />
    
        <br />
        <br />
        <asp:HyperLink ID="HyperLink3" runat="server" 
            NavigateUrl="~/ChangePassword.aspx">Change Password</asp:HyperLink>
    
    </div>
    </form>
</body>
</html>
