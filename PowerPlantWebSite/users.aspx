﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="users.aspx.cs" Inherits="users" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:GridView ID="gdvUsers" 
            Style="z-index: 104;left: 10px; position: absolute; top: 102px" 
            CssClass="dgdStyle" AutoGenerateColumns="False"
            runat="server" BackColor="White" BorderColor="#000040" Width="654px"
            AllowPaging="True" 
            Height="136px" onpageindexchanged="gdvUsers_PageIndexChanged" 
            onpageindexchanging="gdvUsers_PageIndexChanging" >
                                <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" Visible="False"/>
                        <asp:BoundField DataField="firstname" HeaderText="First Name" ReadOnly="True" />
                        <asp:BoundField DataField="lastname" HeaderText="Last Name" ReadOnly="True" />
                        <asp:BoundField DataField="username" HeaderText="User Name" ReadOnly="True" />
                        <asp:CheckBoxField DataField="Deleted" HeaderText="Deleted" ReadOnly="True"/>
                        <asp:BoundField DataField="Role" HeaderText="Role" ReadOnly="True" />
                        <asp:HyperLinkField HeaderText="Edit" Text="Edit" DataNavigateUrlFields="Id"
                            DataNavigateUrlFormatString="~\EditUser.aspx?ID={0}" />

                        </Columns>
        </asp:GridView>
    
    </div>
    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/NewUser.aspx">New 
    User</asp:HyperLink>
    </form>
</body>
</html>
