using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;
using System.Net;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
using System.IO;
using System.Diagnostics;
using System.Linq;
using NRI.SBS.Common;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;



namespace Auto_Update_Library
{
    
    public class Download
    {
      
        private System.Windows.Forms.DataGridView dataGridView1;
        private string proxyAddress = "";

        public string ProxyAddress
        {
            get { return proxyAddress; }
            set { proxyAddress = value; }
        }
        private string messages="";
        public string Messages
        {
            get { return messages; }
        }
        public string Username { get; set; }
        public string password { get; set; }
        public string domain { get; set; }

        public string ftpusername { get; set; }

        public string ftppassword { get; set; }

        public string ftpserverip { get; set; }



        public void ClearMessages()
        {
            messages = "";
        }
        public void AddMessage(string str)
        {
            messages += str + "\r\n";
        }

        private static Download self= null;
        private Download()
        {
            InitializeGridView();
    
        }

        public static Download GetInstance()
        {
            if (self == null)
                self = new Download();
            return self;
        }

        private void InitializeGridView()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();

            // 
            // dataGridView1
            // 
            this.dataGridView1.Name = "dataGridView1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
        }

//---------------------------------ReadM00Excel---------------------------
        private string ReadM00Excel(ItemsToBeDownloaded item, string date, string PPID, bool combinedCycle)
        {
            bool isftp = false;
            DataTable result = null; 
            string path = "";
            string DocName = "";
            if ((!BaseData.GetInstance().Useftp002 && item == ItemsToBeDownloaded.M002) || (!BaseData.GetInstance().Useftp005 && item == ItemsToBeDownloaded.M005))
            {
                isftp = false;
                switch (item)
                {
                    case ItemsToBeDownloaded.M002:
                   
                        try
                        {
                            path = BaseData.GetInstance().M002Path;
                            int ID1 = 0;
                            ID1 = int.Parse(PPID);
                            if (combinedCycle)
                                ID1++;
                            string pathname = folderFormat(date, PPID, "M002");
                            if (pathname != "") path += pathname;
                            string makename = NameFormat(date, ID1.ToString(), "", "M002");
                            if (makename != "") path = path + "\\" + makename;
                        }
                        catch (Exception e)
                        {

                        }                                       

                      
                        break;

                    case ItemsToBeDownloaded.M005:
                        //build Name for FRM005 files
                      
                        try
                        {
                            path = BaseData.GetInstance().M005Path;
                            int ID1 = 0;
                            ID1 = int.Parse(PPID);
                            if (combinedCycle)
                                ID1++;
                            string pathname = folderFormat(date, PPID, "M005");
                            if (pathname != "") path += pathname;
                            string makename = NameFormat(date, ID1.ToString(), "", "M005");
                            if (makename != "") path = path + "\\" + makename;
                        }
                        catch (Exception e)
                        {

                        }
                        
                       
                        break;

                    
                }

                ///////////////////////////////////////////////////////////////
               

            }
            if ((BaseData.GetInstance().Useftp002 && item == ItemsToBeDownloaded.M002) || (BaseData.GetInstance().Useftp005 && item == ItemsToBeDownloaded.M005))
            {

                isftp = true;
                string Sendmonth = "";
                string Sendyear = "";
                string Sendday = "";
                string endid="";
                string endDate = date;
                int endday = int.Parse(endDate.Substring(8));
                int endMonth = int.Parse(endDate.Substring(5, 2));
                int endyear = int.Parse(endDate.Substring(0, 4));
                Sendmonth = endMonth.ToString();
                Sendyear=endyear.ToString();
                Sendday=endday.ToString();
                
                switch (item)
                {
                    case ItemsToBeDownloaded.M002:
                        DocName = "FRM0022_";
                        try
                        {
                            int ID = 0;
                            ID = int.Parse(PPID);
                            if (combinedCycle)
                                ID++;
                            DocName += ID.ToString();
                        }
                        catch (Exception e)
                        {

                        }
                        //add date to Name
                        string mydate = date;
                        mydate = mydate.Replace("/", "");
                        DocName += "_" + mydate;
                        switch (Sendmonth)
                        {
                            case "1":
                                Sendmonth = "Farvardin";
                                break;
                            case "2":
                                Sendmonth = "Ordibehesht";
                                break;
                            case "3":
                                Sendmonth = "Khordad";
                                break;
                            case "4":
                                Sendmonth = "Tir";
                                break;
                            case "5":
                                Sendmonth = "Mordad";
                                break;
                            case "6":
                                Sendmonth = "Shahrivar";
                                break;
                            case "7":
                                Sendmonth = "Mehr";
                                break;
                            case "8":
                                Sendmonth = "Aban";
                                break;
                            case "9":
                                Sendmonth = "Azar";
                                break;
                            case "10":
                                Sendmonth = "Day";
                                break;
                            case "11":
                                Sendmonth = "Bahman";
                                break;
                            case "12":
                                Sendmonth = "Esfand";
                                break;

                        }


                        path = "ftp://" + ftpserverip + "/" + BaseData.GetInstance().M002Path + "/";
                        path += endyear + @"/" + Sendmonth + @"/" + Sendday + @"/" + Sendday + @"/";

                        break;




                    case ItemsToBeDownloaded.M005:
                        //build Name for FRM005 files
                        DocName = "FRM005_";
                        try
                        {
                            int ID = 0;
                            ID = int.Parse(PPID);
                            if (combinedCycle)
                                ID++;
                            DocName += ID.ToString();
                        }
                        catch (Exception e)
                        {

                        }

                        //add date to Name
                        mydate = date;
                        mydate = mydate.Replace("/", "");
                        DocName += "_" + mydate;
                   
                                        
                        switch (endid)
                        {
                            case "206":
                                endid = "Lowshan";
                                break;

                            case "232":
                                endid = "Gilan";
                                break;
                                
                        }

                        switch (Sendmonth)
                        {
                            case "1":
                                Sendmonth = "Farvardin";
                                break;
                            case "2":
                                Sendmonth = "Ordibehesht";
                                break;
                            case "3":
                                Sendmonth = "Khordad";
                                break;
                            case "4":
                                Sendmonth = "Tir";
                                break;
                            case "5":
                                Sendmonth = "Mordad";
                                break;
                            case "6":
                                Sendmonth = "Shahrivar";
                                break;
                            case "7":
                                Sendmonth = "Mehr";
                                break;
                            case "8":
                                Sendmonth = "Aban";
                                break;
                            case "9":
                                Sendmonth = "Azar";
                                break;
                            case "10":
                                Sendmonth = "Day";
                                break;
                            case "11":
                                Sendmonth = "Bahman";
                                break;
                            case "12":
                                Sendmonth = "Esfand";
                                break;
                           
                        }

                        
                        path = "ftp://" + ftpserverip + "/" + BaseData.GetInstance().M005Path + "/";
                        path += endid + @"/" + endyear + @"/" + Sendmonth + @"/" + Sendday + @"/";

                       break;
                      
                }
                

            }


            string TempDate = date;
            int Tempday = int.Parse(TempDate.Substring(8));
            int TempMonth = int.Parse(TempDate.Substring(5, 2));
            int Tempyear = int.Parse(TempDate.Substring(0, 4));
            string Stempday = "", Stempmonth = "";
           
            Stempday = Tempday.ToString();
          
            Stempmonth = TempMonth.ToString();

                

            if (!isftp)
            {
                //path = path + @"\" + Tempyear.ToString() + @"\" + Stempmonth + @"\" + Stempday + @"\" + DocName;
               // path = path + @"\" + Tempyear.ToString() + @"\" + Stempmonth + @"\" + DocName;

            }
            else if(isftp)
            {
                //path = path +DocName;

           
              bool success=FtpDownload(DocName + ".xls", path + ".xls", ftpusername, ftppassword);
              if (success)
              {
                  path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                   "\\NRI\\SBS" + "\\" + DocName;
              }
              else
              {
                 
                  path = "";
                  
              }
              
            }

            return path;
        }

//----------------------------------ftpdownload---------------------------------------
      private bool FtpDownload(string fileName, string path, string user, string pass)
        {
           
            bool done = false;
            FtpWebRequest reqFTP;
            FileStream outputStream = null;
      
            try
            {
                 string filePath = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                 "\\NRI\\SBS";
                               

                //filePath = <<The full path where the file is to be created.>>, 
                //fileName = <<Name of the file to be created(Need not be the name of the file on FTP server).>>
                
                                          
                // FileStream outputStream = new FileStream(filePath + "\\" + fileName, FileMode.Create);
                outputStream = new FileStream(filePath + "\\" + fileName, FileMode.Create);



                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(path));

              
                reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(user, pass);


                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                Stream ftpStream = response.GetResponseStream();
                long cl = response.ContentLength;
                int bufferSize = 2048;
                int readCount;
                byte[] buffer = new byte[bufferSize];

                readCount = ftpStream.Read(buffer, 0, bufferSize);
                while (readCount > 0)
                {
                    outputStream.Write(buffer, 0, readCount);
                    readCount = ftpStream.Read(buffer, 0, bufferSize);
                }

                ftpStream.Close();
                outputStream.Close();
                response.Close();

                done = true;
                             


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                done = false;


                ////////////////////delete created file /////////////////////////////////////////////

                outputStream.Close();

                string delfilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                 "\\NRI\\SBS";
                if (delfilePath != "" && File.Exists(delfilePath + "\\" + fileName))
                {

                    File.Delete(delfilePath + "\\" + fileName);
                }
                
            }

            return done;
        }
       
//-------------------------------M002---------------------------------------------
        public void M002(string date, string PPID, bool PPtype)
        {
            bool IsValid = true;
            string path = ReadM00Excel(ItemsToBeDownloaded.M002, date, PPID, PPtype);

            SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;

            string sheetName = "Sheet1";

            DataTable dde = Utilities.GetTable("select * from FilesNameFormat where filename='M002'");
            if (dde.Rows.Count > 0)
            {
                if (dde.Rows[0]["SheetName"].ToString().Trim() != "" || dde.Rows[0]["SheetName"].ToString().Trim() != null)
                {
                    sheetName = dde.Rows[0]["SheetName"].ToString();


                    if (sheetName == "PlantName")
                    {
                        string english = "";
                        DataTable vv = Utilities.GetTable("select * from powerplant where ppid='" + PPID + "'");
                        if (vv.Rows.Count > 0)
                        {
                            english = vv.Rows[0]["PPName"].ToString().Trim();
                            sheetName = english;
                        }
                    }
                    else if (sheetName == "PlantID")
                    {
                        sheetName = PPID;

                    }

                    
                }

            }
            //read from FRM002.xls into dataTable
            DataTable DT = null;
            string showerror = path;
           
            ////////////////////////// max version2////////////////////////////
            string finalpath = "";

            if (File.Exists(path.Remove(path.Length-1) + ".xls"))
                finalpath = path.Remove(path.Length - 1);

            if (File.Exists(path + ".xls"))
                finalpath = path;
            if (File.Exists(path + "1.xls"))
                finalpath = path + "1";
            if (File.Exists(path + "2.xls"))
                finalpath = path + "2";
            if (File.Exists(path + "3.xls"))
                finalpath = path + "3";
            if (File.Exists(path + "4.xls"))
                finalpath = path + "4";
            if (File.Exists(path + "5.xls"))
                finalpath = path + "5";
            if (File.Exists(path + "6.xls"))
                finalpath = path + "6";
            if (File.Exists(path + "7.xls"))
                finalpath = path + "7";
            if (File.Exists(path + "8.xls"))
                finalpath = path + "8";
            if (File.Exists(path + "9.xls"))
                finalpath = path + "9";
            if (File.Exists(path + "10.xls"))
                finalpath = path + "10";

            finalpath = finalpath + ".xls";
        
            path = finalpath;
            ///////////////////////////////////////////////////////////////////

            if (path != "" && File.Exists(path))
            {
                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                OleDbConnection objConn = new OleDbConnection(sConnectionString);
                objConn.Open();
                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + sheetName + "$]", objConn);
                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                objAdapter1.SelectCommand = objCmdSelect;
                DataSet objDataset1 = new DataSet();
                try
                {
                    objAdapter1.Fill(objDataset1);
                    DT = objDataset1.Tables[0];
                    //dataGridView1.DataSource = objDataset1.Tables[0].DefaultView;
                }
                catch (Exception e)
                {
                    if (messages == "")
                    {
                        messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                    }

                    DT=null;
                }
                objConn.Close();

            }
            else
            {
                AddNotExistMessage(showerror);
            }

            if (DT == null)
                IsValid = false;

            //IS IT A Valid File?
            //if ((IsValid) && (dataGridView1.Columns[1].HeaderText.Contains("M002")))
            if (IsValid) 
                //&& (DT.Columns[1].ColumnName.Contains("M002")))
            {
                SqlCommand MyCom = new SqlCommand();

                //Insert into DB (MainFRM002)
                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                Excel.Workbook book = null;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                //SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                int type = 0;
                int PID = 0;
                if (PPtype) type = 1;



                DataTable dg = Utilities.GetTable("select * from emsformat");
                /////////////////////////////////////////////////if market format//////////////////////////////////////////////////////////

                if (dg.Rows[0]["M002"].ToString() == "False")
                {

                    MyCom.CommandText = "INSERT INTO [MainFRM002] (PPID,TargetMarketDate,PPName,PPType,"
                    + "DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
                    MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                    MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
                    MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
                    MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if ((workSheet.Name == sheetName))
                        {
                            PID = Convert.ToInt32(PPID);

                            MyCom.Parameters["@id"].Value = PID;
                            MyCom.Parameters["@tdate"].Value = date;
                            if (((Excel.Range)workSheet.Cells[5, 2]).Value2 != null)
                                MyCom.Parameters["@name"].Value = ((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString();
                            else MyCom.Parameters["@name"].Value = "";
                            MyCom.Parameters["@type"].Value = type;
                            if (((Excel.Range)workSheet.Cells[2, 2]).Value2 != null)
                                MyCom.Parameters["@idate"].Value = ((Excel.Range)workSheet.Cells[2, 2]).Value2.ToString();
                            else MyCom.Parameters["@idate"].Value = "";
                            if (((Excel.Range)workSheet.Cells[3, 2]).Value2 != null)
                                MyCom.Parameters["@time"].Value = ((Excel.Range)workSheet.Cells[3, 2]).Value2.ToString();
                            else MyCom.Parameters["@time"].Value = "";
                            if (((Excel.Range)workSheet.Cells[7, 2]).Value2 != null)
                                MyCom.Parameters["@revision"].Value = ((Excel.Range)workSheet.Cells[7, 2]).Value2.ToString();
                            else MyCom.Parameters["@revision"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[8, 2]).Value2 != null)
                                MyCom.Parameters["@filled"].Value = ((Excel.Range)workSheet.Cells[8, 2]).Value2.ToString();
                            else MyCom.Parameters["@filled"].Value = "";
                            if (((Excel.Range)workSheet.Cells[9, 2]).Value2 != null)
                                MyCom.Parameters["@approved"].Value = ((Excel.Range)workSheet.Cells[9, 2]).Value2.ToString();
                            else MyCom.Parameters["@approved"].Value = "";
                        }
                    try
                    {

                        /////// delete previous records, if any:

                        SqlCommand MyComDeleteMain = new SqlCommand();
                        MyComDeleteMain.Connection = MyConnection;
                        MyComDeleteMain.CommandText = "Delete From [MainFRM002] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                        MyComDeleteMain.Parameters.Add("@id", SqlDbType.NChar, 10);
                        MyComDeleteMain.Parameters.Add("@tdate", SqlDbType.Char, 10);
                        MyComDeleteMain.Parameters.Add("@type", SqlDbType.SmallInt);
                        MyComDeleteMain.Parameters["@id"].Value = PID;
                        MyComDeleteMain.Parameters["@tdate"].Value = date;
                        MyComDeleteMain.Parameters["@type"].Value = type;
                        MyComDeleteMain.ExecuteNonQuery();
                        MyComDeleteMain.Dispose();
                        ///////////////////////////

                        /////// delete previous records, if any:

                        SqlCommand MyComDeleteBlock = new SqlCommand();
                        MyComDeleteBlock.Connection = MyConnection;
                        MyComDeleteBlock.CommandText = "Delete From [BlockFRM002] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                        MyComDeleteBlock.Parameters.Add("@id", SqlDbType.NChar, 10);
                        MyComDeleteBlock.Parameters.Add("@tdate", SqlDbType.Char, 10);
                        MyComDeleteBlock.Parameters.Add("@type", SqlDbType.SmallInt);
                        MyComDeleteBlock.Parameters["@id"].Value = PID;
                        MyComDeleteBlock.Parameters["@tdate"].Value = date;
                        MyComDeleteBlock.Parameters["@type"].Value = type;
                        MyComDeleteBlock.ExecuteNonQuery();
                        MyComDeleteBlock.Dispose();
                        ///////////////////////////

                        SqlCommand MyComDelete = new SqlCommand();
                        MyComDelete.Connection = MyConnection;

                        MyComDelete.CommandText = "delete from DetailFRM002 where TargetMarketDate=@deleteDate" +
                                " AND PPID=@ppID AND PPType=@ppType AND Estimated<>1";
                        MyComDelete.Parameters.Add("@deleteDate", SqlDbType.Char, 10);
                        MyComDelete.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                        MyComDelete.Parameters.Add("@ppType", SqlDbType.SmallInt);

                        MyComDelete.Parameters["@PPID"].Value = PID;
                        MyComDelete.Parameters["@deleteDate"].Value = date;
                        MyComDelete.Parameters["@ppType"].Value = type;

                        MyComDelete.ExecuteNonQuery();
                        MyComDelete.Dispose();

                        /////// END delete

                        MyCom.ExecuteNonQuery();
                    }
                    catch (Exception exp)
                    {
                        if (messages == "")
                        {
                            messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                        }

                        string str = exp.Message;
                    }

                    //Insert into DB (BlockFRM002)
                    int x = 10;
                    MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@peak", SqlDbType.Real);
                    MyCom.Parameters.Add("@max", SqlDbType.Real);

                    //read directly and cell by cell
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if ((workSheet.Name == sheetName))
                        {

                            while (x < (DT.Rows.Count - 1))
                            {
                                if (DT.Rows[x][0].ToString() != "")
                                {
                                    MyCom.CommandText = "INSERT INTO [BlockFRM002] (PPID,TargetMarketDate,PPType,Block,"
                                    + "Peak,MaxDailyGeneration) VALUES (@id,@tdate,@type,@block,@peak,@max)";

                                    MyCom.Parameters["@id"].Value = PID;
                                    MyCom.Parameters["@tdate"].Value = date;
                                    MyCom.Parameters["@type"].Value = type;
                                    if (((Excel.Range)workSheet.Cells[x + 2, 1]).Value2 != null)
                                        MyCom.Parameters["@block"].Value = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString();
                                    else MyCom.Parameters["@block"].Value = "";
                                    if (((Excel.Range)workSheet.Cells[x + 2, 2]).Value2 != null)
                                        MyCom.Parameters["@peak"].Value = ((Excel.Range)workSheet.Cells[x + 2, 2]).Value2.ToString();
                                    else MyCom.Parameters["@peak"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2, 3]).Value2 != null)
                                        MyCom.Parameters["@max"].Value = ((Excel.Range)workSheet.Cells[x + 2, 3]).Value2.ToString();
                                    else MyCom.Parameters["@max"].Value = 0;
                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        if (messages == "")
                                        {
                                            messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                                        }

                                        string str = exp.Message;
                                    }
                                }
                                x++;
                            }
                        }
                    //Insert into DB (DetailFRM002)
                    x = 10;
                    MyCom.Parameters.Add("@deccap", SqlDbType.Real);
                    MyCom.Parameters.Add("@dispachcap", SqlDbType.Real);
                    MyCom.Parameters.Add("@price1", SqlDbType.Real);
                    MyCom.Parameters.Add("@power1", SqlDbType.Real);
                    MyCom.Parameters.Add("@price2", SqlDbType.Real);
                    MyCom.Parameters.Add("@power2", SqlDbType.Real);
                    MyCom.Parameters.Add("@price3", SqlDbType.Real);
                    MyCom.Parameters.Add("@power3", SqlDbType.Real);
                    MyCom.Parameters.Add("@price4", SqlDbType.Real);
                    MyCom.Parameters.Add("@power4", SqlDbType.Real);
                    MyCom.Parameters.Add("@price5", SqlDbType.Real);
                    MyCom.Parameters.Add("@power5", SqlDbType.Real);
                    MyCom.Parameters.Add("@price6", SqlDbType.Real);
                    MyCom.Parameters.Add("@power6", SqlDbType.Real);
                    MyCom.Parameters.Add("@price7", SqlDbType.Real);
                    MyCom.Parameters.Add("@power7", SqlDbType.Real);
                    MyCom.Parameters.Add("@price8", SqlDbType.Real);
                    MyCom.Parameters.Add("@power8", SqlDbType.Real);
                    MyCom.Parameters.Add("@price9", SqlDbType.Real);
                    MyCom.Parameters.Add("@power9", SqlDbType.Real);
                    MyCom.Parameters.Add("@price10", SqlDbType.Real);
                    MyCom.Parameters.Add("@power10", SqlDbType.Real);
                    MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);

                    string timefill = new PersianDate(DateTime.Now).ToString("d") + "|" + DateTime.Now.Hour + ":" + DateTime.Now.Minute;
                    //read directly and cell by cell
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if ((workSheet.Name == sheetName))
                        {
                            while (x < (DT.Rows.Count - 2))
                            {
                                if (DT.Rows[x][0].ToString() != "")
                                {

                                    for (int j = 0; j < 24; j++)
                                    {
                                        MyCom.CommandText = "INSERT INTO [DetailFRM002] (TargetMarketDate,PPID,Block,PPType,Hour" +
                                        ",DeclaredCapacity,DispachableCapacity,Power1,Price1,Power2,Price2,Power3,Price3,Power4," +
                                        "Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10," +
                                        "Price10,FilledBy,TimeFilled) VALUES (@tdate,@id,@block,@type,@hour,@deccap,@dispachcap,@power1," +
                                        "@price1,@power2,@price2,@power3,@price3,@power4,@price4,@power5,@price5,@power6,@price6," +
                                        "@power7,@price7,@power8,@price8,@power9,@price9,@power10,@price10,'" + "test" + "','" + timefill + "')";


                                        MyCom.Parameters["@id"].Value = PID;
                                        MyCom.Parameters["@tdate"].Value = date;
                                        if (((Excel.Range)workSheet.Cells[x + 2, 1]).Value2 != null)
                                            MyCom.Parameters["@block"].Value = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString();
                                        else MyCom.Parameters["@block"].Value = "";
                                        MyCom.Parameters["@type"].Value = type;
                                        MyCom.Parameters["@hour"].Value = j + 1;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 5]).Value2 != null)
                                            MyCom.Parameters["@deccap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 5]).Value2.ToString();
                                        else MyCom.Parameters["@deccap"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2 != null)
                                            MyCom.Parameters["@dispachcap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2.ToString();
                                        else MyCom.Parameters["@dispachcap"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 8]).Value2 != null)
                                            MyCom.Parameters["@power1"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 8]).Value2.ToString();
                                        else MyCom.Parameters["@power1"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 9]).Value2 != null)
                                            MyCom.Parameters["@price1"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 9]).Value2.ToString();
                                        else MyCom.Parameters["@price1"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 10]).Value2 != null)
                                            MyCom.Parameters["@power2"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 10]).Value2.ToString();
                                        else MyCom.Parameters["@power2"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 11]).Value2 != null)
                                            MyCom.Parameters["@price2"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 11]).Value2.ToString();
                                        else MyCom.Parameters["@price2"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 12]).Value2 != null)
                                            MyCom.Parameters["@power3"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 12]).Value2.ToString();
                                        else MyCom.Parameters["@power3"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 13]).Value2 != null)
                                            MyCom.Parameters["@price3"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 13]).Value2.ToString();
                                        else MyCom.Parameters["@price3"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 14]).Value2 != null)
                                            MyCom.Parameters["@power4"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 14]).Value2.ToString();
                                        else MyCom.Parameters["@power4"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 15]).Value2 != null)
                                            MyCom.Parameters["@price4"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 15]).Value2.ToString();
                                        else MyCom.Parameters["@price4"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 16]).Value2 != null)
                                            MyCom.Parameters["@power5"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 16]).Value2.ToString();
                                        else MyCom.Parameters["@power5"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 17]).Value2 != null)
                                            MyCom.Parameters["@price5"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 17]).Value2.ToString();
                                        else MyCom.Parameters["@price5"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 18]).Value2 != null)
                                            MyCom.Parameters["@power6"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 18]).Value2.ToString();
                                        else MyCom.Parameters["@power6"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 19]).Value2 != null)
                                            MyCom.Parameters["@price6"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 19]).Value2.ToString();
                                        else MyCom.Parameters["@price6"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 20]).Value2 != null)
                                            MyCom.Parameters["@power7"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 20]).Value2.ToString();
                                        else MyCom.Parameters["@power7"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 21]).Value2 != null)
                                            MyCom.Parameters["@price7"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 21]).Value2.ToString();
                                        else MyCom.Parameters["@price7"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 22]).Value2 != null)
                                            MyCom.Parameters["@power8"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 22]).Value2.ToString();
                                        else MyCom.Parameters["@power8"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 23]).Value2 != null)
                                            MyCom.Parameters["@price8"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 23]).Value2.ToString();
                                        else MyCom.Parameters["@price8"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 24]).Value2 != null)
                                            MyCom.Parameters["@power9"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 24]).Value2.ToString();
                                        else MyCom.Parameters["@power9"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 25]).Value2 != null)
                                            MyCom.Parameters["@price9"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 25]).Value2.ToString();
                                        else MyCom.Parameters["@price9"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 26]).Value2 != null)
                                            MyCom.Parameters["@power10"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 26]).Value2.ToString();
                                        else MyCom.Parameters["@power10"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 27]).Value2 != null)
                                            MyCom.Parameters["@price10"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 27]).Value2.ToString();
                                        else MyCom.Parameters["@price10"].Value = 0;
                                        try
                                        {
                                            MyCom.ExecuteNonQuery();
                                        }
                                        catch (Exception exp)
                                        {
                                            if (messages == "")
                                            {
                                                messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                                            }

                                            string str = exp.Message;
                                        }
                                    }
                                }
                                x++;
                            }
                        }


                }

                /////////////////////////////////////////////if ems format///////////////////////////////////////////////////////////
                else
                {

                    MyCom.CommandText = "INSERT INTO [MainFRM002] (PPID,TargetMarketDate,PPName,PPType,"
                    + "DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
                    MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                    MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
                    MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
                    MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if ((workSheet.Name == sheetName))
                        {
                            PID = Convert.ToInt32(PPID);
                            MyCom.Parameters["@id"].Value = PID;
                            MyCom.Parameters["@tdate"].Value = date;                            
                            MyCom.Parameters["@name"].Value = "";
                            MyCom.Parameters["@type"].Value = type;                            
                            MyCom.Parameters["@idate"].Value = "";                           
                            MyCom.Parameters["@time"].Value = "";                           
                            MyCom.Parameters["@revision"].Value = 0;                          
                            MyCom.Parameters["@filled"].Value = "";                           
                            MyCom.Parameters["@approved"].Value = "";
                        }
                    try
                    {

                        /////// delete previous records, if any:

                        SqlCommand MyComDeleteMain = new SqlCommand();
                        MyComDeleteMain.Connection = MyConnection;
                        MyComDeleteMain.CommandText = "Delete From [MainFRM002] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                        MyComDeleteMain.Parameters.Add("@id", SqlDbType.NChar, 10);
                        MyComDeleteMain.Parameters.Add("@tdate", SqlDbType.Char, 10);
                        MyComDeleteMain.Parameters.Add("@type", SqlDbType.SmallInt);
                        MyComDeleteMain.Parameters["@id"].Value = PID;
                        MyComDeleteMain.Parameters["@tdate"].Value = date;
                        MyComDeleteMain.Parameters["@type"].Value = type;
                        MyComDeleteMain.ExecuteNonQuery();
                        MyComDeleteMain.Dispose();
                        ///////////////////////////

                        /////// delete previous records, if any:

                        SqlCommand MyComDeleteBlock = new SqlCommand();
                        MyComDeleteBlock.Connection = MyConnection;
                        MyComDeleteBlock.CommandText = "Delete From [BlockFRM002] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                        MyComDeleteBlock.Parameters.Add("@id", SqlDbType.NChar, 10);
                        MyComDeleteBlock.Parameters.Add("@tdate", SqlDbType.Char, 10);
                        MyComDeleteBlock.Parameters.Add("@type", SqlDbType.SmallInt);
                        MyComDeleteBlock.Parameters["@id"].Value = PID;
                        MyComDeleteBlock.Parameters["@tdate"].Value = date;
                        MyComDeleteBlock.Parameters["@type"].Value = type;
                        MyComDeleteBlock.ExecuteNonQuery();
                        MyComDeleteBlock.Dispose();
                        ///////////////////////////

                        SqlCommand MyComDelete = new SqlCommand();
                        MyComDelete.Connection = MyConnection;

                        MyComDelete.CommandText = "delete from DetailFRM002 where TargetMarketDate=@deleteDate" +
                                " AND PPID=@ppID AND PPType=@ppType AND Estimated<>1";
                        MyComDelete.Parameters.Add("@deleteDate", SqlDbType.Char, 10);
                        MyComDelete.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                        MyComDelete.Parameters.Add("@ppType", SqlDbType.SmallInt);

                        MyComDelete.Parameters["@PPID"].Value = PID;
                        MyComDelete.Parameters["@deleteDate"].Value = date;
                        MyComDelete.Parameters["@ppType"].Value = type;

                        MyComDelete.ExecuteNonQuery();
                        MyComDelete.Dispose();

                        /////// END delete

                        MyCom.ExecuteNonQuery();
                    }
                    catch (Exception exp)
                    {
                        if (messages == "")
                        {
                            messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                        }

                        string str = exp.Message;
                    }

                    //Insert into DB (BlockFRM002)
                    int x = 0;
                    MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                    MyCom.Parameters.Add("@peak", SqlDbType.Real);
                    MyCom.Parameters.Add("@max", SqlDbType.Real);

                    //read directly and cell by cell
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if ((workSheet.Name == sheetName))
                        {

                            while (x < (DT.Rows.Count - 1))
                            {
                                if (DT.Rows[x][0].ToString() != "")
                                {
                                    MyCom.CommandText = "INSERT INTO [BlockFRM002] (PPID,TargetMarketDate,PPType,Block,"
                                    + "Peak,MaxDailyGeneration) VALUES (@id,@tdate,@type,@block,@peak,@max)";

                                    MyCom.Parameters["@id"].Value = PID;
                                    MyCom.Parameters["@tdate"].Value = date;
                                    MyCom.Parameters["@type"].Value = type;
                                    if (((Excel.Range)workSheet.Cells[x + 2, 2]).Value2 != null)
                                    {
                                        string[] n = ((Excel.Range)workSheet.Cells[x + 2, 2]).Value2.ToString().Split('-');
                                        string[] m = n[1].Split('/');
                                        MyCom.Parameters["@block"].Value = m[0];
                                    }
                                    else
                                    {
                                        MyCom.Parameters["@block"].Value = "";
                                    }
                                    if (((Excel.Range)workSheet.Cells[x + 2, 3]).Value2 != null)
                                        MyCom.Parameters["@peak"].Value = ((Excel.Range)workSheet.Cells[x + 2, 3]).Value2.ToString();
                                    else MyCom.Parameters["@peak"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2, 4]).Value2 != null)
                                        MyCom.Parameters["@max"].Value = ((Excel.Range)workSheet.Cells[x + 2, 4]).Value2.ToString();
                                    else MyCom.Parameters["@max"].Value = 0;
                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        if (messages == "")
                                        {
                                            messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                                        }

                                        string str = exp.Message;
                                    }
                                }
                                x++;
                            }
                        }

                    //Insert into DB (DetailFRM002)
                    x = 0;
                    MyCom.Parameters.Add("@deccap", SqlDbType.Real);
                    MyCom.Parameters.Add("@dispachcap", SqlDbType.Real);
                    MyCom.Parameters.Add("@price1", SqlDbType.Real);
                    MyCom.Parameters.Add("@power1", SqlDbType.Real);
                    MyCom.Parameters.Add("@price2", SqlDbType.Real);
                    MyCom.Parameters.Add("@power2", SqlDbType.Real);
                    MyCom.Parameters.Add("@price3", SqlDbType.Real);
                    MyCom.Parameters.Add("@power3", SqlDbType.Real);
                    MyCom.Parameters.Add("@price4", SqlDbType.Real);
                    MyCom.Parameters.Add("@power4", SqlDbType.Real);
                    MyCom.Parameters.Add("@price5", SqlDbType.Real);
                    MyCom.Parameters.Add("@power5", SqlDbType.Real);
                    MyCom.Parameters.Add("@price6", SqlDbType.Real);
                    MyCom.Parameters.Add("@power6", SqlDbType.Real);
                    MyCom.Parameters.Add("@price7", SqlDbType.Real);
                    MyCom.Parameters.Add("@power7", SqlDbType.Real);
                    MyCom.Parameters.Add("@price8", SqlDbType.Real);
                    MyCom.Parameters.Add("@power8", SqlDbType.Real);
                    MyCom.Parameters.Add("@price9", SqlDbType.Real);
                    MyCom.Parameters.Add("@power9", SqlDbType.Real);
                    MyCom.Parameters.Add("@price10", SqlDbType.Real);
                    MyCom.Parameters.Add("@power10", SqlDbType.Real);
                    MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);

                    string timefill = new PersianDate(DateTime.Now).ToString("d") + "|" + DateTime.Now.Hour + ":" + DateTime.Now.Minute;
                    //read directly and cell by cell
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if ((workSheet.Name == sheetName))
                        {
                            while (x < (DT.Rows.Count - 2))
                            {
                                if (DT.Rows[x][0].ToString() != "")
                                {

                                    for (int j = 0; j < 24; j++)
                                    {
                                        MyCom.CommandText = "INSERT INTO [DetailFRM002] (TargetMarketDate,PPID,Block,PPType,Hour" +
                                        ",DeclaredCapacity,DispachableCapacity,Power1,Price1,Power2,Price2,Power3,Price3,Power4," +
                                        "Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10," +
                                        "Price10,FilledBy,TimeFilled) VALUES (@tdate,@id,@block,@type,@hour,@deccap,@dispachcap,@power1," +
                                        "@price1,@power2,@price2,@power3,@price3,@power4,@price4,@power5,@price5,@power6,@price6," +
                                        "@power7,@price7,@power8,@price8,@power9,@price9,@power10,@price10,'" + "test" + "','" + timefill + "')";


                                        MyCom.Parameters["@id"].Value = PID;
                                        MyCom.Parameters["@tdate"].Value = date;
                                        if (((Excel.Range)workSheet.Cells[x + 2, 2]).Value2 != null)
                                        {
                                            string[] n = ((Excel.Range)workSheet.Cells[x + 2, 2]).Value2.ToString().Split('-');
                                            string[] m = n[1].Split('/');
                                            MyCom.Parameters["@block"].Value = m[0];
                                            
                                        }
                                        else MyCom.Parameters["@block"].Value = "";
                                        MyCom.Parameters["@type"].Value = type;
                                        MyCom.Parameters["@hour"].Value = j + 1;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 9]).Value2 != null)
                                            MyCom.Parameters["@deccap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 9]).Value2.ToString();
                                        else MyCom.Parameters["@deccap"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 11]).Value2 != null)
                                            MyCom.Parameters["@dispachcap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 11]).Value2.ToString();
                                        else MyCom.Parameters["@dispachcap"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 12]).Value2 != null)
                                            MyCom.Parameters["@power1"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 12]).Value2.ToString();
                                        else MyCom.Parameters["@power1"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 13]).Value2 != null)
                                            MyCom.Parameters["@price1"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 13]).Value2.ToString();
                                        else MyCom.Parameters["@price1"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 14]).Value2 != null)
                                            MyCom.Parameters["@power2"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 14]).Value2.ToString();
                                        else MyCom.Parameters["@power2"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 15]).Value2 != null)
                                            MyCom.Parameters["@price2"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 15]).Value2.ToString();
                                        else MyCom.Parameters["@price2"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 16]).Value2 != null)
                                            MyCom.Parameters["@power3"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 16]).Value2.ToString();
                                        else MyCom.Parameters["@power3"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 17]).Value2 != null)
                                            MyCom.Parameters["@price3"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 17]).Value2.ToString();
                                        else MyCom.Parameters["@price3"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 18]).Value2 != null)
                                            MyCom.Parameters["@power4"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 18]).Value2.ToString();
                                        else MyCom.Parameters["@power4"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 19]).Value2 != null)
                                            MyCom.Parameters["@price4"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 19]).Value2.ToString();
                                        else MyCom.Parameters["@price4"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 20]).Value2 != null)
                                            MyCom.Parameters["@power5"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 20]).Value2.ToString();
                                        else MyCom.Parameters["@power5"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 21]).Value2 != null)
                                            MyCom.Parameters["@price5"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 21]).Value2.ToString();
                                        else MyCom.Parameters["@price5"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 22]).Value2 != null)
                                            MyCom.Parameters["@power6"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 22]).Value2.ToString();
                                        else MyCom.Parameters["@power6"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 23]).Value2 != null)
                                            MyCom.Parameters["@price6"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 23]).Value2.ToString();
                                        else MyCom.Parameters["@price6"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 24]).Value2 != null)
                                            MyCom.Parameters["@power7"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 24]).Value2.ToString();
                                        else MyCom.Parameters["@power7"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 25]).Value2 != null)
                                            MyCom.Parameters["@price7"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 25]).Value2.ToString();
                                        else MyCom.Parameters["@price7"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 26]).Value2 != null)
                                            MyCom.Parameters["@power8"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 26]).Value2.ToString();
                                        else MyCom.Parameters["@power8"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 27]).Value2 != null)
                                            MyCom.Parameters["@price8"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 27]).Value2.ToString();
                                        else MyCom.Parameters["@price8"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 28]).Value2 != null)
                                            MyCom.Parameters["@power9"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 28]).Value2.ToString();
                                        else MyCom.Parameters["@power9"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 29]).Value2 != null)
                                            MyCom.Parameters["@price9"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 29]).Value2.ToString();
                                        else MyCom.Parameters["@price9"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 30]).Value2 != null)
                                            MyCom.Parameters["@power10"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 30]).Value2.ToString();
                                        else MyCom.Parameters["@power10"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2 + j, 31]).Value2 != null)
                                            MyCom.Parameters["@price10"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 31]).Value2.ToString();
                                        else MyCom.Parameters["@price10"].Value = 0;
                                        try
                                        {
                                            MyCom.ExecuteNonQuery();
                                        }
                                        catch (Exception exp)
                                        {
                                            if (messages == "")
                                            {
                                                messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                                            }

                                            string str = exp.Message;
                                        }
                                    }
                                }
                                x++;
                            }
                        }





                }
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////


                book.Close(false, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();
                System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
            }
            //else MessageBox.Show("Selected File Isnot Valid!");

            if (Auto_Update_Library.BaseData.GetInstance().Useftp002)
            {
                if (path != "" && File.Exists(path + ".xls"))
                {

                    File.Delete(path + ".xls");
                }

            }
                       

        }
//----------------------------AddNotExistMessage-----------------------------
        private void AddNotExistMessage(string path)
        {
            messages += "Excel File " + path + " does not exist...\r\n";
        }
        //--------------------------M005(string date)----------------------------------------
        public void M005(string date, string PPID , bool PPtype)
        {
            bool IsValid = true;

            string path = ReadM00Excel(ItemsToBeDownloaded.M005, date, PPID, PPtype);
          

            SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;

            string sheetName = "FRM005";

            DataTable dde = Utilities.GetTable("select * from FilesNameFormat where filename='M005'");
            if (dde.Rows.Count > 0)
            {
                if (dde.Rows[0]["SheetName"].ToString().Trim() != "" || dde.Rows[0]["SheetName"].ToString().Trim() != null)
                {
                    sheetName = dde.Rows[0]["SheetName"].ToString();


                    if (sheetName == "PlantName")
                    {

                        string english = "";
                        DataTable vv = Utilities.GetTable("select * from powerplant where ppid='" + PPID + "'");
                        if (vv.Rows.Count > 0)
                        {
                            english = vv.Rows[0]["PPName"].ToString().Trim();
                            sheetName = english;
                        }
                    }
                    else if (sheetName == "PlantID")
                    {
                        sheetName = PPID;

                    }

                }

            }
           
            //read from FRM005.xls into dataTable
            DataTable DT = null;

            if (path != "" && File.Exists(path + ".xls"))
            {
                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + path + ".xls; Extended Properties=Excel 4.0;";
                OleDbConnection objConn = new OleDbConnection(sConnectionString);
                objConn.Open();
                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + sheetName + "$]", objConn);
                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                objAdapter1.SelectCommand = objCmdSelect;
                DataSet objDataset1 = new DataSet();
                try
                {
                    objAdapter1.Fill(objDataset1);
                    DT = objDataset1.Tables[0];
                    //dataGridView1.DataSource = objDataset1.Tables[0].DefaultView;
                }
                catch (Exception e)
                {
                    if (messages == "")
                    {
                        messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                    }

                    DT = null;
                }
                objConn.Close();

            }
            else
            {
                AddNotExistMessage(path);
            }

            if (DT == null)
                IsValid = false;

            //IS IT A Valid File?
            //if ((IsValid) && (DT.Columns[1].ColumnName.Contains("M005")))
            if (IsValid)
            {

                //Insert into DB (MainFRM005)
                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                Excel.Workbook book = null;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                book = exobj.Workbooks.Open(path+".xls", 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                //SqlConnection MyConnection = new SqlConnection();
                //MyConnection.ConnectionString = ConStr;
                //MyConnection.Open();
                //SqlCommand MyCom = new SqlCommand();
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                int type = 0;
                int PID = 0;
                if (PPtype) type = 1;

                MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
                MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
                MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
                MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
                MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

                MyCom.CommandText = "INSERT INTO [MainFRM005] (PPID,TargetMarketDate,PPName,PPType,"
                + "DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
                PID = Convert.ToInt32(PPID);

                MyCom.Parameters["@id"].Value = PID;
                MyCom.Parameters["@tdate"].Value = date;
                MyCom.Parameters["@name"].Value = DT.Rows[3][1].ToString();
                MyCom.Parameters["@type"].Value = type;
                MyCom.Parameters["@idate"].Value = DT.Rows[0][1].ToString();
                MyCom.Parameters["@time"].Value = DT.Rows[1][1].ToString();
                MyCom.Parameters["@revision"].Value = DT.Rows[4][1].ToString();
                MyCom.Parameters["@filled"].Value = DT.Rows[5][1].ToString();
                MyCom.Parameters["@approved"].Value = DT.Rows[6][1].ToString();

                try
                {
                    /////// delete previous records, if any:

                    SqlCommand MyComDeleteMain = new SqlCommand();
                    MyComDeleteMain.Connection = MyConnection;
                    MyComDeleteMain.CommandText = "Delete From [MainFRM005] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                    MyComDeleteMain.Parameters.Add("@id", SqlDbType.NChar, 10);
                    MyComDeleteMain.Parameters.Add("@tdate", SqlDbType.Char, 10);
                    MyComDeleteMain.Parameters.Add("@type", SqlDbType.SmallInt);
                    MyComDeleteMain.Parameters["@id"].Value = PID;
                    MyComDeleteMain.Parameters["@tdate"].Value = date;
                    MyComDeleteMain.Parameters["@type"].Value = type;
                    MyComDeleteMain.ExecuteNonQuery();
                    MyComDeleteMain.Dispose();
                    ///////////////////////////

                    /////// delete previous records, if any:

                    SqlCommand MyComDeleteBlock = new SqlCommand();
                    MyComDeleteBlock.Connection = MyConnection;
                    MyComDeleteBlock.CommandText = "Delete From [BlockFRM005] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                    MyComDeleteBlock.Parameters.Add("@id", SqlDbType.NChar, 10);
                    MyComDeleteBlock.Parameters.Add("@tdate", SqlDbType.Char, 10);
                    MyComDeleteBlock.Parameters.Add("@type", SqlDbType.SmallInt);
                    MyComDeleteBlock.Parameters["@id"].Value = PID;
                    MyComDeleteBlock.Parameters["@tdate"].Value = date;
                    MyComDeleteBlock.Parameters["@type"].Value = type;
                    MyComDeleteBlock.ExecuteNonQuery();
                    MyComDeleteBlock.Dispose();
                    ///////////////////////////

                    SqlCommand MyComDelete = new SqlCommand();
                    MyComDelete.Connection = MyConnection;

                    MyComDelete.CommandText = "delete from DetailFRM005 where TargetMarketDate=@deleteDate" +
                            " AND PPID=@ppID AND PPType=@ppType";
                    MyComDelete.Parameters.Add("@deleteDate", SqlDbType.Char, 10);
                    MyComDelete.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                    MyComDelete.Parameters.Add("@ppType", SqlDbType.SmallInt);

                    MyComDelete.Parameters["@ppID"].Value = PID;
                    MyComDelete.Parameters["@deleteDate"].Value = date;
                    MyComDelete.Parameters["@ppType"].Value = type;

                    MyComDelete.ExecuteNonQuery();
                    MyComDelete.Dispose();

                    /////// END delete


                    MyCom.ExecuteNonQuery();
                }
                catch (Exception exp)
                {
                    if (messages == "")
                    {
                        messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                    }

                    string str = exp.Message;
                }

                //Insert into DB (BlockFRM005)
                int x = 10;
                MyCom.Parameters.Add("@num", SqlDbType.NChar, 20);
                MyCom.Parameters.Add("@prequired", SqlDbType.Real);
                MyCom.Parameters.Add("@pdispach", SqlDbType.Real);
                MyCom.Parameters.Add("@drequierd", SqlDbType.Real);
                MyCom.Parameters.Add("@ddispach", SqlDbType.Real);

                //read directly and cell by cell
                foreach (Excel.Worksheet workSheet in book.Worksheets)
                    if (workSheet.Name == sheetName)
                    {
                        while (x < (DT.Rows.Count - 1))
                        {
                            if (DT.Rows[x][0].ToString() != "")
                            {
                                MyCom.CommandText = "INSERT INTO [BlockFRM005] (PPID,TargetMarketDate,PPType,Block,"
                                + "PeakRequired,PeakDispatchable,DailyTotalRequired,DailyTotalDispachable)"
                                + "VALUES (@id,@tdate,@type,@num,@prequired,@pdispach,@drequierd,@ddispach)";

                                MyCom.Parameters["@id"].Value = PID;
                                MyCom.Parameters["@tdate"].Value = date;
                                MyCom.Parameters["@num"].Value = DT.Rows[x][0].ToString();
                                MyCom.Parameters["@type"].Value = type;
                                if (((Excel.Range)workSheet.Cells[x + 2, 27]).Value2 != null)
                                    MyCom.Parameters["@prequired"].Value = ((Excel.Range)workSheet.Cells[x + 2, 27]).Value2.ToString();
                                else MyCom.Parameters["@prequired"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[x + 3, 27]).Value2 != null)
                                    MyCom.Parameters["@pdispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, 27]).Value2.ToString();
                                else MyCom.Parameters["@pdispach"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[x + 2, 28]).Value2 != null)
                                    MyCom.Parameters["@drequierd"].Value = ((Excel.Range)workSheet.Cells[x + 2, 28]).Value2.ToString();
                                else MyCom.Parameters["@drequierd"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[x + 3, 28]).Value2 != null)
                                    MyCom.Parameters["@ddispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, 28]).Value2.ToString();
                                else MyCom.Parameters["@ddispach"].Value = 0;
                                try
                                {

                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception exp)
                                {
                                    if (messages == "")
                                    {
                                        messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                                    }

                                    string str = exp.Message;
                                }
                            }
                            x++;
                        }
                    }
                //Insert into DB (DetailFRM005)
                x = 10;
                MyCom.Parameters.Add("@required", SqlDbType.Real);
                MyCom.Parameters.Add("@dispach", SqlDbType.Real);
                MyCom.Parameters.Add("@economic", SqlDbType.Real);
                MyCom.Parameters.Add("@StartUL", SqlDbType.Real);
                MyCom.Parameters.Add("@EndUL", SqlDbType.Real);
                MyCom.Parameters.Add("@StartOC", SqlDbType.Real);
                MyCom.Parameters.Add("@EndOC", SqlDbType.Real);
                MyCom.Parameters.Add("@contribution", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);

                //read directly and cell by cell
                foreach (Excel.Worksheet workSheet in book.Worksheets)
                    if (workSheet.Name == sheetName)
                    {
                        while (x < (DT.Rows.Count - 2))
                        {
                            if (DT.Rows[x][0].ToString() != "")
                            {
                                for (int j = 0; j < 24; j++)
                                {
                                    MyCom.CommandText = "INSERT INTO [DetailFRM005] (TargetMarketDate,PPID,Block,PPType,Hour,Required,Dispatchable,Economic,Contribution,StartUL,EndUL,StartOC,EndOC) VALUES (@tdate,@id,@num,@type,@hour,@required,@dispach,@economic,@contribution,@StartUL,@EndUL,@StartOC,@EndOC)";

                                    MyCom.Parameters["@id"].Value = PID;
                                    MyCom.Parameters["@tdate"].Value = date;
                                    MyCom.Parameters["@num"].Value = DT.Rows[x][0].ToString();
                                    MyCom.Parameters["@type"].Value = type;

                                    MyCom.Parameters["@hour"].Value = j + 1;
                                    if (((Excel.Range)workSheet.Cells[x + 2, j + 3]).Value2 != null)
                                        MyCom.Parameters["@required"].Value = ((Excel.Range)workSheet.Cells[x + 2, j + 3]).Value2.ToString();
                                    else MyCom.Parameters["@required"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 3, j + 3]).Value2 != null)
                                        MyCom.Parameters["@dispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, j + 3]).Value2.ToString();
                                    else MyCom.Parameters["@dispach"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2 != null)
                                        MyCom.Parameters["@economic"].Value = ((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@economic"].Value = 0;

                                    //add field/////////////////////////////////////////////////////////////////////////
                                    if (((Excel.Range)workSheet.Cells[x + 5, j + 3]).Value2 != null)
                                        MyCom.Parameters["@StartUL"].Value = ((Excel.Range)workSheet.Cells[x + 5, j + 3]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@StartUL"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 6, j + 3]).Value2 != null)
                                        MyCom.Parameters["@EndUL"].Value = ((Excel.Range)workSheet.Cells[x + 6, j + 3]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@EndUL"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 7, j + 3]).Value2 != null)
                                        MyCom.Parameters["@StartOC"].Value = ((Excel.Range)workSheet.Cells[x + 7, j + 3]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@StartOC"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 8, j + 3]).Value2 != null)
                                        MyCom.Parameters["@EndOC"].Value = ((Excel.Range)workSheet.Cells[x + 8, j + 3]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@EndOC"].Value = 0;
                                    //////////////////////////////////////////////////////////////////////////////////////



                                    ////////////////////////////////no contributation/////////////////////////////////////////////////
                                    //if (((Excel.Range)workSheet.Cells[x + 5, j + 3]).Value2 != null)
                                    //    MyCom.Parameters["@contribution"].Value = ((Excel.Range)workSheet.Cells[x + 5, j + 3]).Value2.ToString().Trim();
                                    //else MyCom.Parameters["@contribution"].Value = null;

                                      MyCom.Parameters["@contribution"].Value = "N";

                                    ////////////////////////////////////////////////////////////////////////////

                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        if (messages == "")
                                        {
                                            messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                                        }

                                        string str = exp.Message;
                                    }
                                }
                            }
                            x++;
                        }
                    }

                book.Close(false, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();
                System.Threading.Thread.CurrentThread.CurrentCulture = oldci;

            }

            if (Auto_Update_Library.BaseData.GetInstance().Useftp005)
            {
                if (path != "" && File.Exists(path + ".xls"))
                {

                    File.Delete(path + ".xls");
                }

            }

        }

        public void M005ba(string date, string PPID, bool PPtype)
        {
            bool IsValid = true;

            string path = ReadM00Excelba(ItemsToBeDownloaded.M005ba, date, PPID, PPtype);

            SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;

            string sheetName = "FRM005";

            DataTable dde = Utilities.GetTable("select * from FilesNameFormat where filename='M005ba'");
            if (dde.Rows.Count > 0)
            {
                if (dde.Rows[0]["SheetName"].ToString().Trim() != "" || dde.Rows[0]["SheetName"].ToString().Trim() != null)
                {
                    sheetName = dde.Rows[0]["SheetName"].ToString();


                    if (sheetName == "PlantName")
                    {

                        string english = "";
                        DataTable vv = Utilities.GetTable("select * from powerplant where ppid='" + PPID + "'");
                        if (vv.Rows.Count > 0)
                        {
                            english = vv.Rows[0]["PPName"].ToString().Trim();
                            sheetName = english;
                        }
                    }
                    else if (sheetName == "PlantID")
                    {
                        sheetName = PPID;

                    }

                }

            }
            //read from FRM005.xls into dataTable
            DataTable DT = null;

            if (path != "" && File.Exists(path + ".xls"))
            {
                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ".xls;Extended Properties=Excel 8.0";
                OleDbConnection objConn = new OleDbConnection(sConnectionString);
                objConn.Open();
                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + sheetName + "$]", objConn);
                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                objAdapter1.SelectCommand = objCmdSelect;
                DataSet objDataset1 = new DataSet();
                try
                {
                    objAdapter1.Fill(objDataset1);
                    DT = objDataset1.Tables[0];
                    //dataGridView1.DataSource = objDataset1.Tables[0].DefaultView;
                }
                catch (Exception e)
                {
                    if (messages == "")
                    {
                        messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                    }

                    DT = null;
                }
                objConn.Close();

            }
            else
            {
                AddNotExistMessage(path);
            }

            if (DT == null)
                IsValid = false;

            //IS IT A Valid File?
            //if ((IsValid) && (DT.Columns[1].ColumnName.Contains("M005")))
            if (IsValid)
            {

                //Insert into DB (MainFRM005)
                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                Excel.Workbook book = null;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                book = exobj.Workbooks.Open(path + ".xls", 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                //SqlConnection MyConnection = new SqlConnection();
                //MyConnection.ConnectionString = ConStr;
                //MyConnection.Open();
                //SqlCommand MyCom = new SqlCommand();
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                int type = 0;
                int PID = 0;
                if (PPtype) type = 1;

                MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
                MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
                MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
                MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
                MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

                MyCom.CommandText = "INSERT INTO [baMainFRM005] (PPID,TargetMarketDate,PPName,PPType,"
                + "DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
                PID = Convert.ToInt32(PPID);

                MyCom.Parameters["@id"].Value = PID;
                MyCom.Parameters["@tdate"].Value = date;
                MyCom.Parameters["@name"].Value = DT.Rows[3][1].ToString();
                MyCom.Parameters["@type"].Value = type;
                MyCom.Parameters["@idate"].Value = DT.Rows[0][1].ToString();
                MyCom.Parameters["@time"].Value = DT.Rows[1][1].ToString();
                MyCom.Parameters["@revision"].Value = DT.Rows[4][1].ToString();
                MyCom.Parameters["@filled"].Value = DT.Rows[5][1].ToString();
                MyCom.Parameters["@approved"].Value = DT.Rows[6][1].ToString();

                try
                {
                    /////// delete previous records, if any:

                    SqlCommand MyComDeleteMain = new SqlCommand();
                    MyComDeleteMain.Connection = MyConnection;
                    MyComDeleteMain.CommandText = "Delete From [baMainFRM005] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                    MyComDeleteMain.Parameters.Add("@id", SqlDbType.NChar, 10);
                    MyComDeleteMain.Parameters.Add("@tdate", SqlDbType.Char, 10);
                    MyComDeleteMain.Parameters.Add("@type", SqlDbType.SmallInt);
                    MyComDeleteMain.Parameters["@id"].Value = PID;
                    MyComDeleteMain.Parameters["@tdate"].Value = date;
                    MyComDeleteMain.Parameters["@type"].Value = type;
                    MyComDeleteMain.ExecuteNonQuery();
                    MyComDeleteMain.Dispose();
                    ///////////////////////////

                    /////// delete previous records, if any:

                    SqlCommand MyComDeleteBlock = new SqlCommand();
                    MyComDeleteBlock.Connection = MyConnection;
                    MyComDeleteBlock.CommandText = "Delete From [baBlockFRM005] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                    MyComDeleteBlock.Parameters.Add("@id", SqlDbType.NChar, 10);
                    MyComDeleteBlock.Parameters.Add("@tdate", SqlDbType.Char, 10);
                    MyComDeleteBlock.Parameters.Add("@type", SqlDbType.SmallInt);
                    MyComDeleteBlock.Parameters["@id"].Value = PID;
                    MyComDeleteBlock.Parameters["@tdate"].Value = date;
                    MyComDeleteBlock.Parameters["@type"].Value = type;
                    MyComDeleteBlock.ExecuteNonQuery();
                    MyComDeleteBlock.Dispose();
                    ///////////////////////////

                    SqlCommand MyComDelete = new SqlCommand();
                    MyComDelete.Connection = MyConnection;

                    MyComDelete.CommandText = "delete from baDetailFRM005 where TargetMarketDate=@deleteDate" +
                            " AND PPID=@ppID AND PPType=@ppType";
                    MyComDelete.Parameters.Add("@deleteDate", SqlDbType.Char, 10);
                    MyComDelete.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                    MyComDelete.Parameters.Add("@ppType", SqlDbType.SmallInt);

                    MyComDelete.Parameters["@ppID"].Value = PID;
                    MyComDelete.Parameters["@deleteDate"].Value = date;
                    MyComDelete.Parameters["@ppType"].Value = type;

                    MyComDelete.ExecuteNonQuery();
                    MyComDelete.Dispose();

                    /////// END delete


                    MyCom.ExecuteNonQuery();
                }
                catch (Exception exp)
                {
                    if (messages == "")
                    {
                        messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                    }

                    string str = exp.Message;
                }

                //Insert into DB (BlockFRM005)
                int x = 10;
                MyCom.Parameters.Add("@num", SqlDbType.NChar, 20);
                MyCom.Parameters.Add("@prequired", SqlDbType.Real);
                MyCom.Parameters.Add("@pdispach", SqlDbType.Real);
                MyCom.Parameters.Add("@drequierd", SqlDbType.Real);
                MyCom.Parameters.Add("@ddispach", SqlDbType.Real);

                //read directly and cell by cell
                foreach (Excel.Worksheet workSheet in book.Worksheets)
                    if (workSheet.Name == sheetName)
                    {
                        while (x < (DT.Rows.Count - 1))
                        {
                            if (DT.Rows[x][0].ToString() != "")
                            {
                                MyCom.CommandText = "INSERT INTO [baBlockFRM005] (PPID,TargetMarketDate,PPType,Block,"
                                + "PeakRequired,PeakDispatchable,DailyTotalRequired,DailyTotalDispachable)"
                                + "VALUES (@id,@tdate,@type,@num,@prequired,@pdispach,@drequierd,@ddispach)";

                                MyCom.Parameters["@id"].Value = PID;
                                MyCom.Parameters["@tdate"].Value = date;
                                MyCom.Parameters["@num"].Value = DT.Rows[x][0].ToString();
                                MyCom.Parameters["@type"].Value = type;
                                if (((Excel.Range)workSheet.Cells[x + 2, 27]).Value2 != null)
                                    MyCom.Parameters["@prequired"].Value = ((Excel.Range)workSheet.Cells[x + 2, 27]).Value2.ToString();
                                else MyCom.Parameters["@prequired"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[x + 3, 27]).Value2 != null)
                                    MyCom.Parameters["@pdispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, 27]).Value2.ToString();
                                else MyCom.Parameters["@pdispach"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[x + 2, 28]).Value2 != null)
                                    MyCom.Parameters["@drequierd"].Value = ((Excel.Range)workSheet.Cells[x + 2, 28]).Value2.ToString();
                                else MyCom.Parameters["@drequierd"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[x + 3, 28]).Value2 != null)
                                    MyCom.Parameters["@ddispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, 28]).Value2.ToString();
                                else MyCom.Parameters["@ddispach"].Value = 0;
                                try
                                {

                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception exp)
                                {
                                    if (messages == "")
                                    {
                                        messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                                    }

                                    string str = exp.Message;
                                }
                            }
                            x++;
                        }
                    }
                //Insert into DB (DetailFRM005)
                x = 10;
                MyCom.Parameters.Add("@required", SqlDbType.Real);
                MyCom.Parameters.Add("@dispach", SqlDbType.Real);
                MyCom.Parameters.Add("@economic", SqlDbType.Real);
                MyCom.Parameters.Add("@StartUL", SqlDbType.Real);
                MyCom.Parameters.Add("@EndUL", SqlDbType.Real);
                MyCom.Parameters.Add("@StartOC", SqlDbType.Real);
                MyCom.Parameters.Add("@EndOC", SqlDbType.Real);
                MyCom.Parameters.Add("@contribution", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);

                //read directly and cell by cell
                foreach (Excel.Worksheet workSheet in book.Worksheets)
                    if (workSheet.Name == sheetName)
                    {
                        while (x < (DT.Rows.Count - 2))
                        {
                            if (DT.Rows[x][0].ToString() != "")
                            {
                                for (int j = 0; j < 24; j++)
                                {
                                    MyCom.CommandText = "INSERT INTO [baDetailFRM005] (TargetMarketDate,PPID,Block,PPType,Hour,Required,Dispatchable,Economic,Contribution,StartUL,EndUL,StartOC,EndOC) VALUES (@tdate,@id,@num,@type,@hour,@required,@dispach,@economic,@contribution,@StartUL,@EndUL,@StartOC,@EndOC)";

                                    MyCom.Parameters["@id"].Value = PID;
                                    MyCom.Parameters["@tdate"].Value = date;
                                    MyCom.Parameters["@num"].Value = DT.Rows[x][0].ToString();
                                    MyCom.Parameters["@type"].Value = type;

                                    MyCom.Parameters["@hour"].Value = j + 1;
                                    if (((Excel.Range)workSheet.Cells[x + 2, j + 3]).Value2 != null)
                                        MyCom.Parameters["@required"].Value = ((Excel.Range)workSheet.Cells[x + 2, j + 3]).Value2.ToString();
                                    else MyCom.Parameters["@required"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 3, j + 3]).Value2 != null)
                                        MyCom.Parameters["@dispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, j + 3]).Value2.ToString();
                                    else MyCom.Parameters["@dispach"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2 != null)
                                        MyCom.Parameters["@economic"].Value = ((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@economic"].Value = 0;

                                    //add field/////////////////////////////////////////////////////////////////////////
                                    if (((Excel.Range)workSheet.Cells[x + 5, j + 3]).Value2 != null)
                                        MyCom.Parameters["@StartUL"].Value = ((Excel.Range)workSheet.Cells[x + 5, j + 3]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@StartUL"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 6, j + 3]).Value2 != null)
                                        MyCom.Parameters["@EndUL"].Value = ((Excel.Range)workSheet.Cells[x + 6, j + 3]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@EndUL"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 7, j + 3]).Value2 != null)
                                        MyCom.Parameters["@StartOC"].Value = ((Excel.Range)workSheet.Cells[x + 7, j + 3]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@StartOC"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 8, j + 3]).Value2 != null)
                                        MyCom.Parameters["@EndOC"].Value = ((Excel.Range)workSheet.Cells[x + 8, j + 3]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@EndOC"].Value = 0;
                                    //////////////////////////////////////////////////////////////////////////////////////


                                    ////////////////////////////NO CONTRIBUTATION //////////////////////////////
                                    //if (((Excel.Range)workSheet.Cells[x + 5, j + 3]).Value2 != null)
                                    //    MyCom.Parameters["@contribution"].Value = ((Excel.Range)workSheet.Cells[x + 5, j + 3]).Value2.ToString().Trim();
                                    //else MyCom.Parameters["@contribution"].Value = null;
                                    MyCom.Parameters["@contribution"].Value ="N";
                                    /////////////////////////////////////////////////////////////////////////////
                                    

                                 
                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        if (messages == "")
                                        {
                                            messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                                        }

                                        string str = exp.Message;
                                    }
                                }
                            }
                            x++;
                        }
                    }

                book.Close(false, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();
                System.Threading.Thread.CurrentThread.CurrentCulture = oldci;

            }

            if (Auto_Update_Library.BaseData.GetInstance().Useftp005)
            {
                if (path != "" && File.Exists(path + ".xls"))
                {

                    File.Delete(path + ".xls");
                }

            }

        }
        private string ReadM00Excelba(ItemsToBeDownloaded item, string date, string PPID, bool combinedCycle)
        {
            bool isftp = false;
            DataTable result = null;
            string path = "";
            string DocName = "";
            if ((!BaseData.GetInstance().Useftp002 && item == ItemsToBeDownloaded.M002) || (!BaseData.GetInstance().Useftp005ba && item == ItemsToBeDownloaded.M005ba))
            {
                isftp = false;
                switch (item)
                {
                   

                    case ItemsToBeDownloaded.M005ba:
                        //build Name for FRM005 files

                        try
                        {
                            path = BaseData.GetInstance().M005baPath;
                            int ID1 = 0;
                            ID1 = int.Parse(PPID);
                            if (combinedCycle)
                                ID1++;
                            string pathname = folderFormat(date, PPID, "M005ba");
                            if (pathname != "") path += pathname;
                            string makename = NameFormat(date, ID1.ToString(), "", "M005ba");
                            if (makename != "") path = path + "\\" + makename;
                        }
                        catch (Exception e)
                        {

                        }


                        break;


                }

                ///////////////////////////////////////////////////////////////


            }
            if ((BaseData.GetInstance().Useftp002 && item == ItemsToBeDownloaded.M002) || (BaseData.GetInstance().Useftp005 && item == ItemsToBeDownloaded.M005))
            {

                isftp = true;
                string Sendmonth = "";
                string Sendyear = "";
                string Sendday = "";
                string endid = "";
                string endDate = date;
                int endday = int.Parse(endDate.Substring(8));
                int endMonth = int.Parse(endDate.Substring(5, 2));
                int endyear = int.Parse(endDate.Substring(0, 4));
                Sendmonth = endMonth.ToString();
                Sendyear = endyear.ToString();
                Sendday = endday.ToString();

                switch (item)
                {
                    case ItemsToBeDownloaded.M002:
                        DocName = "FRM0022_";
                        try
                        {
                            int ID = 0;
                            ID = int.Parse(PPID);
                            if (combinedCycle)
                                ID++;
                            DocName += ID.ToString();
                        }
                        catch (Exception e)
                        {

                        }
                        //add date to Name
                        string mydate = date;
                        mydate = mydate.Replace("/", "");
                        DocName += "_" + mydate;
                        switch (Sendmonth)
                        {
                            case "1":
                                Sendmonth = "Farvardin";
                                break;
                            case "2":
                                Sendmonth = "Ordibehesht";
                                break;
                            case "3":
                                Sendmonth = "Khordad";
                                break;
                            case "4":
                                Sendmonth = "Tir";
                                break;
                            case "5":
                                Sendmonth = "Mordad";
                                break;
                            case "6":
                                Sendmonth = "Shahrivar";
                                break;
                            case "7":
                                Sendmonth = "Mehr";
                                break;
                            case "8":
                                Sendmonth = "Aban";
                                break;
                            case "9":
                                Sendmonth = "Azar";
                                break;
                            case "10":
                                Sendmonth = "Day";
                                break;
                            case "11":
                                Sendmonth = "Bahman";
                                break;
                            case "12":
                                Sendmonth = "Esfand";
                                break;

                        }


                        path = "ftp://" + ftpserverip + "/" + BaseData.GetInstance().M002Path + "/";
                        path += endyear + @"/" + Sendmonth + @"/" + Sendday + @"/" + Sendday + @"/";

                        break;




                    case ItemsToBeDownloaded.M005:
                        //build Name for FRM005 files
                        DocName = "FRM005_";
                        try
                        {
                            int ID = 0;
                            ID = int.Parse(PPID);
                            if (combinedCycle)
                                ID++;
                            DocName += ID.ToString();
                        }
                        catch (Exception e)
                        {

                        }

                        //add date to Name
                        mydate = date;
                        mydate = mydate.Replace("/", "");
                        DocName += "_" + mydate;


                        switch (endid)
                        {
                            case "206":
                                endid = "Lowshan";
                                break;

                            case "232":
                                endid = "Gilan";
                                break;

                        }

                        switch (Sendmonth)
                        {
                            case "1":
                                Sendmonth = "Farvardin";
                                break;
                            case "2":
                                Sendmonth = "Ordibehesht";
                                break;
                            case "3":
                                Sendmonth = "Khordad";
                                break;
                            case "4":
                                Sendmonth = "Tir";
                                break;
                            case "5":
                                Sendmonth = "Mordad";
                                break;
                            case "6":
                                Sendmonth = "Shahrivar";
                                break;
                            case "7":
                                Sendmonth = "Mehr";
                                break;
                            case "8":
                                Sendmonth = "Aban";
                                break;
                            case "9":
                                Sendmonth = "Azar";
                                break;
                            case "10":
                                Sendmonth = "Day";
                                break;
                            case "11":
                                Sendmonth = "Bahman";
                                break;
                            case "12":
                                Sendmonth = "Esfand";
                                break;

                        }


                        path = "ftp://" + ftpserverip + "/" + BaseData.GetInstance().M005Path + "/";
                        path += endid + @"/" + endyear + @"/" + Sendmonth + @"/" + Sendday + @"/";

                        break;

                }


            }


            string TempDate = date;
            int Tempday = int.Parse(TempDate.Substring(8));
            int TempMonth = int.Parse(TempDate.Substring(5, 2));
            int Tempyear = int.Parse(TempDate.Substring(0, 4));
            string Stempday = "", Stempmonth = "";

            Stempday = Tempday.ToString();

            Stempmonth = TempMonth.ToString();



            if (!isftp)
            {
                //path = path + @"\" + Tempyear.ToString() + @"\" + Stempmonth + @"\" + Stempday + @"\" + DocName;
                // path = path + @"\" + Tempyear.ToString() + @"\" + Stempmonth + @"\" + DocName;

            }
            else if (isftp)
            {
                //path = path +DocName;


                bool success = FtpDownload(DocName + ".xls", path + ".xls", ftpusername, ftppassword);
                if (success)
                {
                    path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                     "\\NRI\\SBS" + "\\" + DocName;
                }
                else
                {

                    path = "";

                }

            }

            return path;
        }
        //------------------------------------AveragePrice(string date)--------------------------------
        public void AveragePrice()
        {
            try
            {
                SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;

                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;

                //string path = @"c:\data\AveragePrice.xls";

                string temppath = BaseData.GetInstance().M009Path;
                string path = temppath + @"\" + "AveragePrice.xls";


                //string webAddress = "http://www.igmc.ir/pmt/usrFiles/nprice.xls";
                string webAddress = "http://www.igmc.org.ir/pmt/usrFiles/nprice.xls";

                //Downloading AveragePrice.xls from website
                try
                {
                     WebProxy proxy = null;
                     if (proxyAddress != "")
                     {
                         proxy = new WebProxy(proxyAddress);
                         if (Username != "")
                         {
                             proxy.Credentials = new NetworkCredential(Username.Trim(), password.Trim(), domain.Trim());

                         }
                         else
                         {
                             proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                         }
                        
                     }
                    WebClient client = new WebClient();
                    if (proxyAddress != "")
                    {
                       client.Proxy = proxy;
                    }
                    client.DownloadFile(webAddress, path);
                }
                catch (Exception exp)
                {
                    string str = exp.Message;
                    NRI.SBS.Common.CLogCodeErrors.LogError(exp, webAddress, path, proxyAddress);
                    return;
                }


                if (path != "" && File.Exists(path))
                {
                    //Save AS AveragePrice.xls file
                    Excel.Application exobj1 = new Excel.Application();
                    exobj1.Visible = false;
                    exobj1.UserControl = true;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    Excel.Workbook book1 = null;
                    book1 = exobj1.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book1.Save();
                    book1.Close(true, book1, Type.Missing);
                    exobj1.Workbooks.Close();
                    exobj1.Quit();
                    //read from AveragePrice.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "قيمت ";
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    //dataGridView1.DataSource = objDataset1.Tables[0].DefaultView;
                    objConn.Close();

                    SqlCommand command = new SqlCommand();
                    command.Connection = MyConnection;
                    command.CommandText = "DELETE fROM [AveragePrice] WHERE Date='" +
                        objDataset1.Tables[0].Columns[0].ColumnName + "'";
                    command.ExecuteNonQuery();

                    //Insert into DB (AveragePrice)
                    MyCom.Parameters.Add("@date1", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                    MyCom.Parameters.Add("@Pmin", SqlDbType.Int);
                    MyCom.Parameters.Add("@Pmax", SqlDbType.Int);
                    MyCom.Parameters.Add("@Amin", SqlDbType.Int);
                    MyCom.Parameters.Add("@Amax", SqlDbType.Int);
                    MyCom.Parameters.Add("@Aav", SqlDbType.Int);
                    for (int i = 0; i < 24; i++)
                    {

                        MyCom.CommandText = "INSERT INTO [AveragePrice] (Date,Hour,ProposedMin,ProposedMax,AcceptedMin,AcceptedMax,AcceptedAverage) VALUES (@date1,@hour,@Pmin,@Pmax,@Amin,@Amax,@Aav)";
                        //MyCom.Parameters["@date1"].Value = dataGridView1.Columns[0].HeaderText.ToString();
                        //MyCom.Parameters["@hour"].Value = dataGridView1.Rows[i + 3].Cells[0].Value.ToString();
                        //MyCom.Parameters["@Pmin"].Value = dataGridView1.Rows[i + 3].Cells[1].Value.ToString();
                        //MyCom.Parameters["@Pmax"].Value = dataGridView1.Rows[i + 3].Cells[2].Value.ToString();
                        //MyCom.Parameters["@Amin"].Value = dataGridView1.Rows[i + 3].Cells[3].Value.ToString();
                        //MyCom.Parameters["@Amax"].Value = dataGridView1.Rows[i + 3].Cells[4].Value.ToString();
                        //MyCom.Parameters["@Aav"].Value = dataGridView1.Rows[i + 3].Cells[5].Value.ToString();

                        MyCom.Parameters["@date1"].Value = objDataset1.Tables[0].Columns[0].ColumnName;
                        MyCom.Parameters["@hour"].Value = objDataset1.Tables[0].Rows[i + 3][0].ToString();
                        MyCom.Parameters["@Pmin"].Value = objDataset1.Tables[0].Rows[i + 3][1].ToString();
                        MyCom.Parameters["@Pmax"].Value = objDataset1.Tables[0].Rows[i + 3][2].ToString();
                        MyCom.Parameters["@Amin"].Value = objDataset1.Tables[0].Rows[i + 3][3].ToString();
                        MyCom.Parameters["@Amax"].Value = objDataset1.Tables[0].Rows[i + 3][4].ToString();
                        MyCom.Parameters["@Aav"].Value = objDataset1.Tables[0].Rows[i + 3][5].ToString();
                        try
                        {
                            MyCom.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                        }

                    }
                    System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                }
                else
                {
                    AddNotExistMessage(webAddress);
                }

                ////////////////////////////////////////////////////////////////////////
            }
            catch
            {
                AddNotExistMessage("AveragePrice");
            }
        }

        //-------------------------------------LoadForecasting(string date)------------------------------
        public void LoadForecasting()
        {
            try
            {
                SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;

                string path1 = @"c:\data\LoadForecasting.xls";
                //string webAddress = "http://www.igmc.ir/pmt/usrFiles/load.xls";
                string webAddress = "http://www.igmc.org.ir/pmt/usrFiles/load.xls";

                //Downloading LoadForecasting.xls from website
                try
                {
                    WebProxy proxy = null;
                    if (proxyAddress != "")
                    {
                        proxy = new WebProxy(proxyAddress);
                        if (Username != "")
                        {
                            proxy.Credentials = new NetworkCredential(Username.Trim(), password.Trim(), domain.Trim());

                        }
                        else
                        {
                            proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        }
                      
                    }
                    WebClient client = new WebClient();
                    if (proxyAddress != "")
                    {
                       client.Proxy = proxy;

                    }
                    client.DownloadFile(webAddress, path1);
                }
                catch (Exception exp)
                {
                    string str = exp.Message;
                    NRI.SBS.Common.CLogCodeErrors.LogError(exp, webAddress, path1, proxyAddress);

                    return;
                }

                if (path1 != "" && File.Exists(path1))
                {
                    //Save AS LoadForecasting.xls file
                    Excel.Application exobj = new Excel.Application();
                    exobj.Visible = false;
                    exobj.UserControl = true;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    Excel.Workbook book = null;
                    book = exobj.Workbooks.Open(path1, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book.Save();
                    book.Close(true, book, Type.Missing);

                    //read from LoadForecasting.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path1 + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "Lfoc";
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    //objDataset1.Tables[0].DataSource = objDataset1.Tables[0];
                    objConn.Close();

                    //read from LoadForecasting.xls into strings
                    string edate = "";
                    string[] date = new string[4];
                    book = exobj.Workbooks.Open(path1, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if (workSheet.Name == "Lfoc")
                        {
                            edate = ((Excel.Range)workSheet.Cells[3, 4]).Value2.ToString();
                            date[0] = ((Excel.Range)workSheet.Cells[4, 3]).Value2.ToString();
                            date[1] = ((Excel.Range)workSheet.Cells[4, 4]).Value2.ToString();
                            date[2] = ((Excel.Range)workSheet.Cells[4, 5]).Value2.ToString();
                            date[3] = ((Excel.Range)workSheet.Cells[4, 6]).Value2.ToString();
                        }
                    book.Close(false, book, Type.Missing);
                    exobj.Workbooks.Close();
                    exobj.Quit();
                    //Insert into DB (LoadForecasting)
                    MyCom.Parameters.Add("@date11", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@edate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@peak", SqlDbType.Real);
                    MyCom.Parameters.Add("@h1", SqlDbType.Real);
                    MyCom.Parameters.Add("@h2", SqlDbType.Real);
                    MyCom.Parameters.Add("@h3", SqlDbType.Real);
                    MyCom.Parameters.Add("@h4", SqlDbType.Real);
                    MyCom.Parameters.Add("@h5", SqlDbType.Real);
                    MyCom.Parameters.Add("@h6", SqlDbType.Real);
                    MyCom.Parameters.Add("@h7", SqlDbType.Real);
                    MyCom.Parameters.Add("@h8", SqlDbType.Real);
                    MyCom.Parameters.Add("@h9", SqlDbType.Real);
                    MyCom.Parameters.Add("@h10", SqlDbType.Real);
                    MyCom.Parameters.Add("@h11", SqlDbType.Real);
                    MyCom.Parameters.Add("@h12", SqlDbType.Real);
                    MyCom.Parameters.Add("@h13", SqlDbType.Real);
                    MyCom.Parameters.Add("@h14", SqlDbType.Real);
                    MyCom.Parameters.Add("@h15", SqlDbType.Real);
                    MyCom.Parameters.Add("@h16", SqlDbType.Real);
                    MyCom.Parameters.Add("@h17", SqlDbType.Real);
                    MyCom.Parameters.Add("@h18", SqlDbType.Real);
                    MyCom.Parameters.Add("@h19", SqlDbType.Real);
                    MyCom.Parameters.Add("@h20", SqlDbType.Real);
                    MyCom.Parameters.Add("@h21", SqlDbType.Real);
                    MyCom.Parameters.Add("@h22", SqlDbType.Real);
                    MyCom.Parameters.Add("@h23", SqlDbType.Real);
                    MyCom.Parameters.Add("@h24", SqlDbType.Real);


                    SqlCommand command = new SqlCommand();
                    command.Connection = MyConnection;
                    command.CommandText = "DELETE FROM [LoadForecasting] WHERE DateEstimate ='" + edate + "'";
                    command.ExecuteNonQuery();

                    for (int z = 0; z < 4; z++)
                    {
                        MyCom.CommandText = "INSERT INTO [LoadForecasting] (Date,DateEstimate,Peak,Hour1,Hour2,Hour3,Hour4," +
                        "Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19" +
                        ",Hour20,Hour21,Hour22,Hour23,Hour24) VALUES (@date11,@edate,@peak,@h1,@h2,@h3,@h4,@h5,@h6,@h7,@h8" +
                        ",@h9,@h10,@h11,@h12,@h13,@h14,@h15,@h16,@h17,@h18,@h19,@h20,@h21,@h22,@h23,@h24)";
                        MyCom.Parameters["@date11"].Value = date[z];
                        MyCom.Parameters["@edate"].Value = edate;

                        MyCom.Parameters["@peak"].Value = objDataset1.Tables[0].Rows[28][2 + z].ToString();
                        MyCom.Parameters["@h1"].Value = objDataset1.Tables[0].Rows[4][2 + z].ToString();
                        MyCom.Parameters["@h2"].Value = objDataset1.Tables[0].Rows[5][2 + z].ToString();
                        MyCom.Parameters["@h3"].Value = objDataset1.Tables[0].Rows[6][2 + z].ToString();
                        MyCom.Parameters["@h4"].Value = objDataset1.Tables[0].Rows[7][2 + z].ToString();
                        MyCom.Parameters["@h5"].Value = objDataset1.Tables[0].Rows[8][2 + z].ToString();
                        MyCom.Parameters["@h6"].Value = objDataset1.Tables[0].Rows[9][2 + z].ToString();
                        MyCom.Parameters["@h7"].Value = objDataset1.Tables[0].Rows[10][2 + z].ToString();
                        MyCom.Parameters["@h8"].Value = objDataset1.Tables[0].Rows[11][2 + z].ToString();
                        MyCom.Parameters["@h9"].Value = objDataset1.Tables[0].Rows[12][2 + z].ToString();
                        MyCom.Parameters["@h10"].Value = objDataset1.Tables[0].Rows[13][2 + z].ToString();
                        MyCom.Parameters["@h11"].Value = objDataset1.Tables[0].Rows[14][2 + z].ToString();
                        MyCom.Parameters["@h12"].Value = objDataset1.Tables[0].Rows[15][2 + z].ToString();
                        MyCom.Parameters["@h13"].Value = objDataset1.Tables[0].Rows[16][2 + z].ToString();
                        MyCom.Parameters["@h14"].Value = objDataset1.Tables[0].Rows[17][2 + z].ToString();
                        MyCom.Parameters["@h15"].Value = objDataset1.Tables[0].Rows[18][2 + z].ToString();
                        MyCom.Parameters["@h16"].Value = objDataset1.Tables[0].Rows[19][2 + z].ToString();
                        MyCom.Parameters["@h17"].Value = objDataset1.Tables[0].Rows[20][2 + z].ToString();
                        MyCom.Parameters["@h18"].Value = objDataset1.Tables[0].Rows[21][2 + z].ToString();
                        MyCom.Parameters["@h19"].Value = objDataset1.Tables[0].Rows[22][2 + z].ToString();
                        MyCom.Parameters["@h20"].Value = objDataset1.Tables[0].Rows[23][2 + z].ToString();
                        MyCom.Parameters["@h21"].Value = objDataset1.Tables[0].Rows[24][2 + z].ToString();
                        MyCom.Parameters["@h22"].Value = objDataset1.Tables[0].Rows[25][2 + z].ToString();
                        MyCom.Parameters["@h23"].Value = objDataset1.Tables[0].Rows[26][2 + z].ToString();
                        MyCom.Parameters["@h24"].Value = objDataset1.Tables[0].Rows[27][2 + z].ToString();
                        try
                        {
                            MyCom.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                        }
                    }
                    System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                }
                else
                {
                    AddNotExistMessage(webAddress);
                }
                /////////////////////////////////////////////////////////////////////////
            }
            catch
            {
                AddNotExistMessage("LoadForecasting");
            }
        }
        //-----------------------------Rep12Page(string date)----------------------------------
        public void Rep12Page(string date,bool hardinternet)
        {
            try
            {
                string sheetname = "گزارش 12 برگي";
                //string hardPath = @"c:\data\DetailedReports.xls";
                string temppath = BaseData.GetInstance().M009Path;
                string hardPath = temppath + @"\" + "DetailedReports.xls";

                string Rdate = date;
                Rdate = Rdate.Replace("/", "");
                //string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/Rep12Pages.htm";
                string address = "http://sccisrep.igmc.ir/Html/" + Rdate + "/Rep12Pages_R.xls";
               
                DataTable dt8 = Utilities.GetTable("select * from internetaddress");
                if (dt8.Rows.Count > 0)
                {

                   sheetname= dt8.Rows[0]["sheetRep12Page"].ToString();

                    address = dt8.Rows[0]["Rep12Page"].ToString().Trim();
                    if (address.Contains("Date Without Seperator(8char)")) address = address.Replace("Date Without Seperator(8char)", Rdate);
                }

                if (hardinternet == false)
                {
                    //Downloading DetailedReports.xls from website
                    try
                    {
                        if (File.Exists(hardPath))
                            File.Delete(hardPath);

                        WebProxy proxy = null;
                        if (proxyAddress != "")
                        {
                            proxy = new WebProxy(proxyAddress);
                            if (Username != "")
                            {
                                proxy.Credentials = new NetworkCredential(Username.Trim(), password.Trim(), domain.Trim());

                            }
                            else
                            {
                                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                            }

                        }
                        WebClient client = new WebClient();
                        if (proxyAddress != "")
                        {
                            client.Proxy = proxy;
                        }
                        client.DownloadFile(address, hardPath);
                    }
                    catch (Exception exp)
                    {
                        string str = exp.Message;
                        if (messages == "")
                        {
                            messages += "Error Rep12Page In Date  :" + date + "\r\n";
                        }
                        NRI.SBS.Common.CLogCodeErrors.LogError(exp, address, hardPath, proxyAddress);

                    }
                }
                else
                {

                    string TempDate = date;
                    string Tempday = TempDate.Substring(8);
                    string TempMonth = TempDate.Substring(5, 2);
                    string Tempyear = TempDate.Substring(0, 4);                  


                    hardPath = temppath + @"\" + Tempyear.ToString() + @"\" + int.Parse(TempMonth) + @"\" + int.Parse(Tempday) + @"\" + "Rep12Pages_R.xls";
                   // hardPath = temppath + @"\" + "Rep12Pages_R.xls";
                }

                if (!File.Exists(hardPath))
                {
                    AddNotExistMessage(address);
                    return;
                }


                //Save AS DetailedReports.xls file
                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                Excel.Workbook book = null;
                book = exobj.Workbooks.Open(hardPath, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                book.Save();
                book.Close(true, book, Type.Missing);

                //Insert into DB (Rep12Page)
                //string path1 = @"c:\data\DetailedReports.xls";
                //Excel.Application exobj = new Excel.Application();
                //Excel.Workbook book = null;
                book = exobj.Workbooks.Open(hardPath, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;
                SqlCommand MyCom = new SqlCommand();

                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@utype", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@ucode", SqlDbType.SmallInt);
                MyCom.Parameters.Add("@pp", SqlDbType.Real);
                MyCom.Parameters.Add("@ep", SqlDbType.Real);
                MyCom.Parameters.Add("@producable", SqlDbType.Real);
                MyCom.Parameters.Add("@produced", SqlDbType.Real);
                //MyCom.Parameters.Add("@dynamicstore", SqlDbType.Real);
                //MyCom.Parameters.Add("@store", SqlDbType.Real);
                //MyCom.Parameters.Add("@limit", SqlDbType.Real);
                //MyCom.Parameters.Add("@program", SqlDbType.NChar, 20);
                //MyCom.Parameters.Add("@code", SqlDbType.NChar, 10);

                foreach (Excel.Worksheet workSheet in book.Worksheets)
                    if (workSheet.Name ==sheetname )
                    {
                        string mydate = ((Excel.Range)workSheet.Cells[3, 6]).Value2.ToString();
                        mydate = mydate.Remove(0, (mydate.Length - 10));

                        if (date != mydate)
                        {
                            AddNotExistMessage(" Not Existed For Selected Date!   ");
                            return;
                        }

                        SqlCommand command = new SqlCommand();
                        command.Connection = MyConnection;
                        command.CommandText = "DELETE FROM [Rep12Page] WHERE Date ='" + mydate + "'";
                        command.ExecuteNonQuery();

                        int row = 3;
                        while (row < 800)
                        {
                            if ((((Excel.Range)workSheet.Cells[row, 1]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString().Contains("كد")) && (((Excel.Range)workSheet.Cells[row, 14]).Value2 != null))
                            {
                                string code = ((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString();
                                do
                                {
                                    MyCom.CommandText = "INSERT INTO [Rep12Page] (Date,PPCode,UnitType," +
                                    "UnitCode,PracticalPower,ExpectedPower,Producable,Produced) VALUES (@date,@id,@utype,@ucode,@pp,@ep," +
                                    "@producable,@produced)";

                                    MyCom.Connection = MyConnection;

                                    MyCom.Parameters["@date"].Value = mydate;
                                    MyCom.Parameters["@id"].Value = code;
                                    if (((Excel.Range)workSheet.Cells[row, 3]).Value2 != null)
                                        MyCom.Parameters["@utype"].Value = ((Excel.Range)workSheet.Cells[row, 3]).Value2.ToString();
                                    else MyCom.Parameters["@utype"].Value = "";
                                    if (((Excel.Range)workSheet.Cells[row, 4]).Value2 != null)
                                        MyCom.Parameters["@ucode"].Value = ((Excel.Range)workSheet.Cells[row, 4]).Value2.ToString();
                                    else MyCom.Parameters["@ucode"].Value = "";
                                    if (((Excel.Range)workSheet.Cells[row, 5]).Value2 != null)
                                        MyCom.Parameters["@pp"].Value = ((Excel.Range)workSheet.Cells[row, 5]).Value2.ToString();
                                    else MyCom.Parameters["@pp"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 6]).Value2 != null)
                                        MyCom.Parameters["@ep"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();
                                    else MyCom.Parameters["@ep"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 7]).Value2 != null)
                                        MyCom.Parameters["@producable"].Value = ((Excel.Range)workSheet.Cells[row, 7]).Value2.ToString();
                                    else MyCom.Parameters["@producable"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 8]).Value2 != null)
                                        MyCom.Parameters["@produced"].Value = ((Excel.Range)workSheet.Cells[row, 8]).Value2.ToString();
                                    else MyCom.Parameters["@produced"].Value = 0;
                                    //if (((Excel.Range)workSheet.Cells[row, 9]).Value2 != null)
                                    //    MyCom.Parameters["@dynamicstore"].Value = ((Excel.Range)workSheet.Cells[row, 9]).Value2.ToString();
                                    //else MyCom.Parameters["@dynamicstore"].Value = 0;
                                    //if (((Excel.Range)workSheet.Cells[row, 10]).Value2 != null)
                                    //    MyCom.Parameters["@store"].Value = ((Excel.Range)workSheet.Cells[row, 10]).Value2.ToString();
                                    //else MyCom.Parameters["@store"].Value = 0;
                                    //if (((Excel.Range)workSheet.Cells[row, 11]).Value2 != null)
                                    //    MyCom.Parameters["@limit"].Value = ((Excel.Range)workSheet.Cells[row, 11]).Value2.ToString();
                                    //else MyCom.Parameters["@limit"].Value = 0;
                                    //if (((Excel.Range)workSheet.Cells[row, 13]).Value2 != null)
                                    //    MyCom.Parameters["@program"].Value = ((Excel.Range)workSheet.Cells[row, 13]).Value2.ToString();
                                    //else MyCom.Parameters["@program"].Value = "";
                                    //if (((Excel.Range)workSheet.Cells[row, 14]).Value2 != null)
                                    //    MyCom.Parameters["@code"].Value = ((Excel.Range)workSheet.Cells[row, 14]).Value2.ToString();
                                    //else MyCom.Parameters["@code"].Value = ""; 
                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        if (messages == "")
                                        {
                                            messages += "Error Rep12Page In Date  :" + date + "\r\n";
                                        }
                                        string str = exp.Message;
                                    }

                                    row++;
                                } while ((((Excel.Range)workSheet.Cells[row, 2]).Value2 == null) && (((Excel.Range)workSheet.Cells[row, 1]).Value2 == null));
                            }
                            row++;
                        }
                    }
                book.Close(true, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();
                System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
            }
            catch
            {

            }
        }
        //---------------------------Outage(string date)------------------------
        public void Outage(string date)
        {
            //string hardPath = @"c:\data\outage.xls";
            string temppath = BaseData.GetInstance().M009Path;
            string hardPath = temppath + @"\" + "outage.xls";

            string Rdate = date;
            Rdate = Rdate.Replace("/", "");
           // string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/Outage_files/sheet001.htm";
            string address = "http://sccisrep.igmc.ir/Html/" + Rdate + "/Outage_files/sheet001.htm";
            //Downloading outage.xls from website
            try
            {
                if (File.Exists(hardPath))
                    File.Delete(hardPath);

                //WebProxy proxy = new WebProxy();
                //if (Username != "")
                //{
                //    proxy.Credentials = new NetworkCredential(Username.Trim(), password.Trim(), domain.Trim());

                //}
                //else
                //{
                //    proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                //}
                //proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                WebClient client = new WebClient();
                //client.Proxy = proxy;
                client.DownloadFile(address, hardPath);
            }
            catch (Exception exp)
            {
                string str = exp.Message;
                NRI.SBS.Common.CLogCodeErrors.LogError(exp, address, hardPath, proxyAddress);

            }

            if (!File.Exists(hardPath))
            {
                AddNotExistMessage(address);
                return;
            }

            //remove <link and saveas file
            StringBuilder newFileoutage = new StringBuilder();
            string tempoutage = "";
            string[] fileoutage = File.ReadAllLines(hardPath, Encoding.Default);
            foreach (string line in fileoutage)
            {
                if (line.Contains("<link"))
                {
                    tempoutage = line.Remove(0);
                    newFileoutage.Append(tempoutage + "\r\n");
                    continue;
                }
                newFileoutage.Append(line + "\r\n");
            }
            File.WriteAllText(hardPath, newFileoutage.ToString(), Encoding.Default);
            //Insert into DB (Outage)
            Excel.Application exobj = new Excel.Application();
            exobj.Visible = false;
            exobj.UserControl = true;
            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(hardPath, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

            SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;
            SqlCommand MyCom = new SqlCommand();

            MyCom.Parameters.Add("@out_date", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@out_ppcode", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@out_utype", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@out_ucode", SqlDbType.SmallInt);
            MyCom.Parameters.Add("@out_pp", SqlDbType.Real);
            MyCom.Parameters.Add("@out_ep", SqlDbType.Real);
            MyCom.Parameters.Add("@out_program", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@out_code", SqlDbType.NChar, 10);

            foreach (Excel.Worksheet workSheet in book.Worksheets)
                if (workSheet.Name == "outage")
                {
                    string mydate = ((Excel.Range)workSheet.Cells[3, 4]).Value2.ToString();
                    mydate = mydate.Remove(0, (mydate.Length - 10));

                    SqlCommand command = new SqlCommand();
                    command.Connection = MyConnection;
                    command.CommandText = "DELETE FROM [OutageUnits] WHERE Date='" + mydate + "'";
                    command.ExecuteNonQuery();

                    int row = 3;
                    while (row < 240)
                    {
                        if ((((Excel.Range)workSheet.Cells[row, 2]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString().Contains("کد")))
                        {
                            MyCom.CommandText = "INSERT INTO [OutageUnits] (Date,PPCode,UnitType," +
                            "UnitCode,PracticalPower,ExpectedPower,Program,Code) VALUES" +
                            "(@out_date,@out_ppcode,@out_utype,@out_ucode,@out_pp," +
                            "@out_ep,@out_program,@out_code)";

                            MyCom.Connection = MyConnection;
                            MyCom.Parameters["@out_date"].Value = mydate;
                            if (((Excel.Range)workSheet.Cells[row, 2]).Value2 != null)
                                MyCom.Parameters["@out_ppcode"].Value = ((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString();
                            else MyCom.Parameters["@out_ppcode"].Value = "";
                            if (((Excel.Range)workSheet.Cells[row, 4]).Value2 != null)
                                MyCom.Parameters["@out_utype"].Value = ((Excel.Range)workSheet.Cells[row, 4]).Value2.ToString();
                            else MyCom.Parameters["@out_utype"].Value = "";
                            if (((Excel.Range)workSheet.Cells[row, 5]).Value2 != null)
                                MyCom.Parameters["@out_ucode"].Value = ((Excel.Range)workSheet.Cells[row, 5]).Value2.ToString();
                            else MyCom.Parameters["@out_ucode"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 6]).Value2 != null)
                                MyCom.Parameters["@out_pp"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();
                            else MyCom.Parameters["@out_pp"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 7]).Value2 != null)
                                MyCom.Parameters["@out_ep"].Value = ((Excel.Range)workSheet.Cells[row, 7]).Value2.ToString();
                            else MyCom.Parameters["@out_ep"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 9]).Value2 != null)
                                MyCom.Parameters["@out_program"].Value = ((Excel.Range)workSheet.Cells[row, 9]).Value2.ToString();
                            else MyCom.Parameters["@out_program"].Value = "";
                            if (((Excel.Range)workSheet.Cells[row, 10]).Value2 != null)
                                MyCom.Parameters["@out_code"].Value = ((Excel.Range)workSheet.Cells[row, 10]).Value2.ToString();
                            else MyCom.Parameters["@out_code"].Value = "";

                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                            }

                        }
                        row++;
                    }
                }
            book.Close(true, book, Type.Missing);
            exobj.Workbooks.Close();
            exobj.Quit();
            System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
        }
        //---------------------------------Manategh(string date)---------------------------
        public void Manategh(string date,bool hardinternet)
        {
            try
            {
                //string hardPath = @"c:\data\manategh.xls";
                string sheetname="مناطق";
                string temppath = BaseData.GetInstance().M009Path;
                string hardPath = temppath + @"\" + "manategh.xls";


                string Rdate = date;
                Rdate = Rdate.Replace("/", "");
                //string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/Manat_files/sheet001.htm";
                //string address = "http://sccisrep.igmc.ir/Html/" + Rdate + "/Manat_files/sheet001.htm";
                string address = "http://sccisrep.igmc.ir/Html/" + Rdate + "/Manat_R.xls";

                DataTable dt8 = Utilities.GetTable("select * from internetaddress");
                if (dt8.Rows.Count > 0)
                {
                    sheetname= dt8.Rows[0]["sheetManategh"].ToString();
                    address = dt8.Rows[0]["Manategh"].ToString().Trim();
                    if (address.Contains("Date Without Seperator(8char)")) address = address.Replace("Date Without Seperator(8char)", Rdate);
                }


                if (hardinternet == false)
                {
                    //Downloading manategh.xls from website
                    try
                    {
                        if (File.Exists(hardPath))
                            File.Delete(hardPath);

                        WebProxy proxy = null;
                        if (proxyAddress != "")
                        {
                            proxy = new WebProxy(proxyAddress);
                            if (Username != "")
                            {
                                proxy.Credentials = new NetworkCredential(Username.Trim(), password.Trim(), domain.Trim());

                            }
                            else
                            {
                                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                            }

                        }
                        WebClient client = new WebClient();
                        if (proxyAddress != "")
                        {
                            client.Proxy = proxy;
                        }
                        client.DownloadFile(address, hardPath);
                    }
                    catch (Exception exp)
                    {
                        string str = exp.Message;
                        MessageBox.Show(str);

                        NRI.SBS.Common.CLogCodeErrors.LogError(exp, address, hardPath, proxyAddress);

                    }
                }
                else
                {

                    string TempDate = date;
                    string Tempday = TempDate.Substring(8);
                    string TempMonth = TempDate.Substring(5, 2);
                    string Tempyear = TempDate.Substring(0, 4);


                    hardPath = temppath + @"\" + Tempyear.ToString() + @"\" + int.Parse(TempMonth) + @"\" + int.Parse(Tempday) + @"\" + "Manat_R.xls";
                    //hardPath = temppath + @"\" + "Manat_R.xls";
                }

                if (!File.Exists(hardPath))
                {
                    AddNotExistMessage(address);
                    return;
                }

                ////remove <link and saveas file
                //StringBuilder newFilemanategh = new StringBuilder();
                //string tempmanategh = "";
                //string[] filemanategh = File.ReadAllLines(hardPath, Encoding.Default);
                //foreach (string line in filemanategh)
                //{
                //    if (line.Contains("<link"))
                //    {
                //        tempmanategh = line.Remove(0);
                //        newFilemanategh.Append(tempmanategh + "\r\n");
                //        continue;
                //    }
                //    newFilemanategh.Append(line + "\r\n");
                //}
                //File.WriteAllText(hardPath, newFilemanategh.ToString(), Encoding.Default);

                //Insert into DB (Manategh)
                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                Excel.Workbook book = null;
                book = exobj.Workbooks.Open(hardPath, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;
                SqlCommand MyCom = new SqlCommand();

                MyCom.Parameters.Add("@man_date", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@man_code", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@man_title", SqlDbType.NChar, 50);
                MyCom.Parameters.Add("@man_name", SqlDbType.NChar, 20);
                MyCom.Parameters.Add("@man_peak", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h1", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h2", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h3", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h4", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h5", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h6", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h7", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h8", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h9", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h10", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h11", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h12", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h13", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h14", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h15", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h16", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h17", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h18", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h19", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h20", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h21", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h22", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h23", SqlDbType.Real);
                MyCom.Parameters.Add("@man_h24", SqlDbType.Real);

                foreach (Excel.Worksheet workSheet in book.Worksheets)
                    if (workSheet.Name == sheetname)
                    {
                        string mydate = ((Excel.Range)workSheet.Cells[3, 3]).Value2.ToString();

                        if (date != mydate)
                        {
                            AddNotExistMessage(" Not Existed For Selected Date!   ");
                            return;
                        }

                        SqlCommand command = new SqlCommand();
                        command.Connection = MyConnection;
                        command.CommandText = "DELETE FROM [Manategh] WHERE Date='" + mydate + "'";
                        command.ExecuteNonQuery();

                        int row = 3;
                        bool esc = false;

                        while (row < 117)
                        {
                            if ((((Excel.Range)workSheet.Cells[row, 2]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString().Contains("كد")))
                            {
                                string code = ((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString();
                                string name = ((Excel.Range)workSheet.Cells[row, 3]).Value2.ToString();
                                do
                                {
                                    MyCom.CommandText = "INSERT INTO [Manategh] (Date,Code," +
                                    "Title,Name,Peak,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7" +
                                    ",Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15," +
                                    "Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23" +
                                    ",Hour24) VALUES (@man_date,@man_code,@man_title,@man_name," +
                                    "@man_peak,@man_h1,@man_h2,@man_h3,@man_h4,@man_h5,@man_h6" +
                                    ",@man_h7,@man_h8,@man_h9,@man_h10,@man_h11,@man_h12,@man_h13" +
                                    ",@man_h14,@man_h15,@man_h16,@man_h17,@man_h18,@man_h19,@man_h20" +
                                    ",@man_h21,@man_h22,@man_h23,@man_h24)";

                                    MyCom.Connection = MyConnection;

                                    MyCom.Parameters["@man_date"].Value = mydate;
                                    MyCom.Parameters["@man_code"].Value = code;
                                    MyCom.Parameters["@man_name"].Value = name;
                                    if (((Excel.Range)workSheet.Cells[row, 4]).Value2 != null)
                                        MyCom.Parameters["@man_title"].Value = ((Excel.Range)workSheet.Cells[row, 4]).Value2.ToString();
                                    else MyCom.Parameters["@man_title"].Value = "";
                                    if (((Excel.Range)workSheet.Cells[row, 30]).Value2 != null)
                                        MyCom.Parameters["@man_peak"].Value = ((Excel.Range)workSheet.Cells[row, 30]).Value2.ToString();
                                    else MyCom.Parameters["@man_peak"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 5]).Value2 != null)
                                        MyCom.Parameters["@man_h1"].Value = ((Excel.Range)workSheet.Cells[row, 5]).Value2.ToString();
                                    else MyCom.Parameters["@man_h1"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 6]).Value2 != null)
                                        MyCom.Parameters["@man_h2"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();
                                    else MyCom.Parameters["@man_h2"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 7]).Value2 != null)
                                        MyCom.Parameters["@man_h3"].Value = ((Excel.Range)workSheet.Cells[row, 7]).Value2.ToString();
                                    else MyCom.Parameters["@man_h3"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 8]).Value2 != null)
                                        MyCom.Parameters["@man_h4"].Value = ((Excel.Range)workSheet.Cells[row, 8]).Value2.ToString();
                                    else MyCom.Parameters["@man_h4"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 9]).Value2 != null)
                                        MyCom.Parameters["@man_h5"].Value = ((Excel.Range)workSheet.Cells[row, 9]).Value2.ToString();
                                    else MyCom.Parameters["@man_h5"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 10]).Value2 != null)
                                        MyCom.Parameters["@man_h6"].Value = ((Excel.Range)workSheet.Cells[row, 10]).Value2.ToString();
                                    else MyCom.Parameters["@man_h6"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 11]).Value2 != null)
                                        MyCom.Parameters["@man_h7"].Value = ((Excel.Range)workSheet.Cells[row, 11]).Value2.ToString();
                                    else MyCom.Parameters["@man_h7"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 12]).Value2 != null)
                                        MyCom.Parameters["@man_h8"].Value = ((Excel.Range)workSheet.Cells[row, 12]).Value2.ToString();
                                    else MyCom.Parameters["@man_h8"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 13]).Value2 != null)
                                        MyCom.Parameters["@man_h9"].Value = ((Excel.Range)workSheet.Cells[row, 13]).Value2.ToString();
                                    else MyCom.Parameters["@man_h9"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 14]).Value2 != null)
                                        MyCom.Parameters["@man_h10"].Value = ((Excel.Range)workSheet.Cells[row, 14]).Value2.ToString();
                                    else MyCom.Parameters["@man_h10"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 15]).Value2 != null)
                                        MyCom.Parameters["@man_h11"].Value = ((Excel.Range)workSheet.Cells[row, 15]).Value2.ToString();
                                    else MyCom.Parameters["@man_h11"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 16]).Value2 != null)
                                        MyCom.Parameters["@man_h12"].Value = ((Excel.Range)workSheet.Cells[row, 16]).Value2.ToString();
                                    else MyCom.Parameters["@man_h12"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 17]).Value2 != null)
                                        MyCom.Parameters["@man_h13"].Value = ((Excel.Range)workSheet.Cells[row, 17]).Value2.ToString();
                                    else MyCom.Parameters["@man_h13"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 18]).Value2 != null)
                                        MyCom.Parameters["@man_h14"].Value = ((Excel.Range)workSheet.Cells[row, 18]).Value2.ToString();
                                    else MyCom.Parameters["@man_h14"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 19]).Value2 != null)
                                        MyCom.Parameters["@man_h15"].Value = ((Excel.Range)workSheet.Cells[row, 19]).Value2.ToString();
                                    else MyCom.Parameters["@man_h15"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 20]).Value2 != null)
                                        MyCom.Parameters["@man_h16"].Value = ((Excel.Range)workSheet.Cells[row, 20]).Value2.ToString();
                                    else MyCom.Parameters["@man_h16"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 21]).Value2 != null)
                                        MyCom.Parameters["@man_h17"].Value = ((Excel.Range)workSheet.Cells[row, 21]).Value2.ToString();
                                    else MyCom.Parameters["@man_h17"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 22]).Value2 != null)
                                        MyCom.Parameters["@man_h18"].Value = ((Excel.Range)workSheet.Cells[row, 22]).Value2.ToString();
                                    else MyCom.Parameters["@man_h18"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 23]).Value2 != null)
                                        MyCom.Parameters["@man_h19"].Value = ((Excel.Range)workSheet.Cells[row, 23]).Value2.ToString();
                                    else MyCom.Parameters["@man_h19"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 24]).Value2 != null)
                                        MyCom.Parameters["@man_h20"].Value = ((Excel.Range)workSheet.Cells[row, 24]).Value2.ToString();
                                    else MyCom.Parameters["@man_h20"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 25]).Value2 != null)
                                        MyCom.Parameters["@man_h21"].Value = ((Excel.Range)workSheet.Cells[row, 25]).Value2.ToString();
                                    else MyCom.Parameters["@man_h21"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 26]).Value2 != null)
                                        MyCom.Parameters["@man_h22"].Value = ((Excel.Range)workSheet.Cells[row, 26]).Value2.ToString();
                                    else MyCom.Parameters["@man_h22"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 27]).Value2 != null)
                                        MyCom.Parameters["@man_h23"].Value = ((Excel.Range)workSheet.Cells[row, 27]).Value2.ToString();
                                    else MyCom.Parameters["@man_h23"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[row, 28]).Value2 != null)
                                        MyCom.Parameters["@man_h24"].Value = ((Excel.Range)workSheet.Cells[row, 28]).Value2.ToString();
                                    else MyCom.Parameters["@man_h24"].Value = 0;

                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        if (messages == "")
                                        {
                                            messages += "Error Manategh In  Date  :" + date + "\r\n";
                                        }

                                        string str = exp.Message;
                                    }

                                    row++;
                                    esc = true;
                                } while ((((Excel.Range)workSheet.Cells[row, 2]).Value2 == null) && (row < 117));

                            }
                            if (!esc) row++;
                            esc = false;
                        }
                    }

                book.Close(false, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();
                System.Threading.Thread.CurrentThread.CurrentCulture = oldci;

                ///////////////////////////////////////////////////////////////
            }
            catch
            {
                AddNotExistMessage("Manategh");
            }
        }
        //----------------------------------ProducedEnergy(string date)-------------------------
        public void ProducedEnergy(string date,bool hardinternet)
        {
            try
            {
                //string hardPath = @"c:\data\produce-energy.xls";
                string sheetname="توليد";
                string temppath = BaseData.GetInstance().M009Path;
                string hardPath = temppath + @"\" + "produce-energy.xls";


                string Rdate = date;
                Rdate = Rdate.Replace("/", "");
                //string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/En_files/sheet001.htm";
                //string address = "http://sccisrep.igmc.ir/Html/" + Rdate + "/En_files/sheet001.htm";

                string address = "http://sccisrep.igmc.ir/Html/" + Rdate + "/En_R.xls";

                DataTable dt8 = Utilities.GetTable("select * from internetaddress");
                if (dt8.Rows.Count > 0)
                {
                    sheetname = dt8.Rows[0]["sheetProduceEnergy"].ToString();
                    address = dt8.Rows[0]["ProduceEnergy"].ToString().Trim();
                    if (address.Contains("Date Without Seperator(8char)")) address = address.Replace("Date Without Seperator(8char)", Rdate);
                }


                if (hardinternet == false)
                {
                    try
                    {
                        //Downloading produce-energy.xls from website
                        if (File.Exists(hardPath))
                            File.Delete(hardPath);

                        WebProxy proxy = null;

                        if (proxyAddress != "")
                        {
                            proxy = new WebProxy(proxyAddress);
                            if (Username != "")
                            {
                                proxy.Credentials = new NetworkCredential(Username.Trim(), password.Trim(), domain.Trim());

                            }
                            else
                            {
                                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                            }

                        }
                        WebClient client = new WebClient();
                        if (proxyAddress != "")
                        {
                            client.Proxy = proxy;
                        }
                        client.DownloadFile(address, hardPath);
                    }
                    catch (Exception exp)
                    {
                        string str = exp.Message;
                        if (messages == "")
                        {
                            messages += "Error ProducedEnergy In Date  :" + date + "\r\n";
                        }

                        NRI.SBS.Common.CLogCodeErrors.LogError(exp, address, hardPath, proxyAddress);

                    }
                }
                else if (hardinternet == true)
                {
                   // hardPath = temppath + @"\" + "En_R.xls";

                    string TempDate = date;
                    string Tempday = TempDate.Substring(8);
                    string TempMonth = TempDate.Substring(5, 2);
                    string Tempyear = TempDate.Substring(0, 4);


                    hardPath = temppath + @"\" + Tempyear.ToString() + @"\" + int.Parse(TempMonth) + @"\" + int.Parse(Tempday) + @"\" + "En_R.xls";
                }

              
              
      

                if (!File.Exists(hardPath))
                {
                    AddNotExistMessage(address);
                    return;
                }

                ////remove <link and saveas file
                //StringBuilder newFileproduce = new StringBuilder();
                //string tempproduce = "";
                //string[] fileproduce = File.ReadAllLines(hardPath, Encoding.Default);
                //foreach (string line in fileproduce)
                //{
                //    if (line.Contains("<link"))
                //    {
                //        tempproduce = line.Remove(0);
                //        newFileproduce.Append(tempproduce + "\r\n");
                //        continue;
                //    }
                //    newFileproduce.Append(line + "\r\n");
                //}
                //File.WriteAllText(hardPath, newFileproduce.ToString(), Encoding.Default);

                //Insert into DB (ProducedEnergy)
                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                Excel.Workbook book = null;
                book = exobj.Workbooks.Open(hardPath, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;
                SqlCommand MyCom = new SqlCommand();

                MyCom.Parameters.Add("@PE_date", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@PE_code", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@PE_part", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@PE_h1", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h2", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h3", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h4", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h5", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h6", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h7", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h8", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h9", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h10", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h11", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h12", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h13", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h14", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h15", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h16", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h17", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h18", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h19", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h20", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h21", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h22", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h23", SqlDbType.Real);
                MyCom.Parameters.Add("@PE_h24", SqlDbType.Real);

                foreach (Excel.Worksheet workSheet in book.Worksheets)
                    if (workSheet.Name ==sheetname )
                    {
                        string mydate = ((Excel.Range)workSheet.Cells[3, 3]).Value2.ToString();
                        int row = 3;

                        if (date != mydate)
                        {
                            AddNotExistMessage(" Not Existed For Selected Date!   ");
                            return;
                        }

                        SqlCommand command = new SqlCommand();
                        command.Connection = MyConnection;
                        command.CommandText = "DELETE FROM [ProducedEnergy] WHERE Date ='" + mydate + "'";
                        command.ExecuteNonQuery();

                        while (((Excel.Range)workSheet.Cells[row, 4]).Value2 != null)
                        {
                            if ((((Excel.Range)workSheet.Cells[row, 1]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString().Contains("کد")))
                            {

                                MyCom.CommandText = "INSERT INTO [ProducedEnergy] (Date," +
                                "PPCode,Part,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8," +
                                "Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16," +
                                "Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24)" +
                                "VALUES (@PE_date,@PE_code,@PE_part,@PE_h1,@PE_h2,@PE_h3,@PE_h4," +
                                "@PE_h5,@PE_h6,@PE_h7,@PE_h8,@PE_h9,@PE_h10,@PE_h11,@PE_h12," +
                                "@PE_h13,@PE_h14,@PE_h15,@PE_h16,@PE_h17,@PE_h18,@PE_h19," +
                                "@PE_h20,@PE_h21,@PE_h22,@PE_h23,@PE_h24)";

                                MyCom.Connection = MyConnection;

                                MyCom.Parameters["@PE_date"].Value = mydate;
                                if (((Excel.Range)workSheet.Cells[row, 2]).Value2 != null)
                                    MyCom.Parameters["@PE_code"].Value = ((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString();
                                else MyCom.Parameters["@PE_code"].Value = "";
                                if (((Excel.Range)workSheet.Cells[row, 4]).Value2 != null)
                                    MyCom.Parameters["@PE_part"].Value = ((Excel.Range)workSheet.Cells[row, 4]).Value2.ToString();
                                else MyCom.Parameters["@PE_part"].Value = "";
                                if (((Excel.Range)workSheet.Cells[row, 5]).Value2 != null)
                                    MyCom.Parameters["@PE_h1"].Value = ((Excel.Range)workSheet.Cells[row, 5]).Value2.ToString();
                                else MyCom.Parameters["@PE_h1"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 6]).Value2 != null)
                                    MyCom.Parameters["@PE_h2"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();
                                else MyCom.Parameters["@PE_h2"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 7]).Value2 != null)
                                    MyCom.Parameters["@PE_h3"].Value = ((Excel.Range)workSheet.Cells[row, 7]).Value2.ToString();
                                else MyCom.Parameters["@PE_h3"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 8]).Value2 != null)
                                    MyCom.Parameters["@PE_h4"].Value = ((Excel.Range)workSheet.Cells[row, 8]).Value2.ToString();
                                else MyCom.Parameters["@PE_h4"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 9]).Value2 != null)
                                    MyCom.Parameters["@PE_h5"].Value = ((Excel.Range)workSheet.Cells[row, 9]).Value2.ToString();
                                else MyCom.Parameters["@PE_h5"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 10]).Value2 != null)
                                    MyCom.Parameters["@PE_h6"].Value = ((Excel.Range)workSheet.Cells[row, 10]).Value2.ToString();
                                else MyCom.Parameters["@PE_h6"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 11]).Value2 != null)
                                    MyCom.Parameters["@PE_h7"].Value = ((Excel.Range)workSheet.Cells[row, 11]).Value2.ToString();
                                else MyCom.Parameters["@PE_h7"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 12]).Value2 != null)
                                    MyCom.Parameters["@PE_h8"].Value = ((Excel.Range)workSheet.Cells[row, 12]).Value2.ToString();
                                else MyCom.Parameters["@PE_h8"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 13]).Value2 != null)
                                    MyCom.Parameters["@PE_h9"].Value = ((Excel.Range)workSheet.Cells[row, 13]).Value2.ToString();
                                else MyCom.Parameters["@PE_h9"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 14]).Value2 != null)
                                    MyCom.Parameters["@PE_h10"].Value = ((Excel.Range)workSheet.Cells[row, 14]).Value2.ToString();
                                else MyCom.Parameters["@PE_h10"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 15]).Value2 != null)
                                    MyCom.Parameters["@PE_h11"].Value = ((Excel.Range)workSheet.Cells[row, 15]).Value2.ToString();
                                else MyCom.Parameters["@PE_h11"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 16]).Value2 != null)
                                    MyCom.Parameters["@PE_h12"].Value = ((Excel.Range)workSheet.Cells[row, 16]).Value2.ToString();
                                else MyCom.Parameters["@PE_h12"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 17]).Value2 != null)
                                    MyCom.Parameters["@PE_h13"].Value = ((Excel.Range)workSheet.Cells[row, 17]).Value2.ToString();
                                else MyCom.Parameters["@PE_h13"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 18]).Value2 != null)
                                    MyCom.Parameters["@PE_h14"].Value = ((Excel.Range)workSheet.Cells[row, 18]).Value2.ToString();
                                else MyCom.Parameters["@PE_h14"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 19]).Value2 != null)
                                    MyCom.Parameters["@PE_h15"].Value = ((Excel.Range)workSheet.Cells[row, 19]).Value2.ToString();
                                else MyCom.Parameters["@PE_h15"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 20]).Value2 != null)
                                    MyCom.Parameters["@PE_h16"].Value = ((Excel.Range)workSheet.Cells[row, 20]).Value2.ToString();
                                else MyCom.Parameters["@PE_h16"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 21]).Value2 != null)
                                    MyCom.Parameters["@PE_h17"].Value = ((Excel.Range)workSheet.Cells[row, 21]).Value2.ToString();
                                else MyCom.Parameters["@PE_h17"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 22]).Value2 != null)
                                    MyCom.Parameters["@PE_h18"].Value = ((Excel.Range)workSheet.Cells[row, 22]).Value2.ToString();
                                else MyCom.Parameters["@PE_h18"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 23]).Value2 != null)
                                    MyCom.Parameters["@PE_h19"].Value = ((Excel.Range)workSheet.Cells[row, 23]).Value2.ToString();
                                else MyCom.Parameters["@PE_h19"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 24]).Value2 != null)
                                    MyCom.Parameters["@PE_h20"].Value = ((Excel.Range)workSheet.Cells[row, 24]).Value2.ToString();
                                else MyCom.Parameters["@PE_h20"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 25]).Value2 != null)
                                    MyCom.Parameters["@PE_h21"].Value = ((Excel.Range)workSheet.Cells[row, 25]).Value2.ToString();
                                else MyCom.Parameters["@PE_h21"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 26]).Value2 != null)
                                    MyCom.Parameters["@PE_h22"].Value = ((Excel.Range)workSheet.Cells[row, 26]).Value2.ToString();
                                else MyCom.Parameters["@PE_h22"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 27]).Value2 != null)
                                    MyCom.Parameters["@PE_h23"].Value = ((Excel.Range)workSheet.Cells[row, 27]).Value2.ToString();
                                else MyCom.Parameters["@PE_h23"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 28]).Value2 != null)
                                    MyCom.Parameters["@PE_h24"].Value = ((Excel.Range)workSheet.Cells[row, 28]).Value2.ToString();
                                else MyCom.Parameters["@PE_h24"].Value = 0;

                                try
                                {
                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception exp)
                                {
                                    if (messages == "")
                                    {
                                        messages += "Error ProducedEnergy In Date  :" + date + "\r\n";
                                    }
                                    string str = exp.Message;
                                }
                            }
                            row++;
                        }
                    }
                book.Close(false, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();
                System.Threading.Thread.CurrentThread.CurrentCulture = oldci;

                ////////////////////////////////////////////////////////////////////
            }
            catch
            {
                AddNotExistMessage("ProducedEnergy");
            }
        }
        //-------------------------------------InterchangedEnergy(string date)------------------------------
        public void InterchangedEnergy(string date,bool hardinternet)
        {
            try
            {
                string sheetname = "تبادل";
                string temppath = BaseData.GetInstance().M009Path;
                string hardPath = temppath + @"\" + "interchange-energy.xls";

                //string hardPath=@"c:\data\interchange-energy.xls";
                string Rdate = date;
                Rdate = Rdate.Replace("/", "");
                // string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/En_files/sheet002.htm";
                //string address = "http://sccisrep.igmc.ir/Html/" + Rdate + "/En_files/sheet002.htm";
                string address = "http://sccisrep.igmc.ir/Html/" + Rdate + "/En_R.xls";

                DataTable dt8 = Utilities.GetTable("select * from internetaddress");
                if (dt8.Rows.Count > 0)
                {
                    sheetname = dt8.Rows[0]["sheetInterchangedEnergy"].ToString();
                    address = dt8.Rows[0]["InterchangedEnergy"].ToString().Trim();
                    if (address.Contains("Date Without Seperator(8char)")) address = address.Replace("Date Without Seperator(8char)", Rdate);
                }


                if (hardinternet == false)
                {

                    //Downloading interchange-energy.xls from website
                    try
                    {
                        if (File.Exists(hardPath))
                            File.Delete(hardPath);

                        WebProxy proxy = null;
                        if (proxyAddress != "")
                        {
                            proxy = new WebProxy(proxyAddress);
                            if (Username != "")
                            {
                                proxy.Credentials = new NetworkCredential(Username.Trim(), password.Trim(), domain.Trim());

                            }
                            else
                            {
                                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                            }

                        }
                        WebClient client = new WebClient();
                        if (proxyAddress != "")
                        {
                            client.Proxy = proxy;
                        }
                        client.DownloadFile(address, hardPath);
                    }
                    catch (Exception exp)
                    {
                        if (messages == "")
                        {
                            messages += "Error Interchange In Date  :" + date + "\r\n";
                        }
                        string str = exp.Message;
                        NRI.SBS.Common.CLogCodeErrors.LogError(exp, address, hardPath, proxyAddress);

                    }
                }

                else
                {
                    string TempDate = date;
                    string Tempday = TempDate.Substring(8);
                    string TempMonth = TempDate.Substring(5, 2);
                    string Tempyear = TempDate.Substring(0, 4);


                    hardPath = temppath + @"\" + Tempyear.ToString() + @"\" + int.Parse(TempMonth) + @"\" + int.Parse(Tempday) + @"\" + "En_R.xls";


                    //hardPath = temppath + @"\" + "En_R.xls";

                }
                if (!File.Exists(hardPath))
                {
                    AddNotExistMessage(address);
                    return;
                }
                ////remove <link and saveas file
                //StringBuilder newFileinterchange = new StringBuilder();
                //string tempinterchange = "";
                //string[] fileinterchange = File.ReadAllLines(hardPath, Encoding.Default);
                //foreach (string line in fileinterchange)
                //{
                //    if (line.Contains("<link"))
                //    {
                //        tempinterchange = line.Remove(0);
                //        newFileinterchange.Append(tempinterchange + "\r\n");
                //        continue;
                //    }
                //    newFileinterchange.Append(line + "\r\n");
                //}
                //File.WriteAllText(hardPath, newFileinterchange.ToString(), Encoding.Default);

                //Insert into DB (InterchangedEnergy)
                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                Excel.Workbook book = null;
                book = exobj.Workbooks.Open(hardPath, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;
                SqlCommand MyCom = new SqlCommand();



                MyCom.Parameters.Add("@IE_date", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@IE_code", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@IE_name", SqlDbType.NChar, 50);
                MyCom.Parameters.Add("@IE_h1", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h2", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h3", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h4", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h5", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h6", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h7", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h8", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h9", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h10", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h11", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h12", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h13", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h14", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h15", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h16", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h17", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h18", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h19", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h20", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h21", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h22", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h23", SqlDbType.Real);
                MyCom.Parameters.Add("@IE_h24", SqlDbType.Real);

                foreach (Excel.Worksheet workSheet in book.Worksheets)
                    if (workSheet.Name == sheetname)
                    {
                        string mydate = ((Excel.Range)workSheet.Cells[3, 2]).Value2.ToString();

                        if (date != mydate)
                        {
                            AddNotExistMessage(" Not Existed For Selected Date!   ");
                            return;
                        }

                        SqlCommand command = new SqlCommand();
                        command.Connection = MyConnection;
                        command.CommandText = "DELETE fROM [InterchangedEnergy] WHERE Date='" +
                            mydate + "'";
                        command.ExecuteNonQuery();

                        int row = 3;
                        while (((Excel.Range)workSheet.Cells[row, 2]).Value2 != null)
                        {
                            if ((((Excel.Range)workSheet.Cells[row, 1]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString().Contains("کد")))
                            {
                                MyCom.CommandText = "INSERT INTO [InterchangedEnergy] (Date," +
                                "Code,Name,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8," +
                                "Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16," +
                                "Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24)" +
                                "VALUES (@IE_date,@IE_code,@IE_name,@IE_h1,@IE_h2,@IE_h3,@IE_h4," +
                                "@IE_h5,@IE_h6,@IE_h7,@IE_h8,@IE_h9,@IE_h10,@IE_h11,@IE_h12," +
                                "@IE_h13,@IE_h14,@IE_h15,@IE_h16,@IE_h17,@IE_h18,@IE_h19," +
                                "@IE_h20,@IE_h21,@IE_h22,@IE_h23,@IE_h24)";

                                MyCom.Connection = MyConnection;

                                MyCom.Parameters["@IE_date"].Value = mydate;
                                if (((Excel.Range)workSheet.Cells[row, 1]).Value2 != null)
                                    MyCom.Parameters["@IE_code"].Value = ((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString();
                                else MyCom.Parameters["@IE_code"].Value = "";
                                if (((Excel.Range)workSheet.Cells[row, 2]).Value2 != null)
                                    MyCom.Parameters["@IE_name"].Value = ((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString();
                                else MyCom.Parameters["@IE_name"].Value = "";
                                if (((Excel.Range)workSheet.Cells[row, 3]).Value2 != null)
                                    MyCom.Parameters["@IE_h1"].Value = ((Excel.Range)workSheet.Cells[row, 3]).Value2.ToString();
                                else MyCom.Parameters["@IE_h1"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 4]).Value2 != null)
                                    MyCom.Parameters["@IE_h2"].Value = ((Excel.Range)workSheet.Cells[row, 4]).Value2.ToString();
                                else MyCom.Parameters["@IE_h2"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 5]).Value2 != null)
                                    MyCom.Parameters["@IE_h3"].Value = ((Excel.Range)workSheet.Cells[row, 5]).Value2.ToString();
                                else MyCom.Parameters["@IE_h3"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 6]).Value2 != null)
                                    MyCom.Parameters["@IE_h4"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();
                                else MyCom.Parameters["@IE_h4"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 7]).Value2 != null)
                                    MyCom.Parameters["@IE_h5"].Value = ((Excel.Range)workSheet.Cells[row, 7]).Value2.ToString();
                                else MyCom.Parameters["@IE_h5"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 8]).Value2 != null)
                                    MyCom.Parameters["@IE_h6"].Value = ((Excel.Range)workSheet.Cells[row, 8]).Value2.ToString();
                                else MyCom.Parameters["@IE_h6"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 9]).Value2 != null)
                                    MyCom.Parameters["@IE_h7"].Value = ((Excel.Range)workSheet.Cells[row, 9]).Value2.ToString();
                                else MyCom.Parameters["@IE_h7"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 10]).Value2 != null)
                                    MyCom.Parameters["@IE_h8"].Value = ((Excel.Range)workSheet.Cells[row, 10]).Value2.ToString();
                                else MyCom.Parameters["@IE_h8"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 11]).Value2 != null)
                                    MyCom.Parameters["@IE_h9"].Value = ((Excel.Range)workSheet.Cells[row, 11]).Value2.ToString();
                                else MyCom.Parameters["@IE_h9"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 12]).Value2 != null)
                                    MyCom.Parameters["@IE_h10"].Value = ((Excel.Range)workSheet.Cells[row, 12]).Value2.ToString();
                                else MyCom.Parameters["@IE_h10"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 13]).Value2 != null)
                                    MyCom.Parameters["@IE_h11"].Value = ((Excel.Range)workSheet.Cells[row, 13]).Value2.ToString();
                                else MyCom.Parameters["@IE_h11"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 14]).Value2 != null)
                                    MyCom.Parameters["@IE_h12"].Value = ((Excel.Range)workSheet.Cells[row, 14]).Value2.ToString();
                                else MyCom.Parameters["@IE_h12"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 15]).Value2 != null)
                                    MyCom.Parameters["@IE_h13"].Value = ((Excel.Range)workSheet.Cells[row, 15]).Value2.ToString();
                                else MyCom.Parameters["@IE_h13"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 16]).Value2 != null)
                                    MyCom.Parameters["@IE_h14"].Value = ((Excel.Range)workSheet.Cells[row, 16]).Value2.ToString();
                                else MyCom.Parameters["@IE_h14"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 17]).Value2 != null)
                                    MyCom.Parameters["@IE_h15"].Value = ((Excel.Range)workSheet.Cells[row, 17]).Value2.ToString();
                                else MyCom.Parameters["@IE_h15"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 18]).Value2 != null)
                                    MyCom.Parameters["@IE_h16"].Value = ((Excel.Range)workSheet.Cells[row, 18]).Value2.ToString();
                                else MyCom.Parameters["@IE_h16"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 19]).Value2 != null)
                                    MyCom.Parameters["@IE_h17"].Value = ((Excel.Range)workSheet.Cells[row, 19]).Value2.ToString();
                                else MyCom.Parameters["@IE_h17"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 20]).Value2 != null)
                                    MyCom.Parameters["@IE_h18"].Value = ((Excel.Range)workSheet.Cells[row, 20]).Value2.ToString();
                                else MyCom.Parameters["@IE_h18"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 21]).Value2 != null)
                                    MyCom.Parameters["@IE_h19"].Value = ((Excel.Range)workSheet.Cells[row, 21]).Value2.ToString();
                                else MyCom.Parameters["@IE_h19"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 22]).Value2 != null)
                                    MyCom.Parameters["@IE_h20"].Value = ((Excel.Range)workSheet.Cells[row, 22]).Value2.ToString();
                                else MyCom.Parameters["@IE_h20"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 23]).Value2 != null)
                                    MyCom.Parameters["@IE_h21"].Value = ((Excel.Range)workSheet.Cells[row, 23]).Value2.ToString();
                                else MyCom.Parameters["@IE_h21"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 24]).Value2 != null)
                                    MyCom.Parameters["@IE_h22"].Value = ((Excel.Range)workSheet.Cells[row, 24]).Value2.ToString();
                                else MyCom.Parameters["@IE_h22"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 25]).Value2 != null)
                                    MyCom.Parameters["@IE_h23"].Value = ((Excel.Range)workSheet.Cells[row, 25]).Value2.ToString();
                                else MyCom.Parameters["@IE_h23"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 26]).Value2 != null)
                                    MyCom.Parameters["@IE_h24"].Value = ((Excel.Range)workSheet.Cells[row, 26]).Value2.ToString();
                                else MyCom.Parameters["@IE_h24"].Value = 0;

                                try
                                {
                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception exp)
                                {
                                    if (messages == "")
                                    {
                                        messages += "Error Interchange In Date  :" + date + "\r\n";
                                    }
                                    string str = exp.Message;
                                }
                            }
                            row++;
                        }
                    }
                book.Close(false, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();
                System.Threading.Thread.CurrentThread.CurrentCulture = oldci;

                ///////////////////////////////////////////////////////////////
            }
            catch
            {
                AddNotExistMessage("InterchangedEnergy");
            }

        }
        //------------------------------------RegionNetComp(string date)-------------------------------
        public void RegionNetComp(string date,bool hardinternet)
        {
            try
            {
                string sheetname="Regions_PowerPlants";
                string temppath = BaseData.GetInstance().M009Path;
                string hardPath = temppath + @"\" + "regionnetcomp.xls";

                // string hardPath = @"c:\data\regionnetcomp.xls";
                string Rdate = date;
                Rdate = Rdate.Replace("/", "");
                //string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/NetworkComponents_files/sheet006.htm";
                //string address = "http://sccisrep.igmc.ir/Html/" + Rdate + "/NetworkComponents_files/sheet006.htm";
                string address = "http://sccisrep.igmc.ir/Html/" + Rdate + "/NetworkComponents_R.xls";

                DataTable dt8 = Utilities.GetTable("select * from internetaddress");
                if (dt8.Rows.Count > 0)
                {
                    sheetname= dt8.Rows[0]["sheetReginnetcomp"].ToString();
                    address = dt8.Rows[0]["Reginnetcomp"].ToString().Trim();
                    if (address.Contains("Date Without Seperator(8char)")) address = address.Replace("Date Without Seperator(8char)", Rdate);
                }

                if (hardinternet == false)
                {
                    //Downloading regionnetcomp.xls from website
                    try
                    {
                        if (File.Exists(hardPath))
                            File.Delete(hardPath);

                        WebProxy proxy = null;
                        if (proxyAddress != "")
                        {

                            proxy = new WebProxy(proxyAddress);
                            if (Username != "")
                            {
                                proxy.Credentials = new NetworkCredential(Username.Trim(), password.Trim(), domain.Trim());

                            }
                            else
                            {
                                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                            }

                        }
                        WebClient client = new WebClient();
                        if (proxyAddress != "")
                        {
                            client.Proxy = proxy;
                        }
                        client.DownloadFile(address, hardPath);
                    }
                    catch (Exception exp)
                    {
                        if (messages == "")
                        {
                            messages += "Error RegionNet In Date  :" + date + "\r\n";
                        }
                        string str = exp.Message;
                        NRI.SBS.Common.CLogCodeErrors.LogError(exp, address, hardPath, proxyAddress);

                    }
                }
                else
                {

                    string TempDate = date;
                    string Tempday = TempDate.Substring(8);
                    string TempMonth = TempDate.Substring(5, 2);
                    string Tempyear = TempDate.Substring(0, 4);


                    hardPath = temppath + @"\" + Tempyear.ToString() + @"\" + int.Parse(TempMonth) + @"\" + int.Parse(Tempday) + @"\" + "NetworkComponents_R.xls";
                    
                    //hardPath = temppath + @"\" + "NetworkComponents_R.xls";
                }
                if (!File.Exists(hardPath))
                {
                    AddNotExistMessage(address);
                    return;
                }

                ////remove <link and saveas file
                //StringBuilder newFileregion = new StringBuilder();
                //string tempregion = "";
                //string[] fileregion = File.ReadAllLines(hardPath, Encoding.Default);
                //foreach (string line in fileregion)
                //{
                //    if (line.Contains("<link"))
                //    {
                //        tempregion = line.Remove(0);
                //        newFileregion.Append(tempregion + "\r\n");
                //        continue;
                //    }
                //    newFileregion.Append(line + "\r\n");
                //}
                //File.WriteAllText(hardPath, newFileregion.ToString(), Encoding.Default);

                //Insert into DB (RegionNetComp);
                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                Excel.Workbook book = null;
                book = exobj.Workbooks.Open(hardPath, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;
                SqlCommand MyCom = new SqlCommand();

                MyCom.Parameters.Add("@RNC_date", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@RNC_code", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@RNC_ppcode", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@RNC_name", SqlDbType.NChar, 50);
                MyCom.Parameters.Add("@RNC_type", SqlDbType.NChar, 1);
                MyCom.Parameters.Add("@RNC_power", SqlDbType.Real);

                foreach (Excel.Worksheet workSheet in book.Worksheets)
                    if (workSheet.Name ==sheetname )
                    {
                        string mydate = ((Excel.Range)workSheet.Cells[3, 1]).Value2.ToString();
                        mydate = mydate.Remove(0, (mydate.Length - 10));

                        if (date != mydate)
                        {
                            AddNotExistMessage(" Not Existed For Selected Date!   ");
                            return;
                        }

                        SqlCommand command = new SqlCommand();
                        command.Connection = MyConnection;
                        command.CommandText = "DELETE FROM [RegionNetComp] WHERE Date ='" + mydate + "'";
                        command.ExecuteNonQuery();

                        int row = 4;
                        while (row < 230)
                        {
                            if ((((Excel.Range)workSheet.Cells[row, 1]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString().Contains("كد")))
                            {
                                string code = ((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString();
                                row++;
                                while ((((Excel.Range)workSheet.Cells[row, 1]).Value2 != null) && (row < 230))
                                {
                                    MyCom.CommandText = "INSERT INTO [RegionNetComp] (Date," +
                                    "Code,PPCode,Type,Name,Power) VALUES (@RNC_date,@RNC_code" +
                                    ",@RNC_ppcode,@RNC_type,@RNC_name,@RNC_power)";

                                    MyCom.Connection = MyConnection;

                                    MyCom.Parameters["@RNC_date"].Value = mydate;
                                    MyCom.Parameters["@RNC_code"].Value = code;
                                    if (((Excel.Range)workSheet.Cells[row, 1]).Value2 != null)
                                        MyCom.Parameters["@RNC_ppcode"].Value = ((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString();
                                    else MyCom.Parameters["@RNC_ppcode"].Value = "";
                                    if (((Excel.Range)workSheet.Cells[row, 2]).Value2 != null)
                                        MyCom.Parameters["@RNC_name"].Value = ((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString();
                                    else MyCom.Parameters["@RNC_name"].Value = "";
                                    if (((Excel.Range)workSheet.Cells[row, 3]).Value2 != null)
                                        MyCom.Parameters["@RNC_type"].Value = ((Excel.Range)workSheet.Cells[row, 3]).Value2.ToString();
                                    else MyCom.Parameters["@RNC_type"].Value = "";
                                    if (((Excel.Range)workSheet.Cells[row, 6]).Value2 != null)
                                        MyCom.Parameters["@RNC_power"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();
                                    else MyCom.Parameters["@RNC_power"].Value = 0;
                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        if (messages == "")
                                        {
                                            messages += "Error RegionNet In Date  :" + date + "\r\n";
                                        }
                                        string str = exp.Message;
                                    }
                                    row++;
                                }
                            }
                            row++;
                        }
                    }
                book.Close(false, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();
                System.Threading.Thread.CurrentThread.CurrentCulture = oldci;

                /////////////////////////////////////////////////////////////
            }
            catch
            {
                AddNotExistMessage("RegionNetComp");
            }
        }
        //------------------------------------LineNetComp(string date)-------------------------------
        public void LineNetComp(string date,bool hardinternet)
        {
            try
            {
                string sheetname="Lines";
                string temppath = BaseData.GetInstance().M009Path;
                string hardPath = temppath + @"\" + "linenetcomp.xls";

                //string hardPath = @"c:\data\linenetcomp.xls";
                string Rdate = date;
                Rdate = Rdate.Replace("/", "");
                //string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/NetworkComponents_files/sheet002.htm";
                //string address = "http://sccisrep.igmc.ir/Html/" + Rdate + "/NetworkComponents_files/sheet002.htm";
                string address = "http://sccisrep.igmc.ir/Html/" + Rdate + "/NetworkComponents_R.xls";

                DataTable dt8 = Utilities.GetTable("select * from internetaddress");
                if (dt8.Rows.Count > 0)
                {
                    sheetname = dt8.Rows[0]["sheetLinenetcomp"].ToString();
                    address = dt8.Rows[0]["Linenetcomp"].ToString().Trim();
                    if (address.Contains("Date Without Seperator(8char)")) address = address.Replace("Date Without Seperator(8char)", Rdate);
                }
                if (hardinternet == false)
                {
                    //Downloading linesnetcomp.xls from website
                    try
                    {
                        if (File.Exists(hardPath))
                            File.Delete(hardPath);

                        WebProxy proxy = null;
                        if (proxyAddress != "")
                        {
                            proxy = new WebProxy(proxyAddress);
                            if (Username != "")
                            {
                                proxy.Credentials = new NetworkCredential(Username.Trim(), password.Trim(), domain.Trim());

                            }
                            else
                            {
                                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                            }

                        }
                        WebClient client = new WebClient();
                        if (proxyAddress != "")
                        {
                            client.Proxy = proxy;
                        }
                        //client.DownloadFile(address, @"c:\data\linesnetcomp.xls");
                        client.DownloadFile(address, hardPath);
                    }
                    catch (Exception exp)
                    {
                        string str = exp.Message; 
                        if (messages == "")
                        {
                            messages += "Error LineNet In Date  :" + date + "\r\n";
                        }
                        NRI.SBS.Common.CLogCodeErrors.LogError(exp, address, hardPath, proxyAddress);

                    }
                }
                else
                {

                    string TempDate = date;
                    string Tempday = TempDate.Substring(8);
                    string TempMonth = TempDate.Substring(5, 2);
                    string Tempyear = TempDate.Substring(0, 4);


                    hardPath = temppath + @"\" + Tempyear.ToString() + @"\" + int.Parse(TempMonth) + @"\" + int.Parse(Tempday) + @"\" + "NetworkComponents_R.xls";

                   // hardPath = temppath + @"\" + "NetworkComponents_R.xls";
                }

                if (!File.Exists(hardPath))
                {
                    AddNotExistMessage(address);
                    return;
                }
                ////remove <link and saveas file
                //StringBuilder newFileline = new StringBuilder();
                //string templine = "";
                //string[] fileline = File.ReadAllLines(hardPath, Encoding.Default);
                //foreach (string line in fileline)
                //{
                //    if (line.Contains("<link"))
                //    {
                //        templine = line.Remove(0);
                //        newFileline.Append(templine + "\r\n");
                //        continue;
                //    }
                //    newFileline.Append(line + "\r\n");
                //}
                //File.WriteAllText(hardPath, newFileline.ToString(), Encoding.Default);

                //Insert into DB (LineNetComp)
                string path1 = hardPath;
                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                Excel.Workbook book = null;
                book = exobj.Workbooks.Open(path1, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;
                SqlCommand MyCom = new SqlCommand();

                MyCom.Parameters.Add("@LNC_date", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@LNC_code", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@LNC_origin", SqlDbType.NChar, 20);
                MyCom.Parameters.Add("@LNC_destination", SqlDbType.NChar, 20);



                foreach (Excel.Worksheet workSheet in book.Worksheets)
                    if (workSheet.Name ==sheetname )
                    {
                        string mydate = ((Excel.Range)workSheet.Cells[3, 1]).Value2.ToString();
                        mydate = mydate.Remove(0, (mydate.Length - 10));


                        if (date != mydate)
                        {
                            AddNotExistMessage(" Not Existed For Selected Date!   ");
                            return;
                        }

                        SqlCommand command = new SqlCommand();
                        command.Connection = MyConnection;
                        command.CommandText = "DELETE FROM [LineNetComp] WHERE Date='" +
                            mydate + "'";
                        command.ExecuteNonQuery();

                        int row = 4;
                        while (row < 700)
                        {
                            if ((((Excel.Range)workSheet.Cells[row, 1]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString().Contains("كد")) && (((Excel.Range)workSheet.Cells[row, 2]).Value2 != null))
                            {
                                MyCom.CommandText = "INSERT INTO [LineNetComp] (Date," +
                                "Code,Origin,Destination) VALUES (@LNC_date,@LNC_code" +
                                ",@LNC_origin,@LNC_destination)";

                                MyCom.Connection = MyConnection;

                                MyCom.Parameters["@LNC_date"].Value = mydate;
                                if (((Excel.Range)workSheet.Cells[row, 1]).Value2 != null)
                                    MyCom.Parameters["@LNC_code"].Value = ((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString();
                                else MyCom.Parameters["@LNC_code"].Value = "";
                                if (((Excel.Range)workSheet.Cells[row, 5]).Value2 != null)
                                    MyCom.Parameters["@LNC_origin"].Value = ((Excel.Range)workSheet.Cells[row, 5]).Value2.ToString();
                                else MyCom.Parameters["@LNC_origin"].Value = "";
                                if (((Excel.Range)workSheet.Cells[row, 6]).Value2 != null)
                                    MyCom.Parameters["@LNC_destination"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();
                                else MyCom.Parameters["@LNC_destination"].Value = "";
                                try
                                {
                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception exp)
                                {
                                    if (messages == "")
                                    {
                                        messages += "Error LineNet In Date  :" + date + "\r\n";
                                    }
                                    string str = exp.Message;
                                }
                            }
                            row++;
                        }
                    }
                book.Close(false, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();
                System.Threading.Thread.CurrentThread.CurrentCulture = oldci;

                //////////////////////////////////////////////////////////////////
            }
            catch
            {
                AddNotExistMessage("LineNetComp");
            }
        }
        //------------------------------------UnitNetComp(string date)-------------------------------
        public void UnitNetComp(string date,bool hardinternet)
        {
            try
            {
                string sheetname = "PowerPlants_Units_A";
                string temppath = BaseData.GetInstance().M009Path;
                string hardPath = temppath + @"\" + "unitnetcomp.xls";

                //string hardPath = @"c:\data\unitnetcomp.xls";    
                string Rdate = date;
                Rdate = Rdate.Replace("/", "");
                // string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/NetworkComponents_files/sheet008.htm";

                // string address = "http://sccisrep.igmc.ir/Html/" + Rdate + "/NetworkComponents_files/sheet008.htm";
                string address = "http://sccisrep.igmc.ir/Html/" + Rdate + "/NetworkComponents_R.xls";


                DataTable dt8 = Utilities.GetTable("select * from internetaddress");
                if (dt8.Rows.Count > 0)
                {
                    sheetname=dt8.Rows[0]["sheetUnitnetcomp"].ToString();
                    address = dt8.Rows[0]["Unitnetcomp"].ToString().Trim();
                    if (address.Contains("Date Without Seperator(8char)")) address = address.Replace("Date Without Seperator(8char)", Rdate);
                }

                if (hardinternet == false)
                {
                    //Downloading unitnetcomp.xls from website
                    try
                    {
                        if (File.Exists(hardPath))
                            File.Delete(hardPath);

                        WebProxy proxy = null;
                        if (proxyAddress != "")
                        {
                            proxy = new WebProxy(proxyAddress);
                            if (Username != "")
                            {
                                proxy.Credentials = new NetworkCredential(Username.Trim(), password.Trim(), domain.Trim());

                            }
                            else
                            {
                                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                            }

                        }
                        WebClient client = new WebClient();
                        if (proxyAddress != "")
                        {
                            client.Proxy = proxy;
                        }
                        client.DownloadFile(address, hardPath);
                    }
                    catch (Exception exp)
                    {
                        if (messages == "")
                        {
                            messages += "Error UnitNet In Date  :" + date + "\r\n";
                        }
                        string str = exp.Message;
                        NRI.SBS.Common.CLogCodeErrors.LogError(exp, address, hardPath, proxyAddress);

                    }
                }
                else
                {

                    string TempDate = date;
                    string Tempday = TempDate.Substring(8);
                    string TempMonth = TempDate.Substring(5, 2);
                    string Tempyear = TempDate.Substring(0, 4);


                    hardPath = temppath + @"\" + Tempyear.ToString() + @"\" + int.Parse(TempMonth) + @"\" + int.Parse(Tempday) + @"\" + "NetworkComponents_R.xls";

                   // hardPath = temppath + @"\" + "NetworkComponents_R.xls";

                }
                if (!File.Exists(hardPath))
                {
                    AddNotExistMessage(address);
                    return;
                }

                ////remove <link and saveas file
                //StringBuilder newFileunit = new StringBuilder();
                //string tempunit = "";
                //string[] fileunit = File.ReadAllLines(hardPath, Encoding.Default);
                //foreach (string line in fileunit)
                //{
                //    if (line.Contains("<link"))
                //    {
                //        tempunit = line.Remove(0);
                //        newFileunit.Append(tempunit + "\r\n");
                //        continue;
                //    }
                //    newFileunit.Append(line + "\r\n");
                //}
                //File.WriteAllText(hardPath, newFileunit.ToString(), Encoding.Default);

                //Insert into DB (UnitNetComp)
                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                Excel.Workbook book = null;
                book = exobj.Workbooks.Open(hardPath, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;
                SqlCommand MyCom = new SqlCommand();

                MyCom.Parameters.Add("@UNC_date", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@UNC_ppcode", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@UNC_ucode", SqlDbType.NChar, 1);
                MyCom.Parameters.Add("@UNC_name", SqlDbType.NChar, 50);
                MyCom.Parameters.Add("@UNC_xcode", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@UNC_cyclecode", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@UNC_power", SqlDbType.Real);

                foreach (Excel.Worksheet workSheet in book.Worksheets)
                    if (workSheet.Name == sheetname)
                    {
                        string mydate = ((Excel.Range)workSheet.Cells[3, 1]).Value2.ToString();
                        mydate = mydate.Remove(0, (mydate.Length - 10));

                        if (date != mydate)
                        {
                            AddNotExistMessage(" Not Existed For Selected Date!   ");
                            return;
                        }

                        SqlCommand command = new SqlCommand();
                        command.Connection = MyConnection;
                        command.CommandText = "DELETE FROM [UnitNetComp] WHERE Date ='" + mydate + "'";
                        command.ExecuteNonQuery();

                        int row = 4;
                        while (row < 720)
                        {
                            if ((((Excel.Range)workSheet.Cells[row, 1]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString().Contains("كد")))
                            {
                                string code = ((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString();
                                string name = ((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString();
                                string unitcode = ((Excel.Range)workSheet.Cells[row, 3]).Value2.ToString();
                                row++;
                                while ((row < 720) && (((Excel.Range)workSheet.Cells[row, 1]).Value2 == null) && (((Excel.Range)workSheet.Cells[row, 5]).Value2 != null))
                                {
                                    MyCom.CommandText = "INSERT INTO [UnitNetComp] (Date," +
                                    "PPCode,Name,UnitCode,XCode,CycleCode,Power) VALUES (@UNC_date" +
                                    ",@UNC_ppcode,@UNC_name,@UNC_ucode,@UNC_xcode," +
                                    "@UNC_cyclecode,@UNC_power)";

                                    MyCom.Connection = MyConnection;

                                    MyCom.Parameters["@UNC_date"].Value = mydate;
                                    MyCom.Parameters["@UNC_ppcode"].Value = code;
                                    MyCom.Parameters["@UNC_ucode"].Value = unitcode;
                                    MyCom.Parameters["@UNC_name"].Value = name;
                                    if (((Excel.Range)workSheet.Cells[row, 5]).Value2 != null)
                                        MyCom.Parameters["@UNC_xcode"].Value = ((Excel.Range)workSheet.Cells[row, 5]).Value2.ToString();
                                    else MyCom.Parameters["@UNC_xcode"].Value = "";
                                    if (((Excel.Range)workSheet.Cells[row, 6]).Value2 != null)
                                        MyCom.Parameters["@UNC_cyclecode"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();
                                    else MyCom.Parameters["@UNC_cyclecode"].Value = "";
                                    try
                                    {
                                        float x = float.Parse(((Excel.Range)workSheet.Cells[row, 8]).Value2.ToString());
                                        MyCom.Parameters["@UNC_power"].Value = x;
                                    }
                                    catch (Exception exp)
                                    {
                                        MyCom.Parameters["@UNC_power"].Value = 0;
                                    }
                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        if (messages == "")
                                        {
                                            messages += "Error UnitNet In Date  :" + date + "\r\n";
                                        }
                                        string str = exp.Message;
                                    }
                                    row++;
                                }
                            }
                            row++;
                        }
                    }
                book.Close(false, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();
                System.Threading.Thread.CurrentThread.CurrentCulture = oldci;

                //////////////////////////////////////////////////////////////
            }
            catch
            {
                AddNotExistMessage("UnitNetComp");
            }
        }

        public void ChartPrice(string date)
        {
            try
            {
                string path = Auto_Update_Library.BaseData.GetInstance().AveragePrice;
                string TempDate = date;
                int Tempday = int.Parse(TempDate.Substring(8));
                int TempMonth = int.Parse(TempDate.Substring(5, 2));
                int Tempyear = int.Parse(TempDate.Substring(0, 4));
                string Stempday = "", Stempmonth = "";

                Stempday = Tempday.ToString();

                Stempmonth = TempMonth.ToString();
                // old path = path + @"\" + Tempyear.ToString() + @"\" + Stempmonth;

                ////////////////////////////////////////////////////////////

                SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;

                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;

                string namedate = date;
                namedate = namedate.Remove(0, 2);
                namedate = namedate.Replace("/", "_");

                ////  string path = Auto_Update_Library.BaseData.GetInstance().AveragePrice;
               //old path += @"\" + "chart price BAMA " + namedate + ".xls";



                string pathname = folderFormat(date, "", "AveragePrice");
                if (pathname != "") path += pathname;
                string makename = NameFormat(date, "", "", "AveragePrice");
                if (makename != "") path = path + "\\" + makename ;

                ////////////////////////// max version2////////////////////////////
                string finalpath = "";

                if (File.Exists(path.Remove(path.Length - 1) + ".xls"))
                    finalpath = path.Remove(path.Length - 1);

                if (File.Exists(path + ".xls"))
                    finalpath = path;
                if (File.Exists(path + "1.xls"))
                    finalpath = path + "1";
                if (File.Exists(path + "2.xls"))
                    finalpath = path + "2";
                if (File.Exists(path + "3.xls"))
                    finalpath = path + "3";
                if (File.Exists(path + "4.xls"))
                    finalpath = path + "4";
                if (File.Exists(path + "5.xls"))
                    finalpath = path + "5";
                if (File.Exists(path + "6.xls"))
                    finalpath = path + "6";
                if (File.Exists(path + "7.xls"))
                    finalpath = path + "7";
                if (File.Exists(path + "8.xls"))
                    finalpath = path + "8";
                if (File.Exists(path + "9.xls"))
                    finalpath = path + "9";
                if (File.Exists(path + "10.xls"))
                    finalpath = path + "10";

                finalpath = finalpath + ".xls";

                path = finalpath;
                ///////////////////////////////////////////////////////////////////

                if (path != "" && File.Exists(path))
                {
                    //Save AS AveragePrice.xls file
                    Excel.Application exobj1 = new Excel.Application();
                    exobj1.Visible = false;
                    exobj1.UserControl = true;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    Excel.Workbook book1 = null;
                    book1 = exobj1.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book1.Save();
                    book1.Close(true, book1, Type.Missing);
                    exobj1.Workbooks.Close();
                    exobj1.Quit();
                    //read from AveragePrice.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "قيمت ";

                    DataTable dde = Utilities.GetTable("select * from FilesNameFormat where filename='AveragePrice'");
                    if (dde.Rows.Count > 0)
                    {
                        if (dde.Rows[0]["SheetName"].ToString().Trim() != "" || dde.Rows[0]["SheetName"].ToString().Trim() != null)
                        {
                            price = dde.Rows[0]["SheetName"].ToString();
                        }

                    }
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    //dataGridView1.DataSource = objDataset1.Tables[0].DefaultView;
                    objConn.Close();

                    SqlCommand command = new SqlCommand();
                    command.Connection = MyConnection;
                    command.CommandText = "DELETE fROM [AveragePrice] WHERE Date='" +
                        objDataset1.Tables[0].Columns[0].ColumnName + "'";
                    command.ExecuteNonQuery();

                    //Insert into DB (AveragePrice)
                    MyCom.Parameters.Add("@date1", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                    MyCom.Parameters.Add("@Pmin", SqlDbType.Int);
                    MyCom.Parameters.Add("@Pmax", SqlDbType.Int);
                    MyCom.Parameters.Add("@Amin", SqlDbType.Int);
                    MyCom.Parameters.Add("@Amax", SqlDbType.Int);
                    MyCom.Parameters.Add("@Aav", SqlDbType.Int);
                    for (int i = 0; i < 24; i++)
                    {

                        MyCom.CommandText = "INSERT INTO [AveragePrice] (Date,Hour,ProposedMin,ProposedMax,AcceptedMin,AcceptedMax,AcceptedAverage) VALUES (@date1,@hour,@Pmin,@Pmax,@Amin,@Amax,@Aav)";


                        MyCom.Parameters["@date1"].Value = objDataset1.Tables[0].Columns[0].ColumnName;
                        MyCom.Parameters["@hour"].Value = objDataset1.Tables[0].Rows[i + 3][0].ToString();
                        MyCom.Parameters["@Pmin"].Value = objDataset1.Tables[0].Rows[i + 3][1].ToString();
                        MyCom.Parameters["@Pmax"].Value = objDataset1.Tables[0].Rows[i + 3][2].ToString();
                        MyCom.Parameters["@Amin"].Value = objDataset1.Tables[0].Rows[i + 3][3].ToString();
                        MyCom.Parameters["@Amax"].Value = objDataset1.Tables[0].Rows[i + 3][4].ToString();
                        MyCom.Parameters["@Aav"].Value = objDataset1.Tables[0].Rows[i + 3][5].ToString();
                        try
                        {
                            MyCom.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            if (messages == "")
                            {
                                messages += "Error In  Date  :" + date + "\r\n";
                            }

                            string str = exp.Message;
                        }

                    }
                    System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                }

                else
                {
                    AddNotExistMessage(path +" In  Date "+date);
                }
                ////////////////////////////////////////////////////////
            }
            catch
            {
                AddNotExistMessage("ChartPrice");
            }
        }
        public void Lfc(string date)
        {
            try
            {

                ////////////////////////FOR 3 DAYS LATER///////////////////////////////////
                DateTime dd = PersianDateConverter.ToGregorianDateTime(date);
                dd = dd.AddDays(3);
                string estim = new PersianDate(dd).Day.ToString();
                if (estim.Length != 2)
                {
                    estim = "0" + estim;
                }
                //////////////////////////////////////////////////////////////////////////////


                string path1 = Auto_Update_Library.BaseData.GetInstance().LoadForecasting;
                string TempDate = date;
                int Tempday = int.Parse(TempDate.Substring(8));
                int TempMonth = int.Parse(TempDate.Substring(5, 2));
                int Tempyear = int.Parse(TempDate.Substring(0, 4));
                string Stempday = "", Stempmonth = "";

                Stempday = Tempday.ToString();

                Stempmonth = TempMonth.ToString();
               //old path1 = path1 + @"\" + Tempyear.ToString() + @"\" + Stempmonth;

                ///////////////////////////////////////////////////////


                string namedate = date;
                namedate = namedate.Remove(0, 2);
                namedate = namedate.Replace("/", "_");
                SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;

             

                // old path1 += @"\" + "Lfc Bazar  " + namedate + ".xls";
               

                /////////////////////////////////////////////////////////////////
                string pathname = folderFormat(date, "", "LoadForecasting");
                if (pathname != "") path1 += pathname;
                string makename = NameFormat(date, "", "", "LoadForecasting");

                ///////////////////////IF 3 DAYS LATER//////////////////////////////
                if (makename.Contains(TempDate.Substring(8) + "_" + TempDate.Substring(8)))
                {
                    makename = makename.Replace(TempDate.Substring(8) + "_" + TempDate.Substring(8), TempDate.Substring(8) + "_" + estim);
                }
                ///////////////////////////////////////////////////////////////////

                if (makename != "") path1 = path1 + "\\" + makename;
                ///////////////////////////////////////////////////////////////////

               


                ////////////////////////// max version2////////////////////////////
                string finalpath = "";

                if (File.Exists(path1.Remove(path1.Length - 1) + ".xls"))
                    finalpath = path1.Remove(path1.Length - 1);

                if (File.Exists(path1 + ".xls"))
                    finalpath = path1;
                if (File.Exists(path1 + "1.xls"))
                    finalpath = path1 + "1";
                if (File.Exists(path1 + "2.xls"))
                    finalpath = path1 + "2";
                if (File.Exists(path1 + "3.xls"))
                    finalpath = path1 + "3";
                if (File.Exists(path1 + "4.xls"))
                    finalpath = path1 + "4";
                if (File.Exists(path1 + "5.xls"))
                    finalpath = path1 + "5";
                if (File.Exists(path1 + "6.xls"))
                    finalpath = path1 + "6";
                if (File.Exists(path1 + "7.xls"))
                    finalpath = path1 + "7";
                if (File.Exists(path1 + "8.xls"))
                    finalpath = path1 + "8";
                if (File.Exists(path1 + "9.xls"))
                    finalpath = path1 + "9";
                if (File.Exists(path1 + "10.xls"))
                    finalpath = path1 + "10";

                finalpath = finalpath + ".xls";

                path1 = finalpath;
                ///////////////////////////////////////////////////////////////////

                if (path1 != "" && File.Exists(path1))
                {
                    //Save AS LoadForecasting.xls file
                    Excel.Application exobj = new Excel.Application();
                    exobj.Visible = false;
                    exobj.UserControl = true;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    Excel.Workbook book = null;
                    book = exobj.Workbooks.Open(path1, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book.Save();
                    book.Close(true, book, Type.Missing);

                    //read from LoadForecasting.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path1 + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "Lfoc";

                    DataTable dde = Utilities.GetTable("select * from FilesNameFormat where filename='LoadForecasting'");
                    if (dde.Rows.Count > 0)
                    {
                        if (dde.Rows[0]["SheetName"].ToString().Trim() != "" || dde.Rows[0]["SheetName"].ToString().Trim() != null)
                        {
                            price = dde.Rows[0]["SheetName"].ToString();
                        }

                    }
                    
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    //objDataset1.Tables[0].DataSource = objDataset1.Tables[0];
                    objConn.Close();

                    //read from LoadForecasting.xls into strings
                    string edate = "";
                    string[] date1 = new string[4];
                    book = exobj.Workbooks.Open(path1, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if (workSheet.Name == price)
                        {
                            edate = ((Excel.Range)workSheet.Cells[3, 4]).Value2.ToString();
                            date1[0] = ((Excel.Range)workSheet.Cells[4, 3]).Value2.ToString();
                            date1[1] = ((Excel.Range)workSheet.Cells[4, 4]).Value2.ToString();
                            date1[2] = ((Excel.Range)workSheet.Cells[4, 5]).Value2.ToString();
                            date1[3] = ((Excel.Range)workSheet.Cells[4, 6]).Value2.ToString();
                        }
                    book.Close(false, book, Type.Missing);
                    exobj.Workbooks.Close();
                    exobj.Quit();
                    //Insert into DB (LoadForecasting)
                    MyCom.Parameters.Add("@date11", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@edate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@peak", SqlDbType.Real);
                    MyCom.Parameters.Add("@h1", SqlDbType.Real);
                    MyCom.Parameters.Add("@h2", SqlDbType.Real);
                    MyCom.Parameters.Add("@h3", SqlDbType.Real);
                    MyCom.Parameters.Add("@h4", SqlDbType.Real);
                    MyCom.Parameters.Add("@h5", SqlDbType.Real);
                    MyCom.Parameters.Add("@h6", SqlDbType.Real);
                    MyCom.Parameters.Add("@h7", SqlDbType.Real);
                    MyCom.Parameters.Add("@h8", SqlDbType.Real);
                    MyCom.Parameters.Add("@h9", SqlDbType.Real);
                    MyCom.Parameters.Add("@h10", SqlDbType.Real);
                    MyCom.Parameters.Add("@h11", SqlDbType.Real);
                    MyCom.Parameters.Add("@h12", SqlDbType.Real);
                    MyCom.Parameters.Add("@h13", SqlDbType.Real);
                    MyCom.Parameters.Add("@h14", SqlDbType.Real);
                    MyCom.Parameters.Add("@h15", SqlDbType.Real);
                    MyCom.Parameters.Add("@h16", SqlDbType.Real);
                    MyCom.Parameters.Add("@h17", SqlDbType.Real);
                    MyCom.Parameters.Add("@h18", SqlDbType.Real);
                    MyCom.Parameters.Add("@h19", SqlDbType.Real);
                    MyCom.Parameters.Add("@h20", SqlDbType.Real);
                    MyCom.Parameters.Add("@h21", SqlDbType.Real);
                    MyCom.Parameters.Add("@h22", SqlDbType.Real);
                    MyCom.Parameters.Add("@h23", SqlDbType.Real);
                    MyCom.Parameters.Add("@h24", SqlDbType.Real);

                    for (int I = 0; I < 4; I++)
                    {

                        SqlCommand command = new SqlCommand();
                        command.Connection = MyConnection;
                        command.CommandText = "DELETE FROM [LoadForecasting] WHERE DateEstimate ='" + edate + "' AND Date='" + date1[I] + "'";
                        command.ExecuteNonQuery();
                    }

                    for (int z = 0; z < 4; z++)
                    {
                        MyCom.CommandText = "INSERT INTO [LoadForecasting] (Date,DateEstimate,Peak,Hour1,Hour2,Hour3,Hour4," +
                        "Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19" +
                        ",Hour20,Hour21,Hour22,Hour23,Hour24) VALUES (@date11,@edate,@peak,@h1,@h2,@h3,@h4,@h5,@h6,@h7,@h8" +
                        ",@h9,@h10,@h11,@h12,@h13,@h14,@h15,@h16,@h17,@h18,@h19,@h20,@h21,@h22,@h23,@h24)";

                        MyCom.Parameters["@date11"].Value = date1[z];
                        MyCom.Parameters["@edate"].Value = edate;

                        MyCom.Parameters["@peak"].Value = objDataset1.Tables[0].Rows[28][2 + z].ToString();
                        MyCom.Parameters["@h1"].Value = objDataset1.Tables[0].Rows[4][2 + z].ToString();
                        MyCom.Parameters["@h2"].Value = objDataset1.Tables[0].Rows[5][2 + z].ToString();
                        MyCom.Parameters["@h3"].Value = objDataset1.Tables[0].Rows[6][2 + z].ToString();
                        MyCom.Parameters["@h4"].Value = objDataset1.Tables[0].Rows[7][2 + z].ToString();
                        MyCom.Parameters["@h5"].Value = objDataset1.Tables[0].Rows[8][2 + z].ToString();
                        MyCom.Parameters["@h6"].Value = objDataset1.Tables[0].Rows[9][2 + z].ToString();
                        MyCom.Parameters["@h7"].Value = objDataset1.Tables[0].Rows[10][2 + z].ToString();
                        MyCom.Parameters["@h8"].Value = objDataset1.Tables[0].Rows[11][2 + z].ToString();
                        MyCom.Parameters["@h9"].Value = objDataset1.Tables[0].Rows[12][2 + z].ToString();
                        MyCom.Parameters["@h10"].Value = objDataset1.Tables[0].Rows[13][2 + z].ToString();
                        MyCom.Parameters["@h11"].Value = objDataset1.Tables[0].Rows[14][2 + z].ToString();
                        MyCom.Parameters["@h12"].Value = objDataset1.Tables[0].Rows[15][2 + z].ToString();
                        MyCom.Parameters["@h13"].Value = objDataset1.Tables[0].Rows[16][2 + z].ToString();
                        MyCom.Parameters["@h14"].Value = objDataset1.Tables[0].Rows[17][2 + z].ToString();
                        MyCom.Parameters["@h15"].Value = objDataset1.Tables[0].Rows[18][2 + z].ToString();
                        MyCom.Parameters["@h16"].Value = objDataset1.Tables[0].Rows[19][2 + z].ToString();
                        MyCom.Parameters["@h17"].Value = objDataset1.Tables[0].Rows[20][2 + z].ToString();
                        MyCom.Parameters["@h18"].Value = objDataset1.Tables[0].Rows[21][2 + z].ToString();
                        MyCom.Parameters["@h19"].Value = objDataset1.Tables[0].Rows[22][2 + z].ToString();
                        MyCom.Parameters["@h20"].Value = objDataset1.Tables[0].Rows[23][2 + z].ToString();
                        MyCom.Parameters["@h21"].Value = objDataset1.Tables[0].Rows[24][2 + z].ToString();
                        MyCom.Parameters["@h22"].Value = objDataset1.Tables[0].Rows[25][2 + z].ToString();
                        MyCom.Parameters["@h23"].Value = objDataset1.Tables[0].Rows[26][2 + z].ToString();
                        MyCom.Parameters["@h24"].Value = objDataset1.Tables[0].Rows[27][2 + z].ToString();
                        try
                        {
                            MyCom.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            if (messages == "")
                            {
                                messages += "Error In  Date  :" + date + "\r\n";
                            }

                            string str = exp.Message;
                        }
                    }
                    // System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                }
                else
                {
                    AddNotExistMessage(path1+"error in Date :"+date);
                }
                /////////////////////////////////////////////////////////
            }
            catch
            {
                AddNotExistMessage("Lfc");
            }
        }
                
        //public void M002Dispatch1sheet(string date, string PPID, bool PPtype)
        //{
        //    bool IsValid = true;
        //    string path = ReadDispatchExcel("Dispatchable", date, PPID, PPtype);
        //    SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;
        //    string sheetName = "Sheet1";

        //    if (PPID == "206") sheetName = "اصلاحیه فردا";

        //    //read from FRM002.xls into dataTable
        //    DataTable DT = null;
        //    ////////////////////////// max version////////////////////////////
        //    try
        //    {
        //        string pplant = "";

        //        if(path.Contains("TARGET"))
        //        {

        //        string[] thispath = path.Split('_');

        //        pplant = thispath[1];

        //        }

        //        else if(path.Contains("LOSHAN"))
        //        {

        //            pplant = "LOSHAN";
        //        }

        //        int msx = 0;

        //        string hard = BaseData.GetInstance().Dispatchable;
        //        string[] s = date.Split('/');
        //        string y = s[0];
        //        string m = s[1];
        //        string d = s[2];
        //        if (int.Parse(m) < 10) m = m.Remove(0, 1);
        //        if (int.Parse(d) < 10) d = d.Remove(0, 1);
        //        string temp;
        //        string[] arrtemp;

               
        //        string[] arraypath = Directory.GetFiles(hard + @"\" + y + @"\" + m + @"\" + d);
        //        if (arraypath.Length > 0)
        //        {
        //            for (int i = 0; i < arraypath.Length; i++)
        //            {
        //                if (arraypath[i].Contains(pplant))
        //                {
        //                    switch (pplant)
        //                    {
        //                        case "LOSHAN":
        //                            temp = arraypath[i];
        //                            arrtemp = temp.Split('_');


        //                            if (arrtemp.Length == 2)
        //                            {
        //                                //if (msx < int.Parse(arraypath[i].Substring(arraypath[i].Length - 5, 1)))
        //                                if (msx < int.Parse(arrtemp[1].Substring(0, 1)))
        //                                {
        //                                    msx = int.Parse(arrtemp[1].Substring(0, 1));
        //                                    path = arraypath[i];
        //                                }
        //                            }
        //                            else
        //                            {
        //                                path = path + ".xls";
        //                            }

        //                            break;

        //                        case "232":
        //                             temp = arraypath[i];
        //                             arrtemp = temp.Split('_');


        //                            if (arrtemp.Length == 4)
        //                            {
        //                                //if (msx < int.Parse(arraypath[i].Substring(arraypath[i].Length - 5, 1)))
        //                                if (msx < int.Parse(arrtemp[3].Substring(0, 1)))
        //                                {
        //                                    msx = int.Parse(arrtemp[3].Substring(0, 1));
        //                                    path = arraypath[i];
        //                                }
        //                            }
        //                            else
        //                            {
        //                                path = path + ".xls";
        //                            }
        //                            break;

        //                    }

                           
        //                }

        //            }
        //        }
        //    }
        //    catch(Exception et)
        //    {
        //      MessageBox.Show(et.Message);

        //    }
        //    //////////////////////////////////end max version/////////////////////////////////////
        //    if (path != "" && File.Exists(path))
        //    {
        //        String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
        //        OleDbConnection objConn = new OleDbConnection(sConnectionString);
        //        objConn.Open();
        //        OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + sheetName + "$]", objConn);
        //        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
        //        objAdapter1.SelectCommand = objCmdSelect;
        //        DataSet objDataset1 = new DataSet();
        //        try
        //        {
        //            objAdapter1.Fill(objDataset1);
        //            DT = objDataset1.Tables[0];
        //            //dataGridView1.DataSource = objDataset1.Tables[0].DefaultView;
        //        }
        //        catch (Exception e)
        //        {
        //            DT = null;
        //        }
        //        objConn.Close();

        //    }
        //    else
        //    {
        //        AddNotExistMessage(path);
        //    }

        //    if (DT == null)
        //        IsValid = false;

        //    //IS IT A Valid File?
        //    //if ((IsValid) && (dataGridView1.Columns[1].HeaderText.Contains("M002")))
        //    if (IsValid)
        //    //&& (DT.Columns[1].ColumnName.Contains("M002")))
        //    {
        //        SqlCommand MyCom = new SqlCommand();

        //        //Insert into DB (MainFRM002)
        //        Excel.Application exobj = new Excel.Application();
        //        exobj.Visible = false;
        //        exobj.UserControl = true;
        //        Excel.Workbook book = null;
        //        System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
        //        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
        //        book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

        //        //SqlCommand MyCom = new SqlCommand();
        //        MyCom.Connection = MyConnection;
        //        int type = 0;
        //        int PID = 0;
        //        if (PPtype) type = 1;
        //        PID = Convert.ToInt32(PPID);


        //        try
        //        {

        //            DataTable deltable = null;
        //            deltable = utilities.GetTable("Delete From [Dispathable] where PPID='" + PID + "' AND StartDate='" + date + "' AND PackageType='" + type + "'");


        //        }
        //        catch (Exception exp)
        //        {
        //            string str = exp.Message;
        //        }


        //        //Insert into DB (BlockFRM002)
        //        int x = 10;
        //        MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);


        //        //Insert into DB (DetailFRM002)
        //        x = 10;
        //        MyCom.Parameters.Add("@deccap", SqlDbType.Real);
        //        MyCom.Parameters.Add("@dispachcap", SqlDbType.Real);
        //        MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
        //        MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
        //        MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
        //        MyCom.Parameters.Add("@type", SqlDbType.SmallInt);

        //        //read directly and cell by cell
        //        foreach (Excel.Worksheet workSheet in book.Worksheets)
        //            if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1") || (workSheet.Name == sheetName))
        //            {
        //                while (x < (DT.Rows.Count - 2))
        //                {
        //                    if (DT.Rows[x][0].ToString() != "")
        //                    {

        //                        for (int j = 0; j < 24; j++)
        //                        {
        //                            MyCom.CommandText = "INSERT INTO [Dispathable] (StartDate,PPID,Block,PackageType,Hour" +
        //                            ",DeclaredCapacity,DispachableCapacity) VALUES (@tdate,@id,@block,@type,@hour,@deccap,@dispachcap)";

        //                            MyCom.Parameters["@id"].Value = PID;
        //                            MyCom.Parameters["@tdate"].Value = date;

        //                            if (((Excel.Range)workSheet.Cells[x + 2, 1]).Value2 != null)
        //                            {
        //                                string blockcell = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString();
                                     
        //                                if (blockcell.Contains("CC"))
        //                                {
        //                                    blockcell = blockcell.Replace("CC", "C");

        //                                }
        //                                MyCom.Parameters["@block"].Value = blockcell;
        //                            }
        //                            else MyCom.Parameters["@block"].Value = "";




        //                            MyCom.Parameters["@type"].Value = type;
        //                            MyCom.Parameters["@hour"].Value = j + 1;
        //                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 5]).Value2 != null)
        //                                MyCom.Parameters["@deccap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 5]).Value2.ToString();
        //                            else MyCom.Parameters["@deccap"].Value = 0;
        //                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2 != null)
        //                                MyCom.Parameters["@dispachcap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2.ToString();
        //                            else MyCom.Parameters["@dispachcap"].Value = 0;

        //                            try
        //                            {
        //                                MyCom.ExecuteNonQuery();
        //                            }
        //                            catch (Exception exp)
        //                            {
        //                                string str = exp.Message;
        //                            }
        //                        }
        //                    }
        //                    x++;
        //                }
        //            }

        //        book.Close(false, book, Type.Missing);
        //        exobj.Workbooks.Close();
        //        exobj.Quit();
        //        System.Threading.Thread.CurrentThread.CurrentCulture = oldci;

        //    }

        //    if (Auto_Update_Library.BaseData.GetInstance().Useftpdispatch)
        //    {
        //        if (path != "" && File.Exists(path + ".xls"))
        //        {

        //            File.Delete(path + ".xls");
        //        }

        //    }



        //}
        //private string ReadDispatchExcel1sheet(string item, string date, string PPID, bool combinedCycle)
        //{
        //    bool isftp = false;
        //     DataTable result = null;
        //    string path = "";
        //    string DocName = "";

        //    if (!BaseData.GetInstance().Useftpdispatch)
        //    {
        //        isftp = false;
        //    switch (item)
        //    {
        //        case "Dispatchable":
        //            if (PPID == "232")
        //            {
        //                DocName = "TARGET_232";
        //            }
        //            if (PPID == "206")
        //            {

        //                DocName = "LOSHAN ";

        //            }

        //           // DocName = "FRM0022_";
        //            try
        //            {
        //                //int ID = 0;
        //                //ID = int.Parse(PPID);
        //                //if (combinedCycle)
        //                //    ID++;
        //                //DocName += ID.ToString();
        //            }
        //            catch (Exception e)
        //            {

        //            }
        //            //add date to Name
        //            string mydate = date;
        //            mydate = mydate.Replace("/", "");

        //            if (PPID == "232")
        //            {
        //                mydate = mydate.Remove(0, 2);
        //                DocName += "_" + mydate;
        //            }
        //           else if (PPID == "206")
        //            {
                       
        //                DocName +=  mydate;
        //            }



        //            path = BaseData.GetInstance().Dispatchable;
        //            break;

       
        //        }
        //    }
            
        //    else if (BaseData.GetInstance().Useftpdispatch)
        //    {
        //        isftp = true;
        //        string endid = "";
        //        string Sendmonth = "";
        //        string Sendyear = "";
        //        string Sendday = "";
              
        //        string endDate = date;
        //        int endday = int.Parse(endDate.Substring(8));
        //        int endMonth = int.Parse(endDate.Substring(5, 2));
        //        int endyear = int.Parse(endDate.Substring(0, 4));
        //        Sendmonth = endMonth.ToString();
        //        Sendyear = endyear.ToString();
        //        Sendday = endday.ToString();

        //        switch (item)
        //        {
        //            case "Dispatchable":
        //                DocName = "FRM0022_";
        //                try
        //                {
        //                    int ID = 0;
        //                    ID = int.Parse(PPID);
        //                    if (combinedCycle)
        //                        ID++;
        //                    DocName += ID.ToString();
        //                    endid = ID.ToString();
        //                }
        //                catch (Exception e)
        //                {

        //                }
        //                //add date to Name
        //                string mydate = date;
        //                mydate = mydate.Replace("/", "");
        //                DocName += "_" + mydate;

        //                switch (endid)
        //                {
        //                    case "206":
        //                        endid = "Lowshan";
        //                        break;

        //                    case "232":
        //                        endid = "Gilan";
        //                        break;


        //                }

        //                switch (Sendmonth)
        //                {
        //                    case "1":
        //                        Sendmonth = "Farvardin";
        //                        break;
        //                    case "2":
        //                        Sendmonth = "Ordibehesht";
        //                        break;
        //                    case "3":
        //                        Sendmonth = "Khordad";
        //                        break;
        //                    case "4":
        //                        Sendmonth = "Tir";
        //                        break;
        //                    case "5":
        //                        Sendmonth = "Mordad";
        //                        break;
        //                    case "6":
        //                        Sendmonth = "Shahrivar";
        //                        break;
        //                    case "7":
        //                        Sendmonth = "Mehr";
        //                        break;
        //                    case "8":
        //                        Sendmonth = "Aban";
        //                        break;
        //                    case "9":
        //                        Sendmonth = "Azar";
        //                        break;
        //                    case "10":
        //                        Sendmonth = "Day";
        //                        break;
        //                    case "11":
        //                        Sendmonth = "Bahman";
        //                        break;
        //                    case "12":
        //                        Sendmonth = "Esfand";
        //                        break;

        //                }


        //                path = "ftp://" + ftpserverip + "/" + BaseData.GetInstance().Dispatchable + "/";
        //                path += endid + @"/" + endyear + @"/" + Sendmonth + @"/" + Sendday + @"/";

        //                break;
                                                                
        //        }
                
        //    }
        //    string TempDate = date;
        //    int Tempday = int.Parse(TempDate.Substring(8));
        //    int TempMonth = int.Parse(TempDate.Substring(5, 2));
        //    int Tempyear = int.Parse(TempDate.Substring(0, 4));
        //    string Stempday = "", Stempmonth = "";
        //    //if (Tempday < 10) Stempday = "0" + Tempday.ToString(); else 
        //    Stempday = Tempday.ToString();
        //    ////if (TempMonth < 10) Stempmonth = "0" + TempMonth.ToString(); else 
        //    Stempmonth = TempMonth.ToString();

        //    //bool isftp = BaseData.GetInstance().Useftp;


        //    if (!isftp)
        //    {
        //        //path = path + @"\" + Tempyear.ToString() + @"\" + Stempmonth + @"\" + Stempday + @"\" + DocName;
        //        path = path + @"\" + Tempyear.ToString() + @"\" + Stempmonth +  @"\" + DocName;

        //    }
        //    else if (isftp)
        //    {

        //        path = path + DocName;
                                
        //        //////////////////////////third version////////////////////////////////////
               
        //        /////////////////////////////////////////////////////////////////////////
                
        //       bool Success= FtpDownload(DocName + ".xls", path + ".xls", ftpusername, ftppassword);
        //       if (Success)
        //       {
        //           path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
        //            "\\NRI\\SBS" + "\\" + DocName;
        //       }
        //       else
        //       {
        //           path = "";

        //       }
              

        //    }

        //    return path;
        //}
        private string ReadDispatchExcel(string item, string date, string PPID, bool combinedCycle)
        {
            bool isftp = false;
            DataTable result = null;
            string path = "";
            string DocName = "";

            if (!BaseData.GetInstance().Useftpdispatch)
            {
                isftp = false;
                switch (item)
                {
                    case "Dispatchable":
                       
                                                   

                        
                        try
                        {
                            path = BaseData.GetInstance().Dispatchable;
                            int ID1 = 0;
                            ID1 = int.Parse(PPID);
                            if (combinedCycle)
                                ID1++;
                            string pathname = folderFormat(date, PPID, "Dispatch");
                            if (pathname != "") path += pathname;
                            string makename = NameFormat(date, ID1.ToString(), "", "Dispatch");
                            if (makename != "") path = path + "\\" + makename;
                        }
                        catch (Exception e)
                        {

                        }
                        //add date to Name
                        //string mydate = date;
                        //mydate = mydate.Replace("/", "");

                        //    DocName +=mydate;
                      



                      
                        break;


                }
            }

            else if (BaseData.GetInstance().Useftpdispatch)
            {
                isftp = true;
                string endid = "";
                string ENDIDPATH = "";
                string Sendmonth = "";
                string Sendyear = "";
                string Sendday = "";

                string endDate = date;
                int endday = int.Parse(endDate.Substring(8));
                int endMonth = int.Parse(endDate.Substring(5, 2));
                int endyear = int.Parse(endDate.Substring(0, 4));
                Sendmonth = endMonth.ToString();
                Sendyear = endyear.ToString();
                Sendday = endday.ToString();

                switch (item)
                {
                    case "Dispatchable":
                       
                        try
                        {
                            int ID = 0;
                            ID = int.Parse(PPID);
                            if (combinedCycle)
                                ID++;
                                                        
                            endid = ID.ToString();
                            switch (endid)
                            {
                                case "206":
                                    endid = "LOSHAN";
                                    ENDIDPATH = "Lowshan";
                                    break;

                                case "232":
                                    endid = "GILAN";
                                    ENDIDPATH = "Gilan";
                                    break;

                            }

                            DocName += endid.ToString();
                           
                        }
                        catch (Exception e)
                        {

                        }
                        //add date to Name
                        string mydate = date;
                        mydate = mydate.Replace("/", "");
                        DocName += "_" + mydate;

                       
                        //switch (Sendmonth)
                        //{
                        //    case "1":
                        //        Sendmonth = "Farvardin";
                        //        break;
                        //    case "2":
                        //        Sendmonth = "Ordibehesht";
                        //        break;
                        //    case "3":
                        //        Sendmonth = "Khordad";
                        //        break;
                        //    case "4":
                        //        Sendmonth = "Tir";
                        //        break;
                        //    case "5":
                        //        Sendmonth = "Mordad";
                        //        break;
                        //    case "6":
                        //        Sendmonth = "Shahrivar";
                        //        break;
                        //    case "7":
                        //        Sendmonth = "Mehr";
                        //        break;
                        //    case "8":
                        //        Sendmonth = "Aban";
                        //        break;
                        //    case "9":
                        //        Sendmonth = "Azar";
                        //        break;
                        //    case "10":
                        //        Sendmonth = "Day";
                        //        break;
                        //    case "11":
                        //        Sendmonth = "Bahman";
                        //        break;
                        //    case "12":
                        //        Sendmonth = "Esfand";
                        //        break;

                        //}


                        path = "ftp://" + ftpserverip + "/" + BaseData.GetInstance().Dispatchable + "/";
                        path += ENDIDPATH + @"/" + endyear + @"/" + Sendmonth + @"/";

                        break;

                }

            }
            string TempDate = date;
            int Tempday = int.Parse(TempDate.Substring(8));
            int TempMonth = int.Parse(TempDate.Substring(5, 2));
            int Tempyear = int.Parse(TempDate.Substring(0, 4));
            string Stempday = "", Stempmonth = "";
            //if (Tempday < 10) Stempday = "0" + Tempday.ToString(); else 
            Stempday = Tempday.ToString();
            ////if (TempMonth < 10) Stempmonth = "0" + TempMonth.ToString(); else 
            Stempmonth = TempMonth.ToString();

            //bool isftp = BaseData.GetInstance().Useftp;


            if (!isftp)
            {
                //path = path + @"\" + Tempyear.ToString() + @"\" + Stempmonth + @"\" + Stempday + @"\" + DocName;
               // path = path + @"\" + Tempyear.ToString() + @"\" + Stempmonth + @"\" + DocName;

            }
            else if (isftp)
            {
                string VER = "";
                string finalpath = "";
                path = path + DocName;

                //////////////////////////third version////////////////////////////////////
                if (ftpFileExist(path + ".xls"))
                    finalpath = path + ".xls";

                if (ftpFileExist(path + "_10.xls"))
                {
                    finalpath = path + "_10.xls";
                    VER = "_10";
                }
                else if (ftpFileExist(path + "_9.xls"))
                {
                    finalpath = path + "_9.xls";
                    VER = "_9";
                }
               
                else if (ftpFileExist(path + "_8.xls"))
                {
                    finalpath = path + "_8.xls";
                    VER = "_8";
                }
                else if (ftpFileExist(path + "_7.xls"))
                {
                    finalpath = path + "_7.xls";
                    VER = "_7";
                }
                else if (ftpFileExist(path + "_6.xls"))
                {
                    finalpath = path + "_6.xls";
                    VER = "_6";
                }
                else if (ftpFileExist(path + "_5.xls"))
                {
                    finalpath = path + "_5.xls";
                    VER = "_5";
                }
                else if (ftpFileExist(path + "_4.xls"))
                {
                    finalpath = path + "_4.xls";
                    VER = "_4";
                }
                else if (ftpFileExist(path + "_3.xls"))
                {
                    finalpath = path + "_3.xls";
                    VER = "_3";
                }
                else if (ftpFileExist(path + "_2.xls"))
                {
                    finalpath = path + "_2.xls";
                    VER = "_2";
                }
                else if (ftpFileExist(path + "_1.xls"))
                {
                    finalpath = path + "_1.xls";
                    VER = "_1";
                }
                else if (ftpFileExist(path + "_0.xls"))
                {
                    finalpath = path + "_0.xls";
                    VER = "_0";
                }
                /////////////////////////////////////////////////////////////////////////

                bool Success = FtpDownload((DocName + VER) + ".xls", finalpath , ftpusername, ftppassword);
                if (Success)
                {
                    path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) +
                     "\\NRI\\SBS" + "\\" + (DocName + VER);
                }
                else
                {
                    path = "";

                }


            }

            return path;
        }
        public void M002Dispatch(string date, string PPID, bool PPtype)
        {
            bool IsValid = true;
            string path = ReadDispatchExcel("Dispatchable", date, PPID, PPtype);
            SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;
            string sheetName = "Sheet1";
            DataTable dde = Utilities.GetTable("select * from FilesNameFormat where filename='Dispatch'");
            if (dde.Rows.Count > 0)
            {
                if (dde.Rows[0]["SheetName"].ToString().Trim() != "" || dde.Rows[0]["SheetName"].ToString().Trim() != null)
                {
                    sheetName = dde.Rows[0]["SheetName"].ToString();
                    if (sheetName == "PlantName")
                    {
                       
                        string english = "";
                        DataTable vv = Utilities.GetTable("select * from powerplant where ppid='" + PPID + "'");
                        if (vv.Rows.Count > 0)
                        {                            
                            english = vv.Rows[0]["PPName"].ToString().Trim();
                            sheetName = english;
                        }
                    }
                    else if (sheetName == "PlantID")
                    {
                        sheetName = PPID;

                    }
                }

            }


            //if (path.Contains("Program Files")) sheetName = "اصلاحیه فردا";
            //else if (PPID == "206") sheetName = "LOSHAN";
            //else if (PPID == "232") sheetName = "GILAN";


            //read from FRM002.xls into dataTable
            DataTable DT = null;
            ////////////////////////// max version////////////////////////////
            string finalpath = "";

            if (File.Exists(path.Remove(path.Length - 1) + ".xls"))
                finalpath = path.Remove(path.Length - 1);

            if (File.Exists(path + ".xls"))
                finalpath = path;
            if (File.Exists(path + "1.xls"))
                finalpath = path + "1";
            if (File.Exists(path + "2.xls"))
                finalpath = path + "2";
            if (File.Exists(path + "3.xls"))
                finalpath = path + "3";
            if (File.Exists(path + "4.xls"))
                finalpath = path + "4";
            if (File.Exists(path + "5.xls"))
                finalpath = path + "5";
            if (File.Exists(path + "6.xls"))
                finalpath = path + "6";
            if (File.Exists(path + "7.xls"))
                finalpath = path + "7";
            if (File.Exists(path + "8.xls"))
                finalpath = path + "8";
            if (File.Exists(path + "9.xls"))
                finalpath = path + "9";
            if (File.Exists(path + "10.xls"))
                finalpath = path + "10";

            finalpath = finalpath + ".xls";
            /////////////////////////////add one day ///////////////////////////////////

            DateTime dmiladi = PersianDateConverter.ToGregorianDateTime(date);

            if (path.Contains("Program Files"))
            {
                date = new PersianDate(dmiladi.AddDays(1)).ToString("d");
            }


            string date2 = new PersianDate(dmiladi.AddDays(2)).ToString("d");
            string date3 = new PersianDate(dmiladi.AddDays(3)).ToString("d");
                        
            //////////////////////////////////end max version/////////////////////////////////////
            if (finalpath != "" && File.Exists(finalpath))
            {
                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + finalpath + ";Extended Properties=Excel 8.0";
                OleDbConnection objConn = new OleDbConnection(sConnectionString);
                objConn.Open();
                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + sheetName + "$]", objConn);
                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                objAdapter1.SelectCommand = objCmdSelect;
                DataSet objDataset1 = new DataSet();
                try
                {
                    objAdapter1.Fill(objDataset1);
                    DT = objDataset1.Tables[0];
                    //dataGridView1.DataSource = objDataset1.Tables[0].DefaultView;
                }
                catch (Exception e)
                {
                    DT = null;
                    AddNotExistMessage(path);
                }
                objConn.Close();

            }
            else
            {
                AddNotExistMessage(path);
            }

            if (DT == null)
                IsValid = false;

            //IS IT A Valid File?
            //if ((IsValid) && (dataGridView1.Columns[1].HeaderText.Contains("M002")))
            if (IsValid)
            //&& (DT.Columns[1].ColumnName.Contains("M002")))
            {
                SqlCommand MyCom = new SqlCommand();

                //Insert into DB (MainFRM002)
                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                Excel.Workbook book = null;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                book = exobj.Workbooks.Open(finalpath, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                //SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                int type = 0;
                int PID = 0;
                if (PPtype) type = 1;
                PID = Convert.ToInt32(PPID);


                
                string timefill=new PersianDate(DateTime.Now).ToString("d")+"|"+DateTime.Now.Hour+":"+DateTime.Now.Minute ;

                //Insert into DB (BlockFRM002)
                int x = 10;
                MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);


                //Insert into DB (DetailFRM002)
                x = 10;
                MyCom.Parameters.Add("@deccap", SqlDbType.Real);
                MyCom.Parameters.Add("@dispachcap", SqlDbType.Real);
                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@type", SqlDbType.SmallInt);

                //read directly and cell by cell
                foreach (Excel.Worksheet workSheet in book.Worksheets)
                {
                    if ((workSheet.Name == sheetName))
                    {
                        try
                        {

                            DataTable deltable = null;
                            deltable = Utilities.GetTable("Delete From [Dispathable] where PPID='" + PID + "' AND StartDate='" + date + "' AND PackageType='" + type + "'");


                        }
                        catch (Exception exp)
                        {
                            if (messages == "")
                            {
                                messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                            }

                            string str = exp.Message;
                        }


                        x = 10;
                        while (x < (DT.Rows.Count - 2))
                        {
                            if (DT.Rows[x][0].ToString() != "")
                            {

                                for (int j = 0; j < 24; j++)
                                {
                                    MyCom.CommandText = "INSERT INTO [Dispathable] (StartDate,PPID,Block,PackageType,Hour" +
                                    ",DeclaredCapacity,DispachableCapacity,FilledBy,TimeFilled) VALUES (@tdate,@id,@block,@type,@hour,@deccap,@dispachcap,'" + "test" + "','" + timefill + "')";

                                    MyCom.Parameters["@id"].Value = PID;
                                    MyCom.Parameters["@tdate"].Value = date;

                                    if (((Excel.Range)workSheet.Cells[x + 2, 1]).Value2 != null)
                                    {
                                        string blockcell = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString();

                                        if (blockcell.Contains("CC"))
                                        {
                                            blockcell = blockcell.Replace("CC", "C");

                                        }
                                        MyCom.Parameters["@block"].Value = blockcell;
                                    }
                                    else MyCom.Parameters["@block"].Value = "";




                                    MyCom.Parameters["@type"].Value = type;
                                    MyCom.Parameters["@hour"].Value = j + 1;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2 != null)
                                        MyCom.Parameters["@deccap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2.ToString();
                                    else MyCom.Parameters["@deccap"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2 != null)
                                        MyCom.Parameters["@dispachcap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2.ToString();
                                    else MyCom.Parameters["@dispachcap"].Value = 0;

                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        if (messages == "")
                                        {
                                            messages += "Error In PPID : " + PPID + " In Date  :" + date + "\r\n";
                                        }

                                        string str = exp.Message;
                                    }
                                }
                            }
                            x++;
                        }
                    }
                    //if (workSheet.Name == "اصلاحیه پس فردا")
                    //{
                    //    try
                    //    {

                    //        DataTable deltable = null;
                    //        deltable = utilities.GetTable("Delete From [Dispathable] where PPID='" + PID + "' AND StartDate='" + date2 + "' AND PackageType='" + type + "'");


                    //    }
                    //    catch (Exception exp)
                    //    {
                    //        string str = exp.Message;
                    //    }

                    //    x = 10;
                    //    while (x < (DT.Rows.Count - 2))
                    //    {
                    //        if (DT.Rows[x][0].ToString() != "")
                    //        {
                                

                    //            for (int j = 0; j < 24; j++)
                    //            {
                    //                MyCom.CommandText = "INSERT INTO [Dispathable] (StartDate,PPID,Block,PackageType,Hour" +
                    //                ",DeclaredCapacity,DispachableCapacity) VALUES (@tdate,@id,@block,@type,@hour,@deccap,@dispachcap)";

                    //                MyCom.Parameters["@id"].Value = PID;
                    //                MyCom.Parameters["@tdate"].Value = date2;

                    //                if (((Excel.Range)workSheet.Cells[x + 2, 1]).Value2 != null)
                    //                {
                    //                    string blockcell = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString();

                    //                    if (blockcell.Contains("CC"))
                    //                    {
                    //                        blockcell = blockcell.Replace("CC", "C");

                    //                    }
                    //                    MyCom.Parameters["@block"].Value = blockcell;
                    //                }
                    //                else MyCom.Parameters["@block"].Value = "";




                    //                MyCom.Parameters["@type"].Value = type;
                    //                MyCom.Parameters["@hour"].Value = j + 1;
                    //                if (((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2 != null)
                    //                    MyCom.Parameters["@deccap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2.ToString();
                    //                else MyCom.Parameters["@deccap"].Value = 0;
                    //                if (((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2 != null)
                    //                    MyCom.Parameters["@dispachcap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2.ToString();
                    //                else MyCom.Parameters["@dispachcap"].Value = 0;

                    //                try
                    //                {
                    //                    MyCom.ExecuteNonQuery();
                    //                }
                    //                catch (Exception exp)
                    //                {
                    //                    string str = exp.Message;
                    //                }
                    //            }
                    //        }
                    //        x++;
                    //    }
                    //}
                    //if (workSheet.Name == "پیش بینی سه روز آینده")
                    //{
                    //    try
                    //    {

                    //        DataTable deltable = null;
                    //        deltable = utilities.GetTable("Delete From [Dispathable] where PPID='" + PID + "' AND StartDate='" + date3 + "' AND PackageType='" + type + "'");


                    //    }
                    //    catch (Exception exp)
                    //    {
                    //        string str = exp.Message;
                    //    }
                    //    x = 10;
                    //    while (x < (DT.Rows.Count - 2))
                    //    {
                    //        if (DT.Rows[x][0].ToString() != "")
                    //        {
                                

                    //            for (int j = 0; j < 24; j++)
                    //            {
                    //                MyCom.CommandText = "INSERT INTO [Dispathable] (StartDate,PPID,Block,PackageType,Hour" +
                    //                ",DeclaredCapacity,DispachableCapacity) VALUES (@tdate,@id,@block,@type,@hour,@deccap,@dispachcap)";

                    //                MyCom.Parameters["@id"].Value = PID;
                    //                MyCom.Parameters["@tdate"].Value = date3;

                    //                if (((Excel.Range)workSheet.Cells[x + 2, 1]).Value2 != null)
                    //                {
                    //                    string blockcell = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString();

                    //                    if (blockcell.Contains("CC"))
                    //                    {
                    //                        blockcell = blockcell.Replace("CC", "C");

                    //                    }
                    //                    MyCom.Parameters["@block"].Value = blockcell;
                    //                }
                    //                else MyCom.Parameters["@block"].Value = "";




                    //                MyCom.Parameters["@type"].Value = type;
                    //                MyCom.Parameters["@hour"].Value = j + 1;
                    //                if (((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2 != null)
                    //                    MyCom.Parameters["@deccap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2.ToString();
                    //                else MyCom.Parameters["@deccap"].Value = 0;
                    //                if (((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2 != null)
                    //                    MyCom.Parameters["@dispachcap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2.ToString();
                    //                else MyCom.Parameters["@dispachcap"].Value = 0;

                    //                try
                    //                {
                    //                    MyCom.ExecuteNonQuery();
                    //                }
                    //                catch (Exception exp)
                    //                {
                    //                    string str = exp.Message;
                    //                }
                    //            }
                    //        }
                    //        x++;
                    //    }
                    //}
                }

                book.Close(false, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();
                System.Threading.Thread.CurrentThread.CurrentCulture = oldci;

            }

            if (Auto_Update_Library.BaseData.GetInstance().Useftpdispatch)
            {
                if (finalpath != "" && File.Exists(finalpath))
                {

                    File.Delete(finalpath);
                }

            }



        }


        public bool ftpFileExist(string fileName)
        {
            WebClient wc = new WebClient();
            try
            {
                wc.Credentials = new NetworkCredential(ftpusername, ftppassword);
                byte[] fData = wc.DownloadData(fileName);
                if (fData.Length > -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception t)
            {
                string uuu = t.Message;
                // Debug here?
            }
            return false;
        }

        public void EnergySaled(string date)
        {

            bool success = true;
            try
            {
                //price13900510.xls
               

                string TempDate = date;
                string Tempday = TempDate.Substring(8);
                string TempMonth = TempDate.Substring(5, 2);
                string Tempyear = TempDate.Substring(0, 4);

                string path = Auto_Update_Library.BaseData.GetInstance().M0091Path;

                //old path = path + @"\" + Tempyear.ToString() + @"\" + int.Parse(TempMonth);
                //old path += @"\" + "price" + Tempyear + TempMonth + Tempday + ".xls";

                ////////////////////////////////////////////////////////////
                          

                string pathname = folderFormat(date,"","SaledEnergy");
                if (pathname != "") path += pathname;                
                string makename = NameFormat(date, "", "", "SaledEnergy");
                if (makename != "") path = path + "\\" + makename ;


                ////////////////////////////////////////////////////////////////////
                ////////////////////////// max version2////////////////////////////
                string finalpath = "";

                if (File.Exists(path.Remove(path.Length - 1) + ".xls"))
                    finalpath = path.Remove(path.Length - 1);

                if (File.Exists(path + ".xls"))
                    finalpath = path;
                if (File.Exists(path + "1.xls"))
                    finalpath = path + "1";
                if (File.Exists(path + "2.xls"))
                    finalpath = path + "2";
                if (File.Exists(path + "3.xls"))
                    finalpath = path + "3";
                if (File.Exists(path + "4.xls"))
                    finalpath = path + "4";
                if (File.Exists(path + "5.xls"))
                    finalpath = path + "5";
                if (File.Exists(path + "6.xls"))
                    finalpath = path + "6";
                if (File.Exists(path + "7.xls"))
                    finalpath = path + "7";
                if (File.Exists(path + "8.xls"))
                    finalpath = path + "8";
                if (File.Exists(path + "9.xls"))
                    finalpath = path + "9";
                if (File.Exists(path + "10.xls"))
                    finalpath = path + "10";

                finalpath = finalpath + ".xls";

                path = finalpath;
                ///////////////////////////////////////////////////////////////////



                /////////////////////////////////////////////////////////////////////
                if (File.Exists(path))
                {
                    Excel.Application exobj1 = new Excel.Application();
                    exobj1.Visible = false;
                    exobj1.UserControl = true;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    Excel.Workbook book1 = null;
                    book1 = exobj1.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book1.Save();
                    book1.Close(true, book1, Type.Missing);
                    exobj1.Workbooks.Close();
                    exobj1.Quit();

                    //read from AveragePrice.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    //string price = "تراز آمادگی";
                    string price = "DATA";
                    DataTable dde = Utilities.GetTable("select * from FilesNameFormat where filename='SaledEnergy'");
                    if (dde.Rows.Count > 0)
                    {
                        if (dde.Rows[0]["SheetName"].ToString().Trim() != "" || dde.Rows[0]["SheetName"].ToString().Trim() != null)
                        {
                            price = dde.Rows[0]["SheetName"].ToString();
                        }

                    }
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    DataTable TempTable = objDataset1.Tables[0];

                    objConn.Close();

                    //Insert into DB (saled energy)

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@Date", SqlDbType.NVarChar, 50);
                    MyCom.Parameters.Add("@PriceMin", SqlDbType.Real);
                    MyCom.Parameters.Add("@PriceMax", SqlDbType.Real);
                    MyCom.Parameters.Add("@rowindex", SqlDbType.Int);
                    MyCom.Parameters.Add("@Hour1", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour2", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour3", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour4", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour5", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour6", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour7", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour8", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour9", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour10", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour11", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour12", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour13", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour14", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour15", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour16", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour17", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour18", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour19", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour20", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour21", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour22", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour23", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour24", SqlDbType.Real);

                    for (int i = 2; i < 27; i++)
                    {

                        
                        DataTable ddel = Utilities.GetTable("delete from SaledEnergy where Date='" + date + "' and rowindex='" + (i - 2) + "'");


                        MyCom.CommandText = "INSERT INTO [SaledEnergy] (Date,PriceMin,PriceMax,rowindex,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,"
                        + "Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24) VALUES "
                        + "(@Date,@PriceMin,@PriceMax,@rowindex,@Hour1,@Hour2,@Hour3,@Hour4,@Hour5,@Hour6,@Hour7,@Hour8,@Hour9,@Hour10,"
                        + "@Hour11,@Hour12,@Hour13,@Hour14,@Hour15,@Hour16,@Hour17,@Hour18,@Hour19,@Hour20,@Hour21,@Hour22,@Hour23,@Hour24)";
                        SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                        myConnection.Open();

                        MyCom.Connection = myConnection;

                        MyCom.Parameters["@Date"].Value = date;
                        MyCom.Parameters["@rowindex"].Value = (i - 2);

                        MyCom.Parameters["@PriceMin"].Value = TempTable.Rows[i][0];
                        if (TempTable.Rows[i][1].ToString() != "")
                        {
                            MyCom.Parameters["@PriceMax"].Value = TempTable.Rows[i][1];
                        }
                        else
                            MyCom.Parameters["@PriceMax"].Value = TempTable.Rows[i][0];

                        int h = 2;

                        MyCom.Parameters["@Hour1"].Value = TempTable.Rows[i][h];
                        MyCom.Parameters["@Hour2"].Value = TempTable.Rows[i][h + 1];
                        MyCom.Parameters["@Hour3"].Value = TempTable.Rows[i][h + 2];
                        MyCom.Parameters["@Hour4"].Value = TempTable.Rows[i][h + 3];
                        MyCom.Parameters["@Hour5"].Value = TempTable.Rows[i][h + 4];
                        MyCom.Parameters["@Hour6"].Value = TempTable.Rows[i][h + 5];
                        MyCom.Parameters["@Hour7"].Value = TempTable.Rows[i][h + 6];
                        MyCom.Parameters["@Hour8"].Value = TempTable.Rows[i][h + 7];
                        MyCom.Parameters["@Hour9"].Value = TempTable.Rows[i][h + 8];
                        MyCom.Parameters["@Hour10"].Value = TempTable.Rows[i][h + 9];
                        MyCom.Parameters["@Hour11"].Value = TempTable.Rows[i][h + 10];
                        MyCom.Parameters["@Hour12"].Value = TempTable.Rows[i][h + 11];
                        MyCom.Parameters["@Hour13"].Value = TempTable.Rows[i][h + 12];
                        MyCom.Parameters["@Hour14"].Value = TempTable.Rows[i][h + 13];
                        MyCom.Parameters["@Hour15"].Value = TempTable.Rows[i][h + 14];
                        MyCom.Parameters["@Hour16"].Value = TempTable.Rows[i][h + 15];
                        MyCom.Parameters["@Hour17"].Value = TempTable.Rows[i][h + 16];
                        MyCom.Parameters["@Hour18"].Value = TempTable.Rows[i][h + 17];
                        MyCom.Parameters["@Hour19"].Value = TempTable.Rows[i][h + 18];
                        MyCom.Parameters["@Hour20"].Value = TempTable.Rows[i][h + 19];
                        MyCom.Parameters["@Hour21"].Value = TempTable.Rows[i][h + 20];
                        MyCom.Parameters["@Hour22"].Value = TempTable.Rows[i][h + 21];
                        MyCom.Parameters["@Hour23"].Value = TempTable.Rows[i][h + 22];
                        MyCom.Parameters["@Hour24"].Value = TempTable.Rows[i][h + 23];


                        try
                        {
                            MyCom.ExecuteNonQuery();

                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                            AddNotExistMessage(path);
                            success = false;
                            break;
                        }

                        System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                    }
                }
                else
                {
                    AddNotExistMessage(path);

                }

            }
            catch
            {

            }

        }
        public void EnergySaledEC(string date)
        {

            bool success = true;
            try
            {
                //price13900510.xls


                string TempDate = date;
                string Tempday = TempDate.Substring(8);
                string TempMonth = TempDate.Substring(5, 2);
                string Tempyear = TempDate.Substring(0, 4);

                string path = Auto_Update_Library.BaseData.GetInstance().M0091Path;

                //old path = path + @"\" + Tempyear.ToString() + @"\" + int.Parse(TempMonth);
                //old path += @"\" + "price" + Tempyear + TempMonth + Tempday + ".xls";

                ////////////////////////////////////////////////////////////


                string pathname = folderFormat(date, "", "SaledEnergy");
                if (pathname != "") path += pathname;
                string makename = NameFormat(date, "", "", "SaledEnergy");
                if (makename != "") path = path + "\\" + makename;


                ////////////////////////////////////////////////////////////////////
                ////////////////////////// max version2////////////////////////////
                string finalpath = "";

                if (File.Exists(path.Remove(path.Length - 1) + ".xls"))
                    finalpath = path.Remove(path.Length - 1);

                if (File.Exists(path + ".xls"))
                    finalpath = path;
                if (File.Exists(path + "1.xls"))
                    finalpath = path + "1";
                if (File.Exists(path + "2.xls"))
                    finalpath = path + "2";
                if (File.Exists(path + "3.xls"))
                    finalpath = path + "3";
                if (File.Exists(path + "4.xls"))
                    finalpath = path + "4";
                if (File.Exists(path + "5.xls"))
                    finalpath = path + "5";
                if (File.Exists(path + "6.xls"))
                    finalpath = path + "6";
                if (File.Exists(path + "7.xls"))
                    finalpath = path + "7";
                if (File.Exists(path + "8.xls"))
                    finalpath = path + "8";
                if (File.Exists(path + "9.xls"))
                    finalpath = path + "9";
                if (File.Exists(path + "10.xls"))
                    finalpath = path + "10";

                finalpath = finalpath + ".xls";

                path = finalpath;
                ///////////////////////////////////////////////////////////////////



                /////////////////////////////////////////////////////////////////////
                if (File.Exists(path))
                {
                    Excel.Application exobj1 = new Excel.Application();
                    exobj1.Visible = false;
                    exobj1.UserControl = true;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    Excel.Workbook book1 = null;
                    book1 = exobj1.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book1.Save();
                    book1.Close(true, book1, Type.Missing);
                    exobj1.Workbooks.Close();
                    exobj1.Quit();

                    //read from AveragePrice.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    //string price = "تراز آمادگی";
                    string price = "آرایش اقتصادی";
                    //DataTable dde = utilities.GetTable("select * from FilesNameFormat where filename='SaledEnergy'");
                    //if (dde.Rows.Count > 0)
                    //{
                    //    if (dde.Rows[0]["SheetName"].ToString().Trim() != "" || dde.Rows[0]["SheetName"].ToString().Trim() != null)
                    //    {
                    //        price = dde.Rows[0]["SheetName"].ToString();
                    //    }

                    //}
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    DataTable TempTable = objDataset1.Tables[0];

                    objConn.Close();

                    //Insert into DB (saled energy)

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@Date", SqlDbType.NVarChar, 50);
                    MyCom.Parameters.Add("@PriceMin", SqlDbType.Real);
                    MyCom.Parameters.Add("@PriceMax", SqlDbType.Real);
                    MyCom.Parameters.Add("@rowindex", SqlDbType.Int);
                    MyCom.Parameters.Add("@Hour1", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour2", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour3", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour4", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour5", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour6", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour7", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour8", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour9", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour10", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour11", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour12", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour13", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour14", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour15", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour16", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour17", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour18", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour19", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour20", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour21", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour22", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour23", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour24", SqlDbType.Real);

                    for (int i = 2; i < 27; i++)
                    {


                        DataTable ddel = Utilities.GetTable("delete from SaledEnergyEc where Date='" + date + "' and rowindex='" + (i - 2) + "'");


                        MyCom.CommandText = "INSERT INTO [SaledEnergyEc] (Date,PriceMin,PriceMax,rowindex,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,"
                        + "Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24) VALUES "
                        + "(@Date,@PriceMin,@PriceMax,@rowindex,@Hour1,@Hour2,@Hour3,@Hour4,@Hour5,@Hour6,@Hour7,@Hour8,@Hour9,@Hour10,"
                        + "@Hour11,@Hour12,@Hour13,@Hour14,@Hour15,@Hour16,@Hour17,@Hour18,@Hour19,@Hour20,@Hour21,@Hour22,@Hour23,@Hour24)";
                        SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                        myConnection.Open();

                        MyCom.Connection = myConnection;

                        MyCom.Parameters["@Date"].Value = date;
                        MyCom.Parameters["@rowindex"].Value = (i - 2);

                        MyCom.Parameters["@PriceMin"].Value = TempTable.Rows[i][0];
                        if (TempTable.Rows[i][1].ToString() != "")
                        {
                            MyCom.Parameters["@PriceMax"].Value = TempTable.Rows[i][1];
                        }
                        else
                            MyCom.Parameters["@PriceMax"].Value = TempTable.Rows[i][0];

                        int h = 2;

                        MyCom.Parameters["@Hour1"].Value = TempTable.Rows[i][h];
                        MyCom.Parameters["@Hour2"].Value = TempTable.Rows[i][h + 1];
                        MyCom.Parameters["@Hour3"].Value = TempTable.Rows[i][h + 2];
                        MyCom.Parameters["@Hour4"].Value = TempTable.Rows[i][h + 3];
                        MyCom.Parameters["@Hour5"].Value = TempTable.Rows[i][h + 4];
                        MyCom.Parameters["@Hour6"].Value = TempTable.Rows[i][h + 5];
                        MyCom.Parameters["@Hour7"].Value = TempTable.Rows[i][h + 6];
                        MyCom.Parameters["@Hour8"].Value = TempTable.Rows[i][h + 7];
                        MyCom.Parameters["@Hour9"].Value = TempTable.Rows[i][h + 8];
                        MyCom.Parameters["@Hour10"].Value = TempTable.Rows[i][h + 9];
                        MyCom.Parameters["@Hour11"].Value = TempTable.Rows[i][h + 10];
                        MyCom.Parameters["@Hour12"].Value = TempTable.Rows[i][h + 11];
                        MyCom.Parameters["@Hour13"].Value = TempTable.Rows[i][h + 12];
                        MyCom.Parameters["@Hour14"].Value = TempTable.Rows[i][h + 13];
                        MyCom.Parameters["@Hour15"].Value = TempTable.Rows[i][h + 14];
                        MyCom.Parameters["@Hour16"].Value = TempTable.Rows[i][h + 15];
                        MyCom.Parameters["@Hour17"].Value = TempTable.Rows[i][h + 16];
                        MyCom.Parameters["@Hour18"].Value = TempTable.Rows[i][h + 17];
                        MyCom.Parameters["@Hour19"].Value = TempTable.Rows[i][h + 18];
                        MyCom.Parameters["@Hour20"].Value = TempTable.Rows[i][h + 19];
                        MyCom.Parameters["@Hour21"].Value = TempTable.Rows[i][h + 20];
                        MyCom.Parameters["@Hour22"].Value = TempTable.Rows[i][h + 21];
                        MyCom.Parameters["@Hour23"].Value = TempTable.Rows[i][h + 22];
                        MyCom.Parameters["@Hour24"].Value = TempTable.Rows[i][h + 23];


                        try
                        {
                            MyCom.ExecuteNonQuery();

                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                            AddNotExistMessage(path);
                            success = false;
                            break;
                        }

                        System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                    }
                }
                else
                {
                    AddNotExistMessage(path);

                }

            }
            catch
            {

            }

        }
        public void EnergySaledUC(string date)
        {

            bool success = true;
            try
            {
                //price13900510.xls


                string TempDate = date;
                string Tempday = TempDate.Substring(8);
                string TempMonth = TempDate.Substring(5, 2);
                string Tempyear = TempDate.Substring(0, 4);

                string path = Auto_Update_Library.BaseData.GetInstance().M0091Path;

                //old path = path + @"\" + Tempyear.ToString() + @"\" + int.Parse(TempMonth);
                //old path += @"\" + "price" + Tempyear + TempMonth + Tempday + ".xls";

                ////////////////////////////////////////////////////////////


                string pathname = folderFormat(date, "", "SaledEnergy");
                if (pathname != "") path += pathname;
                string makename = NameFormat(date, "", "", "SaledEnergy");
                if (makename != "") path = path + "\\" + makename;


                ////////////////////////////////////////////////////////////////////
                ////////////////////////// max version2////////////////////////////
                string finalpath = "";

                if (File.Exists(path.Remove(path.Length - 1) + ".xls"))
                    finalpath = path.Remove(path.Length - 1);

                if (File.Exists(path + ".xls"))
                    finalpath = path;
                if (File.Exists(path + "1.xls"))
                    finalpath = path + "1";
                if (File.Exists(path + "2.xls"))
                    finalpath = path + "2";
                if (File.Exists(path + "3.xls"))
                    finalpath = path + "3";
                if (File.Exists(path + "4.xls"))
                    finalpath = path + "4";
                if (File.Exists(path + "5.xls"))
                    finalpath = path + "5";
                if (File.Exists(path + "6.xls"))
                    finalpath = path + "6";
                if (File.Exists(path + "7.xls"))
                    finalpath = path + "7";
                if (File.Exists(path + "8.xls"))
                    finalpath = path + "8";
                if (File.Exists(path + "9.xls"))
                    finalpath = path + "9";
                if (File.Exists(path + "10.xls"))
                    finalpath = path + "10";

                finalpath = finalpath + ".xls";

                path = finalpath;
                ///////////////////////////////////////////////////////////////////



                /////////////////////////////////////////////////////////////////////
                if (File.Exists(path))
                {
                    Excel.Application exobj1 = new Excel.Application();
                    exobj1.Visible = false;
                    exobj1.UserControl = true;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    Excel.Workbook book1 = null;
                    book1 = exobj1.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book1.Save();
                    book1.Close(true, book1, Type.Missing);
                    exobj1.Workbooks.Close();
                    exobj1.Quit();

                    //read from AveragePrice.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    //string price = "تراز آمادگی";
                    string price = "آرایش نهایی";
                    //DataTable dde = utilities.GetTable("select * from FilesNameFormat where filename='SaledEnergy'");
                    //if (dde.Rows.Count > 0)
                    //{
                    //    if (dde.Rows[0]["SheetName"].ToString().Trim() != "" || dde.Rows[0]["SheetName"].ToString().Trim() != null)
                    //    {
                    //        price = dde.Rows[0]["SheetName"].ToString();
                    //    }

                    //}
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    DataTable TempTable = objDataset1.Tables[0];

                    objConn.Close();

                    //Insert into DB (saled energy)

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@Date", SqlDbType.NVarChar, 50);
                    MyCom.Parameters.Add("@PriceMin", SqlDbType.Real);
                    MyCom.Parameters.Add("@PriceMax", SqlDbType.Real);
                    MyCom.Parameters.Add("@rowindex", SqlDbType.Int);
                    MyCom.Parameters.Add("@Hour1", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour2", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour3", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour4", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour5", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour6", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour7", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour8", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour9", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour10", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour11", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour12", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour13", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour14", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour15", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour16", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour17", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour18", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour19", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour20", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour21", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour22", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour23", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour24", SqlDbType.Real);

                    for (int i = 2; i < 27; i++)
                    {


                        DataTable ddel = Utilities.GetTable("delete from SaledEnergyuc where Date='" + date + "' and rowindex='" + (i - 2) + "'");


                        MyCom.CommandText = "INSERT INTO [SaledEnergyuc] (Date,PriceMin,PriceMax,rowindex,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,"
                        + "Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24) VALUES "
                        + "(@Date,@PriceMin,@PriceMax,@rowindex,@Hour1,@Hour2,@Hour3,@Hour4,@Hour5,@Hour6,@Hour7,@Hour8,@Hour9,@Hour10,"
                        + "@Hour11,@Hour12,@Hour13,@Hour14,@Hour15,@Hour16,@Hour17,@Hour18,@Hour19,@Hour20,@Hour21,@Hour22,@Hour23,@Hour24)";
                        SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                        myConnection.Open();

                        MyCom.Connection = myConnection;

                        MyCom.Parameters["@Date"].Value = date;
                        MyCom.Parameters["@rowindex"].Value = (i - 2);

                        MyCom.Parameters["@PriceMin"].Value = TempTable.Rows[i][0];
                        if (TempTable.Rows[i][1].ToString() != "")
                        {
                            MyCom.Parameters["@PriceMax"].Value = TempTable.Rows[i][1];
                        }
                        else
                            MyCom.Parameters["@PriceMax"].Value = TempTable.Rows[i][0];

                        int h = 2;

                        MyCom.Parameters["@Hour1"].Value = TempTable.Rows[i][h];
                        MyCom.Parameters["@Hour2"].Value = TempTable.Rows[i][h + 1];
                        MyCom.Parameters["@Hour3"].Value = TempTable.Rows[i][h + 2];
                        MyCom.Parameters["@Hour4"].Value = TempTable.Rows[i][h + 3];
                        MyCom.Parameters["@Hour5"].Value = TempTable.Rows[i][h + 4];
                        MyCom.Parameters["@Hour6"].Value = TempTable.Rows[i][h + 5];
                        MyCom.Parameters["@Hour7"].Value = TempTable.Rows[i][h + 6];
                        MyCom.Parameters["@Hour8"].Value = TempTable.Rows[i][h + 7];
                        MyCom.Parameters["@Hour9"].Value = TempTable.Rows[i][h + 8];
                        MyCom.Parameters["@Hour10"].Value = TempTable.Rows[i][h + 9];
                        MyCom.Parameters["@Hour11"].Value = TempTable.Rows[i][h + 10];
                        MyCom.Parameters["@Hour12"].Value = TempTable.Rows[i][h + 11];
                        MyCom.Parameters["@Hour13"].Value = TempTable.Rows[i][h + 12];
                        MyCom.Parameters["@Hour14"].Value = TempTable.Rows[i][h + 13];
                        MyCom.Parameters["@Hour15"].Value = TempTable.Rows[i][h + 14];
                        MyCom.Parameters["@Hour16"].Value = TempTable.Rows[i][h + 15];
                        MyCom.Parameters["@Hour17"].Value = TempTable.Rows[i][h + 16];
                        MyCom.Parameters["@Hour18"].Value = TempTable.Rows[i][h + 17];
                        MyCom.Parameters["@Hour19"].Value = TempTable.Rows[i][h + 18];
                        MyCom.Parameters["@Hour20"].Value = TempTable.Rows[i][h + 19];
                        MyCom.Parameters["@Hour21"].Value = TempTable.Rows[i][h + 20];
                        MyCom.Parameters["@Hour22"].Value = TempTable.Rows[i][h + 21];
                        MyCom.Parameters["@Hour23"].Value = TempTable.Rows[i][h + 22];
                        MyCom.Parameters["@Hour24"].Value = TempTable.Rows[i][h + 23];


                        try
                        {
                            MyCom.ExecuteNonQuery();

                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                            AddNotExistMessage(path);
                            success = false;
                            break;
                        }

                        System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                    }
                }
                else
                {
                    AddNotExistMessage(path);

                }

            }
            catch
            {

            }

        }

        public void EnergySaledFEC(string date)
        {

            bool success = true;
            try
            {
                //price13900510.xls


                string TempDate = date;
                string Tempday = TempDate.Substring(8);
                string TempMonth = TempDate.Substring(5, 2);
                string Tempyear = TempDate.Substring(0, 4);

                string path = Auto_Update_Library.BaseData.GetInstance().M0091Path;

                //old path = path + @"\" + Tempyear.ToString() + @"\" + int.Parse(TempMonth);
                //old path += @"\" + "price" + Tempyear + TempMonth + Tempday + ".xls";

                ////////////////////////////////////////////////////////////


                string pathname = folderFormat(date, "", "SaledEnergy");
                if (pathname != "") path += pathname;
                string makename = NameFormat(date, "", "", "SaledEnergy");
                if (makename != "") path = path + "\\" + makename;


                ////////////////////////////////////////////////////////////////////
                ////////////////////////// max version2////////////////////////////
                string finalpath = "";

                if (File.Exists(path.Remove(path.Length - 1) + ".xls"))
                    finalpath = path.Remove(path.Length - 1);

                if (File.Exists(path + ".xls"))
                    finalpath = path;
                if (File.Exists(path + "1.xls"))
                    finalpath = path + "1";
                if (File.Exists(path + "2.xls"))
                    finalpath = path + "2";
                if (File.Exists(path + "3.xls"))
                    finalpath = path + "3";
                if (File.Exists(path + "4.xls"))
                    finalpath = path + "4";
                if (File.Exists(path + "5.xls"))
                    finalpath = path + "5";
                if (File.Exists(path + "6.xls"))
                    finalpath = path + "6";
                if (File.Exists(path + "7.xls"))
                    finalpath = path + "7";
                if (File.Exists(path + "8.xls"))
                    finalpath = path + "8";
                if (File.Exists(path + "9.xls"))
                    finalpath = path + "9";
                if (File.Exists(path + "10.xls"))
                    finalpath = path + "10";

                finalpath = finalpath + ".xls";

                path = finalpath;
                ///////////////////////////////////////////////////////////////////



                /////////////////////////////////////////////////////////////////////
                if (File.Exists(path))
                {
                    Excel.Application exobj1 = new Excel.Application();
                    exobj1.Visible = false;
                    exobj1.UserControl = true;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    Excel.Workbook book1 = null;
                    book1 = exobj1.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book1.Save();
                    book1.Close(true, book1, Type.Missing);
                    exobj1.Workbooks.Close();
                    exobj1.Quit();

                    //read from AveragePrice.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    //string price = "تراز آمادگی";
                    string price = "آرایش اقتصادی با محدودیت سوخت";
                    //DataTable dde = utilities.GetTable("select * from FilesNameFormat where filename='SaledEnergy'");
                    //if (dde.Rows.Count > 0)
                    //{
                    //    if (dde.Rows[0]["SheetName"].ToString().Trim() != "" || dde.Rows[0]["SheetName"].ToString().Trim() != null)
                    //    {
                    //        price = dde.Rows[0]["SheetName"].ToString();
                    //    }

                    //}
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    DataTable TempTable = objDataset1.Tables[0];

                    objConn.Close();

                    //Insert into DB (saled energy)

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@Date", SqlDbType.NVarChar, 50);
                    MyCom.Parameters.Add("@PriceMin", SqlDbType.Real);
                    MyCom.Parameters.Add("@PriceMax", SqlDbType.Real);
                    MyCom.Parameters.Add("@rowindex", SqlDbType.Int);
                    MyCom.Parameters.Add("@Hour1", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour2", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour3", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour4", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour5", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour6", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour7", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour8", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour9", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour10", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour11", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour12", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour13", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour14", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour15", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour16", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour17", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour18", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour19", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour20", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour21", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour22", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour23", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour24", SqlDbType.Real);

                    for (int i = 2; i < 27; i++)
                    {


                        DataTable ddel = Utilities.GetTable("delete from fuelSaledEnergyec where Date='" + date + "' and rowindex='" + (i - 2) + "'");


                        MyCom.CommandText = "INSERT INTO [fuelSaledEnergyec] (Date,PriceMin,PriceMax,rowindex,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,"
                        + "Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24) VALUES "
                        + "(@Date,@PriceMin,@PriceMax,@rowindex,@Hour1,@Hour2,@Hour3,@Hour4,@Hour5,@Hour6,@Hour7,@Hour8,@Hour9,@Hour10,"
                        + "@Hour11,@Hour12,@Hour13,@Hour14,@Hour15,@Hour16,@Hour17,@Hour18,@Hour19,@Hour20,@Hour21,@Hour22,@Hour23,@Hour24)";
                        SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                        myConnection.Open();

                        MyCom.Connection = myConnection;

                        MyCom.Parameters["@Date"].Value = date;
                        MyCom.Parameters["@rowindex"].Value = (i - 2);

                        MyCom.Parameters["@PriceMin"].Value = TempTable.Rows[i][0];
                        if (TempTable.Rows[i][1].ToString() != "")
                        {
                            MyCom.Parameters["@PriceMax"].Value = TempTable.Rows[i][1];
                        }
                        else
                            MyCom.Parameters["@PriceMax"].Value = TempTable.Rows[i][0];

                        int h = 2;

                        MyCom.Parameters["@Hour1"].Value = TempTable.Rows[i][h];
                        MyCom.Parameters["@Hour2"].Value = TempTable.Rows[i][h + 1];
                        MyCom.Parameters["@Hour3"].Value = TempTable.Rows[i][h + 2];
                        MyCom.Parameters["@Hour4"].Value = TempTable.Rows[i][h + 3];
                        MyCom.Parameters["@Hour5"].Value = TempTable.Rows[i][h + 4];
                        MyCom.Parameters["@Hour6"].Value = TempTable.Rows[i][h + 5];
                        MyCom.Parameters["@Hour7"].Value = TempTable.Rows[i][h + 6];
                        MyCom.Parameters["@Hour8"].Value = TempTable.Rows[i][h + 7];
                        MyCom.Parameters["@Hour9"].Value = TempTable.Rows[i][h + 8];
                        MyCom.Parameters["@Hour10"].Value = TempTable.Rows[i][h + 9];
                        MyCom.Parameters["@Hour11"].Value = TempTable.Rows[i][h + 10];
                        MyCom.Parameters["@Hour12"].Value = TempTable.Rows[i][h + 11];
                        MyCom.Parameters["@Hour13"].Value = TempTable.Rows[i][h + 12];
                        MyCom.Parameters["@Hour14"].Value = TempTable.Rows[i][h + 13];
                        MyCom.Parameters["@Hour15"].Value = TempTable.Rows[i][h + 14];
                        MyCom.Parameters["@Hour16"].Value = TempTable.Rows[i][h + 15];
                        MyCom.Parameters["@Hour17"].Value = TempTable.Rows[i][h + 16];
                        MyCom.Parameters["@Hour18"].Value = TempTable.Rows[i][h + 17];
                        MyCom.Parameters["@Hour19"].Value = TempTable.Rows[i][h + 18];
                        MyCom.Parameters["@Hour20"].Value = TempTable.Rows[i][h + 19];
                        MyCom.Parameters["@Hour21"].Value = TempTable.Rows[i][h + 20];
                        MyCom.Parameters["@Hour22"].Value = TempTable.Rows[i][h + 21];
                        MyCom.Parameters["@Hour23"].Value = TempTable.Rows[i][h + 22];
                        MyCom.Parameters["@Hour24"].Value = TempTable.Rows[i][h + 23];


                        try
                        {
                            MyCom.ExecuteNonQuery();

                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                            AddNotExistMessage(path);
                            success = false;
                            break;
                        }

                        System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                    }
                }
                else
                {
                    AddNotExistMessage(path);

                }

            }
            catch
            {

            }

        }
        public void EnergySaledFUC(string date)
        {

            bool success = true;
            try
            {
                //price13900510.xls


                string TempDate = date;
                string Tempday = TempDate.Substring(8);
                string TempMonth = TempDate.Substring(5, 2);
                string Tempyear = TempDate.Substring(0, 4);

                string path = Auto_Update_Library.BaseData.GetInstance().M0091Path;

                //old path = path + @"\" + Tempyear.ToString() + @"\" + int.Parse(TempMonth);
                //old path += @"\" + "price" + Tempyear + TempMonth + Tempday + ".xls";

                ////////////////////////////////////////////////////////////


                string pathname = folderFormat(date, "", "SaledEnergy");
                if (pathname != "") path += pathname;
                string makename = NameFormat(date, "", "", "SaledEnergy");
                if (makename != "") path = path + "\\" + makename;


                ////////////////////////////////////////////////////////////////////
                ////////////////////////// max version2////////////////////////////
                string finalpath = "";

                if (File.Exists(path.Remove(path.Length - 1) + ".xls"))
                    finalpath = path.Remove(path.Length - 1);

                if (File.Exists(path + ".xls"))
                    finalpath = path;
                if (File.Exists(path + "1.xls"))
                    finalpath = path + "1";
                if (File.Exists(path + "2.xls"))
                    finalpath = path + "2";
                if (File.Exists(path + "3.xls"))
                    finalpath = path + "3";
                if (File.Exists(path + "4.xls"))
                    finalpath = path + "4";
                if (File.Exists(path + "5.xls"))
                    finalpath = path + "5";
                if (File.Exists(path + "6.xls"))
                    finalpath = path + "6";
                if (File.Exists(path + "7.xls"))
                    finalpath = path + "7";
                if (File.Exists(path + "8.xls"))
                    finalpath = path + "8";
                if (File.Exists(path + "9.xls"))
                    finalpath = path + "9";
                if (File.Exists(path + "10.xls"))
                    finalpath = path + "10";

                finalpath = finalpath + ".xls";

                path = finalpath;
                ///////////////////////////////////////////////////////////////////



                /////////////////////////////////////////////////////////////////////
                if (File.Exists(path))
                {
                    Excel.Application exobj1 = new Excel.Application();
                    exobj1.Visible = false;
                    exobj1.UserControl = true;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    Excel.Workbook book1 = null;
                    book1 = exobj1.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book1.Save();
                    book1.Close(true, book1, Type.Missing);
                    exobj1.Workbooks.Close();
                    exobj1.Quit();

                    //read from AveragePrice.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    //string price = "تراز آمادگی";
                    string price = "آرایش نهایی با محدودیت سوخت";
                    //DataTable dde = utilities.GetTable("select * from FilesNameFormat where filename='SaledEnergy'");
                    //if (dde.Rows.Count > 0)
                    //{
                    //    if (dde.Rows[0]["SheetName"].ToString().Trim() != "" || dde.Rows[0]["SheetName"].ToString().Trim() != null)
                    //    {
                    //        price = dde.Rows[0]["SheetName"].ToString();
                    //    }

                    //}
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    DataTable TempTable = objDataset1.Tables[0];

                    objConn.Close();

                    //Insert into DB (saled energy)

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@Date", SqlDbType.NVarChar, 50);
                    MyCom.Parameters.Add("@PriceMin", SqlDbType.Real);
                    MyCom.Parameters.Add("@PriceMax", SqlDbType.Real);
                    MyCom.Parameters.Add("@rowindex", SqlDbType.Int);
                    MyCom.Parameters.Add("@Hour1", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour2", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour3", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour4", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour5", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour6", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour7", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour8", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour9", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour10", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour11", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour12", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour13", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour14", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour15", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour16", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour17", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour18", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour19", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour20", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour21", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour22", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour23", SqlDbType.Real);
                    MyCom.Parameters.Add("@Hour24", SqlDbType.Real);

                    for (int i = 2; i < 27; i++)
                    {


                        DataTable ddel = Utilities.GetTable("delete from fuelSaledEnergyuc where Date='" + date + "' and rowindex='" + (i - 2) + "'");


                        MyCom.CommandText = "INSERT INTO [fuelSaledEnergyuc] (Date,PriceMin,PriceMax,rowindex,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,"
                        + "Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24) VALUES "
                        + "(@Date,@PriceMin,@PriceMax,@rowindex,@Hour1,@Hour2,@Hour3,@Hour4,@Hour5,@Hour6,@Hour7,@Hour8,@Hour9,@Hour10,"
                        + "@Hour11,@Hour12,@Hour13,@Hour14,@Hour15,@Hour16,@Hour17,@Hour18,@Hour19,@Hour20,@Hour21,@Hour22,@Hour23,@Hour24)";
                        SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                        myConnection.Open();

                        MyCom.Connection = myConnection;

                        MyCom.Parameters["@Date"].Value = date;
                        MyCom.Parameters["@rowindex"].Value = (i - 2);

                        MyCom.Parameters["@PriceMin"].Value = TempTable.Rows[i][0];
                        if (TempTable.Rows[i][1].ToString() != "")
                        {
                            MyCom.Parameters["@PriceMax"].Value = TempTable.Rows[i][1];
                        }
                        else
                            MyCom.Parameters["@PriceMax"].Value = TempTable.Rows[i][0];

                        int h = 2;

                        MyCom.Parameters["@Hour1"].Value = TempTable.Rows[i][h];
                        MyCom.Parameters["@Hour2"].Value = TempTable.Rows[i][h + 1];
                        MyCom.Parameters["@Hour3"].Value = TempTable.Rows[i][h + 2];
                        MyCom.Parameters["@Hour4"].Value = TempTable.Rows[i][h + 3];
                        MyCom.Parameters["@Hour5"].Value = TempTable.Rows[i][h + 4];
                        MyCom.Parameters["@Hour6"].Value = TempTable.Rows[i][h + 5];
                        MyCom.Parameters["@Hour7"].Value = TempTable.Rows[i][h + 6];
                        MyCom.Parameters["@Hour8"].Value = TempTable.Rows[i][h + 7];
                        MyCom.Parameters["@Hour9"].Value = TempTable.Rows[i][h + 8];
                        MyCom.Parameters["@Hour10"].Value = TempTable.Rows[i][h + 9];
                        MyCom.Parameters["@Hour11"].Value = TempTable.Rows[i][h + 10];
                        MyCom.Parameters["@Hour12"].Value = TempTable.Rows[i][h + 11];
                        MyCom.Parameters["@Hour13"].Value = TempTable.Rows[i][h + 12];
                        MyCom.Parameters["@Hour14"].Value = TempTable.Rows[i][h + 13];
                        MyCom.Parameters["@Hour15"].Value = TempTable.Rows[i][h + 14];
                        MyCom.Parameters["@Hour16"].Value = TempTable.Rows[i][h + 15];
                        MyCom.Parameters["@Hour17"].Value = TempTable.Rows[i][h + 16];
                        MyCom.Parameters["@Hour18"].Value = TempTable.Rows[i][h + 17];
                        MyCom.Parameters["@Hour19"].Value = TempTable.Rows[i][h + 18];
                        MyCom.Parameters["@Hour20"].Value = TempTable.Rows[i][h + 19];
                        MyCom.Parameters["@Hour21"].Value = TempTable.Rows[i][h + 20];
                        MyCom.Parameters["@Hour22"].Value = TempTable.Rows[i][h + 21];
                        MyCom.Parameters["@Hour23"].Value = TempTable.Rows[i][h + 22];
                        MyCom.Parameters["@Hour24"].Value = TempTable.Rows[i][h + 23];


                        try
                        {
                            MyCom.ExecuteNonQuery();

                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                            AddNotExistMessage(path);
                            success = false;
                            break;
                        }

                        System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                    }
                }
                else
                {
                    AddNotExistMessage(path);

                }

            }
            catch
            {

            }

        }



        public void filldailybillsum(string billdate)
        {
            string price = "sum";
            //string TempDate = date;
            //string Tempday = TempDate.Substring(8);
            //string TempMonth = TempDate.Substring(5, 2);
            //string Tempyear = TempDate.Substring(0, 4);

            string path = Auto_Update_Library.BaseData.GetInstance().Capacityfactor;

        
            ////////////////////////////////////////////////////////////


            string pathname = folderFormat(billdate, "", "CapacityFactor");
            if (pathname != "") path += pathname;
            string makename = NameFormat(billdate, "", "", "CapacityFactor");
            if (makename != "") path = path + "\\" + makename+".xls";

            if (File.Exists(path))
            {

                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                Excel.Workbook book = null;
                book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                book.Save();
                book.Close(true, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();

                //////////////////////

                //Read From Excel File
                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                OleDbConnection objConn = new OleDbConnection(sConnectionString);
                objConn.Open();

                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                objAdapter1.SelectCommand = objCmdSelect;
                DataSet objDataset1 = new DataSet();
                objAdapter1.Fill(objDataset1);
                DataTable TempTable = objDataset1.Tables[0];
                objConn.Close();

                DataTable dd = Utilities.GetTable("delete from DailyBillSum where date='" + billdate + "'");

                foreach (DataRow m in TempTable.Rows)
                {
                    if (m[1].ToString().Trim() != "")
                    {
                        string ppid = m[0].ToString().Trim();
                        string plantname = m[1].ToString().Trim();

                        string s1 = m[4].ToString().Trim();
                        string s2 = m[5].ToString().Trim();
                        string s3 = m[6].ToString().Trim();
                        string s4 = m[7].ToString().Trim();
                        string s5 = m[8].ToString().Trim();
                        string s6 = m[9].ToString().Trim();
                        string s7 = m[10].ToString().Trim();
                        string s8 = m[11].ToString().Trim();
                        string s9 = m[12].ToString().Trim();
                        string s10 = m[13].ToString().Trim();
                        string s11 = m[14].ToString().Trim();
                        string s12 = m[15].ToString().Trim();
                        string s13 = m[16].ToString().Trim();
                        string s14 = m[17].ToString().Trim();
                        string s15 = m[18].ToString().Trim();
                        string s16 = m[19].ToString().Trim();
                        string s17 = m[20].ToString().Trim();
                        string s18 = m[21].ToString().Trim();
                        string s19 = m[22].ToString().Trim();
                        string s20 = m[23].ToString().Trim();
                        string s21 = m[24].ToString().Trim();
                        string s22 = m[25].ToString().Trim();
                        string s23 = m[26].ToString().Trim();
                        string s24 = m[27].ToString().Trim();
                        string s26 = m[28].ToString().Trim();
                        string s27 = m[29].ToString().Trim();
                        string s28 = m[30].ToString().Trim();
                        string s29 = m[31].ToString().Trim();
                        string s31 = m[32].ToString().Trim();
                        string s36 = m[33].ToString().Trim();
                        string s37 = m[34].ToString().Trim();
                        string s38 = m[35].ToString().Trim();
                        string s39 = m[36].ToString().Trim();
                        string s40 = m[37].ToString().Trim();
                        string s41 = m[38].ToString().Trim();
                        string s42 = m[39].ToString().Trim();
                        string s43 = m[40].ToString().Trim();

                        DataTable d = Utilities.GetTable("insert into DailyBillSum (ppid,plantname,Date,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15,s16,s17,s18,s19,s20,s21,s22,s23,s24,s26,s27,s28,s29,s31,s36,s37,s38,s39,s40,s41,s42,s43)" +
                            "values ('" + ppid + "',N'" + plantname + "','" + billdate + "','" + s1 + "','" + s2 + "','" + s3 + "','" + s4 + "','" + s5 + "','" + s6 + "','" + s7 + "','" + s8 + "','" + s9 + "','" + s10 + "','" + s11 + "','" + s12 + "','" + s13 + "','" + s14 + "','" + s15 + "','" + s16 + "','" + s17 + "','" + s18 + "','" + s19 + "','" + s20 + "','" + s21 + "','" + s22 + "','" + s23 + "','" + s24 + "','" + s26 + "','" + s27 + "','" + s28 + "','" + s29 + "','" + s31 + "','" + s36 + "','" + s37 + "','" + s38 + "','" + s39 + "','" + s40 + "','" + s41 + "','" + s42 + "','" + s43 + "')");


                    }



                }
            }
            else
            {
                AddNotExistMessage(path);
            }

        }
        public void filldailybilltotal(string billdate)
        {
            string price = "total";
            //string TempDate = date;
            //string Tempday = TempDate.Substring(8);
            //string TempMonth = TempDate.Substring(5, 2);
            //string Tempyear = TempDate.Substring(0, 4);

            string path = Auto_Update_Library.BaseData.GetInstance().Capacityfactor;
            string pathname = folderFormat(billdate, "", "CapacityFactor");
            if (pathname != "") path += pathname;
            string makename = NameFormat(billdate, "", "", "CapacityFactor");
            if (makename != "") path = path + "\\" + makename + ".xls";

            ////////////////////////////////////////////////////////////
            if (File.Exists(path))
            {




                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                Excel.Workbook book = null;
                book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                book.Save();
                book.Close(true, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();

                //////////////////////

                //Read From Excel File
                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                OleDbConnection objConn = new OleDbConnection(sConnectionString);
                objConn.Open();

                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                objAdapter1.SelectCommand = objCmdSelect;
                DataSet objDataset1 = new DataSet();
                objAdapter1.Fill(objDataset1);
                DataTable TempTable = objDataset1.Tables[0];
                objConn.Close();


                DataTable dd = Utilities.GetTable("delete from DailyBillTotal where date='" + billdate + "'");

                foreach (DataRow m in TempTable.Rows)
                {
                    if (m[1].ToString().Trim() != "")
                    {
                        string code = m[0].ToString().Trim();
                        string item = m[1].ToString().Trim();
                        string value = (m[2].ToString());

                        DataTable d = Utilities.GetTable("insert into DailyBillTotal (Code,Item,Value,Date) values ('" + code + "',N'" + item + "','" + value + "','" + billdate + "')");

                    }
                }

            }
            else
            {
                AddNotExistMessage(path);
            }


        }
        public void filldailybillplant(string billdate)
        {
            string price = "Day";
            //string TempDate = date;
            //string Tempday = TempDate.Substring(8);
            //string TempMonth = TempDate.Substring(5, 2);
            //string Tempyear = TempDate.Substring(0, 4);

            string path = Auto_Update_Library.BaseData.GetInstance().Capacityfactor;
            string pathname = folderFormat(billdate, "", "CapacityFactor");
            if (pathname != "") path += pathname;
            string makename = NameFormat(billdate, "", "", "CapacityFactor");
            if (makename != "") path = path + "\\" + makename + ".xls";

            ////////////////////////////////////////////////////////////
            if (File.Exists(path))
            {



                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                Excel.Workbook book = null;
                book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                book.Save();
                book.Close(true, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();

                //////////////////////

                //Read From Excel File
                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                OleDbConnection objConn = new OleDbConnection(sConnectionString);
                objConn.Open();

                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                objAdapter1.SelectCommand = objCmdSelect;
                DataSet objDataset1 = new DataSet();
                objAdapter1.Fill(objDataset1);
                DataTable TempTable = objDataset1.Tables[0];
                objConn.Close();

                string date = billdate;
                DataTable dd = Utilities.GetTable("delete from DailyBillPlant where date='" + billdate + "'");

                foreach (DataRow m in TempTable.Rows)
                {
                    if (m[1].ToString().Trim() != "")
                    {
                        string ppid = m[0].ToString().Trim();
                        string plantname = m[1].ToString().Trim();

                        string hour = m[4].ToString();
                        string s1 = m[5].ToString().Trim();
                        string s2 = m[6].ToString().Trim();
                        string s3 = m[7].ToString().Trim();
                        string s4 = m[8].ToString().Trim();
                        string s5 = m[9].ToString().Trim();
                        string s6 = m[10].ToString().Trim();
                        string s7 = m[11].ToString().Trim();
                        string s8 = m[12].ToString().Trim();
                        string s9 = m[13].ToString().Trim();
                        string s10 = m[14].ToString().Trim();
                        string s11 = m[15].ToString().Trim();
                        string s12 = m[16].ToString().Trim();
                        string s13 = m[17].ToString().Trim();
                        string s14 = m[18].ToString().Trim();
                        string s15 = m[19].ToString().Trim();
                        string s16 = m[20].ToString().Trim();
                        string s17 = m[21].ToString().Trim();
                        string s18 = m[22].ToString().Trim();
                        string s19 = m[23].ToString().Trim();
                        string s20 = m[24].ToString().Trim();
                        string s21 = m[25].ToString().Trim();
                        string s22 = m[26].ToString().Trim();
                        string s23 = m[27].ToString().Trim();
                        string s24 = m[28].ToString().Trim();
                        string s26 = m[29].ToString().Trim();
                        string s27 = m[30].ToString().Trim();
                        string s28 = m[31].ToString().Trim();
                        string s29 = m[32].ToString().Trim();
                        string s31 = m[33].ToString().Trim();
                        string s36 = m[34].ToString().Trim();
                        string s37 = m[35].ToString().Trim();
                        string s38 = m[36].ToString().Trim();
                        string s39 = m[37].ToString().Trim();
                        string s40 = m[38].ToString().Trim();
                        string s41 = m[39].ToString().Trim();
                        string s42 = m[40].ToString().Trim();
                        string s43 = m[41].ToString().Trim();
                        DataTable d = Utilities.GetTable("insert into DailyBillPlant (ppid,plantname,Date,hour,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15,s16,s17,s18,s19,s20,s21,s22,s23,s24,s26,s27,s28,s29,s31,s36,s37,s38,s39,s40,s41,s42,s43) values ('" + ppid + "',N'" + plantname + "','" + billdate + "','" + hour + "','" + s1 + "','" + s2 + "','" + s3 + "','" + s4 + "','" + s5 + "','" + s6 + "','" + s7 + "','" + s8 + "','" + s9 + "','" + s10 + "','" + s11 + "','" + s12 + "','" + s13 + "','" + s14 + "','" + s15 + "','" + s16 +
                        "','" + s17 + "','" + s18 + "','" + s19 + "','" + s20 + "','" + s21 + "','" + s22 + "','" + s23 + "','" + s24 + "','" + s26 + "','" + s27 + "','" + s28 + "','" + s29 + "','" + s31 + "','" + s36 + "','" + s37 + "','" + s38 + "','" + s39 + "','" + s40 + "','" + s41 + "','" + s42 + "','" + s43 + "')");


                    }
                }


            }
            else
            {
                AddNotExistMessage(path);
            }


        }
         

        public string NameFormat(string date, string ppid, string pnamefarsi,string type)
        {
                //FRM002
                //FRM005
                //FRM0022
                //M002
                //M005
                //M009
                //Plantname/english
                //Plantname/Farsi
                //PlantID
                //Date Without Seperator/8char
                //Date With Seperator/10char
                //(
                //)
                //_
                //-
                //space
                //VersionNumber/max10

            string excelname = "";


           
            string english = "";
            DataTable dv = Utilities.GetTable("select * from powerplant where ppid='" + ppid + "'");
            if (dv.Rows.Count > 0)
            {
                pnamefarsi = dv.Rows[0]["PNameFarsi"].ToString().Trim();
                english = dv.Rows[0]["PPName"].ToString().Trim();

            }

            string year = date.Substring(0, 4).Trim();
            string month = date.Substring(5, 2).Trim();
            string day = date.Substring(8, 2).Trim();

            DataTable dddd = Utilities.GetTable("select * from FilesNameFormat where filename='" + type + "'");
            if (dddd.Rows.Count > 0 && dddd.Rows[0]["ExcelName"].ToString().Trim() != "")
            {

                string[] x = dddd.Rows[0]["ExcelName"].ToString().Trim().Split('*');

                for (int i = 0; i < x.Length; i++)
                {

                    if (x[i].Trim() == "Plantname/english")
                    {
                        excelname += english;
                    }
                    else if (x[i].Trim() == "Plantname/Farsi")
                    {
                        excelname += pnamefarsi;
                    }
                    else if (x[i].Trim() == "PlantID")
                    {
                        excelname += ppid;
                    }
                    else if (x[i].Trim() == "Date Without Seperator/8char")
                    {
                        string strDate = date;
                        strDate = strDate.Remove(7, 1);
                        strDate = strDate.Remove(4, 1);
                        excelname += strDate;
                    }

                    else if (x[i].Trim() == "Date With Seperator/10char")
                    {
                        excelname += date;
                    }
                    else if (x[i].Trim() == "Year/2char")
                    {
                        excelname += year.Substring(2, 2).Trim();
                    }
                    else if (x[i].Trim() == "Year/4char")
                    {
                        excelname += year.Trim();
                    }
                    else if (x[i].Trim() == "month/2char")
                    {
                        excelname += month.Trim();

                    }
                    else if (x[i].Trim() == "month/1char")
                    {
                        //excelname += month.Substring(1, 1).Trim();
                        excelname += int.Parse(month);
                    }
                    else if (x[i].Trim() == "day/2char")
                    {
                        excelname += day.Trim();

                    }
                    else if (x[i].Trim() == "day/1char")
                    {
                        excelname += day.Substring(1, 1).Trim();

                    }
                    else if (x[i].Trim() == "space")
                    {
                        excelname += " ";
                    }
                    else if (x[i].Trim() == "VersionNumber/max10")
                    {
                        //excelname += "1";
                    }
                    else
                    {
                        excelname += x[i];
                    }

                }



                return excelname;
            }
            return excelname;
        }
        public string folderFormat(string date,string ppid,string type)
        {

            //Year/2char
            //Year/4char
            //month/2char
            //month/1char
            //day/2char
            //day/1char
            //PlantName/English
            //PlantName/Farsi
            //MonthName/Finglish
            //Year-6MaheAval
            //Year-6MaheDovom
            string farsi = "";
            string english = "";
            DataTable vv=Utilities.GetTable("select * from powerplant where ppid='"+ppid+"'");
            if (vv.Rows.Count > 0)
            {
                farsi = vv.Rows[0]["PNameFarsi"].ToString().Trim();
                english = vv.Rows[0]["PPName"].ToString().Trim();

            }
            string xcmonth="Year-6MaheAval";
            string year = date.Substring(0, 4).Trim();
            string month = date.Substring(5, 2).Trim();
            string day = date.Substring(8, 2).Trim();
            if (int.Parse(month) > 6) xcmonth = "Year-6MaheDovom";
            string pathname = "";
            DataTable dddd = Utilities.GetTable("select * from FilesNameFormat where filename='" + type + "'");
            if (dddd.Rows.Count > 0 && dddd.Rows[0]["FoldersFormat"].ToString().Trim() != "")
            {

                string[] x = dddd.Rows[0]["FoldersFormat"].ToString().Trim().Split('*');

                for (int i = 0; i < x.Length; i++)
                {

                    if (x[i].Trim() == "Plantname/english")
                    {
                        pathname +=@"\"+ english;
                    }
                    else if (x[i].Trim() == "Plantname/Farsi")
                    {
                        pathname +=@"\"+ farsi;
                    }
                    else if (x[i].Trim() == "PlantID")
                    {
                        pathname +=@"\"+ ppid;
                    }                
                  
                    else if (x[i].Trim() == "space")
                    {
                        pathname += @"\" + " "; 
                    }
                    else if (x[i].Trim() == "Year/2char")
                    {
                        pathname +=@"\"+ year.Substring(2, 2).Trim();
                    }
                    else if (x[i].Trim() == "Year/4char")
                    {
                        pathname +=@"\"+ year.Trim();
                    }
                    else if (x[i].Trim() == "month/2char")
                    {
                        pathname +=@"\"+ month.Trim();

                    }
                    else if (x[i].Trim() == "month/1char")
                    {
                        //pathname +=@"\"+ month.Substring(1, 1).Trim();
                        pathname += @"\" + int.Parse(month);
                    }
                    else if (x[i].Trim() == "day/2char")
                    {
                        pathname += @"\"+day.Trim();

                    }
                    else if (x[i].Trim() == "day/1char")
                    {
                        pathname +=@"\"+ day.Substring(1, 1).Trim();

                    }
                    else if (x[i].Trim() == "MonthName/Finglish")
                    {
                        string  Sendmonth = int.Parse(month).ToString().Trim();
                        switch (Sendmonth)
                        {
                            case "1":
                                Sendmonth = "Farvardin";
                                break;
                            case "2":
                                Sendmonth = "Ordibehesht";
                                break;
                            case "3":
                                Sendmonth = "Khordad";
                                break;
                            case "4":
                                Sendmonth = "Tir";
                                break;
                            case "5":
                                Sendmonth = "Mordad";
                                break;
                            case "6":
                                Sendmonth = "Shahrivar";
                                break;
                            case "7":
                                Sendmonth = "Mehr";
                                break;
                            case "8":
                                Sendmonth = "Aban";
                                break;
                            case "9":
                                Sendmonth = "Azar";
                                break;
                            case "10":
                                Sendmonth = "Day";
                                break;
                            case "11":
                                Sendmonth = "Bahman";
                                break;
                            case "12":
                                Sendmonth = "Esfand";
                                break;

                        }
                        pathname +=@"\"+ Sendmonth.ToString().Trim();
                    }
                    else if (x[i].Trim() == "Date Without Seperator/8char")
                    {
                        string strDate = date;
                        strDate = strDate.Remove(7, 1);
                        strDate = strDate.Remove(4, 1);
                        pathname +=@"\"+ strDate;
                    }
                    else
                    {
                        pathname +=@"\"+ x[i];
                    }

                }



                return pathname;
            }
            return pathname;
        }

//------------------------------M009------------------------------------
//        public void M009(string date, string PPID ,bool PPtype)
//        {
//            bool IsValid = true;

//            string path = ReadM00Excel(ItemsToBeDownloaded.M009, date, PPID, PPtype);

//            SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;

//            string sheetName = "Sheet1";
//            //read from FRM009.xls into dataTable
//            DataTable DT = null;
//            if (path != "" && File.Exists(path + ".xls"))
//            {
//                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ".xls;Extended Properties=Excel 8.0";
//                OleDbConnection objConn = new OleDbConnection(sConnectionString);
//                objConn.Open();
//                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + sheetName + "$]", objConn);
//                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
//                objAdapter1.SelectCommand = objCmdSelect;
//                DataSet objDataset1 = new DataSet();
//                try
//                {
//                    objAdapter1.Fill(objDataset1);
//                    DT = objDataset1.Tables[0];
//                    //dataGridView1.DataSource = objDataset1.Tables[0].DefaultView;
//                }
//                catch (Exception e)
//                {
//                    DT = null;
//                }
//                objConn.Close();

//            }
//            else
//            {
//                AddNotExistMessage(path);
//            }

//            if (DT == null)
//                IsValid = false;

//            //IS IT A Valid File?
//            //if ((IsValid) && (dataGridView1.Columns[1].HeaderText.Contains("M002")))
//            if (IsValid)
//            {
//                //SqlConnection MyConnection = new SqlConnection(ConnectionManager.ConnectionString);
//                //MyConnection.Open();
//                SqlCommand MyCom = new SqlCommand();
//                MyCom.Connection = MyConnection;


//                //Insert into DB (MainFRM009)
//                Excel.Application exobj = new Excel.Application();
//                exobj.Visible = false;
//                exobj.UserControl = true;
//                Excel.Workbook book = null;
//                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
//                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
//                book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

//                int type = 0;
//                int PID = 0;
//                if (PPtype) type = 1;

//                //Is There any Data for this Plant(and type) at this Date in the MainFRM009 and DetailFRM009?
//                MyCom.CommandText = "SELECT @check=COUNT(PPID) FROM MainFRM009 WHERE PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";
//                MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
//                PID = Convert.ToInt32(PPID);
//                MyCom.Parameters["@id"].Value = PID;
//                MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
//                MyCom.Parameters["@tdate"].Value = date;
//                MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
//                MyCom.Parameters["@type"].Value = type;
//                MyCom.Parameters.Add("@check", SqlDbType.Int);
//                MyCom.Parameters["@check"].Direction = ParameterDirection.Output;
//                MyCom.ExecuteNonQuery();
//                int check = int.Parse(MyCom.Parameters["@check"].Value.ToString());

//                if (check == 0)
//                {
//                    MyCom.CommandText = "INSERT INTO [MainFRM009] (PPID,TargetMarketDate,PPName,PPType,"
//                    + "DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
//                    MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
//                    MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
//                    MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
//                    MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
//                    MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
//                    MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

//                    foreach (Excel.Worksheet workSheet in book.Worksheets)
//                        if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
//                        {
//                            PID = Convert.ToInt32(PPID);

//                            MyCom.Parameters["@id"].Value = PID;
//                            MyCom.Parameters["@tdate"].Value = date;
//                            if (((Excel.Range)workSheet.Cells[5, 2]).Value2 != null)
//                                MyCom.Parameters["@name"].Value = ((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString();
//                            else MyCom.Parameters["@name"].Value = "";
//                            MyCom.Parameters["@type"].Value = type;
//                            if (((Excel.Range)workSheet.Cells[2, 2]).Value2 != null)
//                                MyCom.Parameters["@idate"].Value = ((Excel.Range)workSheet.Cells[2, 2]).Value2.ToString();
//                            else MyCom.Parameters["@idate"].Value = "";
//                            if (((Excel.Range)workSheet.Cells[3, 2]).Value2 != null)
//                                MyCom.Parameters["@time"].Value = ((Excel.Range)workSheet.Cells[3, 2]).Value2.ToString();
//                            else MyCom.Parameters["@time"].Value = "";
//                            if (((Excel.Range)workSheet.Cells[7, 2]).Value2 != null)
//                                MyCom.Parameters["@revision"].Value = ((Excel.Range)workSheet.Cells[7, 2]).Value2.ToString();
//                            else MyCom.Parameters["@revision"].Value = 0;
//                            if (((Excel.Range)workSheet.Cells[8, 2]).Value2 != null)
//                                MyCom.Parameters["@filled"].Value = ((Excel.Range)workSheet.Cells[8, 2]).Value2.ToString();
//                            else MyCom.Parameters["@filled"].Value = "";
//                            if (((Excel.Range)workSheet.Cells[9, 2]).Value2 != null)
//                                MyCom.Parameters["@approved"].Value = ((Excel.Range)workSheet.Cells[9, 2]).Value2.ToString();
//                            else MyCom.Parameters["@approved"].Value = "";
//                        }
//                    try
//                    {
//                        MyCom.ExecuteNonQuery();
//                    }
//                    catch (Exception exp)
//                    {
//                        string str = exp.Message;
//                    }
//                }

//                //Insert into DB (DetailFRM009)
//                int x = 10;
//                MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
//                MyCom.Parameters.Add("@PCode", SqlDbType.Int);
//                MyCom.Parameters.Add("@P", SqlDbType.Real);
//                MyCom.Parameters.Add("@QC", SqlDbType.Real);
//                MyCom.Parameters.Add("@QL", SqlDbType.Real);
//                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
//                //read directly and cell by cell
//                foreach (Excel.Worksheet workSheet in book.Worksheets)
//                    if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
//                    {
//                        while (x < (DT.Rows.Count - 2))
//                        {
//                            if (DT.Rows[x][0].ToString() != "")
//                            {
//                                //Detect PackageCode
//                                string Unit = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString().Trim();
//                                if (type == 0)
//                                {
//                                    if (Unit.Contains("G")) Unit = Unit.Replace("G", "Gas");
//                                    else Unit = Unit.Replace("S", "Steam");
//                                }
//                                else
//                                {
//                                    if (Unit.Contains("G")) Unit = Unit.Replace("G", "Gas cc");
//                                    else Unit = Unit.Replace("S", "Steam cc");
//                                }
//                                SqlCommand MyCom1 = new SqlCommand();
//                                MyCom1.Connection = MyConnection;
//                                MyCom1.CommandText = "SELECT @Result=PackageCode FROM UnitsDataMain WHERE UnitCode=@unit AND PPID=@id";
//                                MyCom1.Parameters.Add("@id", SqlDbType.NChar, 10);
//                                MyCom1.Parameters["@id"].Value = PID;
//                                MyCom1.Parameters.Add("@unit", SqlDbType.NChar, 20);
//                                MyCom1.Parameters["@unit"].Value = Unit;
//                                MyCom1.Parameters.Add("@Result", SqlDbType.Int);
//                                MyCom1.Parameters["@Result"].Direction = ParameterDirection.Output;
//                                MyCom1.ExecuteNonQuery();
//                                int PCode = 0;
//                                if (MyCom1.Parameters["@Result"].Value.ToString() != "")
//                                    PCode = int.Parse(MyCom1.Parameters["@Result"].Value.ToString());
//                                else
//                                {
//                                    if (Unit.Contains("G")) Unit = Unit.Replace("Gas", "Gas cc");
//                                    else Unit = Unit.Replace("Steam", "Steam cc");
//                                    MyCom1.CommandText = "SELECT @Result=PackageCode FROM UnitsDataMain WHERE UnitCode=@unit AND PPID=@id";
//                                    MyCom1.Parameters["@id"].Value = PID;
//                                    MyCom1.Parameters["@unit"].Value = Unit;
//                                    MyCom1.ExecuteNonQuery();
//                                    PCode = int.Parse(MyCom1.Parameters["@Result"].Value.ToString());
//                                }

//                                for (int j = 0; j < 24; j++)
//                                {
//                                    if (check == 0)
//                                        MyCom.CommandText = "INSERT INTO [DetailFRM009] (TargetMarketDate,PPID,Block,PPType,PackageCode,Hour" +
//                                        ",P,QC,QL) VALUES (@tdate,@id,@block,@type,@PCode,@hour,@P,@QC,@QL)";
//                                    else
//                                        MyCom.CommandText = "UPDATE DetailFRM009 SET P=@P,QC=@QC,QL=@QL WHERE " +
//                                        "TargetMarketDate=@tdate AND PPID=@id AND PPType=@type AND Block=@block AND " +
//                                        "PackageCode=@PCode AND Hour=@hour";
//                                    MyCom.Parameters["@id"].Value = PID;
//                                    MyCom.Parameters["@tdate"].Value = date;
//                                    MyCom.Parameters["@block"].Value = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString();
//                                    MyCom.Parameters["@PCode"].Value = PCode;
//                                    MyCom.Parameters["@type"].Value = type;
//                                    MyCom.Parameters["@hour"].Value = j + 1;
//                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 3]).Value2 != null)
//                                        MyCom.Parameters["@P"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 3]).Value2.ToString();
//                                    else MyCom.Parameters["@P"].Value = 0;
//                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 4]).Value2 != null)
//                                        MyCom.Parameters["@QC"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 4]).Value2.ToString();
//                                    else MyCom.Parameters["@QC"].Value = 0;
//                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 5]).Value2 != null)
//                                        MyCom.Parameters["@QL"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 5]).Value2.ToString();
//                                    else MyCom.Parameters["@QL"].Value = 0;

//                                    try
//                                    {
//                                        MyCom.ExecuteNonQuery();
//                                    }
//                                    catch (Exception exp)
//                                    {
//                                        string str = exp.Message;
//                                    }
//                                }
//                            }
//                            x++;
//                        }
//                    }

//                book.Close(false, book, Type.Missing);
//                exobj.Workbooks.Close();
//                exobj.Quit();
//                System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
//                MyConnection.Close();
//            }



//        }
////-----------------------------M0091(string date, string PPID)---------------------------------
//        public void M0091(string date, string PPID,bool PPtype)
//        {
//            bool IsValid = true;

//            string path = ReadM00Excel(ItemsToBeDownloaded.M0091, date, PPID, PPtype);

//            SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;

//            string sheetName = "Sheet1";
//            //read from FRM0091.xls into dataTable
//            DataTable DT = null;
//            if (path != "" && File.Exists(path + ".xls"))
//            {
//                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ".xls;Extended Properties=Excel 8.0";
//                OleDbConnection objConn = new OleDbConnection(sConnectionString);
//                objConn.Open();
//                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + sheetName + "$]", objConn);
//                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
//                objAdapter1.SelectCommand = objCmdSelect;
//                DataSet objDataset1 = new DataSet();
//                try
//                {
//                    objAdapter1.Fill(objDataset1);
//                    DT = objDataset1.Tables[0];
//                    //dataGridView1.DataSource = objDataset1.Tables[0].DefaultView;
//                }
//                catch (Exception e)
//                {
//                    DT = null;
//                }
//                objConn.Close();

//            }
//            else
//            {
//                AddNotExistMessage(path);
//            }

//            if (DT == null)
//                IsValid = false;

//            //IS IT A Valid File?
//            if (IsValid) 
//            {

//                //SqlConnection MyConnection = new SqlConnection(ConnectionManager.ConnectionString);
//                //MyConnection.Open();
//                SqlCommand MyCom = new SqlCommand();
//                MyCom.Connection = MyConnection;

//                //Insert into DB (MainFRM009)
//                Excel.Application exobj = new Excel.Application();
//                exobj.Visible = false;
//                exobj.UserControl = true;
//                Excel.Workbook book = null;
//                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
//                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
//                book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

//                int type = 0;
//                int PID = 0;
//                if (PPtype) type = 1;

//                //Is There any Data for this Plant(and type) at this Date in the MainFRM009 and DetailFRM009?
//                MyCom.CommandText = "SELECT @check=COUNT(PPID) FROM MainFRM009 WHERE PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";
//                MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
//                PID = Convert.ToInt32(PPID);
//                MyCom.Parameters["@id"].Value = PID;
//                MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
//                MyCom.Parameters["@tdate"].Value = date;
//                MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
//                MyCom.Parameters["@type"].Value = type;
//                MyCom.Parameters.Add("@check", SqlDbType.Int);
//                MyCom.Parameters["@check"].Direction = ParameterDirection.Output;
//                MyCom.ExecuteNonQuery();
//                int check = int.Parse(MyCom.Parameters["@check"].Value.ToString());

//                if (check == 0)
//                {
//                    MyCom.CommandText = "INSERT INTO [MainFRM009] (PPID,TargetMarketDate,PPName,PPType,"
//                    + "DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
//                    MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
//                    MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
//                    MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
//                    MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
//                    MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
//                    MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

//                    foreach (Excel.Worksheet workSheet in book.Worksheets)
//                        if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
//                        {
//                            MyCom.Parameters["@id"].Value = PID;
//                            MyCom.Parameters["@tdate"].Value = date;
//                            if (((Excel.Range)workSheet.Cells[5, 2]).Value2 != null)
//                                MyCom.Parameters["@name"].Value = ((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString();
//                            else MyCom.Parameters["@name"].Value = "";
//                            MyCom.Parameters["@type"].Value = type;
//                            if (((Excel.Range)workSheet.Cells[2, 2]).Value2 != null)
//                                MyCom.Parameters["@idate"].Value = ((Excel.Range)workSheet.Cells[2, 2]).Value2.ToString();
//                            else MyCom.Parameters["@idate"].Value = "";
//                            if (((Excel.Range)workSheet.Cells[3, 2]).Value2 != null)
//                                MyCom.Parameters["@time"].Value = ((Excel.Range)workSheet.Cells[3, 2]).Value2.ToString();
//                            else MyCom.Parameters["@time"].Value = "";
//                            if (((Excel.Range)workSheet.Cells[7, 2]).Value2 != null)
//                                MyCom.Parameters["@revision"].Value = ((Excel.Range)workSheet.Cells[7, 2]).Value2.ToString();
//                            else MyCom.Parameters["@revision"].Value = 0;
//                            if (((Excel.Range)workSheet.Cells[8, 2]).Value2 != null)
//                                MyCom.Parameters["@filled"].Value = ((Excel.Range)workSheet.Cells[8, 2]).Value2.ToString();
//                            else MyCom.Parameters["@filled"].Value = "";
//                            if (((Excel.Range)workSheet.Cells[9, 2]).Value2 != null)
//                                MyCom.Parameters["@approved"].Value = ((Excel.Range)workSheet.Cells[9, 2]).Value2.ToString();
//                            else MyCom.Parameters["@approved"].Value = "";
//                        }
//                    try
//                    {
//                        MyCom.ExecuteNonQuery();
//                    }
//                    catch (Exception exp)
//                    {
//                        string str = exp.Message;
//                    }
//                }

//                //Insert into DB (DetailFRM009)
//                int x = 10;
//                MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
//                MyCom.Parameters.Add("@PCode", SqlDbType.Int);
//                MyCom.Parameters.Add("@Consumed", SqlDbType.Real);
//                MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);

//                //read directly and cell by cell
//                foreach (Excel.Worksheet workSheet in book.Worksheets)
//                    if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
//                    {
//                        while (x < (DT.Rows.Count - 2))
//                        {
//                            if (DT.Rows[x][0].ToString() != "")
//                            {
//                                //Detect PackageCode
//                                string Unit = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString().Trim();
//                                if (type == 0)
//                                {
//                                    if (Unit.Contains("G")) Unit = Unit.Replace("G", "Gas");
//                                    else Unit = Unit.Replace("S", "Steam");
//                                }
//                                else
//                                {
//                                    if (Unit.Contains("G")) Unit = Unit.Replace("G", "Gas cc");
//                                    else Unit = Unit.Replace("S", "Steam cc");
//                                }
//                                SqlCommand MyCom1 = new SqlCommand();
//                                MyCom1.Connection = MyConnection;
//                                MyCom1.CommandText = "SELECT @Result=PackageCode FROM UnitsDataMain WHERE UnitCode=@unit AND PPID=@id";
//                                MyCom1.Parameters.Add("@id", SqlDbType.NChar, 10);
//                                MyCom1.Parameters["@id"].Value = PID;
//                                MyCom1.Parameters.Add("@unit", SqlDbType.NChar, 20);
//                                MyCom1.Parameters["@unit"].Value = Unit;
//                                MyCom1.Parameters.Add("@Result", SqlDbType.Int);
//                                MyCom1.Parameters["@Result"].Direction = ParameterDirection.Output;
//                                MyCom1.ExecuteNonQuery();
//                                int PCode = 0;
//                                if (MyCom1.Parameters["@Result"].Value.ToString() != "")
//                                    PCode = int.Parse(MyCom1.Parameters["@Result"].Value.ToString());
//                                else
//                                {
//                                    if (Unit.Contains("G")) Unit = Unit.Replace("Gas", "Gas cc");
//                                    else Unit = Unit.Replace("Steam", "Steam cc");
//                                    MyCom1.CommandText = "SELECT @Result=PackageCode FROM UnitsDataMain WHERE UnitCode=@unit AND PPID=@id";
//                                    MyCom1.Parameters["@id"].Value = PID;
//                                    MyCom1.Parameters["@unit"].Value = Unit;
//                                    MyCom1.ExecuteNonQuery();
//                                    PCode = int.Parse(MyCom1.Parameters["@Result"].Value.ToString());
//                                }

//                                for (int j = 0; j < 24; j++)
//                                {
//                                    if (check == 0)
//                                        MyCom.CommandText = "INSERT INTO [DetailFRM009] (TargetMarketDate,PPID,Block,PPType,PackageCode,Hour" +
//                                        ",Consumed) VALUES (@tdate,@id,@block,@type,@PCode,@hour,@Consumed)";
//                                    else
//                                        MyCom.CommandText = "UPDATE DetailFRM009 SET Consumed=@Consumed WHERE Block=@block " +
//                                        "AND PackageCode=@PCode AND Hour=@hour AND TargetMarketDate=@tdate AND PPID=@id AND PPType=@type";
//                                    MyCom.Parameters["@id"].Value = PID;
//                                    MyCom.Parameters["@tdate"].Value = date;
//                                    MyCom.Parameters["@block"].Value = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString();
//                                    MyCom.Parameters["@PCode"].Value = PCode;
//                                    MyCom.Parameters["@type"].Value = type;
//                                    MyCom.Parameters["@hour"].Value = j + 1;
//                                    if (((Excel.Range)workSheet.Cells[x + 2 + j, 3]).Value2 != null)
//                                        MyCom.Parameters["@Consumed"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 3]).Value2.ToString();
//                                    else MyCom.Parameters["@Consumed"].Value = 0;

//                                    try
//                                    {
//                                        MyCom.ExecuteNonQuery();
//                                    }
//                                    catch (Exception exp)
//                                    {
//                                        string str = exp.Message;
//                                    }
//                                }
//                            }
//                            x++;
//                        }
//                    }


//                book.Close(false, book, Type.Missing);
//                exobj.Workbooks.Close();
//                exobj.Quit();
//                System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
//                MyConnection.Close();
//            }


//        }
//---------------------------------------------------------------------------------------------
    }



}
