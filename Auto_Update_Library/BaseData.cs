﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using NRI.SBS.Common;
using System.Xml.Serialization;
using System.IO;

namespace Auto_Update_Library
{
    public class BaseData
    {
        private static BaseData self = null;

        private BaseData()
        {
            GetDataFromDB();
        }

        public static BaseData GetInstance()
        {
            //if (self == null)
                self = new BaseData();
            return self;
        }

        private string m002Path;

        public string M002Path
        {
            get { return m002Path; }
            set { m002Path = value; }
        }
        private string m005Path;

        public string M005Path
        {
            get { return m005Path; }
            set { m005Path = value; }
        }
        private string m005baPath;

        public string M005baPath
        {
            get { return m005baPath; }
            set { m005baPath = value; }
        }

        private string m009Path;

        public string M009Path
        {
            get { return m009Path; }
            set { m009Path = value; }
        }
        private string m0091Path;

        public string M0091Path
        {
            get { return m0091Path; }
            set { m0091Path = value; }
        }

        private string counterPath;

        public string CounterPath
        {
            get { return counterPath; }
            set { counterPath = value; }
        }

        private string averagePrice;

        public string AveragePrice
        {
            get { return averagePrice; }
            set { averagePrice = value; }
        }

        private string loadForecasting;

        public string LoadForecasting
        {
            get { return loadForecasting; }
            set { loadForecasting = value; }
        }

        private string capacityfactor;

        public string Capacityfactor
        {
            get { return capacityfactor; }
            set { capacityfactor = value; }
        }

        private bool useftp002;

        public bool Useftp002
        {
            get { return useftp002; }
            set { useftp002 = value; }
        }

        private bool useftp005;

        public bool Useftp005
        {
            get { return useftp005; }
            set { useftp005 = value; }
        }

        private bool useftp005ba;

        public bool Useftp005ba
        {
            get { return useftp005ba; }
            set { useftp005ba = value; }
        }
        private bool useftp009;

        public bool Useftp009
        {
            get { return useftp009; }
            set { useftp009 = value; }
        }
        private bool useftp0091;

        public bool Useftp0091
        {
            get { return useftp0091; }
            set { useftp0091 = value; }
        }

        private bool useftpdispatch;

        public bool Useftpdispatch
        {
            get { return useftpdispatch; }
            set { useftpdispatch = value; }
        }

        private bool useftpcounter;

        public bool Useftpcounter
        {
            get { return useftpcounter; }
            set { useftpcounter = value; }
        }


        private bool useftpavg;

        public bool Useftpavg
        {
            get { return useftpavg; }
            set { useftpavg = value; }
        }


        private bool useftpload;

        public bool Useftpload
        {
            get { return useftpload; }
            set { useftpload = value; }
        }

        private bool useftpcapacity;

        public bool Useftpcapacity
        {
            get { return useftpcapacity; }
            set { useftpcapacity = value; }
        }



        private string  dispatchable;

        public string  Dispatchable
        {
            get { return dispatchable; }
            set { dispatchable = value; }
        }



        
        private void GetDataFromDB()
        {
            
            SettingParameters settingParam = new SettingParameters();
            XmlSerializer mySerializer = new XmlSerializer(typeof(SettingParameters));
            try
            {
                string path = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\SBS\\setting.xml";
                FileStream myFileStream = new FileStream(path, FileMode.Open);

                settingParam = (SettingParameters)mySerializer.Deserialize(myFileStream);
                myFileStream.Close();
            }
            catch (System.IO.FileNotFoundException fex)
            {
                System.Windows.Forms.MessageBox.Show(fex.Message);
                throw fex;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                throw ex;
            }
            m005Path = settingParam.M005Path.Trim();
            m005baPath = settingParam.M005Pathba.Trim();
            m002Path = settingParam.M002Path.Trim();
            m009Path = settingParam.M009Path.Trim();
            m0091Path = settingParam.M0091Path.Trim();
            counterPath = settingParam.CounterPath.Trim();
            averagePrice = settingParam.AvgPricePath.Trim();
            loadForecasting = settingParam.LoadForcastingPath.Trim();
            dispatchable = settingParam.DispatchFilePath.Trim();
            capacityfactor = settingParam.CapacityFactorsPath.Trim();


            useftp002 = settingParam.IsFTP002;
            useftp005 = settingParam.IsFTP005;
            useftp005ba = settingParam.IsFTP005ba;
            useftp009 = settingParam.IsFTP009;
            useftp0091 = settingParam.IsFTP0091;
            useftpavg = settingParam.IsFTPavg;
            useftpcapacity = settingParam.IsFTPcapacity;
            useftpcounter = settingParam.IsFTPcounter;
            useftpdispatch = settingParam.IsFTPDispatch;
            useftpload = settingParam.IsFTPload;
          


        }
    }
}
