﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Auto_Update_Library
{
    public class BaseData
    {
        private static BaseData self = null;

        private BaseData()
        {
            GetDataFromDB();
        }

        public static BaseData GetInstance()
        {
            if (self == null)
                self = new BaseData();
            return self;
        }

        private string m002Path;

        public string M002Path
        {
            get { return m002Path; }
            set { m002Path = value; }
        }
        private string m005Path;

        public string M005Path
        {
            get { return m005Path; }
            set { m005Path = value; }
        }

        private void GetDataFromDB()
        {
            string path = "";
            SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;
            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            MyCom.CommandText = "SELECT @add5=M005, @add2=M002 FROM Path";
            MyCom.Parameters.Add("@add5", SqlDbType.NChar, 50);
            MyCom.Parameters["@add5"].Direction = ParameterDirection.Output;
            MyCom.Parameters.Add("@add2", SqlDbType.NChar, 50);
            MyCom.Parameters["@add2"].Direction = ParameterDirection.Output;
            MyCom.ExecuteNonQuery();
            m005Path = MyCom.Parameters["@add5"].Value.ToString().Trim();
            m002Path = MyCom.Parameters["@add2"].Value.ToString().Trim();

        }
    }
}
