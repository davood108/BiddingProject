﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using NRI.DMS.Common;

namespace Auto_Update_Library
{
    public class SingleToneConnectionManager
    {
        private SqlConnection connection = null;

        public SqlConnection Connection
        {
            get { return connection; }
            set { connection = value; }
        }
        private SingleToneConnectionManager()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConnectionManager.ConnectionString;
        }

        private static SingleToneConnectionManager self;

        public static SingleToneConnectionManager GetInstance()
        {
            if (self==null)
                self = new SingleToneConnectionManager();
            if (self.connection.State == System.Data.ConnectionState.Closed)
                self.connection.Open();
            return self;
        }
    }
}
