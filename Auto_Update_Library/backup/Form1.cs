﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;
using System.Net;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
//using Office.Excel;
//using Office.CompoundDocumentFormat;
using System.IO;
using System.Diagnostics;


namespace Auto_Update_Library
{
    public partial class Form1 : Form
    {
        string ConStr = "";
        const int NumPPID = 9;
        string[] PPIDArray = new string[NumPPID];

        public Form1(string ConStr)
        {
            InitializeComponent();
            //ConStr = System.Configuration.ConfigurationManager.ConnectionStrings["AutomaticUpdateInformation"].ConnectionString;
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT PPID FROM PowerPlant", MyConnection);
            Myda.Fill(MyDS, "ppid");
            int i = 0;
            foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
            {
                PPIDArray[i] = MyRow["PPID"].ToString().Trim();
                i++;
            }
            PPIDArray[7] = "132";
            PPIDArray[8] = "145";
        }

//------------------------------M002(string date)------------------------------
        private void M002(string date)
        {
            for (int num002 = 0; num002 < NumPPID; num002++)
            {
                bool IsValid = true;
                //build Name for FRM002 files
                string Doc002 = "";
                Doc002 += PPIDArray[num002];
                //add date to Name
                string mydate = date;
                mydate = mydate.Replace("/", "");
                Doc002 += "-" + mydate;
                //Detect Path
                string path = "";
                SqlConnection MyConnection = new SqlConnection();
                MyConnection.ConnectionString = ConStr;
                MyConnection.Open();
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                MyCom.CommandText = "SELECT @add=M002 FROM Path";
                MyCom.Parameters.Add("@add", SqlDbType.NChar, 50);
                MyCom.Parameters["@add"].Direction = ParameterDirection.Output;
                try
                {
                    MyCom.ExecuteNonQuery();
                    path = MyCom.Parameters["@add"].Value.ToString();
                }
                catch (Exception exp)
                {
                    path = "";
                }

                if (path != "")
                {
                    //read from FRM002.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "Sheet1";
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    try
                    {
                        objAdapter1.Fill(objDataset1);
                        dataGridView1.DataSource = objDataset1.Tables[0].DefaultView;
                    }
                    catch
                    {
                        IsValid = false;
                    }
                    objConn.Close();
                    //IS IT A Valid File?
                    if ((IsValid) && (dataGridView1.Columns[1].HeaderText.Contains("M002")))
                    {
                        //Insert into DB (MainFRM002)
                        Excel.Application exobj = new Excel.Application();
                        Excel.Workbook book = null;
                        book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                        //SqlConnection MyConnection = new SqlConnection();
                        //MyConnection.ConnectionString = ConStr;
                        //MyConnection.Open();
                        //SqlCommand MyCom = new SqlCommand();
                        MyCom.Connection = MyConnection;
                        int type = 0;
                        int PID = 0;
                        string date2 = "";
                        foreach (Excel.Worksheet workSheet in book.Worksheets)
                            if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                            {
                                if ((((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString().Contains("سيكل")) || (((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString().Contains("ccp")))
                                    type = 1;
                                string date1 = ((Excel.Range)workSheet.Cells[4, 2]).Value2.ToString();
                                date2 = date1.Remove(4);
                                date2 += "/";
                                date2 += date1[4];
                                date2 += date1[5];
                                date2 += "/";
                                date2 += date1[6];
                                date2 += date1[7];
                            }

                        MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                        MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                        MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
                        MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
                        MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
                        MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
                        MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
                        MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
                        MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

                        MyCom.CommandText = "INSERT INTO [MainFRM002] (PPID,TargetMarketDate,PPName,PPType,"
                        + "DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
                        foreach (Excel.Worksheet workSheet in book.Worksheets)
                            if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                            {
                                PID = findPPID(((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString());
                                //if ((PID==131)&&(type==1)) PID=132;
                                //if ((PID==144)&&(type==1)) PID=145;
                                MyCom.Parameters["@id"].Value = PID;
                                MyCom.Parameters["@tdate"].Value = date2;
                                MyCom.Parameters["@name"].Value = ((Excel.Range)workSheet.Cells[5, 2]).Value2.ToString();
                                MyCom.Parameters["@type"].Value = type;
                                if (((Excel.Range)workSheet.Cells[2, 2]).Value2 != null)
                                    MyCom.Parameters["@idate"].Value = ((Excel.Range)workSheet.Cells[2, 2]).Value2.ToString();
                                else MyCom.Parameters["@idate"].Value = null;
                                if (((Excel.Range)workSheet.Cells[3, 2]).Value2 != null)
                                    MyCom.Parameters["@time"].Value = ((Excel.Range)workSheet.Cells[3, 2]).Value2.ToString();
                                else MyCom.Parameters["@time"].Value = null;
                                if (((Excel.Range)workSheet.Cells[7, 2]).Value2 != null)
                                    MyCom.Parameters["@revision"].Value = ((Excel.Range)workSheet.Cells[7, 2]).Value2.ToString();
                                else MyCom.Parameters["@revision"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[8, 2]).Value2 != null)
                                    MyCom.Parameters["@filled"].Value = ((Excel.Range)workSheet.Cells[8, 2]).Value2.ToString();
                                else MyCom.Parameters["@filled"].Value = null;
                                if (((Excel.Range)workSheet.Cells[9, 2]).Value2 != null)
                                    MyCom.Parameters["@approved"].Value = ((Excel.Range)workSheet.Cells[9, 2]).Value2.ToString();
                                else MyCom.Parameters["@approved"].Value = null;
                            }
                        try
                        {
                            MyCom.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                        }

                        //Insert into DB (BlockFRM002)
                        int x = 10;
                        MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                        MyCom.Parameters.Add("@peak", SqlDbType.Real);
                        MyCom.Parameters.Add("@max", SqlDbType.Real);
                        while (x < (dataGridView1.Rows.Count - 1))
                        {
                            if (dataGridView1.Rows[x].Cells[0].Value.ToString() != "")
                            {
                                MyCom.CommandText = "INSERT INTO [BlockFRM002] (PPID,TargetMarketDate,PPType,Block,"
                                + "Peak,MaxDailyGeneration) VALUES (@id,@tdate,@type,@block,@peak,@max)";

                                //read directly and cell by cell
                                foreach (Excel.Worksheet workSheet in book.Worksheets)
                                    if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                                    {
                                        MyCom.Parameters["@id"].Value = PID;
                                        MyCom.Parameters["@tdate"].Value = date2;
                                        MyCom.Parameters["@type"].Value = type;
                                        MyCom.Parameters["@block"].Value = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString();
                                        if (((Excel.Range)workSheet.Cells[x + 2, 2]).Value2 != null)
                                            MyCom.Parameters["@peak"].Value = ((Excel.Range)workSheet.Cells[x + 2, 2]).Value2.ToString();
                                        else MyCom.Parameters["@peak"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2, 3]).Value2 != null)
                                            MyCom.Parameters["@max"].Value = ((Excel.Range)workSheet.Cells[x + 2, 3]).Value2.ToString();
                                        else MyCom.Parameters["@max"].Value = 0;
                                    }
                                try
                                {
                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception exp)
                                {
                                    string str = exp.Message;
                                }
                            }
                            x++;
                        }
                        //Insert into DB (DetailFRM002)
                        x = 10;
                        MyCom.Parameters.Add("@deccap", SqlDbType.Real);
                        MyCom.Parameters.Add("@dispachcap", SqlDbType.Real);
                        MyCom.Parameters.Add("@price1", SqlDbType.Real);
                        MyCom.Parameters.Add("@power1", SqlDbType.Real);
                        MyCom.Parameters.Add("@price2", SqlDbType.Real);
                        MyCom.Parameters.Add("@power2", SqlDbType.Real);
                        MyCom.Parameters.Add("@price3", SqlDbType.Real);
                        MyCom.Parameters.Add("@power3", SqlDbType.Real);
                        MyCom.Parameters.Add("@price4", SqlDbType.Real);
                        MyCom.Parameters.Add("@power4", SqlDbType.Real);
                        MyCom.Parameters.Add("@price5", SqlDbType.Real);
                        MyCom.Parameters.Add("@power5", SqlDbType.Real);
                        MyCom.Parameters.Add("@price6", SqlDbType.Real);
                        MyCom.Parameters.Add("@power6", SqlDbType.Real);
                        MyCom.Parameters.Add("@price7", SqlDbType.Real);
                        MyCom.Parameters.Add("@power7", SqlDbType.Real);
                        MyCom.Parameters.Add("@price8", SqlDbType.Real);
                        MyCom.Parameters.Add("@power8", SqlDbType.Real);
                        MyCom.Parameters.Add("@price9", SqlDbType.Real);
                        MyCom.Parameters.Add("@power9", SqlDbType.Real);
                        MyCom.Parameters.Add("@price10", SqlDbType.Real);
                        MyCom.Parameters.Add("@power10", SqlDbType.Real);
                        MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                        while (x < (dataGridView1.Rows.Count - 2))
                        {
                            if (dataGridView1.Rows[x].Cells[0].Value.ToString() != "")
                            {
                                for (int j = 0; j < 24; j++)
                                {
                                    MyCom.CommandText = "INSERT INTO [DetailFRM002] (TargetMarketDate,PPID,Block,PPType,Hour" +
                                    ",DeclaredCapacity,DispachableCapacity,Power1,Price1,Power2,Price2,Power3,Price3,Power4," +
                                    "Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10," +
                                    "Price10) VALUES (@tdate,@id,@block,@type,@hour,@deccap,@dispachcap,@power1," +
                                    "@price1,@power2,@price2,@power3,@price3,@power4,@price4,@power5,@price5,@power6,@price6," +
                                    "@power7,@price7,@power8,@price8,@power9,@price9,@power10,@price10)";

                                    //read directly and cell by cell
                                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                                        if ((workSheet.Name == "sheet1") || (workSheet.Name == "Sheet1"))
                                        {

                                            MyCom.Parameters["@id"].Value = PID;
                                            MyCom.Parameters["@tdate"].Value = date2;
                                            MyCom.Parameters["@block"].Value = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString();
                                            MyCom.Parameters["@type"].Value = type;
                                            MyCom.Parameters["@hour"].Value = j + 1;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 5]).Value2 != null)
                                                MyCom.Parameters["@deccap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 5]).Value2.ToString();
                                            else MyCom.Parameters["@deccap"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2 != null)
                                                MyCom.Parameters["@dispachcap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2.ToString();
                                            else MyCom.Parameters["@dispachcap"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2 != null)
                                                MyCom.Parameters["@power1"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2.ToString();
                                            else MyCom.Parameters["@power1"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 8]).Value2 != null)
                                                MyCom.Parameters["@price1"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 8]).Value2.ToString();
                                            else MyCom.Parameters["@price1"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 9]).Value2 != null)
                                                MyCom.Parameters["@power2"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 9]).Value2.ToString();
                                            else MyCom.Parameters["@power2"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 10]).Value2 != null)
                                                MyCom.Parameters["@price2"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 10]).Value2.ToString();
                                            else MyCom.Parameters["@price2"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 11]).Value2 != null)
                                                MyCom.Parameters["@power3"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 11]).Value2.ToString();
                                            else MyCom.Parameters["@power3"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 12]).Value2 != null)
                                                MyCom.Parameters["@price3"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 12]).Value2.ToString();
                                            else MyCom.Parameters["@price3"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 13]).Value2 != null)
                                                MyCom.Parameters["@power4"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 13]).Value2.ToString();
                                            else MyCom.Parameters["@power4"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 14]).Value2 != null)
                                                MyCom.Parameters["@price4"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 14]).Value2.ToString();
                                            else MyCom.Parameters["@price4"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 15]).Value2 != null)
                                                MyCom.Parameters["@power5"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 15]).Value2.ToString();
                                            else MyCom.Parameters["@power5"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 16]).Value2 != null)
                                                MyCom.Parameters["@price5"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 16]).Value2.ToString();
                                            else MyCom.Parameters["@price5"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 17]).Value2 != null)
                                                MyCom.Parameters["@power6"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 17]).Value2.ToString();
                                            else MyCom.Parameters["@power6"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 18]).Value2 != null)
                                                MyCom.Parameters["@price6"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 18]).Value2.ToString();
                                            else MyCom.Parameters["@price6"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 19]).Value2 != null)
                                                MyCom.Parameters["@power7"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 19]).Value2.ToString();
                                            else MyCom.Parameters["@power7"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 20]).Value2 != null)
                                                MyCom.Parameters["@price7"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 20]).Value2.ToString();
                                            else MyCom.Parameters["@price7"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 21]).Value2 != null)
                                                MyCom.Parameters["@power8"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 21]).Value2.ToString();
                                            else MyCom.Parameters["@power8"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 22]).Value2 != null)
                                                MyCom.Parameters["@price8"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 22]).Value2.ToString();
                                            else MyCom.Parameters["@price8"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 23]).Value2 != null)
                                                MyCom.Parameters["@power9"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 23]).Value2.ToString();
                                            else MyCom.Parameters["@power9"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 24]).Value2 != null)
                                                MyCom.Parameters["@price9"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 24]).Value2.ToString();
                                            else MyCom.Parameters["@price9"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 25]).Value2 != null)
                                                MyCom.Parameters["@power10"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 25]).Value2.ToString();
                                            else MyCom.Parameters["@power10"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2 + j, 26]).Value2 != null)
                                                MyCom.Parameters["@price10"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 26]).Value2.ToString();
                                            else MyCom.Parameters["@price10"].Value = 0;
                                        }
                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        string str = exp.Message;
                                    }
                                }
                            }
                            x++;
                        }

                        MyConnection.Close();
                        book.Close(false, book, Type.Missing);
                    }
                    else MessageBox.Show("Selected File Isnot Valid!");
                }
            }
        }
//--------------------------M005(string date)----------------------------------------
        private void M005(string date)
        {
            for (int num = 0; num < NumPPID; num++)
            {
                bool IsValid = true;
                //build Nmae for FRM005 files
                string DocName = "005-";
                DocName += PPIDArray[num];
                //add date to Name
                string mydate = date;
                mydate = mydate.Replace("/", "");
                DocName += "-" + mydate;
                //Detect Path
                string path = "";
                SqlConnection MyConnection = new SqlConnection();
                MyConnection.ConnectionString = ConStr;
                MyConnection.Open();
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                MyCom.CommandText = "SELECT @add=M005 FROM Path";
                MyCom.Parameters.Add("@add", SqlDbType.NChar, 50);
                MyCom.Parameters["@add"].Direction = ParameterDirection.Output;
                try
                {
                    MyCom.ExecuteNonQuery();
                    path = MyCom.Parameters["@add"].Value.ToString();
                }
                catch (Exception exp)
                {
                    path = "";
                }

                if (path != "")
                {
                    //Is It a Valid File?
                    if (path.Contains("005"))
                    {
                        //read from FRM005.xls into datagridview
                        String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                        OleDbConnection objConn = new OleDbConnection(sConnectionString);
                        objConn.Open();
                        string price = "FRM005";
                        OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                        objAdapter1.SelectCommand = objCmdSelect;
                        DataSet objDataset1 = new DataSet();
                        objAdapter1.Fill(objDataset1);
                        dataGridView1.DataSource = objDataset1.Tables[0].DefaultView;
                        objConn.Close();

                        //Insert into DB (MainFRM005)
                        Excel.Application exobj = new Excel.Application();
                        Excel.Workbook book = null;
                        book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                        //SqlConnection MyConnection = new SqlConnection();
                        //MyConnection.ConnectionString = ConStr;
                        //MyConnection.Open();
                        //SqlCommand MyCom = new SqlCommand();
                        MyCom.Connection = MyConnection;
                        int type = 0;
                        int PID = 0;
                        if ((dataGridView1.Rows[3].Cells[1].Value.ToString().Contains("سيكل")) || (dataGridView1.Rows[3].Cells[1].Value.ToString().Contains("ccp")))
                            type = 1;
                        MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                        MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                        MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
                        MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
                        MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
                        MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
                        MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
                        MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
                        MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

                        MyCom.CommandText = "INSERT INTO [MainFRM005] (PPID,TargetMarketDate,PPName,PPType,"
                        + "DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
                        PID = findPPID(dataGridView1.Rows[3].Cells[1].Value.ToString());
                        //if ((PID==131)&&(type==1)) PID=132;
                        //if ((PID==144)&&(type==1)) PID=145;
                        MyCom.Parameters["@id"].Value = PID;
                        MyCom.Parameters["@tdate"].Value = dataGridView1.Rows[2].Cells[1].Value.ToString();
                        MyCom.Parameters["@name"].Value = dataGridView1.Rows[3].Cells[1].Value.ToString();
                        MyCom.Parameters["@type"].Value = type;
                        MyCom.Parameters["@idate"].Value = dataGridView1.Rows[0].Cells[1].Value.ToString();
                        MyCom.Parameters["@time"].Value = dataGridView1.Rows[1].Cells[1].Value.ToString();
                        MyCom.Parameters["@revision"].Value = dataGridView1.Rows[4].Cells[1].Value.ToString();
                        MyCom.Parameters["@filled"].Value = dataGridView1.Rows[5].Cells[1].Value.ToString();
                        MyCom.Parameters["@approved"].Value = dataGridView1.Rows[6].Cells[1].Value.ToString();

                        try
                        {
                            MyCom.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                        }

                        //Insert into DB (BlockFRM005)
                        int x = 10;
                        MyCom.Parameters.Add("@num", SqlDbType.NChar, 20);
                        MyCom.Parameters.Add("@prequired", SqlDbType.Real);
                        MyCom.Parameters.Add("@pdispach", SqlDbType.Real);
                        MyCom.Parameters.Add("@drequierd", SqlDbType.Real);
                        MyCom.Parameters.Add("@ddispach", SqlDbType.Real);
                        while (x < (dataGridView1.Rows.Count - 1))
                        {
                            if (dataGridView1.Rows[x].Cells[0].Value.ToString() != "")
                            {
                                MyCom.CommandText = "INSERT INTO [BlockFRM005] (PPID,TargetMarketDate,PPType,Block,"
                                + "PeakRequired,PeakDispatchable,DailyTotalRequired,DailyTotalDispachable)"
                                + "VALUES (@id,@tdate,@type,@num,@prequired,@pdispach,@drequierd,@ddispach)";

                                MyCom.Parameters["@id"].Value = PID;
                                MyCom.Parameters["@tdate"].Value = dataGridView1.Rows[2].Cells[1].Value.ToString();
                                MyCom.Parameters["@num"].Value = dataGridView1.Rows[x].Cells[0].Value.ToString();
                                MyCom.Parameters["@type"].Value = type;
                                //read directly and cell by cell
                                foreach (Excel.Worksheet workSheet in book.Worksheets)
                                    if (workSheet.Name == "FRM005")
                                    {
                                        if (((Excel.Range)workSheet.Cells[x + 2, 27]).Value2 != null)
                                            MyCom.Parameters["@prequired"].Value = ((Excel.Range)workSheet.Cells[x + 2, 27]).Value2.ToString();
                                        else MyCom.Parameters["@prequired"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 3, 27]).Value2 != null)
                                            MyCom.Parameters["@pdispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, 27]).Value2.ToString();
                                        else MyCom.Parameters["@pdispach"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2, 28]).Value2 != null)
                                            MyCom.Parameters["@drequierd"].Value = ((Excel.Range)workSheet.Cells[x + 2, 28]).Value2.ToString();
                                        else MyCom.Parameters["@drequierd"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 3, 28]).Value2 != null)
                                            MyCom.Parameters["@ddispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, 28]).Value2.ToString();
                                        else MyCom.Parameters["@ddispach"].Value = 0;
                                    }
                                try
                                {
                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception exp)
                                {
                                    string str = exp.Message;
                                }
                            }
                            x++;
                        }
                        //Insert into DB (DetailFRM005)
                        x = 10;
                        MyCom.Parameters.Add("@required", SqlDbType.Real);
                        MyCom.Parameters.Add("@dispach", SqlDbType.Real);
                        MyCom.Parameters.Add("@contribution", SqlDbType.Char, 2);
                        MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                        while (x < (dataGridView1.Rows.Count - 2))
                        {
                            if (dataGridView1.Rows[x].Cells[0].Value.ToString() != "")
                            {
                                for (int j = 0; j < 24; j++)
                                {
                                    MyCom.CommandText = "INSERT INTO [DetailFRM005] (TargetMarketDate,PPID,Block,PPType,Hour,Required,Dispatchable,Contribution) VALUES (@tdate,@id,@num,@type,@hour,@required,@dispach,@contribution)";

                                    MyCom.Parameters["@id"].Value = PID;
                                    MyCom.Parameters["@tdate"].Value = dataGridView1.Rows[2].Cells[1].Value.ToString();
                                    MyCom.Parameters["@num"].Value = dataGridView1.Rows[x].Cells[0].Value.ToString();
                                    MyCom.Parameters["@type"].Value = type;

                                    //read directly and cell by cell
                                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                                        if (workSheet.Name == "FRM005")
                                        {
                                            MyCom.Parameters["@hour"].Value = j + 1;
                                            if (((Excel.Range)workSheet.Cells[x + 2, j + 3]).Value2 != null)
                                                MyCom.Parameters["@required"].Value = ((Excel.Range)workSheet.Cells[x + 2, j + 3]).Value2.ToString();
                                            else MyCom.Parameters["@required"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 3, j + 3]).Value2 != null)
                                                MyCom.Parameters["@dispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, j + 3]).Value2.ToString();
                                            else MyCom.Parameters["@dispach"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2 != null)
                                                MyCom.Parameters["@contribution"].Value = ((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2.ToString();
                                            else MyCom.Parameters["@contribution"].Value = null;
                                        }
                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        string str = exp.Message;
                                    }
                                }
                            }
                            x++;
                        }
                        MyConnection.Close();
                        book.Close(false, book, Type.Missing);
                    }
                    else MessageBox.Show("Selected File Isnot Valid!");
                }
            }
        }
//------------------------------------AveragePrice(string date)--------------------------------
        private void AveragePrice(string date)
        {
            //detect current date and hour
            System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
            DateTime CurDate = DateTime.Now;
            string day = pr.GetDayOfMonth(CurDate).ToString();
            if (int.Parse(day) < 10) day = "0" + day;
            string mydate = pr.GetYear(CurDate) + "/" + pr.GetMonth(CurDate) + "/" + day;

            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();
            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;

            string path = @"c:\data\AveragePrice.xls"; ;
            if (date==mydate)
            {
            //Downloading AveragePrice.xls from website
            try
            {
                WebProxy proxy = new WebProxy("http://172.16.1.100:8080");
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                WebClient client = new WebClient();
                client.Proxy = proxy;
                client.DownloadFile("http://www.igmc.ir/pmt/usrFiles/nprice.xls", @"c:\data\AveragePrice.xls");
            }
            catch (Exception exp)
            {
                string str = exp.Message;
            }
            }
            else
            {
                MyCom.CommandText = "SELECT @add=LoadForecasting FROM Path";
                MyCom.Parameters.Add("@add", SqlDbType.NChar, 50);
                MyCom.Parameters["@add"].Direction = ParameterDirection.Output;
                try
                {
                    MyCom.ExecuteNonQuery();
                    path = MyCom.Parameters["@add"].Value.ToString();
                }
                catch (Exception exp)
                {
                    path = "";
                }
            }
            if (path != "")
            {
                //Save AS AveragePrice.xls file
            Excel.Application exobj1 = new Excel.Application();
            Excel.Workbook book1 = null;
            book1 = exobj1.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            book1.Save();
            book1.Close(true, book1, Type.Missing);

            //read from AveragePrice.xls into datagridview
            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\\data\\AveragePrice.xls;Extended Properties=Excel 8.0";
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            objConn.Open();
            string price = "قيمت ";
            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price+"$]", objConn);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
            objAdapter1.SelectCommand = objCmdSelect;
            DataSet objDataset1 = new DataSet();
            objAdapter1.Fill (objDataset1);
            dataGridView1.DataSource = objDataset1.Tables[0].DefaultView;
            objConn.Close();

            //Insert into DB (AveragePrice)
            MyCom.Parameters.Add("@date1", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
            MyCom.Parameters.Add("@Pmin", SqlDbType.Int);
            MyCom.Parameters.Add("@Pmax", SqlDbType.Int);
            MyCom.Parameters.Add("@Amin", SqlDbType.Int);
            MyCom.Parameters.Add("@Amax", SqlDbType.Int);
            MyCom.Parameters.Add("@Aav", SqlDbType.Int);
            for (int i = 0; i < 24; i++)
            {
                MyCom.CommandText = "INSERT INTO [AveragePrice] (Date,Hour,ProposedMin,ProposedMax,AcceptedMin,AcceptedMax,AcceptedAverage) VALUES (@date1,@hour,@Pmin,@Pmax,@Amin,@Amax,@Aav)";
                MyCom.Parameters["@date1"].Value = dataGridView1.Columns[0].HeaderText.ToString();
                MyCom.Parameters["@hour"].Value = dataGridView1.Rows[i+3].Cells[0].Value.ToString();
                MyCom.Parameters["@Pmin"].Value = dataGridView1.Rows[i+3].Cells[1].Value.ToString();
                MyCom.Parameters["@Pmax"].Value = dataGridView1.Rows[i+3].Cells[2].Value.ToString();
                MyCom.Parameters["@Amin"].Value = dataGridView1.Rows[i+3].Cells[3].Value.ToString();
                MyCom.Parameters["@Amax"].Value = dataGridView1.Rows[i+3].Cells[4].Value.ToString();
                MyCom.Parameters["@Aav"].Value = dataGridView1.Rows[i+3].Cells[5].Value.ToString();
                try
                {
                    MyCom.ExecuteNonQuery();
                }
                catch (Exception exp)
                {
                    string str = exp.Message;
                }
             
            }
            }
        }
//-------------------------------------LoadForecasting(string date)------------------------------
        private void LoadForecasting(string Ldate)
        {
            //detect current date and hour
            System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
            DateTime CurDate = DateTime.Now;
            string day = pr.GetDayOfMonth(CurDate).ToString();
            if (int.Parse(day) < 10) day = "0" + day;
            string mydate = pr.GetYear(CurDate) + "/" + pr.GetMonth(CurDate) + "/" + day;

            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();
            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;

            string path1 = @"c:\data\LoadForecasting.xls";
            if (Ldate == mydate)
            {
                //Downloading LoadForecasting.xls from website
                try
                {
                    WebProxy proxy = new WebProxy("http://172.16.1.100:8080");
                    proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    WebClient client = new WebClient();
                    client.Proxy = proxy;
                    client.DownloadFile("http://www.igmc.ir/pmt/usrFiles/load.xls", @"c:\data\LoadForecasting.xls");
                }
                catch (Exception exp)
                {
                    string str = exp.Message;
                }
            }
            else
            {
                MyCom.CommandText = "SELECT @add=LoadForecasting FROM Path";
                MyCom.Parameters.Add("@add", SqlDbType.NChar, 50);
                MyCom.Parameters["@add"].Direction = ParameterDirection.Output;
                try
                {
                    MyCom.ExecuteNonQuery();
                    path1 = MyCom.Parameters["@add"].Value.ToString();
                }
                catch (Exception exp)
                {
                    path1 = "";
                }
            }
            if (path1 != "")
            {
                //Save AS LoadForecasting.xls file
                Excel.Application exobj = new Excel.Application();
                Excel.Workbook book = null;
                book = exobj.Workbooks.Open(path1, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                book.Save();
                book.Close(true, book, Type.Missing);

                //read from LoadForecasting.xls into datagridview
                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\\data\\LoadForecasting.xls;Extended Properties=Excel 8.0";
                OleDbConnection objConn = new OleDbConnection(sConnectionString);
                objConn.Open();
                string price = "Lfoc";
                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                objAdapter1.SelectCommand = objCmdSelect;
                DataSet objDataset1 = new DataSet();
                objAdapter1.Fill(objDataset1);
                dataGridView1.DataSource = objDataset1.Tables[0].DefaultView;
                objConn.Close();

                //read from LoadForecasting.xls into strings
                string edate = "";
                string[] date = new string[4];
                book = exobj.Workbooks.Open(path1, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                foreach (Excel.Worksheet workSheet in book.Worksheets)
                    if (workSheet.Name == "Lfoc")
                    {
                        edate = ((Excel.Range)workSheet.Cells[3, 4]).Value2.ToString();
                        date[0] = ((Excel.Range)workSheet.Cells[4, 3]).Value2.ToString();
                        date[1] = ((Excel.Range)workSheet.Cells[4, 4]).Value2.ToString();
                        date[2] = ((Excel.Range)workSheet.Cells[4, 5]).Value2.ToString();
                        date[3] = ((Excel.Range)workSheet.Cells[4, 6]).Value2.ToString();
                    }
                book.Close(false, book, Type.Missing);

                //Insert into DB (LoadForecasting)
                MyCom.Parameters.Add("@date11", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@edate", SqlDbType.Char, 10);
                MyCom.Parameters.Add("@peak", SqlDbType.Real);
                MyCom.Parameters.Add("@h1", SqlDbType.Real);
                MyCom.Parameters.Add("@h2", SqlDbType.Real);
                MyCom.Parameters.Add("@h3", SqlDbType.Real);
                MyCom.Parameters.Add("@h4", SqlDbType.Real);
                MyCom.Parameters.Add("@h5", SqlDbType.Real);
                MyCom.Parameters.Add("@h6", SqlDbType.Real);
                MyCom.Parameters.Add("@h7", SqlDbType.Real);
                MyCom.Parameters.Add("@h8", SqlDbType.Real);
                MyCom.Parameters.Add("@h9", SqlDbType.Real);
                MyCom.Parameters.Add("@h10", SqlDbType.Real);
                MyCom.Parameters.Add("@h11", SqlDbType.Real);
                MyCom.Parameters.Add("@h12", SqlDbType.Real);
                MyCom.Parameters.Add("@h13", SqlDbType.Real);
                MyCom.Parameters.Add("@h14", SqlDbType.Real);
                MyCom.Parameters.Add("@h15", SqlDbType.Real);
                MyCom.Parameters.Add("@h16", SqlDbType.Real);
                MyCom.Parameters.Add("@h17", SqlDbType.Real);
                MyCom.Parameters.Add("@h18", SqlDbType.Real);
                MyCom.Parameters.Add("@h19", SqlDbType.Real);
                MyCom.Parameters.Add("@h20", SqlDbType.Real);
                MyCom.Parameters.Add("@h21", SqlDbType.Real);
                MyCom.Parameters.Add("@h22", SqlDbType.Real);
                MyCom.Parameters.Add("@h23", SqlDbType.Real);
                MyCom.Parameters.Add("@h24", SqlDbType.Real);

                for (int z = 0; z < 4; z++)
                {
                    MyCom.CommandText = "INSERT INTO [LoadForecasting] (Date,DateEstimate,Peak,Hour1,Hour2,Hour3,Hour4," +
                    "Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19" +
                    ",Hour20,Hour21,Hour22,Hour23,Hour24) VALUES (@date11,@edate,@peak,@h1,@h2,@h3,@h4,@h5,@h6,@h7,@h8" +
                    ",@h9,@h10,@h11,@h12,@h13,@h14,@h15,@h16,@h17,@h18,@h19,@h20,@h21,@h22,@h23,@h24)";
                    MyCom.Parameters["@date11"].Value = date[z];
                    MyCom.Parameters["@edate"].Value = edate;
                    MyCom.Parameters["@peak"].Value = dataGridView1.Rows[28].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h1"].Value = dataGridView1.Rows[4].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h2"].Value = dataGridView1.Rows[5].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h3"].Value = dataGridView1.Rows[6].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h4"].Value = dataGridView1.Rows[7].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h5"].Value = dataGridView1.Rows[8].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h6"].Value = dataGridView1.Rows[9].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h7"].Value = dataGridView1.Rows[10].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h8"].Value = dataGridView1.Rows[11].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h9"].Value = dataGridView1.Rows[12].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h10"].Value = dataGridView1.Rows[13].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h11"].Value = dataGridView1.Rows[14].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h12"].Value = dataGridView1.Rows[15].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h13"].Value = dataGridView1.Rows[16].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h14"].Value = dataGridView1.Rows[17].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h15"].Value = dataGridView1.Rows[18].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h16"].Value = dataGridView1.Rows[19].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h17"].Value = dataGridView1.Rows[20].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h18"].Value = dataGridView1.Rows[21].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h19"].Value = dataGridView1.Rows[22].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h20"].Value = dataGridView1.Rows[23].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h21"].Value = dataGridView1.Rows[24].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h22"].Value = dataGridView1.Rows[25].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h23"].Value = dataGridView1.Rows[26].Cells[2 + z].Value.ToString();
                    MyCom.Parameters["@h24"].Value = dataGridView1.Rows[27].Cells[2 + z].Value.ToString();

                    try
                    {
                        MyCom.ExecuteNonQuery();
                    }
                    catch (Exception exp)
                    {
                        string str = exp.Message;
                    }
                }
            }

        }
//-----------------------------Rep12Page(string date)----------------------------------
        private void Rep12Page(string date)
        {
            string Rdate = date;
            Rdate = Rdate.Replace("/", "");
            string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/Rep12Pages.htm";
            //Downloading DetailedReports.xls from website
            try
            {
                WebProxy proxy = new WebProxy("http://172.16.1.100:8080");
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                WebClient client = new WebClient();
                client.Proxy = proxy;
                client.DownloadFile(address, @"c:\data\DetailedReports.xls");
            }
            catch (Exception exp)
            {
                string str = exp.Message;
            }

            //Save AS DetailedReports.xls file
            string path1 = @"c:\data\DetailedReports.xls";
            Excel.Application exobj = new Excel.Application();
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path1, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            book.Save();
            book.Close(true, book, Type.Missing);

            //read from DetailedReports.xls into datagridview
            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\\data\\DetailedReports.xls;Extended Properties=Excel 8.0";
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            objConn.Open();
            string price = "گزارش 12 برگي";
            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
            objAdapter1.SelectCommand = objCmdSelect;
            DataSet objDataset1 = new DataSet();
            objAdapter1.Fill(objDataset1);
            dataGridView1.DataSource = objDataset1.Tables[0].DefaultView;
            objConn.Close();

            //Insert into DB (Rep12Page)
            //string path1 = @"c:\data\DetailedReports.xls";
            //Excel.Application exobj = new Excel.Application();
            //Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path1, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();
            SqlCommand MyCom = new SqlCommand();

            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@utype", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@ucode", SqlDbType.SmallInt);
            MyCom.Parameters.Add("@pp", SqlDbType.Real);
            MyCom.Parameters.Add("@ep", SqlDbType.Real);
            MyCom.Parameters.Add("@producable", SqlDbType.Real);
            MyCom.Parameters.Add("@produced", SqlDbType.Real);
            MyCom.Parameters.Add("@dynamicstore", SqlDbType.Real);
            MyCom.Parameters.Add("@store", SqlDbType.Real);
            MyCom.Parameters.Add("@limit", SqlDbType.Real);
            MyCom.Parameters.Add("@program", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@code", SqlDbType.NChar, 10);

            foreach (Excel.Worksheet workSheet in book.Worksheets)
                if (workSheet.Name == "گزارش 12 برگي")
                {
                    string mydate = ((Excel.Range)workSheet.Cells[3, 6]).Value2.ToString();
                    mydate = mydate.Remove(0, (mydate.Length - 10));
                    int row = 3;
                    while (row < 708)
                    {
                        if ((((Excel.Range)workSheet.Cells[row, 1]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString().Contains("كد")) && (((Excel.Range)workSheet.Cells[row, 14]).Value2 != null))
                        {
                            string code=((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString();
                            do
                            {
                                MyCom.CommandText = "INSERT INTO [Rep12Page] (Date,PPCode,UnitType," +
                                "UnitCode,PracticalPower,ExpectedPower,Producable,Produced,DynamicStore," +
                                "Store,Limitation,Program,Code) VALUES (@date,@id,@utype,@ucode,@pp,@ep," +
                                "@producable,@produced,@dynamicstore,@store,@limit,@program,@code)";
                                
                                MyCom.Connection = MyConnection;

                                MyCom.Parameters["@date"].Value = mydate;
                                MyCom.Parameters["@id"].Value = code;
                                MyCom.Parameters["@utype"].Value = ((Excel.Range)workSheet.Cells[row, 3]).Value2.ToString();
                                MyCom.Parameters["@ucode"].Value = ((Excel.Range)workSheet.Cells[row, 4]).Value2.ToString();
                                MyCom.Parameters["@pp"].Value = ((Excel.Range)workSheet.Cells[row, 5]).Value2.ToString();
                                MyCom.Parameters["@ep"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();
                                MyCom.Parameters["@producable"].Value = ((Excel.Range)workSheet.Cells[row, 7]).Value2.ToString();
                                MyCom.Parameters["@produced"].Value = ((Excel.Range)workSheet.Cells[row, 8]).Value2.ToString();
                                if (((Excel.Range)workSheet.Cells[row, 9]).Value2.ToString() != "")
                                    MyCom.Parameters["@dynamicstore"].Value = ((Excel.Range)workSheet.Cells[row, 9]).Value2.ToString();
                                else MyCom.Parameters["@dynamicstore"].Value = 0;
                                if (((Excel.Range)workSheet.Cells[row, 10]).Value2.ToString() != "")
                                    MyCom.Parameters["@store"].Value = ((Excel.Range)workSheet.Cells[row, 10]).Value2.ToString();
                                else MyCom.Parameters["@store"].Value = 0;
                                MyCom.Parameters["@limit"].Value = ((Excel.Range)workSheet.Cells[row, 11]).Value2.ToString();
                                MyCom.Parameters["@program"].Value = ((Excel.Range)workSheet.Cells[row, 13]).Value2.ToString();
                                MyCom.Parameters["@code"].Value = ((Excel.Range)workSheet.Cells[row, 14]).Value2.ToString();

                                try
                                {
                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception exp)
                                {
                                    string str = exp.Message;
                                }

                                row++;
                            } while ((((Excel.Range)workSheet.Cells[row, 2]).Value2 == null)&&(((Excel.Range)workSheet.Cells[row, 1]).Value2==null));
                        }
                        row++;
                    }
                }

            book.Close(true, book, Type.Missing);
        }
//---------------------------Outage(string date)------------------------
        private void Outage(string date)
        {
            string Rdate = date;
            Rdate = Rdate.Replace("/", "");
            string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/Outage_files/sheet001.htm";
            //Downloading outage.xls from website
            try
            {
                WebProxy proxy = new WebProxy("http://172.16.1.100:8080");
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                WebClient client = new WebClient();
                client.Proxy = proxy;
                client.DownloadFile(address, @"c:\data\outage.xls");
            }
            catch (Exception exp)
            {
                string str = exp.Message;
            }
            //remove <link and saveas file
            StringBuilder newFileoutage = new StringBuilder();
            string tempoutage = "";
            string[] fileoutage = File.ReadAllLines(@"C:\data\outage.xls", Encoding.Default);
            foreach (string line in fileoutage)
            {
                if (line.Contains("<link"))
                {
                    tempoutage = line.Remove(0);
                    newFileoutage.Append(tempoutage + "\r\n");
                    continue;
                }
                newFileoutage.Append(line + "\r\n");
            }
            File.WriteAllText(@"C:\data\outage.xls", newFileoutage.ToString(), Encoding.Default);
            //Insert into DB (Outage)
            string path1 = @"c:\data\outage.xls";
            Excel.Application exobj = new Excel.Application();
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path1, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();
            SqlCommand MyCom = new SqlCommand();
                
            MyCom.Parameters.Add("@out_date", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@out_ppcode", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@out_utype", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@out_ucode", SqlDbType.SmallInt);
            MyCom.Parameters.Add("@out_pp", SqlDbType.Real);
            MyCom.Parameters.Add("@out_ep", SqlDbType.Real);
            MyCom.Parameters.Add("@out_program", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@out_code", SqlDbType.NChar, 10);

            foreach (Excel.Worksheet workSheet in book.Worksheets)
                if (workSheet.Name == "outage")
                {
                    string mydate = ((Excel.Range)workSheet.Cells[3, 4]).Value2.ToString();
                    mydate = mydate.Remove(0, (mydate.Length - 10));
                    int row = 3;
                    while (row < 240)
                    {
                        if ((((Excel.Range)workSheet.Cells[row, 2]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString().Contains("کد")))
                        {
                            MyCom.CommandText = "INSERT INTO [OutageUnits] (Date,PPCode,UnitType," +
                            "UnitCode,PracticalPower,ExpectedPower,Program,Code) VALUES" +
                            "(@out_date,@out_ppcode,@out_utype,@out_ucode,@out_pp,"+
                            "@out_ep,@out_program,@out_code)";

                            MyCom.Connection = MyConnection;
                            MyCom.Parameters["@out_date"].Value = mydate;
                            MyCom.Parameters["@out_ppcode"].Value = ((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString();
                            MyCom.Parameters["@out_utype"].Value = ((Excel.Range)workSheet.Cells[row, 4]).Value2.ToString();
                            MyCom.Parameters["@out_ucode"].Value = ((Excel.Range)workSheet.Cells[row, 5]).Value2.ToString();
                            MyCom.Parameters["@out_pp"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();
                            MyCom.Parameters["@out_ep"].Value = ((Excel.Range)workSheet.Cells[row, 7]).Value2.ToString();
                            MyCom.Parameters["@out_program"].Value = ((Excel.Range)workSheet.Cells[row, 9]).Value2.ToString();
                            MyCom.Parameters["@out_code"].Value = ((Excel.Range)workSheet.Cells[row, 10]).Value2.ToString();

                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                            }

                        }
                        row++;
                    }
                }
            book.Close(true, book, Type.Missing);
        }
//---------------------------------Manategh(string date)---------------------------
        private void Manategh(string date)
        {
            string Rdate = date;
            Rdate = Rdate.Replace("/", "");
            string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/Manat_files/sheet001.htm";
            //Downloading manategh.xls from website
            try
            {
                WebProxy proxy = new WebProxy("http://172.16.1.100:8080");
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                WebClient client = new WebClient();
                client.Proxy = proxy;
                client.DownloadFile(address, @"c:\data\manategh.xls");
            }
            catch (Exception exp)
            {
                string str = exp.Message;
            }

            //remove <link and saveas file
            StringBuilder newFilemanategh = new StringBuilder();
            string tempmanategh = "";
            string[] filemanategh = File.ReadAllLines(@"C:\data\manategh.xls", Encoding.Default);
            foreach (string line in filemanategh)
            {
                if (line.Contains("<link"))
                {
                    tempmanategh = line.Remove(0);
                    newFilemanategh.Append(tempmanategh + "\r\n");
                    continue;
                }
                newFilemanategh.Append(line + "\r\n");
            }
            File.WriteAllText(@"C:\data\manategh.xls", newFilemanategh.ToString(), Encoding.Default);

            //Insert into DB (Manategh)
            string path1 = @"c:\data\manategh.xls";
            Excel.Application exobj = new Excel.Application();
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path1, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();
            SqlCommand MyCom = new SqlCommand();

            MyCom.Parameters.Add("@man_date", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@man_code", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@man_title", SqlDbType.NChar, 50);
            MyCom.Parameters.Add("@man_name", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@man_peak", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h1", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h2", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h3", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h4", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h5", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h6", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h7", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h8", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h9", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h10", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h11", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h12", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h13", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h14", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h15", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h16", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h17", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h18", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h19", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h20", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h21", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h22", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h23", SqlDbType.Real);
            MyCom.Parameters.Add("@man_h24", SqlDbType.Real);

            foreach (Excel.Worksheet workSheet in book.Worksheets)
                if (workSheet.Name == "manategh")
                {
                    string mydate = ((Excel.Range)workSheet.Cells[3, 3]).Value2.ToString();
                    int row = 3;
                    bool esc = false;
                    while (row < 117)
                    {
                        if ((((Excel.Range)workSheet.Cells[row, 2]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString().Contains("كد")) )
                        {
                            string code = ((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString();
                            string name = ((Excel.Range)workSheet.Cells[row, 3]).Value2.ToString();
                            do
                            {
                                MyCom.CommandText = "INSERT INTO [Manategh] (Date,Code,"+
                                "Title,Name,Peak,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7"+
                                ",Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,"+
                                "Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23"+
                                ",Hour24) VALUES (@man_date,@man_code,@man_title,@man_name,"+
                                "@man_peak,@man_h1,@man_h2,@man_h3,@man_h4,@man_h5,@man_h6"+
                                ",@man_h7,@man_h8,@man_h9,@man_h10,@man_h11,@man_h12,@man_h13"+
                                ",@man_h14,@man_h15,@man_h16,@man_h17,@man_h18,@man_h19,@man_h20"+
                                ",@man_h21,@man_h22,@man_h23,@man_h24)";

                                MyCom.Connection = MyConnection;

                                MyCom.Parameters["@man_date"].Value = mydate;
                                MyCom.Parameters["@man_code"].Value = code;
                                MyCom.Parameters["@man_name"].Value = name;
                                MyCom.Parameters["@man_title"].Value = ((Excel.Range)workSheet.Cells[row, 4]).Value2.ToString();
                                MyCom.Parameters["@man_peak"].Value = ((Excel.Range)workSheet.Cells[row, 30]).Value2.ToString();
                                MyCom.Parameters["@man_h1"].Value = ((Excel.Range)workSheet.Cells[row, 5]).Value2.ToString();
                                MyCom.Parameters["@man_h2"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();
                                MyCom.Parameters["@man_h3"].Value = ((Excel.Range)workSheet.Cells[row, 7]).Value2.ToString();
                                MyCom.Parameters["@man_h4"].Value = ((Excel.Range)workSheet.Cells[row, 8]).Value2.ToString();
                                MyCom.Parameters["@man_h5"].Value = ((Excel.Range)workSheet.Cells[row, 9]).Value2.ToString();
                                MyCom.Parameters["@man_h6"].Value = ((Excel.Range)workSheet.Cells[row, 10]).Value2.ToString();
                                MyCom.Parameters["@man_h7"].Value = ((Excel.Range)workSheet.Cells[row, 11]).Value2.ToString();
                                MyCom.Parameters["@man_h8"].Value = ((Excel.Range)workSheet.Cells[row, 12]).Value2.ToString();
                                MyCom.Parameters["@man_h9"].Value = ((Excel.Range)workSheet.Cells[row, 13]).Value2.ToString();
                                MyCom.Parameters["@man_h10"].Value = ((Excel.Range)workSheet.Cells[row, 14]).Value2.ToString();
                                MyCom.Parameters["@man_h11"].Value = ((Excel.Range)workSheet.Cells[row, 15]).Value2.ToString();
                                MyCom.Parameters["@man_h12"].Value = ((Excel.Range)workSheet.Cells[row, 16]).Value2.ToString();
                                MyCom.Parameters["@man_h13"].Value = ((Excel.Range)workSheet.Cells[row, 17]).Value2.ToString();
                                MyCom.Parameters["@man_h14"].Value = ((Excel.Range)workSheet.Cells[row, 18]).Value2.ToString();
                                MyCom.Parameters["@man_h15"].Value = ((Excel.Range)workSheet.Cells[row, 19]).Value2.ToString();
                                MyCom.Parameters["@man_h16"].Value = ((Excel.Range)workSheet.Cells[row, 20]).Value2.ToString();
                                MyCom.Parameters["@man_h17"].Value = ((Excel.Range)workSheet.Cells[row, 21]).Value2.ToString();
                                MyCom.Parameters["@man_h18"].Value = ((Excel.Range)workSheet.Cells[row, 22]).Value2.ToString();
                                MyCom.Parameters["@man_h19"].Value = ((Excel.Range)workSheet.Cells[row, 23]).Value2.ToString();
                                MyCom.Parameters["@man_h20"].Value = ((Excel.Range)workSheet.Cells[row, 24]).Value2.ToString();
                                MyCom.Parameters["@man_h21"].Value = ((Excel.Range)workSheet.Cells[row, 25]).Value2.ToString();
                                MyCom.Parameters["@man_h22"].Value = ((Excel.Range)workSheet.Cells[row, 26]).Value2.ToString();
                                MyCom.Parameters["@man_h23"].Value = ((Excel.Range)workSheet.Cells[row, 27]).Value2.ToString();
                                MyCom.Parameters["@man_h24"].Value = ((Excel.Range)workSheet.Cells[row, 28]).Value2.ToString();

                                try
                                {
                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception exp)
                                {
                                    string str = exp.Message;
                                }

                                row++;
                                esc = true;
                            } while ((((Excel.Range)workSheet.Cells[row, 2]).Value2 == null)&&(row<117));

                        }
                        if (!esc) row++;
                        esc = false;
                    }
                }

            book.Close(true, book, Type.Missing);
        }
//----------------------------------ProducedEnergy(string date)-------------------------
        private void ProducedEnergy(string date)
        {
            string Rdate = date;
            Rdate = Rdate.Replace("/", "");
            string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/En_files/sheet001.htm";
            //Downloading produce-energy.xls from website
            try
            {
                WebProxy proxy = new WebProxy("http://172.16.1.100:8080");
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                WebClient client = new WebClient();
                client.Proxy = proxy;
                client.DownloadFile(address, @"c:\data\produce-energy.xls");
            }
            catch (Exception exp)
            {
                string str = exp.Message;
            }
            //remove <link and saveas file
            StringBuilder newFileproduce = new StringBuilder();
            string tempproduce = "";
            string[] fileproduce = File.ReadAllLines(@"C:\data\produce-energy.xls", Encoding.Default);
            foreach (string line in fileproduce)
            {
                if (line.Contains("<link"))
                {
                    tempproduce = line.Remove(0);
                    newFileproduce.Append(tempproduce + "\r\n");
                    continue;
                }
                newFileproduce.Append(line + "\r\n");
            }
            File.WriteAllText(@"C:\data\produce-energy.xls", newFileproduce.ToString(), Encoding.Default);

            //Insert into DB (ProducedEnergy)
            string path1 = @"c:\data\produce-energy.xls";
            Excel.Application exobj = new Excel.Application();
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path1, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();
            SqlCommand MyCom = new SqlCommand();

            MyCom.Parameters.Add("@PE_date", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@PE_code", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@PE_part", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@PE_h1", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h2", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h3", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h4", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h5", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h6", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h7", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h8", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h9", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h10", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h11", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h12", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h13", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h14", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h15", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h16", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h17", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h18", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h19", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h20", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h21", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h22", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h23", SqlDbType.Real);
            MyCom.Parameters.Add("@PE_h24", SqlDbType.Real);

            foreach (Excel.Worksheet workSheet in book.Worksheets)
                if (workSheet.Name == "produce-energy")
                {
                    string mydate = ((Excel.Range)workSheet.Cells[3, 3]).Value2.ToString();
                    int row = 3;
                    while (row < 134)
                    {
                        if ((((Excel.Range)workSheet.Cells[row, 1]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString().Contains("کد")))
                        {
                            MyCom.CommandText = "INSERT INTO [ProducedEnergy] (Date," +
                            "PPCode,Part,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8," +
                            "Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16," +
                            "Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24)" +
                            "VALUES (@PE_date,@PE_code,@PE_part,@PE_h1,@PE_h2,@PE_h3,@PE_h4," +
                            "@PE_h5,@PE_h6,@PE_h7,@PE_h8,@PE_h9,@PE_h10,@PE_h11,@PE_h12," +
                            "@PE_h13,@PE_h14,@PE_h15,@PE_h16,@PE_h17,@PE_h18,@PE_h19," +
                            "@PE_h20,@PE_h21,@PE_h22,@PE_h23,@PE_h24)";

                            MyCom.Connection = MyConnection;

                            MyCom.Parameters["@PE_date"].Value = mydate;
                            MyCom.Parameters["@PE_code"].Value = ((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString();
                            MyCom.Parameters["@PE_part"].Value = ((Excel.Range)workSheet.Cells[row, 4]).Value2.ToString();
                            if (((Excel.Range)workSheet.Cells[row, 5]).Value2 != null)
                                MyCom.Parameters["@PE_h1"].Value = ((Excel.Range)workSheet.Cells[row, 5]).Value2.ToString();
                            else MyCom.Parameters["@PE_h1"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 6]).Value2 != null)
                                MyCom.Parameters["@PE_h2"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();
                            else MyCom.Parameters["@PE_h2"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 7]).Value2 != null)
                                MyCom.Parameters["@PE_h3"].Value = ((Excel.Range)workSheet.Cells[row, 7]).Value2.ToString();
                            else MyCom.Parameters["@PE_h3"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 8]).Value2 != null)
                                MyCom.Parameters["@PE_h4"].Value = ((Excel.Range)workSheet.Cells[row, 8]).Value2.ToString();
                            else MyCom.Parameters["@PE_h4"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 9]).Value2 != null)
                                MyCom.Parameters["@PE_h5"].Value = ((Excel.Range)workSheet.Cells[row, 9]).Value2.ToString();
                            else MyCom.Parameters["@PE_h5"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 10]).Value2 != null)
                                MyCom.Parameters["@PE_h6"].Value = ((Excel.Range)workSheet.Cells[row, 10]).Value2.ToString();
                            else MyCom.Parameters["@PE_h6"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 11]).Value2 != null)
                                MyCom.Parameters["@PE_h7"].Value = ((Excel.Range)workSheet.Cells[row, 11]).Value2.ToString();
                            else MyCom.Parameters["@PE_h7"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 12]).Value2 != null)
                                MyCom.Parameters["@PE_h8"].Value = ((Excel.Range)workSheet.Cells[row, 12]).Value2.ToString();
                            else MyCom.Parameters["@PE_h8"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 13]).Value2 != null)
                                MyCom.Parameters["@PE_h9"].Value = ((Excel.Range)workSheet.Cells[row, 13]).Value2.ToString();
                            else MyCom.Parameters["@PE_h9"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 14]).Value2 != null)
                                MyCom.Parameters["@PE_h10"].Value = ((Excel.Range)workSheet.Cells[row, 14]).Value2.ToString();
                            else MyCom.Parameters["@PE_h10"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 15]).Value2 != null)
                                MyCom.Parameters["@PE_h11"].Value = ((Excel.Range)workSheet.Cells[row, 15]).Value2.ToString();
                            else MyCom.Parameters["@PE_h11"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 16]).Value2 != null)
                                MyCom.Parameters["@PE_h12"].Value = ((Excel.Range)workSheet.Cells[row, 16]).Value2.ToString();
                            else MyCom.Parameters["@PE_h12"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 17]).Value2 != null)
                                MyCom.Parameters["@PE_h13"].Value = ((Excel.Range)workSheet.Cells[row, 17]).Value2.ToString();
                            else MyCom.Parameters["@PE_h13"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 18]).Value2 != null)
                                MyCom.Parameters["@PE_h14"].Value = ((Excel.Range)workSheet.Cells[row, 18]).Value2.ToString();
                            else MyCom.Parameters["@PE_h14"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 19]).Value2 != null)
                                MyCom.Parameters["@PE_h15"].Value = ((Excel.Range)workSheet.Cells[row, 19]).Value2.ToString();
                            else MyCom.Parameters["@PE_h15"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 20]).Value2 != null)
                                MyCom.Parameters["@PE_h16"].Value = ((Excel.Range)workSheet.Cells[row, 20]).Value2.ToString();
                            else MyCom.Parameters["@PE_h16"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 21]).Value2 != null)
                                MyCom.Parameters["@PE_h17"].Value = ((Excel.Range)workSheet.Cells[row, 21]).Value2.ToString();
                            else MyCom.Parameters["@PE_h17"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 22]).Value2 != null)
                                MyCom.Parameters["@PE_h18"].Value = ((Excel.Range)workSheet.Cells[row, 22]).Value2.ToString();
                            else MyCom.Parameters["@PE_h18"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 23]).Value2 != null)
                                MyCom.Parameters["@PE_h19"].Value = ((Excel.Range)workSheet.Cells[row, 23]).Value2.ToString();
                            else MyCom.Parameters["@PE_h19"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 24]).Value2 != null)
                                MyCom.Parameters["@PE_h20"].Value = ((Excel.Range)workSheet.Cells[row, 24]).Value2.ToString();
                            else MyCom.Parameters["@PE_h20"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 25]).Value2 != null)
                                MyCom.Parameters["@PE_h21"].Value = ((Excel.Range)workSheet.Cells[row, 25]).Value2.ToString();
                            else MyCom.Parameters["@PE_h21"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 26]).Value2 != null)
                                MyCom.Parameters["@PE_h22"].Value = ((Excel.Range)workSheet.Cells[row, 26]).Value2.ToString();
                            else MyCom.Parameters["@PE_h22"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 27]).Value2 != null)
                                MyCom.Parameters["@PE_h23"].Value = ((Excel.Range)workSheet.Cells[row, 27]).Value2.ToString();
                            else MyCom.Parameters["@PE_h23"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 28]).Value2 != null)
                                MyCom.Parameters["@PE_h24"].Value = ((Excel.Range)workSheet.Cells[row, 28]).Value2.ToString();
                            else MyCom.Parameters["@PE_h24"].Value = 0;

                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                            }
                        }
                        row++;
                    }
                }
            book.Close(true, book, Type.Missing);
        }
//-------------------------------------InterchangedEnergy(string date)------------------------------
        private void InterchangedEnergy(string date)
        {
            string Rdate = date;
            Rdate = Rdate.Replace("/", "");
            string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/En_files/sheet002.htm";
            //Downloading interchange-energy.xls from website
            try
            {
                WebProxy proxy = new WebProxy("http://172.16.1.100:8080");
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                WebClient client = new WebClient();
                client.Proxy = proxy;
                client.DownloadFile(address, @"c:\data\interchange-energy.xls");
            }
            catch (Exception exp)
            {
                string str = exp.Message;
            }

            //remove <link and saveas file
            StringBuilder newFileinterchange = new StringBuilder();
            string tempinterchange = "";
            string[] fileinterchange = File.ReadAllLines(@"C:\data\interchange-energy.xls", Encoding.Default);
            foreach (string line in fileinterchange)
            {
                if (line.Contains("<link"))
                {
                    tempinterchange = line.Remove(0);
                    newFileinterchange.Append(tempinterchange + "\r\n");
                    continue;
                }
                newFileinterchange.Append(line + "\r\n");
            }
            File.WriteAllText(@"C:\data\interchange-energy.xls", newFileinterchange.ToString(), Encoding.Default);

            //Insert into DB (InterchangedEnergy)
            string path1 = @"c:\data\interchange-energy.xls";
            Excel.Application exobj = new Excel.Application();
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path1, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();
            SqlCommand MyCom = new SqlCommand();

            MyCom.Parameters.Add("@IE_date", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@IE_code", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@IE_name", SqlDbType.NChar, 50);
            MyCom.Parameters.Add("@IE_h1", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h2", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h3", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h4", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h5", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h6", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h7", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h8", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h9", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h10", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h11", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h12", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h13", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h14", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h15", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h16", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h17", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h18", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h19", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h20", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h21", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h22", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h23", SqlDbType.Real);
            MyCom.Parameters.Add("@IE_h24", SqlDbType.Real);

            foreach (Excel.Worksheet workSheet in book.Worksheets)
                if (workSheet.Name == "interchange-energy")
                {
                    string mydate = ((Excel.Range)workSheet.Cells[3, 2]).Value2.ToString();
                    int row = 3;
                    while (row < 142)
                    {
                        if ((((Excel.Range)workSheet.Cells[row, 1]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString().Contains("کد")))
                        {
                            MyCom.CommandText = "INSERT INTO [InterchangedEnergy] (Date," +
                            "Code,Name,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8," +
                            "Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16," +
                            "Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24)" +
                            "VALUES (@IE_date,@IE_code,@IE_name,@IE_h1,@IE_h2,@IE_h3,@IE_h4," +
                            "@IE_h5,@IE_h6,@IE_h7,@IE_h8,@IE_h9,@IE_h10,@IE_h11,@IE_h12," +
                            "@IE_h13,@IE_h14,@IE_h15,@IE_h16,@IE_h17,@IE_h18,@IE_h19," +
                            "@IE_h20,@IE_h21,@IE_h22,@IE_h23,@IE_h24)";

                            MyCom.Connection = MyConnection;

                            MyCom.Parameters["@IE_date"].Value = mydate;
                            MyCom.Parameters["@IE_code"].Value = ((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString();
                            MyCom.Parameters["@IE_name"].Value = ((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString();
                            if (((Excel.Range)workSheet.Cells[row, 3]).Value2 != null)
                                MyCom.Parameters["@IE_h1"].Value = ((Excel.Range)workSheet.Cells[row, 3]).Value2.ToString();
                            else MyCom.Parameters["@IE_h1"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 4]).Value2 != null)
                                MyCom.Parameters["@IE_h2"].Value = ((Excel.Range)workSheet.Cells[row, 4]).Value2.ToString();
                            else MyCom.Parameters["@IE_h2"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 5]).Value2 != null)
                                MyCom.Parameters["@IE_h3"].Value = ((Excel.Range)workSheet.Cells[row, 5]).Value2.ToString();
                            else MyCom.Parameters["@IE_h3"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 6]).Value2 != null)
                                MyCom.Parameters["@IE_h4"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();
                            else MyCom.Parameters["@IE_h4"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 7]).Value2 != null)
                                MyCom.Parameters["@IE_h5"].Value = ((Excel.Range)workSheet.Cells[row, 7]).Value2.ToString();
                            else MyCom.Parameters["@IE_h5"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 8]).Value2 != null)
                                MyCom.Parameters["@IE_h6"].Value = ((Excel.Range)workSheet.Cells[row, 8]).Value2.ToString();
                            else MyCom.Parameters["@IE_h6"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 9]).Value2 != null)
                                MyCom.Parameters["@IE_h7"].Value = ((Excel.Range)workSheet.Cells[row, 9]).Value2.ToString();
                            else MyCom.Parameters["@IE_h7"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 10]).Value2 != null)
                                MyCom.Parameters["@IE_h8"].Value = ((Excel.Range)workSheet.Cells[row, 10]).Value2.ToString();
                            else MyCom.Parameters["@IE_h8"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 11]).Value2 != null)
                                MyCom.Parameters["@IE_h9"].Value = ((Excel.Range)workSheet.Cells[row, 11]).Value2.ToString();
                            else MyCom.Parameters["@IE_h9"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 12]).Value2 != null)
                                MyCom.Parameters["@IE_h10"].Value = ((Excel.Range)workSheet.Cells[row, 12]).Value2.ToString();
                            else MyCom.Parameters["@IE_h10"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 13]).Value2 != null)
                                MyCom.Parameters["@IE_h11"].Value = ((Excel.Range)workSheet.Cells[row, 13]).Value2.ToString();
                            else MyCom.Parameters["@IE_h11"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 14]).Value2 != null)
                                MyCom.Parameters["@IE_h12"].Value = ((Excel.Range)workSheet.Cells[row, 14]).Value2.ToString();
                            else MyCom.Parameters["@IE_h12"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 15]).Value2 != null)
                                MyCom.Parameters["@IE_h13"].Value = ((Excel.Range)workSheet.Cells[row, 15]).Value2.ToString();
                            else MyCom.Parameters["@IE_h13"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 16]).Value2 != null)
                                MyCom.Parameters["@IE_h14"].Value = ((Excel.Range)workSheet.Cells[row, 16]).Value2.ToString();
                            else MyCom.Parameters["@IE_h14"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 17]).Value2 != null)
                                MyCom.Parameters["@IE_h15"].Value = ((Excel.Range)workSheet.Cells[row, 17]).Value2.ToString();
                            else MyCom.Parameters["@IE_h15"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 18]).Value2 != null)
                                MyCom.Parameters["@IE_h16"].Value = ((Excel.Range)workSheet.Cells[row, 18]).Value2.ToString();
                            else MyCom.Parameters["@IE_h16"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 19]).Value2 != null)
                                MyCom.Parameters["@IE_h17"].Value = ((Excel.Range)workSheet.Cells[row, 19]).Value2.ToString();
                            else MyCom.Parameters["@IE_h17"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 20]).Value2 != null)
                                MyCom.Parameters["@IE_h18"].Value = ((Excel.Range)workSheet.Cells[row, 20]).Value2.ToString();
                            else MyCom.Parameters["@IE_h18"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 21]).Value2 != null)
                                MyCom.Parameters["@IE_h19"].Value = ((Excel.Range)workSheet.Cells[row, 21]).Value2.ToString();
                            else MyCom.Parameters["@IE_h19"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 22]).Value2 != null)
                                MyCom.Parameters["@IE_h20"].Value = ((Excel.Range)workSheet.Cells[row, 22]).Value2.ToString();
                            else MyCom.Parameters["@IE_h20"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 23]).Value2 != null)
                                MyCom.Parameters["@IE_h21"].Value = ((Excel.Range)workSheet.Cells[row, 23]).Value2.ToString();
                            else MyCom.Parameters["@IE_h21"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 24]).Value2 != null)
                                MyCom.Parameters["@IE_h22"].Value = ((Excel.Range)workSheet.Cells[row, 24]).Value2.ToString();
                            else MyCom.Parameters["@IE_h22"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 25]).Value2 != null)
                                MyCom.Parameters["@IE_h23"].Value = ((Excel.Range)workSheet.Cells[row, 25]).Value2.ToString();
                            else MyCom.Parameters["@IE_h23"].Value = 0;
                            if (((Excel.Range)workSheet.Cells[row, 26]).Value2 != null)
                            MyCom.Parameters["@IE_h24"].Value = ((Excel.Range)workSheet.Cells[row, 26]).Value2.ToString();
                            else MyCom.Parameters["@IE_h24"].Value = 0;

                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                            }
                        }
                        row++;
                    }
                }
            book.Close(true, book, Type.Missing);
        }
//------------------------------------RegionNetComp(string date)-------------------------------
        private void RegionNetComp(string date)
        {
            string Rdate = date;
            Rdate = Rdate.Replace("/", "");
            string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/NetworkComponents_files/sheet006.htm";
            //Downloading regionnetcomp.xls from website
            try
            {
                WebProxy proxy = new WebProxy("http://172.16.1.100:8080");
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                WebClient client = new WebClient();
                client.Proxy = proxy;
                client.DownloadFile(address, @"c:\data\regionnetcomp.xls");
            }
            catch (Exception exp)
            {
                string str = exp.Message;
            }

            //remove <link and saveas file
            StringBuilder newFileregion = new StringBuilder();
            string tempregion = "";
            string[] fileregion = File.ReadAllLines(@"C:\data\regionnetcomp.xls", Encoding.Default);
            foreach (string line in fileregion)
            {
                if (line.Contains("<link"))
                {
                    tempregion = line.Remove(0);
                    newFileregion.Append(tempregion + "\r\n");
                    continue;
                }
                newFileregion.Append(line + "\r\n");
            }
            File.WriteAllText(@"C:\data\regionnetcomp.xls", newFileregion.ToString(), Encoding.Default);

            //Insert into DB (RegionNetComp)
            string path1 = @"c:\data\regionnetcomp.xls";
            Excel.Application exobj = new Excel.Application();
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path1, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();
            SqlCommand MyCom = new SqlCommand();

            MyCom.Parameters.Add("@RNC_date", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@RNC_code", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@RNC_ppcode", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@RNC_name", SqlDbType.NChar, 50);
            MyCom.Parameters.Add("@RNC_type", SqlDbType.NChar, 1);
            MyCom.Parameters.Add("@RNC_power", SqlDbType.Real);

            foreach (Excel.Worksheet workSheet in book.Worksheets)
                if (workSheet.Name == "regionnetcomp")
                {
                    string mydate = ((Excel.Range)workSheet.Cells[3, 1]).Value2.ToString();
                    mydate = mydate.Remove(0, (mydate.Length - 10));
                    int row = 4;
                    while (row < 180)
                    {
                        if ((((Excel.Range)workSheet.Cells[row, 1]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString().Contains("كد")) )
                        {
                            string code = ((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString();
                            row++;
                            while ((((Excel.Range)workSheet.Cells[row, 1]).Value2 != null) && (row < 180)) 
                            {
                                MyCom.CommandText = "INSERT INTO [RegionNetComp] (Date," +
                                "Code,PPCode,Type,Name,Power) VALUES (@RNC_date,@RNC_code" +
                                ",@RNC_ppcode,@RNC_type,@RNC_name,@RNC_power)";

                                MyCom.Connection = MyConnection;

                                MyCom.Parameters["@RNC_date"].Value = mydate;
                                MyCom.Parameters["@RNC_code"].Value = code;
                                MyCom.Parameters["@RNC_ppcode"].Value = ((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString();
                                MyCom.Parameters["@RNC_name"].Value = ((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString();
                                MyCom.Parameters["@RNC_type"].Value = ((Excel.Range)workSheet.Cells[row, 3]).Value2.ToString();
                                MyCom.Parameters["@RNC_power"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();

                                try
                                {
                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception exp)
                                {
                                    string str = exp.Message;
                                }
                                row++;
                            } 
                        }
                        row++;
                    }
                }
            book.Close(true, book, Type.Missing);
        }
//------------------------------------LineNetComp(string date)-------------------------------
        private void LineNetComp(string date)
        {
            string Rdate = date;
            Rdate = Rdate.Replace("/", "");
            string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/NetworkComponents_files/sheet002.htm";

            //Downloading linesnetcomp.xls from website
            try
            {
                WebProxy proxy = new WebProxy("http://172.16.1.100:8080");
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                WebClient client = new WebClient();
                client.Proxy = proxy;
                client.DownloadFile(address, @"c:\data\linesnetcomp.xls");
            }
            catch (Exception exp)
            {
                string str = exp.Message;
            }

            //remove <link and saveas file
            StringBuilder newFileline = new StringBuilder();
            string templine = "";
            string[] fileline = File.ReadAllLines(@"C:\data\linenetcomp.xls", Encoding.Default);
            foreach (string line in fileline)
            {
                if (line.Contains("<link"))
                {
                    templine = line.Remove(0);
                    newFileline.Append(templine + "\r\n");
                    continue;
                }
                newFileline.Append(line + "\r\n");
            }
            File.WriteAllText(@"C:\data\linenetcomp.xls", newFileline.ToString(), Encoding.Default);

            //Insert into DB (LineNetComp)
            string path1 = @"c:\data\linenetcomp.xls";
            Excel.Application exobj = new Excel.Application();
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path1, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();
            SqlCommand MyCom = new SqlCommand();

            MyCom.Parameters.Add("@LNC_date", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@LNC_code", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@LNC_origin", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@LNC_destination", SqlDbType.NChar, 20);

            foreach (Excel.Worksheet workSheet in book.Worksheets)
                if (workSheet.Name == "linenetcomp")
                {
                    string mydate = ((Excel.Range)workSheet.Cells[3, 1]).Value2.ToString();
                    mydate = mydate.Remove(0, (mydate.Length - 10));
                    int row = 4;
                    while (row < 700)
                    {
                        if ((((Excel.Range)workSheet.Cells[row, 1]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString().Contains("كد")) && (((Excel.Range)workSheet.Cells[row, 2]).Value2 != null))
                        {
                            MyCom.CommandText = "INSERT INTO [LineNetComp] (Date," +
                            "Code,Origin,Destination) VALUES (@LNC_date,@LNC_code" +
                            ",@LNC_origin,@LNC_destination)";

                            MyCom.Connection = MyConnection;

                            MyCom.Parameters["@LNC_date"].Value = mydate;
                            MyCom.Parameters["@LNC_code"].Value = ((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString();
                            MyCom.Parameters["@LNC_origin"].Value = ((Excel.Range)workSheet.Cells[row, 5]).Value2.ToString();
                            MyCom.Parameters["@LNC_destination"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();

                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                            }
                        }
                        row++;
                    }
                }
            book.Close(true, book, Type.Missing);

        }
//------------------------------------UnitNetComp(string date)-------------------------------
        private void UnitNetComp(string date)
        {
            string Rdate=date;
            Rdate = Rdate.Replace("/", "");
            string address = "http://igmc.ir/sccisrep/Html/" + Rdate + "/NetworkComponents_files/sheet008.htm";
            //Downloading unitnetcomp.xls from website
            try
            {
                WebProxy proxy = new WebProxy("http://172.16.1.100:8080");
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                WebClient client = new WebClient();
                client.Proxy = proxy;
                client.DownloadFile(address, @"c:\data\unitnetcomp.xls");
            }
            catch (Exception exp)
            {
                string str = exp.Message;
            }

            //remove <link and saveas file
            StringBuilder newFileunit = new StringBuilder();
            string tempunit = "";
            string[] fileunit = File.ReadAllLines(@"C:\data\unitnetcomp.xls", Encoding.Default);
            foreach (string line in fileunit)
            {
                if (line.Contains("<link"))
                {
                    tempunit = line.Remove(0);
                    newFileunit.Append(tempunit + "\r\n");
                    continue;
                }
                newFileunit.Append(line + "\r\n");
            }
            File.WriteAllText(@"C:\data\unitnetcomp.xls", newFileunit.ToString(), Encoding.Default);

            //Insert into DB (UnitNetComp)
            string path1 = @"c:\data\unitnetcomp.xls";
            Excel.Application exobj = new Excel.Application();
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path1, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();
            SqlCommand MyCom = new SqlCommand();

            MyCom.Parameters.Add("@UNC_date", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@UNC_ppcode", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@UNC_ucode", SqlDbType.NChar, 1);
            MyCom.Parameters.Add("@UNC_name", SqlDbType.NChar, 50);
            MyCom.Parameters.Add("@UNC_xcode", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@UNC_cyclecode", SqlDbType.NChar, 10);
            MyCom.Parameters.Add("@UNC_power", SqlDbType.Real);

            foreach (Excel.Worksheet workSheet in book.Worksheets)
                if (workSheet.Name == "unitnetcomp")
                {
                    string mydate = ((Excel.Range)workSheet.Cells[3, 1]).Value2.ToString();
                    mydate = mydate.Remove(0, (mydate.Length - 10));
                    int row = 4;
                    while (row < 720)
                    {
                        if ((((Excel.Range)workSheet.Cells[row, 1]).Value2 != null) && (!((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString().Contains("كد")))
                        {
                            string code = ((Excel.Range)workSheet.Cells[row, 1]).Value2.ToString();
                            string name = ((Excel.Range)workSheet.Cells[row, 2]).Value2.ToString();
                            string unitcode = ((Excel.Range)workSheet.Cells[row, 3]).Value2.ToString();
                            row++;
                            while ((row < 720)&&(((Excel.Range)workSheet.Cells[row, 1]).Value2 == null) && (((Excel.Range)workSheet.Cells[row, 5]).Value2 != null))
                            {
                                MyCom.CommandText = "INSERT INTO [UnitNetComp] (Date," +
                                "PPCode,Name,UnitCode,XCode,CycleCode,Power) VALUES (@UNC_date"+
                                ",@UNC_ppcode,@UNC_name,@UNC_ucode,@UNC_xcode,"+
                                "@UNC_cyclecode,@UNC_power)";

                                MyCom.Connection = MyConnection;

                                MyCom.Parameters["@UNC_date"].Value = mydate;
                                MyCom.Parameters["@UNC_ppcode"].Value = code;
                                MyCom.Parameters["@UNC_ucode"].Value = unitcode;
                                MyCom.Parameters["@UNC_name"].Value = name;
                                MyCom.Parameters["@UNC_xcode"].Value = ((Excel.Range)workSheet.Cells[row, 5]).Value2.ToString();
                                MyCom.Parameters["@UNC_cyclecode"].Value = ((Excel.Range)workSheet.Cells[row, 6]).Value2.ToString();
                                try
                                {
                                    float x = float.Parse(((Excel.Range)workSheet.Cells[row, 8]).Value2.ToString());
                                    MyCom.Parameters["@UNC_power"].Value = x;
                                }
                                catch (Exception exp)
                                {
                                    MyCom.Parameters["@UNC_power"].Value = 0;
                                }
                                try
                                {
                                    MyCom.ExecuteNonQuery();
                                }
                                catch (Exception exp)
                                {
                                    string str = exp.Message;
                                }
                                row++;
                            }
                        }
                        row++;
                    }
                }
            book.Close(true, book, Type.Missing);
        }

//------------------------------findPPID-----------------------------
        private int findPPID(string name)
        {
            int ID = 0;
            if (name.Contains("بعثت")) ID = 101;
            else if (name.Contains("پرند")) ID = 104;
            else if (name.Contains("منتظر")) ID = 131;
            else if ((name.Contains("فيروزي")) || (name.Contains("طرشت"))) ID = 133;
            else if ((!name.Contains("بخاري")) && (name.Contains("ري"))) ID = 138;
            else if ((name.Contains("رجائي")) || (name.Contains("رجايي"))) ID = 144;
            else if ((name.Contains("قم")) || (name.Contains("Qom"))) ID = 149;
            return ID;
        }


    }
}
