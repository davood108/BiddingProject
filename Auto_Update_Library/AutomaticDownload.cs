﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using FarsiLibrary.Utils;
using NRI.SBS.Common;

namespace Auto_Update_Library
{
    //public class AutomaticDownload
    //{
    //    //public static string ConStr = "Data Source=.;Initial Catalog=PowerPalntDB;Integrated Security=True";
    //    List<string> PPIDArray = new List<string>();
    //    List<bool> CombinedPlantStatus = new List<bool>();

    //    public AutomaticDownload()
    //    {
    //        PopulatePPIDArray();
    //    }

    //    private void PopulatePPIDArray()
    //    {
    //        DataSet MyDS = new DataSet();
    //        SqlDataAdapter Myda = new SqlDataAdapter();
    //        SqlConnection myConnection  = SingleToneConnectionManager.GetInstance().Connection;

            
    //        Myda.SelectCommand = new SqlCommand("SELECT PPID FROM PowerPlant", myConnection);
    //        Myda.Fill(MyDS, "ppid");

    //        foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
    //        {
    //            string strPPID = MyRow["PPID"].ToString().Trim();
    //            PPIDArray.Add(strPPID);

    //            //////////////////////////

    //            SqlCommand mycom = new SqlCommand();
    //            mycom.Connection = myConnection;
    //            mycom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id SELECT @result2 =count(PPID) FROM [PPUnit] WHERE PPID=@id AND PackageType LIKE 'Combined Cycle%'";
    //            mycom.Parameters.Add("@id", SqlDbType.NChar, 10);
    //            mycom.Parameters["@id"].Value = strPPID;
    //            mycom.Parameters.Add("@result1", SqlDbType.Int);
    //            mycom.Parameters["@result1"].Direction = ParameterDirection.Output;
    //            mycom.Parameters.Add("@result2", SqlDbType.Int);
    //            mycom.Parameters["@result2"].Direction = ParameterDirection.Output;
    //            mycom.ExecuteNonQuery();
    //            int result1 = (int)mycom.Parameters["@result1"].Value;
    //            int result2 = (int)mycom.Parameters["@result2"].Value;
    //            if ((result1 > 1) && (result2 > 0))
    //                CombinedPlantStatus.Add(true);
    //            else
    //                CombinedPlantStatus.Add(false);
    //        }
    //    }

    //    public void ReadPeriodically()
    //    {
    //        Array strItems = Enum.GetValues(typeof(ItemsToBeDownloaded));
    //        for(int i=0; i<strItems.Length; i++)
    //        {
    //            ItemsToBeDownloaded item = (ItemsToBeDownloaded) strItems.GetValue(i);
    //            if (item == ItemsToBeDownloaded.Rep12Pages)
    //            {
    //            }
    //            else if (item != ItemsToBeDownloaded.AveragePrice && item != ItemsToBeDownloaded.LoadForcasting)
    //            {
    //                List<PersianDate> missingDates = GetMissingDates(item);

    //                //int co = 0;
    //                foreach (PersianDate date in missingDates)
    //                {
    //                    //if (co < 3)
    //                        DownLoadInterface(item, date);
    //                    //co++;
    //                }
    //            }
    //            else
    //                DownLoadInterface(item, new PersianDate(DateTime.Now));

    //        }
    //    }

    //    private void DownLoadInterface(ItemsToBeDownloaded item, PersianDate date)
    //    {
    //        string strDate = date.ToString("d");

    //        switch (item)
    //        {
    //            case ItemsToBeDownloaded.M002:
    //                foreach (string PPID in PPIDArray)
    //                {
    //                    Download.GetInstance().M002(strDate, PPID, false);
    //                    if (CombinedPlantStatus[PPIDArray.IndexOf(PPID)])
    //                        Download.GetInstance().M002(strDate, PPID, true);
    //                }
    //                break;

    //            case ItemsToBeDownloaded.M005:
    //                foreach (string PPID in PPIDArray)
    //                {
    //                    Download.GetInstance().M005(strDate, PPID,false);
    //                    if (CombinedPlantStatus[PPIDArray.IndexOf(PPID)])
    //                        Download.GetInstance().M005(strDate, PPID,true);
    //                }
    //                break;
                
    //            case ItemsToBeDownloaded.M009:
    //                foreach (string PPID in PPIDArray)
    //                {
    //                    Download.GetInstance().M009(strDate, PPID, false);
    //                    if (CombinedPlantStatus[PPIDArray.IndexOf(PPID)])
    //                        Download.GetInstance().M009(strDate, PPID, true);
    //                }
    //                break;
                
    //            case ItemsToBeDownloaded.M0091:
    //                foreach (string PPID in PPIDArray)
    //                {
    //                    Download.GetInstance().M0091(strDate, PPID, false);
    //                    if (CombinedPlantStatus[PPIDArray.IndexOf(PPID)])
    //                        Download.GetInstance().M0091(strDate, PPID, true);
    //                }
    //                break;

    //            case ItemsToBeDownloaded.Rep12Pages:
    //                Download.GetInstance().Rep12Page(strDate);
    //                break;

    //            case ItemsToBeDownloaded.AveragePrice:
    //                Download.GetInstance().AveragePrice();
    //                break;

    //            case ItemsToBeDownloaded.Manategh:
    //                Download.GetInstance().Manategh(strDate);
    //                break;

    //            case ItemsToBeDownloaded.InterchangedEnergy:
    //                Download.GetInstance().InterchangedEnergy(strDate);
    //                break;

    //            case ItemsToBeDownloaded.lineNetComp:
    //                Download.GetInstance().LineNetComp(strDate);
    //                break;

    //            case ItemsToBeDownloaded.RegionNetComp:
    //                Download.GetInstance().RegionNetComp(strDate);
    //                break;

    //            case ItemsToBeDownloaded.UnitNetComp:
    //                Download.GetInstance().UnitNetComp(strDate);
    //                break;

    //            case ItemsToBeDownloaded.LoadForcasting:
    //                Download.GetInstance().LoadForecasting();
    //                break;

    //            case ItemsToBeDownloaded.Outage:
    //                Download.GetInstance().Outage(strDate);
    //                break;

    //            case ItemsToBeDownloaded.ProducedEenergy:
    //                Download.GetInstance().ProducedEnergy(strDate);
    //                break;

    //        }

    //    }

    //    private List<PersianDate> GetMissingDates(ItemsToBeDownloaded item)
    //    {
            
    //        string strDBDate = GetLatestDateInDB(item);
    //        //strDBDate = "1388/07/03";
    //        if (strDBDate == "")
    //        {
    //            if (item == ItemsToBeDownloaded.Rep12Pages || item == ItemsToBeDownloaded.Outage ||
    //                item == ItemsToBeDownloaded.Manategh /*|| item == Items.EN */)
    //            strDBDate = DateTime.Now.Subtract(new TimeSpan(365, 0, 0, 0)).ToString("d");
    //        }
    //        if (strDBDate != "")
    //        {
    //            List<PersianDate> missingDates = new List<PersianDate>();

    //            DateTime saveDate = (DateTime)PersianDateConverter.ToGregorianDateTime(new PersianDate(strDBDate));

    //            DateTime dateNow = DateTime.Now;

    //            TimeSpan span = dateNow - saveDate;
    //            while (span.Days > 0)
    //            {
    //                saveDate = saveDate.AddDays(1);
    //                missingDates.Add(new PersianDate(saveDate));
    //                span = dateNow - saveDate;
    //            }
    //            return missingDates;
    //        }
    //        else
    //            return null;
    //    }

    //    private string GetLatestDateInDB(ItemsToBeDownloaded item)
    //    {
    //        string tableName = "", dateColName = "";

    //        switch (item)
    //        {
    //            case ItemsToBeDownloaded.M002:
    //                tableName = "MainFRM002";
    //                dateColName = "TargetMarketDate"; 
    //                break;

    //            case ItemsToBeDownloaded.M005:
    //                tableName = "MainFRM005";
    //                dateColName = "TargetMarketDate"; 
    //                break;

    //            case ItemsToBeDownloaded.Rep12Pages:
    //                tableName = "Rep12Page";
    //                dateColName = "Date";
    //                break;

    //            case ItemsToBeDownloaded.AveragePrice:
    //                tableName = "AveragePrice";
    //                dateColName = "Date";
    //                break;

    //            case ItemsToBeDownloaded.Manategh:
    //                tableName = "Manategh";
    //                dateColName = "Date";
    //                break;

    //            case ItemsToBeDownloaded.InterchangedEnergy:
    //                tableName = "InterchangedEnergy";
    //                dateColName = "Date";
    //                break;

    //            case ItemsToBeDownloaded.lineNetComp:
    //                tableName = "LineNetComp";
    //                dateColName = "Date";
    //                break;

    //            case ItemsToBeDownloaded.RegionNetComp:
    //                tableName = "RegionNetComp";
    //                dateColName = "Date";
    //                break;

    //            case ItemsToBeDownloaded.UnitNetComp:
    //                tableName = "UnitNetComp";
    //                dateColName = "Date";
    //                break;

    //            case ItemsToBeDownloaded.LoadForcasting:
    //                tableName = "LoadForecasting";
    //                dateColName = "Date";
    //                break;

    //            case ItemsToBeDownloaded.Outage:
    //                tableName = "OutageUnits";
    //                dateColName = "Date";
    //                break;

    //            case ItemsToBeDownloaded.ProducedEenergy:
    //                tableName = "ProducedEnergy";
    //                dateColName = "Date";
    //                break;

    //        }

    //        SqlConnection MyConnection = SingleToneConnectionManager.GetInstance().Connection;
    //        SqlCommand MyCom = new SqlCommand();
    //        //MyCom.CommandText = "select @result1 = max(@tableName.@dateColName) from @tableName";
    //        MyCom.CommandText = "select @result1 = max(" + tableName + "." + dateColName + ") from " + tableName;

    //        MyCom.Connection = MyConnection;

    //        MyCom.Parameters.Add("@tableName", SqlDbType.Char, 20);
    //        MyCom.Parameters["@tableName"].Value = tableName;

    //        MyCom.Parameters.Add("@dateColName", SqlDbType.Char, 20);
    //        MyCom.Parameters["@dateColName"].Value = dateColName;

    //        MyCom.Parameters.Add("@result1", SqlDbType.Char, 10);
    //        MyCom.Parameters["@result1"].Direction = ParameterDirection.Output;


    //        MyCom.ExecuteNonQuery();

    //        string result1 = "";
    //        result1 = (string)MyCom.Parameters["@result1"].Value;
    //        return result1;


    //    }

    //    //private List<PersianDate> GetMissingDatesTillNow(string strDBDate)
    //    //{
    //    //    strDBDate = "1388/11/03";
    //    //    List<PersianDate> missingDates = new List<PersianDate>();

    //    //    //DateTime saveDate = (DateTime)PersianDateConverter.ToGregorianDateTime(new PersianDate(strDBDate));

    //    //    //DateTime dateNow = DateTime.Now;

    //    //    //TimeSpan span = dateNow - saveDate;
    //    //    //while (span.Days > 0)
    //    //    //{
    //    //    //    saveDate = saveDate.AddDays(1);
    //    //    //    missingDates.Add(new PersianDate(saveDate));
    //    //    //    span = dateNow - saveDate;
    //    //    //}
    //    //    return missingDates;


    //    //}


    //}
}
