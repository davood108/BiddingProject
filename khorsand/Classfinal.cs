﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Common;
using ILOG.Concert;
using ILOG.CPLEX;
using PowerPlantProject;
using FarsiLibrary.Utils;
using NRI.SBS.Common;
using System.Collections;
namespace PowerPlantProject
{

    public class Classfinal
    {
        int BiddingStrategySettingId;
        string ConStr = NRI.SBS.Common.ConnectionManager.ConnectionString;
        string biddingDate;
        string currentDate;
        string ppName;
        private SqlConnection oSqlConnection = null;
        private SqlCommand oSqlCommand = null;
        private SqlDataReader oSqlDataReader = null;
        private DataTable oDataTable = null;
        private string[] plant;
        int Plants_Num;
        int Package_Num;
        int[] Plant_Packages_Num;
        bool Ispre = false;

        public Classfinal(string PPName, string CurrentDate, string BiddingDate,bool PreSolve)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();
            biddingDate = BiddingDate;
            currentDate = CurrentDate;
            Ispre = PreSolve;
            ppName = PPName.Trim();

            if (ppName.ToLower().Trim() == "all")
            {
                oDataTable = utilities.returntbl("select distinct PPID from UnitsDataMain ");
                Plants_Num = oDataTable.Rows.Count;

                plant = new string[Plants_Num];
                for (int il = 0; il < Plants_Num; il++)
                {
                    plant[il] = oDataTable.Rows[il][0].ToString().Trim();

                }
            }
            else
            {
                SqlCommand MyCom1 = new SqlCommand();
                MyCom1.Connection = myConnection;
                MyCom1.CommandText = "SELECT @num=PPID FROM PowerPlant WHERE PPName=@name";
                MyCom1.Parameters.Add("@num", SqlDbType.NChar, 10);
                MyCom1.Parameters.Add("@name", SqlDbType.NChar, 20);
                MyCom1.Parameters["@num"].Direction = ParameterDirection.Output;
                MyCom1.Parameters["@name"].Value = ppName.Trim();
                MyCom1.ExecuteNonQuery();
                
                Plants_Num = 1;
                plant = new string[1];
                plant[0] = MyCom1.Parameters["@num"].Value.ToString().Trim();


            }
            /////////

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            MyCom.CommandText = "select @id=max(id) from dbo.BiddingStrategySetting " +
                " where plant=@ppname AND  CurrentDate=@curDate AND BiddingDate=@bidDate";
            MyCom.Parameters.Add("@id", SqlDbType.Int);
            MyCom.Parameters.Add("@ppname", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@curDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@bidDate", SqlDbType.Char, 10);
            MyCom.Parameters["@id"].Direction = ParameterDirection.Output;
            MyCom.Parameters["@ppname"].Value = ppName.Trim();
            MyCom.Parameters["@curDate"].Value = CurrentDate;
            MyCom.Parameters["@bidDate"].Value = BiddingDate;
            MyCom.ExecuteNonQuery();
            myConnection.Close();
            BiddingStrategySettingId = int.Parse(MyCom.Parameters["@id"].Value.ToString().Trim());
        }



        //public double[, , ,] powval;
        //public double[, , ,] priceval;
        //public double[, , ] valpmax;

        // public double tot;

        private string buildmaxdate(string[] arrmax)
        {
            string maxdate = null;
            for (int fd = 0; fd < arrmax.Length; fd++)
            {
                if (maxdate == null || string.Compare(maxdate, arrmax[fd]) < 0)
                    maxdate = arrmax[fd];
            }
            return maxdate;

        }

        public bool value()
        {

            string strCmd = "select Max (PackageCode) from UnitsDataMain ";
            if (Plants_Num == 1)
                strCmd += " where ppid=" + plant[0];
            oDataTable = utilities.returntbl(strCmd);
            Package_Num = (int)oDataTable.Rows[0][0];

      
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //Unit Number
            int Units_Num = 0;

            int[] Plant_units_Num = new int[Plants_Num];
            for (int re = 0; re < Plants_Num; re++)
            {
                oDataTable = utilities.returntbl("select UnitCode from UnitsDataMain where PPID='" + plant[re] + "'");

                Plant_units_Num[re] = oDataTable.Rows.Count;
                if (Units_Num <= oDataTable.Rows.Count)
                {
                    Units_Num = oDataTable.Rows.Count;
                }

            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //Fixed Set :
            int CplexPrice = 0;
            int CplexPower = 0;
            int Hour = 24;
            int Dec_Num = 4;
            int Data_Num = 25;
            int Start_Num = 48;
            int Mmax = 2;
            int Step_Num = 10;
            int Xtrain = 201;
            int Power_Num = 4;
            int Power_W = 4;
            int oldday = 7;
            int Const_selectdays = 7;
            int GastostartSteam = 7;


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int Risk_Max = 1400;                // Max Error Price in Base Hour
            int Risk_Peak = 1000;                // Max Error Price in Peak Hour
            double Factor_Delta_Ave = 250;      // Price O-F
            int Limit_variance = 1000;          // Max Error forecast
            int Limit_variance_Down = 100;      // Min Error forecast
            double Price_delta_Max = 1400;
            double Price_delta_Min = 100;
            double Factor_Power = 0.9;
            double Factor_Power_Region = 0.9;
            double Factor_Dispatch_Befor = 0.9;
            double Factor_Price = 0.991;
            double Egas = 100;
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Unit_Dec = new string[Plants_Num, Units_Num, Dec_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Dec_Num; k++)
                    {
                        switch (k)
                        {
                            case 0:
                                oDataTable = utilities.returntbl("select UnitType  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString().Trim();
                                break;
                            case 1:
                                oDataTable = utilities.returntbl("select PackageType  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString().Trim();
                                break;
                            case 2:
                                Unit_Dec[j, i, k] = "Gas";
                                break;
                            case 3:
                                oDataTable = utilities.returntbl("select  SecondFuel  from UnitsDataMain where  PPID='" + plant[j] + "'");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString().Trim();
                                break;
                        }
                    }

                }

            }
             //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int b = 0;

            // detect max date in database...................................  
            oDataTable = utilities.returntbl("select Date from BaseData");
            int ndate = oDataTable.Rows.Count;
            string[] arrmaxdate1 = new string[ndate];
            for (b = 0; b < ndate; b++)
            {
                arrmaxdate1[b] = oDataTable.Rows[b][0].ToString().Trim();
            }

            string smaxdate = buildmaxdate(arrmaxdate1);


            oDataTable = utilities.returntbl("select GasPrice,MazutPrice,GasOilPrice from BaseData where Date like '" + smaxdate + '%' + "'");
            double[] FuelPrice_Free = new double[3];
            for (b = 0; b < 3; b++)
            {
                if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][b].ToString() != "")
                {
                    FuelPrice_Free[b] = double.Parse(oDataTable.Rows[0][b].ToString());
                }
                else
                {
                    FuelPrice_Free[0] = 690;
                    FuelPrice_Free[1] = 5053;
                    FuelPrice_Free[2] = 7057;
                }

            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int bb = 0;
            // detect max date in database...................................  
            oDataTable = utilities.returntbl("select Date from EfficiencyMarket");
            int ndate2 = oDataTable.Rows.Count;
            string[] arrmaxdate2 = new string[ndate];
            for (bb = 0; bb < ndate2; bb++)
            {
                arrmaxdate2[bb] = oDataTable.Rows[bb][0].ToString().Trim();
            }

            string smaxdate2 = buildmaxdate(arrmaxdate2);


            oDataTable = utilities.returntbl(" select Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24"+
            " from EfficiencyMarket where Date like '" + smaxdate2 + '%' + "'");
            double[] Marketefficient = new double[24];
            for (bb = 0; bb < 24; bb++)
            {
                if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][bb].ToString() != "")
                {
                    Marketefficient[bb] = double.Parse(oDataTable.Rows[0][bb].ToString());
                }
                else
                {
                    Marketefficient[bb] = 35;
                }

            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            oDataTable = utilities.returntbl("select GasSubsidiesPrice,MazutSubsidiesPrice,GasOilSubsidiesPrice from BaseData where Date like '" + smaxdate + '%' + "'");
            double[] FuelPrice = new double[3];
                for (b = 0; b < 3; b++)
                {
                    if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][b].ToString() != "")
                    {
                        FuelPrice[b] = double.Parse(oDataTable.Rows[0][b].ToString());
                    }
                    else
                    {
                        FuelPrice[0] = 27.9;
                        FuelPrice[1] = 57.467;
                        FuelPrice[2] = 30.467;
                    }

                }
         


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            oDataTable = utilities.returntbl("select max(MarketPriceMax) from BaseData where Date like '" + smaxdate + '%' + "'");
            double[] CapPrice = new double[1];

            if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][0].ToString() != "")
            {
                CapPrice[0] = double.Parse(oDataTable.Rows[0][0].ToString());
                CapPrice[0] = CapPrice[0] - 1; 
            }
            else
            {
                CapPrice[0] = 109999;

            }

            //Error bid for distance is accept.
            double[] ErrorBid = new double[1];
            ErrorBid[0] = CapPrice[0] / 440;

            string[] Run = new string[2];
            Run[0] = "Automatic";
            Run[1] = "Customize";


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            Plant_Packages_Num = new int[Plants_Num];
            
            List<string>[] PackageTypes = new List<string>[Plants_Num];

            for (int re = 0; re < Plants_Num; re++)
            {
                oDataTable = utilities.returntbl("select Max(PackageCode) from UnitsDataMain where  PPID='" + plant[re] + "'");
                Plant_Packages_Num[re] = (int)oDataTable.Rows[0][0];

                oDataTable = utilities.returntbl("select distinct PackageType from UnitsDataMain where  PPID='" + plant[re] + "'");
                //Plant_Packages_Num[re] = (int)oDataTable.Rows.count;

                PackageTypes[re] = new List<string>();

                foreach (string typeName in Enum.GetNames(typeof(PackageTypePriority)))
                {
                    //////////// have to make sure later whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                    bool found = false;
                    foreach (DataRow row in oDataTable.Rows)
                        if (row["PackageType"].ToString().Trim().Contains(typeName))
                            found = true;
                    
                    if (found)
                        PackageTypes[re].Add(typeName);
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,] Package = new string[Plants_Num, Package_Num];

            for (int j = 0; j < Plants_Num; j++)
            {
                string cmd = "select PackageType from dbo.UnitsDataMain where PPID=" + plant[j] +
                             " AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%')AND(UnitCode not like 'Gas cc%')))";
                oDataTable = utilities.returntbl(cmd);

                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    Package[j, k] = oDataTable.Rows[k][0].ToString().Trim();
                }

            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,] Unit = new string[Plants_Num, Units_Num];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    oDataTable = utilities.returntbl("select UnitCode from UnitsDataMain where PPID='" + plant[j] + "'");
                    Unit[j, i] = oDataTable.Rows[i][0].ToString().Trim();
                }
            }


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Unit_Data = new double[Plants_Num, Units_Num, Data_Num];
            // 0 : Package
            // 1 : RampUp
            // 2 : RampDown
            // 3 : Pmin
            // 4 : pmax
            // 5 : RampStart&shut
            // 6 : MinOn
            // 7 : MinOff
            // 8 :  Am
            // 9 :  Bm
            // 10 : Cm
            // 11 : Bmain
            // 12 : Cmain
            // 13 : Dmain
            // 14 : Variable cost
            // 15 : Fixed Cost
            // 16 : Tcold
            // 17 : Thot
            // 18 : Cold start cost
            // 19 : Hot start cost

            for (int j = 0; j < Plants_Num; j++)
            {
                oDataTable = utilities.returntbl("select *  from UnitsDataMain where  PPID='" + plant[j] + "'");
                if (oDataTable.Rows.Count > 0)
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int k = 0; k < Data_Num; k++)
                        {
                            switch (k)
                            {
                                case 0:

                                    Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["PackageCode"].ToString());
                                    break;
                                case 1:
                                    if (oDataTable.Rows[i]["RampUpRate"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["RampUpRate"].ToString());
                                        Unit_Data[j, i, k] = 60 * Unit_Data[j, i, k];
                                    }
                                    break;
                                case 2:
                                    Unit_Data[j, i, k] = Unit_Data[j, i, 1];
                                    break;

                                case 3:

                                    Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["PMin"].ToString());
                                    break;

                                case 4:

                                    Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["PMax"].ToString());
                                    break;
                                case 5:
                                    double pow = Unit_Data[j, i, 1];
                                    if (Unit_Data[j, i, 3] > pow)
                                    {
                                        Unit_Data[j, i, k] = Unit_Data[j, i, 3];
                                    }
                                    else
                                    {
                                        Unit_Data[j, i, k] = pow;
                                    }
                                    break;
                                case 6:
                                    if (oDataTable.Rows[i]["TUp"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["TUp"].ToString());
                                    }
                                    else Unit_Data[j, i, k] = 3;

                                    break;
                                case 7:
                                    if (oDataTable.Rows[i]["TDown"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["TDown"].ToString());

                                    }
                                    else Unit_Data[j, i, k] = 10;

                                    break;

                                case 8:
                                    if (oDataTable.Rows[i]["PrimaryFuelAmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["PrimaryFuelAmargin"].ToString());
                                    }
                                    break;
                                case 9:
                                    if (oDataTable.Rows[i]["PrimaryFuelBmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["PrimaryFuelBmargin"].ToString());
                                    }
                                    break;
                                case 10:
                                    if (oDataTable.Rows[i]["PrimaryFuelCmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["PrimaryFuelCmargin"].ToString());
                                    }
                                    break;
                                case 11:
                                    if (oDataTable.Rows[i]["BMaintenance"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["BMaintenance"].ToString());
                                    }
                                    break;

                                case 12:
                                    if (oDataTable.Rows[i]["CMaintenance"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["CMaintenance"].ToString());
                                    }
                                    break;
                                case 13:
                                    Unit_Data[j, i, k] = Unit_Data[j, i, 12];

                                    break;
                                case 14:

                                    if (oDataTable.Rows[i]["VariableCost"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["VariableCost"].ToString());
                                    }
                                    break;
                                case 15:
                                    if (oDataTable.Rows[i]["FixedCost"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["FixedCost"].ToString());
                                    }
                                    break;

                                case 16:
                                    if (oDataTable.Rows[i]["TStartCold"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["TStartCold"].ToString());
                                    }
                                    break;
                                case 17:

                                    if (oDataTable.Rows[i]["TStartHot"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["TStartHot"].ToString());
                                    }

                                    break;
                                case 18:
                                    if (oDataTable.Rows[i]["CostStartHot"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["CostStartHot"].ToString());
                                    }


                                    break;

                                case 19:
                                    if (oDataTable.Rows[i]["CostStartCold"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["CostStartCold"].ToString());
                                    }

                                    break;

                                case 20:
                                    if (oDataTable.Rows[i]["SecondFuelAmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["SecondFuelAmargin"].ToString());
                                    }

                                    break;
                                case 21:
                                    if (oDataTable.Rows[i]["SecondFuelBmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["SecondFuelBmargin"].ToString());
                                    }

                                    break;
                                case 22:
                                    if (oDataTable.Rows[i]["SecondFuelCmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["SecondFuelCmargin"].ToString());
                                    }
                                    break;
                                case 23:
                                    if (oDataTable.Rows[i]["HeatValuePrimaryFuel"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["HeatValuePrimaryFuel"].ToString());
                                    }
                                    break;
                                case 24:
                                    if (oDataTable.Rows[i]["HeatValueSecondaryFuel"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = double.Parse(oDataTable.Rows[i]["HeatValueSecondaryFuel"].ToString());
                                    }
                                    break;
                            }
                        }
                    }
            }
            //Rampe rate is correct :
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Data_Num; k++)
                    {
                        if (Unit_Data[j, i, 1] > (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]))
                        {
                            Unit_Data[j, i, 1] = (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]);
                            Unit_Data[j, i, 2] = (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]);
                        }
                    }
                }
            }
            //Rampe rate Startup is correct :
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Data_Num; k++)
                    {
                        if (Unit_Data[j, i, 5] > (Unit_Data[j, i, 4]))
                        {
                            Unit_Data[j, i, 5] = (Unit_Data[j, i, 4]);
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            // package iky kam she :
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Unit_Data[j, i, 0] = Unit_Data[j, i, 0] - 1;
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~khorsand~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
         
            double[, ,] maxs = new double[Plants_Num, Const_selectdays, 24];
            for (int j = 0; j < Plants_Num; j++)
            {


                DataSet myDs = UtilityPowerPlantFunctions.GetUnits(int.Parse(plant[j]));
                PersianDate test1 = new PersianDate(currentDate);
                DateTime Now = PersianDateConverter.ToGregorianDateTime(test1);

                DataTable[] orderedPackages = UtilityPowerPlantFunctions.OrderPackages(myDs);

                ///////////////////////////////for presolve/////////////////////////////////////
                string finalplant = plant[j];

                DataTable baseplantfor7day = utilities.returntbl("select PPName from dbo.BaseData where Date =(select max(Date) from dbo.BaseData)order by BaseID desc");
                DataTable idtable7day = utilities.returntbl("select PPID from dbo.PowerPlant  where PPName='" + baseplantfor7day.Rows[0][0].ToString().Trim() + "'");

                string preplant7day = idtable7day.Rows[0][0].ToString().Trim();

                DataTable packagetable7day = utilities.returntbl("SELECT DISTINCT  PackageType  FROM dbo.PPUnit WHERE PPID='" + preplant7day.Trim() + "'");

                string packtype7day = "";
                for (int i = 0; i < packagetable7day.Rows.Count; i++)
                {
                    if (packagetable7day.Rows[i][0].ToString().Trim() == "Steam")
                    {
                        packtype7day = "Steam";
                        break;
                    }
                    else if (packagetable7day.Rows[i][0].ToString().Trim() == "Combined Cycle") packtype7day = "CC";

                }
                

                DataSet myDs7 = UtilityPowerPlantFunctions.GetUnits(int.Parse(preplant7day));
                DataTable[] orderedPackages7 = UtilityPowerPlantFunctions.OrderPackages(myDs7);
                foreach (DataTable dtPackageType in orderedPackages)
                {
                    foreach (DataTable dtPackageType7 in orderedPackages7)
                    {
                        if ((Ispre == true))
                        {
                            if (((ppName.ToLower().Trim() == "all") && (dtPackageType.ToString() == "Gas")) || (ppName.ToLower().Trim() != "all"))
                            {
                                orderedPackages = orderedPackages7;
                                finalplant = preplant7day;
                                break;
                            }
                        }
                    }

                }

               
                foreach (DataTable dtPackageType in orderedPackages)
                {
                   

                    for (int daysbefore = 0; daysbefore < Const_selectdays; daysbefore++)
                    {
                        DateTime selectedDate = Now.Subtract(new TimeSpan(daysbefore, 0, 0, 0));

                        ////////////check 002 and 005 not empty////////////////////////////

                      

                        if (FindCommon002and005(selectedDate, finalplant) == "nocontinue")
                            {

                                return false;
                            }
                            else
                            {
                                selectedDate = PersianDateConverter.ToGregorianDateTime(FindCommon002and005(selectedDate,finalplant));
                            }


                      

                        ///////////////////////////////////////////////////////////////////
                        int packageOrder = 0;
                        if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                        orderedPackages.Length > 1)
                            packageOrder = 1;

                        CMatrix NSelectPrice = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid
                               (Convert.ToInt32(finalplant),dtPackageType, selectedDate, packageOrder);



                        for (int k = 0; k < 24; k++)
                            maxs[j, daysbefore, k] = NSelectPrice[0, k];
                    }
                }
            }

            ////////////////////////////khorsand////////////////////////////////


            ////////////////////////////add array second-primary////////////////////////////////
            double[, , ,] SecondaryParam = new double[Plants_Num, Units_Num, 2, 6];
            double[, , ,] PrimayParam = new double[Plants_Num, Units_Num, 2, 6];

            ///////2 for primary and secondary////////////////////////
            ///////6 for primary or secondary parameters//////////////


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    DataTable PRIMARYTABLE = utilities.returntbl("select PrimaryPowerA,PrimaryPowerB,PrimaryPowerC,PrimaryHeatRateA,PrimaryHeatRateB,PrimaryHeatRateC from dbo.UnitsDataMain where PPID='" + plant[j] + "' and UnitCode='" + Unit[j, i] + "'");
                    DataTable SECONDARYTABLE = utilities.returntbl("select SecondaryPowerA,SecondaryPowerB,SecondaryPowerC,SecondaryHeatRateA,SecondaryHeatRateB,SecondaryHeatRateC  from dbo.UnitsDataMain where PPID='" + plant[j] + "' and UnitCode='" + Unit[j, i] + "'");

                    for (int l = 0; l < 2; l++)
                    {
                        for (int m = 0; m < 6; m++)
                        {

                            if (l == 0)
                            {
                                switch (m)
                                {
                                    case 0:
                                        if (PRIMARYTABLE.Rows[0][0].ToString() != "")
                                        {
                                            PrimayParam[j, i, 0, 0] = double.Parse(PRIMARYTABLE.Rows[0][0].ToString());
                                        }
                                        break;
                                    case 1:
                                        if (PRIMARYTABLE.Rows[0][1].ToString() != "")
                                        {
                                            PrimayParam[j, i, 0, 1] = double.Parse(PRIMARYTABLE.Rows[0][1].ToString());
                                        }
                                        break;
                                    case 2:
                                        if (PRIMARYTABLE.Rows[0][2].ToString() != "")
                                        {
                                            PrimayParam[j, i, 0, 2] = double.Parse(PRIMARYTABLE.Rows[0][2].ToString());
                                        }
                                        break;
                                    case 3:
                                        if (PRIMARYTABLE.Rows[0][3].ToString() != "")
                                        {
                                            PrimayParam[j, i, 0, 3] = double.Parse(PRIMARYTABLE.Rows[0][3].ToString());
                                        }
                                        break;
                                    case 4:
                                        if (PRIMARYTABLE.Rows[0][4].ToString() != "")
                                        {
                                            PrimayParam[j, i, 0, 4] = double.Parse(PRIMARYTABLE.Rows[0][4].ToString());
                                        }
                                        break;
                                    case 5:
                                        if (PRIMARYTABLE.Rows[0][5].ToString() != "")
                                        {
                                            PrimayParam[j, i, 0, 5] = double.Parse(PRIMARYTABLE.Rows[0][5].ToString());
                                        }
                                        break;
                                }

                            }

                            else if (l == 1)
                            {
                                switch (m)
                                {
                                    case 0:
                                        if (SECONDARYTABLE.Rows[0][0].ToString() != "")
                                        {
                                            SecondaryParam[j, i, 1, 0] = double.Parse(SECONDARYTABLE.Rows[0][0].ToString());
                                        }
                                        break;
                                    case 1:
                                        if (SECONDARYTABLE.Rows[0][1].ToString() != "")
                                        {
                                            SecondaryParam[j, i, 1, 1] = double.Parse(SECONDARYTABLE.Rows[0][1].ToString());
                                        }
                                        break;
                                    case 2:
                                        if (SECONDARYTABLE.Rows[0][2].ToString() != "")
                                        {
                                            SecondaryParam[j, i, 1, 2] = double.Parse(SECONDARYTABLE.Rows[0][2].ToString());
                                        }
                                        break;
                                    case 3:
                                        if (SECONDARYTABLE.Rows[0][3].ToString() != "")
                                        {
                                            SecondaryParam[j, i, 1, 3] = double.Parse(SECONDARYTABLE.Rows[0][3].ToString());
                                        }
                                        break;
                                    case 4:
                                        if (SECONDARYTABLE.Rows[0][4].ToString() != "")
                                        {
                                            SecondaryParam[j, i, 1, 4] = double.Parse(SECONDARYTABLE.Rows[0][4].ToString());
                                        }
                                        break;
                                    case 5:
                                        if (SECONDARYTABLE.Rows[0][5].ToString() != "")
                                        {
                                            SecondaryParam[j, i, 1, 5] = double.Parse(SECONDARYTABLE.Rows[0][5].ToString());
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            double save_cpu=0 ;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                  if(SecondaryParam[j, i, 1, 0]> SecondaryParam[j, i, 1, 1])
                  {
                    save_cpu =SecondaryParam[j, i, 1, 1];
                    SecondaryParam[j, i, 1, 1]= SecondaryParam[j, i, 1, 0];
                    SecondaryParam[j, i, 1, 0]=  save_cpu;
                  }
                  if(SecondaryParam[j, i, 1, 1]> SecondaryParam[j, i, 1, 2])
                  {
                    save_cpu =SecondaryParam[j, i, 1, 2];
                    SecondaryParam[j, i, 1, 2]= SecondaryParam[j, i, 1, 1];
                    SecondaryParam[j, i, 1, 1]=  save_cpu;
                  }
                   if(SecondaryParam[j, i, 1, 0]> SecondaryParam[j, i, 1, 1])
                  {
                    save_cpu =SecondaryParam[j, i, 1, 1];
                    SecondaryParam[j, i, 1, 1]= SecondaryParam[j, i, 1, 0];
                    SecondaryParam[j, i, 1, 0]=  save_cpu;
                  }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                  if(SecondaryParam[j, i, 1, 3]> SecondaryParam[j, i, 1, 4])
                  {
                    save_cpu =SecondaryParam[j, i, 1, 4];
                    SecondaryParam[j, i, 1, 3]= SecondaryParam[j, i, 1, 4];
                    SecondaryParam[j, i, 1, 4]=  save_cpu;
                  }
                  if(SecondaryParam[j, i, 1, 4]> SecondaryParam[j, i, 1, 5])
                  {
                    save_cpu =SecondaryParam[j, i, 1, 5];
                    SecondaryParam[j, i, 1, 4]= SecondaryParam[j, i, 1, 5];
                    SecondaryParam[j, i, 1, 5]=  save_cpu;
                  }
                  if(SecondaryParam[j, i, 1, 3]> SecondaryParam[j, i, 1, 4])
                  {
                    save_cpu =SecondaryParam[j, i, 1, 4];
                    SecondaryParam[j, i, 1, 3]= SecondaryParam[j, i, 1, 4];
                    SecondaryParam[j, i, 1, 4]=  save_cpu;
                  }
                }
            }
            //****************************************************************
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                  if(PrimayParam[j, i, 0, 0]> PrimayParam[j, i, 0, 1])
                  {
                    save_cpu =SecondaryParam[j, i, 0, 1];
                    PrimayParam[j, i, 0, 1]= PrimayParam[j, i, 0, 0];
                    PrimayParam[j, i, 0, 0]=  save_cpu;
                  }
                  if(PrimayParam[j, i, 0, 1]> PrimayParam[j, i, 0, 2])
                  {
                    save_cpu =PrimayParam[j, i, 0, 2];
                    PrimayParam[j, i, 0, 2]= PrimayParam[j, i, 0, 1];
                    PrimayParam[j, i, 0, 1]=  save_cpu;
                  }
                   if(PrimayParam[j, i, 0, 0]> PrimayParam[j, i, 0, 1])
                  {
                    save_cpu =PrimayParam[j, i, 0, 1];
                    PrimayParam[j, i, 0, 1]= PrimayParam[j, i, 0, 0];
                    PrimayParam[j, i, 0, 0]=  save_cpu;
                  }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                  if(PrimayParam[j, i, 0, 3]> PrimayParam[j, i, 0, 4])
                  {
                    save_cpu =PrimayParam[j, i, 0, 4];
                    PrimayParam[j, i, 0, 3]= PrimayParam[j, i, 0, 4];
                    PrimayParam[j, i, 0, 4]=  save_cpu;
                  }
                  if(PrimayParam[j, i, 0, 4]> PrimayParam[j, i, 0, 5])
                  {
                    save_cpu =PrimayParam[j, i, 0, 5];
                    PrimayParam[j, i, 0, 4]= PrimayParam[j, i, 0, 5];
                    PrimayParam[j, i, 0, 5]=  save_cpu;
                  }
                  if(PrimayParam[j, i, 1, 3]> PrimayParam[j, i, 1, 4])
                  {
                    save_cpu =PrimayParam[j, i, 0, 4];
                    PrimayParam[j, i, 0, 3]= PrimayParam[j, i, 0, 4];
                    PrimayParam[j, i, 0, 4]=  save_cpu;
                  }
                }
            }
            double[, ,] SecondFuel_power = new double[Plants_Num, Units_Num, 3];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int m = 0; m < 3; m++)
                    {
                        SecondFuel_power[j, i, m] = SecondaryParam[j, i, 1, m];
                    }
                }
            }
            double[, , ] SecondFuel_Fuel = new double[Plants_Num, Units_Num, 3];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int m = 0; m < 3; m++)
                    {
                        SecondFuel_Fuel[j, i, m] = SecondaryParam[j, i, 1, m + 3];
                    }
                }
            }
            double[, , ] PrimayFuel_power = new double[Plants_Num, Units_Num, 3];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int m = 0; m < 3; m++)
                    {
                        PrimayFuel_power[j, i, m] = PrimayParam[j, i, 0, m];
                    }
                }
            }
            double[, ,] PrimayFuel_Fuel = new double[Plants_Num, Units_Num, 3];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int m = 0; m < 3; m++)
                    {
                        PrimayFuel_Fuel[j, i, m] = PrimayParam[j, i, 0, m + 3];
                    }
                }
            }
            double[,,] FixedCost = new double[Plants_Num, Units_Num,2];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    FixedCost[j, i, 0] = 0;
                    FixedCost[j, i, 1] = 0;
                    if ((PrimayFuel_Fuel[j, i, 0] > 0) & (PrimayFuel_power[j, i, 0] > 0))
                    {
                        FixedCost[j, i, 0] = PrimayFuel_Fuel[j, i, 0] * Unit_Data[j, i, 3] / PrimayFuel_power[j, i, 0];
                    }
                    if ((SecondFuel_Fuel[j, i, 0] > 0) & (SecondFuel_power[j, i, 0] > 0))
                    {
                        FixedCost[j, i, 1] = SecondFuel_Fuel[j, i, 0] * Unit_Data[j, i, 3] / SecondFuel_power[j, i, 0];
                    }
                }
            }
            double[, ,] Efficient = new double[Plants_Num, Units_Num, 2];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Efficient[j, i, 0] = 0;
                    Efficient[j, i, 1] = 0;
                    if ((PrimayFuel_Fuel[j, i, 2] > 0) & (PrimayFuel_power[j, i, 2] > 0))
                    {
                        Efficient[j, i, 0] = (860 * PrimayFuel_power[j, i, 2]*1000*100) / (Unit_Data[j, i, 23] * PrimayFuel_Fuel[j, i, 2]);
                    }
                    else
                    {
                        if (Unit_Data[j, i, 8] == 0)
                        {
                            Efficient[j, i, 0] = 0;
                        }
                        else
                        {
                            Efficient[j, i, 0] = (860 * Unit_Data[j, i, 4] * 1000 * 100) / ((Unit_Data[j, i, 8] * Unit_Data[j, i, 4] * Unit_Data[j, i, 4] + Unit_Data[j, i, 9] * Unit_Data[j, i, 4] + Unit_Data[j, i, 10]) * Unit_Data[j, i, 23]);
                        }
                    }
                    if ((SecondFuel_Fuel[j, i, 2] > 0) & (SecondFuel_power[j, i, 2] > 0))
                    {
                        Efficient[j, i, 1] = (860 * SecondFuel_power[j, i, 2] * 1000 * 100) / (Unit_Data[j, i, 24] * SecondFuel_Fuel[j, i, 2]);
                    }
                    else
                    {
                        if (Unit_Data[j, i, 20] == 0)
                        {
                            Efficient[j, i, 1] = 0;
                        }
                        else
                        {
                            Efficient[j, i, 1] = (860 * Unit_Data[j, i, 4] * 1000 * 100) / ((Unit_Data[j, i, 20] * Unit_Data[j, i, 4] * Unit_Data[j, i, 4] + Unit_Data[j, i, 21] * Unit_Data[j, i, 4] + Unit_Data[j, i, 22]) * Unit_Data[j, i, 24]);
                        }
                    }
                }
            }

            double[, , ,] MarginCost = new double[Plants_Num, Units_Num, 2,2];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int m = 0; m < 2; m++)
                    {
                        MarginCost[j, i, 0, m] = 0;
                        MarginCost[j, i, 1, m] = 0;
                        if ((PrimayFuel_Fuel[j, i, m] > 0) & (PrimayFuel_power[j, i, m] > 0) & (PrimayFuel_Fuel[j, i, m+1] > 0) & (PrimayFuel_power[j, i, m+1] > 0))
                        {
                            MarginCost[j, i, 0, m] = (PrimayFuel_Fuel[j, i, m+1] - PrimayFuel_Fuel[j, i, m]) / (PrimayFuel_power[j, i, m+1] - PrimayFuel_power[j, i, m]);
                        }
                        if ((SecondFuel_Fuel[j, i, m] > 0) & (SecondFuel_power[j, i, m] > 0) & (SecondFuel_Fuel[j, i, m+1] > 0) & (SecondFuel_power[j, i, m+1] > 0))
                        {
                            MarginCost[j, i, 1, m] = (SecondFuel_Fuel[j, i, m + 1] - SecondFuel_Fuel[j, i, m]) / (SecondFuel_power[j, i, m + 1] - SecondFuel_power[j, i, m]);
                        }
                    }
                }
            }
            double[, ,] Cost_Web = new double[Plants_Num, Units_Num, 2];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Cost_Web[j, i, 0] = 0;
                    Cost_Web[j, i, 1] = 0;
                    if ((PrimayFuel_Fuel[j, i, 0] > 0) & (PrimayFuel_power[j, i, 0] > 0) & (PrimayFuel_Fuel[j, i, 1] > 0) & (PrimayFuel_power[j, i, 1] > 0) & (PrimayFuel_Fuel[j, i, 2] > 0) & (PrimayFuel_power[j, i, 2] > 0))
                    {
                        Cost_Web[j, i, 0] = 1;
                    }
                    if ((SecondFuel_Fuel[j, i, 0] > 0) & (SecondFuel_power[j, i, 0] > 0) & (SecondFuel_Fuel[j, i, 1] > 0) & (SecondFuel_power[j, i, 1] > 0) & (SecondFuel_Fuel[j, i, 2] > 0) & (SecondFuel_power[j, i, 2] > 0))
                    {
                        Cost_Web[j, i, 1] = 1;
                    }
                }
            }
           
            ////////////////////////////////////////////////////////////////
            DataTable tablebasedata = utilities.returntbl("select GasHeatValueAverage,MazutHeatValueAverage,GasOilHeatValueAverage from dbo.BaseData where Date in (select  max(Date) from dbo.BaseData)order by BaseID desc");
            double[] HeatAverage = new double[3];

            //[0]GasHeatValueAverage
            //[1]MazutHeatValueAverage
            //[2]GasOilHeatValueAverage
            if (tablebasedata.Rows[0][0].ToString() != "")
            {
                HeatAverage[0] = double.Parse(tablebasedata.Rows[0][0].ToString());
            }
            if (tablebasedata.Rows[0][1].ToString() != "")
            {

                HeatAverage[1] = double.Parse(tablebasedata.Rows[0][1].ToString());

            }
            if (tablebasedata.Rows[0][2].ToString() != "")
            {

                HeatAverage[2] = double.Parse(tablebasedata.Rows[0][2].ToString());

            }


           /////////////////////////////////////////////////////
            double[, , ,] FuelPrice_unit = new double[Plants_Num, Units_Num, Hour, 3];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Efficient[j, i, 0] >= Marketefficient[h]))
                        {
                            FuelPrice_unit[j, i, h, 0] = FuelPrice[0];
                        }
                       
                        if ((Unit_Dec[j, i, 0] == "Steam") && (Efficient[j, i, 1] >= HeatAverage[1] * Marketefficient[h] / HeatAverage[0]))
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice[1];
                            FuelPrice_unit[j, i, h, 2] = FuelPrice[2];
                        }
                        
                    
                        if ((Unit_Dec[j, i, 0] == "Gas") && (Efficient[j, i, 1] >= HeatAverage[2] * Marketefficient[h] / HeatAverage[0]))
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice[1];
                            FuelPrice_unit[j, i, h, 2] = FuelPrice[2];
                        }
                   

                        if ((Efficient[j, i, 0] < Marketefficient[h]))
                        {
                            FuelPrice_unit[j, i, h, 0] = FuelPrice[0] * (Efficient[j, i, 0] / Marketefficient[h]) + FuelPrice_Free[0] * ((Marketefficient[h] - Efficient[j, i, 0]) / Marketefficient[h]);
                        }

                      
                        if ((Unit_Dec[j, i, 0] == "Steam") && (Efficient[j, i, 1] < HeatAverage[1] * Marketefficient[h] / HeatAverage[0]))
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice[1] * (Efficient[j, i, 1] / Marketefficient[h]) + FuelPrice_Free[1] * ((Marketefficient[h] - Efficient[j, i, 1]) / Marketefficient[h]);
                            FuelPrice_unit[j, i, h, 2] = FuelPrice[2] * (Efficient[j, i, 1] / Marketefficient[h]) + FuelPrice_Free[2] * ((Marketefficient[h] - Efficient[j, i, 1]) / Marketefficient[h]);
                        }
                        if ((Unit_Dec[j, i, 0] == "Steam") && (Unit_Dec[j, i, 1] == "CC"))
                        {
                            FuelPrice_unit[j, i, h, 0] = 0;
                            FuelPrice_unit[j, i, h, 1] = 0;
                            FuelPrice_unit[j, i, h, 2] = 0;
                        }
                        
                        if ((Unit_Dec[j, i, 0] == "Gas") && (Efficient[j, i, 1] < HeatAverage[2] * Marketefficient[h] / HeatAverage[0]))
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice[1] * (Efficient[j, i, 1] / Marketefficient[h]) + FuelPrice_Free[1] * ((Marketefficient[h] - Efficient[j, i, 1]) / Marketefficient[h]);
                            FuelPrice_unit[j, i, h, 2] = FuelPrice[2] * (Efficient[j, i, 1] / Marketefficient[h]) + FuelPrice_Free[2] * ((Marketefficient[h] - Efficient[j, i, 1]) / Marketefficient[h]);
                        }
                    
                    }
                }
            }

            double[, ,] Cost_Per_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int m = 0; m < Mmax; m++)
                        {
                            strCmd = "select count(*) from ConditionUnit" +
                                " where PPID= '" + plant[j] + "'" +
                                " and UnitCode = '" + Unit[j, i] + "'" +
                                " and SecondFuel=1" +
                                " and SecondFuelStartDate<'" + biddingDate + " '" +
                                " AND SecondFuelEndDate>'" + biddingDate + "'";

                            if (utilities.returntbl(strCmd).Rows[0][0].ToString().Trim() != "0")
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Cost_Per_Plant_unit[j, i, h] = (Unit_Data[j, i, 20] * Unit_Data[j, i, 3] * Unit_Data[j, i, 3] + Unit_Data[j, i, 21] * Unit_Data[j, i, 3] + Unit_Data[j, i, 22]) * FuelPrice_unit[j, i, h, 1] / Unit_Data[j, i, 3]; ;
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Cost_Per_Plant_unit[j, i, h] = FixedCost[j, i, 1] * FuelPrice_unit[j, i, h, 1] / Unit_Data[j, i, 3];
                                }
                            }
                            else
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Cost_Per_Plant_unit[j, i, h] = (Unit_Data[j, i, 8] * Unit_Data[j, i, 3] * Unit_Data[j, i, 3] + Unit_Data[j, i, 9] * Unit_Data[j, i, 3] + Unit_Data[j, i, 10]) * FuelPrice_unit[j, i, h, 0] / Unit_Data[j, i, 3]; ;
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Cost_Per_Plant_unit[j, i, h] = FixedCost[j, i, 0] * FuelPrice_unit[j, i, h, 0] / Unit_Data[j, i, 3]; ;
                                }
                            }
                        }
                    }
                }
            }
            double[, ,] Cost_Per_Plant_Pack = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k))
                            {
                                if (Unit_Dec[j, i, 1].ToString().Trim() != "CC")
                                {
                                    Cost_Per_Plant_Pack[j, k, h] = Cost_Per_Plant_unit[j, i, h];
                                }
                                if ((Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                                {
                                    Cost_Per_Plant_Pack[j, k, h] = Cost_Per_Plant_unit[j, i, h]/1.5;
                                }
                            }
                        }
                    }
                }
            }
            /////////////////////////////////with power///////////////////////////////////////////
            //double[] PowerForecast = new double[24];
            //DataTable powertable = utilities.returntbl("select * from dbo.PowerForecast where date='"+biddingDate+"'and gencode='Tehran' order by Id desc");
            //if (powertable.Rows.Count > 0)
            //{
               
            //    for (int i = 0; i < 24; i++)
            //    {
            //        PowerForecast[i] = double.Parse(powertable.Rows[0]["Hour" + (i + 1)].ToString());

            //    }
            //}
           
            //double[] PowerForecast_Must = new double[24];
            //DataTable powertable2 = utilities.returntbl("select * from PowerForecastRival where date='" + biddingDate + "'and gencode='Tehran' order by Id desc");
            //if (powertable.Rows.Count > 0)
            //{

            //    for (int i = 0; i < 24; i++)
            //    {
            //        PowerForecast_Must[i] = double.Parse(powertable2.Rows[0]["Hour" + (i + 1)].ToString());
            //    }
            //}
            
            /////////////////////////////////end with power////////////////////////////////////////

            string[] ptimmForPowerLimitedUnit = new string[Plants_Num];

            if (Plants_Num == 1)
            {
                strCmd = " select distinct PPID from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate<='" + biddingDate +
                    "'AND PPID=" + plant[0];

                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ptimmForPowerLimitedUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }
            }
            else
            {
                strCmd = " select distinct PPID from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate<='" + biddingDate + "'";

                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ptimmForPowerLimitedUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < plann; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ptimmForPowerLimitedUnit[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ptimmForPowerLimitedUnit[i] = temp[i];
            }

            /////////////////////////////////////////////////

            int[] unitname = new int[Plants_Num];

            strCmd = " select UnitCode from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate<='" + biddingDate;


            for (b = 0; b < Plants_Num; b++)
            {
                string strCmd2 = strCmd +
                    "'and PPID='" + ptimmForPowerLimitedUnit[b] + "'";
                oDataTable = utilities.returntbl(strCmd2);
                unitname[b] = oDataTable.Rows.Count;
            }


            double[, ,] Limit_Power = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Dispatch_Complete = new double[Plants_Num, Units_Num, Hour];




            for (int j = 0; j < Plants_Num; j++)
            {

                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        strCmd = "select Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24" +
                            " from dbo.PowerLimitedUnit where  PPID='" + ptimmForPowerLimitedUnit[j] + "'and UnitCode='" + Unit[j, i] + "' and StartDate<='" + biddingDate + "'order by StartDate desc";

                        //strCmd = " select Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24" +
                        //    "  from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate<='" + biddingDate +
                        //    "' AND PowerLimitedUnit.EndDate>='" + biddingDate +
                        //    "' and PPID='" + ptimmForPowerLimitedUnit[j] + "'" +
                        //" and UnitCode='" + Unit[j, i] + "'";

                        oDataTable = utilities.returntbl(strCmd);

                        //foreach (DataRow res in oDataTable.Rows)
                        //{

                        if (oDataTable.Rows.Count > 0)
                        {
                            DataRow res = oDataTable.Rows[0];
                            Limit_Power[j, i, h] = double.Parse(res[h].ToString());

                            if (res[h].ToString() == "")
                            {
                                Limit_Power[j, i, h] = Unit_Data[j, i, 4];
                              
                            }
                        }
                        else
                        {
                            Limit_Power[j, i, h] = Unit_Data[j, i, 4];
                        }
                    }
                }
            }
            ////////////////////////////////
            // Dispachable Daily
            int[, ,] Outservice_Plant_unit = new int[Plants_Num, Units_Num, 24];
             for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Dispatch_Complete[j, i, h] = 1;
                        if (Limit_Power[j, i, h] > 1.2*Unit_Data[j, i, 4])
                        {
                            Limit_Power[j, i, h] = Unit_Data[j, i, 4];
                            Dispatch_Complete[j, i, h] = 0;
                        }
                        //if (Limit_Power[j, i, h] < 0.95 * Unit_Data[j, i, 3])
                        //{
                        //    Limit_Power[j, i, h] = Unit_Data[j, i, 4];
                        //    Dispatch_Complete[j, i, h] = 0;
                        //}
                        //if (Limit_Power[j, i, h] == 0)
                        //{
                        //    Limit_Power[j, i, h] = Unit_Data[j, i, 4];
                        //    Dispatch_Complete[j, i, h] = 0;
                        //}
                        if (Limit_Power[j, i, h] == 0)
                        {
                           Limit_Power[j, i, h] = Unit_Data[j, i, 4];
                            Outservice_Plant_unit[j, i, h] = 1;
                        }
                    }
                }
            }
            double[, ,] Dispatch_Complete_Pack = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < 24; h++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k))
                            {
                                Dispatch_Complete_Pack[j, k, h] = Dispatch_Complete_Pack[j, k, h] + Dispatch_Complete[j, i, h];
                            }
                        }
                    }
                }
            }
            double[, ,] Pmax_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Pmax_Plant_unit[j, i, h] = Limit_Power[j, i, h];
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[] ppkForConditionUnit = new string[Plants_Num];

            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID,Date from dbo.ConditionUnit where( ConditionUnit.OutServiceStartDate<='" + biddingDate +
                    "'AND ConditionUnit.OutServiceEndDate>='" + biddingDate +
                    "')or( dbo.ConditionUnit.MaintenanceStartDate<='" + biddingDate +
                    "'and dbo.ConditionUnit.MaintenanceEndDate>='" + biddingDate + "')" +
                    " order by Date Desc";
                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ppkForConditionUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < plann; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppkForConditionUnit[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }
                for (int i = 0; i < Plants_Num; i++)
                    ppkForConditionUnit[i] = temp[i];
            }
            else
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where ((ConditionUnit.OutServiceStartDate<='" + biddingDate +
                    "'AND ConditionUnit.OutServiceEndDate>='" + biddingDate +
                    "')or (dbo.ConditionUnit.MaintenanceStartDate<='" + biddingDate +
                    "'and dbo.ConditionUnit.MaintenanceEndDate>='" + biddingDate + "'" +
                    " ))AND PPID=" + plant[0];
                //" order by Date Desc";

                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ppkForConditionUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }
            }

            ///////////////////////
            int[] uukForConditionUnit = new int[Plants_Num];
            strCmd = " select UnitCode from dbo.ConditionUnit where(( ConditionUnit.OutServiceStartDate<='" + biddingDate +
                       "'AND ConditionUnit.OutServiceEndDate>='" + biddingDate +
                       "')or( dbo.ConditionUnit.MaintenanceStartDate<='" + biddingDate +
                       "'and dbo.ConditionUnit.MaintenanceEndDate>='" + biddingDate;


            for (b = 0; b < Plants_Num; b++)
            {
                string strCmd2 = strCmd +
                    "'))and PPID='" + ppkForConditionUnit[b] + "'" + " order by Date Desc"; ;
                oDataTable = utilities.returntbl(strCmd2);
                ///////j//////////
                if (oDataTable.Rows.Count != 0)
                {
                    uukForConditionUnit[b] = oDataTable.Rows.Count;
                }
            }


            //--------------------------------------------------------------
            bool om = false;
            bool op = false;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < 24; h++)
                    {

                        ////////////////////////////maintenance///////////////////////
                        DataTable secondcondition = utilities.returntbl("SELECT  Maintenance,MaintenanceStartDate,MaintenanceEndDate," +
                           " MaintenanceStartHour,MaintenanceEndHour FROM [ConditionUnit] where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "'and Date<=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "')order by Date desc");

                        if (secondcondition.Rows.Count > 0)
                        {
                            DataRow MyRow = secondcondition.Rows[0];
                            
                            if (bool.Parse(MyRow[0].ToString().Trim()))
                            {
                                
                                int myhour = h;
                                string startDate = MyRow[1].ToString().Trim();
                                string endDate = MyRow[2].ToString().Trim();
                                int startHour = (int.Parse(MyRow[3].ToString().Trim())-1);
                                int endHour = (int.Parse(MyRow[4].ToString().Trim())-1);


                                if (startDate == endDate && biddingDate==endDate)
                                {
                                    if (myhour > endHour) om = false;
                                                                      
                                    else om = true;
                                    
                                    if (myhour < startHour) om = false;
                                                                           
                                }

                                 else if (biddingDate == startDate)
                                    if (myhour < startHour)
                                        om = false;
                                    else om = true;
                                else if (biddingDate == endDate)
                                    if (myhour > endHour)
                                        om = false;
                                    else om = true;
                                else if (CheckDateSF(biddingDate, startDate, endDate))
                                    om = true;
                                else om = false;
                            }

                            else om = false;
                        }
                        else om = false;
                        ///////////////////outservice///////////////////
                        DataTable outcondition = utilities.returntbl("SELECT  OutService,OutServiceStartDate,OutServiceEndDate," +
                          " OutServiceStartHour,OutServiceEndHour FROM [ConditionUnit] where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "'and Date<=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "')order by Date desc");

                        if (outcondition.Rows.Count > 0)
                        {
                            DataRow MyRow = outcondition.Rows[0];
                            
                            if (bool.Parse(MyRow[0].ToString().Trim()))
                            {
                                
                                int myhour = h;
                                string startDate = MyRow[1].ToString().Trim();
                                string endDate = MyRow[2].ToString().Trim();
                                int startHour =(int.Parse(MyRow[3].ToString().Trim())-1);
                                int endHour =(int.Parse(MyRow[4].ToString().Trim())-1);

                                if (startDate == endDate && biddingDate == endDate)
                                {
                                    if (myhour > endHour) op = false;
                                    else op = true;

                                    if (myhour < startHour) op = false;
                                   
                                }
                                    
                                else if (biddingDate == startDate)
                                    if (myhour < startHour)
                                        op = false;
                                    else op = true;
                                else if (biddingDate == endDate)
                                    if (myhour > endHour)
                                        op = false;
                                    else op = true;
                                
                                else if (CheckDateSF(biddingDate, startDate, endDate))
                                    op = true;
                                else op = false;
                            }
                            else op = false;
                        }

                        else op = false;

                //////////////////////////////////////////////////////////////////////

                        if (om == true || op==true)
                        {
                            Outservice_Plant_unit[j, i, h] = 1;
                        }
                                             
                    }
                }
            }

            int[, ,] CC_Steam_Out = new int[Plants_Num, Package_Num, Hour];
            for (int h = 0; h < Hour; h++)
            {
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        CC_Steam_Out[j, k, h] = 0;
                        if (Package[j, k].ToString().Trim() == "CC")
                        {
                            for (int i = 0; i < Plant_units_Num[j]; i++)
                            {
                                if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam") & (Unit_Data[j, i, 0] == k) & (Outservice_Plant_unit[j,i,h]==1))
                                {
                                    CC_Steam_Out[j, k, h] = 1;  
                                }
                            }

                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
         
            ///////////////////////////////khorsand/////////////////////////////////
           
            int[,] MustMaxop = new int[Plants_Num, Package_Num];

            for (int j = 0; j < Plants_Num; j++)
            {
                
                DataTable detectcode = utilities.returntbl("SELECT MustMax,UnitCode FROM dbo.ConditionUnit WHERE PPID='"+plant[j]+"' and Date=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "')");

                foreach (DataRow myrow in detectcode.Rows)
                {
                    DataTable mustmaxcondition = utilities.returntbl("SELECT distinct PackageCode FROM dbo.UnitsDataMain WHERE UnitCode='" + myrow[1].ToString().Trim() + "'and PPID='" + plant[j] + "'");
                    int detectonepack=int.Parse(mustmaxcondition.Rows[0][0].ToString());
                  
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {

                        if ((mustmaxcondition.Rows.Count > 0) && (bool.Parse(myrow[0].ToString())) && (k == (detectonepack-1)))
                        {
                            MustMaxop[j, k] = 1;
                        }

                    }
                }
            }

            /////////////////////////////////end///////////////////////////////////
            

            DataTable leveltable = utilities.returntbl("select CostLevel,id from dbo.BiddingStrategySetting where id=(select max(id) from dbo.BiddingStrategySetting where Plant='"+ppName+"')");
            string CostLevel = leveltable.Rows[0][0].ToString().Trim();
            
            
            /////////////////////////////////////////////////////
            int[,] MustRunop = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    DataTable mustruncondition = utilities.returntbl("select MustRun  from dbo.ConditionUnit where PPID='" + plant[j] + "' and  UnitCode='" + Unit[j, i] + "' and Date=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "' and  UnitCode='" + Unit[j, i] + "')");
                    if (mustruncondition.Rows.Count > 0)
                    {
                        if (mustruncondition.Rows[0][0].ToString() != "" && bool.Parse(mustruncondition.Rows[0][0].ToString()))
                        {
                            MustRunop[j, i] = 1;
                        }

                    }
                    else
                    {
                        MustRunop[j, i] = 0;
                    }

                }
            }
            /////////////////////////////////////////////////////////////////////
            int[, ,] MustRun = new int[Plants_Num, Units_Num, Hour];
            if (CostLevel == "Yes")
            {
                for (int j = 0; j < Plants_Num; j++)
                {

                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            MustRun[j, i, h] = 1;

                            for (int k = 0; k < Plant_Packages_Num[j]; k++)
                            {

                                if (((int)Unit_Data[j, i, 0] == k) & (Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Steam") & (Outservice_Plant_unit[j, i, h] == 0))
                                {
                                    MustRun[j, i, h] = 0;
                                }
                                if (((int)Unit_Data[j, i, 0] == k) & (Unit_Dec[j, i, 1].ToString().Trim() == "Steam") & (Unit_Dec[j, i, 0].ToString().Trim() == "Steam") & (Outservice_Plant_unit[j, i, h] == 0))
                                {
                                    MustRun[j, i, h] = 0;
                                }
                                if (((int)Unit_Data[j, i, 0] == k) & (Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Outservice_Plant_unit[j, i, h] == 0) & (CC_Steam_Out[j, k, h] == 0))
                                {
                                    MustRun[j, i, h] = 0;
                                }
                                if (MustRunop[j, i] == 1)
                                {
                                    MustRun[j, i, h] = 0;
                                }
                            }
                        }
                    }
                }
            }
            else
            {

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            MustRun[j, i, h] = 1;
                        }
                    }

                }
            }
          
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        FuelPrice_unit[j, i, h, 0] = FuelPrice_unit[j, i, h, 0];
                        if (Unit_Dec[j, i, 0].ToString().Trim() == "Gas")
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice_unit[j, i, h, 2];
                        }
                        if (Unit_Dec[j, i, 0].ToString().Trim() == "Steam")
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice_unit[j, i, h, 1];
                        }
                        if((MustRun[j, i, h]==0)&(Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                        {
                            FuelPrice_unit[j, i, h, 0]=FuelPrice[0]; 
                            FuelPrice_unit[j, i, h, 1]=FuelPrice[1]; 
                        }
                        if((MustRun[j, i, h]==0)&(Unit_Dec[j, i, 0].ToString().Trim() == "Gas"))
                        {
                            FuelPrice_unit[j, i, h, 0]=FuelPrice[0]; 
                            FuelPrice_unit[j, i, h, 1]=FuelPrice[2]; 
                        }
                        if ((Unit_Dec[j, i, 0] == "Steam") && (Unit_Dec[j, i, 1] == "CC"))
                        {
                            FuelPrice_unit[j, i, h, 0] = 0;
                            FuelPrice_unit[j, i, h, 1] = 0;
                            FuelPrice_unit[j, i, h, 2] = 0;
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //outService for Package :
            int[,,] Outservice = new int[Plants_Num, Package_Num,24];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < 24; h++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k))
                            {
                                Outservice[j, k,h] = Outservice[j, k,h] + Outservice_Plant_unit[j, i, h];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,,] service_Max = new double[Plants_Num, Package_Num,24];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h< 24; h++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k))
                            {
                                if (Outservice_Plant_unit[j, i, h] == 1)
                                {
                                    service_Max[j, k,h] = service_Max[j, k,h] + Pmax_Plant_unit[j, i, h];
                                }
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,,] Outservice_Plant_Pack = new string[Plants_Num, Package_Num,24];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < 24; h++)
                    {
                        Outservice_Plant_Pack[j, k,h] = "No";
                        if ((Package[j, k].ToString().Trim() == "CC") & (Outservice[j, k, h] > 2))
                        {
                            Outservice_Plant_Pack[j, k, h] = "Yes";
                        }
                        if ((Package[j, k].ToString().Trim() != "CC") & (Outservice[j, k, h] > 0))
                        {
                            Outservice_Plant_Pack[j, k,h] = "Yes";
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] SecondFuel_Plant_unit = new int[Plants_Num, Units_Num];
            string[] ppkForSecondFuel = new string[Plants_Num];

            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID,Date from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'" +
                    " order by Date Desc";
                oDataTable = utilities.returntbl(strCmd);

                int ppkn1 = oDataTable.Rows.Count;

                for (b = 0; b < oDataTable.Rows.Count; b++)
                    ppkForSecondFuel[b] = oDataTable.Rows[b][0].ToString().Trim();

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < ppkn1; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppkForSecondFuel[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ppkForSecondFuel[i] = temp[i];

            }
            else
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'" +
                    " AND PPID=" + plant[0];
                //" order by Date Desc";

                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                {
                    ppkForSecondFuel[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

            }

            /////////////////////////////////////

            int[] uukForSecondFuel = new int[Plants_Num];
            strCmd = " select UnitCode  from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate;



            for (b = 0; b < Plants_Num; b++)
            {
                string strCmd2 = strCmd + "'and PPID='" + ppkForSecondFuel[b] + "'" + " order by Date Desc"; ;
                oDataTable = utilities.returntbl(strCmd2);
                uukForSecondFuel[b] = oDataTable.Rows.Count;

            }



            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    strCmd = " select SecondFuel  from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                        "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate +
                        "'and PPID='" + ppkForSecondFuel[j] + "'" +
                        " and UnitCode='" + Unit[j, i] + "'" +
                    " order by Date Desc";


                    oDataTable = utilities.returntbl(strCmd);
                    if (oDataTable.Rows.Count > 0)
                    {
                        DataRow res = oDataTable.Rows[0];
                        if (bool.Parse(res[0].ToString()))
                        {

                            SecondFuel_Plant_unit[j, i] = 1;
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] FuelQuantity_Plant_unit = new double[Plants_Num, Units_Num];
            string[] ppForQuantity = new string[Plants_Num];

            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID,Date from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'" +
                    " order by Date Desc";

                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                    ppForQuantity[b] = oDataTable.Rows[b][0].ToString().Trim();

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < oDataTable.Rows.Count; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppForQuantity[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ppForQuantity[i] = temp[i];


            }
            else
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                         "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'" +
                         " AND PPID = " + plant[0];
                // " order by Date Desc";

                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                    ppForQuantity[b] = oDataTable.Rows[b][0].ToString().Trim();
            }


            int[] uuForQuantity = new int[Plants_Num];
            for (b = 0; b < Plants_Num; b++)
            {
                strCmd = " select UnitCode from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate +
                    "'and PPID='" + ppForQuantity[b] + "'" +
                    " order by Date Desc";


                oDataTable = utilities.returntbl(strCmd);
                uuForQuantity[b] = oDataTable.Rows.Count;
            }


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    strCmd = " select FuelForOneDay from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                        "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate +
                        "'and PPID='" + ppForQuantity[j] + "'" +
                         " and UnitCode='" + Unit[j, i] + "'" +
                    " order by Date Desc";

                    oDataTable = utilities.returntbl(strCmd);

                    if (oDataTable.Rows.Count > 0)
                    {
                        DataRow res = oDataTable.Rows[0];

                        FuelQuantity_Plant_unit[j, i] = double.Parse(res[0].ToString());

                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            double[] LoadForecasting = GetNearestLoadForecastingDateVlues(biddingDate);

            double[] LoadAverage = new double[Hour];
            LoadAverage[0] = LoadForecasting[1];
            for (int h = 1; h < Hour; h++)
            {
                LoadAverage[h] = LoadForecasting[h] + LoadAverage[(h - 1)];
            }

            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------
            //           Price        //
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------

            //**************************************************************************
            //int row = 0;
            //Price forecasting :
            string[,] PackageTypesList = new string[Plants_Num, Enum.GetNames(typeof(PackageTypePriority)).Length];

            for (int j = 0; j < Plants_Num; j++)
            {

                string cmd = "select Distinct PackageType from dbo.UnitsDataMain where PPID=" + plant[j];
                //" AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%')AND(UnitCode not like 'Gas cc%')))";
                oDataTable = utilities.returntbl(cmd);
                for (int k = 0; k < oDataTable.Rows.Count; k++)
                    PackageTypesList[j, k] = oDataTable.Rows[k][0].ToString().Trim();

            }

            double[, ,] PriceForecastings = new double[Plants_Num, Hour, Enum.GetNames(typeof(PackageTypePriority)).Length];
           // double[, ,] PriceForecastings2 = new double[Plants_Num, Hour, Enum.GetNames(typeof(PackageTypePriority)).Length];

            bool[] forcatingAvailable = new bool[Plants_Num];

            for (int j = 0; j < Plants_Num; j++)
            {

                //row = 0;
                for (int p = 0; p<PackageTypes[j].Count; p++)
                {
                    forcatingAvailable[j] = true;
                    string strCommand = "select FinalForecastHourly.forecast from FinalForecastHourly " +
                            " inner join FinalForecast " +
                            " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id  " +
                            " where FinalForecast.PPId =  " + plant[j] +
                            " AND FinalForecast.date = '" + biddingDate + "'";

                    oDataTable = utilities.returntbl(strCommand + " AND packageType = '" + PackageTypesList[j, p]
                    + "' ORDER BY FinalForecastHourly.hour");
                    ////////////????????????????????????????????????????????????????
                    if (oDataTable.Rows.Count == 0)
                        forcatingAvailable[j] = false;

                    PriceForecastings = InsertToPriceForecasting(oDataTable, PriceForecastings, j, p);

                    //////////////////for presolve/////////////////////////


                    if ((ppName.ToLower().Trim() == "all")&&(PackageTypesList[j, p] == "Gas")&& (Ispre == true))
                    {
                        DataTable baseplant = utilities.returntbl("select PPName from dbo.BaseData where Date =(select max(Date) from dbo.BaseData)order by BaseID desc");
                        DataTable idtable = utilities.returntbl("select PPID from dbo.PowerPlant  where PPName='" + baseplant.Rows[0][0].ToString().Trim() + "'");

                        string preplant = idtable.Rows[0][0].ToString().Trim();

                        DataTable packagetable = utilities.returntbl("SELECT DISTINCT  PackageType  FROM dbo.PPUnit WHERE PPID='" + preplant.Trim() + "'");

                        string packtype = "";
                        for (int i = 0; i < packagetable.Rows.Count; i++)
                        {
                            if (packagetable.Rows[i][0].ToString().Trim() == "Steam")
                            {
                                packtype = "Steam";
                                break;
                            }
                            else if (packagetable.Rows[i][0].ToString().Trim() == "Combined Cycle") packtype = "CC";

                        }



                        string strCommand2 = "select FinalForecastHourly.forecast from FinalForecastHourly " +
                           " inner join FinalForecast " +
                           " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id  " +
                           " where FinalForecast.PPId =  " + preplant.Trim() +
                           " AND FinalForecast.date = '" + biddingDate + "'";

                       DataTable oDataTable2 = utilities.returntbl(strCommand2 + " AND packageType = '" + packtype
                        + "' ORDER BY FinalForecastHourly.hour");
                        PriceForecastings = PreSolveInsertToPriceForecasting(oDataTable2, PriceForecastings, j, p);
                       
                    }
                    else if((ppName.ToLower().Trim() != "all")&& (Ispre == true))
                    {
                        DataTable baseplant = utilities.returntbl("select PPName from dbo.BaseData where Date =(select max(Date) from dbo.BaseData)order by BaseID desc");
                        DataTable idtable = utilities.returntbl("select PPID from dbo.PowerPlant  where PPName='" + baseplant.Rows[0][0].ToString().Trim() + "'");

                        string preplant = idtable.Rows[0][0].ToString().Trim();

                        DataTable packagetable = utilities.returntbl("SELECT DISTINCT  PackageType  FROM dbo.PPUnit WHERE PPID='" + preplant.Trim() + "'");

                        string packtype = "";
                        for (int i = 0; i < packagetable.Rows.Count; i++)
                        {
                            if (packagetable.Rows[i][0].ToString().Trim() == "Steam")
                            {
                                packtype = "Steam";
                                break;
                            }
                            else if (packagetable.Rows[i][0].ToString().Trim() == "Combined Cycle") packtype = "CC";

                        }



                        string strCommand2 = "select FinalForecastHourly.forecast from FinalForecastHourly " +
                           " inner join FinalForecast " +
                           " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id  " +
                           " where FinalForecast.PPId =  " + preplant.Trim() +
                           " AND FinalForecast.date = '" + biddingDate + "'";

                        DataTable oDataTable2 = utilities.returntbl(strCommand2 + " AND packageType = '" + packtype
                         + "' ORDER BY FinalForecastHourly.hour");
                        PriceForecastings = PreSolveInsertToPriceForecasting(oDataTable2, PriceForecastings, j, p);


                    }

                    //////////////////////////////////////////////////////////////////////////////////////
                    
                }

                for (int h = 0; h < Hour; h++)
                {
                    for (int row = 0; row < PackageTypes[j].Count/*Enum.GetNames(typeof(PackageTypePriority)).Length*/; row++)
                        if (PriceForecastings[j, h, row] > CapPrice[0])
                            PriceForecastings[j, h, row] = CapPrice[0];
                }
            }


            double[, ,] PriceForecasting = new double[Plants_Num, Package_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int p = 0;
                         p < PackageTypes[j].Count/*Enum.GetNames(typeof(PackageTypePriority)).Length && PackageTypesList[j, p] != null && PackageTypesList[j, p] != ""*/;
                         p++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        if (Package[j, k] == PackageTypesList[j, p])
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                PriceForecasting[j, k, h] = PriceForecastings[j, h, p];
                            }
                        }
                    }
                }
            }


            //**************************************************************************
            //**************************************************************************
            //Price forecasting: pd_range :
            //int row1;

            double[, , ,] pd_ranges = new double[Plants_Num, Hour, Xtrain, 3]; // form hamidi
            double[, , ,] pd_range = new double[Plants_Num, Package_Num, Hour, Xtrain]; // to Khorsand

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int pNum = 0; pNum < PackageTypes[j].Count; pNum++)
                {
                    PackageTypePriority enumPackage =(PackageTypePriority) Enum.Parse(typeof(PackageTypePriority),PackageTypes[j][pNum]);
                    double[,] result = Get_pd_Range(plant[j], enumPackage, biddingDate);

                    if ((ppName.ToLower().Trim() == "all") && (enumPackage.ToString() == "Gas") && (Ispre == true))
                    {
                        result = PreSolveGet_pd_Range(plant[j], enumPackage, biddingDate);
                    }
                    else if ((ppName.ToLower().Trim() != "all") && (Ispre == true))
                    {
                        result = PreSolveGet_pd_Range(plant[j], enumPackage, biddingDate);
                    }


                    for (int h = 0; h < 24; h++)
                        for (int k = 0; k < Xtrain; k++)
                            pd_ranges[j, h, k, pNum] = result[h, k];
                }

            }



            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    int packageTypeOrder = PackageTypes[j].IndexOf(Package[j, k]);

                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            pd_range[j, k, h, x] = pd_ranges[j, h, x, packageTypeOrder];

                        }
                    }
                }
            }
            //**************************************************************************
            //**************************************************************************
            //Price forecasting : pdist:

            double[, , ,] pdists = new double[Plants_Num, Hour, Xtrain, 3]; // from hamidi
            double[, , ,] pdist = new double[Plants_Num, Package_Num, Hour, Xtrain]; // to Khorsand
            double[, ,] pdist_Sum = new double[Plants_Num, Package_Num, Hour]; // from peyman

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int pNum = 0; pNum < PackageTypes[j].Count; pNum++)
                {

                    PackageTypePriority enumPackage = (PackageTypePriority)Enum.Parse(typeof(PackageTypePriority), PackageTypes[j][pNum]);

                    double[,] result = Get_pdist(plant[j], enumPackage, biddingDate);
                   
                    if ((ppName.ToLower().Trim() == "all")&&(enumPackage.ToString() == "Gas") && (Ispre == true))
                    {
                        result = PreSolveGet_pdist(plant[j], enumPackage, biddingDate);

                    }
                    else if ((ppName.ToLower().Trim() != "all") && (Ispre == true))
                    {
                        result = PreSolveGet_pdist(plant[j], enumPackage, biddingDate);
                    }


                    for (int h = 0; h < 24; h++)
                        for (int k = 0; k < Xtrain; k++)
                            pdists[j, h, k, pNum] = result[h, k];

                }

            }


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    int packageTypeOrder = PackageTypes[j].IndexOf(Package[j, k]);
                    
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            pdist[j, k, h, x] = pdists[j, h, x, packageTypeOrder];
                            pdist_Sum[j, k, h] = pdist[j, k, h, x] + pdist_Sum[j, k, h];

                        }
                    }

                }
            }

            //////////////////////////////////////////// From Peyman
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            {
                                {
                                    pdist[j, k, h, x] = pdist[j, k, h, x] / pdist_Sum[j, k, h];
                                }
                            }
                        }
                    }

                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            
            string[, ,] UL_State = new string[Plants_Num, Package_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        UL_State[j, k, h] = "N";
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            // variance Price Forecasting :
            double[, ,] variances = new double[Plants_Num, Hour, 3]; // from hamidi
            double[, ,] Variance = new double[Plants_Num, Package_Num, Hour]; // to Khorsand

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int pNum = 0; pNum < PackageTypes[j].Count; pNum++)
                {
                    PackageTypePriority enumPackage = (PackageTypePriority)Enum.Parse(typeof(PackageTypePriority), PackageTypes[j][pNum]);
                    double[] result = GetVariance(plant[j], enumPackage, biddingDate);


                   
                    if ((ppName.ToLower().Trim() == "all")&&(enumPackage.ToString() == "Gas") && (Ispre == true))
                    {
                        result = PreSolveGetVariance(plant[j], enumPackage, biddingDate);
                    }
                    else if ((Ispre == true) && (ppName.ToLower().Trim() != "all"))
                    {
                        result = PreSolveGetVariance(plant[j], enumPackage, biddingDate);
                    }

                    

                    for (int h = 0; h < 24; h++)
                        variances[j, h, 0] = result[h];
                }
            }


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        int packageTypeOrder = PackageTypes[j].IndexOf(Package[j, k]); 
                        
                        Variance[j, k, h] = variances[j, h, packageTypeOrder];

                    }
                }
            }

            /////////////////////////////////////////////

            double[,] Sum_Variance = new double[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Sum_Variance[j, k] = Sum_Variance[j, k] + Variance[j, k, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Average_Variance = new double[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    Average_Variance[j, k] = (Sum_Variance[j, k] / 24);
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Max_Variance = new double[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Max_Variance[j, k] < Variance[j, k, h])
                        {
                            Max_Variance[j, k] = Variance[j, k, h];
                        }
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Variance[j, k, h]> Limit_variance)
                       {
                           Variance[j, k, h] = Limit_variance;
                       }
                        if (Variance[j, k, h] < Limit_variance_Down)
                        {
                            Variance[j, k, h] = Limit_variance_Down;
                        }

                    }
                }
            }
            //*************************************************************************
            double[, ,] Dispatch_Proposal = new double[Plants_Num, Package_Num, Hour];
            //*************************************************************************
            double[, ,] Price_UL = new double[Plants_Num, Package_Num, Hour];
            double[, ,] Price_optimal = new double[Plants_Num, Package_Num, Hour];
            double[, ,] Price_Margin = new double[Plants_Num, Package_Num, Hour];
            //*************************************************************************
            double[, , ,] Price = new double[Plants_Num, Package_Num, Hour, Step_Num];
            double[, , ,] Power = new double[Plants_Num, Package_Num, Hour, Step_Num];
            //*************************************************************************
            double[, ,] Power_memory = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Power_if = new double[Plants_Num, Units_Num, Hour];
            //*************************************************************************
            double[, ,] Power_Error = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Power_Error_Pack = new double[Plants_Num, Package_Num, Hour];
            //*************************************************************************
            int[, , ,] X_select = new int[Plants_Num, Package_Num, Hour, Power_Num];
            //*************************************************************************
            double[, ,] Power_memory_Pack = new double[Plants_Num, Package_Num, Hour];
            double[, ,] Power_if_Pack = new double[Plants_Num, Package_Num, Hour];
            double[, ,] Power_if_CC = new double[Plants_Num, Package_Num, Hour];
            double[, ,] Test = new double[Plants_Num, Units_Num, Hour];
            //**************************************************************************
            int[, ,] CO_steam = new int[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        CO_steam[j, i, h] = 2;
                    }
                }
            }

            //**************************************************************************
            int[, ,] CO_Dispatch = new int[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        CO_Dispatch[j, i, h] = 2;
                    }
                }
            }

            //**************************************************************************
            string[] Level = new string[Plants_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                Level[j] = "Level1";
                if (plant[j] == "104")
                {
                    Level[j] = "Level2";
                }
            }

            int[] Cost_Level = new int[Plants_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                Cost_Level[j] = 0;
                if (Level[j].ToString().TrimEnd() == "Level1")
                {
                    Cost_Level[j] = 0;
                }
                if (Level[j].ToString().TrimEnd() == "Level2")
                {
                    Cost_Level[j] = 50;
                }
                if (Level[j].ToString().TrimEnd() == "Level3")
                {
                    Cost_Level[j] = 75;
                }
                if (Level[j].ToString().TrimEnd() == "Level4")
                {
                    Cost_Level[j] = 100;
                }
                if (Level[j].ToString().TrimEnd() == "Level5")
                {
                    Cost_Level[j] = 125;
                }
                if (Level[j].ToString().TrimEnd() == "Level6")
                {
                    Cost_Level[j] = 150;
                }
                if (Level[j].ToString().TrimEnd() == "Level7")
                {
                    Cost_Level[j] = 175;
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Pmax_Plant_Pack = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k) && (Outservice_Plant_unit[j, i,h] == 0))
                            {
                                Pmax_Plant_Pack[j, k, h] = Pmax_Plant_Pack[j, k, h] + Pmax_Plant_unit[j, i, h];
                            }
                        }
                    }
                }
            }
           //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Capacity_Plant_Pack = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k) && (Outservice_Plant_unit[j, i,h] == 0))
                            {
                                Capacity_Plant_Pack[j, k, h] = Capacity_Plant_Pack[j, k, h] + Unit_Data[j, i, 4];
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


            //////////////////////////////////WITH POWER///////////////////////////////////////
            //DataTable allnumtable = utilities.returntbl("select distinct PPID from UnitsDataMain");
            //int allnum = allnumtable.Rows.Count;

            //////////////////for all except rivals/////////////////
            //int[] allpackaged = new int[allnum];
            //int[] allunitnums = new int[allnum];
            
          
            //string[] allplantadd = new string[allnum];
            //////////////////////////////////////

            //for (int il = 0; il < allnum; il++)
            //{
            //    allplantadd[il] = allnumtable.Rows[il][0].ToString().Trim();

            //}

            //for (int i = 0; i < allnum; i++)
            //{
            //    DataTable oplantpackall = utilities.returntbl("select Max(PackageCode) from UnitsDataMain where  PPID='" + allplantadd[i] + "'");
            //    allpackaged[i] = (int)oplantpackall.Rows[0][0];
            //}

            //int maxallunit = 0;
            //for (int re = 0; re < allnum; re++)
            //{
            //    DataTable allnametable = utilities.returntbl("select UnitCode from UnitsDataMain where PPID='" + allplantadd[re] + "'");

            //   allunitnums[re] = allnametable.Rows.Count;
            //   if (maxallunit <= allnametable.Rows.Count)
            //   {
            //       maxallunit = allnametable.Rows.Count;
            //   }
              
            //}


            //string[,] allunitnames = new string[allnum, maxallunit];
            //string[,] allpacktype = new string[allnum, maxallunit];
            //int[,] allpackcode = new int [allnum, maxallunit];

            //for (int j = 0; j < allnum; j++)
            //{
            //    DataTable allnametable = utilities.returntbl("select UnitCode,PackageType,PackageCode from UnitsDataMain where PPID='" + allplantadd[j] + "'");
            //    for (int i = 0; i < allunitnums[j]; i++)
            //    {
            //        allunitnames[j, i] = allnametable.Rows[i][0].ToString().Trim();
            //        allpacktype[j, i] = allnametable.Rows[i][1].ToString().Trim();
            //        allpackcode[j,i] = int.Parse(allnametable.Rows[i][2].ToString().Trim());
            //    }
            //}



            //  double [,] allpmax=new double[allnum,maxallunit];

            //for (int j = 0; j < allnum; j++)
            //{
             
            //    for (int i = 0; i < allunitnums[j]; i++)
            //    {
            //        DataTable pmaxtable = utilities.returntbl("select PMax from dbo.UnitsDataMain where UnitCode='" + allunitnames[j, i].Trim() + "'and PPID='" + allplantadd[j] + "'");
            //        allpmax[j, i] = double.Parse(pmaxtable.Rows[0][0].ToString());
            //    }

            //}
            ////////////////////////////////////////////////////////////////

            //int[, ,] allOutservice_Plant_unit = new int[allnum,maxallunit,24];

            //string[] allppkForConditionUnit = new string[allnum];

            //if (allnum > 1)
            //{
            //    strCmd = " select distinct PPID,Date from dbo.ConditionUnit where( ConditionUnit.OutServiceStartDate<='" + biddingDate +
            //        "'AND ConditionUnit.OutServiceEndDate>='" + biddingDate +
            //        "')or( dbo.ConditionUnit.MaintenanceStartDate<='" + biddingDate +
            //        "'and dbo.ConditionUnit.MaintenanceEndDate>='" + biddingDate + "')" +
            //        " order by Date Desc";
            //    oDataTable = utilities.returntbl(strCmd);
            //    int plann = oDataTable.Rows.Count;

            //    for (b = 0; b < plann; b++)
            //    {
            //        allppkForConditionUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
            //    }

            //    string[] alltemp = new string[allnum];
            //    for (int ax6 = 0; ax6 < plann; ax6++)
            //    {
            //        for (int an6 = 0; an6 < Plants_Num; an6++)
            //        {
            //            if (allppkForConditionUnit[ax6] == allplantadd[an6])
            //            {
            //                alltemp[an6] = allplantadd[an6];
            //            }
            //        }
            //    }
            //    for (int i = 0; i < allnum; i++)
            //        allppkForConditionUnit[i] = alltemp[i];
            //}
           

            /////////////////////////
            //int[] alluukForConditionUnit = new int[allnum];
            //strCmd = " select UnitCode from dbo.ConditionUnit where(( ConditionUnit.OutServiceStartDate<='" + biddingDate +
            //           "'AND ConditionUnit.OutServiceEndDate>='" + biddingDate +
            //           "')or( dbo.ConditionUnit.MaintenanceStartDate<='" + biddingDate +
            //           "'and dbo.ConditionUnit.MaintenanceEndDate>='" + biddingDate;


            //for (b = 0; b < allnum; b++)
            //{
            //    string strCmd2 = strCmd +
            //        "'))and PPID='" + allppkForConditionUnit[b] + "'" + " order by Date Desc"; ;
            //    oDataTable = utilities.returntbl(strCmd2);
            //    ///////j//////////
            //    if (oDataTable.Rows.Count != 0)
            //    {
            //       alluukForConditionUnit[b] = oDataTable.Rows.Count;
            //    }
            //}


            ////--------------------------------------------------------------
            //bool allom = false;
            //bool allop = false;
            //for (int j = 0; j < allnum; j++)
            //{
            //    for (int i = 0; i < allunitnums[j]; i++)
            //    {
            //        for (int h = 0; h < 24; h++)
            //        {

            //            ////////////////////////////maintenance///////////////////////
            //            DataTable secondcondition = utilities.returntbl("SELECT  Maintenance,MaintenanceStartDate,MaintenanceEndDate," +
            //               " MaintenanceStartHour,MaintenanceEndHour FROM [ConditionUnit] where PPID='" +allplantadd[j] + "' AND UnitCode='" + allunitnames[j,i] + "'and Date<=(select max(Date) from dbo.ConditionUnit where PPID='" + allplantadd[j] + "' AND UnitCode='" + allunitnames[j,i] + "')order by Date desc");

            //            if (secondcondition.Rows.Count > 0)
            //            {
            //                DataRow MyRow = secondcondition.Rows[0];

            //                if (bool.Parse(MyRow[0].ToString().Trim()))
            //                {

            //                    int myhour = h;
            //                    string startDate = MyRow[1].ToString().Trim();
            //                    string endDate = MyRow[2].ToString().Trim();
            //                    int startHour = (int.Parse(MyRow[3].ToString().Trim()) - 1);
            //                    int endHour = (int.Parse(MyRow[4].ToString().Trim()) - 1);


            //                    if (startDate == endDate && biddingDate == endDate)
            //                    {
            //                        if (myhour > endHour) allom = false;

            //                        else allom = true;

            //                        if (myhour < startHour) allom = false;

            //                    }

            //                    else if (biddingDate == startDate)
            //                        if (myhour < startHour)
            //                            allom = false;
            //                        else om = true;
            //                    else if (biddingDate == endDate)
            //                        if (myhour > endHour)
            //                            allom = false;
            //                        else om = true;
            //                    else if (CheckDateSF(biddingDate, startDate, endDate))
            //                        allom = true;
            //                    else allom = false;
            //                }

            //                else allom = false;
            //            }
            //            else allom = false;
            //            ///////////////////outservice///////////////////
            //            DataTable outcondition = utilities.returntbl("SELECT  OutService,OutServiceStartDate,OutServiceEndDate," +
            //              " OutServiceStartHour,OutServiceEndHour FROM [ConditionUnit] where PPID='" + allplantadd[j] + "' AND UnitCode='" + allunitnames[j,i] + "'and Date<=(select max(Date) from dbo.ConditionUnit where PPID='" + allplantadd[j] + "' AND UnitCode='" + allunitnames[j,i] + "')order by Date desc");

            //            if (outcondition.Rows.Count > 0)
            //            {
            //                DataRow MyRow = outcondition.Rows[0];

            //                if (bool.Parse(MyRow[0].ToString().Trim()))
            //                {

            //                    int myhour = h;
            //                    string startDate = MyRow[1].ToString().Trim();
            //                    string endDate = MyRow[2].ToString().Trim();
            //                    int startHour = (int.Parse(MyRow[3].ToString().Trim()) - 1);
            //                    int endHour = (int.Parse(MyRow[4].ToString().Trim()) - 1);

            //                    if (startDate == endDate && biddingDate == endDate)
            //                    {
            //                        if (myhour > endHour) allop = false;
            //                        else op = true;

            //                        if (myhour < startHour) allop = false;

            //                    }

            //                    else if (biddingDate == startDate)
            //                        if (myhour < startHour)
            //                            allop = false;
            //                        else allop = true;
            //                    else if (biddingDate == endDate)
            //                        if (myhour > endHour)
            //                            allop = false;
            //                        else allop = true;

            //                    else if (CheckDateSF(biddingDate, startDate, endDate))
            //                        allop = true;
            //                    else allop = false;
            //                }
            //                else allop = false;
            //            }

            //            else allop = false;

            //            //////////////////////////////////////////////////////////////////////

            //            if (allom == true || allop == true)
            //            {
            //               allOutservice_Plant_unit[j, i, h] = 1;
            //            }

            //        }
            //    }
            //}



            ////////////////////////////////////////////////////////////////



            //double[] Capacity_Base = new double[ Hour];
            //for (int j = 0; j < allnum; j++)
            //{

            //    for (int h = 0; h < Hour; h++)
            //    {
            //        for (int i = 0; i < allunitnums[j]; i++)
            //        {
            //            if ((allOutservice_Plant_unit[j, i, h] == 0) && ((allpacktype[j, i] == "CC") || (allpacktype[j, i] == "Steam")))
            //            {
            //                Capacity_Base[h] = Capacity_Base[h] + allpmax[j, i];
            //            }
            //        }
            //    }

            //}

            ////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //double[] Capacity_Peak = new double[Hour];
            //for (int j = 0; j < allnum; j++)
            //{

            //    for (int h = 0; h < Hour; h++)
            //    {
            //        for (int i = 0; i < allunitnums[j]; i++)
            //        {
            //            if ((allOutservice_Plant_unit[j, i, h] == 0) && (allpacktype[j, i] == "Gas"))
            //            {
            //                Capacity_Peak[h] = Capacity_Peak[h] + allpmax[j, i];
            //            }
            //        }
            //    }

            //}
            ////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


            //double[] Capacity_Region = new double[Hour];
            //double prival = checkrival();
            //for (int h = 0; h < Hour; h++)
            //{
            //    Capacity_Region[h] += prival;
            //    for (int j = 0; j < allnum; j++)
            //    {
            //        for (int i = 0; i < allunitnums[j]; i++)
            //        {
            //            if ((allOutservice_Plant_unit[j, i, h] == 0))
            //            {
            //                Capacity_Region[h] += allpmax[j, i];
            //            }
            //        }
            //    }

            //}
                     
                        
             //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Pmin_Plant_Pack = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k) && (Outservice_Plant_unit[j, i, h] == 0))
                            {
                                Pmin_Plant_Pack[j, k, h] = Pmin_Plant_Pack[j, k, h] + Unit_Data[j, i, 3];
                            }
                        }
                    }
                }
            }
            //////////////////////////////////////////////////////////////////////////////////////////////
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Number_Count_up = new int[Plants_Num, Units_Num];
            int[,] Number_Count_down = new int[Plants_Num, Units_Num];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] History_Require = new double[Plants_Num, Package_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] History_Dispatch = new double[Plants_Num, Package_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] History_UL = new string[Plants_Num, Package_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,] blocks = Getblock();

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    oDataTable = utilities.returntbl("select Required,Dispatchable,Contribution  from dbo.DetailFRM005 where PPID='" + plant[j] +
                    "'and TargetMarketDate='" + currentDate +
                    "'and Block='" + blocks[j, k] + "'");

                    int r = oDataTable.Rows.Count;

                    if (r > 0)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            History_Require[j, k, h] = double.Parse(oDataTable.Rows[h][0].ToString());
                            History_Dispatch[j, k, h] = double.Parse(oDataTable.Rows[h][1].ToString());
                            History_UL[j, k, h] = oDataTable.Rows[h][2].ToString().Trim();
                        }
                    }
                    else 
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            if (Plant_Packages_Num[j] == Plant_units_Num[j])
                            {
                                History_Require[j, k, h] = Pmax_Plant_Pack[j, k, h];
                                History_Dispatch[j, k, h] = Pmax_Plant_Pack[j, k, h];
                                History_UL[j, k, h] = "N";
                            }
                        }

                    }

                }
            }
            double[, ,] Capacity_Require = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Package[j, k].ToString().Trim() != "Gas"))
                        {
                            Capacity_Require[j, k, h] = History_Require[j, k, h];
                        }
                    }
                }
            }
            double[, ,] Capacity_Dispatch = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Package[j, k].ToString().Trim() != "Gas"))
                        {
                            Capacity_Dispatch[j, k, h] = History_Dispatch[j, k, h];
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (History_UL[j, k, h] == "UL")
                        {
                            UL_State[j, k, h] = "UL";
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            int test_n = 0;
            int[, ,] OneGas = new int[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        OneGas[j, k, h] = 0;
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        if (((int)Unit_Data[j, i, 0] == k))
                        {
                            test_n = 0;
                            if (Package[j, k].ToString().Trim() != "CC")
                            {
                                for (int h = 1; h < Hour; h++)
                                {
                                    if ((History_Require[j, k, Hour - h] > 0) & (test_n == 0))
                                    {
                                        Number_Count_up[j, i] = h;
                                    }
                                    else
                                    {
                                        test_n = 1;
                                    }
                                }
                            }
                            if (Package[j, k].ToString().Trim() == "CC")
                            {
                                for (int h = 1; h < Hour; h++)
                                {
                                    if ((History_Require[j, k, Hour - h] > 0) & (test_n == 0) & (History_Require[j, k, Hour - h] < (Capacity_Plant_Pack[j, k, Hour - h]) / (1.8)) & (OneGas[j, k, h] == 0))
                                    {
                                        OneGas[j, k, h] = 1;
                                        Number_Count_up[j, i] = h;
                                    }
                                    if ((History_Require[j, k, Hour - h] > 0) & (test_n == 0) & (History_Require[j, k, Hour - h] > (Capacity_Plant_Pack[j, k, Hour - h]) / (1.8)))
                                    {
                                        Number_Count_up[j, i] = h;
                                    }
                                    if ((History_Require[j, k, Hour - h] == 0))
                                    {
                                        test_n = 1;
                                    }
                                }
                                if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                                {
                                    Number_Count_up[j, i] = 23;
                                }
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        OneGas[j, k, h] = 0;
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if (((int)Unit_Data[j, i, 0] == k))
                        {
                            test_n = 0;
                            if (Package[j, k].ToString().Trim() != "CC")
                            {
                                for (int h = 1; h < Hour; h++)
                                {
                                    if ((History_Require[j, k, Hour - h] == 0) & (test_n == 0))
                                    {
                                        Number_Count_down[j,i] = h;
                                    }
                                    else
                                    {
                                        test_n = 1;
                                    }
                                }
                            }
                            if (Package[j, k].ToString().Trim() == "CC")
                            {
                                for (int h = 1; h < Hour; h++)
                                {
                                    if ((History_Require[j, k, Hour - h] > 0) & (test_n == 0) & (History_Require[j, k, Hour - h] < (Capacity_Plant_Pack[j, k, Hour - h]) / (2.1)) & (OneGas[j, k, h] == 0))
                                    {
                                        OneGas[j, k, h] = 1;
                                        Number_Count_down[j, i] = h;
                                    }
                                    if ((History_Require[j, k, Hour - h] == 0) & (test_n == 0))
                                    {
                                        Number_Count_down[j, i] = h;
                                    }
                                    if ((History_Require[j, k, Hour - h] > (History_Require[j, k, Hour - h]) / (2.1)))
                                    {
                                        test_n = 1;
                                    }
                                }
                                if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                                {
                                    Number_Count_down[j, i] = 0;
                                }
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if ((Number_Count_down[j, i] > 0) & (Number_Count_up[j, i] > 0))
                    {
                        Number_Count_down[j, i] = 0;
                    }
                    if (Outservice_Plant_unit[j, i, 0] == 1)
                    {
                        Number_Count_down[j, i] = 0;
                    }
                }
            }
              
            int[,] hisstart_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_up[j, i] < GastostartSteam)
                    {
                        hisstart_Plant_unit[j, i] = Number_Count_up[j, i];
                    }
                    if (Number_Count_up[j, i] >= GastostartSteam)
                    {
                        hisstart_Plant_unit[j, i] = GastostartSteam-1;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] HisShut_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_down[j, i] < Unit_Data[j, i, 16])
                    {
                        HisShut_Plant_unit[j, i] = Number_Count_down[j, i];
                    }
                    if (Number_Count_down[j, i] >= Unit_Data[j, i, 16])
                    {
                        HisShut_Plant_unit[j, i] = (int)(Unit_Data[j, i, 16] - 1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Powerhistory_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_up[j, i] >= 0)
                    {
                        Powerhistory_Plant_unit[j, i] = ((int)Unit_Data[j, i, 3]);
                    }
                    if (Number_Count_down[j, i] > 0)
                    {
                        Powerhistory_Plant_unit[j, i] = 0;
                    }
                }
            }
            int[,] V_history_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_up[j, i] >= 0)
                    {
                        V_history_Plant_unit[j, i] = 1;
                    }
                    if (Number_Count_down[j, i] > 0)
                    {
                        V_history_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Hisup_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if ((Number_Count_up[j, i] < Unit_Data[j, i, 6]) & (Number_Count_up[j, i] != 0))
                    {
                        Hisup_Plant_unit[j, i] = (int)Unit_Data[j, i, 6] - Number_Count_up[j, i];
                    }
                    if (Number_Count_up[j, i] >= Unit_Data[j, i, 6])
                    {
                        Hisup_Plant_unit[j, i] = 0;
                    }
                    if (Number_Count_up[j, i] == 0)
                    {
                        Hisup_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Hisdown_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if ((Number_Count_down[j, i] < Unit_Data[j, i, 7]) & (Number_Count_down[j, i] > 0))
                    {
                        Hisdown_Plant_unit[j, i] = (int)Unit_Data[j, i, 7] - Number_Count_down[j, i];
                    }
                    if (Number_Count_down[j, i] >= Unit_Data[j, i, 7])
                    {
                        Hisdown_Plant_unit[j, i] = 0;
                    }
                    if (Number_Count_down[j, i] == 0)
                    {
                        Hisdown_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[] ppMinPower = new string[Plants_Num];
            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID from dbo.ConditionPlant where ConditionPlant.OutServiceStartDate<='" + biddingDate +
                    "'AND ConditionPlant.OutServiceEndDate>='" + biddingDate + "'";
                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                {
                    ppMinPower[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < oDataTable.Rows.Count; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppMinPower[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ppMinPower[i] = temp[i];
            }

            else
            {
                strCmd = " select distinct PPID from dbo.ConditionPlant where ConditionPlant.OutServiceStartDate<='" + biddingDate +
                        "'AND ConditionPlant.OutServiceEndDate>='" + biddingDate + "'" +
                        " AND PPID=" + plant[0];
                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                {
                    ppMinPower[b] = oDataTable.Rows[b][0].ToString().Trim();
                }
            }


            double[,] Min_Power = new double[Plants_Num, Hour];


            for (int j = 0; j < Plants_Num; j++)
            {
                strCmd = " select PowerMinHour1,PowerMinHour2,PowerMinHour3,PowerMinHour4,PowerMinHour5,PowerMinHour6,PowerMinHour7,PowerMinHour8,PowerMinHour9,PowerMinHour10,PowerMinHour11,PowerMinHour12,PowerMinHour13,PowerMinHour14,PowerMinHour15,PowerMinHour16,PowerMinHour17,PowerMinHour18,PowerMinHour19,PowerMinHour20,PowerMinHour21,PowerMinHour22,PowerMinHour23,PowerMinHour24 from dbo.ConditionPlant" +
                     " where ConditionPlant.PowerMinStartDate<='" + biddingDate +
                     "'AND ConditionPlant.PowerMinEndDate>='" + biddingDate +
                     "'and PPID='" + ppMinPower[j] + "'";
                oDataTable = utilities.returntbl(strCmd);

                if (oDataTable.Rows.Count > 0)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Min_Power[j, h] = double.Parse(oDataTable.Rows[0][h].ToString());
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[] Price_Min = new double[Hour];
            ///////////////////////

            string CommandText5 = "select Date,AcceptedMin from AveragePrice where Date<='" + biddingDate.Trim() + "'";
            oDataTable = utilities.returntbl(CommandText5);


            DateTime dtDate = PersianDateConverter.ToGregorianDateTime(biddingDate);
            DataRow rowNearest = null;
            TimeSpan minSpan = new TimeSpan();

            foreach (DataRow row in oDataTable.Rows)
            {
                DateTime tempDate = PersianDateConverter.ToGregorianDateTime
                    (row["Date"].ToString().Trim());
                TimeSpan tempSpan = dtDate - tempDate;
                if (rowNearest == null || minSpan > tempSpan)
                {
                    minSpan = tempSpan;
                    rowNearest = row;
                }
            }

            if (rowNearest != null)
            {
                string date2 = rowNearest["Date"].ToString().Trim();

                //////////////////////////////////////////


                string commandtext6 = "select AcceptedMin from AveragePrice where Date='" + date2.ToString().Trim() + "'";
                oDataTable = utilities.returntbl(commandtext6);


                for (int h = 0; h < 24 && h < oDataTable.Rows.Count; h++)
                {

                    Price_Min[h] = double.Parse(oDataTable.Rows[h][0].ToString());
                }

            }

            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------

            double[, , ,] Price_lost_initial = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 1; x < Xtrain; x++)
                        {
                            Price_lost_initial[j, k, h, x] = Math.Abs(pd_range[j, k, h, x] - pd_range[j, k, h, (x - 1)]+1) * pdist[j, k, h, x] + Price_lost_initial[j, k, h, (x - 1)];
                        }
                        Price_lost_initial[j, k, h, 0] = Price_lost_initial[j, k, h, 1];
                    }
                }
            }

            //**************************************************************************
            double[, , ,] Price_lost = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            //Price_lost[j, k, h, x] = Price_lost_initial[j, k, h, x] + Math.Abs(1 - Price_lost_initial[j, k, h, (Xtrain - 1)]) / 2;
                            Price_lost[j, k, h, x] = Price_lost_initial[j, k, h, x];
                        }
                    }
                }

            }
            //**************************************************************************


            double[, , ,] Price_win = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Price_win[j, k, h, x] = Price_lost_initial[j, k, h, (Xtrain - 1)] - Price_lost[j, k, h, x];
                        }
                    }
                }
            }

            //**************************************************************************


            double[, , ,] Price_win_price = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Price_win_price[j, k, h, x] = Price_win[j, k, h, x] * pdist[j, k, h, x];
                        }
                    }
                }

            }

            //**************************************************************************


            double[, , ,] Price_lost_price = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Price_lost_price[j, k, h, x] = Price_lost[j, k, h, x] * pdist[j, k, h, x];
                        }
                    }
                }
            }

            //**************************************************************************


            double[, , ,] Profit_win_lost = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Profit_win_lost[j, k, h, x] = Price_win_price[j, k, h, x] * pd_range[j, k, h, x] - Price_lost_price[j, k, h, x] * (pd_range[j, k, h, x] - Price_Min[h]);
                        }
                    }
                }
            }


            //**************************************************************************

            double[, , ,] Profit_win = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Profit_win[j, k, h, x] = Price_win_price[j, k, h, x] * pd_range[j, k, h, x];
                        }
                    }
                }

            }
            //**************************************************************************

            double[, , ,] Profit_Margin = new double[Plants_Num, Package_Num, Hour, Xtrain];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int x = 0; x < Xtrain; x++)
                        {
                            Profit_Margin[j, k, h, x] = Price_lost_price[j, k, h, x] * pd_range[j, k, h, x];
                        }
                    }
                }
            }

            //**************************************************************************
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            try
            {
                Cplex cplex = new Cplex();

                // define variable :

                INumVar[, ,] Point_UL = new INumVar[Plants_Num, Package_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Point_UL[j, k, h] = cplex.NumVar(-double.MaxValue, double.MaxValue);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
                INumVar[, ,] Point_Opimal = new INumVar[Plants_Num, Package_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Point_Opimal[j, k, h] = cplex.NumVar(-double.MaxValue, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
                INumVar[, ,] Point_Margin = new INumVar[Plants_Num, Package_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Point_Margin[j, k, h] = cplex.NumVar(-double.MaxValue, double.MaxValue);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
                ILinearNumExpr Sum_Point_Margin = cplex.LinearNumExpr();
                double _Sum_Point_Margin = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Point_Margin.AddTerm(_Sum_Point_Margin, Point_Margin[j, k, h]);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                ILinearNumExpr Sum_Point_UL = cplex.LinearNumExpr();
                double _Sum_Point_UL = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Point_UL.AddTerm(_Sum_Point_UL, Point_UL[j, k, h]);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                ILinearNumExpr Sum_Point_Opimal = cplex.LinearNumExpr();
                double _Sum_Point_Opimal = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Point_Opimal.AddTerm(_Sum_Point_Opimal, Point_Opimal[j, k, h]);
                        }
                    }
                }


                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int x = 0; x < Xtrain; x++)
                            {
                                cplex.AddGe(Point_UL[j, k, h], Profit_win_lost[j, k, h, x]);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int x = 0; x < Xtrain; x++)
                            {
                                cplex.AddGe(Point_Opimal[j, k, h], Profit_win[j, k, h, x]);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int x = 0; x < Xtrain; x++)
                            {
                                cplex.AddGe(Point_Margin[j, k, h], Profit_Margin[j, k, h, x]);
                            }
                        }
                    }
                }

                //*******************************************************************************************************
                cplex.AddMinimize(cplex.Sum(Sum_Point_Opimal, cplex.Sum(Sum_Point_Margin, Sum_Point_UL)));
                //*******************************************************************************************************
                //Export
                //*******************************************************************************************************

                if (cplex.Solve())
                {
                    CplexPrice = 1;
                    //////////////////////////new////////////////////////
                    int[, ,] xpeak = new int[Plants_Num, Package_Num, Hour];
                    double[, ,] rangepeak = new double[Plants_Num, Package_Num, Hour];

                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {

                                for (int x = 0; x < Xtrain-1; x++)
                                {

                                    if (pdist[j, k, h, x + 1] < pdist[j, k, h, x])
                                    {

                                        xpeak[j, k, h] = x;
                                        rangepeak[j, k, h] = pd_range[j, k, h, x];
                                        break;


                                    }

                                }
                            }
                        }
                    }
                    /////////////////////////////////////////////////////////////////////


                    int[, ,] Bench = new int[Plants_Num, Package_Num, Hour];
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                PriceForecasting[j, k, h] = rangepeak[j, k, h];
                                Bench[j, k, h] = 0;
                                for (int x = 0; x < Xtrain; x++)
                                {
                                    if (Profit_win[j, k, h, x] == cplex.GetValue(Point_Opimal[j, k, h]))
                                    {
                                        Price_optimal[j, k, h] = pd_range[j, k, h, x];
                                        X_select[j, k, h, 1] = x;
                                    }
                                    if (Profit_win_lost[j, k, h, x] == cplex.GetValue(Point_UL[j, k, h]))
                                    {
                                        Price_UL[j, k, h] = pd_range[j, k, h, x];
                                        X_select[j, k, h, 0] = x;
                                    }

                                    if (Profit_Margin[j, k, h, x] == cplex.GetValue(Point_Margin[j, k, h]))
                                    {
                                        Price_Margin[j, k, h] = pd_range[j, k, h, x];
                                        X_select[j, k, h, 3] = x;
                                    }
                                    if ((rangepeak[j, k, h] < pd_range[j, k, h, x]) & (Bench[j, k, h] == 0))
                                    {
                                        Bench[j, k, h] = 1;
                                    }
                                    if (Bench[j, k, h] == 1)
                                    {
                                        X_select[j, k, h, 2] = x;
                                        Bench[j, k, h] = 2;
                                    }
                                }
                            }
                        }
                    }



                    ////////////khorsand new////////////// 
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                if (PriceForecasting[j, k, h] > Price_Margin[j, k, h])
                                {
                                    PriceForecasting[j, k, h] = (Price_Margin[j, k, h] + Price_optimal[j, k, h]) / 2;
                                }
                                if (Price_optimal[j, k, h] > PriceForecasting[j, k, h])
                                {
                                    Price_optimal[j, k, h] = (PriceForecasting[j, k, h] + Price_UL[j, k, h]) / 2;
                                }
                                if (Price_UL[j, k, h] > Price_optimal[j, k, h])
                                {
                                    Price_UL[j, k, h] = (Price_optimal[j, k, h] - ErrorBid[0]);
                                }
                                if (PriceForecasting[j, k, h] < Price_optimal[j, k, h])
                                {
                                    PriceForecasting[j, k, h] = Price_Margin[j, k, h] + Price_optimal[j, k, h] / 2;
                                }
                            }
                        }
                    }

            ///////////////////////////////////////////////////
                }

                cplex.End();
            }
            catch (ILOG.CPLEX.CouldNotInstallColumnException e1)
            {
                System.Console.WriteLine("concert exception" + e1 + "caught");
            }

            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------
            //Program-2 : Calculate PBUC //
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~khorsand~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //double[, ,] maxs = new double[Plants_Num, Const_selectdays, 24];
            //for (int j = 0; j < Plants_Num; j++)
            //{
            //    DataSet myDs = UtilityPowerPlantFunctions.GetUnits(int.Parse(plant[j]));
            //    PersianDate test1 = new PersianDate(currentDate);
            //    DateTime Now = PersianDateConverter.ToGregorianDateTime(test1);

            //    DataTable[] orderedPackages = UtilityPowerPlantFunctions.OrderPackages(myDs);


            //    foreach (DataTable dtPackageType in orderedPackages)
            //    {
            //        for (int daysbefore = 0; daysbefore < Const_selectdays; daysbefore++)
            //        {
            //            DateTime selectedDate = Now.Subtract(new TimeSpan(daysbefore, 0, 0, 0));

            //            ////////////check 002 and 005 not empty////////////////////////////

            //            selectedDate = PersianDateConverter.ToGregorianDateTime(FindCommon002and005(selectedDate));

           
            //            ///////////////////////////////////////////////////////////////////
            //            int packageOrder = 0;
            //            if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
            //            orderedPackages.Length > 1)
            //                packageOrder = 1;

            //            CMatrix NSelectPrice = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid
            //                   (Convert.ToInt32(plant[j]), dtPackageType, selectedDate, packageOrder);
                      


            //            for (int k = 0; k < 24; k++)
            //                maxs[j, daysbefore, k] = NSelectPrice[0, k];
            //        }
            //    }
            //}

            //////////////////////////////khorsand////////////////////////////////


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            double[, , ,] Old_price = new double[Plants_Num, Package_Num, oldday, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int d = 0; d < oldday; d++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Old_price[j, k, d, h] = maxs[j, d, h];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_sum = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_sum[j, k, h] = 0;
                        for (int d = 0; d < oldday; d++)
                        {
                            Old_price_sum[j, k, h] = Old_price_sum[j, k, h] + Old_price[j, k, d, h];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_Ave = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_Ave[j, k, h] = Old_price_sum[j, k, h] * Math.Pow(oldday, -1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price_diff = new double[Plants_Num, Package_Num, oldday, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int d = 0; d < oldday; d++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Old_price_diff[j, k, d, h] = Old_price[j, k, d, h] - Old_price_Ave[j, k, h];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_diff_Max = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_diff_Max[j, k, h] = 0;
                        for (int d = 0; d < oldday; d++)
                        {
                            if (Old_price_diff_Max[j, k, h] < Math.Abs(Old_price_diff[j, k, d, h]))
                            {
                                Old_price_diff_Max[j, k, h] = Math.Abs(Old_price_diff[j, k, d, h]);
                            }
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, , ,] Old_price_pow = new double[Plants_Num, Package_Num, oldday, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int d = 0; d < oldday; d++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Old_price_pow[j, k, d, h] = Math.Pow(Old_price_diff[j, k, d, h], 2);
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_pow_sum = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_pow_sum[j, k, h] = 0;
                        for (int d = 0; d < oldday; d++)
                        {
                            Old_price_pow_sum[j, k, h] = Old_price_pow_sum[j, k, h] + Old_price_pow[j, k, d, h];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_pow_Ave = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_pow_Ave[j, k, h] = Old_price_pow_sum[j, k, h] * Math.Pow(oldday, -1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Old_price_delta = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Old_price_delta[j, k, h] = Math.Pow(Old_price_pow_Ave[j, k, h], 0.5);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] price_diff = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        price_diff[j, k, h] = Math.Abs(Old_price_Ave[j, k, h] - PriceForecasting[j, k, h]);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Price_delta = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Price_delta[j, k, h] = Old_price_delta[j, k, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Select_Hour_Cap = new string[Hour, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Old_price_Ave[j, k, h] > (Factor_Price * CapPrice[0])))
                        {
                            Select_Hour_Cap[j, k, h] = "Yes";
                        }
                        else
                        {
                            Select_Hour_Cap[j, k, h] = "NO";
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Price_delta_sum = new double[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Price_delta_sum[j, k] = Price_delta[j, k, h] + Price_delta_sum[j, k];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Price_delta_Ave = new double[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    Price_delta_Ave[j, k] = Price_delta_sum[j, k] * Math.Pow(24, -1); ;
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Torerance = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Torerance[j, k, h] = Risk_Max;
                        if (Select_Hour_Cap[j, k, h] == "Yes")
                        {
                            Torerance[j, k, h] = Risk_Peak;
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Price_delta[j, k, h] > Price_delta_Max)
                        {
                            Price_delta[j, k, h] = Price_delta_Max;
                        }
                        if (Price_delta[j, k, h] < Price_delta_Min)
                        {
                            Price_delta[j, k, h] = Price_delta_Min;
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
           
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (PriceForecasting[j, k, h] >= Old_price_Ave[j, k, h])
                        {
                            Price_UL[j, k, h] = Price_UL[j, k, h] - Variance[j, k, h];
                            Price_optimal[j, k, h] = Price_optimal[j, k, h] - Variance[j, k, h];
                            Price_Margin[j, k, h] = Price_Margin[j, k, h] - Variance[j, k, h];
                            PriceForecasting[j, k, h] = PriceForecasting[j, k, h] - Variance[j, k, h];
                        }
                        else
                        {
                            Price_UL[j, k, h] = Price_UL[j, k, h] + Variance[j, k, h];
                            Price_optimal[j, k, h] = Price_optimal[j, k, h] + Variance[j, k, h];
                            Price_Margin[j, k, h] = Price_Margin[j, k, h] + Variance[j, k, h];
                            PriceForecasting[j, k, h] = PriceForecasting[j, k, h] + Variance[j, k, h];
                        }

                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Margin_up = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if(PriceForecasting[j, k, h] >= Old_price_Ave[j, k, h])
                        {
                            Margin_up[j, k, h] = Price_delta[j, k, h] + Old_price_Ave[j, k, h] + Torerance[j, k, h];
                        }
                        if (PriceForecasting[j, k, h] < Old_price_Ave[j, k, h])
                        {
                            Margin_up[j, k, h] = Price_delta[j, k, h] + Old_price_Ave[j, k, h]+Torerance[j, k, h];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Margin_down = new double[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (PriceForecasting[j, k, h] >= Old_price_Ave[j, k, h])
                        {
                            Margin_down[j, k, h] = Old_price_Ave[j, k, h] - Price_delta[j, k, h] - Torerance[j, k, h];
                        }
                        if (PriceForecasting[j, k, h] < Old_price_Ave[j, k, h])
                        {
                            Margin_down[j, k, h] = Old_price_Ave[j, k, h] - Price_delta[j, k, h] - Torerance[j, k, h]; ;
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Hour_Cap_Automatic = new string[Plants_Num, Package_Num, Hour];
            string[, ,] Hour_Cap_customize = new string[Plants_Num, Package_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Hour_State_Automatic = new string[Plants_Num, Package_Num, Hour];
            string[, ,] Hour_State_customize = new string[Plants_Num, Package_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


            strCmd = "select * from BiddingStrategySetting where id=" + BiddingStrategySettingId.ToString();
            oDataTable = utilities.returntbl(strCmd);

            string runSetting = oDataTable.Rows[0]["RunSetting"].ToString().Trim();
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,] Run_State_user = new string[Plants_Num, Package_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    Run_State_user[j, k] = runSetting.Trim();
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            if (runSetting == "Automatic")
            {
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Hour_Cap_Automatic[j, k, h] = oDataTable.Rows[0]["PeakBid"].ToString().Trim();
                            Hour_State_Automatic[j, k, h] = oDataTable.Rows[0]["StrategyBidding"].ToString().Trim();
                        }
                    }
                }
            }
            else
            {
                strCmd = "select * from BiddingStrategyCustomized where FkBiddingStrategySettingId=" + BiddingStrategySettingId.ToString() + " order by hour";
                oDataTable = utilities.returntbl(strCmd);

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < 24; h++)
                        {
                            Hour_Cap_customize[j, k, h] = oDataTable.Rows[h]["Peak"].ToString().Trim();
                            Hour_State_customize[j, k, h] = oDataTable.Rows[h]["Strategy"].ToString().Trim();
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[, ,] Hour_Cap_User = new string[Hour, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Hour_Cap_User[j, k, h] = "NO";
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Run_State_user[j, k] == "Automatic")
                        {
                            Hour_Cap_User[j, k, h] = Hour_Cap_Automatic[j, k, h];
                        }
                        if (Run_State_user[j, k] == "Customize" && Hour_Cap_Automatic[j, k, h]!="")
                        {
                            Hour_Cap_User[j, k, h] = Hour_Cap_customize[j, k, h];
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[, ,] Hour_State = new string[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Hour_State[j, k, h] = "U-O-F";
                        if (Old_price_delta[j, k, h] < Factor_Delta_Ave)
                        {
                            Hour_State[j, k, h] = "O-F";
                        }
                        if (Select_Hour_Cap[j, k, h] == "Yes")
                        {
                            Hour_State[j, k, h] = "O-F";
                        }
                        //if ((PowerForecast_Must[h] >= Factor_Power * (Capacity_Base[h] + Capacity_Peak[h])))
                        //{
                        //    Hour_State[j, k, h] = "F-M";
                        //}
                        //if ((PowerForecast[h] >= Factor_Power_Region * Capacity_Region[h]))
                        //{
                        //    Hour_State[j, k, h] = "F-M";
                        //}
                        if ((Capacity_Require[j,k,h] < Factor_Dispatch_Befor * Capacity_Dispatch[j,k,h]))
                        {
                            Hour_State[j, k, h] = "U-O";
                        }
                    }
                }
            }
            
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Price_UL[j, k, h] < Margin_down[j, k, h]) && (Price_optimal[j, k, h] > Margin_down[j, k, h]))
                        {
                            Price_UL[j, k, h] = Margin_down[j, k, h];
                        }
                        if ((Price_Margin[j, k, h] > Margin_up[j, k, h]) && (PriceForecasting[j, k, h] < Margin_up[j, k, h]))
                        {
                            Price_Margin[j, k, h] = Margin_up[j, k, h];
                        }
                        if ((Price_Margin[j, k, h] > Margin_up[j, k, h]) && (PriceForecasting[j, k, h] > Margin_up[j, k, h]) && (Price_optimal[j, k, h] < Margin_up[j, k, h]))
                        {
                            PriceForecasting[j, k, h] = Margin_up[j, k, h] - ErrorBid[0];
                            Price_Margin[j, k, h] = Margin_up[j, k, h] ;
                        }
                        if ((Price_Margin[j, k, h] > Margin_up[j, k, h]) && (PriceForecasting[j, k, h] > Margin_up[j, k, h]) && (Price_optimal[j, k, h] > Margin_up[j, k, h]) && (Price_UL[j, k, h] < Margin_up[j, k, h]))
                        {
                            Price_Margin[j, k, h] = Margin_up[j, k, h] + ErrorBid[0]; 
                            PriceForecasting[j, k, h] = Margin_up[j, k, h];
                            Price_optimal[j, k, h] = Margin_up[j, k, h] - ErrorBid[0];
                        }
                        if ((Price_Margin[j, k, h] > Margin_up[j, k, h]) && (PriceForecasting[j, k, h] > Margin_up[j, k, h]) && (Price_optimal[j, k, h] > Margin_up[j, k, h]) && (Price_UL[j, k, h] > Margin_up[j, k, h]))
                        {
                            Price_Margin[j, k, h] = Margin_up[j, k, h] + ErrorBid[0];
                            PriceForecasting[j, k, h] = Margin_up[j, k, h];
                            Price_optimal[j, k, h] = Margin_up[j, k, h] - ErrorBid[0];
                            Price_UL[j, k, h] = Margin_up[j, k, h] - ErrorBid[0];
                        }
                        if ((Price_UL[j, k, h] < Margin_down[j, k, h]) && (Price_optimal[j, k, h] < Margin_down[j, k, h]) && (PriceForecasting[j, k, h] > Margin_down[j, k, h]))
                        {
                            Price_optimal[j, k, h] = PriceForecasting[j, k, h] - ErrorBid[0];
                            Price_UL[j, k, h] = PriceForecasting[j, k, h] - 2*ErrorBid[0];
                        }
                        if ((Price_UL[j, k, h] < Margin_down[j, k, h]) && (Price_optimal[j, k, h] < Margin_down[j, k, h]) && (PriceForecasting[j, k, h] < Margin_down[j, k, h]))
                        {
                            PriceForecasting[j, k, h] = Margin_down[j, k, h]; 
                            Price_optimal[j, k, h] = Margin_down[j, k, h] - ErrorBid[0];
                            Price_UL[j, k, h] = Margin_down[j, k, h]- 2* ErrorBid[0];
                        }
                        if ((Price_UL[j, k, h] < Margin_down[j, k, h]) && (Price_optimal[j, k, h] < Margin_down[j, k, h]) && (PriceForecasting[j, k, h] < Margin_down[j, k, h]) && (Price_Margin[j, k, h] < Margin_down[j, k, h]))
                        {
                            Price_Margin[j, k, h] = Margin_down[j, k, h] + ErrorBid[0];
                            PriceForecasting[j, k, h] = Margin_down[j, k, h];
                            Price_optimal[j, k, h] = Margin_down[j, k, h] - ErrorBid[0];
                            Price_UL[j, k, h] = Margin_down[j, k, h] - 2*ErrorBid[0];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Package[j, k].ToString().Trim() == "Gas")
                        {
                            PriceForecasting[j, k, h] = PriceForecasting[j, k, h]+ Egas;
                            Price_optimal[j, k, h] = Price_optimal[j, k, h]+ Egas;
                            Price_UL[j, k, h] = Price_UL[j, k, h]+ Egas;
                            Price_Margin[j, k, h] = Price_Margin[j, k, h] + Egas;
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[, ,] Hour_State_User = new string[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Hour_State_User[j, k, h] = "Medium";
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Run_State_user[j, k] == "Automatic")
                        {
                            Hour_State_User[j, k, h] = Hour_State_Automatic[j, k, h];
                        }
                        if (Run_State_user[j, k] == "Customize" && Hour_State_customize[j, k, h]!="")
                        {
                            Hour_State_User[j, k, h] = Hour_State_customize[j, k, h];
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "O-F"))
                        {
                            Hour_State[j, k, h] = "U-O";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "F-M"))
                        {
                            Hour_State[j, k, h] = "O-F";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "U-F"))
                        {
                            Hour_State[j, k, h] = "U-O";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "U-M"))
                        {
                            Hour_State[j, k, h] = "U-F";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "O-M"))
                        {
                            Hour_State[j, k, h] = "U-F";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "U-O-F"))
                        {
                            Hour_State[j, k, h] = "U-O";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "U-O-M"))
                        {
                            Hour_State[j, k, h] = "U-O-F";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "U-F-M"))
                        {
                            Hour_State[j, k, h] = "U-O-F";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "O-F-M"))
                        {
                            Hour_State[j, k, h] = "U-O-F";
                        }
                        if ((Hour_State_User[j, k, h] == "Low") & (Hour_State[j, k, h] == "U-O-F-M"))
                        {
                            Hour_State[j, k, h] = "U-O-F";
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "O-F"))
                        {
                            Hour_State[j, k, h] = "F-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "U-O"))
                        {
                            Hour_State[j, k, h] = "O-F";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "U-F"))
                        {
                            Hour_State[j, k, h] = "O-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "U-M"))
                        {
                            Hour_State[j, k, h] = "O-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "O-M"))
                        {
                            Hour_State[j, k, h] = "F-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "U-O-F"))
                        {
                            Hour_State[j, k, h] = "O-F-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "U-O-M"))
                        {
                            Hour_State[j, k, h] = "O-F-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "U-F-M"))
                        {
                            Hour_State[j, k, h] = "O-F-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "O-F-M"))
                        {
                            Hour_State[j, k, h] = "F-M";
                        }
                        if ((Hour_State_User[j, k, h] == "High") & (Hour_State[j, k, h] == "U-O-F-M"))
                        {
                            Hour_State[j, k, h] = "O-F-M";
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (UL_State[j, k, h] == "UL")
                        {
                            Hour_State[j, k, h] = "U-O";
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Run_State = new string[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Run_State[j, k, h] = Hour_State[j, k, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Cap_Bid = new string[Plants_Num, Package_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Cap_Bid[j, k, h] = Hour_Cap_User[j, k, h];
                    }
                }
            }
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------

            double[,,] Tbid_Plant_unit = new double[Plants_Num, Units_Num,Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Tbid_Plant_unit[j, i ,h] = (Pmax_Plant_unit[j, i, h] - Unit_Data[j, i, 3]) / Mmax;
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[, , ,] Fbid_Plant_unit = new double[Plants_Num, Units_Num, Hour, Mmax];
            bool SF = false;

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int m = 0; m < Mmax; m++)
                        {

                            //strCmd = "select count(*) from ConditionUnit" +
                            //    " where PPID= '" + plant[j] + "'" +
                            //    " and UnitCode = '" + Unit[j, i] + "'" +
                            //    " and SecondFuel=1" +
                            //    " and SecondFuelStartDate<'" + biddingDate + " '" +
                            //    " AND SecondFuelEndDate>'" + biddingDate + "'";

                            DataTable secondcondition = utilities.returntbl("SELECT SecondFuel,SecondFuelStartDate,SecondFuelEndDate," +
                            "SecondFuelStartHour,SecondFuelEndHour FROM [ConditionUnit] where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "'and Date<=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "')order by Date desc");

                            if (secondcondition.Rows.Count > 0)
                            {
                                DataRow MyRow = secondcondition.Rows[0];
                                //foreach (DataRow MyRow in secondcondition.Rows)
                                //{
                                if (bool.Parse(MyRow[0].ToString().Trim()))
                                {
                                    //DateTime CurDate = DateTime.Now;
                                    //int myhour=CurDate.Hour;
                                    int myhour = h;
                                    string startDate = MyRow[1].ToString().Trim();
                                    string endDate = MyRow[2].ToString().Trim();
                                    int startHour = (int.Parse(MyRow[3].ToString().Trim())-1);
                                    int endHour = (int.Parse(MyRow[4].ToString().Trim())-1);



                                    if (startDate == endDate && biddingDate == endDate)
                                    {
                                        if (myhour > endHour)

                                            SF = false;
                                        else SF = true;

                                        if (myhour < startHour)
                                            SF = false;


                                    }
                                                                               

                                   else if (biddingDate == startDate)
                                        if (myhour < startHour)
                                            SF = false;
                                        else SF = true;
                                    else if (biddingDate == endDate)
                                        if (myhour > endHour)
                                            SF = false;
                                        else SF = true;
                                    else if (CheckDateSF(biddingDate, startDate, endDate))
                                        SF = true;
                                    else SF = false;
                                }
                                else SF = false;

                            }

                            else SF = false;
                            //}


                            if (SF == true)
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Fbid_Plant_unit[j, i, h, m] = (Unit_Data[j, i, 21] + ((2 * (m + 1)) - 1) * Unit_Data[j, i, 20] * Tbid_Plant_unit[j, i, h] + (2 * Unit_Data[j, i, 20] * Unit_Data[j, i, 3])) * FuelPrice_unit[j, i, h, 1];
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Fbid_Plant_unit[j, i, h, m] = MarginCost[j, i, 1, m] * FuelPrice_unit[j, i, h, 1];
                                }
                            }
                            else
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Fbid_Plant_unit[j, i, h, m] = (Unit_Data[j, i, 9] + ((2 * (m + 1)) - 1) * Unit_Data[j, i, 8] * Tbid_Plant_unit[j, i, h] + (2 * Unit_Data[j, i, 8] * Unit_Data[j, i, 3])) * FuelPrice_unit[j, i, h, 0];
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Fbid_Plant_unit[j, i, h, m] = MarginCost[j, i, 0, m] * FuelPrice_unit[j, i, h, 0];
                                }
                            }
                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[,,] Fmin_Plant_unit = new double[Plants_Num, Units_Num ,Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int m = 0; m < Mmax; m++)
                        {
                            strCmd = "select count(*) from ConditionUnit" +
                                " where PPID= '" + plant[j] + "'" +
                                " and UnitCode = '" + Unit[j, i] + "'" +
                                " and SecondFuel=1" +
                                " and SecondFuelStartDate<'" + biddingDate + " '" +
                                " AND SecondFuelEndDate>'" + biddingDate + "'";

                            if (utilities.returntbl(strCmd).Rows[0][0].ToString().Trim() != "0")
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Fmin_Plant_unit[j, i, h] = (Unit_Data[j, i, 20] * Unit_Data[j, i, 3] * Unit_Data[j, i, 3] + Unit_Data[j, i, 21] * Unit_Data[j, i, 3] + Unit_Data[j, i, 22]) * FuelPrice_unit[j, i, h, 1];
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Fmin_Plant_unit[j, i, h] =  FixedCost[j, i, 1] * FuelPrice_unit[j, i, h, 1];
                                }
                            }
                            else
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Fmin_Plant_unit[j, i, h] = (Unit_Data[j, i, 8] * Unit_Data[j, i, 3] * Unit_Data[j, i, 3] + Unit_Data[j, i, 9] * Unit_Data[j, i, 3] + Unit_Data[j, i, 10]) * FuelPrice_unit[j, i, h, 0];
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Fmin_Plant_unit[j, i, h] = FixedCost[j, i, 0] * FuelPrice_unit[j, i, h, 0];
                                }
                            }
                        }
                    }
                   }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[, ,] Fstart_Plant_unit = new double[Plants_Num, Units_Num, Start_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int t = 0; t < Start_Num; t++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            if (t < (Unit_Data[j, i, 17]))
                            {
                                Fstart_Plant_unit[j, i, t] = Unit_Data[j, i, 18];
                            }
                            else
                            {
                                Fstart_Plant_unit[j, i, t] = Unit_Data[j, i, 19];
                            }
                            if (MustRun[j, i, h] == 0)
                            {
                                Fstart_Plant_unit[j, i, t] = 0;
                            }
                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[, ,] Price_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k))
                            {
                                Price_Plant_unit[j, i, h] = PriceForecasting[j, k, h];
                            }
                        }
                    }
                }
            }

            int[, ,] VI_Plant_unit = new int[Plants_Num, Units_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        VI_Plant_unit[j, i, h] = 1;

                        strCmd = "select MaintenanceStartDate , MaintenanceEndDate,MaintenanceStartHour, MaintenanceEndHour" +
                            " from ConditionUnit" +
                            " where PPID= '" + plant[j] + "'" +
                            " and UnitCode = '" + Unit[j, i] + "'" +
                            " and Maintenance= 1" +
                            " and (MaintenanceStartDate ='" + biddingDate + "' or MaintenanceEndDate= '" + biddingDate + "')" +
                            " order by Date Desc";
                        oDataTable = utilities.returntbl(strCmd);

                        if (oDataTable.Rows.Count > 0)
                        {
                            DataRow row = oDataTable.Rows[0];
                            if (biddingDate == row["MaintenanceStartDate"].ToString().Trim())
                            {
                                if (row["MaintenanceStartHour"].ToString().Trim() == (h + 1).ToString())
                                    for (int g = h; g < Hour; g++)
                                        VI_Plant_unit[j, i, g] = 0;
                            }
                            else if (biddingDate == row["MaintenanceEndDate"].ToString().Trim())
                            {
                                if (row["MaintenanceEndHour"].ToString().Trim() == (h + 1).ToString())
                                    for (int g = 0; g <= h; g++)
                                        VI_Plant_unit[j, i, g] = 0;
                            }
                        }

                    }

                }
            }

            //*******************************************************************************************************

            try
            {
                Cplex cplex = new Cplex();


                // define variable :
                IIntVar[, ,] V_Plant_unit = new IIntVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            V_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                        }

                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                IIntVar[, ,] X_Plant_unit = new IIntVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if (Unit_Dec[j, i, 1].ToString().Trim() == "CC")
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                X_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                IIntVar[, ,] Y_Plant_unit = new IIntVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Y_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                        }

                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                IIntVar[,,] Z_Plant_unit = new IIntVar[Plants_Num, Units_Num,24];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < 24; h++)
                        {
                            Z_Plant_unit[j, i,h] = cplex.IntVar(0, 1);

                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddLe(V_Plant_unit[j, i, h], VI_Plant_unit[j, i, h]);

                        }
                    }
                }

                ////////////////////////////////////////////////////////////////////////////
                // define power(i,h):
                INumVar[, ,] Power_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Power_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }

                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Reserve(i,h):
                INumVar[, ,] Pres_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Pres_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define delta(i,h,m):
                INumVar[, , ,] delta_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour, Mmax];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int m = 0; m < Mmax; m++)
                            {
                                delta_Plant_unit[j, i, h, m] = cplex.NumVar(0.0, double.MaxValue);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Cost(i,h):
                INumVar[, ,] Cost_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Cost_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
               // define Benefit(i,h):
                INumVar[, ,] Income_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Income_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define CostStart(i,h):
                INumVar[, ,] Benefit_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Benefit_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[, ,] CostStart_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            CostStart_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }

                //**************************************************************************************************
                //  define Sum_Cost_day :
                ILinearNumExpr Sum_Cost_day = cplex.LinearNumExpr();

                double _Sum_Cost_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Cost_day.AddTerm(_Sum_Cost_day, Cost_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //  define Sum_CostStart_day :
                ILinearNumExpr Sum_CostStart_day = cplex.LinearNumExpr();

                double _Sum_CostStart_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_CostStart_day.AddTerm(_Sum_CostStart_day, Cost_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //  define Sum_Power_day :
                ILinearNumExpr Sum_Power_day = cplex.LinearNumExpr();
                double _Sum_Power_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Power_day.AddTerm(_Sum_Power_day, Power_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //  define Sum_Income_day :
                ILinearNumExpr Sum_Income_day = cplex.LinearNumExpr();

                double _Sum_Income_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                          Sum_Income_day.AddTerm(_Sum_Income_day, Income_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //  define Sum_Benefit_day :
                ILinearNumExpr Sum_Benefit_day = cplex.LinearNumExpr();

                double _Sum_Benefit_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Benefit_day.AddTerm(_Sum_Benefit_day, Benefit_Plant_unit[j, i, h]);
                        }
                    }
                }
                //*******************************************************************************************************
                //define Equation_(h):Income

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddEq(Income_Plant_unit[j, i, h], cplex.Prod(Price_Plant_unit[j, i, h], Power_Plant_unit[j, i, h]));
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //define Equation_(h):Outservice
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddEq(Z_Plant_unit[j, i,h], Outservice_Plant_unit[j, i, h]);
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //define Equation_(h):Maintenance
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddLe(cplex.Sum(V_Plant_unit[j, i, h], Z_Plant_unit[j, i,h]), 1);
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //define Equation_(h):Profit
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddEq(Benefit_Plant_unit[j, i, h], cplex.Diff(Income_Plant_unit[j, i, h], cplex.Sum(CostStart_Plant_unit[j, i, h], Cost_Plant_unit[j, i, h])));
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //define Equation_(h):Pmin,Pmax
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddGe(Power_Plant_unit[j, i, h], cplex.Prod(Unit_Data[j, i, 3], V_Plant_unit[j, i, h]));
                        }
                    }
                };

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddLe(Power_Plant_unit[j, i, h], cplex.Prod(Pmax_Plant_unit[j, i, h], V_Plant_unit[j, i, h]));
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):Delta
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int m = 0; m < Mmax; m++)
                            {
                                cplex.AddLe(delta_Plant_unit[j, i, h, m], Tbid_Plant_unit[j, i ,h]);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):P_delta
                double _Sum_Delta_m = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            ILinearNumExpr Sum_Delta_m = cplex.LinearNumExpr();
                            for (int m = 0; m < Mmax; m++)
                            {
                                Sum_Delta_m.AddTerm(_Sum_Delta_m, delta_Plant_unit[j, i, h, m]);
                            }
                            cplex.AddEq(Power_Plant_unit[j, i, h], cplex.Sum(cplex.Prod(Unit_Data[j, i, 3], V_Plant_unit[j, i, h]), Sum_Delta_m));
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):Cost
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            ILinearNumExpr DeltaBid_m = cplex.LinearNumExpr();
                            for (int m = 0; m < Mmax; m++)
                            {
                                DeltaBid_m.AddTerm(Fbid_Plant_unit[j, i, h , m], delta_Plant_unit[j, i, h, m]);
                            }
                            cplex.AddEq(Cost_Plant_unit[j, i, h], cplex.Sum(DeltaBid_m, cplex.Prod(Fmin_Plant_unit[j, i, h], V_Plant_unit[j, i, h])));
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^uncomment^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //define Equation_(h):Minupdown_1
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hisup_Plant_unit[j, i]; h++)
                        {
                            cplex.AddEq(V_Plant_unit[j, i, h], 1);
                        }
                    }
                };

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hisdown_Plant_unit[j, i]; h++)
                        {
                            cplex.AddEq(V_Plant_unit[j, i, h], 0);
                        }
                    }
                };

                ////^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //// define Equation_(h):Minupdown_2
                double _Minup_2 = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h <= (Hour - Unit_Data[j, i, 6]); h++)
                        {
                            ILinearNumExpr v_up = cplex.LinearNumExpr();
                            for (int hh = h; hh <= (h + Unit_Data[j, i, 6] - 1); hh++)
                            {
                                v_up.AddTerm(_Minup_2, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(v_up, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(V_Plant_unit[j, i, h], V_Plant_unit[j, i, h - 1])));
                            }
                            else
                            {
                                cplex.AddGe(v_up, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(V_Plant_unit[j, i, h], V_history_Plant_unit[j, i])));
                            }
                        }
                    }
                }

                double _Mindown_2 = -1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h <= (Hour - (int)Unit_Data[j, i, 7]); h++)
                        {
                            ILinearNumExpr v_down = cplex.LinearNumExpr();
                            for (int hh = h; hh <= (h + Unit_Data[j, i, 7] - 1); hh++)
                            {
                                v_down.AddTerm(_Mindown_2, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(v_down, cplex.Prod(Unit_Data[j, i, 7], cplex.Diff(cplex.Diff(V_Plant_unit[j, i, h - 1], V_Plant_unit[j, i, h]), 1)));
                            }
                            else
                            {
                                cplex.AddGe(v_down, cplex.Prod(Unit_Data[j, i, 7], cplex.Diff(cplex.Diff(V_history_Plant_unit[j, i], V_Plant_unit[j, i, h]), 1)));
                            }
                        }
                    }
                }

                ////^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //// define Equation_(h):Minupdown_3
                double _Minup_h = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = (Hour - (int)Unit_Data[j, i, 6] + 1); h < Hour; h++)
                        {
                            ILinearNumExpr vh_up = cplex.LinearNumExpr();
                            for (int hh = h; hh < Hour; hh++)
                            {
                                vh_up.AddTerm(_Minup_h, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(cplex.Diff(vh_up, cplex.Diff(cplex.Prod((Hour - h), V_Plant_unit[j, i, h]), cplex.Prod((Hour - h), V_Plant_unit[j, i, h - 1]))), 0);
                            }
                            else
                            {
                                cplex.AddGe(cplex.Diff(vh_up, cplex.Diff(cplex.Prod((Hour - h), V_Plant_unit[j, i, h]), ((Hour - h) * V_history_Plant_unit[j, i]))), 0);
                            }
                        }
                    }
                }

                double _Mindown_h = -1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = (Hour - (int)Unit_Data[j, i, 6] + 1); h < Hour; h++)
                        {
                            ILinearNumExpr vh_down = cplex.LinearNumExpr();
                            for (int hh = h; hh < Hour; hh++)
                            {
                                vh_down.AddTerm(_Mindown_h, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(cplex.Sum((Hour - h), cplex.Diff(vh_down, cplex.Diff(cplex.Prod((Hour - h), V_Plant_unit[j, i, h - 1]), cplex.Prod((Hour - h), V_Plant_unit[j, i, h])))), 0);
                            }
                            else
                            {
                                cplex.AddGe(cplex.Sum((Hour - h), cplex.Diff(vh_down, cplex.Diff(((Hour - h) * (1 - V_history_Plant_unit[j, i])), cplex.Prod((Hour - h), V_Plant_unit[j, i, h])))), 0);
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):Ramp Rate
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h < Hour; h++)
                        {
                            if (h < 23)
                            {
                                cplex.AddLe(Pres_Plant_unit[j, i, h], cplex.Sum(cplex.Prod(Pmax_Plant_unit[j, i, h] * 10, V_Plant_unit[j, i, h + 1]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h], V_Plant_unit[j, i, h + 1]))));
                            }

                            if ((h > 0))
                            {
                                cplex.AddLe(Power_Plant_unit[j, i, h], cplex.Sum(Power_Plant_unit[j, i, h - 1], cplex.Sum(cplex.Prod(Unit_Data[j, i, 1], V_Plant_unit[j, i, h]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h], V_Plant_unit[j, i, h - 1])), cplex.Prod(Pmax_Plant_unit[j, i, h] * 10, cplex.Diff(1, V_Plant_unit[j, i, h])))));
                                cplex.AddLe(cplex.Diff(Power_Plant_unit[j, i, h - 1], Power_Plant_unit[j, i, h]), cplex.Sum(cplex.Prod(Unit_Data[j, i, 2], V_Plant_unit[j, i, h]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h - 1], V_Plant_unit[j, i, h])), cplex.Diff(Pmax_Plant_unit[j, i, h] * 10, cplex.Diff(1, V_Plant_unit[j, i, h - 1]))));
                            }
                            if (h == 0)
                            {
                                cplex.AddLe(Power_Plant_unit[j, i, h], cplex.Sum(Powerhistory_Plant_unit[j, i], cplex.Sum(cplex.Prod(Unit_Data[j, i, 1], V_Plant_unit[j, i, h]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h], V_history_Plant_unit[j, i])), cplex.Prod(Pmax_Plant_unit[j, i, h] * 10, cplex.Diff(1, V_Plant_unit[j, i, h])))));
                                cplex.AddLe(cplex.Diff(Powerhistory_Plant_unit[j, i], Power_Plant_unit[j, i, h]), cplex.Sum(cplex.Prod(Unit_Data[j, i, 2], V_Plant_unit[j, i, h]), cplex.Sum(cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_history_Plant_unit[j, i], V_Plant_unit[j, i, h])), (Pmax_Plant_unit[j, i, h] * 10 * (1 - V_history_Plant_unit[j, i])))));
                            }
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):CC_power
                double _Unit_Gas_CC = 0.5;
                for (int h = 0; h < Hour; h++)
                {
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            if (Package[j, k].ToString().Trim() == "CC")
                            {
                                ILinearNumExpr Unit_Gas_CC = cplex.LinearNumExpr();
                                {
                                    for (int i = 0; i < Plant_units_Num[j]; i++)
                                    {
                                        if ((Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Data[j, i, 0] == k) & (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                                        {
                                            Unit_Gas_CC.AddTerm(_Unit_Gas_CC, Power_Plant_unit[j, i, h]);
                                        }
                                    }
                                }
                                for (int i = 0; i < Plant_units_Num[j]; i++)
                                {
                                    if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam") & (Unit_Data[j, i, 0] == k) & (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                                    {
                                        cplex.AddGe(Unit_Gas_CC, cplex.Diff(Power_Plant_unit[j, i, h], cplex.Prod(cplex.Diff(1, V_Plant_unit[j, i, h]), Pmax_Plant_unit[j, i, h])));
                                    }
                                }
                            }
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):CC_X
                double _X_1 = 1;
                double _X_2 = 1;

                int[] i_Start = new int[1];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                        {
                            i_Start[0] = GastostartSteam;
                        }
                        if (i_Start[0] == 0)
                        {
                            i_Start[0] = GastostartSteam;
                        }
                    }
                }

                double[] _X_Start = new double[1];
                double _Pmax_Steam = 0;
                _X_Start[0] = Math.Pow(i_Start[0], -1);
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        if (Package[j, k].ToString().Trim() == "CC")
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                for (int i = 0; i < Plant_units_Num[j]; i++)
                                {
                                    if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Data[j, i, 0] == (k)))
                                    {
                                        if (h < (i_Start[0] + 1))
                                        {
                                            ILinearNumExpr X_1 = cplex.LinearNumExpr();
                                            for (int hh = 0; hh <= h; hh++)
                                            {
                                                X_1.AddTerm(_X_1, V_Plant_unit[j, i, hh]);
                                            }
                                            cplex.AddLe(cplex.Diff(cplex.Prod(_X_Start[0], cplex.Sum(X_1, hisstart_Plant_unit[j, i])), 1), X_Plant_unit[j, i, h]);
                                            cplex.AddLe(X_Plant_unit[j, i, h], cplex.Prod(_X_Start[0], cplex.Sum(X_1, hisstart_Plant_unit[j, i])));

                                        }
                                        if (h > (i_Start[0]))
                                        {
                                            ILinearNumExpr X_2 = cplex.LinearNumExpr();
                                            for (int hh = h - i_Start[0]; hh <= h; hh++)
                                            {

                                                X_2.AddTerm(_X_2, V_Plant_unit[j, i, hh]);
                                            }

                                            cplex.AddLe(cplex.Diff(cplex.Prod(_X_Start[0], X_2), 1), X_Plant_unit[j, i, h]);
                                            cplex.AddLe(X_Plant_unit[j, i, h], cplex.Prod(_X_Start[0], X_2));
                                        }
                                    }
                                }

                                for (int ii = 0; ii < Plant_units_Num[j]; ii++)
                                {
                                    if ((Unit_Dec[j, ii, 1].ToString().Trim() == "CC") & (Unit_Dec[j, ii, 0].ToString().Trim() == "Steam") & (Unit_Data[j, ii, 0] == (k)))
                                    {
                                        _Pmax_Steam = Pmax_Plant_unit[j, ii, h];
                                        ILinearNumExpr X_CC = cplex.LinearNumExpr();
                                        for (int i = 0; i < Plant_units_Num[j]; i++)
                                        {
                                            if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Data[j, i, 0] == (k)))
                                            {
                                                X_CC.AddTerm(_Pmax_Steam * 0.5, X_Plant_unit[j, i, h]);
                                            }
                                        }
                                        cplex.AddGe(X_CC, Power_Plant_unit[j, ii, h]);
                                    }
                                }
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):Start up
                double _V_Start1 = 1;
                double _V_Start2 = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int t = 0; t <= h; t++)
                            {
                                if ((t < h) & (t <= Unit_Data[j, i, 16]))
                                {
                                    ILinearNumExpr V_Start1 = cplex.LinearNumExpr();
                                    for (int tt = 0; tt <= t; tt++)
                                    {
                                        V_Start1.AddTerm(_V_Start1, V_Plant_unit[j, i, (h - tt - 1)]);
                                    }
                                    cplex.AddGe(CostStart_Plant_unit[j, i, h], cplex.Prod(Fstart_Plant_unit[j, i, t], cplex.Diff(V_Plant_unit[j, i, h], V_Start1)));
                                }
                                if ((t == h) & (V_history_Plant_unit[j, i] == 0) & (t <= Unit_Data[j, i, 16]))
                                {
                                    ILinearNumExpr V_Start2 = cplex.LinearNumExpr();
                                    for (int tt = 0; tt < (t + 1); tt++)
                                    {
                                        V_Start2.AddTerm(_V_Start2, V_Plant_unit[j, i, (h - tt)]);
                                    }
                                    cplex.AddGe(CostStart_Plant_unit[j, i, h], cplex.Prod(Fstart_Plant_unit[j, i, (t + HisShut_Plant_unit[j, i])], cplex.Diff(V_Plant_unit[j, i, h], V_Start2)));
                                }
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):Inc_Dec_Ramp
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h < Hour; h++)
                        {
                            if (h > 0)
                            {
                                cplex.AddLe(Y_Plant_unit[j, i, h], cplex.Sum(1, cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Power_Plant_unit[j, i, h - 1]))));
                                cplex.AddGe(Y_Plant_unit[j, i, h], cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Power_Plant_unit[j, i, h - 1])));
                            }
                            if (h == 0)
                            {
                                cplex.AddLe(Y_Plant_unit[j, i, h], cplex.Sum(1, cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Powerhistory_Plant_unit[j, i]))));
                                cplex.AddGe(Y_Plant_unit[j, i, h], cplex.Prod(Math.Pow(1000, -1), cplex.Diff(Power_Plant_unit[j, i, h], Powerhistory_Plant_unit[j, i])));
                            }
                        }
                    }
                }
                ////^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //// define Equation_(h):Inc_Dec_Ramp-2
                double _Inc_Ramp_2 = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < (Hour - (int)Unit_Data[j, i, 6]); h++)
                        {
                            ILinearNumExpr Y_up = cplex.LinearNumExpr();
                            for (int hh = h + 1; hh <= (h + Unit_Data[j, i, 6]); hh++)
                            {
                                Y_up.AddTerm(_Inc_Ramp_2, Y_Plant_unit[j, i, hh]);
                            }
                            if (h < 23)
                            {
                                cplex.AddGe(Y_up, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(Y_Plant_unit[j, i, h + 1], Y_Plant_unit[j, i, h])));
                            }
                        }
                    }
                }

                ////^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //// define Equation_(h):Inc_Dec_Ramp_3
                double _Dec_Ramp_2 = -1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h < (Hour - (int)Unit_Data[j, i, 6]); h++)
                        {
                            ILinearNumExpr Y_down = cplex.LinearNumExpr();
                            for (int hh = (h + 1); hh <= (h + Unit_Data[j, i, 6]); hh++)
                            {
                                Y_down.AddTerm(_Dec_Ramp_2, Y_Plant_unit[j, i, hh]);
                            }
                            if (h < 23)
                            {
                                cplex.AddGe(Y_down, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(cplex.Diff(Y_Plant_unit[j, i, h], Y_Plant_unit[j, i, (h + 1)]), 1)));
                            }
                        }
                    }
                }
                ////^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                double _Inc_Ramp_3 = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = (Hour - (int)Unit_Data[j, i, 6]); h < Hour; h++)
                        {
                            ILinearNumExpr YY_up = cplex.LinearNumExpr();
                            for (int hh = (h + 1); hh < Hour; hh++)
                            {
                                YY_up.AddTerm(_Inc_Ramp_3, Y_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(cplex.Diff(YY_up, cplex.Diff(cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, h]), cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, h - 1]))), 0);
                            }
                        }
                    }
                }

                double _Dec_Ramp_3 = -1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = (Hour - (int)Unit_Data[j, i, 6]); h < Hour; h++)
                        {
                            ILinearNumExpr Yh_down = cplex.LinearNumExpr();
                            for (int hh = (h + 1); hh < Hour; hh++)
                            {
                                Yh_down.AddTerm(_Dec_Ramp_3, Y_Plant_unit[j, i, hh]);
                            }
                            if (h < 23)
                            {
                                cplex.AddGe(cplex.Sum((Hour - h - 1), cplex.Diff(Yh_down, cplex.Diff(cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, h]), cplex.Prod((Hour - h - 1), Y_Plant_unit[j, i, (h + 1)])))), 0);
                            }
                        }
                    }
                }
                //*****************************************uncomment * *********************************************************
                cplex.AddMaximize(Sum_Benefit_day);
                //*******************************************************************************************************
                //Export

                if (cplex.Solve())
                {
                    CplexPower = 1;
                    //MessageBox.Show(" Value =  Calculate : PBUC    " + cplex.ObjValue + "   " + cplex.GetStatus());
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                Power_Error[j, i, h] = 0;
                                Power_memory[j, i, h] = cplex.GetValue(Power_Plant_unit[j, i, h]);

                                if (Power_memory[j, i, h] < 2)
                                {
                                    Power_Error[j, i, h] = 1;
                                }
                            }
                        }
                    }

                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                for (int i = 0; i < Plant_units_Num[j]; i++)
                                {
                                    if (((int)Unit_Data[j, i, 0] == (k)))
                                    {
                                        if ((Power_Error[j, i, h] == 0) & (Package[j, k].ToString().Trim() != "CC"))
                                        {
                                            Power_if_Pack[j, k, h] = Unit_Data[j, i, 3];
                                            Power_memory_Pack[j, k, h] = Power_memory[j, i, h];
                                            Dispatch_Proposal[j, k, h] = Pmax_Plant_unit[j, i, h];
                                        }
                                        if ((Power_memory[j, i, h] < (Pmax_Plant_unit[j, i, h])) && (Package[j, k].ToString().Trim() == "CC") && (Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                                        {   //  Steam cc is Half :
                                            CO_steam[j, i, h] = 1;
                                            CO_Dispatch[j, i, h] = 1;
                                        }
                                        if ((Power_Error[j, i, h] == 0) && (Package[j, k].ToString().Trim() == "CC") && (Outservice_Plant_unit[j, i,h] == 0))
                                        {
                                            Power_memory_Pack[j, k, h] = Power_memory_Pack[j, k, h] + Power_memory[j, i, h];
                                            Power_if_Pack[j, k, h] = Power_if_Pack[j, k, h] + CO_steam[j, i, h] * Unit_Data[j, i, 3]/2;
                                            Dispatch_Proposal[j, k, h] = Dispatch_Proposal[j, k, h] + CO_Dispatch[j, i, h] * Pmax_Plant_unit[j, i, h] / 2;
                                        }
                                    }
                                }
                            }
                        }
                    }


                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                Power_Error_Pack[j, k, h] = 0;
                                if (Power_memory_Pack[j, k, h] < 3)
                                {
                                    Power_Error_Pack[j, k, h] = 1;
                                    Power_if_Pack[j, k, h] = 0;
                                    Power_memory_Pack[j, k, h] =  0;
                                }
                            }
                        }
                    }
                }
                cplex.End();
            }
            catch (ILOG.CPLEX.Cplex.UnknownObjectException e1)
            {
                MessageBox.Show("concert exception" + e1.ToString() + "caught");
            }
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------
            //Program-3 : Calculate Bid Power //
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------
            
            double[, , ,] Weight_Lost = new double[Plants_Num, Package_Num, Hour, Power_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int n = 0; n < Power_Num; n++)
                        {
                            Weight_Lost[j, k, h, n] = Math.Pow(Price_lost[j, k, h, X_select[j, k, h, n]] * pd_range[j, k, h, X_select[j, k, h, n]], -1);
                            if ( Price_lost[j, k, h, X_select[j, k, h, n]] * pd_range[j, k, h, X_select[j, k, h, n]] == 0)
                            {
                                //Weight_Lost[j, k, h, n] = 1;
                            }
                        }
                    }
                }
            }


            double[, , , ,] Weight_Lost_per = new double[Plants_Num, Package_Num, Hour, Power_Num, Power_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int w = 0; w < Power_Num; w++)
                        {
                            if (w == 0)
                            {
                                Weight_Lost_per[j, k, h, 1, w] = Weight_Lost[j, k, h, 1] / (Weight_Lost[j, k, h, 1] + Weight_Lost[j, k, h, 2]);
                                Weight_Lost_per[j, k, h, 2, w] = Weight_Lost[j, k, h, 2] / (Weight_Lost[j, k, h, 1] + Weight_Lost[j, k, h, 2]);
                            }
                            if (w == 1)
                            {
                                Weight_Lost_per[j, k, h, 1, w] = Weight_Lost[j, k, h, 1] / (Weight_Lost[j, k, h, 1] + Weight_Lost[j, k, h, 3]);
                                Weight_Lost_per[j, k, h, 3, w] = Weight_Lost[j, k, h, 3] / (Weight_Lost[j, k, h, 1] + Weight_Lost[j, k, h, 3]);
                            }
                            if (w == 2)
                            {
                                Weight_Lost_per[j, k, h, 2, w] = Weight_Lost[j, k, h, 2] / (Weight_Lost[j, k, h, 3] + Weight_Lost[j, k, h, 2]);
                                Weight_Lost_per[j, k, h, 3, w] = Weight_Lost[j, k, h, 3] / (Weight_Lost[j, k, h, 3] + Weight_Lost[j, k, h, 2]);
                            }
                            if (w == 3)
                            {
                                Weight_Lost_per[j, k, h, 1, w] = Weight_Lost[j, k, h, 1] / (Weight_Lost[j, k, h, 1] + Weight_Lost[j, k, h, 2] + Weight_Lost[j, k, h, 3]);
                                Weight_Lost_per[j, k, h, 2, w] = Weight_Lost[j, k, h, 2] / (Weight_Lost[j, k, h, 1] + Weight_Lost[j, k, h, 2] + Weight_Lost[j, k, h, 3]);
                                Weight_Lost_per[j, k, h, 3, w] = Weight_Lost[j, k, h, 3] / (Weight_Lost[j, k, h, 1] + Weight_Lost[j, k, h, 2] + Weight_Lost[j, k, h, 3]);
                            }
                        }
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[, , , ,] Power_bid_Plant_Pack = new double[Plants_Num, Package_Num, Hour, Power_Num, Power_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int w = 0; w < Power_Num; w++)
                        {
                            if (w == 0)
                            {
                                Power_bid_Plant_Pack[j, k, h, 0, w] = Power_if_Pack[j, k, h];
                                Power_bid_Plant_Pack[j, k, h, 1, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 1, w]);
                                Power_bid_Plant_Pack[j, k, h, 2, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 2, w]);
                                Power_bid_Plant_Pack[j, k, h, 3, w] = 0;

                            }
                            if (w == 1)
                            {
                                Power_bid_Plant_Pack[j, k, h, 0, w] = Power_if_Pack[j, k, h];
                                Power_bid_Plant_Pack[j, k, h, 1, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 1, w]);
                                Power_bid_Plant_Pack[j, k, h, 2, w] = 0;
                                Power_bid_Plant_Pack[j, k, h, 3, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 3, w]);
                            }
                            if (w == 2)
                            {
                                Power_bid_Plant_Pack[j, k, h, 0, w] = Power_if_Pack[j, k, h];
                                Power_bid_Plant_Pack[j, k, h, 1, w] = 0;
                                Power_bid_Plant_Pack[j, k, h, 2, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 2, w]);
                                Power_bid_Plant_Pack[j, k, h, 3, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 3, w]);
                            }
                            if (w == 3)
                            {
                                Power_bid_Plant_Pack[j, k, h, 0, w] = Power_if_Pack[j, k, h];
                                Power_bid_Plant_Pack[j, k, h, 1, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 1, w]);
                                Power_bid_Plant_Pack[j, k, h, 2, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 2, w]);
                                Power_bid_Plant_Pack[j, k, h, 3, w] = (Power_memory_Pack[j, k, h] - Power_if_Pack[j, k, h]) * (Weight_Lost_per[j, k, h, 3, w]);
                            }
                        }

                    }
                }
            }
            ;

            //*******************************************************************************************************
            //Export
            // MessageBox.Show(" Value =   Power:   ");
         
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int s = 0; s < Step_Num; s++)
                        {
                            if ((Run_State[j, k, h] == "U-O") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if ((s < 3) & (s > 0))
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h] + s * (Price_optimal[j, k, h] - Price_UL[j, k, h]) / 3;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 3)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                }
                                if (s > 3)
                                {

                                    Price[j, k, h, s] = Price_optimal[j, k, h] + (s - 3) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "U-F") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if ((s < 3) & (s > 0))
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h] + s * (PriceForecasting[j, k, h] - Price_UL[j, k, h]) / 3;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 3)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                }
                                if (s > 3)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 3) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "U-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if ((s < 3) & (s > 0))
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h] + s * (Price_Margin[j, k, h] - Price_UL[j, k, h]) / 3;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 3)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                }
                                if (s > 3)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 3) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "O-F") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if ((s < 3) & (s > 0))
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h] + s * (PriceForecasting[j, k, h] - Price_optimal[j, k, h]) / 3;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 3)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                }
                                if (s > 3)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 3) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "O-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if ((s < 3) & (s > 0))
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h] + s * (Price_Margin[j, k, h] - Price_optimal[j, k, h]) / 3;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 3)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                }
                                if (s > 3)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 3) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "F-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if ((s < 3) & (s > 0))
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] + s * (Price_Margin[j, k, h] - PriceForecasting[j, k, h]) / 3;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 3)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - 6 * 0.2;
                                }
                                if (s > 3)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 3) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_memory_Pack[j, k, h]) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "U-O-F") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if (s == 1)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h] + (Price_optimal[j, k, h] - Price_UL[j, k, h]) / 2;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 2)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + Power_bid_Plant_Pack[j, k, h, 1, 0]);
                                }
                                if ((s < 5) & (s > 2))
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h] + (s - 2) * (PriceForecasting[j, k, h] - Price_optimal[j, k, h]) / 3;
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + Power_bid_Plant_Pack[j, k, h, 1, 0]) + 0.2 * (s - 2);
                                }
                                if (s == 5)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 0]) + (Power_bid_Plant_Pack[j, k, h, 2, 0])) - 4 * 0.2;
                                }
                                if ((s < 10) & (s > 5))
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 5) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 0]) + (Power_bid_Plant_Pack[j, k, h, 2, 0])) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "U-O-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if (s == 1)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h] + (Price_optimal[j, k, h] - Price_UL[j, k, h]) / 2;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 2)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 1]));
                                }
                                if ((s < 5) & (s > 2))
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h] + (s - 2) * (Price_Margin[j, k, h] - Price_optimal[j, k, h]) / 3;
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 1])) + 0.2 * (s - 2);
                                }
                                if (s == 5)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 1]) + (Power_bid_Plant_Pack[j, k, h, 3, 1])) - 4 * 0.2;
                                }
                                if ((s < 10) & (s > 5))
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 5) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 1]) + (Power_bid_Plant_Pack[j, k, h, 3, 1])) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "U-F-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if (s == 1)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h] + (PriceForecasting[j, k, h] - Price_UL[j, k, h]) / 2;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 2)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2]));
                                }
                                if ((s < 5) & (s > 2))
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 2) * (Price_Margin[j, k, h] - PriceForecasting[j, k, h]) / 3;
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2])) + 0.2 * (s - 2);
                                }
                                if (s == 5)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2]) + (Power_bid_Plant_Pack[j, k, h, 3, 2])) - 4 * 0.2;
                                }
                                if ((s < 10) & (s > 5))
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 5) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2]) + (Power_bid_Plant_Pack[j, k, h, 3, 2])) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "O-F-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if (s == 1)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h] + (PriceForecasting[j, k, h] - Price_optimal[j, k, h]) / 2;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 2)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2]));
                                }
                                if ((s < 5) & (s > 2))
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] + (s - 2) * (Price_Margin[j, k, h] - PriceForecasting[j, k, h]) / 3;
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2])) + 0.2 * (s - 2);
                                }
                                if (s == 5)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2]) + (Power_bid_Plant_Pack[j, k, h, 3, 2])) - 4 * 0.2;
                                }
                                if ((s < 10) & (s > 5))
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 5) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 2, 2]) + (Power_bid_Plant_Pack[j, k, h, 3, 2])) - (9 - s) * 0.2;
                                }
                            }

                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            if ((Run_State[j, k, h] == "U-O-F-M") & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h];
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h];
                                }
                                if (s == 1)
                                {
                                    Price[j, k, h, s] = Price_UL[j, k, h] + (Price_optimal[j, k, h] - Price_UL[j, k, h]) / 2;
                                    Power[j, k, h, s] = Power_if_Pack[j, k, h] + 0.2 * s;
                                }
                                if (s == 2)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 3]));
                                }
                                if (s == 3)
                                {
                                    Price[j, k, h, s] = Price_optimal[j, k, h] + (PriceForecasting[j, k, h] - Price_optimal[j, k, h]) / 2;
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 3])) + 0.2;
                                }
                                if (s == 4)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 3]) + (Power_bid_Plant_Pack[j, k, h, 2, 3]));
                                }
                                if (s == 5)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h] + (Price_Margin[j, k, h] - PriceForecasting[j, k, h]) / 2;
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 3]) + (Power_bid_Plant_Pack[j, k, h, 2, 3]) + 0.2);
                                }
                                if (s == 6)
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 3]) + (Power_bid_Plant_Pack[j, k, h, 2, 3]) + (Power_bid_Plant_Pack[j, k, h, 3, 3])) - 3 * 0.2;
                                }
                                if ((s < 10) & (s > 6))
                                {
                                    Price[j, k, h, s] = Price_Margin[j, k, h] + (s - 6) * ErrorBid[0];
                                    Power[j, k, h, s] = (Power_if_Pack[j, k, h] + (Power_bid_Plant_Pack[j, k, h, 1, 3]) + (Power_bid_Plant_Pack[j, k, h, 2, 3]) + (Power_bid_Plant_Pack[j, k, h, 3, 3])) - (9 - s) * 0.2;
                                }
                            }
                            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                    
                            if ((Cap_Bid[j, k, h] == "yes") & (Power_memory_Pack[j, k, h] > 2.9))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = CapPrice[0] * 0.999;
                                    Power[j, k, h, s] = Dispatch_Proposal[j, k, h] - 0.2;
                                }
                                if (s == 1)
                                {
                                    Price[j, k, h, s] = CapPrice[0] * 0.9993;
                                    Power[j, k, h, s] = Dispatch_Proposal[j, k, h] - 0.1;
                                }
                                if (s == 2)
                                {
                                    Price[j, k, h, s] = CapPrice[0] * 0.9996;
                                    Power[j, k, h, s] = Dispatch_Proposal[j, k, h];
                                }
                                if (s > 2)
                                {
                                    Price[j, k, h, s] = 0;
                                    Power[j, k, h, s] = 0;
                                }
                               
                            }

                            /////////////////////////////////////////////

                            if ((MustMaxop[j, k] == 1) & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = PriceForecasting[j, k, h];
                                    Power[j, k, h, s] = Dispatch_Proposal[j, k, h];
                                }
                                if (s > 0)
                                {
                                    Price[j, k, h, s] = 0;
                                    Power[j, k, h, s] = 0;
                                }
                            }

                            ////////////////////////////////////////////////
                                                        
                        }
                    }
                }
            }


      


            //Khanum Khorsand : lotfan bejaie .... sharte check box max (opertional Data = Current date "Nearest")=True ra begzarid.
            //if ((......) & (Power_memory_Pack[j, k, h] > (Power_if_Pack[j, k, h]) + 2))
            //{
            //    if (s == 0)
            //    {
            //        Price[j, k, h, s] = PriceForecasting[j, k, h];
            //        Power[j, k, h, s] = Dispatch_Proposal[j, k, h];
            //    }
            //    if (s > 0)
            //    {
            //        Price[j, k, h, s] = 0;
            //        Power[j, k, h, s] = 0;
            //    }
            //}
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            // Error cplex is delete : 
            int test_Zero = 0;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        test_Zero = 0;

                        for (int ss = 0; ss < Step_Num; ss++)
                        {
                            if (Power[j, k, h, ss] > 2.9)
                            {
                                test_Zero = 1;
                            }
                        }
                        if (test_Zero == 0)
                        {
                            for (int ss = 0; ss < Step_Num; ss++)
                            {
                                Price[j, k, h, ss] = 0;
                                Power[j, k, h, ss] = 0;
                            }
                        }
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Dispatch_Complete_Pack[j, k, h] == 1) & ((Package[j, k].ToString().Trim() != "CC")))
                        {
                            Dispatch_Proposal[j, k, h] = Pmax_Plant_Pack[j, k, h];
                        }
                        if ((Dispatch_Complete_Pack[j, k, h] == 3) & ((Package[j, k].ToString().Trim() == "CC")))
                        {
                            Dispatch_Proposal[j, k, h] = Pmax_Plant_Pack[j, k, h];
                        }
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int s = 1; s < Step_Num; s++)
                        {
                            if ((Power[j, k, h, s] == 0) & (Power[j, k, h, s - 1] > 0) & (Power[j, k, h, s - 1] < Dispatch_Proposal[j, k, h]))
                            {
                                Power[j, k, h, s] = Dispatch_Proposal[j, k, h];
                            }
                        }
                        if ((Power_Error_Pack[j, k, h] == 1) & ((service_Max[j, k, h] == Pmax_Plant_Pack[j, k, h]) | (Outservice_Plant_Pack[j, k, h] == "Yes")))
                        {
                            Price[j, k, h, 0] = 0;
                            Power[j, k, h, 0] = 0;

                            for (int s = 1; s < Step_Num; s++)
                            {
                                Price[j, k, h, s] = 0;
                                Power[j, k, h, s] = 0;
                            }
                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            int test_cap = 0;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        test_cap = 0;

                        if (Outservice_Plant_Pack[j, k, h] == "Yes")
                        {
                            for (int ss = 0; ss < Step_Num; ss++)
                            {
                                Price[j, k, h, ss] = 0;
                                Power[j, k, h, ss] = 0;
                            }
                            Dispatch_Proposal[j, k, h] = 0;
                        }
                        if ((Dispatch_Proposal[j, k, h] > 0) & ((Power[j, k, h, 0] == 0) | (Price[j, k, h, 0] == 0)) & (PriceForecasting[j, k, h] < Cost_Per_Plant_Pack[j, k, h]) & (CapPrice[0] > Cost_Per_Plant_Pack[j, k, h]) & (PriceForecasting[j, k, h] >= Cost_Per_Plant_Pack[j, k, h]))
                        {
                            Price[j, k, h, 0] = (Cost_Per_Plant_Pack[j, k, h]);
                            Power[j, k, h, 0] = Pmax_Plant_Pack[j, k, h];
                            for (int ss = 1; ss < Step_Num; ss++)
                            {
                                Price[j, k, h, ss] = 0;
                                Power[j, k, h, ss] = 0;
                            }
                            Dispatch_Proposal[j, k, h] = Pmax_Plant_Pack[j, k, h];
                        }
                        if ((Dispatch_Proposal[j, k, h] > 0) & ((Power[j, k, h, 0] == 0) | (Price[j, k, h, 0] == 0)))
                        {
                            Price[j, k, h, 0] = (CapPrice[0]);
                            Power[j, k, h, 0] = Pmax_Plant_Pack[j, k, h];
                            for (int ss = 1; ss < Step_Num; ss++)
                            {
                                Price[j, k, h, ss] = 0;
                                Power[j, k, h, ss] = 0;
                            }
                            Dispatch_Proposal[j, k, h] = Pmax_Plant_Pack[j, k, h];
                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        test_cap = 0;
                        for (int s = 0; s < Step_Num; s++)
                        {
                            if ((Price[j, k, h, s] > CapPrice[0]) & (test_cap == 0))
                            {
                                Price[j, k, h, s] = (CapPrice[0]);
                                Power[j, k, h, s] = Dispatch_Proposal[j, k, h];
                                test_cap = 1;
                                if (s < 9)
                                {
                                    for (int ss = (s + 1); ss < Step_Num; ss++)
                                    {
                                        Price[j, k, h, ss] = 0;
                                        Power[j, k, h, ss] = 0;

                                    }
                                }
                            }
                        }
                    }
                }
            }
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int s = 1; s < Step_Num; s++)
                        {
                            if ((Price[j, k, h, s - 1] > Price[j, k, h, s - 1]) | (Power[j, k, h, s - 1] > Power[j, k, h, s - 1]))
                            {
                                System.Windows.Forms.MessageBox.Show("   Error_1 : Answer is Fail.   " + "  Plant  " + j.ToString() + "  Unit  " + k.ToString() + "  Hour  " + h.ToString() + "  Step  " + s.ToString());
                            }
                        }
                    }
                }
            }


            //////////////////////////////////////////NEW////////////////////////////////////////////
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {

                        if (forcatingAvailable[j] == false ||
                            (Price_Margin[j, k, h] == 0 && PriceForecasting[j, k, h] == 0 && Price_optimal[j, k, h] == 0 && Price_UL[j, k, h] == 0)
                            )
                        {
                            System.Windows.Forms.MessageBox.Show("Import Data is Invalid!!! \r\nPlease run PriceForecasting Again... ");
                            return false;
                        }
                        //if (Price_Margin[j, k, h] <= PriceForecasting[j, k, h] && PriceForecasting[j, k, h] <= Price_optimal[j, k, h] && Price_optimal[j, k, h] <= Price_UL[j, k, h])
                        //{
                        //    System.Windows.Forms.MessageBox.Show("The Process Has Been Canceled");
                        //    return;
                        //}
                    }
                }
            }
            //////////////////////////////////////////NEW//////////////////////////////////////////
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {

                        if (Price[j, k, h, 0] == 0 && Power[j, k, h, 0] == 0)
                        {

                            Dispatch_Proposal[j, k, h] = 0;

                        }

                    }

                }
            }

            //////////////////////////////////////////NEW//////////////////////////////////////////
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {

                        if (Price[j, k, h, 0] == CapPrice[0] )
                        {
                            for (int s = 0; s < Step_Num; s++)
                            {
                                if (s == 0)
                                {
                                    Price[j, k, h, s] = CapPrice[0] - ErrorBid[0] / 4;
                                    Power[j, k, h, s] = Pmin_Plant_Pack[j, k, h];
                                }
                                if (s == 1)
                                {
                                    Price[j, k, h, s] = CapPrice[0] - ErrorBid[0] / 8;
                                    Power[j, k, h, s] = (Pmin_Plant_Pack[j, k, h] + Dispatch_Proposal[j, k, h]) / 2;
                                }
                                if (s == 2)
                                {
                                    Price[j, k, h, s] = CapPrice[0];
                                    Power[j, k, h, s] = Dispatch_Proposal[j, k, h];
                                }
                                if (s > 2)
                                {
                                    Price[j, k, h, s] = 0;
                                    Power[j, k, h, s] = 0;
                                }
                            }

                        }

                    }

                }
            }

            /////////////////////////////////////////////////////////////////////////////////////
            if (CplexPrice == 0)
            {
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int s = 0; s < Step_Num; s++)
                            {
                                Price[j, k, h, s] = 0;
                                Power[j, k, h, s] = 0;
                            }
                        }
                    }
                }
                MessageBox.Show("Input Information is not Complete");
                return false;
            }
            if (CplexPower == 0)
            {
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int s = 0; s < Step_Num; s++)
                            {
                                Price[j, k, h, s] = 0;
                                Power[j, k, h, s] = 0;
                            }
                        }
                    }
                }
                MessageBox.Show("Operational Information is not Correct");
                return false;
            }


            int pptype = 0;
            // string block = "";


            string[,] blocks002 = Getblock002();

            for (int j = 0; j < Plants_Num; j++)
            {
                strCmd = "delete from DetailFRM002 where TargetMarketDate ='" + biddingDate + "'" +
             " and ppid='" + plant[j].Trim() + "'and Estimated=1";
                oDataTable = utilities.returntbl(strCmd);


                /////////////////////))))))))))))))))))))))

                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    pptype = 0;
                    if (Package[j, k] == PackageTypePriority.CC.ToString() && PackageTypes[j].Count>1)
                        pptype = 1;                  

                    for (int h = 0; h < Hour; h++)
                    {

                        string firstCommand = "insert into DetailFRM002 (Estimated,TargetMarketDate, PPID,PPType,Block, Hour,DeclaredCapacity,DispachableCapacity ";

                        string secondCommand = "values (1,'" + biddingDate + "','" + plant[j] + "','" +
                            pptype.ToString() + "','" + blocks002[j, k] + "'," +
                            (h + 1).ToString() + "," +
                            Dispatch_Proposal[j, k, h].ToString() + ","
                            + Dispatch_Proposal[j, k, h].ToString().Trim();
                        for (int s = 0; s < Step_Num; s++)
                        {
                            firstCommand += ",Power" + (s + 1).ToString().Trim();
                            secondCommand += "," + Power[j, k, h, s].ToString().Trim();

                            firstCommand += ",Price" + (s + 1).ToString().Trim();
                            secondCommand += "," + Price[j, k, h, s].ToString().Trim();


                        }
                        firstCommand += " ) ";
                        secondCommand += " ) ";
                        //for (int s = 0; s < Step_Num; s++)
                        //{
                        //oDataTable = utilities.returntbl("insert into Table_1 values('" + plant[j].ToString() + "','" + k.ToString() + "','" + h.ToString() + "','" + Pmax_Plant_Pack[j, k, h].ToString() + "','" + Power[j, k, h, s].ToString() + "','" + Price[j, k, h, s].ToString() + "','" + s.ToString() + "' )");
                        oDataTable = utilities.returntbl(firstCommand + secondCommand);
                        //}
                    }
                }
            }

          
            ///////////////////////////////////////end///////////////////////////////////////////
            return true;
            ////////////////////////////////////////////////////////////////////////////////////
            //l1: return false;
            ////////////////////////////////////////
        }



        private double[] GetForcast(string ppId, PackageTypePriority packageType, string date)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText = "select FinalForecastHourly.hour, FinalForecastHourly.forecast" +
                    " from FinalForecastHourly inner join FinalForecast" +
                    " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id" +
                    " where FinalForecast.PPId=@ppID AND FinalForecast.PackageType=@packageType AND FinalForecast.date=@date" +
                    " order by FinalForecastHourly.hour";

            MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
            MyCom.Parameters["@ppID"].Value = ppId.ToString().Trim();
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = date;
            MyCom.Parameters.Add("@packageType", SqlDbType.Char, 20);
            MyCom.Parameters["@packageType"].Value = packageType.ToString().Trim();
            Maxda.Fill(MaxDS);

            double[] forecasts = new double[24];
            if (MaxDS.Tables[0].Rows.Count > 0)
                for (int j = 0; j < 24; j++)
                    forecasts[j] = Double.Parse(MaxDS.Tables[0].Rows[j]["forecast"].ToString());
            return forecasts;
        }

        //private double[] GetForcast(string date)
        //{
        //    double[] forecasts = new double[9,24];

        //    SqlConnection MyConnection = new SqlConnection();
        //    MyConnection.ConnectionString = ConStr;
        //    MyConnection.Open();

        //    SqlCommand MyCom = new SqlCommand();
        //    MyCom.Connection = MyConnection;
        //    DataSet MaxDS = new DataSet();
        //    SqlDataAdapter Maxda = new SqlDataAdapter();
        //    Maxda.SelectCommand = MyCom;

        //    // Insert into FinalForcast table
        //    MyCom.CommandText = "select FinalForecastHourly.forecast" +
        //            " from FinalForecastHourly inner join FinalForecast" +
        //            " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id" +
        //            " where FinalForecast.PPId=@ppID AND FinalForecast.MixUnitStatus=@mixUnitStatus AND FinalForecast.date=@date" +
        //            " order by FinalForecastHourly.hour";

        //    MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
        //    MyCom.Parameters["@ppID"].Value = ppId.ToString().Trim();
        //    MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
        //    MyCom.Parameters["@date"].Value = date;
        //    MyCom.Parameters.Add("@mixUnitStatus", SqlDbType.Char, 20);
        //    MyCom.Parameters["@mixUnitStatus"].Value = mixUnitStatus.ToString().Trim();
        //    Maxda.Fill(MaxDS);

        //    if (MaxDS.Tables[0].Rows.Count > 0)
        //        for (int j = 0; j < 24; j++)
        //            forecasts[j] = Double.Parse(MaxDS.Tables[0].Rows[j][0].ToString());
        //    return forecasts;
        //}

        private double[] GetVariance(string ppId, PackageTypePriority packageType, string date)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText = "select FinalForecastHourly.hour, FinalForecastHourly.vr" +
                    " from FinalForecastHourly inner join FinalForecast" +
                    " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id" +
                    " where FinalForecast.PPId=@ppID AND FinalForecast.PackageType=@packageType AND FinalForecast.date=@date" +
                    " order by FinalForecastHourly.hour";

            MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
            MyCom.Parameters["@ppID"].Value = ppId.ToString().Trim();
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = date;
            MyCom.Parameters.Add("@packageType", SqlDbType.Char, 20);
            MyCom.Parameters["@packageType"].Value = packageType.ToString().Trim();
            Maxda.Fill(MaxDS);

            double[] variance = new double[24];
            if (MaxDS.Tables[0].Rows.Count > 0)
                for (int j = 0; j < 24; j++)
                    variance[j] = Double.Parse(MaxDS.Tables[0].Rows[j]["vr"].ToString());
            return variance;
        }

        private double[,] Get_pd_Range(string ppId, PackageTypePriority packageType, string date)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            double[,] pd_Range = new double[24, 201];

            for (int j = 0; j < 24; j++)
            {
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;

                // Insert into FinalForcast table
                MyCom.CommandText =
                    "select FinalForecast201Item.[index], FinalForecast201Item.ff" +
                    " from FinalForecast201Item inner join FinalForecastHourly " +
                    "on FinalForecastHourly.id = FinalForecast201Item.fk_ffHourly " +
                    "inner join FinalForecast " +
                    "on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id " +
                    "where FinalForecast.PPId=@PPId AND FinalForecast.PackageType=@packageType " +
                    "AND FinalForecast.date=@date AND FinalForecastHourly.hour=@hour " +
                    "order by FinalForecast201Item.[index]";


                MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                MyCom.Parameters["@ppID"].Value = ppId.ToString().Trim();
                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = date;
                MyCom.Parameters.Add("@packageType", SqlDbType.Char, 20);
                MyCom.Parameters["@packageType"].Value = packageType.ToString().Trim();
                MyCom.Parameters.Add("@hour", SqlDbType.Int);
                MyCom.Parameters["@hour"].Value = j + 1;
                Maxda.Fill(MaxDS);

                if (MaxDS.Tables[0].Rows.Count > 0)
                    for (int k = 0; k < 201; k++)
                        pd_Range[j, k] = Double.Parse(MaxDS.Tables[0].Rows[k]["ff"].ToString());
            }
            return pd_Range;
        }

        private double[,] Get_pdist(string ppId, PackageTypePriority packageType, string date)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            double[,] pdist = new double[24, 201];

            for (int j = 0; j < 24; j++)
            {
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;

                // Insert into FinalForcast table
                MyCom.CommandText =
                    "select FinalForecast201Item.pdist" +
                    " from FinalForecast201Item inner join FinalForecastHourly " +
                    "on FinalForecastHourly.id = FinalForecast201Item.fk_ffHourly " +
                    "inner join FinalForecast " +
                    "on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id " +
                    "where FinalForecast.PPId=@PPId AND FinalForecast.PackageType=@packageType " +
                    "AND FinalForecast.date=@date AND FinalForecastHourly.hour=@hour " +
                    "order by FinalForecast201Item.[index]";


                MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                MyCom.Parameters["@ppID"].Value = ppId.ToString().Trim();
                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = date;
                MyCom.Parameters.Add("@packageType", SqlDbType.Char, 20);
                MyCom.Parameters["@packageType"].Value = packageType.ToString().Trim();
                MyCom.Parameters.Add("@hour", SqlDbType.Int);
                MyCom.Parameters["@hour"].Value = j + 1;
                Maxda.Fill(MaxDS);

                if (MaxDS.Tables[0].Rows.Count > 0)
                    for (int k = 0; k < 201; k++)
                        pdist[j, k] = Double.Parse(MaxDS.Tables[0].Rows[k][0].ToString());
            }
             return pdist;
        }

        private double[] Get_HourCap(string ppId, string date)
        {
            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            double[] hourCap = new double[24];

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText =
                "select BiddingStrategyCustomized.hour, BiddingStrategyCustomized.Peak " +
            "from BiddingStrategyCustomized inner join BiddingStrategySetting " +
            "on BiddingStrategyCustomized.FkBiddingStrategySettingId = BiddingStrategySetting.id " +
            "where BiddingStrategySetting.BiddingDate= @date AND BiddingStrategySetting.Plant=@ppID " +
            "order by BiddingStrategyCustomized.hour";

            MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
            MyCom.Parameters["@ppID"].Value = ppId.ToString().Trim();
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = date;

            Maxda.Fill(MaxDS);

            if (MaxDS.Tables[0].Rows.Count > 0)
                for (int k = 0; k < 24; k++)
                    hourCap[k] = Double.Parse(MaxDS.Tables[0].Rows[k]["Peak"].ToString());

            return hourCap;
        }

        private string GetDaysBefore(string strDate, int days)
        {
            DateTime date = PersianDateConverter.ToGregorianDateTime(strDate);
            date = date.Subtract(new TimeSpan(days, 0, 0, 0));
            return new PersianDate(date).ToString("d");
        }

        private double[] GetNearestLoadForecastingDateVlues(string biddingDate)
        {
            double[] result = new double[24];

            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            // double[] hourCap = new double[24];

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText =
                "select Date,DateEstimate from LoadForecasting where Date<='" + biddingDate.Trim() + "'";
            Maxda.Fill(MaxDS);


            DateTime dtDate = PersianDateConverter.ToGregorianDateTime(biddingDate);
            DataRow rowNearest = null;
            TimeSpan minSpan = new TimeSpan();
            //if (MaxDS.Tables[0].Rows.Count>0)
            //    minSpan = 
            foreach (DataRow row in MaxDS.Tables[0].Rows)
            {
                DateTime tempDate = PersianDateConverter.ToGregorianDateTime
                    (row["Date"].ToString().Trim());
                TimeSpan tempSpan = dtDate - tempDate;
                if (rowNearest == null || minSpan > tempSpan)
                {
                    minSpan = tempSpan;
                    rowNearest = row;
                }
            }

            if (rowNearest != null)
            {
                string date2 = rowNearest["Date"].ToString().Trim();

                //////////////////////////////////////////
                // in case there are more than one date, find the one with the maxmimum DateEstimate
                MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                MaxDS = new DataSet();
                Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;

                // Insert into FinalForcast table

                MyCom.CommandText =
                    "select LoadForecasting.* from LoadForecasting where Date='" + date2.ToString().Trim() + "'";
                Maxda.Fill(MaxDS);


                DataRow maxDateRow = null;
                string maxDate = null;

                foreach (DataRow row in MaxDS.Tables[0].Rows)
                {
                    string temp = row["DateEstimate"].ToString().Trim();
                    if (maxDate == null || String.Compare(maxDate, temp) < 0)
                    {
                        maxDate = temp;
                        maxDateRow = row;
                    }
                }

                for (int h = 0; h < 24; h++)
                {
                    string colName = "Hour" + (h + 1).ToString().Trim();
                    result[h] = double.Parse(maxDateRow[colName].ToString());
                }
            }

            return result;
        }

        private double[, ,] InsertToPriceForecasting(DataTable dt, double[, ,] PriceForecastings, int plantIndex, int row)
        {
            for (int hour = 0; hour < dt.Rows.Count; hour++)
                PriceForecastings[plantIndex, hour, row] = double.Parse(dt.Rows[hour]["forecast"].ToString().Trim());
            return PriceForecastings;
        }

        private string[,] Getblock()
        {

            string[,] blocks = new string[Plants_Num, Package_Num];
            string temp = "";

            for (int j = 0; j < Plants_Num; j++)
            {
                //  int x=int.Parse(plant[j]);
                // oDataTable = utilities.returntbl("select  PackageType,UnitCode,PackageCode from dbo.UnitsDataMain where PPID='" + plant[j] + "'order by UnitCode");

                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {



                    oDataTable = utilities.returntbl("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + (k + 1).ToString() + "'");

                    int ppid = int.Parse(plant[j]);

                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {
                        Initial = "C" + (k + 1).ToString();

                        if (Findccplant(plant[j]))
                        {
                            ppid++;
                        }
                    }

                    else if (packageType == "Gas")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
    plant[j] + "'" + " and packageCode='" + (k + 1).ToString() + "' and packageType ='Gas'");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
plant[j] + "'" + " and packageCode='" + (k + 1).ToString() + "' and packageType ='Steam'");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                    }


                    string blockName = ppid.ToString() + "-" + Initial;

                    blocks[j, k] = blockName;



                }
            }
            return blocks;

        }
       
        ////********************************************** Get 7 day
        DateTime b;
        DateTime c;
       
        private string[,] Getblock002()
        {
            string[,] blocks1 = new string[Plants_Num, Package_Num];
            string temp = "";

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {

                    oDataTable = utilities.returntbl("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + (k + 1).ToString() + "'");

                    int ppid = int.Parse(plant[j]);

                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {
                        Initial = "C" + (k + 1).ToString();
                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + (k + 1).ToString() + "' and packageType ='Gas'");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + (k + 1).ToString() + "' and packageType ='Steam'");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                    }

                    string blockName1 = Initial;

                    blocks1[j, k] = blockName1;

                }
            }
            return blocks1;



        }



        //public bool Findccplant(string ppid)
        //{
        //    bool ss = false;
        //    oDataTable = utilities.returntbl("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
        //    for (int i = 0; i < oDataTable.Rows.Count; i++)
        //    {
        //        if (((oDataTable.Rows[0][0].ToString().Substring(0, 1) == "C") && (oDataTable.Rows[i][0].ToString().Substring(0, 1) == "S")) || ((oDataTable.Rows[0][0].ToString().Substring(0, 1) == "S") && (oDataTable.Rows[i][0].ToString().Substring(0, 1) == "C")))

        //            ss = true;


        //    }
        //    return ss;
        //}

        public bool Findccplant(string ppid)
        {
            int tr = 0;

            oDataTable = utilities.returntbl("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count > 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }
        private bool CheckDateSF(string date, string start, string end)
        {
            bool result = false;
            string temp1 = date.Remove(4);
            start = start.Trim();
            end = end.Trim();
            if ((start != "") && (end != ""))
            {
                string temp2 = start.Remove(4);
                if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
                else
                {
                    temp1 = date.Remove(7);
                    temp1 = temp1.Remove(0, temp1.Length - 2);
                    temp2 = start.Remove(7);
                    temp2 = temp2.Remove(0, temp2.Length - 2);
                    if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                    else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
                    else
                    {
                        temp1 = date.Remove(0, date.Length - 2);
                        temp2 = start.Remove(0, start.Length - 2);
                        if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                        else result = false;
                    }
                }
                if (!result) return (result);
                else
                {
                    temp1 = date.Remove(4);
                    temp2 = end.Remove(4);
                    if (int.Parse(temp1) < int.Parse(temp2)) return (true);
                    else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
                    else
                    {
                        temp1 = date.Remove(7);
                        temp1 = temp1.Remove(0, temp1.Length - 2);
                        temp2 = end.Remove(7);
                        temp2 = temp2.Remove(0, temp2.Length - 2);
                        if (int.Parse(temp1) < int.Parse(temp2)) return (true);
                        else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
                        else
                        {
                            temp1 = date.Remove(0, date.Length - 2);
                            temp2 = end.Remove(0, end.Length - 2);
                            if (int.Parse(temp1) < int.Parse(temp2))// || (temp1 == temp2))
                                return (true);
                            else return (false);
                        }
                    }
                }
            }
            return false;
        }
        //private double  checkrival()
        //    {

        //        DataTable regtableH = null;

        //        DataTable fdat = utilities.returntbl("SELECT PPID,PackageType FROM dbo.PPUnit ");
        //        string exp = "";
        //        int count = 0;
        //        for (int i = 0; i < fdat.Rows.Count; i++)
        //        {
        //            if (i != (fdat.Rows.Count - 1))
        //            {

        //                if ((fdat.Rows[i][0].ToString().Trim() == fdat.Rows[i + 1][0].ToString().Trim()) && (fdat.Rows[i][1].ToString().Trim() == "Combined Cycle" || fdat.Rows[i + 1][1].ToString().Trim() == "Combined Cycle"))
        //                {
        //                    count++;
        //                    exp += (int.Parse(fdat.Rows[i + 1][0].ToString()) + 1).ToString() + ":";

        //                }
        //            }
        //        }
        //        string[] exepcode = exp.Split(':');
        //        int lenght = exepcode.Length;
               
        //        regtableH = utilities.returntbl("Select distinct PPCode from dbo.RegionNetComp where  Date='" + FindNearRegionDate(biddingDate) + "'AND Code='R01' AND PPCode NOT IN (SELECT PPID FROM PowerPlant)");
        //            string[] revcodeh = new string[regtableH.Rows.Count];
                  
        //            for (int i = 0; i < regtableH.Rows.Count; i++)
        //            {
        //                revcodeh[i] = regtableH.Rows[i][0].ToString().Trim();
        //            }

                            
        //            ///////////////////////////delete  132, 145////////////////////
        //            for (int i = 0; i < regtableH.Rows.Count; i++)
        //            {
        //                for (int j = 0; j < lenght; j++)
        //                {
        //                    if (revcodeh[i].ToString().Trim() == exepcode[j])
        //                    {
        //                        revcodeh[i] = "0";
                             
        //                    }
        //                }

        //            }

        //            /////////////////////////////////////////////////////////////
                    
        //            ArrayList revh=new ArrayList();
        //            for (int i = 0; i < regtableH.Rows.Count; i++)
        //            {
        //                if (revcodeh[i] != "0")
        //                {
        //                   revh.Add(revcodeh[i]);

        //                }              
        //            }


        //        double [] maxhour=new double[revh.Count];
               
        //            for (int i = 0; i < revh.Count; i++)
        //            {
        //                double maxp = 0;
        //                DataTable peaktable = utilities.returntbl("select * from dbo.ProducedEnergy where PPCode='" + revh[i] + "' and Date='" + FindNearProduceDate(biddingDate) + "'");
        //                for (int d = 0; d <24; d++)
        //                {
        //                    if (maxp <= double.Parse(peaktable.Rows[0]["Hour" + (d + 1)].ToString()))
        //                        maxp = double.Parse(peaktable.Rows[0]["Hour" + (d + 1)].ToString());

        //                }

        //                maxhour[i] = maxp;


        //            }

        //        /////////////sum of max hour//////////////////////
        //            double sum = 0;
        //            for (int i = 0; i < revh.Count; i++)
        //            {
        //                sum += maxhour[i];

        //            }

        //            return sum;

        //    }
        private string FindNearRegionDate(string date)
        {
            DataTable odat = utilities.returntbl("Select distinct Date from dbo.RegionNetComp where Date<='" + date + "'AND Code='R01' order by Date desc");
            return odat.Rows[0][0].ToString().Trim();

        }
        private string FindNearProduceDate(string date)
        {
            DataTable odat = utilities.returntbl("Select distinct Date from dbo.ProducedEnergy where Date<='" + date + "'order by Date desc");
            return odat.Rows[0][0].ToString().Trim();

        }

        private string FindCommon002and005(DateTime selectedDate,string plant)
        {
            string common = "";
          
                ///////////////check 002 and 005 not empty////////////////////////////

                DataTable omoo5 = utilities.returntbl("select * from dbo.DetailFRM005 where TargetMarketDate='" + new PersianDate(selectedDate).ToString("d") + "'and PPID='" + plant + "'");
                DataTable omoo2 = utilities.returntbl("select * from dbo.DetailFRM002 where TargetMarketDate='" + new PersianDate(selectedDate).ToString("d") + "'and PPID='" + plant + "'");

                if (omoo2.Rows.Count == 0 || omoo5.Rows.Count == 0)
                {
                    PersianDate test1 = new PersianDate(currentDate);
                    DateTime Now = PersianDateConverter.ToGregorianDateTime(test1);

                    DateTime ENDDate = Now.Subtract(new TimeSpan(6, 0, 0, 0));
                    for (int daysbefore = 0; daysbefore < 7; daysbefore++)
                    {

                        DateTime SAMPLEDate = Now.Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                        if (SAMPLEDate != ENDDate)
                        {

                            string date5 = "";
                            string date2 = "";

                            if (omoo2.Rows.Count == 0)
                            {
                                DataTable nearomoo2 = utilities.returntbl("select distinct TargetMarketDate from dbo.DetailFRM002 where TargetMarketDate<='" + new PersianDate(SAMPLEDate).ToString("d") + "'and PPID='" + plant + "'order by TargetMarketDate desc");

                                date2 = nearomoo2.Rows[0][0].ToString();
                                bool inweek = finddatein7daym002(date2, plant);
                                if (inweek == false)
                                {
                                    DialogResult result = MessageBox.Show("M002 AND M005 Files In Previous week Is Not Existed!. \r\n", "Bid Allocation End Unsuccesssfully!", MessageBoxButtons.OK);
                                    if (result == DialogResult.OK)
                                    {
                                        return "nocontinue";

                                    }
                                }

                            }
                            if (omoo5.Rows.Count == 0)
                            {
                                DataTable nearomoo5 = utilities.returntbl("select distinct TargetMarketDate from dbo.DetailFRM005 where TargetMarketDate<='" + new PersianDate(SAMPLEDate).ToString("d") + "'and PPID='" + plant + "'order by TargetMarketDate desc");
                                date5 = nearomoo5.Rows[0][0].ToString();
                                bool inweek1 = finddatein7daym005(date2, plant);
                                if (inweek1 == false)
                                {
                                    DialogResult result1 = MessageBox.Show("M002 AND M005 Files In Previous week Is Not Existed!. \r\n", "Bid Allocation End Unsuccesssfully!", MessageBoxButtons.OK);
                                    if (result1 == DialogResult.OK)
                                    {
                                        return "nocontinue";

                                    }
                                }
                            }

                            if ((date2 == "" && date5 != ""))
                            {
                                date2 = date5;
                            }
                            else if ((date5 == "" && date2 != ""))
                            {
                                date5 = date2;
                            }
                            if (date5 == date2)
                            {
                                common = date2;

                                break;
                            }

                        }

                    }

                }

                else
                    common = new PersianDate(selectedDate).ToString("d");

          

            return common;

        }
        private bool finddatein7daym002(string  date,string plant)
        {

            PersianDate test2 = new PersianDate(currentDate);
            DateTime Now1 = PersianDateConverter.ToGregorianDateTime(test2);
                    
         
                    for (int daysbefore = 0; daysbefore < 7; daysbefore++)
                    {
                        DateTime ENDDate = Now1.Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                        DataTable nearomoo2 = utilities.returntbl("select distinct TargetMarketDate from dbo.DetailFRM002 where TargetMarketDate='" + new PersianDate(ENDDate).ToString("d") + "'and PPID='" + plant + "'order by TargetMarketDate desc");
                        if (nearomoo2.Rows.Count > 0)
                        {
                            if (date == nearomoo2.Rows[0][0].ToString())
                            {
                                return true;
                                break;
                            }
                        }
                    }
                    return false;

        }
        private bool finddatein7daym005(string date, string plant)
        {

            PersianDate test2 = new PersianDate(currentDate);
            DateTime Now1 = PersianDateConverter.ToGregorianDateTime(test2);


            for (int daysbefore = 0; daysbefore < 7; daysbefore++)
            {
                DateTime ENDDate = Now1.Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                DataTable nearomoo5 = utilities.returntbl("select distinct TargetMarketDate from dbo.DetailFRM005 where TargetMarketDate='" + new PersianDate(ENDDate).ToString("d") + "'and PPID='" + plant + "'order by TargetMarketDate desc");
                if (nearomoo5.Rows.Count > 0)
                {
                    if (date == nearomoo5.Rows[0][0].ToString())
                    {
                        return true;
                        break;
                    }
                }
            }
            return false;

        }

        private double[] PreSolveGetVariance(string ppId, PackageTypePriority packageType, string date)
        {

            DataTable baseplant = utilities.returntbl("select PPName from dbo.BaseData where Date =(select max(Date) from dbo.BaseData)order by BaseID desc");
            DataTable idtable = utilities.returntbl("select PPID from dbo.PowerPlant  where PPName='" + baseplant.Rows[0][0].ToString().Trim() + "'");
          
            string preplant = idtable.Rows[0][0].ToString().Trim();

            DataTable packagetable = utilities.returntbl("SELECT DISTINCT  PackageType  FROM dbo.PPUnit WHERE PPID='" + preplant.Trim()+ "'");

            string packtype = "";
            for(int i=0;i<packagetable.Rows.Count;i++)
            {
                if (packagetable.Rows[i][0].ToString().Trim() == "Steam")
                {
                     packtype = "Steam";
                     break;
                }
                else if (packagetable.Rows[i][0].ToString().Trim() == "Combined Cycle") packtype = "CC";
               
            }

            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText = "select FinalForecastHourly.hour, FinalForecastHourly.vr" +
                    " from FinalForecastHourly inner join FinalForecast" +
                    " on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id" +
                    " where FinalForecast.PPId=@ppID AND FinalForecast.PackageType=@packageType AND FinalForecast.date=@date" +
                    " order by FinalForecastHourly.hour";

            MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
            MyCom.Parameters["@ppID"].Value = preplant.Trim(); 
            MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
            MyCom.Parameters["@date"].Value = date;
            MyCom.Parameters.Add("@packageType", SqlDbType.Char, 20);
            MyCom.Parameters["@packageType"].Value = packtype.Trim();
            Maxda.Fill(MaxDS);

            double[] variance = new double[24];
            if (MaxDS.Tables[0].Rows.Count > 0)
                for (int j = 0; j < 24; j++)
                    variance[j] = Double.Parse(MaxDS.Tables[0].Rows[j]["vr"].ToString());
            return variance;
        }

        private double[,] PreSolveGet_pd_Range(string ppId, PackageTypePriority packageType, string date)
        {
            DataTable baseplant = utilities.returntbl("select PPName from dbo.BaseData where Date =(select max(Date) from dbo.BaseData)order by BaseID desc");
            DataTable idtable = utilities.returntbl("select PPID from dbo.PowerPlant  where PPName='" + baseplant.Rows[0][0].ToString().Trim() + "'");

            string preplant = idtable.Rows[0][0].ToString().Trim();

            DataTable packagetable = utilities.returntbl("SELECT DISTINCT  PackageType  FROM dbo.PPUnit WHERE PPID='" + preplant.Trim() + "'");

            string packtype = "";
            for (int i = 0; i < packagetable.Rows.Count; i++)
            {
                if (packagetable.Rows[i][0].ToString().Trim() == "Steam")
                {
                    packtype = "Steam";
                    break;
                }
                else if (packagetable.Rows[i][0].ToString().Trim() == "Combined Cycle") packtype = "CC";

            }

            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            double[,] pd_Range = new double[24, 201];

            for (int j = 0; j < 24; j++)
            {
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;

                // Insert into FinalForcast table
                MyCom.CommandText =
                    "select FinalForecast201Item.[index], FinalForecast201Item.ff" +
                    " from FinalForecast201Item inner join FinalForecastHourly " +
                    "on FinalForecastHourly.id = FinalForecast201Item.fk_ffHourly " +
                    "inner join FinalForecast " +
                    "on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id " +
                    "where FinalForecast.PPId=@PPId AND FinalForecast.PackageType=@packageType " +
                    "AND FinalForecast.date=@date AND FinalForecastHourly.hour=@hour " +
                    "order by FinalForecast201Item.[index]";


                MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                MyCom.Parameters["@ppID"].Value = preplant.Trim();
                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = date;
                MyCom.Parameters.Add("@packageType", SqlDbType.Char, 20);
                MyCom.Parameters["@packageType"].Value = packtype.Trim();
                MyCom.Parameters.Add("@hour", SqlDbType.Int);
                MyCom.Parameters["@hour"].Value = j + 1;
                Maxda.Fill(MaxDS);

                if (MaxDS.Tables[0].Rows.Count > 0)
                    for (int k = 0; k < 201; k++)
                        pd_Range[j, k] = Double.Parse(MaxDS.Tables[0].Rows[k]["ff"].ToString());
            }
            return pd_Range;
        }

        private double[,] PreSolveGet_pdist(string ppId, PackageTypePriority packageType, string date)
        {
            DataTable baseplant = utilities.returntbl("select PPName from dbo.BaseData where Date =(select max(Date) from dbo.BaseData)order by BaseID desc");
            DataTable idtable = utilities.returntbl("select PPID from dbo.PowerPlant  where PPName='" + baseplant.Rows[0][0].ToString().Trim() + "'");

            string preplant = idtable.Rows[0][0].ToString().Trim();

            DataTable packagetable = utilities.returntbl("SELECT DISTINCT  PackageType  FROM dbo.PPUnit WHERE PPID='" + preplant.Trim() + "'");

            string packtype = "";
            for (int i = 0; i < packagetable.Rows.Count; i++)
            {
                if (packagetable.Rows[i][0].ToString().Trim() == "Steam")
                {
                    packtype = "Steam";
                    break;
                }
                else if (packagetable.Rows[i][0].ToString().Trim() == "Combined Cycle") packtype = "CC";

            }


            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            double[,] pdist = new double[24, 201];

            for (int j = 0; j < 24; j++)
            {
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                DataSet MaxDS = new DataSet();
                SqlDataAdapter Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;

                // Insert into FinalForcast table
                MyCom.CommandText =
                    "select FinalForecast201Item.pdist" +
                    " from FinalForecast201Item inner join FinalForecastHourly " +
                    "on FinalForecastHourly.id = FinalForecast201Item.fk_ffHourly " +
                    "inner join FinalForecast " +
                    "on FinalForecastHourly.fk_FinalForecastId = FinalForecast.id " +
                    "where FinalForecast.PPId=@PPId AND FinalForecast.PackageType=@packageType " +
                    "AND FinalForecast.date=@date AND FinalForecastHourly.hour=@hour " +
                    "order by FinalForecast201Item.[index]";


                MyCom.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                MyCom.Parameters["@ppID"].Value = preplant.Trim();
                MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                MyCom.Parameters["@date"].Value = date;
                MyCom.Parameters.Add("@packageType", SqlDbType.Char, 20);
                MyCom.Parameters["@packageType"].Value = packtype.Trim();
                MyCom.Parameters.Add("@hour", SqlDbType.Int);
                MyCom.Parameters["@hour"].Value = j + 1;
                Maxda.Fill(MaxDS);

                if (MaxDS.Tables[0].Rows.Count > 0)
                    for (int k = 0; k < 201; k++)
                        pdist[j, k] = Double.Parse(MaxDS.Tables[0].Rows[k][0].ToString());
            }
            return pdist;
        }

        private double[, ,] PreSolveInsertToPriceForecasting(DataTable dt, double[, ,] PriceForecastings, int plantIndex, int row)
        {
            for (int hour = 0; hour < dt.Rows.Count; hour++)
                PriceForecastings[plantIndex, hour, row] = double.Parse(dt.Rows[hour]["forecast"].ToString().Trim());
            return PriceForecastings;
        }
    }
 
}
