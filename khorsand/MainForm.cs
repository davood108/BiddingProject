﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
//using Microsoft.Office.Core;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using System.Collections;
using System.IO;

namespace PowerPlantProject
{
    public partial class MainForm : Form
    {
        //Is There this PlantID in the PowerPlant?
        // dar FindPPID meghdar dehi mishavad
        string billdate = "";
        string monthlydate = "";
        string COMmonthlydate = "";
        string fmonthlybillplant = "";
        bool CheckPPID;
        string GenCo = "";
        string GenName = "";
        string gencode = "";
        //Form3 NewPlant;
        public string PPID="0";
        public int line;
        public int TextboxTab = 0;
        //string ConStr = "";
        ArrayList PPIDArray = new ArrayList();
        ArrayList PPIDType = new ArrayList();
        ArrayList PPIDName = new ArrayList();

        //const int NumPPID = 20;
        //string[] PPIDArray = new string[NumPPID];
        double PAvailableCapacity = 0, PTotalPower = 0, PULPower = 0, PBidPower = 0, PIncrementPower = 0,
        PDecreasePower = 0, PCapacityPayment = 0, PBidPayment = 0, PULPayment = 0, PIncrementPayment = 0,
        PDecreasePayment = 0, PEnergyPayment = 0, PIncome = 0, PCost = 0, PBenefit = 0;
        bool m002Generated = false;

        bool TreeView1IsSelected = false;
        DataTable UnitsData = null;

        string predismanualdate = null;

        public MainForm()
        {
            
            InitializeComponent();

           
            //NewPlant = new Form3(this);
            //ConStr = System.Configuration.ConfigurationManager.ConnectionStrings["PowerPlantProject"].ConnectionString;
            //ConStr = "Data Source=.;Initial Catalog=PowerPalntDB;Integrated Security=True";

            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            Myda.SelectCommand = new SqlCommand("SELECT PPID,PPName FROM PowerPlant", myConnection);
            Myda.Fill(MyDS, "ppid");
            foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
            {
                PPIDArray.Add(MyRow["PPID"].ToString().Trim());
                PPIDName.Add(MyRow["PPName"].ToString().Trim());

                PPIDType.Add("real");
            }

            for (int i = 0; i < PPIDArray.Count; i++)
            { 
                SqlCommand mycom=new SqlCommand();
                mycom.Connection = myConnection;
                mycom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id SELECT @result2 =count(PPID) FROM [PPUnit] WHERE PPID=@id AND PackageType LIKE 'Combined Cycle%'";
                mycom.Parameters.Add("@id", SqlDbType.NChar, 10);
                mycom.Parameters["@id"].Value = PPIDArray[i].ToString();
                mycom.Parameters.Add("@result1", SqlDbType.Int);
                mycom.Parameters["@result1"].Direction = ParameterDirection.Output;
                mycom.Parameters.Add("@result2", SqlDbType.Int);
                mycom.Parameters["@result2"].Direction = ParameterDirection.Output;
                mycom.ExecuteNonQuery();
                int result1 = (int)mycom.Parameters["@result1"].Value;
                int result2 = (int)mycom.Parameters["@result2"].Value;
                if ((result1 > 1) && (result2 > 0))
                {
                    PPIDArray.Add((int.Parse(PPIDArray[i].ToString())) + 1);
                    PPIDType.Add("virtual");
                }
            }

            BDCurGrid.DataSource = null;

            //SET Calendars
            FALocalizeManager.CustomCulture = FALocalizeManager.FarsiCulture;
            GDStartDateCal.SelectedDateTime = System.DateTime.Now;
            GDStartDateCal.IsNull = true;
            GDEndDateCal.SelectedDateTime = System.DateTime.Now;
            GDEndDateCal.IsNull = true;
            BDCal.SelectedDateTime = System.DateTime.Now;
            BDCal.IsNull = true;
            FRUnitCal.SelectedDateTime = System.DateTime.Now;
            FRUnitCal.IsNull = true;
            FRPlantCal.SelectedDateTime = System.DateTime.Now;
            FRPlantCal.IsNull = true;
            MRCal.SelectedDateTime = System.DateTime.Now;
            MRCal.IsNull = true;
            ODUnitServiceStartDate.SelectedDateTime = System.DateTime.Now;
            ODUnitServiceStartDate.IsNull = true;
            ODUnitServiceEndDate.SelectedDateTime = System.DateTime.Now;
            ODUnitServiceEndDate.IsNull = true;
            ODUnitMainStartDate.SelectedDateTime = System.DateTime.Now;
            ODUnitMainStartDate.IsNull = true;
            ODUnitMainEndDate.SelectedDateTime = System.DateTime.Now;
            ODUnitMainEndDate.IsNull = true;
            ODUnitFuelStartDate.SelectedDateTime = System.DateTime.Now;
            ODUnitFuelStartDate.IsNull = true;
            ODUnitFuelEndDate.SelectedDateTime = System.DateTime.Now;
            ODUnitFuelEndDate.IsNull = true;
            ODUnitPowerStartDate.SelectedDateTime = System.DateTime.Now;
            ODUnitPowerStartDate.IsNull = true;
            faDatePickDispatch.SelectedDateTime = System.DateTime.Now;
            faDatePickDispatch.IsNull = true;

            faDatePickerbill.SelectedDateTime = System.DateTime.Now;
            faDatePickerbill.IsNull = false;

           // ODUnitPowerEndDate.SelectedDateTime = System.DateTime.Now;
            //ODUnitPowerEndDate.IsNull = true;
            myConnection.Close();

        }
        //---------------------Form2_Load-----------------------------------
        private void Form2_Load(object sender, EventArgs e)
        {
            try
            {

                Color fileResultformback = formcolors.getcolor().Formbackcolor;
                Color fileResultbuttonback = formcolors.getcolor().Buttonbackcolor;
                Color fileResultpanelback = formcolors.getcolor().Panelbackcolor;
                Color fileResulttextback = formcolors.getcolor().Textbackcolor;
                Color fileResultgridback = formcolors.getcolor().Gridbackcolor;

                this.BackColor = fileResultformback;


                ///////////////////////buttons///////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is Button)
                        c.BackColor = fileResultbuttonback;
                }
                foreach (Control c in GDMainPanel.Controls)
                {
                    if (c is Button)
                        c.BackColor = fileResultbuttonback;
                }
                foreach (Control c in GDPowerGb.Controls)
                {
                    if (c is Button)
                        c.BackColor = fileResultbuttonback;
                }
                foreach (Control c in groupBox17.Controls)
                {
                    if (c is Button)
                        c.BackColor = fileResultbuttonback;
                }

                foreach (Control c in ConsumedGb.Controls)
                {
                    if (c is Button)
                        c.BackColor = fileResultbuttonback;
                }
                foreach (Control c in panel95.Controls)
                {
                    if (c is Button)
                        c.BackColor = fileResultbuttonback;
                }
                foreach (Control c in ODMainPanel.Controls)
                {
                    if (c is Button)
                        c.BackColor = fileResultbuttonback;
                }

                foreach (Control c in BDMainPanel.Controls)
                {
                    if (c is Button)
                        c.BackColor = fileResultbuttonback;
                }

                foreach (Control c in FRMainPanel.Controls)
                {
                    if (c is Button)
                        c.BackColor = fileResultbuttonback;
                }
                foreach (Control c in MRMainPanel.Controls)
                {
                    if (c is Button)
                        c.BackColor = fileResultbuttonback;
                }
                foreach (Control c in ODUnitPowerGB.Controls)
                {
                    if (c is Button)
                        c.BackColor = fileResultbuttonback;
                }
                foreach (Control c in groupBoxBiddingStrategy3.Controls)
                {
                    if (c is Button)
                        c.BackColor = fileResultbuttonback;
                }

                /////////////////////////////////////////////////////////////////////

                ///////////////////////////tabpage, panel/////////////////////////////////

                GeneralData.BackColor = fileResultpanelback;
                OperationalData.BackColor = fileResultpanelback;
                BidData.BackColor = fileResultpanelback;
                MarketResults.BackColor = fileResultpanelback;
                FinancialReport.BackColor = fileResultpanelback;
                BiddingStrategy.BackColor = fileResultpanelback;
                tbPageMaintenance.BackColor = fileResultpanelback;
                panel1.BackColor = fileResultpanelback;
                panel2.BackColor = fileResultpanelback;
                menuStrip1.BackColor = fileResultpanelback;
                GDPostpanel.BackColor = fileResultpanelback;
                FRPlantPanel.BackColor = fileResultpanelback;
                 paneldailybill.BackColor = fileResultpanelback;
                tbmarketbill.BackColor = fileResultpanelback;
                btnbilling.BackColor = fileResultbuttonback;
               
                ////////////////////////////////////////////////////////////
                ///////////////////////////////textbox///////////////////////////////////////////

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox)
                        c.BackColor = fileResulttextback;
                    else if (c is ListBox)
                        c.BackColor = fileResulttextback;
                    else if (c is ComboBox)
                        c.BackColor = fileResulttextback;
                }
                foreach (Control c in ConsumedGb.Controls)
                {
                    if (c is TextBox)
                        c.BackColor = fileResulttextback;
                    else if (c is ListBox)
                        c.BackColor = fileResulttextback;
                    else if (c is ComboBox)
                        c.BackColor = fileResulttextback;
                }
                foreach (Control c in GDPowerGb.Controls)
                {
                    if (c is TextBox)
                        c.BackColor = fileResulttextback;
                    else if (c is ListBox)
                        c.BackColor = fileResulttextback;
                    else if (c is ComboBox)
                        c.BackColor = fileResulttextback;
                }
                foreach (Control c in groupBox17.Controls)
                {
                    if (c is TextBox)
                        c.BackColor = fileResulttextback;
                    else if (c is ListBox)
                        c.BackColor = fileResulttextback;
                    else if (c is ComboBox)
                        c.BackColor = fileResulttextback;
                }

                foreach (Control c in groupBox20.Controls)
                {
                    if (c is TextBox)
                        c.BackColor = fileResulttextback;
                    else if (c is ListBox)
                        c.BackColor = fileResulttextback;
                    else if (c is ComboBox)
                        c.BackColor = fileResulttextback;

                }
                foreach (Control c in ODUnitPowerGB.Controls)
                {
                    if (c is TextBox)
                        c.BackColor = fileResulttextback;

                }


                BDStateTb.BackColor = fileResulttextback;
                BDPowerTb.BackColor = fileResulttextback;
                BDMaxBidTb.BackColor = fileResulttextback;
                MRPlantOnUnitTb.BackColor = fileResulttextback;
                MRUnitMaxBidTb.BackColor = fileResulttextback; ;
                MRUnitPowerTb.BackColor = fileResulttextback;
                MRUnitStateTb.BackColor = fileResulttextback;
                MRPlantPowerTb.BackColor = fileResulttextback;
                FRPlantAvaCapTb.BackColor = fileResulttextback;
                FRPlantBenefitTB.BackColor = fileResulttextback;
                FRPlantBidPayTb.BackColor = fileResulttextback;
                FRPlantBidPowerTb.BackColor = fileResulttextback;
                FRPlantCapPayTb.BackColor = fileResulttextback;
                FRPlantCostTb.BackColor = fileResulttextback; ;
                FRPlantDecPayTb.BackColor = fileResulttextback; ;
                FRPlantDecPowerTb.BackColor = fileResulttextback;
                FRPlantEnergyPayTb.BackColor = fileResulttextback;
                FRPlantIncomeTb.BackColor = fileResulttextback;
                FRPlantIncPayTb.BackColor = fileResulttextback;
                FRPlantIncPowerTb.BackColor = fileResulttextback;
                FRPlantTotalPowerTb.BackColor = fileResulttextback;
                FRPlantULPayTb.BackColor = fileResulttextback;
                FRPlantULPowerTb.BackColor = fileResulttextback;
                FRUnitAmainTb.BackColor = fileResulttextback;
                FRUnitAmargTb1.BackColor = fileResulttextback;
                FRUnitAmargTb2.BackColor = fileResulttextback;
                FRUnitBmainTb.BackColor = fileResulttextback;
                FRUnitBmargTb1.BackColor = fileResulttextback;
                FRUnitBmargTb2.BackColor = fileResulttextback;
                FRUnitCapacityTb.BackColor = fileResulttextback;
                FRUnitCapitalTb.BackColor = fileResulttextback;
                FRUnitCapPayTb.BackColor = fileResulttextback;
                FRUnitCmargTb1.BackColor = fileResulttextback;
                FRUnitCmargTb2.BackColor = fileResulttextback;
                FRUnitColdTb.BackColor = fileResulttextback;
                FRUnitEneryPayTb.BackColor = fileResulttextback;
                FRUnitFixedTb.BackColor = fileResulttextback;
                FRUnitFuelCostTb.BackColor = fileResulttextback;
                FRUnitFuelNoCostTb.BackColor = fileResulttextback;
                FRUnitHotTb.BackColor = fileResulttextback;
                FRUnitIncomeTb.BackColor = fileResulttextback;
                FRUnitPActiveTb.BackColor = fileResulttextback;
                FRUnitQReactiveTb.BackColor = fileResulttextback;
                FRUnitTotalPowerTb.BackColor = fileResulttextback;
                FRUnitULPowerTb.BackColor = fileResulttextback;
                FRUnitVariableTb.BackColor = fileResulttextback;
                ODUnitFuelTB.BackColor = fileResulttextback;
                GDcapacityTB.BackColor = fileResulttextback;
                GDFuelTB.BackColor = fileResulttextback;
                GDMainTypeTB.BackColor = fileResulttextback;
                GDPmaxTB.BackColor = fileResulttextback;
                GDPminTB.BackColor = fileResulttextback;
                GDPowerSerialTb.BackColor = fileResulttextback;
                GDpowertransserialtb.BackColor = fileResulttextback;
                GDPrimaryFuelTB.BackColor = fileResulttextback;
                GDRampRateTB.BackColor = fileResulttextback;
                GDSecondaryFuelTB.BackColor = fileResulttextback;
                GDStateTB.BackColor = fileResulttextback;
                GDTimeColdStartTB.BackColor = fileResulttextback;
                GDTimeDownTB.BackColor = fileResulttextback;
                GDTimeHotStartTB.BackColor = fileResulttextback;
                GDTimeUpTB.BackColor = fileResulttextback;
                GDtxtAvc.BackColor = fileResulttextback;
                GDConsumedSerialTb.BackColor = fileResulttextback;
                GDMainTypeTB.BackColor = fileResulttextback;
                txtMaintCIDur.BackColor = fileResulttextback;
                txtMaintCIHours.BackColor = fileResulttextback;
                txtMaintHGPDur.BackColor = fileResulttextback;
                txtMaintHGPHours.BackColor = fileResulttextback;
                txtMaintMODur.BackColor = fileResulttextback;
                txtMaintMOHours.BackColor = fileResulttextback;
                txtMaintStatus.BackColor = fileResulttextback;
                txtDurabilityHours.BackColor = fileResulttextback;
                txtMaintLastHour.BackColor = fileResulttextback;
                txtMaintStartUpFactor.BackColor = fileResulttextback;
                txtMaintFuelFactor.BackColor = fileResulttextback;
                txtMaintHGPHours.BackColor = fileResulttextback;
                txtMaintHGPDur.BackColor = fileResulttextback;
                cmbCostLevel.BackColor = fileResulttextback;
                cmbPeakBid.BackColor = fileResulttextback;
                txtpowertraining.BackColor = fileResulttextback;
                txtTraining.BackColor = fileResulttextback;
                cmbRunSetting.BackColor = fileResulttextback;
                cmbStrategyBidding.BackColor = fileResulttextback;
                lstStatus.BackColor = fileResulttextback;
                cmbMaintShownPlant.BackColor = fileResulttextback;
                ////////////////////////////////////////////////////////////////////////////////                   
                //////////////////////////////////grids////////////////////////////////////////////



                ODUnitPowerGrid1.RowsDefaultCellStyle.BackColor = fileResultgridback;
                ODUnitPowerGrid1.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;


                ODUnitPowerGrid2.RowsDefaultCellStyle.BackColor = fileResultgridback;
                ODUnitPowerGrid2.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;


                MRCurGrid2.RowsDefaultCellStyle.BackColor = fileResultgridback;
                MRCurGrid2.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;


                MRCurGrid1.RowsDefaultCellStyle.BackColor = fileResultgridback;
                MRCurGrid1.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;



                BDCurGrid.RowsDefaultCellStyle.BackColor = fileResultgridback;
                BDCurGrid.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;


                Grid230.RowsDefaultCellStyle.BackColor = fileResultgridback;
                Grid230.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;


                Grid400.RowsDefaultCellStyle.BackColor = fileResultgridback;
                Grid400.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;


                PlantGV1.RowsDefaultCellStyle.BackColor = fileResultgridback;
                PlantGV1.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;


                PlantGV2.RowsDefaultCellStyle.BackColor = fileResultgridback;
                PlantGV2.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;


                dataGridDispatch.RowsDefaultCellStyle.BackColor = fileResultgridback;
                dataGridDispatch.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;


                dgbill.RowsDefaultCellStyle.BackColor = fileResultgridback;
                dgbill.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;

                dataGridViewECO.RowsDefaultCellStyle.BackColor = fileResultgridback;
                dataGridViewECO.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;


                dataGridDispatch.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                ODUnitPowerGrid1.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                ODUnitPowerGrid2.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                MRCurGrid2.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                MRCurGrid1.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                BDCurGrid.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                Grid230.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                Grid400.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                PlantGV1.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                PlantGV2.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                dgbill.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                dataGridViewECO.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;

               



                ////////////////////////////////////timer/////////////////////////////////////////// 


                System.Globalization.PersianCalendar p = new System.Globalization.PersianCalendar();

                label115.Text = p.GetSecond(DateTime.Now) + " : " + p.GetMinute(DateTime.Now) + " : " + p.GetHour(DateTime.Now);
                timer1.Start();


            }
            catch
            {

            }


            try
            {
                if (User.getUser().Role == DefinedRoles.Noaccessplant||
                    User.getUser().Role == DefinedRoles.Unassigned)
                {
                    addToolStripMenuItem.Enabled = false;
                    DeletetoolStripMenuItem.Enabled = false;
                }
                //~~~~~~~~~~~~~~~~~~~~~~~~user and delete and backup~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`
                toolStripuseraccount.Enabled = false;
                deleteDataToolStripMenuItem.Enabled = false;           
                toolStripMenuItemExport.Visible = false;
                backUpToolStripMenuItem.Enabled = false;

                if (User.getUser().Role == DefinedRoles.nriAdministrator ||
                  User.getUser().Role == DefinedRoles.Administrator)
                {
                    toolStripuseraccount.Enabled = true;
                    deleteDataToolStripMenuItem.Enabled = true;
                    toolStripMenuItemExport.Visible = true;
                    backUpToolStripMenuItem.Enabled = true;
                }
                
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`
                //-----------------------remove maintenance temorary-------------------------------
                MainTabs.TabPages.Remove(MainTabs.TabPages["tbPageMaintenance"]);               
                //--------------------------------------------------------

                lbllogname.Text += "    "+User.getUser().Username.Trim();
            }
            catch { }

            buildTreeView1();
            LoadBiddingStrategy();
            LoadMaintenance();

            if (User.getUser().Role == DefinedRoles.NoMarketBill ||
           User.getUser().Role == DefinedRoles.ExceptComputational ||
           User.getUser().Role == DefinedRoles.Unassigned ||
           User.getUser().Role == DefinedRoles.PlantUser)
            {
                MainTabs.TabPages.Remove(MainTabs.TabPages["tbmarketbill"]);
                addToolStripMenuItem.Enabled = false;
                DeletetoolStripMenuItem.Enabled = false;
            }

            //AutomaticFillEconomics();
            //Initialze StartDate for Calculate
            //string StartDate="1389/01/01";
            //JustOneTime(StartDate);
        }
        //-------------------------------bulidTreeView1---------------------------------
        private void buildTreeView1()
        {
            DataTable dt = utilities.returntbl("select * from BaseGencoInfo");
            if (dt.Rows.Count > 0)
            {
                //GenCo = dt.Rows[0]["GencoNameEnglish"].ToString().Substring(0, 1).Trim().ToUpper() + "rec";
                GenCo = dt.Rows[0]["GencoNameEnglish"].ToString();
                GenName = dt.Rows[0]["GencoNameEnglish"].ToString().Trim();
                gencode = dt.Rows[0]["GencoCode"].ToString().Trim();
            }
            else
            {
                MessageBox.Show("Please Fill GencoForm At First .", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);               
            }
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            treeView1.Nodes.Clear();
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();

            //Insert Trec
            TreeNode MyNode = new TreeNode();
            MyNode.Text = GenCo;
            treeView1.Nodes.Add(MyNode);

            //Insert Plants
            Myda.SelectCommand = new SqlCommand("SELECT PPName FROM PowerPlant order by ppid", myConnection);
            Myda.Fill(MyDS, "plantname");
            TreeNode PlantNode = new TreeNode();
            PlantNode.Text = "Plant";
            MyNode.Nodes.Add(PlantNode);
            foreach (DataRow MyRow in MyDS.Tables["plantname"].Rows)
            {
                TreeNode ChildNode = new TreeNode();
                ChildNode.Text = MyRow["PPName"].ToString().Trim();
                PlantNode.Nodes.Add(ChildNode);
            }

            //Insert Transmission
            Myda.SelectCommand = new SqlCommand("SELECT DISTINCT LineNumber FROM TransLine", myConnection);
            Myda.Fill(MyDS, "transtype");
            myConnection.Close();

            TreeNode TransNode = new TreeNode();
            TransNode.Text = "Transmission";
            MyNode.Nodes.Add(TransNode);
            foreach (DataRow MyRow in MyDS.Tables["transtype"].Rows)
            {
                TreeNode ChildNode = new TreeNode();
                ChildNode.Text = MyRow["LineNumber"].ToString().Trim();
                TransNode.Nodes.Add(ChildNode);
            }
            //Insert Substation
            Myda.SelectCommand = new SqlCommand("SELECT DISTINCT PostNumber FROM PostVoltage", myConnection);
            Myda.Fill(MyDS, "postnumber");
            myConnection.Close();

            TreeNode PostNode = new TreeNode();
            PostNode.Text = "SubStation";
            MyNode.Nodes.Add(PostNode);
            foreach (DataRow MyRow in MyDS.Tables["postnumber"].Rows)
            {
                TreeNode ChildNode = new TreeNode();
                ChildNode.Text = MyRow["PostNumber"].ToString().Trim();
                PostNode.Nodes.Add(ChildNode);
            }
            treeView1.ExpandAll();
        }
        //--------------------buildPPtree----------------------------------------
        private void buildPPtree(string Num)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            treeView2.Nodes.Clear();
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT DISTINCT PackageType FROM UnitsDataMain WHERE PPID=@Num", myConnection);
            Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
            Myda.SelectCommand.Parameters["@Num"].Value = Num;
            Myda.Fill(MyDS, "UnitType");

            foreach (DataRow MyRow in MyDS.Tables["UnitType"].Rows)
            {
                string Utype = MyRow["PackageType"].ToString().Trim();
                Utype = Utype.Trim();
                switch (Utype)
                {
                    case "Steam":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Steam%' order by PPID,UnitCode", myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = Num;
                        Myda.Fill(MyDS, "Steam");
                        TreeNode MyNode = new TreeNode();
                        MyNode.Text = "Steam";
                        treeView2.Nodes.Add(MyNode);
                        string temp = "";
                        DataView sdv = new DataView(MyDS.Tables["Steam"]);
                        sdv.Sort = "UnitCode ASC";
                        foreach (DataRow ChildRow in sdv.Table.Rows)
                        //foreach (DataRow ChildRow in MyDS.Tables["Steam"].Rows)
                        {
                            temp = ChildRow["unitCode"].ToString().Trim();
                            if (temp.Length == 6)
                            {
                                TreeNode ChildNode = new TreeNode();
                                ChildNode.Text = ChildRow["UnitCode"].ToString().Trim();
                                MyNode.Nodes.Add(ChildNode);
                            }
                        }
                        foreach (DataRow ChildRow in sdv.Table.Rows)
                        //foreach (DataRow ChildRow in MyDS.Tables["Steam"].Rows)
                        {
                            temp = ChildRow["UnitCode"].ToString().Trim();
                            if (temp.Length == 7)
                            {
                                TreeNode ChildNode = new TreeNode();
                                ChildNode.Text = ChildRow["UnitCode"].ToString().Trim();
                                MyNode.Nodes.Add(ChildNode);
                            }
                        }
                        break;
                    case "Gas":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Gas%' order by UnitCode", myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = Num;
                        Myda.Fill(MyDS, "Gas");
                        TreeNode Node1 = new TreeNode();
                        Node1.Text = "Gas";
                        treeView2.Nodes.Add(Node1);
                        DataView gdv = new DataView(MyDS.Tables["Gas"]);
                        gdv.Sort = "UnitCode";
                        foreach (DataRow ChildRow in gdv.Table.Rows)
                        //foreach (DataRow ChildRow in MyDS.Tables["Gas"].Rows)
                        {
                            temp = ChildRow["UnitCode"].ToString().Trim();
                            if (temp.Length == 4)
                            {
                                TreeNode ChildNode = new TreeNode();
                                ChildNode.Text = ChildRow["UnitCode"].ToString().Trim();
                                Node1.Nodes.Add(ChildNode);
                            }
                        }
                        foreach (DataRow ChildRow in gdv.Table.Rows)
                        //foreach (DataRow ChildRow in MyDS.Tables["Gas"].Rows)
                        {
                            temp = ChildRow["UnitCode"].ToString().Trim();
                            if (temp.Length == 5)
                            {
                                TreeNode ChildNode = new TreeNode();
                                ChildNode.Text = ChildRow["UnitCode"].ToString().Trim();
                                Node1.Nodes.Add(ChildNode);
                            }
                        }
                        break;
                    default:
                        Myda.SelectCommand = new SqlCommand("SELECT DISTINCT PackageCode FROM [UnitsDataMain] WHERE PPID=@Num AND PackageType LIKE 'CC%'", myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = Num;
                        Myda.Fill(MyDS, "CCPackage");
                        foreach (DataRow ChildRow in MyDS.Tables["CCPackage"].Rows)
                        {
                            TreeNode PNode = new TreeNode();
                            PNode.Text = "Combined Cycle" + ChildRow["PackageCode"].ToString().Trim();
                            treeView2.Nodes.Add(PNode);
                        }

                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageCode FROM [UnitsDataMain] WHERE PPID=@Num AND PackageType LIKE 'CC%' order by UnitCode", myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = Num;
                        Myda.Fill(MyDS, "Combined");
                        DataView cdv = new DataView(MyDS.Tables["Combined"]);
                        cdv.Sort = "UnitCode ASC";
                        foreach (DataRow ChildRow in cdv.Table.Rows)
                            //foreach (DataRow ChildRow in MyDS.Tables["Combined"].Rows)
                            foreach (TreeNode mynode in treeView2.Nodes)
                                if (mynode.Text.Contains(ChildRow["PackageCode"].ToString().Trim()))
                                {
                                    TreeNode GNode = new TreeNode();
                                    GNode.Text = ChildRow["UnitCode"].ToString().Trim();
                                    mynode.Nodes.Add(GNode);
                                }
                        break;
                }
            }

            MyDS.Dispose();
            Myda.Dispose();
            myConnection.Close();
            treeView2.ExpandAll();
        }

        //---------------------Form2_FormClosed-----------------------------------------
        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        //------------------------buildTRANStree----------------------------------
        private void buildTRANStree(string Num)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            TreeNode MyNode, ChildNode, CNode;
            treeView2.Nodes.Clear();
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT LineNumber,Name,LineCode FROM TransLine WHERE LineNumber=@Num ORDER BY TransLine.Name", myConnection);
            Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.Int);
            Myda.SelectCommand.Parameters["@Num"].Value = Num;
            Myda.Fill(MyDS, "Trans");

            foreach (DataRow MyRow in MyDS.Tables["Trans"].Rows)
            {
                bool Rthereis = false;
                int type = int.Parse(MyRow["LineNumber"].ToString().Trim());
                foreach (TreeNode node in treeView2.Nodes)
                {
                    if (node.Text == MyRow["LineNumber"].ToString().Trim())
                    {
                        Rthereis = true;
                        bool Cthereis = false;
                        foreach (TreeNode node1 in node.Nodes)
                            if (node1.Text == MyRow["Name"].ToString().Trim())
                            {
                                Cthereis = true;
                                CNode = new TreeNode();
                                CNode.Text = MyRow["LineCode"].ToString().Trim();
                                node1.Nodes.Add(CNode);
                            }
                        if (!Cthereis)
                        {
                            ChildNode = new TreeNode();
                            ChildNode.Text = MyRow["Name"].ToString().Trim();
                            node.Nodes.Add(ChildNode);
                            CNode = new TreeNode();
                            CNode.Text = MyRow["LineCode"].ToString().Trim();
                            ChildNode.Nodes.Add(CNode);
                        }
                    }
                }
                if (!Rthereis)
                {
                    MyNode = new TreeNode();
                    MyNode.Text = MyRow["LineNumber"].ToString().Trim();
                    treeView2.Nodes.Add(MyNode);
                    bool Cthereis = false;
                    foreach (TreeNode node1 in MyNode.Nodes)
                        if (node1.Text == MyRow["Name"].ToString().Trim())
                        {
                            Cthereis = true;
                            CNode = new TreeNode();
                            CNode.Text = MyRow["LineCode"].ToString().Trim();
                            node1.Nodes.Add(CNode);
                        }
                    if (!Cthereis)
                    {
                        ChildNode = new TreeNode();
                        ChildNode.Text = MyRow["Name"].ToString().Trim();
                        MyNode.Nodes.Add(ChildNode);
                        CNode = new TreeNode();
                        CNode.Text = MyRow["LineCode"].ToString().Trim();
                        ChildNode.Nodes.Add(CNode);
                    }
                }
            }

            MyDS.Dispose();
            Myda.Dispose();
            myConnection.Close();
            treeView2.ExpandAll();
        }
//----------------------------CheckDate--------------------------------
        private bool CheckDate(string date, string start, string end)
        {
            bool result = false;
            string temp1 = date.Remove(4);
            start = start.Trim();
            end = end.Trim();
            if ((start != "") && (end != ""))
            {
                string temp2 = start.Remove(4);
                if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
                else
                {
                    temp1 = date.Remove(7);
                    temp1 = temp1.Remove(0, temp1.Length - 2);
                    temp2 = start.Remove(7);
                    temp2 = temp2.Remove(0, temp2.Length - 2);
                    if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                    else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
                    else
                    {
                        temp1 = date.Remove(0, date.Length - 2);
                        temp2 = start.Remove(0, start.Length - 2);
                        if ((int.Parse(temp1) > int.Parse(temp2)) || (temp1 == temp2)) result = true;
                        else result = false;
                    }
                }
                if (!result) return (result);
                else
                {
                    temp1 = date.Remove(4);
                    temp2 = end.Remove(4);
                    if (int.Parse(temp1) < int.Parse(temp2)) return (true);
                    else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
                    else
                    {
                        temp1 = date.Remove(7);
                        temp1 = temp1.Remove(0, temp1.Length - 2);
                        temp2 = end.Remove(7);
                        temp2 = temp2.Remove(0, temp2.Length - 2);
                        if (int.Parse(temp1) < int.Parse(temp2)) return (true);
                        else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
                        else
                        {
                            temp1 = date.Remove(0, date.Length - 2);
                            temp2 = end.Remove(0, end.Length - 2);
                            if ((int.Parse(temp1) < int.Parse(temp2) )|| (temp1 == temp2))
                                return (true);
                            else return (false);
                        }
                    }
                }
            }
            return false;
        }

      
      
      
        //------------------------SetHeader2Plant----------------------------

        private void SetHeader2Plant(string package, string unit, string type)
        {
            //datePickerDurability.SelectedDateTime = new PersianDate(DateTime.Now);

            groupBox3.Visible = false;
            btnex009.Visible = false;
            btnex009.Enabled = false;
            GDHeaderGB.Visible = true;
            ODHeaderGB.Visible = true;
            MRHeaderGB.Visible = true;
            BDHeaderGb.Visible = true;
            FRHeaderGb.Visible = true;
            GDNewBtn.Visible = true;
            GDDeleteBtn.Visible = true;
            ODSaveBtn.Visible = true;
            FROKBtn.Visible = true;
            FRUpdateBtn.Visible = true;
            BDPlotBtn.Visible = true;
            btnbidprint.Visible = true;
            MRPlotBtn.Visible = true;
            GDPlantLb.Visible = true;
            ODPlantLb.Visible = true;
            FRPlantLb.Visible = true;
            BDPlantLb.Visible = true;
            MRPlantLb.Visible = true;
            Point pnt = new Point(62, 23);
            GDPlantLb.Location = pnt;
            ODPlantLb.Location = pnt;
            MRPlantLb.Location = pnt;
            BDPlantLb.Location = pnt;
            FRPlantLb.Location = pnt;
            FRPlantPanel.Visible = false;
            FRUnitPanel.Visible = true;
            MRUnitCurGb.Visible = true;
            MRPlantCurGb.Visible = false;
            //ODUnitPanel.Visible = true;
            panel97.Visible = true;
            //ODPlantPanel.Visible = false;
            GDUnitPanel.Visible = true;
            GDGasGroup.Visible = false;
            GDSteamGroup.Visible = false;
            GDHeaderPanel.Visible = true;
            ODHeaderPanel.Visible = true;
            MRHeaderPanel.Visible = true;
            FRHeaderPanel.Visible = true;
            BDHeaderPanel.Visible = true;
            BDCur1.Visible = true;
            BDCur2.Visible = true;
            L1.Visible = true;
            L1.Text = "Plant: ";
            L5.Visible = true;
            L5.Text = "Plant: ";
            L9.Visible = true;
            L9.Text = "Plant: ";
            L13.Visible = true;
            L13.Text = "Plant: ";
            L17.Visible = true;
            L17.Text = "Plant: ";
            Currentgb.Text = "CURRENT STATE";
            Maintenancegb.Text = "MAINTENANCE";
            Currentgb.Visible = false;
            Maintenancegb.Visible = false;
            GDUnitMaintenanceGB.Visible = true;
            GDPmaxLb.Text = "Pmax";
            GDPminLb.Text = "Pmin";
            GDTUpLb.Text = "Time Up";
            GDTimeDownLb.Text = "Time Down";
            GDColdLb.Text = "Time Cold Start";
            GDHotLb.Text = "Time Hot Start";
            GDRampLb.Text = "Ramp Rate";
            GDConsLb.Text = "Heat Value Primary Fuel";
            GDStateLb.Text = "STATE";
            GDFuelLb.Text = "FUEL";
            GDPackLb.Text = package;
            ODPackLb.Text = package;
            MRPackLb.Text = package;
            BDPackLb.Text = package;
            FRPackLb.Text = package;
            GDUnitLb.Text = unit;
            ODUnitLb.Text = unit;
            MRUnitLb.Text = unit;
            BDUnitLb.Text = unit;
            FRUnitLb.Text = unit;
            GDTypeLb.Text = type;
            ODTypeLb.Text = type;
            MRTypeLb.Text = type;
            BDTypeLb.Text = type;
            FRTypeLb.Text = type;
            BDCur1.Visible = true;
            BDCur2.Visible = true;
            MRGB.Visible = true;
            GDDeleteBtn.Text = "Edit Mode";
            GDNewBtn.Enabled = false;
            GDNewBtn.Text = "Save";
            FROKBtn.Visible = true;
            FRUpdateBtn.Enabled = false;
            //??????????????????
            FRUpdateBtn.Enabled = true;
            FRUnitCapitalTb.ReadOnly = true;
            FRUnitFixedTb.ReadOnly = true;
            FRUnitVariableTb.ReadOnly = true;
            FRUnitAmargTb1.ReadOnly = true;
            FRUnitBmargTb1.ReadOnly = true;
            FRUnitCmargTb1.ReadOnly = true;
            FRUnitAmargTb2.ReadOnly = true;
            FRUnitBmargTb2.ReadOnly = true;
            FRUnitCmargTb2.ReadOnly = true;
            FRUnitAmainTb.ReadOnly = true;
            FRUnitBmainTb.ReadOnly = true;
            FRUnitColdTb.ReadOnly = true;
            FRUnitHotTb.ReadOnly = true;
            GDSecondaryFuelPanel.Visible = true;
            Point PrimaryPnt = new Point(80, 74);
            GDPrimaryFuelPanel.Location = PrimaryPnt;
            Point powerPnt = new Point(33, 10);
            Powergb.Location = powerPnt;
            Countergb.Visible = true;
            FRPlot.Visible = false;
            btnfrprint.Visible = false;
            btnfinanceexcel.Visible = false;
            FRRun.Visible = false;
            FRRunAuto.Visible = false;


            ODSaveBtn.Visible = true;
            faDatePickDispatch.Visible = false;
            dataGridDispatch.Visible = false;

            rbestimateplant.Visible = false;
            rbrealplant.Visible = false;

            panel94.Visible = false;
            panelAvc.Visible = true;

            mrbtnpprint.Visible = true;
            mrbtnpprint.Enabled = false;
            rdbam005ba.Visible = true;
            rdm005.Visible = true;
        }
        //------------------------SetHeader1Plant---------------------------
        private void SetHeader1Plant()
        {
            groupBox3.Visible = false;
            GDPostpanel.Visible = false;
            btnex009.Visible = true;
            btnex009.Enabled = true;
            GDHeaderGB.Visible = true;
            ODHeaderGB.Visible = true;
            MRHeaderGB.Visible = true;
            BDHeaderGb.Visible = false;
            FRHeaderGb.Visible = true;
            GDNewBtn.Visible = true;
            GDDeleteBtn.Visible = true;
            ODSaveBtn.Visible = true;
            BDPlotBtn.Visible = false;
            btnbidprint.Visible = false;
            MRPlotBtn.Visible = true;
            FROKBtn.Visible = false;
            FRUpdateBtn.Visible = false;
            GDPlantLb.Visible = true;
            ODPlantLb.Visible = true;
            FRPlantLb.Visible = true;
            BDPlantLb.Visible = true;
            MRPlantLb.Visible = true;
            Point pnt = new Point(62, 23);
            GDPlantLb.Location = pnt;
            ODPlantLb.Location = pnt;
            MRPlantLb.Location = pnt;
            BDPlantLb.Location = pnt;
            FRPlantLb.Location = pnt;
            FRPlantPanel.Visible = true;
            FRUnitPanel.Visible = false;
            MRUnitCurGb.Visible = false;
            MRPlantCurGb.Visible = true;
            ODUnitPanel.Visible = false;
            panel97.Visible = false;
            //ODPlantPanel.Visible = true;
            GDUnitPanel.Visible = false;
            GDGasGroup.Visible = true;
            GDSteamGroup.Visible = true;
            GDHeaderPanel.Visible = false;
            ODHeaderPanel.Visible = false;
            MRHeaderPanel.Visible = false;
            FRHeaderPanel.Visible = false;
            BDHeaderPanel.Visible = false;
            Grid230.Visible = false;
            Grid400.Visible = false;
            PlantGV1.Visible = true;
            PlantGV2.Visible = true;
            BDCur1.Visible = false;
            BDCur2.Visible = false;
            L1.Visible = true;
            L1.Text = "Plant: ";
            L5.Visible = true;
            L5.Text = "Plant: ";
            L9.Visible = true;
            L9.Text = "Plant: ";
            L13.Visible = true;
            L13.Text = "Plant: ";
            L17.Visible = true;
            L17.Text = "Plant: ";
            BDCur1.Visible = false;
            BDCur2.Visible = false;
            MRGB.Visible = true;
            Point pnt1 = new Point(30, 1);
            MRLabel1.Location = pnt1;
            MRLabel2.Location = pnt1;
            MRLabel1.Text = "ON UNITS";
            MRLabel2.Text = "POWER";
            GDDeleteBtn.Text = "Delete Unit";
            GDNewBtn.Enabled = true;
            GDNewBtn.Text = "Add Unit";
            //??????????????????age faghat neveshtan dar DB dashte bashe doroste???????????
            FRUpdateBtn.Enabled = true;
            FROKBtn.Visible = false;
            FRPlot.Visible = true;
            btnfrprint.Visible = true;
            btnfinanceexcel.Visible = true;
            FRRun.Visible = true;
            FRRunAuto.Visible = true;

            ODSaveBtn.Visible = false;
            faDatePickDispatch.Visible = true;
            dataGridDispatch.Visible = true;

            rbestimateplant.Visible = true;
            rbrealplant.Visible = true;

            panel94.Visible = true;
            panelAvc.Visible = false;

            mrbtnpprint.Visible = false;
            rdbam005ba.Visible = true;
            rdm005.Visible = true;
        }
        //-----------------------------SetHeader1Transmission------------------------
        private void SetHeader1Transmission()
        {
            rdbam005ba.Visible = true;
            rdm005.Visible = true;

            groupBox3.Visible = false;
            GDPostpanel.Visible = false;

            btnex009.Visible = false;
            btnex009.Enabled = false;
            GDHeaderGB.Visible = true;
            ODHeaderGB.Visible = true;
            MRHeaderGB.Visible = true;
            BDHeaderGb.Visible = true;
            FRHeaderGb.Visible = true;
            GDNewBtn.Visible = true;
            GDDeleteBtn.Visible = true;
            ODSaveBtn.Visible = false;
            BDPlotBtn.Visible = false;
            btnbidprint.Visible = false;
            MRPlotBtn.Visible = false;
            FROKBtn.Visible = false;
            FRUpdateBtn.Visible = false;
            GDHeaderPanel.Visible = false;
            ODHeaderPanel.Visible = false;
            MRHeaderPanel.Visible = false;
            FRHeaderPanel.Visible = false;
            BDHeaderPanel.Visible = false;
            FRPlantPanel.Visible = false;
            FRUnitPanel.Visible = false;
            ODUnitPanel.Visible = false;
            panel97.Visible = false;
            //ODPlantPanel.Visible = false;
            BDCur1.Visible = false;
            BDCur2.Visible = false;
            GDPlantLb.Text = GenName;
            GDPlantLb.Visible = true;
            MRPlantLb.Visible = true;
            ODPlantLb.Visible = true;
            FRPlantLb.Visible = true;
            BDPlantLb.Visible = true;
            Point pnt = new Point(100, 23);
            GDPlantLb.Location = pnt;
            ODPlantLb.Location = pnt;
            MRPlantLb.Location = pnt;
            BDPlantLb.Location = pnt;
            FRPlantLb.Location = pnt;
            L1.Visible = true;
            L1.Text = "Transmission: ";
            //L5.Visible = false;
            L9.Visible = true;
            L9.Text = "Transmission: ";
            //L13.Visible = false;
            //L17.Visible = false;
            L5.Text = "Transmission: ";
            L13.Text = "Transmission: ";
            L17.Text = "Transmission: ";
            Grid230.Visible = true;
            Grid400.Visible = true;
            PlantGV1.Visible = false;
            PlantGV2.Visible = false;
            Currentgb.Text = "OTHER OWNER";
            Point pnt1 = new Point(1, 1);
            MRLabel1.Location = pnt1;
            MRLabel2.Location=pnt1;
            MRLabel1.Text = "Capacity Payment";
            MRLabel2.Text = "Energy Payment";
            GDGasGroup.Text = "400";
            GDSteamGroup.Text = "230";
            GDGasGroup.Visible = true;
            GDSteamGroup.Visible = true;
            GDUnitPanel.Visible = false;
            MRGB.Visible = true;
            MRUnitCurGb.Visible = false;
            MRPlantCurGb.Visible = true;
            GDDeleteBtn.Text = "Delete Line";
            GDNewBtn.Enabled = true;
            GDNewBtn.Text = "Add Line";
            FRPlot.Visible = false;
            btnfrprint.Visible = false;
            btnfinanceexcel.Visible = false;
            FRRun.Visible = false;
            FRRunAuto.Visible = false;

            faDatePickDispatch.Visible = false;
            dataGridDispatch.Visible = false;
            rbestimateplant.Visible = false;
            rbrealplant.Visible = false;
            panel94.Visible = false;
            panelAvc.Visible = false;
            mrbtnpprint.Visible = false;
        }
        //------------------------------SetHeader2Transmission----------------------
        private void SetHeader2Transmission()
        {
            rdbam005ba.Visible = true;
            rdm005.Visible = true;

            GDPostpanel.Visible = false;
            btnex009.Visible = false;
            btnex009.Enabled = false;
            GDHeaderGB.Visible = true;
            ODHeaderGB.Visible = true;
            MRHeaderGB.Visible = true;
            BDHeaderGb.Visible = true;
            FRHeaderGb.Visible = true;
            GDNewBtn.Visible = true;
            GDDeleteBtn.Visible = true;
            ODSaveBtn.Visible = false;
            BDPlotBtn.Visible = false;
            btnbidprint.Visible = false;
            MRPlotBtn.Visible = false;
            FROKBtn.Visible = false;
            FRUpdateBtn.Visible = false;
            GDHeaderPanel.Visible = false;
            ODHeaderPanel.Visible = false;
            MRHeaderPanel.Visible = false;
            FRHeaderPanel.Visible = false;
            BDHeaderPanel.Visible = false;
            GDPlantLb.Visible = true;
            MRPlantLb.Visible = true;
            ODPlantLb.Visible = true;
            FRPlantLb.Visible = true;
            BDPlantLb.Visible = true;
            Point pnt = new Point(62, 23);
            GDPlantLb.Location = pnt;
            ODPlantLb.Location = pnt;
            MRPlantLb.Location = pnt;
            BDPlantLb.Location = pnt;
            FRPlantLb.Location = pnt;
            L1.Visible = true;
            L1.Text = "Line: ";
            //L5.Visible = false;
            L9.Visible = true;
            L9.Text = "Line: ";
            //L13.Visible = false;
            //L17.Visible = false;
            L5.Text = "Line: ";
            L13.Text = "Line: ";
            L17.Text = "Line: ";
            Currentgb.Text = "OTHER OWNER";
            Maintenancegb.Text = "OUT SERVICE";
            Currentgb.Visible = true;
            Maintenancegb.Visible = true;
            GDUnitMaintenanceGB.Visible = false;
            GDPmaxLb.Text = "From Bus";
            GDPminLb.Text = "To Bus";
            GDTUpLb.Text = "Circuit ID";
            GDTimeDownLb.Text = "    Line-R";
            GDColdLb.Text = "     Line-X";
            GDHotLb.Text = "Suseptance";
            GDRampLb.Text = "Line Length";
            GDConsLb.Text = "         Status";
            GDStateLb.Text = "Genco";
            GDFuelLb.Text = "Percent";
            GDGasGroup.Visible = false;
            GDSteamGroup.Visible = false;
            GDUnitPanel.Visible = true;
            MRGB.Visible = true;
            MRUnitCurGb.Visible = false;
            MRPlantCurGb.Visible = true;
            GDDeleteBtn.Text = "Edit Mode";
            GDNewBtn.Enabled = false;
            GDNewBtn.Text = "Save";
            GDSecondaryFuelPanel.Visible = false;
            Point PrimaryPnt = new Point(220, 74);
            GDPrimaryFuelPanel.Location = PrimaryPnt;

            //Point powerPnt = new Point(180, 10);
            //Powergb.Location = powerPnt;


            Countergb.Visible = false;

             /////////////////////////////////////

            Point Pntgbb = new Point(100, 10);
            Powergb.Location = Pntgbb;


            Point ppp = new Point(460, 10);
            groupBox3.Location = ppp;
            groupBox3.Visible = true;
            FRPlot.Visible = false;
            btnfrprint.Visible = false;
            btnfinanceexcel.Visible = false;
            FRRun.Visible = false;
            FRRunAuto.Visible = false;


            faDatePickDispatch.Visible = false;
            dataGridDispatch.Visible = false;

            rbestimateplant.Visible = false;
            rbrealplant.Visible = false;
            panel94.Visible = false;
            panelAvc.Visible = false;

            mrbtnpprint.Visible = false;
        }
        //-------------------------SetHeader1Substation----------------
        private void SetHeader1Substation()
        {
            groupBox3.Visible = false;
            GDPostpanel.Visible = true;
            btnex009.Visible = false;
            btnex009.Enabled = false;
            GDHeaderGB.Visible = false;
            ODHeaderGB.Visible = false;
            MRHeaderGB.Visible = false;
            BDHeaderGb.Visible = false;
            FRHeaderGb.Visible = false;
            GDNewBtn.Visible = false;
            GDDeleteBtn.Visible = false;
            ODSaveBtn.Visible = false;
            BDPlotBtn.Visible = false;
            btnbidprint.Visible = false;
            MRPlotBtn.Visible = false;
            FROKBtn.Visible = false;
            FRUpdateBtn.Visible = false;
            GDPlantLb.Visible = false;
            ODPlantLb.Visible = false;
            FRPlantLb.Visible = false;
            BDPlantLb.Visible = false;
            MRPlantLb.Visible = false;
            FRPlantPanel.Visible = false;
            FRUnitPanel.Visible = false;
            MRUnitCurGb.Visible = false;
            MRPlantCurGb.Visible = false;
            ODUnitPanel.Visible = false;
            panel97.Visible = false;
            //ODPlantPanel.Visible = true;
            GDUnitPanel.Visible = false;
            GDGasGroup.Visible = false;
            GDSteamGroup.Visible = false;
            GDHeaderPanel.Visible = false;
            ODHeaderPanel.Visible = false;
            MRHeaderPanel.Visible = false;
            FRHeaderPanel.Visible = false;
            BDHeaderPanel.Visible = false;
            Grid230.Visible = false;
            Grid400.Visible = false;
            PlantGV1.Visible = false;
            PlantGV2.Visible = false;
            BDCur1.Visible = false;
            BDCur2.Visible = false;
            L1.Visible = false;
            //L1.Text = "SubStation: ";
            L5.Visible = false;
            //L5.Text = "SubStation: ";
            L9.Visible = false;
            //L9.Text = "SubStation: ";
            L13.Visible = false;
            //L13.Text = "SubStation: ";
            L17.Visible = false;
            //L17.Text = "SubStation: ";
            BDCur1.Visible = false;
            BDCur2.Visible = false;
            MRGB.Visible = false;
            //Point pnt1 = new Point(30, 1);
            //MRLabel1.Location = pnt1;
            //MRLabel2.Location = pnt1;
            //MRLabel1.Text = "ON UNITS";
            //MRLabel2.Text = "POWER";
            //GDDeleteBtn.Text = "Delete Unit";
            GDNewBtn.Enabled = false;
            //GDNewBtn.Text = "Add Unit";
            //??????????????????age faghat neveshtan dar DB dashte bashe doroste???????????
            FRUpdateBtn.Enabled = false;
            FROKBtn.Visible = false;
            FRPlot.Visible = false;
            btnfrprint.Visible = false;
            btnfinanceexcel.Visible = false;
            FRRun.Visible = false;


            ODSaveBtn.Visible = false;
            faDatePickDispatch.Visible = false;
            dataGridDispatch.Visible = false;

            rbestimateplant.Visible = false;
            rbrealplant.Visible = false;
            GDPostpanel.Visible = false;
            panel94.Visible = false;

            mrbtnpprint.Visible = false;

            rdbam005ba.Visible = true;
            rdm005.Visible = true;
        }
        //--------------SetHeader2SubStation---------------
        private void SetHeader2SubStation()
        {
            GDPostpanel.Visible = true;
        }
        //------------------post------------------------------------------------
        private void buildPostTree(string Num)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            TreeNode MyNode, ChildNode, CNode;
            treeView2.Nodes.Clear();
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT VoltageLevel,VoltageNumber FROM PostVoltage WHERE PostNumber=@Num ORDER BY VoltageLevel asc", myConnection);
            Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.Int);
            Myda.SelectCommand.Parameters["@Num"].Value = Num;
            Myda.Fill(MyDS, "post");

            TreeNode myNode = new TreeNode();
            myNode.Text = Num;
            treeView2.Nodes.Add(myNode);

            foreach (DataRow MyRow in MyDS.Tables["post"].Rows)
            {
                TreeNode childNode = new TreeNode();
                childNode.Text = MyRow[0].ToString().Trim();
                myNode.Nodes.Add(childNode);

            }

            MyDS.Dispose();
            Myda.Dispose();
            myConnection.Close();
            treeView2.ExpandAll();
        }
        private void FillPostVoltage(string PostNumber, string VoltageLevel)
        {

            txtaddvoltagenum.Text = "";
            txtaddpostnum.Text = "";
            txtpostname.Text = "";
            txtaddvoltagenum.Enabled = true;
            radioAddpost.Checked = false;
            radioEditPost.Checked = false;


            DataTable volNum = utilities.returntbl("Select VoltageNumber From PostVoltage Where VoltageLevel='" + VoltageLevel + "' and PostNumber='" + PostNumber + "'");
            VolNumLb.Text = volNum.Rows[0][0].ToString().Trim();
            postnumlb.Text = PostNumber.Trim();
            voltagelevellb.Text = VoltageLevel;

            //from and to
            FromListBox.Items.Clear();
            ToListBox.Items.Clear();
            //////DataTable fromTb = utilities.returntbl("SELECT distinct FromPost  From FromToPost Where VoltageNumber='" + VolNumLb.Text + "'");
            //////foreach (DataRow MyRow in fromTb.Rows)
            //////{
            //////    FromListBox.Items.Add(MyRow[0].ToString().Trim());
            //////}
            //////DataTable ToTb = utilities.returntbl("SELECT distinct ToPost From FromToPost Where VoltageNumber='" + VolNumLb.Text + "'");
            //////foreach (DataRow MyRow in ToTb.Rows)
            //////{
            //////    ToListBox.Items.Add(MyRow[0].ToString().Trim());
            //////}

            DataTable fromTb = utilities.returntbl("select LineCode from dbo.Lines where frombus='" + VolNumLb.Text.Trim() + "'");
            foreach (DataRow MyRow in fromTb.Rows)
            {
                FromListBox.Items.Add(MyRow[0].ToString().Trim());
            }

            DataTable ToTb = utilities.returntbl("select LineCode from dbo.Lines where Tobus='" + VolNumLb.Text.Trim() + "'");
            foreach (DataRow MyRow in ToTb.Rows)
            {
                ToListBox.Items.Add(MyRow[0].ToString().Trim());
            }

            AddPowerList.Items.Clear();

            DataTable ss = utilities.returntbl("select distinct ppcode from dbo.RegionNetComp where code='"+gencode+"'");
           
            string x = "";
            foreach (DataRow m in ss.Rows)
            {
                if (Findpackplant(m[0].ToString().Trim()))
                {
                    DataTable oDat = utilities.returntbl("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + m[0].ToString().Trim() + "'");
                    for (int i = 0; i < oDat.Rows.Count; i++)
                    {
                        x += m[0].ToString().Trim() + "-" + oDat.Rows[i][0].ToString().Substring(0,1).Trim() + ",";

                    }
                }
                else
                {
                    x += m[0].ToString().Trim() + ",";
                }
            }
            string[] name = x.Split(',');

            //name[0] = "203";
            //name[1] = "206-G";
            //name[2] = "206-S";
            //name[3] = "232";

            //DataTable PowerPost = utilities.returntbl("select distinct PPCode from dbo.RegionNetComp where Code='R03' and PPCode Not in (Select PowerSerial From PowerPost where VoltageNumber='" + VolNumLb.Text + "')order by PPCode asc");
            //foreach (DataRow MyRow in PowerPost.Rows)
            //{


            //    AddPowerList.Items.Add(MyRow[0].ToString().Trim());

            //}

            DataTable PowerPost = utilities.returntbl("Select PowerSerial From PowerPost where VoltageNumber='" + VolNumLb.Text + "'");
            for (int i = 0; i < name.Length-1; i++)
            {
                if (PowerPost.Rows.Count > 0)
                {
                    bool enter = true;
                    foreach (DataRow MyRow in PowerPost.Rows)
                    {

                        if (name[i] == MyRow[0].ToString().Trim())
                        {
                            enter = false;
                            break;
                        }

                    }
                    if (enter)
                        AddPowerList.Items.Add(name[i]);
                }
                else
                    AddPowerList.Items.Add(name[i]);
            }
            DeletePowerList.Items.Clear();
            DataTable availablePower = utilities.returntbl("Select PowerSerial From PowerPost where VoltageNumber='" + VolNumLb.Text + "'order by PowerSerial asc");
            foreach (DataRow MyRow in availablePower.Rows)
            {
                DeletePowerList.Items.Add(MyRow[0].ToString().Trim());
            }
            DeleteConsumeList.Items.Clear();
            DataTable availableConsume = utilities.returntbl("Select ConsumedSerial From ConsumedPost where VoltageNumber='" + VolNumLb.Text + "'order by ConsumedSerial asc");
            foreach (DataRow MyRow in availableConsume.Rows)
            {
                DeleteConsumeList.Items.Add(MyRow[0].ToString().Trim());
            }
        }

        //----------------------------------------treeView1_NodeMouseClick-------------------------------

        void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            ///////////////////////////////if genco is null at first time//////////////////////////////////////////////
            ///////////////////////////////////if changed///////////////////////////////////////////////
            DataTable dt1 = utilities.returntbl("select * from BaseGencoInfo");
            if (dt1.Rows.Count > 0)
            {
                GenCo = dt1.Rows[0]["GencoNameEnglish"].ToString().Substring(0, 1).Trim().ToUpper() + "rec";
                GenName = dt1.Rows[0]["GencoNameEnglish"].ToString().Trim();
                gencode = dt1.Rows[0]["GencoCode"].ToString().Trim();
            }
            lblGencoValue.Text = GenName;
          
            ////////////////////////////////////////////////////////////////////////////////

            TreeView1IsSelected = true;
            if (MainTabs.SelectedTab.Name == "BiddingStrategy")
            {
                MainTabs_SelectedIndexChanged(sender, e);
            }

            ////////filled by bid data////////////////////////////////////////////
            label113.Text = "";
            ////////////////////filled by operational data///////////////////////////////////////
            label114.Text = "";
            /////////////////////////////////////////////////////////////////////////////


            //Clear Grids
            PlantGV1.DataSource = null;
            if (PlantGV1.Rows != null) PlantGV1.Rows.Clear();
            PlantGV2.DataSource = null;
            if (PlantGV2.Rows != null) PlantGV2.Rows.Clear();
            //GeneralDataHeaderPanel.Visible = false;
            string PPName = "";
            if (e.Node.Parent == null || e.Node.Text == "Plant")
            {
                lblPlantValue.Text = "All";
                lblMaintPlant.Text = "All";
                L9.Text = "All Plants";
                if (L9.Text == "All Plants")
                {
                    rbestimateplant.Visible = false;
                    rbrealplant.Visible = false;
                    MRHeaderPanel.Visible = false;
                   
                }

            }
            if (e.Node.Parent != null) //be sure the root isnot selected!
            {
                if (e.Node.Parent.Text == "Transmission")
                {
                    SetHeader1Transmission();
                    string TransType = e.Node.Text;
                    TransType = TransType.Trim();
                    MRPlantLb.Text = TransType + " (KV)";
                    BDPlantLb.Text = TransType + " (KV)";
                    FRPlantLb.Text = TransType + " (KV)";
                    ODPlantLb.Text = TransType + " (KV)";
                    buildTRANStree(TransType);
                    FillTransmissionGrid("230");
                    FillTransmissionGrid("400");
                    line = int.Parse(TransType);
                    //TAB : Market Result
                    FillMRTransmission(TransType);
                }
                else if (e.Node.Parent.Text == "Plant")
                {
                    SetHeader1Plant();
                    PPName = e.Node.Text;
                    PPName = PPName.Trim();
                    //Detect PPID
                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Connection = new SqlConnection(ConnectionManager.ConnectionString);
                    MyCom.Connection.Open();
                    MyCom.CommandText = "SELECT @num=PPID FROM PowerPlant WHERE PPName=@name";
                    MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                    MyCom.Parameters.Add("@name", SqlDbType.NChar, 20);
                    MyCom.Parameters["@num"].Direction = ParameterDirection.Output;
                    MyCom.Parameters["@name"].Value = PPName;
                    MyCom.ExecuteNonQuery();
                    string id = MyCom.Parameters["@num"].Value.ToString().Trim();
                    MyCom.Connection.Close();
                    buildPPtree(id);
                    PPID = id;
                    GDPlantLb.Text = PPName;
                    ODPlantLb.Text = PPName;
                    MRPlantLb.Text = PPName;
                    BDPlantLb.Text = PPName;
                    FRPlantLb.Text = PPName;
                    lblMaintPlant.Text = PPName.Trim();
                    lblPlantValue.Text = PPName.Trim();
                    lbplantmarket.Text = PPID;
                    //TAB : GENERAL DATA
                    FillPlantGrid(id);
                    FillODPlant(id,"");
                    //TAB: OPERATIONAL DATA
                    //FillODValues(id);
                    //TAB :MARKETRESULTS
                    FillMRVlues(id);
                    //TAB : FINANCIAL REPORT
                    FillFRValues(id);
                }
                else if (e.Node.Parent.Text == "SubStation")
                {
                    SetHeader1Substation();
                    string PostType = e.Node.Text;
                    PostType = PostType.Trim();
                    buildPostTree(PostType);
                }
                else treeView2.Nodes.Clear();
            }
            if ((e.Node.Parent == null) || (e.Node.Parent.Text == GenCo))
            {
                treeView2.Nodes.Clear();
                ResetTabs();
            }
        }
        //-----------------------------------------treeView2_NodeMouseClick-----------------------------------
        private void treeView2_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {

            ///////////////////////////////////if changed///////////////////////////////////////////////
            DataTable dt1 = utilities.returntbl("select * from BaseGencoInfo");
            if (dt1.Rows.Count > 0)
            {
                GenCo = dt1.Rows[0]["GencoNameEnglish"].ToString().Substring(0, 1).Trim().ToUpper() + "rec";
                GenName = dt1.Rows[0]["GencoNameEnglish"].ToString().Trim();
                gencode = dt1.Rows[0]["GencoCode"].ToString().Trim();
            }
            lblGencoValue.Text = GenName;

            ////////////////////////////////////////////////////////////////////////////////

            ////////////////////filled by operational data///////////////////////////////////////
            label114.Text = "";
            /////////////////////////////////////////////////////////////////////////////


            if (e.Node.Parent != null) //be sure the root isnot selected!
            {
                string parent = e.Node.Parent.Text;
                //if a unit of plants is selected
                if ((parent.Contains("Steam")) || (parent.Contains("Gas")) || (parent.Contains("Combined")))
                {
                    string unit = e.Node.Text;
                    unit = unit.Trim();
                    string package = e.Node.Parent.Text;
                    package = package.Trim();
                    string type = unit.ToLower();
                    if (type.Contains("steam")) type = "Steam";
                    else type = "Gas";

                    //Get Index Of Selected Unit(Row) in GridView (In Generad data Tab)
                    int index = 0;
                    if (GDSteamGroup.Text.Contains(package))
                        for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                        {
                            if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                                index = i;
                        }
                    else
                        for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                        {
                            if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                                index = i;
                        }

                    //set HeaderPanel of Tabs
                    SetHeader2Plant(package, unit, type);

                    //TAB:GENERAL DATA
                    // set values of TextBoxes
                    FillGDUnit(unit, package, index);

                    //TAB:Operational DATA
                    // set values of TextBoxes
                    FillODUnit(unit, package, index);

                    //TAB : MARKET RESULTS
                    //Set TextBoxes And Grids
                    FillMRUnit(unit, package, index);

                    //TAB : BID DATA
                    //SET TextBoxes AND DataGrid
                    FillBDUnit(unit, package, index);

                    //TAB : FINANCIAL REPORT
                    //SET TextBoxes
                    FillFRUnit(unit, package, index);

                }

                //if a line of Transmission is selected
                else if (e.Node.Parent.Parent != null)
                {
                    SetHeader2Transmission();
                    string TransLine = e.Node.Text.Trim();
                    string TransType = e.Node.Parent.Parent.Text.Trim();
                    GDPlantLb.Text = TransLine;
                    ODPlantLb.Text = TransLine;
                    MRPlantLb.Text = TransLine;
                    BDPlantLb.Text = TransLine;
                    FRPlantLb.Text = TransLine;
                    FillGDLine(TransType, TransLine);
                    FillMRLine(TransType, TransLine);
                }
                //if a child of substation is selected
                else if (e.Node.Nodes.Count == 0)
                {
                    //jalal...
                    string VoltageLevel = e.Node.Text;
                    //230...
                    string PostNumber = e.Node.Parent.Text;
                    SetHeader2SubStation();
                    FillPostVoltage(PostNumber, VoltageLevel);
                }
            }
            else
            {
                if (e.Node.Text.Contains("Combined Cycle"))
                {

                    FillMRCombine(e.Node.Text.Trim(), lblPlantValue.Text.Trim());
                    FillODUnitCombine(e.Node.Text.Trim());
                }

            }
        }
        //----------------------------------PlantGV1_ColumnHeaderMouseClick-------------------------------
        private void PlantGV1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Detect Farsi Date
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");
            int myhour = DateTime.Now.Hour;
            string type = GDGasGroup.Text;
            type = type.Trim();
            if (type == "Combined Cycle") type = "CC";
            FillPlantGV1Remained(mydate, PPID, type, myhour);

        }
        //----------------------------PlantGV2_ColumnHeaderMouseClick-----------------------------------------
        private void PlantGV2_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Detect Farsi Date
            PersianDate prDate = PersianDateConverter.ToPersianDate(DateTime.Now);
            string mydate = prDate.ToString("d");
            int myhour = DateTime.Now.Hour;
            string type = GDSteamGroup.Text;
            type = type.Trim();
            if (type == "Combined Cycle") type = "CC";
            FillPlantGV2Remained(mydate, PPID, type, myhour);
        }
        //---------------------------------CheckValidated----------------------------------------
        private bool CheckValidated(string text, int type)
        {
            if (text != "")
            {
                if (type == 1)
                {
                    try
                    {
                        double k = double.Parse(text);
                        return true;
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
                else
                {
                    try
                    {
                        int k = int.Parse(text);
                        return true;
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
            }
            else return true;
        }
        //-----------------------------ODUnitFuelTB_Validated----------------------------------
        private void ODUnitFuelTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(ODUnitFuelTB.Text, 1))
                errorProvider1.SetError(ODUnitFuelTB, "");
            else if (ODUnitFuelCheck.Checked)
                errorProvider1.SetError(ODUnitFuelTB, "just real number!");
         
        }
        //-------------------------------ODUnitPowerGrid1_Validated-------------------------------------
        private void ODUnitPowerGrid1_Validated(object sender, EventArgs e)
        {
            errorProvider1.SetError(ODUnitPowerGrid1, "");
            if (ODUnitPowerCheck.Checked)
            {
                DataTable P = utilities.returntbl("select PMin,PMax FROM UnitsDataMain WHERE PPID='"+PPID+"' AND UnitCode='"+ODUnitLb.Text.Trim()+"'");
                for (int i = 0; i < (ODUnitPowerGrid1.ColumnCount - 1); i++)
                {
                    if (ODUnitPowerGrid1.Rows[0].Cells[i].Value != null)
                    {
                        if (CheckValidated(ODUnitPowerGrid1.Rows[0].Cells[i].Value.ToString(), 1))
                        { 
                            double value=double.Parse(ODUnitPowerGrid1.Rows[0].Cells[i].Value.ToString());
                        if ((value<double.Parse(P.Rows[0][0].ToString()))||(value>((1.1)*double.Parse(P.Rows[0][1].ToString()))))
                            if (value!=0)
                                errorProvider1.SetError(ODUnitPowerGrid1, "Values Must be between Pmin and Pmax!");
                        }
                        else errorProvider1.SetError(ODUnitPowerGrid1, "Values Must be between Pmin and Pmax!");    
                    }
                }
            }
        }
        //-------------------------------ODUnitPowerGrid2_Validated-------------------------------
        private void ODUnitPowerGrid2_Validated(object sender, EventArgs e)
        {
            errorProvider1.SetError(ODUnitPowerGrid2, "");
            if (ODUnitPowerCheck.Checked)
            {
                DataTable P = utilities.returntbl("select PMin,PMax FROM UnitsDataMain WHERE PPID='" + PPID + "' AND UnitCode='" + ODUnitLb.Text.Trim() + "'");
                for (int i = 0; i < (ODUnitPowerGrid2.ColumnCount - 1); i++)
                {
                    if (ODUnitPowerGrid2.Rows[0].Cells[i].Value != null)
                    {
                        if (CheckValidated(ODUnitPowerGrid2.Rows[0].Cells[i].Value.ToString(), 1))
                        {
                            double value = double.Parse(ODUnitPowerGrid2.Rows[0].Cells[i].Value.ToString());
                            if ((value < double.Parse(P.Rows[0][0].ToString())) || (value > ((1.1) * double.Parse(P.Rows[0][1].ToString()))))
                                if (value != 0)
                                    errorProvider1.SetError(ODUnitPowerGrid2, "Values Must be between Pmin and Pmax!");
                        }
                        else errorProvider1.SetError(ODUnitPowerGrid2, "Values Must be between Pmin and Pmax!");
                    }
                }
            }

        }
        //---------------------------ODUnitPowerCheck_CheckedChanged--------------------------------------
        private void ODUnitPowerCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (ODUnitPowerCheck.Checked)
            {
                ODUnitPowerStartDate.Enabled = true;
               // ODUnitPowerEndDate.Enabled = true;
                ODUnitPowerGrid1.Enabled = true;
                ODUnitPowerGrid2.Enabled = true;
            }
            else
            {
                ODUnitPowerStartDate.Enabled = false;
                //ODUnitPowerEndDate.Enabled = false;
                ODUnitPowerGrid1.Enabled = false;
                ODUnitPowerGrid2.Enabled = false;
            }
        }
        //----------------------------------ODUnitOutCheck_CheckedChanged------------------------------
        private void ODUnitOutCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (ODUnitOutCheck.Checked)
            {
                ODUnitServiceStartDate.Enabled = true;
                ODUnitServiceEndDate.Enabled = true;
                ODOutServiceStartHour.Enabled = true;
                ODOutServiceEndHour.Enabled = true;
            }
            else
            {
                ODUnitServiceStartDate.Enabled = false;
                ODUnitServiceEndDate.Enabled = false;
                ODOutServiceStartHour.Enabled = false;
                ODOutServiceEndHour.Enabled = false;
            }
        }
        //-----------------------------ODUnitMainCheck_CheckedChanged-------------------------------
        private void ODUnitMainCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (ODUnitMainCheck.Checked)
            {
                ODUnitMainStartDate.Enabled = true;
                ODUnitMainEndDate.Enabled = true;
                ODMaintenanceStartHour.Enabled = true;
                ODMaintenanceEndHour.Enabled = true;
                ODMaintenanceType.Enabled = true;
            }
            else
            {
                ODUnitMainStartDate.Enabled = false;
                ODUnitMainEndDate.Enabled = false;
                ODMaintenanceEndHour.Enabled = false;
                ODMaintenanceStartHour.Enabled = false;
                ODMaintenanceType.Enabled = false;
            }
        }
        //--------------------------ODUnitFuelCheck_CheckedChanged--------------------------------
        private void ODUnitFuelCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (ODUnitFuelCheck.Checked)
            {
                ODUnitFuelStartDate.Enabled = true;
                ODUnitFuelEndDate.Enabled = true;
                ODSecondFuelEndHour.Enabled = true;
                ODSecondFuelStartHour.Enabled = true;
                ODUnitFuelTB.Enabled = true;
                textSECONDFUELPERCENT.Enabled = true;
            }
            else
            {
                ODUnitFuelStartDate.Enabled = false;
                ODUnitFuelEndDate.Enabled = false;
                ODSecondFuelStartHour.Enabled = false;
                ODSecondFuelEndHour.Enabled = false;
                ODUnitFuelTB.Enabled = false;
                textSECONDFUELPERCENT.Enabled = false;
            }
        }
        //--------------------------FRPlantBenefitTB_Validated--------------------------------
        private void FRPlantBenefitTB_Validated(object sender, EventArgs e)
        {

        }
        //------------------------------FRPlantIncomeTb_Validated------------------------------
        private void FRPlantIncomeTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantIncomeTb.Text, 1))
                errorProvider1.SetError(FRPlantIncomeTb, "");
            else errorProvider1.SetError(FRPlantIncomeTb, "just real number!");
        }
        //-------------------------------FRPlantCostTb_Validated---------------------------------
        private void FRPlantCostTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantCostTb.Text, 1))
                errorProvider1.SetError(FRPlantCostTb, "");
            else errorProvider1.SetError(FRPlantCostTb, "just real number!");
        }
        //---------------------------------FRPlantAvaCapTb_Validated----------------------------------
        private void FRPlantAvaCapTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantAvaCapTb.Text, 1))
                errorProvider1.SetError(FRPlantAvaCapTb, "");
            else errorProvider1.SetError(FRPlantAvaCapTb, "just real number!");
        }
        //---------------------------------FRPlantTotalPowerTb_Validated---------------------------------
        private void FRPlantTotalPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantTotalPowerTb.Text, 1))
                errorProvider1.SetError(FRPlantTotalPowerTb, "");
            else errorProvider1.SetError(FRPlantTotalPowerTb, "just real number!");
        }
        //------------------------------------FRPlantBidPowerTb_Validated-------------------------------------
        private void FRPlantBidPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantBidPowerTb.Text, 1))
                errorProvider1.SetError(FRPlantBidPowerTb, "");
            else errorProvider1.SetError(FRPlantBidPowerTb, "just real number!");
        }
        //--------------------------------------FRPlantULPowerTb_Validated--------------------------------------
        private void FRPlantULPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantULPowerTb.Text, 1))
                errorProvider1.SetError(FRPlantULPowerTb, "");
            else errorProvider1.SetError(FRPlantULPowerTb, "just real number!");
        }
        //---------------------------------------FRPlantIncPowerTb_Validated---------------------------------
        private void FRPlantIncPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantIncPowerTb.Text, 1))
                errorProvider1.SetError(FRPlantIncPowerTb, "");
            else errorProvider1.SetError(FRPlantIncPowerTb, "just real number!");
        }
        //---------------------------------------FRPlantDecPowerTb_Validated------------------------------------
        private void FRPlantDecPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantDecPowerTb.Text, 1))
                errorProvider1.SetError(FRPlantDecPowerTb, "");
            else errorProvider1.SetError(FRPlantDecPowerTb, "just real number!");
        }
        //----------------------------------------FRPlantCapPayTb_Validated-----------------------------------
        private void FRPlantCapPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantCapPayTb.Text, 1))
                errorProvider1.SetError(FRPlantCapPayTb, "");
            else errorProvider1.SetError(FRPlantCapPayTb, "just real number!");
        }
        //---------------------------------------FRPlantEnergyPayTb_Validated-------------------------------------
        private void FRPlantEnergyPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantEnergyPayTb.Text, 1))
                errorProvider1.SetError(FRPlantEnergyPayTb, "");
            else errorProvider1.SetError(FRPlantEnergyPayTb, "just real number!");
        }
        //------------------------------------FRPlantBidPayTb_Validated--------------------------------------------
        private void FRPlantBidPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantBidPayTb.Text, 1))
                errorProvider1.SetError(FRPlantBidPayTb, "");
            else errorProvider1.SetError(FRPlantBidPayTb, "just real number!");
        }
        //------------------------------------FRPlantULPayTb_Validated----------------------------------------
        private void FRPlantULPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantULPayTb.Text, 1))
                errorProvider1.SetError(FRPlantULPayTb, "");
            else errorProvider1.SetError(FRPlantULPayTb, "just real number!");
        }
        //------------------------------------FRPlantIncPayTb_Validated-----------------------------------
        private void FRPlantIncPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantIncPayTb.Text, 1))
                errorProvider1.SetError(FRPlantIncPayTb, "");
            else errorProvider1.SetError(FRPlantIncPayTb, "just real number!");
        }
        //-----------------------------------FRPlantDecPayTb_Validated----------------------------------------
        private void FRPlantDecPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRPlantDecPayTb.Text, 1))
                errorProvider1.SetError(FRPlantDecPayTb, "");
            else errorProvider1.SetError(FRPlantDecPayTb, "just real number!");
        }
        //-----------------------------------RUnitCapacityTb_Validated----------------------------------------
        private void FRUnitCapacityTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitCapacityTb.Text, 1))
                errorProvider1.SetError(FRUnitCapacityTb, "");
            else errorProvider1.SetError(FRUnitCapacityTb, "just real number!");
        }
        //------------------------------------FRUnitTotalPowerTb_Validated-------------------------------------
        private void FRUnitTotalPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitTotalPowerTb.Text, 1))
                errorProvider1.SetError(FRUnitTotalPowerTb, "");
            else errorProvider1.SetError(FRUnitTotalPowerTb, "just real number!");
        }
        //-----------------------------------FRUnitULPowerTb_Validated-------------------------------------------
        private void FRUnitULPowerTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitULPowerTb.Text, 1))
                errorProvider1.SetError(FRUnitULPowerTb, "");
            else errorProvider1.SetError(FRUnitULPowerTb, "just real number!");
        }
        //-------------------------------------FRUnitCapPayTb_Validated--------------------------------------------
        private void FRUnitCapPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitCapPayTb.Text, 1))
                errorProvider1.SetError(FRUnitCapPayTb, "");
            else errorProvider1.SetError(FRUnitCapPayTb, "just real number!");
        }
        //--------------------------------------FRUnitEneryPayTb_Validated-----------------------------------------
        private void FRUnitEneryPayTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitEneryPayTb.Text, 1))
                errorProvider1.SetError(FRUnitEneryPayTb, "");
            else errorProvider1.SetError(FRUnitEneryPayTb, "just real number!");
        }
        //---------------------------------------FRUnitIncomeTb_Validated------------------------------------------
        private void FRUnitIncomeTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitIncomeTb.Text, 1))
                errorProvider1.SetError(FRUnitIncomeTb, "");
            else errorProvider1.SetError(FRUnitIncomeTb, "just real number!");
        }

//---------------------------------DisableUnitTab---------------------------
        private void DisableUnitTab(bool enableStatues, string unitType)
        {

            GDNewBtn.Enabled = !enableStatues;
            GDcapacityTB.ReadOnly = enableStatues;
            GDPmaxTB.ReadOnly = enableStatues;
            GDPminTB.ReadOnly = enableStatues;
            GDTimeUpTB.ReadOnly = enableStatues;
            GDTimeDownTB.ReadOnly = enableStatues;
            GDTimeColdStartTB.ReadOnly = enableStatues;
            GDTimeHotStartTB.ReadOnly = enableStatues;
            GDRampRateTB.ReadOnly = enableStatues;
            GDPowerSerialTb.ReadOnly = enableStatues;
            GDpowertransserialtb.ReadOnly = enableStatues;
            GDConsumedSerialTb.ReadOnly = enableStatues;
            GDtxtAvc.ReadOnly = enableStatues;

            txtMaintHGPDur.ReadOnly = enableStatues;
            txtMaintHGPHours.ReadOnly = enableStatues;
            if (unitType == "Steam")
            {
                txtMaintHGPDur.ReadOnly = true;
                txtMaintHGPHours.ReadOnly = true;
            }

            txtDurabilityHours.ReadOnly = enableStatues;
            txtMaintCIDur.ReadOnly = enableStatues;
            txtMaintCIHours.ReadOnly = enableStatues;
            txtMaintMODur.ReadOnly = enableStatues;
            txtMaintMOHours.ReadOnly = enableStatues;
            datePickerDurability.Readonly = enableStatues;
            datePickerDurability.Enabled = !enableStatues;

            txtMaintFuelFactor.ReadOnly = enableStatues;
            txtMaintLastHour.ReadOnly = enableStatues;
            txtMaintStartUpFactor.ReadOnly = enableStatues;
            cmbMaintLastMaintType.Enabled = !enableStatues;

        }
        //-----------------------------------addToolStripMenuItem_Click--------------------------------------------------
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //NewPlant.Show();
            //this.Hide();
            AddPlantForm newPlant = new AddPlantForm();
            DialogResult result = newPlant.ShowDialog();
            if (result == DialogResult.OK) buildTreeView1();
        }
        //-------------------------------------PlantGV1_DataError-------------------------------------------------------
        private void PlantGV1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            if ((!PlantGV1.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 1))
            {
                try
                {
                    int i = int.Parse(PlantGV1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
        }
        //------------------------------------------PlantGV2_DataError----------------------------------------
        private void PlantGV2_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            if ((!PlantGV2.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 1))
            {
                try
                {
                    int i = int.Parse(PlantGV2.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
        }

        //-----------------------------------GDcapacityTB_Validated------------------------------------
        private void GDcapacityTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(GDcapacityTB.Text, 1))
                errorProvider1.SetError(GDcapacityTB, "");
            else errorProvider1.SetError(GDcapacityTB, "just real number!");

        }
       

        //------------------------------------GDPmaxTB_Validated------------------------------------
        private void GDPmaxTB_Validated(object sender, EventArgs e)
        {
            if (Currentgb.Text.Contains("STATE"))
            {
                if (CheckValidated(GDPmaxTB.Text, 1))
                    errorProvider1.SetError(GDPmaxTB, "");
                else errorProvider1.SetError(GDPmaxTB, "just real number!");
            }
            //if (Currentgb.Text.Contains("OWNER"))
            //{
            //    if (GDPmaxTB.Text != "")
            //        errorProvider1.SetError(GDPmaxTB, "");
            //    else errorProvider1.SetError(GDPmaxTB, "string");

            //}
            //else
            //{
            //    if (CheckValidated(GDPmaxTB.Text, 0))
            //        errorProvider1.SetError(GDPmaxTB, "");
            //    else errorProvider1.SetError(GDPmaxTB, "just Integer!");
            //}
            if (Currentgb.Text.Contains("OWNER"))
            {
                try
                {
                    if (GDPmaxTB.Text != "")
                    {
                        try
                        {
                            int i = int.Parse(GDPmaxTB.Text);
                        }
                        catch
                        {
                            string f = (string)GDPmaxTB.Text;
                        }


                    }
                    errorProvider1.SetError(GDPmaxTB, "");
                }
                catch
                {
                    errorProvider1.SetError(GDPmaxTB, "just Integer or string!");
                }
            }
        }
        //-----------------------------------GDPminTB_Validated--------------------------------------
        private void GDPminTB_Validated(object sender, EventArgs e)
        {
            if (Currentgb.Text.Contains("STATE"))
            {
                if (CheckValidated(GDPminTB.Text, 1))
                    errorProvider1.SetError(GDPminTB, "");
                else errorProvider1.SetError(GDPminTB, "just real number!");
            }
            //if (Currentgb.Text.Contains("OWNER"))
            //{
            //    if (GDPmaxTB.Text != "")
            //        errorProvider1.SetError(GDPmaxTB, "");
            //    else errorProvider1.SetError(GDPmaxTB, "string");

            //}
            //else
            //{
            //    if (CheckValidated(GDPmaxTB.Text, 0))
            //        errorProvider1.SetError(GDPmaxTB, "");
            //    else errorProvider1.SetError(GDPmaxTB, "just Integer!");
            //}
            if (Currentgb.Text.Contains("OWNER"))
            {
                try
                {
                    if (GDPminTB.Text != "")
                    {
                        try
                        {
                            int i = int.Parse(GDPminTB.Text);
                        }
                        catch
                        {
                            string f = (string)GDPminTB.Text;
                        }


                    }
                    errorProvider1.SetError(GDPminTB, "");
                }
                catch
                {
                    errorProvider1.SetError(GDPminTB, "just Integer or string!");
                }
            }
            //if (CheckValidated(GDPminTB.Text, 0))
            //    errorProvider1.SetError(GDPminTB, "");
            //else errorProvider1.SetError(GDPminTB, "just integer!");
        }
        //-----------------------------------GDTimeUpTB_Validated--------------------------------
        private void GDTimeUpTB_Validated(object sender, EventArgs e)
        {
            if (Currentgb.Text.Contains("STATE"))
            {
                if (CheckValidated(GDTimeUpTB.Text, 0))
                    errorProvider1.SetError(GDTimeUpTB, "");
                else errorProvider1.SetError(GDTimeUpTB, "just integer!");
            }
        }
        //--------------------------------------
        private void GDtxtAvc_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(GDtxtAvc.Text, 1))
                errorProvider1.SetError(GDtxtAvc, "");
            else errorProvider1.SetError(GDtxtAvc, "just real number!");
        }
        
        //--------------------------------GDTimeDownTB_Validated---------------------------------------
        private void GDTimeDownTB_Validated(object sender, EventArgs e)
        {
            if (Currentgb.Text.Contains("STATE"))
            {
                if (CheckValidated(GDTimeDownTB.Text, 0))
                    errorProvider1.SetError(GDTimeDownTB, "");
                else errorProvider1.SetError(GDTimeDownTB, "just integer!");
            }
            else
            {
                if (CheckValidated(GDTimeDownTB.Text, 1))
                    errorProvider1.SetError(GDTimeDownTB, "");
                else errorProvider1.SetError(GDTimeDownTB, "just real number!");
            }
        }
        //----------------------------------GDTimeColdStartTB_Validated-------------------------------------
        private void GDTimeColdStartTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(GDTimeColdStartTB.Text, 1))
                errorProvider1.SetError(GDTimeColdStartTB, "");
            else errorProvider1.SetError(GDTimeColdStartTB, "just real number!");
        }
        //--------------------------------GDTimeHotStartTB_Validated---------------------------------------
        private void GDTimeHotStartTB_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(GDTimeHotStartTB.Text, 1))
                errorProvider1.SetError(GDTimeHotStartTB, "");
            else errorProvider1.SetError(GDTimeHotStartTB, "just real number!");
        }
        //-------------------------------GDRampRateTB_Validated-------------------------------------
        private void GDRampRateTB_Validated(object sender, EventArgs e)
        {
            if (Currentgb.Text.Contains("STATE"))
            {
                if (CheckValidated(GDRampRateTB.Text, 0))
                    errorProvider1.SetError(GDRampRateTB, "");
                else errorProvider1.SetError(GDRampRateTB, "just integer!");
            }
            else
            {
                if (CheckValidated(GDRampRateTB.Text, 1))
                    errorProvider1.SetError(GDRampRateTB, "");
                else errorProvider1.SetError(GDRampRateTB, "just real number!");
            }
        }
        //----------------------------------FRUnitFixedTb_Validated-------------------------------------
        private void FRUnitFixedTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitFixedTb.Text, 1))
                errorProvider1.SetError(FRUnitFixedTb, "");
            else errorProvider1.SetError(FRUnitFixedTb, "just real number!");
        }
        //-----------------------------------FRUnitVariableTb_Validated----------------------------------
        private void FRUnitVariableTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitVariableTb.Text, 1))
                errorProvider1.SetError(FRUnitVariableTb, "");
            else errorProvider1.SetError(FRUnitVariableTb, "just real number!");
        }
        //----------------------------------FRUnitAmargTb1_Validated------------------------------------
        private void FRUnitAmargTb1_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitAmargTb1.Text, 1))
                errorProvider1.SetError(FRUnitAmargTb1, "");
            else errorProvider1.SetError(FRUnitAmargTb1, "just real number!");
        }
        //----------------------------------FRUnitBmargTb1_Validated--------------------------------------
        private void FRUnitBmargTb1_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitBmargTb1.Text, 1))
                errorProvider1.SetError(FRUnitBmargTb1, "");
            else errorProvider1.SetError(FRUnitBmargTb1, "just real number!");
        }
        //-------------------------------------FRUnitCmargTb1_Validated-------------------------------------
        private void FRUnitCmargTb1_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitCmargTb1.Text, 1))
                errorProvider1.SetError(FRUnitCmargTb1, "");
            else errorProvider1.SetError(FRUnitCmargTb1, "just real number!");
        }
        //---------------------------------------FRUnitAmargTb2_Validated------------------------------------
        private void FRUnitAmargTb2_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitAmargTb2.Text, 1))
                errorProvider1.SetError(FRUnitAmargTb2, "");
            else errorProvider1.SetError(FRUnitAmargTb2, "just real number!");
        }
        //--------------------------------------FRUnitBmargTb2_Validated--------------------------------------
        private void FRUnitBmargTb2_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitBmargTb2.Text, 1))
                errorProvider1.SetError(FRUnitBmargTb2, "");
            else errorProvider1.SetError(FRUnitBmargTb2, "just real number!");
        }
        //--------------------------------------FRUnitCmargTb2_Validated-----------------------------------------
        private void FRUnitCmargTb2_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitCmargTb2.Text, 1))
                errorProvider1.SetError(FRUnitCmargTb2, "");
            else errorProvider1.SetError(FRUnitCmargTb2, "just real number!");
        }
        //--------------------------------------FRUnitColdTb_Validated----------------------------------------------
        private void FRUnitColdTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitColdTb.Text, 1))
                errorProvider1.SetError(FRUnitColdTb, "");
            else errorProvider1.SetError(FRUnitColdTb, "just real number!");
        }
        //-------------------------------------FRUnitHotTb_Validated--------------------------------------------
        private void FRUnitHotTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitHotTb.Text, 1))
                errorProvider1.SetError(FRUnitHotTb, "");
            else errorProvider1.SetError(FRUnitHotTb, "just real number!");
        }
       
        //-----------------------------------------Grid400_DataError---------------------------------------
        private void Grid400_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            if ((!Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 1))
            {
                try
                {
                    int i = int.Parse(Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
            if ((!Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 2))
            {
                try
                {
                    int i = int.Parse(Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
            if ((!Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 3))
            {
                try
                {
                    double i = double.Parse(Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
            if ((!Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 6))
            {
                try
                {
                    double i = double.Parse(Grid400.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
        }
        //-------------------------------------------Grid230_DataError-----------------------------------------
        private void Grid230_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
            if ((!Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 1))
            {
                try
                {
                    int i = int.Parse(Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
            if ((!Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 2))
            {
                try
                {
                    int i = int.Parse(Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Integer!");
                }
            }
            if ((!Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 3))
            {
                try
                {
                    double i = double.Parse(Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Real Number!");
                }
            }
            if ((!Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) && (e.ColumnIndex == 6))
            {
                try
                {
                    double i = double.Parse(Grid230.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch
                {
                    MessageBox.Show("You must Insert Real Number!");
                }
            }
        }
        //-------------------------------------MRPlotBtn_Click------------------------------
        private void MRPlotBtn_Click(object sender, EventArgs e)
        {
            //if (MRHeaderPanel.Visible)
            //    DrawUnitnemoodar
            //else DrawPlanNemoodar
        }

        //-----------------------------------m002ToolStripMenuItem_Click-------------------------------------
        private void m002ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExcelFormat formfile = new ExcelFormat(predismanualdate);
            DialogResult re = formfile.ShowDialog();
            if (re == DialogResult.OK)
            {
                string[] fileResult = formfile.result.Split(',');
                bool IsValid = true;
                DataTable TempTable = null;
                DialogResult re1 = openFileDialog1.ShowDialog();
                if (re1 != DialogResult.Cancel)
                {
                    string price = fileResult[0].ToString();
                    string path = openFileDialog1.FileName;                  
                    
                       
                        //read from FRM002.xls into datagridview
                        String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                        OleDbConnection objConn = new OleDbConnection(sConnectionString);
                        objConn.Open();
                      
                        OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                        objAdapter1.SelectCommand = objCmdSelect;
                        DataSet objDataset1 = new DataSet();
                        try
                        {
                            objAdapter1.Fill(objDataset1);
                            //TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                            TempTable = objDataset1.Tables[0];
                        }
                        catch (Exception ex)
                        {
                            IsValid = false;
                            throw ex;
                        }
                        objConn.Close();
                        //IS IT A Valid File?
                        //if ((IsValid) && (TempGV.Columns[1].HeaderText.Contains("M002")))
                        if ((IsValid) && (TempTable.Columns[1].ColumnName.Contains("M002")))
                        {
                            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                            myConnection.Open();

                            //Insert into DB (MainFRM002)
                            //string path =@"c:\data\" + Doc002 + ".xls";
                            Excel.Application exobj = new Excel.Application();
                            exobj.Visible = true;
                            exobj.UserControl = true;
                            Excel.Workbook book = null;
                            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                            book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                            SqlCommand MyCom = new SqlCommand();
                            MyCom.Connection = myConnection;
                            int type = 0;
                            string PID = "0";

                            string date2 = fileResult[1];
                          

                            MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                            MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                            MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
                            MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
                            MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
                            MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
                            MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
                            MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
                            MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

                            MyCom.CommandText = "INSERT INTO [MainFRM002] (PPID,TargetMarketDate,PPName,PPType,"
                            + "DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
                            foreach (Excel.Worksheet workSheet in book.Worksheets)
                                if ((workSheet.Name == price))
                                {
                                    PID = fileResult[2];

                                    ///////////////////////////////ptype///////////////////////////////////

                                    DataTable ddt = utilities.returntbl("SELECT  count(PPID) as s1  FROM [PPUnit] WHERE PPID='" + PID + "'"); 
                                    DataTable ddtc= utilities.returntbl("SELECT count(PPID) as s2 FROM [PPUnit] WHERE PPID='" + PID + "' AND PackageType LIKE 'Combined Cycle%'");
                                    int result1 =MyintParse1(ddt.Rows[0]["s1"].ToString());
                                    int result2 = MyintParse1(ddtc.Rows[0]["s2"].ToString());
                                    if ((result1 > 1) && (result2 > 0))
                                    {
                                        type = 1;
                                    }
                                    else
                                    {
                                        type = 0;
                                    }

                                    //////////////////////////////////////////////////////////////////////////
                                  
                                  
                                    MyCom.Parameters["@id"].Value = PID;
                                    MyCom.Parameters["@tdate"].Value = date2;
                                    MyCom.Parameters["@name"].Value = "";
                                    MyCom.Parameters["@type"].Value = type;
                                    if (((Excel.Range)workSheet.Cells[2, 2]).Value2 != null)
                                        MyCom.Parameters["@idate"].Value = ((Excel.Range)workSheet.Cells[2, 2]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@idate"].Value = "";
                                    if (((Excel.Range)workSheet.Cells[3, 2]).Value2 != null)
                                        MyCom.Parameters["@time"].Value = ((Excel.Range)workSheet.Cells[3, 2]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@time"].Value = "";
                                    if (((Excel.Range)workSheet.Cells[7, 2]).Value2 != null)
                                        MyCom.Parameters["@revision"].Value = ((Excel.Range)workSheet.Cells[7, 2]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@revision"].Value = 0;
                                    if (((Excel.Range)workSheet.Cells[8, 2]).Value2 != null)
                                        MyCom.Parameters["@filled"].Value = ((Excel.Range)workSheet.Cells[8, 2]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@filled"].Value = "";
                                    if (((Excel.Range)workSheet.Cells[9, 2]).Value2 != null)
                                        MyCom.Parameters["@approved"].Value = ((Excel.Range)workSheet.Cells[9, 2]).Value2.ToString().Trim();
                                    else MyCom.Parameters["@approved"].Value = "";
                                }
                            try
                            {
                                /////// delete previous records, if any:

                                SqlCommand MyComDeleteMain = new SqlCommand();
                                MyComDeleteMain.Connection = myConnection;
                                MyComDeleteMain.CommandText = "Delete From [MainFRM002] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                                MyComDeleteMain.Parameters.Add("@id", SqlDbType.NChar, 10);
                                MyComDeleteMain.Parameters.Add("@tdate", SqlDbType.Char, 10);
                                MyComDeleteMain.Parameters.Add("@type", SqlDbType.SmallInt);
                                MyComDeleteMain.Parameters["@id"].Value = PID;
                                MyComDeleteMain.Parameters["@tdate"].Value = date2;
                                MyComDeleteMain.Parameters["@type"].Value = type;
                                MyComDeleteMain.ExecuteNonQuery();
                                MyComDeleteMain.Dispose();
                                ///////////////////////////

                                /////// delete previous records, if any:

                                SqlCommand MyComDeleteBlock = new SqlCommand();
                                MyComDeleteBlock.Connection = myConnection;
                                MyComDeleteBlock.CommandText = "Delete From [BlockFRM002] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                                MyComDeleteBlock.Parameters.Add("@id", SqlDbType.NChar, 10);
                                MyComDeleteBlock.Parameters.Add("@tdate", SqlDbType.Char, 10);
                                MyComDeleteBlock.Parameters.Add("@type", SqlDbType.SmallInt);
                                MyComDeleteBlock.Parameters["@id"].Value = PID;
                                MyComDeleteBlock.Parameters["@tdate"].Value = date2;
                                MyComDeleteBlock.Parameters["@type"].Value = type;
                                MyComDeleteBlock.ExecuteNonQuery();
                                MyComDeleteBlock.Dispose();
                                ///////////////////////////

                                SqlCommand MyComDelete = new SqlCommand();
                                MyComDelete.Connection = myConnection;

                                MyComDelete.CommandText = "delete from DetailFRM002 where TargetMarketDate=@deleteDate" +
                                        " AND PPID=@ppID AND PPType=@ppType AND Estimated<>1";
                                MyComDelete.Parameters.Add("@deleteDate", SqlDbType.Char, 10);
                                MyComDelete.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                                MyComDelete.Parameters.Add("@ppType", SqlDbType.SmallInt);

                                MyComDelete.Parameters["@PPID"].Value = PID;
                                MyComDelete.Parameters["@deleteDate"].Value = date2;
                                MyComDelete.Parameters["@ppType"].Value = type;

                                MyComDelete.ExecuteNonQuery();
                                MyComDelete.Dispose();

                                /////// END delete

                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                                IsValid = false;
                            }

                            //Insert into DB (BlockFRM002)
                            int x = 10;
                            MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                            MyCom.Parameters.Add("@peak", SqlDbType.Real);
                            MyCom.Parameters.Add("@max", SqlDbType.Real);
                            string timefill = new PersianDate(DateTime.Now).ToString("d") + "|" + DateTime.Now.Hour + ":" + DateTime.Now.Minute;
                            //read directly and cell by cell
                            foreach (Excel.Worksheet workSheet in book.Worksheets)
                                if (workSheet.Name == price)
                                {
                                    while (x < TempTable.Rows.Count)
                                    {
                                        if (TempTable.Rows[x][0].ToString().Trim() != "")
                                        {
                                            MyCom.CommandText = "INSERT INTO [BlockFRM002] (PPID,TargetMarketDate,PPType,Block,"
                                            + "Peak,MaxDailyGeneration) VALUES (@id,@tdate,@type,@block,@peak,@max)";

                                            MyCom.Parameters["@id"].Value = PID;
                                            MyCom.Parameters["@tdate"].Value = date2;
                                            MyCom.Parameters["@type"].Value = type;
                                            MyCom.Parameters["@block"].Value = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString().Trim();
                                            if (((Excel.Range)workSheet.Cells[x + 2, 2]).Value2 != null)
                                                MyCom.Parameters["@peak"].Value = ((Excel.Range)workSheet.Cells[x + 2, 2]).Value2.ToString().Trim();
                                            else MyCom.Parameters["@peak"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 2, 3]).Value2 != null)
                                                MyCom.Parameters["@max"].Value = ((Excel.Range)workSheet.Cells[x + 2, 3]).Value2.ToString().Trim();
                                            else MyCom.Parameters["@max"].Value = 0;
                                            try
                                            {
                                                MyCom.ExecuteNonQuery();
                                            }
                                            catch (Exception exp)
                                            {
                                                string str = exp.Message;
                                                IsValid = false;
                                            }
                                        }
                                        x++;
                                    }
                                }
                            //Insert into DB (DetailFRM002)
                            x = 10;
                            MyCom.Parameters.Add("@deccap", SqlDbType.Real);
                            MyCom.Parameters.Add("@dispachcap", SqlDbType.Real);
                            MyCom.Parameters.Add("@price1", SqlDbType.Real);
                            MyCom.Parameters.Add("@power1", SqlDbType.Real);
                            MyCom.Parameters.Add("@price2", SqlDbType.Real);
                            MyCom.Parameters.Add("@power2", SqlDbType.Real);
                            MyCom.Parameters.Add("@price3", SqlDbType.Real);
                            MyCom.Parameters.Add("@power3", SqlDbType.Real);
                            MyCom.Parameters.Add("@price4", SqlDbType.Real);
                            MyCom.Parameters.Add("@power4", SqlDbType.Real);
                            MyCom.Parameters.Add("@price5", SqlDbType.Real);
                            MyCom.Parameters.Add("@power5", SqlDbType.Real);
                            MyCom.Parameters.Add("@price6", SqlDbType.Real);
                            MyCom.Parameters.Add("@power6", SqlDbType.Real);
                            MyCom.Parameters.Add("@price7", SqlDbType.Real);
                            MyCom.Parameters.Add("@power7", SqlDbType.Real);
                            MyCom.Parameters.Add("@price8", SqlDbType.Real);
                            MyCom.Parameters.Add("@power8", SqlDbType.Real);
                            MyCom.Parameters.Add("@price9", SqlDbType.Real);
                            MyCom.Parameters.Add("@power9", SqlDbType.Real);
                            MyCom.Parameters.Add("@price10", SqlDbType.Real);
                            MyCom.Parameters.Add("@power10", SqlDbType.Real);
                            MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);

                            string fill = User.getUser().LastName.ToString().Trim();
                            //read directly and cell by cell
                            foreach (Excel.Worksheet workSheet in book.Worksheets)
                                if (workSheet.Name == price)
                                {
                                    while (x < (TempTable.Rows.Count - 1))
                                    {
                                        if (TempTable.Rows[x][0].ToString().Trim() != "")
                                        {
                                            for (int j = 0; j < 24; j++)
                                            {
                                                MyCom.CommandText = "INSERT INTO [DetailFRM002] (TargetMarketDate,PPID,Block,PPType,Hour" +
                                                ",DeclaredCapacity,DispachableCapacity,Power1,Price1,Power2,Price2,Power3,Price3,Power4," +
                                                "Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10," +
                                                "Price10,FilledBy,TimeFilled) VALUES (@tdate,@id,@block,@type,@hour,@deccap,@dispachcap,@power1," +
                                                "@price1,@power2,@price2,@power3,@price3,@power4,@price4,@power5,@price5,@power6,@price6," +
                                                "@power7,@price7,@power8,@price8,@power9,@price9,@power10,@price10,'" + fill + "','"+timefill+"')";


                                                MyCom.Parameters["@id"].Value = PID;
                                                MyCom.Parameters["@tdate"].Value = date2;
                                                MyCom.Parameters["@block"].Value = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString().Trim();
                                                MyCom.Parameters["@type"].Value = type;
                                                MyCom.Parameters["@hour"].Value = j + 1;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2 != null)
                                                    MyCom.Parameters["@deccap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@deccap"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2 != null)
                                                    MyCom.Parameters["@dispachcap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@dispachcap"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 8]).Value2 != null)
                                                    MyCom.Parameters["@power1"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 8]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@power1"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 9]).Value2 != null)
                                                    MyCom.Parameters["@price1"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 9]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@price1"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 10]).Value2 != null)
                                                    MyCom.Parameters["@power2"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 10]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@power2"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 11]).Value2 != null)
                                                    MyCom.Parameters["@price2"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 11]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@price2"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 12]).Value2 != null)
                                                    MyCom.Parameters["@power3"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 12]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@power3"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 13]).Value2 != null)
                                                    MyCom.Parameters["@price3"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 13]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@price3"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 14]).Value2 != null)
                                                    MyCom.Parameters["@power4"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 14]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@power4"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 15]).Value2 != null)
                                                    MyCom.Parameters["@price4"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 15]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@price4"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 16]).Value2 != null)
                                                    MyCom.Parameters["@power5"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 16]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@power5"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 17]).Value2 != null)
                                                    MyCom.Parameters["@price5"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 17]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@price5"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 18]).Value2 != null)
                                                    MyCom.Parameters["@power6"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 18]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@power6"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 19]).Value2 != null)
                                                    MyCom.Parameters["@price6"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 19]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@price6"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 20]).Value2 != null)
                                                    MyCom.Parameters["@power7"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 20]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@power7"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 21]).Value2 != null)
                                                    MyCom.Parameters["@price7"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 21]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@price7"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 22]).Value2 != null)
                                                    MyCom.Parameters["@power8"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 22]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@power8"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 23]).Value2 != null)
                                                    MyCom.Parameters["@price8"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 23]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@price8"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 24]).Value2 != null)
                                                    MyCom.Parameters["@power9"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 24]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@power9"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 25]).Value2 != null)
                                                    MyCom.Parameters["@price9"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 25]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@price9"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 26]).Value2 != null)
                                                    MyCom.Parameters["@power10"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 26]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@power10"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 27]).Value2 != null)
                                                    MyCom.Parameters["@price10"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 27]).Value2.ToString().Trim();
                                                else MyCom.Parameters["@price10"].Value = 0;
                                                try
                                                {
                                                    MyCom.ExecuteNonQuery();
                                                }
                                                catch (Exception exp)
                                                {
                                                    IsValid = false;
                                                    string str = exp.Message;
                                                }
                                            }
                                        }
                                        x++;
                                    }
                                }
                            myConnection.Close();

                            book.Close(false, book, Type.Missing);
                            exobj.Workbooks.Close();
                            exobj.Quit();
                            System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                          if(IsValid)  MessageBox.Show("Selected File Has been saved!");
                        }
                        else MessageBox.Show("Selected File Is not Valid!");
                    
                }
            }
        }
        //----------------------------------m005ToolStripMenuItem_Click---------------------------------------
        private void m005ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExcelFormat formfile = new ExcelFormat(predismanualdate);
            DialogResult re = formfile.ShowDialog();
            if (re == DialogResult.OK)
            {
                string[] fileResult = formfile.result.Split(',');
                bool IsValid = true;
                DataTable TempTable = null;
                DialogResult re1 = openFileDialog1.ShowDialog();
                if (re1 != DialogResult.Cancel)
                {
                    string price = fileResult[0].ToString();
                    string path = openFileDialog1.FileName;
                    //read from FRM005.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();

                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    try
                    {
                        objAdapter1.Fill(objDataset1);
                        TempTable = objDataset1.Tables[0];
                    }
                    catch
                    {
                        IsValid = false;
                    }
                    //TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                    objConn.Close();

                    //Insert into DB (MainFRM005)
                    if ((IsValid))
                    {
                        Excel.Application exobj = new Excel.Application();
                        exobj.Visible = true;
                        exobj.UserControl = true;
                        Excel.Workbook book = null;
                        System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                        book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                        SqlCommand MyCom = new SqlCommand();
                        SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                        myConnection.Open();

                        MyCom.Connection = myConnection;
                        int type = 0;
                        string PID = "0";

                        string date = fileResult[1];



                        MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                        MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                        MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
                        MyCom.Parameters.Add("@name", SqlDbType.NChar, 50);
                        MyCom.Parameters.Add("@idate", SqlDbType.Char, 10);
                        MyCom.Parameters.Add("@time", SqlDbType.Char, 10);
                        MyCom.Parameters.Add("@revision", SqlDbType.NChar, 20);
                        MyCom.Parameters.Add("@filled", SqlDbType.NChar, 20);
                        MyCom.Parameters.Add("@approved", SqlDbType.NChar, 20);

                        MyCom.CommandText = "INSERT INTO [MainFRM005] (PPID,TargetMarketDate,PPName,PPType,"
                        + "DateOfIssue,TimeOfIssue,Revision,FilledBy,ApprovedBy)VALUES (@id,@tdate,@name,@type,@idate,@time,@revision,@filled,@approved)";
                        PID = fileResult[2];

                        ////////////////////////////////////ptype/////////////////////////////////////
                 
                        DataTable ddt = utilities.returntbl("SELECT  count(PPID) as s1  FROM [PPUnit] WHERE PPID='" + PID + "'");
                        DataTable ddtc = utilities.returntbl("SELECT count(PPID) as s2 FROM [PPUnit] WHERE PPID='" + PID + "' AND PackageType LIKE 'Combined Cycle%'");
                        int result1 = MyintParse1(ddt.Rows[0]["s1"].ToString());
                        int result2 = MyintParse1(ddtc.Rows[0]["s2"].ToString());
                        if ((result1 > 1) && (result2 > 0))
                        {
                            type = 1;
                        }
                        else
                        {
                            type = 0;
                        }

                        //////////////////////////////////////////////////////////////////////////
                                                                             

                        MyCom.Parameters["@id"].Value = PID;
                        MyCom.Parameters["@tdate"].Value = date;
                        MyCom.Parameters["@name"].Value = TempTable.Rows[3][1].ToString().Trim();
                        MyCom.Parameters["@type"].Value = type;
                        MyCom.Parameters["@idate"].Value = TempTable.Rows[0][1].ToString().Trim();
                        MyCom.Parameters["@time"].Value = TempTable.Rows[1][1].ToString().Trim();
                        MyCom.Parameters["@revision"].Value = TempTable.Rows[4][1].ToString().Trim();
                        MyCom.Parameters["@filled"].Value = TempTable.Rows[5][1].ToString().Trim();
                        MyCom.Parameters["@approved"].Value = TempTable.Rows[6][1].ToString().Trim();

                        try
                        {
                            /////// delete previous records, if any:

                            SqlCommand MyComDeleteMain = new SqlCommand();
                            MyComDeleteMain.Connection = myConnection;
                            MyComDeleteMain.CommandText = "Delete From [MainFRM005] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                            MyComDeleteMain.Parameters.Add("@id", SqlDbType.NChar, 10);
                            MyComDeleteMain.Parameters.Add("@tdate", SqlDbType.Char, 10);
                            MyComDeleteMain.Parameters.Add("@type", SqlDbType.SmallInt);
                            MyComDeleteMain.Parameters["@id"].Value = PID;
                            MyComDeleteMain.Parameters["@tdate"].Value = date;
                            MyComDeleteMain.Parameters["@type"].Value = type;
                            MyComDeleteMain.ExecuteNonQuery();
                            MyComDeleteMain.Dispose();
                            ///////////////////////////

                            /////// delete previous records, if any:

                            SqlCommand MyComDeleteBlock = new SqlCommand();
                            MyComDeleteBlock.Connection = myConnection;
                            MyComDeleteBlock.CommandText = "Delete From [BlockFRM005] where PPID=@id AND TargetMarketDate=@tdate AND PPType=@type";


                            MyComDeleteBlock.Parameters.Add("@id", SqlDbType.NChar, 10);
                            MyComDeleteBlock.Parameters.Add("@tdate", SqlDbType.Char, 10);
                            MyComDeleteBlock.Parameters.Add("@type", SqlDbType.SmallInt);
                            MyComDeleteBlock.Parameters["@id"].Value = PID;
                            MyComDeleteBlock.Parameters["@tdate"].Value = date;
                            MyComDeleteBlock.Parameters["@type"].Value = type;
                            MyComDeleteBlock.ExecuteNonQuery();
                            MyComDeleteBlock.Dispose();
                            ///////////////////////////

                            SqlCommand MyComDelete = new SqlCommand();
                            MyComDelete.Connection = myConnection;

                            MyComDelete.CommandText = "delete from DetailFRM005 where TargetMarketDate=@deleteDate" +
                                    " AND PPID=@ppID AND PPType=@ppType";
                            MyComDelete.Parameters.Add("@deleteDate", SqlDbType.Char, 10);
                            MyComDelete.Parameters.Add("@ppID", SqlDbType.NChar, 10);
                            MyComDelete.Parameters.Add("@ppType", SqlDbType.SmallInt);

                            MyComDelete.Parameters["@ppID"].Value = PID;
                            MyComDelete.Parameters["@deleteDate"].Value = date;
                            MyComDelete.Parameters["@ppType"].Value = type;

                            MyComDelete.ExecuteNonQuery();
                            MyComDelete.Dispose();

                            /////// END delete

                            MyCom.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            IsValid = false;
                            string str = exp.Message;
                        }

                        //Insert into DB (BlockFRM005)
                        int x = 10;
                        MyCom.Parameters.Add("@num", SqlDbType.NChar, 20);
                        MyCom.Parameters.Add("@prequired", SqlDbType.Real);
                        MyCom.Parameters.Add("@pdispach", SqlDbType.Real);
                        MyCom.Parameters.Add("@drequierd", SqlDbType.Real);
                        MyCom.Parameters.Add("@ddispach", SqlDbType.Real);

                        //read directly and cell by cell
                        foreach (Excel.Worksheet workSheet in book.Worksheets)
                            if (workSheet.Name == price)
                            {
                                while (x < TempTable.Rows.Count)
                                {
                                    if (TempTable.Rows[x][0].ToString().Trim() != "")
                                    {
                                        MyCom.CommandText = "INSERT INTO [BlockFRM005] (PPID,TargetMarketDate,PPType,Block,"
                                        + "PeakRequired,PeakDispatchable,DailyTotalRequired,DailyTotalDispachable)"
                                        + "VALUES (@id,@tdate,@type,@num,@prequired,@pdispach,@drequierd,@ddispach)";

                                        MyCom.Parameters["@id"].Value = PID;
                                        MyCom.Parameters["@tdate"].Value = date;
                                        MyCom.Parameters["@num"].Value = TempTable.Rows[x][0].ToString().Trim();
                                        MyCom.Parameters["@type"].Value = type;
                                        if (((Excel.Range)workSheet.Cells[x + 2, 27]).Value2 != null)
                                            MyCom.Parameters["@prequired"].Value = ((Excel.Range)workSheet.Cells[x + 2, 27]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@prequired"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 3, 27]).Value2 != null)
                                            MyCom.Parameters["@pdispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, 27]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@pdispach"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 2, 28]).Value2 != null)
                                            MyCom.Parameters["@drequierd"].Value = ((Excel.Range)workSheet.Cells[x + 2, 28]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@drequierd"].Value = 0;
                                        if (((Excel.Range)workSheet.Cells[x + 3, 28]).Value2 != null)
                                            MyCom.Parameters["@ddispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, 28]).Value2.ToString().Trim();
                                        else MyCom.Parameters["@ddispach"].Value = 0;
                                        try
                                        {
                                            MyCom.ExecuteNonQuery();
                                        }
                                        catch (Exception exp)
                                        {
                                            IsValid = false;
                                            string str = exp.Message;
                                        }
                                    }
                                    x++;
                                }
                            }
                        //Insert into DB (DetailFRM005)
                        x = 10;
                        MyCom.Parameters.Add("@required", SqlDbType.Real);
                        MyCom.Parameters.Add("@dispach", SqlDbType.Real);
                        MyCom.Parameters.Add("@economic", SqlDbType.Real);
                        MyCom.Parameters.Add("@contribution", SqlDbType.NChar, 10);
                        MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);

                        //read directly and cell by cell
                        foreach (Excel.Worksheet workSheet in book.Worksheets)
                            if (workSheet.Name == price)
                            {
                                while (x < (TempTable.Rows.Count - 1))
                                {
                                    if (TempTable.Rows[x][0].ToString().Trim() != "")
                                    {
                                        for (int j = 0; j < 24; j++)
                                        {
                                            MyCom.CommandText = "INSERT INTO [DetailFRM005] (TargetMarketDate,PPID,Block,PPType,Hour,Required,Dispatchable,Economic,Contribution) VALUES (@tdate,@id,@num,@type,@hour,@required,@dispach,@economic,@contribution)";

                                            MyCom.Parameters["@id"].Value = PID;
                                            MyCom.Parameters["@tdate"].Value = date;
                                            MyCom.Parameters["@num"].Value = TempTable.Rows[x][0].ToString().Trim();
                                            MyCom.Parameters["@type"].Value = type;

                                            MyCom.Parameters["@hour"].Value = j + 1;
                                            if (((Excel.Range)workSheet.Cells[x + 2, j + 3]).Value2 != null)
                                                MyCom.Parameters["@required"].Value = ((Excel.Range)workSheet.Cells[x + 2, j + 3]).Value2.ToString().Trim();
                                            else MyCom.Parameters["@required"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 3, j + 3]).Value2 != null)
                                                MyCom.Parameters["@dispach"].Value = ((Excel.Range)workSheet.Cells[x + 3, j + 3]).Value2.ToString().Trim();
                                            else MyCom.Parameters["@dispach"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2 != null)
                                                MyCom.Parameters["@economic"].Value = ((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2.ToString().Trim();
                                            else MyCom.Parameters["@economic"].Value = 0;
                                            if (((Excel.Range)workSheet.Cells[x + 5, j + 3]).Value2 != null)
                                                MyCom.Parameters["@contribution"].Value = ((Excel.Range)workSheet.Cells[x + 5, j + 3]).Value2.ToString().Trim();
                                            else MyCom.Parameters["@contribution"].Value = null;

                                            //if (((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2 != null)
                                            //    MyCom.Parameters["@contribution"].Value = ((Excel.Range)workSheet.Cells[x + 4, j + 3]).Value2.ToString().Trim();
                                            //else MyCom.Parameters["@contribution"].Value = null;
                                            try
                                            {
                                                MyCom.ExecuteNonQuery();
                                            }
                                            catch (Exception exp)
                                            {
                                                string str = exp.Message;
                                                IsValid = false;
                                            }
                                        }
                                    }
                                    x++;
                                }
                            }
                        myConnection.Close();
                        book.Close(false, book, Type.Missing);
                        exobj.Workbooks.Close();
                        exobj.Quit();
                        System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                        if (IsValid) MessageBox.Show("Selected File Has been saved!");
                    }
                }
                else MessageBox.Show("Selected File Isnot Valid!");
            }
        }
        //---------------------------------------averagePriceToolStripMenuItem_Click-------------------------------------
        private void averagePriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool check = true;
            ExcelFormat formfile = new ExcelFormat(predismanualdate);
            formfile.cmbsheet.Enabled = true;
            formfile.cmbppid.Enabled = false;
            formfile.datePicker.Enabled = false;
            DialogResult re = formfile.ShowDialog();
            if (re == DialogResult.OK)
            {
                string[] fileResult = formfile.result.Split(',');
                bool IsValid = true;
                DataTable TempTable = null;
                DialogResult re1 = openFileDialog1.ShowDialog();
                if (re1 != DialogResult.Cancel)
                {
                    string price = "قيمت ";
                    price = fileResult[0].ToString();
                    
                    string path = openFileDialog1.FileName;
               
                    //Save AS AveragePrice.xls file
                    //string path = @"c:\data\AveragePrice.xls";
                    Excel.Application exobj1 = new Excel.Application();
                    exobj1.Visible = true;
                    exobj1.UserControl = true;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    Excel.Workbook book1 = null;
                    book1 = exobj1.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book1.Save();
                    book1.Close(true, book1, Type.Missing);
                    exobj1.Workbooks.Close();
                    exobj1.Quit();

                    //read from AveragePrice.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();                   
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    try
                    {
                        objAdapter1.Fill(objDataset1);
                        TempTable = objDataset1.Tables[0];
                    }
                    catch
                    {
                        check = false;
                    }
                    //TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                    objConn.Close();

                    //Insert into DB (AveragePrice)
 
                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@date1", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                    MyCom.Parameters.Add("@Pmin", SqlDbType.Int);
                    MyCom.Parameters.Add("@Pmax", SqlDbType.Int);
                    MyCom.Parameters.Add("@Amin", SqlDbType.Int);
                    MyCom.Parameters.Add("@Amax", SqlDbType.Int);
                    MyCom.Parameters.Add("@Aav", SqlDbType.Int);

                    DataTable ddel = utilities.returntbl("delete from AveragePrice where Date='" + TempTable.Columns[0].ColumnName.ToString().Trim() + "'");

                    for (int i = 0; i < 24; i++)
                    {
                        //has Selected file saved before?
                        if (check)
                        {
                           

                            MyCom.CommandText = "INSERT INTO [AveragePrice] (Date,Hour,ProposedMin,ProposedMax,AcceptedMin,AcceptedMax,AcceptedAverage) VALUES (@date1,@hour,@Pmin,@Pmax,@Amin,@Amax,@Aav)";
                            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                            myConnection.Open();

                            MyCom.Connection = myConnection;
                            //MyCom.Parameters["@date1"].Value = TempGV.Columns[0].HeaderText.ToString().Trim();
                            MyCom.Parameters["@date1"].Value = TempTable.Columns[0].ColumnName.ToString().Trim();
                            MyCom.Parameters["@hour"].Value = TempTable.Rows[i + 3][0].ToString().Trim();
                            MyCom.Parameters["@Pmin"].Value = TempTable.Rows[i + 3][1].ToString().Trim();
                            MyCom.Parameters["@Pmax"].Value = TempTable.Rows[i + 3][2].ToString().Trim();
                            MyCom.Parameters["@Amin"].Value = TempTable.Rows[i + 3][3].ToString().Trim();
                            MyCom.Parameters["@Amax"].Value = TempTable.Rows[i + 3][4].ToString().Trim();
                            MyCom.Parameters["@Aav"].Value = TempTable.Rows[i + 3][5].ToString().Trim();
                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                                if (str.Contains("PRIMARY KEY"))
                                {
                                    MessageBox.Show("Selected File Had been saved Before!");
                                    check = false;
                                }
                            }
                            finally
                            {
                                myConnection.Close();
                            }
                        }
                    }
                    if (check) MessageBox.Show("Selected File Has been saved!");
                    else MessageBox.Show("Selected File Isnot Valid!");

                    System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                }
                else MessageBox.Show("Selected File Isnot Valid!");
            }
       
        }
        //-------------------------------loadForecastingToolStripMenuItem_Click------------------------------------
        private void loadForecastingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool check = true;
            ExcelFormat formfile = new ExcelFormat(predismanualdate);
            formfile.cmbsheet.Enabled = true;
            formfile.cmbppid.Enabled = false;
            formfile.datePicker.Enabled = false;
            DialogResult re = formfile.ShowDialog();
            if (re == DialogResult.OK)
            {
                string[] fileResult = formfile.result.Split(',');
                bool IsValid = true;
                DataTable TempTable = null;
                DialogResult re1 = openFileDialog1.ShowDialog();
                if (re1 != DialogResult.Cancel)
                {
                    string price = fileResult[0].ToString().Trim();
                    string path = openFileDialog1.FileName;
                    //Save AS LoadForecasting.xls file
                    //string path1 = @"c:\data\LoadForecasting.xls";
                    Excel.Application exobj = new Excel.Application();
                    exobj.Visible = true;
                    exobj.UserControl = true;
                    Excel.Workbook book = null;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book.Save();
                    book.Close(true, book, Type.Missing);

                    //read from LoadForecasting.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    try
                    {

                        objAdapter1.Fill(objDataset1);
                         TempTable = objDataset1.Tables[0];
                    }
                    catch
                    {
                        check = false;
                    }
                    //TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                    objConn.Close();

                    //read from LoadForecasting.xls into strings
                    string edate = "";
                    string[] date = new string[4];
                    book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    foreach (Excel.Worksheet workSheet in book.Worksheets)
                        if (workSheet.Name ==price)
                        {
                            edate = ((Excel.Range)workSheet.Cells[3, 4]).Value2.ToString().Trim();
                            date[0] = ((Excel.Range)workSheet.Cells[4, 3]).Value2.ToString().Trim();
                            date[1] = ((Excel.Range)workSheet.Cells[4, 4]).Value2.ToString().Trim();
                            date[2] = ((Excel.Range)workSheet.Cells[4, 5]).Value2.ToString().Trim();
                            date[3] = ((Excel.Range)workSheet.Cells[4, 6]).Value2.ToString().Trim();
                        }
                    book.Close(false, book, Type.Missing);
                    exobj.Workbooks.Close();
                    exobj.Quit();

                    //Insert into DB (LoadForecasting)

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@date11", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@edate", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@peak", SqlDbType.Real);
                    MyCom.Parameters.Add("@h1", SqlDbType.Real);
                    MyCom.Parameters.Add("@h2", SqlDbType.Real);
                    MyCom.Parameters.Add("@h3", SqlDbType.Real);
                    MyCom.Parameters.Add("@h4", SqlDbType.Real);
                    MyCom.Parameters.Add("@h5", SqlDbType.Real);
                    MyCom.Parameters.Add("@h6", SqlDbType.Real);
                    MyCom.Parameters.Add("@h7", SqlDbType.Real);
                    MyCom.Parameters.Add("@h8", SqlDbType.Real);
                    MyCom.Parameters.Add("@h9", SqlDbType.Real);
                    MyCom.Parameters.Add("@h10", SqlDbType.Real);
                    MyCom.Parameters.Add("@h11", SqlDbType.Real);
                    MyCom.Parameters.Add("@h12", SqlDbType.Real);
                    MyCom.Parameters.Add("@h13", SqlDbType.Real);
                    MyCom.Parameters.Add("@h14", SqlDbType.Real);
                    MyCom.Parameters.Add("@h15", SqlDbType.Real);
                    MyCom.Parameters.Add("@h16", SqlDbType.Real);
                    MyCom.Parameters.Add("@h17", SqlDbType.Real);
                    MyCom.Parameters.Add("@h18", SqlDbType.Real);
                    MyCom.Parameters.Add("@h19", SqlDbType.Real);
                    MyCom.Parameters.Add("@h20", SqlDbType.Real);
                    MyCom.Parameters.Add("@h21", SqlDbType.Real);
                    MyCom.Parameters.Add("@h22", SqlDbType.Real);
                    MyCom.Parameters.Add("@h23", SqlDbType.Real);
                    MyCom.Parameters.Add("@h24", SqlDbType.Real);
                    if (check)
                    {
                        for (int z = 0; z < 4; z++)
                        {
                            DataTable ddel = utilities.returntbl("DELETE FROM [LoadForecasting] WHERE DateEstimate ='" + edate + "' AND Date='" + date[z] + "'");

                            MyCom.CommandText = "INSERT INTO [LoadForecasting] (Date,DateEstimate,Peak,Hour1,Hour2,Hour3,Hour4," +
                            "Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19" +
                            ",Hour20,Hour21,Hour22,Hour23,Hour24) VALUES (@date11,@edate,@peak,@h1,@h2,@h3,@h4,@h5,@h6,@h7,@h8" +
                            ",@h9,@h10,@h11,@h12,@h13,@h14,@h15,@h16,@h17,@h18,@h19,@h20,@h21,@h22,@h23,@h24)";
                            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                            myConnection.Open();

                            MyCom.Connection = myConnection;
                            MyCom.Parameters["@date11"].Value = date[z];
                            MyCom.Parameters["@edate"].Value = edate;
                            MyCom.Parameters["@peak"].Value = TempTable.Rows[28][2 + z].ToString().Trim();
                            MyCom.Parameters["@h1"].Value = TempTable.Rows[4][2 + z].ToString().Trim();
                            MyCom.Parameters["@h2"].Value = TempTable.Rows[5][2 + z].ToString().Trim();
                            MyCom.Parameters["@h3"].Value = TempTable.Rows[6][2 + z].ToString().Trim();
                            MyCom.Parameters["@h4"].Value = TempTable.Rows[7][2 + z].ToString().Trim();
                            MyCom.Parameters["@h5"].Value = TempTable.Rows[8][2 + z].ToString().Trim();
                            MyCom.Parameters["@h6"].Value = TempTable.Rows[9][2 + z].ToString().Trim();
                            MyCom.Parameters["@h7"].Value = TempTable.Rows[10][2 + z].ToString().Trim();
                            MyCom.Parameters["@h8"].Value = TempTable.Rows[11][2 + z].ToString().Trim();
                            MyCom.Parameters["@h9"].Value = TempTable.Rows[12][2 + z].ToString().Trim();
                            MyCom.Parameters["@h10"].Value = TempTable.Rows[13][2 + z].ToString().Trim();
                            MyCom.Parameters["@h11"].Value = TempTable.Rows[14][2 + z].ToString().Trim();
                            MyCom.Parameters["@h12"].Value = TempTable.Rows[15][2 + z].ToString().Trim();
                            MyCom.Parameters["@h13"].Value = TempTable.Rows[16][2 + z].ToString().Trim();
                            MyCom.Parameters["@h14"].Value = TempTable.Rows[17][2 + z].ToString().Trim();
                            MyCom.Parameters["@h15"].Value = TempTable.Rows[18][2 + z].ToString().Trim();
                            MyCom.Parameters["@h16"].Value = TempTable.Rows[19][2 + z].ToString().Trim();
                            MyCom.Parameters["@h17"].Value = TempTable.Rows[20][2 + z].ToString().Trim();
                            MyCom.Parameters["@h18"].Value = TempTable.Rows[21][2 + z].ToString().Trim();
                            MyCom.Parameters["@h19"].Value = TempTable.Rows[22][2 + z].ToString().Trim();
                            MyCom.Parameters["@h20"].Value = TempTable.Rows[23][2 + z].ToString().Trim();
                            MyCom.Parameters["@h21"].Value = TempTable.Rows[24][2 + z].ToString().Trim();
                            MyCom.Parameters["@h22"].Value = TempTable.Rows[25][2 + z].ToString().Trim();
                            MyCom.Parameters["@h23"].Value = TempTable.Rows[26][2 + z].ToString().Trim();
                            MyCom.Parameters["@h24"].Value = TempTable.Rows[27][2 + z].ToString().Trim();

                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                            }
                            finally
                            {
                                myConnection.Close();
                            }
                        }
                        MessageBox.Show("Selected File Has been Saved!");
                    }
                    else MessageBox.Show("Selected File Isnot Valid!");
                    System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                }
                else MessageBox.Show("Selected File Isnot Valid!");
            }
        }
        //---------------------------toolStripMenuItem2_Click------------------------------------------------
       
        //-----------------------------------annualFactorToolStripMenuItem_Click---------------------------------------
        private void annualFactorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool check = true;
            DialogResult re = openFileDialog1.ShowDialog();
            if (re != DialogResult.Cancel)
            {

                string path = openFileDialog1.FileName;
                //Is It a Valid File?
                if (path.Contains("HCPF"))
                {
                    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                    myConnection.Open();

                    //read from HCPF.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "Data";
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    //TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                    DataTable TempTable = objDataset1.Tables[0];
                    objConn.Close();

                    //Insert into DB (HCPF)

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@date", SqlDbType.Char, 10);
                    MyCom.Parameters.Add("@h1", SqlDbType.Real);
                    MyCom.Parameters.Add("@h2", SqlDbType.Real);
                    MyCom.Parameters.Add("@h3", SqlDbType.Real);
                    MyCom.Parameters.Add("@h4", SqlDbType.Real);
                    MyCom.Parameters.Add("@h5", SqlDbType.Real);
                    MyCom.Parameters.Add("@h6", SqlDbType.Real);
                    MyCom.Parameters.Add("@h7", SqlDbType.Real);
                    MyCom.Parameters.Add("@h8", SqlDbType.Real);
                    MyCom.Parameters.Add("@h9", SqlDbType.Real);
                    MyCom.Parameters.Add("@h10", SqlDbType.Real);
                    MyCom.Parameters.Add("@h11", SqlDbType.Real);
                    MyCom.Parameters.Add("@h12", SqlDbType.Real);
                    MyCom.Parameters.Add("@h13", SqlDbType.Real);
                    MyCom.Parameters.Add("@h14", SqlDbType.Real);
                    MyCom.Parameters.Add("@h15", SqlDbType.Real);
                    MyCom.Parameters.Add("@h16", SqlDbType.Real);
                    MyCom.Parameters.Add("@h17", SqlDbType.Real);
                    MyCom.Parameters.Add("@h18", SqlDbType.Real);
                    MyCom.Parameters.Add("@h19", SqlDbType.Real);
                    MyCom.Parameters.Add("@h20", SqlDbType.Real);
                    MyCom.Parameters.Add("@h21", SqlDbType.Real);
                    MyCom.Parameters.Add("@h22", SqlDbType.Real);
                    MyCom.Parameters.Add("@h23", SqlDbType.Real);
                    MyCom.Parameters.Add("@h24", SqlDbType.Real);
                    MyCom.Connection = myConnection;
                    int i = 0;
                    while (i < TempTable.Rows.Count)
                    {
                        //has Selected file saved before?
                        if ((check) && (TempTable.Rows[i][0] != null) && (TempTable.Rows[i][0].ToString().Trim() != ""))
                        {
                            MyCom.CommandText = "INSERT INTO [HCPF] (Date,H1,H2,H3,H4,H5,H6,H7,H8,H9,H10,H11,H12," +
                            "H13,H14,H15,H16,H17,H18,H19,H20,H21,H22,H23,H24) VALUES (@date,@h1,@h2,@h3,@h4,@h5,@h6" +
                            ",@h7,@h8,@h9,@h10,@h11,@h12,@h13,@h14,@h15,@h16,@h17,@h18,@h19,@h20,@h21,@h22,@h23,@h24)";

                            //Translate Date in the Right Formet 1111/11/11
                            string date1 = TempTable.Rows[i][0].ToString().Trim();
                            string date2 = date1.Remove(4);
                            date2 += "/";
                            date2 += date1[4];
                            date2 += date1[5];
                            date2 += "/";
                            date2 += date1[6];
                            date2 += date1[7];
                            MyCom.Parameters["@date"].Value = date2;
                            MyCom.Parameters["@h1"].Value = TempTable.Rows[i][1].ToString().Trim();
                            MyCom.Parameters["@h2"].Value = TempTable.Rows[i][2].ToString().Trim();
                            MyCom.Parameters["@h3"].Value = TempTable.Rows[i][3].ToString().Trim();
                            MyCom.Parameters["@h4"].Value = TempTable.Rows[i][4].ToString().Trim();
                            MyCom.Parameters["@h5"].Value = TempTable.Rows[i][5].ToString().Trim();
                            MyCom.Parameters["@h6"].Value = TempTable.Rows[i][6].ToString().Trim();
                            MyCom.Parameters["@h7"].Value = TempTable.Rows[i][7].ToString().Trim();
                            MyCom.Parameters["@h8"].Value = TempTable.Rows[i][8].ToString().Trim();
                            MyCom.Parameters["@h9"].Value = TempTable.Rows[i][9].ToString().Trim();
                            MyCom.Parameters["@h10"].Value = TempTable.Rows[i][10].ToString().Trim();
                            MyCom.Parameters["@h11"].Value = TempTable.Rows[i][11].ToString().Trim();
                            MyCom.Parameters["@h12"].Value = TempTable.Rows[i][12].ToString().Trim();
                            MyCom.Parameters["@h13"].Value = TempTable.Rows[i][13].ToString().Trim();
                            MyCom.Parameters["@h14"].Value = TempTable.Rows[i][14].ToString().Trim();
                            MyCom.Parameters["@h15"].Value = TempTable.Rows[i][15].ToString().Trim();
                            MyCom.Parameters["@h16"].Value = TempTable.Rows[i][16].ToString().Trim();
                            MyCom.Parameters["@h17"].Value = TempTable.Rows[i][17].ToString().Trim();
                            MyCom.Parameters["@h18"].Value = TempTable.Rows[i][18].ToString().Trim();
                            MyCom.Parameters["@h19"].Value = TempTable.Rows[i][19].ToString().Trim();
                            MyCom.Parameters["@h20"].Value = TempTable.Rows[i][20].ToString().Trim();
                            MyCom.Parameters["@h21"].Value = TempTable.Rows[i][21].ToString().Trim();
                            MyCom.Parameters["@h22"].Value = TempTable.Rows[i][22].ToString().Trim();
                            MyCom.Parameters["@h23"].Value = TempTable.Rows[i][23].ToString().Trim();
                            MyCom.Parameters["@h24"].Value = TempTable.Rows[i][24].ToString().Trim();

                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                                if (str.Contains("PRIMARY KEY"))
                                {
                                    //MessageBox.Show("Selected File Had been saved Before!");
                                    check = false;
                                }
                            }
                        }
                        //Selected file has been saved before?
                        if ((!check) && (TempTable.Rows[i][0] != null) && (TempTable.Rows[i][0].ToString().Trim() != ""))
                        {
                            MyCom.CommandText = "UPDATE [HCPF] SET H1=@h1,H2=@h2,H3=@h3,H4=@h4,H5=@h5,H6=@h6," +
                            "H7=@h7,H8=@h8,H9=@h9,H10=@h10,H11=@h11,H12=@h12,H13=@h13,H14=@h14,H15=@h15,H16=@h16," +
                            "H17=@h17,H18=@h18,H19=@h19,H20=@h20,H21=@h21,H22=@h22,H23=@h23,H24=@h24 WHERE Date=@date";

                            //Translate Date in the Right Formet 1111/11/11
                            string date1 = TempTable.Rows[i][0].ToString().Trim();
                            string date2 = date1.Remove(4);
                            date2 += "/";
                            date2 += date1[4];
                            date2 += date1[5];
                            date2 += "/";
                            date2 += date1[6];
                            date2 += date1[7];
                            MyCom.Parameters["@date"].Value = date2;
                            MyCom.Parameters["@h1"].Value = TempTable.Rows[i][1].ToString().Trim();
                            MyCom.Parameters["@h2"].Value = TempTable.Rows[i][2].ToString().Trim();
                            MyCom.Parameters["@h3"].Value = TempTable.Rows[i][3].ToString().Trim();
                            MyCom.Parameters["@h4"].Value = TempTable.Rows[i][4].ToString().Trim();
                            MyCom.Parameters["@h5"].Value = TempTable.Rows[i][5].ToString().Trim();
                            MyCom.Parameters["@h6"].Value = TempTable.Rows[i][6].ToString().Trim();
                            MyCom.Parameters["@h7"].Value = TempTable.Rows[i][7].ToString().Trim();
                            MyCom.Parameters["@h8"].Value = TempTable.Rows[i][8].ToString().Trim();
                            MyCom.Parameters["@h9"].Value = TempTable.Rows[i][9].ToString().Trim();
                            MyCom.Parameters["@h10"].Value = TempTable.Rows[i][10].ToString().Trim();
                            MyCom.Parameters["@h11"].Value = TempTable.Rows[i][11].ToString().Trim();
                            MyCom.Parameters["@h12"].Value = TempTable.Rows[i][12].ToString().Trim();
                            MyCom.Parameters["@h13"].Value = TempTable.Rows[i][13].ToString().Trim();
                            MyCom.Parameters["@h14"].Value = TempTable.Rows[i][14].ToString().Trim();
                            MyCom.Parameters["@h15"].Value = TempTable.Rows[i][15].ToString().Trim();
                            MyCom.Parameters["@h16"].Value = TempTable.Rows[i][16].ToString().Trim();
                            MyCom.Parameters["@h17"].Value = TempTable.Rows[i][17].ToString().Trim();
                            MyCom.Parameters["@h18"].Value = TempTable.Rows[i][18].ToString().Trim();
                            MyCom.Parameters["@h19"].Value = TempTable.Rows[i][19].ToString().Trim();
                            MyCom.Parameters["@h20"].Value = TempTable.Rows[i][20].ToString().Trim();
                            MyCom.Parameters["@h21"].Value = TempTable.Rows[i][21].ToString().Trim();
                            MyCom.Parameters["@h22"].Value = TempTable.Rows[i][22].ToString().Trim();
                            MyCom.Parameters["@h23"].Value = TempTable.Rows[i][23].ToString().Trim();
                            MyCom.Parameters["@h24"].Value = TempTable.Rows[i][24].ToString().Trim();

                            //try
                            //{
                            MyCom.ExecuteNonQuery();
                            //}
                            //catch (Exception exp)
                            //{
                            //    string str = exp.Message;
                            //}
                        }
                        i++;
                    }
                    myConnection.Close();
                    MessageBox.Show("Selected File Has been saved!");
                }
                else MessageBox.Show("Selected File Isnot Valid!");
            }
        }
        //----------------------------------weekFactorToolStripMenuItem-------------------------------------------
        private void weekFactorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool check = true;
            DialogResult re = openFileDialog1.ShowDialog();
            if (re != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName;
                //Is It a Valid File?
                if (path.Contains("WCPF"))
                {
                    //read from WCPF.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();
                    string price = "Data";
                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    //TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                    DataTable TempTable = objDataset1.Tables[0];
                    objConn.Close();

                    //Insert into DB (WCPF)

                    SqlCommand MyCom = new SqlCommand();
                    MyCom.Parameters.Add("@year", SqlDbType.Int);
                    MyCom.Parameters.Add("@week", SqlDbType.Int);
                    MyCom.Parameters.Add("@value", SqlDbType.Real);
                    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                    myConnection.Open();

                    MyCom.Connection = myConnection;
                    int i = 0;
                    while (i < TempTable.Rows.Count)
                    {
                        //has Selected file saved before?
                        if ((check) && (TempTable.Rows[i][0] != null) && (TempTable.Rows[i][0].ToString().Trim() != ""))
                        {
                            MyCom.CommandText = "INSERT INTO [WCPF] (Year,Week,Value) VALUES (@year,@week,@value)";
                            MyCom.Parameters["@year"].Value = TempTable.Rows[i][0].ToString().Trim();
                            MyCom.Parameters["@week"].Value = TempTable.Rows[i][1].ToString().Trim();
                            MyCom.Parameters["@value"].Value = TempTable.Rows[i][2].ToString().Trim();
                            try
                            {
                                MyCom.ExecuteNonQuery();
                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                                if (str.Contains("PRIMARY KEY"))
                                {
                                    //MessageBox.Show("Selected File Had been saved Before!");
                                    check = false;
                                }
                            }
                        }
                        if ((!check) && (TempTable.Rows[i][0] != null) && (TempTable.Rows[i][0].ToString().Trim() != ""))
                        {
                            MyCom.CommandText = "UPDATE [WCPF] SET Value=@value WHERE Year=@year AND Week=@week";
                            MyCom.Parameters["@year"].Value = TempTable.Rows[i][0].ToString().Trim();
                            MyCom.Parameters["@week"].Value = TempTable.Rows[i][1].ToString().Trim();
                            MyCom.Parameters["@value"].Value = TempTable.Rows[i][2].ToString().Trim();
                            //try
                            //{
                            MyCom.ExecuteNonQuery();
                            //}
                            //catch (Exception exp)
                            //{
                            //    string str = exp.Message;
                            //}
                        }
                        i++;
                    }
                    myConnection.Close();
                    MessageBox.Show("Selected File Has been saved!");
                }
                else MessageBox.Show("Selected File Isnot Valid!");
            }
        }

        //---------------------------------------------autoPathToolStripMenuItem_Click---------------------------------------
        //private void autoPathToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    PathForm Path = new PathForm();
        //    Path.ShowDialog();

        //}

        //-------------------------------FRUnitCal_ValueChanged--------------------------------
        private void FRUnitCal_ValueChanged(object sender, EventArgs e)
        {
            string unit = FRUnitLb.Text.Trim();
            string package = FRPackLb.Text.Trim();
            int index = 0;
            if (GDSteamGroup.Text.Contains(package))
                for (int i = 0; i < (PlantGV2.RowCount - 1); i++)
                {
                    if (PlantGV2.Rows[i].Cells[0].Value.ToString().Contains(unit))
                        index = i;
                }
            else
                for (int i = 0; i < (PlantGV1.RowCount - 1); i++)
                {
                    if (PlantGV1.Rows[i].Cells[0].Value.ToString().Contains(unit))
                        index = i;
                }
            FillFRUnitRevenue(unit, package, index);
        }


        private void FRUnitAmainTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitAmainTb.Text, 1))
                errorProvider1.SetError(FRUnitAmainTb, "");
            else errorProvider1.SetError(FRUnitAmainTb, "just real number!");
        }

        private void FRUnitBmainTb_Validated(object sender, EventArgs e)
        {
            if (CheckValidated(FRUnitBmainTb.Text, 1))
                errorProvider1.SetError(FRUnitBmainTb, "");
            else errorProvider1.SetError(FRUnitBmainTb, "just real number!");

        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainTabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            TreeNode selectedNode = null;
            if (e.GetType() == typeof(TreeNodeMouseClickEventArgs))
            {
                TreeNodeMouseClickEventArgs e2 = (TreeNodeMouseClickEventArgs)e;
                selectedNode = e2.Node;
            }
            else
                selectedNode = treeView1.SelectedNode;

            if (MainTabs.SelectedTab == MainTabs.TabPages["FinancialReport"])
            {

                //AutomaticFillEconomics();
            }

        }

 


        public bool MultiplePackageType(int ppid)
        {
            string query = "select distinct packageType from [unitsdatamain] where ppid=" + ppid.ToString();
            if (utilities.returntbl(query).Rows.Count == 0)
                return false;
            else
                return true;
        }


        private DataSet GetUnits(int ppId)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();


            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            Myda.SelectCommand = new SqlCommand("SELECT DISTINCT PackageType FROM UnitsDataMain WHERE PPID=@Num", myConnection);
            Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
            Myda.SelectCommand.Parameters["@Num"].Value = ppId;
            Myda.Fill(MyDS, "UnitType");

            foreach (DataRow MyRow in MyDS.Tables["UnitType"].Rows)
            {
                string Utype = MyRow["PackageType"].ToString().Trim();
                Utype = Utype.Trim();
                switch (Utype)
                {
                    case "Steam":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Steam%'",
                            myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, "Steam");

                        DataView sdv = new DataView(MyDS.Tables["Steam"]);
                        sdv.Sort = "UnitCode ASC";
                        break;
                    case "Gas":
                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM UnitsDataMain WHERE PPID=@Num AND PackageType LIKE 'Gas%'",
                            myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, "Gas");
                        DataView gdv = new DataView(MyDS.Tables["Gas"]);
                        gdv.Sort = "UnitCode";

                        break;
                    default:

                        Myda.SelectCommand = new SqlCommand("SELECT UnitCode,PackageType,PackageCode FROM [UnitsDataMain] WHERE PPID=@Num AND PackageType LIKE 'CC%'",
                            myConnection);
                        Myda.SelectCommand.Parameters.Add("@Num", SqlDbType.NChar, 10);
                        Myda.SelectCommand.Parameters["@Num"].Value = ppId;
                        Myda.Fill(MyDS, "Combined");
                        DataView cdv = new DataView(MyDS.Tables["Combined"]);
                        cdv.Sort = "UnitCode ASC";
                        break;
                }
            }

            MyDS.Dispose();
            Myda.Dispose();

            myConnection.Close();
            return MyDS;

        }
//------------------------------GDPrimaryFuelTB_Validated-----------------------------------
        private void GDPrimaryFuelTB_Validated(object sender, EventArgs e)
        {
            if (GDConsLb.Text.Trim() != "Status")
            {
                if (CheckValidated(GDPrimaryFuelTB.Text, 1))
                    errorProvider1.SetError(GDPrimaryFuelTB, "");
                else errorProvider1.SetError(GDPrimaryFuelTB, "just real number!");
            }
        }
//------------------------------GDSecondaryFuelTB_Validated-----------------------------------
        private void GDSecondaryFuelTB_Validated(object sender, EventArgs e)
        {
            if (GDConsLb.Text.Trim() != "Status")
            {
                if (CheckValidated(GDSecondaryFuelTB.Text, 1))
                    errorProvider1.SetError(GDSecondaryFuelTB, "");
                else errorProvider1.SetError(GDSecondaryFuelTB, "just real number!");
            }
        }
//-----------------------------efficiencyMarketToolStripMenuItem_Click-------------------------------
        private void efficiencyMarketToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EfficiencyMarketForm efficiency = new EfficiencyMarketForm();
            efficiency.ShowDialog();
        }
//---------------------------------DeletetoolStripMenuItem_Click--------------------------------------
        private void DeletetoolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((TreeView1IsSelected) && (treeView1.SelectedNode.Parent != null) && (treeView1.SelectedNode.Parent.Text == "Plant"))
            {
                DialogResult result = MessageBox.Show("Are You Sure to delete Selected Plant?\r\n if you want to delete please change presolve plant in base data.", "Warning", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                    myConnection.Open();

                    SqlCommand MyComDeleteMain = new SqlCommand();
                    MyComDeleteMain.Connection = myConnection;
                    MyComDeleteMain.CommandText = "DELETE FROM [UnitsDataMain] WHERE PPID=@id DELETE FROM " +
                    "PPUnit WHERE PPID=@id  DELETE FROM PowerPlant WHERE PPID=@id DELETE FROM MainFRM002 " +
                    "WHERE PPID=@id DELETE FROM BlockFRM002 WHERE PPID=@id DELETE FROM DetailFRM002 WHERE " +
                    "PPID=@id DELETE FROM MainFRM005 WHERE PPID=@id DELETE FROM BlockFRM005 WHERE PPID=@id " +
                    "DELETE FROM DetailFRM005 WHERE PPID=@id DELETE FROM MainFRM009 WHERE PPID=@id DELETE " +
                    "FROM DetailFRM009 WHERE PPID=@id DELETE FROM ConditionPlant WHERE PPID=@id DELETE FROM" +
                    " ConditionUnit WHERE PPID=@id DELETE FROM EconomicUnit WHERE PPID=@id DELETE FROM " +
                    "EconomicPlant WHERE PPID=@id DELETE FROM FinalForecast WHERE PPId=@id DELETE FROM " +
                    "MaintenanceGeneralData WHERE PPID=@id DELETE FROM OutageUnits WHERE PPCode=@id " +
                    "DELETE FROM PowerLimitedunit WHERE PPID=@id DELETE FROM ProducedEnergy WHERE PPCode" +
                    "=@id DELETE FROM RegionNetComp WHERE PPCode=@id DELETE FROM Rep12Page WHERE PPCode=@id" +
                    " DELETE FROM UnitNetComp WHERE PPCode=@id DELETE FROM MaintenanceResult WHERE PPId=@id"+
                   " DELETE from BaseData where PPName='" + treeView1.SelectedNode.Text.Trim()+ "'";

                    MyComDeleteMain.Parameters.Add("@id", SqlDbType.NChar, 10);
                    MyComDeleteMain.Parameters["@id"].Value = PPID;


                    MyComDeleteMain.ExecuteNonQuery();
                    MyComDeleteMain.Dispose();
                    myConnection.Close();

                    buildTreeView1();
                    treeView2.Nodes.Clear();
                    ResetTabs();
                }
            }
            else
                MessageBox.Show("First Select a Plant,Please!");
        }
//--------------------------------ResetTabs--------------------------------
        private void ResetTabs()
        {
            GDPlantLb.Text = "";
            ODPlantLb.Text = "";
            MRPlantLb.Text = "";
            BDPlantLb.Text = "";
            FRPlantLb.Text = "";
            //lblMaintPlant.Text = "";
            //GeneralData
            GDGasGroup.Visible = false;
            GDSteamGroup.Visible = false;
            //FinancialReport
            ClearFRPlantTab();
            //MarketResult
            MRCurGrid1.DataSource = null;
            MRCurGrid2.DataSource = null;
            if (MRCurGrid1.Rows != null) MRCurGrid1.Rows.Clear();
            if (MRCurGrid2.Rows != null) MRCurGrid2.Rows.Clear();
            MRPlantOnUnitTb.Text = "";
            MRPlantPowerTb.Text = "";

        }
//-------------------------------------treeView1_Leave-------------------------------
        private void treeView1_Leave(object sender, EventArgs e)
        {
            TreeView1IsSelected = false;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            cmbMaintLastMaintType.Items.Clear();
        }

       


        
        
//--------------------------------------------------------------------------
       private string ReadLS2Files(int k)
        {

            DataTable plantname = utilities.returntbl("select PPName from dbo.PowerPlant where PPID='" + PPIDArray[k] + "'");
            string ppnamek = plantname.Rows[0][0].ToString().Trim();

            string status = "Incomplete Data or Serial Number For Plant <<<<" + ppnamek +">>>>\r\n";
            int loopcount = 0;
            UnitsData = null;
            UnitsData = utilities.returntbl("SELECT UnitCode,PackageCode,PowerSerial,ConsumedSerial,StateConnection,BusNumber FROM [UnitsDataMain] WHERE PPID='" + PPIDArray[k]+"'");
            foreach (DataRow MyRow in UnitsData.Rows)
            {
                for (int column = 0; column < UnitsData.Columns.Count; column++)
                {
                    if (MyRow[column].ToString() == "") MyRow[column] = "0";
                    MyRow[4] = "0";
                    MyRow[5] = "0";
                }
                int PowerCount = 0, ConsumeCount = 0;
                foreach (DataRow SecondRow in UnitsData.Rows)
                {
                        if (SecondRow[2].ToString().Trim() == MyRow[2].ToString().Trim()) PowerCount++;
                        if (SecondRow[3].ToString().Trim() == MyRow[3].ToString().Trim()) ConsumeCount++;
                }
                MyRow[4] = PowerCount;
                MyRow[5] = ConsumeCount;
            }
            //try
            //{
            foreach (DataRow MyRow in UnitsData.Rows)
            {
          
                for (int Count = 2; Count < 4; Count++)
                {
                    string Path = Auto_Update_Library.BaseData.GetInstance().CounterPath;
                    Path += @"\" + MyRow[Count].ToString().Trim();
                    Path += ".xls";
                    if (File.Exists(Path)) File.Delete(Path);
                    if (File.Exists(Path.Replace("xls", "ls2")))
                    {
                        //Save AS ls2file to xls file
                        Path = Path.Replace("xls", "ls2");
                        Excel.Application exobj = new Excel.Application();
                        exobj.Visible = false;
                        exobj.UserControl = true;
                        System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                        Excel.Workbook book = (Excel.Workbook)exobj.Workbooks.Add(Path);
                        Path = Path.Replace("ls2", "xls");
                        book.SaveAs(Path, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                        ///////////////
                        book.Close(false, book, Type.Missing);
                        exobj.Workbooks.Close();
                        exobj.Quit();

                        //////////////////////

                        //Read From Excel File
                        String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Path + ";Extended Properties=Excel 8.0";
                        OleDbConnection objConn = new OleDbConnection(sConnectionString);
                        objConn.Open();
                        string price = MyRow[Count].ToString().Trim();
                        OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                        objAdapter1.SelectCommand = objCmdSelect;
                        DataSet objDataset1 = new DataSet();
                        objAdapter1.Fill(objDataset1);
                        DataTable TempTable = objDataset1.Tables[0];
                        objConn.Close();

                        if (Count == 2) //Power Serial Type
                        {
                            int index = 4;
                            while (index < TempTable.Rows.Count)
                            {
                                if (TempTable.Rows[index][1].ToString().Trim() != "")
                                {
                                    //Seperate Time
                                    string myDateTime = TempTable.Rows[index][1].ToString().Trim();
                                    string TempTime = myDateTime.Substring(myDateTime.Length - 11, 11).Trim();
                                    string myDate = myDateTime.Remove(myDateTime.Length - 11, 11).Trim();
                                    char[] temp = new char[1];
                                    temp[0] = ':';
                                    int pos1 = TempTime.IndexOfAny(temp);
                                    string myTime = TempTime.Remove(pos1);
                                    if ((TempTime.Contains("PM")) && (myTime != "12"))
                                        myTime = (int.Parse(myTime) + 12).ToString();
                                    if ((TempTime.Contains("AM")) && (myTime == "12"))
                                        myTime = "0";
                                    //Seperate Date
                                    System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
                                    DateTime CurDate = DateTime.Parse(myDate);
                                    int imonth = pr.GetMonth(CurDate);
                                    int iyear = pr.GetYear(CurDate);
                                    int iday = pr.GetDayOfMonth(CurDate);
                                    string day = iday.ToString();
                                    if (int.Parse(day) < 10) day = "0" + day;
                                    string month = imonth.ToString();
                                    if (int.Parse(month) < 10) month = "0" + month;
                                    myDate = iyear.ToString() + "/" + month + "/" + day;

                                    //Is There any Data for this Plant(and type) at this Date in the MainFRM009 and DetailFRM009?
                                    DataTable IsThere = null;
                                    IsThere = utilities.returntbl("SELECT COUNT(*) FROM DetailFRM009 WHERE PPID='" + PPIDArray[k] + "' AND Block='" + MyRow[0].ToString().Trim() + "' AND TargetMarketDate='" + myDate + "' AND Hour=" + myTime);
                                    int check = int.Parse(IsThere.Rows[0][0].ToString());

                                    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                                    myConnection.Open();
                                    SqlCommand MyCom = new SqlCommand();
                                    MyCom.Connection = myConnection;
                                    MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                                    MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                                    MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                                    MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                                    MyCom.Parameters.Add("@PCode", SqlDbType.Int);
                                    MyCom.Parameters.Add("@P", SqlDbType.Real);
                                    MyCom.Parameters.Add("@QC", SqlDbType.Real);
                                    MyCom.Parameters.Add("@QL", SqlDbType.Real);

                                    //Insert Into DATABASE
                                    if (check == 0)
                                        MyCom.CommandText = "INSERT INTO [DetailFRM009] (TargetMarketDate,PPID" +
                                        ",Block,PackageCode,Hour,P,QC,QL) VALUES (@tdate,@id,@block,@PCode,@hour,@P,@QC,@QL)";
                                    else
                                        MyCom.CommandText = "UPDATE DetailFRM009 SET P=@P,QC=@QC,QL=@QL WHERE " +
                                        "TargetMarketDate=@tdate AND PPID=@id AND Block=@block AND " +
                                        "PackageCode=@PCode AND Hour=@hour";

                                    MyCom.Parameters["@id"].Value = PPIDArray[k].ToString();
                                    MyCom.Parameters["@tdate"].Value = myDate;
                                    MyCom.Parameters["@block"].Value = MyRow[0].ToString().Trim();
                                    MyCom.Parameters["@PCode"].Value = MyRow[1].ToString().Trim();
                                    MyCom.Parameters["@hour"].Value = myTime;
                                    if (TempTable.Rows[index][2].ToString().Trim() != "0")
                                        MyCom.Parameters["@P"].Value = (double.Parse(TempTable.Rows[index][2].ToString().Trim()) / (double.Parse(MyRow[4].ToString().Trim()))) / 1000000.0;
                                    else if (TempTable.Rows[index][3].ToString().Trim() != "0")
                                        MyCom.Parameters["@P"].Value = (double.Parse(TempTable.Rows[index][3].ToString().Trim()) / (double.Parse(MyRow[4].ToString().Trim()))) / 1000000.0;


                                    else if (check != 0)
                                    {

                                        double consumed = 0.0;
                                        IsThere = utilities.returntbl("SELECT Consumed FROM DetailFRM009 WHERE PPID='" + PPIDArray[k] + "' AND Block='" + MyRow[0].ToString().Trim() + "' AND TargetMarketDate='" + myDate + "' AND Hour=" + myTime);
                                        //double consumed = double.Parse(IsThere.Rows[0][0].ToString().Trim());
                                        //MyCom.Parameters["@P"].Value = consumed;
                                        if (IsThere.Rows[0][0].ToString() != "")
                                        {
                                            consumed = double.Parse(IsThere.Rows[0][0].ToString().Trim());
                                        }

                                        MyCom.Parameters["@P"].Value = consumed;
                                    }
                                    ////////////if all is zero//////////////////////////////////
                                    else
                                        MyCom.Parameters["@P"].Value = 0.0;
                                    /////////////////////////////////////////////////////////////////



                                    if (TempTable.Rows[index][4].ToString().Trim() != "")
                                        MyCom.Parameters["@QC"].Value = double.Parse(TempTable.Rows[index][4].ToString().Trim()) / 1000000.0;
                                    else MyCom.Parameters["@QC"].Value = 0;
                                    if (TempTable.Rows[index][5].ToString().Trim() != "")
                                        MyCom.Parameters["@QL"].Value = double.Parse(TempTable.Rows[index][5].ToString().Trim()) / 1000000.0;
                                    else MyCom.Parameters["@QL"].Value = 0;
                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        string str = exp.Message;
                                    }
                                    myConnection.Close();
                                }
                                index++;
                            }
                        }
                        else //if (Count==3):: Consumed Serial Type
                        {
                            int index = 4;
                            while (index < TempTable.Rows.Count)
                            {
                                if (TempTable.Rows[index][1].ToString().Trim() != "")
                                {
                                    //Seperate Time
                                    string myDateTime = TempTable.Rows[index][1].ToString().Trim();
                                    string TempTime = myDateTime.Substring(myDateTime.Length - 11, 11).Trim();
                                    string myDate = myDateTime.Remove(myDateTime.Length - 11, 11).Trim();
                                    char[] temp = new char[1];
                                    temp[0] = ':';
                                    int pos1 = TempTime.IndexOfAny(temp);
                                    string myTime = TempTime.Remove(pos1);
                                    if ((TempTime.Contains("PM")) && (myTime != "12"))
                                        myTime = (int.Parse(myTime) + 12).ToString();
                                    if ((TempTime.Contains("AM")) && (myTime == "12"))
                                        myTime = "0";
                                    //Seperate Date
                                    System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
                                    DateTime CurDate = DateTime.Parse(myDate);//DateTime.Now;
                                    int imonth = pr.GetMonth(CurDate);
                                    int iyear = pr.GetYear(CurDate);
                                    int iday = pr.GetDayOfMonth(CurDate);
                                    string day = iday.ToString();
                                    if (int.Parse(day) < 10) day = "0" + day;
                                    string month = imonth.ToString();
                                    if (int.Parse(month) < 10) month = "0" + month;
                                    myDate = iyear.ToString() + "/" + month + "/" + day;

                                    //Is There any Data for this Plant(and type) at this Date in the MainFRM009 and DetailFRM009?
                                    DataTable IsThere = null;
                                    IsThere = utilities.returntbl("SELECT COUNT(*) FROM DetailFRM009 WHERE PPID='" + PPIDArray[k] + "' AND Block='" + MyRow[0].ToString().Trim() + "' AND TargetMarketDate='" + myDate + "' AND Hour=" + myTime);
                                    int check = int.Parse(IsThere.Rows[0][0].ToString());

                                    //SET P With Consumed
                                    //Insert Into DATABASE
                                    SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                                    myConnection.Open();
                                    SqlCommand MyCom = new SqlCommand();
                                    MyCom.Connection = myConnection;
                                    MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                                    MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                                    MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                                    MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                                    MyCom.Parameters.Add("@PCode", SqlDbType.Int);
                                    MyCom.Parameters.Add("@P", SqlDbType.Real);
                                    MyCom.Parameters.Add("@Consumed", SqlDbType.Real);

                                    if (check == 0)
                                        MyCom.CommandText = "INSERT INTO [DetailFRM009] (TargetMarketDate,PPID,Block,PackageCode,Hour" +
                                        ",Consumed) VALUES (@tdate,@id,@block,@PCode,@hour,@Consumed)";
                                    else //if (check != 0)
                                    {
                                        IsThere = utilities.returntbl("SELECT P FROM DetailFRM009 WHERE PPID='" + PPIDArray[k] + "' AND Block='" + MyRow[0].ToString().Trim() + "' AND TargetMarketDate='" + myDate + "' AND Hour=" + myTime);
                                        double MyP = double.Parse(IsThere.Rows[0][0].ToString().Trim());

                                        if (MyP == 0)
                                            MyCom.CommandText = "UPDATE DetailFRM009 SET Consumed=@Consumed,P=@P WHERE Block=@block " +
                                            "AND PackageCode=@PCode AND Hour=@hour AND TargetMarketDate=@tdate AND PPID=@id";
                                        else //if (MyP!=0)
                                            MyCom.CommandText = "UPDATE DetailFRM009 SET Consumed=@Consumed WHERE Block=@block " +
                                            "AND PackageCode=@PCode AND Hour=@hour AND TargetMarketDate=@tdate AND PPID=@id";

                                    }

                                    MyCom.Parameters["@id"].Value = PPIDArray[k].ToString();
                                    MyCom.Parameters["@tdate"].Value = myDate;
                                    MyCom.Parameters["@block"].Value = MyRow[0].ToString().Trim();
                                    MyCom.Parameters["@PCode"].Value = MyRow[1].ToString().Trim();
                                    MyCom.Parameters["@hour"].Value = myTime;
                                    if (TempTable.Rows[index][2].ToString().Trim() != "0")
                                        MyCom.Parameters["@Consumed"].Value = (double.Parse(TempTable.Rows[index][2].ToString().Trim()) / (double.Parse(MyRow[5].ToString().Trim()))) / 1000000.0;
                                    else if (TempTable.Rows[index][3].ToString().Trim() != "0")
                                        MyCom.Parameters["@Consumed"].Value = (double.Parse(TempTable.Rows[index][3].ToString().Trim()) / (double.Parse(MyRow[5].ToString().Trim()))) / 1000000.0;
                                    try
                                    {
                                        MyCom.Parameters["@P"].Value = ((double)MyCom.Parameters["@Consumed"].Value / (MyDoubleParse(MyRow[4].ToString()))) / 1000000.0;
                                    }
                                    catch
                                    {
                                        
                                    }
                                    try
                                    {
                                        MyCom.ExecuteNonQuery();
                                    }
                                    catch (Exception exp)
                                    {
                                        string str = exp.Message;
                                    }
                                    myConnection.Close();

                                }
                                index++;
                            }

                        }
                        //book.Close(false, book, Type.Missing);
                        //exobj.Workbooks.Close();
                        //exobj.Quit();
                        System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                        if (loopcount == 0)
                        {
                            status = status.Replace("Incomplete", "Complete");
                        }
                    }
                    else
                    {
                    
                        if (Count == 2)
                        {
                            if (loopcount != 0)
                            {
                                status = status.Replace("Complete", "Incomplete");
                            }
                            status += "error" +" Unit:  " + MyRow[0].ToString().Trim()+ "   ";
                        }
                        loopcount++;
                    }
                   
                }
                       
            }
                            

            return status;
          
        }

        private void ReadLS2Trans(int k)
        {
            DataTable tableline = utilities.returntbl("select distinct  LineCode from dbo.TransLine ");
            string[] arrlinecode = new string[tableline.Rows.Count];
            for (int i = 0; i < tableline.Rows.Count; i++)
            {
                arrlinecode[i] = tableline.Rows[i][0].ToString().Trim();
            }
            DataTable trans1 = utilities.returntbl("SELECT  PowerSerialtrans FROM dbo.Lines WHERE LineCode='" + arrlinecode[k] + "'");

            string Path = Auto_Update_Library.BaseData.GetInstance().CounterPath;
            Path += @"\" + trans1.Rows[0][0].ToString().Trim();
            Path += ".xls";
            if (File.Exists(Path)) File.Delete(Path);
            if (File.Exists(Path.Replace("xls", "ls2")))
            {
                //Save AS ls2file to xls file
                Path = Path.Replace("xls", "ls2");
                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                Excel.Workbook book = (Excel.Workbook)exobj.Workbooks.Add(Path);
                Path = Path.Replace("ls2", "xls");
                book.SaveAs(Path, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                ////////////////////////
                book.Close(false, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();
                ///////////////////////


                //Read From Excel File
                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Path + ";Extended Properties=Excel 8.0";
                OleDbConnection objConn = new OleDbConnection(sConnectionString);
                objConn.Open();
                string price = trans1.Rows[0][0].ToString().Trim();
                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                objAdapter1.SelectCommand = objCmdSelect;
                DataSet objDataset1 = new DataSet();
                objAdapter1.Fill(objDataset1);
                DataTable TempTable = objDataset1.Tables[0];
                objConn.Close();

                int index = 4;
                while (index < TempTable.Rows.Count)
                {
                    if (TempTable.Rows[index][1].ToString().Trim() != "")
                    {
                        //Seperate Time
                        string myDateTime = TempTable.Rows[index][1].ToString().Trim();
                        string TempTime = myDateTime.Substring(myDateTime.Length - 11, 11).Trim();
                        string myDate = myDateTime.Remove(myDateTime.Length - 11, 11).Trim();
                        char[] temp = new char[1];
                        temp[0] = ':';
                        int pos1 = TempTime.IndexOfAny(temp);
                        string myTime = TempTime.Remove(pos1);
                        if ((TempTime.Contains("PM")) && (myTime != "12"))
                            myTime = (int.Parse(myTime) + 12).ToString();
                        if ((TempTime.Contains("AM")) && (myTime == "12"))
                            myTime = "0";
                        //Seperate Date
                        System.Globalization.PersianCalendar pr = new System.Globalization.PersianCalendar();
                        DateTime CurDate = DateTime.Parse(myDate);
                        int imonth = pr.GetMonth(CurDate);
                        int iyear = pr.GetYear(CurDate);
                        int iday = pr.GetDayOfMonth(CurDate);
                        string day = iday.ToString();
                        if (int.Parse(day) < 10) day = "0" + day;
                        string month = imonth.ToString();
                        if (int.Parse(month) < 10) month = "0" + month;
                        myDate = iyear.ToString() + "/" + month + "/" + day;

                        //Is There any Data for this Plant(and type) at this Date in the MainFRM009 and DetailFRM009?
                        DataTable IsThere = null;
                        IsThere = utilities.returntbl("SELECT COUNT(*) FROM DetailFRM009LINE WHERE LineCode='" + arrlinecode[k] + "' AND TargetMarketDate='" + myDate + "' AND Hour=" + myTime);
                        int check = int.Parse(IsThere.Rows[0][0].ToString());

                        SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                        myConnection.Open();
                        SqlCommand MyCom = new SqlCommand();
                        MyCom.Connection = myConnection;
                        MyCom.Parameters.Add("@ld", SqlDbType.NChar, 10);

                        MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                        MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);

                        MyCom.Parameters.Add("@P", SqlDbType.Real);
                        MyCom.Parameters.Add("@fl", SqlDbType.Int);

                        //Insert Into DATABASE
                        if (check == 0)
                            MyCom.CommandText = "INSERT INTO [DetailFRM009LINE] (TargetMarketDate,LineCode" +
                            ",Hour,P,FlagLine) VALUES (@tdate,@ld,@hour,@P,@fl)";
                        else
                            MyCom.CommandText = "UPDATE DetailFRM009LINE SET P=@P,FlagLine=@fl WHERE " +
                            "TargetMarketDate=@tdate AND LineCode=@ld " +
                            " AND Hour=@hour";

                        MyCom.Parameters["@ld"].Value = arrlinecode[k].ToString();
                        MyCom.Parameters["@tdate"].Value = myDate;
                        MyCom.Parameters["@hour"].Value = myTime;
                        if (TempTable.Rows[index][2].ToString().Trim() != "0")
                        {
                            MyCom.Parameters["@fl"].Value = -1;
                            MyCom.Parameters["@P"].Value = double.Parse(TempTable.Rows[index][2].ToString().Trim()) / 1000000.0;
                        }
                        else
                        {
                            MyCom.Parameters["@fl"].Value = 1;
                            MyCom.Parameters["@P"].Value = double.Parse(TempTable.Rows[index][3].ToString().Trim()) / 1000000.0;

                        }
                        try
                        {
                            MyCom.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            string str = exp.Message;
                        }
                        myConnection.Close();
                    }
                    index++;
                }
                //book.Close(false, book, Type.Missing);
                //exobj.Workbooks.Close();
                //exobj.Quit();
               System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
            }


        }

        /////////////////////////////Maintenence panel/////////////////////////
        private void txtDurabilityHours_Validated(object sender, EventArgs e)
        {
           
            if(!CheckValidated(txtDurabilityHours.Text,0))
                  errorProvider1.SetError(txtDurabilityHours, "Just integer!");
            else if (txtDurabilityHours.Text == "")
                errorProvider1.SetError(txtDurabilityHours, "Please Fill Blank!");
            else
              errorProvider1.SetError(txtDurabilityHours, "");
         
        }

        private void datePickerDurability_Validated(object sender, EventArgs e)
        {
            if (datePickerDurability.Text == "")
                errorProvider1.SetError(datePickerDurability, "Please Fill Blank!");
            else
                errorProvider1.SetError(datePickerDurability, "");
            
        }

        private void txtMaintLastHour_Validated(object sender, EventArgs e)
        {
            if (!CheckValidated(txtMaintLastHour.Text, 0))
                errorProvider1.SetError(txtMaintLastHour, "Just integer!");
            else if (txtMaintLastHour.Text == "")
                errorProvider1.SetError(txtMaintLastHour, "Please Fill Blank!");
            else
                errorProvider1.SetError(txtMaintLastHour, "");
        }

        private void cmbMaintLastMaintType_Validated(object sender, EventArgs e)
        {
            if (cmbMaintLastMaintType.Text == "")
                errorProvider1.SetError(cmbMaintLastMaintType, "Please Fill Blank!");
            else
                errorProvider1.SetError(cmbMaintLastMaintType, "");
        }

        private void txtMaintFuelFactor_Validated(object sender, EventArgs e)
        {
            if (!CheckValidated(txtMaintFuelFactor.Text, 1))
                errorProvider1.SetError(txtMaintFuelFactor, "Just Real!");
            else if (txtMaintFuelFactor.Text == "")
                errorProvider1.SetError(txtMaintFuelFactor, "Please Fill Blank!");
            else
                errorProvider1.SetError(txtMaintFuelFactor, "");
        }

        private void txtMaintStartUpFactor_Validated(object sender, EventArgs e)
        {
            if (!CheckValidated(txtMaintStartUpFactor.Text, 1))
                errorProvider1.SetError(txtMaintStartUpFactor, "Just Real!");
            else if (txtMaintStartUpFactor.Text == "")
                errorProvider1.SetError(txtMaintStartUpFactor, "Please Fill Blank!");
            else
                errorProvider1.SetError(txtMaintStartUpFactor, "");
        }

        private void txtMaintCIHours_Validated(object sender, EventArgs e)
        {
            if (!CheckValidated(txtMaintCIHours.Text, 0))
                errorProvider1.SetError(txtMaintCIHours, "Just integer!");
            else if (txtMaintCIHours.Text == "")
                errorProvider1.SetError(txtMaintCIHours, "Please Fill Blank!");
            else
                errorProvider1.SetError(txtMaintCIHours, "");
        }

        private void txtMaintCIDur_Validated(object sender, EventArgs e)
        {
            if (!CheckValidated(txtMaintCIDur.Text, 0))
                errorProvider1.SetError(txtMaintCIDur, "Just integer!");
            else if (txtMaintCIDur.Text == "")
                errorProvider1.SetError(txtMaintCIDur, "Please Fill Blank!");
            else
                errorProvider1.SetError(txtMaintCIDur, "");
        }

        private void txtMaintHGPHours_Validated(object sender, EventArgs e)
        {
            if (!CheckValidated(txtMaintHGPHours.Text, 0))
                errorProvider1.SetError(txtMaintHGPHours, "Just integer!");
            else if (txtMaintHGPHours.Text == "")
                errorProvider1.SetError(txtMaintHGPHours, "Please Fill Blank!");
            else
                errorProvider1.SetError(txtMaintHGPHours, "");
        }

        private void txtMaintHGPDur_Validated(object sender, EventArgs e)
        {
            if (!CheckValidated(txtMaintHGPDur.Text, 0))
                errorProvider1.SetError(txtMaintHGPDur, "Just integer!");
            else if (txtMaintHGPDur.Text == "")
                errorProvider1.SetError(txtMaintHGPDur, "Please Fill Blank!");
            else
                errorProvider1.SetError(txtMaintHGPDur, "");
        }

        private void txtMaintMOHours_Validated(object sender, EventArgs e)
        {
            if (!CheckValidated(txtMaintMOHours.Text, 0))
                errorProvider1.SetError(txtMaintMOHours, "Just integer!");
            else if (txtMaintMOHours.Text == "")
                errorProvider1.SetError(txtMaintMOHours, "Please Fill Blank!");
            else
                errorProvider1.SetError(txtMaintMOHours, "");
        }

        private void txtMaintMODur_Validated(object sender, EventArgs e)
        {
            if (!CheckValidated(txtMaintMODur.Text, 0))
                errorProvider1.SetError(txtMaintMODur, "Just integer!");
            else if (txtMaintMODur.Text == "")
                errorProvider1.SetError(txtMaintMODur, "Please Fill Blank!");
            else
                errorProvider1.SetError(txtMaintMODur, "");
        }

        private void allPlantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportForm myReport = new ReportForm("allplant");
            myReport.ShowDialog();

        }

        private void plantReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportForm myReport = new ReportForm("selectedplant");
            myReport.ShowDialog();

        }

        private void unitsReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportForm myReport = new ReportForm("selectedunit");
            myReport.ShowDialog();

        }

        private void MRPlotBtn_Click_1(object sender, EventArgs e)
        {
            bool limmit = false;
            if (rdbam005ba.Checked) limmit = true;

            if (MRHeaderPanel.Visible)//unit is selected
            {           
                MarketResultUnitPlotForm frm = new MarketResultUnitPlotForm(limmit);
                frm.Date = MRCal.Text;
                frm.PPId = PPID;
                frm.unit = MRUnitLb.Text.Trim();
                frm.package = MRPackLb.Text.Trim();
                frm.unitType = MRTypeLb.Text.Trim();
                frm.ShowDialog();

            }
            else if (L9.Text.Contains("Transmission"))
            {
            }
            else if (L9.Text.Contains("Line"))
            {
            }
            else //plant selected
            {
                bool Estimated=false;
               

                if (rbestimateplant.Checked) Estimated = true;
                else Estimated = false;
                               

                MarketResultPlantPlotForm frm = new MarketResultPlantPlotForm(Estimated,limmit);
                frm.Date = MRCal.Text;
                frm.PlantId = PPID;
                frm.ShowDialog();
            }
        }

        private void BDPlotBtn_Click(object sender, EventArgs e)
        {
            if (BDHeaderPanel.Visible)//unit is selected
            {
                BidDataUnitPlotForm frm = new BidDataUnitPlotForm();
                frm.Date = BDCal.Text;
                frm.PPId = PPID;
                frm.unit = BDUnitLb.Text.Trim();
                frm.package = BDPackLb.Text.Trim();
                frm.unitType = BDTypeLb.Text.Trim();
                //frm.isRealBid = RealBidCheck.Checked;
                frm.ShowDialog();
            }
        }

        private void autoPathToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            PathForm Path = new PathForm();
            Path.ShowDialog();
        }

        private void baseDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BaseDataForm baseData = new BaseDataForm();
            baseData.ShowDialog();
        }

        //private void lS2PlantsToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    string msg = "";
        //    for (int k = 0; k < PPIDArray.Count; k++)
        //    {
        //        if ((PPIDArray[k].ToString() != "0") && (PPIDType[k].ToString() == "real"))
        //        {

        //            msg += ReadLS2Files(k) + "\r\n";

        //        }

        //    }

        //    //////////////////errors

        //    if (msg.Contains("error"))
        //    {

        //        MessageBox.Show(msg.Replace("error", ""));
        //    }
        //    else
        //    {
        //        MessageBox.Show("Download Complete");

        //    }
        //}

        //private void lS2LinesToolStripMenuItem1_Click(object sender, EventArgs e)
        //{
        //    DataTable numline = utilities.returntbl("select distinct count(*) from dbo.TransLine");
        //    for (int k = 0; k < int.Parse(numline.Rows[0][0].ToString()); k++)
        //    {
        //        if (numline.Rows[0][0].ToString() != "0")
        //            ReadLS2Trans(k);
        //    }

        //}

        private void toolStripMenuItemStatus_Click(object sender, EventArgs e)
        {
            StatusForm sform = new StatusForm();
            sform.ShowDialog();
        }

        private void toolStripMenuItemExport_Click(object sender, EventArgs e)
        {
            Export009_0091Form exportf = new Export009_0091Form();
            exportf.ShowDialog();
        }

        private void ls2IntervalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LS2Interval frm = new LS2Interval();
            frm.Show();
        }

        private void ls2LineIntervalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ls2IntervalLine ls = new Ls2IntervalLine();
            ls.Show();

        }

        private void loadIntervalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormDownloadInterval frm = new FormDownloadInterval();
            frm.Show();
        }

        private void dispatchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExcelFormat formfile = new ExcelFormat(predismanualdate);
            DialogResult re = formfile.ShowDialog();
            if (re == DialogResult.OK)
            {
                string[] fileResult = formfile.result.Split(',');
                bool IsValid = true;
                DataTable TempTable = null;
                DialogResult re1 = openFileDialog1.ShowDialog();
                if (re1 != DialogResult.Cancel)
                {
                    string price = fileResult[0].ToString();
                    string path = openFileDialog1.FileName;
                    //read from FRM002.xls into datagridview
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();

                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    try
                    {
                        objAdapter1.Fill(objDataset1);
                        //TempGV.DataSource = objDataset1.Tables[0].DefaultView;
                        TempTable = objDataset1.Tables[0];
                    }
                    catch (Exception ex)
                    {
                        IsValid = false;
                        //throw ex;
                        MessageBox.Show("Invalid SheetName");
                    }
                    objConn.Close();
                    //IS IT A Valid File?
                    //if ((IsValid) && (TempGV.Columns[1].HeaderText.Contains("M002")))
                    if ((IsValid))
                    {
                        SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                        myConnection.Open();

                        //Insert into DB (MainFRM002)
                        //string path =@"c:\data\" + Doc002 + ".xls";
                        Excel.Application exobj = new Excel.Application();
                        exobj.Visible = true;
                        exobj.UserControl = true;
                        Excel.Workbook book = null;
                        System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                        book = exobj.Workbooks.Open(path, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                        SqlCommand MyCom = new SqlCommand();
                        MyCom.Connection = myConnection;
                        int type = 0;

                        
                        predismanualdate = fileResult[1];
                        string date2 = fileResult[1];
                                     
                        string pid = "";

                        try
                        {
                            
                            pid = fileResult[2];

                            /////////////////////////////////ptype///////////////////////////////////
                            //DataTable dt = utilities.returntbl("select COUNT(PPID) FROM PowerPlant WHERE PPID='" + pid + "'");

                            //int result = int.Parse(dt.Rows[0][0].ToString());


                            //CheckPPID = true;
                            //if (result == 0)
                            //{

                            //    CheckPPID = false;
                            //}
                            ////////////////////////////////////////////////////////////////////////////
                            
                            //if (!CheckPPID) type = 1; else type = 0;
                            
                            //////////////////if type 1 and ppid++//////////////////
                            //if (type == 1) pid = (int.Parse(pid) - 1).ToString();
                            ////////////////////////////////////////////////////////



                            ///////////////////////////////ptype///////////////////////////////////

                            DataTable ddt = utilities.returntbl("SELECT  count(PPID) as s1  FROM [PPUnit] WHERE PPID='" + pid + "'");
                            DataTable ddtc = utilities.returntbl("SELECT count(PPID) as s2 FROM [PPUnit] WHERE PPID='" + pid + "' AND PackageType LIKE 'Combined Cycle%'");
                            int result1 = MyintParse1(ddt.Rows[0]["s1"].ToString());
                            int result2 = MyintParse1(ddtc.Rows[0]["s2"].ToString());
                            if ((result1 > 1) && (result2 > 0))
                            {
                                type = 1;
                            }
                            else
                            {
                                type = 0;
                            }

                            //////////////////////////////////////////////////////////////////////////
                            string timefill = new PersianDate(DateTime.Now).ToString("d") + "|" + DateTime.Now.Hour + ":" + DateTime.Now.Minute;      

                            try
                            {
                                /////// delete previous records, if any:


                                DataTable deltable = utilities.returntbl("Delete From [Dispathable] where PPID='" + pid + "' AND StartDate='" + date2 + "' AND PackageType='" + type + "'");
                                /////// END delete


                            }
                            catch (Exception exp)
                            {
                                string str = exp.Message;
                                IsValid = false;
                            }


                            MyCom.Parameters.Add("@block", SqlDbType.NChar, 20);
                            MyCom.Parameters.Add("@deccap", SqlDbType.Real);
                            MyCom.Parameters.Add("@dispachcap", SqlDbType.Real);
                            MyCom.Parameters.Add("@hour", SqlDbType.SmallInt);
                            MyCom.Parameters.Add("@id", SqlDbType.NChar, 10);
                            MyCom.Parameters.Add("@tdate", SqlDbType.Char, 10);
                            MyCom.Parameters.Add("@type", SqlDbType.SmallInt);
                            
                            int x = 10;

                            foreach (Excel.Worksheet workSheet in book.Worksheets)
                                if (workSheet.Name == price)
                                {
                                    while (x < (TempTable.Rows.Count - 1))
                                    {
                                        if (TempTable.Rows[x][0].ToString().Trim() != "")
                                        {
                                            for (int j = 0; j < 24; j++)
                                            {
                                                MyCom.CommandText = "INSERT INTO [Dispathable] (StartDate,PPID,Block,PackageType,Hour" +
                                                                  ",DeclaredCapacity,DispachableCapacity,FilledBy,TimeFilled) VALUES (@tdate,@id,@block,@type,@hour,@deccap,@dispachcap,'" + User.getUser().LastName.ToString().Trim() + "','"+timefill+"')";

                                                MyCom.Parameters["@id"].Value = pid;
                                                MyCom.Parameters["@tdate"].Value = date2;
                                                if (((Excel.Range)workSheet.Cells[x + 2, 1]).Value2 != null)
                                                {
                                                    string blockcell = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString();
                                                    // MyCom.Parameters["@block"].Value = ((Excel.Range)workSheet.Cells[x + 2, 1]).Value2.ToString();

                                                    if (blockcell.Contains("CC"))
                                                    {
                                                        blockcell = blockcell.Replace("CC", "C");

                                                    }
                                                    MyCom.Parameters["@block"].Value = blockcell;
                                                }
                                                else MyCom.Parameters["@block"].Value = "";
                                                MyCom.Parameters["@type"].Value = type;
                                                MyCom.Parameters["@hour"].Value = j + 1;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2 != null)
                                                    MyCom.Parameters["@deccap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 6]).Value2.ToString();
                                                else MyCom.Parameters["@deccap"].Value = 0;
                                                if (((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2 != null)
                                                    MyCom.Parameters["@dispachcap"].Value = ((Excel.Range)workSheet.Cells[x + 2 + j, 7]).Value2.ToString();
                                                else MyCom.Parameters["@dispachcap"].Value = 0;

                                                try
                                                {
                                                    MyCom.ExecuteNonQuery();
                                                }
                                                catch (Exception exp)
                                                {
                                                    string str = exp.Message;
                                                    IsValid = false;
                                                }
                                            }
                                        }
                                        x++;
                                    }
                                }
                            myConnection.Close();

                            book.Close(false, book, Type.Missing);
                            exobj.Workbooks.Close();
                            exobj.Quit();
                            System.Threading.Thread.CurrentThread.CurrentCulture = oldci;

                        }
                        catch
                        {
                            MessageBox.Show("Invalid Excel Format!");
                            book.Close(false, book, Type.Missing);
                            exobj.Workbooks.Close();
                            exobj.Quit();
                            System.Threading.Thread.CurrentThread.CurrentCulture = oldci;
                            IsValid = false;
                        }
                        if (IsValid) MessageBox.Show("Selected File Hase Been Saved!");
                    }
                    else MessageBox.Show("Selected File Is not Valid!");
                }
            }
        }

        private void weekendCalenderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WeekendCalender frm = new WeekendCalender();
            frm.Show();
        }

        private void dispatchConstrantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DispatchConstrants frm = new DispatchConstrants();
            frm.Show();
        }

        private void oPFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Opf_Result f = new Opf_Result();
            f.Show();
        }

        private void sensetivityAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SensetivityAnalysis frm = new SensetivityAnalysis();
            frm.Show();
        }

        private void ls2PostIntervalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ls2PostInterval frm = new Ls2PostInterval();
            frm.Show();
        }

        private void allTypeIntervalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AllTypeInterval frm = new AllTypeInterval();
            frm.Show();

        }

        private void backUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BackupForm m = new BackupForm();
            m.Show();
        }

        private void deleteDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteDataForm frm = new DeleteDataForm();
            frm.Show();
        }

        private void SaledEnergytoolStripMenu_Click(object sender, EventArgs e)
        {
            bool success = true;
            ExcelFormat formfile = new ExcelFormat(predismanualdate);
            formfile.cmbsheet.Enabled = true;
            formfile.cmbppid.Enabled = false;
            formfile.datePicker.Enabled = true; ;
            DialogResult re = formfile.ShowDialog();
            if (re == DialogResult.OK)
            {
                string[] fileResult = formfile.result.Split(',');
               
                DataTable TempTable = null;
                DialogResult re1 = openFileDialog1.ShowDialog();
                if (re1 != DialogResult.Cancel)
                {
                    string price = fileResult[0].ToString();
                    string path = openFileDialog1.FileName;
                    string datte = fileResult[1];

                    string month = datte.Substring(5, 2);
                    string year = datte.Substring(0, 4);
                    string day = datte.Substring(8, 2);

                                     

                        Excel.Application exobj1 = new Excel.Application();
                        exobj1.Visible = true;
                        exobj1.UserControl = true;
                        System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                        Excel.Workbook book1 = null;
                        book1 = exobj1.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                        book1.Save();
                        book1.Close(true, book1, Type.Missing);
                        exobj1.Workbooks.Close();
                        exobj1.Quit();

                        //read from AveragePrice.xls into datagridview
                        String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                        OleDbConnection objConn = new OleDbConnection(sConnectionString);
                        objConn.Open();
                      
                        OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                        objAdapter1.SelectCommand = objCmdSelect;
                        DataSet objDataset1 = new DataSet();
                        try
                        {
                            objAdapter1.Fill(objDataset1);
                            TempTable = objDataset1.Tables[0];
                        }
                        catch
                        {
                            success = false;
                        }

                        objConn.Close();

                        //Insert into DB (saled energy)

                        SqlCommand MyCom = new SqlCommand();
                        MyCom.Parameters.Add("@Date", SqlDbType.NVarChar, 50);
                        MyCom.Parameters.Add("@PriceMin", SqlDbType.Real);
                        MyCom.Parameters.Add("@PriceMax", SqlDbType.Real);
                        MyCom.Parameters.Add("@rowindex", SqlDbType.Int);
                        MyCom.Parameters.Add("@Hour1", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour2", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour3", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour4", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour5", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour6", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour7", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour8", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour9", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour10", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour11", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour12", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour13", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour14", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour15", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour16", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour17", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour18", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour19", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour20", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour21", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour22", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour23", SqlDbType.Real);
                        MyCom.Parameters.Add("@Hour24", SqlDbType.Real);


                        try
                        {

                            for (int i = 2; i < 21; i++)
                            {
                                DataTable ddel = utilities.returntbl("delete from SaledEnergy where Date='" + (year + "/" + month + "/" + day) + "' and rowindex='" + (i - 2) + "'");


                                MyCom.CommandText = "INSERT INTO [SaledEnergy] (Date,PriceMin,PriceMax,rowindex,Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,"
                                + "Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24) VALUES "
                                + "(@Date,@PriceMin,@PriceMax,@rowindex,@Hour1,@Hour2,@Hour3,@Hour4,@Hour5,@Hour6,@Hour7,@Hour8,@Hour9,@Hour10,"
                                + "@Hour11,@Hour12,@Hour13,@Hour14,@Hour15,@Hour16,@Hour17,@Hour18,@Hour19,@Hour20,@Hour21,@Hour22,@Hour23,@Hour24)";
                                SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
                                myConnection.Open();

                                MyCom.Connection = myConnection;

                                MyCom.Parameters["@Date"].Value = year + "/" + month + "/" + day;
                                MyCom.Parameters["@rowindex"].Value = (i - 2);

                                MyCom.Parameters["@PriceMin"].Value = TempTable.Rows[i][0];
                                if (TempTable.Rows[i][1].ToString() != "")
                                {
                                    MyCom.Parameters["@PriceMax"].Value = TempTable.Rows[i][1];
                                }
                                else
                                    MyCom.Parameters["@PriceMax"].Value = TempTable.Rows[i][0];

                                int h = 2;

                                MyCom.Parameters["@Hour1"].Value = TempTable.Rows[i][h];
                                MyCom.Parameters["@Hour2"].Value = TempTable.Rows[i][h + 1];
                                MyCom.Parameters["@Hour3"].Value = TempTable.Rows[i][h + 2];
                                MyCom.Parameters["@Hour4"].Value = TempTable.Rows[i][h + 3];
                                MyCom.Parameters["@Hour5"].Value = TempTable.Rows[i][h + 4];
                                MyCom.Parameters["@Hour6"].Value = TempTable.Rows[i][h + 5];
                                MyCom.Parameters["@Hour7"].Value = TempTable.Rows[i][h + 6];
                                MyCom.Parameters["@Hour8"].Value = TempTable.Rows[i][h + 7];
                                MyCom.Parameters["@Hour9"].Value = TempTable.Rows[i][h + 8];
                                MyCom.Parameters["@Hour10"].Value = TempTable.Rows[i][h + 9];
                                MyCom.Parameters["@Hour11"].Value = TempTable.Rows[i][h + 10];
                                MyCom.Parameters["@Hour12"].Value = TempTable.Rows[i][h + 11];
                                MyCom.Parameters["@Hour13"].Value = TempTable.Rows[i][h + 12];
                                MyCom.Parameters["@Hour14"].Value = TempTable.Rows[i][h + 13];
                                MyCom.Parameters["@Hour15"].Value = TempTable.Rows[i][h + 14];
                                MyCom.Parameters["@Hour16"].Value = TempTable.Rows[i][h + 15];
                                MyCom.Parameters["@Hour17"].Value = TempTable.Rows[i][h + 16];
                                MyCom.Parameters["@Hour18"].Value = TempTable.Rows[i][h + 17];
                                MyCom.Parameters["@Hour19"].Value = TempTable.Rows[i][h + 18];
                                MyCom.Parameters["@Hour20"].Value = TempTable.Rows[i][h + 19];
                                MyCom.Parameters["@Hour21"].Value = TempTable.Rows[i][h + 20];
                                MyCom.Parameters["@Hour22"].Value = TempTable.Rows[i][h + 21];
                                MyCom.Parameters["@Hour23"].Value = TempTable.Rows[i][h + 22];
                                MyCom.Parameters["@Hour24"].Value = TempTable.Rows[i][h + 23];


                                try
                                {
                                    MyCom.ExecuteNonQuery();

                                }
                                catch (Exception exp)
                                {
                                    string str = exp.Message;
                                    MessageBox.Show("Error In Import !");
                                    success = false;
                                    break;
                                }


                                System.Threading.Thread.CurrentThread.CurrentCulture = oldci;



                            }

                        }
                        catch
                        {

                        }
                        if (success)
                        {
                            MessageBox.Show("Selected File Has been saved!");
                        }

                        else
                        {
                            MessageBox.Show(" Please Selecte Correct Path File !");

                        }

                }
        }
    //}
    //    }
    //        catch
    //        {
    //            MessageBox.Show("Error In Import!");
    //        }
        }

        private void ToolStripspecialtime_Click(object sender, EventArgs e)
        {
            SpecialTimes s = new SpecialTimes();
            s.Show();
        }

        private void addUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddUserForm ad = new AddUserForm();
            ad.Show();

        }

        private void EditUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditUserForm edit = new EditUserForm();
            edit.Show();
        }

        private void marketInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MarketInformation m = new MarketInformation();
            m.Show();
        }

        private void gencoNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GencoForm c = new GencoForm();
            c.Show();
        }

        private void backToLogInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f = new Form1();
            f.Show();
            
        }

        public bool Findpackplant(string ppid)
        {
          
            //means 206 have 2 package and one ppid
           DataTable oDataTable = utilities.returntbl("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            

                if (oDataTable.Rows.Count > 1 && Findccplant(ppid)==false )

                    return true;
            

            return false;
        }
        public bool Findccplant(string ppid)
        {
            int tr = 0;

           DataTable oDataTable = utilities.returntbl("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count > 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }
        public bool Findcconetype(string ppid)
        {
            int tr = 0;

           DataTable oDataTable = utilities.returntbl("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count == 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }

       

        private void formsDesignToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormsStyle f1 = new FormsStyle();
            f1.ShowDialog();

           
                Color fileResultformback= formcolors.getcolor().Formbackcolor;
                Color fileResultbuttonback = formcolors.getcolor().Buttonbackcolor;
                Color fileResultpanelback = formcolors.getcolor().Panelbackcolor;
                Color fileResulttextback = formcolors.getcolor().Textbackcolor;
                Color fileResultgridback = formcolors.getcolor().Gridbackcolor;
                try
                {
                    this.BackColor = fileResultformback;


                    ///////////////////////buttons///////////////////////////////
                    
                    foreach (Control c in this.Controls)
                    {
                        if (c is Button)
                            c.BackColor = fileResultbuttonback;
                    }
                    foreach (Control c in GDMainPanel.Controls)
                    {
                        if (c is Button)
                            c.BackColor = fileResultbuttonback;
                    }
                    foreach (Control c in GDPowerGb.Controls)
                    {
                        if (c is Button)
                            c.BackColor = fileResultbuttonback;
                    }
                    foreach (Control c in groupBox17.Controls)
                    {
                        if (c is Button)
                            c.BackColor = fileResultbuttonback;
                    }

                    foreach (Control c in ConsumedGb.Controls)
                    {
                        if (c is Button)
                            c.BackColor = fileResultbuttonback;
                    }
                    foreach (Control c in panel95.Controls)
                    {
                        if (c is Button)
                            c.BackColor = fileResultbuttonback;
                    }
                    foreach (Control c in ODMainPanel.Controls)
                    {
                        if (c is Button)
                            c.BackColor = fileResultbuttonback;
                    }

                    foreach (Control c in BDMainPanel.Controls)
                    {
                        if (c is Button)
                            c.BackColor = fileResultbuttonback;
                    }

                    foreach (Control c in FRMainPanel.Controls)
                    {
                        if (c is Button)
                            c.BackColor = fileResultbuttonback;
                    }
                    foreach (Control c in MRMainPanel.Controls)
                    {
                        if (c is Button)
                            c.BackColor = fileResultbuttonback;
                    }
                    foreach (Control c in ODUnitPowerGB.Controls)
                    {
                        if (c is Button)
                            c.BackColor = fileResultbuttonback;
                    }
                    foreach (Control c in groupBoxBiddingStrategy3.Controls)
                    {
                        if (c is Button)
                            c.BackColor = fileResultbuttonback;
                    }

                    /////////////////////////////////////////////////////////////////////

                    ///////////////////////////tabpage, panel/////////////////////////////////

                    GeneralData.BackColor = fileResultpanelback;
                    OperationalData.BackColor = fileResultpanelback;
                    BidData.BackColor = fileResultpanelback;
                    MarketResults.BackColor = fileResultpanelback;
                    FinancialReport.BackColor = fileResultpanelback;
                    BiddingStrategy.BackColor = fileResultpanelback;
                    tbPageMaintenance.BackColor = fileResultpanelback;
                    panel1.BackColor = fileResultpanelback;
                    panel2.BackColor = fileResultpanelback;
                    menuStrip1.BackColor = fileResultpanelback;
                    GDPostpanel.BackColor = fileResultpanelback;
                    tbmarketbill.BackColor = fileResultpanelback;
                    paneldailybill.BackColor = fileResultpanelback;
                    btnbilling.BackColor = fileResultbuttonback;
                    ////////////////////////////////////////////////////////////
                    ///////////////////////////////textbox///////////////////////////////////////////

                    foreach (Control c in this.Controls)
                    {
                        if (c is TextBox)
                            c.BackColor = fileResulttextback;
                        else if (c is ListBox)
                            c.BackColor = fileResulttextback;
                        else if (c is ComboBox)
                            c.BackColor = fileResulttextback;
                    }
                    foreach (Control c in ConsumedGb.Controls)
                    {
                        if (c is TextBox)
                            c.BackColor = fileResulttextback;
                        else if (c is ListBox)
                            c.BackColor = fileResulttextback;
                        else if (c is ComboBox)
                            c.BackColor = fileResulttextback;
                    }
                    foreach (Control c in GDPowerGb.Controls)
                    {
                        if (c is TextBox)
                            c.BackColor = fileResulttextback;
                        else if (c is ListBox)
                            c.BackColor = fileResulttextback;
                        else if (c is ComboBox)
                            c.BackColor = fileResulttextback;
                    }
                    foreach (Control c in groupBox17.Controls)
                    {
                        if (c is TextBox)
                            c.BackColor = fileResulttextback;
                        else if (c is ListBox)
                            c.BackColor = fileResulttextback;
                        else if (c is ComboBox)
                            c.BackColor = fileResulttextback;
                    }

                    foreach (Control c in groupBox20.Controls)
                    {
                        if (c is TextBox)
                            c.BackColor = fileResulttextback;
                        else if (c is ListBox)
                            c.BackColor = fileResulttextback;
                        else if (c is ComboBox)
                            c.BackColor = fileResulttextback;

                    }
                    foreach (Control c in ODUnitPowerGB.Controls)
                    {
                        if (c is TextBox)
                            c.BackColor = fileResulttextback;
                      
                    }

               
                    BDStateTb.BackColor = fileResulttextback;
                    BDPowerTb.BackColor = fileResulttextback;
                    BDMaxBidTb.BackColor = fileResulttextback;
                    MRPlantOnUnitTb.BackColor = fileResulttextback;
                    MRUnitMaxBidTb.BackColor = fileResulttextback; ;
                    MRUnitPowerTb.BackColor = fileResulttextback;
                    MRUnitStateTb.BackColor = fileResulttextback;
                    MRPlantPowerTb.BackColor = fileResulttextback;
                    FRPlantAvaCapTb.BackColor = fileResulttextback;
                    FRPlantBenefitTB.BackColor = fileResulttextback; 
                    FRPlantBidPayTb.BackColor = fileResulttextback;
                    FRPlantBidPowerTb.BackColor = fileResulttextback;
                    FRPlantCapPayTb.BackColor = fileResulttextback;
                    FRPlantCostTb.BackColor = fileResulttextback; ;
                    FRPlantDecPayTb.BackColor = fileResulttextback; ;
                    FRPlantDecPowerTb.BackColor = fileResulttextback;
                    FRPlantEnergyPayTb.BackColor = fileResulttextback;
                    FRPlantIncomeTb.BackColor = fileResulttextback;
                    FRPlantIncPayTb.BackColor = fileResulttextback;
                    FRPlantIncPowerTb.BackColor = fileResulttextback;
                    FRPlantTotalPowerTb.BackColor = fileResulttextback;
                    FRPlantULPayTb.BackColor = fileResulttextback;
                    FRPlantULPowerTb.BackColor = fileResulttextback;
                    FRUnitAmainTb.BackColor = fileResulttextback;
                    FRUnitAmargTb1.BackColor = fileResulttextback;
                    FRUnitAmargTb2.BackColor = fileResulttextback;
                    FRUnitBmainTb.BackColor = fileResulttextback;
                    FRUnitBmargTb1.BackColor = fileResulttextback;
                    FRUnitBmargTb2.BackColor = fileResulttextback;
                    FRUnitCapacityTb.BackColor = fileResulttextback;
                    FRUnitCapitalTb.BackColor = fileResulttextback;
                    FRUnitCapPayTb.BackColor = fileResulttextback;
                    FRUnitCmargTb1.BackColor = fileResulttextback;
                    FRUnitCmargTb2.BackColor = fileResulttextback;
                    FRUnitColdTb.BackColor = fileResulttextback;
                    FRUnitEneryPayTb.BackColor = fileResulttextback;
                    FRUnitFixedTb.BackColor = fileResulttextback;
                    FRUnitFuelCostTb.BackColor = fileResulttextback;
                    FRUnitFuelNoCostTb.BackColor = fileResulttextback;
                    FRUnitHotTb.BackColor = fileResulttextback;
                    FRUnitIncomeTb.BackColor = fileResulttextback;
                    FRUnitPActiveTb.BackColor = fileResulttextback;
                    FRUnitQReactiveTb.BackColor = fileResulttextback;
                    FRUnitTotalPowerTb.BackColor = fileResulttextback;
                    FRUnitULPowerTb.BackColor = fileResulttextback;
                    FRUnitVariableTb.BackColor = fileResulttextback;
                    ODUnitFuelTB.BackColor = fileResulttextback;
                    GDcapacityTB.BackColor = fileResulttextback;
                    GDFuelTB.BackColor = fileResulttextback;
                    GDMainTypeTB.BackColor = fileResulttextback;
                    GDPmaxTB.BackColor = fileResulttextback;
                    GDPminTB.BackColor = fileResulttextback;
                    GDPowerSerialTb.BackColor = fileResulttextback;
                    GDpowertransserialtb.BackColor = fileResulttextback;
                    GDPrimaryFuelTB.BackColor = fileResulttextback;
                    GDRampRateTB.BackColor = fileResulttextback;
                    GDSecondaryFuelTB.BackColor = fileResulttextback;
                    GDStateTB.BackColor = fileResulttextback;
                    GDTimeColdStartTB.BackColor = fileResulttextback;
                    GDTimeDownTB.BackColor = fileResulttextback;
                    GDTimeHotStartTB.BackColor = fileResulttextback;
                    GDTimeUpTB.BackColor = fileResulttextback;
                    GDtxtAvc.BackColor = fileResulttextback;
                    GDConsumedSerialTb.BackColor = fileResulttextback;
                    GDMainTypeTB.BackColor = fileResulttextback;
                    txtMaintCIDur.BackColor = fileResulttextback;
                    txtMaintCIHours.BackColor = fileResulttextback;
                    txtMaintHGPDur.BackColor = fileResulttextback;
                    txtMaintHGPHours.BackColor = fileResulttextback;
                    txtMaintMODur.BackColor = fileResulttextback;
                    txtMaintMOHours.BackColor = fileResulttextback;
                    txtMaintStatus.BackColor = fileResulttextback;
                    txtDurabilityHours.BackColor = fileResulttextback;
                    txtMaintLastHour.BackColor = fileResulttextback;
                    txtMaintStartUpFactor.BackColor = fileResulttextback;
                    txtMaintFuelFactor.BackColor = fileResulttextback;
                    txtMaintHGPHours.BackColor = fileResulttextback;
                    txtMaintHGPDur.BackColor = fileResulttextback;
                    cmbCostLevel.BackColor = fileResulttextback;
                    cmbPeakBid.BackColor = fileResulttextback;
                    txtpowertraining.BackColor = fileResulttextback;
                    txtTraining.BackColor = fileResulttextback;
                    cmbRunSetting.BackColor = fileResulttextback;
                    cmbStrategyBidding.BackColor = fileResulttextback;
                    lstStatus.BackColor=fileResulttextback;
                    cmbMaintShownPlant.BackColor = fileResulttextback;
                    ////////////////////////////////////////////////////////////////////////////////                   
                    //////////////////////////////////grids////////////////////////////////////////////


                    ODUnitPowerGrid1.RowsDefaultCellStyle.BackColor = fileResultgridback;
                    ODUnitPowerGrid1.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;
                  

                    ODUnitPowerGrid2.RowsDefaultCellStyle.BackColor = fileResultgridback;
                    ODUnitPowerGrid2.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;
                   
                    
                    MRCurGrid2.RowsDefaultCellStyle.BackColor = fileResultgridback;
                    MRCurGrid2.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;
                   

                    MRCurGrid1.RowsDefaultCellStyle.BackColor = fileResultgridback;
                    MRCurGrid1.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;

                    
                   
                    BDCurGrid.RowsDefaultCellStyle.BackColor = fileResultgridback;
                    BDCurGrid.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;

                   
                    Grid230.RowsDefaultCellStyle.BackColor = fileResultgridback;
                    Grid230.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;

                   
                    Grid400.RowsDefaultCellStyle.BackColor = fileResultgridback;
                    Grid400.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;

                   
                    PlantGV1.RowsDefaultCellStyle.BackColor = fileResultgridback;
                    PlantGV1.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;

                  
                    PlantGV2.RowsDefaultCellStyle.BackColor = fileResultgridback;
                    PlantGV2.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;

                   
                    dataGridDispatch.RowsDefaultCellStyle.BackColor = fileResultgridback;
                    dataGridDispatch.RowsDefaultCellStyle.SelectionBackColor = fileResultgridback;




                    dataGridDispatch.ColumnHeadersDefaultCellStyle.BackColor =fileResultpanelback;
                    ODUnitPowerGrid1.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                    ODUnitPowerGrid2.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                    MRCurGrid2.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                    MRCurGrid1.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                    BDCurGrid.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                    Grid230.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                    Grid400.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                    PlantGV1.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                    PlantGV2.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                    dgbill.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                    dataGridViewECO.ColumnHeadersDefaultCellStyle.BackColor = fileResultpanelback;
                    

                    


                    ///////////////////////////////////////////////////////////////////////////////////

                }
                catch
                {

                }
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            System.Globalization.PersianCalendar p = new System.Globalization.PersianCalendar();
            label115.Text = p.GetSecond(DateTime.Now) + " : " + p.GetMinute(DateTime.Now) + " : " + p.GetHour(DateTime.Now);
        }


        private void contactUsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Email em = new Email();
            em.Show();
        }

        private void emailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Email em = new Email();
            em.Show();
        }

        private void pDFToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                "\\RUP-IMPLEMENTAETION5.pdf");
            }
            catch
            {
                MessageBox.Show("Pdf File Not Existed In My Documents");
            }
        }

        private void wordToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                  "\\RUP-IMPLEMENTAETION.doc");
            }
            catch
            {
                MessageBox.Show("Doc File Not Existed In My Documents");
            }
        }

        private void btnbidprint_Click(object sender, EventArgs e)
        {
            try
            {
                string name = "";
                if (RealBidCheck.Checked)
                {
                    name = "پيشنهاد قيمت";
                }
                else if (EstimatedBidCheck.Checked)
                {

                    name = "پيشنهاد قيمت توسط نرم افزار";
                }

                DataTable dt = new DataTable();
                DataColumn[] dsc = new DataColumn[] { };
                foreach (DataGridViewColumn c in BDCurGrid.Columns)
                {
                    DataColumn dc = new DataColumn();
                    dc.ColumnName = c.HeaderText;
                    dc.DataType = c.ValueType;
                    dt.Columns.Add(dc);
                }
                foreach (DataGridViewRow r in BDCurGrid.Rows)
                {
                    DataRow drow = dt.NewRow();
                    foreach (DataGridViewCell cell in r.Cells)
                    {
                        try
                        {
                            drow[cell.OwningColumn.HeaderText] = cell.Value;
                        }
                        catch
                        {
                        }
                    }
                    dt.Rows.Add(drow);

                }





                BidDataPrint m = new BidDataPrint(dt, name, BDCal.Text.Trim(), BDPlantLb.Text,BDUnitLb.Text);
                m.Show();
            }
            catch
            {

            }


        }

        private void profileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Profile m = new Profile();
            m.Show();
        }

        private void internetplantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Metering_Internet_Plant m = new Metering_Internet_Plant();
            m.Show();
        }

        private void dailyBillToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            ///////////////////////////////////////////////////////////////////////////////////////
            DialogResult re1 = openFileDialog1.ShowDialog();
            if (re1 != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName; ;
                filldailybillsum(path);
                filldailybilltotal(path);
                filldailybillplant(path);
            }
            ///////////////////////////////////////////////////////////////////////////////////////
        }




        public void filldailybillsum(string path)
        {
            string price = "sum";           
             

                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                Excel.Workbook book = null;
                book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                book.Save();
                book.Close(true, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();

                //////////////////////

                //Read From Excel File
                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                OleDbConnection objConn = new OleDbConnection(sConnectionString);
                objConn.Open();

                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                objAdapter1.SelectCommand = objCmdSelect;
                DataSet objDataset1 = new DataSet();
                objAdapter1.Fill(objDataset1);
                DataTable TempTable = objDataset1.Tables[0];
                objConn.Close();
                billdate = TempTable.Rows[0][3].ToString().Trim();
                DataTable dd = utilities.returntbl("delete from DailyBillSum where date='"+billdate+"'");

                foreach (DataRow m in TempTable.Rows)
                {
                    if (m[1].ToString().Trim() != "")
                    {
                        string ppid = m[0].ToString().Trim();
                        string plantname = m[1].ToString().Trim();
                      
                        string s1=m[4].ToString().Trim();
                        string s2=m[5].ToString().Trim();
                        string s3 = m[6].ToString().Trim();
                        string s4 = m[7].ToString().Trim();
                        string s5 = m[8].ToString().Trim();
                        string s6 = m[9].ToString().Trim();
                        string s7 = m[10].ToString().Trim();
                        string s8 = m[11].ToString().Trim();
                        string s9 = m[12].ToString().Trim();
                        string s10 = m[13].ToString().Trim();
                        string s11 = m[14].ToString().Trim();
                        string s12 = m[15].ToString().Trim();
                        string s13 = m[16].ToString().Trim();
                        string s14 = m[17].ToString().Trim();
                        string s15 = m[18].ToString().Trim();
                        string s16 = m[19].ToString().Trim();
                        string s17 = m[20].ToString().Trim();
                        string s18 = m[21].ToString().Trim();
                        string s19 = m[22].ToString().Trim();
                        string s20 = m[23].ToString().Trim();
                        string s21 = m[24].ToString().Trim();
                        string s22 = m[25].ToString().Trim();
                        string s23 = m[26].ToString().Trim();
                        string s24 = m[27].ToString().Trim();
                        string s26 = m[28].ToString().Trim();
                        string s27 = m[29].ToString().Trim();
                        string s28 = m[30].ToString().Trim();
                        string s29 = m[31].ToString().Trim();
                        string s31 = m[32].ToString().Trim();
                        string s36 = m[33].ToString().Trim();
                        string s37 = m[34].ToString().Trim();
                        string s38 = m[35].ToString().Trim();
                        string s39 = m[36].ToString().Trim();
                        string s40 = m[37].ToString().Trim();
                        string s41 = m[38].ToString().Trim();
                        string s42 = m[39].ToString().Trim();
                        string s43 = m[40].ToString().Trim();
                
                        DataTable d = utilities.returntbl("insert into DailyBillSum (ppid,plantname,Date,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15,s16,s17,s18,s19,s20,s21,s22,s23,s24,s26,s27,s28,s29,s31,s36,s37,s38,s39,s40,s41,s42,s43)" +
                            "values ('" + ppid + "',N'" + plantname + "','" + billdate + "','"+s1+"','"+s2+"','"+s3+"','"+s4+"','"+s5+"','"+s6+"','"+s7+"','"+s8+"','"+s9+"','"+s10+"','"+s11+"','"+s12+"','"+s13+"','"+s14+"','"+s15+"','"+s16+"','"+s17+"','"+s18+"','"+s19+"','"+s20+"','"+s21+"','"+s22+"','"+s23+"','"+s24+"','"+s26+"','"+s27+"','"+s28+"','"+s29+"','"+s31+"','"+s36+"','"+s37+"','"+s38+"','"+s39+"','"+s40+"','"+s41+"','"+s42+"','"+s43+"')");
                           

                    }
                


            }


        }
        public void filldailybilltotal(string path)
        {
            string price = "total";         

                Excel.Application exobj = new Excel.Application();
                exobj.Visible = false;
                exobj.UserControl = true;
                System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                Excel.Workbook book = null;
                book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                book.Save();
                book.Close(true, book, Type.Missing);
                exobj.Workbooks.Close();
                exobj.Quit();

                //////////////////////

                //Read From Excel File
                String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                OleDbConnection objConn = new OleDbConnection(sConnectionString);
                objConn.Open();

                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                objAdapter1.SelectCommand = objCmdSelect;
                DataSet objDataset1 = new DataSet();
                objAdapter1.Fill(objDataset1);
                DataTable TempTable = objDataset1.Tables[0];
                objConn.Close();

                string date = billdate;
                DataTable dd = utilities.returntbl("delete from DailyBillTotal where date='"+billdate+"'");

                foreach (DataRow m in TempTable.Rows)
                {
                    if (m[1].ToString().Trim() != "")
                    {
                        string code = m[0].ToString().Trim();
                        string item = m[1].ToString().Trim();
                        double  value = MyDoubleParse( m[2].ToString());
                       
                        DataTable d = utilities.returntbl("insert into DailyBillTotal (Code,Item,Value,Date) values ('" + code + "',N'" + item + "','" + value + "','" + date + "')");

                    }
                }


            


        }
        public void filldailybillplant(string path)
        {
            string price = "Day";

            Excel.Application exobj = new Excel.Application();
            exobj.Visible = false;
            exobj.UserControl = true;
            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            book.Save();
            book.Close(true, book, Type.Missing);
            exobj.Workbooks.Close();
            exobj.Quit();

            //////////////////////

            //Read From Excel File
            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            objConn.Open();

            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
            objAdapter1.SelectCommand = objCmdSelect;
            DataSet objDataset1 = new DataSet();
            objAdapter1.Fill(objDataset1);
            DataTable TempTable = objDataset1.Tables[0];
            objConn.Close();

            string date = billdate;
            DataTable dd = utilities.returntbl("delete from DailyBillPlant where date='" + billdate + "'");

            foreach (DataRow m in TempTable.Rows)
            {
                if (m[1].ToString().Trim() != "")
                {
                    string ppid = m[0].ToString().Trim();
                    string plantname = m[1].ToString().Trim();
                    
                    double hour = MyDoubleParse(m[4].ToString());
                    string s1 = m[5].ToString().Trim();
                    string s2 = m[6].ToString().Trim();
                    string s3 = m[7].ToString().Trim();
                    string s4 = m[8].ToString().Trim();
                    string s5 = m[9].ToString().Trim();
                    string s6 = m[10].ToString().Trim();
                    string s7 = m[11].ToString().Trim();
                    string s8 = m[12].ToString().Trim();
                    string s9 = m[13].ToString().Trim();
                    string s10 = m[14].ToString().Trim();
                    string s11 = m[15].ToString().Trim();
                    string s12 = m[16].ToString().Trim();
                    string s13 = m[17].ToString().Trim();
                    string s14 = m[18].ToString().Trim();
                    string s15 = m[19].ToString().Trim();
                    string s16 = m[20].ToString().Trim();
                    string s17 = m[21].ToString().Trim();
                    string s18 = m[22].ToString().Trim();
                    string s19 = m[23].ToString().Trim();
                    string s20 = m[24].ToString().Trim();
                    string s21 = m[25].ToString().Trim();
                    string s22 = m[26].ToString().Trim();
                    string s23 = m[27].ToString().Trim();
                    string s24 = m[28].ToString().Trim();
                    string s26 = m[29].ToString().Trim();
                    string s27 = m[30].ToString().Trim();
                    string s28 = m[31].ToString().Trim();
                    string s29 = m[32].ToString().Trim();
                    string s31 = m[33].ToString().Trim();
                    string s36 = m[34].ToString().Trim();
                    string s37 = m[35].ToString().Trim();
                    string s38 = m[36].ToString().Trim();
                    string s39 = m[37].ToString().Trim();
                    string s40 = m[38].ToString().Trim();
                    string s41 = m[39].ToString().Trim();
                    string s42 = m[40].ToString().Trim();
                    string s43 = m[41].ToString().Trim();
                    DataTable d = utilities.returntbl("insert into DailyBillPlant (ppid,plantname,Date,hour,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15,s16,s17,s18,s19,s20,s21,s22,s23,s24,s26,s27,s28,s29,s31,s36,s37,s38,s39,s40,s41,s42,s43) values ('" +ppid + "',N'" + plantname + "','" + billdate + "','" + hour + "','"+ s1 +"','" + s2+ "','"+ s3 +"','"+ s4 +"','"+ s5 +"','"+ s6 +"','"+s7+"','"+s8+"','"+s9+"','"+s10+"','"+s11+"','"+s12+"','"+s13+"','"+s14+"','"+s15+"','"+s16+
                    "','"+s17+"','"+s18+"','"+s19+"','"+s20+"','"+s21+"','"+s22+"','"+s23+"','"+s24+"','"+s26+"','"+s27+"','"+s28+"','"+s29+"','"+s31+"','"+s36+"','"+s37+"','"+s38+"','"+s39+"','"+s40+"','"+s41+"','"+s42+"','"+s43+"')");
                      

                }
            }





        }

        //oldprivate void monthlyBillToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    DialogResult re1 = openFileDialog1.ShowDialog();
        //    if (re1 != DialogResult.Cancel)
        //    {
        //        string path = openFileDialog1.FileName; ;
        //        fillmonthlybilldate(path);
        //        fillmonthlybilltotal(path);

        //        ////////////////////////////////////////////////////////////////
        //        int num = 31;
        //        string year = monthlydate.Substring(0, 4);
        //        string month = monthlydate.Substring(5, 2);
        //        if (int.Parse(year) % 4 == 3 && month == "12")
        //        {
        //            num = 30;
        //        }
        //        else if (month == "12")
        //        {
        //            num = 29;
        //        }                 
               
                
        //        for (int i = 1; i <= num; i++)
        //        {
        //            fillmonthlybillplant(path,i);

        //        }
               
        //    }



        //}
        private void monthlyBillToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult re1 = openFileDialog1.ShowDialog();
            if (re1 != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName; ;

                findpidbill(path);
                fillmonthlybilltotal(path);
                fillmonthlybilldate(path);
               

                ////////////////////////////////////////////////////////////////
                int num = 31;
                string year = monthlydate.Substring(0, 4);
                string month = monthlydate.Substring(5, 2);
                if (int.Parse(year) % 4 == 3 && month == "12")
                {
                    num = 30;
                }
                else if (month == "12")
                {
                    num = 29;
                }


                for (int i = 1; i <= num; i++)
                {
                    fillmonthlybillplant(path, i);

                }
                MessageBox.Show("Completed");
            }


            ////set default bill items
            DataTable s = utilities.returntbl("delete from billitem where  date like'%" + COMmonthlydate.Replace(".xls", "").Trim().Substring(0, 4) + "/" + COMmonthlydate.Replace(".xls", "").Trim().Substring(4, 2) + "%'");


            s = utilities.returntbl("insert into billitem (date,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10)values('" + COMmonthlydate.Replace(".xls", "").Trim().Substring(0, 4) + "/" + COMmonthlydate.Replace(".xls", "").Trim().Substring(4, 2)+"/01" + "','1','1','1','1','1','1','1','1','1','1')");

             


           

        }
        public void fillmonthlybilldate(string path)
        {
            string price = "Month";


            Excel.Application exobj = new Excel.Application();
            exobj.Visible = false;
            exobj.UserControl = true;
            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            book.Save();
            book.Close(true, book, Type.Missing);
            exobj.Workbooks.Close();
            exobj.Quit();

            //////////////////////

            //Read From Excel File
            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            objConn.Open();

            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
            objAdapter1.SelectCommand = objCmdSelect;
            DataSet objDataset1 = new DataSet();
            objAdapter1.Fill(objDataset1);
            DataTable TempTable = objDataset1.Tables[0];
            objConn.Close();
            monthlydate = TempTable.Rows[0][3].ToString().Substring(0, 7).Trim();
            DataTable dd = utilities.returntbl("delete from MonthlyBillDate where substring(date,1,7)='" +monthlydate + "'and ppid='"+fmonthlybillplant+"'");

            foreach (DataRow m in TempTable.Rows)
            {
                if (m[1].ToString().Trim() != "")
                {
                    string ppid = m[0].ToString().Trim();
                    string plantname = m[1].ToString().Trim();

                    string n = "";
                    string v = "";

                    DataTable bb = utilities.returntbl("select distinct code from dbo.MonthlyBillTotal where month='"+monthlydate+"'order by code asc");
                    string[] name = new string[bb.Rows.Count];
                    for (int y = 0; y < name.Length; y++)
                    {
                        name[y] = bb.Rows[y][0].ToString().Trim();


                        if (name[y].Contains("01") || name[y].Contains("02") || name[y].Contains("03") || name[y].Contains("04") || name[y].Contains("05") || name[y].Contains("06") || name[y].Contains("07") || name[y].Contains("08") || name[y].Contains("09"))
                            name[y] = name[y].Replace("0", "").Trim();
                        n += name[y].Trim() + ",";
                        if (y == name.Length - 1)
                        {
                            v += m[y + 4].ToString().Trim();
                        }
                        else
                        {
                            v += m[y + 4].ToString().Trim() + "','";
                        }
                    }
                    n = n.TrimEnd(',');
                    v = v.TrimEnd('"','"');
                    //string s1 = m[4].ToString().Trim();
                    //string s2 = m[5].ToString().Trim();
                    //string s3 = m[6].ToString().Trim();
                    //string s4 = m[7].ToString().Trim();
                    //string s5 = m[8].ToString().Trim();
                    //string s6 = m[9].ToString().Trim();
                    //string s7 = m[10].ToString().Trim();
                    //string s8 = m[11].ToString().Trim();
                    //string s9 = m[12].ToString().Trim();
                    //string s10 = m[13].ToString().Trim();
                    //string s11 = m[14].ToString().Trim();
                    //string s12 = m[15].ToString().Trim();
                    //string s13 = m[16].ToString().Trim();
                    //string s14 = m[17].ToString().Trim();
                    //string s15 = m[18].ToString().Trim();
                    //string s16 = m[19].ToString().Trim();
                    //string s17 = m[20].ToString().Trim();
                    //string s18 = m[21].ToString().Trim();
                    //string s19 = m[22].ToString().Trim();
                    //string s20 = m[23].ToString().Trim();
                    //string s21 = m[24].ToString().Trim();
                    //string s22 = m[25].ToString().Trim();
                    //string s23 = m[26].ToString().Trim();
                    //string s24 = m[27].ToString().Trim();
                    //string s26 = m[28].ToString().Trim();
                    //string s27 = m[29].ToString().Trim();
                    //string s28 = m[30].ToString().Trim();
                    //string s29 = m[31].ToString().Trim();
                    //string s31 = m[32].ToString().Trim();
                    //string s36 = m[33].ToString().Trim();
                    //string s37 = m[34].ToString().Trim();
                    //string s38 = m[35].ToString().Trim();
                    //string s39 = m[36].ToString().Trim();
                    //string s40 = m[37].ToString().Trim();
                    //string s41 = m[38].ToString().Trim();
                    //string s42 = m[39].ToString().Trim();
                    //string s43 = m[40].ToString().Trim();
                     string ddate = m[3].ToString().Trim();
                    //DataTable d = utilities.returntbl("insert into MonthlyBillDate (ppid,plantname,Date,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15,s16,s17,s18,s19,s20,s21,s22,s23,s24,s26,s27,s28,s29,s31,s36,s37,s38,s39,s40,s41,s42,s43)" +
                    //    "values ('" + ppid + "',N'" + plantname + "','" + ddate + "','" + s1 + "','" + s2 + "','" + s3 + "','" + s4 + "','" + s5 + "','" + s6 + "','" + s7 + "','" + s8 + "','" + s9 + "','" + s10 + "','" + s11 + "','" + s12 + "','" + s13 + "','" + s14 + "','" + s15 + "','" + s16 + "','" + s17 + "','" + s18 + "','" + s19 + "','" + s20 + "','" + s21 + "','" + s22 + "','" + s23 + "','" + s24 + "','" + s26 + "','" + s27 + "','" + s28 + "','" + s29 + "','" + s31 + "','" + s36 + "','" + s37 + "','" + s38 + "','" + s39 + "','" + s40 + "','" + s41 + "','" + s42 + "','" + s43 + "')");
                  
                    DataTable d = utilities.returntbl("insert into MonthlyBillDate (ppid,plantname,Date,"+n+")" +
                     "values ('" + ppid + "',N'" + plantname + "','" + ddate +"','"+v+"')");


                }



            }


        }
        public void fillmonthlybilltotal(string path)
        {
            string price = "total";

            Excel.Application exobj = new Excel.Application();
            exobj.Visible = false;
            exobj.UserControl = true;
            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            book.Save();
            book.Close(true, book, Type.Missing);
            exobj.Workbooks.Close();
            exobj.Quit();

            //////////////////////

            //Read From Excel File
            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            objConn.Open();

            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
            objAdapter1.SelectCommand = objCmdSelect;
            DataSet objDataset1 = new DataSet();
            objAdapter1.Fill(objDataset1);
            DataTable TempTable = objDataset1.Tables[0];
            objConn.Close();
            string cxx = openFileDialog1.SafeFileName;
            monthlydate = cxx.Substring(0, 4) + "/" + cxx.Substring(4, 2);
            COMmonthlydate = cxx;
           // string date = billdate;
            DataTable dd = utilities.returntbl("delete from MonthlyBillTotal where month='" + monthlydate + "'and ppid='"+fmonthlybillplant+"'");

            foreach (DataRow m in TempTable.Rows)
            {
                if (m[1].ToString().Trim() != "")
                {
                    string code = m[0].ToString().Trim();
                    string item = m[1].ToString().Trim();
                    double value = MyDoubleParse(m[2].ToString());
               

                    DataTable d = utilities.returntbl("insert into MonthlyBillTotal (ppid,Code,Item,Value,month) values ('"+fmonthlybillplant+"','" + code + "',N'" + item + "','" + value + "','" + monthlydate + "')");

                }
            }





        }

        public void findpidbill(string path)
        {
            string price = "Month";


            Excel.Application exobj = new Excel.Application();
            exobj.Visible = false;
            exobj.UserControl = true;
            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            book.Save();
            book.Close(true, book, Type.Missing);
            exobj.Workbooks.Close();
            exobj.Quit();

            //////////////////////

            //Read From Excel File
            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            objConn.Open();

            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
            objAdapter1.SelectCommand = objCmdSelect;
            DataSet objDataset1 = new DataSet();
            objAdapter1.Fill(objDataset1);
            DataTable TempTable = objDataset1.Tables[0];
            objConn.Close();


            foreach (DataRow m in TempTable.Rows)
            {
                if (m[1].ToString().Trim() != "")
                {

                    fmonthlybillplant = m[0].ToString().Trim();
                    break;
                }



            }


        }

        public void fillmonthlybillplant(string path,int day)
        {
            string price = "Day_"+day.ToString().Trim();

            Excel.Application exobj = new Excel.Application();
            exobj.Visible = false;
            exobj.UserControl = true;
            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            book.Save();
            book.Close(true, book, Type.Missing);
            exobj.Workbooks.Close();
            exobj.Quit();

            //////////////////////

            //Read From Excel File
            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            objConn.Open();

            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
            objAdapter1.SelectCommand = objCmdSelect;
            DataSet objDataset1 = new DataSet();
            objAdapter1.Fill(objDataset1);
            DataTable TempTable = objDataset1.Tables[0];
            objConn.Close();

            string date = TempTable.Rows[0][3].ToString();
            DataTable dd = utilities.returntbl("delete from MonthlyBillPlant where date='" + date + "'and ppid='"+fmonthlybillplant+"'");

            foreach (DataRow m in TempTable.Rows)
            {
                if (m[1].ToString().Trim() != "")
                {
                    string ppid = m[0].ToString().Trim();
                    string plantname = m[1].ToString().Trim();

                    string n = "";
                    string v = "";

                    DataTable bb = utilities.returntbl("select distinct code from dbo.MonthlyBillTotal where month='"+date.Substring(0,7).Trim()+"'  order by code asc");
                    string[] name = new string[bb.Rows.Count];
                    for (int y = 0; y < name.Length; y++)
                    {
                        name[y] = bb.Rows[y][0].ToString().Trim();

                        if (name[y].Contains("01") || name[y].Contains("02") || name[y].Contains("03") || name[y].Contains("04") || name[y].Contains("05") || name[y].Contains("06") || name[y].Contains("07") || name[y].Contains("08") || name[y].Contains("09"))
                            name[y] = name[y].Replace("0", "").Trim();
                        n += name[y].Trim() + ",";
                        if (y == name.Length - 1) v += m[y + 5].ToString().Trim();
                        else v += m[y + 5].ToString().Trim() + "','";
                    }
                    n = n.TrimEnd(',');
                    v = v.TrimEnd('"', '"');
                    double hour = MyDoubleParse(m[4].ToString());
                    //string s1 = m[5].ToString().Trim();
                    //string s2 = m[6].ToString().Trim();
                    //string s3 = m[7].ToString().Trim();
                    //string s4 = m[8].ToString().Trim();
                    //string s5 = m[9].ToString().Trim();
                    //string s6 = m[10].ToString().Trim();
                    //string s7 = m[11].ToString().Trim();
                    //string s8 = m[12].ToString().Trim();
                    //string s9 = m[13].ToString().Trim();
                    //string s10 = m[14].ToString().Trim();
                    //string s11 = m[15].ToString().Trim();
                    //string s12 = m[16].ToString().Trim();
                    //string s13 = m[17].ToString().Trim();
                    //string s14 = m[18].ToString().Trim();
                    //string s15 = m[19].ToString().Trim();
                    //string s16 = m[20].ToString().Trim();
                    //string s17 = m[21].ToString().Trim();
                    //string s18 = m[22].ToString().Trim();
                    //string s19 = m[23].ToString().Trim();
                    //string s20 = m[24].ToString().Trim();
                    //string s21 = m[25].ToString().Trim();
                    //string s22 = m[26].ToString().Trim();
                    //string s23 = m[27].ToString().Trim();
                    //string s24 = m[28].ToString().Trim();
                    //string s26 = m[29].ToString().Trim();
                    //string s27 = m[30].ToString().Trim();
                    //string s28 = m[31].ToString().Trim();
                    //string s29 = m[32].ToString().Trim();
                    //string s31 = m[33].ToString().Trim();
                    //string s36 = m[34].ToString().Trim();
                    //string s37 = m[35].ToString().Trim();
                    //string s38 = m[36].ToString().Trim();
                    //string s39 = m[37].ToString().Trim();
                    //string s40 = m[38].ToString().Trim();
                    //string s41 = m[39].ToString().Trim();
                    //string s42 = m[40].ToString().Trim();
                    //string s43 = m[41].ToString().Trim();
                    //DataTable d = utilities.returntbl("insert into MonthlyBillPlant (ppid,plantname,Date,hour,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15,s16,s17,s18,s19,s20,s21,s22,s23,s24,s26,s27,s28,s29,s31,s36,s37,s38,s39,s40,s41,s42,s43) values ('" + ppid + "',N'" + plantname + "','" + date + "','" + hour + "','" + s1 + "','" + s2 + "','" + s3 + "','" + s4 + "','" + s5 + "','" + s6 + "','" + s7 + "','" + s8 + "','" + s9 + "','" + s10 + "','" + s11 + "','" + s12 + "','" + s13 + "','" + s14 + "','" + s15 + "','" + s16 +
                    //"','" + s17 + "','" + s18 + "','" + s19 + "','" + s20 + "','" + s21 + "','" + s22 + "','" + s23 + "','" + s24 + "','" + s26 + "','" + s27 + "','" + s28 + "','" + s29 + "','" + s31 + "','" + s36 + "','" + s37 + "','" + s38 + "','" + s39 + "','" + s40 + "','" + s41 + "','" + s42 + "','" + s43 + "')");
                    DataTable d = utilities.returntbl("insert into MonthlyBillPlant (ppid,plantname,Date,hour," + n + ")" +
                    "values ('" + ppid + "',N'" + plantname + "','" + date + "','" + hour + "','" + v + "')");


                }
            }





        }

      
        private void defaultPriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DefaultPrice m = new DefaultPrice();
            m.Show();
        }

       
        

        private void billItemToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            BillItem n = new BillItem();
            n.Show();
        }

        private void mustRunToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MustHourly n = new MustHourly();
            n.Show();
        }

        private void trainingPriceToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            TrainingPrice n = new TrainingPrice();
            n.ShowDialog();
        }

        private void fuelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FuelSetting f = new FuelSetting();
            f.ShowDialog();
        }

        private void powerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hub_Contract n = new Hub_Contract();
            n.ShowDialog();
        }

        private void bourseSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bourse n = new Bourse();
            n.ShowDialog();
        }

        private void hubToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult re1 = openFileDialog1.ShowDialog();
            if (re1 != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName; ;
                fillyearhub(path);
                MessageBox.Show("Save.");
            }
        }

        private void transmissionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult re1 = openFileDialog1.ShowDialog();
            if (re1 != DialogResult.Cancel)
            {
                string path = openFileDialog1.FileName; ;
                fillyeartrans(path);
                MessageBox.Show("Save.");
            }
        }

        public void fillyearhub(string path)
        {
            string price = "Gen";


            Excel.Application exobj = new Excel.Application();
            exobj.Visible = false;
            exobj.UserControl = true;
            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            book.Save();
            book.Close(true, book, Type.Missing);
            exobj.Workbooks.Close();
            exobj.Quit();

            //////////////////////

            //Read From Excel File
            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            objConn.Open();

            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
            objAdapter1.SelectCommand = objCmdSelect;
            DataSet objDataset1 = new DataSet();
            objAdapter1.Fill(objDataset1);
            DataTable TempTable = objDataset1.Tables[0];
            objConn.Close();
            string year = openFileDialog1.SafeFileName.Replace("Usages-final", "").Substring(0, 5).Trim();


            foreach (DataRow m in TempTable.Rows)
            {
                if (m[1].ToString().Trim() != "")
                {
                    string ppid = m[0].ToString().Trim();
                    string plantname = m[1].ToString().Trim();
                    DataTable xx = utilities.returntbl("select distinct PPID from dbo.PowerPlant");
                    foreach (DataRow b in xx.Rows)
                    {
                        if (ppid == b[0].ToString().Trim())
                        {
                            string vv = "";
                            int i = 2;
                            while (i <= 38)
                            {
                                if (i == 38)
                                {
                                    vv += m[i].ToString().Trim();
                                }
                                else
                                {
                                    vv += m[i].ToString().Trim() + "','" + m[i + 1].ToString().Trim() + "','" + m[i + 2].ToString().Trim() + "','";
                                }
                                i = i + 3;
                            }



                            DataTable dd = utilities.returntbl("delete from yearhub where year='" + year + "'and ppid='" + ppid + "'");

                            DataTable d = utilities.returntbl("insert into yearhub (year,ppid,M1Low,M1Mid,M1Peak,M2Low,M2Mid,M2Peak,M3Low,M3Mid,M3Peak,M4Low,M4Mid,M4Peak,M5Low,M5Mid,M5Peak,M6Low,M6Mid,M6Peak,M7Low,M7Mid,M7Peak,M8Low,M8Mid,M8Peak,M9Low,M9Mid,M9Peak,M10Low,M10Mid,M10Peak,M11Low,M11Mid,M11Peak,M12Low,M12Mid,M12Peak,avg)" +
                                "values ('" + year + "',N'" + ppid + "','" + vv + "')");

                        }
                    }


                }



            }
        }


        public void fillyeartrans(string path)
        {
            string price = "Gen";


            Excel.Application exobj = new Excel.Application();
            exobj.Visible = false;
            exobj.UserControl = true;
            System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Excel.Workbook book = null;
            book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            book.Save();
            book.Close(true, book, Type.Missing);
            exobj.Workbooks.Close();
            exobj.Quit();

            //////////////////////

            //Read From Excel File
            String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
            OleDbConnection objConn = new OleDbConnection(sConnectionString);
            objConn.Open();

            OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
            objAdapter1.SelectCommand = objCmdSelect;
            DataSet objDataset1 = new DataSet();
            objAdapter1.Fill(objDataset1);
            DataTable TempTable = objDataset1.Tables[0];
            objConn.Close();
            string year = openFileDialog1.SafeFileName.Replace("Loss-final", "").Substring(0, 5).Trim();


            foreach (DataRow m in TempTable.Rows)
            {
                if (m[1].ToString().Trim() != "")
                {
                    string ppid = m[0].ToString().Trim();
                    string plantname = m[1].ToString().Trim();
                    DataTable xx = utilities.returntbl("select distinct PPID from dbo.PowerPlant");
                    foreach (DataRow b in xx.Rows)
                    {
                        if (ppid == b[0].ToString().Trim())
                        {
                           
                            string vv = "";
                            int i = 2;
                            while (i <= 38)
                            {
                                if (i == 38)
                                {
                                    vv += m[i].ToString().Trim();
                                }
                                else
                                {
                                    vv += m[i].ToString().Trim() + "','" + m[i + 1].ToString().Trim() + "','" + m[i + 2].ToString().Trim() + "','";
                                }
                                i = i + 3;
                            }



                            DataTable dd = utilities.returntbl("delete from yeartrans where year='" + year + "'and ppid='" + ppid + "'");

                            DataTable d = utilities.returntbl("insert into yeartrans (year,ppid,M1Low,M1Mid,M1Peak,M2Low,M2Mid,M2Peak,M3Low,M3Mid,M3Peak,M4Low,M4Mid,M4Peak,M5Low,M5Mid,M5Peak,M6Low,M6Mid,M6Peak,M7Low,M7Mid,M7Peak,M8Low,M8Mid,M8Peak,M9Low,M9Mid,M9Peak,M10Low,M10Mid,M10Peak,M11Low,M11Mid,M11Peak,M12Low,M12Mid,M12Peak,avg)" +
                                "values ('" + year + "',N'" + ppid + "','" + vv + "')");

                        }
                    }


                }



            }
        }

       

        private void billReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrintFinancialReportEx n = new PrintFinancialReportEx();
            n.Show();
        }

        private void billingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BillWizard m = new BillWizard();
            m.ShowDialog();
        }

      

        private void salesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaleBill m = new SaleBill();
            m.ShowDialog();
        }

        private void pactualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult re1 = openFileDialog1.ShowDialog();
            if (re1 != DialogResult.Cancel)
            {

                try
                {
                    string path = openFileDialog1.FileName; ;

                    string price = "UnitState";


                    Excel.Application exobj = new Excel.Application();
                    exobj.Visible = false;
                    exobj.UserControl = true;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    Excel.Workbook book = null;
                    book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book.Save();
                    book.Close(true, book, Type.Missing);
                    exobj.Workbooks.Close();
                    exobj.Quit();

                    //////////////////////

                    //Read From Excel File
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();

                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    DataTable TempTable = objDataset1.Tables[0];
                    objConn.Close();


                    string temp = "";
                    string h = "";
                    int i = 0;
                    double[] p = new double[24];
                    foreach (DataRow m in TempTable.Rows)
                    {
                        if (m[0].ToString().Trim() != "")
                        {
                            string[] ccc = m[0].ToString().Trim().Split('-');
                            string ppid = ccc[0];
                            string plantname = ccc[1];

                            string unit = m[1].ToString().Trim();
                            string nextunit = "";
                            if (i + 1 < TempTable.Rows.Count - 1)
                            {
                                nextunit = TempTable.Rows[i + 1][1].ToString();
                            }
                            string date = m[2].ToString().Trim();
                            string start = m[3].ToString().Trim();
                            string end = m[4].ToString().Trim();
                            string power = m[5].ToString().Trim();
                            string status = m[6].ToString().Trim();





                            if (h == "")
                            {
                                if (start != "-") start = start.Substring(0, 2);
                                if (end != "-") end = end.Substring(0, 2);

                                if (start == "-") start = "0";
                                if (end == "-") end = "24";

                                start = (MyDoubleParse(start) + 1).ToString();
                                end = (MyDoubleParse(end) + 1).ToString();
                                if (end == "25") end = "24";
                            }
                            else
                            {
                                if (end != "-") end = end.Substring(0, 2);
                                if (end == "-") end = "24";
                                start = (MyDoubleParse(h) + 1).ToString();
                                end = (MyDoubleParse(end) + 1).ToString();
                                if (end == "25") end = "24";
                            }

                            int x = int.Parse(start);
                            int y = int.Parse(end);

                            for (int u = x; u <= y; u++)
                            {
                                p[u - 1] = MyDoubleParse(power);

                            }

                            h = end;


                            if (unit != nextunit)
                            {
                                DataTable dd = utilities.returntbl("delete from pactual where date='" + date + "'and ppid='" + ppid + "'and unit='" + unit + "'");
                                //insert
                                try
                                {

                                    bool enter = true;
                                    string st1 = "insert into pactual (date,ppid,unit,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15,h16,h17,h18,h19,h20,h21,h22,h23,h24)values('" + date + "','" + ppid + "','" + unit;

                                    string st2 = "";
                                    string v = "";
                                    for (int ii = 0; ii < 24; ii++)
                                    {
                                        try
                                        {
                                            v = p[ii].ToString();

                                        }
                                        catch
                                        {
                                            enter = false;
                                            MessageBox.Show("Please Fill All Hours");
                                            break;

                                        }


                                        st2 += ("','" + v);
                                        if (ii == 23) st2 += "')";
                                    }
                                    if (enter)
                                    {
                                        DataTable ind = utilities.returntbl(st1 + st2);
                                        //  MessageBox.Show("Saved.");

                                    }
                                }
                                catch
                                {
                                    MessageBox.Show("error!!!!");

                                }

                                h = "";
                                p = new double[24];
                            }



                        }
                        i++;
                    }

                    MessageBox.Show("Successfull.");
                }
                catch
                {
                    MessageBox.Show("error!!!!");
                }
                //



            }
        }

        private void powerLimmitedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult re1 = openFileDialog1.ShowDialog();
            if (re1 != DialogResult.Cancel)
            {

                try
                {
                    string path = openFileDialog1.FileName; ;

                    string price = "AvEnergy";


                    Excel.Application exobj = new Excel.Application();
                    exobj.Visible = false;
                    exobj.UserControl = true;
                    System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                    Excel.Workbook book = null;
                    book = exobj.Workbooks.Open(path, 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    book.Save();
                    book.Close(true, book, Type.Missing);
                    exobj.Workbooks.Close();
                    exobj.Quit();

                    //////////////////////

                    //Read From Excel File
                    String sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=Excel 8.0";
                    OleDbConnection objConn = new OleDbConnection(sConnectionString);
                    objConn.Open();

                    OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + price + "$]", objConn);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmdSelect;
                    DataSet objDataset1 = new DataSet();
                    objAdapter1.Fill(objDataset1);
                    DataTable TempTable = objDataset1.Tables[0];
                    objConn.Close();


                    string temp = "";
                    string h = "";
                    int i = 0;
                    double[] p = new double[24];
                    string tempplant = "";
                    string tempunit = "";
                    foreach (DataRow m in TempTable.Rows)
                    {
                        if (m[0].ToString().Trim() != "")
                        {
                            string[] ccc = m[0].ToString().Trim().Split('-');
                            string ppid = ccc[0];
                            string plantname = ccc[1];

                            string unit = m[1].ToString().Trim();
                     
                            string date = m[2].ToString().Trim();
                            string hour = m[3].ToString().Trim();
                            string S= m[4].ToString().Trim();
                            string DO = m[5].ToString().Trim();
                            string D1 = m[6].ToString().Trim();
                            string D2 = m[7].ToString().Trim();

                            if (m[0].ToString().Trim() != "-")
                            {
                                DataTable dd = utilities.returntbl("delete from powerlimmited where date='" + date + "'and ppid='" + ppid + "'and unit='" + unit + "'");
                                tempplant = ppid;
                                tempunit = unit;
                            }


                            if (unit == "-") unit = tempunit;
                            if (ppid == "") ppid = tempplant;
                         

                               
                                //insert
                                try
                                {

                                   
                                    DataTable dins = utilities.returntbl("insert into powerlimmited (date,ppid,unit,hour,S,DO,D1,D2)values('" + date + "','" + ppid + "','" + unit + "','"+hour+"','"+S+"','"+DO+"','"+D1+"','"+D2+"')");
                                                     
                                }
                                catch
                                {
                                    MessageBox.Show("error!!!!");

                                }

                             
                          
                        }
                        i++;
                    }

                    MessageBox.Show("Successfull.");
                }
                catch
                {
                    MessageBox.Show("error!!!!");
                }
                //



            }
        }

        private void priorityOnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PriorityOn m = new PriorityOn();
            m.Show();
        }

        

     
        private void daramadReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Daramad n = new Daramad("declared");
            n.Show();
        }

        private void ls2FileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LS2File f = new LS2File();
            f.ShowDialog();
        }

        private void toolStripMenuItem44_Click(object sender, EventArgs e)
        {
            MustHourly m = new MustHourly();
            m.Show();
        }

        private void consumeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Consume m = new Consume();
            m.Show();
        }

   
      
    

    
      

       

            
       
     

                           

        

        //private void lblMaintPlant_TextChanged(object sender, EventArgs e)
        //{
        //    FillMaintenanceStatusBox();
        //}

        //private void cmbMaintShownPlant_TextChanged(object sender, EventArgs e)
        //{
        //    FillStackedBarChartPlant(cmbMaintShownPlant.SelectedIndex);
        //}


       
    }
}