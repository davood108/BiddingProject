﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;
using System.IO;
using Microsoft.Win32;
using ILOG.Concert;
using ILOG.CPLEX;
using Excel = Microsoft.Office.Interop.Excel;

namespace PowerPlantProject
{

    public class ManualClassfinal
    {
        bool[,] setparam;
        int BiddingStrategySettingId;
        string ConStr = NRI.SBS.Common.ConnectionManager.ConnectionString;
        string biddingDate;
        string currentDate;
        string ppName;
        private SqlConnection oSqlConnection = null;
        private SqlCommand oSqlCommand = null;
        private SqlDataReader oSqlDataReader = null;
        private DataTable oDataTable = null;
        private string[] plant;
        int Plants_Num;
        int Package_Num;
        int[] Plant_Packages_Num;
        double[, ,] Pmax_Plant_Pack;
        double[, ,] Pmin_Plant_Pack;
        string[, ,] Outservice_Plant_Pack;
        double[, ,] Pmax_Plant_Pack_fix;
        string message = "";
        DateTime predate005 = DateTime.Now;
        DateTime predate002 = DateTime.Now;
        string smaxdate = "";
        bool IsAvg;
        double Capparam;
        int Units_Num = 0;
        public string Path;
        int[] Plant_units_Num;
        bool Ispre = false;
        bool IsMcp = false;


        public ManualClassfinal(string PPName, string CurrentDate, string BiddingDate,string path,bool isavg,double CapParam)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();
            biddingDate = BiddingDate;
            currentDate = CurrentDate;
       
            IsAvg = isavg;
            Capparam = CapParam;
            ppName = PPName.Trim();
          
            Path = path;
            if (ppName.ToLower().Trim() == "all")
            {
                oDataTable = utilities.returntbl("select distinct PPID from UnitsDataMain ");
                Plants_Num = oDataTable.Rows.Count;

                plant = new string[Plants_Num];
                for (int il = 0; il < Plants_Num; il++)
                {
                    plant[il] = oDataTable.Rows[il][0].ToString().Trim();

                }
            }
            else
            {
                SqlCommand MyCom1 = new SqlCommand();
                MyCom1.Connection = myConnection;
                MyCom1.CommandText = "SELECT @num=PPID FROM PowerPlant WHERE PPName=@name";
                MyCom1.Parameters.Add("@num", SqlDbType.NChar, 10);
                MyCom1.Parameters.Add("@name", SqlDbType.NChar, 20);
                MyCom1.Parameters["@num"].Direction = ParameterDirection.Output;
                MyCom1.Parameters["@name"].Value = ppName.Trim();
                MyCom1.ExecuteNonQuery();

                Plants_Num = 1;
                plant = new string[1];
                plant[0] = MyCom1.Parameters["@num"].Value.ToString().Trim();


            }

            /////////

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = myConnection;
            MyCom.CommandText = "select @id=max(id) from dbo.BiddingStrategySetting " +
                " where plant=@ppname AND  CurrentDate=@curDate AND BiddingDate=@bidDate";
            MyCom.Parameters.Add("@id", SqlDbType.Int);
            MyCom.Parameters.Add("@ppname", SqlDbType.NChar, 20);
            MyCom.Parameters.Add("@curDate", SqlDbType.Char, 10);
            MyCom.Parameters.Add("@bidDate", SqlDbType.Char, 10);
            MyCom.Parameters["@id"].Direction = ParameterDirection.Output;
            MyCom.Parameters["@ppname"].Value = ppName.Trim();
            MyCom.Parameters["@curDate"].Value = CurrentDate;
            MyCom.Parameters["@bidDate"].Value = BiddingDate;
            MyCom.ExecuteNonQuery();
            myConnection.Close();

            DataTable odid = utilities.returntbl("select max(id) from dbo.BiddingStrategySetting " +
                " where plant='"+ppName.Trim()+"' AND  CurrentDate='"+CurrentDate+"' AND BiddingDate='"+BiddingDate+"'");
            if (odid.Rows[0][0].ToString() == "")
            {

                odid = utilities.returntbl("select id from dbo.BiddingStrategySetting " +
                " where plant='" + ppName.Trim() + "' AND  BiddingDate<='" + BiddingDate + "'order by id desc ");
            }
            BiddingStrategySettingId = int.Parse(odid.Rows[0][0].ToString());
        }



    

        private string buildmaxdate(string[] arrmax)
        {
            string maxdate = null;
            for (int fd = 0; fd < arrmax.Length; fd++)
            {
                if (maxdate == null || string.Compare(maxdate, arrmax[fd]) < 0)
                    maxdate = arrmax[fd];
            }
            return maxdate;

        }

        public bool value()
        {

            DataTable dts1 = null;
            setparam = new bool[Plants_Num, 8];
            if (ppName != "All")
            {
                dts1 = utilities.returntbl("select * from BidRunSetting where Plant='" + ppName + "'");
                if (dts1.Rows.Count > 0)
                {
                    setparam[0, 0] = MyboolParse(dts1.Rows[0]["FlagOpf"].ToString());
                    setparam[0, 1] = MyboolParse(dts1.Rows[0]["FlagSensetivity"].ToString());
                    setparam[0, 2] = MyboolParse(dts1.Rows[0]["FlagMarketPower"].ToString());
                    setparam[0, 3] = MyboolParse(dts1.Rows[0]["FlagLossPayment"].ToString());
                    setparam[0, 4] = MyboolParse(dts1.Rows[0]["FlagPresolve"].ToString());
                    setparam[0, 5] = MyboolParse(dts1.Rows[0]["FlagCostFunction"].ToString());
                    setparam[0, 6] = MyboolParse(dts1.Rows[0]["FairPlay"].ToString());
                    setparam[0, 7] = MyboolParse(dts1.Rows[0]["LMP"].ToString());

                }
            }
            else
            {

                DataTable dd = utilities.returntbl("select PPName from dbo.PowerPlant");
                for (int i = 0; i < Plants_Num; i++)
                {
                    dts1 = utilities.returntbl("select * from BidRunSetting where Plant='" + dd.Rows[i][0].ToString().Trim() + "'");
                    if (dts1.Rows.Count > 0)
                    {
                        setparam[i, 0] = MyboolParse(dts1.Rows[0]["FlagOpf"].ToString());
                        setparam[i, 1] = MyboolParse(dts1.Rows[0]["FlagSensetivity"].ToString());
                        setparam[i, 2] = MyboolParse(dts1.Rows[0]["FlagMarketPower"].ToString());
                        setparam[i, 3] = MyboolParse(dts1.Rows[0]["FlagLossPayment"].ToString());
                        setparam[i, 4] = MyboolParse(dts1.Rows[0]["FlagPresolve"].ToString());
                        setparam[i, 5] = MyboolParse(dts1.Rows[0]["FlagCostFunction"].ToString());
                        setparam[i, 6] = MyboolParse(dts1.Rows[0]["FairPlay"].ToString());
                        setparam[i, 7] = MyboolParse(dts1.Rows[0]["LMP"].ToString());

                    }
                }
            }

            //opf result//------------------------------------------------------------------
            double[,] opfvalue = new double[Plants_Num, 24];

            for (int j = 0; j < Plants_Num; j++)
            {
                DataTable dopf = utilities.returntbl("select * from dbo.PowerOpf where Date='" + biddingDate + "' and PPID='" + plant[j] + "'");
                foreach (DataRow m in dopf.Rows)
                {
                    int h = int.Parse(m[2].ToString());
                    opfvalue[j, h - 1] = MyDoubleParse(m[3].ToString());

                }
                if (plant[j] == "206")
                {

                    for (int h = 0; h < 24; h++)
                    {
                        DataTable dopf2 = utilities.returntbl("select sum(Value) from dbo.PowerOpf where Date='" + biddingDate + "' and Hour='" + (h + 1) + "' and PPID like '" + plant[j] + '%' + "'");
                        foreach (DataRow mm in dopf2.Rows)
                        {

                            opfvalue[j, h] = MyDoubleParse(mm[0].ToString());

                        }
                    }
                }
                if (Findccplant(plant[j]))
                {

                    for (int h = 0; h < 24; h++)
                    {
                        DataTable dopf2 = utilities.returntbl("select sum(Value) from dbo.PowerOpf where Date='" + biddingDate + "' and Hour='" + (h + 1) + "' and (PPID='" + plant[j] + "' or PPID='" + (int.Parse(plant[j]) + 1) + "')");
                        foreach (DataRow mm in dopf2.Rows)
                        {

                            opfvalue[j, h] = MyDoubleParse(mm[0].ToString());

                        }
                    }
                }
            }
            //------------------------------------------------------------------------------
            bool IsWeekEnd = false;
            bool yeterdayweekend = false;
            bool two_yeterdayweekend = false;

            DateTime yestdate = PersianDateConverter.ToGregorianDateTime(biddingDate).AddDays(-1);
            DateTime twoyesterday = PersianDateConverter.ToGregorianDateTime(biddingDate).AddDays(-2);
            string peryesterday = new PersianDate(yestdate).ToString("d");
            string pretwoyesterday = new PersianDate(twoyesterday).ToString("d");


            DataTable dtweekend = utilities.returntbl("select distinct Date from dbo.WeekEnd");

            foreach (DataRow nrow in dtweekend.Rows)
            {

                if (nrow[0].ToString().Trim() == biddingDate)
                {
                    IsWeekEnd = true;
                    break;

                }


            }
            //----------------------------------yesterday is weekend-----------------------------//

            foreach (DataRow nrow in dtweekend.Rows)
            {
                if (nrow[0].ToString().Trim() == peryesterday.Trim())
                {
                    yeterdayweekend = true;
                    break;
                }
                if (nrow[0].ToString().Trim() == pretwoyesterday.Trim())
                {
                    two_yeterdayweekend = true;
                    break;
                }
            }
            //-----------------------------------------------------------------------------------------      

            string strCmd = "select Max (PackageCode) from UnitsDataMain ";
            if (Plants_Num == 1)
                strCmd += " where ppid=" + plant[0];
            oDataTable = utilities.returntbl(strCmd);
            Package_Num = (int)oDataTable.Rows[0][0];

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //Unit Number
            Plant_units_Num = new int[Plants_Num];
            for (int re = 0; re < Plants_Num; re++)
            {
                oDataTable = utilities.returntbl("select UnitCode from UnitsDataMain where PPID='" + plant[re] + "'");

                Plant_units_Num[re] = oDataTable.Rows.Count;
                if (Units_Num <= oDataTable.Rows.Count)
                {
                    Units_Num = oDataTable.Rows.Count;
                }

            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //Fixed Set :
            int CplexPrice = 0;
            int CplexPower = 0;
            int Hour = 24;
            int Dec_Num = 4;
            int Data_Num = 26;
            int Start_Num = 48;
            int Mmax = 2;
            int Step_Num = 10;
            int Xtrain = 201;
            int Power_Num = 4;
            int oldday = 3;
            int Const_selectdays = 7;
            int GastostartSteam = 1;

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] Unit_Dec = new string[Plants_Num, Units_Num, Dec_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Dec_Num; k++)
                    {
                        switch (k)
                        {
                            case 0:
                                oDataTable = utilities.returntbl("select UnitType  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype asc");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString().Trim();
                                break;
                            case 1:
                                oDataTable = utilities.returntbl("select PackageType  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype asc");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString().Trim();
                                break;
                            case 2:
                                Unit_Dec[j, i, k] = "Gas";
                                break;
                            case 3:
                                oDataTable = utilities.returntbl("select  SecondFuel  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype asc");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString().Trim();
                                break;
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int b = 0;

            // detect max date in database...................................  
            oDataTable = utilities.returntbl("select Date from BaseData");
            int ndate = oDataTable.Rows.Count;
            string[] arrmaxdate1 = new string[ndate];
            for (b = 0; b < ndate; b++)
            {
                arrmaxdate1[b] = oDataTable.Rows[b][0].ToString().Trim();
            }

            //string smaxdate = buildmaxdate(arrmaxdate1);

            DataTable dtsmaxdate = utilities.returntbl("select * from dbo.BaseData where Date<='" + biddingDate + "'order by BaseID desc");
            if (dtsmaxdate.Rows.Count > 0)
            {
                string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
                int ib = 0;
                foreach (DataRow m in dtsmaxdate.Rows)
                {
                    arrbasedata[ib] = m["Date"].ToString();
                    ib++;
                }
                smaxdate = buildmaxdate(arrbasedata);
            }
            else
            {
                dtsmaxdate = utilities.returntbl("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
                smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
            }

            oDataTable = utilities.returntbl("select GasPrice,MazutPrice,GasOilPrice from BaseData where Date= '" + smaxdate + "'order by BaseID desc");

            double[] FuelPrice_Free = new double[3];
            for (b = 0; b < 3; b++)
            {
                if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][b].ToString() != "")
                {
                    FuelPrice_Free[b] = MyDoubleParse(oDataTable.Rows[0][b].ToString());
                }
                else
                {
                    FuelPrice_Free[0] = 690;
                    FuelPrice_Free[1] = 5053;
                    FuelPrice_Free[2] = 7057;
                }

            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int bb = 0;
            // detect max date in database...................................  
            oDataTable = utilities.returntbl("select Date from EfficiencyMarket");
            int ndate2 = oDataTable.Rows.Count;
            string[] arrmaxdate2 = new string[ndate];
            for (bb = 0; bb < ndate2; bb++)
            {
                arrmaxdate2[bb] = oDataTable.Rows[bb][0].ToString().Trim();
            }

            string smaxdate2 = buildmaxdate(arrmaxdate2);


            oDataTable = utilities.returntbl(" select Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24" +
            " from EfficiencyMarket where Date like '" + smaxdate2 + '%' + "'");
            double[] Marketefficient = new double[24];
            for (bb = 0; bb < 24; bb++)
            {
                if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][bb].ToString() != "")
                {
                    Marketefficient[bb] = MyDoubleParse(oDataTable.Rows[0][bb].ToString());
                }
                else
                {
                    Marketefficient[bb] = 35;
                }

            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            oDataTable = utilities.returntbl("select GasProduction,SteamProduction,CCProduction from BaseData where Date = '" + smaxdate + "'order by BaseID desc");
            double[] ProductionPrice = new double[3];
            for (b = 0; b < 3; b++)
            {
                if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][b].ToString() != "")
                {
                    ProductionPrice[b] = MyDoubleParse(oDataTable.Rows[0][b].ToString());
                }
                else
                {
                    ProductionPrice[0] = 15000;
                    ProductionPrice[1] = 15000;
                    ProductionPrice[2] = 15000;
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            oDataTable = utilities.returntbl("select GasSubsidiesPrice,MazutSubsidiesPrice,GasOilSubsidiesPrice from BaseData where Date = '" + smaxdate + "'order by BaseID desc");
            double[] FuelPrice = new double[3];
            for (b = 0; b < 3; b++)
            {
                if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][b].ToString() != "")
                {
                    FuelPrice[b] = MyDoubleParse(oDataTable.Rows[0][b].ToString());
                }
                else
                {
                    FuelPrice[0] = 27.9;
                    FuelPrice[1] = 57.467;
                    FuelPrice[2] = 30.467;
                }

            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            oDataTable = utilities.returntbl("select MarketPriceMax from BaseData where Date = '" + smaxdate + "'order by BaseID desc");
            double[] CapPrice = new double[2];

            if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][0].ToString() != "")
            {
                CapPrice[0] = MyDoubleParse(oDataTable.Rows[0][0].ToString());
                CapPrice[0] = CapPrice[0] - 200;
                CapPrice[1] = CapPrice[0] * 0.0006061;
            }
            else
            {
                CapPrice[0] = 329800;

                CapPrice[1] = 200;
            }

            //Error bid for distance is accept.
            double[] ErrorBid = new double[1];
            ErrorBid[0] = CapPrice[1] * 5;
            string[] Run = new string[2];
            Run[0] = "Automatic";
            Run[1] = "Customize";


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            Plant_Packages_Num = new int[Plants_Num];

            List<string>[] PackageTypes = new List<string>[Plants_Num];

            for (int re = 0; re < Plants_Num; re++)
            {
                oDataTable = utilities.returntbl("select Max(PackageCode) from UnitsDataMain where  PPID='" + plant[re] + "'");
                Plant_Packages_Num[re] = (int)oDataTable.Rows[0][0];

                oDataTable = utilities.returntbl("select distinct PackageType from UnitsDataMain where  PPID='" + plant[re] + "'");
                //Plant_Packages_Num[re] = (int)oDataTable.Rows.count;

                PackageTypes[re] = new List<string>();

                foreach (string typeName in Enum.GetNames(typeof(PackageTypePriority)))
                {
                    //////////// have to make sure later whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                    bool found = false;
                    foreach (DataRow row in oDataTable.Rows)
                        if (row["PackageType"].ToString().Trim().Contains(typeName))
                            found = true;

                    if (found)
                        PackageTypes[re].Add(typeName);
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,] Package = new string[Plants_Num, Package_Num];

            for (int j = 0; j < Plants_Num; j++)
            {
                string cmd = "select PackageType from dbo.UnitsDataMain where PPID=" + plant[j] +
                             " AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%')AND(UnitCode not like 'Gas cc%')))";
                oDataTable = utilities.returntbl(cmd);

                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    Package[j, k] = oDataTable.Rows[k][0].ToString().Trim();
                }

            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[,] Unit = new string[Plants_Num, Units_Num];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    oDataTable = utilities.returntbl("select UnitCode from UnitsDataMain where PPID='" + plant[j] + "'ORDER BY packagecode,unittype asc");
                    Unit[j, i] = oDataTable.Rows[i][0].ToString().Trim();
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Toreranceup = new double[Plants_Num, Units_Num, Hour];
            double[, ,] ToreranceDown = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Limit_variance_Down = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Limit_variance = new double[Plants_Num, Units_Num, Hour];

            double Factor_Dispatch_Befor = 0.9;
            double Factor_Price = 0.997;
            double Egas = 500;
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            double[, ,] Unit_Data = new double[Plants_Num, Units_Num, Data_Num];
            // 0 : Package
            // 1 : RampUp
            // 2 : RampDown
            // 3 : Pmin
            // 4 : pmax
            // 5 : RampStart&shut
            // 6 : MinOn
            // 7 : MinOff
            // 8 :  Am
            // 9 :  Bm
            // 10 : Cm
            // 11 : Bmain
            // 12 : Cmain
            // 13 : Dmain
            // 14 : Variable cost
            // 15 : Fixed Cost
            // 16 : Tcold
            // 17 : Thot
            // 18 : Cold start cost
            // 19 : Hot start cost

            for (int j = 0; j < Plants_Num; j++)
            {
                oDataTable = utilities.returntbl("select *  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype asc");
                if (oDataTable.Rows.Count > 0)
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int k = 0; k < Data_Num; k++)
                        {
                            switch (k)
                            {
                                case 0:

                                    Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PackageCode"].ToString());
                                    break;
                                case 1:
                                    if (oDataTable.Rows[i]["RampUpRate"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["RampUpRate"].ToString());
                                        Unit_Data[j, i, k] = 30 * Unit_Data[j, i, k];
                                    }
                                    break;
                                case 2:
                                    Unit_Data[j, i, k] = Unit_Data[j, i, 1];
                                    break;

                                case 3:

                                    Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PMin"].ToString());
                                    break;

                                case 4:

                                    Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PMax"].ToString());
                                    break;
                                case 5:
                                    double pow = Unit_Data[j, i, 1];
                                    if (Unit_Data[j, i, 3] > pow)
                                    {
                                        Unit_Data[j, i, k] = Unit_Data[j, i, 3];
                                    }
                                    else
                                    {
                                        Unit_Data[j, i, k] = pow;
                                    }
                                    break;
                                case 6:
                                    if (oDataTable.Rows[i]["TUp"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TUp"].ToString());
                                    }
                                    else Unit_Data[j, i, k] = 3;

                                    break;
                                case 7:
                                    if (oDataTable.Rows[i]["TDown"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TDown"].ToString());

                                    }
                                    else Unit_Data[j, i, k] = 10;

                                    break;

                                case 8:
                                    if (oDataTable.Rows[i]["PrimaryFuelAmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PrimaryFuelAmargin"].ToString());
                                    }
                                    break;
                                case 9:
                                    if (oDataTable.Rows[i]["PrimaryFuelBmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PrimaryFuelBmargin"].ToString());
                                    }
                                    break;
                                case 10:
                                    if (oDataTable.Rows[i]["PrimaryFuelCmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PrimaryFuelCmargin"].ToString());
                                    }
                                    break;
                                case 11:
                                    if (oDataTable.Rows[i]["BMaintenance"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["BMaintenance"].ToString());
                                    }
                                    break;

                                case 12:
                                    if (oDataTable.Rows[i]["CMaintenance"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["CMaintenance"].ToString());
                                    }
                                    break;
                                case 13:
                                    Unit_Data[j, i, k] = Unit_Data[j, i, 12];

                                    break;
                                case 14:

                                    if (oDataTable.Rows[i]["VariableCost"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["VariableCost"].ToString());
                                    }
                                    break;
                                case 15:
                                    if (oDataTable.Rows[i]["FixedCost"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["FixedCost"].ToString());
                                    }
                                    break;

                                case 16:
                                    if (oDataTable.Rows[i]["TStartCold"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TStartCold"].ToString());
                                    }
                                    break;
                                case 17:

                                    if (oDataTable.Rows[i]["TStartHot"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TStartHot"].ToString());
                                    }

                                    break;
                                case 18:
                                    if (oDataTable.Rows[i]["CostStartHot"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["CostStartHot"].ToString());
                                    }


                                    break;

                                case 19:
                                    if (oDataTable.Rows[i]["CostStartCold"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["CostStartCold"].ToString());
                                    }

                                    break;

                                case 20:
                                    if (oDataTable.Rows[i]["SecondFuelAmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["SecondFuelAmargin"].ToString());
                                    }

                                    break;
                                case 21:
                                    if (oDataTable.Rows[i]["SecondFuelBmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["SecondFuelBmargin"].ToString());
                                    }

                                    break;
                                case 22:
                                    if (oDataTable.Rows[i]["SecondFuelCmargin"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["SecondFuelCmargin"].ToString());
                                    }
                                    break;
                                case 23:
                                    if (oDataTable.Rows[i]["HeatValuePrimaryFuel"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["HeatValuePrimaryFuel"].ToString());
                                    }
                                    break;
                                case 24:
                                    if (oDataTable.Rows[i]["HeatValueSecondaryFuel"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["HeatValueSecondaryFuel"].ToString());
                                    }
                                    break;
                                case 25:
                                    if (oDataTable.Rows[i]["CapitalCost"].ToString() != "")
                                    {
                                        Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["CapitalCost"].ToString());
                                    }
                                    break;
                            }
                        }
                    }
            }
            //Rampe rate is correct :
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Data_Num; k++)
                    {
                        if (Unit_Data[j, i, 1] > (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]))
                        {
                            Unit_Data[j, i, 1] = (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]);
                            Unit_Data[j, i, 2] = (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]);
                        }
                    }
                }
            }
            //Rampe rate Startup is correct :
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Data_Num; k++)
                    {
                        if (Unit_Data[j, i, 5] > (Unit_Data[j, i, 4]))
                        {
                            Unit_Data[j, i, 5] = (Unit_Data[j, i, 4]);
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            // package yeki kam she :
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Unit_Data[j, i, 0] = Unit_Data[j, i, 0] - 1;
                }
            }

            ////////////////////////////khorsand////////////////////////////////
            checkdispatch();

            ////////////////////////////add array second-primary////////////////////////////////
            double[, , ,] SecondaryParam = new double[Plants_Num, Units_Num, 2, 6];
            double[, , ,] PrimayParam = new double[Plants_Num, Units_Num, 2, 6];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    DataTable PRIMARYTABLE = utilities.returntbl("select PrimaryPowerA,PrimaryPowerB,PrimaryPowerC,PrimaryHeatRateA,PrimaryHeatRateB,PrimaryHeatRateC from dbo.UnitsDataMain where PPID='" + plant[j] + "' and UnitCode='" + Unit[j, i] + "'");
                    DataTable SECONDARYTABLE = utilities.returntbl("select SecondaryPowerA,SecondaryPowerB,SecondaryPowerC,SecondaryHeatRateA,SecondaryHeatRateB,SecondaryHeatRateC  from dbo.UnitsDataMain where PPID='" + plant[j] + "' and UnitCode='" + Unit[j, i] + "'");

                    for (int l = 0; l < 2; l++)
                    {
                        for (int m = 0; m < 6; m++)
                        {

                            if (l == 0)
                            {
                                switch (m)
                                {
                                    case 0:
                                        if (PRIMARYTABLE.Rows[0][0].ToString() != "")
                                        {
                                            PrimayParam[j, i, 0, 0] = MyDoubleParse(PRIMARYTABLE.Rows[0][0].ToString());
                                        }
                                        break;
                                    case 1:
                                        if (PRIMARYTABLE.Rows[0][1].ToString() != "")
                                        {
                                            PrimayParam[j, i, 0, 1] = MyDoubleParse(PRIMARYTABLE.Rows[0][1].ToString());
                                        }
                                        break;
                                    case 2:
                                        if (PRIMARYTABLE.Rows[0][2].ToString() != "")
                                        {
                                            PrimayParam[j, i, 0, 2] = MyDoubleParse(PRIMARYTABLE.Rows[0][2].ToString());
                                        }
                                        break;
                                    case 3:
                                        if (PRIMARYTABLE.Rows[0][3].ToString() != "")
                                        {
                                            PrimayParam[j, i, 0, 3] = MyDoubleParse(PRIMARYTABLE.Rows[0][3].ToString());
                                        }
                                        break;
                                    case 4:
                                        if (PRIMARYTABLE.Rows[0][4].ToString() != "")
                                        {
                                            PrimayParam[j, i, 0, 4] = MyDoubleParse(PRIMARYTABLE.Rows[0][4].ToString());
                                        }
                                        break;
                                    case 5:
                                        if (PRIMARYTABLE.Rows[0][5].ToString() != "")
                                        {
                                            PrimayParam[j, i, 0, 5] = MyDoubleParse(PRIMARYTABLE.Rows[0][5].ToString());
                                        }
                                        break;
                                }

                            }

                            else if (l == 1)
                            {
                                switch (m)
                                {
                                    case 0:
                                        if (SECONDARYTABLE.Rows[0][0].ToString() != "")
                                        {
                                            SecondaryParam[j, i, 1, 0] = MyDoubleParse(SECONDARYTABLE.Rows[0][0].ToString());
                                        }
                                        break;
                                    case 1:
                                        if (SECONDARYTABLE.Rows[0][1].ToString() != "")
                                        {
                                            SecondaryParam[j, i, 1, 1] = MyDoubleParse(SECONDARYTABLE.Rows[0][1].ToString());
                                        }
                                        break;
                                    case 2:
                                        if (SECONDARYTABLE.Rows[0][2].ToString() != "")
                                        {
                                            SecondaryParam[j, i, 1, 2] = MyDoubleParse(SECONDARYTABLE.Rows[0][2].ToString());
                                        }
                                        break;
                                    case 3:
                                        if (SECONDARYTABLE.Rows[0][3].ToString() != "")
                                        {
                                            SecondaryParam[j, i, 1, 3] = MyDoubleParse(SECONDARYTABLE.Rows[0][3].ToString());
                                        }
                                        break;
                                    case 4:
                                        if (SECONDARYTABLE.Rows[0][4].ToString() != "")
                                        {
                                            SecondaryParam[j, i, 1, 4] = MyDoubleParse(SECONDARYTABLE.Rows[0][4].ToString());
                                        }
                                        break;
                                    case 5:
                                        if (SECONDARYTABLE.Rows[0][5].ToString() != "")
                                        {
                                            SecondaryParam[j, i, 1, 5] = MyDoubleParse(SECONDARYTABLE.Rows[0][5].ToString());
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            double save_cpu = 0;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (SecondaryParam[j, i, 1, 0] > SecondaryParam[j, i, 1, 1])
                    {
                        save_cpu = SecondaryParam[j, i, 1, 1];
                        SecondaryParam[j, i, 1, 1] = SecondaryParam[j, i, 1, 0];
                        SecondaryParam[j, i, 1, 0] = save_cpu;
                    }
                    if (SecondaryParam[j, i, 1, 1] > SecondaryParam[j, i, 1, 2])
                    {
                        save_cpu = SecondaryParam[j, i, 1, 2];
                        SecondaryParam[j, i, 1, 2] = SecondaryParam[j, i, 1, 1];
                        SecondaryParam[j, i, 1, 1] = save_cpu;
                    }
                    if (SecondaryParam[j, i, 1, 0] > SecondaryParam[j, i, 1, 1])
                    {
                        save_cpu = SecondaryParam[j, i, 1, 1];
                        SecondaryParam[j, i, 1, 1] = SecondaryParam[j, i, 1, 0];
                        SecondaryParam[j, i, 1, 0] = save_cpu;
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (SecondaryParam[j, i, 1, 3] > SecondaryParam[j, i, 1, 4])
                    {
                        save_cpu = SecondaryParam[j, i, 1, 4];
                        SecondaryParam[j, i, 1, 3] = SecondaryParam[j, i, 1, 4];
                        SecondaryParam[j, i, 1, 4] = save_cpu;
                    }
                    if (SecondaryParam[j, i, 1, 4] > SecondaryParam[j, i, 1, 5])
                    {
                        save_cpu = SecondaryParam[j, i, 1, 5];
                        SecondaryParam[j, i, 1, 4] = SecondaryParam[j, i, 1, 5];
                        SecondaryParam[j, i, 1, 5] = save_cpu;
                    }
                    if (SecondaryParam[j, i, 1, 3] > SecondaryParam[j, i, 1, 4])
                    {
                        save_cpu = SecondaryParam[j, i, 1, 4];
                        SecondaryParam[j, i, 1, 3] = SecondaryParam[j, i, 1, 4];
                        SecondaryParam[j, i, 1, 4] = save_cpu;
                    }
                }
            }
            //****************************************************************
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (PrimayParam[j, i, 0, 0] > PrimayParam[j, i, 0, 1])
                    {
                        save_cpu = SecondaryParam[j, i, 0, 1];
                        PrimayParam[j, i, 0, 1] = PrimayParam[j, i, 0, 0];
                        PrimayParam[j, i, 0, 0] = save_cpu;
                    }
                    if (PrimayParam[j, i, 0, 1] > PrimayParam[j, i, 0, 2])
                    {
                        save_cpu = PrimayParam[j, i, 0, 2];
                        PrimayParam[j, i, 0, 2] = PrimayParam[j, i, 0, 1];
                        PrimayParam[j, i, 0, 1] = save_cpu;
                    }
                    if (PrimayParam[j, i, 0, 0] > PrimayParam[j, i, 0, 1])
                    {
                        save_cpu = PrimayParam[j, i, 0, 1];
                        PrimayParam[j, i, 0, 1] = PrimayParam[j, i, 0, 0];
                        PrimayParam[j, i, 0, 0] = save_cpu;
                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (PrimayParam[j, i, 0, 3] > PrimayParam[j, i, 0, 4])
                    {
                        save_cpu = PrimayParam[j, i, 0, 4];
                        PrimayParam[j, i, 0, 3] = PrimayParam[j, i, 0, 4];
                        PrimayParam[j, i, 0, 4] = save_cpu;
                    }
                    if (PrimayParam[j, i, 0, 4] > PrimayParam[j, i, 0, 5])
                    {
                        save_cpu = PrimayParam[j, i, 0, 5];
                        PrimayParam[j, i, 0, 4] = PrimayParam[j, i, 0, 5];
                        PrimayParam[j, i, 0, 5] = save_cpu;
                    }
                    if (PrimayParam[j, i, 1, 3] > PrimayParam[j, i, 1, 4])
                    {
                        save_cpu = PrimayParam[j, i, 0, 4];
                        PrimayParam[j, i, 0, 3] = PrimayParam[j, i, 0, 4];
                        PrimayParam[j, i, 0, 4] = save_cpu;
                    }
                }
            }
            double[, ,] SecondFuel_power = new double[Plants_Num, Units_Num, 3];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int m = 0; m < 3; m++)
                    {
                        SecondFuel_power[j, i, m] = SecondaryParam[j, i, 1, m];
                    }
                }
            }
            double[, ,] SecondFuel_Fuel = new double[Plants_Num, Units_Num, 3];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int m = 0; m < 3; m++)
                    {
                        SecondFuel_Fuel[j, i, m] = SecondaryParam[j, i, 1, m + 3];
                    }
                }
            }
            double[, ,] PrimayFuel_power = new double[Plants_Num, Units_Num, 3];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int m = 0; m < 3; m++)
                    {
                        PrimayFuel_power[j, i, m] = PrimayParam[j, i, 0, m];
                    }
                }
            }
            double[, ,] PrimayFuel_Fuel = new double[Plants_Num, Units_Num, 3];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int m = 0; m < 3; m++)
                    {
                        PrimayFuel_Fuel[j, i, m] = PrimayParam[j, i, 0, m + 3];
                    }
                }
            }
            double[, ,] FixedCost = new double[Plants_Num, Units_Num, 2];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    FixedCost[j, i, 0] = 0;
                    FixedCost[j, i, 1] = 0;
                    if ((PrimayFuel_Fuel[j, i, 0] > 0) & (PrimayFuel_power[j, i, 0] > 0))
                    {
                        FixedCost[j, i, 0] = PrimayFuel_Fuel[j, i, 0] * Unit_Data[j, i, 3] / PrimayFuel_power[j, i, 0];
                    }
                    if ((SecondFuel_Fuel[j, i, 0] > 0) & (SecondFuel_power[j, i, 0] > 0))
                    {
                        FixedCost[j, i, 1] = SecondFuel_Fuel[j, i, 0] * Unit_Data[j, i, 3] / SecondFuel_power[j, i, 0];
                    }
                }
            }
            double[, ,] Efficient = new double[Plants_Num, Units_Num, 2];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Efficient[j, i, 0] = 0;
                    Efficient[j, i, 1] = 0;
                    if ((PrimayFuel_Fuel[j, i, 2] > 0) & (PrimayFuel_power[j, i, 2] > 0))
                    {
                        Efficient[j, i, 0] = (860 * PrimayFuel_power[j, i, 2] * 1000 * 100) / (Unit_Data[j, i, 23] * PrimayFuel_Fuel[j, i, 2]);
                    }
                    else
                    {
                        if (Unit_Data[j, i, 8] == 0)
                        {
                            Efficient[j, i, 0] = 0;
                        }
                        else
                        {
                            Efficient[j, i, 0] = (860 * Unit_Data[j, i, 4] * 1000 * 100) / ((Unit_Data[j, i, 8] * Unit_Data[j, i, 4] * Unit_Data[j, i, 4] + Unit_Data[j, i, 9] * Unit_Data[j, i, 4] + Unit_Data[j, i, 10]) * Unit_Data[j, i, 23]);
                        }
                    }
                    if ((SecondFuel_Fuel[j, i, 2] > 0) & (SecondFuel_power[j, i, 2] > 0))
                    {
                        Efficient[j, i, 1] = (860 * SecondFuel_power[j, i, 2] * 1000 * 100) / (Unit_Data[j, i, 24] * SecondFuel_Fuel[j, i, 2]);
                    }
                    else
                    {
                        if (Unit_Data[j, i, 20] == 0)
                        {
                            Efficient[j, i, 1] = 0;
                        }
                        else
                        {
                            Efficient[j, i, 1] = (860 * Unit_Data[j, i, 4] * 1000 * 100) / ((Unit_Data[j, i, 20] * Unit_Data[j, i, 4] * Unit_Data[j, i, 4] + Unit_Data[j, i, 21] * Unit_Data[j, i, 4] + Unit_Data[j, i, 22]) * Unit_Data[j, i, 24]);
                        }
                    }
                }
            }

            double[, , ,] MarginCost = new double[Plants_Num, Units_Num, 2, 2];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int m = 0; m < 2; m++)
                    {
                        MarginCost[j, i, 0, m] = 0;
                        MarginCost[j, i, 1, m] = 0;
                        if ((PrimayFuel_Fuel[j, i, m] > 0) & (PrimayFuel_power[j, i, m] > 0) & (PrimayFuel_Fuel[j, i, m + 1] > 0) & (PrimayFuel_power[j, i, m + 1] > 0))
                        {
                            MarginCost[j, i, 0, m] = (PrimayFuel_Fuel[j, i, m + 1] - PrimayFuel_Fuel[j, i, m]) / (PrimayFuel_power[j, i, m + 1] - PrimayFuel_power[j, i, m]);
                        }
                        if ((SecondFuel_Fuel[j, i, m] > 0) & (SecondFuel_power[j, i, m] > 0) & (SecondFuel_Fuel[j, i, m + 1] > 0) & (SecondFuel_power[j, i, m + 1] > 0))
                        {
                            MarginCost[j, i, 1, m] = (SecondFuel_Fuel[j, i, m + 1] - SecondFuel_Fuel[j, i, m]) / (SecondFuel_power[j, i, m + 1] - SecondFuel_power[j, i, m]);
                        }
                    }
                }
            }
            double[, ,] Cost_Web = new double[Plants_Num, Units_Num, 2];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    Cost_Web[j, i, 0] = 0;
                    Cost_Web[j, i, 1] = 0;
                    if ((PrimayFuel_Fuel[j, i, 0] > 0) & (PrimayFuel_power[j, i, 0] > 0) & (PrimayFuel_Fuel[j, i, 1] > 0) & (PrimayFuel_power[j, i, 1] > 0) & (PrimayFuel_Fuel[j, i, 2] > 0) & (PrimayFuel_power[j, i, 2] > 0))
                    {
                        Cost_Web[j, i, 0] = 1;
                    }
                    if ((SecondFuel_Fuel[j, i, 0] > 0) & (SecondFuel_power[j, i, 0] > 0) & (SecondFuel_Fuel[j, i, 1] > 0) & (SecondFuel_power[j, i, 1] > 0) & (SecondFuel_Fuel[j, i, 2] > 0) & (SecondFuel_power[j, i, 2] > 0))
                    {
                        Cost_Web[j, i, 1] = 1;
                    }
                }
            }

            ////////////////////////////////////////////////////////////////
            DataTable tablebasedata = utilities.returntbl("select GasHeatValueAverage,MazutHeatValueAverage,GasOilHeatValueAverage from dbo.BaseData where Date='" + smaxdate + "'order by BaseID desc");
            double[] HeatAverage = new double[3];

            //[0]GasHeatValueAverage
            //[1]MazutHeatValueAverage
            //[2]GasOilHeatValueAverage
            if (tablebasedata.Rows[0][0].ToString() != "")
            {
                HeatAverage[0] = MyDoubleParse(tablebasedata.Rows[0][0].ToString());
            }
            if (tablebasedata.Rows[0][1].ToString() != "")
            {

                HeatAverage[1] = MyDoubleParse(tablebasedata.Rows[0][1].ToString());

            }
            if (tablebasedata.Rows[0][2].ToString() != "")
            {

                HeatAverage[2] = MyDoubleParse(tablebasedata.Rows[0][2].ToString());

            }
            double[] Min_Plant = new double[Plants_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                if (plant[j] == "101")
                {
                    Min_Plant[j] = 100000;
                }
                if (plant[j] == "104")
                {
                    Min_Plant[j] = 150000;
                }
                if (plant[j] == "131")
                {
                    Min_Plant[j] = 100000;
                }
                if (plant[j] == "133")
                {
                    Min_Plant[j] = 100000;
                }
                if (plant[j] == "138")
                {
                    Min_Plant[j] = 250000;
                }
                if (plant[j] == "144")
                {
                    Min_Plant[j] = 100000;
                }
                if (plant[j] == "149")
                {
                    Min_Plant[j] = 10000;
                }
            }

            /////////////////////////////////////////////////////
            double[, , ,] FuelPrice_unit = new double[Plants_Num, Units_Num, Hour, 3];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Efficient[j, i, 0] >= Marketefficient[h]))
                        {
                            FuelPrice_unit[j, i, h, 0] = FuelPrice[0];
                        }

                        if ((Unit_Dec[j, i, 0] == "Steam") && (Efficient[j, i, 1] >= HeatAverage[1] * Marketefficient[h] / HeatAverage[0]))
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice[1];
                            FuelPrice_unit[j, i, h, 2] = FuelPrice[2];
                        }


                        if ((Unit_Dec[j, i, 0] == "Gas") && (Efficient[j, i, 1] >= HeatAverage[2] * Marketefficient[h] / HeatAverage[0]))
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice[1];
                            FuelPrice_unit[j, i, h, 2] = FuelPrice[2];
                        }


                        if ((Efficient[j, i, 0] < Marketefficient[h]))
                        {
                            FuelPrice_unit[j, i, h, 0] = FuelPrice[0] * (Efficient[j, i, 0] / Marketefficient[h]) + FuelPrice_Free[0] * ((Marketefficient[h] - Efficient[j, i, 0]) / Marketefficient[h]);
                        }


                        if ((Unit_Dec[j, i, 0] == "Steam") && (Efficient[j, i, 1] < HeatAverage[1] * Marketefficient[h] / HeatAverage[0]))
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice[1] * (Efficient[j, i, 1] / Marketefficient[h]) + FuelPrice_Free[1] * ((Marketefficient[h] - Efficient[j, i, 1]) / Marketefficient[h]);
                            FuelPrice_unit[j, i, h, 2] = FuelPrice[2] * (Efficient[j, i, 1] / Marketefficient[h]) + FuelPrice_Free[2] * ((Marketefficient[h] - Efficient[j, i, 1]) / Marketefficient[h]);
                        }
                        if ((Unit_Dec[j, i, 0] == "Steam") && (Unit_Dec[j, i, 1] == "CC"))
                        {
                            FuelPrice_unit[j, i, h, 0] = 0;
                            FuelPrice_unit[j, i, h, 1] = 0;
                            FuelPrice_unit[j, i, h, 2] = 0;
                        }

                        if ((Unit_Dec[j, i, 0] == "Gas") && (Efficient[j, i, 1] < HeatAverage[2] * Marketefficient[h] / HeatAverage[0]))
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice[1] * (Efficient[j, i, 1] / Marketefficient[h]) + FuelPrice_Free[1] * ((Marketefficient[h] - Efficient[j, i, 1]) / Marketefficient[h]);
                            FuelPrice_unit[j, i, h, 2] = FuelPrice[2] * (Efficient[j, i, 1] / Marketefficient[h]) + FuelPrice_Free[2] * ((Marketefficient[h] - Efficient[j, i, 1]) / Marketefficient[h]);
                        }
                    }
                }
            }
            //***********************************************************************************
            MaxPrice(DateTime.Now);
            double MarketMinPrice = 22500;
            oDataTable = utilities.returntbl("select MarketPriceMin from BaseData where Date = '" + smaxdate + "'order by BaseID desc");
            if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][0].ToString() != "")
            {
                MarketMinPrice = MyDoubleParse(oDataTable.Rows[0][0].ToString());
            }


            oDataTable = utilities.returntbl("select CapacityPayment from BaseData where Date = '" + smaxdate + "'order by BaseID desc");
            double[, ,] Price_Capacity_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k))
                            {
                                if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][0].ToString() != "")
                                {
                                    Price_Capacity_unit[j, i, h] = MyDoubleParse(oDataTable.Rows[0][0].ToString());
                                }
                                else
                                {
                                    Price_Capacity_unit[j, i, h] = 88000;
                                }
                                if (plant[j] == "138" || plant[j] == "104")
                                {
                                    Price_Capacity_unit[j, i, h] = 0;
                                }
                                if (plant[j] == "543")
                                {
                                    Price_Capacity_unit[j, i, h] = 88000;
                                }
                            }
                        }
                    }
                }
            }
            //***********************************************************************************
            double[, ,] TehranStrategy = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k))
                            {
                                TehranStrategy[j, i, h] = 0;
                                if (plant[j] == "104")
                                {
                                    TehranStrategy[j, i, h] = 0;
                                    Unit_Data[j, i, 11] = 30000;
                                    Unit_Data[j, i, 12] = 30000;
                                }
                            }
                        }
                    }
                }
            }
            //***********************************************************************************
            double[, ,] TehranStrategy_Cost = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k))
                            {
                                TehranStrategy_Cost[j, i, h] = 0;
                            }
                        }
                    }
                }
            }
            //***********************************************************************************
            for (int j = 0; j < Plants_Num; j++)
            {

                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (setparam[j, 5] == false)
                    {
                        Unit_Data[j, i, 12] = 1;
                        Unit_Data[j, i, 15] = 1;
                        Unit_Data[j, i, 25] = 1;
                        Unit_Data[j, i, 14] = 1;
                        for (int h = 0; h < Hour; h++)
                        {
                            FuelPrice_unit[j, i, h, 0] = 1;
                            FuelPrice_unit[j, i, h, 1] = 1;
                        }
                    }
                }
            }
            //***********************************************************************************
            double[, ,] Cost_Per_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int m = 0; m < Mmax; m++)
                        {
                            strCmd = "select count(*) from ConditionUnit" +
                                " where PPID= '" + plant[j] + "'" +
                                " and UnitCode = '" + Unit[j, i] + "'" +
                                " and SecondFuel=1" +
                                " and SecondFuelStartDate<'" + biddingDate + " '" +
                                " AND SecondFuelEndDate>'" + biddingDate + "'";

                            if (utilities.returntbl(strCmd).Rows[0][0].ToString().Trim() != "0")
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Cost_Per_Plant_unit[j, i, h] = ((Unit_Data[j, i, 20] * Unit_Data[j, i, 4] * Unit_Data[j, i, 4] + Unit_Data[j, i, 21] * Unit_Data[j, i, 4] + Unit_Data[j, i, 22]) * FuelPrice_unit[j, i, h, 1] / Unit_Data[j, i, 4]) + Unit_Data[j, i, 12] + ((Unit_Data[j, i, 15] + Unit_Data[j, i, 25] - Price_Capacity_unit[j, i, h]) * TehranStrategy_Cost[j, i, h]) + Unit_Data[j, i, 14];
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Cost_Per_Plant_unit[j, i, h] = (SecondFuel_Fuel[j, i, 2] * FuelPrice_unit[j, i, h, 1] / (Unit_Data[j, i, 4])) + Unit_Data[j, i, 12] + ((Unit_Data[j, i, 15] + Unit_Data[j, i, 25] - Price_Capacity_unit[j, i, h]) * TehranStrategy_Cost[j, i, h]) + Unit_Data[j, i, 14];
                                }
                            }
                            else
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Cost_Per_Plant_unit[j, i, h] = ((Unit_Data[j, i, 8] * Unit_Data[j, i, 4] * Unit_Data[j, i, 4] + Unit_Data[j, i, 9] * Unit_Data[j, i, 4] + Unit_Data[j, i, 10]) * FuelPrice_unit[j, i, h, 0] / (Unit_Data[j, i, 4])) + Unit_Data[j, i, 12] + (Unit_Data[j, i, 15] + Unit_Data[j, i, 25] - Price_Capacity_unit[j, i, h]) * TehranStrategy_Cost[j, i, h] + Unit_Data[j, i, 14];
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Cost_Per_Plant_unit[j, i, h] = (PrimayFuel_Fuel[j, i, 2] * FuelPrice_unit[j, i, h, 0] / (Unit_Data[j, i, 4])) + Unit_Data[j, i, 12] + ((Unit_Data[j, i, 15] + Unit_Data[j, i, 25] - Price_Capacity_unit[j, i, h]) * TehranStrategy_Cost[j, i, h]) + Unit_Data[j, i, 14];
                                }
                            }
                        }
                    }
                }
            }

            //***********************************************************************************
            double[, ,] Cost_Variable_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int m = 0; m < Mmax; m++)
                        {
                            strCmd = "select count(*) from ConditionUnit" +
                                " where PPID= '" + plant[j] + "'" +
                                " and UnitCode = '" + Unit[j, i] + "'" +
                                " and SecondFuel=1" +
                                " and SecondFuelStartDate<'" + biddingDate + " '" +
                                " AND SecondFuelEndDate>'" + biddingDate + "'";

                            if (utilities.returntbl(strCmd).Rows[0][0].ToString().Trim() != "0")
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Cost_Variable_unit[j, i, h] = ((Unit_Data[j, i, 20] * Unit_Data[j, i, 4] * Unit_Data[j, i, 4] + Unit_Data[j, i, 21] * Unit_Data[j, i, 4] + Unit_Data[j, i, 22]) * FuelPrice_unit[j, i, h, 1] / (Unit_Data[j, i, 4])) + Unit_Data[j, i, 12] + Unit_Data[j, i, 14];
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Cost_Variable_unit[j, i, h] = (SecondFuel_Fuel[j, i, 2] * FuelPrice_unit[j, i, h, 1] / (Unit_Data[j, i, 4])) + Unit_Data[j, i, 12] + Unit_Data[j, i, 14];
                                }
                            }
                            else
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Cost_Variable_unit[j, i, h] = ((Unit_Data[j, i, 8] * Unit_Data[j, i, 4] * Unit_Data[j, i, 4] + Unit_Data[j, i, 9] * Unit_Data[j, i, 4] + Unit_Data[j, i, 10]) * FuelPrice_unit[j, i, h, 0] / (Unit_Data[j, i, 4])) + Unit_Data[j, i, 12] + Unit_Data[j, i, 14];
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Cost_Variable_unit[j, i, h] = (PrimayFuel_Fuel[j, i, 2] * FuelPrice_unit[j, i, h, 0] / (Unit_Data[j, i, 4])) + Unit_Data[j, i, 12] + Unit_Data[j, i, 14];
                                }
                            }
                        }
                    }
                }
            }
            //***********************************************************************************
            double[, ,] Cost_variable_Pack = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Cost_Per_Plant_Pack = new double[Plants_Num, Units_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        Cost_variable_Pack[j, i, h] = Cost_Variable_unit[j, i, h];
                        Cost_Per_Plant_Pack[j, i, h] = Cost_Per_Plant_unit[j, i, h];
                        if (Cost_variable_Pack[j, i, h] < Min_Plant[j])
                        {
                            Cost_variable_Pack[j, i, h] = Min_Plant[j];
                        }

                    }
                }
            }
            //***********************************************************************************
            string[] ptimmForPowerLimitedUnit = new string[Plants_Num];

            if (Plants_Num == 1)
            {
                strCmd = " select distinct PPID from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate='" + biddingDate +
                    "' and isEmpty=0 AND PPID=" + plant[0];

                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ptimmForPowerLimitedUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }
            }
            else
            {
                strCmd = " select distinct PPID from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate='" + biddingDate + "'and isEmpty=0";

                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ptimmForPowerLimitedUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < plann; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ptimmForPowerLimitedUnit[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ptimmForPowerLimitedUnit[i] = temp[i];
            }
            /////////////////////////////////////////////////
            int[] unitname = new int[Plants_Num];

            strCmd = " select UnitCode from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate='" + biddingDate;


            for (b = 0; b < Plants_Num; b++)
            {
                string strCmd2 = strCmd +
                    "'and PPID='" + ptimmForPowerLimitedUnit[b] + "'and isEmpty=0";
                oDataTable = utilities.returntbl(strCmd2);
                unitname[b] = oDataTable.Rows.Count;
            }
            //***********************************************************************************
            double[, ,] Limit_Power = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Limit_Dispatch = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Dispatch_Complete = new double[Plants_Num, Units_Num, Hour];
            string[,] DispatchBlock = Getblock002();
            bool[,] DispatchFlag = new bool[Plants_Num, Units_Num];

            int[, ,] Outservice_Plant_unit = new int[Plants_Num, Units_Num, 24];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Dispatch_Complete[j, i, h] = 0;
                    }
                }
            }
            ///////////////////////////////////////////

            for (int j = 0; j < Plants_Num; j++)
            {
                DataTable checkdate = null;
                DataTable CHECKALL = utilities.returntbl("SELECT distinct StartDate FROM dbo.PowerLimitedUnit WHERE PPID='" + ptimmForPowerLimitedUnit[j] + "'and StartDate='" + biddingDate + "'and isEmpty=0 order by StartDate desc");
                if (CHECKALL.Rows.Count > 0)
                {
                    checkdate = utilities.returntbl("SELECT count(*) FROM dbo.PowerLimitedUnit WHERE PPID='" + ptimmForPowerLimitedUnit[j] + "'and StartDate='" + CHECKALL.Rows[0][0].ToString().Trim() + "'and isEmpty=0");

                    if (checkdate.Rows.Count > 0)
                    {
                        if (int.Parse(checkdate.Rows[0][0].ToString()) == Plant_units_Num[j])
                        {
                            ////////////////////////////////////////////////
                            //for (int j = 0; j < Plants_Num; j++)
                            //{

                            for (int i = 0; i < Plant_units_Num[j]; i++)
                            {

                                strCmd = "select Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24" +
                                       " from dbo.PowerLimitedUnit where  PPID='" + ptimmForPowerLimitedUnit[j] + "'and UnitCode='" + Unit[j, i] + "' and StartDate='" + biddingDate + "'and isEmpty=0 order by StartDate desc";

                                oDataTable = utilities.returntbl(strCmd);


                                for (int h = 0; h < Hour; h++)
                                {

                                    if (oDataTable.Rows.Count > 0)
                                    {
                                        DataRow res = oDataTable.Rows[0];
                                        Limit_Power[j, i, h] = MyDoubleParse(res[h].ToString());
                                        Dispatch_Complete[j, i, h] = 1;
                                        if (res[h].ToString() == "")
                                        {
                                            Dispatch_Complete[j, i, h] = 0;
                                        }
                                    }
                                    else
                                    {
                                        Dispatch_Complete[j, i, h] = 0;
                                    }
                                }
                            }
                        }

                        else
                        {
                            for (int k = 0; k < Plant_units_Num[j]; k++)
                            {
                                DataTable drr = utilities.returntbl("select PackageType  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype ASC");
                                string pack = drr.Rows[k][0].ToString().Trim();

                                string ptype = "0";
                                if (Unit_Dec[j, k, 1].Contains("cc") || Unit_Dec[j, k, 1].Contains("CC")) ptype = "1";
                                if (plant[j] == "232") ptype = "0";

                                DataTable UseDispatch = utilities.returntbl("select Hour,DispachableCapacity from dbo.Dispathable where PPID='" + plant[j] + "' and Block='" + DispatchBlock[j, k] + "'and PackageType='" + ptype + "' and StartDate='" + biddingDate + "'order by StartDate desc");

                                if (UseDispatch.Rows.Count > 0)
                                {
                                    for (int h = 0; h < 24; h++)
                                    {
                                        Limit_Dispatch[j, k, h] = MyDoubleParse(UseDispatch.Rows[h][1].ToString());
                                    }
                                    DispatchFlag[j, k] = true;

                                }
                            }
                        }
                    }
                }

                else
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        DataTable drr = utilities.returntbl("select PackageType  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype ASC");
                        string pack = drr.Rows[k][0].ToString().Trim();

                        string ptype = "0";
                        if (Unit_Dec[j, k, 1].Contains("cc") || Unit_Dec[j, k, 1].Contains("CC")) ptype = "1";
                        if (plant[j] == "232") ptype = "0";

                        DataTable UseDispatch = utilities.returntbl("select Hour,DispachableCapacity from dbo.Dispathable where PPID='" + plant[j] + "'and PackageType='" + ptype + "' and Block='" + DispatchBlock[j, k] + "' and StartDate='" + biddingDate + "'order by StartDate desc");

                        if (UseDispatch.Rows.Count > 0)
                        {
                            for (int h = 0; h < 24; h++)
                            {
                                Limit_Dispatch[j, k, h] = MyDoubleParse(UseDispatch.Rows[h][1].ToString());
                            }
                            DispatchFlag[j, k] = true;
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[] ppkForConditionUnit = new string[Plants_Num];

            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID,Date from dbo.ConditionUnit where( ConditionUnit.OutServiceStartDate<='" + biddingDate +
                    "'AND ConditionUnit.OutServiceEndDate>='" + biddingDate +
                    "')or( dbo.ConditionUnit.MaintenanceStartDate<='" + biddingDate +
                    "'and dbo.ConditionUnit.MaintenanceEndDate>='" + biddingDate + "')" +
                    " order by Date Desc";
                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ppkForConditionUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < plann; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppkForConditionUnit[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }
                for (int i = 0; i < Plants_Num; i++)
                    ppkForConditionUnit[i] = temp[i];
            }
            else
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where ((ConditionUnit.OutServiceStartDate<='" + biddingDate +
                    "'AND ConditionUnit.OutServiceEndDate>='" + biddingDate +
                    "')or (dbo.ConditionUnit.MaintenanceStartDate<='" + biddingDate +
                    "'and dbo.ConditionUnit.MaintenanceEndDate>='" + biddingDate + "'" +
                    " ))AND PPID=" + plant[0];
                //" order by Date Desc";

                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ppkForConditionUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }
            }

            ///////////////////////
            int[] uukForConditionUnit = new int[Plants_Num];
            strCmd = " select UnitCode from dbo.ConditionUnit where(( ConditionUnit.OutServiceStartDate<='" + biddingDate +
                       "'AND ConditionUnit.OutServiceEndDate>='" + biddingDate +
                       "')or( dbo.ConditionUnit.MaintenanceStartDate<='" + biddingDate +
                       "'and dbo.ConditionUnit.MaintenanceEndDate>='" + biddingDate;


            for (b = 0; b < Plants_Num; b++)
            {
                string strCmd2 = strCmd +
                    "'))and PPID='" + ppkForConditionUnit[b] + "'" + " order by Date Desc"; ;
                oDataTable = utilities.returntbl(strCmd2);
                ///////j//////////
                if (oDataTable.Rows.Count != 0)
                {
                    uukForConditionUnit[b] = oDataTable.Rows.Count;
                }
            }
            //--------------------------------------------------------------
            bool om = false;
            bool op = false;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < 24; h++)
                    {

                        ////////////////////////////maintenance///////////////////////
                        DataTable secondcondition = utilities.returntbl("SELECT  Maintenance,MaintenanceStartDate,MaintenanceEndDate," +
                           " MaintenanceStartHour,MaintenanceEndHour FROM [ConditionUnit] where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "'and Date<=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "')order by Date desc");

                        if (secondcondition.Rows.Count > 0)
                        {
                            DataRow MyRow = secondcondition.Rows[0];

                            if (bool.Parse(MyRow[0].ToString().Trim()))
                            {

                                int myhour = h;
                                string startDate = MyRow[1].ToString().Trim();
                                string endDate = MyRow[2].ToString().Trim();
                                int startHour = (int.Parse(MyRow[3].ToString().Trim()) - 1);
                                int endHour = (int.Parse(MyRow[4].ToString().Trim()) - 1);


                                if (startDate == endDate && biddingDate == endDate)
                                {
                                    if (myhour > endHour) om = false;

                                    else om = true;

                                    if (myhour < startHour) om = false;

                                }

                                else if (biddingDate == startDate)
                                    if (myhour < startHour)
                                        om = false;
                                    else om = true;
                                else if (biddingDate == endDate)
                                    if (myhour > endHour)
                                        om = false;
                                    else om = true;
                                else if (CheckDateSF(biddingDate, startDate, endDate))
                                    om = true;
                                else om = false;
                            }

                            else om = false;
                        }
                        else om = false;
                        ///////////////////outservice///////////////////
                        DataTable outcondition = utilities.returntbl("SELECT  OutService,OutServiceStartDate,OutServiceEndDate," +
                          " OutServiceStartHour,OutServiceEndHour FROM [ConditionUnit] where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "'and Date<=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "')order by Date desc");

                        if (outcondition.Rows.Count > 0)
                        {
                            DataRow MyRow = outcondition.Rows[0];

                            if (bool.Parse(MyRow[0].ToString().Trim()))
                            {

                                int myhour = h;
                                string startDate = MyRow[1].ToString().Trim();
                                string endDate = MyRow[2].ToString().Trim();
                                int startHour = (int.Parse(MyRow[3].ToString().Trim()) - 1);
                                int endHour = (int.Parse(MyRow[4].ToString().Trim()) - 1);

                                if (startDate == endDate && biddingDate == endDate)
                                {
                                    if (myhour > endHour) op = false;
                                    else op = true;

                                    if (myhour < startHour) op = false;

                                }
                                else if (biddingDate == startDate)
                                    if (myhour < startHour)
                                        op = false;
                                    else op = true;
                                else if (biddingDate == endDate)
                                    if (myhour > endHour)
                                        op = false;
                                    else op = true;

                                else if (CheckDateSF(biddingDate, startDate, endDate))
                                    op = true;
                                else op = false;
                            }
                            else op = false;
                        }
                        else op = false;

                        //////////////////////////////////////////////////////////////////////

                        if (om == true || op == true)
                        {
                            Outservice_Plant_unit[j, i, h] = 1;
                        }

                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Pmax_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Pmax_Plant_unit_fix = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Dispatch_Complete_Pack = new double[Plants_Num, Units_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Dispatch_Complete_Pack[j, i, h] = Dispatch_Complete[j, i, h];

                        Pmax_Plant_unit[j, i, h] = Unit_Data[j, i, 4];
                        Pmax_Plant_unit_fix[j, i, h] = Unit_Data[j, i, 4];
                        if (Dispatch_Complete[j, i, h] == 1)
                        {
                            Pmax_Plant_unit[j, i, h] = Limit_Power[j, i, h];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Pmax_Plant_unit[j, i, h] == 0) && (Dispatch_Complete[j, i, h] == 1))
                        {
                            Outservice_Plant_unit[j, i, h] = 1;
                        }
                        if (Pmax_Plant_unit[j, i, h] == 0)
                        {
                            Pmax_Plant_unit[j, i, h] = Unit_Data[j, i, 4];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if ((DispatchFlag[j, i] == true))
                        {
                            Pmax_Plant_unit[j, i, h] = Limit_Dispatch[j, i, h];
                            if (Pmax_Plant_unit[j, i, h] > 0)
                            {
                                Outservice_Plant_unit[j, i, h] = 0;
                            }
                            if (Pmax_Plant_unit[j, i, h] == 0)
                            {
                                Outservice_Plant_unit[j, i, h] = 1;
                                Pmax_Plant_unit[j, i, h] = Unit_Data[j, i, 4];
                            }
                        }
                        if (Pmax_Plant_unit[j, i, h] == 0)
                        {
                            Pmax_Plant_unit[j, i, h] = Unit_Data[j, i, 4];
                        }
                        if ((Pmax_Plant_unit[j, i, h] < Unit_Data[j, i, 3]) && (Outservice_Plant_unit[j, i, h] == 0))
                        {
                            MessageBox.Show("Plant Dispatch" + plant[j] + "Unit" + i.ToString().Trim() + "Hour" + h.ToString().Trim() + " is not corrent");
                            break;
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[, ,] Outservice = new int[Plants_Num, Units_Num, 24];
            double[, ,] service_Max = new double[Plants_Num, Units_Num, 24];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < 24; h++)
                    {
                        Outservice[j, i, h] = Outservice_Plant_unit[j, i, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

           Outservice_Plant_Pack = new string[Plants_Num, Units_Num, 24];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < 24; h++)
                    {
                        Outservice_Plant_Pack[j, i, h] = "No";
                        if ((Outservice[j, i, h] > 0))
                        {
                            Outservice_Plant_Pack[j, i, h] = "Yes";
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Pmax_Plant_Pack = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Pmax_Plant_Pack_fix = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Pmin_Plant_Pack_fix = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if (Outservice_Plant_unit[j, i, h] == 0)
                        {
                            Pmax_Plant_Pack[j, i, h] = Pmax_Plant_unit[j, i, h];
                        }
                        if ((DispatchFlag[j, i] == true) && (Outservice_Plant_Pack[j, i, h] == "No"))
                        {
                            Pmax_Plant_Pack[j, i, h] = Limit_Dispatch[j, i, h];
                        }
                        Pmax_Plant_Pack_fix[j, i, h] = Pmax_Plant_unit_fix[j, i, h];
                        Pmin_Plant_Pack_fix[j, i, h] = Unit_Data[j, i, 3];
                        if ((Pmax_Plant_Pack[j, i, h] < Unit_Data[j, i, 3]) && (Outservice_Plant_Pack[j, i, h] == "No"))
                        {
                            MessageBox.Show("Errorr : Plant Dispatch" + plant[j] + "Unit" + i.ToString().Trim() + "Hour" + h.ToString().Trim() + " is not corrent");
                            break;
                        }
                    }

                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Pmax_Plant_Pack[j, i, h] == 0)
                        {
                            Outservice_Plant_Pack[j, i, h] = "Yes";
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //int[, ,] CC_Steam_Out = new int[Plants_Num, Units_Num, Hour];
            //for (int h = 0; h < Hour; h++)
            //{
            //    for (int j = 0; j < Plants_Num; j++)
            //    {
            //        for (int i = 0; i < Plant_units_Num[j]; i++)
            //        {
            //            CC_Steam_Out[j, i, h] = 0;
            //            if (Package[j, i].ToString().Trim() == "CC")
            //            {
            //                if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam") & (Outservice_Plant_unit[j, i, h] == 1))
            //                {
            //                    CC_Steam_Out[j, i, h] = 1;
            //                }
            //            }
            //        }
            //    }
            //}
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] MustMaxop = new int[Plants_Num, Units_Num];

            for (int j = 0; j < Plants_Num; j++)
            {

                DataTable detectcode = utilities.returntbl("SELECT MustMax,UnitCode FROM dbo.ConditionUnit WHERE PPID='" + plant[j] + "' and Date=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "')");

                foreach (DataRow myrow in detectcode.Rows)
                {
                    DataTable mustmaxcondition = utilities.returntbl("SELECT distinct PackageCode FROM dbo.UnitsDataMain WHERE UnitCode='" + myrow[1].ToString().Trim() + "'and PPID='" + plant[j] + "'");
                    int detectonepack = int.Parse(mustmaxcondition.Rows[0][0].ToString());

                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if (myrow[0].ToString() != "")
                        {
                            if ((mustmaxcondition.Rows.Count > 0) && (bool.Parse(myrow[0].ToString())) && (k == (detectonepack - 1)))
                            {
                                MustMaxop[j, k] = 1;
                            }
                        }
                    }
                }
            }

            DataTable leveltable = utilities.returntbl("select CostLevel,id from dbo.BiddingStrategySetting where id='" + BiddingStrategySettingId.ToString() + "'");
            string CostLevel = leveltable.Rows[0][0].ToString().Trim();

            /////////////////////////////////////////////////////
            int[,] MustRunop = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    DataTable mustruncondition = utilities.returntbl("select MustRun  from dbo.ConditionUnit where PPID='" + plant[j] + "' and  UnitCode='" + Unit[j, i] + "' and Date=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "' and  UnitCode='" + Unit[j, i] + "')");
                    if (mustruncondition.Rows.Count > 0)
                    {
                        if (mustruncondition.Rows[0][0].ToString() != "" && bool.Parse(mustruncondition.Rows[0][0].ToString()))
                        {
                            MustRunop[j, i] = 1;
                        }

                    }
                    else
                    {
                        MustRunop[j, i] = 0;
                    }

                }
            }
            /////////////////////////////////////////////////////////////////////
            int[, ,] MustRun = new int[Plants_Num, Units_Num, Hour];
            if (CostLevel == "Yes")
            {
                for (int j = 0; j < Plants_Num; j++)
                {

                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            MustRun[j, i, h] = 1;

                            if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Steam") & (Outservice_Plant_unit[j, i, h] == 0))
                            {
                                MustRun[j, i, h] = 0;
                            }
                            if ((Unit_Dec[j, i, 1].ToString().Trim() == "Steam") & (Unit_Dec[j, i, 0].ToString().Trim() == "Steam") & (Outservice_Plant_unit[j, i, h] == 0))
                            {
                                MustRun[j, i, h] = 0;
                            }
                            if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Outservice_Plant_unit[j, i, h] == 0))
                            {
                                MustRun[j, i, h] = 0;
                            }
                            if (MustRunop[j, i] == 1)
                            {
                                MustRun[j, i, h] = 0;
                            }

                        }
                    }
                }
            }
            else
            {
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            MustRun[j, i, h] = 1;
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        FuelPrice_unit[j, i, h, 0] = FuelPrice_unit[j, i, h, 0];
                        if (Unit_Dec[j, i, 0].ToString().Trim() == "Gas")
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice_unit[j, i, h, 2];
                        }
                        if (Unit_Dec[j, i, 0].ToString().Trim() == "Steam")
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice_unit[j, i, h, 1];
                        }
                        if ((Unit_Dec[j, i, 0] == "Steam") && (Unit_Dec[j, i, 1] == "CC"))
                        {
                            FuelPrice_unit[j, i, h, 0] = 0;
                            FuelPrice_unit[j, i, h, 1] = 0;
                            FuelPrice_unit[j, i, h, 2] = 0;
                        }
                        if (MustRun[j, i, h] == 0)
                        {
                            FuelPrice_unit[j, i, h, 0] = 0;
                            FuelPrice_unit[j, i, h, 1] = 0;
                            FuelPrice_unit[j, i, h, 2] = 0;
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] SecondFuel_Plant_unit = new int[Plants_Num, Units_Num];
            string[] ppkForSecondFuel = new string[Plants_Num];

            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID,Date from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'" +
                    " order by Date Desc";
                oDataTable = utilities.returntbl(strCmd);

                int ppkn1 = oDataTable.Rows.Count;

                for (b = 0; b < oDataTable.Rows.Count; b++)
                    ppkForSecondFuel[b] = oDataTable.Rows[b][0].ToString().Trim();

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < ppkn1; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppkForSecondFuel[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ppkForSecondFuel[i] = temp[i];

            }
            else
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'" +
                    " AND PPID=" + plant[0];
                //" order by Date Desc";

                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                {
                    ppkForSecondFuel[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            int[] uukForSecondFuel = new int[Plants_Num];
            strCmd = " select UnitCode  from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate;



            for (b = 0; b < Plants_Num; b++)
            {
                string strCmd2 = strCmd + "'and PPID='" + ppkForSecondFuel[b] + "'" + " order by Date Desc"; ;
                oDataTable = utilities.returntbl(strCmd2);
                uukForSecondFuel[b] = oDataTable.Rows.Count;

            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    strCmd = " select SecondFuel  from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                        "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate +
                        "'and PPID='" + ppkForSecondFuel[j] + "'" +
                        " and UnitCode='" + Unit[j, i] + "'" +
                    " order by Date Desc";


                    oDataTable = utilities.returntbl(strCmd);
                    if (oDataTable.Rows.Count > 0)
                    {
                        DataRow res = oDataTable.Rows[0];
                        if (bool.Parse(res[0].ToString()))
                        {
                            SecondFuel_Plant_unit[j, i] = 1;
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] FuelQuantity_Plant_unit = new double[Plants_Num, Units_Num];
            string[] ppForQuantity = new string[Plants_Num];

            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID,Date from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'" +
                    " order by Date Desc";

                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                    ppForQuantity[b] = oDataTable.Rows[b][0].ToString().Trim();

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < oDataTable.Rows.Count; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppForQuantity[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ppForQuantity[i] = temp[i];


            }
            else
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                         "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'" +
                         " AND PPID = " + plant[0];
                // " order by Date Desc";

                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                    ppForQuantity[b] = oDataTable.Rows[b][0].ToString().Trim();
            }


            int[] uuForQuantity = new int[Plants_Num];
            for (b = 0; b < Plants_Num; b++)
            {
                strCmd = " select UnitCode from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate +
                    "'and PPID='" + ppForQuantity[b] + "'" +
                    " order by Date Desc";


                oDataTable = utilities.returntbl(strCmd);
                uuForQuantity[b] = oDataTable.Rows.Count;
            }


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    strCmd = " select FuelForOneDay from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                        "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate +
                        "'and PPID='" + ppForQuantity[j] + "'" +
                         " and UnitCode='" + Unit[j, i] + "'" +
                    " order by Date Desc";

                    oDataTable = utilities.returntbl(strCmd);

                    if (oDataTable.Rows.Count > 0)
                    {
                        DataRow res = oDataTable.Rows[0];

                        FuelQuantity_Plant_unit[j, i] = MyDoubleParse(res[0].ToString());

                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            double[] LoadForecasting = GetNearestLoadForecastingDateVlues(biddingDate);

            double[] LoadAverage = new double[Hour];
            LoadAverage[0] = LoadForecasting[1];
            for (int h = 1; h < Hour; h++)
            {
                LoadAverage[h] = LoadForecasting[h] + LoadAverage[(h - 1)];
            }

            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------
            //        Price        //
            //------------------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------------------

            string[,] PackageTypesList = new string[Plants_Num, Enum.GetNames(typeof(PackageTypePriority)).Length];

            for (int j = 0; j < Plants_Num; j++)
            {

                string cmd = "select Distinct PackageType from dbo.UnitsDataMain where PPID=" + plant[j];
                //" AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%')AND(UnitCode not like 'Gas cc%')))";
                oDataTable = utilities.returntbl(cmd);
                for (int k = 0; k < oDataTable.Rows.Count; k++)
                    PackageTypesList[j, k] = oDataTable.Rows[k][0].ToString().Trim();

            }

         
           
            //*************************************************************************
            double[, ,] Dispatch_Proposal = new double[Plants_Num, Units_Num, Hour];
            //*************************************************************************
            double[, ,] Price_UL = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Price_optimal = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Price_Margin = new double[Plants_Num, Units_Num, Hour];
            //*************************************************************************
            double[, , ,] Price = new double[Plants_Num, Units_Num, Hour, Step_Num];
            double[, , ,] Power = new double[Plants_Num, Units_Num, Hour, Step_Num];
            //*************************************************************************
            double[, ,] Power_memory = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Power_if = new double[Plants_Num, Units_Num, Hour];
            //*************************************************************************
            double[, ,] Power_Error = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Power_Error_Pack = new double[Plants_Num, Units_Num, Hour];
            //*************************************************************************
            int[, , ,] X_select = new int[Plants_Num, Units_Num, Hour, Power_Num];
            //*************************************************************************
            double[, ,] Power_memory_Pack = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Power_if_Pack = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Power_if_CC = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Test = new double[Plants_Num, Units_Num, Hour];
            //**************************************************************************
            int[, ,] CO_steam = new int[Plants_Num, Units_Num, Hour];
            int[, ,] CO_Dispatch = new int[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        CO_steam[j, i, h] = 2;
                        CO_Dispatch[j, i, h] = 2;
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Capacity_Plant_Pack = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        Capacity_Plant_Pack[j, i, h] = Unit_Data[j, i, 4];
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Pmin_Plant_Pack = new double[Plants_Num, Units_Num, Hour];
            
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam") && (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                        {
                            Pmin_Plant_Pack[j, i, h] = 2 * Unit_Data[j, i, 3];
                            if (Pmin_Plant_Pack[j, i, h] > (0.95 * Pmax_Plant_Pack[j, i, h]))
                            {
                                Pmin_Plant_Pack[j, i, h] = (0.5 * Pmax_Plant_Pack[j, i, h]);
                            }
                        }
                        else if ((Unit_Dec[j, i, 0].ToString().Trim() == "Gas") && (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                        {
                            Pmin_Plant_Pack[j, i, h] = Unit_Data[j, i, 3];
                            if (Pmin_Plant_Pack[j, i, h] > (0.95 * Pmax_Plant_Pack[j, i, h]))
                            {
                                Pmin_Plant_Pack[j, i, h] = (0.7 * Pmax_Plant_Pack[j, i, h]);
                            }
                        }
                        else
                        {
                            Pmin_Plant_Pack[j, i, h] = Unit_Data[j, i, 3];
                            if (Pmin_Plant_Pack[j, i, h] > (0.95 * Pmax_Plant_Pack[j, i, h]))
                            {
                                Pmin_Plant_Pack[j, i, h] = (0.5 * Pmax_Plant_Pack[j, i, h]);
                            }
                        }
                        if (Pmin_Plant_Pack[j, i, h] == 0)
                        {
                            Pmin_Plant_Pack[j, i, h] = Unit_Data[j, i, 3];
                        }
                    }
                }
            }



            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Pmin_Plant_Pack_Total = new double[Plants_Num, Hour];
            double[, ,] Pmin_Plant_Pack_Total_Check = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        Pmin_Plant_Pack_Total[j, h] = 1000;
                        Pmin_Plant_Pack_Total_Check[j, i, h] = Pmin_Plant_Pack[j, i, h];

                        if ((Pmin_Plant_Pack_Total_Check[j, i, h] < Pmin_Plant_Pack_Total[j, h]) && (Pmin_Plant_Pack_Total_Check[j, i, h] != 0))
                        {
                            Pmin_Plant_Pack_Total[j, h] = Pmin_Plant_Pack_Total_Check[j, i, h];
                        }
                    }
                    if (Pmin_Plant_Pack_Total[j, h] == 1000)
                    {
                        MessageBox.Show("Pmin_Plant_Pack_Total is Wrong");
                    }
                }
            }


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] PDiff_Max_Min_Plant_Pack_Check = new double[Plants_Num, Units_Num, Hour];
            double[,] PDiff_Max_Min_Plant_Pack = new double[Plants_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if ((Outservice_Plant_unit[j, i, h] == 0))
                        {
                            PDiff_Max_Min_Plant_Pack_Check[j, i, h] = 0.1 * Pmin_Plant_Pack[j, i, h];
                        }
                        if (PDiff_Max_Min_Plant_Pack_Check[j, i, h] > PDiff_Max_Min_Plant_Pack[j, h])
                        {
                            PDiff_Max_Min_Plant_Pack[j, h] = PDiff_Max_Min_Plant_Pack_Check[j, i, h];
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Number_Count_up = new int[Plants_Num, Units_Num];
            int[,] Number_Count_down = new int[Plants_Num, Units_Num];
            double[, ,] Price_memory = new double[Plants_Num, Units_Num, Hour];
            int[, ,] Eh_memory = new int[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] History_Require = new double[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] History_Dispatch = new double[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] History_UL = new string[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] History_Req_T = new double[Plants_Num, Hour];
            double[,] History_Dis_T = new double[Plants_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] History_Req_Y = new double[Plants_Num, Hour];
            double[,] History_Dis_Y = new double[Plants_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[,] blocks = Getblock();
            DataTable baseplant1 = utilities.returntbl("select PPName from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            string[] ss1 = baseplant1.Rows[0][0].ToString().Trim().Split('-');
            DataTable idtable1 = utilities.returntbl("select PPID from dbo.PowerPlant  where PPName='" + ss1[0].Trim() + "'");

            string preplant1 = idtable1.Rows[0][0].ToString().Trim();
            int indexpre = 0;
            string[,] prenameblocks = preblock(preplant1);
            string preonepack = prenameblocks[0, 0];
            DataTable oDatapre = utilities.returntbl("select Max(PackageCode) from UnitsDataMain where  PPID='" + preplant1 + "'");
            int packprenumber = (int)oDatapre.Rows[0][0];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    oDataTable = utilities.returntbl("select Required,Dispatchable,Contribution  from dbo.DetailFRM005 where PPID='" + plant[j] +
                    "'and TargetMarketDate='" + near005table(blocks[j, k], plant[j]) +
                    "'and Block='" + blocks[j, k] + "'");

                    int r = oDataTable.Rows.Count;

                    if (r > 0)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            History_Require[j, k, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                            History_Dispatch[j, k, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                            History_UL[j, k, h] = oDataTable.Rows[h][2].ToString().Trim();
                        }
                    }
                    else
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            if (Plant_units_Num[j] == Plant_units_Num[j])
                            {
                                History_Require[j, k, h] = Pmax_Plant_Pack[j, k, h];
                                History_Dispatch[j, k, h] = Pmax_Plant_Pack[j, k, h];
                                History_UL[j, k, h] = "N";
                            }
                        }

                    }

                }
            }


            //-------------------------------------------------------------------------------

            double[, ,] Capacity_Require = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Capacity_Dispatch = new double[Plants_Num, Units_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                bool indexu = false;
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    if (setparam[j, 4])
                    {
                        if (k < packprenumber)
                        {
                            oDataTable = utilities.returntbl("select Required,Dispatchable,Contribution  from dbo.DetailFRM005 where PPID='" + preplant1 +
                            "'and TargetMarketDate='" + near005table(prenameblocks[0, k], preplant1) +
                            "'and Block='" + prenameblocks[0, k] + "'");
                        }
                        else
                        {
                            oDataTable = utilities.returntbl("select Required,Dispatchable,Contribution  from dbo.DetailFRM005 where PPID='" + preplant1 +
                            "'and TargetMarketDate='" + near005table(prenameblocks[0, 3], preplant1) +
                            "'and Block='" + prenameblocks[0, packprenumber - 1] + "'");
                        }
                    }
                    else
                    {
                        oDataTable = utilities.returntbl("select Required,Dispatchable,Contribution  from dbo.DetailFRM005 where PPID='" + plant[j] +
                        "'and TargetMarketDate='" + near005table(blocks[j, k], plant[j]) +
                        "'and Block='" + blocks[j, k] + "'");
                    }
                    int r0 = oDataTable.Rows.Count;

                    if (r0 > 0)
                    {
                        foreach (DataRow m in oDataTable.Rows)
                        {
                            if (m["Required"].ToString().Trim() == "0")
                            {
                                indexu = true;
                                break;
                            }

                        }
                        if (indexu == false)
                        {
                            for (int kk = 0; kk < Plant_units_Num[j]; kk++)
                            {
                                for (int h = 0; h < Hour; h++)
                                {
                                    if ((setparam[j, 4]) && (oDataTable.Rows.Count > 0))
                                    {
                                        Capacity_Require[j, kk, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                                        Capacity_Dispatch[j, kk, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                                        History_UL[j, kk, h] = oDataTable.Rows[h][2].ToString().Trim();
                                    }
                                    else
                                    {
                                        Capacity_Require[j, kk, h] = History_Require[j, k, h];
                                        Capacity_Dispatch[j, kk, h] = History_Dispatch[j, k, h];
                                        History_UL[j, kk, h] = History_UL[j, k, h];
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if ((Number_Count_down[j, i] > 0) & (Number_Count_up[j, i] > 0))
                    {
                        Number_Count_down[j, i] = 0;
                    }
                    if (Outservice_Plant_unit[j, i, 0] == 1)
                    {
                        Number_Count_down[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] hisstart_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_up[j, i] < GastostartSteam)
                    {
                        hisstart_Plant_unit[j, i] = Number_Count_up[j, i];
                    }
                    if (Number_Count_up[j, i] >= GastostartSteam)
                    {
                        hisstart_Plant_unit[j, i] = GastostartSteam - 1;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] HisShut_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_down[j, i] < Unit_Data[j, i, 16])
                    {
                        HisShut_Plant_unit[j, i] = Number_Count_down[j, i];
                    }
                    if (Number_Count_down[j, i] >= Unit_Data[j, i, 16])
                    {
                        HisShut_Plant_unit[j, i] = (int)(Unit_Data[j, i, 16] - 1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Powerhistory_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_up[j, i] >= 0)
                    {
                        Powerhistory_Plant_unit[j, i] = ((int)Unit_Data[j, i, 3]);
                    }
                    if (Number_Count_down[j, i] > 0)
                    {
                        Powerhistory_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] V_history_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_up[j, i] >= 0)
                    {
                        V_history_Plant_unit[j, i] = 1;
                    }
                    if (Number_Count_down[j, i] > 0)
                    {
                        V_history_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Hisup_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if ((Number_Count_up[j, i] < Unit_Data[j, i, 6]) & (Number_Count_up[j, i] != 0))
                    {
                        Hisup_Plant_unit[j, i] = (int)Unit_Data[j, i, 6] - Number_Count_up[j, i];
                    }
                    if (Number_Count_up[j, i] >= Unit_Data[j, i, 6])
                    {
                        Hisup_Plant_unit[j, i] = 0;
                    }
                    if (Number_Count_up[j, i] == 0)
                    {
                        Hisup_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Hisdown_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if ((Number_Count_down[j, i] < Unit_Data[j, i, 7]) & (Number_Count_down[j, i] > 0))
                    {
                        Hisdown_Plant_unit[j, i] = (int)Unit_Data[j, i, 7] - Number_Count_down[j, i];
                    }
                    if (Number_Count_down[j, i] >= Unit_Data[j, i, 7])
                    {
                        Hisdown_Plant_unit[j, i] = 0;
                    }
                    if (Number_Count_down[j, i] == 0)
                    {
                        Hisdown_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[] ppMinPower = new string[Plants_Num];
            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID from dbo.ConditionPlant where ConditionPlant.OutServiceStartDate<='" + biddingDate +
                    "'AND ConditionPlant.OutServiceEndDate>='" + biddingDate + "'";
                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                {
                    ppMinPower[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < oDataTable.Rows.Count; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppMinPower[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }
                for (int i = 0; i < Plants_Num; i++)
                    ppMinPower[i] = temp[i];
            }

            else
            {
                strCmd = " select distinct PPID from dbo.ConditionPlant where ConditionPlant.OutServiceStartDate<='" + biddingDate +
                        "'AND ConditionPlant.OutServiceEndDate>='" + biddingDate + "'" +
                        " AND PPID=" + plant[0];
                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                {
                    ppMinPower[b] = oDataTable.Rows[b][0].ToString().Trim();
                }
            }


            double[,] Min_Power = new double[Plants_Num, Hour];


            for (int j = 0; j < Plants_Num; j++)
            {
                strCmd = " select PowerMinHour1,PowerMinHour2,PowerMinHour3,PowerMinHour4,PowerMinHour5,PowerMinHour6,PowerMinHour7,PowerMinHour8,PowerMinHour9,PowerMinHour10,PowerMinHour11,PowerMinHour12,PowerMinHour13,PowerMinHour14,PowerMinHour15,PowerMinHour16,PowerMinHour17,PowerMinHour18,PowerMinHour19,PowerMinHour20,PowerMinHour21,PowerMinHour22,PowerMinHour23,PowerMinHour24 from dbo.ConditionPlant" +
                     " where ConditionPlant.PowerMinStartDate<='" + biddingDate +
                     "'AND ConditionPlant.PowerMinEndDate>='" + biddingDate +
                     "'and PPID='" + ppMinPower[j] + "'";
                oDataTable = utilities.returntbl(strCmd);

                if (oDataTable.Rows.Count > 0)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Min_Power[j, h] = MyDoubleParse(oDataTable.Rows[0][h].ToString());
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[] Price_Min = new double[Hour];
            ///////////////////////

            string CommandText5 = "select Date,AcceptedMin from AveragePrice where Date<='" + biddingDate.Trim() + "'";
            oDataTable = utilities.returntbl(CommandText5);


            DateTime dtDate = PersianDateConverter.ToGregorianDateTime(biddingDate);
            DataRow rowNearest = null;
            TimeSpan minSpan = new TimeSpan();

            foreach (DataRow row in oDataTable.Rows)
            {
                DateTime tempDate = PersianDateConverter.ToGregorianDateTime
                    (row["Date"].ToString().Trim());
                TimeSpan tempSpan = dtDate - tempDate;
                if (rowNearest == null || minSpan > tempSpan)
                {
                    minSpan = tempSpan;
                    rowNearest = row;
                }
            }

            if (rowNearest != null)
            {
                string date2 = rowNearest["Date"].ToString().Trim();

                //////////////////////////////////////////


                string commandtext6 = "select AcceptedMin from AveragePrice where Date='" + date2.ToString().Trim() + "'";
                oDataTable = utilities.returntbl(commandtext6);


                for (int h = 0; h < 24 && h < oDataTable.Rows.Count; h++)
                {

                    Price_Min[h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                }

            }

           

          
          



     


            if (LongTask_Export())
            {

                return true;
            }
            else
            {
                return false;
            }
          
        }


        public bool  LongTask_Export()
        {
           
              
            int jplant = 0;
            int success = 0;
            

                string strCmd = "select * from powerplant";

                if (ppName != "All")
                {
                    strCmd += " WHERE PPName= '" + ppName + "'";
                }
                DataTable dtPlants = utilities.returntbl(strCmd);
                        

                foreach (DataRow row in dtPlants.Rows)
                {
                    //try
                    //{
                        ExportM002Excel(Path, int.Parse(row[0].ToString()), row[2].ToString().Trim(), jplant);
                     
                        success++;
                    //}
                    //catch (System.Exception ex)
                    //{
                      
                    //}
                    jplant++;

                    
                }                     


           
            if (success > 0)
            {
                MessageBox.Show("Export of " + success.ToString() + " file(s) of "+ ppName +" to " + Path + " completed successfully!");
                return true;
            }
            else
                return false;
          
        }
   
        private void ExportM002Excel(string path, int ppid, string ppnameFarsi,int jindex)
        {
            List<string> Packages = new List<string>();

            string cmd = "select distinct PackageType from dbo.UnitsDataMain where PPID='" + ppid.ToString() + "'" +
            " AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%') AND (UnitCode not like 'Gas cc%')))";
            DataTable oDataTable = utilities.returntbl(cmd);
            foreach (DataRow row in oDataTable.Rows)
            {
                Packages.Add(row["PackageType"].ToString().Trim());
            }

            string[] headers = { "Block/Unit No", "Peak", "Max. Daily Generation", "Hour", "Bilat",
                                   "Declared Capacity", "Dispatchable Capacity"};

            string[] firstColValues = { "Form Name :", "Date Of Issue :", "Time Of Issue :", "Target Market Date :",
                                    "Power Plant Name :", "Code :", "Revision :", 
                                    "Filled By :", "Approved By :" };
            bool ismix = true;
            for (int u = 0; u < Packages.Count; u++)
            {
                if (Packages[u] == "CC")
                {
                    ismix = false;
                    break;
                }

            }

            for (int packageIndex = 0; packageIndex < Packages.Count; packageIndex++)
            {
                int rowIndex = 0;
                int colIndex = 50;

                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                Excel.Application oExcel = new Excel.Application();

                oExcel.SheetsInNewWorkbook = 1;
                Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(System.Reflection.Missing.Value);
                Excel.Worksheet sheet = null;
                Excel.Range range = null;

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers
                double test = Pmax_Plant_Pack[0, 0, 0];



                rowIndex = 0;
                colIndex = 0;
                int sheetIndex = 1;
                if (sheet != null)
                    sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
                else
                    sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

                sheet.Name = "Sheet" + (sheetIndex);

                int ppidToPrint = ppid;
                string ppnameFarsiModified = ppnameFarsi;

                if (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count > 1)
                {
                    ppnameFarsiModified = "سيكل تركيبي" + " " + ppnameFarsiModified;
                    ppidToPrint++;
                }

                string[] secondColValues = { "M0022", 
                                          DateTime.Now.ToString("d"), DateTime.Now.ToString("T"),
                                          biddingDate,
                                          ppnameFarsiModified, ppidToPrint.ToString(), 
                                          "0", "", "" };


                for (int i = 0; i < firstColValues.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[i + 1, 1];
                    range.Value2 = firstColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    range = (Excel.Range)sheet.Cells[i + 1, 2];
                    range.Value2 = secondColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


                rowIndex = firstColValues.Length + 2;
                colIndex = 0;
                for (int i = 0; i < headers.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = headers[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
                for (int i = 1; i <= 10; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = "Power_" + i.ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = "Price_" + i.ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                string strCmd = "select distinct block from dbo.Dispathable where ppid='" + ppid.ToString() +
                "' AND StartDate='1389/01/02'  and   PackageType=" + (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count > 1 ? 1 : 0).ToString()+"order by block";
         

                oDataTable = utilities.returntbl(strCmd);

                //string[] blocks = new string[oDataTable.Rows.Count];

                string packname1=Packages[packageIndex];
                //if (ismix) packname1 = "All";

                string[] blocks1;

                

                if (ismix)  blocks1=block002(ppid.ToString());

                else
                    blocks1 = Getblock002pack(ppid, packname1);


                string[] blocks = new string[blocks1.Length];

                //for (int i = 0; i < oDataTable.Rows.Count; i++)
                //    blocks[i] = oDataTable.Rows[i][0].ToString().Trim();

                int y = 0;
                
                 int yy = 0;

                    while (yy < blocks1.Length)
                    {

                        

                            blocks[y] = blocks1[yy].Trim();
                            if (y < blocks1.Length)
                            {
                                y++;
                            }
                       
                        yy++;
                    }
              

                ////////////////////////////////////////////////////////////////////////////////
                    string[] tempblock = new string[blocks.Length];
                    for (int i = 0; i < blocks.Length; i++)
                    {
                        tempblock[i] = blocks[i];
                    }

                    if (ppid == 232)
                    {
                        Array.Sort(blocks);
                    }
                    if (ppid == 206)
                    {
                      
                        ///////////////////////////////////////////////////////////////////////
                        int y1 = 0;
                        for (int ii = 0; ii < Enum.GetNames(typeof(PackageTypePriority)).Length; ii++)
                        {
                            int yy1 = 0;

                            while (yy1 < blocks.Length)
                            {

                                PackageTypePriority MMpppo = (PackageTypePriority)Enum.Parse(typeof(PackageTypePriority), ii.ToString());
                                if (MMpppo.ToString().Substring(0, 1) == tempblock[yy1].Trim().Substring(0, 1))
                                {
                                    string temp=tempblock[yy1].Trim();
                                    blocks[y1] = temp;
                                    if (y1 < blocks.Length)
                                    {
                                        y1++;
                                    }
                                }
                                yy1++;
                            }
                        }
                    }




                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                int kindex=0;
                rowIndex = firstColValues.Length + 3;
                foreach (string blockName in blocks)
                {
                    if (blockName == "G37" || blockName == "G38"
                        || blockName == "G41")
                    {
                        break;
                    }

                    string blockshow = blockName;
                    if (blockshow.Contains("-C")) blockshow = blockshow.Replace("-C", "").Trim();


                    int firstLine = rowIndex;
                    range = (Excel.Range)sheet.Cells[rowIndex, 1];
                    range.Value2 =blockshow;
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                   

                    strCmd = "select * from ManualData where ppid='" + ppid.ToString() + "'" +
                          " AND Block='" + blockName + "'" +
                          " AND TargetMarketDate='" + biddingDate + "'" +
                          " order by block, hour";


                    oDataTable = utilities.returntbl(strCmd);

                      
                    colIndex = 4;
                    
                    int hindex=0;
                    double max = 0;


                    if (oDataTable.Rows.Count > 0)
                    {
                       
                        foreach (DataRow row in oDataTable.Rows)
                        {
                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            // range.Value2 = (int.Parse(row["Hour"].ToString().Trim())).ToString();
                            range.Value2 = (hindex + 1).ToString();
                            //range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                            //////////////////bilat////////////////////////////////////
                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];                        
                            range.Value2 = "0";
                            /////////////////////////////////////////////////////////////

                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            // range.Value2 = row["DeclaredCapacity"].ToString().Trim();
                            range.Value2 = Pmax_Plant_Pack[jindex, kindex, hindex].ToString();

                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            // range.Value2 = row["DispachableCapacity"].ToString().Trim();
                            range.Value2 = Pmax_Plant_Pack[jindex, kindex, hindex].ToString();


                            //double dC = double.Parse(row["DispachableCapacity"].ToString().Trim());

                            //if (dC > max)
                            //  max = dC;
                            DataTable basetabledata = utilities.returntbl("select MarketPriceMax from dbo.BaseData where Date ='"+smaxdate+"'order by BaseID desc");
                            double cap=0.0;
                            if(basetabledata.Rows.Count>0)
                            {
                                cap=MyDoubleParse(basetabledata.Rows[0][0].ToString());
                            }
                           for (int i = 1; i <= 10; i++)
                           
                            {
                                string pow = "";
                                string pric = "";
                                string pminrow = row["PminPrice"].ToString().Trim();
                                string pmaxrow = row["PmaxPrice"].ToString().Trim();
                                string pinterval = row["Interval"].ToString().Trim();

                                if (pinterval == "0")
                                    pinterval = "500";

                                if (pminrow != "0" && pmaxrow != "0")
                                {
                                    if (double.Parse(pmaxrow) > double.Parse(pminrow))
                                    {
                                        if (i == 1)
                                        {

                                            if (Outservice_Plant_Pack[jindex, kindex, hindex] == "Yes")
                                            {

                                                pric = cap.ToString();
                                                pow = Pmax_Plant_Pack[jindex, kindex, hindex].ToString();
                                                //-------------------inser to excell----------------------
                                                range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                                range.Value2 = pow.Trim();

                                                range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                                range.Value2 = pric.Trim();
                                                break;
                                            }

                                            DataTable dd = utilities.returntbl("select AcceptedAverage from dbo.AveragePrice where  Date in (select  max(Date) from dbo.AveragePrice)and Hour='" + (hindex+1) + "'");
                                            if (!IsAvg)
                                            {
                                                if (Pmin_Plant_Pack[jindex, kindex, hindex].ToString() != "0")
                                                {
                                                    pow = Pmin_Plant_Pack[jindex, kindex, hindex].ToString();
                                                }
                                                else
                                                {
                                                    pow = "0";
                                                    break;
                                                }
                                                pric = pminrow;
                                                if (MyDoubleParse(pric) >= cap)
                                                {
                                                    pow = Pmax_Plant_Pack[jindex, kindex, hindex].ToString();
                                                    pric = cap.ToString();

                                                    //-------------------inser to excell----------------------
                                                    range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                                    range.Value2 = pow.Trim();

                                                    range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                                    range.Value2 = pric.Trim();
                                                    break;

                                                }
                                            }
                                            else
                                            {
                                                pow = Pmin_Plant_Pack[jindex, kindex, hindex].ToString();
                                                pric = (Capparam * MyDoubleParse(dd.Rows[0][0].ToString())).ToString();
                                                if (MyDoubleParse(pric) >= cap)
                                                {
                                                    //pow = Pmax_Plant_Pack[jindex, kindex, hindex].ToString();
                                                    //pric = cap.ToString();

                                                    //-------------------inser to excell----------------------
                                                    //range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                                    //range.Value2 = pow.Trim();

                                                    pric = cap.ToString().Trim();
                                                    range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                                    range.Value2 = pric.Trim();
                                                    break;

                                                }

                                            }

                                        }
                                        else if (i == 2)
                                        {
                                            if (Pmin_Plant_Pack[jindex, kindex, hindex] != 0)
                                            {
                                                pow = (Pmin_Plant_Pack[jindex, kindex, hindex] + 0.2).ToString();
                                            }
                                            else
                                            {
                                                pow = "0";
                                                break;
                                            }
                                            pric = ((double.Parse(pminrow)) + (((i-1) * (double.Parse(pmaxrow) - double.Parse(pminrow))) / 3)).ToString();
                                            if (MyDoubleParse(pric) >= cap)
                                            {
                                                pow = Pmax_Plant_Pack[jindex, kindex, hindex].ToString();
                                                pric = cap.ToString();
                                                //-------------------inser to excell----------------------
                                                range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                                range.Value2 = pow.Trim();

                                                range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                                range.Value2 = pric.Trim();
                                               break;

                                            }
                                        }
                                        else if (i == 3)
                                        {
                                            if (Pmin_Plant_Pack[jindex, kindex, hindex] != 0)
                                            {
                                                pow = (Pmin_Plant_Pack[jindex, kindex, hindex] + 0.4).ToString();
                                            }
                                            else
                                            {
                                                pow = "0";
                                                break;
                                            }
                                            pric = ((double.Parse(pminrow)) + (((i-1) * (double.Parse(pmaxrow) - double.Parse(pminrow))) / 3)).ToString();
                                            if (MyDoubleParse(pric) >= cap)
                                            {
                                                pow = Pmax_Plant_Pack[jindex, kindex, hindex].ToString();
                                                pric = cap.ToString();
                                                //-------------------inser to excell----------------------
                                                range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                                range.Value2 = pow.Trim();

                                                range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                                range.Value2 = pric.Trim();
                                               break;
                                            }
                                        }
                                        else
                                        {
                                            if (Pmax_Plant_Pack[jindex, kindex, hindex] != 0)
                                            {

                                                pow = ((Pmax_Plant_Pack[jindex, kindex, hindex]) - ((10 - i) * (0.2))).ToString();
                                            }
                                            else
                                            {
                                                pow = "0";
                                                break;

                                            }
                                            pric = ((double.Parse(pmaxrow)) + ((i - 4) * double.Parse(pinterval))).ToString();
                                            if (MyDoubleParse(pric) >= cap)
                                            {
                                                pow = Pmax_Plant_Pack[jindex, kindex, hindex].ToString();
                                                pric = cap.ToString();
                                                //-------------------inser to excell----------------------
                                                range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                                range.Value2 = pow.Trim();

                                                range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                                range.Value2 = pric.Trim();
                                                break;

                                            }
                                        }

                                    }
                                    else
                                        MessageBox.Show("Please insert price max bigger than price min");
                                }
                                else
                                {

                                    int PPType = 0;
                                    if (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count > 1) PPType = 1;
                                 
                                    DataTable dr = utilities.returntbl("select * from DetailFRM002 where ppid='" + ppid.ToString() + "'" +
                                          " AND Block='" + blockName + "'and PPType='"+ PPType +"'"+
                                          " AND TargetMarketDate='" + biddingDate + "'" +
                                          " AND Estimated=1" +
                                          " order by block, hour");


                                  
                                    if (dr.Rows.Count > 0)
                                    {
                                        pow = dr.Rows[hindex]["Power" + i.ToString()].ToString().Trim();

                                        pric = dr.Rows[hindex]["Price" + i.ToString()].ToString().Trim();

                                      
                                    }
                                }
                                //-------------------inser to excell----------------------
                                range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                range.Value2 = pow.Trim();

                                range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                range.Value2 = pric.Trim();
                               

                              
                            }

                            rowIndex++;
                            hindex++;
                            colIndex = 4;
                          
                        }
                    }

                    else
                    {
                        int PPType = 0;
                        if (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count > 1) PPType = 1;
                                 
                       strCmd = "select * from DetailFRM002 where ppid='" + ppid.ToString() + "'" +
                      " AND Block='" + blockName + "'and PPType='" + PPType + "'" +
                      " AND TargetMarketDate='" + biddingDate + "'" +
                      " AND Estimated=1" +
                      " order by block, hour";

                        oDataTable = utilities.returntbl(strCmd);

                        foreach (DataRow row in oDataTable.Rows)
                        {
                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 = (int.Parse(row["Hour"].ToString().Trim())).ToString();
                            //range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 = row["DeclaredCapacity"].ToString().Trim();


                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 = row["DispachableCapacity"].ToString().Trim();
                            double dC = double.Parse(row["DispachableCapacity"].ToString().Trim());

                            if (dC > max)
                                max = dC;

                            for (int i = 1; i <= 10; i++)
                            {
                                range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                range.Value2 = row["Power" + i.ToString()].ToString().Trim();

                                range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                                range.Value2 = row["Price" + i.ToString()].ToString().Trim();
                            }

                            rowIndex++;
                            colIndex = 4;
                        }

                    }

                    range = (Excel.Range)sheet.Cells[firstLine, 2];
                    range.Value2 = max.ToString();

                    if((kindex+2)<Package_Num)
                    {
                    kindex++;
                    }
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                sheet = ((Excel.Worksheet)WB.Worksheets["Sheet1"]);

                range = (Excel.Range)sheet.Cells[1, 1];
                range = range.EntireColumn;
                range.AutoFit();

                range = (Excel.Range)sheet.Cells[1, 2];
                range = range.EntireColumn;
                range.AutoFit();

                sheet.Activate();
               
                string strDate = biddingDate;
                strDate = strDate.Remove(7, 1);
                strDate = strDate.Remove(4, 1);
                string filePath = path + "\\" +"FRM0022_"+ ppidToPrint.ToString() + "_" + strDate + ".xls";
                //oExcel.Visible = true;
                WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                WB.Close(Missing.Value, Missing.Value, Missing.Value);
                oExcel.Workbooks.Close();
                oExcel.Quit();

            }

        }

      
        private string GetDaysBefore(string strDate, int days)
        {
            DateTime date = PersianDateConverter.ToGregorianDateTime(strDate);
            date = date.Subtract(new TimeSpan(days, 0, 0, 0));
            return new PersianDate(date).ToString("d");
        }

        private double[] GetNearestLoadForecastingDateVlues(string biddingDate)
        {
            double[] result = new double[24];

            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            // double[] hourCap = new double[24];

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText =
                "select Date,DateEstimate from LoadForecasting where Date<='" + biddingDate.Trim() + "'";
            Maxda.Fill(MaxDS);


            DateTime dtDate = PersianDateConverter.ToGregorianDateTime(biddingDate);
            DataRow rowNearest = null;
            TimeSpan minSpan = new TimeSpan();
            //if (MaxDS.Tables[0].Rows.Count>0)
            //    minSpan = 
            foreach (DataRow row in MaxDS.Tables[0].Rows)
            {
                DateTime tempDate = PersianDateConverter.ToGregorianDateTime
                    (row["Date"].ToString().Trim());
                TimeSpan tempSpan = dtDate - tempDate;
                if (rowNearest == null || minSpan > tempSpan)
                {
                    minSpan = tempSpan;
                    rowNearest = row;
                }
            }

            if (rowNearest != null)
            {
                string date2 = rowNearest["Date"].ToString().Trim();

                //////////////////////////////////////////
                // in case there are more than one date, find the one with the maxmimum DateEstimate
                MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                MaxDS = new DataSet();
                Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;

                // Insert into FinalForcast table

                MyCom.CommandText =
                    "select LoadForecasting.* from LoadForecasting where Date='" + date2.ToString().Trim() + "'";
                Maxda.Fill(MaxDS);


                DataRow maxDateRow = null;
                string maxDate = null;

                foreach (DataRow row in MaxDS.Tables[0].Rows)
                {
                    string temp = row["DateEstimate"].ToString().Trim();
                    if (maxDate == null || String.Compare(maxDate, temp) < 0)
                    {
                        maxDate = temp;
                        maxDateRow = row;
                    }
                }

                for (int h = 0; h < 24; h++)
                {
                    string colName = "Hour" + (h + 1).ToString().Trim();
                    result[h] = double.Parse(maxDateRow[colName].ToString());
                }
            }

            return result;
        }


        private string[,] Getblock()
        {

            string[,] blocks = new string[Plants_Num, Units_Num];
            DataTable oDataTable = null;


            oDataTable = utilities.returntbl("select distinct PPID from dbo.PowerPlant");




            for (int j = 0; j < Plants_Num; j++)
            {
                int packcode = 0;
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    if (packcode < Plant_Packages_Num[j]) packcode++;

                    oDataTable = utilities.returntbl("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "'");

                    int ppid = int.Parse(plant[j]);

                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {
                        Initial = "C" + (k + 1).ToString();

                        if (Findccplant(plant[j]))
                        {
                            ppid++;
                        }


                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY unittype asc");
                        int loopcount = 0;
                        foreach (DataRow mn in oDataTable.Rows)
                        {

                            string ptype = mn[0].ToString().Trim();
                            ptype = ptype.ToLower();
                            ptype = ptype.Replace("cc", "c");
                            string[] sp = ptype.Split('c');
                            ptype = sp[0].Trim() + sp[1].Trim();
                            if (ptype.Contains("gas"))
                            {
                                ptype = ptype.Replace("gas", "G");

                            }
                            else
                            {
                                ptype = ptype.Replace("steam", "S");

                            }
                            ptype = ppid + "-" + ptype;

                            blocks[j, k] = ptype;

                            loopcount++;
                            if (loopcount != oDataTable.Rows.Count) k++;
                        }
                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                       plant[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Gas' ORDER BY unittype asc");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;
                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Steam' ORDER BY unittype asc");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;
                    }


                }
            }
            return blocks;

        }
        private string[,] Getblock002()
        {
            string[,] blocks1 = new string[Plants_Num, Units_Num];

            DataTable oDataTable = null;



            oDataTable = utilities.returntbl("select distinct PPID from dbo.PowerPlant");





            for (int j = 0; j < Plants_Num; j++)
            {
                int packcode = 0;
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    if (packcode < Plant_Packages_Num[j]) packcode++;
                    oDataTable = utilities.returntbl("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                       plant[j] + "'" + " and packageCode='" + packcode + "'");


                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY unittype asc");
                        int loopcount = 0;
                        foreach (DataRow mn in oDataTable.Rows)
                        {

                            string ptype = mn[0].ToString().Trim();
                            ptype = ptype.ToLower();
                            ptype = ptype.Replace("cc", "c");
                            string[] sp = ptype.Split('c');
                            ptype = sp[0].Trim() + sp[1].Trim();
                            if (ptype.Contains("gas"))
                                ptype = ptype.Replace("gas", "G");
                            else if (ptype.Contains("steam"))
                                ptype = ptype.Replace("steam", "S");

                            blocks1[j, k] = ptype;

                            loopcount++;
                            if (loopcount != oDataTable.Rows.Count) k++;

                        }



                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'ORDER BY unittype asc");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                        string blockName1 = Initial;

                        blocks1[j, k] = blockName1;

                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'ORDER BY unittype asc");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                        string blockName1 = Initial;

                        blocks1[j, k] = blockName1;

                    }


                }
            }
            return blocks1;







        }
        private string[] Getblock002pack(int ppid,string packtype)
        {
            DataTable oDataTable = null;
            DataTable oData = null;

            if (packtype == "All")
            {

                oData = utilities.returntbl("select unitcode,packageCode,PackageType from dbo.UnitsDataMain where PPID='" +
                   ppid + "'" );

            }
            else
            {
                oData = utilities.returntbl("select unitcode,packageCode,PackageType from dbo.UnitsDataMain where PPID='" +
                   ppid + "'" + " and PackageType='" + packtype + "'");

            }
           

            string[] blocks1 = new string[oData.Rows.Count];


            DataTable oda = utilities.returntbl("select  distinct(packagecode) from dbo.UnitsDataMain where PPID='" +
              ppid + "'and PackageType='" + packtype + "'");

            int plantpack = oda.Rows.Count; 
                     


            int packcode =int.Parse(oda.Rows[0][0].ToString())-1;
            int packc = 0;
            for (int k = 0; k < oData.Rows.Count; k++)
            {
                if (packc < plantpack)
                {
                    packc++;
                    packcode++;
                }
                string packageType = packtype;

                string Initial = "";
                if (packageType == "CC")
                {
                    oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                    ppid + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY unittype asc");
                    int loopcount = 0;
                    foreach (DataRow mn in oDataTable.Rows)
                    {

                        string ptype = mn[0].ToString().Trim();
                        ptype = ptype.ToLower();
                        ptype = ptype.Replace("cc", "c");
                        string[] sp = ptype.Split('c');
                        ptype = sp[0].Trim() + sp[1].Trim();
                        if (ptype.Contains("gas"))
                            ptype = ptype.Replace("gas", "G");
                        else if (ptype.Contains("steam"))
                            ptype = ptype.Replace("steam", "S");

                        blocks1[k] = ptype+"-C";

                        loopcount++;
                        if (loopcount != oDataTable.Rows.Count) k++;

                    }



                }
                else if (packageType == "Gas")
                {
                    oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                   ppid + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'ORDER BY unittype asc");

                    Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                    string blockName1 = Initial;

                    blocks1[k] = blockName1;

                }
                else if (packageType == "Steam")
                {
                    oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                    ppid + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'ORDER BY unittype asc");

                    Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                    string blockName1 = Initial;

                    blocks1[k] = blockName1;

                }


            }

            return blocks1;       




        }


        public bool Findccplant(string ppid)
        {
            int tr = 0;

            oDataTable = utilities.returntbl("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count > 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }
        private bool CheckDateSF(string date, string start, string end)
        {
            bool result = false;
            string temp1 = date.Remove(4);
            start = start.Trim();
            end = end.Trim();
            if ((start != "") && (end != ""))
            {
                string temp2 = start.Remove(4);
                if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
                else
                {
                    temp1 = date.Remove(7);
                    temp1 = temp1.Remove(0, temp1.Length - 2);
                    temp2 = start.Remove(7);
                    temp2 = temp2.Remove(0, temp2.Length - 2);
                    if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                    else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
                    else
                    {
                        temp1 = date.Remove(0, date.Length - 2);
                        temp2 = start.Remove(0, start.Length - 2);
                        if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                        else result = false;
                    }
                }
                if (!result) return (result);
                else
                {
                    temp1 = date.Remove(4);
                    temp2 = end.Remove(4);
                    if (int.Parse(temp1) < int.Parse(temp2)) return (true);
                    else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
                    else
                    {
                        temp1 = date.Remove(7);
                        temp1 = temp1.Remove(0, temp1.Length - 2);
                        temp2 = end.Remove(7);
                        temp2 = temp2.Remove(0, temp2.Length - 2);
                        if (int.Parse(temp1) < int.Parse(temp2)) return (true);
                        else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
                        else
                        {
                            temp1 = date.Remove(0, date.Length - 2);
                            temp2 = end.Remove(0, end.Length - 2);
                            if (int.Parse(temp1) < int.Parse(temp2))// || (temp1 == temp2))
                                return (true);
                            else return (false);
                        }
                    }
                }
            }
            return false;
        }
     
        private string FindNearRegionDate(string date)
        {
            DataTable odat = utilities.returntbl("Select distinct Date from dbo.RegionNetComp where Date<='" + date + "'AND Code='R03' order by Date desc");
            return odat.Rows[0][0].ToString().Trim();

        }
        private string FindNearProduceDate(string date)
        {
            DataTable odat = utilities.returntbl("Select distinct Date from dbo.ProducedEnergy where Date<='" + date + "'order by Date desc");
            return odat.Rows[0][0].ToString().Trim();

        }
    
        private string FindCommon002and005(DateTime selectedDate, string plant)
        {

           

            string common = "";

            ///////////////check 002 and 005 not empty////////////////////////////
            DataTable name = utilities.returntbl("select distinct(PPName) from dbo.PowerPlant where PPID='" +plant + "'");
           
            DataTable omoo5 = utilities.returntbl("select * from dbo.DetailFRM005 where TargetMarketDate='" + new PersianDate(selectedDate).ToString("d") + "'and PPID='" + plant + "'");
            DataTable omoo2 = utilities.returntbl("select * from dbo.DetailFRM002 where TargetMarketDate='" + new PersianDate(selectedDate).ToString("d") + "'and PPID='" + plant + "'and Estimated=0");

            if (omoo2.Rows.Count == 0 || omoo5.Rows.Count == 0)
            {
                PersianDate test1 = new PersianDate(currentDate);
                DateTime Now = PersianDateConverter.ToGregorianDateTime(test1);

                DateTime ENDDate = Now.Subtract(new TimeSpan(6, 0, 0, 0));
                for (int daysbefore = 0; daysbefore < 7; daysbefore++)
                {

                    DateTime SAMPLEDate = Now.Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                    if (SAMPLEDate != ENDDate)
                    {

                        string date5 = "";
                        string date2 = "";

                        if (omoo2.Rows.Count == 0)
                        {

                            ///////////////
                            if (predate002 != selectedDate)
                            {

                                message += "M002 File" + " Date :" + new PersianDate(selectedDate).ToString("d") + "   Plant :" + name.Rows[0][0].ToString().Trim() + "\r\n";
                                predate002 = selectedDate;
                            }
                         

                            /////////////
                            DataTable nearomoo2 = utilities.returntbl("select distinct TargetMarketDate from dbo.DetailFRM002 where TargetMarketDate<='" + new PersianDate(SAMPLEDate).ToString("d") + "'and PPID='" + plant + "'order by TargetMarketDate desc");

                            date2 = nearomoo2.Rows[0][0].ToString();
                            bool inweek = finddatein7daym002(date2, plant);
                            if (inweek == false)
                            {
                                DialogResult result = MessageBox.Show("M002 AND M005 Files In Previous week Is Not Existed!. \r\n", "Bid Allocation End Unsuccesssfully!", MessageBoxButtons.OK);
                                if (result == DialogResult.OK)
                                {
                                    return "nocontinue";

                                }
                            }

                        }
                        if (omoo5.Rows.Count == 0)
                        {
                            if (predate005 != selectedDate)
                            {
                                message += "M005 File" + " Date :" + new PersianDate(selectedDate).ToString("d") + "   Plant :" + name.Rows[0][0].ToString().Trim() + "\r\n";
                                predate005 = selectedDate;
                            }

                            DataTable nearomoo5 = utilities.returntbl("select distinct TargetMarketDate from dbo.DetailFRM005 where TargetMarketDate<='" + new PersianDate(SAMPLEDate).ToString("d") + "'and PPID='" + plant + "'order by TargetMarketDate desc");
                            date5 = nearomoo5.Rows[0][0].ToString();
                            bool inweek1 = finddatein7daym005(date5, plant);
                            if (inweek1 == false)
                            {
                                DialogResult result1 = MessageBox.Show("M002 AND M005 Files In Previous week Is Not Existed!. \r\n", "Bid Allocation End Unsuccesssfully!", MessageBoxButtons.OK);
                                if (result1 == DialogResult.OK)
                                {
                                    return "nocontinue";

                                }
                            }
                        }

                        if ((date2 == "" && date5 != ""))
                        {
                            date2 = date5;
                        }

                        if ((date5 == "" && date2 != ""))
                        {
                            date5 = date2;
                        }

                        if (string.Compare(date2, date5) < 0)
                        {
                            date5 = date2;
                        }
                        if (string.Compare(date5, date2) < 0)
                        {
                            date2 = date5;
                        }

                        if (date5 == date2)
                        {
                            common = date2;

                            break;
                        }
                     

                    }

                }

            }

            else
                common = new PersianDate(selectedDate).ToString("d");



            return common;

        }
        private bool finddatein7daym002(string date, string plant)
        {

            PersianDate test2 = new PersianDate(currentDate);
            DateTime Now1 = PersianDateConverter.ToGregorianDateTime(test2);


            for (int daysbefore = 0; daysbefore < 7; daysbefore++)
            {
                DateTime ENDDate = Now1.Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                DataTable nearomoo2 = utilities.returntbl("select distinct TargetMarketDate from dbo.DetailFRM002 where TargetMarketDate='" + new PersianDate(ENDDate).ToString("d") + "'and PPID='" + plant + "'and Estimated=0 order by TargetMarketDate desc");
                if (nearomoo2.Rows.Count > 0)
                {
                    if (date == nearomoo2.Rows[0][0].ToString())
                    {
                        return true;
                        break;
                    }
                }
            }
            return false;

        }
        private bool finddatein7daym005(string date, string plant)
        {

            PersianDate test2 = new PersianDate(currentDate);
            DateTime Now1 = PersianDateConverter.ToGregorianDateTime(test2);


            for (int daysbefore = 0; daysbefore < 7; daysbefore++)
            {
                DateTime ENDDate = Now1.Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                DataTable nearomoo5 = utilities.returntbl("select distinct TargetMarketDate from dbo.DetailFRM005 where TargetMarketDate='" + new PersianDate(ENDDate).ToString("d") + "'and PPID='" + plant + "'order by TargetMarketDate desc");
                if (nearomoo5.Rows.Count > 0)
                {
                    if (date == nearomoo5.Rows[0][0].ToString())
                    {
                        return true;
                        break;
                    }
                }
            }
            return false;

        }

        private static double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }

        private static bool MyboolParse(string str)
        {
            if (str.Trim() == "")
                return false;
            else
            {
                try { return bool.Parse(str.Trim()); }
                catch { return false; }
            }
        }
        private double[,] MaxPrice(DateTime date)
        {
            double[,] maxs = new double[Plants_Num, 24];

            for (int j = 0; j < Plants_Num; j++)
            {
                DataSet myDs = UtilityPowerPlantFunctions.GetUnits(int.Parse(plant[j]));

                DataTable[] orderedPackages = UtilityPowerPlantFunctions.OrderPackages(myDs);

                ///////////////////////////////////////////////////////////////////////////
                string finalplant = plant[j];

                DataTable baseplantfor7day = utilities.returntbl("select PPName from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                string[] s = baseplantfor7day.Rows[0][0].ToString().Trim().Split('-');
                DataTable idtable7day = utilities.returntbl("select PPID from dbo.PowerPlant  where PPName='" + s[0].Trim() + "'");

                string preplant7day = idtable7day.Rows[0][0].ToString().Trim();

                DataTable packagetable7day = utilities.returntbl("SELECT DISTINCT  PackageType  FROM dbo.PPUnit WHERE PPID='" + preplant7day.Trim() + "'");

                string packtype7day = "";
                try
                {
                    packtype7day = s[1].Trim();
                }
                catch
                {

                }

                if (packtype7day == "Combined Cycle") packtype7day = "CC";
                if (packtype7day == "" || packtype7day == null)
                {
                    for (int i = 0; i < packagetable7day.Rows.Count; i++)
                    {
                        if (packagetable7day.Rows[i][0].ToString().Trim() == "Steam")
                        {
                            packtype7day = "Steam";
                            break;
                        }
                        else if (packagetable7day.Rows[i][0].ToString().Trim() == "Combined Cycle") packtype7day = "CC";

                    }
                }
                DataSet myDs7 = UtilityPowerPlantFunctions.GetUnits(int.Parse(preplant7day));
                DataTable[] orderedPackages7 = OrderPackages7(myDs7);
                foreach (DataTable dtPackageType in orderedPackages)
                {
                    foreach (DataTable dtPackageType7 in orderedPackages7)
                    {
                        if ((setparam[j, 4] == true))
                        {
                            orderedPackages = orderedPackages7;
                            finalplant = preplant7day;
                            break;

                        }
                    }

                }


                ////////////////////////////////////////////////////////////////////////////

                foreach (DataTable dtPackageType in orderedPackages)
                {

                    int packageOrder = 0;
                    if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                    orderedPackages.Length > 1)
                        packageOrder = 1;
                    CMatrix NSelectPrice = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid
                           (Convert.ToInt32(finalplant), dtPackageType, date, packageOrder, biddingDate);
                    if (!NSelectPrice.Zero || NSelectPrice != null)
                    {
                        for (int h = 0; h < 24; h++)
                        {
                            maxs[j, h] = NSelectPrice[0, h];
                        }
                        break;
                    }
                }

            }
            return maxs;
        }

        public DataTable[] OrderPackages7(DataSet myDs)
        {
            DataTable[] orderedPackages = new DataTable[1];
            int count = 0;


            DataTable baseplant = utilities.returntbl("select PPName from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            string[] s = baseplant.Rows[0][0].ToString().Trim().Split('-');
            DataTable idtable = utilities.returntbl("select PPID from dbo.PowerPlant  where PPName='" + s[0].Trim() + "'");

            string preplant = idtable.Rows[0][0].ToString().Trim();

            string packtype = "";
            try
            {
                packtype = s[1].Trim();
            }
            catch
            {

            }

            if (packtype == "Combined Cycle") packtype = "CC";

            if (packtype == "" || packtype == null)
            {
                foreach (string typeName in Enum.GetNames(typeof(PackageTypePriority)))
                {
                    //////////// have to make sure later whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                    if (myDs.Tables.Contains(typeName))
                        orderedPackages[0] = myDs.Tables[myDs.Tables.IndexOf(typeName)];
                }
            }
            else
            {
                //////////// have to make sure later whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                //DataTable dd = new DataTable(packtype);
                //string oo = packtype;
                //orderedPackages[0] = dd;

                foreach (string typeName in Enum.GetNames(typeof(PackageTypePriority)))
                {
                    //////////// have to make sure later whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                    if (typeName == packtype)
                        orderedPackages[0] = myDs.Tables[myDs.Tables.IndexOf(typeName)];
                }


            }

            return orderedPackages;
        }

        private string near005table(string block, string plant)
        {
            string date1 = currentDate;
            DateTime date = PersianDateConverter.ToGregorianDateTime(date1);
            DataTable oDataTable = utilities.returntbl("select Required,Dispatchable,Contribution  from dbo.DetailFRM005 where PPID='" + plant +
                            "'and TargetMarketDate='" + date + "'and Block='" + block + "'");
            if (oDataTable.Rows.Count == 0)
            {
                for (int d = 1; d < 100; d++)
                {
                    DateTime saddate = date;
                    saddate = saddate.AddDays(-d);
                    oDataTable = utilities.returntbl("select Required,Dispatchable,Contribution  from dbo.DetailFRM005 where PPID='" + plant +
                               "'and TargetMarketDate='" + new PersianDate(saddate).ToString("d") +
                               "'and Block='" + block + "'");
                    if (oDataTable.Rows.Count > 0)
                    {
                        date1 = new PersianDate(saddate).ToString("d");
                        break;
                    }
                }
            }
            return date1;
        }
        private string[,] preblock(string ppidpre)
        {
            DataTable oDatapre = utilities.returntbl("select unitcode from UnitsDataMain where  PPID='" + ppidpre + "'");
            int packprenumber = oDatapre.Rows.Count;

            DataTable oDatapre1 = utilities.returntbl("select max(packagecode) from UnitsDataMain where  PPID='" + ppidpre + "'");
            int pack = (int)oDatapre1.Rows[0][0];

            string[,] blocks = new string[1, packprenumber];
            string temp = "";


            for (int j = 0; j < 1; j++)
            {
                int packcode = 0;
                for (int k = 0; k < oDatapre.Rows.Count; k++)
                {
                    if (packcode < pack) packcode++;

                    oDataTable = utilities.returntbl("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                        ppidpre + "'" + " and packageCode='" + packcode + "'");

                    int ppid = int.Parse(ppidpre);

                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {                    

                        if (Findccplant(plant[j]))
                        {
                            ppid++;
                        }


                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        ppidpre + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY unittype asc");
                        int loopcount = 0;
                        foreach (DataRow mn in oDataTable.Rows)
                        {

                            string ptype = mn[0].ToString().Trim();
                            ptype = ptype.ToLower();
                            ptype = ptype.Replace("cc", "c");
                            string[] sp = ptype.Split('c');
                            ptype = sp[0].Trim() + sp[1].Trim();
                            if (ptype.Contains("gas"))
                            {
                                ptype = ptype.Replace("gas", "G");

                            }
                            else
                            {
                                ptype = ptype.Replace("steam", "S");

                            }
                            ptype = ppid + "-" + ptype;

                            blocks[j, k] = ptype;

                            loopcount++;
                            if (loopcount != oDataTable.Rows.Count) k++;
                        }
                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                       ppidpre + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'ORDER BY unittype asc");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;
                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        ppidpre + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'ORDER BY unittype asc");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;
                    }


                }
            }
            return blocks;


        }

        private string[] block002(string ppidpre)
        {
            DataTable oDatapre = utilities.returntbl("select unitcode from UnitsDataMain where  PPID='" + ppidpre + "'");
            int packprenumber = oDatapre.Rows.Count;

            DataTable oDatapre1 = utilities.returntbl("select max(packagecode) from UnitsDataMain where  PPID='" + ppidpre + "'");
            int pack = (int)oDatapre1.Rows[0][0];

            string[] blocks = new string[packprenumber];
            string temp = "";


            
                int packcode = 0;
                for (int k = 0; k < oDatapre.Rows.Count; k++)
                {
                    if (packcode < pack) packcode++;

                    oDataTable = utilities.returntbl("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                        ppidpre + "'" + " and packageCode='" + packcode + "'");

                    int ppid = int.Parse(ppidpre);

                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {


                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        ppidpre + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY unittype asc");
                        int loopcount = 0;
                        foreach (DataRow mn in oDataTable.Rows)
                        {

                            string ptype = mn[0].ToString().Trim();
                            ptype = ptype.ToLower();
                            ptype = ptype.Replace("cc", "c");
                            string[] sp = ptype.Split('c');
                            ptype = sp[0].Trim() + sp[1].Trim();
                            if (ptype.Contains("gas"))
                            {
                                ptype = ptype.Replace("gas", "G");

                            }
                            else
                            {
                                ptype = ptype.Replace("steam", "S");

                            }
                         

                            blocks[k] = ptype;

                            loopcount++;
                            if (loopcount != oDataTable.Rows.Count) k++;
                        }
                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                       ppidpre + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'ORDER BY unittype asc");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                        string blockName = Initial;

                        blocks[k] = blockName;
                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        ppidpre + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'ORDER BY unittype asc");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                        string blockName = Initial;

                        blocks[k] = blockName;
                    }


               
            }
            return blocks;


        }
        private void checkdispatch()
        {
             int allindex=0;
             string message1 = "";
             string[,] DisBlock = Getblock002();
            
        
             for (int j = 0; j < Plants_Num; j++)
             {
                 DataTable name = utilities.returntbl("select distinct(PPName) from dbo.PowerPlant where PPID='" + plant[j] + "'");
                 for (int k = 0; k < Plant_units_Num[j]; k++)
                 {
                     for (int h = 1; h <= 24; h++)
                     {
                         DataTable dt = utilities.returntbl("select distinct(count (*)) as result from dbo.Dispathable where StartDate='" + biddingDate + "' and PPID='" + plant[j] + "' and Block='" + DisBlock[j, k] + "' and hour='" + h + "'");
                         if (int.Parse(dt.Rows[0][0].ToString()) == 0)
                         {
                             allindex++;
                             message1 += " For Plant : " + name.Rows[0][0].ToString().Trim() + "  In Date :" + biddingDate + "  In Block :" + DisBlock[j,k] + "   Not Exist !" + "\r\n";
                             break;                            
                         }

                     }
                 }
             }
             if (message1 != "")
             {
                 if (allindex < 30 && allindex != 0)
                 {
                     MessageBox.Show(message1, " Dispatchable Data Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 }
                 else if (allindex != 0)
                 {
                     MessageBox.Show("Please Fill Dispatchable Data For All Plants .", " Dispatchable Data Not Exist !", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 }
             }
        }
    }

}
