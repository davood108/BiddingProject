﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net;
using System.Globalization;
using FarsiLibrary.Utils;
using FarsiLibrary.Resources;
using FarsiLibrary.Win;
using System.Diagnostics;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using Core_pf_Dll;
using System.Threading;
using System.Reflection;
using NRI.SBS.Common;

namespace PowerPlantProject
{
    public class ExportThreadContainer
    {
        string path = "";
        string biddingDate;
        string lblPlantValue;
        
        public ExportThreadContainer(string Path, string BiddingDate, string lablePlant)
        {
            path = Path;
            biddingDate = BiddingDate;
            lblPlantValue = lablePlant;
        }

        public void LongTask_Export()
        {
            int success = 0;
            if (lblPlantValue.ToLower() != "all")
            {
                SqlCommand MyCom = new SqlCommand();
                MyCom.Connection = new SqlConnection(ConnectionManager.ConnectionString);
                MyCom.Connection.Open();

                MyCom.CommandText = "SELECT @num=PPID, @nameFarsi=PNameFarsi FROM PowerPlant WHERE PPName=@name";
                MyCom.Parameters.Add("@num", SqlDbType.NChar, 10);
                MyCom.Parameters.Add("@name", SqlDbType.NChar, 20);
                MyCom.Parameters["@num"].Direction = ParameterDirection.Output;
                MyCom.Parameters["@name"].Value = lblPlantValue;
                MyCom.Parameters.Add("@nameFarsi", SqlDbType.NVarChar, 50);
                MyCom.Parameters["@nameFarsi"].Direction = ParameterDirection.Output;
                MyCom.ExecuteNonQuery();
                MyCom.Connection.Close();
                try
                {
                    ExportM002Excel(path,
                        int.Parse(MyCom.Parameters["@num"].Value.ToString().Trim()),
                        MyCom.Parameters["@nameFarsi"].Value.ToString().Trim());
                    success++;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                DataTable dt = utilities.returntbl("select * from powerplant");
                foreach (DataRow row in dt.Rows)
                {
                    try
                    {
                        ExportM002Excel(path,
                            int.Parse(row["PPID"].ToString().Trim()),
                            row["PNameFarsi"].ToString().Trim());
                        success++;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }
            }
            //if (success>0)
            //    MessageBox.Show("Export of " + success.ToString() + " file(s) to " + path + " completed successfully!");
            /////////////////
            if (success > 0)
            {

                MessageBox.Show("Export of " + success.ToString() + " file(s) to " + path + " completed successfully!");

                //----------------------------------copy to real---------------------------------------------------------

                //int coping = 0;
                //DialogResult Result;
                //Result = MessageBox.Show("Do you Want To Copy Estimated Bid In Real Bid in Date : " + biddingDate + " And Plant :" + lblPlantValue + " ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //if (Result == DialogResult.Yes)
                //{
                //    DataTable dd = null;
                //    DataTable blockdd = null;
                //    if (lblPlantValue.Trim() != "All")
                //    {
                //        dd = utilities.returntbl("select PPID from dbo.PowerPlant where PPName='" + lblPlantValue + "'");

                //    }
                //    else
                //    {
                //        dd = utilities.returntbl("select * from powerplant");

                //    }
                //    foreach (DataRow midrow in dd.Rows)
                //    {

                //        blockdd = utilities.returntbl("select distinct Block from dbo.DetailFRM002 where PPID='" + midrow[0].ToString().Trim() + "' and TargetMarketDate='" + biddingDate + "'and Estimated=1");
                //        foreach (DataRow mrow in blockdd.Rows)
                //        {

                //            DataTable dt = utilities.returntbl("select * from dbo.DetailFRM002 where PPID='" + midrow[0].ToString().Trim() + "' and TargetMarketDate='" + biddingDate + "' and Block='" + mrow[0].ToString().Trim() + "'and Estimated=1 ORDER BY Hour ASC");

                //            DataTable dcheck = utilities.returntbl("select * from dbo.DetailFRM002 where PPID='" + midrow[0].ToString().Trim() + "' and TargetMarketDate='" + biddingDate + "' and Block='" + mrow[0].ToString().Trim() + "'and Estimated=0");

                //            if (dcheck.Rows.Count == 0 && dt.Rows.Count == 24)
                //            {
                //                DataTable dVALDEL = utilities.returntbl("delete from dbo.DetailFRM002 where PPID='" + midrow[0].ToString().Trim() + "' and TargetMarketDate='" + biddingDate + "'and Estimated=0  and Block='" + mrow[0].ToString().Trim() + "'");


                //                foreach (DataRow mrow1 in dt.Rows)
                //                {
                //                    string Date = mrow1["TargetMarketDate"].ToString().Trim();
                //                    string Plant = mrow1["PPID"].ToString().Trim();
                //                    string pptype = mrow1["PPType"].ToString().Trim();
                //                    string block = mrow1["Block"].ToString().Trim();
                //                    string hour = mrow1["Hour"].ToString().Trim();
                //                    string estimated = "0";
                //                    string declaredcapacity = mrow1["DeclaredCapacity"].ToString().Trim();
                //                    string dispatchablecapacity = mrow1["DispachableCapacity"].ToString().Trim();
                //                    string Power1 = mrow1["Power1"].ToString().Trim();
                //                    string Power2 = mrow1["Power2"].ToString().Trim();
                //                    string Power3 = mrow1["Power3"].ToString().Trim();
                //                    string Power4 = mrow1["Power4"].ToString().Trim();
                //                    string Power5 = mrow1["Power5"].ToString().Trim();
                //                    string Power6 = mrow1["Power6"].ToString().Trim();
                //                    string Power7 = mrow1["Power7"].ToString().Trim();
                //                    string Power8 = mrow1["Power8"].ToString().Trim();
                //                    string Power9 = mrow1["Power9"].ToString().Trim();
                //                    string Power10 = mrow1["Power10"].ToString().Trim();
                //                    string price1 = mrow1["Price1"].ToString().Trim();
                //                    string price2 = mrow1["Price2"].ToString().Trim();
                //                    string price3 = mrow1["Price3"].ToString().Trim();
                //                    string price4 = mrow1["Price4"].ToString().Trim();
                //                    string price5 = mrow1["Price5"].ToString().Trim();
                //                    string price6 = mrow1["Price6"].ToString().Trim();
                //                    string price7 = mrow1["Price7"].ToString().Trim();
                //                    string price8 = mrow1["Price8"].ToString().Trim();
                //                    string price9 = mrow1["Price9"].ToString().Trim();
                //                    string price10 = mrow1["Price10"].ToString().Trim();

                //                    try
                //                    {
                //                        DataTable dinsert = utilities.returntbl("insert INTO DetailFRM002 (TargetMarketDate,PPID,PPType,Block,Hour,Estimated,DeclaredCapacity,DispachableCapacity"
                //                            + ",Power1,Price1,Power2,Price2,Power3,Price3,Power4,Price4,Power5,Price5,Power6,Price6,Power7,Price7,Power8,Price8,Power9,Price9,Power10,Price10) VALUES ('" + Date + "','" + Plant + "','" + pptype + "','" + block +
                //                            "','" + hour + "','" + estimated + "','" + declaredcapacity + "','" + dispatchablecapacity + "','" + Power1 + "','" + price1 + "','" + Power2 + "','" + price2 + "','" + Power3 + "','" + price3 + "','" + Power4 + "','" + price4 + "','" + Power5 + "','" + price5 +
                //                            "','" + Power6 + "','" + price6 + "','" + Power7 + "','" + price7 + "','" + Power8 + "','" + price8 + "','" + Power9 + "','" + price9 + "','" + Power10 + "','" + price10 + "')");

                //                        coping++;
                //                    }
                //                    catch
                //                    {


                //                    }


                //                }
                //            }

                //        }

                //    }

                //    if (coping == blockdd.Rows.Count * dd.Rows.Count * 24 && lblPlantValue.Trim() != "All")
                //    {
                //        MessageBox.Show("Finished Successfully ");
                //    }
                //    if (lblPlantValue.Trim() == "All")
                //    {

                //        MessageBox.Show("Finished Successfully");
                //    }


                //}


            }






            //Excel.Application exobj1 = new Excel.Application();
            //exobj1.Visible = true;
            //exobj1.UserControl = true;
            //System.Globalization.CultureInfo oldci = System.Threading.Thread.CurrentThread.CurrentCulture;
            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            //Excel.Workbook book1 = null;
            //book1 = exobj1.Workbooks.Open("C://test.xls", 0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            //book1.Save();
            //book1.Close(true, book1, Type.Missing);
            //exobj1.Workbooks.Close();
            //exobj1.Quit();


            //////////////////
        }
        private void ExportM002Excel(string path, int ppid, string ppnameFarsi)
        {
            List<string> Packages = new List<string>();

            string cmd = "select distinct PackageType from dbo.UnitsDataMain where PPID='" + ppid.ToString() + "'"+
" AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%') AND (UnitCode not like 'Gas cc%')))";
            DataTable oDataTable = utilities.returntbl(cmd);
            foreach (DataRow row in oDataTable.Rows )
            {
                Packages.Add(row["PackageType"].ToString().Trim());
            }

            string[] headers = { "Block/Unit No", "Peak", "Max. Daily Generation", "Hour", "Bilat",
                                   "Declared Capacity", "Dispatchable Capacity"};

            string[] firstColValues = { "Form Name :", "Date Of Issue :", "Time Of Issue :", "Target Market Date :",
                                    "Power Plant Name :", "Code :", "Revision :", 
                                    "Filled By :", "Approved By :" };

            for (int packageIndex =0; packageIndex<Packages.Count; packageIndex++)
            {
                int rowIndex = 0;
                int colIndex = 50;

                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                Excel.Application oExcel = new Excel.Application();

                oExcel.SheetsInNewWorkbook = 1;
                Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
                Excel.Worksheet sheet = null;
                Excel.Range range = null;

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers




                rowIndex = 0;
                colIndex = 0;
                int sheetIndex = 1;
                if (sheet != null)
                    sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
                else
                    sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

                sheet.Name = "Sheet" + (sheetIndex);

                int ppidToPrint = ppid;
                string ppnameFarsiModified = ppnameFarsi;

                if (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count>1)
                {
                    ppnameFarsiModified = "سيكل تركيبي" + " " + ppnameFarsiModified;
                    ppidToPrint++;
                }

                string[] secondColValues = { "M0022", 
                                          DateTime.Now.ToString("d"), DateTime.Now.ToString("T"),
                                          biddingDate,
                                          ppnameFarsiModified, ppidToPrint.ToString(), 
                                          "0", "", "" };


                for (int i = 0; i < firstColValues.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[i + 1, 1];
                    range.Value2 = firstColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    range = (Excel.Range)sheet.Cells[i + 1, 2];
                    range.Value2 = secondColValues[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


                rowIndex = firstColValues.Length + 2;
                colIndex = 0;
                for (int i = 0; i < headers.Length; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = headers[i];
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
                for (int i = 1; i <= 10; i++)
                {
                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = "Power_" + i.ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
                    range.Value2 = "Price_" + i.ToString();
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                string strCmd = "select distinct block from DetailFRM002 where ppid='" + ppid.ToString() +
                "' AND TargetMarketDate='" + biddingDate + "'and Estimated=1 " +
                " and pptype=" + (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count > 1 ? 1 : 0).ToString();
                //if (mixUnitStatus == MixUnits.JustCombined)
                //    strCmd += " AND block like 'C%'";

                //else if (mixUnitStatus == MixUnits.ExeptCombined)
                //    strCmd += " AND block like 'S%'";
               
                    strCmd += " order by block";
             

                oDataTable = utilities.returntbl(strCmd);

                string[] blocks = new string[oDataTable.Rows.Count];

                for (int i = 0; i < oDataTable.Rows.Count; i++)
                    blocks[i] = oDataTable.Rows[i][0].ToString().Trim();

                if (ppid == 232)
                {
                    Array.Sort(blocks);
                }
                if (ppid == 206)
                {
                    ///////////////////////////////////////////////////////////////////////
                    int y = 0;
                    for (int ii = 0; ii < Enum.GetNames(typeof(PackageTypePriority)).Length; ii++)
                    {
                        int yy = 0;

                        while (yy < oDataTable.Rows.Count)
                        {

                            PackageTypePriority MMpppo = (PackageTypePriority)Enum.Parse(typeof(PackageTypePriority), ii.ToString());
                            if (MMpppo.ToString().Substring(0, 1) == oDataTable.Rows[yy][0].ToString().Trim().Substring(0, 1))
                            {

                                blocks[y] = oDataTable.Rows[yy][0].ToString().Trim();
                                if (y < oDataTable.Rows.Count)
                                {
                                    y++;
                                }
                            }
                            yy++;
                        }
                    }
                }
             
             ////////////////////////////////////////////////////////////////////////////////



                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                rowIndex = firstColValues.Length + 3;
                foreach (string blockName in blocks)
                {
                    int firstLine = rowIndex;
                    range = (Excel.Range)sheet.Cells[rowIndex, 1];
                    range.Value2 = blockName;
                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    ////////!!!!!!! Peak!!!>???????????????
                    string pacblock = "0";
                    if (Packages[packageIndex] == PackageTypePriority.CC.ToString() && Packages.Count > 1) pacblock = "1";



                    strCmd = "select * from DetailFRM002 where ppid='" + ppid.ToString() + "'" +
                        " AND Block='" + blockName + "'" +
                        " AND TargetMarketDate='" + biddingDate + "'" +
                        " AND Estimated=1" +
                        " and pptype='"+pacblock+"'order by block, hour";
                    oDataTable = utilities.returntbl(strCmd);

                    colIndex = 4;

                    double max = 0;
                    foreach (DataRow row in oDataTable.Rows)
                    {
                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = (int.Parse(row["Hour"].ToString().Trim())).ToString();
                        //range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                        //-------------------------BILAT-------------------------------//
                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = "0";
                        ///////////////////////////////////////////////////////////////////////

                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["DeclaredCapacity"].ToString().Trim();


                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                        range.Value2 = row["DispachableCapacity"].ToString().Trim();
                        double dC = double.Parse(row["DispachableCapacity"].ToString().Trim());

                        if (dC > max)
                            max = dC;

                        for (int i = 1; i <= 10; i++)
                        {
                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 = row["Power" + i.ToString()].ToString().Trim();

                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
                            range.Value2 = row["Price" + i.ToString()].ToString().Trim();
                        }

                        rowIndex++;
                        colIndex = 4;
                    }

                    range = (Excel.Range)sheet.Cells[firstLine, 2];
                    range.Value2 = max.ToString();
                }

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                sheet = ((Excel.Worksheet)WB.Worksheets["Sheet1"]);

                range = (Excel.Range)sheet.Cells[1, 1];
                range = range.EntireColumn;
                range.AutoFit();

                range = (Excel.Range)sheet.Cells[1, 2];
                range = range.EntireColumn;
                range.AutoFit();

                sheet.Activate();
                //oExcel.Visible = true;
                //FRM0022_206_13900502
                string strDate = biddingDate;
                strDate = strDate.Remove(7, 1);
                strDate = strDate.Remove(4, 1);
                string filePath = path + "\\" + "FRM0022_" + ppidToPrint.ToString() + "_" + strDate + ".xls";
                //oExcel.Visible = true;
                WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                WB.Close(Missing.Value, Missing.Value, Missing.Value);
                oExcel.Workbooks.Close();
                oExcel.Quit();

            } 

        }

//        private void ExportM002Excel(string path, int ppid, string ppnameFarsi)
//        {
//            List<string> Package = new List<string>();

//            string cmd = "select distict PackageType from dbo.UnitsDataMain where PPID=" + plant[j] +
//" AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%')AND(UnitCode not like 'Gas cc%')))";
//            oDataTable = utilities.returntbl(cmd);
//            for (int k = 0; k < Plant_Packages_Num[j]; k++)
//            {
//                Package.Add(oDataTable.Rows[k][0].ToString().Trim());
//            }



//            MixUnits mixUnitStatus = MixUnits.All;
//            if (MainForm.CombinedCyclePowerPlant(ppid.ToString()))
//                mixUnitStatus = MixUnits.ExeptCombined;

//            string[] headers = { "Block/Unit No", "Peak", "Max. Daily Generation", "Hour", 
//                                   "Declared Capacity", "Dispatchable Capacity"};

//            string[] firstColValues = { "Form Name :", "Date Of Issue :", "Time Of Issue :", "Target Market Date :",
//                                    "Power Plant Name :", "Code :", "Revision :", 
//                                    "Filled By :", "Approved By :" };

//            do
//            {
//                int rowIndex = 0;
//                int colIndex = 50;

//                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
//                Excel.Application oExcel = new Excel.Application();

//                oExcel.SheetsInNewWorkbook = 1;
//                Excel.Workbook WB = (Excel.Workbook)oExcel.Workbooks.Add(Missing.Value);
//                Excel.Worksheet sheet = null;
//                Excel.Range range = null;

//                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ For Headers
//                string ppnameFarsiModified = ppnameFarsi;
//                if (mixUnitStatus == MixUnits.ExeptCombined)
//                    ppnameFarsiModified = "سيكل تركيبي" + " " + ppnameFarsiModified;



//                rowIndex = 0;
//                colIndex = 0;
//                int sheetIndex = 1;
//                if (sheet != null)
//                    sheet = (Excel.Worksheet)WB.Worksheets.Add(Missing.Value, sheet, Missing.Value, Missing.Value);
//                else
//                    sheet = (Excel.Worksheet)WB.Worksheets["Sheet1"];

//                sheet.Name = "Page " + (sheetIndex);

//                int ppidToPrint = ppid;
//                if (mixUnitStatus == MixUnits.JustCombined)// the second one
//                    ppidToPrint++;
//                string[] secondColValues = { "M0022", 
//                                          DateTime.Now.ToString("d"), DateTime.Now.ToString("T"),
//                                          biddingDate,
//                                          ppnameFarsiModified, ppidToPrint.ToString(), 
//                                          "0", "", "" };


//                for (int i = 0; i < firstColValues.Length; i++)
//                {
//                    range = (Excel.Range)sheet.Cells[i + 1, 1];
//                    range.Value2 = firstColValues[i];
//                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

//                    range = (Excel.Range)sheet.Cells[i + 1, 2];
//                    range.Value2 = secondColValues[i];
//                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
//                }

//                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


//                rowIndex = firstColValues.Length + 2;
//                colIndex = 0;
//                for (int i = 0; i < headers.Length; i++)
//                {
//                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
//                    range.Value2 = headers[i];
//                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
//                }
//                for (int i = 1; i <= 10; i++)
//                {
//                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
//                    range.Value2 = "Power_" + i.ToString();
//                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

//                    range = (Excel.Range)sheet.Cells[rowIndex, ++colIndex];
//                    range.Value2 = "Price_" + i.ToString();
//                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
//                }
//                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//                string strCmd = "select distinct block from DetailFRM002 where ppid='" + ppid.ToString() +
//                "' AND TargetMarketDate='" + biddingDate + "'";
//                if (mixUnitStatus == MixUnits.JustCombined)
//                    strCmd += " AND block like 'C%'";

//                //else if (mixUnitStatus == MixUnits.ExeptCombined)
//                //    strCmd += " AND block like 'S%'";
//                strCmd += " order by block";

//                DataTable oDataTable = utilities.returntbl(strCmd);

//                string[] blocks = new string[oDataTable.Rows.Count];
//                for (int i = 0; i < oDataTable.Rows.Count; i++)
//                    blocks[i] = oDataTable.Rows[i][0].ToString().Trim();

//                //strCmd = "select * from DetailFRM002 where ppid='" + ppid.ToString() +
//                //    "' AND TargetMarketDate='" + datePickerBidding.Text + "' order by block, hour";
//                //oDataTable = utilities.returntbl(strCmd);

//                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//                rowIndex = firstColValues.Length + 3;
//                foreach (string blockName in blocks)
//                {
//                    int firstLine = rowIndex;
//                    range = (Excel.Range)sheet.Cells[rowIndex, 1];
//                    range.Value2 = blockName;
//                    range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

//                    ////////!!!!!!! Peak!!!>???????????????

//                    strCmd = "select * from DetailFRM002 where ppid='" + ppid.ToString() + "'" +
//                        " AND Block='" + blockName + "'" +
//                        " AND TargetMarketDate='" + biddingDate + "'" +
//                        " AND Estimated=1" +
//                        " order by block, hour";
//                    oDataTable = utilities.returntbl(strCmd);

//                    colIndex = 4;

//                    double max = 0;
//                    foreach (DataRow row in oDataTable.Rows)
//                    {
//                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
//                        range.Value2 = (int.Parse(row["Hour"].ToString().Trim())).ToString();
//                        //range.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

//                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
//                        range.Value2 = row["DeclaredCapacity"].ToString().Trim();


//                        range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
//                        range.Value2 = row["DispachableCapacity"].ToString().Trim();
//                        double dC = double.Parse(row["DispachableCapacity"].ToString().Trim());

//                        if (dC > max)
//                            max = dC;

//                        for (int i = 1; i <= 10; i++)
//                        {
//                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
//                            range.Value2 = row["Power" + i.ToString()].ToString().Trim();

//                            range = (Excel.Range)sheet.Cells[rowIndex, colIndex++];
//                            range.Value2 = row["Price" + i.ToString()].ToString().Trim();
//                        }

//                        rowIndex++;
//                        colIndex = 4;
//                    }

//                    range = (Excel.Range)sheet.Cells[firstLine, 2];
//                    range.Value2 = max.ToString();
//                }

//                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//                sheet = ((Excel.Worksheet)WB.Worksheets["Page 1"]);

//                range = (Excel.Range)sheet.Cells[1, 1];
//                range = range.EntireColumn;
//                range.AutoFit();

//                range = (Excel.Range)sheet.Cells[1, 2];
//                range = range.EntireColumn;
//                range.AutoFit();

//                sheet.Activate();
//                //oExcel.Visible = true;

//                string strDate = biddingDate;
//                strDate = strDate.Remove(7, 1);
//                strDate = strDate.Remove(4, 1);
//                string filePath = path + "\\" + ppidToPrint.ToString() + "-" + strDate + ".xls";
//                //oExcel.Visible = true;
//                WB.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive, Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
//                WB.Close(Missing.Value, Missing.Value, Missing.Value);
//                oExcel.Workbooks.Close();
//                oExcel.Quit();

//                if (mixUnitStatus == MixUnits.ExeptCombined)
//                    mixUnitStatus = MixUnits.JustCombined;
//                else
//                    mixUnitStatus = MixUnits.All;

//            } while (mixUnitStatus != MixUnits.All);

//        }

    }
}
