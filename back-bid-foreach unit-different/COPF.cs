﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Common;
using ILOG.Concert;
using ILOG.CPLEX;
using PowerPlantProject;
using FarsiLibrary.Utils;
using NRI.SBS.Common;
using System.Collections;
namespace PowerPlantProject
{

    public class COPF
    {
        int BiddingStrategySettingId;
        string ConStr = NRI.SBS.Common.ConnectionManager.ConnectionString;
        string biddingDate;
        string currentDate;
        string ppName;
        private SqlConnection oSqlConnection = null;
        private SqlCommand oSqlCommand = null;
        private SqlDataReader oSqlDataReader = null;
        private DataTable oDataTable = null;
        private string[] plant;
        int Plants_Num;
        int Package_Num;
        int Units_Num = 0;
        int[] Plant_units_Num;
        int[] Plant_Packages_Num;
        bool Ispre = false;
        ArrayList PPIDArray = new ArrayList();
        ArrayList PPIDType = new ArrayList();
        ArrayList PPIDName = new ArrayList();
        string smaxdate = "";
        bool[,] setparam;
        //Fixed Set :
        int CplexPrice = 0;
        int CplexPower = 0;
        int Hour = 24;
        int Dec_Num = 4;
        int Data_Num = 26;
        int Start_Num = 48;
        int Mmax = 2;
        int Step_Num = 10;
        int Xtrain = 201;
        int Power_Num = 4;
        int oldday = 7;
        int Const_selectdays = 7;
        int GastostartSteam = 1;
        string OPF = "Limit";
      
    
        

        int lineTypesNo = Enum.GetNames(typeof(Line_codes)).Length;

        public COPF(string CurrentDate, string BiddingDate, bool OPF)
        {
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();
            biddingDate = BiddingDate;
            currentDate = CurrentDate;
            
        }
        public bool value()
       {

           int b = 0;

           // detect max date in database...................................  
           oDataTable = utilities.returntbl("select Date from BaseData");
           int ndate = oDataTable.Rows.Count;
           string[] arrmaxdate1 = new string[ndate];
           for (b = 0; b < ndate; b++)
           {
               arrmaxdate1[b] = oDataTable.Rows[b][0].ToString().Trim();
           }

           //string smaxdate = buildmaxdate(arrmaxdate1);

           DataTable dtsmaxdate = utilities.returntbl("select * from dbo.BaseData where Date<='" + biddingDate + "'order by BaseID desc");
           if (dtsmaxdate.Rows.Count > 0)
           {
               string[] arrbasedata = new string[dtsmaxdate.Rows.Count];
               int ib = 0;
               foreach (DataRow m in dtsmaxdate.Rows)
               {
                   arrbasedata[ib] = m["Date"].ToString();
                   ib++;
               }
               smaxdate = buildmaxdate(arrbasedata);
           }
           else
           {
               dtsmaxdate = utilities.returntbl("select * from dbo.BaseData where BaseID=(select min(BaseID) from dbo.BaseData)");
               smaxdate = dtsmaxdate.Rows[0]["Date"].ToString();
           }

           oDataTable = utilities.returntbl("select GasPrice,MazutPrice,GasOilPrice from BaseData where Date= '" + smaxdate + "'order by BaseID desc");

           double[] FuelPrice_Free = new double[3];
           for (b = 0; b < 3; b++)
           {
               if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][b].ToString() != "")
               {
                   FuelPrice_Free[b] = MyDoubleParse(oDataTable.Rows[0][b].ToString());
               }
               else
               {
                   FuelPrice_Free[0] = 690;
                   FuelPrice_Free[1] = 5053;
                   FuelPrice_Free[2] = 7057;
               }

           }
           //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          
           // detect max date in database...................................  
           oDataTable = utilities.returntbl("select Date from EfficiencyMarket");
           int ndate2 = oDataTable.Rows.Count;
           string[] arrmaxdate2 = new string[ndate];
           for (int bb = 0; bb < ndate2; bb++)
           {
               arrmaxdate2[bb] = oDataTable.Rows[bb][0].ToString().Trim();
           }

           string smaxdate2 = buildmaxdate(arrmaxdate2);


           oDataTable = utilities.returntbl(" select Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24" +
           " from EfficiencyMarket where Date like '" + smaxdate2 + '%' + "'");
           double[] Marketefficient = new double[24];
           for (int bb = 0; bb < 24; bb++)
           {
               if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][bb].ToString() != "")
               {
                   Marketefficient[bb] = MyDoubleParse(oDataTable.Rows[0][bb].ToString());
               }
               else
               {
                   Marketefficient[bb] = 35;
               }

           }
           //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
           oDataTable = utilities.returntbl("select GasProduction,SteamProduction,CCProduction from BaseData where Date = '" + smaxdate + "'order by BaseID desc");
           double[] ProductionPrice = new double[3];
           for (b = 0; b < 3; b++)
           {
               if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][b].ToString() != "")
               {
                   ProductionPrice[b] = MyDoubleParse(oDataTable.Rows[0][b].ToString());
               }
               else
               {
                   ProductionPrice[0] = 15000;
                   ProductionPrice[1] = 15000;
                   ProductionPrice[2] = 15000;
               }
           }
           //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
           oDataTable = utilities.returntbl("select GasSubsidiesPrice,MazutSubsidiesPrice,GasOilSubsidiesPrice from BaseData where Date = '" + smaxdate + "'order by BaseID desc");
           double[] FuelPrice = new double[3];
           for (b = 0; b < 3; b++)
           {
               if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][b].ToString() != "")
               {
                   FuelPrice[b] = MyDoubleParse(oDataTable.Rows[0][b].ToString());
               }
               else
               {
                   FuelPrice[0] = 27.9;
                   FuelPrice[1] = 57.467;
                   FuelPrice[2] = 30.467;
               }

           }
           //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

           oDataTable = utilities.returntbl("select MarketPriceMax from BaseData where Date = '" + smaxdate + "'order by BaseID desc");
           double[] CapPrice = new double[2];

           if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][0].ToString() != "")
           {
               CapPrice[0] = MyDoubleParse(oDataTable.Rows[0][0].ToString());
               CapPrice[0] = CapPrice[0] - 200;
               CapPrice[1] = CapPrice[0] * 0.0006061;
           }
           else
           {
               CapPrice[0] = 329800;

               CapPrice[1] = 200;
           }

           //Error bid for distance is accept.
           double[] ErrorBid = new double[1];
           ErrorBid[0] = CapPrice[1] * 5;
           string[] Run = new string[2];
           Run[0] = "Automatic";
           Run[1] = "Customize";


           //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
           oDataTable = utilities.returntbl("select distinct PPID from UnitsDataMain ");
           Plants_Num = oDataTable.Rows.Count;
           plant = new string[Plants_Num];
           for (int il = 0; il < Plants_Num; il++)
           {
               plant[il] = oDataTable.Rows[il][0].ToString().Trim();
           }

           string strCmd = "select Max (PackageCode) from UnitsDataMain ";
           if (Plants_Num == 1)
               strCmd += " where ppid=" + plant[0];
           oDataTable = utilities.returntbl(strCmd);
           Package_Num = (int)oDataTable.Rows[0][0];
           //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
           Plant_Packages_Num = new int[Plants_Num];

           List<string>[] PackageTypes = new List<string>[Plants_Num];

           for (int re = 0; re < Plants_Num; re++)
           {
               oDataTable = utilities.returntbl("select Max(PackageCode) from UnitsDataMain where  PPID='" + plant[re] + "'");
               Plant_Packages_Num[re] = (int)oDataTable.Rows[0][0];

               oDataTable = utilities.returntbl("select distinct PackageType from UnitsDataMain where  PPID='" + plant[re] + "'");
               //Plant_Packages_Num[re] = (int)oDataTable.Rows.count;

               PackageTypes[re] = new List<string>();

               foreach (string typeName in Enum.GetNames(typeof(PackageTypePriority)))
               {
                   //////////// have to make sure later whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                   bool found = false;
                   foreach (DataRow row in oDataTable.Rows)
                       if (row["PackageType"].ToString().Trim().Contains(typeName))
                           found = true;

                   if (found)
                       PackageTypes[re].Add(typeName);
               }
           }

           //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

           string[,] Package = new string[Plants_Num, Package_Num];

           for (int j = 0; j < Plants_Num; j++)
           {
               string cmd = "select PackageType from dbo.UnitsDataMain where PPID=" + plant[j] +
                            " AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%')AND(UnitCode not like 'Gas cc%')))";
               oDataTable = utilities.returntbl(cmd);

               for (int k = 0; k < Plant_Packages_Num[j]; k++)
               {
                   Package[j, k] = oDataTable.Rows[k][0].ToString().Trim();
               }

           }

           //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                       
           //Unit Number
           Plant_units_Num = new int[Plants_Num];
           for (int re = 0; re < Plants_Num; re++)
           {
               oDataTable = utilities.returntbl("select UnitCode from UnitsDataMain where PPID='" + plant[re] + "'");

               Plant_units_Num[re] = oDataTable.Rows.Count;
               if (Units_Num <= oDataTable.Rows.Count)
               {
                   Units_Num = oDataTable.Rows.Count;
               }

           }
           string[,] Unit = new string[Plants_Num, Units_Num];

           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   oDataTable = utilities.returntbl("select UnitCode from UnitsDataMain where PPID='" + plant[j] + "'ORDER BY packagecode,unittype");
                   Unit[j, i] = oDataTable.Rows[i][0].ToString().Trim();
               }
           }
           //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
           double[, ,] Toreranceup = new double[Plants_Num, Units_Num, Hour];
           double[, ,] ToreranceDown = new double[Plants_Num, Units_Num, Hour];
           double[, ,] Limit_variance_Down = new double[Plants_Num, Units_Num, Hour];
           double[, ,] Limit_variance = new double[Plants_Num, Units_Num, Hour];

           double Factor_Dispatch_Befor = 0.9;
           double Factor_Price = 0.997;
           double Egas = 500;
           //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

           double[, ,] Unit_Data = new double[Plants_Num, Units_Num, Data_Num];
           // 0 : Package
           // 1 : RampUp
           // 2 : RampDown
           // 3 : Pmin
           // 4 : pmax
           // 5 : RampStart&shut
           // 6 : MinOn
           // 7 : MinOff
           // 8 :  Am
           // 9 :  Bm
           // 10 : Cm
           // 11 : Bmain
           // 12 : Cmain
           // 13 : Dmain
           // 14 : Variable cost
           // 15 : Fixed Cost
           // 16 : Tcold
           // 17 : Thot
           // 18 : Cold start cost
           // 19 : Hot start cost

           for (int j = 0; j < Plants_Num; j++)
           {
               oDataTable = utilities.returntbl("select *  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype");
               if (oDataTable.Rows.Count > 0)
                   for (int i = 0; i < Plant_units_Num[j]; i++)
                   {
                       for (int k = 0; k < Data_Num; k++)
                       {
                           switch (k)
                           {
                               case 0:

                                   Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PackageCode"].ToString());
                                   break;
                               case 1:
                                   if (oDataTable.Rows[i]["RampUpRate"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["RampUpRate"].ToString());
                                       Unit_Data[j, i, k] = 30 * Unit_Data[j, i, k];
                                   }
                                   break;
                               case 2:
                                   Unit_Data[j, i, k] = Unit_Data[j, i, 1];
                                   break;

                               case 3:

                                   Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PMin"].ToString());
                                   break;

                               case 4:

                                   Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PMax"].ToString());
                                   break;
                               case 5:
                                   double pow = Unit_Data[j, i, 1];
                                   if (Unit_Data[j, i, 3] > pow)
                                   {
                                       Unit_Data[j, i, k] = Unit_Data[j, i, 3];
                                   }
                                   else
                                   {
                                       Unit_Data[j, i, k] = pow;
                                   }
                                   break;
                               case 6:
                                   if (oDataTable.Rows[i]["TUp"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TUp"].ToString());
                                   }
                                   else Unit_Data[j, i, k] = 3;

                                   break;
                               case 7:
                                   if (oDataTable.Rows[i]["TDown"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TDown"].ToString());

                                   }
                                   else Unit_Data[j, i, k] = 10;

                                   break;

                               case 8:
                                   if (oDataTable.Rows[i]["PrimaryFuelAmargin"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PrimaryFuelAmargin"].ToString());
                                   }
                                   break;
                               case 9:
                                   if (oDataTable.Rows[i]["PrimaryFuelBmargin"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PrimaryFuelBmargin"].ToString());
                                   }
                                   break;
                               case 10:
                                   if (oDataTable.Rows[i]["PrimaryFuelCmargin"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["PrimaryFuelCmargin"].ToString());
                                   }
                                   break;
                               case 11:
                                   if (oDataTable.Rows[i]["BMaintenance"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["BMaintenance"].ToString());
                                   }
                                   break;

                               case 12:
                                   if (oDataTable.Rows[i]["CMaintenance"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["CMaintenance"].ToString());
                                   }
                                   break;
                               case 13:
                                   Unit_Data[j, i, k] = Unit_Data[j, i, 12];

                                   break;
                               case 14:

                                   if (oDataTable.Rows[i]["VariableCost"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["VariableCost"].ToString());
                                   }
                                   break;
                               case 15:
                                   if (oDataTable.Rows[i]["FixedCost"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["FixedCost"].ToString());
                                   }
                                   break;

                               case 16:
                                   if (oDataTable.Rows[i]["TStartCold"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TStartCold"].ToString());
                                   }
                                   break;
                               case 17:

                                   if (oDataTable.Rows[i]["TStartHot"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["TStartHot"].ToString());
                                   }

                                   break;
                               case 18:
                                   if (oDataTable.Rows[i]["CostStartHot"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["CostStartHot"].ToString());
                                   }


                                   break;

                               case 19:
                                   if (oDataTable.Rows[i]["CostStartCold"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["CostStartCold"].ToString());
                                   }

                                   break;

                               case 20:
                                   if (oDataTable.Rows[i]["SecondFuelAmargin"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["SecondFuelAmargin"].ToString());
                                   }

                                   break;
                               case 21:
                                   if (oDataTable.Rows[i]["SecondFuelBmargin"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["SecondFuelBmargin"].ToString());
                                   }

                                   break;
                               case 22:
                                   if (oDataTable.Rows[i]["SecondFuelCmargin"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["SecondFuelCmargin"].ToString());
                                   }
                                   break;
                               case 23:
                                   if (oDataTable.Rows[i]["HeatValuePrimaryFuel"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["HeatValuePrimaryFuel"].ToString());
                                   }
                                   break;
                               case 24:
                                   if (oDataTable.Rows[i]["HeatValueSecondaryFuel"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["HeatValueSecondaryFuel"].ToString());
                                   }
                                   break;
                               case 25:
                                   if (oDataTable.Rows[i]["CapitalCost"].ToString() != "")
                                   {
                                       Unit_Data[j, i, k] = MyDoubleParse(oDataTable.Rows[i]["CapitalCost"].ToString());
                                   }
                                   break;
                           }
                       }
                   }
           }
           //Rampe rate is correct :
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   for (int k = 0; k < Data_Num; k++)
                   {
                       if (Unit_Data[j, i, 1] > (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]))
                       {
                           Unit_Data[j, i, 1] = (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]);
                           Unit_Data[j, i, 2] = (Unit_Data[j, i, 4] - Unit_Data[j, i, 3]);
                       }
                   }
               }
           }
           //Rampe rate Startup is correct :
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   for (int k = 0; k < Data_Num; k++)
                   {
                       if (Unit_Data[j, i, 5] > (Unit_Data[j, i, 4]))
                       {
                           Unit_Data[j, i, 5] = (Unit_Data[j, i, 4]);
                       }
                   }
               }
           }
           //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
           // package yeki kam she :
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   Unit_Data[j, i, 0] = Unit_Data[j, i, 0] - 1;
               }
           }

           ////////////////////////////khorsand////////////////////////////////
           //checkdispatch();

           ////////////////////////////add array second-primary////////////////////////////////
           double[, , ,] SecondaryParam = new double[Plants_Num, Units_Num, 2, 6];
           double[, , ,] PrimayParam = new double[Plants_Num, Units_Num, 2, 6];
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   DataTable PRIMARYTABLE = utilities.returntbl("select PrimaryPowerA,PrimaryPowerB,PrimaryPowerC,PrimaryHeatRateA,PrimaryHeatRateB,PrimaryHeatRateC from dbo.UnitsDataMain where PPID='" + plant[j] + "' and UnitCode='" + Unit[j, i] + "'");
                   DataTable SECONDARYTABLE = utilities.returntbl("select SecondaryPowerA,SecondaryPowerB,SecondaryPowerC,SecondaryHeatRateA,SecondaryHeatRateB,SecondaryHeatRateC  from dbo.UnitsDataMain where PPID='" + plant[j] + "' and UnitCode='" + Unit[j, i] + "'");

                   for (int l = 0; l < 2; l++)
                   {
                       for (int m = 0; m < 6; m++)
                       {

                           if (l == 0)
                           {
                               switch (m)
                               {
                                   case 0:
                                       if (PRIMARYTABLE.Rows[0][0].ToString() != "")
                                       {
                                           PrimayParam[j, i, 0, 0] = MyDoubleParse(PRIMARYTABLE.Rows[0][0].ToString());
                                       }
                                       break;
                                   case 1:
                                       if (PRIMARYTABLE.Rows[0][1].ToString() != "")
                                       {
                                           PrimayParam[j, i, 0, 1] = MyDoubleParse(PRIMARYTABLE.Rows[0][1].ToString());
                                       }
                                       break;
                                   case 2:
                                       if (PRIMARYTABLE.Rows[0][2].ToString() != "")
                                       {
                                           PrimayParam[j, i, 0, 2] = MyDoubleParse(PRIMARYTABLE.Rows[0][2].ToString());
                                       }
                                       break;
                                   case 3:
                                       if (PRIMARYTABLE.Rows[0][3].ToString() != "")
                                       {
                                           PrimayParam[j, i, 0, 3] = MyDoubleParse(PRIMARYTABLE.Rows[0][3].ToString());
                                       }
                                       break;
                                   case 4:
                                       if (PRIMARYTABLE.Rows[0][4].ToString() != "")
                                       {
                                           PrimayParam[j, i, 0, 4] = MyDoubleParse(PRIMARYTABLE.Rows[0][4].ToString());
                                       }
                                       break;
                                   case 5:
                                       if (PRIMARYTABLE.Rows[0][5].ToString() != "")
                                       {
                                           PrimayParam[j, i, 0, 5] = MyDoubleParse(PRIMARYTABLE.Rows[0][5].ToString());
                                       }
                                       break;
                               }

                           }

                           else if (l == 1)
                           {
                               switch (m)
                               {
                                   case 0:
                                       if (SECONDARYTABLE.Rows[0][0].ToString() != "")
                                       {
                                           SecondaryParam[j, i, 1, 0] = MyDoubleParse(SECONDARYTABLE.Rows[0][0].ToString());
                                       }
                                       break;
                                   case 1:
                                       if (SECONDARYTABLE.Rows[0][1].ToString() != "")
                                       {
                                           SecondaryParam[j, i, 1, 1] = MyDoubleParse(SECONDARYTABLE.Rows[0][1].ToString());
                                       }
                                       break;
                                   case 2:
                                       if (SECONDARYTABLE.Rows[0][2].ToString() != "")
                                       {
                                           SecondaryParam[j, i, 1, 2] = MyDoubleParse(SECONDARYTABLE.Rows[0][2].ToString());
                                       }
                                       break;
                                   case 3:
                                       if (SECONDARYTABLE.Rows[0][3].ToString() != "")
                                       {
                                           SecondaryParam[j, i, 1, 3] = MyDoubleParse(SECONDARYTABLE.Rows[0][3].ToString());
                                       }
                                       break;
                                   case 4:
                                       if (SECONDARYTABLE.Rows[0][4].ToString() != "")
                                       {
                                           SecondaryParam[j, i, 1, 4] = MyDoubleParse(SECONDARYTABLE.Rows[0][4].ToString());
                                       }
                                       break;
                                   case 5:
                                       if (SECONDARYTABLE.Rows[0][5].ToString() != "")
                                       {
                                           SecondaryParam[j, i, 1, 5] = MyDoubleParse(SECONDARYTABLE.Rows[0][5].ToString());
                                       }
                                       break;
                               }
                           }
                       }
                   }
               }
           }
           double save_cpu = 0;
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   if (SecondaryParam[j, i, 1, 0] > SecondaryParam[j, i, 1, 1])
                   {
                       save_cpu = SecondaryParam[j, i, 1, 1];
                       SecondaryParam[j, i, 1, 1] = SecondaryParam[j, i, 1, 0];
                       SecondaryParam[j, i, 1, 0] = save_cpu;
                   }
                   if (SecondaryParam[j, i, 1, 1] > SecondaryParam[j, i, 1, 2])
                   {
                       save_cpu = SecondaryParam[j, i, 1, 2];
                       SecondaryParam[j, i, 1, 2] = SecondaryParam[j, i, 1, 1];
                       SecondaryParam[j, i, 1, 1] = save_cpu;
                   }
                   if (SecondaryParam[j, i, 1, 0] > SecondaryParam[j, i, 1, 1])
                   {
                       save_cpu = SecondaryParam[j, i, 1, 1];
                       SecondaryParam[j, i, 1, 1] = SecondaryParam[j, i, 1, 0];
                       SecondaryParam[j, i, 1, 0] = save_cpu;
                   }
               }
           }
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   if (SecondaryParam[j, i, 1, 3] > SecondaryParam[j, i, 1, 4])
                   {
                       save_cpu = SecondaryParam[j, i, 1, 4];
                       SecondaryParam[j, i, 1, 3] = SecondaryParam[j, i, 1, 4];
                       SecondaryParam[j, i, 1, 4] = save_cpu;
                   }
                   if (SecondaryParam[j, i, 1, 4] > SecondaryParam[j, i, 1, 5])
                   {
                       save_cpu = SecondaryParam[j, i, 1, 5];
                       SecondaryParam[j, i, 1, 4] = SecondaryParam[j, i, 1, 5];
                       SecondaryParam[j, i, 1, 5] = save_cpu;
                   }
                   if (SecondaryParam[j, i, 1, 3] > SecondaryParam[j, i, 1, 4])
                   {
                       save_cpu = SecondaryParam[j, i, 1, 4];
                       SecondaryParam[j, i, 1, 3] = SecondaryParam[j, i, 1, 4];
                       SecondaryParam[j, i, 1, 4] = save_cpu;
                   }
               }
           }
           //****************************************************************
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   if (PrimayParam[j, i, 0, 0] > PrimayParam[j, i, 0, 1])
                   {
                       save_cpu = SecondaryParam[j, i, 0, 1];
                       PrimayParam[j, i, 0, 1] = PrimayParam[j, i, 0, 0];
                       PrimayParam[j, i, 0, 0] = save_cpu;
                   }
                   if (PrimayParam[j, i, 0, 1] > PrimayParam[j, i, 0, 2])
                   {
                       save_cpu = PrimayParam[j, i, 0, 2];
                       PrimayParam[j, i, 0, 2] = PrimayParam[j, i, 0, 1];
                       PrimayParam[j, i, 0, 1] = save_cpu;
                   }
                   if (PrimayParam[j, i, 0, 0] > PrimayParam[j, i, 0, 1])
                   {
                       save_cpu = PrimayParam[j, i, 0, 1];
                       PrimayParam[j, i, 0, 1] = PrimayParam[j, i, 0, 0];
                       PrimayParam[j, i, 0, 0] = save_cpu;
                   }
               }
           }
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   if (PrimayParam[j, i, 0, 3] > PrimayParam[j, i, 0, 4])
                   {
                       save_cpu = PrimayParam[j, i, 0, 4];
                       PrimayParam[j, i, 0, 3] = PrimayParam[j, i, 0, 4];
                       PrimayParam[j, i, 0, 4] = save_cpu;
                   }
                   if (PrimayParam[j, i, 0, 4] > PrimayParam[j, i, 0, 5])
                   {
                       save_cpu = PrimayParam[j, i, 0, 5];
                       PrimayParam[j, i, 0, 4] = PrimayParam[j, i, 0, 5];
                       PrimayParam[j, i, 0, 5] = save_cpu;
                   }
                   if (PrimayParam[j, i, 1, 3] > PrimayParam[j, i, 1, 4])
                   {
                       save_cpu = PrimayParam[j, i, 0, 4];
                       PrimayParam[j, i, 0, 3] = PrimayParam[j, i, 0, 4];
                       PrimayParam[j, i, 0, 4] = save_cpu;
                   }
               }
           }
           double[, ,] SecondFuel_power = new double[Plants_Num, Units_Num, 3];
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   for (int m = 0; m < 3; m++)
                   {
                       SecondFuel_power[j, i, m] = SecondaryParam[j, i, 1, m];
                   }
               }
           }
           double[, ,] SecondFuel_Fuel = new double[Plants_Num, Units_Num, 3];
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   for (int m = 0; m < 3; m++)
                   {
                       SecondFuel_Fuel[j, i, m] = SecondaryParam[j, i, 1, m + 3];
                   }
               }
           }
           double[, ,] PrimayFuel_power = new double[Plants_Num, Units_Num, 3];
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   for (int m = 0; m < 3; m++)
                   {
                       PrimayFuel_power[j, i, m] = PrimayParam[j, i, 0, m];
                   }
               }
           }
           double[, ,] PrimayFuel_Fuel = new double[Plants_Num, Units_Num, 3];
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   for (int m = 0; m < 3; m++)
                   {
                       PrimayFuel_Fuel[j, i, m] = PrimayParam[j, i, 0, m + 3];
                   }
               }
           }
           double[, ,] FixedCost = new double[Plants_Num, Units_Num, 2];
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   FixedCost[j, i, 0] = 0;
                   FixedCost[j, i, 1] = 0;
                   if ((PrimayFuel_Fuel[j, i, 0] > 0) & (PrimayFuel_power[j, i, 0] > 0))
                   {
                       FixedCost[j, i, 0] = PrimayFuel_Fuel[j, i, 0] * Unit_Data[j, i, 3] / PrimayFuel_power[j, i, 0];
                   }
                   if ((SecondFuel_Fuel[j, i, 0] > 0) & (SecondFuel_power[j, i, 0] > 0))
                   {
                       FixedCost[j, i, 1] = SecondFuel_Fuel[j, i, 0] * Unit_Data[j, i, 3] / SecondFuel_power[j, i, 0];
                   }
               }
           }
           double[, ,] Efficient = new double[Plants_Num, Units_Num, 2];
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   Efficient[j, i, 0] = 0;
                   Efficient[j, i, 1] = 0;
                   if ((PrimayFuel_Fuel[j, i, 2] > 0) & (PrimayFuel_power[j, i, 2] > 0))
                   {
                       Efficient[j, i, 0] = (860 * PrimayFuel_power[j, i, 2] * 1000 * 100) / (Unit_Data[j, i, 23] * PrimayFuel_Fuel[j, i, 2]);
                   }
                   else
                   {
                       if (Unit_Data[j, i, 8] == 0)
                       {
                           Efficient[j, i, 0] = 0;
                       }
                       else
                       {
                           Efficient[j, i, 0] = (860 * Unit_Data[j, i, 4] * 1000 * 100) / ((Unit_Data[j, i, 8] * Unit_Data[j, i, 4] * Unit_Data[j, i, 4] + Unit_Data[j, i, 9] * Unit_Data[j, i, 4] + Unit_Data[j, i, 10]) * Unit_Data[j, i, 23]);
                       }
                   }
                   if ((SecondFuel_Fuel[j, i, 2] > 0) & (SecondFuel_power[j, i, 2] > 0))
                   {
                       Efficient[j, i, 1] = (860 * SecondFuel_power[j, i, 2] * 1000 * 100) / (Unit_Data[j, i, 24] * SecondFuel_Fuel[j, i, 2]);
                   }
                   else
                   {
                       if (Unit_Data[j, i, 20] == 0)
                       {
                           Efficient[j, i, 1] = 0;
                       }
                       else
                       {
                           Efficient[j, i, 1] = (860 * Unit_Data[j, i, 4] * 1000 * 100) / ((Unit_Data[j, i, 20] * Unit_Data[j, i, 4] * Unit_Data[j, i, 4] + Unit_Data[j, i, 21] * Unit_Data[j, i, 4] + Unit_Data[j, i, 22]) * Unit_Data[j, i, 24]);
                       }
                   }
               }
           }

           double[, , ,] MarginCost = new double[Plants_Num, Units_Num, 2, 2];
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   for (int m = 0; m < 2; m++)
                   {
                       MarginCost[j, i, 0, m] = 0;
                       MarginCost[j, i, 1, m] = 0;
                       if ((PrimayFuel_Fuel[j, i, m] > 0) & (PrimayFuel_power[j, i, m] > 0) & (PrimayFuel_Fuel[j, i, m + 1] > 0) & (PrimayFuel_power[j, i, m + 1] > 0))
                       {
                           MarginCost[j, i, 0, m] = (PrimayFuel_Fuel[j, i, m + 1] - PrimayFuel_Fuel[j, i, m]) / (PrimayFuel_power[j, i, m + 1] - PrimayFuel_power[j, i, m]);
                       }
                       if ((SecondFuel_Fuel[j, i, m] > 0) & (SecondFuel_power[j, i, m] > 0) & (SecondFuel_Fuel[j, i, m + 1] > 0) & (SecondFuel_power[j, i, m + 1] > 0))
                       {
                           MarginCost[j, i, 1, m] = (SecondFuel_Fuel[j, i, m + 1] - SecondFuel_Fuel[j, i, m]) / (SecondFuel_power[j, i, m + 1] - SecondFuel_power[j, i, m]);
                       }
                   }
               }
           }
           double[, ,] Cost_Web = new double[Plants_Num, Units_Num, 2];
           for (int j = 0; j < Plants_Num; j++)
           {
               for (int i = 0; i < Plant_units_Num[j]; i++)
               {
                   Cost_Web[j, i, 0] = 0;
                   Cost_Web[j, i, 1] = 0;
                   if ((PrimayFuel_Fuel[j, i, 0] > 0) & (PrimayFuel_power[j, i, 0] > 0) & (PrimayFuel_Fuel[j, i, 1] > 0) & (PrimayFuel_power[j, i, 1] > 0) & (PrimayFuel_Fuel[j, i, 2] > 0) & (PrimayFuel_power[j, i, 2] > 0))
                   {
                       Cost_Web[j, i, 0] = 1;
                   }
                   if ((SecondFuel_Fuel[j, i, 0] > 0) & (SecondFuel_power[j, i, 0] > 0) & (SecondFuel_Fuel[j, i, 1] > 0) & (SecondFuel_power[j, i, 1] > 0) & (SecondFuel_Fuel[j, i, 2] > 0) & (SecondFuel_power[j, i, 2] > 0))
                   {
                       Cost_Web[j, i, 1] = 1;
                   }
               }
           }

           ////////////////////////////////////////////////////////////////
           DataTable tablebasedata = utilities.returntbl("select GasHeatValueAverage,MazutHeatValueAverage,GasOilHeatValueAverage from dbo.BaseData where Date='" + smaxdate + "'order by BaseID desc");
           double[] HeatAverage = new double[3];

           //[0]GasHeatValueAverage
           //[1]MazutHeatValueAverage
           //[2]GasOilHeatValueAverage
           if (tablebasedata.Rows[0][0].ToString() != "")
           {
               HeatAverage[0] = MyDoubleParse(tablebasedata.Rows[0][0].ToString());
           }
           if (tablebasedata.Rows[0][1].ToString() != "")
           {

               HeatAverage[1] = MyDoubleParse(tablebasedata.Rows[0][1].ToString());

           }
           if (tablebasedata.Rows[0][2].ToString() != "")
           {

               HeatAverage[2] = MyDoubleParse(tablebasedata.Rows[0][2].ToString());

           }
            double[] Min_Plant = new double[Plants_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                if (plant[j] == "101")
                {
                    Min_Plant[j] = 0.5 * CapPrice[0];
                }
                if (plant[j] == "104")
                {
                    Min_Plant[j] = 0.82 * CapPrice[0];
                }
                if (plant[j] == "131")
                {
                    Min_Plant[j] = 0.8 * CapPrice[0];
                }
                if (plant[j] == "133")
                {
                    Min_Plant[j] = 0.5 * CapPrice[0];
                }
                if (plant[j] == "138")
                {
                    Min_Plant[j] = 0.85 * CapPrice[0];
                }
                if (plant[j] == "144")
                {
                    Min_Plant[j] = 0.8 * CapPrice[0];
                }
                if (plant[j] == "149")
                {
                    Min_Plant[j] = 0.8 * CapPrice[0];
                }
                if (plant[j] == "232")
                {
                    Min_Plant[j] = 0.8 * CapPrice[0];
                }
                if (plant[j] == "206")
                {
                    Min_Plant[j] = 0.81 * CapPrice[0];
                }
            }

            /////////////////////////////////////////////////////
            string[, ,] Unit_Dec = new string[Plants_Num, Units_Num, 4];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int k = 0; k < Dec_Num; k++)
                    {
                        switch (k)
                        {
                            case 0:
                                oDataTable = utilities.returntbl("select UnitType  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype asc");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString().Trim();
                                break;
                            case 1:
                                oDataTable = utilities.returntbl("select PackageType  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype asc");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString().Trim();
                                break;
                            case 2:
                                Unit_Dec[j, i, k] = "Gas";
                                break;
                            case 3:
                                oDataTable = utilities.returntbl("select  SecondFuel  from UnitsDataMain where  PPID='" + plant[j] + "'ORDER BY packagecode,unittype asc");
                                Unit_Dec[j, i, k] = oDataTable.Rows[i][0].ToString().Trim();
                                break;
                        }
                    }
                }
            }
            double[, , ,] FuelPrice_unit = new double[Plants_Num, Units_Num, Hour, 3];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Efficient[j, i, 0] >= Marketefficient[h]))
                        {
                            FuelPrice_unit[j, i, h, 0] = FuelPrice[0];
                        }

                        if ((Unit_Dec[j, i, 0] == "Steam") && (Efficient[j, i, 1] >= HeatAverage[1] * Marketefficient[h] / HeatAverage[0]))
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice[1];
                            FuelPrice_unit[j, i, h, 2] = FuelPrice[2];
                        }


                        if ((Unit_Dec[j, i, 0] == "Gas") && (Efficient[j, i, 1] >= HeatAverage[2] * Marketefficient[h] / HeatAverage[0]))
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice[1];
                            FuelPrice_unit[j, i, h, 2] = FuelPrice[2];
                        }


                        if ((Efficient[j, i, 0] < Marketefficient[h]))
                        {
                            FuelPrice_unit[j, i, h, 0] = FuelPrice[0] * (Efficient[j, i, 0] / Marketefficient[h]) + FuelPrice_Free[0] * ((Marketefficient[h] - Efficient[j, i, 0]) / Marketefficient[h]);
                        }


                        if ((Unit_Dec[j, i, 0] == "Steam") && (Efficient[j, i, 1] < HeatAverage[1] * Marketefficient[h] / HeatAverage[0]))
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice[1] * (Efficient[j, i, 1] / Marketefficient[h]) + FuelPrice_Free[1] * ((Marketefficient[h] - Efficient[j, i, 1]) / Marketefficient[h]);
                            FuelPrice_unit[j, i, h, 2] = FuelPrice[2] * (Efficient[j, i, 1] / Marketefficient[h]) + FuelPrice_Free[2] * ((Marketefficient[h] - Efficient[j, i, 1]) / Marketefficient[h]);
                        }
                        if ((Unit_Dec[j, i, 0] == "Steam") && (Unit_Dec[j, i, 1] == "CC"))
                        {
                            FuelPrice_unit[j, i, h, 0] = 0;
                            FuelPrice_unit[j, i, h, 1] = 0;
                            FuelPrice_unit[j, i, h, 2] = 0;
                        }

                        if ((Unit_Dec[j, i, 0] == "Gas") && (Efficient[j, i, 1] < HeatAverage[2] * Marketefficient[h] / HeatAverage[0]))
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice[1] * (Efficient[j, i, 1] / Marketefficient[h]) + FuelPrice_Free[1] * ((Marketefficient[h] - Efficient[j, i, 1]) / Marketefficient[h]);
                            FuelPrice_unit[j, i, h, 2] = FuelPrice[2] * (Efficient[j, i, 1] / Marketefficient[h]) + FuelPrice_Free[2] * ((Marketefficient[h] - Efficient[j, i, 1]) / Marketefficient[h]);
                        }
                    }
                }
            }
            //***********************************************************************************
            //MaxPrice(DateTime.Now);
            double MarketMinPrice = 22500;
            oDataTable = utilities.returntbl("select MarketPriceMin from BaseData where Date = '" + smaxdate + "'order by BaseID desc");
            if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][0].ToString() != "")
            {
                MarketMinPrice = MyDoubleParse(oDataTable.Rows[0][0].ToString());
            }


            oDataTable = utilities.returntbl("select CapacityPayment from BaseData where Date = '" + smaxdate + "'order by BaseID desc");
            double[, ,] Price_Capacity_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k))
                            {
                                if (oDataTable.Rows.Count > 0 && oDataTable.Rows[0][0].ToString() != "")
                                {
                                    Price_Capacity_unit[j, i, h] = MyDoubleParse(oDataTable.Rows[0][0].ToString());
                                }
                                else
                                {
                                    Price_Capacity_unit[j, i, h] = 88000;
                                }
                                if (plant[j] == "138" || plant[j] == "104")
                                {
                                    Price_Capacity_unit[j, i, h] = 0;
                                }
                                if (plant[j] == "543")
                                {
                                    Price_Capacity_unit[j, i, h] = 88000;
                                }
                            }
                        }
                    }
                }
            }
            //***********************************************************************************
            double[, ,] TehranStrategy = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k))
                            {
                                TehranStrategy[j, i, h] = 0;
                                if (plant[j] == "104")
                                {
                                    TehranStrategy[j, i, h] = 0;
                                    Unit_Data[j, i, 11] = 30000;
                                    Unit_Data[j, i, 12] = 30000;
                                }
                            }
                        }
                    }
                }
            }
            //***********************************************************************************
            double[, ,] TehranStrategy_Cost = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            if (((int)Unit_Data[j, i, 0] == k))
                            {
                                TehranStrategy_Cost[j, i, h] = 0;
                            }
                        }
                    }
                }
            }
            //***********************************************************************************

            DataTable dts1 = null;
            setparam = new bool[Plants_Num, 8];
            if (ppName != "All")
            {
                dts1 = utilities.returntbl("select * from BidRunSetting where Plant='" + ppName + "'");
                if (dts1.Rows.Count > 0)
                {
                    setparam[0, 0] = MyboolParse(dts1.Rows[0]["FlagOpf"].ToString());
                    setparam[0, 1] = MyboolParse(dts1.Rows[0]["FlagSensetivity"].ToString());
                    setparam[0, 2] = MyboolParse(dts1.Rows[0]["FlagMarketPower"].ToString());
                    setparam[0, 3] = MyboolParse(dts1.Rows[0]["FlagLossPayment"].ToString());
                    setparam[0, 4] = MyboolParse(dts1.Rows[0]["FlagPresolve"].ToString());
                    setparam[0, 5] = MyboolParse(dts1.Rows[0]["FlagCostFunction"].ToString());
                    setparam[0, 6] = MyboolParse(dts1.Rows[0]["FairPlay"].ToString());
                    setparam[0, 7] = MyboolParse(dts1.Rows[0]["LMP"].ToString());

                }
            }
            else
            {

                DataTable dd = utilities.returntbl("select PPName from dbo.PowerPlant");
                for (int i = 0; i < Plants_Num; i++)
                {
                    dts1 = utilities.returntbl("select * from BidRunSetting where Plant='" + dd.Rows[i][0].ToString().Trim() + "'");
                    if (dts1.Rows.Count > 0)
                    {
                        setparam[i, 0] = MyboolParse(dts1.Rows[0]["FlagOpf"].ToString());
                        setparam[i, 1] = MyboolParse(dts1.Rows[0]["FlagSensetivity"].ToString());
                        setparam[i, 2] = MyboolParse(dts1.Rows[0]["FlagMarketPower"].ToString());
                        setparam[i, 3] = MyboolParse(dts1.Rows[0]["FlagLossPayment"].ToString());
                        setparam[i, 4] = MyboolParse(dts1.Rows[0]["FlagPresolve"].ToString());
                        setparam[i, 5] = MyboolParse(dts1.Rows[0]["FlagCostFunction"].ToString());
                        setparam[i, 6] = MyboolParse(dts1.Rows[0]["FairPlay"].ToString());
                        setparam[i, 7] = MyboolParse(dts1.Rows[0]["LMP"].ToString());

                    }
                }
            }
            for (int j = 0; j < Plants_Num; j++)
            {

                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (setparam[j, 5] == false)
                    {
                        Unit_Data[j, i, 12] = 1;
                        Unit_Data[j, i, 15] = 1;
                        Unit_Data[j, i, 25] = 1;
                        Unit_Data[j, i, 14] = 1;
                        for (int h = 0; h < Hour; h++)
                        {
                            FuelPrice_unit[j, i, h, 0] = 1;
                            FuelPrice_unit[j, i, h, 1] = 1;
                        }
                    }
                }
            }
            //***********************************************************************************
            double[, ,] Cost_Per_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int m = 0; m < Mmax; m++)
                        {
                            strCmd = "select count(*) from ConditionUnit" +
                                " where PPID= '" + plant[j] + "'" +
                                " and UnitCode = '" + Unit[j, i] + "'" +
                                " and SecondFuel=1" +
                                " and SecondFuelStartDate<'" + biddingDate + " '" +
                                " AND SecondFuelEndDate>'" + biddingDate + "'";

                            if (utilities.returntbl(strCmd).Rows[0][0].ToString().Trim() != "0")
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Cost_Per_Plant_unit[j, i, h] = ((Unit_Data[j, i, 20] * Unit_Data[j, i, 4] * Unit_Data[j, i, 4] + Unit_Data[j, i, 21] * Unit_Data[j, i, 4] + Unit_Data[j, i, 22]) * FuelPrice_unit[j, i, h, 1] / Unit_Data[j, i, 4]) + Unit_Data[j, i, 12] + ((Unit_Data[j, i, 15] + Unit_Data[j, i, 25] - Price_Capacity_unit[j, i, h]) * TehranStrategy_Cost[j, i, h]) + Unit_Data[j, i, 14];
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Cost_Per_Plant_unit[j, i, h] = (SecondFuel_Fuel[j, i, 2] * FuelPrice_unit[j, i, h, 1] / (Unit_Data[j, i, 4])) + Unit_Data[j, i, 12] + ((Unit_Data[j, i, 15] + Unit_Data[j, i, 25] - Price_Capacity_unit[j, i, h]) * TehranStrategy_Cost[j, i, h]) + Unit_Data[j, i, 14];
                                }
                            }
                            else
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Cost_Per_Plant_unit[j, i, h] = ((Unit_Data[j, i, 8] * Unit_Data[j, i, 4] * Unit_Data[j, i, 4] + Unit_Data[j, i, 9] * Unit_Data[j, i, 4] + Unit_Data[j, i, 10]) * FuelPrice_unit[j, i, h, 0] / (Unit_Data[j, i, 4])) + Unit_Data[j, i, 12] + (Unit_Data[j, i, 15] + Unit_Data[j, i, 25] - Price_Capacity_unit[j, i, h]) * TehranStrategy_Cost[j, i, h] + Unit_Data[j, i, 14];
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Cost_Per_Plant_unit[j, i, h] = (PrimayFuel_Fuel[j, i, 2] * FuelPrice_unit[j, i, h, 0] / (Unit_Data[j, i, 4])) + Unit_Data[j, i, 12] + ((Unit_Data[j, i, 15] + Unit_Data[j, i, 25] - Price_Capacity_unit[j, i, h]) * TehranStrategy_Cost[j, i, h]) + Unit_Data[j, i, 14];
                                }
                            }
                        }
                    }
                }
            }

            //***********************************************************************************
            double[, ,] Cost_Variable_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int m = 0; m < Mmax; m++)
                        {
                            strCmd = "select count(*) from ConditionUnit" +
                                " where PPID= '" + plant[j] + "'" +
                                " and UnitCode = '" + Unit[j, i] + "'" +
                                " and SecondFuel=1" +
                                " and SecondFuelStartDate<'" + biddingDate + " '" +
                                " AND SecondFuelEndDate>'" + biddingDate + "'";

                            if (utilities.returntbl(strCmd).Rows[0][0].ToString().Trim() != "0")
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Cost_Variable_unit[j, i, h] = ((Unit_Data[j, i, 20] * Unit_Data[j, i, 4] * Unit_Data[j, i, 4] + Unit_Data[j, i, 21] * Unit_Data[j, i, 4] + Unit_Data[j, i, 22]) * FuelPrice_unit[j, i, h, 1] / (Unit_Data[j, i, 4])) + Unit_Data[j, i, 12] + Unit_Data[j, i, 14];
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Cost_Variable_unit[j, i, h] = (SecondFuel_Fuel[j, i, 2] * FuelPrice_unit[j, i, h, 1] / (Unit_Data[j, i, 4])) + Unit_Data[j, i, 12] + Unit_Data[j, i, 14];
                                }
                            }
                            else
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Cost_Variable_unit[j, i, h] = ((Unit_Data[j, i, 8] * Unit_Data[j, i, 4] * Unit_Data[j, i, 4] + Unit_Data[j, i, 9] * Unit_Data[j, i, 4] + Unit_Data[j, i, 10]) * FuelPrice_unit[j, i, h, 0] / (Unit_Data[j, i, 4])) + Unit_Data[j, i, 12] + Unit_Data[j, i, 14];
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Cost_Variable_unit[j, i, h] = (PrimayFuel_Fuel[j, i, 2] * FuelPrice_unit[j, i, h, 0] / (Unit_Data[j, i, 4])) + Unit_Data[j, i, 12] + Unit_Data[j, i, 14];
                                }
                            }
                        }
                    }
                }
            }
            //***********************************************************************************
            double[, ,] Cost_variable_Pack = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Cost_Per_Plant_Pack = new double[Plants_Num, Units_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        Cost_variable_Pack[j, i, h] = Cost_Variable_unit[j, i, h];
                        Cost_Per_Plant_Pack[j, i, h] = Cost_Per_Plant_unit[j, i, h];
                        if (Cost_variable_Pack[j, i, h] < Min_Plant[j])
                        {
                            Cost_variable_Pack[j, i, h] = Min_Plant[j];
                        }

                    }
                }
            }
            //***********************************************************************************
            string[] ptimmForPowerLimitedUnit = new string[Plants_Num];

            if (Plants_Num == 1)
            {
                strCmd = " select distinct PPID from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate='" + biddingDate +
                    "' and isEmpty=0 AND PPID=" + plant[0];

                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ptimmForPowerLimitedUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }
            }
            else
            {
                strCmd = " select distinct PPID from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate='" + biddingDate + "'and isEmpty=0";

                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ptimmForPowerLimitedUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < plann; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ptimmForPowerLimitedUnit[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ptimmForPowerLimitedUnit[i] = temp[i];
            }
            /////////////////////////////////////////////////
            int[] unitname = new int[Plants_Num];

            strCmd = " select UnitCode from dbo.PowerLimitedUnit where PowerLimitedUnit.StartDate='" + biddingDate;


            for (b = 0; b < Plants_Num; b++)
            {
                string strCmd2 = strCmd +
                    "'and PPID='" + ptimmForPowerLimitedUnit[b] + "'and isEmpty=0";
                oDataTable = utilities.returntbl(strCmd2);
                unitname[b] = oDataTable.Rows.Count;
            }
            //***********************************************************************************
            double[, ,] Limit_Power = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Limit_Dispatch = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Dispatch_Complete = new double[Plants_Num, Units_Num, Hour];
            string[,] DispatchBlock = Getblock002();
            bool[,] DispatchFlag = new bool[Plants_Num, Units_Num];

            int[, ,] Outservice_Plant_unit = new int[Plants_Num, Units_Num, 24];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Dispatch_Complete[j, i, h] = 0;
                    }
                }
            }
            ///////////////////////////////////////////

            for (int j = 0; j < Plants_Num; j++)
            {
                DataTable checkdate = null;
                DataTable CHECKALL = utilities.returntbl("SELECT distinct StartDate FROM dbo.PowerLimitedUnit WHERE PPID='" + ptimmForPowerLimitedUnit[j] + "'and StartDate='" + biddingDate + "'and isEmpty=0 order by StartDate desc");
                if (CHECKALL.Rows.Count > 0)
                {
                    checkdate = utilities.returntbl("SELECT count(*) FROM dbo.PowerLimitedUnit WHERE PPID='" + ptimmForPowerLimitedUnit[j] + "'and StartDate='" + CHECKALL.Rows[0][0].ToString().Trim() + "'and isEmpty=0");

                    if (checkdate.Rows.Count > 0)
                    {
                        if (int.Parse(checkdate.Rows[0][0].ToString()) == Plant_units_Num[j])
                        {
                            ////////////////////////////////////////////////
                            //for (int j = 0; j < Plants_Num; j++)
                            //{

                            for (int i = 0; i < Plant_units_Num[j]; i++)
                            {

                                strCmd = "select Hour1,Hour2,Hour3,Hour4,Hour5,Hour6,Hour7,Hour8,Hour9,Hour10,Hour11,Hour12,Hour13,Hour14,Hour15,Hour16,Hour17,Hour18,Hour19,Hour20,Hour21,Hour22,Hour23,Hour24" +
                                       " from dbo.PowerLimitedUnit where  PPID='" + ptimmForPowerLimitedUnit[j] + "'and UnitCode='" + Unit[j, i] + "' and StartDate='" + biddingDate + "'and isEmpty=0 order by StartDate desc";

                                oDataTable = utilities.returntbl(strCmd);


                                for (int h = 0; h < Hour; h++)
                                {

                                    if (oDataTable.Rows.Count > 0)
                                    {
                                        DataRow res = oDataTable.Rows[0];
                                        Limit_Power[j, i, h] = MyDoubleParse(res[h].ToString());
                                        Dispatch_Complete[j, i, h] = 1;
                                        if (res[h].ToString() == "")
                                        {
                                            Dispatch_Complete[j, i, h] = 0;
                                        }
                                    }
                                    else
                                    {
                                        Dispatch_Complete[j, i, h] = 0;
                                    }
                                }
                            }
                        }

                        else
                        {
                            for (int k = 0; k < Plant_units_Num[j]; k++)
                            {
                                string ptype = "0";
                                if (Unit_Dec[j, k, 1].Contains("cc") || Unit_Dec[j, k, 1].Contains("CC")) ptype = "1";
                                if (plant[j] == "232") ptype = "0";

                                DataTable UseDispatch = utilities.returntbl("select Hour,DispachableCapacity from dbo.Dispathable where PPID='" + plant[j] + "'and PackageType='"+ ptype +"' and Block='" + DispatchBlock[j, k] + "' and StartDate='" + biddingDate + "'order by StartDate desc");

                                if (UseDispatch.Rows.Count > 0)
                                {
                                    for (int h = 0; h < 24; h++)
                                    {
                                        Limit_Dispatch[j, k, h] = MyDoubleParse(UseDispatch.Rows[h][1].ToString());
                                    }
                                    DispatchFlag[j, k] = true;

                                }
                            }
                        }
                    }
                }

                else
                {
                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        string ptype = "0";
                        if (Unit_Dec[j, k, 1].Contains("cc") || Unit_Dec[j, k, 1].Contains("CC")) ptype = "1";
                        if (plant[j] == "232") ptype = "0";

                        DataTable UseDispatch = utilities.returntbl("select Hour,DispachableCapacity from dbo.Dispathable where PPID='" + plant[j] + "'and PackageType='"+ ptype +"'and  Block='" + DispatchBlock[j, k] + "' and StartDate='" + biddingDate + "'order by StartDate desc");

                        if (UseDispatch.Rows.Count > 0)
                        {
                            for (int h = 0; h < 24; h++)
                            {
                                Limit_Dispatch[j, k, h] = MyDoubleParse(UseDispatch.Rows[h][1].ToString());
                            }
                            DispatchFlag[j, k] = true;
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[] ppkForConditionUnit = new string[Plants_Num];

            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID,Date from dbo.ConditionUnit where( ConditionUnit.OutServiceStartDate<='" + biddingDate +
                    "'AND ConditionUnit.OutServiceEndDate>='" + biddingDate +
                    "')or( dbo.ConditionUnit.MaintenanceStartDate<='" + biddingDate +
                    "'and dbo.ConditionUnit.MaintenanceEndDate>='" + biddingDate + "')" +
                    " order by Date Desc";
                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ppkForConditionUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < plann; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppkForConditionUnit[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }
                for (int i = 0; i < Plants_Num; i++)
                    ppkForConditionUnit[i] = temp[i];
            }
            else
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where ((ConditionUnit.OutServiceStartDate<='" + biddingDate +
                    "'AND ConditionUnit.OutServiceEndDate>='" + biddingDate +
                    "')or (dbo.ConditionUnit.MaintenanceStartDate<='" + biddingDate +
                    "'and dbo.ConditionUnit.MaintenanceEndDate>='" + biddingDate + "'" +
                    " ))AND PPID=" + plant[0];
                //" order by Date Desc";

                oDataTable = utilities.returntbl(strCmd);
                int plann = oDataTable.Rows.Count;

                for (b = 0; b < plann; b++)
                {
                    ppkForConditionUnit[b] = oDataTable.Rows[b][0].ToString().Trim();
                }
            }

            ///////////////////////
            int[] uukForConditionUnit = new int[Plants_Num];
            strCmd = " select UnitCode from dbo.ConditionUnit where(( ConditionUnit.OutServiceStartDate<='" + biddingDate +
                       "'AND ConditionUnit.OutServiceEndDate>='" + biddingDate +
                       "')or( dbo.ConditionUnit.MaintenanceStartDate<='" + biddingDate +
                       "'and dbo.ConditionUnit.MaintenanceEndDate>='" + biddingDate;


            for (b = 0; b < Plants_Num; b++)
            {
                string strCmd2 = strCmd +
                    "'))and PPID='" + ppkForConditionUnit[b] + "'" + " order by Date Desc"; ;
                oDataTable = utilities.returntbl(strCmd2);
                ///////j//////////
                if (oDataTable.Rows.Count != 0)
                {
                    uukForConditionUnit[b] = oDataTable.Rows.Count;
                }
            }
            //--------------------------------------------------------------
            bool om = false;
            bool op = false;
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < 24; h++)
                    {

                        ////////////////////////////maintenance///////////////////////
                        DataTable secondcondition = utilities.returntbl("SELECT  Maintenance,MaintenanceStartDate,MaintenanceEndDate," +
                           " MaintenanceStartHour,MaintenanceEndHour FROM [ConditionUnit] where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "'and Date<=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "')order by Date desc");

                        if (secondcondition.Rows.Count > 0)
                        {
                            DataRow MyRow = secondcondition.Rows[0];

                            if (bool.Parse(MyRow[0].ToString().Trim()))
                            {

                                int myhour = h;
                                string startDate = MyRow[1].ToString().Trim();
                                string endDate = MyRow[2].ToString().Trim();
                                int startHour = (int.Parse(MyRow[3].ToString().Trim()) - 1);
                                int endHour = (int.Parse(MyRow[4].ToString().Trim()) - 1);


                                if (startDate == endDate && biddingDate == endDate)
                                {
                                    if (myhour > endHour) om = false;

                                    else om = true;

                                    if (myhour < startHour) om = false;

                                }

                                else if (biddingDate == startDate)
                                    if (myhour < startHour)
                                        om = false;
                                    else om = true;
                                else if (biddingDate == endDate)
                                    if (myhour > endHour)
                                        om = false;
                                    else om = true;
                                else if (CheckDateSF(biddingDate, startDate, endDate))
                                    om = true;
                                else om = false;
                            }

                            else om = false;
                        }
                        else om = false;
                        ///////////////////outservice///////////////////
                        DataTable outcondition = utilities.returntbl("SELECT  OutService,OutServiceStartDate,OutServiceEndDate," +
                          " OutServiceStartHour,OutServiceEndHour FROM [ConditionUnit] where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "'and Date<=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "')order by Date desc");

                        if (outcondition.Rows.Count > 0)
                        {
                            DataRow MyRow = outcondition.Rows[0];

                            if (bool.Parse(MyRow[0].ToString().Trim()))
                            {

                                int myhour = h;
                                string startDate = MyRow[1].ToString().Trim();
                                string endDate = MyRow[2].ToString().Trim();
                                int startHour = (int.Parse(MyRow[3].ToString().Trim()) - 1);
                                int endHour = (int.Parse(MyRow[4].ToString().Trim()) - 1);

                                if (startDate == endDate && biddingDate == endDate)
                                {
                                    if (myhour > endHour) op = false;
                                    else op = true;

                                    if (myhour < startHour) op = false;

                                }
                                else if (biddingDate == startDate)
                                    if (myhour < startHour)
                                        op = false;
                                    else op = true;
                                else if (biddingDate == endDate)
                                    if (myhour > endHour)
                                        op = false;
                                    else op = true;

                                else if (CheckDateSF(biddingDate, startDate, endDate))
                                    op = true;
                                else op = false;
                            }
                            else op = false;
                        }
                        else op = false;

                        //////////////////////////////////////////////////////////////////////

                        if (om == true || op == true)
                        {
                            Outservice_Plant_unit[j, i, h] = 1;
                        }

                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Pmax_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Pmax_Plant_unit_fix = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Dispatch_Complete_Pack = new double[Plants_Num, Units_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Dispatch_Complete_Pack[j, i, h] = Dispatch_Complete[j, i, h];

                        Pmax_Plant_unit[j, i, h] = Unit_Data[j, i, 4];
                        Pmax_Plant_unit_fix[j, i, h] = Unit_Data[j, i, 4];
                        if (Dispatch_Complete[j, i, h] == 1)
                        {
                            Pmax_Plant_unit[j, i, h] = Limit_Power[j, i, h];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((Pmax_Plant_unit[j, i, h] == 0) && (Dispatch_Complete[j, i, h] == 1))
                        {
                            Outservice_Plant_unit[j, i, h] = 1;
                        }
                        if (Pmax_Plant_unit[j, i, h] == 0)
                        {
                            Pmax_Plant_unit[j, i, h] = Unit_Data[j, i, 4];
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if ((DispatchFlag[j, i] == true))
                        {
                            Pmax_Plant_unit[j, i, h] = Limit_Dispatch[j, i, h];
                            if (Pmax_Plant_unit[j, i, h] > 0)
                            {
                                Outservice_Plant_unit[j, i, h] = 0;
                            }
                            if (Pmax_Plant_unit[j, i, h] == 0)
                            {
                                Outservice_Plant_unit[j, i, h] = 1;
                                Pmax_Plant_unit[j, i, h] = Unit_Data[j, i, 4];
                            }
                        }
                        if (Pmax_Plant_unit[j, i, h] == 0)
                        {
                            Pmax_Plant_unit[j, i, h] = Unit_Data[j, i, 4];
                        }
                        if ((Pmax_Plant_unit[j, i, h] < Unit_Data[j, i, 3]) && (Outservice_Plant_unit[j, i, h] == 0))
                        {
                            MessageBox.Show("Plant Dispatch" + plant[j] + "Unit" + i.ToString().Trim() + "Hour" + h.ToString().Trim() + " is not corrent");
                            break;
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[, ,] Outservice = new int[Plants_Num, Units_Num, 24];
            double[, ,] service_Max = new double[Plants_Num, Units_Num, 24];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < 24; h++)
                    {
                        Outservice[j, i, h] = Outservice_Plant_unit[j, i, h];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            string[, ,] Outservice_Plant_Pack = new string[Plants_Num, Units_Num, 24];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < 24; h++)
                    {
                        Outservice_Plant_Pack[j, i, h] = "No";
                        if ((Outservice[j, i, h] > 0))
                        {
                            Outservice_Plant_Pack[j, i, h] = "Yes";
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Pmax_Plant_Pack = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Pmax_Plant_Pack_fix = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Pmin_Plant_Pack_fix = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if (Outservice_Plant_unit[j, i, h] == 0)
                        {
                            Pmax_Plant_Pack[j, i, h] = Pmax_Plant_unit[j, i, h];
                        }
                        if ((DispatchFlag[j, i] == true) && (Outservice_Plant_Pack[j, i, h] == "No"))
                        {
                            Pmax_Plant_Pack[j, i, h] = Limit_Dispatch[j, i, h];
                        }
                        Pmax_Plant_Pack_fix[j, i, h] = Pmax_Plant_unit_fix[j, i, h];
                        Pmin_Plant_Pack_fix[j, i, h] = Unit_Data[j, i, 3];
                        if ((Pmax_Plant_Pack[j, i, h] < Unit_Data[j, i, 3]) && (Outservice_Plant_Pack[j, i, h] == "No"))
                        {
                            MessageBox.Show("Errorr : Plant Dispatch" + plant[j] + "Unit" + i.ToString().Trim() + "Hour" + h.ToString().Trim() + " is not corrent");
                            break;
                        }
                    }

                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (Pmax_Plant_Pack[j, i, h] == 0)
                        {
                            Outservice_Plant_Pack[j, i, h] = "Yes";
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //int[, ,] CC_Steam_Out = new int[Plants_Num, Units_Num, Hour];
            //for (int h = 0; h < Hour; h++)
            //{
            //    for (int j = 0; j < Plants_Num; j++)
            //    {
            //        for (int i = 0; i < Plant_units_Num[j]; i++)
            //        {
            //            CC_Steam_Out[j, i, h] = 0;
            //            if (Package[j, i].ToString().Trim() == "CC")
            //            {
            //                if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam") & (Outservice_Plant_unit[j, i, h] == 1))
            //                {
            //                    CC_Steam_Out[j, i, h] = 1;
            //                }
            //            }
            //        }
            //    }
            //}
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] MustMaxop = new int[Plants_Num, Units_Num];

            for (int j = 0; j < Plants_Num; j++)
            {

                DataTable detectcode = utilities.returntbl("SELECT MustMax,UnitCode FROM dbo.ConditionUnit WHERE PPID='" + plant[j] + "' and Date=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "')");

                foreach (DataRow myrow in detectcode.Rows)
                {
                    DataTable mustmaxcondition = utilities.returntbl("SELECT distinct PackageCode FROM dbo.UnitsDataMain WHERE UnitCode='" + myrow[1].ToString().Trim() + "'and PPID='" + plant[j] + "'");
                    int detectonepack = int.Parse(mustmaxcondition.Rows[0][0].ToString());

                    for (int k = 0; k < Plant_units_Num[j]; k++)
                    {
                        if (myrow[0].ToString() != "")
                        {
                            if ((mustmaxcondition.Rows.Count > 0) && (bool.Parse(myrow[0].ToString())) && (k == (detectonepack - 1)))
                            {
                                MustMaxop[j, k] = 1;
                            }
                        }
                    }
                }
            }

            DataTable leveltable = utilities.returntbl("select CostLevel,id from dbo.BiddingStrategySetting where id='" + BiddingStrategySettingId.ToString() + "'");
 
            /////////////////////////////////////////////////////
            int[,] MustRunop = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    DataTable mustruncondition = utilities.returntbl("select MustRun  from dbo.ConditionUnit where PPID='" + plant[j] + "' and  UnitCode='" + Unit[j, i] + "' and Date=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "' and  UnitCode='" + Unit[j, i] + "')");
                    if (mustruncondition.Rows.Count > 0)
                    {
                        if (mustruncondition.Rows[0][0].ToString() != "" && bool.Parse(mustruncondition.Rows[0][0].ToString()))
                        {
                            MustRunop[j, i] = 1;
                        }

                    }
                    else
                    {
                        MustRunop[j, i] = 0;
                    }

                }
            }
            /////////////////////////////////////////////////////////////////////
            int[, ,] MustRun = new int[Plants_Num, Units_Num, Hour];
                           for (int j = 0; j < Plants_Num; j++)
                {

                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            MustRun[j, i, h] = 1;

                            if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Steam") & (Outservice_Plant_unit[j, i, h] == 0))
                            {
                                MustRun[j, i, h] = 0;
                            }
                            if ((Unit_Dec[j, i, 1].ToString().Trim() == "Steam") & (Unit_Dec[j, i, 0].ToString().Trim() == "Steam") & (Outservice_Plant_unit[j, i, h] == 0))
                            {
                                MustRun[j, i, h] = 0;
                            }
                            if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Outservice_Plant_unit[j, i, h] == 0))
                            {
                                MustRun[j, i, h] = 0;
                            }
                            if (MustRunop[j, i] == 1)
                            {
                                MustRun[j, i, h] = 0;
                            }

                        }
                    }
                }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        FuelPrice_unit[j, i, h, 0] = FuelPrice_unit[j, i, h, 0];
                        if (Unit_Dec[j, i, 0].ToString().Trim() == "Gas")
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice_unit[j, i, h, 2];
                        }
                        if (Unit_Dec[j, i, 0].ToString().Trim() == "Steam")
                        {
                            FuelPrice_unit[j, i, h, 1] = FuelPrice_unit[j, i, h, 1];
                        }
                        if ((Unit_Dec[j, i, 0] == "Steam") && (Unit_Dec[j, i, 1] == "CC"))
                        {
                            FuelPrice_unit[j, i, h, 0] = 0;
                            FuelPrice_unit[j, i, h, 1] = 0;
                            FuelPrice_unit[j, i, h, 2] = 0;
                        }
                        if (MustRun[j, i, h] == 0)
                        {
                            FuelPrice_unit[j, i, h, 0] = 0;
                            FuelPrice_unit[j, i, h, 1] = 0;
                            FuelPrice_unit[j, i, h, 2] = 0;
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] SecondFuel_Plant_unit = new int[Plants_Num, Units_Num];
            string[] ppkForSecondFuel = new string[Plants_Num];

            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID,Date from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'" +
                    " order by Date Desc";
                oDataTable = utilities.returntbl(strCmd);

                int ppkn1 = oDataTable.Rows.Count;

                for (b = 0; b < oDataTable.Rows.Count; b++)
                    ppkForSecondFuel[b] = oDataTable.Rows[b][0].ToString().Trim();

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < ppkn1; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppkForSecondFuel[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ppkForSecondFuel[i] = temp[i];

            }
            else
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'" +
                    " AND PPID=" + plant[0];
                //" order by Date Desc";

                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                {
                    ppkForSecondFuel[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            int[] uukForSecondFuel = new int[Plants_Num];
            strCmd = " select UnitCode  from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate;



            for (b = 0; b < Plants_Num; b++)
            {
                string strCmd2 = strCmd + "'and PPID='" + ppkForSecondFuel[b] + "'" + " order by Date Desc"; ;
                oDataTable = utilities.returntbl(strCmd2);
                uukForSecondFuel[b] = oDataTable.Rows.Count;

            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    strCmd = " select SecondFuel  from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                        "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate +
                        "'and PPID='" + ppkForSecondFuel[j] + "'" +
                        " and UnitCode='" + Unit[j, i] + "'" +
                    " order by Date Desc";


                    oDataTable = utilities.returntbl(strCmd);
                    if (oDataTable.Rows.Count > 0)
                    {
                        DataRow res = oDataTable.Rows[0];
                        if (bool.Parse(res[0].ToString()))
                        {
                            SecondFuel_Plant_unit[j, i] = 1;
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] FuelQuantity_Plant_unit = new double[Plants_Num, Units_Num];
            string[] ppForQuantity = new string[Plants_Num];

            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID,Date from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'" +
                    " order by Date Desc";

                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                    ppForQuantity[b] = oDataTable.Rows[b][0].ToString().Trim();

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < oDataTable.Rows.Count; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppForQuantity[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }

                for (int i = 0; i < Plants_Num; i++)
                    ppForQuantity[i] = temp[i];


            }
            else
            {
                strCmd = " select distinct PPID from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                         "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate + "'" +
                         " AND PPID = " + plant[0];
                // " order by Date Desc";

                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                    ppForQuantity[b] = oDataTable.Rows[b][0].ToString().Trim();
            }


            int[] uuForQuantity = new int[Plants_Num];
            for (b = 0; b < Plants_Num; b++)
            {
                strCmd = " select UnitCode from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                    "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate +
                    "'and PPID='" + ppForQuantity[b] + "'" +
                    " order by Date Desc";


                oDataTable = utilities.returntbl(strCmd);
                uuForQuantity[b] = oDataTable.Rows.Count;
            }


            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    strCmd = " select FuelForOneDay from dbo.ConditionUnit where ConditionUnit.SecondFuelStartDate<'" + biddingDate +
                        "'AND ConditionUnit.SecondFuelEndDate>'" + biddingDate +
                        "'and PPID='" + ppForQuantity[j] + "'" +
                         " and UnitCode='" + Unit[j, i] + "'" +
                    " order by Date Desc";

                    oDataTable = utilities.returntbl(strCmd);

                    if (oDataTable.Rows.Count > 0)
                    {
                        DataRow res = oDataTable.Rows[0];

                        FuelQuantity_Plant_unit[j, i] = MyDoubleParse(res[0].ToString());

                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            double[] LoadForecasting = GetNearestLoadForecastingDateVlues(biddingDate);

            double[] LoadAverage = new double[Hour];
            LoadAverage[0] = LoadForecasting[1];
            for (int h = 1; h < Hour; h++)
            {
                LoadAverage[h] = LoadForecasting[h] + LoadAverage[(h - 1)];
            }

            //------------------------------------------------------------------------------------------------------------------------

            string[,] PackageTypesList = new string[Plants_Num, Enum.GetNames(typeof(PackageTypePriority)).Length];

            for (int j = 0; j < Plants_Num; j++)
            {

                string cmd = "select Distinct PackageType from dbo.UnitsDataMain where PPID=" + plant[j];
                //" AND (( UnitCode like 'Steam%') OR ((UnitCode not like 'Gascc%')AND(UnitCode not like 'Gas cc%')))";
                oDataTable = utilities.returntbl(cmd);
                for (int k = 0; k < oDataTable.Rows.Count; k++)
                    PackageTypesList[j, k] = oDataTable.Rows[k][0].ToString().Trim();

            }



            int[, ,] CO_steam = new int[Plants_Num, Units_Num, Hour];
            int[, ,] CO_Dispatch = new int[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        CO_steam[j, i, h] = 2;
                        CO_Dispatch[j, i, h] = 2;
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Capacity_Plant_Pack = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        Capacity_Plant_Pack[j, i, h] = Unit_Data[j, i, 4];
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Pmin_Plant_Pack = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam") && (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                        {
                            Pmin_Plant_Pack[j, i, h] = 2 * Unit_Data[j, i, 3];
                            if (Pmin_Plant_Pack[j, i, h] > (0.95 * Pmax_Plant_Pack[j, i, h]))
                            {
                                Pmin_Plant_Pack[j, i, h] = (0.5 * Pmax_Plant_Pack[j, i, h]);
                            }
                        }
                        else if ((Unit_Dec[j, i, 0].ToString().Trim() == "Gas") && (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                        {
                            Pmin_Plant_Pack[j, i, h] = Unit_Data[j, i, 3];
                            if (Pmin_Plant_Pack[j, i, h] > (0.95 * Pmax_Plant_Pack[j, i, h]))
                            {
                                Pmin_Plant_Pack[j, i, h] = (0.7 * Pmax_Plant_Pack[j, i, h]);
                            }
                        }
                        else
                        {
                            Pmin_Plant_Pack[j, i, h] = Unit_Data[j, i, 3];
                            if (Pmin_Plant_Pack[j, i, h] > (0.95 * Pmax_Plant_Pack[j, i, h]))
                            {
                                Pmin_Plant_Pack[j, i, h] = (0.5 * Pmax_Plant_Pack[j, i, h]);
                            }
                        }
                        if (Pmin_Plant_Pack[j, i, h] == 0)
                        {
                            Pmin_Plant_Pack[j, i, h] = Unit_Data[j, i, 3];
                        }
                    }
                }
            }
           

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] Pmin_Plant_Pack_Total = new double[Plants_Num, Hour];
            double[, ,] Pmin_Plant_Pack_Total_Check = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        Pmin_Plant_Pack_Total[j, h] = 1000;
                        Pmin_Plant_Pack_Total_Check[j, i, h] = Pmin_Plant_Pack[j, i, h];

                        if ((Pmin_Plant_Pack_Total_Check[j, i, h] < Pmin_Plant_Pack_Total[j, h]) && (Pmin_Plant_Pack_Total_Check[j, i, h] != 0))
                        {
                            Pmin_Plant_Pack_Total[j, h] = Pmin_Plant_Pack_Total_Check[j, i, h];
                        }
                    }
                    if (Pmin_Plant_Pack_Total[j, h] == 1000)
                    {
                        MessageBox.Show("Pmin_Plant_Pack_Total is Wrong");
                    }
                }
            }


            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] PDiff_Max_Min_Plant_Pack_Check = new double[Plants_Num, Units_Num, Hour];
            double[,] PDiff_Max_Min_Plant_Pack = new double[Plants_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if ((Outservice_Plant_unit[j, i, h] == 0))
                        {
                            PDiff_Max_Min_Plant_Pack_Check[j, i, h] = 0.1 * Pmin_Plant_Pack[j, i, h];
                        }
                        if (PDiff_Max_Min_Plant_Pack_Check[j, i, h] > PDiff_Max_Min_Plant_Pack[j, h])
                        {
                            PDiff_Max_Min_Plant_Pack[j, h] = PDiff_Max_Min_Plant_Pack_Check[j, i, h];
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Number_Count_up = new int[Plants_Num, Units_Num];
            int[,] Number_Count_down = new int[Plants_Num, Units_Num];
            double[, ,] Price_memory = new double[Plants_Num, Units_Num, Hour];
            int[, ,] Eh_memory = new int[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] History_Require = new double[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] History_Dispatch = new double[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[, ,] History_UL = new string[Plants_Num, Units_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] History_Req_T = new double[Plants_Num, Hour];
            double[,] History_Dis_T = new double[Plants_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[,] History_Req_Y = new double[Plants_Num, Hour];
            double[,] History_Dis_Y = new double[Plants_Num, Hour];
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[,] blocks = Getblock();
            DataTable baseplant1 = utilities.returntbl("select PPName from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            string[] ss1 = baseplant1.Rows[0][0].ToString().Trim().Split('-');
            DataTable idtable1 = utilities.returntbl("select PPID from dbo.PowerPlant  where PPName='" + ss1[0].Trim() + "'");

            string preplant1 = idtable1.Rows[0][0].ToString().Trim();
            int indexpre = 0;
            string[,] prenameblocks = preblock(preplant1);
            string preonepack = prenameblocks[0, 0];
            DataTable oDatapre = utilities.returntbl("select Max(PackageCode) from UnitsDataMain where  PPID='" + preplant1 + "'");
            int packprenumber = (int)oDatapre.Rows[0][0];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    oDataTable = utilities.returntbl("select Required,Dispatchable,Contribution  from dbo.DetailFRM005 where PPID='" + plant[j] +
                    "'and TargetMarketDate='" + near005table(blocks[j, k], plant[j]) +
                    "'and Block='" + blocks[j, k] + "'");

                    int r = oDataTable.Rows.Count;

                    if (r > 0)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            History_Require[j, k, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                            History_Dispatch[j, k, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                            History_UL[j, k, h] = oDataTable.Rows[h][2].ToString().Trim();
                        }
                    }
                    else
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            if (Plant_units_Num[j] == Plant_units_Num[j])
                            {
                                History_Require[j, k, h] = Pmax_Plant_Pack[j, k, h];
                                History_Dispatch[j, k, h] = Pmax_Plant_Pack[j, k, h];
                                History_UL[j, k, h] = "N";
                            }
                        }

                    }

                }
            }


            //-------------------------------------------------------------------------------

            double[, ,] Capacity_Require = new double[Plants_Num, Units_Num, Hour];
            double[, ,] Capacity_Dispatch = new double[Plants_Num, Units_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                bool indexu = false;
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    if (setparam[j, 4])
                    {
                        if (k < packprenumber)
                        {
                            oDataTable = utilities.returntbl("select Required,Dispatchable,Contribution  from dbo.DetailFRM005 where PPID='" + preplant1 +
                            "'and TargetMarketDate='" + near005table(prenameblocks[0, k], preplant1) +
                            "'and Block='" + prenameblocks[0, k] + "'");
                        }
                        else
                        {
                            oDataTable = utilities.returntbl("select Required,Dispatchable,Contribution  from dbo.DetailFRM005 where PPID='" + preplant1 +
                            "'and TargetMarketDate='" + near005table(prenameblocks[0, 3], preplant1) +
                            "'and Block='" + prenameblocks[0, packprenumber - 1] + "'");
                        }
                    }
                    else
                    {
                        oDataTable = utilities.returntbl("select Required,Dispatchable,Contribution  from dbo.DetailFRM005 where PPID='" + plant[j] +
                        "'and TargetMarketDate='" + near005table(blocks[j, k], plant[j]) +
                        "'and Block='" + blocks[j, k] + "'");
                    }
                    int r0 = oDataTable.Rows.Count;

                    if (r0 > 0)
                    {
                        foreach (DataRow m in oDataTable.Rows)
                        {
                            if (m["Required"].ToString().Trim() == "0")
                            {
                                indexu = true;
                                break;
                            }

                        }
                        if (indexu == false)
                        {
                            for (int kk = 0; kk < Plant_units_Num[j]; kk++)
                            {
                                for (int h = 0; h < Hour; h++)
                                {
                                    if ((setparam[j, 4]) && (oDataTable.Rows.Count > 0))
                                    {
                                        Capacity_Require[j, kk, h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                                        Capacity_Dispatch[j, kk, h] = MyDoubleParse(oDataTable.Rows[h][1].ToString());
                                        History_UL[j, kk, h] = oDataTable.Rows[h][2].ToString().Trim();
                                    }
                                    else
                                    {
                                        Capacity_Require[j, kk, h] = History_Require[j, k, h];
                                        Capacity_Dispatch[j, kk, h] = History_Dispatch[j, k, h];
                                        History_UL[j, kk, h] = History_UL[j, k, h];
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int test_n = 0;
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        test_n = 0;
                        for (int h = 1; h < Hour; h++)
                        {
                            if ((History_Require[j, k, Hour - h] > 0) & (test_n == 0))
                            {
                                Number_Count_up[j, i] = h;
                            }
                            else
                            {
                                test_n = 1;
                            }
                        }

                        if (Unit_Dec[j, k, 1] == "CC")
                        {
                            if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                            {
                                Number_Count_up[j, i] = 23;
                            }
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        test_n = 0;

                        for (int h = 1; h < Hour; h++)
                        {
                            if ((History_Require[j, k, Hour - h] == 0) & (test_n == 0))
                            {
                                Number_Count_down[j, i] = h;
                            }
                            else
                            {
                                test_n = 1;
                            }
                        }

                        if (Unit_Dec[j, k, 1] == "CC")
                        {
                            if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                            {
                                Number_Count_down[j, i] = 0;
                            }
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if ((Number_Count_down[j, i] > 0) & (Number_Count_up[j, i] > 0))
                    {
                        Number_Count_down[j, i] = 0;
                    }
                    if (Outservice_Plant_unit[j, i, 0] == 1)
                    {
                        Number_Count_down[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] hisstart_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_up[j, i] < GastostartSteam)
                    {
                        hisstart_Plant_unit[j, i] = Number_Count_up[j, i];
                    }
                    if (Number_Count_up[j, i] >= GastostartSteam)
                    {
                        hisstart_Plant_unit[j, i] = GastostartSteam - 1;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] HisShut_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_down[j, i] < Unit_Data[j, i, 16])
                    {
                        HisShut_Plant_unit[j, i] = Number_Count_down[j, i];
                    }
                    if (Number_Count_down[j, i] >= Unit_Data[j, i, 16])
                    {
                        HisShut_Plant_unit[j, i] = (int)(Unit_Data[j, i, 16] - 1);
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Powerhistory_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_up[j, i] >= 0)
                    {
                        Powerhistory_Plant_unit[j, i] = ((int)Unit_Data[j, i, 3]);
                    }
                    if (Number_Count_down[j, i] > 0)
                    {
                        Powerhistory_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] V_history_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if (Number_Count_up[j, i] >= 0)
                    {
                        V_history_Plant_unit[j, i] = 1;
                    }
                    if (Number_Count_down[j, i] > 0)
                    {
                        V_history_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Hisup_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if ((Number_Count_up[j, i] < Unit_Data[j, i, 6]) & (Number_Count_up[j, i] != 0))
                    {
                        Hisup_Plant_unit[j, i] = (int)Unit_Data[j, i, 6] - Number_Count_up[j, i];
                    }
                    if (Number_Count_up[j, i] >= Unit_Data[j, i, 6])
                    {
                        Hisup_Plant_unit[j, i] = 0;
                    }
                    if (Number_Count_up[j, i] == 0)
                    {
                        Hisup_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[,] Hisdown_Plant_unit = new int[Plants_Num, Units_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    if ((Number_Count_down[j, i] < Unit_Data[j, i, 7]) & (Number_Count_down[j, i] > 0))
                    {
                        Hisdown_Plant_unit[j, i] = (int)Unit_Data[j, i, 7] - Number_Count_down[j, i];
                    }
                    if (Number_Count_down[j, i] >= Unit_Data[j, i, 7])
                    {
                        Hisdown_Plant_unit[j, i] = 0;
                    }
                    if (Number_Count_down[j, i] == 0)
                    {
                        Hisdown_Plant_unit[j, i] = 0;
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            string[] ppMinPower = new string[Plants_Num];
            if (Plants_Num > 1)
            {
                strCmd = " select distinct PPID from dbo.ConditionPlant where ConditionPlant.OutServiceStartDate<='" + biddingDate +
                    "'AND ConditionPlant.OutServiceEndDate>='" + biddingDate + "'";
                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                {
                    ppMinPower[b] = oDataTable.Rows[b][0].ToString().Trim();
                }

                string[] temp = new string[Plants_Num];
                for (int ax6 = 0; ax6 < oDataTable.Rows.Count; ax6++)
                {
                    for (int an6 = 0; an6 < Plants_Num; an6++)
                    {
                        if (ppMinPower[ax6] == plant[an6])
                        {
                            temp[an6] = plant[an6];
                        }
                    }
                }
                for (int i = 0; i < Plants_Num; i++)
                    ppMinPower[i] = temp[i];
            }

            else
            {
                strCmd = " select distinct PPID from dbo.ConditionPlant where ConditionPlant.OutServiceStartDate<='" + biddingDate +
                        "'AND ConditionPlant.OutServiceEndDate>='" + biddingDate + "'" +
                        " AND PPID=" + plant[0];
                oDataTable = utilities.returntbl(strCmd);

                for (b = 0; b < oDataTable.Rows.Count; b++)
                {
                    ppMinPower[b] = oDataTable.Rows[b][0].ToString().Trim();
                }
            }


            double[,] Min_Power = new double[Plants_Num, Hour];


            for (int j = 0; j < Plants_Num; j++)
            {
                strCmd = " select PowerMinHour1,PowerMinHour2,PowerMinHour3,PowerMinHour4,PowerMinHour5,PowerMinHour6,PowerMinHour7,PowerMinHour8,PowerMinHour9,PowerMinHour10,PowerMinHour11,PowerMinHour12,PowerMinHour13,PowerMinHour14,PowerMinHour15,PowerMinHour16,PowerMinHour17,PowerMinHour18,PowerMinHour19,PowerMinHour20,PowerMinHour21,PowerMinHour22,PowerMinHour23,PowerMinHour24 from dbo.ConditionPlant" +
                     " where ConditionPlant.PowerMinStartDate<='" + biddingDate +
                     "'AND ConditionPlant.PowerMinEndDate>='" + biddingDate +
                     "'and PPID='" + ppMinPower[j] + "'";
                oDataTable = utilities.returntbl(strCmd);

                if (oDataTable.Rows.Count > 0)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Min_Power[j, h] = MyDoubleParse(oDataTable.Rows[0][h].ToString());
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[] Price_Min = new double[Hour];
            ///////////////////////

            string CommandText5 = "select Date,AcceptedMin from AveragePrice where Date<='" + biddingDate.Trim() + "'";
            oDataTable = utilities.returntbl(CommandText5);


            DateTime dtDate = PersianDateConverter.ToGregorianDateTime(biddingDate);
            DataRow rowNearest = null;
            TimeSpan minSpan = new TimeSpan();

            foreach (DataRow row in oDataTable.Rows)
            {
                DateTime tempDate = PersianDateConverter.ToGregorianDateTime
                    (row["Date"].ToString().Trim());
                TimeSpan tempSpan = dtDate - tempDate;
                if (rowNearest == null || minSpan > tempSpan)
                {
                    minSpan = tempSpan;
                    rowNearest = row;
                }
            }

            if (rowNearest != null)
            {
                string date2 = rowNearest["Date"].ToString().Trim();

                //////////////////////////////////////////


                string commandtext6 = "select AcceptedMin from AveragePrice where Date='" + date2.ToString().Trim() + "'";
                oDataTable = utilities.returntbl(commandtext6);


                for (int h = 0; h < 24 && h < oDataTable.Rows.Count; h++)
                {

                    Price_Min[h] = MyDoubleParse(oDataTable.Rows[h][0].ToString());
                }

            }
            //------------------------------------------------------------------------------------------------------------------------
            double[, ,] Tbid_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Tbid_Plant_unit[j, i, h] = (Pmax_Plant_unit[j, i, h] - Unit_Data[j, i, 3]) / Mmax;
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[, , ,] Fbid_Plant_unit = new double[Plants_Num, Units_Num, Hour, Mmax];
            bool SF = false;

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int m = 0; m < Mmax; m++)
                        {

                            //strCmd = "select count(*) from ConditionUnit" +
                            //    " where PPID= '" + plant[j] + "'" +
                            //    " and UnitCode = '" + Unit[j, i] + "'" +
                            //    " and SecondFuel=1" +
                            //    " and SecondFuelStartDate<'" + biddingDate + " '" +
                            //    " AND SecondFuelEndDate>'" + biddingDate + "'";

                            DataTable secondcondition = utilities.returntbl("SELECT SecondFuel,SecondFuelStartDate,SecondFuelEndDate," +
                            "SecondFuelStartHour,SecondFuelEndHour FROM [ConditionUnit] where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "'and Date<=(select max(Date) from dbo.ConditionUnit where PPID='" + plant[j] + "' AND UnitCode='" + Unit[j, i] + "')order by Date desc");

                            if (secondcondition.Rows.Count > 0)
                            {
                                DataRow MyRow = secondcondition.Rows[0];
                                //foreach (DataRow MyRow in secondcondition.Rows)
                                //{
                                if (bool.Parse(MyRow[0].ToString().Trim()))
                                {
                                    //DateTime CurDate = DateTime.Now;
                                    //int myhour=CurDate.Hour;
                                    int myhour = h;
                                    string startDate = MyRow[1].ToString().Trim();
                                    string endDate = MyRow[2].ToString().Trim();
                                    int startHour = (int.Parse(MyRow[3].ToString().Trim()) - 1);
                                    int endHour = (int.Parse(MyRow[4].ToString().Trim()) - 1);



                                    if (startDate == endDate && biddingDate == endDate)
                                    {
                                        if (myhour > endHour)

                                            SF = false;
                                        else SF = true;

                                        if (myhour < startHour)
                                            SF = false;
                                    }
                                    else if (biddingDate == startDate)
                                        if (myhour < startHour)
                                            SF = false;
                                        else SF = true;
                                    else if (biddingDate == endDate)
                                        if (myhour > endHour)
                                            SF = false;
                                        else SF = true;
                                    else if (CheckDateSF(biddingDate, startDate, endDate))
                                        SF = true;
                                    else SF = false;
                                }
                                else SF = false;

                            }

                            else SF = false;
                            //}


                            if (SF == true)
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Fbid_Plant_unit[j, i, h, m] = ((Unit_Data[j, i, 21] + ((2 * (m + 1)) - 1) * Unit_Data[j, i, 20] * Tbid_Plant_unit[j, i, h] + (2 * Unit_Data[j, i, 20] * Unit_Data[j, i, 3])) * FuelPrice_unit[j, i, h, 1]) + (Unit_Data[j, i, 12] + Unit_Data[j, i, 14]);
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Fbid_Plant_unit[j, i, h, m] = (MarginCost[j, i, 1, m] * FuelPrice_unit[j, i, h, 1]) + (Unit_Data[j, i, 12] + Unit_Data[j, i, 14]);
                                }
                            }
                            else
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Fbid_Plant_unit[j, i, h, m] = (Unit_Data[j, i, 9] + ((2 * (m + 1)) - 1) * Unit_Data[j, i, 8] * Tbid_Plant_unit[j, i, h] + (2 * Unit_Data[j, i, 8] * Unit_Data[j, i, 3])) * FuelPrice_unit[j, i, h, 0] + +(Unit_Data[j, i, 12] + Unit_Data[j, i, 14]);
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Fbid_Plant_unit[j, i, h, m] = MarginCost[j, i, 0, m] * FuelPrice_unit[j, i, h, 0] + (Unit_Data[j, i, 12] + Unit_Data[j, i, 14]);
                                }
                            }
                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[, ,] Fmin_Plant_unit = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int m = 0; m < Mmax; m++)
                        {
                            strCmd = "select count(*) from ConditionUnit" +
                                " where PPID= '" + plant[j] + "'" +
                                " and UnitCode = '" + Unit[j, i] + "'" +
                                " and SecondFuel=1" +
                                " and SecondFuelStartDate<'" + biddingDate + " '" +
                                " AND SecondFuelEndDate>'" + biddingDate + "'";

                            if (utilities.returntbl(strCmd).Rows[0][0].ToString().Trim() != "0")
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Fmin_Plant_unit[j, i, h] = ((Unit_Data[j, i, 20] * Unit_Data[j, i, 3] * Unit_Data[j, i, 3] + Unit_Data[j, i, 21] * Unit_Data[j, i, 3] + Unit_Data[j, i, 22]) * FuelPrice_unit[j, i, h, 1]) + (Unit_Data[j, i, 12] + Unit_Data[j, i, 14]) * Unit_Data[j, i, 3];
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Fmin_Plant_unit[j, i, h] = (FixedCost[j, i, 1] * FuelPrice_unit[j, i, h, 1]) + (Unit_Data[j, i, 12] + Unit_Data[j, i, 14]) * Unit_Data[j, i, 3];
                                }
                            }
                            else
                            {
                                if (Cost_Web[j, i, 0] == 0)
                                {
                                    Fmin_Plant_unit[j, i, h] = ((Unit_Data[j, i, 8] * Unit_Data[j, i, 3] * Unit_Data[j, i, 3] + Unit_Data[j, i, 9] * Unit_Data[j, i, 3] + Unit_Data[j, i, 10]) * FuelPrice_unit[j, i, h, 0]) + (Unit_Data[j, i, 12] + Unit_Data[j, i, 14]) * Unit_Data[j, i, 3];
                                }
                                if (Cost_Web[j, i, 0] == 1)
                                {
                                    Fmin_Plant_unit[j, i, h] = (FixedCost[j, i, 0] * FuelPrice_unit[j, i, h, 0]) + (Unit_Data[j, i, 12] + Unit_Data[j, i, 14]) * Unit_Data[j, i, 3];
                                }
                            }
                        }
                    }
                }
            }

            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            double[, ,] Fstart_Plant_unit = new double[Plants_Num, Units_Num, Start_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int t = 0; t < Start_Num; t++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            if (t < (Unit_Data[j, i, 17]))
                            {
                                Fstart_Plant_unit[j, i, t] = Unit_Data[j, i, 18];
                            }
                            else
                            {
                                Fstart_Plant_unit[j, i, t] = Unit_Data[j, i, 19];
                            }
                            if (MustRun[j, i, h] == 0)
                            {
                                Fstart_Plant_unit[j, i, t] = 0;
                            }
                        }
                    }
                }
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            int[, ,] VI_Plant_unit = new int[Plants_Num, Units_Num, Hour];

            for (int j = 0; j < Plants_Num; j++)
            {
                for (int i = 0; i < Plant_units_Num[j]; i++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        VI_Plant_unit[j, i, h] = 1;

                        strCmd = "select MaintenanceStartDate , MaintenanceEndDate,MaintenanceStartHour, MaintenanceEndHour" +
                            " from ConditionUnit" +
                            " where PPID= '" + plant[j] + "'" +
                            " and UnitCode = '" + Unit[j, i] + "'" +
                            " and Maintenance= 1" +
                            " and (MaintenanceStartDate ='" + biddingDate + "' or MaintenanceEndDate= '" + biddingDate + "')" +
                            " order by Date Desc";
                        oDataTable = utilities.returntbl(strCmd);

                        if (oDataTable.Rows.Count > 0)
                        {
                            DataRow row = oDataTable.Rows[0];
                            if (biddingDate == row["MaintenanceStartDate"].ToString().Trim())
                            {
                                if (row["MaintenanceStartHour"].ToString().Trim() == (h + 1).ToString())
                                    for (int g = h; g < Hour; g++)
                                        VI_Plant_unit[j, i, g] = 0;
                            }
                            else if (biddingDate == row["MaintenanceEndDate"].ToString().Trim())
                            {
                                if (row["MaintenanceEndHour"].ToString().Trim() == (h + 1).ToString())
                                    for (int g = 0; g <= h; g++)
                                        VI_Plant_unit[j, i, g] = 0;
                            }
                        }

                    }

                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Error_e = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            Error_e[j, i, h] = 0;
                        }
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            double[, ,] Pminchek = new double[Plants_Num, Units_Num, Hour];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Pminchek[j, k, h] = 0;
                    }
                }
            }
            //***********************************first opf command************************************
            // int Bus_Num = 3;
            int Bus_Num = 0;

            DataTable busnum = utilities.returntbl("select distinct count(VoltageNumber) from dbo.PostVoltage");
            if (busnum.Rows.Count > 0)
            {
                Bus_Num = int.Parse(busnum.Rows[0][0].ToString().Trim());
            }
               

            ////////////////////////////////important param/////////////////////////////////

            double[] BusNumber = new double[Bus_Num];
            DataTable busnumtabl = utilities.returntbl("SELECT DISTINCT VoltageNumber FROM dbo.PostVoltage");
            for (int bb = 0; bb < Bus_Num; bb++)
            {
                BusNumber[bb] = MyDoubleParse(busnumtabl.Rows[bb][0].ToString().Trim());
            }


            //--------------------------------------------------------------------


            int lineTypesNo = Enum.GetNames(typeof(Line_Opf)).Length;
            double[, ,] lineinfo = new double[lineTypesNo, 24, 2];
 
            //---------------------------------OO9POST-------------------------------

            int day_num = 14;
            int day_nums = 7;

            double[, , ,] Postinfo = new double[Bus_Num, day_num, Hour, 2];
            string[] daynamepost = new string[day_num];

            DataTable ddr = utilities.returntbl("select distinct VoltageNumber from  dbo.DetailFRM009Post order by VoltageNumber asc");

            DateTime Date = PersianDateConverter.ToGregorianDateTime(biddingDate);

            for (int bb = 0; bb < Bus_Num; bb++)
            {
                bool enter = false;
                foreach (DataRow m in ddr.Rows)
                {
                    if (m[0].ToString().Trim() == BusNumber[bb].ToString().Trim())
                    {
                        enter = true;
                        break;
                    }

                }
                if (enter)
                {
                    for (int n = 0; n < day_num; n++)
                    {
                        for (int h = 0; h < 24; h++)
                        {
                            for (int index = 0; index < 2; index++)
                            {
                                DateTime FinalDate = Date.Subtract(new TimeSpan(n, 0, 0, 0));
                                daynamepost[n] = FinalDate.DayOfWeek.ToString().Trim();

                                if (index == 0)
                                {
                                    DataTable dt = utilities.returntbl("select sum(P) from dbo.DetailFRM009Post where Hour='" + h + "' and TargetMarketDate='" + new PersianDate(FinalDate).ToString("d") + "'and VoltageNumber='" + BusNumber[bb] + "'and FlagLine=-1");

                                    Postinfo[bb, n, h, 0] = MyDoubleParse(dt.Rows[0][0].ToString());

                                }
                                else
                                {
                                    DataTable dt = utilities.returntbl("select sum(P) from dbo.DetailFRM009Post where Hour='" + h + "' and TargetMarketDate='" + new PersianDate(FinalDate).ToString("d") + "'and VoltageNumber='" + BusNumber[bb] + "'and FlagLine=1");

                                    Postinfo[bb, n, h, 1] = MyDoubleParse(dt.Rows[0][0].ToString());

                                }
                            }
                        }
                    }
                }
            }
            //-------- -----------------line------------------------------------///////
            ///////////////////outservice///////////////////
            
             DataTable dline=utilities.returntbl("select distinct LineCode from dbo.Lines");
             bool[] outline = new bool[dline.Rows.Count];
             string[] OutName = new string[dline.Rows.Count];
             int indexoutline=0;
             foreach (DataRow mrow in dline.Rows)
             {
                 DataTable outcondition = utilities.returntbl("select OutService,OutServiceStartDate,OutServiceEndDate from dbo.Lines where LineCode='" + mrow[0].ToString().Trim() + "' and OutServiceStartDate<='" + biddingDate + "' and OutServiceEndDate>='" + biddingDate + "'");

                 if (outcondition.Rows.Count > 0)
                 {
                     DataRow MyRow = outcondition.Rows[0];

                     if (bool.Parse(MyRow[0].ToString().Trim()))
                     {
                         OutName[indexoutline] = mrow[0].ToString().Trim();
                         outline[indexoutline] = true;
                     }
                     else
                     {
                         outline[indexoutline] = false;
                    }
                   
                 }
                 indexoutline++;
             }

                 //--------------------------------dispatch constrant---------------------------------//
          
                  DataTable dp = utilities.returntbl("select * from dbo.DispatchConstrant");
                  double[] quantity = new double[dp.Rows.Count];
                  int indexflow = 0;
                  foreach (DataRow mt in dp.Rows)
                  {
                      quantity[indexflow] = MyDoubleParse(mt["Quantity"].ToString());
                      indexflow++;
                  }

                  //-----------------------------------line interchange---------------------------------------------


                  int Line_Num = 0;
                  DataTable linenum = utilities.returntbl("select distinct count(LineCode) from dbo.TransLine");
                  if (linenum.Rows.Count > 0)
                  {
                      Line_Num = int.Parse(linenum.Rows[0][0].ToString().Trim());
                  }

                  string[] Line = new string[Line_Num];
                  string[] T_line = new string[Line_Num];

                  DataTable linecode = utilities.returntbl("select distinct LineCode from dbo.TransLine");

                  for (int L = 0; L < Line_Num; L++)
                  {
                      Line[L] = linecode.Rows[L][0].ToString().Trim();
                  }

                  double[, ,] lineinterchange = new double[Line_Num, Hour, day_num];
                  string[] linedayname = new string[day_num];
  
                  DateTime Date12 = PersianDateConverter.ToGregorianDateTime(biddingDate);

                  for (int lnum = 1; lnum <= lineTypesNo; lnum++)
                  {
                      Line_Opf line = (Line_Opf)Enum.Parse(typeof(Line_Opf), lnum.ToString());

                      string fdate = currentDate;

                      if (fdate != "")
                      {
                          bool enter = false;
                          int Lindex = 0;
                          for (int L = 0; L < Line_Num; L++)
                          {
                              if ((line.ToString() == Line[L]))
                              {
                                  enter = true;
                                  Lindex = L;
                                  break;
                              }

                          }
                          if (enter)
                          {

                              for (int n = 0; n < day_nums; n++)
                              {
                                  DateTime FinalDate = Date12.Subtract(new TimeSpan(n, 0, 0, 0));
                                  linedayname[n] = FinalDate.DayOfWeek.ToString().Trim();
                                  for (int h = 0; h < 24; h++)
                                  {

                                      for (int index = 0; index < 2; index++)
                                      {
                                          if (index == 0)
                                          {
                                              DataTable dtnull = utilities.returntbl("select sum(P) from dbo.DetailFRM009LINE where Hour='"
                                                + h + "' and TargetMarketDate='" + Nearest009line(line.ToString()) + "'and LineCode='" + line.ToString() + "'and FlagLine=-1");

                                              DataTable dt = utilities.returntbl("select sum(P) from dbo.DetailFRM009LINE where Hour='"
                                                  + h + "' and TargetMarketDate='" + new PersianDate(FinalDate).ToString("d") + "'and LineCode='" + line.ToString() + "'and FlagLine=-1");

                                              if (dt.Rows[0][0].ToString() != "")
                                              {
                                                  lineinfo[lnum - 1, h, 0] = MyDoubleParse(dt.Rows[0][0].ToString());
                                                  if ( lineinfo[lnum - 1, h, 0] != 0)
                                                  {
                                                      lineinterchange[Lindex, h, n] = lineinfo[lnum - 1, h, 0];
                                                  }
                                              }
                                              else if (dtnull.Rows[0][0].ToString() != "")
                                              {

                                                  lineinfo[lnum - 1, h, 0] = MyDoubleParse(dtnull.Rows[0][0].ToString());
                                                  if (lineinfo[lnum - 1, h, 0] != 0)
                                                  {
                                                      lineinterchange[Lindex, h, n] = lineinfo[lnum - 1, h, 0];
                                                  }
                                              }
                                              else
                                              {
                                                  DataTable dinter = utilities.returntbl("select Hour" + (h + 1) + " from dbo.InterchangedEnergy where Code='" + line.ToString() + "' and Date='" + fdate + "'");
                                                  if (dinter.Rows.Count > 0)
                                                  {
                                                      lineinfo[lnum - 1, h, 0] = MyDoubleParse(dinter.Rows[0][0].ToString());
                                                      if (lineinfo[lnum - 1, h, 0] != 0)
                                                      {
                                                          lineinterchange[Lindex, h, n] = lineinfo[lnum - 1, h, 0];
                                                      }
                                                  }
                                              }
                                          }
                                          else
                                          {
                                              DataTable dtnull = utilities.returntbl("select sum(P) from dbo.DetailFRM009LINE where Hour='"
                                                  + h + "' and TargetMarketDate='" + Nearest009line(line.ToString()) + "'and LineCode='" + line.ToString() + "'and FlagLine=1");


                                              DataTable dt = utilities.returntbl("select sum(P) from dbo.DetailFRM009LINE where Hour='"
                                                  + h + "' and TargetMarketDate='" + new PersianDate(FinalDate).ToString("d") + "'and LineCode='" + line.ToString() + "'and FlagLine=1");
                                              if (dt.Rows[0][0].ToString() != "")
                                              {
                                                   if (lineinfo[lnum - 1, h, 1] != 0)
                                                  {
                                                      lineinterchange[Lindex, h, n] = -lineinfo[lnum - 1, h, 1];
                                                  }
                                              }
                                              else if (dtnull.Rows[0][0].ToString() != "")
                                              {
                                                  lineinfo[lnum - 1, h, 1] = MyDoubleParse(dtnull.Rows[0][0].ToString());
                                                  if (lineinfo[lnum - 1, h, 1] != 0)
                                                  {
                                                      lineinterchange[Lindex, h, n] = -lineinfo[lnum - 1, h, 1];
                                                  }
                                              }
                                          }
                                      }
                                  }
                              }
                          }
                      }
                  }
                  //--------------------------------------------------------
                  double[] EndLine = new double[Line_Num];
                  double[] StartLine = new double[Line_Num];
                  double[] XLine = new double[Line_Num];
                  double[] CapacityLine = new double[Line_Num];
                  double[] Circuit = new double[Line_Num];
                  double[] Line_Len = new double[Line_Num];

                  for (int L = 0; L < Line_Num; L++)
                  {
                      DataTable transline = utilities.returntbl("select distinct FromBus,ToBus,LineX,RateA,CircuitID,LineLength from dbo.Lines where LineCode='" + Line[L] + "'");
                      EndLine[L] = MyDoubleParse(transline.Rows[0][1].ToString().Trim());
                      StartLine[L] = MyDoubleParse(transline.Rows[0][0].ToString().Trim());
                      XLine[L] = MyDoubleParse(transline.Rows[0][2].ToString().Trim());
                      CapacityLine[L] = MyDoubleParse(transline.Rows[0][3].ToString().Trim());
                      Circuit[L] = MyDoubleParse(transline.Rows[0][4].ToString().Trim());
                      Line_Len[L] = MyDoubleParse(transline.Rows[0][5].ToString().Trim());
                  }
                  for (int L = 0; L < Line_Num; L++)
                  {
                      CapacityLine[L] = CapacityLine[L] * Circuit[L];
                      XLine[L] = XLine[L] * Line_Len[L] * Math.Pow(Circuit[L], -1);
                      //XLine[L] = 0.15;
                  }
            ////-----------------------------------line interchange---------------------------------------------
            int day_for = 6;
            double[,] Line_factor = new double[day_num, Hour];
            double[,] Sum_lineint = new double[day_num, Hour];
            for (int L = 0; L < Line_Num; L++)
            {
                for (int d = 0; d < day_num; d++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Sum_lineint[d, h] = Sum_lineint[d, h] + lineinterchange[L, h, d];
                    }
                }
            }
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            DateTime bidtime1 = PersianDateConverter.ToGregorianDateTime(biddingDate);
            string[] daysbefore14 = new String[14];

            for (int daysbefore = 0; daysbefore < 14; daysbefore++)
            {

                DateTime selectedDate = bidtime1.Subtract(new TimeSpan(daysbefore, 0, 0, 0));
                string perpro = new PersianDate(selectedDate).ToString("d");
                daysbefore14[daysbefore] = perpro;

            }

            //------------------------------------------------------------------------------
            bool IsWeekEnd = false;
            bool yeterdayweekend = false;

            DateTime yestdate = PersianDateConverter.ToGregorianDateTime(biddingDate).AddDays(-1);
            string peryesterday = new PersianDate(yestdate).ToString("d");
            DataTable dtweekend = utilities.returntbl("select distinct Date from dbo.WeekEnd");

            foreach (DataRow nrow in dtweekend.Rows)
            {

                if (nrow[0].ToString().Trim() == biddingDate)
                {
                    IsWeekEnd = true;
                    break;
                }
            }
            ///////////////////////////////////////////////////////////////////////////////////
            double[,] lineinter = new double[Line_Num, Hour];
            for (int L = 0; L < Line_Num; L++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int d = 0; d < day_num; d++)
                    {
                        if (daysbefore14[0] == "Friday")
                        {
                         lineinter[L, h] = lineinterchange[L, h, 6];
                        }
                        else
                        {
                            if (lineinterchange[L, h, d] != 0)
                            {
                                if ((lineinterchange[L, h, (d + 1)] != 0) && (daysbefore14[d + 1] != "Friday"))
                                {
                                    lineinter[L, h] = lineinterchange[L, h, (d + 1)];
                                    break;
                                }
                            }
                        }
                    }
                    if (CapacityLine[L] < lineinter[L, h])
                    {
                        lineinter[L, h] = CapacityLine[L];
                    }
                    if (-CapacityLine[L] > lineinter[L, h])
                    {
                        lineinter[L, h] = -CapacityLine[L];
                    }
                }
            }

            int indexd1 = 0;
            for (int w = 2; w < day_num; w++)
            {

                if (linedayname[w] == "Friday")
                {
                    indexd1 = w;
                    break;
                }
            }

            for (int L = 0; L < Line_Num; L++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    if (IsWeekEnd)
                    {
                        lineinter[L, h] = lineinterchange[L, h, indexd1];
                    }
                }

            }

            //--------------------------------------------------------
            DateTime Date11 = PersianDateConverter.ToGregorianDateTime(currentDate);
            double[,] manategh14 = new double[14,24];

            for (int i = 0; i < day_num; i++)
            {
                DateTime FinalDate = Date11.Subtract(new TimeSpan(i, 0, 0, 0));
                DataTable DT1 = utilities.returntbl("select * from dbo.Manategh where Date='" + new PersianDate(FinalDate).ToString("d") + "' and Code='R03'AND Title=N'توليد بدون صنايع'");
                if (DT1.Rows.Count > 0)
                {
                    for (int T = 0; T < 24; T++)
                    {
                        manategh14[i, T] = MyDoubleParse(DT1.Rows[0][T + 5].ToString());

                    }
                }
            }
            //-------------------------------------------------------
            double[] manategh = new double[ 24];

            DataTable DT = utilities.returntbl("select * from dbo.Manategh where Date='" + currentDate + "' and Code='R03'AND Title=N'توليد بدون صنايع'");
            if (DT.Rows.Count > 0)
            {
                for (int T = 0; T < 24; T++)
                {
                    manategh[T] = MyDoubleParse(DT.Rows[0][T + 5].ToString());

                }

            }
            else
            {
                DataTable DT2 = utilities.returntbl("select * from dbo.Manategh where Date='" + FindNearManategh(currentDate) + "' and Code='R03'AND Title=N'توليد بدون صنايع'");
                if (DT2.Rows.Count > 0)
                {
                    for (int T = 0; T < 24; T++)
                    {
                        manategh[T] = MyDoubleParse(DT2.Rows[0][T + 5].ToString());

                    }
                }
            }
            //--------------------------------------------------------
            double[,,] Demand_factor = new double[Bus_Num,day_num, Hour];

            for (int bb = 0; bb < Bus_Num; bb++)
            {
                for (int d = 0; d < day_num; d++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if (d <= day_for)
                        {
                            if ((Postinfo[bb, 6, h, 0] != 0) && (Postinfo[bb, (7 + d), h, 0] != 0))
                            {
                                Demand_factor[bb, d, h] = (Postinfo[bb, 6, h, 0] / Postinfo[bb, (7 + d), h, 0]);
                            }
                        }
                        else
                        {
                            Demand_factor[bb, d, h] = Demand_factor[bb, (d - 7), h];
                        }
                        if (Demand_factor[bb, d, h] > 1.5)
                        {
                            Demand_factor[bb, d, h] = 1.5;
                        }
                        if (Demand_factor[bb, d, h] < -1.5)
                        {
                            Demand_factor[bb, d, h] = -1.5;
                        }
                        if ((Demand_factor[bb, d, h] < 0.5) && (Demand_factor[bb, d, h] > -0.5) && (Demand_factor[bb, d, h]!=0))
                        {
                            if (Demand_factor[bb, d, h] > 0)
                            {
                                Demand_factor[bb, d, h] = 0.5;
                            }
                            if (Demand_factor[bb, d, h] < 0)
                            {
                                Demand_factor[bb, d, h] = -0.5;
                            }
                        }
                    }
                }
            }
            //--------------------------------------------------------
            double[,] Demand_Bus = new double[Bus_Num, Hour];

            for (int bb = 0; bb < Bus_Num; bb++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int d = 0; d < (day_num - 1); d++)
                    {
                        if ((Demand_factor[bb, d, h] != 0) && (Postinfo[bb, d, h, 0] != 0))
                        {
                            if ((Demand_factor[bb, (d + 1), h] != 0) && (Postinfo[bb, (d + 1), h, 0] != 0))
                            {
                                Demand_Bus[bb, h] = Demand_factor[bb, (d+1 ), h] * Postinfo[bb, (d+1 ), h, 0];
                                break;
                            }
                        }
                    }
                }
            }
            //--------------------------------------------------------
            double[,] Demand_Bus_test = new double[Bus_Num, Hour];

            for (int bb = 0; bb < Bus_Num; bb++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    Demand_Bus_test[bb, h] = Postinfo[bb, 6, h, 0];
                }
            }
            ////////////////////////////////////////////////////////////////////////////////////////
            double[,] Power_Bus = new double[Bus_Num, Hour];
            double[,] sumbus = new double[Bus_Num, Hour];
            double[,] sumbus_yest = new double[Bus_Num, Hour];
             double[,] sumbus_Hour_yest=new double[Bus_Num, Hour];
             double[,] sumbus_Save = new double[Bus_Num, Hour];
            double[,] sumbus_Cost = new double[Bus_Num, Hour];
            int [] Plant_post_pack=new int[Plants_Num];
            double[] Lost = new double[Hour];
            double[] Sum_Demand = new double[Hour];
            for (int h = 0; h < Hour; h++)
            {
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    Sum_Demand[h] = (Sum_Demand[h] + Demand_Bus[bb, h]);
                    sumbus_Cost[bb, h]= 1000;
                }
            }
            for (int h = 0; h < Hour; h++)
            {
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    Lost[h] = 1;
                    if ((manategh[h] != 0) && (Sum_Demand[h] != 0))
                    {
                        Lost[h] =((manategh[h] - Sum_lineint[0, h]) / (Sum_Demand[h]));
                    }
                }
            }
            for (int h = 0; h < Hour; h++)
            {
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    Sum_Demand[h] = 0;
                }
            }

            for (int bb = 0; bb < Bus_Num; bb++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    for (int d = 0; d < (day_num - 1); d++)
                    {
                        if ((Demand_factor[bb, d, h] != 0) && (Postinfo[bb, d, h, 0] != 0))
                        {
                            if ((Demand_factor[bb, (d + 1), h] != 0) && (Postinfo[bb, (d + 1), h, 0] != 0))
                            {
                                Demand_Bus[bb, h] = Lost[h] * Demand_factor[bb, (d + 1), h] * Postinfo[bb, (d + 1), h, 0];
                                break;
                            }
                        }
                    }
                }
            }
         
            int  indexd = 0;
            for (int w = 2; w < day_num; w++)
            {
                if (daynamepost[w] == "Friday")
                {
                    indexd = w;
                    break;
                }
            }
            for (int bb = 0; bb < Bus_Num; bb++)
            {
                for (int h = 0; h < Hour; h++)
                {
                    if (IsWeekEnd)
                    {
                        Demand_Bus[bb, h] = Postinfo[bb, indexd, h, 0];
                    }
                }
            }

            ////////////////////////////////////////////////////////////////////////////////////////
 
            for (int h = 0; h < Hour; h++)
            {
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    Sum_Demand[h] = (Sum_Demand[h] + Demand_Bus[bb, h]);
                }
            }
           //////////////////////////////////////////////////////////////////////////////
            DataSet MyDS = new DataSet();
            SqlDataAdapter Myda = new SqlDataAdapter();
            SqlConnection myConnection = new SqlConnection(ConnectionManager.ConnectionString);
            myConnection.Open();

            Myda.SelectCommand = new SqlCommand("SELECT PPID,PPName FROM PowerPlant", myConnection);
            Myda.Fill(MyDS, "ppid");
            foreach (DataRow MyRow in MyDS.Tables["ppid"].Rows)
            {
                PPIDArray.Add(MyRow["PPID"].ToString().Trim());
                PPIDName.Add(MyRow["PPName"].ToString().Trim());
                PPIDType.Add("real");
            }

            for (int i = 0; i < PPIDArray.Count; i++)
            { 
                SqlCommand mycom=new SqlCommand();
                mycom.Connection = myConnection;
                mycom.CommandText = "SELECT  @result1 =count(PPID) FROM [PPUnit] WHERE PPID=@id SELECT @result2 =count(PPID) FROM [PPUnit] WHERE PPID=@id AND PackageType LIKE 'Combined Cycle%'";
                mycom.Parameters.Add("@id", SqlDbType.NChar, 10);
                mycom.Parameters["@id"].Value = PPIDArray[i].ToString();
                mycom.Parameters.Add("@result1", SqlDbType.Int);
                mycom.Parameters["@result1"].Direction = ParameterDirection.Output;
                mycom.Parameters.Add("@result2", SqlDbType.Int);
                mycom.Parameters["@result2"].Direction = ParameterDirection.Output;
                mycom.ExecuteNonQuery();
                int result1 = (int)mycom.Parameters["@result1"].Value;
                int result2 = (int)mycom.Parameters["@result2"].Value;
                if ((result1 > 1) && (result2 > 0))
                {
                    PPIDArray.Add((int.Parse(PPIDArray[i].ToString())) + 1);
                    PPIDType.Add("virtual");
                }
            }

          ////////////////////////////////////////////////////////////////////////
            string[] firstpack_post=new string[Plants_Num];
            for (int j = 0; j < Plants_Num; j++)
            {
                for (int k = 0; k < Plant_Packages_Num[j]; k++)
                {
                    firstpack_post[j] = Package[j, k];
                    break;
                }
            }
            ///////////////////////////////////////////////////////////////////////
            for (int bb = 0; bb < Bus_Num; bb++)
            {
                DataTable bpower = utilities.returntbl("select distinct PowerSerial from dbo.PowerPost where VoltageNumber='" + BusNumber[bb] + "'order by PowerSerial ");
                if (bpower != null)
                {
                    if (bpower.Rows.Count > 0)
                    {
                        int index = 0;

                        foreach (DataRow mrow in bpower.Rows)
                        {
                             bool nextppid = false;

                            for (int j = 0; j < Plants_Num; j++)
                            {
                              
                                DataTable dt = utilities.returntbl("SELECT count(PPID) as result1 FROM [PPUnit] WHERE PPID='"+plant[j]+"'");
                                DataTable d2 = utilities.returntbl("SELECT count(PPID) as result2 FROM [PPUnit] WHERE PPID='"+plant[j]+"' AND PackageType LIKE 'Combined Cycle%'");
                                if (int.Parse(dt.Rows[0]["result1"].ToString()) > 1 && int.Parse(d2.Rows[0]["result2"].ToString()) > 0)
                                {
                                    DataTable dt3 = utilities.returntbl("select count(*) from dbo.UnitsDataMain where PPID='" + plant[j] + "' and PackageType!='"+firstpack_post[j]+"'");
                                    Plant_post_pack[j] = Plant_Packages_Num[j] - int.Parse(dt3.Rows[0][0].ToString());
                                }
                                else
                                    Plant_post_pack[j] = Plant_Packages_Num[j];


                                if (mrow[0].ToString().Trim() == plant[j])
                                {
                                    index = j;
                                    nextppid = false;
                                    break;
                                }
                                else
                                {
                                    if (int.Parse(dt.Rows[0]["result1"].ToString()) > 1 && int.Parse(d2.Rows[0]["result2"].ToString()) > 0)
                                    {
                                        if (mrow[0].ToString().Trim() == (int.Parse(plant[j]) + 1).ToString())
                                        {
                                            index = j;
                                            nextppid = true;
                                            break;
                                        }
                                    }
                                    else
                                        index = -1;
                                }
                            }
                            if (index != -1)
                            {
                                if (nextppid==true)
                                {
                                    for (int k = 0; k < Plant_post_pack[index]; k++)
                                    {
                                        for (int h = 0; h < Hour; h++)
                                        {
                                            sumbus[bb, h] = sumbus[bb, h] + Pmax_Plant_Pack[index, k, h];
                                            sumbus_Cost[bb, h] = Cost_variable_Pack[index, k, h];
                                        }
                                    }
                                }
                                else 
                                {
                                    int first = 0;
                                    DataTable dt = utilities.returntbl("SELECT count(PPID) as result1 FROM [PPUnit] WHERE PPID='" + plant[index] + "'");
                                    DataTable d2 = utilities.returntbl("SELECT count(PPID) as result2 FROM [PPUnit] WHERE PPID='" + plant[index] + "' AND PackageType LIKE 'Combined Cycle%'");
                                    if (int.Parse(dt.Rows[0]["result1"].ToString()) > 1 && int.Parse(d2.Rows[0]["result2"].ToString()) > 0)
                                    {
                                        DataTable dt3 = utilities.returntbl("select count(*) from dbo.UnitsDataMain where PPID='" + plant[index] + "' and PackageType!='" + firstpack_post[index] + "'");
                                      
                                        first =  Plant_Packages_Num[index] - int.Parse(dt3.Rows[0][0].ToString());
                                       
                                    }

                                    for (int k = first; k < Plant_Packages_Num[index]; k++)
                                    {
                                        for (int h = 0; h < Hour; h++)
                                        {
                                            sumbus[bb, h] = sumbus[bb, h] + Pmax_Plant_Pack[index, k, h];
                                            sumbus_Cost[bb, h] = Cost_variable_Pack[index, k, h];
                                        }
                                    }

                                }
                            }
                            else
                            {
                                DataTable dtnull = utilities.returntbl("select sum(Hour1),sum(Hour2),sum(Hour3),sum(Hour4),sum(Hour5)," +
                                "sum(Hour6),sum(Hour7),sum(Hour8),sum(Hour9)," +
                                "sum(Hour10),sum(Hour11),sum(Hour12),sum(Hour13),sum(Hour14)," +
                                "sum(Hour15),sum(Hour16),sum(Hour17),sum(Hour18),sum(Hour19),sum(Hour20)" +
                                ",sum(Hour21),sum(Hour22),sum(Hour23),sum(Hour24)" +
                                " from dbo.ProducedEnergy where Date='" + currentDate + "' and PPCode='" + mrow[0].ToString().Trim() + "'");
                                //DataTable dtnull = utilities.returntbl("select sum(Hour1),sum(Hour2),sum(Hour3),sum(Hour4),sum(Hour5),sum(Hour6),sum(Hour7),sum(Hour8),sum(Hour9),sum(Hour10),sum(Hour11),sum(Hour12),sum(Hour13),sum(Hour14),sum(Hour15),sum(Hour16),sum(Hour17),sum(Hour18), sum(Hour19),sum(Hour20),sum(Hour21),sum(Hour22),sum(Hour23),sum(Hour23) from  dbo.ProducedEnergy where Date= (select max (Date) from dbo.ProducedEnergy)and PPCode='" + mrow[0].ToString().Trim() + "'");
                                for (int h1 = 0; h1 < 24; h1++)
                                {
                                    sumbus[bb, h1] = sumbus[bb, h1] + MyDoubleParse(dtnull.Rows[0][h1].ToString());
                                    sumbus_Cost[bb, h1] = 0;
                                }
                            }
                        }
                    }
                }
            }

            for (int bb = 0; bb < Bus_Num; bb++)
            {
                DataTable bpower = utilities.returntbl("select distinct PowerSerial from dbo.PowerPost where VoltageNumber='" + BusNumber[bb] + "'order by PowerSerial ");
                if (bpower != null)
                {
                    if (bpower.Rows.Count > 0)
                    {
                        foreach (DataRow mrow in bpower.Rows)
                        {
                            {
                                DataTable dtnull = utilities.returntbl("select sum(Hour1),sum(Hour2),sum(Hour3),sum(Hour4),sum(Hour5)," +
                                "sum(Hour6),sum(Hour7),sum(Hour8),sum(Hour9)," +
                                "sum(Hour10),sum(Hour11),sum(Hour12),sum(Hour13),sum(Hour14)," +
                                "sum(Hour15),sum(Hour16),sum(Hour17),sum(Hour18),sum(Hour19),sum(Hour20)" +
                                ",sum(Hour21),sum(Hour22),sum(Hour23),sum(Hour24)" +
                                " from dbo.ProducedEnergy where Date='" + currentDate + "' and PPCode='" + mrow[0].ToString().Trim() + "'");
                                //DataTable dtnull = utilities.returntbl("select sum(Hour1),sum(Hour2),sum(Hour3),sum(Hour4),sum(Hour5),sum(Hour6),sum(Hour7),sum(Hour8),sum(Hour9),sum(Hour10),sum(Hour11),sum(Hour12),sum(Hour13),sum(Hour14),sum(Hour15),sum(Hour16),sum(Hour17),sum(Hour18), sum(Hour19),sum(Hour20),sum(Hour21),sum(Hour22),sum(Hour23),sum(Hour23) from  dbo.ProducedEnergy where Date= (select max (Date) from dbo.ProducedEnergy)and PPCode='" + mrow[0].ToString().Trim() + "'");
                                for (int h1 = 0; h1 < 24; h1++)
                                {
                                    sumbus_yest[bb, h1] = sumbus_yest[bb, h1] + MyDoubleParse(dtnull.Rows[0][h1].ToString());
                                }
                            }
                        }
                    }
                }
            }


            for (int bb = 0; bb < Bus_Num; bb++)
            {
                for (int h1 = 0; h1 < 24; h1++)
                {
                    {
                        sumbus_Save[bb, h1] = sumbus_yest[bb, h1];
                    }
                }
            }

            for (int bb = 0; bb < Bus_Num; bb++)
            {
                for (int h1 = 0; h1 < 24; h1++)
                {
                    if (sumbus_Save[bb,h1] >= 1.09 * sumbus[bb, h1])
                    {
                        sumbus_Save[bb,h1] = 0.5 * sumbus_Save[bb,h1];
                    }
                    else
                    {
                        sumbus_Save[bb,h1] = 0.7* sumbus_Save[bb,h1];
                    }
                }
            }

            for (int bb = 0; bb < Bus_Num; bb++)
            {
                for (int h1 = 0; h1 < 24; h1++)
                {
                    {
                        sumbus_Hour_yest[bb, h1] = sumbus_Save[bb, h1];
                    }
                }
            }

            double[,] Y_Bus = new double[Bus_Num, Bus_Num];
            for (int bb = 0; bb < Bus_Num; bb++)
            {
                for (int bc = 0; bc < Bus_Num; bc++)
                {

                    Y_Bus[bb, bc] = 0;
                }
            }
            for (int L = 0; L < Line_Num; L++)
            {
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int bc = 0; bc < Bus_Num; bc++)
                    {
                        if ((((StartLine[L] == BusNumber[bb]) & (EndLine[L] == BusNumber[bc])) | ((StartLine[L] == BusNumber[bc]) & (EndLine[L] == BusNumber[bb]))) && (outline[L]==false))
                        {
                            Y_Bus[bb, bc] = Y_Bus[bb, bc] + Math.Pow(XLine[L], -1);
                        }
                    }
                }
            }
            
            for (int bb = 0; bb < Bus_Num; bb++)
            {
                for (int bc = 0; bc < Bus_Num; bc++)
                {
                    Y_Bus[bb, bc] = -Y_Bus[bb, bc];
                }
            }

            for (int bb = 0; bb < Bus_Num; bb++)
            {
                for (int bc = 0; bc < Bus_Num; bc++)
                {
                    if (bc != bb)
                    {
                        Y_Bus[bb, bb] = Y_Bus[bb, bb] + Y_Bus[bb, bc];
                    }
                }
            }

            for (int bb = 0; bb < Bus_Num; bb++)
            {
                Y_Bus[bb, bb] = -Y_Bus[bb, bb];
            }



            double[,] PowerExport = new double[Bus_Num, Hour];
            double[,] TetaExport = new double[Bus_Num, Hour];
            double[,] FlowExport = new double[Line_Num, Hour];
            double[] TestExport = new double[Hour];
            double[] TestExport2 = new double[Hour];
            double[] TestExport3 = new double[Hour];
            double[] TestExport4 = new double[Hour];
            double[,] TestExport5 = new double[Bus_Num, Hour];
            double[,,] TestExport6 = new double[Plants_Num, Units_Num, Hour];


            try
            {
                Cplex cplex = new Cplex();

                 //define variable :
                IIntVar[, ,] V_Plant_unit = new IIntVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            V_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                        }

                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                IIntVar[, ,] X_Plant_unit = new IIntVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if (Unit_Dec[j, i, 1].ToString().Trim() == "CC")
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                X_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                            }
                        }
                    }
                }

               // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                IIntVar[, ,] Y_Plant_unit = new IIntVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Y_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                        }
                    }
                }

               // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                IIntVar[, ,] Z_Plant_unit = new IIntVar[Plants_Num, Units_Num, 24];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < 24; h++)
                        {
                            Z_Plant_unit[j, i, h] = cplex.IntVar(0, 1);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddLe(V_Plant_unit[j, i, h], VI_Plant_unit[j, i, h]);
                        }
                    }
                }

                //////////////////////////////////////////////////////////////////////////
                // define power(i,h):
                INumVar[, ,] Power_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Power_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Reserve(i,h):
                INumVar[, ,] Pres_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Pres_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define delta(i,h,m):
                INumVar[, , ,] delta_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour, Mmax];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int m = 0; m < Mmax; m++)
                            {
                                delta_Plant_unit[j, i, h, m] = cplex.NumVar(0.0, double.MaxValue);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Cost(i,h):
                INumVar[, ,] Cost_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Cost_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define CostStart(i,h):
                INumVar[, ,] Benefit_Plant_unit = new INumVar[Plants_Num, Units_Num, Hour];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Benefit_Plant_unit[j, i, h] = cplex.NumVar(0.0, double.MaxValue);
                        }
                    }
                }

                //**************************************************************************************************
                //  define Sum_Cost_day :
                ILinearNumExpr Sum_Cost_day = cplex.LinearNumExpr();

                double _Sum_Cost_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Cost_day.AddTerm(_Sum_Cost_day, Cost_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //  define Sum_CostStart_day :
                ILinearNumExpr Sum_CostStart_day = cplex.LinearNumExpr();

                double _Sum_CostStart_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_CostStart_day.AddTerm(_Sum_CostStart_day, Cost_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //  define Sum_Power_day :
                ILinearNumExpr Sum_Power_day = cplex.LinearNumExpr();
                double _Sum_Power_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Power_day.AddTerm(_Sum_Power_day, Power_Plant_unit[j, i, h]);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //  define Sum_Benefit_day :
                ILinearNumExpr Sum_Benefit_day = cplex.LinearNumExpr();

                double _Sum_Benefit_day = 1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            Sum_Benefit_day.AddTerm(_Sum_Benefit_day, Benefit_Plant_unit[j, i, h]);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //define Equation_(h):Outservice
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddEq(Z_Plant_unit[j, i, h], Outservice_Plant_unit[j, i, h]);
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //define Equation_(h):Maintenance
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddLe(cplex.Sum(V_Plant_unit[j, i, h], Z_Plant_unit[j, i, h]), 1);
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //define Equation_(h):Profit
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddEq(Benefit_Plant_unit[j, i, h], Cost_Plant_unit[j, i, h]);
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //define Equation_(h):Pmin,Pmax
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddGe(Power_Plant_unit[j, i, h], cplex.Prod(Unit_Data[j, i, 3], V_Plant_unit[j, i, h]));
                        }
                    }
                };

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            cplex.AddLe(Power_Plant_unit[j, i, h], cplex.Prod(Pmax_Plant_unit[j, i, h], V_Plant_unit[j, i, h]));
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):Delta
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            for (int m = 0; m < Mmax; m++)
                            {
                                cplex.AddLe(delta_Plant_unit[j, i, h, m], Tbid_Plant_unit[j, i, h]);
                            }
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):P_delta
                double _Sum_Delta_m = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            ILinearNumExpr Sum_Delta_m = cplex.LinearNumExpr();
                            for (int m = 0; m < Mmax; m++)
                            {
                                Sum_Delta_m.AddTerm(_Sum_Delta_m, delta_Plant_unit[j, i, h, m]);
                            }
                            cplex.AddEq(Power_Plant_unit[j, i, h], cplex.Sum(cplex.Prod(Unit_Data[j, i, 3], V_Plant_unit[j, i, h]), Sum_Delta_m));
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):Cost
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            ILinearNumExpr DeltaBid_m = cplex.LinearNumExpr();
                            for (int m = 0; m < Mmax; m++)
                            {
                                DeltaBid_m.AddTerm(Fbid_Plant_unit[j, i, h, m], delta_Plant_unit[j, i, h, m]);
                            }
                            cplex.AddEq(Cost_Plant_unit[j, i, h], cplex.Sum(DeltaBid_m, cplex.Prod(Fmin_Plant_unit[j, i, h], V_Plant_unit[j, i, h])));
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^uncomment^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //define Equation_(h):Minupdown_1
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = 0; h < Hisup_Plant_unit[j, i]; h++)
                        {
                            cplex.AddEq(V_Plant_unit[j, i, h], 1);
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):Minupdown_2
                double _Minup_2 = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h <= (Hour - Unit_Data[j, i, 6]); h++)
                        {
                            ILinearNumExpr v_up = cplex.LinearNumExpr();
                            for (int hh = h; hh <= (h + Unit_Data[j, i, 6] - 1); hh++)
                            {
                                v_up.AddTerm(_Minup_2, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(v_up, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(V_Plant_unit[j, i, h], V_Plant_unit[j, i, h - 1])));
                            }
                            else
                            {
                                cplex.AddGe(v_up, cplex.Prod(Unit_Data[j, i, 6], cplex.Diff(V_Plant_unit[j, i, h], V_history_Plant_unit[j, i])));
                            }
                        }
                    }
                }
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                double _Mindown_2 = -1;

                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h <= (Hour - (int)Unit_Data[j, i, 7]); h++)
                        {
                            ILinearNumExpr v_down = cplex.LinearNumExpr();
                            for (int hh = h; hh <= (h + Unit_Data[j, i, 7] - 1); hh++)
                            {
                                v_down.AddTerm(_Mindown_2, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(v_down, cplex.Prod(Unit_Data[j, i, 7], cplex.Diff(cplex.Diff(V_Plant_unit[j, i, h - 1], V_Plant_unit[j, i, h]), 1)));
                            }
                            else
                            {
                                cplex.AddGe(v_down, cplex.Prod(Unit_Data[j, i, 7], cplex.Diff(cplex.Diff(V_history_Plant_unit[j, i], V_Plant_unit[j, i, h]), 1)));
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):Minupdown_3
                double _Minup_h = 1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = (Hour - (int)Unit_Data[j, i, 6] + 1); h < Hour; h++)
                        {
                            ILinearNumExpr vh_up = cplex.LinearNumExpr();
                            for (int hh = h; hh < Hour; hh++)
                            {
                                vh_up.AddTerm(_Minup_h, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(cplex.Diff(vh_up, cplex.Diff(cplex.Prod((Hour - h), V_Plant_unit[j, i, h]), cplex.Prod((Hour - h), V_Plant_unit[j, i, h - 1]))), 0);
                            }
                            else
                            {
                                cplex.AddGe(cplex.Diff(vh_up, cplex.Diff(cplex.Prod((Hour - h), V_Plant_unit[j, i, h]), ((Hour - h) * V_history_Plant_unit[j, i]))), 0);
                            }
                        }
                    }
                }
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                double _Mindown_h = -1;
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        for (int h = (Hour - (int)Unit_Data[j, i, 6] + 1); h < Hour; h++)
                        {
                            ILinearNumExpr vh_down = cplex.LinearNumExpr();
                            for (int hh = h; hh < Hour; hh++)
                            {
                                vh_down.AddTerm(_Mindown_h, V_Plant_unit[j, i, hh]);
                            }
                            if (h > 0)
                            {
                                cplex.AddGe(cplex.Sum((Hour - h), cplex.Diff(vh_down, cplex.Diff(cplex.Prod((Hour - h), V_Plant_unit[j, i, h - 1]), cplex.Prod((Hour - h), V_Plant_unit[j, i, h])))), 0);
                            }
                            else
                            {
                                cplex.AddGe(cplex.Sum((Hour - h), cplex.Diff(vh_down, cplex.Diff(((Hour - h) * (1 - V_history_Plant_unit[j, i])), cplex.Prod((Hour - h), V_Plant_unit[j, i, h])))), 0);
                            }
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):Ramp Rate
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {

                        for (int h = 0; h < Hour; h++)
                        {
                            if (h < 23)
                            {
                                cplex.AddLe(Pres_Plant_unit[j, i, h], cplex.Sum(cplex.Prod(Pmax_Plant_unit[j, i, h] * 10, V_Plant_unit[j, i, h + 1]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h], V_Plant_unit[j, i, h + 1]))));
                            }

                            if ((h > 0))
                            {
                                cplex.AddLe(Power_Plant_unit[j, i, h], cplex.Sum(Power_Plant_unit[j, i, h - 1], cplex.Sum(cplex.Prod(Unit_Data[j, i, 1], V_Plant_unit[j, i, h]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h], V_Plant_unit[j, i, h - 1])), cplex.Prod(Pmax_Plant_unit[j, i, h] * 10, cplex.Diff(1, V_Plant_unit[j, i, h])))));
                                cplex.AddLe(cplex.Diff(Power_Plant_unit[j, i, h - 1], Power_Plant_unit[j, i, h]), cplex.Sum(cplex.Prod(Unit_Data[j, i, 2], V_Plant_unit[j, i, h]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h - 1], V_Plant_unit[j, i, h])), cplex.Diff(Pmax_Plant_unit[j, i, h] * 10, cplex.Diff(1, V_Plant_unit[j, i, h - 1]))));
                            }
                            if (h == 0)
                            {
                                cplex.AddLe(Power_Plant_unit[j, i, h], cplex.Sum(Powerhistory_Plant_unit[j, i], cplex.Sum(cplex.Prod(Unit_Data[j, i, 1], V_Plant_unit[j, i, h]), cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_Plant_unit[j, i, h], V_history_Plant_unit[j, i])), cplex.Prod(Pmax_Plant_unit[j, i, h] * 10, cplex.Diff(1, V_Plant_unit[j, i, h])))));
                                cplex.AddLe(cplex.Diff(Powerhistory_Plant_unit[j, i], Power_Plant_unit[j, i, h]), cplex.Sum(cplex.Prod(Unit_Data[j, i, 2], V_Plant_unit[j, i, h]), cplex.Sum(cplex.Prod(Unit_Data[j, i, 5], cplex.Diff(V_history_Plant_unit[j, i], V_Plant_unit[j, i, h])), (Pmax_Plant_unit[j, i, h] * 10 * (1 - V_history_Plant_unit[j, i])))));
                            }
                        }
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):CC_power
                double _Unit_Gas_CC = 0.5;
                for (int h = 0; h < Hour; h++)
                {
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int k = 0; k < Plant_Packages_Num[j]; k++)
                        {
                            if (Package[j, k].ToString().Trim() == "CC")
                            {
                                ILinearNumExpr Unit_Gas_CC = cplex.LinearNumExpr();
                                {
                                    for (int i = 0; i < Plant_units_Num[j]; i++)
                                    {
                                        if ((Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Data[j, i, 0] == k) & (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                                        {
                                            Unit_Gas_CC.AddTerm(_Unit_Gas_CC, Power_Plant_unit[j, i, h]);
                                        }
                                    }
                                }
                                for (int i = 0; i < Plant_units_Num[j]; i++)
                                {
                                    if ((Unit_Dec[j, i, 0].ToString().Trim() == "Steam") & (Unit_Data[j, i, 0] == k) & (Unit_Dec[j, i, 1].ToString().Trim() == "CC"))
                                    {
                                        cplex.AddGe(Unit_Gas_CC, cplex.Diff(Power_Plant_unit[j, i, h], cplex.Prod(cplex.Diff(1, V_Plant_unit[j, i, h]), Pmax_Plant_unit[j, i, h])));
                                    }
                                }
                            }
                        }
                    }
                };

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):CC_X
                double _X_1 = 1;
                double _X_2 = 1;

                int[] i_Start = new int[1];
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int i = 0; i < Plant_units_Num[j]; i++)
                    {
                        if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Steam"))
                        {
                            i_Start[0] = GastostartSteam;
                        }
                        if (i_Start[0] == 0)
                        {
                            i_Start[0] = GastostartSteam;
                        }
                    }
                }
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                double[] _X_Start = new double[1];
                double _Pmax_Steam = 0;
                _X_Start[0] = Math.Pow(i_Start[0], -1);
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int k = 0; k < Plant_Packages_Num[j]; k++)
                    {
                        if (Package[j, k].ToString().Trim() == "CC")
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                                for (int i = 0; i < Plant_units_Num[j]; i++)
                                {
                                    if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Data[j, i, 0] == (k)))
                                    {
                                        if (h < (i_Start[0] + 1))
                                        {
                                            ILinearNumExpr X_1 = cplex.LinearNumExpr();
                                            for (int hh = 0; hh <= h; hh++)
                                            {
                                                X_1.AddTerm(_X_1, V_Plant_unit[j, i, hh]);
                                            }
                                            cplex.AddLe(cplex.Diff(cplex.Prod(_X_Start[0], cplex.Sum(X_1, hisstart_Plant_unit[j, i])), 1), X_Plant_unit[j, i, h]);
                                            cplex.AddLe(X_Plant_unit[j, i, h], cplex.Prod(_X_Start[0], cplex.Sum(X_1, hisstart_Plant_unit[j, i])));

                                        }
                                        if (h > (i_Start[0]))
                                        {
                                            ILinearNumExpr X_2 = cplex.LinearNumExpr();
                                            for (int hh = h - i_Start[0]; hh <= h; hh++)
                                            {

                                                X_2.AddTerm(_X_2, V_Plant_unit[j, i, hh]);
                                            }

                                            cplex.AddLe(cplex.Diff(cplex.Prod(_X_Start[0], X_2), 1), X_Plant_unit[j, i, h]);
                                            cplex.AddLe(X_Plant_unit[j, i, h], cplex.Prod(_X_Start[0], X_2));
                                        }
                                    }
                                }

                                for (int ii = 0; ii < Plant_units_Num[j]; ii++)
                                {
                                    if ((Unit_Dec[j, ii, 1].ToString().Trim() == "CC") & (Unit_Dec[j, ii, 0].ToString().Trim() == "Steam") & (Unit_Data[j, ii, 0] == (k)))
                                    {
                                        _Pmax_Steam = Pmax_Plant_unit[j, ii, h];
                                        ILinearNumExpr X_CC = cplex.LinearNumExpr();
                                        for (int i = 0; i < Plant_units_Num[j]; i++)
                                        {
                                            if ((Unit_Dec[j, i, 1].ToString().Trim() == "CC") & (Unit_Dec[j, i, 0].ToString().Trim() == "Gas") & (Unit_Data[j, i, 0] == (k)))
                                            {
                                                X_CC.AddTerm(_Pmax_Steam * 0.5, X_Plant_unit[j, i, h]);
                                            }
                                        }
                                        cplex.AddGe(X_CC, Power_Plant_unit[j, ii, h]);
                                    }
                                }
                            }
                        }
                    }
                }

       
                INumVar[,] Power = new INumVar[Bus_Num, Hour];
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Power[bb, h] = cplex.NumVar(-double.MaxValue, double.MaxValue);
                        //sumbus[8, h] = 0;
                    }
                }
                INumVar[] Power_Cost_Total_Hour = new INumVar[Hour];
                for (int h = 0; h < Hour; h++)
                {
                    Power_Cost_Total_Hour[h] = cplex.NumVar(-double.MaxValue, double.MaxValue);
                }


                INumVar[,] Line_Flow = new INumVar[Line_Num, Hour];
                for (int L = 0; L < Line_Num; L++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Line_Flow[L, h] = cplex.NumVar(-double.MaxValue, double.MaxValue);
                    }
                }

                INumVar[,] Line_Flow_n = new INumVar[Line_Num, Hour];
                for (int L = 0; L < Line_Num; L++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Line_Flow_n[L, h] = cplex.NumVar(0, double.MaxValue);
                    }
                }
                INumVar[,] Line_Flow_m = new INumVar[Line_Num, Hour];
                for (int L = 0; L < Line_Num; L++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Line_Flow_m[L, h] = cplex.NumVar(0, double.MaxValue);
                    }
                }

                for (int L = 0; L < Line_Num; L++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                       cplex.AddEq(Line_Flow[L, h],cplex.Diff(Line_Flow_m[L, h],Line_Flow_n[L, h]));
                    }
                }


                INumVar[,] Diff_Res = new INumVar[Line_Num, Hour];
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Diff_Res[bb, h] = cplex.NumVar(-double.MaxValue, double.MaxValue);
                    }
                }

                INumVar[,] Diff_Res_n = new INumVar[Line_Num, Hour];
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Diff_Res_n[bb, h] = cplex.NumVar(0, double.MaxValue);
                    }
                }
                INumVar[,] Diff_Res_m = new INumVar[Line_Num, Hour];
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Diff_Res_m[bb, h] = cplex.NumVar(0, double.MaxValue);
                    }
                }

                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        cplex.AddEq(Diff_Res[bb, h], cplex.Diff(Diff_Res_m[bb, h], Diff_Res_n[bb, h]));
                    }
                }

            bool[] inpower = new bool[Bus_Num];
            for (int bb = 0; bb < Bus_Num; bb++)
            {
                DataTable dt1 = utilities.returntbl("select PowerSerial from dbo.PowerPost where VoltageNumber='" + BusNumber[bb] + "' ");
                if (dt1.Rows.Count > 0)
                {
                    for (int I = 0; I < PPIDArray.Count; I++)
                    {
                        if (dt1.Rows[0][0].ToString().Trim() == PPIDArray[I].ToString().Trim())
                        {

                            inpower[bb] = true;
                        }
                    }

                }
            }
                double[,] Cost_diff = new double[Bus_Num, Hour];
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        if ((inpower[bb]) || (sumbus_yest[bb, h]!=0))
                        {
                            Cost_diff[bb,h]=1000;
                        }
                        
                    }
                }
                //----------------------------------------------------------------------------------//
                int num = 0;
                DataTable outl1 = utilities.returntbl("select LineCode from dbo.Lines where OutService=1  and OutServiceStartDate<='" + biddingDate + "' and OutServiceEndDate>='" + biddingDate + "'");
                if (outl1.Rows.Count > 0) num = outl1.Rows.Count;
                else num = 1;

                string[] justout = new string[num];
                int indexout = 0;
                foreach (DataRow mt in outl1.Rows)
                {
                    justout[indexout] = mt[0].ToString().Trim();
                    indexout++;
                }


                PersianDate t1 = new PersianDate(biddingDate);
                DateTime bidNow = PersianDateConverter.ToGregorianDateTime(t1);
                int monthofbidding = bidNow.Month;
                int temperature = 0;

                switch (monthofbidding)
                {
                    case 1:
                        temperature = 25;
                        break;
                    case 2:
                        temperature = 25;
                        break;

                    case 3:
                        temperature = 25;
                        break;

                    case 4:
                        temperature = 30;
                        break;

                    case 5:
                        temperature = 30;
                        break;

                    case 6:
                        temperature = 30;
                        break;

                    case 7:
                        temperature = 40;
                        break;

                    case 8:
                        temperature = 40;
                        break;

                    case 9:
                        temperature = 40;
                        break;

                    case 10:
                        temperature = 30;
                        break;

                    case 11:
                        temperature = 30;
                        break;

                    case 12:
                        temperature = 30;
                        break;
                }
                // temperature = 40; 
                DataTable dflow1 = utilities.returntbl("select * from dbo.DispatchConstrant");

                for (int h = 0; h < Hour; h++)
                {
                    ILinearNumExpr Sum_Line_Flow = cplex.LinearNumExpr();


                    foreach (DataRow mt in dflow1.Rows)
                    {
                        for (int y = 0; y < num; y++)
                        {
                            if (mt[10].ToString().Trim() == "EveryTime")
                            {
                                for (int i = 0; i < 4; i++)
                                {
                                    for (int L = 0; L < Line_Num; L++)
                                    {
                                        if ((mt[9].ToString().Trim() == "EveryTemperature") || (mt[9].ToString().Trim() == temperature.ToString()))
                                        {
                                            if ((mt[i].ToString().Trim() == Line[L]))
                                            {
                                                Sum_Line_Flow.AddTerm(0.01 * MyDoubleParse(mt[i + 4].ToString().Trim()), Line_Flow_m[L, h]);
                                                Sum_Line_Flow.AddTerm(0.01 * MyDoubleParse(mt[i + 4].ToString().Trim()), Line_Flow_n[L, h]);
                                            }
                                        }
                                    }
                                }
                                cplex.AddGe(MyDoubleParse(mt[8].ToString().Trim()), Sum_Line_Flow);
                            }
                            else if ((mt[10].ToString().Trim() == justout[y]))
                            {
                                for (int i = 0; i < 4; i++)
                                {
                                    for (int L = 0; L < Line_Num; L++)
                                    {
                                        if ((mt[9].ToString().Trim() == "EveryTemperature") || (mt[9].ToString().Trim() == temperature.ToString()))
                                        {
                                            if ((mt[i].ToString().Trim() == Line[L]))
                                            {
                                                Sum_Line_Flow.AddTerm(0.01 * MyDoubleParse(mt[i + 4].ToString().Trim()), Line_Flow_m[L, h]);
                                                Sum_Line_Flow.AddTerm(0.01 * MyDoubleParse(mt[i + 4].ToString().Trim()), Line_Flow_n[L, h]);
                                            }
                                        }
                                    }
                                }
                                cplex.AddGe(MyDoubleParse(mt[8].ToString().Trim()), Sum_Line_Flow);
                            }
                        }
                    }
                }
                      
                //-------------------------------------------------------------------------------
                INumVar[,] Power_Plant = new INumVar[Bus_Num, Hour];
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Power_Plant[bb, h] = cplex.NumVar(0, double.MaxValue);
                    }
                }

                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    //DataTable bpower = utilities.returntbl("select sum(Hour1),sum(Hour2),sum(Hour3),sum(Hour4),sum(Hour5),sum(Hour6),sum(Hour7),sum(Hour8),sum(Hour9),sum(Hour10),sum(Hour11),sum(Hour12),sum(Hour13),sum(Hour14),sum(Hour15),sum(Hour16),sum(Hour17),sum(Hour18),sum(Hour19),sum(Hour20),sum(Hour21),sum(Hour22),sum(Hour23),sum(Hour24) from dbo.PowerLimitedUnit where StartDate='" + biddingDate + "' and PPID in (select  distinct PowerSerial from dbo.PowerPost where VoltageNumber='" + BusNumber[bb] + "')");
                    DataTable bpower = utilities.returntbl("select distinct PowerSerial from dbo.PowerPost where VoltageNumber='" + BusNumber[bb] + "'order by PowerSerial ");
                    if (bpower != null)
                    {
                        if (bpower.Rows.Count > 0)
                        {
                            int index = 0;

                            foreach (DataRow mrow in bpower.Rows)
                            {
                                bool nextppid = false;

                                for (int j = 0; j < Plants_Num; j++)
                                {

                                    DataTable dt = utilities.returntbl("SELECT count(PPID) as result1 FROM [PPUnit] WHERE PPID='" + plant[j] + "'");
                                    DataTable d2 = utilities.returntbl("SELECT count(PPID) as result2 FROM [PPUnit] WHERE PPID='" + plant[j] + "' AND PackageType LIKE 'Combined Cycle%'");
                                    if (int.Parse(dt.Rows[0]["result1"].ToString()) > 1 && int.Parse(d2.Rows[0]["result2"].ToString()) > 0)
                                    {
                                        DataTable dt3 = utilities.returntbl("select count(*) from dbo.UnitsDataMain where PPID='" + plant[j] + "' and PackageType!='" + firstpack_post[j] + "'");
                                        Plant_post_pack[j] = Plant_Packages_Num[j] - int.Parse(dt3.Rows[0][0].ToString());
                                    }
                                    else
                                        Plant_post_pack[j] = Plant_Packages_Num[j];


                                    if (mrow[0].ToString().Trim() == plant[j])
                                    {
                                        index = j;
                                        nextppid = false;
                                        break;
                                    }
                                    else
                                    {
                                        if (int.Parse(dt.Rows[0]["result1"].ToString()) > 1 && int.Parse(d2.Rows[0]["result2"].ToString()) > 0)
                                        {
                                            if (mrow[0].ToString().Trim() == (int.Parse(plant[j]) + 1).ToString())
                                            {
                                                index = j;
                                                nextppid = true;
                                                break;
                                            }
                                        }
                                        else
                                            index = -1;
                                    }
                                }
                                if (index != -1)
                                {
                                    if (nextppid == true)
                                    {

                                        for (int h = 0; h < Hour; h++)
                                        {
                                            ILinearNumExpr Sum_unit = cplex.LinearNumExpr();
                                            for (int k = 0; k < Plant_post_pack[index]; k++)
                                            {
                                                for (int i = 0; i < Plant_units_Num[index]; i++)
                                                {
                                                    if (((int)Unit_Data[index, i, 0] == k))
                                                    {
                                                        Sum_unit.AddTerm(1, Power_Plant_unit[index, i, h]);
                                                    }
                                                }
                                            }
                                            cplex.AddEq(Power_Plant[bb, h], Sum_unit);
                                        }
                                    }
                                    else
                                    {
                                        int first = 0;
                                        DataTable dt = utilities.returntbl("SELECT count(PPID) as result1 FROM [PPUnit] WHERE PPID='" + plant[index] + "'");
                                        DataTable d2 = utilities.returntbl("SELECT count(PPID) as result2 FROM [PPUnit] WHERE PPID='" + plant[index] + "' AND PackageType LIKE 'Combined Cycle%'");
                                        if (int.Parse(dt.Rows[0]["result1"].ToString()) > 1 && int.Parse(d2.Rows[0]["result2"].ToString()) > 0)
                                        {
                                            DataTable dt3 = utilities.returntbl("select count(*) from dbo.UnitsDataMain where PPID='" + plant[index] + "' and PackageType!='" + firstpack_post[index] + "'");

                                            first = Plant_Packages_Num[index] - int.Parse(dt3.Rows[0][0].ToString());

                                        }
                                        // ----------------- k=first k<plant_post_pack//-----before edit--------------------//

                                        for (int h = 0; h < Hour; h++)
                                        {
                                            ILinearNumExpr Sum_unit = cplex.LinearNumExpr();
                                            for (int k = first; k < Plant_Packages_Num[index]; k++)
                                            {
                                                for (int i = 0; i < Plant_units_Num[index]; i++)
                                                {
                                                    if (((int)Unit_Data[index, i, 0] == k))
                                                    {
                                                        Sum_unit.AddTerm(1, Power_Plant_unit[index, i, h]);
                                                    }
                                                }
                                            }
                                            cplex.AddEq(Power_Plant[bb, h], Sum_unit);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                INumVar[,] Teta = new INumVar[Bus_Num, Hour];
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Teta[bb, h] = cplex.NumVar(-3.14, 3.14);
                        cplex.AddEq(Teta[1, h], 0);
                    }
                }

                INumVar[,] Load_shed = new INumVar[Bus_Num, Hour];
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Load_shed[bb, h] = cplex.NumVar(0, double.MaxValue);
                    }
                }

                INumVar[] Load_shed_Total = new INumVar[Hour];
                for (int h = 0; h < Hour; h++)
                {
                    Load_shed_Total[h] = cplex.NumVar(0, double.MaxValue);
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[,] Load_shed_m = new INumVar[Bus_Num, Hour];
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Load_shed_m[bb, h] = cplex.NumVar(-double.MaxValue, 0);
                    }
                }

                INumVar[] Load_shed_Total_m = new INumVar[Hour];
                for (int h = 0; h < Hour; h++)
                {
                    Load_shed_Total_m[h] = cplex.NumVar(-double.MaxValue, 0);
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                INumVar[] Power_Total = new INumVar[Hour];
                for (int h = 0; h < Hour; h++)
                {
                    Power_Total[h] = cplex.NumVar(-double.MaxValue, double.MaxValue);
                }

                INumVar[] Alien_Total = new INumVar[Hour];
                for (int h = 0; h < Hour; h++)
                {
                    Alien_Total[h] = cplex.NumVar(-double.MaxValue, double.MaxValue);
                }


                INumVar[,] Line_Alien = new INumVar[Line_Num, Hour];
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        Line_Alien[bb, h] = cplex.NumVar(-double.MaxValue, double.MaxValue);
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):
                for (int h = 0; h < Hour; h++)
                {
                    for (int L = 0; L < Line_Num; L++)
                    {
                        if (outline[L])
                        {
                            lineinter[L, h] = 0;
                        }
                    }
                }
                int[,] Line_Count = new int[Line_Num, Hour];
                double[,] Bus_Alien = new double[Bus_Num, Hour];

                for (int h = 0; h < Hour; h++)
                {
                    for (int bb = 0; bb < Bus_Num; bb++)
                    {
                        for (int L = 0; L < Line_Num; L++)
                        {
                            if ((StartLine[L] == BusNumber[bb]))
                            {
                                Bus_Alien[bb, h] = Bus_Alien[bb, h] + lineinter[L, h];
                            }
                        }
                    }
                }

                for (int h = 0; h < Hour; h++)
                {
                    for (int bb = 0; bb < Bus_Num; bb++)
                    {
                        {
                            cplex.AddEq(Line_Alien[bb, h], Bus_Alien[bb, h]);
                        }
                    }
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):

                for (int h = 0; h < Hour; h++)
                {
                    ILinearNumExpr Sum_Line_Alien = cplex.LinearNumExpr();
                    for (int bc = 0; bc < Bus_Num; bc++)
                    {
                        Sum_Line_Alien.AddTerm(1, Line_Alien[bc, h]);
                    }
                    cplex.AddEq(Alien_Total[h], Sum_Line_Alien);
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        cplex.AddLe(Power_Plant[bb, h], sumbus[bb, h]);
                    }
                }; 
                 //define Equation_(h):
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        cplex.AddEq(Diff_Res[bb, h], cplex.Diff(Power_Plant[bb, h], sumbus_yest[bb, h]));
                    }
                };
               // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):
                for (int h = 0; h < Hour; h++)
                {
                    for (int L = 0; L < Line_Num; L++)
                    {
                        if (outline[L])
                        {
                            cplex.AddEq(Line_Flow[L, h], 0);
                        }
                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):

                for (int h = 0; h < Hour; h++)
                {
                    ILinearNumExpr Sum_Cost = cplex.LinearNumExpr();
                    for (int bc = 0; bc < Bus_Num; bc++)
                    {
                        Sum_Cost.AddTerm(sumbus_Cost[bc, h], Power_Plant[bc, h]);
                        Sum_Cost.AddTerm(3000000, Load_shed[bc, h]);
                        Sum_Cost.AddTerm(-3000000, Load_shed_m[bc, h]);
                        //Sum_Cost.AddTerm(Cost_diff[bc,h], Diff_Res_m[bc, h]);
                        //Sum_Cost.AddTerm(Cost_diff[bc,h], Diff_Res_n[bc, h]);
                    }
                    cplex.AddEq(Power_Cost_Total_Hour[h], Sum_Cost);
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):
                for (int h = 0; h < Hour; h++)
                {
                    ILinearNumExpr Sum_Load_shed = cplex.LinearNumExpr();
                    for (int bb = 0; bb < Bus_Num; bb++)
                    {
                        Sum_Load_shed.AddTerm(1, Load_shed[bb, h]);
                    }
                    cplex.AddEq(Load_shed_Total[h], Sum_Load_shed);
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):
                for (int h = 0; h < Hour; h++)
                {
                    ILinearNumExpr Sum_Load_shed_m = cplex.LinearNumExpr();
                    for (int bb = 0; bb < Bus_Num; bb++)
                    {
                        Sum_Load_shed_m.AddTerm(1, Load_shed_m[bb, h]);
                    }
                    cplex.AddEq(Load_shed_Total_m[h], Sum_Load_shed_m);
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):
                ILinearNumExpr Sum_Cost_Total = cplex.LinearNumExpr();
                for (int h = 0; h < Hour; h++)
                {
                    Sum_Cost_Total.AddTerm(1, Power_Cost_Total_Hour[h]);
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):
                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        ILinearNumExpr Sum_Teta = cplex.LinearNumExpr();
                        for (int bc = 0; bc < Bus_Num; bc++)
                        {
                            Sum_Teta.AddTerm(Y_Bus[bb, bc], Teta[bc, h]);
                        }
                        cplex.AddEq(Sum_Teta, cplex.Prod(Power[bb, h], Math.Pow(100, -1)));
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):
                for (int L = 0; L < Line_Num; L++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        ILinearNumExpr Sum_Teta_Start = cplex.LinearNumExpr();
                        for (int bb = 0; bb < Bus_Num; bb++)
                        {
                            if (StartLine[L] == BusNumber[bb])
                            {
                                Sum_Teta_Start.AddTerm(1, Teta[bb, h]);
                            }
                        }
                        ILinearNumExpr Sum_Teta_End = cplex.LinearNumExpr();
                        for (int bb = 0; bb < Bus_Num; bb++)
                        {
                            if (EndLine[L] == BusNumber[bb])
                            {
                                Sum_Teta_End.AddTerm(1, Teta[bb, h]);
                            }
                        }
                        cplex.AddLe(cplex.Diff(Sum_Teta_Start, Sum_Teta_End), CapacityLine[L] * XLine[L] * Math.Pow(100, -1));
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):
                for (int j = 0; j < Plants_Num; j++)
                {
                    for (int bb = 0; bb < Bus_Num; bb++)
                    {
                        DataTable dt = utilities.returntbl("SELECT count(PPID) as result1 FROM [PPUnit] WHERE PPID='" + plant[j] + "'");
                        DataTable d2 = utilities.returntbl("SELECT count(PPID) as result2 FROM [PPUnit] WHERE PPID='" + plant[j] + "' AND PackageType LIKE 'Combined Cycle%'");
                        if (int.Parse(dt.Rows[0]["result1"].ToString()) > 1 && int.Parse(d2.Rows[0]["result2"].ToString()) > 0)
                        {
                            d2 = utilities.returntbl("select VoltageNumber from dbo.PowerPost where PowerSerial='" + plant[j] + "' or PowerSerial='" + (int.Parse(plant[j]) + 1) + "'");

                            int i1 = 0;
                            int i2 = 0;
                            for (int g = 0; g < Bus_Num; g++)
                            {
                                if (int.Parse(d2.Rows[0][0].ToString()) == BusNumber[g])
                                {
                                    i1 = g;
                                }
                                if (int.Parse(d2.Rows[1][0].ToString()) == BusNumber[g])
                                {
                                    i2 = g;
                                }
                            }

                            for (int h = 0; h < Hour; h++)
                            {
                                cplex.AddGe(cplex.Prod(Power_Plant[i1, h], sumbus[i2, h]), cplex.Prod(Power_Plant[i2, h], sumbus[i1, h]));
                                cplex.AddGe(cplex.Prod(Power_Plant[i2, h], sumbus[i1, h]), cplex.Prod(Power_Plant[i1, h], sumbus[i2, h]*0.8));
                                cplex.AddGe(cplex.Prod(Power_Plant[33, h], sumbus[2, h]), cplex.Prod(Power_Plant[2, h], sumbus[33, h]));
                                cplex.AddGe(cplex.Prod(Power_Plant[2, h], sumbus[33, h]), cplex.Prod(Power_Plant[33, h], sumbus[2, h]*0.8));
                            }
                            break;
                        }

                    }
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):
                for (int L = 0; L < Line_Num; L++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        ILinearNumExpr Sum_Teta_Start = cplex.LinearNumExpr();
                        for (int bb = 0; bb < Bus_Num; bb++)
                        {
                            if (StartLine[L] == BusNumber[bb])
                            {
                                Sum_Teta_Start.AddTerm(1, Teta[bb, h]);
                            }
                        }
                        ILinearNumExpr Sum_Teta_End = cplex.LinearNumExpr();
                        for (int bb = 0; bb < Bus_Num; bb++)
                        {
                            if (EndLine[L] == BusNumber[bb])
                            {
                                Sum_Teta_End.AddTerm(1, Teta[bb, h]);
                            }
                        }
                        cplex.AddGe(cplex.Diff(Sum_Teta_Start, Sum_Teta_End), CapacityLine[L] * XLine[L] * Math.Pow(100, -1) * -1);
                    }
                };
                // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):
                for (int L = 0; L < Line_Num; L++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        ILinearNumExpr Sum_Teta_Start = cplex.LinearNumExpr();
                        for (int bb = 0; bb < Bus_Num; bb++)
                        {
                            if (StartLine[L] == BusNumber[bb])
                            {
                                Sum_Teta_Start.AddTerm(1, Teta[bb, h]);
                            }
                        }
                        ILinearNumExpr Sum_Teta_End = cplex.LinearNumExpr();
                        for (int bb = 0; bb < Bus_Num; bb++)
                        {
                            if (EndLine[L] == BusNumber[bb])
                            {
                                Sum_Teta_End.AddTerm(1, Teta[bb, h]);
                            }
                        }
                        cplex.AddEq(cplex.Prod(cplex.Diff(Sum_Teta_Start, Sum_Teta_End), Math.Pow(XLine[L], -1) * Math.Pow(100, +1)), Line_Flow[L, h]);
                    }
                };
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                // define Equation_(h):
                for (int h = 0; h < Hour; h++)
                {
                    ILinearNumExpr Sum_PowerPlant = cplex.LinearNumExpr();
                    for (int bb = 0; bb < Bus_Num; bb++)
                    {
                        Sum_PowerPlant.AddTerm(1, Power_Plant[bb, h]);
                    }
                    cplex.AddEq(Power_Total[h], Sum_PowerPlant);
                    cplex.AddGe(Power_Total[h], cplex.Sum(cplex.Sum(Sum_Demand[h], Alien_Total[h]), cplex.Sum(Load_shed_Total_m[h], Load_shed_Total[h])));
                }

                for (int bb = 0; bb < Bus_Num; bb++)
                {
                    for (int h = 0; h < Hour; h++)
                    {
                        cplex.AddEq(Power[bb, h], cplex.Diff(Power_Plant[bb, h], cplex.Sum(Load_shed_m[bb, h], cplex.Sum(Demand_Bus[bb, h], cplex.Sum(Load_shed[bb, h], Line_Alien[bb, h])))));
                    }
                }

                //*****************************************uncomment * *********************************************************
                //cplex.AddMaximize(cplex.Sum(Sum_Benefit_day, Sum_Cost_Total));
                
                //*******************************************************************************************************
                //Export
                //*******************************************************************************************************
                cplex.AddMinimize(Sum_Cost_Total);
                cplex.SetParam(Cplex.DoubleParam.EpGap, 0.1);
                cplex.SetParam(Cplex.DoubleParam.EpOpt, 0.001);
                cplex.SetParam(Cplex.DoubleParam.EpAGap, 0.001);
                cplex.SetParam(Cplex.DoubleParam.TiLim, 100000);
                cplex.SetParam(Cplex.IntParam.ItLim, 100000);
                
                //*******************************************************************************************************

                if (cplex.Solve())
                {
                    MessageBox.Show(" Value =  Calculate   " + cplex.ObjValue + "   " + cplex.GetStatus());
                    for (int bb = 0; bb < Bus_Num; bb++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                            PowerExport[bb, h] = cplex.GetValue(Power_Plant[bb, h]);
                            TetaExport[bb, h] = cplex.GetValue(Teta[bb, h]);
                            TestExport[h] = cplex.GetValue(Load_shed_Total[h]);
                            TestExport2[h] = cplex.GetValue(Power_Total[h]);
                            TestExport3[h] = cplex.GetValue(Alien_Total[h]);
                            TestExport4[h] = cplex.GetValue(Load_shed_Total_m[h]);
                            TestExport5[bb,h] = cplex.GetValue(Load_shed_m[bb,h]);
                        }
                    }
                    for (int j = 0; j < Plants_Num; j++)
                    {
                        for (int i = 0; i < Plant_units_Num[j]; i++)
                        {
                            for (int h = 0; h < Hour; h++)
                            {
                              TestExport6[j, i, h] = cplex.GetValue(Power_Plant_unit[j, i, h]);
                            }
                        }
                    }
                    for (int L = 0; L < Line_Num; L++)
                    {
                        for (int h = 0; h < Hour; h++)
                        {
                          FlowExport[L, h] = cplex.GetValue(Line_Flow[L, h]);
                          if (Math.Abs(FlowExport[L, h]) > (0.9 * CapacityLine[L]))
                          {
                              T_line[L] = Line[L];
                          }
                        }
                    }
                }

                cplex.End();
                //return false;
            }
            catch (ILOG.CPLEX.CouldNotInstallColumnException e1)
            {
                return false;
            }


            //-------------------------inserto db line flow--------------------------------------------//



            for (int L = 0; L < Line_Num; L++)
            {
                bool eopf = false;
                for (int lnum = 1; lnum <= lineTypesNo; lnum++)
                {

                    Line_Opf line = (Line_Opf)Enum.Parse(typeof(Line_Opf), lnum.ToString());
                    if (Line[L] == line.ToString().Trim())
                    {
                        eopf = true;
                        break;


                    }

                }

                for (int h = 0; h < Hour; h++)
                {

                    if (eopf == false)
                    {
                        DataTable del1 = utilities.returntbl("delete from dbo.LineFlow where LineCode='" + Line[L] + "'and Hour='" + (h + 1) + "'and Date='" + biddingDate + "'");
                        if (FlowExport[L, h] > -1 && FlowExport[L, h] < 1) FlowExport[L, h] = 0.0;
                        DataTable dt = utilities.returntbl("insert into dbo.LineFlow (LineCode,Hour,Value,Date)values('" + Line[L] + "','" + (h + 1) + "','" + FlowExport[L, h] + "','" + biddingDate + "')");
                    }
                    else
                    {
                        //DataTable dinter = utilities.returntbl("select * from dbo.InterchangedEnergy where Code='" + Line[L] + "' and Date='" + biddingDate + "'");

                        DataTable del2 = utilities.returntbl("delete from dbo.LineFlow where LineCode='" + Line[L] + "'and Hour='" + (h + 1) + "' and Date='" + biddingDate + "'");
                        DataTable dt = utilities.returntbl("insert into dbo.LineFlow (LineCode,Hour,Value,Date)values('" + Line[L] + "','" + (h + 1) + "','" + lineinter[L, h] + "','" + biddingDate + "')");

                    }

                }

            }

            //--------------------------inserto db power opf------------------------------------------------
            // DataTable powerreg = utilities.returntbl("select distinct PPCode from dbo.RegionNetComp where Code='R01'");
            //--------------------------inserto db power opf------------------------------------------------
            // DataTable powerreg = utilities.returntbl("select distinct PPCode from dbo.RegionNetComp where Code='R01'");
            for (int bb = 0; bb < Bus_Num; bb++)
            {

                DataTable dt = utilities.returntbl("select distinct PowerSerial,VoltageNumber from dbo.PowerPost where VoltageNumber='" + BusNumber[bb] + "'");
                foreach (DataRow mrow in dt.Rows)
                {

                    //if (mrow[1].ToString().Trim() == BusNumber[bb].ToString())
                    //{
                    int i1 = 0;
                    for (int g = 0; g < Bus_Num; g++)
                    {
                        if (mrow[1].ToString().Trim() == BusNumber[g].ToString())
                        {
                            i1 = g;
                            break;
                        }
                    }

                    for (int h = 0; h < Hour; h++)
                    {
                        DataTable del2 = utilities.returntbl("delete from PowerOpf where PPID='" + mrow[0].ToString().Trim() + "'and Hour='" + (h + 1) + "'and Date='" + biddingDate + "'");
                        DataTable dt2 = utilities.returntbl("insert into PowerOpf (PPID,Hour,Value,Date)values('" + mrow[0].ToString().Trim() + "','" + (h + 1) + "','" + PowerExport[i1, h] + "','" + biddingDate + "')");
                    }
                    //}
                }

            }


            //----------------------------------------------------------------------------------------------

            return true;
       }


        private string buildmaxdate(string[] arrmax)
        {
            string maxdate = null;
            for (int fd = 0; fd < arrmax.Length; fd++)
            {
                if (maxdate == null || string.Compare(maxdate, arrmax[fd]) < 0)
                    maxdate = arrmax[fd];
            }
            return maxdate;

        }
     

        DateTime b;
        DateTime c;

        private string[,] Getblock()
        {

            string[,] blocks = new string[Plants_Num, Units_Num];
            DataTable oDataTable = null;


            oDataTable = utilities.returntbl("select distinct PPID from dbo.PowerPlant");




            for (int j = 0; j < Plants_Num; j++)
            {
                int packcode = 0;
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    if (packcode < Plant_Packages_Num[j]) packcode++;

                    oDataTable = utilities.returntbl("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "'");

                    int ppid = int.Parse(plant[j]);

                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {
                        Initial = "C" + (k + 1).ToString();

                        if (Findccplant(plant[j]))
                        {
                            ppid++;
                        }


                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY UnitType ASC");
                        int loopcount = 0;
                        foreach (DataRow mn in oDataTable.Rows)
                        {

                            string ptype = mn[0].ToString().Trim();
                            ptype = ptype.ToLower();
                            ptype = ptype.Replace("cc", "c");
                            string[] sp = ptype.Split('c');
                            ptype = sp[0].Trim() + sp[1].Trim();
                            if (ptype.Contains("gas"))
                            {
                                ptype = ptype.Replace("gas", "G");

                            }
                            else
                            {
                                ptype = ptype.Replace("steam", "S");

                            }
                            ptype = ppid + "-" + ptype;

                            blocks[j, k] = ptype;

                            loopcount++;
                            if (loopcount != oDataTable.Rows.Count) k++;
                        }
                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                       plant[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'ORDER BY UnitType ASC");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;
                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;
                    }


                }
            }
            return blocks;

        }
        private string[,] Getblock002()
        {
            string[,] blocks1 = new string[Plants_Num, Units_Num];

            DataTable oDataTable = null;



            oDataTable = utilities.returntbl("select distinct PPID from dbo.PowerPlant");





            for (int j = 0; j < Plants_Num; j++)
            {

                int packcode = 0;
                for (int k = 0; k < Plant_units_Num[j]; k++)
                {
                    if (packcode < Plant_Packages_Num[j]) packcode++;
                    oDataTable = utilities.returntbl("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                       plant[j] + "'" + " and packageCode='" + packcode + "'");


                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY UnitType ASC");
                        int loopcount = 0;
                        foreach (DataRow mn in oDataTable.Rows)
                        {

                            string ptype = mn[0].ToString().Trim();
                            ptype = ptype.ToLower();
                            ptype = ptype.Replace("cc", "c");
                            string[] sp = ptype.Split('c');
                            ptype = sp[0].Trim() + sp[1].Trim();
                            if (ptype.Contains("gas"))
                                ptype = ptype.Replace("gas", "G");
                            else if (ptype.Contains("steam"))
                                ptype = ptype.Replace("steam", "S");

                            blocks1[j, k] = ptype;

                            loopcount++;
                            if (loopcount != oDataTable.Rows.Count) k++;

                        }



                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'ORDER BY UnitType ASC");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                        string blockName1 = Initial;

                        blocks1[j, k] = blockName1;

                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        plant[j] + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'ORDER BY UnitType ASC");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                        string blockName1 = Initial;

                        blocks1[j, k] = blockName1;

                    }


                }
            }
            return blocks1;

            

        }
        private bool CheckDateSF(string date, string start, string end)
        {
            bool result = false;
            string temp1 = date.Remove(4);
            start = start.Trim();
            end = end.Trim();
            if ((start != "") && (end != ""))
            {
                string temp2 = start.Remove(4);
                if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
                else
                {
                    temp1 = date.Remove(7);
                    temp1 = temp1.Remove(0, temp1.Length - 2);
                    temp2 = start.Remove(7);
                    temp2 = temp2.Remove(0, temp2.Length - 2);
                    if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                    else if (int.Parse(temp1) < int.Parse(temp2)) result = false;
                    else
                    {
                        temp1 = date.Remove(0, date.Length - 2);
                        temp2 = start.Remove(0, start.Length - 2);
                        if (int.Parse(temp1) > int.Parse(temp2)) result = true;
                        else result = false;
                    }
                }
                if (!result) return (result);
                else
                {
                    temp1 = date.Remove(4);
                    temp2 = end.Remove(4);
                    if (int.Parse(temp1) < int.Parse(temp2)) return (true);
                    else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
                    else
                    {
                        temp1 = date.Remove(7);
                        temp1 = temp1.Remove(0, temp1.Length - 2);
                        temp2 = end.Remove(7);
                        temp2 = temp2.Remove(0, temp2.Length - 2);
                        if (int.Parse(temp1) < int.Parse(temp2)) return (true);
                        else if (int.Parse(temp1) > int.Parse(temp2)) return (false);
                        else
                        {
                            temp1 = date.Remove(0, date.Length - 2);
                            temp2 = end.Remove(0, end.Length - 2);
                            if (int.Parse(temp1) < int.Parse(temp2))// || (temp1 == temp2))
                                return (true);
                            else return (false);
                        }
                    }
                }
            }
            return false;
        }
        private double[] GetNearestLoadForecastingDateVlues(string biddingDate)
        {
            double[] result = new double[24];

            SqlConnection MyConnection = new SqlConnection();
            MyConnection.ConnectionString = ConStr;
            MyConnection.Open();

            // double[] hourCap = new double[24];

            SqlCommand MyCom = new SqlCommand();
            MyCom.Connection = MyConnection;
            DataSet MaxDS = new DataSet();
            SqlDataAdapter Maxda = new SqlDataAdapter();
            Maxda.SelectCommand = MyCom;

            // Insert into FinalForcast table
            MyCom.CommandText =
                "select Date,DateEstimate from LoadForecasting where Date<='" + biddingDate.Trim() + "'";
            Maxda.Fill(MaxDS);


            DateTime dtDate = PersianDateConverter.ToGregorianDateTime(biddingDate);
            DataRow rowNearest = null;
            TimeSpan minSpan = new TimeSpan();
            //if (MaxDS.Tables[0].Rows.Count>0)
            //    minSpan = 
            foreach (DataRow row in MaxDS.Tables[0].Rows)
            {
                DateTime tempDate = PersianDateConverter.ToGregorianDateTime
                    (row["Date"].ToString().Trim());
                TimeSpan tempSpan = dtDate - tempDate;
                if (rowNearest == null || minSpan > tempSpan)
                {
                    minSpan = tempSpan;
                    rowNearest = row;
                }
            }

            if (rowNearest != null)
            {
                string date2 = rowNearest["Date"].ToString().Trim();

                //////////////////////////////////////////
                // in case there are more than one date, find the one with the maxmimum DateEstimate
                MyCom = new SqlCommand();
                MyCom.Connection = MyConnection;
                MaxDS = new DataSet();
                Maxda = new SqlDataAdapter();
                Maxda.SelectCommand = MyCom;

                // Insert into FinalForcast table

                MyCom.CommandText =
                    "select LoadForecasting.* from LoadForecasting where Date='" + date2.ToString().Trim() + "'";
                Maxda.Fill(MaxDS);


                DataRow maxDateRow = null;
                string maxDate = null;

                foreach (DataRow row in MaxDS.Tables[0].Rows)
                {
                    string temp = row["DateEstimate"].ToString().Trim();
                    if (maxDate == null || String.Compare(maxDate, temp) < 0)
                    {
                        maxDate = temp;
                        maxDateRow = row;
                    }
                }

                for (int h = 0; h < 24; h++)
                {
                    string colName = "Hour" + (h + 1).ToString().Trim();
                    result[h] = MyDoubleParse(maxDateRow[colName].ToString());
                }
            }

            return result;
        }
        public bool Findccplant(string ppid)
        {
            int tr = 0;

            oDataTable = utilities.returntbl("select  distinct  PackageType from dbo.UnitsDataMain where ppid='" + ppid + "'");
            for (int i = 0; i < oDataTable.Rows.Count; i++)
            {

                if (oDataTable.Rows.Count > 1 && oDataTable.Rows[i][0].ToString().Contains("CC"))

                    return true;
            }

            return false;
        }
        private double MyDoubleParse(string str)
        {
            if (str.Trim() == "")
                return 0;
            else
            {
                try { return double.Parse(str.Trim()); }
                catch { return 0; }
            }
        }
        private string Nearest009line(string linecode)
        {
            string finaldate = "";
            DataTable dmax = utilities.returntbl("select distinct TargetMarketDate from dbo.DetailFRM009LINE where TargetMarketDate<=(select max(TargetMarketDate) from dbo.DetailFRM009LINE)order by TargetMarketDate asc");
            DataTable dtime = utilities.returntbl("select max(TargetMarketDate) from dbo.DetailFRM009LINE");
            DateTime maxtime = PersianDateConverter.ToGregorianDateTime(dtime.Rows[0][0].ToString());
            for (int d = 0; d < dmax.Rows.Count; d++)
            {
                DateTime time = maxtime.Subtract(new TimeSpan(d, 0, 0, 0));
                DataTable dt = utilities.returntbl("select * from dbo.DetailFRM009LINE where LineCode='" + linecode +
                    "' and TargetMarketDate='" + new PersianDate(time).ToString("d") + "'");
                if (dt.Rows.Count == 24)
                {
                    finaldate = new PersianDate(time).ToString("d");
                    break;
                }
            }

            return finaldate;
        }
        private double[] Nearest009post(string busnum, string serial, int h)
        {
            double[] sum = new double[2];
            string finaldate = "";
            DataTable dmax = utilities.returntbl("select distinct TargetMarketDate from DetailFRM009Post where TargetMarketDate<=(select max(TargetMarketDate) from DetailFRM009Post)order by TargetMarketDate asc");
            DataTable dtime = utilities.returntbl("select max(TargetMarketDate) from DetailFRM009Post");
            DateTime maxtime = PersianDateConverter.ToGregorianDateTime(dtime.Rows[0][0].ToString());


            for (int d = 0; d < dmax.Rows.Count; d++)
            {
                DateTime time = maxtime.Subtract(new TimeSpan(d, 0, 0, 0));
                DataTable dt = utilities.returntbl("select * from DetailFRM009Post where VoltageNumber='" + busnum +
                    "' and TargetMarketDate='" + new PersianDate(time).ToString("d") + "'and ConsumedSerial='" + serial + "'AND Hour='" + h + "'");
                if (dt.Rows.Count > 0)
                {
                    finaldate = new PersianDate(time).ToString("d");



                    if (dt.Rows[0][5].ToString() == "1")
                    {
                        sum[1] = MyDoubleParse(dt.Rows[0][4].ToString());
                    }
                    else
                        sum[0] = MyDoubleParse(dt.Rows[0][4].ToString());


                    break;
                }
            }

            return sum;
        }

        private static bool MyboolParse(string str)
        {
            if (str.Trim() == "")
                return false;
            else
            {
                try { return bool.Parse(str.Trim()); }
                catch { return false; }
            }
        }
       
        private string near005table(string block, string plant)
        {
            string date1 = currentDate;
            DateTime date = PersianDateConverter.ToGregorianDateTime(date1);
            DataTable oDataTable = utilities.returntbl("select Required,Dispatchable,Contribution  from dbo.DetailFRM005 where PPID='" + plant +
                            "'and TargetMarketDate='" + date + "'and Block='" + block + "'");
            if (oDataTable.Rows.Count == 0)
            {
                for (int d = 1; d < 100; d++)
                {
                    DateTime saddate = date;
                    saddate = saddate.AddDays(-d);
                    oDataTable = utilities.returntbl("select Required,Dispatchable,Contribution  from dbo.DetailFRM005 where PPID='" + plant +
                               "'and TargetMarketDate='" + new PersianDate(saddate).ToString("d") +
                               "'and Block='" + block + "'");
                    if (oDataTable.Rows.Count > 0)
                    {
                        date1 = new PersianDate(saddate).ToString("d");
                        break;
                    }
                }
            }
            return date1;
        }
        private double[,] MaxPrice(DateTime date)
        {
            double[,] maxs = new double[Plants_Num, 24];

            for (int j = 0; j < Plants_Num; j++)
            {
                DataSet myDs = UtilityPowerPlantFunctions.GetUnits(int.Parse(plant[j]));

                DataTable[] orderedPackages = UtilityPowerPlantFunctions.OrderPackages(myDs);

                ///////////////////////////////////////////////////////////////////////////
                string finalplant = plant[j];

                DataTable baseplantfor7day = utilities.returntbl("select PPName from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
                string[] s = baseplantfor7day.Rows[0][0].ToString().Trim().Split('-');
                DataTable idtable7day = utilities.returntbl("select PPID from dbo.PowerPlant  where PPName='" + s[0].Trim() + "'");

                string preplant7day = idtable7day.Rows[0][0].ToString().Trim();

                DataTable packagetable7day = utilities.returntbl("SELECT DISTINCT  PackageType  FROM dbo.PPUnit WHERE PPID='" + preplant7day.Trim() + "'");

                string packtype7day = "";
                try
                {
                    packtype7day = s[1].Trim();
                }
                catch
                {

                }

                if (packtype7day == "Combined Cycle") packtype7day = "CC";
                if (packtype7day == "" || packtype7day == null)
                {
                    for (int i = 0; i < packagetable7day.Rows.Count; i++)
                    {
                        if (packagetable7day.Rows[i][0].ToString().Trim() == "Steam")
                        {
                            packtype7day = "Steam";
                            break;
                        }
                        else if (packagetable7day.Rows[i][0].ToString().Trim() == "Combined Cycle") packtype7day = "CC";

                    }
                }
                DataSet myDs7 = UtilityPowerPlantFunctions.GetUnits(int.Parse(preplant7day));
                DataTable[] orderedPackages7 = OrderPackages7(myDs7);
                foreach (DataTable dtPackageType in orderedPackages)
                {
                    foreach (DataTable dtPackageType7 in orderedPackages7)
                    {
                        if ((setparam[j, 4] == true))
                        {
                            orderedPackages = orderedPackages7;
                            finalplant = preplant7day;
                            break;

                        }
                    }

                }


                ////////////////////////////////////////////////////////////////////////////

                foreach (DataTable dtPackageType in orderedPackages)
                {

                    int packageOrder = 0;
                    if (dtPackageType.TableName.Contains(PackageTypePriority.CC.ToString()) &&
                    orderedPackages.Length > 1)
                        packageOrder = 1;
                    CMatrix NSelectPrice = UtilityPowerPlantFunctions.CalculatePowerPlantMaxBid
                           (Convert.ToInt32(finalplant), dtPackageType, date, packageOrder, biddingDate);
                    if (!NSelectPrice.Zero || NSelectPrice != null)
                    {
                        for (int h = 0; h < 24; h++)
                        {
                            maxs[j, h] = NSelectPrice[0, h];
                        }
                        break;
                    }
                }

            }
            return maxs;
        }
        private string[,] preblock(string ppidpre)
        {
            DataTable oDatapre = utilities.returntbl("select unitcode from UnitsDataMain where  PPID='" + ppidpre + "'");
            int packprenumber = oDatapre.Rows.Count;

            DataTable oDatapre1 = utilities.returntbl("select max(packagecode) from UnitsDataMain where  PPID='" + ppidpre + "'");
            int pack = (int)oDatapre1.Rows[0][0];

            string[,] blocks = new string[1, packprenumber];
            string temp = "";


            for (int j = 0; j < 1; j++)
            {
                int packcode = 0;
                for (int k = 0; k < oDatapre.Rows.Count; k++)
                {
                    if (packcode < pack) packcode++;

                    oDataTable = utilities.returntbl("select distinct PackageType from dbo.UnitsDataMain where PPID='" +
                        ppidpre + "'" + " and packageCode='" + packcode + "'");

                    int ppid = int.Parse(ppidpre);

                    DataRow myRow = oDataTable.Rows[0];

                    string packageType = myRow["PackageType"].ToString().Trim();

                    string Initial = "";
                    if (packageType == "CC")
                    {

                        if (Findccplant(ppidpre))
                        {
                            ppid++;
                        }

                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        ppidpre + "'" + " and packageCode='" + packcode + "'and PackageType='" + packageType + "'ORDER BY UnitType ASC");
                        int loopcount = 0;
                        foreach (DataRow mn in oDataTable.Rows)
                        {

                            string ptype = mn[0].ToString().Trim();
                            ptype = ptype.ToLower();
                            ptype = ptype.Replace("cc", "c");
                            string[] sp = ptype.Split('c');
                            ptype = sp[0].Trim() + sp[1].Trim();
                            if (ptype.Contains("gas"))
                            {
                                ptype = ptype.Replace("gas", "G");

                            }
                            else
                            {
                                ptype = ptype.Replace("steam", "S");

                            }
                            ptype = ppid + "-" + ptype;

                            blocks[j, k] = ptype;

                            loopcount++;
                            if (loopcount != oDataTable.Rows.Count) k++;
                        }
                    }
                    else if (packageType == "Gas")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                       ppidpre + "'" + " and packageCode='" + packcode + "' and packageType ='Gas'ORDER BY UnitType ASC");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Gas", "G").Trim();
                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;
                    }
                    else if (packageType == "Steam")
                    {
                        oDataTable = utilities.returntbl("select UnitCode from dbo.UnitsDataMain where PPID='" +
                        ppidpre + "'" + " and packageCode='" + packcode + "' and packageType ='Steam'ORDER BY UnitType ASC");

                        Initial = oDataTable.Rows[0][0].ToString().Replace("Steam", "S").Trim();
                        string blockName = ppid.ToString() + "-" + Initial;

                        blocks[j, k] = blockName;
                    }


                }
            }
            return blocks;


        }

        private string FindNearManategh(string date)
        {
            DataTable odat = utilities.returntbl("Select distinct Date from Manategh where Date<='" + date + "'AND Code='R03' order by Date desc");
            return odat.Rows[0][0].ToString().Trim();
        }
        public DataTable[] OrderPackages7(DataSet myDs)
        {
            DataTable[] orderedPackages = new DataTable[1];
            int count = 0;


            DataTable baseplant = utilities.returntbl("select PPName from dbo.BaseData where Date ='" + smaxdate + "'order by BaseID desc");
            string[] s = baseplant.Rows[0][0].ToString().Trim().Split('-');
            DataTable idtable = utilities.returntbl("select PPID from dbo.PowerPlant  where PPName='" + s[0].Trim() + "'");

            string preplant = idtable.Rows[0][0].ToString().Trim();

            string packtype = "";
            try
            {
                packtype = s[1].Trim();
            }
            catch
            {

            }

            if (packtype == "Combined Cycle") packtype = "CC";

            if (packtype == "" || packtype == null)
            {
                foreach (string typeName in Enum.GetNames(typeof(PackageTypePriority)))
                {
                    //////////// have to make sure later whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                    if (myDs.Tables.Contains(typeName))
                        orderedPackages[0] = myDs.Tables[myDs.Tables.IndexOf(typeName)];
                }
            }
            else
            {
                //////////// have to make sure later whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                //DataTable dd = new DataTable(packtype);
                //string oo = packtype;
                //orderedPackages[0] = dd;

                foreach (string typeName in Enum.GetNames(typeof(PackageTypePriority)))
                {
                    //////////// have to make sure later whether the names are the same!!!!!!!!!!!!!!!!!!!!!
                    if (typeName == packtype)
                        orderedPackages[0] = myDs.Tables[myDs.Tables.IndexOf(typeName)];
                }


            }

            return orderedPackages;
        }

        //			cplex.setParam(IloCplex.DoubleParam.TiLim, 2000000);
        //			cplex.setParam(IloCplex.IntParam.ItLim,1000000000);
        //			cplex.setParam(IloCplex.DoubleParam.EpGap,0.00000001);
        //			cplex.setParam(IloCplex.DoubleParam.WorkMem,512000);
        //			cplex.setParam(IloCplex.StringParam.WorkDir,"C");
        //			cplex.setParam(IloCplex.IntParam.NodeFileInd,3);
        //			cplex.setParam(IloCplex.DoubleParam.TreLim,1e+76);
        //          cplex.setParam(IloCplex.DoubleParam.ObjULim,10e300);
        //           cplex.setParam(IloCplex.DoubleParam.EpGap, 1);
    }
}
